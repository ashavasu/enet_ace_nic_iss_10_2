/************************************************************************
 * Copyright (C) Future Software Limited,1997-98,2002
 *
 * $Id: napttmr.c,v 1.6 2013/10/25 10:50:23 siva Exp $
 *
 * Description: This has functions for Napt Timer Submodule
 *
 ************************************************************************/

#ifndef _NAPTTMR_C
#define _NAPTTMR_C
#include "natinc.h"
#include "natwincs.h"

PRIVATE tTimerListId NaptTimerListId;
/************************************************************************/
/*  Function Name : NaptInitTimer                                       */
/*  Description   : This function Initializes the Timer List.           */
/*  Parameter(s)  : VOID                                                */
/*  Return Values : OSIX_SUCCESS/OSIX_FAILURE                           */
/************************************************************************/
PUBLIC INT4
NaptInitTimer (VOID)
{
    UINT1               au1TaskName[NAT_TASK_NAME_LEN] = { NAT_ZERO };

    STRCPY (au1TaskName, (const UINT1 *) CFA_TASK_NAME);

    NaptTimerListId = (tTimerListId) NAPT_TIMER_LIST_ID;

    /* Creating TimerList & Registering callback function with the list */
    if (TmrCreateTimerList
        (au1TaskName, (UINT4) NAPT_TIMER_EXPIRY_EVENT, NULL,
         &NaptTimerListId) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Timer Init Failed \n");
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name : NaptDeInitTimer                                     */
/*  Description   :                                                     */
/*                : This function De-Initializes the Timer List.        */
/*  Parameter(s)  : VOID                                                */
/*  Return Values : OSIX_SUCCESS/OSIX_FAILURE                           */
/************************************************************************/

PUBLIC INT4
NaptDeInitTimer (VOID)
{

    if (TmrDeleteTimerList (NaptTimerListId) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Timer DeInit Failed \n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NaptStartTimer                                       */
/*  Description   : This function  starts the timer for the given value. */
/*  Parameter(s)  : pTimer - Pointer to the timer to be started          */
/*                : TimeOut -  Time out value                            */
/*  Return Values : OSIX_SUCCESS/OSIX_FAILURE                            */
/*************************************************************************/

INT4
NaptStartTimer (tNaptTimer * pTimer, UINT4 u4TimeOut)
{
    if (pTimer->u1TimerStatus == NAT_ENABLE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Timer Already Activated \n");
        return (OSIX_FAILURE);
    }
    /* Starting the Timer only if it is not started already */
    if (TmrStartTimer
        (NaptTimerListId, &pTimer->TimerNode,
         (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Start Timer Failed \n");
        return (OSIX_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_ENABLE;

    return (OSIX_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NaptStopTimer                                        */
/*  Description   :                                                      */
/*                : This procedure stops the timer 'pTimer' .            */
/*  Parameter(s)  :                                                      */
/*                : pTimer  -  pointer to the Timer block                */
/*  Return Values : VOID                                                 */
/*************************************************************************/

INT4
NaptStopTimer (tNaptTimer * pTimer)
{
    if (pTimer->u1TimerStatus == NAT_DISABLE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Timer Already DeActivated \n");
        return (OSIX_FAILURE);
    }

    if (TmrStopTimer (NaptTimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Stop Timer Failed \n");
        return (OSIX_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_DISABLE;

    return (OSIX_SUCCESS);
}

/*************************************************************************/
/*  Function Name : NaptRestartTimer                                     */
/*  Description   :                                                      */
/*                : This procedure restarts the timer 'pTimer' .         */
/*  Parameter(s)  :                                                      */
/*                : pTimer  -  pointer to the Timer block                */
/*  Return Values : VOID                                                 */
/*************************************************************************/

INT4
NaptReStartTimer (tNaptTimer * pTimer, UINT4 u4TimeOut)
{
    if (pTimer->u1TimerStatus == NAT_DISABLE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Timer Not Activated \n");
    }

    if (TmrStopTimer (NaptTimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Stop Timer Failed \n");
    }

    if (TmrStartTimer (NaptTimerListId, &pTimer->TimerNode,
                       (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) !=
        TMR_SUCCESS)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT",
                 "\n Napt Start Timer Failed \n");
        return (OSIX_FAILURE);
    }

    pTimer->u1TimerStatus = NAT_ENABLE;

    return (OSIX_SUCCESS);
}

/******************************************************************************
 *                                                                            *
 *  Function Name : NaptReConfigureTimer                                      * 
 *                                                                            * 
 *  Description   :                                                           *
 *                : This procedure reconfigures the timeout for all the       *
 *                  Napt Entries.                                             *
 *                                                                            *
 *  Parameter(s)  : u4Timeout                                                 * 
 *                                                                            *
 *  Return Values : OSIX_SUCCESS/OSIX_FAILURE                                 * 
 *                                                                            *
 ******************************************************************************/

INT4
NaptReConfigureTimer (INT4 i4IfNum, UINT4 u4Timeout)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];

    if (i4IfNum <= NAT_MAX_NUM_IF)
    {
        if (gapNatIfTable[i4IfNum] != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if (pStaticNaptEntryNode->NaptTimerNode.TimerNode.u4Data
                    != NAT_ZERO)
                {
                    if (TmrResizeTimer (NaptTimerListId,
                                        &(pStaticNaptEntryNode->NaptTimerNode.
                                          TimerNode),
                                        u4Timeout) != OSIX_SUCCESS)
                    {
                        return (OSIX_FAILURE);
                    }
                }
            }
        }
    }

    return (OSIX_SUCCESS);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name : NaptProcessTimeOut                                        */
/*  Description   :                                                           */
/*                : This procedure calls the appropriate procedures           */
/*                  to handle the timer expiry condition.                     */
/*  Parameter(s)  : None                                                      */
/*  Return Values : VOID                                                      */
/******************************************************************************/

PUBLIC VOID
NaptProcessTimeOut (tTimerListId TimerListId)
{
    tTmrAppTimer       *pTimer = NULL;
    tTmrAppTimer       *pListHead = NULL;
    tNaptTimer         *pNaptTmr = NULL;
    pListHead = TmrGetNextExpiredTimer (NaptTimerListId);

    UNUSED_PARAM (TimerListId);

    /* Searching in the Napt timer list for identifying the 
       Timer whose expiry has been reached */
    while (pListHead != NULL)
    {
        pTimer = pListHead;
        pNaptTmr = (tNaptTimer *) pTimer;

        switch (pNaptTmr->u1TimerId)
        {
            case NAPT_TIMER_ID:
                pNaptTmr->u1TimerStatus = NAT_DISABLE;
                NatLock ();
                NaptDeleteEntry (pNaptTmr);
                NatUnLock ();
                break;

            default:
                /* nattimeout process code can be included 
                   in case we don't want to have seperate 
                   timer list for Nat */
                break;
        }
        pListHead = TmrGetNextExpiredTimer (NaptTimerListId);
    }

    return;
}

/*****************************************************************************/
/*  Function Name :  NaptDeleteEntry ();                                     */
/*  Parameter(s)  :  (tNaptTimer *)                                          */
/*  Return Values :  OSIX_SUCCESS/OSIX_FAILURE                               */
/*****************************************************************************/
INT4
NaptDeleteEntry (tNaptTimer * pTimer)
{

    tStaticNaptEntry   *pStaticNaptEntry = NULL;
    INT4                i4NatStaticNaptInterfaceNum = NAT_ZERO;
    INT4                i4StartLocPort = NAT_ZERO;
    INT4                i4EndLocPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;

    pStaticNaptEntry = pTimer->pNaptBackPtr;
    /* Getting If Info from NaptTimer Node */
    i4NatStaticNaptInterfaceNum = pTimer->i4IfIndex;

    i4StartLocPort = pStaticNaptEntry->u2StartLocPort;
    i4EndLocPort = pStaticNaptEntry->u2EndLocPort;
    i4ProtocolNumber = pStaticNaptEntry->u2ProtocolNumber;

    if ((nmhSetNatStaticNaptEntryStatus (i4NatStaticNaptInterfaceNum,
                                         pStaticNaptEntry->u4LocIpAddr,
                                         i4StartLocPort,
                                         i4EndLocPort,
                                         i4ProtocolNumber,
                                         DESTROY)) == SNMP_FAILURE)
    {
        MOD_TRC (gu4NatTrc, OS_RESOURCE_TRC, "NAT", "\n Failed to "
                 "Delete \n");
        return (OSIX_FAILURE);
    }

#ifdef UPNP_WANTED
    SEND_NOTIFY_TO_CONTROL_POINTS ();
#endif

    return (OSIX_SUCCESS);

}
#endif /* _NAPTTMR_C */
