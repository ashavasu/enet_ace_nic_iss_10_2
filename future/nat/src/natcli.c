#ifndef __NATCLI_C__
#define __NATCLI_C__
/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natcli.c,v 1.38 2017/11/23 14:12:17 siva Exp $
 *
 * Description:This file contains CLI processing for NAT commands
 *
 *******************************************************************/
#include "natinc.h"
#include "natwincs.h"
#ifdef UPNP_WANTED
UINT4               gu4NatUpnpInit = UPNP_E_FAILURE;
#endif /* UPNP_WANTED */
#include "sipalg_inc.h"

INT4
cli_process_nat_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS] = { NULL };
    INT1                i1Argno = NAT_ZERO;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4IfNum = NAT_ZERO;
    INT4                i4AppLocalPort = NAT_ZERO;
    INT4                i4DesLen = NAT_ZERO;
    INT4                i4PortNoStart = NAT_ZERO;
    INT4                i4PortNoEnd = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    UINT4               u4NatInterface = NAT_ZERO;
    UINT4               u4NaptStatus = NAT_ZERO;
    UINT4               u4NatLocalIpAddr = NAT_ZERO;
    UINT4               u4NatTranslatedIpAddr = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    UINT4               u4Mask = NAT_ZERO;
    UINT4               u4ErrCode = NAT_ZERO;
    UINT4               u4Option = NAT_ZERO;
    UINT4               u4WanIfIpAddr = NAT_ZERO;
    INT4                i4TimeoutValue = NAT_ZERO;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    INT4                i4PolicyId = NAT_ZERO;
    UINT2               u2NatTranslatedPort = NAT_ZERO;
    UINT2               u2NatProtocolNumber = NAT_ZERO;
    UINT1              *pu1Inst = NULL;
    UINT4               u4AppType = NAT_ZERO;
    UINT1               au1AppName[CLI_NAT_APP_NAME_LEN] = { NAT_ZERO };
    UINT1               au1TrigOutPort[CLI_NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };
    UINT1               au1TrigInPort[CLI_NAT_MAX_PORT_STRING_LEN]
        = { NAT_ZERO };
    UINT2               u2Protocol = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE *pStaticNaptDes = NULL;

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    va_start (ap, u4Command);
    /* second arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store fourteen arguements at the max. This is because fwl commands do not
     * take more than fourteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (TRUE)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == NAT_CLI_MAX_COMMANDS_PARAMS)
        {
            break;
        }
    }

    va_end (ap);
    i4IfNum = CLI_GET_IFINDEX ();

    CliRegisterLock (CliHandle, NatLock, NatUnLock);
    NAT_LOCK ();

    switch (u4Command)
    {
        case CLI_SET_NAT_STATUS:

            i4RetVal = NatCliSetGlobalNatStatus (CliHandle, i4IfNum,
                                                 CLI_PTR_TO_U4 (args
                                                                [NAT_INDEX_0]));
            break;

        case CLI_ADD_VIRTUAL_SERVER:
            /* args[0] = local ip address
             * args[1] = wan port number for statndard applications
             *           for ex. 80 for http, 21 for ftp, 25 for telnet etc.
             * args[2] = wan port number for other applications.
             * args[3] = description
             * args[4] = port no on which the server is running on the 
             *           local machine
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            u4NatInterface = (UINT4) i4IfNum;
            u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
            u4AppType = CLI_PTR_TO_U4 (args[NAT_INDEX_1]);

            if (u4AppType == NAT_OTHER_APP_PORT)
            {
                i4StartLocalPort = CLI_PTR_TO_I4 (args[NAT_INDEX_2]);
            }
            else
            {
                if (CLI_PTR_TO_U4 (args[NAT_INDEX_2]) != NAT_ZERO)
                {
                    i4StartLocalPort = CLI_PTR_TO_I4 (args[NAT_INDEX_2]);
                }
                else
                {
                    i4StartLocalPort = CLI_PTR_TO_I4 (args[NAT_INDEX_1]);
                }
            }

            if (args[NAT_INDEX_3] != NULL)
            {
                i4DesLen = (INT4) STRLEN (args[NAT_INDEX_3]);
            }
            else
            {
                i4DesLen = NAT_ZERO;
            }
            if ((pStaticNaptDes = allocmem_octetstring (i4DesLen)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            MEMCPY (pStaticNaptDes->pu1_OctetList, args[NAT_INDEX_3], i4DesLen);
            pStaticNaptDes->i4_Length = i4DesLen;

            if (args[NAT_INDEX_4] != NULL)
            {
                i4AppLocalPort = CLI_PTR_TO_I4 (args[NAT_INDEX_4]);
            }
            else
            {
                i4AppLocalPort = i4StartLocalPort;
            }

            if (args[NAT_INDEX_5] != NULL)
            {
                u2Protocol = (*(UINT2 *) args[NAT_INDEX_5]);
            }
            else
            {
                u2Protocol = NAT_PROTO_ANY;
            }

            i4RetVal = NatCliAddVirtualServer (CliHandle,
                                               u4NatInterface,
                                               u4NatLocalIpAddr,
                                               i4StartLocalPort,
                                               i4AppLocalPort,
                                               u2Protocol, pStaticNaptDes);

            free_octetstring (pStaticNaptDes);
            break;

        case CLI_ENABLE_VIRTUAL_SERVER:
            /* args[0] = local ip address
             * args[1] = application name - for ex. http, ftp, telnet etc
             * args[2] = port no  - for ex. http, ftp, telnet etc
             * args[3] = all option indicator
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4AppType = CLI_PTR_TO_U4 (args[NAT_INDEX_1]);
            if (u4AppType == NAT_VIRTUAL_SERVER_ALL)
            {
                i4RetVal = NatCliEnableAllVirtualServer (CliHandle);
            }
            else
            {
                u4NatInterface = (UINT4) i4IfNum;
                u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
                i4StartLocalPort = (INT4) u4AppType;
                i4EndLocalPort = i4StartLocalPort;

                i4RetVal = NatCliEnableVirtualServer (CliHandle, u4NatInterface,
                                                      u4NatLocalIpAddr,
                                                      i4StartLocalPort,
                                                      i4EndLocalPort);
            }

            break;

        case CLI_DISABLE_VIRTUAL_SERVER:
            /* args[0] = local ip address
             * args[1] = application name - for ex. http, ftp, telnet etc
             * args[2] = port no  - for ex. http, ftp, telnet etc
             * args[3] = all option indicator
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4AppType = CLI_PTR_TO_U4 (args[NAT_INDEX_1]);
            if (u4AppType == NAT_VIRTUAL_SERVER_ALL)
            {
                i4RetVal = NatCliDisableAllVirtualServer (CliHandle);
            }
            else
            {
                u4NatInterface = (UINT4) i4IfNum;
                u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
                i4StartLocalPort = (INT4) u4AppType;
                i4EndLocalPort = i4StartLocalPort;

                i4RetVal = NatCliDisableVirtualServer (CliHandle,
                                                       u4NatInterface,
                                                       u4NatLocalIpAddr,
                                                       i4StartLocalPort,
                                                       i4EndLocalPort);
            }

            break;

        case CLI_DEL_VIRTUAL_SERVER:
            /* args[0] = local ip address
             * args[1] = port number on which the server is listening
             *           on the local machine
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4AppType = CLI_PTR_TO_U4 (args[NAT_INDEX_1]);

            if (u4AppType == NAT_VIRTUAL_SERVER_ALL)
            {
                i4RetVal = NatCliDelAllVirtualServer (CliHandle, i4IfNum);
            }
            else
            {
                u4NatInterface = (UINT4) i4IfNum;
                u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
                i4StartLocalPort = (INT4) u4AppType;
                i4EndLocalPort = i4StartLocalPort;
                u2Protocol = (*(UINT2 *) args[NAT_INDEX_2]);

                i4RetVal = NatCliDelVirtualServer (CliHandle, u4NatInterface,
                                                   u4NatLocalIpAddr,
                                                   i4StartLocalPort,
                                                   i4EndLocalPort,
                                                   (INT4) u2Protocol);
            }

            break;

        case CLI_ADD_NAT_IFACE_ENTRY:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NaptStatus = CLI_PTR_TO_U4 (args[NAT_INDEX_0]);

            u4WanIfIpAddr = NatGetWanInterfaceIpAddr ((UINT4) i4IfNum);

            if (u4WanIfIpAddr == NAT_ZERO)
            {
                CLI_SET_ERR (CLI_NAT_ERR_IP_NOT_SET_FOR_WAN);
                i4RetVal = CLI_FAILURE;
                break;
            }

            i4RetVal = NatCliAddNatInterfaceEntry (CliHandle, u4NaptStatus,
                                                   i4IfNum);
            break;

        case CLI_ADD_NAT_STATIC_ENTRY:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatInterface = (UINT4) i4IfNum;
            u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
            u4NatTranslatedIpAddr = (*args[NAT_INDEX_1]);

            i4RetVal = NatCliAddNatStaticEntry (CliHandle, u4NatInterface,
                                                u4NatLocalIpAddr,
                                                u4NatTranslatedIpAddr);
            break;

        case CLI_DEL_NAT_STATIC_ENTRY:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatLocalIpAddr = (*args[NAT_INDEX_0]);

            i4RetVal = NatCliDelNatStaticEntry (CliHandle, (UINT4) i4IfNum,
                                                u4NatLocalIpAddr);
            break;

        case CLI_ADD_PORT_TRIGGER:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (STRLEN (args[NAT_INDEX_2]) >= CLI_NAT_MAX_PORT_STRING_LEN)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_PORT_RANGE);
                i4RetVal = CLI_FAILURE;
                break;
            }
            else if (STRLEN (args[NAT_INDEX_3]) >= CLI_NAT_MAX_PORT_STRING_LEN)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_PORT_RANGE);
                i4RetVal = CLI_FAILURE;
                break;
            }

            else if (STRLEN (args[NAT_INDEX_0]) >= CLI_NAT_APP_NAME_LEN)
            {
                CliPrintf (CliHandle,
                           "\r%% Maximum length of NAT application name can be 64"
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            STRNCPY (au1AppName, args[NAT_INDEX_0],
                     CLI_NAT_APP_NAME_LEN - NAT_ONE);
            u2Protocol = (UINT2) CLI_PTR_TO_U4 (args[NAT_INDEX_1]);
            STRNCPY (au1TrigOutPort, args[NAT_INDEX_2],
                     CLI_NAT_MAX_PORT_STRING_LEN - NAT_ONE);
            STRNCPY (au1TrigInPort, args[NAT_INDEX_3],
                     CLI_NAT_MAX_PORT_STRING_LEN - NAT_ONE);
            /*else
               { */
            i4RetVal = NatCliAddPortTrigger (CliHandle, au1AppName,
                                             au1TrigOutPort, au1TrigInPort,
                                             u2Protocol);
            /* } */
            break;

        case CLI_DEL_PORT_TRIGGER:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (STRLEN (args[NAT_INDEX_0]) >= CLI_NAT_APP_NAME_LEN)
            {
                CliPrintf (CliHandle,
                           "\r%% Maximum length of NAT application name can be 64"
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;

            }
            STRNCPY (au1AppName, args[NAT_INDEX_0],
                     CLI_NAT_APP_NAME_LEN - NAT_ONE);

            i4RetVal = NatCliDelPortTrigger (CliHandle, au1AppName);

            break;

        case CLI_ADD_NAT_GLOBAL_ENTRY:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatInterface = (UINT4) i4IfNum;
            u4IpAddr = CLI_INET_ADDR (args[NAT_INDEX_0]);
            u4IpAddr = *args[NAT_INDEX_0];
            u4Mask = CLI_INET_ADDR (args[NAT_INDEX_1]);
            u4Mask = *args[NAT_INDEX_1];
            i4RetVal = NatCliAddNatGlobalEntry (CliHandle, u4NatInterface,
                                                u4IpAddr, u4Mask);

            break;

        case CLI_DEL_NAT_GLOBAL_ENTRY:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatInterface = (UINT4) i4IfNum;
            u4IpAddr = CLI_INET_ADDR (args[NAT_INDEX_0]);
            u4IpAddr = *args[NAT_INDEX_0];
            i4RetVal = NatCliDelNatGlobalEntry (CliHandle, u4NatInterface,
                                                u4IpAddr);
            break;

        case CLI_ADD_PORT_RANGE_FORWARDING:
            /* args[0] = local ip address
             * args[1] = Protocol name
             * args[2] = Start port number
             * args[3] = End port number
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatInterface = (UINT4) i4IfNum;
            u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
            u2NatProtocolNumber = (UINT2) CLI_PTR_TO_U4 (args[NAT_INDEX_1]);
            i4StartLocalPort = (INT4) (*args[NAT_INDEX_2]);
            i4EndLocalPort = (INT4) (*args[NAT_INDEX_3]);
            if (CfaIpIfIsLocalNet (u4NatLocalIpAddr) == OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_IP_INVALID);
                i4RetVal = CLI_FAILURE;
            }

            u4NatTranslatedIpAddr = NAT_ZERO;
            /*
             * If the range is same then translated port will be
             * Start Local port of the portrange else it will be
             * zero which gets filled during packet processing
             */
            if (i4StartLocalPort == i4EndLocalPort)
            {
                u2NatTranslatedPort = (UINT2) i4StartLocalPort;
            }
            if ((i4StartLocalPort > i4EndLocalPort))
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_PORT_RANGE);
                i4RetVal = CLI_FAILURE;
            }
            else
            {
                i4RetVal = NatCliAddPortRangeForwarding (CliHandle,
                                                         u4NatInterface,
                                                         u4NatLocalIpAddr,
                                                         u4NatTranslatedIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         u2NatTranslatedPort,
                                                         u2NatProtocolNumber);
            }
            break;

        case CLI_DEL_PORT_RANGE_FORWARDING:
            /* args[0] = local ip address
             * args[1] = Protocol Number
             * args[2] = Start port number
             * args[3] = End port number
             */
            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured "
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4NatInterface = (UINT4) i4IfNum;
            u4NatLocalIpAddr = (*args[NAT_INDEX_0]);
            u2NatProtocolNumber = (UINT2) CLI_PTR_TO_U4 (args[NAT_INDEX_1]);
            i4PortNoStart = (INT4) (*args[NAT_INDEX_2]);
            i4PortNoEnd = (INT4) (*args[NAT_INDEX_3]);

            if ((i4PortNoStart > i4PortNoEnd))
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_PORT_RANGE);
                i4RetVal = CLI_FAILURE;
            }
            else
            {
                i4StartLocalPort = i4PortNoStart;
                i4EndLocalPort = i4PortNoEnd;

                i4RetVal = NatCliDelPortRangeForwarding (CliHandle,
                                                         u4NatInterface,
                                                         u4NatLocalIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         u2NatProtocolNumber);
            }

            break;

        case CLI_NAT_STATUS_IFACE:

            i4IfNum = CLI_GET_IFINDEX ();

            if (i4IfNum <= NAT_ZERO)
            {
                CliPrintf (CliHandle, "\r%% WAN Interface is not configured"
                           "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }

            i4RetVal = NatCliSetInterfaceNatStatus (CliHandle, i4IfNum,
                                                    CLI_PTR_TO_U4 (args
                                                                   [NAT_INDEX_0]));
            break;

        case CLI_SET_NAT_TIMEOUT:
            u4Option = CLI_PTR_TO_U4 (args[NAT_INDEX_0]);
            i4TimeoutValue = (*(INT4 *) args[NAT_INDEX_1]);

            i4RetVal = NatCliSetNatTimeout (CliHandle, u4Option,
                                            i4TimeoutValue);
            break;

        case CLI_RESET_NAT_TIMEOUT:
            u4Option = CLI_PTR_TO_U4 (args[NAT_INDEX_0]);

            if (u4Option == CLI_NAT_IDLE_TIMEOUT)
            {
                i4RetVal = NatCliSetNatTimeout (CliHandle, u4Option,
                                                NAT_DEF_IDLE_TIMEOUT);
            }
            if (u4Option == CLI_NAT_TCP_TIMEOUT)
            {
                i4RetVal = NatCliSetNatTimeout (CliHandle, u4Option,
                                                NAT_DEF_TCP_TIMEOUT);
            }
            if (u4Option == CLI_NAT_UDP_TIMEOUT)
            {
                i4RetVal = NatCliSetNatTimeout (CliHandle, u4Option,
                                                NAT_DEF_UDP_TIMEOUT);
            }

            break;

        case CLI_ADD_NAT_STATIC_POLICY:
            /* CLI command : static inside <policy_id> access-list <filterno> */
            /* args[0] => direction (inside/outside)
             * args[1] => Policy Identifier. 
             * args[2] => Policy Filter Identifier for inside.
             *            Translated IP address for outside.  
             */
            i4PolicyId = *(INT4 *) (args[NAT_INDEX_1]);

            if (NAT_INSIDE == CLI_PTR_TO_I4 (args[NAT_INDEX_0]))
            {
                STRCPY (au1AclName, args[NAT_INDEX_2]);
                i4RetVal =
                    NatCliConfigurePolicy (CliHandle, NAT_STATIC_POLICY,
                                           i4PolicyId, au1AclName,
                                           NAT_STATUS_CREATE_AND_WAIT);
            }
            else
            {
                u4NatTranslatedIpAddr = (*args[NAT_INDEX_2]);
                i4RetVal =
                    NatCliUpdatePolicy (CliHandle, NAT_STATIC_POLICY,
                                        i4PolicyId, u4NatTranslatedIpAddr);
            }
            break;

        case CLI_ADD_NAT_DYNAMIC_POLICY:
            /* CLI command : nat inside <policy_id> access-list <filterno> */
            /* args[0] => direction (inside/outside)
             * args[1] => Policy Identifier. 
             * args[2] => Policy Filter Identifier for inside.
             *            Translated IP address for outside.  
             */
            i4PolicyId = *(INT4 *) (args[NAT_INDEX_1]);

            if (NAT_INSIDE == CLI_PTR_TO_I4 (args[NAT_INDEX_0]))
            {
                STRCPY (au1AclName, args[NAT_INDEX_2]);
                i4RetVal =
                    NatCliConfigurePolicy (CliHandle, NAT_DYNAMIC_POLICY,
                                           i4PolicyId, au1AclName,
                                           NAT_STATUS_CREATE_AND_WAIT);
            }
            else
            {
                u4NatTranslatedIpAddr = (*args[NAT_INDEX_2]);
                i4RetVal =
                    NatCliUpdatePolicy (CliHandle, NAT_DYNAMIC_POLICY,
                                        i4PolicyId, u4NatTranslatedIpAddr);
            }
            break;

        case CLI_DEL_NAT_STATIC_POLICY:
            /* CLI command:no static inside <policy_id> access-list <filter_Id>
             * args[0] => Policy Identifier. 
             * args[1] => Filter Identifier. 
             */
            i4PolicyId = *(INT4 *) (args[NAT_INDEX_0]);
            if (STRLEN (args[NAT_INDEX_1]) >= NAT_MAX_ACL_NAME_LEN)
            {
                CliPrintf (CliHandle,
                           "\r%% Maximum length of ACL name can be 36" "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            STRNCPY (au1AclName, args[NAT_INDEX_1],
                     NAT_MAX_ACL_NAME_LEN - NAT_ONE);

            i4RetVal = NatCliConfigurePolicy (CliHandle, NAT_STATIC_POLICY,
                                              i4PolicyId, au1AclName,
                                              NAT_STATUS_DESTROY);
            break;

        case CLI_DEL_NAT_DYNAMIC_POLICY:
            /* CLI command: no nat inside <policy_id> access-list <filter_Id>
             * args[0] => Policy Identifier. 
             * args[1] => Filter Identifier. 
             */
            i4PolicyId = *(INT4 *) (args[NAT_INDEX_0]);
            if (STRLEN (args[NAT_INDEX_1]) >= NAT_MAX_ACL_NAME_LEN)
            {
                CliPrintf (CliHandle,
                           "\r%% Maximum length of ACL name can be 36" "!\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            STRNCPY (au1AclName, args[NAT_INDEX_1],
                     NAT_MAX_ACL_NAME_LEN - NAT_ONE);

            i4RetVal = NatCliConfigurePolicy (CliHandle, NAT_DYNAMIC_POLICY,
                                              i4PolicyId, au1AclName,
                                              NAT_STATUS_DESTROY);
            break;

        case CLI_SHOW_NAT_CONFIG:

            i4RetVal = NatCliShowNatConfig (CliHandle);

            break;

        case CLI_SHOW_NAT_GLOBAL_TABLE:

            i4RetVal = NatCliShowGlobalTable (CliHandle);

            break;

        case CLI_SHOW_NAT_STATIC_TABLE:

            i4RetVal = NatCliShowStaticTable (CliHandle);

            break;

        case CLI_SHOW_NAT_TRANSLATIONS:

            i4RetVal = NatCliShowDynamicTable (CliHandle);

            break;

        case CLI_SHOW_VIRTUAL_SERVERS:

            i4RetVal = NatCliShowVirtualServers (CliHandle);

            break;

        case CLI_SHOW_PORT_RANGE_FORWARDING:

            i4RetVal = NatCliShowPortRangeForwarding (CliHandle);

            break;

        case CLI_SHOW_NAT_IFACE_CONFIG:

            i4RetVal = NatCliProcessShowNatIfaceConfig (CliHandle);

            break;

        case CLI_SHOW_PORT_TRIGGER:

            NatCliShowPortTrigger (CliHandle);
            i4RetVal = CLI_SUCCESS;

            break;
        case CLI_SHOW_PORT_TRIGGER_RESERVED:

            NatCliShowPortTriggerReserved (CliHandle);
            i4RetVal = CLI_SUCCESS;

            break;
#ifndef SECURITY_KERNEL_MAKE_WANTED
        case CLI_SHOW_SIP_NAT_STATICNAPT:

            NatCliSipShowStaticNapt (CliHandle);
            break;
#endif
        case CLI_SHOW_SIP_NAT_PARTIAL:

            NatCliSipShowPartial (CliHandle);
            break;
        case CLI_SET_NAT_TRACE_LEVEL:

            i4RetVal = NatSetDebugLevel (CLI_PTR_TO_U4 (args[NAT_INDEX_0]));
            break;

        case CLI_RESET_NAT_TRACE_LEVEL:

            i4RetVal = NatResetDebugLevel (CLI_PTR_TO_U4 (args[NAT_INDEX_0]));
            break;

        case CLI_SHOW_NAT_SIP_ALG_PORT:
            i4RetVal = NatCliShowSipAlgPort (CliHandle);
            break;

        case CLI_SHOW_NAT_SIP_ALG_PARTIAL_ENTRY_TIMER:
            i4RetVal = NatCliShowSipAlgPartialEntryTimer (CliHandle);
            break;

        case CLI_SET_NAT_SIP_ALG_PORT:
            i4RetVal = NatCliSetSipAlgPort (CliHandle,
                                            CLI_PTR_TO_U4 (args[NAT_INDEX_0]));
            break;

        case CLI_SET_NAT_SIP_ALG_PARTIAL_ENTRY_TIMER:
            i4RetVal = NatCliSetSipAlgPartialEntryTimer (CliHandle,
                                                         CLI_PTR_TO_U4 (args
                                                                        [NAT_INDEX_0]));
            break;
        case CLI_SHOW_NAT_POLICY_TABLE:
            /* CLI Command : show ip nat policy */
            i4RetVal = NatCliShowPolicyTable (CliHandle);
            break;

        default:
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_CMD);
            i4RetVal = CLI_FAILURE;
            break;
    }                            /* switch (u4Command) */

    /* Display the error message */
    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > NAT_ZERO) && (u4ErrCode < CLI_NAT_ERR_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", NatCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (NAT_ZERO);
    }
    NAT_UNLOCK ();

    CLI_SET_CMD_STATUS ((UINT4) i4RetVal);
    CliUnRegisterLock (CliHandle);
    if (pu1Inst == NULL)
    {
        return i4RetVal;
    }
    return i4RetVal;

}                                /* End cli_process_nat_cmd() */

/**************************************************************************
 * Function Name :  NatCliSetGlobalNatStatus                               *
 *                                                                         *
 * Description   :  This function the is used to set the global NAT status *
 *                  which enables or disables NAT globally                 *
 *                                                                         *
 * Input (s)     :  CliHandle - Handle to CLI session                      *
 *                  u4NatStatus - Flag to set NAT status                   *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
 ***************************************************************************/
INT4
NatCliSetGlobalNatStatus (tCliHandle CliHandle, INT4 i4IfNum, UINT4 u4NatStatus)
{
    UINT4               u4Error = SNMP_SUCCESS;
    INT4                i4Val = NAT_ENABLE;

    UNUSED_PARAM (i4IfNum);
#ifdef UPNP_WANTED
    CHR1               *pu1IpAddress = NULL;
    CHR1               *pu1DescDocName = NULL;
    UINT2               u2Port = NAT_ZERO;
    CHR1               *pu1WebDirPath = NULL;
#endif

    nmhGetNatEnable (&i4Val);
    if (nmhTestv2NatEnable (&u4Error, (INT4) u4NatStatus) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_STATUS);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetNatEnable ((INT4) u4NatStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

#ifdef UPNP_WANTED
    if (u4NatStatus == CLI_NAT_ENABLE)
    {
        if (gu4NatUpnpInit == UPNP_E_FAILURE)
        {
            u4Error = UpnpNatServiceStart (pu1IpAddress, u2Port,
                                           pu1DescDocName, pu1WebDirPath);
            CliPrintf (NAT_TRC_ON, "UPnPServiceStart status : %d\r\n", u4Error);
            if (u4Error != UPNP_E_SUCCESS)
            {
                UpnpNatDeviceStop ();
            }
            else
            {
                gu4NatUpnpInit = UPNP_E_SUCCESS;
            }
        }
    }
    else
    {
        if (gu4NatUpnpInit == UPNP_E_SUCCESS)
        {
            UpnpNatDeviceStop ();
            gu4NatUpnpInit = UPNP_E_FAILURE;
        }
    }
#endif /*UPNP_WANTED */
    /* Has been moved to Interface Specific Nat */
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliAddVirtualServer                                 *
*                                                                         *
* Description   :  This function adds a virtual server.                   *
*                  If an entry already exists with the given indices,     *
*                  it modifies the entry.                                 *
*                                                                         *
* Input (s)     :  CliHandle                                              *
*                  u4Interface                                            *
*                  u4LocalIpAddr                                          *
*                  u2StartLocalPort                                       *
*                  u2AppLocalPort                                         *
*                  pStaticNaptDes                                         *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS on success                                 *
*                  CLI_FAILURE on failure                                 *
*                                                                         *
***************************************************************************/
INT4
NatCliAddVirtualServer (tCliHandle CliHandle, UINT4 u4Interface,
                        UINT4 u4LocalIpAddr, INT4 i4StartLocalPort,
                        INT4 i4AppLocalPort, UINT2 u2Protocol,
                        tSNMP_OCTET_STRING_TYPE * pStaticNaptDes)
{
    UINT4               u4Error = NAT_ZERO;
    INT4                i4RowStatus = NAT_ZERO;

    UINT4               u4TranslatedIpAddr = NAT_ZERO;
    INT4                i4Interface = (INT4) u4Interface;
    INT4                i4ProtocolNumber = NAT_PROTO_ANY;
    INT4                i4TranslatedPort = i4StartLocalPort;
    INT4                i4CurrTranslatedPort = NAT_ZERO;
    INT1                i1RetVal = NAT_ZERO;
    if (CfaGetIfIpAddr ((INT4) u4Interface, &u4TranslatedIpAddr) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
        return CLI_FAILURE;
    }

    i4ProtocolNumber = (INT4) u2Protocol;

    /* Corresponding to the given Local Ip and interface index,
     * a row may or may not be existing in the static table.
     * If a row already exists, modify the translated local Ip.
     * If a row doesnt exist, create one and set the tranlated local Ip */

    if (nmhGetNatStaticNaptEntryStatus (i4Interface, u4LocalIpAddr,
                                        i4AppLocalPort, i4AppLocalPort,
                                        i4ProtocolNumber, &i4RowStatus)
        == SNMP_FAILURE)
    {
        /* NEW ENTRY */
        if (nmhTestv2NatStaticNaptEntryStatus (&u4Error, i4Interface,
                                               u4LocalIpAddr, i4AppLocalPort,
                                               i4AppLocalPort, i4ProtocolNumber,
                                               NAT_STATUS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            switch (u4Error)
            {
                case SNMP_ERR_RESOURCE_UNAVAILABLE:
                    CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
                    break;
                case SNMP_ERR_INCONSISTENT_VALUE:
                    CLI_SET_ERR (CLI_NAT_ERR_CONFLICTING_PORT_RANGE);
                    break;
                case SNMP_ERR_WRONG_VALUE:
                    CLI_SET_ERR (CLI_NAT_ERR_INVALID_LOCAL_IP);
                    break;
                case SNMP_ERR_BAD_VALUE:
                    CLI_SET_ERR (CLI_NAT_ERR_IP_CONFLICT);
                    break;
                default:
                    break;
            }
            return CLI_FAILURE;
        }

        if (nmhTestv2NatStaticNaptTranslatedLocalPort (&u4Error,
                                                       i4Interface,
                                                       u4LocalIpAddr,
                                                       i4AppLocalPort,
                                                       i4AppLocalPort,
                                                       i4ProtocolNumber,
                                                       i4TranslatedPort)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_SAME_GLOBAL_PORT);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptEntryStatus (i4Interface,
                                            u4LocalIpAddr,
                                            i4AppLocalPort,
                                            i4AppLocalPort,
                                            i4ProtocolNumber,
                                            NAT_STATUS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptTranslatedLocalIp (i4Interface,
                                                  u4LocalIpAddr,
                                                  i4AppLocalPort,
                                                  i4AppLocalPort,
                                                  i4ProtocolNumber,
                                                  u4TranslatedIpAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptTranslatedLocalPort (i4Interface,
                                                    u4LocalIpAddr,
                                                    i4AppLocalPort,
                                                    i4AppLocalPort,
                                                    i4ProtocolNumber,
                                                    i4TranslatedPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptDescription (i4Interface,
                                            u4LocalIpAddr,
                                            i4AppLocalPort,
                                            i4AppLocalPort,
                                            i4ProtocolNumber,
                                            pStaticNaptDes) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2NatStaticNaptEntryStatus (&u4Error,
                                               i4Interface,
                                               u4LocalIpAddr,
                                               i4AppLocalPort,
                                               i4AppLocalPort,
                                               i4ProtocolNumber,
                                               NAT_STATUS_ACTIVE) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_SAME_GLOBAL_PORT);
            i1RetVal = nmhSetNatStaticNaptEntryStatus (i4Interface,
                                                       u4LocalIpAddr,
                                                       i4AppLocalPort,
                                                       i4AppLocalPort,
                                                       i4ProtocolNumber,
                                                       NAT_STATUS_DESTROY);
            UNUSED_PARAM (i1RetVal);
            return CLI_FAILURE;
        }

        /* Activate the virtual server entry */
        if (nmhSetNatStaticNaptEntryStatus (i4Interface,
                                            u4LocalIpAddr,
                                            i4AppLocalPort,
                                            i4AppLocalPort,
                                            i4ProtocolNumber,
                                            NAT_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    else
    {
        /* EXISTING ENTRY */
        if (i4RowStatus == NAT_STATUS_ACTIVE)
        {
            if (nmhSetNatStaticNaptEntryStatus (i4Interface,
                                                u4LocalIpAddr,
                                                i4AppLocalPort,
                                                i4AppLocalPort,
                                                i4ProtocolNumber,
                                                NAT_STATUS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetNatStaticNaptTranslatedLocalIp (i4Interface,
                                                      u4LocalIpAddr,
                                                      i4AppLocalPort,
                                                      i4AppLocalPort,
                                                      i4ProtocolNumber,
                                                      u4TranslatedIpAddr)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            /* Store the current value of global port */
            if (nmhGetNatStaticNaptTranslatedLocalPort (i4Interface,
                                                        u4LocalIpAddr,
                                                        i4AppLocalPort,
                                                        i4AppLocalPort,
                                                        i4ProtocolNumber,
                                                        &i4CurrTranslatedPort)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetNatStaticNaptTranslatedLocalPort (i4Interface,
                                                        u4LocalIpAddr,
                                                        i4AppLocalPort,
                                                        i4AppLocalPort,
                                                        i4ProtocolNumber,
                                                        i4TranslatedPort)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2NatStaticNaptEntryStatus (&u4Error,
                                                   i4Interface,
                                                   u4LocalIpAddr,
                                                   i4AppLocalPort,
                                                   i4AppLocalPort,
                                                   i4ProtocolNumber,
                                                   NAT_STATUS_ACTIVE) ==
                SNMP_FAILURE)
            {
                /* Configure the existing entry with the current global port 
                 * value and make it active */
                if (nmhSetNatStaticNaptTranslatedLocalPort (i4Interface,
                                                            u4LocalIpAddr,
                                                            i4AppLocalPort,
                                                            i4AppLocalPort,
                                                            i4ProtocolNumber,
                                                            i4CurrTranslatedPort)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                }
                if (nmhSetNatStaticNaptEntryStatus (i4Interface,
                                                    u4LocalIpAddr,
                                                    i4AppLocalPort,
                                                    i4AppLocalPort,
                                                    i4ProtocolNumber,
                                                    NAT_STATUS_ACTIVE)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                }

                CLI_SET_ERR (CLI_NAT_ERR_SAME_GLOBAL_PORT);
                return CLI_FAILURE;
            }

            if (nmhSetNatStaticNaptDescription (i4Interface,
                                                u4LocalIpAddr,
                                                i4AppLocalPort,
                                                i4AppLocalPort,
                                                i4ProtocolNumber,
                                                pStaticNaptDes) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetNatStaticNaptEntryStatus (i4Interface,
                                                u4LocalIpAddr,
                                                i4AppLocalPort,
                                                i4AppLocalPort,
                                                i4ProtocolNumber,
                                                NAT_STATUS_ACTIVE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }
        else
        {
            CLI_SET_ERR (CLI_NAT_ERR_ENTRY_NOT_ACTIVE);
            return CLI_FAILURE;
        }
    }
}

/**************************************************************************
 * Function Name :  NatCliEnableVirtualServer                              *
 *                                                                         *
 * Description   :  This function enables the virtual server               *
 *                                                                         *
 * Input (s)     :  pu1NatInput - Contains the indices of the row to       *
 *                                be deleted.                              *
 * Output (s)    :  None.                                                   *
 *                                                                         *
 * Returns       :  None.                                                  *
 ***************************************************************************/
INT1
NatCliEnableVirtualServer (tCliHandle CliHandle,
                           UINT4 u4Interface,
                           UINT4 u4LocalIpAddr,
                           INT4 i4StartLocalPort, INT4 i4EndLocalPort)
{
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_PROTO_ANY;
    UINT4               u4Error = NAT_ZERO;
    INT4                i4TranslatedPort = NAT_ZERO;

    i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                 u4LocalIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber, &i4Status);

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        return CLI_FAILURE;
    }

    if (i4Status == NAT_STATUS_NOT_READY)
    { /** Try configuring the Translated Local IP and Translated Local Port**/
        UINT4               u4IpAddr = NAT_ZERO;
        if (CfaGetIfIpAddr ((INT4) u4Interface, &u4IpAddr) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((u4IpAddr == NAT_ZERO) ||
            (nmhSetNatStaticNaptTranslatedLocalIp
             ((INT4) u4Interface,
              u4LocalIpAddr,
              i4StartLocalPort,
              i4EndLocalPort,
              i4ProtocolNumber,
              u4IpAddr) == SNMP_FAILURE)
            ||
            (nmhSetNatStaticNaptTranslatedLocalPort
             ((INT4) u4Interface,
              u4LocalIpAddr,
              i4StartLocalPort,
              i4EndLocalPort,
              i4ProtocolNumber, i4TranslatedPort) == SNMP_FAILURE))
        {
            CLI_SET_ERR (CLI_NAT_ERR_WAN_IF_NOT_READY);
            return CLI_FAILURE;
        }
    }

    if (i4Status != NAT_STATUS_ACTIVE)
    {
        if (nmhTestv2NatStaticNaptEntryStatus (&u4Error,
                                               (INT4) u4Interface,
                                               u4LocalIpAddr,
                                               i4StartLocalPort,
                                               i4EndLocalPort,
                                               i4ProtocolNumber,
                                               NAT_STATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_SAME_GLOBAL_PORT);
            return CLI_FAILURE;
        }

        i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                     u4LocalIpAddr,
                                                     i4StartLocalPort,
                                                     i4EndLocalPort,
                                                     i4ProtocolNumber,
                                                     NAT_STATUS_ACTIVE);

    }

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name : NatCliEnableAllVirtualServer                           *
 *                                                                        *
 * Description   : This function enables all virtual servers              *
 *                                                                        *
 * Input (s)     : CliHandle                                              *
 *
 * Output (s)    : None                                                   *
 *
 * Returns       : CLI_SUCCESS on Success , CLI_FAILURE on Failure
 * ***********************************************************************/

INT1
NatCliEnableAllVirtualServer (tCliHandle CliHandle)
{
    INT4                i4NextIfNum = NAT_ZERO;
    UINT4               u4NextLocIpAddr = NAT_ZERO;
    INT4                i4NextStartLocalPort = NAT_ZERO;
    INT4                i4NextEndLocalPort = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    UINT4               u4Interface = NAT_ZERO;
    UINT4               u4LocalIpAddr = NAT_ZERO;
    UINT4               u4Error = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;

    if ((nmhGetFirstIndexNatStaticNaptTable (&i4NextIfNum,
                                             &u4NextLocIpAddr,
                                             &i4NextStartLocalPort,
                                             &i4NextEndLocalPort,
                                             &i4NextProtocolNumber)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    else
    {
        do
        {
            u4Interface = (UINT4) i4NextIfNum;
            u4LocalIpAddr = u4NextLocIpAddr;
            i4StartLocalPort = i4NextStartLocalPort;
            i4EndLocalPort = i4NextEndLocalPort;
            i4ProtocolNumber = i4NextProtocolNumber;
            i4RetValue = SNMP_FAILURE;
            i4Status = NAT_ZERO;

            i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                         u4LocalIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         i4ProtocolNumber,
                                                         &i4Status);

            if (i4RetValue != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
                return CLI_FAILURE;
            }

            if (i4Status == NAT_STATUS_NOT_READY)
            {
                /* Try configuring the Translated Local IP 
                   and Translated Local Port */
                UINT4               u4IpAddr = NAT_ZERO;
                INT4                i4TranslatedPort = NAT_ZERO;
                if (CfaGetIfIpAddr ((INT4) u4Interface, &u4IpAddr) ==
                    OSIX_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if ((u4IpAddr == NAT_ZERO) ||
                    (nmhSetNatStaticNaptTranslatedLocalIp
                     ((INT4) u4Interface,
                      u4LocalIpAddr,
                      i4StartLocalPort,
                      i4EndLocalPort,
                      i4ProtocolNumber,
                      u4IpAddr) == SNMP_FAILURE)
                    ||
                    (nmhSetNatStaticNaptTranslatedLocalPort
                     ((INT4) u4Interface,
                      u4LocalIpAddr,
                      i4StartLocalPort,
                      i4EndLocalPort,
                      i4ProtocolNumber, i4TranslatedPort) == SNMP_FAILURE))
                {
                    CLI_SET_ERR (CLI_NAT_ERR_WAN_IF_NOT_READY);
                    return CLI_FAILURE;
                }
            }

            if (i4Status != NAT_STATUS_ACTIVE)
            {
                if (nmhTestv2NatStaticNaptEntryStatus (&u4Error,
                                                       (INT4) u4Interface,
                                                       u4LocalIpAddr,
                                                       i4StartLocalPort,
                                                       i4EndLocalPort,
                                                       i4ProtocolNumber,
                                                       NAT_STATUS_ACTIVE)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_NAT_ERR_SAME_GLOBAL_PORT);
                    return CLI_FAILURE;
                }

                i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                             u4LocalIpAddr,
                                                             i4StartLocalPort,
                                                             i4EndLocalPort,
                                                             i4ProtocolNumber,
                                                             NAT_STATUS_ACTIVE);

            }

            if (i4RetValue != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        while ((nmhGetNextIndexNatStaticNaptTable ((INT4) u4Interface,
                                                   &i4NextIfNum,
                                                   u4LocalIpAddr,
                                                   &u4NextLocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4NextStartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4NextEndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4NextProtocolNumber)) ==
               SNMP_SUCCESS);
    }

    return CLI_SUCCESS;
}

/*************************************************************************
 * Function name    : NatCliDisableAllVirtualServer
 *
 * Description      : This function disables all the virtual servers
 *
 * Inputs (s)       : CliHandle
 *
 * Outputs          : None
 *
 * Returns          : CLI_SUCCESS on Success , CLI_FAILURE on Failure
 * ***********************************************************************/

INT1
NatCliDisableAllVirtualServer (tCliHandle CliHandle)
{
    INT4                i4NextIfNum = NAT_ZERO;
    UINT4               u4NextLocIpAddr = NAT_ZERO;
    INT4                i4NextStartLocalPort = NAT_ZERO;
    INT4                i4NextEndLocalPort = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    UINT4               u4Interface = NAT_ZERO;
    UINT4               u4LocalIpAddr = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;

    if ((nmhGetFirstIndexNatStaticNaptTable (&i4NextIfNum,
                                             &u4NextLocIpAddr,
                                             &i4NextStartLocalPort,
                                             &i4NextEndLocalPort,
                                             &i4NextProtocolNumber)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    else
    {
        do
        {
            u4Interface = (UINT4) i4NextIfNum;
            u4LocalIpAddr = u4NextLocIpAddr;
            i4StartLocalPort = i4NextStartLocalPort;
            i4EndLocalPort = i4NextEndLocalPort;
            i4ProtocolNumber = i4NextProtocolNumber;
            i4Status = NAT_ZERO;
            i4RetValue = SNMP_FAILURE;

            i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                         u4LocalIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         i4ProtocolNumber,
                                                         &i4Status);

            if (i4RetValue != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
                return CLI_FAILURE;
            }

            if (i4Status == NAT_STATUS_ACTIVE)
            {
                i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                             u4LocalIpAddr,
                                                             i4StartLocalPort,
                                                             i4EndLocalPort,
                                                             i4ProtocolNumber,
                                                             NAT_STATUS_NOT_IN_SERVICE);

                if (i4RetValue != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

        }
        while ((nmhGetNextIndexNatStaticNaptTable ((INT4) u4Interface,
                                                   &i4NextIfNum,
                                                   u4LocalIpAddr,
                                                   &u4NextLocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4NextStartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4NextEndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4NextProtocolNumber)) ==
               SNMP_SUCCESS);

    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * 
 * Function name : NatCliDelAllVirtualServer
 *
 * Description   : This function deletes all the virtual servers
 *
 * Input (s)     : CliHandle
 *                 i4IfNum - Interface Index
 *
 * Output (s)    : None
 *
 * Results       : CLI_SUCCESS on Success , CLI_FAILURE on Failure
 * ***********************************************************************/

INT1
NatCliDelAllVirtualServer (tCliHandle CliHandle, INT4 i4IfNum)
{
    INT4                i4NextIfNum = NAT_ZERO;
    UINT4               u4NextLocIpAddr = NAT_ZERO;
    INT4                i4NextStartLocalPort = NAT_ZERO;
    INT4                i4NextEndLocalPort = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    UINT4               u4Interface = NAT_ZERO;
    UINT4               u4LocalIpAddr = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4NextExists = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;

    if ((nmhGetFirstIndexNatStaticNaptTable (&i4NextIfNum,
                                             &u4NextLocIpAddr,
                                             &i4NextStartLocalPort,
                                             &i4NextEndLocalPort,
                                             &i4NextProtocolNumber)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_TABLE_EMPTY);
        return CLI_FAILURE;
    }
    else
    {
        do
        {
            u4Interface = (UINT4) i4NextIfNum;
            u4LocalIpAddr = u4NextLocIpAddr;
            i4StartLocalPort = i4NextStartLocalPort;
            i4EndLocalPort = i4NextEndLocalPort;
            i4ProtocolNumber = i4NextProtocolNumber;
            i4Status = NAT_ZERO;
            i4RetValue = SNMP_FAILURE;

            i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                         u4LocalIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         i4ProtocolNumber,
                                                         &i4Status);

            if (i4RetValue != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
                return CLI_FAILURE;
            }

            /*Before Destroying the entry get the next entry from the table */
            i4NextExists =
                nmhGetNextIndexNatStaticNaptTable ((INT4) u4Interface,
                                                   &i4NextIfNum, u4LocalIpAddr,
                                                   &u4NextLocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4NextStartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4NextEndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4NextProtocolNumber);

            /* Dont delete PortRange entries */
            if ((u4Interface == (UINT4) i4IfNum) &&
                (i4StartLocalPort == i4EndLocalPort))
            {
                i4RetValue =
                    nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                    u4LocalIpAddr,
                                                    i4StartLocalPort,
                                                    i4EndLocalPort,
                                                    i4ProtocolNumber,
                                                    NAT_STATUS_DESTROY);

                if (i4RetValue == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        while (i4NextExists == SNMP_SUCCESS);

    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name :  NatCliDisableVirtualServer                             *
 *                                                                         *
 * Description   :  This function disables the virtual server              *
 *                                                                         *
 * Input (s)     :  pu1NatInput - Contains the indices of the row to       *
 *                                be deleted.                              *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  TRUE on success, FAILURE on error                      *
 ***************************************************************************/

INT1
NatCliDisableVirtualServer (tCliHandle CliHandle,
                            UINT4 u4Interface,
                            UINT4 u4LocalIpAddr,
                            INT4 i4StartLocalPort, INT4 i4EndLocalPort)
{
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_PROTO_ANY;

    i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                 u4LocalIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber, &i4Status);

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        return CLI_FAILURE;
    }

    if (i4Status == NAT_STATUS_ACTIVE)
    {
        i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                     u4LocalIpAddr,
                                                     i4StartLocalPort,
                                                     i4EndLocalPort,
                                                     i4ProtocolNumber,
                                                     NAT_STATUS_NOT_IN_SERVICE);
    }

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        /* CliPrintf(CliHandle, " Could not disable Virtual Server Entry ! \r\n"); */
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name :  NatCliDelVirtualServer                                 *
 *                                                                         *
 * Description   :  This function deletes the virtual server               *
 *                                                                         *
 * Input (s)     :  CliHandle     - Handle to CLI session                  *
 *                  u4Interface   -                                        *
 *                  u4LocalIpAddr -                                        *
 *                  u2LocalPort   -                                        *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  CLI_SUCCESS on success                                 *
 *                  CLI_FAILURE on failure                                 *
 *                                                                         *
 ***************************************************************************/
INT4
NatCliDelVirtualServer (tCliHandle CliHandle, UINT4 u4Interface,
                        UINT4 u4LocalIpAddr, INT4 i4StartLocalPort,
                        INT4 i4EndLocalPort, INT4 i4ProtocolNumber)
{
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4Status = NAT_ZERO;

    UNUSED_PARAM (CliHandle);
    i4RetValue = nmhGetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                 u4LocalIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber, &i4Status);

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        return CLI_FAILURE;
    }

    i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                 u4LocalIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber,
                                                 NAT_STATUS_DESTROY);

    if (i4RetValue == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliAddNatInterfaceEntry                             *
*                                                                         *
* Description   :  This function is invoked to set Nat on interface       *
*                                                                         *
* Input (s)     :  CliHandle    - Handle to CLI session                   *
*               :  u4NaptStatus - NAT Status Enable/Disable               *
*               :  i4IfNum      - Interface Number                        *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/

INT4
NatCliAddNatInterfaceEntry (tCliHandle CliHandle, UINT4 u4NaptStatus,
                            INT4 i4IfNum)
{

    INT4                i4RowStatus = NAT_ZERO;
    INT4                i4RetValue = NAT_ZERO;
    UINT4               u4Error = NAT_ZERO;
    INT4                i4NatStatus = NAT_ZERO;

    i4RetValue = nmhGetNatIfEntryStatus (i4IfNum, &i4RowStatus);

    if (i4RetValue != SNMP_SUCCESS)
    {
        if (nmhTestv2NatIfEntryStatus (&u4Error, (INT4) i4IfNum,
                                       NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
            return CLI_FAILURE;
        }

        if (nmhSetNatIfEntryStatus (i4IfNum,
                                    NAT_STATUS_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
            return CLI_FAILURE;
        }
    }

    else if (i4RetValue == SNMP_SUCCESS)
    {
        if (nmhTestv2NatIfEntryStatus (&u4Error, (INT4) i4IfNum,
                                       NAT_STATUS_NOT_IN_SERVICE)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_STATUS);
            return CLI_FAILURE;
        }

        if (nmhSetNatIfEntryStatus (i4IfNum, NAT_STATUS_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhSetNatIfNat (i4IfNum, NAT_ENABLE);

    /* NAPT can be set only if NAT is enabled on the interface */
    if (u4NaptStatus == NAT_ENABLE)
    {
        nmhGetNatIfNat (i4IfNum, (INT4 *) &i4NatStatus);

        if (i4NatStatus == NAT_ENABLE)
        {
            nmhSetNatIfNapt (i4IfNum, (INT4) u4NaptStatus);
        }
        else
        {
            CLI_SET_ERR (CLI_NAT_ERR_NAT_DOWN);
            return CLI_FAILURE;
        }

    }
    else if (u4NaptStatus == NAT_DISABLE)
    {
        nmhSetNatIfNapt (i4IfNum, (INT4) u4NaptStatus);
    }

    nmhGetNatIfNat (i4IfNum, (INT4 *) &i4NatStatus);

    if (i4NatStatus == NAT_ENABLE)
    {
        nmhSetNatIfTwoWayNat (i4IfNum, i4NatStatus);
    }
    else
    {
        CLI_SET_ERR (CLI_NAT_ERR_NAT_DOWN);
        return CLI_FAILURE;
    }

    if (nmhSetNatIfEntryStatus (i4IfNum, NAT_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliAddNatStaticEntry                                *
*                                                                         *
* Description   :  This function adds an entry to the staic NAT table.    *
*                  If an entry already exists with the given indices,     *
*                  it modifies the entry.                                 *
*                                                                         *
* Input (s)     :  None                                                   *
*                                                                         *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  Returns CLI_SUCCESS or CLI_FAILURE                     *
***************************************************************************/
INT4
NatCliAddNatStaticEntry (tCliHandle CliHandle, UINT4 u4Interface,
                         UINT4 u4LocalIpAddr, UINT4 u4TransIpAddr)
{
    INT4                i4RowStatus = NAT_ZERO;
    UINT4               u4Error = NAT_ZERO;

    UNUSED_PARAM (CliHandle);

    /* Test the objects before trying to set */
    if (nmhTestv2NatStaticTranslatedLocalIp (&u4Error,
                                             (INT4) u4Interface, u4LocalIpAddr,
                                             u4TransIpAddr) != SNMP_SUCCESS)
    {
        switch (u4Error)
        {
            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
                break;
            default:
                CLI_SET_ERR (CLI_NAT_ERR_IP_CONFLICT);
                break;
        }
        return CLI_FAILURE;
    }

    /* Corresponding to the given Local Ip and interface index, 
     * a row may or may not be existing in the static table. 
     * If a row already exists, modify the translated local Ip.
     * If a row doesnt exist, create one and set the translated local Ip */

    if (nmhGetNatStaticEntryStatus
        ((INT4) u4Interface, u4LocalIpAddr, &i4RowStatus) != SNMP_SUCCESS)
    {
        /* New Entry */
        if (nmhTestv2NatStaticEntryStatus (&u4Error, (INT4) u4Interface,
                                           u4LocalIpAddr,
                                           NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_LOCAL_IP);
            return CLI_FAILURE;
        }
        if (nmhSetNatStaticEntryStatus ((INT4) u4Interface, u4LocalIpAddr,
                                        NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
            return CLI_FAILURE;
        }
        if (nmhSetNatStaticTranslatedLocalIp ((INT4) u4Interface, u4LocalIpAddr,
                                              u4TransIpAddr) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetNatStaticEntryStatus ((INT4) u4Interface, u4LocalIpAddr,
                                        NAT_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Existing Entry */
        if (i4RowStatus == NAT_STATUS_ACTIVE)
        {
            if (nmhSetNatStaticEntryStatus ((INT4) u4Interface, u4LocalIpAddr,
                                            NAT_STATUS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetNatStaticTranslatedLocalIp
                ((INT4) u4Interface, u4LocalIpAddr,
                 u4TransIpAddr) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetNatStaticEntryStatus ((INT4) u4Interface, u4LocalIpAddr,
                                            NAT_STATUS_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            CLI_SET_ERR (CLI_NAT_ERR_ENTRY_NOT_ACTIVE);
            return CLI_FAILURE;
        }
    }
    return (CLI_SUCCESS);
}

/**************************************************************************
 * Function Name :  NatCliDelNatStaticEntry                                *
 *                                                                         *
 * Description   :  This function the given entry from the NAT static table*
 *                                                                         *
 * Input (s)     :  None                                                   *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  Returns CLI_SUCCESS or CLI_FAILURE                     *
 ***************************************************************************/
INT4
NatCliDelNatStaticEntry (tCliHandle CliHandle, UINT4 u4Interface,
                         UINT4 u4LocalIpAddr)
{
    INT4                i4RetValue = NAT_ZERO;

    UNUSED_PARAM (CliHandle);

    i4RetValue = nmhSetNatStaticEntryStatus ((INT4) u4Interface, u4LocalIpAddr,
                                             NAT_STATUS_DESTROY);

    if (i4RetValue != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);
}

/**************************************************************************
 * Function Name :  NatCliAddPortTrigger                                   *
 *                                                                         *
 * Description   :  This function is creates port trigger for a particular *
 *                  application.                                           *
 *                                                                         *
 * Input (s)     :  pu1NatInput - Contains the port trigger info for an    *
 *                  application         .                                  *
 * Output (s)    :  ppu1Output  - Formated output message.                 *
 *                                                                         *
 * Returns       :  Returns CLI_SUCCESS always                             *
 ***************************************************************************/
INT4
NatCliAddPortTrigger (tCliHandle CliHandle, UINT1 *pu1AppName,
                      UINT1 *pu1TrigOutPort, UINT1 *pu1TrigInPort,
                      UINT2 u2Protocol)
{
    UINT4               u4Error = NAT_ZERO;
    INT4                i4RetRowStatus = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE *pInBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pOutBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pApplicationName = NULL;

    UNUSED_PARAM (CliHandle);

    pInBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pInBoundPortRange == NULL)
    {
        return (CLI_FAILURE);
    }

    pOutBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pOutBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        return (CLI_FAILURE);
    }

    pApplicationName = allocmem_octetstring (NAT_MAX_APP_NAME_LEN);

    if (pApplicationName == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        return (CLI_FAILURE);
    }

    MEMSET (pInBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);
    MEMSET (pOutBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);
    MEMSET (pApplicationName->pu1_OctetList, NAT_ZERO, NAT_MAX_APP_NAME_LEN);

    pInBoundPortRange->i4_Length = (INT4) STRLEN (pu1TrigInPort);

    MEMCPY (pInBoundPortRange->pu1_OctetList, pu1TrigInPort,
            pInBoundPortRange->i4_Length);

    pOutBoundPortRange->i4_Length = (INT4) STRLEN (pu1TrigOutPort);

    MEMCPY (pOutBoundPortRange->pu1_OctetList, pu1TrigOutPort,
            pOutBoundPortRange->i4_Length);

    pApplicationName->i4_Length = (INT4) STRLEN (pu1AppName);

    MEMCPY (pApplicationName->pu1_OctetList, pu1AppName,
            pApplicationName->i4_Length);

    if (nmhGetNatPortTrigInfoEntryStatus
        (pInBoundPortRange, pOutBoundPortRange, u2Protocol,
         &i4RetRowStatus) == SNMP_FAILURE)
    {
        /* New Entry Add */
        if (nmhTestv2NatPortTrigInfoEntryStatus (&u4Error, pInBoundPortRange,
                                                 pOutBoundPortRange, u2Protocol,
                                                 CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            if (u4Error == SNMP_ERR_WRONG_LENGTH)
            {
                /* Table limit exceeded */
                CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
            }

            if (u4Error == SNMP_ERR_WRONG_VALUE)
            {
                /* Incorrect Range of Port */
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_PORT_RANGE);
            }

            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        /* Setting Row Status as Create & Wait */
        if (nmhSetNatPortTrigInfoEntryStatus (pInBoundPortRange,
                                              pOutBoundPortRange, u2Protocol,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        if (nmhTestv2NatPortTrigInfoAppName (&u4Error, pInBoundPortRange,
                                             pOutBoundPortRange, u2Protocol,
                                             pApplicationName) == SNMP_FAILURE)
        {
            if (u4Error == SNMP_ERR_WRONG_LENGTH)
            {
                CLI_SET_ERR (CLI_NAT_ERR_INVALID_APP_NAME_LEN);
            }

            if (u4Error == SNMP_ERR_WRONG_VALUE)
            {
                /* Application name already existing */
                CLI_SET_ERR (CLI_NAT_ERR_APP_NAME_CONFLICT);
            }

            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);

        }
        /* Setting Application Name */
        if (nmhSetNatPortTrigInfoAppName (pInBoundPortRange, pOutBoundPortRange,
                                          u2Protocol,
                                          pApplicationName) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        if (nmhTestv2NatPortTrigInfoEntryStatus (&u4Error, pInBoundPortRange,
                                                 pOutBoundPortRange, u2Protocol,
                                                 ACTIVE) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }
        /* Setting Row Status as Active */
        if (nmhSetNatPortTrigInfoEntryStatus (pInBoundPortRange,
                                              pOutBoundPortRange, u2Protocol,
                                              ACTIVE) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

    }

    free_octetstring (pInBoundPortRange);
    free_octetstring (pOutBoundPortRange);
    free_octetstring (pApplicationName);

    return (CLI_SUCCESS);

}

/**************************************************************************
 * Function Name :  NatCliDelPortTrigger                                   *
 *                                                                         *
 * Description   :  This function deletes the port trigger entry           *
 *                                                                         *
 * Input (s)     :  pu1NatInput - Contains the application name            *
 *                                be deleted.                              *
 * Output (s)    :  ppu1Output  - Formated output message.                 *
 *                                                                         *
 * Returns       :  Returns CLI_SUCCESS always                             *
 ***************************************************************************/
INT4
NatCliDelPortTrigger (tCliHandle CliHandle, UINT1 *pu1AppName)
{
    UINT4               u4Error = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    INT4                i4RetRowStatus = NAT_ZERO;
    UINT1               u1Found = NAT_ZERO;
    UINT1               au1TempAppName[NAT_MAX_APP_NAME_LEN] = { NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE *pInBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pOutBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextInBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextOutBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pApplicationName = NULL;

    UNUSED_PARAM (CliHandle);

    pInBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pInBoundPortRange == NULL)
    {
        return (CLI_FAILURE);
    }

    pOutBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pOutBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        return (CLI_FAILURE);
    }

    pNextInBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pNextInBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        return (CLI_FAILURE);
    }

    pNextOutBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pNextOutBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        return (CLI_FAILURE);
    }

    MEMSET (pNextInBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_APP_NAME_LEN);
    pNextInBoundPortRange->i4_Length = NAT_ZERO;

    MEMSET (pNextOutBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_APP_NAME_LEN);
    pNextOutBoundPortRange->i4_Length = NAT_ZERO;

    /* Scanning the list and getting the index for the application name */
    if (nmhGetFirstIndexNatPortTrigInfoTable (pInBoundPortRange,
                                              pOutBoundPortRange,
                                              &i4ProtocolNumber) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_TABLE_EMPTY);
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        free_octetstring (pNextOutBoundPortRange);
        return (CLI_FAILURE);
    }

    pApplicationName = allocmem_octetstring (NAT_MAX_APP_NAME_LEN);

    if (pApplicationName == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        free_octetstring (pNextOutBoundPortRange);
        return (CLI_FAILURE);
    }

    MEMSET (pApplicationName->pu1_OctetList, NAT_ZERO, NAT_MAX_APP_NAME_LEN);
    pApplicationName->i4_Length = NAT_ZERO;

    do
    {
        if ((pNextInBoundPortRange->i4_Length != NAT_ZERO)
            || (pNextOutBoundPortRange->i4_Length != NAT_ZERO))
        {
            MEMSET (pInBoundPortRange->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_PORT_STRING_LEN);
            MEMCPY (pInBoundPortRange->pu1_OctetList,
                    pNextInBoundPortRange->pu1_OctetList,
                    pNextInBoundPortRange->i4_Length);
            pInBoundPortRange->i4_Length = pNextInBoundPortRange->i4_Length;

            MEMSET (pOutBoundPortRange->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_PORT_STRING_LEN);
            MEMCPY (pOutBoundPortRange->pu1_OctetList,
                    pNextOutBoundPortRange->pu1_OctetList,
                    pNextOutBoundPortRange->i4_Length);
            pOutBoundPortRange->i4_Length = pNextOutBoundPortRange->i4_Length;

            i4ProtocolNumber = i4NextProtocolNumber;

            MEMSET (pApplicationName->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_APP_NAME_LEN);
        }

        if (nmhGetNatPortTrigInfoAppName (pInBoundPortRange, pOutBoundPortRange,
                                          i4ProtocolNumber,
                                          pApplicationName) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pNextInBoundPortRange);
            free_octetstring (pNextOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        MEMSET (au1TempAppName, NAT_ZERO, NAT_MAX_APP_NAME_LEN);
        MEMCPY (au1TempAppName, pu1AppName, STRLEN (pu1AppName));

        /* Fill indexes if application name found */
        if (MEMCMP (au1TempAppName, pApplicationName->pu1_OctetList,
                    NAT_MAX_APP_NAME_LEN) == NAT_ZERO)
        {
            u1Found = NAT_ONE;
            break;
        }

    }
    while (nmhGetNextIndexNatPortTrigInfoTable (pInBoundPortRange,
                                                pNextInBoundPortRange,
                                                pOutBoundPortRange,
                                                pNextOutBoundPortRange,
                                                i4ProtocolNumber,
                                                &i4NextProtocolNumber) !=
           SNMP_FAILURE);

    if (u1Found == NAT_ZERO)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        free_octetstring (pNextOutBoundPortRange);
        free_octetstring (pApplicationName);
        return (CLI_FAILURE);
    }

    if (nmhGetNatPortTrigInfoEntryStatus
        (pInBoundPortRange, pOutBoundPortRange, i4ProtocolNumber,
         &i4RetRowStatus) == SNMP_SUCCESS)
    {
        /* Existing Entry Delete */
        if (nmhTestv2NatPortTrigInfoEntryStatus (&u4Error, pInBoundPortRange,
                                                 pOutBoundPortRange,
                                                 i4ProtocolNumber,
                                                 DESTROY) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pNextInBoundPortRange);
            free_octetstring (pNextOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        if (nmhSetNatPortTrigInfoEntryStatus (pInBoundPortRange,
                                              pOutBoundPortRange,
                                              i4ProtocolNumber,
                                              DESTROY) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pNextInBoundPortRange);
            free_octetstring (pNextOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }
    }

    free_octetstring (pInBoundPortRange);
    free_octetstring (pOutBoundPortRange);
    free_octetstring (pNextInBoundPortRange);
    free_octetstring (pNextOutBoundPortRange);
    free_octetstring (pApplicationName);

    return (CLI_SUCCESS);

}

/**************************************************************************
* Function Name :  NatCliAddNatGlobalEntry                                *
*                                                                         *
* Description   :  This function adds an entry to the NAT's Global        *
*                  address table.                                         *
*                  If an entry already exists with the given indices,     *
*                  it modifies the entry.                                 *
*                                                                         *
* Input (s)     :  CliHandle      - CliContext ID.                        *
*                  u4Interface    - interface index.                      *
*                  u4GlobalIpAddr - Global IpAddress for added.           *
*                  u4Mask         - Mask for Global IpAddress.            *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE.                               *
***************************************************************************/
INT4
NatCliAddNatGlobalEntry (tCliHandle CliHandle, UINT4 u4Interface,
                         UINT4 u4GlobalIpAddr, UINT4 u4Mask)
{
    INT4                i4RowStatus = NAT_ZERO;
    UINT4               u4Error = NAT_ZERO;

    /* Test the object before trying to set */

    if (nmhTestv2NatGlobalAddressMask (&u4Error,
                                       (INT4) u4Interface, u4GlobalIpAddr,
                                       u4Mask) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_INPUT);
        return CLI_FAILURE;
    }

    /* Corresponding to the given Global Ip and interface index,
     * a row may or may not be existing in the global address table.
     * If a row already exists, modify the Global Address mask.
     * If a row doesnt exist, create one and set the  Global Address mask */

    if (nmhGetNatGlobalAddressEntryStatus ((INT4) u4Interface, u4GlobalIpAddr,
                                           &i4RowStatus) != SNMP_SUCCESS)
    {
        /* New Entry */
        if (nmhTestv2NatGlobalAddressEntryStatus (&u4Error,
                                                  (INT4) u4Interface,
                                                  u4GlobalIpAddr,
                                                  NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_INPUT);
            return CLI_FAILURE;
        }
        if (nmhSetNatGlobalAddressEntryStatus
            ((INT4) u4Interface, u4GlobalIpAddr,
             NAT_STATUS_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
            return CLI_FAILURE;
        }
        if (nmhSetNatGlobalAddressMask
            ((INT4) u4Interface, u4GlobalIpAddr, u4Mask) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetNatGlobalAddressEntryStatus
            ((INT4) u4Interface, u4GlobalIpAddr,
             NAT_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Existing Entry */
        if (i4RowStatus == NAT_STATUS_ACTIVE)
        {
            if (nmhSetNatGlobalAddressEntryStatus
                ((INT4) u4Interface, u4GlobalIpAddr,
                 NAT_STATUS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetNatGlobalAddressMask
                ((INT4) u4Interface, u4GlobalIpAddr, u4Mask) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetNatGlobalAddressEntryStatus
                ((INT4) u4Interface, u4GlobalIpAddr,
                 NAT_STATUS_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else
        {
            CLI_SET_ERR (CLI_NAT_ERR_ENTRY_NOT_ACTIVE);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name :  NatCliDelNatGlobalEntry                                *
 *                                                                         *
 * Description   :  This function Delete the the given GlobalIP entry from * 
 *                  the NAT Global address table.                          *
 *                                                                         *
 * Input (s)     :  CliHandle      - CliContext ID.                        *
 *                  u4Interface    - interface index.                      *
 *                  u4GlobalIpAddr - Global IpAddress for added.           *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.                               *
 ***************************************************************************/
INT4
NatCliDelNatGlobalEntry (tCliHandle CliHandle, UINT4 u4Interface,
                         UINT4 u4GlobalIpAddr)
{
    UINT4               u4ErrorCode = NAT_ZERO;

    /* Test first whether entry can be deleted or not */
    if (nmhTestv2NatGlobalAddressEntryStatus (&u4ErrorCode,
                                              (INT4) u4Interface,
                                              u4GlobalIpAddr,
                                              NAT_STATUS_DESTROY)
        != SNMP_SUCCESS)
    {
        /* If u4ErrorCode is SNMP_ERR_INCONSISTENT_VALUE then 
         * this is last entry in the global pool table and can't be 
         * deleted. 
         */
        if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INCONSISTENT_ENTRY);
        }
        else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        }
        return CLI_FAILURE;
    }

    if ((nmhSetNatGlobalAddressEntryStatus ((INT4) u4Interface,
                                            u4GlobalIpAddr,
                                            NAT_STATUS_DESTROY))
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

 /**************************************************************************
 * Function Name :  NatCliPortRangeForwarding                              *
 *                                                                         *
 * Description   :  This function adds a port range forwarding.            *
 *                  If an entry already exists with in the given port      *
 *                  range, it does not add the entry.                      *
 *                                                                         *
 * Input (s)     :  CliHandle          - Handle to CLI session             *
 *                  u4Interface        -                                   *
 *                  u4LocalIpAddr      -                                   *
 *                  u4TranslatedIpAddr -                                   *
 *                  u2StartLocalPort   -                                   *
 *                  u2EndLocalPort     -                                   *
 *                  u2ProtocolNumber   -                                   *
 *                                                                         *
 * Returns       :  CLI_SUCCESS on success                                 *
 *                  CLI_FAILURE on failure                                 *
 *                                                                         *
 **************************************************************************/
INT4
NatCliAddPortRangeForwarding (tCliHandle CliHandle,
                              UINT4 u4Interface,
                              UINT4 u4LocalIpAddr,
                              UINT4 u4TranslatedIpAddr,
                              INT4 i4StartLocalPort,
                              INT4 i4EndLocalPort,
                              UINT2 u2TranslatedPort, UINT2 u2ProtocolNumber)
{

    UINT4               u4Error = NAT_ZERO;
    INT4                i4RowStatus = NAT_ZERO;

    INT4                i4ProtocolNumber = (INT4) u2ProtocolNumber;
    INT4                i4Interface = (INT4) u4Interface;

    if (u4TranslatedIpAddr == NAT_ZERO)
    {
        /* i4Interface will be a valid one....Get the ip address */
        if (CfaGetIfIpAddr (i4Interface, &u4TranslatedIpAddr) != CFA_SUCCESS)
        {
            /* This should never happen... */
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
            return CLI_FAILURE;
        }
    }

    /* Corresponding to the given Local Ip and interface index
     * and with in the given port range a row may or may not be
     * existing in the static table.
     * If a row already exists, it does not add the entry.
     * If a row doesnt exist, create one and set the tranlated local Ip */
    if (nmhGetNatStaticNaptEntryStatus (i4Interface, u4LocalIpAddr,
                                        i4StartLocalPort, i4EndLocalPort,
                                        i4ProtocolNumber, &i4RowStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2NatStaticNaptEntryStatus (&u4Error, i4Interface,
                                               u4LocalIpAddr, i4StartLocalPort,
                                               i4EndLocalPort, i4ProtocolNumber,
                                               NAT_STATUS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            switch (u4Error)
            {
                case SNMP_ERR_RESOURCE_UNAVAILABLE:
                    CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
                    break;
                case SNMP_ERR_INCONSISTENT_VALUE:
                    CLI_SET_ERR (CLI_NAT_ERR_CONFLICTING_PORT_RANGE);
                    break;
                default:
                    CLI_SET_ERR (CLI_NAT_ERR_INVALID_INPUT);
                    break;
            }
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptEntryStatus (i4Interface, u4LocalIpAddr,
                                            i4StartLocalPort, i4EndLocalPort,
                                            i4ProtocolNumber,
                                            NAT_STATUS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptTranslatedLocalIp (i4Interface, u4LocalIpAddr,
                                                  i4StartLocalPort,
                                                  i4EndLocalPort,
                                                  i4ProtocolNumber,
                                                  u4TranslatedIpAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetNatStaticNaptTranslatedLocalPort (i4Interface,
                                                    u4LocalIpAddr,
                                                    i4StartLocalPort,
                                                    i4EndLocalPort,
                                                    i4ProtocolNumber,
                                                    u2TranslatedPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /*
         * Activate the port range forwarding
         */

        if (nmhSetNatStaticNaptEntryStatus (i4Interface, u4LocalIpAddr,
                                            i4StartLocalPort, i4EndLocalPort,
                                            i4ProtocolNumber, NAT_STATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    else
    {
        /* 
         * Entry exists - Cannot be modified 
         */
        if (i4RowStatus == NAT_STATUS_ACTIVE)
        {
            CLI_SET_ERR (CLI_NAT_ERR_EXISTING_ENTRY);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

}

 /**************************************************************************
 * Function Name :  NatCliDelPortRangeForwarding                           *
 *                                                                         *
 * Description   :  This function deletes the port range forwarding entry  *
 *                                                                         *
 * Input (s)     :  CliHandle          - Handle to CLI session             *
 *                  u4Interface        -                                   *
 *                  u4LocalIpAddr      -                                   *
 *                  u2StartLocalPort   -                                   *
 *                  u2EndLocalPort     -                                   *
 *                  u2ProtocolNumber   -                                   *
 *                                                                         *
 * Returns       :  CLI_SUCCESS on success                                 *
 *                  CLI_FAILURE on failure                                 *
 *                                                                         *
 **************************************************************************/
INT4
NatCliDelPortRangeForwarding (tCliHandle CliHandle,
                              UINT4 u4Interface,
                              UINT4 u4LocalIpAddr,
                              INT4 i4StartLocalPort,
                              INT4 i4EndLocalPort, UINT2 u2ProtocolNumber)
{
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4ProtocolNumber = (INT4) u2ProtocolNumber;

    UNUSED_PARAM (CliHandle);

    i4RetValue = nmhSetNatStaticNaptEntryStatus ((INT4) u4Interface,
                                                 u4LocalIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber,
                                                 NAT_STATUS_DESTROY);

    if (i4RetValue == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ENTRY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliSetInterfaceNatStatus                            *
*                                                                         *
* Description   :  This function is invoked to set the Interface NAT      *
*                  Status .                                               *
*                                                                         *
* Input (s)     :  CliHandle - Handle to CLI session                      *
*                  i4IfNum   - Interface Number                           *
*                  u4NatStatus - Enable or Disable                        * 
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliSetInterfaceNatStatus (tCliHandle CliHandle, INT4 i4IfNum,
                             UINT4 u4NatStatus)
{
    INT4                i4RowStatus = NAT_ZERO;
    INT4                i4RetValue = NAT_ZERO;
    UINT4               u4Error = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;
    UINT4               u4WanIfIpAddr = NAT_ZERO;
    INT4                i4NaptStatus = NAT_ZERO;

    if ((nmhGetNatIfNat (i4IfNum, (INT4 *) &u4Status) == SNMP_SUCCESS) &&
        (u4Status == u4NatStatus))
    {
        CliPrintf (NAT_TRC_ON, "Nat ALready Enabled in Interface Index "
                   "%d\r\n", i4IfNum);
    }
    if (nmhGetNatIfNapt (i4IfNum, &i4NaptStatus) == SNMP_SUCCESS)
    {
        if ((i4NaptStatus == NAT_ENABLE) && (u4NatStatus == NAT_DISABLE))
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot Disable NAT if NAPT is Enabled ! \r\n");
            return (CLI_FAILURE);
        }
    }

    u4WanIfIpAddr = NatGetWanInterfaceIpAddr ((UINT4) i4IfNum);

    if ((u4WanIfIpAddr == NAT_ZERO) && (u4NatStatus == NAT_ENABLE))
    {
        CLI_SET_ERR (CLI_NAT_ERR_IP_NOT_SET_FOR_WAN);
        return (CLI_FAILURE);
    }

    i4RetValue = nmhGetNatIfEntryStatus (i4IfNum, &i4RowStatus);

    if (i4RetValue != SNMP_SUCCESS)
    {
        if (nmhTestv2NatIfEntryStatus (&u4Error, (INT4) i4IfNum,
                                       NAT_STATUS_CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_INTF);
            return CLI_FAILURE;
        }

        if (nmhSetNatIfEntryStatus (i4IfNum,
                                    NAT_STATUS_CREATE_AND_WAIT) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
            return CLI_FAILURE;
        }
    }
    else if (i4RetValue == SNMP_SUCCESS)
    {
        if (nmhTestv2NatIfEntryStatus (&u4Error, (INT4) i4IfNum,
                                       NAT_STATUS_NOT_IN_SERVICE)
            != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_NAT_ERR_INVALID_STATUS);
            return CLI_FAILURE;
        }

        if (nmhSetNatIfEntryStatus (i4IfNum, NAT_STATUS_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4NatStatus == NAT_ENABLE)
    {
        if (nmhGetNatGlobalAddressEntryStatus (i4IfNum, u4WanIfIpAddr,
                                               &i4RowStatus) == SNMP_FAILURE)
        {
            if (nmhTestv2NatGlobalAddressEntryStatus
                (&u4Error, i4IfNum, u4WanIfIpAddr, NAT_STATUS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_AUTOIP_CFG_FAILURE);
                return CLI_FAILURE;
            }

            if (nmhSetNatGlobalAddressEntryStatus (i4IfNum, u4WanIfIpAddr,
                                                   NAT_STATUS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
                return CLI_FAILURE;
            }
            if (nmhTestv2NatGlobalAddressMask (&u4Error, i4IfNum,
                                               u4WanIfIpAddr, NAT_INIT_MASK)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetNatGlobalAddressMask (i4IfNum, u4WanIfIpAddr,
                                            NAT_INIT_MASK) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2NatGlobalAddressEntryStatus
                (&u4Error, i4IfNum, u4WanIfIpAddr, NAT_STATUS_ACTIVE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_AUTOIP_CFG_FAILURE);
                return CLI_FAILURE;
            }

            if (nmhSetNatGlobalAddressEntryStatus (i4IfNum, u4WanIfIpAddr,
                                                   NAT_STATUS_ACTIVE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (nmhTestv2NatGlobalAddressEntryStatus
            (&u4Error, i4IfNum, u4WanIfIpAddr, NAT_STATUS_DESTROY) !=
            SNMP_FAILURE)
        {
            if (nmhSetNatGlobalAddressEntryStatus (i4IfNum, u4WanIfIpAddr,
                                                   NAT_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_NAT_ERR_LACK_OF_RES);
                return CLI_FAILURE;
            }
        }
        NatDeleteDynamicEntry ((UINT4) i4IfNum);
    }

    if ((nmhTestv2NatIfNat (&u4Error, i4IfNum, (INT4) u4NatStatus) ==
         SNMP_FAILURE))
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetNatIfNat (i4IfNum, (INT4) u4NatStatus)) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if ((nmhTestv2NatIfNapt (&u4Error, i4IfNum, (INT4) u4NatStatus)) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetNatIfNapt (i4IfNum, (INT4) u4NatStatus)) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if ((nmhTestv2NatIfTwoWayNat (&u4Error, i4IfNum, (INT4) u4NatStatus))
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetNatIfTwoWayNat (i4IfNum, (INT4) u4NatStatus)) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetNatIfEntryStatus (i4IfNum, NAT_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliShowNatConfig                                    *
*                                                                         *
* Description   :  This function is invoked to get the global NAT         *
*                  configuratiion and store in the output buffer.         *
*                                                                         *
* Input (s)     :  CliHandle - Handle to CLI session                      *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliShowNatConfig (tCliHandle CliHandle)
{
    INT4                i4Val = NAT_ZERO;
    tNatGlobalConf      NatGlobalConf;

    nmhGetNatEnable (&i4Val);
    NatGlobalConf.i4Flag = i4Val;

    nmhGetNatTypicalNumberOfEntries (&i4Val);
    NatGlobalConf.u4NumEntries = (UINT4) i4Val;

    nmhGetNatTranslatedLocalPortStart (&i4Val);
    NatGlobalConf.u4LocalPortStart = (UINT4) i4Val;

    nmhGetNatIdleTimeOut (&i4Val);
    NatGlobalConf.u4IdleTimeOut = (UINT4) i4Val;

    nmhGetNatTcpTimeOut (&i4Val);
    NatGlobalConf.u4TcpTimeOut = (UINT4) i4Val;

    nmhGetNatUdpTimeOut (&i4Val);
    NatGlobalConf.u4UdpTimeOut = (UINT4) i4Val;

    nmhGetNatTrcFlag (&i4Val);
    NatGlobalConf.u4Trc = (UINT4) i4Val;

    NatCliFormatShowConfig (CliHandle, &NatGlobalConf);

    return (CLI_SUCCESS);
}

/**************************************************************************
* Function Name :  NatCliFormatShowConfig                                 *
*                                                                         *
* Description   :  This function is invoked to format and print the       *
*                  Nat Configurations                                     *
*                                                                         *
* Input (s)     :  CliHandle - Handle to CLI session                      *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  None                                                   *
***************************************************************************/
VOID
NatCliFormatShowConfig (tCliHandle CliHandle, tNatGlobalConf * pNatGlobalConf)
{
    CliPrintf (CliHandle, "\r\nNAT Configuration");
    CliPrintf (CliHandle, "\r\n-----------------\r\n");

    NatCliFormatStatus (CliHandle, pNatGlobalConf->i4Flag,
                        "NAT Status                  : %s\r\n");

    CliPrintf (CliHandle, "Maximum Translation Entries : %ld\r\n",
               pNatGlobalConf->u4NumEntries);

    CliPrintf (CliHandle, "Start Free Port             : %ld\r\n",
               pNatGlobalConf->u4LocalPortStart);

    CliPrintf (CliHandle, "Idle Timeout                : %ld seconds\r\n",
               pNatGlobalConf->u4IdleTimeOut);

    CliPrintf (CliHandle, "TCP  Timeout                : %ld seconds\r\n",
               pNatGlobalConf->u4TcpTimeOut);

    CliPrintf (CliHandle, "UDP  Timeout                : %ld seconds\r\n",
               pNatGlobalConf->u4UdpTimeOut);

    CliPrintf (CliHandle, "\r\n\r\n");
}

/**************************************************************************
* Function Name :  NatCliFormatStatus                                     *
*                                                                         *
* Description   :  This function is invoked to format and print the       *
*                  Nat Global Status                                      *
*                                                                         *
* Input (s)     :  CliHandle - Handle to CLI session                      *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliFormatStatus (tCliHandle CliHandle, INT4 i4Status, CONST CHR1 * pu1String)
{

    CliPrintf (CliHandle, pu1String,
               ((i4Status == NAT_ENABLE) ? "Enabled" : "Disabled"));
    return (CLI_SUCCESS);
}

/**************************************************************************
* Function Name :  NatCliShowGlobalTable                                  *
*                                                                         *
* Description   :  This function is invoked to get the NAT's global       *
*                  address table and store in the output buffer.          *
*                                                                         *
* Input (s)     :  CliHandle                                              *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliShowGlobalTable (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    UINT1               au1IfName[NAT_CLI_MAX_INTF_NAME_LEN] = { NAT_ZERO };
    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4GlobalAddrTranslatedLocIp = NAT_ZERO;
    UINT4               u4GlobalAddressMask = NAT_ZERO;

    if (nmhGetFirstIndexNatGlobalAddressTable (&i4IfNum,
                                               &u4GlobalAddrTranslatedLocIp)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nGlobal Addresses Configured\r\n"
               "---------------------------\r\n");
    CliPrintf (CliHandle, "\r\nInterface   Translated Local IP\
               Translated IP Address Range");

    CliPrintf (CliHandle, "\r\n---------   -------------------\
               ---------------------------\r\n");

    do
    {
        if (nmhGetNatGlobalAddressMask (i4IfNum,
                                        u4GlobalAddrTranslatedLocIp,
                                        &u4GlobalAddressMask) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        CfaCliGetIfName ((UINT4) i4IfNum, (INT1 *) au1IfName);

        CliPrintf (CliHandle, "%-12s", au1IfName);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GlobalAddrTranslatedLocIp);
        CliPrintf (CliHandle, "%-25s", pu1String);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GlobalAddressMask);
        CliPrintf (CliHandle, "%s\r\n", pu1String);

    }
    while (nmhGetNextIndexNatGlobalAddressTable (i4IfNum,
                                                 &i4IfNum,
                                                 u4GlobalAddrTranslatedLocIp,
                                                 &u4GlobalAddrTranslatedLocIp)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliShowStaticTable                                  *
*                                                                         *
* Description   :  This function is invoked to get the NAT  static        *
*                  table and store in the output buffer.                  *
*                                                                         *
* Input (s)     :  CliHandle                                              *
*                                                                         *
* Output (s)    :  None.                                                  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliShowStaticTable (tCliHandle CliHandle)
{
    UINT1               au1IfName[NAT_CLI_MAX_INTF_NAME_LEN] = { NAT_ZERO };
    CHR1               *pu1String = NULL;
    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4StaticLocalIp = NAT_ZERO;
    UINT4               u4TranslatedLocalIp = NAT_ZERO;

    if (nmhGetFirstIndexNatStaticTable (&i4IfNum, &u4StaticLocalIp)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nStatic Address Mapping\r\n"
               "----------------------\r\n");
    CliPrintf (CliHandle,
               "\r\nInterface   Local IP        Translated Local IP");
    CliPrintf (CliHandle,
               "\r\n---------   ---------       -------------------\r\n");

    do
    {
        if (nmhGetNatStaticTranslatedLocalIp (i4IfNum, u4StaticLocalIp,
                                              &u4TranslatedLocalIp)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CfaCliGetIfName ((UINT4) i4IfNum, (INT1 *) au1IfName);

        CliPrintf (CliHandle, "%-12s", au1IfName);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StaticLocalIp);

        CliPrintf (CliHandle, "%-16s", pu1String);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TranslatedLocalIp);

        CliPrintf (CliHandle, "%s\r\n", pu1String);

    }
    while (nmhGetNextIndexNatStaticTable (i4IfNum,
                                          &i4IfNum,
                                          u4StaticLocalIp,
                                          &u4StaticLocalIp) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliShowDynamicTable                                 *
*                                                                         *
* Description   :  This function is invoked to get the NAT's dynamic      *
*                  mapping table and store in the output buffer.          *
*                                                                         *
* Input (s)     :  CliHandle.                                             *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliShowDynamicTable (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    UINT1               au1IfName[NAT_CLI_MAX_INTF_NAME_LEN] = { NAT_ZERO };

    INT4                i4DynamicTransIfNum = NAT_ZERO;
    UINT4               u4DynamicTransLocIp = NAT_ZERO;
    INT4                i4DynamicTransLocPort = NAT_ZERO;
    UINT4               u4DynamicTransOutsideIp = NAT_ZERO;
    INT4                i4DynamicTransOutsidePort = NAT_ZERO;
    UINT4               u4DynamicTransTranslatedLocIp = NAT_ZERO;
    INT4                i4DynamicTransTranslatedLocPort = NAT_ZERO;

    if (nmhGetFirstIndexNatDynamicTransTable (&i4DynamicTransIfNum,
                                              &u4DynamicTransLocIp,
                                              &i4DynamicTransLocPort,
                                              &u4DynamicTransOutsideIp,
                                              &i4DynamicTransOutsidePort)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nTranslated Addresses\r\n"
               "--------------------\r\n\r\n");

    CliPrintf (CliHandle, "Interface Local           Translated\
               Local Translated Destination    Dest.\r\n");

    CliPrintf (CliHandle,
               "          IP              Local IP        Port   Port       IP           Port \r\n");

    CliPrintf (CliHandle, "--------  --------------- -------------   ------\
            --------- -----------  -------\r\n");

    /* scan the Dynamic mapping table to get all the dynamic nat translation 
     * nodes. Format the output and store in the output buffer */
    do
    {
        if (nmhGetNatDynamicTransTranslatedLocalIp (i4DynamicTransIfNum,
                                                    u4DynamicTransLocIp,
                                                    i4DynamicTransLocPort,
                                                    u4DynamicTransOutsideIp,
                                                    i4DynamicTransOutsidePort,
                                                    &u4DynamicTransTranslatedLocIp)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetNatDynamicTransTranslatedLocalPort (i4DynamicTransIfNum,
                                                      u4DynamicTransLocIp,
                                                      i4DynamicTransLocPort,
                                                      u4DynamicTransOutsideIp,
                                                      i4DynamicTransOutsidePort,
                                                      &i4DynamicTransTranslatedLocPort)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        CfaCliGetIfName ((UINT4) i4DynamicTransIfNum, (INT1 *) au1IfName);

        CliPrintf (CliHandle, "%-10s", au1IfName);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DynamicTransLocIp);
        CliPrintf (CliHandle, "%-16s", pu1String);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DynamicTransTranslatedLocIp);
        CliPrintf (CliHandle, "%-16s", pu1String);

        CliPrintf (CliHandle, "%-7d", i4DynamicTransLocPort);

        CliPrintf (CliHandle, "%-10d", i4DynamicTransTranslatedLocPort);

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DynamicTransOutsideIp);
        CliPrintf (CliHandle, "%-15s", pu1String);

        CliPrintf (CliHandle, "%u\r\n", i4DynamicTransOutsidePort);

    }
    while (nmhGetNextIndexNatDynamicTransTable (i4DynamicTransIfNum,
                                                &i4DynamicTransIfNum,
                                                u4DynamicTransLocIp,
                                                &u4DynamicTransLocIp,
                                                i4DynamicTransLocPort,
                                                &i4DynamicTransLocPort,
                                                u4DynamicTransOutsideIp,
                                                &u4DynamicTransOutsideIp,
                                                i4DynamicTransOutsidePort,
                                                &i4DynamicTransOutsidePort)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliShowVirtualServers                               *
*                                                                         *
* Description   :  This function is invoked to get the Virtual Servers    *
*                  conguration                                            *
*                  table and store in the output buffer.                  *
*                                                                         *
* Input (s)     :  CliHandle                                              *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
***************************************************************************/
INT4
NatCliShowVirtualServers (tCliHandle CliHandle)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { NAT_ZERO };
    CHR1               *pu1String = NULL;

    INT4                i4IfNum = NAT_ZERO;
    INT4                i4NextIfNum = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    UINT4               u4NextLocIpAddr = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4NextStartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4NextEndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    INT4                i4TranslatedLocPort = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    INT4                i4RowStatus = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE *pDescription = NULL;

    if ((nmhGetFirstIndexNatStaticNaptTable (&i4NextIfNum,
                                             &u4NextLocIpAddr,
                                             &i4NextStartLocalPort,
                                             &i4NextEndLocalPort,
                                             &i4NextProtocolNumber))
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else
    {
        if ((pDescription =
             allocmem_octetstring (NAT_NAPT_ENTRY_DESCRN_LEN +
                                   NAT_ONE)) == NULL)
        {
            return CLI_FAILURE;
        }
        MEMSET (pDescription->pu1_OctetList, NAT_ZERO,
                NAT_NAPT_ENTRY_DESCRN_LEN + NAT_ONE);
        CliPrintf (CliHandle, "\r\nVirtual Servers Configuration\r\n"
                   "-----------------------------\r\n");
        do
        {
            i4IfNum = i4NextIfNum;
            u4LocIpAddr = u4NextLocIpAddr;
            i4StartLocalPort = i4NextStartLocalPort;
            i4EndLocalPort = i4NextEndLocalPort;
            i4ProtocolNumber = i4NextProtocolNumber;
            if (i4NextStartLocalPort != i4NextEndLocalPort)
            {
                /* It is a port range entry. No need to show it as a 
                 * virtual server*/
                continue;
            }

            if ((nmhGetNatStaticNaptTranslatedLocalPort (i4IfNum,
                                                         u4LocIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         i4ProtocolNumber,
                                                         &i4TranslatedLocPort))
                == SNMP_FAILURE)
            {
                free_octetstring (pDescription);
                return CLI_FAILURE;
            }

            if ((nmhGetNatStaticNaptTranslatedLocalIp (i4IfNum,
                                                       u4LocIpAddr,
                                                       i4StartLocalPort,
                                                       i4EndLocalPort,
                                                       i4ProtocolNumber,
                                                       &u4TranslatedLocIpAddr))
                == SNMP_FAILURE)
            {
                free_octetstring (pDescription);
                return CLI_FAILURE;
            }

            /*
             *  add virtual server configuration should not depend on 
             *  Translated Local IP so translatedLocalIP validation is
             *  commented - for HDC 45559 
             */
            /*if ((u4TranslatedLocIpAddr == NAT_ZERO  )
             * || (i4TranslatedLocPort == NAT_ZERO  )) */
            if (i4TranslatedLocPort == NAT_ZERO)
            {
                /* No need to display any zero ip port entry which may
                 * be restored by msr which is using cas.conf of older
                 * version of software */
                continue;
            }

            if ((nmhGetNatStaticNaptEntryStatus (i4IfNum,
                                                 u4LocIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber,
                                                 &i4RowStatus)) == SNMP_FAILURE)
            {
                free_octetstring (pDescription);
                return CLI_FAILURE;
            }

            if (nmhGetNatStaticNaptDescription (i4IfNum,
                                                u4LocIpAddr,
                                                i4StartLocalPort,
                                                i4EndLocalPort,
                                                i4ProtocolNumber,
                                                pDescription) == SNMP_FAILURE)
            {
                free_octetstring (pDescription);
                return CLI_FAILURE;
            }

            CfaCliGetIfName ((UINT4) i4IfNum, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "Interface   :  %s\r\n", au1IfName);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocIpAddr);
            CliPrintf (CliHandle, "Local IP    :  %-17s  ", pu1String);

            CliPrintf (CliHandle, "Local Port  :  %d\r\n", i4StartLocalPort);

            if (i4NextProtocolNumber == NAT_TCP)
            {
                CliPrintf (CliHandle, "Protocol    :  TCP\r\n");
            }
            else if (i4NextProtocolNumber == NAT_UDP)
            {
                CliPrintf (CliHandle, "Protocol    :  UDP\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Protocol    :  ANY\r\n");
            }

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TranslatedLocIpAddr);
            CliPrintf (CliHandle, "Global IP   :  %-17s  ", pu1String);

            CliPrintf (CliHandle, "Global Port :  %d\r\n", i4TranslatedLocPort);

            CliPrintf (CliHandle, "Description :  %s\r\n",
                       pDescription->pu1_OctetList);

            CliPrintf (CliHandle, "Status      :  %s\r\n",
                       ((i4RowStatus ==
                         NAT_STATUS_ACTIVE) ? "Enabled" : "Disabled"));

            CliPrintf
                (CliHandle,
                 "------------------------------------------------------\r\n");
        }
        while ((nmhGetNextIndexNatStaticNaptTable (i4IfNum,
                                                   &i4NextIfNum,
                                                   u4LocIpAddr,
                                                   &u4NextLocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4NextStartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4NextEndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4NextProtocolNumber))
               == SNMP_SUCCESS);
        free_octetstring (pDescription);
    }

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/**************************************************************************
* Function Name :  NatCliShowPortRangeForwarding                          *
*                                                                         *
* Description   :  This function displays the Port Range Forwarding       *
*                  configuration table                                    *
*                                                                         *
* Input (s)     :  CliHandle                                              *
*                                                                         *
* Output (s)    :  Contents of Port Range Forwarding configuration table  *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliShowPortRangeForwarding (tCliHandle CliHandle)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { NAT_ZERO };
    CHR1               *pu1String = NULL;

    INT4                i4IfNum = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4TranslatedLocPort = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    INT4                i4RowStatus = NAT_ZERO;

    if ((nmhGetFirstIndexNatStaticNaptTable (&i4IfNum,
                                             &u4LocIpAddr,
                                             &i4StartLocalPort,
                                             &i4EndLocalPort,
                                             &i4ProtocolNumber)) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "\r\nPort Range Configuration\r\n"
                   "------------------------\r\n");
        CliPrintf (CliHandle,
                   "\r\n  I/f        Local IP       Start Port   End Port\
                   Protocol   Status");
        CliPrintf (CliHandle,
                   "\r\n-------   ---------------   ----------   --------\
                   --------   ------\r\n");

        do
        {
            if ((nmhGetNatStaticNaptTranslatedLocalPort (i4IfNum,
                                                         u4LocIpAddr,
                                                         i4StartLocalPort,
                                                         i4EndLocalPort,
                                                         i4ProtocolNumber,
                                                         &i4TranslatedLocPort))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if ((nmhGetNatStaticNaptTranslatedLocalIp (i4IfNum,
                                                       u4LocIpAddr,
                                                       i4StartLocalPort,
                                                       i4EndLocalPort,
                                                       i4ProtocolNumber,
                                                       &u4TranslatedLocIpAddr))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if ((nmhGetNatStaticNaptEntryStatus (i4IfNum,
                                                 u4LocIpAddr,
                                                 i4StartLocalPort,
                                                 i4EndLocalPort,
                                                 i4ProtocolNumber,
                                                 &i4RowStatus)) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            CfaCliGetIfName ((UINT4) i4IfNum, (INT1 *) au1IfName);

            CliPrintf (CliHandle, "%-10s", au1IfName);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocIpAddr);
            CliPrintf (CliHandle, "%-18s", pu1String);
            CliPrintf (CliHandle, "%-13d", i4StartLocalPort);
            CliPrintf (CliHandle, "%-11d", i4EndLocalPort);
            CliPrintf (CliHandle, "%-11d", i4ProtocolNumber);

            CliPrintf (CliHandle, "%s\r\n",
                       ((i4RowStatus ==
                         NAT_STATUS_ACTIVE) ? "Enabled" : "Disabled"));

        }
        while ((nmhGetNextIndexNatStaticNaptTable (i4IfNum,
                                                   &i4IfNum,
                                                   u4LocIpAddr,
                                                   &u4LocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4StartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4EndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4ProtocolNumber)) ==
               SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/**************************************************************************
 * Function Name :  NatCliProcessShowNatIfaceConfig                        *
 *                                                                         *
 * Description   :  This function used to Display the Nat interface Status *
 *                                                                         *
 * Input (s)     :  CliHandle      - CliContext ID.                        *
 *                                                                         *
 * Output (s)    :  Nat interface Status.                                  *
 *                                                                         *
 * Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
 ***************************************************************************/

INT4
NatCliProcessShowNatIfaceConfig (tCliHandle CliHandle)
{
    UINT1               au1IfName[NAT_CLI_MAX_INTF_NAME_LEN] = { NAT_ZERO };
    INT4                i4NatIfInterfaceNumber = NAT_ZERO;
    INT4                i4PrevNatIfInterfaceNumber = NAT_ZERO;
    INT4                i4RetValNatIfNat = NAT_ZERO;
    INT4                i4RetValNatIfNapt = NAT_ZERO;

    CliPrintf (CliHandle, "\r\nNAT Configuration on the WAN Interface\r\n");
    CliPrintf (CliHandle, "\r--------------------------------------");
    CliPrintf (CliHandle, "\r\nInterface       NAT         NAPT  \r\n");
    CliPrintf (CliHandle, "---------       ------      -------  \r\n");

    if (nmhGetFirstIndexNatIfTable (&i4NatIfInterfaceNumber) == SNMP_FAILURE)
    {
        /*CliPrintf (CliHandle, "No Data Get First Index. \r\n"); */
        return CLI_FAILURE;
    }

    do
    {
        if (CfaCliGetIfName ((UINT4) i4NatIfInterfaceNumber, (INT1 *)
                             au1IfName) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "Interface %d is not Up.\r\n",
                       i4NatIfInterfaceNumber);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "%-16s", au1IfName);

        if (nmhGetNatIfNat (i4NatIfInterfaceNumber, &i4RetValNatIfNat)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "%-12s",
                   ((NAT_ENABLE == i4RetValNatIfNat) ? "Enabled" : "Disabled"));

        if (nmhGetNatIfNapt (i4NatIfInterfaceNumber, &i4RetValNatIfNapt)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "%-12s\r\n",
                   ((NAT_ENABLE ==
                     i4RetValNatIfNapt) ? "Enabled" : "Disabled"));

        i4PrevNatIfInterfaceNumber = i4NatIfInterfaceNumber;
        i4NatIfInterfaceNumber = NAT_ZERO;

    }
    while (nmhGetNextIndexNatIfTable (i4PrevNatIfInterfaceNumber,
                                      &i4NatIfInterfaceNumber) == SNMP_SUCCESS);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/**************************************************************************
* Function Name :  NatCliShowPortTrigger                                  *
*                                                                         *
* Description   :  This function is invoked to get the Port trigger       *
*                  reseved list table and store in the output buffer.     *
*                                                                         *
* Input (s)     :  pu1NatInput - Not used.                                *
*                                                                         *
* Output (s)    :  ppu1Output - Formated output message.                  *
*                                                                         *
* Returns       :  None.                                                  *
**************************************************************************/

INT4
NatCliShowPortTrigger (tCliHandle CliHandle)
{
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE *pInBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pOutBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextInBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextOutBoundPortRange = NULL;
    tSNMP_OCTET_STRING_TYPE *pApplicationName = NULL;

    pInBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pInBoundPortRange == NULL)
    {
        return (CLI_FAILURE);
    }

    pOutBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pOutBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        return (CLI_FAILURE);
    }

    pNextInBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pNextInBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        return (CLI_FAILURE);
    }

    pNextOutBoundPortRange = allocmem_octetstring (NAT_MAX_PORT_STRING_LEN);

    if (pNextOutBoundPortRange == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        return (CLI_FAILURE);
    }

    pApplicationName = allocmem_octetstring (NAT_MAX_APP_NAME_LEN);

    if (pApplicationName == NULL)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        free_octetstring (pNextOutBoundPortRange);
        return (CLI_FAILURE);
    }

    MEMSET (pApplicationName->pu1_OctetList, NAT_ZERO, NAT_MAX_APP_NAME_LEN);
    MEMSET (pInBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);
    MEMSET (pNextInBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);
    MEMSET (pOutBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);
    MEMSET (pNextOutBoundPortRange->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN);

    pApplicationName->i4_Length = NAT_ZERO;
    pInBoundPortRange->i4_Length = NAT_ZERO;
    pNextInBoundPortRange->i4_Length = NAT_ZERO;
    pOutBoundPortRange->i4_Length = NAT_ZERO;
    pNextOutBoundPortRange->i4_Length = NAT_ZERO;

    /* scan all the interfaces to get the list of port trigger
     * nodes. Format the output and store in the output buffer */
    if (nmhGetFirstIndexNatPortTrigInfoTable (pInBoundPortRange,
                                              pOutBoundPortRange,
                                              &i4ProtocolNumber) ==
        SNMP_FAILURE)
    {
        free_octetstring (pInBoundPortRange);
        free_octetstring (pOutBoundPortRange);
        free_octetstring (pNextInBoundPortRange);
        free_octetstring (pNextOutBoundPortRange);
        free_octetstring (pApplicationName);
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle,
               "Application     Outbound       Inbound         Protocol \r\n");

    CliPrintf (CliHandle,
               "Name            Port Range     Port Range             \r\n");

    CliPrintf (CliHandle,
               "--------------- -------------- --------------- -------- \r\n");

    do
    {
        if ((pNextInBoundPortRange->i4_Length != NAT_ZERO)
            || (pNextOutBoundPortRange->i4_Length != NAT_ZERO))
        {
            MEMSET (pInBoundPortRange->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_PORT_STRING_LEN);
            MEMCPY (pInBoundPortRange->pu1_OctetList,
                    pNextInBoundPortRange->pu1_OctetList,
                    pNextInBoundPortRange->i4_Length);
            pInBoundPortRange->i4_Length = pNextInBoundPortRange->i4_Length;

            MEMSET (pOutBoundPortRange->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_PORT_STRING_LEN);
            MEMCPY (pOutBoundPortRange->pu1_OctetList,
                    pNextOutBoundPortRange->pu1_OctetList,
                    pNextOutBoundPortRange->i4_Length);
            pOutBoundPortRange->i4_Length = pNextOutBoundPortRange->i4_Length;

            i4ProtocolNumber = i4NextProtocolNumber;

            MEMSET (pApplicationName->pu1_OctetList, NAT_ZERO,
                    NAT_MAX_APP_NAME_LEN);
        }

        if (nmhGetNatPortTrigInfoAppName (pInBoundPortRange, pOutBoundPortRange,
                                          i4ProtocolNumber,
                                          pApplicationName) == SNMP_FAILURE)
        {
            free_octetstring (pInBoundPortRange);
            free_octetstring (pOutBoundPortRange);
            free_octetstring (pNextInBoundPortRange);
            free_octetstring (pNextOutBoundPortRange);
            free_octetstring (pApplicationName);
            return (CLI_FAILURE);
        }

        CliPrintf (CliHandle, "%-17s", pApplicationName->pu1_OctetList);

        CliPrintf (CliHandle, "%-15s", pOutBoundPortRange->pu1_OctetList);

        CliPrintf (CliHandle, "%-16s", pInBoundPortRange->pu1_OctetList);

        if (i4ProtocolNumber == NAT_TCP)
        {
            CliPrintf (CliHandle, "TCP");
        }
        else if (i4ProtocolNumber == NAT_UDP)
        {
            CliPrintf (CliHandle, "UDP");
        }
        else
        {
            CliPrintf (CliHandle, "ANY");
        }
        CliPrintf (CliHandle, "\r\n");

    }
    while (nmhGetNextIndexNatPortTrigInfoTable (pInBoundPortRange,
                                                pNextInBoundPortRange,
                                                pOutBoundPortRange,
                                                pNextOutBoundPortRange,
                                                i4ProtocolNumber,
                                                &i4NextProtocolNumber) !=
           SNMP_FAILURE);

    free_octetstring (pInBoundPortRange);
    free_octetstring (pOutBoundPortRange);
    free_octetstring (pNextInBoundPortRange);
    free_octetstring (pNextOutBoundPortRange);
    free_octetstring (pApplicationName);

    return (CLI_SUCCESS);
}

/**************************************************************************
* Function Name :  NatCliShowPortTriggerReserved                          *
*                                                                         *
* Description   :  This function is invoked to get the Port trigger       *
*                  reseved list table and store in the output buffer.     *
*                                                                         *
* Input (s)     :  pu1NatInput - Not used.                                *
*                                                                         *
* Output (s)    :  ppu1Output - Formated output message.                  *
*                                                                         *
* Returns       :  None.                                                  *
**************************************************************************/

VOID
NatCliShowPortTriggerReserved (tCliHandle CliHandle)
{
    CHR1               *pu1String = NULL;
    tSNMP_OCTET_STRING_TYPE *pDisplayString = NULL;
    INT4                i4AppIndex = NAT_ZERO;
    INT4                i4Protocol = NAT_ZERO;
    UINT4               u4IpAddress = NAT_ZERO;

    if ((pDisplayString =
         allocmem_octetstring (NAT_MAX_PORT_STRING_LEN + NAT_ONE)) == NULL)
    {
        return;
    }
    MEMSET (pDisplayString->pu1_OctetList, NAT_ZERO,
            NAT_MAX_PORT_STRING_LEN + NAT_ONE);

    CliPrintf (CliHandle,
               "Application     Local           Remote           Outbound\
               Inbound     Prot\r\n");

    CliPrintf (CliHandle,
               "Name            IpAddress       IpAddress        Port Range\
               Port Range  ocol\r\n");

    CliPrintf (CliHandle,
               "------------- ---------------- ---------------- -----------\
               ----------- -----\r\n");

    /* scan all the interfaces to get the list of port trigger
     * nodes. Format the output and store in the output buffer */

    if (SNMP_FAILURE == nmhGetFirstIndexNatRsvdPortTrigInfoTable (&i4AppIndex))
    {
        CliPrintf (CliHandle, "\r\n");
        free_octetstring (pDisplayString);
        return;
    }

    do
    {
        /* Name */
        nmhGetNatRsvdPortTrigInfoAppName (i4AppIndex, pDisplayString);
        CliPrintf (CliHandle, "%-14s", pDisplayString->pu1_OctetList);
        MEMSET (pDisplayString->pu1_OctetList, NAT_ZERO,
                NAT_MAX_PORT_STRING_LEN + NAT_ONE);

        /* Local IP Address */
        nmhGetNatRsvdPortTrigInfoLocalIp (i4AppIndex, &u4IpAddress);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddress);
        CliPrintf (CliHandle, "%-17s", pu1String);

        /* Remote IP Address */
        nmhGetNatRsvdPortTrigInfoRemoteIp (i4AppIndex, &u4IpAddress);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddress);
        CliPrintf (CliHandle, "%-17s", pu1String);

        /* Outbound port range */
        nmhGetNatRsvdPortTrigInfoOutBoundPortRange (i4AppIndex, pDisplayString);
        CliPrintf (CliHandle, "%-12s", pDisplayString->pu1_OctetList);
        MEMSET (pDisplayString->pu1_OctetList, NAT_ZERO,
                NAT_MAX_PORT_STRING_LEN + NAT_ONE);

        /* Inbound port range */
        nmhGetNatRsvdPortTrigInfoInBoundPortRange (i4AppIndex, pDisplayString);
        CliPrintf (CliHandle, "%-12s", pDisplayString->pu1_OctetList);
        MEMSET (pDisplayString->pu1_OctetList, NAT_ZERO,
                NAT_MAX_PORT_STRING_LEN + NAT_ONE);

        /* Protocol */
        nmhGetNatRsvdPortTrigInfoProtocol (i4AppIndex, &i4Protocol);
        if (i4Protocol == NAT_TCP)
        {
            CliPrintf (CliHandle, "TCP");
        }
        else if (i4Protocol == NAT_UDP)
        {
            CliPrintf (CliHandle, "UDP");
        }
        else
        {
            CliPrintf (CliHandle, "ANY");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    while (SNMP_SUCCESS == nmhGetNextIndexNatRsvdPortTrigInfoTable (i4AppIndex,
                                                                    &i4AppIndex));

    CliPrintf (CliHandle, "\r\n");
    free_octetstring (pDisplayString);
    return;
}

/* No  MIB */
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : NatGetNatConfigPrompt                                   */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
NatGetNatConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = NAT_ZERO;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (NAT_CLI_MODE);

    if (STRNCMP (pi1ModeName, NAT_CLI_MODE, u4Len) != NAT_ZERO)
    {
        return FALSE;
    }
    STRCPY (pi1DispStr, "(config-nat)#");
    return TRUE;
}

#ifndef SECURITY_KERNEL_MAKE_WANTED
/**************************************************************************
* Function Name :  NatCliSipShowStaticNapt                                *
*                                                                         *
* Description   :  This function is invoked to get the static NAPT        *
*                  Entries addded for SIP ALG.                            *
*                                                                         *
* Input (s)     :  pu1NatInput - Not used.                                *
*                                                                         *
* Output (s)    :  ppu1Output - Formated output message.                  *
*                                                                         *
* Returns       :  None.                                                  *
**************************************************************************/
INT1
NatCliSipShowStaticNapt (tCliHandle CliHandle)
{
    INT4                i4IfNum = NAT_ZERO;
    UINT1               au1IfName[NAT_CLI_INTF_NAME_LEN] = { NAT_ZERO };
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    CHR1               *pu1String = NULL;

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "Interface       Local            Local      Transltd\
               Transltd  Timer\r\n");

    CliPrintf (CliHandle, "Name            IpAddress        Port       Address\
               Port      Status\r\n");

    CliPrintf (CliHandle,
               "-------------   ---------------- ---------- ---------------\
               --------- ------\r\n");

    for (i4IfNum = NAT_ONE; i4IfNum < NAT_MAX_NUM_IF; i4IfNum++)
    {
        pIfNode = gapNatIfTable[i4IfNum];
        if (pIfNode != NULL)
        {
            TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                          tStaticNaptEntry *)
            {
                if ((pStaticNaptEntryNode->u1AppCallStatus == NAT_ON_HOLD) ||
                    (pStaticNaptEntryNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
                {
                    CfaCliGetIfName ((UINT4) i4IfNum, (INT1 *) au1IfName);
                    CliPrintf (CliHandle, "%-16s", au1IfName);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               pStaticNaptEntryNode->
                                               u4LocIpAddr);
                    CliPrintf (CliHandle, "%-17s", pu1String);

                    CliPrintf (CliHandle, "%-11d",
                               pStaticNaptEntryNode->u2StartLocPort);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               pStaticNaptEntryNode->
                                               u4TranslatedLocIpAddr);
                    CliPrintf (CliHandle, "%-16s", pu1String);

                    CliPrintf (CliHandle, "%-10d",
                               pStaticNaptEntryNode->u2TranslatedLocPort);

                    CliPrintf (CliHandle, "%s\r\n",
                               ((pStaticNaptEntryNode->u1AppCallStatus ==
                                 NAT_ON_HOLD) ? "Off" : "On"));
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  NatCliSipShowPartial                                   *
*                                                                         *
* Description   :  This function gets the Partial NAT entries added       *
*                  for SIP ALG                                            *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT1
NatCliSipShowPartial (tCliHandle CliHandle)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;

    PrintPartialEntriesHdr (CliHandle);

    TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                  tNatPartialLinkNode *)
    {
        if ((pNatPartialLinkNode->u1AppCallStatus == NAT_ON_HOLD) ||
            (pNatPartialLinkNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
        {
            PrintSipParitalEntry (CliHandle, pNatPartialLinkNode);
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}
#endif
/**************************************************************************
* Function Name :  NatCliSetNatTimeout                                    *
*                                                                         *
* Description   :  This function is invoked to get the Port trigger       *
*                  reseved list table and store in the output buffer.     *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  u4Option  - Option for setting                         *
*                              a) Idle TImeout b) TCP Timeout             *
*                              c) UDP Timeout                             *
*                  i4TimeoutValue - Timeout Value to set.                 *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliSetNatTimeout (tCliHandle CliHandle, UINT4 u4Option, INT4 i4TimeoutValue)
{
    UINT4               u4ErrorCode = NAT_ZERO;

    switch (u4Option)
    {
        case CLI_NAT_IDLE_TIMEOUT:
            if ((nmhTestv2NatIdleTimeOut (&u4ErrorCode, i4TimeoutValue)) ==
                SNMP_SUCCESS)
            {
                if ((nmhSetNatIdleTimeOut (i4TimeoutValue)) == SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
            }
            break;
        case CLI_NAT_TCP_TIMEOUT:
            if ((nmhTestv2NatTcpTimeOut (&u4ErrorCode, i4TimeoutValue)) ==
                SNMP_SUCCESS)
            {
                if ((nmhSetNatTcpTimeOut (i4TimeoutValue)) == SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
            }
            break;
        case CLI_NAT_UDP_TIMEOUT:
            if ((nmhTestv2NatUdpTimeOut (&u4ErrorCode, i4TimeoutValue)) ==
                SNMP_SUCCESS)
            {
                if ((nmhSetNatUdpTimeOut (i4TimeoutValue)) == SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
            }
            break;
        default:
            break;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssNatShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the NAT debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssNatShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = NAT_ZERO;

    nmhGetNatTrcFlag (&i4DbgLevel);
    if (i4DbgLevel == NAT_ZERO)
    {
        return;
    }
    CliPrintf (CliHandle, "\rNAT :");

    if (i4DbgLevel & NAT_DBG_ENTRY)
    {
        CliPrintf (CliHandle, "\r\n  NAT entry debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_EXIT)
    {
        CliPrintf (CliHandle, "\r\n  NAT exit debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_PKT_FLOW)
    {
        CliPrintf (CliHandle, "\r\n  NAT packet flow debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_DNS)
    {
        CliPrintf (CliHandle, "\r\n  NAT DNS debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_FTP)
    {
        CliPrintf (CliHandle, "\r\n  NAT FTP debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_HTTP)
    {
        CliPrintf (CliHandle, "\r\n  NAT HTTP debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_SMTP)
    {
        CliPrintf (CliHandle, "\r\n  NAT SMTP debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_ICMP)
    {
        CliPrintf (CliHandle, "\r\n  NAT ICMP debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_PPTP)
    {
        CliPrintf (CliHandle, "\r\n  NAT PPTP debugging is on");
    }
    if (i4DbgLevel & NAT_DBG_MEM)
    {
        CliPrintf (CliHandle, "\r\n  NAT memory debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/**************************************************************************
* Function Name :  NatCliShowSipAlgPort                                   *
*                                                                         *
* Description   :  This function displays the port for sip alg            *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliShowSipAlgPort (tCliHandle CliHandle)
{
    INT4                i4SipAlgPort = NAT_ZERO;
    if (SNMP_SUCCESS == nmhGetSipAlgPort (&i4SipAlgPort))
    {
        CliPrintf (CliHandle, "\r Sip Alg Port is: %d\n\r", i4SipAlgPort);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/**************************************************************************
* Function Name :  NatCliShowSipAlgPartialEntryTimer                       *
*                                                                         *
* Description   :  This function displays the value of the timer for      *
*                  partial entry for sip alg                              *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliShowSipAlgPartialEntryTimer (tCliHandle CliHandle)
{
    INT4                i4SipAlgPartialEntryTmer = NAT_ZERO;
    if (SNMP_SUCCESS ==
        nmhGetNatSipAlgPartialEntryTimeOut (&i4SipAlgPartialEntryTmer))
    {
        CliPrintf (CliHandle, "\r Sip Time-Out: %d\n\r",
                   i4SipAlgPartialEntryTmer);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;

}

/**************************************************************************
* Function Name :  NatCliSetSipAlgPort                                    *
*                                                                         *
* Description   :  This function sets the port for sip alg                *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                  u4SipAlgPort - value to be set                         *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliSetSipAlgPort (tCliHandle CliHandle, UINT4 u4SipAlgPort)
{
    UINT4               u4Error = NAT_ZERO;

    UNUSED_PARAM (CliHandle);

    if (SNMP_SUCCESS == nmhTestv2SipAlgPort (&u4Error, (INT4) u4SipAlgPort))
    {
        if (SNMP_SUCCESS == nmhSetSipAlgPort ((INT4) u4SipAlgPort))
        {
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;
}

/**************************************************************************
* Function Name :  NatCliSetSipAlgPartialEntryTimer                       *
*                                                                         *
* Description   :  This function sets the value of the partial timer      *
*                  entry for SIP ALG                                      *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                  u4SipAlgPartialEntryTimer - value to be set            *
*                                                                         *
* Output (s)    :  None                                                   *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT4
NatCliSetSipAlgPartialEntryTimer (tCliHandle CliHandle,
                                  UINT4 u4SipAlgPartialEntryTimer)
{
    UINT4               u4Error = NAT_ZERO;
    UNUSED_PARAM (CliHandle);

    if (SNMP_SUCCESS ==
        nmhTestv2NatSipAlgPartialEntryTimeOut (&u4Error,
                                               (INT4)
                                               u4SipAlgPartialEntryTimer))
    {
        if (SNMP_SUCCESS ==
            nmhSetNatSipAlgPartialEntryTimeOut ((INT4)
                                                u4SipAlgPartialEntryTimer))
        {
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;

}

#ifdef SECURITY_KERNEL_MAKE_WANTED
/**************************************************************************
* Function Name :  NatCliSipShowPartial                                   *
*                                                                         *
* Description   :  This function gets the Partial NAT entries added       *
*                  for SIP ALG                                            *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
INT1
NatCliSipShowPartial (tCliHandle CliHandle)
{
    INT4                rc;
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatSipShowPartialLinks lv;

    lv.cmd = NAT_SIP_SHOW_PARTIAL_ENTRIES;
    lv.u4PrintCount = NAT_ZERO;
    lv.u1EntryFound = OSIX_FALSE;

    rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
    if (rc < NAT_ZERO)
    {
        CliPrintf (CliHandle, "%% ERROR :  Error reading the information from\
                   kernel !\r\n");
        return CLI_FAILURE;
    }

    PrintPartialEntriesHdr (CliHandle);

    while (rc >= NAT_ZERO)
    {
        pNatPartialLinkNode = &(lv.NatPartialLinkNode);
        if (lv.u1EntryFound == OSIX_FALSE)
        {
            break;
        }
        lv.u4PrintCount++;

        PrintSipParitalEntry (CliHandle, pNatPartialLinkNode);

        rc = ioctl (gi4SecDevFd, NAT_NMH_IOCTL, &lv);
        if (rc < NAT_ZERO)
        {
            CliPrintf (CliHandle,
                       "%% ERROR :  Error reading the information from\
                       kernel !\r\n");
            return CLI_FAILURE;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}
#endif
/**************************************************************************
* Function Name :  PrintPartialEntriesHdr                                 *
*                                                                         *
* Description   :  This function prints the CLI Header for Partial NAT    *
*                  entries added for SIP ALG                              *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
VOID
PrintPartialEntriesHdr (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\r\nNat Partial List Configuration\r\n");
    CliPrintf (CliHandle, "------------------------------\r\n");
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle,
               "Local            Local      Translated        Translated\
               Remote           Remote     Proto  Direc Timer\r\n");

    CliPrintf (CliHandle, "IpAddress        Port       Address           Port\
               Address          Port       col    tion  Status\r\n");

    CliPrintf (CliHandle,
               "---------------- ---------- ----------------- ----------\
               ---------------- ---------- ------ ----- -------\r\n");
}

/**************************************************************************
* Function Name :  PrintSipParitalEntry                                   *
*                                                                         *
* Description   :  This function prints the Partial NAT entries added     *
*                  for SIP ALG                                            *
*                                                                         *
* Input (s)     :  CliHandle - Handle to the Current CLI Context          *
*                                                                         *
*                  pNatPartialLinkNode - Partial Link Node                *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  CLI_SUCCESS/CLI_FAILURE                                *
**************************************************************************/
VOID
PrintSipParitalEntry (tCliHandle CliHandle,
                      tNatPartialLinkNode * pNatPartialLinkNode)
{
    CHR1               *pu1String = NULL;

    if (pNatPartialLinkNode->u4LocIpAddr != NAT_ZERO)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, pNatPartialLinkNode->u4LocIpAddr);
        CliPrintf (CliHandle, "%-17s", pu1String);
    }
    else
        CliPrintf (CliHandle, "                ");

    if (pNatPartialLinkNode->u2LocPort != NAT_ZERO)
    {
        CliPrintf (CliHandle, "%-11d", pNatPartialLinkNode->u2LocPort);
    }
    else
        CliPrintf (CliHandle, "           ");

    if (pNatPartialLinkNode->u4TranslatedLocIpAddr != NAT_ZERO)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, pNatPartialLinkNode->
                                   u4TranslatedLocIpAddr);
        CliPrintf (CliHandle, "%-18s", pu1String);
    }
    else
        CliPrintf (CliHandle, "                 ");

    if (pNatPartialLinkNode->u2TranslatedLocPort != NAT_ZERO)
    {
        CliPrintf (CliHandle, "%-12d",
                   pNatPartialLinkNode->u2TranslatedLocPort);
    }
    else
        CliPrintf (CliHandle, "              ");

    if (pNatPartialLinkNode->u4OutIpAddr != NAT_ZERO)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, pNatPartialLinkNode->u4OutIpAddr);
        CliPrintf (CliHandle, "%-17s", pu1String);
    }
    else
        CliPrintf (CliHandle, "                ");

    if (pNatPartialLinkNode->u2OutPort != NAT_ZERO)
    {
        CliPrintf (CliHandle, "%-11d", pNatPartialLinkNode->u2OutPort);
    }
    else
        CliPrintf (CliHandle, "           ");

    if (pNatPartialLinkNode->u1PktType == NAT_TCP)
    {
        CliPrintf (CliHandle, "TCP    ");
    }
    else if (pNatPartialLinkNode->u1PktType == NAT_UDP)
    {
        CliPrintf (CliHandle, "UDP    ");
    }
    else
    {
        CliPrintf (CliHandle, "ANY    ");
    }
    CliPrintf (CliHandle, "%s",
               ((pNatPartialLinkNode->u1Direction ==
                 NAT_OUTBOUND) ? "OUT   " : "IN    "));
    CliPrintf (CliHandle, "%s\r\n",
               ((pNatPartialLinkNode->u1AppCallStatus ==
                 NAT_ON_HOLD) ? "Off" : "On"));

    return;
}

/*****************************************************************************/
/* Function Name      : NatShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in NAT module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
NatShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    if (NatShowRunningConfigScalar (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Display the Policy NAT configurations. */
    NatCliShowRunningConfigPolicy (CliHandle);

    if (NatShowRunningConfigInterface (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    UNUSED_PARAM (u4Module);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : NatShowRunningConfigScalar                           */
/*                                                                           */
/* Description        : Displays current configurations done in NAT scalar   */
/*                        objects                                            */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*****************************************************************************/
INT4
NatShowRunningConfigScalar (tCliHandle CliHandle)
{
    INT4                i4NatGlobalStatus = NAT_ZERO;
    INT4                i4NatIdleTimeOut = NAT_ZERO;
    INT4                i4NatTcpTimeOut = NAT_ZERO;
    INT4                i4NatUdpTimeOut = NAT_ZERO;

    nmhGetNatEnable (&i4NatGlobalStatus);
    nmhGetNatIdleTimeOut (&i4NatIdleTimeOut);
    nmhGetNatTcpTimeOut (&i4NatTcpTimeOut);
    nmhGetNatUdpTimeOut (&i4NatUdpTimeOut);

    if (i4NatGlobalStatus != NAT_ENABLE)
    {
        CliPrintf (CliHandle, "no ip nat\r\n");
    }
    if (i4NatIdleTimeOut != NAT_DEF_IDLE_TIMEOUT)
    {
        CliPrintf (CliHandle, "ip nat idle timeout %d\r\n", i4NatIdleTimeOut);
    }
    if (i4NatTcpTimeOut != NAT_DEF_TCP_TIMEOUT)
    {
        CliPrintf (CliHandle, "ip nat tcp timeout %d\r\n", i4NatTcpTimeOut);
    }
    if (i4NatUdpTimeOut != NAT_DEF_UDP_TIMEOUT)
    {
        CliPrintf (CliHandle, "ip nat udp timeout %d\r\n", i4NatUdpTimeOut);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : NatShowRunningConfigInterface                        */
/*                                                                           */
/* Description        : Displays current configurations in NAT for           */
/*                      specified  interface                                 */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
NatShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT1                i1RetVal = NAT_ZERO;
    INT4                i4Index = NAT_ZERO;
    INT4                i4NextIndex = NAT_ZERO;
    INT4                i4NatIfNat = NAT_ZERO;
    INT4                i4NatIfNapt = NAT_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { NAT_ZERO };

    if (nmhGetFirstIndexNatIfTable (&i4Index) == SNMP_SUCCESS)
    {
        i4NextIndex = i4Index;

        do
        {
            i4Index = i4NextIndex;
            i1RetVal = nmhGetNatIfNat (i4Index, &i4NatIfNat);
            if (i1RetVal == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i1RetVal = nmhGetNatIfNapt (i4Index, &i4NatIfNapt);
            if (i1RetVal == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if ((i4NatIfNat == NAT_ENABLE) && (i4NatIfNapt == NAT_ENABLE))
            {
                MEMSET (&au1IfName[NAT_INDEX_0], NAT_ZERO,
                        CFA_MAX_PORT_NAME_LENGTH);
                CfaCliConfGetIfName ((UINT4) i4Index,
                                     (INT1 *) &au1IfName[NAT_INDEX_0]);

                CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

                NatShowRunningConfigInterfaceDetails (CliHandle, i4Index);

                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while (nmhGetNextIndexNatIfTable (i4Index,
                                          &i4NextIndex) == SNMP_SUCCESS);

    }
    else
    {
        return (CLI_FAILURE);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : NatShowRunningConfigInterfaceDetails                 */
/*                                                                           */
/* Description        : This function displays the currently operating       */
/*                      NAT configurations for a particular interface.       */
/*                                                                           */
/* Input(s)           : CliHandle,i4Index                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
NatShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4IfNum = NAT_ZERO;
    INT4                i4NextIfNum = NAT_ZERO;
    INT4                i4StartLocalPort = NAT_ZERO;
    INT4                i4NextStartLocalPort = NAT_ZERO;
    INT4                i4EndLocalPort = NAT_ZERO;
    INT4                i4NextEndLocalPort = NAT_ZERO;
    INT4                i4ProtocolNumber = NAT_ZERO;
    INT4                i4NextProtocolNumber = NAT_ZERO;
    INT4                i4TranslatedLocPort = NAT_ZERO;
    INT4                i4RowStatus = NAT_ZERO;
    INT4                i4OtherApp = NAT_ZERO;
    INT4                i4RetValNatIfNat = NAT_ZERO;
    INT4                i4RetValNatIfNapt = NAT_ZERO;
    UINT4               u4NextLocIpAddr = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    UINT4               u4StaticLocalIp = NAT_ZERO;
    UINT4               u4TranslatedLocalIp = NAT_ZERO;
    UINT4               u4GlobalAddrTranslatedLocIp = NAT_ZERO;
    UINT4               u4GlobalAddressMask = NAT_ZERO;
    UINT4               u4WanIfIp = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE *pDescription = NULL;
    CHR1               *pu1String = NULL;

    nmhGetNatIfNat (i4Index, &i4RetValNatIfNat);
    nmhGetNatIfNapt (i4Index, &i4RetValNatIfNapt);

    if ((i4RetValNatIfNat == NAT_ENABLE) && (i4RetValNatIfNapt == NAT_ENABLE))
    {
        CliPrintf (CliHandle, "interface nat enable\r\n");

    }
    else if ((i4RetValNatIfNat == NAT_ENABLE) &&
             (i4RetValNatIfNapt == NAT_DISABLE))
    {
        CliPrintf (CliHandle, "interface nat enable\r\n");
        CliPrintf (CliHandle, "ip nat napt disable\r\n");

    }
/** virtual server or port range **/
    if ((nmhGetFirstIndexNatStaticNaptTable (&i4NextIfNum,
                                             &u4NextLocIpAddr,
                                             &i4NextStartLocalPort,
                                             &i4NextEndLocalPort,
                                             &i4NextProtocolNumber))
        == SNMP_SUCCESS)
    {
        if ((pDescription =
             allocmem_octetstring (NAT_NAPT_ENTRY_DESCRN_LEN +
                                   NAT_ONE)) == NULL)
        {
            return CLI_FAILURE;
        }
        MEMSET (pDescription->pu1_OctetList, NAT_ZERO,
                NAT_NAPT_ENTRY_DESCRN_LEN + NAT_ONE);
        do
        {
            i4OtherApp = NAT_ZERO;
            i4IfNum = i4NextIfNum;
            u4LocIpAddr = u4NextLocIpAddr;
            i4StartLocalPort = i4NextStartLocalPort;
            i4EndLocalPort = i4NextEndLocalPort;
            i4ProtocolNumber = i4NextProtocolNumber;
            if (i4NextIfNum == i4Index)
            {
                nmhGetNatStaticNaptEntryStatus (i4IfNum,
                                                u4LocIpAddr,
                                                i4StartLocalPort,
                                                i4EndLocalPort,
                                                i4ProtocolNumber, &i4RowStatus);

                nmhGetNatStaticNaptTranslatedLocalPort (i4IfNum,
                                                        u4LocIpAddr,
                                                        i4StartLocalPort,
                                                        i4EndLocalPort,
                                                        i4ProtocolNumber,
                                                        &i4TranslatedLocPort);

                nmhGetNatStaticNaptTranslatedLocalIp (i4IfNum,
                                                      u4LocIpAddr,
                                                      i4StartLocalPort,
                                                      i4EndLocalPort,
                                                      i4ProtocolNumber,
                                                      &u4TranslatedLocIpAddr);

                nmhGetNatStaticNaptDescription (i4IfNum,
                                                u4LocIpAddr,
                                                i4StartLocalPort,
                                                i4EndLocalPort,
                                                i4ProtocolNumber, pDescription);

                if (i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    if (i4NextStartLocalPort != i4NextEndLocalPort)
                    {
                        /* It is a port range entry. No need to show it as a 
                         * virtual server*/
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocIpAddr);
                        CliPrintf (CliHandle, "portrange %s ", pu1String);
                        switch (i4ProtocolNumber)
                        {
                            case NAT_TCP:
                                CliPrintf (CliHandle, "tcp ");
                                break;
                            case NAT_UDP:
                                CliPrintf (CliHandle, "udp ");
                                break;
                            default:
                                CliPrintf (CliHandle, "any ");
                                break;
                        }
                        CliPrintf (CliHandle, "%d %d\r\n", i4NextStartLocalPort,
                                   i4NextEndLocalPort);
                        continue;
                    }

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocIpAddr);
                    CliPrintf (CliHandle, "virtual server %s ", pu1String);
                    switch (i4StartLocalPort)
                    {
                        case NAT_AUTH_APP_PORT:
                            CliPrintf (CliHandle, "auth ");
                            break;
                        case NAT_DNS_APP_PORT:
                            CliPrintf (CliHandle, "dns ");
                            break;
                        case NAT_FTP_APP_PORT:
                            CliPrintf (CliHandle, "ftp ");
                            break;
                        case NAT_POP3_APP_PORT:
                            CliPrintf (CliHandle, "pop3 ");
                            break;
                        case NAT_PPTP_APP_PORT:
                            CliPrintf (CliHandle, "pptp ");
                            break;
                        case NAT_SMTP_APP_PORT:
                            CliPrintf (CliHandle, "smtp ");
                            break;
                        case NAT_TELNET_APP_PORT:
                            CliPrintf (CliHandle, "telnet ");
                            break;
                        case NAT_HTTP_APP_PORT:
                            CliPrintf (CliHandle, "http ");
                            break;
                        case NAT_NNTP_APP_PORT:
                            CliPrintf (CliHandle, "nntp ");
                            break;
                        case NAT_SNMP_APP_PORT:
                            CliPrintf (CliHandle, "snmp ");
                            break;
                        default:
                            CliPrintf (CliHandle, "%d ", i4StartLocalPort);
                            i4OtherApp = NAT_OTHER_APP_PORT;
                            break;
                    }
                    if (i4OtherApp == NAT_OTHER_APP_PORT)
                    {
                        CliPrintf (CliHandle, "other %d ", i4TranslatedLocPort);

                    }
                    switch (i4ProtocolNumber)
                    {
                        case NAT_TCP:
                            CliPrintf (CliHandle, "tcp ");
                            break;
                        case NAT_UDP:
                            CliPrintf (CliHandle, "udp ");
                            break;
                        default:
                            break;
                    }
                    CliPrintf (CliHandle, "%s\r\n",
                               pDescription->pu1_OctetList);
                }
                else
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LocIpAddr);
                    CliPrintf (CliHandle, "disable virtual server %s %d\r\n",
                               pu1String, i4StartLocalPort);
                }
            }
        }
        while ((nmhGetNextIndexNatStaticNaptTable (i4IfNum,
                                                   &i4NextIfNum,
                                                   u4LocIpAddr,
                                                   &u4NextLocIpAddr,
                                                   i4StartLocalPort,
                                                   &i4NextStartLocalPort,
                                                   i4EndLocalPort,
                                                   &i4NextEndLocalPort,
                                                   i4ProtocolNumber,
                                                   &i4NextProtocolNumber))
               == SNMP_SUCCESS);
        free_octetstring (pDescription);
    }
/** static NAT **/
    if (nmhGetFirstIndexNatStaticTable (&i4IfNum, &u4StaticLocalIp)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Index == i4IfNum)
            {
                nmhGetNatStaticTranslatedLocalIp (i4IfNum, u4StaticLocalIp,
                                                  &u4TranslatedLocalIp);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StaticLocalIp);
                CliPrintf (CliHandle, "static nat %s ", pu1String);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TranslatedLocalIp);
                CliPrintf (CliHandle, "%s\r\n", pu1String);
            }
        }
        while (nmhGetNextIndexNatStaticTable (i4IfNum,
                                              &i4IfNum,
                                              u4StaticLocalIp,
                                              &u4StaticLocalIp) ==
               SNMP_SUCCESS);
    }
 /** ip nat pool **/
    if (nmhGetFirstIndexNatGlobalAddressTable (&i4IfNum,
                                               &u4GlobalAddrTranslatedLocIp)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4IfNum == i4Index)
            {
                nmhGetNatGlobalAddressMask (i4IfNum,
                                            u4GlobalAddrTranslatedLocIp,
                                            &u4GlobalAddressMask);
                u4WanIfIp = NatGetWanInterfaceIpAddr ((UINT4) i4IfNum);
                if (u4WanIfIp != u4GlobalAddrTranslatedLocIp)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               u4GlobalAddrTranslatedLocIp);
                    CliPrintf (CliHandle, "ip nat pool %s ", pu1String);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GlobalAddressMask);
                    CliPrintf (CliHandle, "%s\r\n", pu1String);
                }
            }
        }
        while (nmhGetNextIndexNatGlobalAddressTable (i4IfNum,
                                                     &i4IfNum,
                                                     u4GlobalAddrTranslatedLocIp,
                                                     &u4GlobalAddrTranslatedLocIp)
               == SNMP_SUCCESS);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : NatCliConfigurePolicy                                  */
/*  Description     : This function creates a policy entry if u1Action is    */
/*                    CREATE_AND_WAIT.                                       */
/*                    This function deletes a policy entry if u1Action is    */
/*                    DESTROY.                                               */
/*                    The policy entry can either be static or dynamic       */
/*                    depending on the i4PolicyType.                         */
/*  Input(s)        : CliHandle  - CLI handler.                              */
/*                    i4PolicyType - static /dynamic policy type.            */
/*                    i4PolicyId   - Policy Identifier.                      */
/*                    pu1AclName   - Filter Identifier.                      */
/*                    u1Action     - CREATE_AND_WAIT/DESTROY.                */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : None                                       */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Commands Referred           :                                            */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatCliConfigurePolicy (tCliHandle CliHandle,
                       INT4 i4PolicyType, INT4 i4PolicyId, UINT1 *pu1AclName,
                       UINT1 u1Action)
{
    UINT4               u4ErrorCode = NAT_ZERO;
    tSNMP_OCTET_STRING_TYPE AclName;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);

    AclName.i4_Length = (INT4) STRLEN (pu1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, pu1AclName, AclName.i4_Length);
    if (SNMP_FAILURE ==
        nmhTestv2NatPolicyEntryStatus (&u4ErrorCode,
                                       i4PolicyType, i4PolicyId, &AclName,
                                       u1Action))
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_ACCESS_LIST);
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetNatPolicyEntryStatus (i4PolicyType, i4PolicyId, &AclName,
                                    u1Action))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatCliUpdatePolicy                                     */
/*  Description     : This function updates any existing policy in the system*/
/*  Output(s)       : None.                                                  */
/*  Input(s)        : CliHandle  - CLI handler.                              */
/*                    i4PolicyType - static /dynamic policy type.            */
/*                    i4PolicyId   - Policy Identifier.                      */
/*                    pu1AclName   - Filter Identifier.                      */
/*                    u4TranslatedIp - Translated Ip Address.                */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : None                                       */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Commands Referred           :                                            */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatCliUpdatePolicy (tCliHandle CliHandle,
                    INT4 i4PolicyType, INT4 i4PolicyId, UINT4 u4TranslatedIp)
{
    UINT4               u4ErrorCode = NAT_ZERO;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    UINT1               au1NextAclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE AclName = { NULL, NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE NextAclName = { NULL, NAT_ZERO };

    /* To update the translated IP address. 
     * i. Set the row status to not-in-service
     * ii. set the translated IP address.
     * iii. Set the row status to active again. 
     */

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    MEMSET (au1NextAclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);

    AclName.i4_Length = (INT4) STRLEN (au1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, au1AclName, AclName.i4_Length);

    NextAclName.i4_Length = (INT4) STRLEN (au1NextAclName);
    NextAclName.pu1_OctetList = au1NextAclName;
    MEMCPY (NextAclName.pu1_OctetList, au1NextAclName, NextAclName.i4_Length);

    if (nmhGetNextIndexNatPolicyTable (i4PolicyType,
                                       &i4PolicyType,
                                       i4PolicyId,
                                       &i4PolicyId,
                                       &AclName, &NextAclName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_NAT_ERR_INVALID_POLICY);
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhTestv2NatPolicyEntryStatus (&u4ErrorCode,
                                       i4PolicyType, i4PolicyId, &NextAclName,
                                       NOT_IN_SERVICE))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhTestv2NatPolicyTranslatedIp (&u4ErrorCode,
                                        i4PolicyType, i4PolicyId, &NextAclName,
                                        u4TranslatedIp))
    {
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetNatPolicyEntryStatus (i4PolicyType, i4PolicyId, &NextAclName,
                                    NOT_IN_SERVICE))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetNatPolicyTranslatedIp (i4PolicyType, i4PolicyId, &NextAclName,
                                     u4TranslatedIp))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetNatPolicyEntryStatus (i4PolicyType, i4PolicyId, &NextAclName,
                                    ACTIVE))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatCliShowPolicyTable                                  */
/*  Description     : This function displays the policy table for NAT.       */
/*  Input(s)        : CliHandle - CLI Handle.                                */
/*  Output(s)       : None.                                                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   :                                            */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Commands referred           :  show ip nat policy                        */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT4
NatCliShowPolicyTable (tCliHandle CliHandle)
{
    INT4                i4PolicyId = NAT_ZERO;
    UINT4               u4TranslatedIp = NAT_ZERO;
    INT4                i4PolicyType = NAT_ZERO;
    CHR1               *pu1IpAddress = NULL;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    UINT1               au1NextAclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE AclName = { NULL, NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE NextAclName = { NULL, NAT_ZERO };

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    MEMSET (au1NextAclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);

    AclName.i4_Length = (INT4) STRLEN (au1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, au1AclName, AclName.i4_Length);
    if (SNMP_FAILURE ==
        nmhGetFirstIndexNatPolicyTable (&i4PolicyType, &i4PolicyId, &AclName))
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nNAT Policy Table \r\n");
    CliPrintf (CliHandle, "---------------- \r\n");
    CliPrintf (CliHandle, "\r\nPolicyType     PolicyId       AclName"
               "    Translated IP\r\n");
    CliPrintf (CliHandle, "----------     --------       --------"
               "    -------------\r\n");
    NextAclName.i4_Length = AclName.i4_Length;
    NextAclName.pu1_OctetList = au1NextAclName;
    MEMCPY (NextAclName.pu1_OctetList, AclName.pu1_OctetList,
            AclName.i4_Length);
    do
    {
        nmhGetNatPolicyTranslatedIp (i4PolicyType, i4PolicyId, &NextAclName,
                                     &u4TranslatedIp);
        if (NAT_STATIC_POLICY == i4PolicyType)
        {
            CliPrintf (CliHandle, "%-15s", "Static");
        }
        else
        {
            CliPrintf (CliHandle, "%-15s", "Dynamic");
        }
        CliPrintf (CliHandle, "%-15d", i4PolicyId);
        CliPrintf (CliHandle, "%-15s", NextAclName.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddress, u4TranslatedIp);
        CliPrintf (CliHandle, "%s", pu1IpAddress);
        CliPrintf (CliHandle, "\r\n");

        AclName.i4_Length = NextAclName.i4_Length;
        AclName.pu1_OctetList = NextAclName.pu1_OctetList;
        MEMCPY (AclName.pu1_OctetList, NextAclName.pu1_OctetList,
                NextAclName.i4_Length);
    }
    while (SNMP_SUCCESS == nmhGetNextIndexNatPolicyTable (i4PolicyType,
                                                          &i4PolicyType,
                                                          i4PolicyId,
                                                          &i4PolicyId,
                                                          &AclName,
                                                          &NextAclName));

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : NatCliShowRunningConfigPolicy                          */
/*  Description     : This function displays the policy table configurations */
/*                    for NAT.                                               */
/*  Input(s)        : CliHandle - CLI Handle.                                */
/*  Output(s)       : None.                                                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   :                                            */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Commands referred           :  show running config nat                   */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
NatCliShowRunningConfigPolicy (tCliHandle CliHandle)
{
    INT4                i4PolicyId = NAT_ZERO;
    UINT4               u4TranslatedIpAddr = NAT_ZERO;
    INT4                i4PolicyType = NAT_ZERO;
    CHR1               *pu1PolicyType = NULL;
    CHR1               *pu1IpAddress = NULL;
    UINT1               au1AclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    UINT1               au1NextAclName[NAT_MAX_ACL_NAME_LEN] = { NAT_ZERO };
    tSNMP_OCTET_STRING_TYPE AclName;
    tSNMP_OCTET_STRING_TYPE NextAclName;

    MEMSET (au1AclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);
    MEMSET (au1NextAclName, NAT_ZERO, NAT_MAX_ACL_NAME_LEN);

    AclName.i4_Length = (INT4) STRLEN (au1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, au1AclName, AclName.i4_Length);
    if (SNMP_FAILURE ==
        nmhGetFirstIndexNatPolicyTable (&i4PolicyType, &i4PolicyId, &AclName))
    {
        return;
    }

    NextAclName.i4_Length = AclName.i4_Length;
    NextAclName.pu1_OctetList = au1NextAclName;
    MEMCPY (NextAclName.pu1_OctetList, AclName.pu1_OctetList,
            AclName.i4_Length);
    do
    {
        nmhGetNatPolicyTranslatedIp (i4PolicyType, i4PolicyId, &AclName,
                                     &u4TranslatedIpAddr);
        if (i4PolicyType == NAT_STATIC_POLICY)
        {
            pu1PolicyType = "static";
        }
        else
        {
            pu1PolicyType = "nat";
        }
        CliPrintf (CliHandle, "\r\n%s inside %d ", pu1PolicyType, i4PolicyId);
        CliPrintf (CliHandle, "access-list %s\r\n", AclName.pu1_OctetList);

        if (u4TranslatedIpAddr != NAT_ZERO)
        {
            CliPrintf (CliHandle, "\r\n%s outside %d ", pu1PolicyType,
                       i4PolicyId);
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddress, u4TranslatedIpAddr);
            CliPrintf (CliHandle, "%s\r\n", pu1IpAddress);
        }
        CliPrintf (CliHandle, "!\r\n");

        AclName.i4_Length = NextAclName.i4_Length;
        AclName.pu1_OctetList = NextAclName.pu1_OctetList;
        MEMCPY (AclName.pu1_OctetList, NextAclName.pu1_OctetList,
                NextAclName.i4_Length);
    }
    while (SNMP_SUCCESS == nmhGetNextIndexNatPolicyTable (i4PolicyType,
                                                          &i4PolicyType,
                                                          i4PolicyId,
                                                          &i4PolicyId,
                                                          &AclName,
                                                          &NextAclName));

    return;

}
#endif /* __NATCLI_C__ */
