
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: natuser.c,v 1.4 2015/08/08 12:35:27 siva Exp $
 *
 * Description: When the NAT module is compiled in Kernel mode,
 * a few functions are still handled on the user mode. This file
 * contains the intialization routines of those functions which are
 * handled in the user mode.
 *******************************************************************/

#include "natinc.h"
#include "fsnatwr.h"

/***************************************************************************
* Function Name    :  NatInit
* Description      :  This function registers the NAT Mib with the SNMP
*                     Module.
* Input (s)        :  None.
*
* Output (s)       :  None.
* Returns          :  SUCCESS - After the registration with SNMP Module.
***************************************************************************/
PUBLIC INT4
NatInit (INT1 *pDummy)
{
    UNUSED_PARAM (pDummy);
    RegisterFSNAT ();
    NAT_INIT_COMPLETE (OSIX_SUCCESS);
    return SUCCESS;
}

/***************************************************************************
* Function Name    :  NatUtilGetGlobalNatStatus
* Description      :  This function retirn the global NAT status
*                     
* Input (s)        :  None.
*
* Output (s)       :  None.
* Returns          :  NAT_ENABLE/NAT_DISABLE.
***************************************************************************/
INT4
NatUtilGetGlobalNatStatus ()
{
    INT4                i4NatStatus = NAT_DISABLE;

    nmhGetNatEnable (&i4NatStatus);
    return i4NatStatus;
}

/***************************************************************************
 * Function Name  :  NatCheckIfNatEnable                                   *
 * Description    :  This function checks whether NAT is enabled on the    *
 *                   given interface                                       *
 *                                                                         *
 * Input (s)      :  u4IfNum - Interface number to be checked for NAT      *
 *                   enable                                                *
 *                                                                         *
 * Output (s)     :  None                                                  *
 * Returns        :  NAT_ENABLE or NAT_DISABLE                             *
 *                                                                         *
 **************************************************************************/

PUBLIC UINT4
NatCheckIfNatEnable (UINT4 u4IfNum)
{
    INT4                i4RetValNatIfNat = NAT_DISABLE;

    nmhGetNatIfNat (u4IfNum, &i4RetValNatIfNat);
    return ((UINT4) i4RetValNatIfNat);
}

/***************************************************************************
* Function Name    :  NatCheckIfNaptEnable                                 *
* Description      :  This function checks whether NAPT is enabled on the  *
*                     given interface                                      *
*                                                                          *
* Input (s)        :  u4IfNum - Interface number to be checked for NAPT    *
*                     enable                                               *
*                                                                          *
* Output (s)       :  None                                                 *
* Returns          :  NAT_ENABLE or NAT_DISABLE                            *
*                                                                          *
****************************************************************************/

PUBLIC UINT4
NatCheckIfNaptEnable (UINT4 u4IfNum)
{
    INT4                i4RetValNatIfNapt = NAT_DISABLE;

    nmhGetNatIfNapt (u4IfNum, &i4RetValNatIfNapt);
    return ((UINT4) i4RetValNatIfNapt);
}

/***************************************************************************
* Function Name  :  NatCheckIfAddMcastEntry
* Description    :  This function checks whether a multicast entry has to be
*                   created or not
* *
*
* Input (s)    :  Pointer to HeaderInfo
*
* Output (s)    :  Pointer to HeaderInfo containing the full Translation info
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/
UINT4
NatCheckIfAddMcastEntry (UINT4 u4IfNum)
{
    UINT4               u4Iface;
    UINT4               u4IfStatus;

    u4IfStatus = NatCheckIfNatEnable (u4IfNum);
    if (u4IfStatus == NAT_ENABLE)
    {
        /* NAT is enabled on the interface, pkt is received
         * from outside network, Return SUCCESS so that
         * Entry can be created in the HW
         */
        return NAT_SUCCESS;
    }

    for (u4Iface = 0; u4Iface <= NAT_MAX_NUM_IF + 1; u4Iface++)
    {
        u4IfStatus = NatCheckIfNatEnable (u4Iface);
        if (u4IfStatus == NAT_ENABLE)
        {
            /* NAT is enabled on some other interface, pkt is
             * received from inside network, Right now return
             * FAILURE and set a global variable so that Mcast
             * data is forwarded in control plane itself
             */
            /* In future we can check if there are any listners
             * in the outside network, if YES we can return FAILURE
             * else we can return SUCCESS so that entry can be
             * created in HW
             */
            return NAT_FAILURE;
        }
    }

    return NAT_SUCCESS;
}

/**************************************************************************
 * Function Name :  NatGetWanInterfaceIpAddr                              *
 * Description   :  This function the is used to get the WAN Interface    *
 *                  IpAddress  according to IfIndex                       *
 * Input (s)     :  i4WanIf  - WAN Interface Index                        *
 * Output (s)    :  None                                                  *
 * Returns       :  i4WanIf - WAN Interface Index                         *
 **************************************************************************/
UINT4
NatGetWanInterfaceIpAddr (UINT4 u4WanIf)
{
    UINT4               u4WanIfIpAddr = 0;

    CfaGetIfIpAddr (u4WanIf, &u4WanIfIpAddr);
    return (u4WanIfIpAddr);
}

/*****************************************************************************/
/* Function Name      : NatLock                                              */
/*                                                                           */
/* Description        : This function is used to take the CFA  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
NatLock (VOID)
{
    CFA_LOCK();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : NatUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the CFA                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
NatUnLock (VOID)
{
    CFA_UNLOCK();
    return SNMP_SUCCESS;
}
