/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natllr.c,v 1.14 2014/02/14 13:57:30 siva Exp $
 *
 * Description:This file contains functions to support
 *             SNMP SET for object like natGlobalAddressEntrystatus
 *             in the Global Address Table
 *
 *******************************************************************/
#ifndef _NATLLR_C_
#define _NATLLR_C_

#include "natinc.h"

/***************************************************************************
* Function Name    :  NatGatActive                     *
* Description    :  This function sets the row status of the Global Address
*                   node to ACTIVE     
*                                      *
* Input (s)    :  Interface Number , Global IP Address         *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatGatActive (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr)
{
    tInterfaceInfo     *pIfNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;

    pTranslatedLocalIpAddrNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Active a  Row in the Global Address Table\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "TranslatedLocalIpAddress =%u\n",
              u4TranslatedLocalIpAddr);

    if (pIfNode != NULL)
    {
        /* The Interface exists */
        TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                      pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
        {
            if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocalIpAddr)
            {

                /* If row exists with ACTIVE status return success */

                if (pTranslatedLocalIpAddrNode->i4RowStatus ==
                    NAT_STATUS_ACTIVE)
                {
                    NAT_TRC (NAT_TRC_ON, "Row is Active\n");
                    if (gapNatIfTable[i4IfNum]->i4RowStatus
                        != NAT_STATUS_ACTIVE)
                    {
                        gapNatIfTable[i4IfNum]->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    return SNMP_SUCCESS;
                }

                /* If the status is NOT_IN_SERVICE, only then change it 
                 * to ACTIVE */

                if (pTranslatedLocalIpAddrNode->i4RowStatus ==
                    NAT_STATUS_NOT_IN_SERVICE)
                {
                    NAT_TRC (NAT_TRC_ON, "Row notinservice - Actived\n");
                    pTranslatedLocalIpAddrNode->i4RowStatus = NAT_STATUS_ACTIVE;

                    /* If Interface status is NOT active no need to initialise
                     * the fields of Interface.
                     */

                    if (gapNatIfTable[i4IfNum]->i4RowStatus
                        != NAT_STATUS_ACTIVE)
                    {
                        gapNatIfTable[i4IfNum]->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    else
                    {
                        if (gapNatIfTable[i4IfNum]->u4CurrTranslatedLocIpAddr
                            == NAT_ZERO)
                        {
                            /*Nat interface Entry should be initialised */
                            NatIfInitFields ((UINT4) i4IfNum);
                        }
                    }
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row status of Global Address\
                                  Table of index %d and %d is set to active\n", i4IfNum, u4TranslatedLocalIpAddr);
                    return SNMP_SUCCESS;
                }
                else
                    break;
            }
        }

    }

    /* row does not exist or exists with status NOTREADY, return failure */

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of  index %d and %d "
                  "does not exist\n", i4IfNum, u4TranslatedLocalIpAddr);
    NAT_TRC (NAT_TRC_ON, "Row notready - failure in activation\n");
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatGatCreateAndGo                    *
* Description    :  This function creates the row if it does not exist   *
*             already                        *
* Input (s)    :  Interface Number , Global IP Address         *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatGatCreateAndGo (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr)
{

    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;
    UINT4               u4InterfaceIpAddr = NAT_ZERO;

    pTranslatedLocalIpAddrNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Global Address Table and go\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "TranslatedLocalIpAddress =%u\n",
              u4TranslatedLocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                      pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
        {
            /* If row exists, can not create .Return FAILURE */

            if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocalIpAddr)
            {
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Global Address Table of\
                              index %d and %d already    exist\n", i4IfNum, u4TranslatedLocalIpAddr);
                return SNMP_FAILURE;
            }

        }
    }

    if (pIfNode == NULL)
    {
        /* Interface entry does not exist */
        /* Allocate memory in NatIfTable to add the Global Address List. */

        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        /* Initialise Nat Interface Table */
        NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
    }

    /* 
     * Create the row with status NOT_READY . Because, mask need to be set
     * before making the status ACTIVE
     */

    if (NAT_MEM_ALLOCATE
        (NAT_GLOBAL_ADDR_POOL_ID,
         pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr = u4TranslatedLocalIpAddr;
    pTranslatedLocalIpAddrNode->u4Mask = NAT_INIT_MASK;
    pTranslatedLocalIpAddrNode->i4RowStatus = NAT_STATUS_NOT_READY;

    /* Adding the node as first when the adress to be added is a 
     * interface address
     */
    if (CfaGetIfIpAddr (i4IfNum, &u4InterfaceIpAddr) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u4TranslatedLocalIpAddr == u4InterfaceIpAddr)
    {
        TMO_SLL_Insert (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                        NULL,
                        &(pTranslatedLocalIpAddrNode->TranslatedLocIpAddrNode));
    }
    else
    {
        TMO_SLL_Add (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                     &(pTranslatedLocalIpAddrNode->TranslatedLocIpAddrNode));
    }

    NAT_TRC (NAT_TRC_ON, "Row Created Successfully \n");
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of index %d and %d "
                  "is created \n", i4IfNum, u4TranslatedLocalIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatGatCreateAndWait                  *
* Description    :  This function creates a row in the table if it does  *
*                   not exist already and sets the status to NOT_READY   *
*                                      *
* Input (s)    :  Interface Number, Global IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on successful creation of the row    *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatGatCreateAndWait (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr)
{

    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;
    UINT4               u4InterfaceIpAddr = NAT_ZERO;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON,
             "Create a  Row in the Global Address Table and wait\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "TranslatedLocalIpAddress =%u\n",
              u4TranslatedLocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                      pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
        {
            /* If row exists, can not create .Return FAILURE */

            if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocalIpAddr)
            {
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Global Address Table of\
                              index %d and %d already exist\n", i4IfNum, u4TranslatedLocalIpAddr);
                return SNMP_FAILURE;
            }
        }
    }

    if (pIfNode == NULL)
    {
        /* Interface entry does not exist */
        /* Create NatIfTable entry for that interface */
        /* Allocate memory in NatIfTable to add the Global Address List. */

        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }

        NATIFTABLE_INITIALIZE (gapNatIfTable[i4IfNum], NAT_STATUS_INVLD);
    }

    /* 
     * Create the row with status NOT_READY . Because, mask need to be set
     * before making the status ACTIVE
     */

    if (NAT_MEM_ALLOCATE
        (NAT_GLOBAL_ADDR_POOL_ID,
         pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr = u4TranslatedLocalIpAddr;
    pTranslatedLocalIpAddrNode->u4Mask = NAT_INIT_MASK;
    pTranslatedLocalIpAddrNode->i4RowStatus = NAT_STATUS_NOT_READY;

    /* Adding the node as first when the adress to be added is a 
     * interface address
     */
    if (CfaGetIfIpAddr (i4IfNum, &u4InterfaceIpAddr) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u4TranslatedLocalIpAddr == u4InterfaceIpAddr)
    {
        TMO_SLL_Insert (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                        NULL,
                        &(pTranslatedLocalIpAddrNode->TranslatedLocIpAddrNode));
    }
    else
    {
        TMO_SLL_Add (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                     &(pTranslatedLocalIpAddrNode->TranslatedLocIpAddrNode));
    }

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address    Table of\
                  index %d and %d is created \n", i4IfNum, u4TranslatedLocalIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatGatNotInService                   *
* Description    :  This funcions sets the status of an active row to    *
*                   NOT IN SERVICE                    *
*                                      *
* Input (s)    :  Interface Number, Global IP Address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on setting the status to NOTINSERVICE  *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatGatNotInService (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr)
{

    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON,
             "Change the status of a Row in the Global Address Table "
             "to not-in-service\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "TranslatedLocalIpAddress =%u\n",
              u4TranslatedLocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                      pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
        {
            if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocalIpAddr)
            {

                /* If the row exists with status ACTIVE, change it
                 * to NOT_IN_SERVICE
                 */

                if (pTranslatedLocalIpAddrNode->i4RowStatus ==
                    NAT_STATUS_ACTIVE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Entry Active - Changed to notinservice\n");
                    pTranslatedLocalIpAddrNode->i4RowStatus =
                        NAT_STATUS_NOT_IN_SERVICE;
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row status of Global Address Table of "
                                  "index %d and %d is set to NOT_IN_SERVICE \n",
                                  i4IfNum, u4TranslatedLocalIpAddr);
                    return SNMP_SUCCESS;
                }

                /* If the row exists with status NOT_IN_SERVICE,\
                 * return success */

                if (pTranslatedLocalIpAddrNode->i4RowStatus ==
                    NAT_STATUS_NOT_IN_SERVICE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Entry notinservice - return Success\n");
                    return SNMP_SUCCESS;
                }
                return SNMP_FAILURE;
            }

        }
    }

    /* If row doesn't exist, return failure */

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of\
                  index %d and %d does not exist\n", i4IfNum, u4TranslatedLocalIpAddr);
    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name    :  NatGatDestroy                    *
* Description    :  This function removes the row from the global    *
*             address table                    *
*                                      *
* Input (s)    :  Interface number, Global IP address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on successful removal of the entry,if   *
*             it exists.                       *
****************************************************************************/

PUBLIC INT1
NatGatDestroy (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr)
{

    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrPrevNode = NULL;
    tNatFreeGipListNode *pNatFreeGipListNode = NULL;
    tNatFreeGipListNode *pNatFreeGipListPrevNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;
    UINT4               u4RangeMask = NAT_ZERO;
#ifdef UPNP_WANTED
    CHR1               *pu1value = NULL;
#endif

    pTranslatedLocalIpAddrNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Delete a row in the Global Address Table \n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "TranslatedLocalIpAddress =%x\n",
              u4TranslatedLocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&(pIfNode->TranslatedLocIpList),
                      pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
        {
            /* If row exists, delete the row */

            if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                u4TranslatedLocalIpAddr)
            {
                u4RangeMask = pTranslatedLocalIpAddrNode->u4Mask;
                TMO_SLL_Delete (&(pIfNode->TranslatedLocIpList),
                                &(pTranslatedLocalIpAddrNode->
                                  TranslatedLocIpAddrNode));
                NatMemReleaseMemBlock (NAT_GLOBAL_ADDR_POOL_ID,
                                       (UINT1 *) pTranslatedLocalIpAddrNode);

                /*
                 * If there is/are reference(s) to any of the IP addresses deleted
                 * above then these references should be deleted. 
                 */
                TMO_SLL_Scan (&pIfNode->NatFreeGipList, pNatFreeGipListNode,
                              tNatFreeGipListNode *)
                {

                    /* 
                     * If the deleted Global Ip address is present in the FreeGip List
                     * then the address should be deleted from the FreeGip List
                     */
                    if ((pNatFreeGipListNode->u4TranslatedLocIpAddr >=
                         u4TranslatedLocalIpAddr) &&
                        ((pNatFreeGipListNode->u4TranslatedLocIpAddr &
                          u4RangeMask) ==
                         (u4TranslatedLocalIpAddr & u4RangeMask)))
                    {
                        TMO_SLL_Delete (&pIfNode->NatFreeGipList,
                                        &(pNatFreeGipListNode->
                                          NatFreeGipListNode));
                        NatMemReleaseMemBlock (NAT_FREE_GLOBAL_LIST_POOL_ID,
                                               (UINT1 *) pNatFreeGipListNode);
                        pNatFreeGipListNode = pNatFreeGipListPrevNode;
                    }
                    pNatFreeGipListPrevNode = pNatFreeGipListNode;
                }
                /*
                 *Check is made whether the current IpAddress indicator lies in the
                 * range given in the Global Ip Address node being destroyed. If
                 * present then the values are replaced with the starting value 
                 * of next Global IpAddress Node is given. If no next Global IP node is
                 * present in the List then the indicators are initialised to zero.
                 * This is also done for the Dns Gloabl Ip indicator.
                 */
                if ((pIfNode->u4CurrTranslatedLocIpAddr >=
                     u4TranslatedLocalIpAddr)
                    && (((pIfNode->u4CurrTranslatedLocIpAddr & u4RangeMask) ==
                         (u4TranslatedLocalIpAddr & u4RangeMask)) ||
                        (pIfNode->u4CurrTranslatedLocIpAddr ==
                         u4TranslatedLocalIpAddr + NAT_ONE)))
                {
                    if (((pTranslatedLocalIpAddrNode =
                          (tTranslatedLocIpAddrNode *)
                          TMO_SLL_First (&(pIfNode->TranslatedLocIpList))) !=
                         NULL)
                        && (pTranslatedLocalIpAddrNode->i4RowStatus ==
                            NAT_STATUS_ACTIVE))
                    {
                        NatGetSubnetRangeIpAddress (pTranslatedLocalIpAddrNode->
                                                    u4TranslatedLocIpAddr,
                                                    pTranslatedLocalIpAddrNode->
                                                    u4Mask,
                                                    &(pIfNode->
                                                      u4CurrTranslatedLocIpAddr),
                                                    &(pIfNode->
                                                      u4TranslatedLocIpRangeEnd));
                    }
                    else
                    {
                        pIfNode->u4CurrTranslatedLocIpAddr = NAT_ZERO;
#ifdef UPNP_WANTED
                        CLI_CONVERT_IPADDR_TO_STR (pu1value,
                                                   (pIfNode->
                                                    u4CurrTranslatedLocIpAddr));
                        SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_EXT_IPADD,
                                                    pu1value);
                        SEND_NOTIFY_TO_CONTROL_POINTS ();
                        NAT_TRC1 (NAT_TRC_ON, "ExternalIPAddress :%lu\n",
                                  (pIfNode->u4CurrTranslatedLocIpAddr));
#endif /* UPNP_WANTED */
                        pIfNode->u4TranslatedLocIpRangeEnd = NAT_ZERO;
                    }
                }
                if ((pIfNode->u4CurrDnsTranslatedLocIpInUse >=
                     u4TranslatedLocalIpAddr)
                    && ((pIfNode->u4CurrDnsTranslatedLocIpInUse & u4RangeMask)
                        == (u4TranslatedLocalIpAddr & u4RangeMask)))
                {
/*                    if ((pTranslatedLocalIpAddrNode != NULL)
                        && (pTranslatedLocalIpAddrNode->i4RowStatus ==
                            NAT_STATUS_ACTIVE))
                    {
                        pIfNode->u4CurrDnsTranslatedLocIpInUse =
                            pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr;
                        pIfNode->u4CurrDnsTranslatedLocIpRangeEnd =
                            pIfNode->u4TranslatedLocIpRangeEnd;
                    }
                    else
                    {*/
                    pIfNode->u4CurrDnsTranslatedLocIpInUse = NAT_ZERO;
                    pIfNode->u4CurrDnsTranslatedLocIpRangeEnd = NAT_ZERO;
                    /*} */
                }
                NatDeleteDynamicEntryWithIPAndMask
                    ((UINT4) i4IfNum, u4TranslatedLocalIpAddr, u4RangeMask);

                pTranslatedLocalIpAddrNode = pTranslatedLocalIpAddrPrevNode;
            }
            pTranslatedLocalIpAddrPrevNode = pTranslatedLocalIpAddrNode;
        }                        /*TMO_SLL_SCAN for the TranslatedLocIpList */
    }                            /* if(pIfNode != NULL ) */

    /* If the row doesn't exist, return success */

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Global Address Table of\
                  index %d and %x is destroyed\n", i4IfNum, u4TranslatedLocalIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatLatActive                     *
* Description    :  This function sets the row status of Local IP node 
*                   to ACTIVE      *
*                                      *
* Input (s)    :  Interface Number , Local IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatLatActive (INT4 i4IfNum, UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;

    NAT_TRC (NAT_TRC_ON, "Active a  Row in the Local Address Table\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocIpAddr);
    /*
     * Note- Local IP list is not specific to any interface */
    TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
    {
        if ((pLocIpAddrNode->u4InterfaceNumber == (UINT4) i4IfNum) &&
            (pLocIpAddrNode->u4LocIpNum == u4LocIpAddr))
        {
            /* Row exists already with status NAT_STATUS_ACTIVE */
            if (pLocIpAddrNode->i4RowStatus == NAT_STATUS_ACTIVE)
            {
                NAT_TRC (NAT_TRC_ON, "Row is Active\n");
                return SNMP_SUCCESS;
            }
            else
            {
                /* If the row not_in_service, change the status to
                 * ACTIVE and return success
                 */
                if (pLocIpAddrNode->i4RowStatus == NAT_STATUS_NOT_IN_SERVICE)
                {
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row status of Local Address Table of  "
                                  "index %d and %  is set to active\n",
                                  i4IfNum, u4LocIpAddr);
                    NAT_TRC (NAT_TRC_ON, "Row notinservice - Actived\n");
                    pLocIpAddrNode->i4RowStatus = NAT_STATUS_ACTIVE;
                    return SNMP_SUCCESS;
                }
                else
                    return SNMP_FAILURE;
            }
        }
    }

    /* Row does not exist, return failure */
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of index %d and %d "
                  "does not exist\n", i4IfNum, u4LocIpAddr);
    NAT_TRC (NAT_TRC_ON, "Row notready/not exist - failure in activation\n");
    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name    :  NatLatCreateAndGo                    *
* Description    :  This function creates the row if it does not exist   *
*             already                        *
* Input (s)    :  Interface Number , Local IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatLatCreateAndGo (INT4 i4IfNum, UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Local Address Table and go\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocIpAddr);

    TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
    {
        if ((pLocIpAddrNode->u4InterfaceNumber == (UINT4) i4IfNum) &&
            (pLocIpAddrNode->u4LocIpNum == u4LocIpAddr))
        {
            MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Local Address Table of index %d and %d "
                          "already exist\n", i4IfNum, u4LocIpAddr);
            NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
            return SNMP_FAILURE;
        }
    }

    /*Add the row if it doesn't already exist */

    if (NAT_MEM_ALLOCATE
        (NAT_LOCAL_ADDR_POOL_ID, pLocIpAddrNode, tLocIpAddrNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pLocIpAddrNode->u4InterfaceNumber = (UINT4) i4IfNum;
    pLocIpAddrNode->u4LocIpNum = u4LocIpAddr;
    pLocIpAddrNode->i4RowStatus = NAT_STATUS_NOT_READY;
    TMO_SLL_Add (&gNatLocIpAddrList, &(pLocIpAddrNode->LocIpAddrNode));
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of\
                  index %d and %d is created \n", i4IfNum, u4LocIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatLatCreateAndWait                  *
* Description    :  This function creates a row in the table if it does  *
*             not exist already and sets the status to NOT_READY   *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on successful creation of the row    *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatLatCreateAndWait (INT4 i4IfNum, UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Local Address Table and wait\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocIpAddr);

    /*Search in the local IP addr List */

    TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
    {
        if ((pLocIpAddrNode->u4InterfaceNumber == (UINT4) i4IfNum) &&
            (pLocIpAddrNode->u4LocIpNum == u4LocIpAddr))
        {

            /* Row exists with NAT_STATUS_ACTIVE status */

            MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Local Address Table of index %d and %d "
                          "already exist\n", i4IfNum, u4LocIpAddr);
            NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
            return SNMP_FAILURE;
        }
    }

    /*Create the row with status NOTREADY */

    if (NAT_MEM_ALLOCATE
        (NAT_LOCAL_ADDR_POOL_ID, pLocIpAddrNode, tLocIpAddrNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pLocIpAddrNode->u4InterfaceNumber = (UINT4) i4IfNum;
    pLocIpAddrNode->u4LocIpNum = u4LocIpAddr;
    pLocIpAddrNode->i4RowStatus = NAT_STATUS_NOT_READY;
    TMO_SLL_Add (&gNatLocIpAddrList, &(pLocIpAddrNode->LocIpAddrNode));
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of\
                  index %d and %d is created \n", i4IfNum, u4LocIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatLatNotInService                   *
* Description    :  This funcions sets the status of an active row to    *
*             NOT IN SERVICE                    *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on setting the status to NOTINSERVICE  *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatLatNotInService (INT4 i4IfNum, UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;
    NAT_TRC (NAT_TRC_ON,
             "Change the status of a Row in the Local Address Table "
             "to not-in-service\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocIpAddr);

    TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
    {
        if ((pLocIpAddrNode->u4InterfaceNumber == (UINT4) i4IfNum) &&
            (pLocIpAddrNode->u4LocIpNum == u4LocIpAddr))
        {

            /* If row exists with status not-in-service, return success */

            if (pLocIpAddrNode->i4RowStatus == NAT_STATUS_NOT_IN_SERVICE)
            {
                NAT_TRC (NAT_TRC_ON, "Entry notinservice - return Success\n");
                return SNMP_SUCCESS;
            }

            /* If row exists with status ACTIVE, change it to
             * not_in_service
             */

            if (pLocIpAddrNode->i4RowStatus == NAT_STATUS_ACTIVE)
            {
                NAT_TRC (NAT_TRC_ON,
                         "Entry Active - Changed to notinservice\n");
                pLocIpAddrNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT", "The row status of\
                              Local Address    Table of\
                              index %d and %d is set to\
                              NOT_IN_SERVICE \n", i4IfNum, u4LocIpAddr);
                return SNMP_SUCCESS;
            }
            else
                break;
        }

    }

    /*Row doesnt exist, or exists with status not_ready, return failure */

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of\
                  index %d and %d does not exist\n", i4IfNum, u4LocIpAddr);
    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name    :  NatLatDestroy                    *
* Description    :  This function removes the row from the Local    *
*             address table                    *
*                                      *
* Input (s)    :  Interface number, Local  IP address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on successful removal of the entry,if   *
*             it exists.                       *
****************************************************************************/

PUBLIC INT1
NatLatDestroy (INT4 i4IfNum, UINT4 u4LocIpAddr)
{
    tLocIpAddrNode     *pLocIpAddrNode = NULL;
    tLocIpAddrNode     *pLocIpAddrPrevNode = NULL;
    INT4                i4Index = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "Delete a row in the Local Address Table \n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocIpAddr);

    TMO_SLL_Scan (&gNatLocIpAddrList, pLocIpAddrNode, tLocIpAddrNode *)
    {

        /* If row exists, delete it from the list */

        if ((pLocIpAddrNode->u4InterfaceNumber == (UINT4) i4IfNum) &&
            (pLocIpAddrNode->u4LocIpNum == u4LocIpAddr))
        {
            TMO_SLL_Delete (&gNatLocIpAddrList,
                            &(pLocIpAddrNode->LocIpAddrNode));
            NatMemReleaseMemBlock (NAT_LOCAL_ADDR_POOL_ID,
                                   (UINT1 *) pLocIpAddrNode);
            pLocIpAddrNode = pLocIpAddrPrevNode;
        }
        pLocIpAddrPrevNode = pLocIpAddrNode;
    }

    if (TMO_SLL_First (&gNatLocIpAddrList) == NULL)
    {
        for (i4Index = NAT_ZERO; i4Index <= NAT_MAX_NUM_IF; i4Index++)
        {
            if (gapNatIfTable[i4Index] != NULL)
            {
                gapNatIfTable[i4Index]->u1NatEnable = NAT_DISABLE;
                gapNatIfTable[i4Index]->i4RowStatus = NAT_STATUS_NOT_READY;
            }
        }
    }

    /* if row doesnt exist, return success */

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Local Address Table of  index %d and %d "
                  "is destroyed\n", i4IfNum, u4LocIpAddr);
    NAT_TRC (NAT_TRC_ON, "Deleted the Row from Local Addr Table\n");
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatSttActive                     *
* Description    :  This function sets the row status of Static entry
*                   to ACTIVE      *
*                                      *
* Input (s)    :  Interface Number , Local    IP Address         *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatSttActive (INT4 i4IfNum, UINT4 u4LocalIpAddr)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Active a  Row in the Static Table\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                      tStaticEntryNode *)
        {
            if (pStaticEntryNode->u4LocIpAddr == u4LocalIpAddr)
            {
                if (pStaticEntryNode->i4RowStatus == NAT_STATUS_NOT_IN_SERVICE)
                {
                    NAT_TRC (NAT_TRC_ON, "Row notinservice - Actived\n");
                    pStaticEntryNode->i4RowStatus = NAT_STATUS_ACTIVE;
                    if (gapNatIfTable[i4IfNum]->i4RowStatus
                        != NAT_STATUS_ACTIVE)
                    {
                        gapNatIfTable[i4IfNum]->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row status of Static Address Table of "
                                  "index %d and %d is set to active\n",
                                  i4IfNum, u4LocalIpAddr);
                    return SNMP_SUCCESS;
                }
                if (pStaticEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    NAT_TRC (NAT_TRC_ON, "Row is Active\n");
                    return SNMP_SUCCESS;
                }
                else
                    break;
            }
        }
    }

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of\
                  index %d and %d does not exist\n", i4IfNum, u4LocalIpAddr);
    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name    :  NatSttCreateAndGo                    *
* Description    :  This function creates the row if it does not exist   *
*             already                        *
* Input (s)    :  Interface Number , Local IP Address          *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatSttCreateAndGo (INT4 i4IfNum, UINT4 u4LocalIpAddr)
{
    tStaticEntryNode   *pStaticEntryNode = NULL;
    tStaticEntryNode   *pStaticEntryTemp = NULL;
    tStaticEntryNode   *pStaticEntryPrev = NULL;
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Static Table and go\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                      tStaticEntryNode *)
        {
            if (pStaticEntryNode->u4LocIpAddr == u4LocalIpAddr)
            {
                NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Static Address Table of index %d "
                              "and %d already exist\n", i4IfNum, u4LocalIpAddr);
                return SNMP_FAILURE;
            }
        }
    }

    if (pIfNode == NULL)
    {
        /* Create the Interface */
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        pIfNode = gapNatIfTable[i4IfNum];
        NATIFTABLE_INITIALIZE (pIfNode, NAT_STATUS_NOT_READY);

    }

    if (NAT_MEM_ALLOCATE
        (NAT_STATIC_POOL_ID, pStaticEntryNode, tStaticEntryNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pStaticEntryNode->u4LocIpAddr = u4LocalIpAddr;
    pStaticEntryNode->i4RowStatus = NAT_STATUS_NOT_READY;

    /* Find the position to which the new node is to be inserted */
    TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryTemp, tStaticEntryNode *)
    {
        if (pStaticEntryTemp->u4LocIpAddr > u4LocalIpAddr)
        {
            break;
        }
        pStaticEntryPrev = pStaticEntryTemp;
    }

    TMO_SLL_Insert (&pIfNode->StaticList, &(pStaticEntryPrev->StaticTable),
                    &(pStaticEntryNode->StaticTable));

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of  index %d and %d is "
                  "created \n", i4IfNum, u4LocalIpAddr);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatSttCreateAndWait                  *
* Description    :  This function creates a row in the table if it does  *
*             not exist already and sets the status to NOT_READY   *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on successful creation of the row    *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/
PUBLIC INT1
NatSttCreateAndWait (INT4 i4IfNum, UINT4 u4LocalIpAddr)
{
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Static Table and wait\n");
    return (NatSttCreateAndGo (i4IfNum, u4LocalIpAddr));
}

/***************************************************************************
* Function Name    :  NatSttNotInService                   *
* Description    :  This funcions sets the status of an active row to    *
*                   NOT IN SERVICE                    *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on setting the status to NOTINSERVICE  *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/
PUBLIC INT1
NatSttNotInService (INT4 i4IfNum, UINT4 u4LocalIpAddr)
{

    tStaticEntryNode   *pStaticEntryNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON,
             "Change the status of a Row in Static Table to not-in-service\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                      tStaticEntryNode *)
        {
            if (pStaticEntryNode->u4LocIpAddr == u4LocalIpAddr)
            {
                if (pStaticEntryNode->i4RowStatus == NAT_STATUS_NOT_IN_SERVICE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Entry notinservice - return Success\n");
                    return SNMP_SUCCESS;
                }
                if (pStaticEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    /* Remove all the dynamic entries associated with the given 
                     * static translation entry */
                    NatDeleteSpecifiedEntry
                        ((UINT4) i4IfNum, u4LocalIpAddr,
                         pStaticEntryNode->u4TranslatedLocIpAddr,
                         NAT_ZERO, NAT_ZERO);

                    NAT_TRC (NAT_TRC_ON,
                             "Entry Active - Changed to notinservice\n");
                    pStaticEntryNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
                    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row of Static Address Table of\
                                  index %d and %d is\
                                  created \n", i4IfNum, u4LocalIpAddr);
                    return SNMP_SUCCESS;
                }
                else
                    break;
            }

        }
    }

    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of index %d "
                  "and %d does not exist\n", i4IfNum, u4LocalIpAddr);
    NAT_TRC (NAT_TRC_ON, "Row doesn't exist - Failure setting notinservice\n");
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatSttDestroy                    *
* Description    :  This function removes the row from the Static    *
*             address table                    *
*                                      *
* Input (s)    :  Interface number, Local  IP address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on successful removal of the entry,if   *
*             it exists.                       *
****************************************************************************/

PUBLIC INT1
NatSttDestroy (INT4 i4IfNum, UINT4 u4LocalIpAddr)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticEntryNode   *pStaticEntryNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Delete a row in the Static Table \n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticList, pStaticEntryNode,
                      tStaticEntryNode *)
        {
            if (pStaticEntryNode->u4LocIpAddr == u4LocalIpAddr)
            {
                /* Remove all the dynamic entries associated with the given 
                 * static translation entry */
                NatDeleteSpecifiedEntry
                    ((UINT4) i4IfNum, u4LocalIpAddr,
                     pStaticEntryNode->u4TranslatedLocIpAddr, NAT_ZERO,
                     NAT_ZERO);

                TMO_SLL_Delete (&pIfNode->StaticList,
                                &(pStaticEntryNode->StaticTable));
                NatMemReleaseMemBlock (NAT_STATIC_POOL_ID,
                                       (UINT1 *) pStaticEntryNode);

                NAT_TRC (NAT_TRC_ON, "Deleted the Row from Static Table\n");
                MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Static Address Table of index %d "
                              "and %d is destroyed\n", i4IfNum, u4LocalIpAddr);
                return SNMP_SUCCESS;
            }
        }
    }

    NAT_TRC (NAT_TRC_ON, "Failed to delete the Row from Static Table\n");
    MOD_TRC_ARG2 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of index %d "
                  "and %d not found \n", i4IfNum, u4LocalIpAddr);
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatSttNaptActive                     *
* Description    :  This function sets the row status of Static entry
*                   to ACTIVE      *
*                                      *
* Input (s)    :  Interface Number , Local    IP Address         *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatSttNaptActive (INT4 i4IfNum, UINT4 u4LocalIpAddr, UINT2 u2StartLocalPort,
                  UINT2 u2EndLocalPort, UINT2 u2ProtocolNumber)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tDynamicEntry      *pDynamicEntry = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Active a  Row in the Static Table\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);
    NAT_DBG1 (NAT_DBG_LLR, "StartLocalPort =%u\n", u2StartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "EndLocalPort =%u\n", u2EndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "EndLocalPort =%u\n", u2EndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "Protocol Number =%u\n", u2ProtocolNumber);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntryNode->u4LocIpAddr == u4LocalIpAddr) &&
                (pStaticNaptEntryNode->u2StartLocPort == u2StartLocalPort) &&
                (pStaticNaptEntryNode->u2EndLocPort == u2EndLocalPort) &&
                ((pStaticNaptEntryNode->u2ProtocolNumber == u2ProtocolNumber) ||
                 (pStaticNaptEntryNode->u2ProtocolNumber == NAT_PROTO_ANY)))
            {
                /* Searching the dynamic list to get the translated port
                 * which is getting used by already created dynamic entry,
                 * since this this neccessary to delete dynamic entry and 
                 * to do corresponding clean up with the help of the Api 
                 * "NatDeleteSpecifiedEntry" */
                TMO_SLL_Scan (&gNatDynamicList, pDynamicEntry, tDynamicEntry *)
                {
                    if ((pDynamicEntry->u4LocIpAddr == u4LocalIpAddr) &&
                        (pDynamicEntry->u2LocPort == u2StartLocalPort) &&
                        (pDynamicEntry->u4TranslatedLocIpAddr ==
                         pStaticNaptEntryNode->u4TranslatedLocIpAddr) &&
                        (pDynamicEntry->u4IfNum == (UINT4) i4IfNum))
                    {
                        if (u2ProtocolNumber != NAT_PROTO_ANY)
                        {
                            if (pDynamicEntry->u1PktType !=
                                (UINT1) u2ProtocolNumber)
                            {
                                continue;
                            }
                        }

                        NatDeleteSpecifiedEntry ((UINT4) i4IfNum, u4LocalIpAddr,
                                                 pDynamicEntry->
                                                 u4TranslatedLocIpAddr,
                                                 u2StartLocalPort,
                                                 pDynamicEntry->
                                                 u2TranslatedLocPort);
                        break;
                    }
                }

                if ((pStaticNaptEntryNode->i4RowStatus ==
                     NAT_STATUS_NOT_IN_SERVICE) ||
                    (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_NOT_READY))
                {
                    NAT_TRC (NAT_TRC_ON, "Row notinservice - Actived\n");
                    pStaticNaptEntryNode->i4RowStatus = NAT_STATUS_ACTIVE;
                    if (gapNatIfTable[i4IfNum]->i4RowStatus
                        != NAT_STATUS_ACTIVE)
                    {
                        gapNatIfTable[i4IfNum]->i4RowStatus =
                            NAT_STATUS_NOT_IN_SERVICE;
                    }
                    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row status of Static Napt Table of "
                                  "index %d,%u %u %u and %u is set to active\n",
                                  i4IfNum, u4LocalIpAddr, u2StartLocalPort,
                                  u2EndLocalPort, u2ProtocolNumber);

                    /* UPNP NAPT TIMER */
                    pStaticNaptEntryNode->NaptTimerNode.u1TimerId
                        = NAPT_TIMER_ID;
                    pStaticNaptEntryNode->NaptTimerNode.pNaptBackPtr
                        = pStaticNaptEntryNode;
                    /* Keeping If Info in Napt Timer Node */
                    pStaticNaptEntryNode->NaptTimerNode.i4IfIndex = i4IfNum;

                    if ((pStaticNaptEntryNode->NaptTimerNode.TimerNode.u4Data)
                        != NAT_ZERO)
                    {
                        if (NaptReStartTimer
                            (&(pStaticNaptEntryNode->NaptTimerNode),
                             pStaticNaptEntryNode->NaptTimerNode.TimerNode.
                             u4Data) == OSIX_FAILURE)
                        {
                            NAT_TRC (NAT_TRC_ON, "Failed to Start Timer \n");
                            return (SNMP_FAILURE);
                        }

                    }
                    /* END */
                    return SNMP_SUCCESS;

                }
                if (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {

                    NAT_TRC (NAT_TRC_ON, "Row is Active\n");
                    return SNMP_SUCCESS;
                }
                else
                {
                    break;
                }
            }
        }
    }

    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of  index %d,"
                  "%u %u %u and %u does not exist\n",
                  i4IfNum, u4LocalIpAddr, u2StartLocalPort, u2EndLocalPort,
                  u2ProtocolNumber);

    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name    :  NatSttNaptCreateAndGo                    *
* Description    :  This function creates the row if it does not exist   *
*             already                        *
* Input (s)    :  Interface Number , Local IP Address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/
PUBLIC INT1
NatSttNaptCreateAndGo (INT4 i4IfNum, UINT4 u4LocalIpAddr,
                       UINT2 u2StartLocalPort, UINT2 u2EndLocalPort,
                       UINT2 u2ProtocolNumber)
{
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryTemp = NULL;
    tStaticNaptEntry   *pStaticNaptEntryPrev = NULL;
    tInterfaceInfo     *pIfNode = NULL;
#ifdef UPNP_WANTED
    CHR1                au1Value[NAT_SIZE_OF_PM_PORT_VAL];
#endif
    pStaticNaptEntryNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Static Table and go\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);
    NAT_DBG1 (NAT_DBG_LLR, "StartLocalPort =%u\n", u2StartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "EndLocalPort =%u\n", u2EndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "Protocol Number =%u\n", u2ProtocolNumber);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntryNode->u4LocIpAddr == u4LocalIpAddr) &&
                (pStaticNaptEntryNode->u2StartLocPort == u2StartLocalPort) &&
                (pStaticNaptEntryNode->u2EndLocPort == u2EndLocalPort) &&
                ((pStaticNaptEntryNode->u2ProtocolNumber == u2ProtocolNumber) ||
                 (pStaticNaptEntryNode->u2ProtocolNumber == NAT_PROTO_ANY) ||
                 (NAT_PROTO_ANY == u2ProtocolNumber)))
            {
                NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Static Napt Table of index %d "
                              "%d %u %u and %u already exist\n", i4IfNum,
                              u4LocalIpAddr, u2StartLocalPort, u2EndLocalPort,
                              u2ProtocolNumber);
                return SNMP_FAILURE;
            }
        }
    }

    if (pIfNode == NULL)
    {
        /* Create the Interface */
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return SNMP_FAILURE;
        }
        pIfNode = gapNatIfTable[i4IfNum];
        NATIFTABLE_INITIALIZE (pIfNode, NAT_STATUS_NOT_READY);

    }

    if (NAT_MEM_ALLOCATE
        (NAT_STATIC_NAPT_POOL_ID, pStaticNaptEntryNode, tStaticNaptEntry) ==
        NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }
    MEMSET (pStaticNaptEntryNode, NAT_ZERO, sizeof (tStaticNaptEntry));
    pStaticNaptEntryNode->u4LocIpAddr = u4LocalIpAddr;
    pStaticNaptEntryNode->u2StartLocPort = u2StartLocalPort;
    pStaticNaptEntryNode->u2EndLocPort = u2EndLocalPort;
    pStaticNaptEntryNode->u4TranslatedLocIpAddr = NAT_ZERO;
    pStaticNaptEntryNode->u2TranslatedLocPort = NAT_ZERO;
    pStaticNaptEntryNode->i4RowStatus = NAT_STATUS_NOT_READY;
    pStaticNaptEntryNode->u2ProtocolNumber = u2ProtocolNumber;
    /* UPNP NAPT TIMER */
    pStaticNaptEntryNode->NaptTimerNode.TimerNode.u4Data = NAT_ZERO;
    /* END */
    MEMSET (pStaticNaptEntryNode->i1Description, NAT_ZERO,
            NAT_NAPT_ENTRY_DESCRN_LEN);
    pStaticNaptEntryNode->u1AppCallStatus = NAT_ZERO;
    /* Add the node to the list in the lexicographical order of the indices */
    TMO_SLL_Scan (&gapNatIfTable[i4IfNum]->StaticNaptList,
                  pStaticNaptEntryTemp, tStaticNaptEntry *)
    {
        if (pStaticNaptEntryTemp->u4LocIpAddr > u4LocalIpAddr)
        {
            break;
        }
        if (pStaticNaptEntryTemp->u4LocIpAddr == u4LocalIpAddr)
        {
            if (pStaticNaptEntryTemp->u2StartLocPort > u2StartLocalPort)
            {
                break;
            }
            if (pStaticNaptEntryTemp->u2StartLocPort == u2StartLocalPort)
            {
                if (pStaticNaptEntryTemp->u2EndLocPort > u2EndLocalPort)
                {
                    break;
                }
                if (pStaticNaptEntryTemp->u2EndLocPort == u2EndLocalPort)
                {
                    if (pStaticNaptEntryTemp->u2ProtocolNumber >
                        u2ProtocolNumber)
                    {
                        break;
                    }
                }
            }
        }
        pStaticNaptEntryPrev = pStaticNaptEntryTemp;
    }

    TMO_SLL_Insert (&gapNatIfTable[i4IfNum]->StaticNaptList,
                    &(pStaticNaptEntryPrev->StaticNaptTable),
                    &(pStaticNaptEntryNode->StaticNaptTable));

#ifdef UPNP_WANTED
    SPRINTF (au1Value, "%d", (unsigned int)
             ((pIfNode->StaticNaptList).u4_Count));
    SET_UPNP_NAT_STATE_VAR_VAL (NAT_VARIABLE_TYPE_PORT_MAP_ENT_COUNT, au1Value);
    SEND_NOTIFY_TO_CONTROL_POINTS ();
    NAT_TRC1 (NAT_TRC_ON, "PortMapEntryCounts :%d\n", (unsigned int)
              ((pIfNode->StaticNaptList).u4_Count));
#endif /* UPNP_WANTED */
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT", "The row of Static Napt Table of\
                  index %d, %u %u %u and %u is created \n", i4IfNum, u4LocalIpAddr, u2StartLocalPort, u2EndLocalPort, u2ProtocolNumber);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatSttNaptCreateAndWait                  *
* Description    :  This function creates a row in the table if it does  *
*             not exist already and sets the status to NOT_READY   *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on successful creation of the row    *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/
PUBLIC INT1
NatSttNaptCreateAndWait (INT4 i4IfNum, UINT4 u4LocalIpAddr,
                         UINT2 u2StartLocalPort, UINT2 u2EndLocalPort,
                         UINT2 u2ProtocolNumber)
{
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the StaticNapt Table and wait\n");
    return (NatSttNaptCreateAndGo (i4IfNum, u4LocalIpAddr, u2StartLocalPort,
                                   u2EndLocalPort, u2ProtocolNumber));
}

/***************************************************************************
* Function Name    :  NatSttNaptNotInService                   *
* Description    :  This funcions sets the status of an active row to    *
*                   NOT IN SERVICE                    *
*                                      *
* Input (s)    :  Interface Number, Local  IP Address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on setting the status to NOTINSERVICE  *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/
PUBLIC INT1
NatSttNaptNotInService (INT4 i4IfNum, UINT4 u4LocalIpAddr,
                        UINT2 u2StartLocalPort, UINT2 u2EndLocalPort,
                        UINT2 u2ProtocolNumber)
{

    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;

    pStaticNaptEntryNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Change the status of a Row in\
             StaticNapt Table to not-in-service\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);
    NAT_DBG1 (NAT_DBG_LLR, "StartLocalPort =%u\n", u2StartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "EndLocalPort =%u\n", u2EndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "Protocol Number =%u\n", u2ProtocolNumber);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntryNode->u4LocIpAddr == u4LocalIpAddr) &&
                (pStaticNaptEntryNode->u2StartLocPort == u2StartLocalPort) &&
                (pStaticNaptEntryNode->u2EndLocPort == u2EndLocalPort) &&
                ((pStaticNaptEntryNode->u2ProtocolNumber == u2ProtocolNumber) ||
                 (pStaticNaptEntryNode->u2ProtocolNumber == NAT_PROTO_ANY)))
            {
                if (pStaticNaptEntryNode->i4RowStatus ==
                    NAT_STATUS_NOT_IN_SERVICE)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Entry notinservice - return Success\n");
                    return SNMP_SUCCESS;
                }
                if (pStaticNaptEntryNode->i4RowStatus == NAT_STATUS_ACTIVE)
                {
                    NatDeleteSpecifiedEntry
                        ((UINT4) i4IfNum, u4LocalIpAddr,
                         pStaticNaptEntryNode->u4TranslatedLocIpAddr,
                         u2StartLocalPort,
                         pStaticNaptEntryNode->u2TranslatedLocPort);

                    NAT_TRC (NAT_TRC_ON,
                             "Entry Active - Changed to notinservice\n");
                    pStaticNaptEntryNode->i4RowStatus =
                        NAT_STATUS_NOT_IN_SERVICE;
                    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                                  "The row of Static Napt Table of  index %d,"
                                  "%u %u %u and %u is created \n",
                                  i4IfNum, u4LocalIpAddr, u2StartLocalPort,
                                  u2EndLocalPort, u2ProtocolNumber);
                    /* UPNP NAPT TIMER */
                    NaptStopTimer (&(pStaticNaptEntryNode->NaptTimerNode));
                    /* END */
                    return SNMP_SUCCESS;
                }
                else
                    break;
            }

        }
    }

    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Napt Table of index %d ,%u %u %u"
                  "and %u does not exist\n", i4IfNum, u4LocalIpAddr,
                  u2StartLocalPort, u2EndLocalPort, u2ProtocolNumber);
    NAT_TRC (NAT_TRC_ON, "Row doesn't exist - Failure setting notinservice\n");
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatSttNaptDestroy                    *
* Description    :  This function removes the row from the Static    *
*             address table                    *
*                                      *
* Input (s)    :  Interface number, Local  IP address          *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on successful removal of the entry,if   *
*             it exists.                       *
****************************************************************************/

PUBLIC INT1
NatSttNaptDestroy (INT4 i4IfNum, UINT4 u4LocalIpAddr, UINT2 u2StartLocalPort,
                   UINT2 u2EndLocalPort, UINT2 u2ProtocolNumber)
{
    tInterfaceInfo     *pIfNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
#ifdef UPNP_WANTED
    CHR1                au1Value[NAT_SIZE_OF_PM_PORT_VAL];
#endif

    pStaticNaptEntryNode = NULL;
    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Delete a row in the Static Table \n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);
    NAT_DBG1 (NAT_DBG_LLR, "LocalIpAddress =%u\n", u4LocalIpAddr);
    NAT_DBG1 (NAT_DBG_LLR, "StartLocalPort =%u\n", u2StartLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "EndLocalPort =%u\n", u2EndLocalPort);
    NAT_DBG1 (NAT_DBG_LLR, "Protocol Number =%u\n", u2ProtocolNumber);

    if (pIfNode != NULL)
    {
        TMO_SLL_Scan (&pIfNode->StaticNaptList, pStaticNaptEntryNode,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntryNode->u4LocIpAddr == u4LocalIpAddr) &&
                (pStaticNaptEntryNode->u2StartLocPort == u2StartLocalPort) &&
                (pStaticNaptEntryNode->u2EndLocPort == u2EndLocalPort) &&
                (pStaticNaptEntryNode->u2ProtocolNumber == u2ProtocolNumber))
            {
                if ((pStaticNaptEntryNode->u1AppCallStatus != NAT_NOT_ON_HOLD)
                    && (pStaticNaptEntryNode->u1AppCallStatus != NAT_ON_HOLD))
                {

                    NatDeleteSpecifiedEntry
                        ((UINT4) i4IfNum, u4LocalIpAddr,
                         pStaticNaptEntryNode->u4TranslatedLocIpAddr,
                         u2StartLocalPort,
                         pStaticNaptEntryNode->u2TranslatedLocPort);
                }

                TMO_SLL_Delete (&pIfNode->StaticNaptList,
                                &(pStaticNaptEntryNode->StaticNaptTable));
                NatMemReleaseMemBlock (NAT_STATIC_NAPT_POOL_ID,
                                       (UINT1 *) pStaticNaptEntryNode);

#ifdef UPNP_WANTED
                SPRINTF (au1Value, "%d", (unsigned int)
                         ((pIfNode->StaticNaptList).u4_Count));
                SET_UPNP_NAT_STATE_VAR_VAL
                    (NAT_VARIABLE_TYPE_PORT_MAP_ENT_COUNT, au1Value);
                SEND_NOTIFY_TO_CONTROL_POINTS ();
                NAT_TRC1 (NAT_TRC_ON, "PortMapEntryCounts :%d\n",
                          (unsigned int) ((pIfNode->StaticNaptList).u4_Count));
#endif /* UPNP_WANTED */
                NAT_TRC (NAT_TRC_ON, "Deleted the Row from Static Table\n");
                MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                              "The row of Static Address Table\
                              of index %d, %u %u %u " "and %u is destroyed\n", i4IfNum, u4LocalIpAddr, u2StartLocalPort, u2EndLocalPort, u2ProtocolNumber);
                return SNMP_SUCCESS;
            }
        }
    }

    NAT_TRC (NAT_TRC_ON, "Failed to delete the Row from Static Table\n");
    MOD_TRC_ARG5 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Static Address Table of index %d, %u %u %u "
                  "and %u is destroyed\n", i4IfNum, u4LocalIpAddr,
                  u2StartLocalPort, u2EndLocalPort, u2ProtocolNumber);
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatIftActive                     *
* Description    :  This function sets the Interface row status to ACTIVE      *
*                                      *
* Input (s)    :  Interface Number                    *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/

PUBLIC INT1
NatIftActive (INT4 i4IfNum)
{
    tInterfaceInfo     *pIfNode = NULL;
/*   tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode;
   tLocIpAddrNode *pLocIpAddrNode; */

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Active a  Row in the Interface Table\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);

    if (pIfNode == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIfNode->i4RowStatus == NAT_STATUS_INVLD)
    {
        return SNMP_FAILURE;
    }

    if (pIfNode->i4RowStatus == NAT_STATUS_ACTIVE)
    {
        NAT_TRC (NAT_TRC_ON, "Row is Active\n");
        NatIfInitFields ((UINT4) i4IfNum);
        return SNMP_SUCCESS;
    }

    if (pIfNode->i4RowStatus == NAT_STATUS_NOT_IN_SERVICE)
    {
        MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                      "The row status of Interface Table of index %d "
                      "is set to active\n", i4IfNum);
        NAT_TRC (NAT_TRC_ON, "Row notinservice - Actived\n");
        pIfNode->i4RowStatus = NAT_STATUS_ACTIVE;
        /*
         * If the Interface entries are not initalised to some values then
         * initialise the entries in the Interface table which has been made
         * active
         */
        NatIfInitFields ((UINT4) i4IfNum);

        if (pIfNode->u1NatEnable == NAT_DISABLE)
        {
            FsNatDisableOnIntf (i4IfNum);
        }
        else
        {
            FsNatEnableOnIntf (i4IfNum);
        }

        return SNMP_SUCCESS;
    }

    MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT",
             "Either Local Address Table or Translated Local Address Table "
             "is not configured properly. \n");
    NAT_TRC (NAT_TRC_ON, "Row notready/not exist - failure in activation\n");

    return SNMP_FAILURE;

}

/***************************************************************************
* Function Name  :  NatIftCreateAndGo                    *
* Description    :  This function creates the Interface row if it does not
*                   exist already                        *
* Input (s)    :  Interface Number                    *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on setting the status to ACTIVE     *
*             SNMP_FAILURE otherwise                *
****************************************************************************/

PUBLIC INT1
NatIftCreateAndGo (INT4 i4IfNum)
{
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the Interface Table and go\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);

    if (pIfNode != NULL)
    {
        if (pIfNode->i4RowStatus != NAT_STATUS_INVLD)
        {
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "Row of Interface Table of index %d already exist\n",
                          i4IfNum);
            NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
            return SNMP_FAILURE;
        }
        else
        {
            pIfNode->i4RowStatus = NAT_STATUS_NOT_READY;
            return SNMP_SUCCESS;
        }
    }

    /* Add an entry */

    if (NAT_MEM_ALLOCATE
        (NAT_INTERFACE_INFO_POOL_ID,
         gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pIfNode = gapNatIfTable[i4IfNum];
    NATIFTABLE_INITIALIZE (pIfNode, NAT_STATUS_NOT_READY);
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d is created \n",
                  i4IfNum);
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatIftCreateAndWait                  *
* Description    :  This function creates a row in the table if it does  *
*             not exist already and sets the status to NOT_READY   *
*                                      *
* Input (s)    :  Interface Number                    *
*                                      *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on successful creation of the row    *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatIftCreateAndWait (INT4 i4IfNum)
{
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Create a  Row in the interface table and wait\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);

    if (pIfNode != NULL)
    {
        if (pIfNode->i4RowStatus != NAT_STATUS_INVLD)
        {
            NAT_TRC (NAT_TRC_ON, "Row exists - Create Failed\n");
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                          "The row of Interface Table of index\
                          %d already exist\n", i4IfNum);
            return SNMP_FAILURE;
        }
        else
        {
            pIfNode->i4RowStatus = NAT_STATUS_NOT_READY;
            return SNMP_SUCCESS;
        }
    }

    /*Create Entry */

    if (NAT_MEM_ALLOCATE
        (NAT_INTERFACE_INFO_POOL_ID,
         gapNatIfTable[i4IfNum], tInterfaceInfo) == NULL)
    {
        MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
        return SNMP_FAILURE;
    }

    pIfNode = gapNatIfTable[i4IfNum];
    NATIFTABLE_INITIALIZE (pIfNode, NAT_STATUS_NOT_READY);
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d is created \n",
                  i4IfNum);
    NAT_TRC (NAT_TRC_ON, "Created the Row Successfully\n");
    return SNMP_SUCCESS;

}

/***************************************************************************
* Function Name    :  NatIftNotInService                   *
* Description    :  This funcions sets the status of an active row to    *
*             NOT IN SERVICE                    *
*                                      *
* Input (s)    :  Interface Number                    *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS, on setting the status to NOTINSERVICE  *
*             SNMP_FAILURE, otherwise.                *
****************************************************************************/

PUBLIC INT1
NatIftNotInService (INT4 i4IfNum)
{
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON,
             "Change the status of a Row in the interface table "
             "to not-in-service\n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);

    if (pIfNode != NULL)
    {
        if ((pIfNode->i4RowStatus != NAT_STATUS_INVLD) &&
            (pIfNode->i4RowStatus != NAT_STATUS_NOT_READY))
        {
            NAT_TRC (NAT_TRC_ON, "Entry Status Changed to notinservice\n");
            MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT", "The row status of\
                          Interface Table of  index  %d is set\
                          to NOT_IN_SERVICE \n", i4IfNum);
            pIfNode->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
            return SNMP_SUCCESS;
        }
    }

    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of index    %d does not exist\n",
                  i4IfNum);
    NAT_TRC (NAT_TRC_ON, "Row doesn't exist - Failure setting notinservice\n");
    return SNMP_FAILURE;
}

/***************************************************************************
* Function Name    :  NatIftDestroy                    *
* Description    :  This function removes the row from the global    *
*             address table                    *
*                                      *
* Input (s)    :  Interface number                    *
*                                      *
* Output (s)    :  NONE                         *
* Returns      :  SNMP_SUCCESS on successful removal of the entry,if   *
*             it exists.                       *
****************************************************************************/

PUBLIC INT1
NatIftDestroy (INT4 i4IfNum)
{
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[i4IfNum];
    NAT_TRC (NAT_TRC_ON, "Delete a row in the Interface Table \n");
    NAT_DBG1 (NAT_DBG_LLR, "Interface Number =%d\n", i4IfNum);

    if (pIfNode != NULL)
    {
        pIfNode->i4RowStatus = NAT_STATUS_INVLD;
        NATIFTABLE_RESET (pIfNode);

        NatMemReleaseMemBlock (NAT_INTERFACE_INFO_POOL_ID,
                               (UINT1 *) gapNatIfTable[i4IfNum]);
        gapNatIfTable[i4IfNum] = NULL;
    }

    NAT_TRC (NAT_TRC_ON, "Deleted the row in the Static Table\n");
    MOD_TRC_ARG1 (gu4NatTrc, MGMT_TRC, "NAT",
                  "The row of Interface Table of  index %d is destroyed\n",
                  i4IfNum);
    return SNMP_SUCCESS;

}

/***********************************************************************#****
* Function Name :  NatUpdateMask                                            *
*                                                                           *
* Description   :  This function updates the fields                         *
*                  u4CurrentTranslatedLocalIp and u4Range in the NAT        *
*                  Interface table when the corresponding Global IP mask    *
*                  is modified                                              *
*                                                                           *
* Input (s)     :  Interface number, global IP addr, address mask           *
*                                                                           *
* Output (s)    :  NONE                                                     *
*                                                                           *
* Returns       :  SNMP_SUCCESS/SNMP_FAILURE                                *
****************************************************************************/

PUBLIC INT1
NatUpdateMask (INT4 i4IfNum, UINT4 u4TranslatedLocalIpAddr, UINT4 u4Mask)
{
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    /* tTranslatedLocIpAddrNode *pNewTranslatedLocalIpAddrNode; */
    INT1                i1Before = NAT_ZERO;
    UINT4               u4OldTranslatedLocalIpRangeEnd = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "Entering the function NatUpdateMask \n");

    i1Before = NAT_FALSE;
    pTranslatedLocalIpAddrNode = NULL;
    /* pNewTranslatedLocalIpAddrNode = NULL; */
    if (gapNatIfTable[i4IfNum] == NULL)
    {
        NAT_DBG1 (NAT_DBG_LLR, "Interface table for the interface %d is NULL\n",
                  i4IfNum);
        NAT_TRC (NAT_TRC_ON, "Exiting the function NatUpdateMask : Failure \n");
        return SNMP_FAILURE;
    }

    NAT_TRC (NAT_TRC_ON, "Setting the mask for current IP & the Range\n");
    TMO_SLL_Scan (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                  pTranslatedLocalIpAddrNode, tTranslatedLocIpAddrNode *)
    {
        if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
            u4TranslatedLocalIpAddr)
        {
            u4OldTranslatedLocalIpRangeEnd =
                pTranslatedLocalIpAddrNode->
                u4TranslatedLocIpAddr | (~(pTranslatedLocalIpAddrNode->u4Mask));
            if (u4OldTranslatedLocalIpRangeEnd ==
                gapNatIfTable[i4IfNum]->u4TranslatedLocIpRangeEnd)
            {
                gapNatIfTable[i4IfNum]->u4TranslatedLocIpRangeEnd =
                    u4TranslatedLocalIpAddr | (~(u4Mask));
                pTranslatedLocalIpAddrNode->u4Mask = u4Mask;
                return SNMP_SUCCESS;
            }
            else
                break;
        }
    }

    pTranslatedLocalIpAddrNode = (tTranslatedLocIpAddrNode *)
        TMO_SLL_First (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList));
    if (pTranslatedLocalIpAddrNode == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "Exiting the function NatUpdateMask : Failure \n");
        return SNMP_FAILURE;
    }

    u4OldTranslatedLocalIpRangeEnd =
        pTranslatedLocalIpAddrNode->
        u4TranslatedLocIpAddr | (~(pTranslatedLocalIpAddrNode->u4Mask));
    while (gapNatIfTable[i4IfNum]->u4TranslatedLocIpRangeEnd !=
           u4OldTranslatedLocalIpRangeEnd)
    {
        if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
            u4TranslatedLocalIpAddr)
        {
            i1Before = NAT_TRUE;
            break;
        }
        pTranslatedLocalIpAddrNode = (tTranslatedLocIpAddrNode *)
            TMO_SLL_Next (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                          &(pTranslatedLocalIpAddrNode->
                            TranslatedLocIpAddrNode));
        if (pTranslatedLocalIpAddrNode == NULL)
        {
            break;
        }
        u4OldTranslatedLocalIpRangeEnd =
            pTranslatedLocalIpAddrNode->
            u4TranslatedLocIpAddr | (~(pTranslatedLocalIpAddrNode->u4Mask));
    }

    if (i1Before == NAT_TRUE)
    {
        /*Can not change the mask */
        NAT_TRC (NAT_TRC_ON, "Exiting the function NatUpdateMask : Failure \n");
        return SNMP_FAILURE;
    }
    else
    {
        /*  The node is after the current ip node */
        if (pTranslatedLocalIpAddrNode != NULL)
        {
            while ((pTranslatedLocalIpAddrNode = (tTranslatedLocIpAddrNode *)
                    TMO_SLL_Next
                    (&(gapNatIfTable[i4IfNum]->TranslatedLocIpList),
                     &(pTranslatedLocalIpAddrNode->
                       TranslatedLocIpAddrNode))) != NULL)
            {
                if (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr ==
                    u4TranslatedLocalIpAddr)
                {
                    NAT_TRC (NAT_TRC_ON,
                             "Set the Mask ; The current node is after the node"
                             "pointed by u4CurrTranslatedLocalIpAddr \n");
                    pTranslatedLocalIpAddrNode->u4Mask = u4Mask;
                    break;
                }

            }

        }
    }

    /*Successfully updated the mask */
    NAT_TRC (NAT_TRC_ON, "Exiting the function NatUpdateMask : Success \n");
    return SNMP_SUCCESS;

}

#endif /* _NATLLR_C_ */
