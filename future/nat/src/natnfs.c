/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natnfs.c,v 1.2 2013/10/25 10:52:27 siva Exp $
 *
 * Description:. This file contains functions for parsing
 *             the nfs packets and modifying the nfs payload.
 *
 *******************************************************************/
#include "natinc.h"

/********************************************************************************  Function Name    : NatProcessNfs
*  Description        : NAT is maintaining a separate table for NFS.This
*             function will create a separate dynamic entry in
*             the NFS table and maintains all the nfs entries
*             corresponding to different NFS sessions flowing
*             across the NAT router.
*  Input(s)        : pBuf - Pointer to the NFS packet.
*             pHeaderInfo - Pointer to the structure containing
*             NAT/NAPT entry details corresponding to the session
*             in which the current packet is exchanged.
*  Output(s)        : pBuf - Modified  packet.
*  Returns        : NAT_SUCCESS always
*******************************************************************************/
UINT4
NatProcessNfs (tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_ZERO;

    /*
     * calls function which searches for the session entry in the NFS session
     * table.If the entry is not there then a new session is created.
     */
    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatProcessNfs \n");
    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessNfs \n");
    u4Status = NAT_FAILURE;
    u4Status = NatSearchNfsTable (pHeaderInfo);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatProcessNfs \n");
    return (u4Status);
}
