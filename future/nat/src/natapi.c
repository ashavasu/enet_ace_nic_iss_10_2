/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natapi.c,v 1.15 2014/01/31 13:10:30 siva Exp $ 
 *
 * Description:This file contains the APIs exported by NAT for
 *             other modules          
 *******************************************************************/
#ifndef _NATAPI_C
#define _NATAPI_C

#include "natinc.h"
#include "fsnatlw.h"
/***************************************************************************
 * Function Name : NatApiHandleCleanAllBinds
 *
 * Description   : This Function is used to clean all the NAT bindings
 *
 * Input         : None
 *
 * Output        : None
 * 
 * Returns       : NAT_SUCCESS/NAT_FAILURE         
 *
 **************************************************************************/

UINT4
NatApiHandleCleanAllBinds ()
{
    tStaticNaptEntry   *pStaticNaptEntryNode = NULL;
    tStaticNaptEntry   *pNextStaticNapt = NULL;
    tInterfaceInfo     *pIfNode = NULL;
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pNatPartialNextNode = NULL;
    tDynamicEntry      *pDynamicEntry = NULL;
    tDynamicEntry      *pNextNode = NULL;
    INT4                i4IfNum = NAT_ZERO;

    CFA_LOCK ();

    for (i4IfNum = NAT_ZERO; i4IfNum < NAT_MAX_NUM_IF; i4IfNum++)
    {
        pIfNode = gapNatIfTable[i4IfNum];

        if (pIfNode != NULL)
        {
            pStaticNaptEntryNode =
                (tStaticNaptEntry *) TMO_SLL_First (&pIfNode->StaticNaptList);

            while (pStaticNaptEntryNode != NULL)
            {
                if ((pStaticNaptEntryNode->u1AppCallStatus == NAT_ON_HOLD) ||
                    (pStaticNaptEntryNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
                {
                    pNextStaticNapt = (tStaticNaptEntry *) TMO_SLL_Next
                        (&pIfNode->StaticNaptList,
                         (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);

                    /* Cleaning virtual server entry */
                    TMO_SLL_Delete (&pIfNode->StaticNaptList,
                                    (tTMO_SLL_NODE *) (VOID *)
                                    pStaticNaptEntryNode);

                    NatMemReleaseMemBlock (NAT_STATIC_NAPT_POOL_ID,
                                           (UINT1 *) pStaticNaptEntryNode);
                }
                else
                {
                    pNextStaticNapt = (tStaticNaptEntry *) TMO_SLL_Next
                        (&pIfNode->StaticNaptList,
                         (tTMO_SLL_NODE *) (VOID *) pStaticNaptEntryNode);
                }

                pStaticNaptEntryNode = pNextStaticNapt;
            }                    /* End of while */
        }                        /* End of If */
    }                            /* End of for */

    /* Scan the Dynamic list and delete all SIP entries */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {
        pNextNode =
            (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                            (tTMO_SLL_NODE *) (VOID *)
                                            pDynamicEntry);

        if ((pDynamicEntry->u1AppCallStatus == NAT_ON_HOLD) ||
            (pDynamicEntry->u1AppCallStatus == NAT_NOT_ON_HOLD))
        {
            NatRemoveDynamicEntry (pDynamicEntry);
        }
        pDynamicEntry = pNextNode;
    }

    /* Scan the partial list and delete all SIP entries */
    pNatPartialLinkNode =
        (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

    while (pNatPartialLinkNode != NULL)
    {
        pNatPartialNextNode =
            (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                  (tTMO_SLL_NODE *) (VOID *)
                                                  pNatPartialLinkNode);

        if ((pNatPartialLinkNode->u1AppCallStatus == NAT_ON_HOLD) ||
            (pNatPartialLinkNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
        {

            TMO_SLL_Delete (&gNatPartialLinksList,
                            (tTMO_SLL_NODE *) (VOID *) pNatPartialLinkNode);
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);
        }

        pNatPartialLinkNode = pNatPartialNextNode;
    }

    CFA_UNLOCK ();

    return (NAT_SUCCESS);
}

/****************************************************************************
 *
 * Function Name : NatApiCheckInterfaceNatEnabled
 *
 * Description   : Gets first Nat Enabled Interface
 *
 * Inputs        : u4ExtNatIp - External IP address
 *
 * Output        : i4OutInterfaceNum - First NAT Enabled Interface 
 *
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 *
 * **************************************************************************/

INT4
NatApiCheckInterfaceNatEnabled (INT4 *i4OutInterfaceNum, UINT4 u4ExtNatIp)
{
    INT4                i4NatIfInterfaceNumber = NAT_ZERO;
    INT4                i4ReturnVal = NAT_SUCCESS;
    INT4                i4RetValNatIfNat = NAT_ZERO;
    INT4                i4PrevNatIfInterfaceNumber = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;

    /* Tring to get a nat enbaled interface */

    CFA_LOCK ();

    if (nmhGetFirstIndexNatIfTable (&i4NatIfInterfaceNumber) == SNMP_FAILURE)
    {
        *i4OutInterfaceNum = NAT_ZERO;
        i4ReturnVal = NAT_FAILURE;
    }

    do
    {
        if (i4ReturnVal == NAT_FAILURE)
        {
            break;
        }

        if (nmhGetNatIfNat (i4NatIfInterfaceNumber, &i4RetValNatIfNat)
            == SNMP_FAILURE)
        {
            i4ReturnVal = NAT_FAILURE;
            break;
        }
        if (NAT_ENABLE == i4RetValNatIfNat)
        {
            if (CfaGetIfIpAddr (i4NatIfInterfaceNumber,
                                &u4IpAddr) == OSIX_FAILURE)
            {
                i4ReturnVal = NAT_FAILURE;
                break;
            }

            if (u4ExtNatIp == u4IpAddr)
            {
                break;
            }
        }

        i4PrevNatIfInterfaceNumber = i4NatIfInterfaceNumber;
        i4NatIfInterfaceNumber = NAT_ZERO;
    }
    while (nmhGetNextIndexNatIfTable (i4PrevNatIfInterfaceNumber,
                                      &i4NatIfInterfaceNumber) == SNMP_SUCCESS);

    if (i4NatIfInterfaceNumber == NAT_ZERO)
    {
        *i4OutInterfaceNum = NAT_ZERO;
        i4ReturnVal = NAT_FAILURE;
    }
    else
    {
        *i4OutInterfaceNum = i4NatIfInterfaceNumber;
    }

    CFA_UNLOCK ();
    return i4ReturnVal;
}

/***************************************************************************
 *                                                                         *
 * Function Name : NatApiAddPortMappingEntry                               *
 *                                                                         *
 * Description   : This Function is used to Add a NAPT Table Entry         *
 *                                                                         *
 * Input (s)     : u4IfNum            - Interface Number                   *
 *                 u4LocalIpAddr      - Local IP Address                   * 
 *                 i4AppLocalPort     - Local Port                         *
 *                 i4ProtocolNumber   - Protocol number                    *
 *                 u4TranslatedIpAddr - Translated IP Address              *
 *                 pi4TranslatedPort  - Translated port                    *
 *                 i4NewRowStatus     - Row status                         *
 *                 u2Direction        - Traffic direction                  *
 *                 i4Timeout          - Timeout value                      *
 *                 u1CallHoldFlag     - Timer flag                         *
 *                 pu1Desc            - Descriptor                         *
 *                                                                         *
 * Output (s)    : pu4ErrorCode - Error code                               *
 *                 pDynamicEntry - Dynamic entry pointer                   *
 *                                                                         *
 * Returns       : NAT_SUCCESS/NAT_FAILURE                                 *
 *                                                                         *
 ***************************************************************************/

UINT4
NatApiAddPortMappingEntry (UINT4 u4IfNum, UINT4 u4LocalIpAddr,
                           INT4 i4AppLocalPort, INT4 i4ProtocolNumber,
                           UINT4 u4TranslatedIpAddr, INT4 *pi4TranslatedPort,
                           INT4 i4NewRowStatus, UINT2 u2Direction,
                           INT4 i4TimeOut, UINT1 u1CallHoldFlag,
                           UINT1 *pu1Desc, UINT4 *pu4ErrorCode,
                           tDynamicEntry * pDynamicEntry)
{

    INT4                i4RetVal = NAT_SUCCESS;
    INT4                i4RetVal1 = NAT_SUCCESS;
    INT4                i4RetVal2 = NAT_SUCCESS;

    UNUSED_PARAM (pDynamicEntry);
    UNUSED_PARAM (i4NewRowStatus);
    UNUSED_PARAM (i4TimeOut);
    UNUSED_PARAM (pu1Desc);

    if (i4ProtocolNumber == NAT_PROTO_ANY)
    {
        i4ProtocolNumber = NAT_TCP;

        i4RetVal1 = NatUtilAddPartialList (u4LocalIpAddr, NAT_ZERO,
                                           (UINT2) i4AppLocalPort,
                                           NAT_ZERO,
                                           u4TranslatedIpAddr,
                                           pi4TranslatedPort,
                                           (UINT2) i4ProtocolNumber,
                                           u2Direction, (INT4) u4IfNum,
                                           u1CallHoldFlag);

        i4ProtocolNumber = NAT_UDP;

        i4RetVal2 = NatUtilAddPartialList (u4LocalIpAddr, NAT_ZERO,
                                           (UINT2) i4AppLocalPort,
                                           NAT_ZERO,
                                           u4TranslatedIpAddr,
                                           pi4TranslatedPort,
                                           (UINT2) i4ProtocolNumber,
                                           u2Direction, (INT4) u4IfNum,
                                           u1CallHoldFlag);
    }
    else
    {

        i4RetVal = NatUtilAddPartialList (u4LocalIpAddr, NAT_ZERO,
                                          (UINT2) i4AppLocalPort,
                                          NAT_ZERO,
                                          u4TranslatedIpAddr,
                                          pi4TranslatedPort,
                                          (UINT2) i4ProtocolNumber, u2Direction,
                                          (INT4) u4IfNum, u1CallHoldFlag);
    }

    if ((i4RetVal1 != NAT_SUCCESS) || (i4RetVal2 != NAT_SUCCESS) ||
        (i4RetVal != NAT_SUCCESS))
    {

        if ((i4RetVal == NATIPC_BINDING_ALREADY_EXISTS) ||
            (i4RetVal1 == NATIPC_BINDING_ALREADY_EXISTS) ||
            (i4RetVal2 == NATIPC_BINDING_ALREADY_EXISTS))
        {
            *pu4ErrorCode = NATIPC_BINDING_ALREADY_EXISTS;
        }
        else
        {
            *pu4ErrorCode = NATIPC_INTERNAL_ERROR;
        }

        return NAT_FAILURE;
    }

    return NAT_SUCCESS;
}

/***************************************************************************
 * Function Name : NatApiDelPortMappingEntry                               *
 *                                                                         *
 * Description   : This Function is used to  Delete a NAPT Table Entry     *
 *                                                                         *
 * Input (s)     : u4IfNum           -  Interface Number                   *
 *                 pPortMappingEntry - PortMappingEntry                    *
 *                                                                         *
 * Output (s)    :  None.                                                  *
 *                                                                         *
 * Returns       : NAT_SUCCESS/NAT_FAILURE                                 *
 *                                                                         *
 ***************************************************************************/

UINT4
NatApiDelPortMappingEntry (INT4 i4IfNum, UINT4 u4LocIpAddr,
                           UINT4 u4TranslatedIpAddr, UINT4 u4TranslatedLocPort,
                           INT4 i4StartLocalPort, UINT2 u2Protocol,
                           UINT2 u2Direction, UINT4 *pu4ErrorCode)
{

    INT4                i4RetVal = NAT_SUCCESS;
    INT4                i4RetVal1 = NAT_SUCCESS;
    INT4                i4RetVal2 = NAT_SUCCESS;

    UNUSED_PARAM (u4TranslatedLocPort);
    if (u2Direction == NAT_ANY)
    {
        if (u2Protocol == NAT_PROTO_ANY)
        {

            i4RetVal1 =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_TCP,
                                       NAT_INBOUND, i4IfNum, NAT_ZERO);

            i4RetVal1 =
                NatUtilDelPartialList (u4LocIpAddr,
                                       (UINT2) i4StartLocalPort, NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_TCP,
                                       NAT_OUTBOUND, i4IfNum, NAT_ZERO);

            i4RetVal2 =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_UDP,
                                       NAT_INBOUND, i4IfNum, NAT_ZERO);

            i4RetVal2 =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_UDP,
                                       NAT_OUTBOUND, i4IfNum, NAT_ZERO);
        }
        else
        {
            i4RetVal =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort,
                                       (UINT1) u2Protocol, NAT_INBOUND, i4IfNum,
                                       NAT_ZERO);

            i4RetVal =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort,
                                       (UINT1) u2Protocol, NAT_OUTBOUND,
                                       i4IfNum, NAT_ZERO);
        }
    }
    else
    {
        if (u2Protocol == NAT_PROTO_ANY)
        {

            i4RetVal1 =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_TCP,
                                       NAT_INBOUND, i4IfNum, NAT_ZERO);

            i4RetVal2 =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort, NAT_UDP,
                                       NAT_INBOUND, i4IfNum, NAT_ZERO);

        }
        else
        {
            i4RetVal =
                NatUtilDelPartialList (u4LocIpAddr, (UINT2) i4StartLocalPort,
                                       NAT_ZERO,
                                       NAT_ZERO, u4TranslatedIpAddr,
                                       (UINT2) u4TranslatedLocPort,
                                       (UINT1) u2Protocol, NAT_INBOUND, i4IfNum,
                                       NAT_ZERO);

        }
    }

    if ((i4RetVal != NAT_SUCCESS) ||
        (i4RetVal1 != NAT_SUCCESS) || (i4RetVal2 != NAT_SUCCESS))
    {

        if ((i4RetVal == NATIPC_PINHOLE_NOT_EXISTS) ||
            (i4RetVal1 == NATIPC_PINHOLE_NOT_EXISTS) ||
            (i4RetVal2 == NATIPC_PINHOLE_NOT_EXISTS))
        {
            *pu4ErrorCode = NATIPC_BINDING_NOT_EXISTS;
        }
        else
        {
            *pu4ErrorCode = NATIPC_INTERNAL_ERROR;
        }

        return NAT_FAILURE;
    }

    return NAT_SUCCESS;

}

/***************************************************************************
 * Function Name : NatApiHandleOpenPinhole
 *
 * Description   : This Function is used to handle open pinhole request 
 *
 * Inputs        : pUpnpPortMappingEntry - > Pointer to PortMap Structure
 *
 * Output        : None
 * 
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 * 
 * ***********************************************************************/

INT4
NatApiHandleOpenPinhole (INT4 i4IfNum, UINT4 u4SrcIpAddr, UINT2 u2SrcPort,
                         UINT2 u2Protocol, UINT2 u2Direction,
                         UINT4 u4DestIpAddr, UINT2 u2DestPort,
                         UINT1 u1CallHoldFlag, UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = NAT_SUCCESS;
    INT4                i4RetVal1 = NAT_SUCCESS;
    INT4                i4RetVal2 = NAT_SUCCESS;
    INT4                i4DummyTransPort = NAT_ZERO;

    if (u2Protocol == NAT_PROTO_ANY)
    {
        u2Protocol = NAT_TCP;

        i4RetVal1 = NatUtilAddPartialList (u4SrcIpAddr, u4DestIpAddr,
                                           u2SrcPort, u2DestPort,
                                           NAT_ZERO, &i4DummyTransPort,
                                           u2Protocol, u2Direction,
                                           i4IfNum, u1CallHoldFlag);

        u2Protocol = NAT_UDP;

        i4RetVal2 = NatUtilAddPartialList (u4SrcIpAddr, u4DestIpAddr,
                                           u2SrcPort, u2DestPort,
                                           NAT_ZERO, &i4DummyTransPort,
                                           u2Protocol, u2Direction,
                                           i4IfNum, u1CallHoldFlag);

    }
    else
    {
        i4RetVal = NatUtilAddPartialList (u4SrcIpAddr, u4DestIpAddr,
                                          u2SrcPort, u2DestPort,
                                          NAT_ZERO, &i4DummyTransPort,
                                          u2Protocol, u2Direction,
                                          i4IfNum, u1CallHoldFlag);
    }

    if ((i4RetVal1 != NAT_SUCCESS) || (i4RetVal2 != NAT_SUCCESS) ||
        (i4RetVal != NAT_SUCCESS))
    {
        if ((i4RetVal == NATIPC_PINHOLE_ALREADY_EXISTS) ||
            (i4RetVal1 == NATIPC_PINHOLE_ALREADY_EXISTS) ||
            (i4RetVal2 == NATIPC_PINHOLE_ALREADY_EXISTS))
        {
            *pu4ErrorCode = NATIPC_PINHOLE_ALREADY_EXISTS;
        }
        else
        {
            *pu4ErrorCode = NATIPC_INTERNAL_ERROR;
        }
        return NAT_FAILURE;
    }

    return NAT_SUCCESS;
}

/***************************************************************************
 * Function Name : NatApiHandleClosePinhole
 *
 * Description   : This Function is used to handle open pinhole request 
 *
 * Inputs        : pUpnpPortMappingEntry - > Pointer to PortMap Structure
 *
 * Output        : None
 * 
 * Returns       : NAT_SUCCESS/NAT_FAILURE
 * 
 * ***********************************************************************/

INT4
NatApiHandleClosePinhole (INT4 i4IfNum, UINT4 u4SrcIpAddr, UINT2 u2SrcPort,
                          UINT2 u2Protocol, UINT2 u2Direction,
                          UINT4 u4DestIpAddr, UINT2 u2DestPort,
                          UINT1 u1CallHoldFlag, UINT4 *pu4ErrorCode)
{
    INT4                i4RetVal = NAT_SUCCESS;
    INT4                i4RetVal1 = NAT_SUCCESS;
    INT4                i4RetVal2 = NAT_SUCCESS;

    if (u2Protocol == NAT_PROTO_ANY)
    {
        i4RetVal1 = NatUtilDelPartialList (u4SrcIpAddr, u2SrcPort,
                                           u4DestIpAddr, u2DestPort,
                                           NAT_ZERO, NAT_ZERO,
                                           NAT_TCP,
                                           (UINT1) u2Direction,
                                           i4IfNum, u1CallHoldFlag);

        i4RetVal2 = NatUtilDelPartialList (u4SrcIpAddr, u2SrcPort,
                                           u4DestIpAddr, u2DestPort,
                                           NAT_ZERO, NAT_ZERO,
                                           NAT_UDP,
                                           (UINT1) u2Direction,
                                           i4IfNum, u1CallHoldFlag);

    }
    else
    {
        i4RetVal = NatUtilDelPartialList (u4SrcIpAddr, u2SrcPort,
                                          u4DestIpAddr, u2DestPort,
                                          NAT_ZERO, NAT_ZERO,
                                          (UINT1) u2Protocol,
                                          (UINT1) u2Direction,
                                          i4IfNum, u1CallHoldFlag);

    }

    if ((i4RetVal1 != NAT_SUCCESS) ||
        (i4RetVal2 != NAT_SUCCESS) || (i4RetVal != NAT_SUCCESS))
    {
        if ((i4RetVal == NATIPC_PINHOLE_NOT_EXISTS) ||
            (i4RetVal1 == NATIPC_PINHOLE_NOT_EXISTS) ||
            (i4RetVal2 == NATIPC_PINHOLE_NOT_EXISTS))
        {
            *pu4ErrorCode = NATIPC_PINHOLE_NOT_EXISTS;
        }
        else
        {
            *pu4ErrorCode = NATIPC_INTERNAL_ERROR;
        }
        return NAT_FAILURE;
    }

    return NAT_SUCCESS;
}

/****************************************************************************
 *
 * Function Name : NatApiNaptSearchDynamicList
 *
 * Description   : Searches the dynamic list against Local Ip, Local Port
 *                 and Nat IP and returns the Pointer to dynamic entry
 *
 * Inputs        : u4LocIpAddr, u2LocPort, u4TranslatedIpAddr and the pkt type
 *
 * Output        : pDynamicEntry 
 *
 * Returns       : u2Port (NAT port dynamic entry) / or 0
 *
 * **************************************************************************/

UINT2
NatApiNaptSearchDynamicList (UINT4 u4LocIpAddr, UINT2 u2LocPort,
                             UINT4 u4TranslatedIpAddr,
                             UINT1 u1PktType, tDynamicEntry ** ppDynamicEntry)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    UINT2               u2Port = NAT_ZERO;

    *ppDynamicEntry = NULL;

    TMO_SLL_Scan (&gNatDynamicList, pDynamicEntry, tDynamicEntry *)
    {
        if ((pDynamicEntry->u4LocIpAddr == u4LocIpAddr) &&
            (pDynamicEntry->u2LocPort == u2LocPort) &&
            (pDynamicEntry->u4TranslatedLocIpAddr == u4TranslatedIpAddr))
        {
            if (u1PktType != NAT_ANY)
            {
                if (pDynamicEntry->u1PktType != u1PktType)
                {
                    continue;
                }
            }

            /* A dynamic entry matching the Local IP/Port and Translated IP is
             * found */
            *ppDynamicEntry = pDynamicEntry;
            return (pDynamicEntry->u2TranslatedLocPort);
        }
    }

    /* If the match for the dynamic entry is not found return 0  */
    u2Port = NATIPC_PORT_NUM_ZERO;

    return (u2Port);
}

/****************************************************************************
 *
 * Function Name : NatApiUpdateDynamicCallStatus
 *
 * Description   : Searches the dynamic list against Remote IP, Remote Port
 *                 and protocol for partial list based dynamic entry updation 
 *                 (or) Local IP, Local Port for napt based dynamic entry and 
 *                 updates the hold status if a matching entry is found.
 *
 * Inputs        : u4IpAddr, u2Port, u4TranslatedLocIpAddr, u2TranslatedLocPort,
 *                 u1DynamicEntryType, u1PktType, u1AppCallStatus
 *
 * Output        : NONE
 *
 * Returns       : NAT_SUCCESS, NAT_FAILURE
 *
 * **************************************************************************/

UINT4
NatApiUpdateDynamicCallStatus (UINT4 u4IpAddr,
                               UINT2 u2Port,
                               UINT4 u4TranslatedLocIpAddr,
                               UINT2 u2TranslatedLocPort,
                               UINT1 u1PktType,
                               UINT1 u1DynamicEntryType, UINT1 u1AppCallStatus)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    UINT1               u1Found = NAT_ZERO;

    if (u1DynamicEntryType == NAT_OUT_PARTIAL)
    {
        TMO_SLL_Scan (&gNatDynamicList, pDynamicEntry, tDynamicEntry *)
        {
            if ((pDynamicEntry->u4OutIpAddr == u4IpAddr) &&
                (pDynamicEntry->u2OutPort == u2Port) &&
                (pDynamicEntry->u1Type == u1DynamicEntryType))
            {
                if (u1PktType != NAT_ANY)
                {
                    if (pDynamicEntry->u1PktType != u1PktType)
                    {
                        continue;
                    }
                }

                pDynamicEntry->u1AppCallStatus = u1AppCallStatus;
                u1Found = NAT_ONE;
            }
        }
    }
    else if (u1DynamicEntryType == NAT_IN_PARTIAL)
    {
        TMO_SLL_Scan (&gNatDynamicList, pDynamicEntry, tDynamicEntry *)
        {
            if ((pDynamicEntry->u4LocIpAddr == u4IpAddr) &&
                (pDynamicEntry->u2LocPort == u2Port) &&
                (pDynamicEntry->u4TranslatedLocIpAddr ==
                 u4TranslatedLocIpAddr) &&
                (pDynamicEntry->u2TranslatedLocPort ==
                 u2TranslatedLocPort) &&
                (pDynamicEntry->u1Type == u1DynamicEntryType))
            {
                if (u1PktType != NAT_ANY)
                {
                    if (pDynamicEntry->u1PktType != u1PktType)
                    {
                        continue;
                    }
                }

                pDynamicEntry->u1AppCallStatus = u1AppCallStatus;
                u1Found = NAT_ONE;
            }
        }
    }

    if (u1Found)
    {
        return NAT_SUCCESS;
    }

    return NAT_FAILURE;

}

/****************************************************************************
 *
 * Function Name : NatApiPartialCleanup
 *
 * Description   : Searches the dynamic list against Remote IP, Remote Port
 *                 and protocol for the given partial list and removes the
 *                 matching dynamic entry.
 *
 * Inputs        : tNatPartialLinkNode *
 *
 * Output        : NONE
 *
 * Returns       : NONE
 *
 * **************************************************************************/

VOID
NatApiPartialCleanup (tNatPartialLinkNode * pPartialNode)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    tDynamicEntry      *pNextDynamicEntry = NULL;
    UINT2               u2Port = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    UINT1               u1PktType = NAT_ZERO;

    u4IpAddr = pPartialNode->u4OutIpAddr;
    u2Port = pPartialNode->u2OutPort;
    u1PktType = pPartialNode->u1PktType;

    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {

        pNextDynamicEntry = (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                                            (tTMO_SLL_NODE
                                                             *) (VOID *)
                                                            pDynamicEntry);

        if ((pDynamicEntry->u4OutIpAddr == u4IpAddr) &&
            (pDynamicEntry->u2OutPort == u2Port) &&
            (NAT_OUT_PARTIAL == pDynamicEntry->u1Type))
        {
            if (u1PktType != NAT_ANY)
            {
                if (pDynamicEntry->u1PktType != u1PktType)
                {
                    continue;
                }
            }

            NatRemoveDynamicEntry (pDynamicEntry);
        }

        pDynamicEntry = pNextDynamicEntry;
    }
}

/***************************************************************************
* Function Name  :  NatSipFindPartialNode                                  *
*                                                                          *
* Description    :  This function finds the RTP partial node from the RTCP *
*                   partial entry (or) RTCP partial node from RTP node     *
*                                                                          * 
* Input (s)      :  pNatPartialEntry - NAT Partial Entry                   *
*                                                                          *
* Output (s)     :  None                                                   * 
*                                                                          *
* Returns        :  tNatPartialLinkNode* (or) NULL                         *
***************************************************************************/
tNatPartialLinkNode *
NatAppFindPartialNode (tNatPartialLinkNode * pPartialNode)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT4               u4OutIpAddr = NAT_ZERO;
    UINT2               u2OutPort = NAT_ZERO;
    UINT1               u1Direction = NAT_ZERO;

    pNatPartialLinkNode = NULL;

    u4OutIpAddr = pPartialNode->u4OutIpAddr;
    u1Direction = pPartialNode->u1Direction;

    if (NAT_ZERO == ((pPartialNode->u2OutPort) % MODULO_DIVISOR_2))
    {
        u2OutPort = (UINT2) (pPartialNode->u2OutPort + NAT_ONE);
    }
    else
    {
        u2OutPort = (UINT2) (pPartialNode->u2OutPort - NAT_ONE);
    }

    TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                  tNatPartialLinkNode *)
    {
        if ((pNatPartialLinkNode->u4OutIpAddr == u4OutIpAddr) &&
            (pNatPartialLinkNode->u1Direction == u1Direction) &&
            (pNatPartialLinkNode->u2OutPort == u2OutPort))
        {
            return pNatPartialLinkNode;
        }
    }
    return ((tNatPartialLinkNode *) NULL);
}

/******************************************************************************
 *                                                                            *
 * Function     : NatAddWanUaPortHashEntry                                    *
 *                                                                            *
 * Description  : This funtion is used to add a HashNode Entry.               *
 *                This Hash Node Stores Information About WANUA IP, Base Port,*
 *                Number of Contiguous Ports, Parity of the Base Port and     *
 *                Protocol. This Information is used to handle Early Data in  *
 *                SIP.                                                        *
 *                                                                            *
 * Input        : u4IpAddr   - IP Address of WANUA                            *
 *                u2BasePort - Base Port Of WANUA                             *
 *                u2NumPorts - Number of Contiguous Ports Starting From       *
 *                             BasePort.                                      *
 *                eBaseParity - Parity of BasePort. either ODD/EVEN/ANY       *
 *                u2OutProtocol - Protocoal either NAT_TCP/NAT_UDP/           *
 *                                NAT_PROTO_ANY                               *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : NAT_FAILURE/NAT_SUCCESS                                     *
 *                                                                            *
 ******************************************************************************/

UINT4
NatAddWanUaPortHashEntry (UINT4 u4IpAddr, UINT2 u2BasePort,
                          UINT2 u2NumPorts, eParity eBaseParity,
                          UINT2 u2OutProtocol)
{

    tNatWanUaHashNode  *pNatWanUaHashNode = NULL;
    UINT4               u4HashKey = NAT_ZERO;

    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatAddWanUaPortHashEntry() : " "Entering Function");
    u4HashKey = NatGetHashKeyFromIP (u4IpAddr);

    if (u2OutProtocol == NAT_PROTO_ANY)
    {
        if (NAT_MEM_ALLOCATE (NAT_SIP_HASH_POOL_ID,
                              pNatWanUaHashNode, tNatWanUaHashNode) == NULL)
        {
            MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                     "\n Unable to allocate memory for NatWanUaHashNode"
                     " From NAT_SIP_HASH_POOL_ID\n");
            return NAT_FAILURE;
        }
        /* Initialize Hash Node */
        TMO_HASH_Init_Node ((tTMO_HASH_NODE *) (VOID *) pNatWanUaHashNode);

        pNatWanUaHashNode->u4OutIpAdd = u4IpAddr;
        pNatWanUaHashNode->u2OutBasePort = u2BasePort;
        pNatWanUaHashNode->u2TotalBindings = u2NumPorts;
        pNatWanUaHashNode->eTransBasePortParity = eBaseParity;
        pNatWanUaHashNode->u2OutProtocol = NAT_TCP;

        TMO_HASH_Add_Node (gpNatWanUaHashTbl,
                           (tTMO_HASH_NODE *) (VOID *) pNatWanUaHashNode,
                           u4HashKey, NULL);
        u2OutProtocol = NAT_UDP;
    }

    if (NAT_MEM_ALLOCATE (NAT_SIP_HASH_POOL_ID,
                          pNatWanUaHashNode, tNatWanUaHashNode) == NULL)
    {
        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                 "\n Unable to allocate memory for NatWanUaHashNode"
                 " From NAT_SIP_HASH_POOL_ID\n");
        return NAT_FAILURE;
    }
    /* Initialize Hash Node */
    TMO_HASH_Init_Node ((tTMO_HASH_NODE *) (VOID *) pNatWanUaHashNode);
    /* Fill All Data */
    pNatWanUaHashNode->u4OutIpAdd = u4IpAddr;
    pNatWanUaHashNode->u2OutBasePort = u2BasePort;
    pNatWanUaHashNode->u2TotalBindings = u2NumPorts;
    pNatWanUaHashNode->eTransBasePortParity = eBaseParity;
    pNatWanUaHashNode->u2OutProtocol = u2OutProtocol;

    TMO_HASH_Add_Node (gpNatWanUaHashTbl,
                       (tTMO_HASH_NODE *) (VOID *) pNatWanUaHashNode, u4HashKey,
                       NULL);

    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatAddWanUaPortHashEntry() : " "Exiting Function");
    return NAT_SUCCESS;

}

/******************************************************************************
 *                                                                            *
 * Function     : NatDelWanUaPortHashEntry                                    *
 *                                                                            *
 * Description  : This funtion is used to Delete a HashNode Entry.            *
 *                This Searches for the coresponding HashNode by checking     *
 *                whether given port is in-between (Base-Port) and (BasePort  *
 *                + Number of Bindings).                                      *
 *                This Hash Node Stores Information About WANUA IP, Port and  *
 *                Number of Contiguous Ports. This Information is used to     *
 *                Handle Early Data in SIP.                                   *
 *                                                                            *
 * Input        : u4IpAddr   - IP Address of WANUA                            *
 *                u2Port          - Any Port in-between (BasePort) and        *
 *                                  (BasePort + Number of Bindings)           *
 *                u2TotalBindings - Number of Contiguous Ports Starting From  *
 *                                  BasePort.                                 *
 *                u2Protocol      - Protocoal either NAT_TCP/NAT_UDP/         *
 *                                  NAT_PROTO_ANY                             *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : NAT_FAILURE/NAT_SUCCESS                                     *
 *                                                                            *
 ******************************************************************************/

UINT4
NatDelWanUaPortHashEntry (UINT4 u4IpAddr, UINT2 u2Port, UINT2 u2TotalBindings,
                          UINT2 u2Protocol)
{

    tNatWanUaHashNode  *pNatWanUaHashNode = NULL;
    tNatWanUaHashNode  *pNatWanUaHashPrevNode = NULL;
    UINT4               u4HashKey = NAT_ZERO;
    UINT2               u2NumPorts = NAT_ZERO;
    eParity             eAddBaseParity = NAT_ZERO;
    UINT2               u2NumNodeToBeDeleted = NAT_ONE;
    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatDelWanUaPortHashEntry() : " "Entering Function");
    if (u2Protocol == NAT_PROTO_ANY)
    {
        u2NumNodeToBeDeleted = NAT_MAX_TRANS_PROT;
    }
    u4HashKey = NatGetHashKeyFromIP (u4IpAddr);
    /*
     * Example
     * BasePort : 2000
     * Num Bindings : 5
     * Parity : EVEN
     */
    TMO_HASH_Scan_Bucket (gpNatWanUaHashTbl, u4HashKey, pNatWanUaHashNode,
                          tNatWanUaHashNode *)
    {
        if ((pNatWanUaHashNode->u4OutIpAdd == u4IpAddr) &&
            ((pNatWanUaHashNode->u2OutProtocol == u2Protocol) ||
             (u2Protocol == NAT_PROTO_ANY)))
        {
            /*
             * Received
             * 2000 >= Port <2000 + 5
             * Num Bindings : NAT_DEL_ALL_HASH_BINDINGS
             */
            if ((u2TotalBindings == NAT_DEL_ALL_HASH_BINDINGS) &&
                (u2Port >= pNatWanUaHashNode->u2OutBasePort) &&
                (u2Port < (pNatWanUaHashNode->u2OutBasePort +
                           pNatWanUaHashNode->u2TotalBindings)))
            {
                TMO_HASH_Delete_Node (gpNatWanUaHashTbl,
                                      (tTMO_SLL_NODE *) (VOID *)
                                      pNatWanUaHashNode, u4HashKey);
                NatMemReleaseMemBlock (NAT_SIP_HASH_POOL_ID,
                                       (UINT1 *) pNatWanUaHashNode);
                pNatWanUaHashNode = pNatWanUaHashPrevNode;
                u2NumNodeToBeDeleted--;
                if (u2NumNodeToBeDeleted != NAT_ZERO)
                {
                    continue;
                }
                MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                         "\n NatDelWanUaPortHashEntry() : " "Exiting Function");
                return NAT_SUCCESS;
            }

            /*
             * Received
             * Port : 2000
             * Num Bindings : 5
             */
            if ((u2Port == pNatWanUaHashNode->u2OutBasePort) &&
                (u2TotalBindings == pNatWanUaHashNode->u2TotalBindings))
            {
                TMO_HASH_Delete_Node (gpNatWanUaHashTbl,
                                      (tTMO_SLL_NODE *) (VOID *)
                                      pNatWanUaHashNode, u4HashKey);
                NatMemReleaseMemBlock (NAT_SIP_HASH_POOL_ID,
                                       (UINT1 *) pNatWanUaHashNode);
                u2NumNodeToBeDeleted--;
                if (u2NumNodeToBeDeleted != NAT_ZERO)
                {
                    continue;
                }
                MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                         "\n NatDelWanUaPortHashEntry() : " "Exiting Function");
                return NAT_SUCCESS;
            }

            /*
             * Received
             * Port : 2000
             * Num Bindings : 3
             */
            if ((u2Port == pNatWanUaHashNode->u2OutBasePort) &&
                (u2TotalBindings < pNatWanUaHashNode->u2TotalBindings))
            {
                pNatWanUaHashNode->u2OutBasePort =
                    (UINT2) (pNatWanUaHashNode->u2OutBasePort +
                             u2TotalBindings);
                pNatWanUaHashNode->u2TotalBindings =
                    (UINT2) (pNatWanUaHashNode->u2TotalBindings -
                             u2TotalBindings);
                u2NumNodeToBeDeleted--;
                if (u2NumNodeToBeDeleted != NAT_ZERO)
                {
                    continue;
                }
                MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                         "\n NatDelWanUaPortHashEntry() : " "Exiting Function");
                return NAT_SUCCESS;
            }
            if ((u2Port > pNatWanUaHashNode->u2OutBasePort) &&
                (u2Port < (pNatWanUaHashNode->u2OutBasePort +
                           pNatWanUaHashNode->u2TotalBindings)))
            {
                /*
                 * Received
                 * Port : 2002
                 * Num Bindings : 3
                 */
                if (((pNatWanUaHashNode->u2OutBasePort +
                      pNatWanUaHashNode->u2TotalBindings)
                     - u2Port) == u2TotalBindings)
                {

                    pNatWanUaHashNode->u2TotalBindings =
                        (UINT2) (pNatWanUaHashNode->u2TotalBindings -
                                 u2TotalBindings);
                    u2NumNodeToBeDeleted--;
                    if (u2NumNodeToBeDeleted != NAT_ZERO)
                    {
                        continue;
                    }
                    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                             "\n NatDelWanUaPortHashEntry() : "
                             "Exiting Function");
                    return NAT_SUCCESS;
                }
                /*
                 * Received
                 * Port : 2001
                 * Num Bindings : 2
                 */
                if (((pNatWanUaHashNode->u2OutBasePort +
                      pNatWanUaHashNode->u2TotalBindings)
                     - u2Port) > u2TotalBindings)
                {
                    u2NumPorts =
                        (UINT2) ((pNatWanUaHashNode->u2OutBasePort +
                                  pNatWanUaHashNode->u2TotalBindings - u2Port) -
                                 u2TotalBindings);
                    if (pNatWanUaHashNode->eTransBasePortParity == PARITY_ANY)
                    {
                        eAddBaseParity = PARITY_ANY;
                    }
                    else
                    {
                        eAddBaseParity =
                            pNatWanUaHashNode->eTransBasePortParity;
                        if (((u2Port + u2TotalBindings) -
                             pNatWanUaHashNode->u2OutBasePort) %
                            MODULO_DIVISOR_2 == NAT_ONE)
                        {
                            if (pNatWanUaHashNode->eTransBasePortParity ==
                                PARITY_ODD)
                            {
                                eAddBaseParity = PARITY_EVEN;
                            }
                            else
                            {
                                eAddBaseParity = PARITY_ODD;
                            }

                        }
                    }
                    if ((NatAddWanUaPortHashEntry
                         (u4IpAddr, (UINT2) (u2Port + u2TotalBindings),
                          u2NumPorts, eAddBaseParity,
                          pNatWanUaHashNode->u2OutProtocol)) == NAT_FAILURE)
                    {
                        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                                 "\n NatDelWanUaPortHashEntry() : "
                                 "Exiting Function");
                        return NAT_FAILURE;
                    }

                    pNatWanUaHashNode->u2TotalBindings =
                        (UINT2) (u2Port - pNatWanUaHashNode->u2OutBasePort);
                    u2NumNodeToBeDeleted--;
                    if (u2NumNodeToBeDeleted != NAT_ZERO)
                    {
                        continue;
                    }
                    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                             "\n NatDelWanUaPortHashEntry() : "
                             "Exiting Function");
                    return NAT_SUCCESS;
                }
            }

        }
        pNatWanUaHashPrevNode = pNatWanUaHashNode;
    }
    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT", "\n NatWanUaHashNode Not Found");
    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatDelWanUaPortHashEntry() : " "Exiting Function");

    return NAT_FAILURE;

}

/******************************************************************************
 *                                                                            *
 * Function     : NatFindWanUaPortHashEntry                                   *
 *                                                                            *
 * Description  : This funtion is used to Search for a HashNode Entry.        *
 *                This Hash Node Stores Information About WANUA IP, base Port,*
 *                Number of Contiguous Ports, Parity of the Base Port and     *
 *                Protocol Information. This Information is used to handle    *
 *                Early Data in SIP.                                          *
 *                                                                            *
 * Input        : u4IpAddr   - IP Address of WANUA                            *
 *                u2Port     - This Port can be any value lies between        *
 *                             BasePort and BasePort+NumPorts.                *
 *                u2Protocol - Protocoal either NAT_TCP/NAT_UDP/              *
 *                             NAT_PROTO_ANY                                  *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : Pointer to tNatWanUaHashNode Structure on Success          *
 *                NULL when Node was not found.                               *
 *                                                                            *
 ******************************************************************************/

tNatWanUaHashNode  *
NatFindWanUaPortHashEntry (UINT4 u4IpAddr, UINT2 u2Port, UINT2 u2Protocol)
{

    tNatWanUaHashNode  *pNatWanUaHashNode = NULL;
    UINT4               u4HashKey = NAT_ZERO;

    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatFindWanUaPortHashEntry() : " "Entering Function");
    if (u2Protocol == NAT_PROTO_ANY)
    {
        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                 "\n NatFindWanUaPortHashEntry() : " "Invalid Input");
        MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                 "\n NatFindWanUaPortHashEntry() : " "Exiting Function");
        return NULL;
    }
    u4HashKey = NatGetHashKeyFromIP (u4IpAddr);

    TMO_HASH_Scan_Bucket (gpNatWanUaHashTbl, u4HashKey, pNatWanUaHashNode,
                          tNatWanUaHashNode *)
    {
        if ((pNatWanUaHashNode->u4OutIpAdd == u4IpAddr) &&
            (u2Protocol == pNatWanUaHashNode->u2OutProtocol) &&
            (u2Port >= pNatWanUaHashNode->u2OutBasePort) &&
            (u2Port < (pNatWanUaHashNode->u2OutBasePort +
                       pNatWanUaHashNode->u2TotalBindings)))
        {
            MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                     "\n NatFindWanUaPortHashEntry() : "
                     "NatWanUaHashNode Found");
            MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
                     "\n NatFindWanUaPortHashEntry() : " "Entering Function");
            return pNatWanUaHashNode;
        }
    }
    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatFindWanUaPortHashEntry() : " "NatWanUaHashNode Not Found");
    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatFindWanUaPortHashEntry() : " "Exiting Function");

    return NULL;

}

/******************************************************************************
 *                                                                            *
 * Function     : NatGetHashKeyFromIP                                         *
 *                                                                            *
 * Description  : This funtion is used Find HashKey From given IP Address.    *
 *                                                                            *
 * Input        : u4IpAddr   - IP Address of WANUA                            *
 *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : HashKey                                                     *
 *                                                                            *
 ******************************************************************************/

UINT4
NatGetHashKeyFromIP (UINT4 u4IpAddr)
{
    UINT4               u4HashKey = NAT_ZERO;

    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatGetHashKeyFromIP() : " "Entering Function");
    /*
       u4IpAddr = 192.168.30.120
       u4IpAddr = 3232243320
       u4IpAddr = 11000000 10101000 00011110 01111000
       192 = 11000000(lsb 2 bits = LSB1)
       168 = 10101000(all 8 bits = ALL1)
       30  = 00011110(lsb 2 bits = LSB2)
       120 = 01111000(all 8 bits = ALL2)
       HashKey = (LSB1 ALL1) XOR (LSB2 ALL2)
       = (0010101000) XOR (1001111000)
       = 1011010000
       = 720
     */
    u4HashKey = (((u4IpAddr & NAT_EXTRACT_LSB2_BITS) >> NAT_SIXTEEN) |
                 ((u4IpAddr & NAT_3RD_BYTE_MASK) >> NAT_SIXTEEN)) ^
        ((u4IpAddr & NAT_MASK_WITH_VAL_768) |
         (u4IpAddr & NAT_EXTRACT_MSB8_BITS));

    MOD_TRC (gu4NatTrc, INIT_SHUT_TRC, "NAT",
             "\n NatGetHashKeyFromIP() : " "Exiting Function");
    return u4HashKey;
}

/*****************************************************************************/
/*  Function Name   : NatApiSearchPolicyTable.                               */
/*  Description     : This function searches the NAT policy table (both      */
/*                    static and dynamic to find if the IP address passed as */
/*                    input is available as the translated IP in the table.  */
/*  Input(s)        : u4IpAddress - Input IP address information.            */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           : None                                       */
/*  Global Variables Referred   : gStaticPolicyNatList,gDynamicPolicyNatList */
/*  Global variables Modified   : None                                       */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
INT1
NatApiSearchPolicyTable (UINT4 u4IpAddress)
{

    if ((NAT_SUCCESS == NatPolicySearchStaticTable (u4IpAddress)) ||
        (NAT_SUCCESS == NatPolicySearchDynamicTable (u4IpAddress)))
    {
        return NAT_SUCCESS;
    }

    return NAT_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : NatApiAllocateMemory                                   */
/*  Description     : This function will allocate memory pool.               */
/*  Input(s)        : u2Noctets:       The number of bytes of memory to be   */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
VOID               *
NatApiAllocateMemory (VOID)
{
    UINT1              *pu1Block = NULL;
    pu1Block = MemAllocMemBlk (NAT_SIP_POOL_ID);
    return (VOID *) pu1Block;
}

/*****************************************************************************/
/*  Function Name   : NatApiAllocateMemory                                   */
/*  Description     : This function will Release memory pool.               */
/*  Input(s)        : noctets(IN):       The number of bytes of memory to be */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  NAT_SUCCESS if the search succeeds.       */
/*                                 NAT_FAILURE if the search fails.          */
/*****************************************************************************/
VOID
NatApiReleaseMemory (VOID *pNoctets)
{
    MemReleaseMemBlock (NAT_SIP_POOL_ID, (UINT1 *) pNoctets);
}

#endif /* _NATAPI_C */
