/********************************************************************
 * Copyright (C) Future Sotware,2002
 * 
 *  $Id: nattrig.c,v 1.8 2014/01/25 13:52:26 siva Exp $  
 *  
 *  Description:This file contains the functionality of port trigger
 *
 ********************************************************************/
#include "natinc.h"
/***********************************************************************/
/*  Function Name : NatGetFreeTrigInfoEntry                            */
/*  Description   : This function gets the index of the free entry     */
/*                : in gaTrigInfo[]                                     */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :Index of the Free Entry In gaTrigInfo[]              */
/***********************************************************************/
PUBLIC UINT1
NatGetFreeTrigInfoEntry (VOID)
{

    UINT1               u1AppCount = NAT_ZERO;

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (gaTrigInfo[u1AppCount].u1Status == NAT_ZERO)
        {
            return (u1AppCount);
        }
    }
    return (u1AppCount);
}

/***********************************************************************/
/*  Function Name : NatGetFreeRsvdTrigInfoEntry                        */
/*  Description   : This function gets the index of the free entry     */
/*                : in gaRsvdTrigInfo[] array                           */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :Index of the free entry in gaRsvdTrigInfo            */
/***********************************************************************/
PUBLIC UINT1
NatGetFreeRsvdTrigInfoEntry (VOID)
{
    UINT1               u1AppCount = NAT_ZERO;

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (gaRsvdTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_FREE)
        {
            return (u1AppCount);
        }
    }
    return (u1AppCount);
}

/***********************************************************************/
/*  Function Name : NatFreeTrigInfoEntry                               */
/*  Description   : This function marks the matching index of          */
/*                : gaTrigInfo[] array  as free                         */
/*                :                                                    */
/*  Input(s)      : pu1AppName :- The name of the application whose    */
/*                : info to be removed from gaTrigInfo[]                */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatFreeTrigInfoEntry (UINT1 *pu1AppName)
{
    UINT1               u1AppCount = NAT_ZERO;
    UINT1               u1RsvdAppCount = NAT_ZERO;

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (STRCMP (pu1AppName, gaTrigInfo[u1AppCount].au1AppName) == NAT_ZERO)
        {
            break;
        }
    }

    if (u1AppCount >= NAT_MAX_APP_ALLOWED)
    {
        return (NAT_FAILURE);
    }

    for (u1RsvdAppCount = NAT_ZERO; u1RsvdAppCount < NAT_MAX_APP_ALLOWED;
         u1RsvdAppCount++)
    {
        if (gaRsvdTrigInfo[u1RsvdAppCount].pTrigInfo ==
            &(gaTrigInfo[u1AppCount]))
        {
            break;
        }
    }

    if (u1RsvdAppCount < NAT_MAX_APP_ALLOWED)
    {

        FL_NAT_DEL_HW_PORTTRIGGER_ENTRIES (&gaRsvdTrigInfo[u1RsvdAppCount]);

        MEMSET (&(gaRsvdTrigInfo[u1RsvdAppCount]), NAT_ZERO,
                sizeof (tRsvdTrigInfo));
    }
    MEMSET (&(gaTrigInfo[u1AppCount]), NAT_ZERO, sizeof (tTriggerInfo));
    return (NAT_SUCCESS);
}

/***********************************************************************/
/*  Function Name : NatFreeRsvdTrigInfoEntry                           */
/*  Description   : This function marks the matching index of          */
/*                : gaRsvdTrigInfo array  as free                       */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/

PUBLIC VOID
NatFreeRsvdTrigInfoEntry (VOID)
{
    UINT1               u1AppCount;
    UINT4               u4CurrTime;

    FL_NAT_GET_PORTTRIG_TIMESTAMP ();

    NAT_GET_SYS_TIME (&u4CurrTime);

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (gaRsvdTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_FREE)
        {
            continue;
        }
        if ((u4CurrTime - gaRsvdTrigInfo[u1AppCount].u4TimeStamp)
            >= gu4NatIdleTimeOut)
        {
            gaRsvdTrigInfo[u1AppCount].pTrigInfo->u1RsvdInfoStatus =
                NAT_TRIG_ENTRY_AVAIL;

            FL_NAT_DEL_HW_PORTTRIGGER_ENTRIES (&gaRsvdTrigInfo[u1AppCount]);

            MEMSET (&(gaRsvdTrigInfo[u1AppCount]), NAT_ZERO,
                    sizeof (tRsvdTrigInfo));
        }
    }
    return;
}

/***********************************************************************/
/*  Function Name : NatAddEntryToTrigInfoList                          */
/*  Description   : This function is invoked by CLI for configuring    */
/*                : port trigger info                                  */
/*                :                                                    */
/*  Input(s)      : pu1AppName :- The Name of the application          */
/*                : pu1OutPortString:- Destination Ports from LAN to   */
/*                : LAN. Format (6000,6010-6030,8000)                  */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatAddEntryToTrigInfoList (UINT1 *pu1AppName, UINT1 *pu1OutPortString,
                           UINT1 *pu1InPortString, UINT2 u2Protocol)
{
    UINT1               u1ArrayIndex = NAT_ZERO;
    tPortRange          aInPortRange[NAT_MAX_PORT_ALLOWED];
    tPortRange          aOutPortRange[NAT_MAX_PORT_ALLOWED];
    UINT2               u2MaxPortVal = NAT_ZERO;

    MEMSET (aInPortRange, NAT_ZERO,
            (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));
    MEMSET (aOutPortRange, NAT_ZERO,
            (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));

    if ((u2Protocol != NAT_TCP) && (u2Protocol != NAT_UDP)
        && (u2Protocol != NAT_PROTO_ANY))
    {
        return (NAT_FAILURE);
    }

    if (NatPortString2PortRange (aOutPortRange, pu1OutPortString,
                                 &u2MaxPortVal) == NAT_FAILURE)
    {
        return (NAT_FAILURE);
    }

    if (NatPortString2PortRange (aInPortRange, pu1InPortString,
                                 &u2MaxPortVal) == NAT_FAILURE)
    {
        return (NAT_FAILURE);
    }

    if (NatCheckDuplicateTriggerPort (pu1AppName, aOutPortRange,
                                      aInPortRange, u2Protocol) == NAT_FAILURE)
    {
        return (NAT_FAILURE);
    }

    u1ArrayIndex = NatGetFreeTrigInfoEntry ();
    if (u1ArrayIndex == NAT_MAX_APP_ALLOWED)
    {
        return (NAT_FAILURE);
    }

    STRNCPY (gaTrigInfo[u1ArrayIndex].au1AppName, pu1AppName,
             NAT_MAX_APP_NAME_LEN - NAT_ONE);
    STRNCPY (gaTrigInfo[u1ArrayIndex].au1OutPortString, pu1OutPortString,
             NAT_MAX_PORT_STRING_LEN - NAT_ONE);
    STRNCPY (gaTrigInfo[u1ArrayIndex].au1InPortString, pu1InPortString,
             NAT_MAX_PORT_STRING_LEN - NAT_ONE);
    MEMCPY (gaTrigInfo[u1ArrayIndex].aOutPortRange, aOutPortRange,
            (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));
    MEMCPY (gaTrigInfo[u1ArrayIndex].aInPortRange, aInPortRange,
            (sizeof (tPortRange) * NAT_MAX_PORT_ALLOWED));
    gaTrigInfo[u1ArrayIndex].u2Protocol = u2Protocol;
    gaTrigInfo[u1ArrayIndex].u1Status = NAT_TRIG_ENTRY_AVAIL;

    (gu4NatNextFreeTranslatedLocPort =
     (gu4NatNextFreeTranslatedLocPort > u2MaxPortVal) ?
     gu4NatNextFreeTranslatedLocPort : (UINT4) (u2MaxPortVal + NAT_ONE));

    return (NAT_SUCCESS);
}

/***********************************************************************/
/*  Function Name : NatAddEntryToRsvdTrigInfoList                      */
/*  Description   : This function is used to add an entry in reserved  */
/*                :                                                    */
/*  Input(s)      : pTrigInfo :- Pointer to trigger info               */
/*                : u4InIpAddr:- Address of the Host on Lan Side       */
/*                : u4OutIpAddr:- Address of the Host on Wan Side      */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatAddEntryToRsvdTrigInfoList (tTriggerInfo * pTrigInfo, UINT4 u4InIpAddr,
                               UINT4 u4OutIpAddr)
{

    UINT1               u1ArrayIndex = NAT_ZERO;

    u1ArrayIndex = NatGetFreeRsvdTrigInfoEntry ();

    if (u1ArrayIndex == NAT_MAX_APP_ALLOWED)
    {
        return (NAT_FAILURE);
    }

    gaRsvdTrigInfo[u1ArrayIndex].pTrigInfo = pTrigInfo;
    gaRsvdTrigInfo[u1ArrayIndex].u4LocalIpAddr = u4InIpAddr;
    gaRsvdTrigInfo[u1ArrayIndex].u4RemoteIpAddr = u4OutIpAddr;
    NAT_GET_SYS_TIME (&(gaRsvdTrigInfo[u1ArrayIndex].u4TimeStamp));
    gaRsvdTrigInfo[u1ArrayIndex].u1Status = NAT_TRIG_ENTRY_AVAIL;
    gaRsvdTrigInfo[u1ArrayIndex].pTrigInfo->u1RsvdInfoStatus =
        NAT_TRIG_ENTRY_RSVD;

    /* Check whether Timer is started or not , if not do it now */
    if (gu4NatTmrEnable == NAT_DISABLE)
    {
        /* START TIMER */
        if (NatStartTimer () == NAT_FAILURE)
        {
            return (NAT_FAILURE);
        }
        gu4NatTmrEnable = NAT_ENABLE;
        NAT_TRC (NAT_TRC_ON, "\n NAT Timer Started \n");
    }
    return (NAT_SUCCESS);

}

/***********************************************************************/
/*  Function Name : NatPortString2PortRange                            */
/*  Description   : This function converts the port in the string      */
/*                : format to range format                             */
/*  Input(s)      : pPortRange:- Pointer to Ports in Range format      */
/*                : pu1PortString:- Pointer to ports in string format  */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatPortString2PortRange (tPortRange * pPortRange, UINT1 *pu1PortString,
                         UINT2 *pu2MaxPortVal)
{
    UINT2               u2Len = NAT_ZERO;
    UINT1               u1Count = NAT_ZERO;
    UINT1               u1PortCount = NAT_ZERO;
    UINT1               u1Flag = NAT_ZERO;

    u2Len = (UINT2) (STRNLEN (pu1PortString, NAT_MAX_PORT_STRING_LEN));

    if (u2Len == NAT_MAX_PORT_STRING_LEN)
    {
        return (NAT_FAILURE);
    }

    do
    {
        if (pu1PortString[u1Count] == '-')
        {
            u1Flag = NAT_ONE;
            pPortRange[u1PortCount].u2EndPort = NAT_ZERO;
            u1Count++;
            continue;
        }

        if ((pu1PortString[u1Count] != ',') && (pu1PortString[u1Count] != '\0'))
        {
            if ((pu1PortString[u1Count] < '0')
                || (pu1PortString[u1Count]) > '9')
            {
                return (NAT_FAILURE);
            }
            if (u1Flag == NAT_ZERO)
            {
                pPortRange[u1PortCount].u2StartPort =
                    (UINT2) ((pPortRange[u1PortCount].u2StartPort *
                              NAT_UNIT_POSITION + (pu1PortString[u1Count] -
                                                   '0')));
                pPortRange[u1PortCount].u2EndPort =
                    pPortRange[u1PortCount].u2StartPort;
            }
            else
            {
                pPortRange[u1PortCount].u2EndPort =
                    (UINT2) ((pPortRange[u1PortCount].u2EndPort *
                              NAT_UNIT_POSITION + (pu1PortString[u1Count] -
                                                   '0')));
            }
        }

        else if ((pu1PortString[u1Count] == ',') ||
                 (pu1PortString[u1Count] == '\0'))
        {
            if (pPortRange[u1PortCount].u2StartPort >
                pPortRange[u1PortCount].u2EndPort)
            {
                return (NAT_FAILURE);
            }

            u1Flag = NAT_ZERO;
            ((*pu2MaxPortVal) = ((*pu2MaxPortVal) >
                                 pPortRange[u1PortCount].u2EndPort) ?
             (*pu2MaxPortVal) : pPortRange[u1PortCount].u2EndPort);
            u1PortCount++;
        }
        u1Count++;
    }
    while (u1Count <= u2Len);

    return (NAT_SUCCESS);
}

/***********************************************************************/
/*  Function Name : NatCheckDuplicateTriggerPort                       */
/*  Description   : This function checks if the Port ranges of two     */
/*                : applications overlap                               */
/*  Input(s)      : pu1AppName:- Pointer to the application name       */
/*                : pOutPortRange:- Pointer to OutPorts in Range       */
/*                : format                                             */
/*                : pInPortRange:- Pointer to InPorts in Range format  */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatCheckDuplicateTriggerPort (UINT1 *pu1AppName, tPortRange * pOutPortRange,
                              tPortRange * pInPortRange, UINT2 u2Protocol)
{

    UINT1               u1AppCount = NAT_ZERO;
    UINT1               u1AppPortCount = NAT_ZERO;
    UINT1               u1PortCount = NAT_ZERO;

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if (STRNCMP (pu1AppName, gaTrigInfo[u1AppCount].au1AppName,
                     NAT_MAX_APP_NAME_LEN) == NAT_ZERO)
        {
            return (NAT_FAILURE);
        }

        if (gaTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_FREE)
        {
            continue;
        }

        for (u1AppPortCount = NAT_ZERO;
             u1AppPortCount < NAT_MAX_PORT_ALLOWED; u1AppPortCount++)
        {
            if ((gaTrigInfo[u1AppCount].aOutPortRange[u1AppPortCount].
                 u2EndPort == NAT_ZERO) &&
                (gaTrigInfo[u1AppCount].aOutPortRange[u1AppPortCount].
                 u2StartPort == NAT_ZERO))
            {
                continue;
            }

            for (u1PortCount = NAT_ZERO;
                 u1PortCount < NAT_MAX_PORT_ALLOWED; u1PortCount++)
            {
                if ((pOutPortRange[u1PortCount].u2StartPort <=
                     gaTrigInfo[u1AppCount].aOutPortRange[u1AppPortCount].
                     u2EndPort) &&
                    (pOutPortRange[u1PortCount].u2EndPort >=
                     gaTrigInfo[u1AppCount].aOutPortRange[u1AppPortCount].
                     u2StartPort))
                {
                    if ((gaTrigInfo[u1AppCount].u2Protocol == NAT_PROTO_ANY)
                        || (gaTrigInfo[u1AppCount].u2Protocol == u2Protocol)
                        || (u2Protocol == NAT_PROTO_ANY))
                    {
                        return (NAT_FAILURE);
                    }
                }
            }
        }

        for (u1AppPortCount = NAT_ZERO;
             u1AppPortCount < NAT_MAX_PORT_ALLOWED; u1AppPortCount++)
        {

            if ((gaTrigInfo[u1AppCount].aInPortRange[u1AppPortCount].
                 u2StartPort == NAT_ZERO) &&
                (gaTrigInfo[u1AppCount].aInPortRange[u1AppPortCount].
                 u2EndPort == NAT_ZERO))
            {
                continue;
            }

            for (u1PortCount = NAT_ZERO;
                 u1PortCount < NAT_MAX_PORT_ALLOWED; u1PortCount++)
            {
                if ((pInPortRange[u1PortCount].u2StartPort <=
                     gaTrigInfo[u1AppCount].aInPortRange[u1AppPortCount].
                     u2EndPort) &&
                    (pInPortRange[u1PortCount].u2EndPort >=
                     gaTrigInfo[u1AppCount].aInPortRange[u1AppPortCount].
                     u2StartPort))
                {
                    if ((gaTrigInfo[u1AppCount].u2Protocol == NAT_PROTO_ANY)
                        || (gaTrigInfo[u1AppCount].u2Protocol == u2Protocol)
                        || (u2Protocol == NAT_PROTO_ANY))
                    {
                        return (NAT_FAILURE);
                    }
                }
            }
        }
    }
    return (NAT_SUCCESS);
}

/***********************************************************************/
/*  Function Name : NatSearchOutBoundRsvdPortTriggerInfo               */
/*  Description   : This function matches the Destination Ports        */
/*                : against OutPort Info or Source ports against the   */
/*                : InPorts of gaRsvdTrigInfo in OutBound Direction     */
/*  Input(s)      : pHeaderInfo:- Pointer to the Header Info           */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatSearchOutBoundRsvdPortTriggerInfo (tHeaderInfo * pHeaderInfo)
{
    UINT1               u1AppCount = NAT_ZERO;
    UINT1               u1PortCount = NAT_ZERO;
    tGlobalInfo         GlobalInfo;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    if (pHeaderInfo->u2InPort == NAT_ZERO)
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    if ((pHeaderInfo->u1PktType != NAT_TCP) &&
        (pHeaderInfo->u1PktType != NAT_UDP))
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if ((gaRsvdTrigInfo[u1AppCount].u1Status !=
             NAT_TRIG_ENTRY_AVAIL) ||
            (gaRsvdTrigInfo[u1AppCount].pTrigInfo == NULL))
        {
            continue;
        }

        for (u1PortCount = NAT_ZERO;
             u1PortCount < NAT_MAX_PORT_ALLOWED; u1PortCount++)
        {
            /*        if (u1AppCount >= NAT_MAX_APP_ALLOWED)
               {
               continue;
               } */
            if ((pHeaderInfo->u2OutPort >=
                 gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                 aOutPortRange[u1PortCount].u2StartPort)
                && (pHeaderInfo->u2OutPort <=
                    gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                    aOutPortRange[u1PortCount].u2EndPort)
                &&
                ((pHeaderInfo->u1PktType ==
                  gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)
                 || (NAT_PROTO_ANY ==
                     gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)))
            {

                if ((pHeaderInfo->u4InIpAddr != gaRsvdTrigInfo[u1AppCount].
                     u4LocalIpAddr) || (pHeaderInfo->u4OutIpAddr !=
                                        gaRsvdTrigInfo[u1AppCount].
                                        u4RemoteIpAddr))
                {
                    return (NAT_FAILURE);
                }

                NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                            NAT_ZERO,
                                            pHeaderInfo->u4OutIpAddr,
                                            pHeaderInfo->u2OutPort,
                                            pHeaderInfo->u4IfNum, &GlobalInfo);

                if ((GlobalInfo.u4TranslatedLocIpAddr == NAT_ZERO)
                    || (GlobalInfo.u2TranslatedLocPort != NAT_ZERO))
                {
                    return (NAT_FAILURE);
                }
                pHeaderInfo->u4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
                NAT_GET_SYS_TIME (&(gaRsvdTrigInfo[u1AppCount].u4TimeStamp));
                return (NAT_SUCCESS);
            }
            else if ((pHeaderInfo->u2InPort >=
                      gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                      aInPortRange[u1PortCount].
                      u2StartPort) &&
                     (pHeaderInfo->u2InPort <=
                      gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                      aInPortRange[u1PortCount].u2EndPort) &&
                     ((pHeaderInfo->u1PktType ==
                       gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)
                      || (NAT_PROTO_ANY ==
                          gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)))
            {

                NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                            NAT_ZERO,
                                            pHeaderInfo->u4OutIpAddr,
                                            pHeaderInfo->u2OutPort,
                                            pHeaderInfo->u4IfNum, &GlobalInfo);

                if ((GlobalInfo.u4TranslatedLocIpAddr == NAT_ZERO)
                    || (GlobalInfo.u2TranslatedLocPort != NAT_ZERO))
                {
                    return (NAT_FAILURE);
                }
                pHeaderInfo->u4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
                NAT_GET_SYS_TIME (&(gaRsvdTrigInfo[u1AppCount].u4TimeStamp));
                return (NAT_SUCCESS);
            }
        }
    }
    return (NAT_PORT_TRIG_PROCEED);
}

/***********************************************************************/
/*  Function Name : NatSearchInBoundRsvdPortTrigInf                */
/*  Description   : This function matches the Destination Ports        */
/*                : against OutPort Info or Source ports against the   */
/*                : InPorts of gaRsvdTrigInfo in InBound Direction      */
/*  Input(s)      : pHeaderInfo:- Pointer to the Header Info           */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatSearchInBoundRsvdPortTrigInf (tHeaderInfo * pHeaderInfo)
{
    UINT1               u1AppCount = NAT_ZERO;
    UINT1               u1PortCount = NAT_ZERO;

    if (pHeaderInfo->u2InPort == NAT_ZERO)
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    if ((pHeaderInfo->u1PktType != NAT_TCP) &&
        (pHeaderInfo->u1PktType != NAT_UDP))
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if ((gaRsvdTrigInfo[u1AppCount].u1Status != NAT_TRIG_ENTRY_AVAIL) ||
            (gaRsvdTrigInfo[u1AppCount].pTrigInfo == NULL))
        {
            continue;
        }

        for (u1PortCount = NAT_ZERO;
             u1PortCount < NAT_MAX_PORT_ALLOWED; u1PortCount++)
        {
            if ((pHeaderInfo->u2InPort >=
                 gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                 aInPortRange[u1PortCount].u2StartPort) &&
                (pHeaderInfo->u2InPort <=
                 gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                 aInPortRange[u1PortCount].u2EndPort) &&
                ((pHeaderInfo->u1PktType ==
                  gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol) ||
                 (NAT_PROTO_ANY ==
                  gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)))
            {
                if (gaRsvdTrigInfo[u1AppCount].u4LocalIpAddr == NAT_ZERO)
                {
                    return (NAT_FAILURE);
                }

                pHeaderInfo->u4InIpAddr =
                    gaRsvdTrigInfo[u1AppCount].u4LocalIpAddr;

                NAT_GET_SYS_TIME (&(gaRsvdTrigInfo[u1AppCount].u4TimeStamp));
                return (NAT_SUCCESS);
            }
            else if ((pHeaderInfo->u2OutPort >=
                      gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                      aOutPortRange[u1PortCount].u2StartPort) &&
                     (pHeaderInfo->u2OutPort <=
                      gaRsvdTrigInfo[u1AppCount].pTrigInfo->
                      aOutPortRange[u1PortCount].u2EndPort) &&
                     ((pHeaderInfo->u1PktType ==
                       gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol) ||
                      (NAT_PROTO_ANY ==
                       gaRsvdTrigInfo[u1AppCount].pTrigInfo->u2Protocol)))
            {
                if (gaRsvdTrigInfo[u1AppCount].u4LocalIpAddr == NAT_ZERO)
                {
                    return (NAT_FAILURE);
                }

                pHeaderInfo->u4InIpAddr =
                    gaRsvdTrigInfo[u1AppCount].u4LocalIpAddr;

                NAT_GET_SYS_TIME (&(gaRsvdTrigInfo[u1AppCount].u4TimeStamp));
                return (NAT_SUCCESS);
            }
        }
    }

    return (NAT_PORT_TRIG_PROCEED);
}

/***********************************************************************/
/*  Function Name : NatSearchOutBoundPortTrigInf                   */
/*  Description   : This function matches the Destination Ports        */
/*                : against OutPort Info or Source ports against the   */
/*                : InPorts of gaTrigInfo in OutBound Direction.        */
/*                : Moves the matching entry to gaRsvdTrigInfo[]        */
/*  Input(s)      : pHeaderInfo:- Pointer to the Header Info           */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/
PUBLIC UINT1
NatSearchOutBoundPortTrigInf (tHeaderInfo * pHeaderInfo)
{
    UINT1               u1AppCount = NAT_ZERO;
    UINT1               u1PortCount = NAT_ZERO;
    tGlobalInfo         GlobalInfo;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));

    if (pHeaderInfo->u2InPort == NAT_ZERO)
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    if ((pHeaderInfo->u1PktType != NAT_TCP) &&
        (pHeaderInfo->u1PktType != NAT_UDP))
    {
        return (NAT_PORT_TRIG_PROCEED);
    }

    for (u1AppCount = NAT_ZERO; u1AppCount < NAT_MAX_APP_ALLOWED; u1AppCount++)
    {
        if ((gaTrigInfo[u1AppCount].u1Status == NAT_TRIG_ENTRY_FREE) ||
            (gaTrigInfo[u1AppCount].u1RsvdInfoStatus == NAT_TRIG_ENTRY_RSVD))
        {
            continue;
        }

        for (u1PortCount = NAT_ZERO;
             u1PortCount < NAT_MAX_PORT_ALLOWED; u1PortCount++)
        {
            if ((pHeaderInfo->u2OutPort >=
                 gaTrigInfo[u1AppCount].aOutPortRange[u1PortCount].
                 u2StartPort) &&
                (pHeaderInfo->u2OutPort <=
                 gaTrigInfo[u1AppCount].aOutPortRange[u1PortCount].u2EndPort) &&
                ((pHeaderInfo->u1PktType == gaTrigInfo[u1AppCount].u2Protocol)
                 || (NAT_PROTO_ANY == gaTrigInfo[u1AppCount].u2Protocol)))
            {
                NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                            NAT_ZERO,
                                            pHeaderInfo->u4OutIpAddr,
                                            pHeaderInfo->u2OutPort,
                                            pHeaderInfo->u4IfNum, &GlobalInfo);

                if ((GlobalInfo.u4TranslatedLocIpAddr == NAT_ZERO)
                    || (GlobalInfo.u2TranslatedLocPort != NAT_ZERO))
                {
                    return (NAT_FAILURE);
                }

                if (NatAddEntryToRsvdTrigInfoList (&(gaTrigInfo[u1AppCount]),
                                                   pHeaderInfo->
                                                   u4InIpAddr,
                                                   pHeaderInfo->u4OutIpAddr)
                    == NAT_FAILURE)
                {
                    return (NAT_FAILURE);
                }

                pHeaderInfo->u4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
                return (NAT_SUCCESS);
            }
        }
    }
    return (NAT_PORT_TRIG_PROCEED);
}

/***********************************************************************/
/*  Function Name : NatPortTrigInfoLookUp                              */
/*  Description   : This function does a look up in the gaTrigInfo[]    */
/*                : and aRsvdInfoEntry[]                               */
/*  Input(s)      : pHeaderInfo:- Pointer to the Header Info           */
/*                : pBuf :- Pointer the data                           */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :NAT_SUCCESS or NAT_FAILURE                          */
/***********************************************************************/

PUBLIC UINT4
NatPortTrigInfoLookUp (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Status = NAT_ZERO;

    if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
    {
        u4Status = NatSearchOutBoundRsvdPortTriggerInfo (pHeaderInfo);
        if (u4Status == NAT_SUCCESS)
        {
            NatIpHeaderModify (pBuf, pHeaderInfo);
            return (NAT_SUCCESS);
        }
        else if (u4Status == NAT_PORT_TRIG_PROCEED)
        {
            u4Status = NatSearchOutBoundPortTrigInf (pHeaderInfo);
            if (u4Status == NAT_SUCCESS)
            {
                NatIpHeaderModify (pBuf, pHeaderInfo);
                return (NAT_SUCCESS);
            }
        }
        return (u4Status);
    }
    else if (pHeaderInfo->u4Direction == NAT_INBOUND)
    {
        u4Status = NatSearchInBoundRsvdPortTrigInf (pHeaderInfo);
        if (u4Status == NAT_SUCCESS)
        {
            NatIpHeaderModify (pBuf, pHeaderInfo);
            return (NAT_SUCCESS);
        }
        return (u4Status);
    }
    else
    {
        return (NAT_FAILURE);
    }
}

/************************************************************************/
/*  Function Name : NatUtilGetGlobalArrayIndex */
/*  Description   : This function does a look up in the global          */
/*                  gaTrigInfo[] and on the basis of trigger table index */
 /*                 and returns the global array index                  */
/*  Input(s)      : Table Index (1) - pInPortStr                        */
/*                : Table index (2) - pOutPortStr                       */
/*                : Table Index (3) - u2Protocol                        */
/*  Output(s)     : Global Array Index                                  */
/*  Return        : Array Index on SUCCESS or -1 on FAILURE             */
/************************************************************************/
PUBLIC UINT4
NatUtilGetGlobalArrayIndex (UINT1 *pu1InBoundPortStr,
                            UINT1 *pu1OutBoundPortStr, UINT2 u2Protocol)
{
    INT4                i4ArrayIndex = NAT_ZERO;
    INT4                i4NotFound = NAT_NOT_FOUND;

    for (i4ArrayIndex = NAT_ZERO;
         i4ArrayIndex < NAT_MAX_APP_ALLOWED; i4ArrayIndex++)
    {
        if ((MEMCMP
             (pu1InBoundPortStr, gaTrigInfo[i4ArrayIndex].au1InPortString,
              NAT_MAX_PORT_STRING_LEN) == NAT_ZERO)
            &&
            (MEMCMP
             (pu1OutBoundPortStr, gaTrigInfo[i4ArrayIndex].au1OutPortString,
              NAT_MAX_PORT_STRING_LEN) == NAT_ZERO)
            && (u2Protocol == gaTrigInfo[i4ArrayIndex].u2Protocol))
        {
            return ((UINT4) i4ArrayIndex);
        }
    }
    return ((UINT4) i4NotFound);
}
