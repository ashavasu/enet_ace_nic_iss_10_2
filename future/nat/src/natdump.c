/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natdump.c,v 1.2 2010/11/23 04:23:58 siva Exp $
 *
 * Description: This file contains dump packet routine(s)
 *
 *******************************************************************/
#ifndef _NATDMP_C
#define _NATDMP_C
#include "natinc.h"

PUBLIC void
DumpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Length)
{
    UINT4               u4Count;
    UINT1               u1Char;
    UINT1               u1Str[NAT_DUMP_BUF_SIZE];
    UINT1              *pu1Temp;
    UINT4               u4Len;

    u4Count = 0;
    pu1Temp = u1Str;

    while (u4Count < u4Length)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, &u1Char, u4Count, 1);
        u4Len = SNPRINTF ((char *) pu1Temp, NAT_DUMP_BUF_SIZE, "%x ", u1Char);
        pu1Temp += u4Len;
        u4Count++;

        if ((u4Count % 16) == 0)
        {
            u4Len = SNPRINTF ((char *) pu1Temp, NAT_DUMP_BUF_SIZE, "\n");
            pu1Temp += u4Len;
            UtlTrcPrint (u1Str);
            pu1Temp = u1Str;
        }
    }
    u4Len = SNPRINTF ((char *) pu1Temp, NAT_DUMP_BUF_SIZE, "\n");
    pu1Temp += u4Len;
    UtlTrcPrint (u1Str);
}
#endif /* _NATDMP_C */
