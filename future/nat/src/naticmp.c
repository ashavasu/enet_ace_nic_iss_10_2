/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: naticmp.c,v 1.10 2015/10/10 11:03:42 siva Exp $
 *
 * Description: This file contains functions required for modifying
 *              ICMP packet
 *
 *******************************************************************/
#ifndef _NATICMP_C
#define _NATICMP_C
#include "natinc.h"
/********************************************************************************          FUNCTIONS PROTOTYPE USED BY THE ICMP    MODULE
*******************************************************************************/

PRIVATE INT4        NatIcmpQueryProcess
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction, UINT4 u4IcmpOffset,
           UINT4 u4IfNum, UINT4 u4Type));
PRIVATE UINT1 NatFindIcmpType ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf));
PRIVATE UINT2       NatGetIcmpQueryIdentifier
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset));
PRIVATE UINT4       NatGetIcmpQueryCode
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset));
PRIVATE UINT1       NatGetIcmpQueryTypeNumber
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset));
PRIVATE INT4        NatModifyIcmpErrorPayload
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction, UINT4 u4Offset,
           UINT4 u4IfNum));

/***************************************************************************
* Function Name    :  NatModifyIcmpErrorPayload
* Description    :  This function modifies the ICMP header and also the
*                   embedded IP header information in the ICMP error packet.
*
* Input (s) : 1. pBuf - This contains the IP header along with ICMP
*                       packet.
*             2. u4Direction - This gives the direction of the packet.
*             3. u4IfNum - This gives the outside interface number on
*             which the packet is travelling.
*             4. u4IcmpOffset - Gives the start offset of the ICMP
*             payload
*
* Output (s)    :  Modified pBuf
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE INT4
NatModifyIcmpErrorPayload (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction,
                           UINT4 u4IcmpOffset, UINT4 u4IfNum)
{
    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    tHeaderInfo         HeaderInfo;
    UINT4               u4EmbedPktDir = NAT_ZERO;
    INT4                i4Status = NAT_ZERO;
    UINT2               u2ChkSum = NAT_ZERO;
    UINT2               u2OldIpChsum = NAT_ZERO;
    UINT2               u2NewIpChsum = NAT_ZERO;
    UINT2               u2OldTransChsum = NAT_ZERO;
    UINT2               u2NewTransChsum = NAT_ZERO;
    UINT2               u2OldPort = NAT_ZERO;
    UINT4               u4OldIpAddr = NAT_ZERO;
    UINT4               u4NewIpAddr = NAT_ZERO;
    UINT4               u4IcmpType = NAT_ZERO;
    UINT4               u4IcmpCode = NAT_ZERO;
    UINT2               u2IcmpChksum = NAT_ZERO;
    UINT2               u2OldIdentifier = NAT_ZERO;
    UINT2               u2NewPort = NAT_ZERO;
    UINT2               u2Flag = NAT_ZERO;
    UINT4               u4PktLen = NAT_ZERO;
    INT4                i4RetVal = NAT_ZERO;
    u2Flag = TRUE;

    MEMSET (&HeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    /*
     * This function modifies the payload in the ICMP error pkt. 
     * The following fields get changed - IP addr in IP hdr,
     * The Port in UDP/TCP hdr and Cksum at both the headers
     *
     */
    i4Status = NAT_FAILURE;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatModifyIcmpErrorPayload \n");

    /* To avoid fragmentation of invalid length packets */
    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Check for valid length of ICMP error packet
     * Outer IP header + ICMP header (8 bytes) +
     * Inner IP header + TCP/UDP/ICMP Header (Minimum 8 bytes)
     */

    if (u4PktLen
        < (u4IcmpOffset + NAT_EIGHT + NAT_IP_HEADER_LENGTH + NAT_EIGHT))
    {
        return NAT_FAILURE;
    }

    /* Fragment the packet into ICMP header and payload */
    i4RetVal =
        CRU_BUF_Fragment_BufChain (pBuf, u4IcmpOffset + NAT_EIGHT, &pRestBuf);
    UNUSED_PARAM (i4RetVal);

    if (pRestBuf == NULL)
    {
        return NAT_FAILURE;
    }

    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pRestBuf);

    if (u4PktLen < NAT_IP_HEADER_LENGTH + NAT_EIGHT)
    {
        CRU_BUF_Release_MsgBufChain (pRestBuf, FALSE);
        return NAT_FAILURE;
    }

    if (u4Direction == NAT_INBOUND)
    {
        u4EmbedPktDir = NAT_OUTBOUND;
    }
    else
    {
        u4EmbedPktDir = NAT_INBOUND;
    }

    /* Search in the dynamic table using the payload header information */
    NatInitHeaderInfo (pRestBuf, &HeaderInfo, u4EmbedPktDir, u4IfNum);
    HeaderInfo.u4Direction = u4Direction;
    if ((HeaderInfo.u2InPort == NAT_NFS) ||
        (HeaderInfo.u2OutPort == NAT_NFS) ||
        (HeaderInfo.u2InPort == NAT_RPC) ||
        (HeaderInfo.u2OutPort == NAT_RPC) ||
        (HeaderInfo.u2InPort == NAT_PM) || (HeaderInfo.u2OutPort == NAT_PM))
    {
        /*What about the other ports related to NFS??? */
        i4Status = (INT4) NatSearchNfsTable (&HeaderInfo);
    }
    else
    {
        /* 
         * added the follwing lines for this reason-
         * If the packet type is ICMP then get the Inside
         * Port(ICMP ID) and Outside Port.
         */
        if (HeaderInfo.u1PktType == NAT_ICMP)
        {
            /*inside port is the ICMP ID */
            HeaderInfo.u2InPort = NatGetIcmpQueryIdentifier
                (pRestBuf, (UINT4) HeaderInfo.u1IpHeadLen);
            u2OldIdentifier = HeaderInfo.u2InPort;

            /*outside port is calculated using ICMP Type and Code */
            u4IcmpType = (UINT4) NatGetIcmpQueryTypeNumber
                (pRestBuf, (UINT4) HeaderInfo.u1IpHeadLen);
            u4IcmpCode = NatGetIcmpQueryCode (pRestBuf,
                                              (UINT4) HeaderInfo.u1IpHeadLen);
            HeaderInfo.u2OutPort
                = (UINT2) ((u4IcmpType << NAT_EIGHT) + u4IcmpCode);
        }
        i4Status = (INT4) NatGetDynamicEntry (&HeaderInfo, NAT_SEARCH);
    }
    if (NAT_SUCCESS == i4Status)
    {
        /* Modify the embedded header */
        /* the direction of the embedded payload is opposite to that of the ICMP
         * pkt */
        HeaderInfo.u4Direction = u4EmbedPktDir;
        CRU_BUF_Copy_FromBufChain (pRestBuf, (UINT1 *) &u2OldIpChsum,
                                   NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
        u2OldIpChsum = OSIX_NTOHS (u2OldIpChsum);
        /*
         * getting the Transport hdr chksum in the payload before it 
         * is being modified
         * Note--> Protocol Specific
         */
        switch (HeaderInfo.u1PktType)
        {
            case NAT_ICMP:
                CRU_BUF_Copy_FromBufChain (pRestBuf,
                                           (UINT1 *) &u2OldTransChsum,
                                           (UINT4) HeaderInfo.u1IpHeadLen +
                                           NAT_WORD_LEN, NAT_WORD_LEN);
                break;

            case NAT_TCP:
                if (u4PktLen >= (UINT4) (HeaderInfo.u1IpHeadLen +
                                         NAT_TCP_CKSUM_OFFSET + NAT_WORD_LEN))
                {
                    CRU_BUF_Copy_FromBufChain (pRestBuf,
                                               (UINT1 *) &u2OldTransChsum,
                                               (UINT4) HeaderInfo.u1IpHeadLen +
                                               NAT_TCP_CKSUM_OFFSET,
                                               NAT_WORD_LEN);
                }
                else
                {
                    u2Flag = FALSE;
                }
                break;

            case NAT_UDP:
                CRU_BUF_Copy_FromBufChain (pRestBuf,
                                           (UINT1 *) &u2OldTransChsum,
                                           (UINT4) HeaderInfo.u1IpHeadLen +
                                           NAT_UDP_CKSUM_OFFSET, NAT_WORD_LEN);
                break;
            default:
                /*
                 * Not being handled for now
                 */
                break;
        }
        u2OldTransChsum = OSIX_NTOHS (u2OldTransChsum);

        /*Modify IP header in the payload */
        NatIpHeaderModify (pRestBuf, &HeaderInfo);
        CRU_BUF_Copy_FromBufChain (pRestBuf,
                                   (UINT1 *) &u2NewIpChsum,
                                   NAT_IP_CHKSUM_OFFSET, NAT_WORD_LEN);
        u2NewIpChsum = OSIX_NTOHS (u2NewIpChsum);
        /*
         * Copy the Port in the Payload part according to the direction of the
         * payload packet
         */

        /*
         * change the header at the transport layer according to the
         *  type of transport header.
         *  Fields changed - Inside Port, Checksum.
         *  Packet types supported- UDP, TCP, ICMP.
         */

        switch (HeaderInfo.u1PktType)
        {
            case NAT_UDP:
                NatUdpHeaderModify (pRestBuf, &HeaderInfo);
                CRU_BUF_Copy_FromBufChain (pRestBuf,
                                           (UINT1 *) &u2NewTransChsum,
                                           (UINT4) HeaderInfo.u1IpHeadLen +
                                           NAT_UDP_CKSUM_OFFSET, NAT_WORD_LEN);
                break;

            case NAT_TCP:
                /*
                   if(TCP header should lie in the payload )
                 */
                NatTcpHeaderModify (pRestBuf, &HeaderInfo);
                if (u2Flag == TRUE)
                {
                    CRU_BUF_Copy_FromBufChain (pRestBuf,
                                               (UINT1 *) &u2NewTransChsum,
                                               (UINT4) HeaderInfo.u1IpHeadLen +
                                               NAT_TCP_CKSUM_OFFSET,
                                               NAT_WORD_LEN);
                }
                CRU_BUF_Copy_FromBufChain (pRestBuf,
                                           (UINT1 *) &u2NewTransChsum,
                                           (UINT4) HeaderInfo.u1IpHeadLen +
                                           NAT_TCP_CKSUM_OFFSET, NAT_WORD_LEN);
                break;

            case NAT_ICMP:
                u2NewTransChsum = u2OldTransChsum;
                NatChecksumAdjust ((UINT1 *) &u2IcmpChksum,
                                   (UINT1 *) &u2OldIdentifier,
                                   NAT_WORD_LEN,
                                   (UINT1 *)
                                   &(HeaderInfo.u2InPort), NAT_WORD_LEN);

                /* The Inside Port for ICMP is the ICMP-Id */
                u2NewPort = OSIX_HTONS (HeaderInfo.u2InPort);
                CRU_BUF_Copy_OverBufChain (pRestBuf,
                                           (UINT1 *) &u2NewPort,
                                           (UINT4) HeaderInfo.u1IpHeadLen
                                           + NAT_ICMP_QUERY_ID_OFFSET,
                                           NAT_WORD_LEN);

                u2NewTransChsum = OSIX_HTONS (u2NewTransChsum);
                CRU_BUF_Copy_OverBufChain (pRestBuf,
                                           (UINT1 *) &u2NewTransChsum,
                                           (UINT4) HeaderInfo.u1IpHeadLen
                                           + NAT_WORD_LEN, NAT_WORD_LEN);
                break;
            default:
                /*
                 * Not supported for now
                 */
                break;

        }
        u2NewTransChsum = OSIX_NTOHS (u2NewTransChsum);

        /* join the ICMP hdr and the payload */
        CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);

        /* Adjusting the ICMP header Checksum */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ChkSum,
                                   ICMP_CHK_OFF_22, NAT_TWO_BYTES);
        u2ChkSum = OSIX_NTOHS (u2ChkSum);
        /*
         * getting the old and the new IP address and Port */
        if (u4EmbedPktDir == NAT_OUTBOUND)
        {
            u4OldIpAddr = (HeaderInfo.pDynamicEntry->u4TranslatedLocIpAddr);
            u2OldPort = (HeaderInfo.pDynamicEntry->u2TranslatedLocPort);
        }
        else
        {
            u4OldIpAddr = (HeaderInfo.pDynamicEntry->u4LocIpAddr);
            u2OldPort = (HeaderInfo.pDynamicEntry->u2LocPort);
        }
        u4NewIpAddr = (HeaderInfo.u4InIpAddr);
        u2NewPort = (HeaderInfo.u2InPort);
        /* Adjust the ICMP Checksum according to the change in the
         * IP address in the payload IP header, Checksum in the payload 
         * IP header, Checksum in the payload Transport header, Port in
         *  the Payload Transport header
         */
        /*ALARM !!!!! - ICMP chkcum not adjusted according to the change in the
         * Port value at the payload Transport Layer or ICMP Id */
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) &u4OldIpAddr,
                           NAT_IP_ADDR_LEN, (UINT1 *) &u4NewIpAddr,
                           NAT_IP_ADDR_LEN);
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) &u2OldIpChsum,
                           NAT_WORD_LEN, (UINT1 *) &u2NewIpChsum, NAT_WORD_LEN);
        /*
         * Adjust the ICMP Checksum according to the change in the
         * Checksum in the "Payload Transport header", 
         * Port in the Payload Transport header.
         * Note - Here it is only done for UDP ,TCP and ICMP.
         */
        if ((HeaderInfo.u1PktType == NAT_UDP)
            || (HeaderInfo.u1PktType == NAT_TCP)
            || (HeaderInfo.u1PktType == NAT_ICMP))
        {
            NatChecksumAdjust ((UINT1 *) &u2ChkSum,
                               (UINT1 *) &u2OldPort, NAT_WORD_LEN,
                               (UINT1 *) &u2NewPort, NAT_WORD_LEN);
            if (u2Flag == TRUE)
            {
                NatChecksumAdjust ((UINT1 *) &u2ChkSum,
                                   (UINT1 *) &u2OldTransChsum, NAT_WORD_LEN,
                                   (UINT1 *) &u2NewTransChsum, NAT_WORD_LEN);
            }
        }

        u2ChkSum = OSIX_HTONS (u2ChkSum);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2ChkSum,
                                   u4IcmpOffset + NAT_TWO, NAT_WORD_LEN);
        /*Adjust of ICMP header ChkSum ends here */

        /*Modify the IP header of the ICMP error packet */
        if (NAT_INBOUND == u4Direction)
        {
            HeaderInfo.u4OutIpAddr = NatGetSrcIpAddr (pBuf);
        }
        HeaderInfo.u4Direction = u4Direction;
        /* Modify the IP header */
        HeaderInfo.u1PktType = NAT_ICMP;
        NatIpHeaderModify (pBuf, &HeaderInfo);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pRestBuf, FALSE);
    }
    NAT_TRC (NAT_TRC_ON, "\n Entering NatModifyIcmpErrorPayload \n");
    return (i4Status);
}

/***************************************************************************
* Function Name    :  NatFindIcmpType
* Description    :  This function finds whether the ICMP packet is an error
*                   or query packet.
*
* Input (s)    :  pBuf - Contains IP header along with ICMP payload.
*
* Output (s)    :  None
* Returns      :  ICMP Type
*
****************************************************************************/

PRIVATE UINT1
NatFindIcmpType (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1IcmpType = NAT_ZERO;
    UINT1               u1HeaderOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFindIcmpType \n");

    u1HeaderOffset = (UINT1) (NatGetIpHeaderLength (pBuf));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1IcmpType, u1HeaderOffset,
                               NAT_ONE_BYTE);

    switch (u1IcmpType)
    {

        case NAT_ZERO:
            u1IcmpType = NAT_ICMP_ECHO_REPLY;
            break;
        case NAT_ICMP_TYPE_ECHO:
            u1IcmpType = NAT_ICMP_ECHO_REQUEST;
            break;
        case NAT_ICMP_TYPE_TIMESTAMP:
        case NAT_ICMP_TYPE_TIMESTAMP_REPLY:
        case NAT_ICMP_TYPE_INFO_REQUEST:
        case NAT_ICMP_TYPE_INFO_REPLY:
        case NAT_ICMP_TYPE_ADDR_MASK_REQ:
        case NAT_ICMP_TYPE_ADDR_MASK_REPLY:
            u1IcmpType = NAT_ICMP_QUERY_MSG;
            break;
        default:
            u1IcmpType = NAT_ICMP_ERROR_MSG;
            break;
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFindIcmpType \n");
    return (u1IcmpType);
}

/***************************************************************************
* Function Name    :  NatGetIcmpQueryIdentifier
* Description    :  This function extracts the ICMP query ID from the ICMP
*             header.
*
* Input (s)    :  1. pBuf - Contains IP header along with ICMP payload.
*             2. u4IcmpOffset - Gives the start offset of the ICMP
*             payload
*
* Output (s)    :  None
* Returns      :  ICMP query Identifier
*
****************************************************************************/

PRIVATE UINT2
NatGetIcmpQueryIdentifier (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset)
{
    UINT2               u2IcmpQueryId = NAT_ZERO;

    u2IcmpQueryId = NAT_ZERO;
    u4IcmpOffset += NAT_ICMP_QUERY_ID_OFFSET;
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatGetIcmpQueryIdentifier \n");
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2IcmpQueryId,
                               u4IcmpOffset, NAT_WORD_LEN);
    NAT_DBG1 (NAT_DBG_ICMP, "\n ICMP query ID is %d \n", u2IcmpQueryId);

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatGetIcmpQueryIdentifier \n");
    return (OSIX_NTOHS (u2IcmpQueryId));
}

/***************************************************************************
* Function Name    :  NatGetIcmpQueryTypeNumber
* Description    :  This function extracts the ICMP Type from the ICMP
*             header.
*
* Input (s)    :  1. pBuf - Contains IP header along with ICMP payload.
*             2. u4IcmpOffset - Gives the start offset of the ICMP
*             payload
*
* Output (s)    :  None
* Returns      :  ICMP Type Number
*
****************************************************************************/

PRIVATE UINT1
NatGetIcmpQueryTypeNumber (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset)
{

    UINT1               u1IcmpQueryTypeNum = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatGetIcmpQueryTypeNumber \n");
    u1IcmpQueryTypeNum = NAT_ZERO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1IcmpQueryTypeNum,
                               u4IcmpOffset, NAT_ONE_BYTE);
    NAT_DBG1 (NAT_DBG_ICMP, "\nICMP query type number is %d\n",
              u1IcmpQueryTypeNum);

    /* Matching the reply type to its respective query type number */
    switch (u1IcmpQueryTypeNum)
    {
        case NAT_ZERO:
            u1IcmpQueryTypeNum = NAT_ICMP_TYPE_ECHO;
            break;
        case NAT_ICMP_TYPE_TIMESTAMP_REPLY:
            u1IcmpQueryTypeNum = NAT_ICMP_TYPE_TIMESTAMP;
            break;
        case NAT_ICMP_TYPE_INFO_REPLY:
            u1IcmpQueryTypeNum = NAT_ICMP_TYPE_INFO_REQUEST;
            break;
        case NAT_ICMP_TYPE_ADDR_MASK_REPLY:
            u1IcmpQueryTypeNum = NAT_ICMP_TYPE_ADDR_MASK_REQ;
            break;
        default:
            break;
    }

    NAT_DBG (NAT_DBG_ENTRY, "\n Exiting NatGetIcmpQueryTypeNumber \n");
    return (u1IcmpQueryTypeNum);
}

/***************************************************************************
* Function Name    :  NatGetIcmpQueryCode
* Description    :  This function extracts the ICMP code from the ICMP
*             header.
*
* Input (s)    :  1. pBuf - Contains IP header along with ICMP payload.
*             2. u4IcmpOffset - Gives the start offset of the ICMP
*             payload
*
* Output (s)    :  None
* Returns      :  ICMP query code
*
****************************************************************************/

PRIVATE UINT4
NatGetIcmpQueryCode (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IcmpOffset)
{

    UINT4               u4IcmpQueryCode = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatGetIcmpQueryCode \n");
    u4IcmpQueryCode = NAT_ZERO;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4IcmpQueryCode,
                               u4IcmpOffset + NAT_ICMP_QUERY_CODE_OFFSET,
                               NAT_ONE_BYTE);
    NAT_DBG1 (NAT_DBG_ICMP, "\n ICMP query code is %d \n", u4IcmpQueryCode);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetIcmpQueryCode \n");
    return (u4IcmpQueryCode);
}

/***************************************************************************
* Function Name    :  NatIcmpQueryProcess
* Description    :  This function process the ICMP query packet. In this
*                   packet only the ICMP header information is  to be
*                   modified.
*
* Input (s)    :  1. pBuf - Contains IP header along with ICMP query
*             payload.
*             2. u4Direction - gives the direction of the packet.
*             3. u4IcmpOffset - gives the start offset of the ICMP
*             query packet.
*             4. u4IfNum - Interface number.
*             5. u4Type - ICMP type - ECHO or REPLY
* Output (s)    :  Modified pBuf
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE INT4
NatIcmpQueryProcess (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction,
                     UINT4 u4IcmpOffset, UINT4 u4IfNum, UINT4 u4Type)
{
    tHeaderInfo         HeaderInfo;
	INT4                i4SearchStatus = NAT_FAILURE;
	INT4                i4HeaderStatus = NAT_FAILURE;
	INT4                i4SearchDynEntry   = NAT_FAILURE;
	INT4                i4PolicyStatus   = NAT_FAILURE;

    UINT4               u4IcmpType = NAT_ZERO;
    UINT4               u4IntfIpAddr = NAT_ZERO;
    UINT4               u4IcmpCode = NAT_ZERO;
    UINT4               u4StaticIpAddr = NAT_ZERO;
    UINT4               u4PolicyIpAddr = NAT_ZERO;
    UINT4               u4DestIp = NAT_ZERO;
    UINT2               u2OutPort	= NAT_ZERO;
    UINT2               u2OldIdentifier = NAT_ZERO;
    UINT2               u2IcmpChksum = NAT_ZERO;


    tStaticNaptEntry   *pStaticNaptEntry = NULL; 
    NAT_TRC (NAT_TRC_ON, "\n Inside NatIcmpQueryProcess");

    i4SearchStatus = NAT_FAILURE;

    /* Extract the identifier from packet and assign it as local port */
    HeaderInfo.u2InPort = NatGetIcmpQueryIdentifier (pBuf, u4IcmpOffset);
    u2OldIdentifier = OSIX_HTONS (HeaderInfo.u2InPort);
    /*
     * obtain the Query Type and Code, concat them and assign it as outside
     * port.
     */
    u4IcmpType = (UINT4) NatGetIcmpQueryTypeNumber (pBuf, u4IcmpOffset);
    u4IcmpCode = NatGetIcmpQueryCode (pBuf, u4IcmpOffset);
    HeaderInfo.u4IfNum = u4IfNum;
    HeaderInfo.u2OutPort = (UINT2) ((u4IcmpType << NAT_EIGHT) + u4IcmpCode);

    /* Extract the IP addresses corresponding to the direction */
    if (NAT_OUTBOUND == u4Direction)
    {
        HeaderInfo.u4InIpAddr = NatGetSrcIpAddr (pBuf);
        HeaderInfo.u4OutIpAddr = NatGetDestIpAddr (pBuf);
    }
    else
    {
        HeaderInfo.u4InIpAddr = NatGetDestIpAddr (pBuf);
        HeaderInfo.u4OutIpAddr = NatGetSrcIpAddr (pBuf);
    }
    HeaderInfo.u4Direction = u4Direction;
    HeaderInfo.u1PktType = NAT_ICMP;
    HeaderInfo.i4Delta = NAT_ZERO;

    if (CfaGetIfIpAddr ((INT4) u4IfNum, &u4IntfIpAddr) == OSIX_FAILURE)
    {
        return NAT_FAILURE;
    }
    if (u4Direction == NAT_OUTBOUND)
    {
        if ((u4Type == NAT_ICMP_ECHO_REPLY) &&
            ((i4SearchStatus =
              (INT4) NatGetDynamicEntry (&HeaderInfo,
                                         NAT_SEARCH)) == NAT_FAILURE))
        {
            return NAT_FAILURE;
        }
        else if (i4SearchStatus == NAT_SUCCESS)
        {
            i4HeaderStatus = NAT_SUCCESS;
        }
        /* if the request is going from switch itself,
         * dont translate it. */
        if (u4Type == NAT_ICMP_ECHO_REQUEST)
        {
            if (HeaderInfo.u4InIpAddr == u4IntfIpAddr)
            {
                return NAT_SUCCESS;
            }
        }
    }
    else
    {
        /* if the ping reply is to switch, dont translate it 
         * we can identify whether the reply, is to switch,
         * if there is no current translation entry and the
         * destination is to switch*/
        if (HeaderInfo.u4InIpAddr == u4IntfIpAddr)
        {
            if ((u4Type == NAT_ICMP_ECHO_REPLY) &&
                ((i4SearchStatus =
                  (INT4) NatGetDynamicEntry (&HeaderInfo,
                                             NAT_SEARCH)) == NAT_FAILURE))
            {
                return NAT_SUCCESS;
            }
            else if (i4SearchStatus == NAT_SUCCESS)
            {
                /* if Entry was there, NatGetDynamicEntry will update the 
                 * header and hence we should avoid calling the same
                 * function again */
                i4HeaderStatus = NAT_SUCCESS;
            }
        }
    }

    /*
     * Search in the Dynamic table. If entry is present update the headerInfo
     * else create a new entry.
     */
    if (i4HeaderStatus == NAT_FAILURE)
    {
             /* When WAN host pings to unreachable translated IP address for which already 
              * a dynamic entry is present ,then allow the ping and reply without any NAT translation.
              * If there is no dynamic entry present for the DestIp (i.e translated IP) , and 
              * if there is no static entry in the stitic table / the NAT policy table/NAPT table,
              * then return without NAT translation and let the IP module take care of the ping request.
              * If there is a static entry/Static NAT policy /NAPT entry , then the traffic should be allowed to be translated.
              *
              * This case  was observed in the following scenario:
              * 1. Create a static NAT translation entry with a non-existing IP Address (but in the network of the WAN interface IP)
              * 2. Ping from LAN to WAN and see that dynamic entry is created
              * 3. Now ping from WAN to the non-existing IP address. 
              ** This packet should be returned without NAT translation.
              *
              *
              */

	    if ((u4Direction == NAT_INBOUND) && (u4Type == NAT_ICMP_ECHO_REQUEST))
	    {
		    i4SearchDynEntry =  (INT4) NatGetDynamicEntry (&HeaderInfo,NAT_SEARCH);
		    if (i4SearchDynEntry == NAT_FAILURE)
		    {
			    u4StaticIpAddr = NatSearchStaticTable (HeaderInfo.u4InIpAddr,
					    u4Direction,
					    u4IfNum);
			    i4PolicyStatus = NatPolicyGetStaticEntry (&u4PolicyIpAddr,HeaderInfo.u4InIpAddr,
					    HeaderInfo.u4OutIpAddr,HeaderInfo.u2OutPort,u4Direction);
                
                UNUSED_PARAM (i4PolicyStatus);

			    pStaticNaptEntry = NatGetStaticNaptEntry (&u4DestIp, &u2OutPort,
					    u4Direction,u4IfNum ,NAT_TCP);

			    if((u4PolicyIpAddr == NAT_ZERO) && (u4StaticIpAddr == NAT_ZERO) &&(pStaticNaptEntry == NULL))
				    return NAT_SUCCESS; 
		    }
	    }
        if (i4SearchDynEntry != NAT_SUCCESS)
        {
              i4SearchStatus = (INT4) NatGetDynamicEntry (&HeaderInfo, NAT_CREATE);
        }
    }
    if ((i4SearchStatus == NAT_SUCCESS)  || (i4SearchDynEntry == NAT_SUCCESS))
    {
        /* Modify ICMP Id and do the  Checksum Adjustment */
        HeaderInfo.u2InPort = OSIX_HTONS (HeaderInfo.u2InPort);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &(HeaderInfo.u2InPort),
                                   u4IcmpOffset + NAT_ICMP_QUERY_ID_OFFSET,
                                   NAT_WORD_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2IcmpChksum,
                                   u4IcmpOffset + NAT_WORD_LEN, NAT_WORD_LEN);
        NatChecksumAdjust ((UINT1 *) &u2IcmpChksum,
                           (UINT1 *) &u2OldIdentifier, NAT_WORD_LEN,
                           (UINT1 *) &(HeaderInfo.u2InPort), NAT_WORD_LEN);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2IcmpChksum,
                                   u4IcmpOffset + NAT_WORD_LEN, NAT_WORD_LEN);
        /* Modify the IP header */
        NatIpHeaderModify (pBuf, &HeaderInfo);
        i4SearchStatus = NAT_SUCCESS;

    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatIcmpQueryProcess \n");
    return (i4SearchStatus);
}

/***************************************************************************
* Function Name    :  NatProcessIcmp
* Description    :  This function process ICMP error and query messages types
*                   and does the required modification.
*
* Input (s)    :  1. pBuf - This contains the IP header along with ICMP
*             packet.
*             2. u4Direction - This gives the direction of the packet.
*             3. u4IfNum - This gives the outside interface number on
*             which the packet is travelling.
*
* Output (s)    :  Modified pBuf
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC INT4
NatProcessIcmp (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Direction, UINT4 u4IfNum)
{
    UINT4               u4IcmpType = NAT_ZERO;
    INT4                i4Status = NAT_ZERO;
    UINT4               u4IcmpOffset = NAT_ZERO;
    UINT4               u4IntfIpAddr = NAT_ZERO;
    UINT4               u4DestIpAddr = NAT_ZERO;
    UINT4               u4SrcIpAddr = NAT_ZERO;

    /*
     * this function finds the type of ICMP packet ( Query packet or Error
     * message packet) and process the packet accordingly.
     */
    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessIcmp.");

    u4IcmpOffset = u4IcmpType = NAT_ZERO;

    i4Status = NAT_SUCCESS;
    /* Get the ICMP payload start offset */
    u4IcmpOffset = NatGetIpHeaderLength (pBuf);

    /* Get the Type of the ICMP packet whether it is error or query message */
    u4IcmpType = (UINT4) NatFindIcmpType (pBuf);

    /* DSL_ADD -Start */

    if (u4IcmpType == NAT_ICMP_ECHO_REQUEST)
    {
        /* PING ECHO Request from the Global network to Router interface 
         * should be allowed without any translations.
         */

        u4DestIpAddr = NatGetDestIpAddr (pBuf);
        if (CfaGetIfIpAddr ((INT4) u4IfNum, &u4IntfIpAddr) == OSIX_FAILURE)
        {
            return NAT_FAILURE;
        }
        if (u4DestIpAddr == u4IntfIpAddr)
        {
            return NAT_SUCCESS;
        }
    }
    else if (u4IcmpType == NAT_ICMP_ECHO_REPLY)
    {
        /* PING ECHO Reply from the Router will be 
         * allowed without translations 
         */

        u4SrcIpAddr = NatGetSrcIpAddr (pBuf);
        if (CfaGetIfIpAddr ((INT4) u4IfNum, &u4IntfIpAddr) == OSIX_FAILURE)
        {
            return NAT_FAILURE;
        }
        if (u4SrcIpAddr == u4IntfIpAddr)
        {
            return NAT_SUCCESS;
        }
    }

    /* Treat the ping like a query message if it is echo request / reply */
    if ((u4IcmpType == NAT_ICMP_ECHO_REQUEST) ||
        (u4IcmpType == NAT_ICMP_ECHO_REPLY))
    {
        /*Its a ICMP query packet */
        NAT_TRC (NAT_TRC_ON, "\n Inside ICMP query message packetInside.");
        i4Status =
            NatIcmpQueryProcess (pBuf, u4Direction, u4IcmpOffset, u4IfNum,
                                 u4IcmpType);
    }
    else
    {
        /* Its a ICMP error packet. */
        NAT_TRC (NAT_TRC_ON, "\n ICMP error packet. ");

        i4Status =
            NatModifyIcmpErrorPayload (pBuf, u4Direction, u4IcmpOffset,
                                       u4IfNum);
    }

    if (i4Status == NAT_FAILURE)
    {
        NAT_TRC (NAT_TRC_ON, "\n NatProcessIcmp Failure. ");

        i4Status = NAT_FAILURE;
    }
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessIcmp \n");
    return (i4Status);
}
#endif /* _NATICMP_C */
