/********************************************************************
 * Copyright (C) Future Sotware,2002
 * 
 *  $Id: natrtsp.c,v 1.8 2013/10/25 10:52:27 siva Exp $ 
 *  
 *  Description:This file contains functions required for
 *             modifying the rtsp and pna payload if required.
 *
 ********************************************************************/
#include "natinc.h"

PRIVATE INT4        NatProcessSmediaPktInOut
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE INT4        NatProcessRTSPPktOut
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pTcpBuf,
           tHeaderInfo * pHeaderInfo, UINT4 u4DataLen,
           CONST CHR1 * pSearchStr));
PRIVATE INT4        NatProcessPNAPktOut
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pPnaBuf,
           tHeaderInfo * pHeaderInfo, UINT4 u4PktLen));
PRIVATE INT4        NatProcessPNAPktIn
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pPnaBuf,
           tHeaderInfo * pHeaderInfo, UINT4 u4PktLen));

/**************************************************************************
* Name           :  NatProcessSmediaPkt 
* Description    : Processes the RTSP control packets 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated RTSP control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PUBLIC INT4
NatProcessSmediaPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    if (NatProcessSmediaPktInOut (pBuf, pHeaderInfo) == NAT_FAILURE)
    {
        return NAT_FAILURE;
    }

    return NAT_SUCCESS;
}

/**************************************************************************
* Name           : NatProcessSmediaPktInOut 
* Description    : Processes the RTSP control packets moving from Local 
*                  network to Global network.It translates the SETUP or 
*                  PNA command packet coming from RTSP clients in the Local
*                  network and 200.OK command from RTSP server in the 
*                  Global network.
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated RTSP control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
NatProcessSmediaPktInOut (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHeaderInfo * pHeaderInfo)
{
    UINT4               u4PktLen = NAT_ZERO;
    UINT4               u4PnaOffset = NAT_ZERO;
    UINT4               u4RtspOffset = NAT_ZERO;
    INT4                i4Rtsp200Offset = NAT_ZERO;
    INT4                i4RtspOKOffset = NAT_ZERO;
    INT4                i4Status = NAT_SUCCESS;
    UINT4               u4PnaDataLen = NAT_ZERO;
    UINT4               u4RtspDataLen = NAT_ZERO;
    tLinearBuf         *pLinearBuf = NULL;
    UINT1              *pTcpRtspPkt = NULL;

    NAT_TRC2 (NAT_TRC_ON, "\n Smedia: Translating Out Pkt. Port"
              "#(In/Out): %u/%u \n",
              pHeaderInfo->u2InPort, pHeaderInfo->u2OutPort);

    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    u4PnaOffset = pHeaderInfo->u1IpHeadLen;
    u4PnaDataLen = u4PktLen - u4PnaOffset;

    u4RtspOffset =
        (UINT4) pHeaderInfo->u1IpHeadLen +
        (UINT4) pHeaderInfo->u1TransportHeadLen;
    u4RtspDataLen = u4PktLen - u4RtspOffset;

    if (u4PktLen <= u4RtspOffset)
    {
        NAT_TRC (NAT_TRC_ON, "\n Smedia : Invalid Pkt Len, No Translation");
        return NAT_SUCCESS;
    }

    /*pLinearBuf = (UINT1 *) NAT_MALLOC (u4PnaDataLen * sizeof (UINT1)); */
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)
    {
        NAT_TRC (NAT_TRC_ON, "\n Smedia: Buffer allocation failure");
        return NAT_FAILURE;
    }
    /* Contains payload of RTSP packet */
    pTcpRtspPkt = (UINT1 *) pLinearBuf + pHeaderInfo->u1TransportHeadLen;

    /* copying the TCP packet (including the TCP hdr) into a linear buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pLinearBuf, u4PnaOffset,
                               u4PnaDataLen);

    if ((pHeaderInfo->u2OutPort == NAT_RTSP_PORT) ||
        (pHeaderInfo->u2OutPort == NAT_PNA_PORT))
    {
        if (u4RtspDataLen >= STRLEN ("SETUP"))
        {
            /* Rtsp server is in the Global Network */
            NAT_TRC (NAT_TRC_ON, "\n Smedia: Server is Inside Global Network");

            /* Process SETUP command */
            if (MEMCMP (pTcpRtspPkt, "SETUP", STRLEN ("SETUP")) == NAT_ZERO)
            {
                NAT_TRC (NAT_TRC_ON, "\n RTSP: Processing SETUP Msg");

                /* got the RTSP packet with the SETUP command. */
                i4Status = NatProcessRTSPPktOut (pBuf, pTcpRtspPkt, pHeaderInfo,
                                                 u4RtspDataLen, "client_port");
            }
        }

        if (u4RtspDataLen >= STRLEN ("PNA"))
        {
            /* Process PNA command */
            if (MEMCMP (pTcpRtspPkt, "PNA", STRLEN ("PNA")) == NAT_ZERO)
            {
                NAT_TRC (NAT_TRC_ON, "\n PNA: Processing Out Pkt");

                i4Status =
                    NatProcessPNAPktOut (pBuf, (UINT1 *) pLinearBuf,
                                         pHeaderInfo, u4PktLen);
            }
        }
    }
    else if ((pHeaderInfo->u2InPort == NAT_RTSP_PORT) ||
             (pHeaderInfo->u2InPort == NAT_PNA_PORT))
    {
        NAT_TRC (NAT_TRC_ON, "\n Smedia: Server is Inside Local Network");

        /* Processing PNA packet for creating outbound flow information */
        if (u4RtspDataLen >= STRLEN ("PNA"))
        {
            /* Process PNA command */
            if (MEMCMP (pTcpRtspPkt, "PNA", STRLEN ("PNA")) == NAT_ZERO)
            {
                NAT_TRC (NAT_TRC_ON, "\n PNA: Processing In Pkt");

                i4Status =
                    NatProcessPNAPktIn (pBuf, (UINT1 *) pLinearBuf, pHeaderInfo,
                                        u4PktLen);
            }
        }

        if (u4RtspDataLen >= STRLEN ("200"))
        {
            /* Rtsp server is in the Local Network */
            /* Process  the "200...OK" command. */

            /* the function below is defined in natcom.c */
            i4Rtsp200Offset = NatSearchString (pTcpRtspPkt, u4RtspDataLen,
                                               "200");
            if (i4Rtsp200Offset >= NAT_ZERO)
            {
                i4RtspOKOffset = NatSearchString ((pTcpRtspPkt +
                                                   i4Rtsp200Offset),
                                                  (u4RtspDataLen -
                                                   (UINT4) i4Rtsp200Offset),
                                                  "OK");
                if (i4RtspOKOffset >= NAT_ZERO)
                {
                    NAT_TRC (NAT_TRC_ON, "\n RTSP: Processing 200 OK Msg");
                    i4Status = NatProcessRTSPPktOut (pBuf, pTcpRtspPkt,
                                                     pHeaderInfo, u4RtspDataLen,
                                                     "server_port");
                }
            }
        }
    }

    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
    return (i4Status);
}

/**************************************************************************
* Name           : NatProcessRTSPPktOut 
* Description    : Processes the RTSP control packets. This function  
*                  searches for the string pSearchStr, then it takes 
*                  the ports given in the packet and translates them to
*                  a new value and replaces them in the packet. 
* 
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated RTSP control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
* ****************************************************************************/

PRIVATE INT4
NatProcessRTSPPktOut (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pTcpBuf,
                      tHeaderInfo * pHeaderInfo, UINT4 u4DataLen,
                      CONST CHR1 * pSearchStr)
{
    tCRU_BUF_CHAIN_HEADER *pRestBuf = NULL;
    tHeaderInfo         NewHeaderInfo;
    CONST CHR1         *pTrPortStr = "Transport:";
    UINT1              *pPortData = NULL;
    UINT1              *pTransPort = NULL;
    UINT4               u4State = NAT_ZERO;
    INT4                i4Offset = NAT_ZERO;
    UINT4               u4PortDataLen = NAT_ZERO;
    UINT4               u4OrigPortDataLen = NAT_ZERO;
    UINT4               u4NumOfPort = NAT_ZERO;
    INT4                i4SessionCreated = NAT_FALSE;
    UINT4               u4ByteCount = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    INT4                i4Status = NAT_SUCCESS;
    INT4                i4Delta = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;
    INT4                i4PktFragOffset = NAT_ZERO;
    UINT2               u2TransBasePort = NAT_ZERO;
    UINT2               u2LocBasePort = NAT_ZERO;
    UINT2               u2StartPort = NAT_ZERO;
    UINT2               u2EndPort = NAT_ZERO;
    UINT2               u2Chksum = NAT_ZERO;
    UINT2               au2Port[NAT_RTSP_PORT_GROUP] = { NAT_ZERO };
    tLinearBuf         *pLinearBuf = NULL;
    INT4                i4OrigDataOffset = NAT_ZERO;
    CHR1                aTempPortBuf[NAT_MAX_LEN];

    /* search for transport string */
    i4Offset = NatSearchString (pTcpBuf, u4DataLen, pTrPortStr);

    /* no transport string found, hence return */
    if (i4Offset < NAT_ZERO)
    {
        NAT_TRC (NAT_TRC_ON, "\n RTSP: Transport string not found");
        NAT_TRC (NAT_TRC_ON, "\n RTSP: Out Pkt Translation Failure");
        return NAT_SUCCESS;
    }

    /* save the offset where pkt needs to be fragmented */
    i4PktFragOffset = pHeaderInfo->u1IpHeadLen +
        pHeaderInfo->u1TransportHeadLen + i4Offset;

    if ((i4PktFragOffset & NAT_OFFSET_FRAG /*0x1 */ ) != NAT_ZERO)
    {
        i4PktFragOffset--;
        i4Offset--;
    }

    /* till transport string, no change to be done in the payload */
    pTransPort = pTcpBuf + i4Offset;

    /* get the remaining data size after transport str */
    u4PortDataLen = u4DataLen - (UINT4) i4Offset;

    /* preserve the length before change in the payload */
    u4OrigPortDataLen = u4PortDataLen;
    i4OrigDataOffset = i4Offset;

    /*pLinearBuf = (UINT1 *) NAT_MALLOC (u4DataLen * sizeof (UINT1)); */
    if (NAT_MEM_ALLOCATE
        (NAT_LINEAR_BUF_POOL_ID, pLinearBuf, tLinearBuf) == NULL)

    {
        NAT_TRC (NAT_TRC_ON, "\n RTSP: Buffer allocation failure");
        return NAT_FAILURE;
    }

    pPortData = (UINT1 *) pLinearBuf;

    /* seach and change ports */
    while (u4PortDataLen > STRLEN (pSearchStr))
    {
        i4Offset = NatSearchString (pTransPort, u4PortDataLen, pSearchStr);

        if (i4Offset < NAT_ZERO)
        {
            NAT_TRC (NAT_TRC_ON, "\n RTSP: Server/Client string not found");
            NAT_TRC (NAT_TRC_ON, "\n RTSP: Out Pkt Translation Failure");
            break;
        }

        /* copy client/server str to tmp buff */
        MEMCPY (pPortData, pTransPort, i4Offset + NAT_ONE);
        pPortData += (i4Offset + NAT_ONE);
        pTransPort += i4Offset;

        /* initialise the params used in the port search */
        au2Port[NAT_INDEX_0] = au2Port[NAT_INDEX_1] = NAT_ZERO;
        u2StartPort = u2EndPort = NAT_ZERO;
        u4State = NAT_ZERO;

        for (u4ByteCount = (UINT4) i4Offset; u4ByteCount < u4PortDataLen;
             u4ByteCount++)
        {
            switch (u4State)
            {
                case NAT_ZERO:
                    if (*pTransPort == '=')
                    {
                        u4State++;
                    }
                    break;
                case NAT_ONE:
                    if (ISDIGIT (*pTransPort) != NAT_ZERO)
                    {
                        au2Port[NAT_INDEX_0] =
                            (UINT2) (au2Port[NAT_INDEX_0] *
                                     NAT_TEN + *pTransPort - '0');
                    }
                    else
                    {
                        if (*pTransPort == ';')
                        {
                            u4State = NAT_THREE;
                        }
                        else if (*pTransPort == '-')
                        {
                            u4State++;
                        }
                        else if (*pTransPort == NAT_RTSP_0D)
                        {
                            if (*(pTransPort + NAT_ONE) == NAT_RTSP_0A)
                            {
                                u4State = NAT_THREE;
                            }
                        }
                    }
                    break;

                case NAT_TWO:
                    if (ISDIGIT (*pTransPort) != NAT_ZERO)
                    {
                        au2Port[NAT_INDEX_1] =
                            (UINT2) (au2Port[NAT_INDEX_1] *
                                     NAT_TEN + *pTransPort - '0');
                    }
                    else
                    {
                        u4State++;
                    }
                    break;

                case NAT_THREE:
                    u2LocBasePort = au2Port[NAT_INDEX_0];
                    u2StartPort = au2Port[NAT_INDEX_0];
                    u2EndPort = au2Port[NAT_INDEX_1];

                    if (i4SessionCreated == NAT_FALSE)
                    {
                        i4SessionCreated = NAT_TRUE;

                        /* Adding the information in the NatPartialLinksList */
                        NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
                        NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
                        NewHeaderInfo.u4Direction =
                            (pHeaderInfo->u4Direction == NAT_OUTBOUND) ?
                            NAT_INBOUND : NAT_OUTBOUND;
                        NewHeaderInfo.u1PktType = NAT_UDP;

                        NewHeaderInfo.u4InIpAddr =
                            pHeaderInfo->pDynamicEntry->u4LocIpAddr,
                            NewHeaderInfo.u2InPort = u2LocBasePort;

                        u4ConnectionStatus =
                            NatSearchListAndTable (&NewHeaderInfo,
                                                   &u4TransIpAddr,
                                                   &u2TransBasePort);

                        if (NAT_FAILURE == u4ConnectionStatus)
                        {
                            /* if Napt is enabled use trans. 
                             * port else use the Local port */
                            if (gapNatIfTable
                                [pHeaderInfo->u4IfNum]->u1NaptEnable
                                == NAT_ENABLE)
                            {
                                /* the function below is defined in natcom.c */
                                if (NatGetFreeGlobalPortGroup
                                    (&u2TransBasePort,
                                     NAT_RTSP_PORT_GROUP,
                                     PARITY_EVEN, BIND_TYPE_MEDIA)
                                    == NAT_FAILURE)
                                {

                                    NatMemReleaseMemBlock
                                        (NAT_LINEAR_BUF_POOL_ID,
                                         (UINT1 *) pLinearBuf);
                                    return NAT_FAILURE;
                                }
                            }
                            else
                            {
                                u2TransBasePort = u2StartPort;
                            }

                            for (u4NumOfPort = NAT_ZERO;
                                 u4NumOfPort < NAT_RTSP_PORT_GROUP;
                                 u4NumOfPort++)
                            {
                                if (NewHeaderInfo.u4Direction == NAT_INBOUND)
                                {
                                    NewHeaderInfo.u4InIpAddr =
                                        pHeaderInfo->u4InIpAddr;
                                    NewHeaderInfo.u2InPort =
                                        (UINT2) (u2TransBasePort + u4NumOfPort);
                                }
                                NatAddPartialLinksList (&NewHeaderInfo,
                                                        pHeaderInfo->
                                                        pDynamicEntry->
                                                        u4LocIpAddr,
                                                        (UINT2) (u2LocBasePort +
                                                                 u4NumOfPort),
                                                        NAT_NON_PERSISTENT);
                                NAT_TRC2 (NAT_TRC_ON,
                                          "\nRTSP: New entry added to Tmp list."
                                          "Port #(In/Out): %u/%u \n",
                                          NewHeaderInfo.u2InPort,
                                          NewHeaderInfo.u2OutPort);

                            }
                        }
                    }

                    if (gapNatIfTable[pHeaderInfo->u4IfNum]->u1NaptEnable
                        != NAT_ENABLE)
                    {
                        /* NAPT not enabled, no port change in payload */

                        NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                               (UINT1 *) pLinearBuf);
                        return (NAT_SUCCESS);
                    }

                    /* NAPT is enabled, change the local ports in the payload */
                    if (u2TransBasePort != u2LocBasePort)
                    {

                        if (u2EndPort == NAT_ZERO)
                        {
                            /* copy the translated port */
                            SPRINTF (aTempPortBuf, "%d", u2TransBasePort);
                        }
                        else
                        {
                            /* copy the translated port */
                            SPRINTF (aTempPortBuf,
                                     "%d-%d", u2TransBasePort,
                                     (u2TransBasePort + NAT_RTSP_PORT_GROUP -
                                      NAT_ONE));
                        }
                        MEMCPY (pPortData, aTempPortBuf, STRLEN (aTempPortBuf));
                        pPortData += STRLEN (aTempPortBuf);

                        /* end the port range with ; */
                        *pPortData = ';';
                        pPortData++;

                    }
                    u4State++;
                    break;
                default:
                    break;
            }

            /* if parsing complete, exit from the loop */
            if (u4State > NAT_THREE)
            {
                break;
            }

            /* increment pkt pointer */
            pTransPort++;
        }

        /* update current data offset */
        u4PortDataLen -= u4ByteCount;

    }

    /* copy the remaining data to tmp buff */
    MEMCPY (pPortData, pTransPort, u4PortDataLen);
    pPortData += u4PortDataLen;
    *pPortData = '\0';

    /* Get change in length for seq and Ack adjustment */
    i4NewLen = (INT4) (pPortData - (UINT1 *) pLinearBuf);
    i4Delta = i4NewLen - (INT4) u4OrigPortDataLen;
    pHeaderInfo->i4Delta = i4Delta;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Chksum,
                               (NAT_TCP_CKSUM_OFFSET +
                                (UINT4) pHeaderInfo->u1IpHeadLen),
                               NAT_WORD_LEN);

    /* adjust TCP check sum */
    NatChecksumAdjust ((UINT1 *) &u2Chksum,
                       (pTcpBuf + i4OrigDataOffset), (INT4) u4OrigPortDataLen,
                       (UINT1 *) pLinearBuf, i4NewLen);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Chksum,
                               (NAT_TCP_CKSUM_OFFSET +
                                (UINT4) pHeaderInfo->u1IpHeadLen),
                               NAT_WORD_LEN);

    /* Adding the new ports in the Buffer to the packet pBuf */
    if (i4Delta > NAT_ZERO)
    {
        if (CRU_BUF_Fragment_BufChain (pBuf, (UINT4) i4PktFragOffset, &pRestBuf)
            == CRU_FAILURE)
        {

            NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                   (UINT1 *) pLinearBuf);
            return NAT_FAILURE;
        }
        CRU_BUF_Release_MsgBufChain (pRestBuf, TRUE);

        pRestBuf = CRU_BUF_Allocate_MsgBufChain ((UINT4) i4NewLen, NAT_ZERO);
        if (pRestBuf == NULL)
        {

            NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID,
                                   (UINT1 *) pLinearBuf);
            return NAT_FAILURE;
        }

        CRU_BUF_Copy_OverBufChain (pRestBuf, (UINT1 *) pLinearBuf,
                                   NAT_ZERO, (UINT4) i4NewLen);
        CRU_BUF_Concat_MsgBufChains (pBuf, pRestBuf);
    }
    else
    {
        /* If size is same or less, copy over the buffer chain */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pLinearBuf,
                                   (UINT4) i4PktFragOffset, (UINT4) i4NewLen);
    }

    NAT_TRC (NAT_TRC_ON, "\n RTSP: Outbound Pkt Translation success");

    NatMemReleaseMemBlock (NAT_LINEAR_BUF_POOL_ID, (UINT1 *) pLinearBuf);
    return (i4Status);

}

/**************************************************************************
* Name           : NatProcessPNAPktOut 
* Description    : Processes the PNA control packets 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated PNA control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
*****************************************************************************/

PRIVATE INT4
NatProcessPNAPktOut (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pPnaBuf,
                     tHeaderInfo * pHeaderInfo, UINT4 u4PktLen)
{
    tGlobalInfo         GlobalInfo;
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4PnaOffset = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT2               u2MsgId = NAT_ZERO;
    UINT2               u2MsgLen = NAT_ZERO;
    UINT2               u2LocalPort = NAT_ZERO;
    UINT2               u2NewPort = NAT_ZERO;
    UINT2               u2OldPort = NAT_ZERO;
    UINT2               u2TransportCksum = NAT_ZERO;
    INT1                i1PktChanged = NAT_FALSE;
    UINT1              *pTempBuf = NULL;

    MEMSET (&NewHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    u4PnaOffset = (UINT4) (pHeaderInfo->u1IpHeadLen);
    pTempBuf = pPnaBuf + pHeaderInfo->u1TransportHeadLen;
    pTempBuf += NAT_IP_ADDR_LEN + NAT_ONE;

    /* get all the ports and replace them */
    while ((pTempBuf + NAT_IP_ADDR_LEN) < (pPnaBuf + (u4PktLen - u4PnaOffset)))
    {

        MEMCPY (&u2MsgId, pTempBuf, NAT_WORD_LEN);
        u2MsgId = OSIX_NTOHS (u2MsgId);

        if (u2MsgId == NAT_ZERO)
        {
            NAT_TRC (NAT_TRC_ON, "\n PNA: Msg Id 0, No Translation ");
            break;
        }

        pTempBuf += NAT_WORD_LEN;
        MEMCPY (&u2MsgLen, pTempBuf, NAT_WORD_LEN);

        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        pTempBuf += NAT_WORD_LEN;

        if ((u2MsgId == NAT_ONE) || (u2MsgId == NAT_SEVEN))
        {
            MEMCPY (&u2LocalPort, pTempBuf, NAT_WORD_LEN);
            u2OldPort = u2LocalPort;
            u2LocalPort = OSIX_NTOHS (u2LocalPort);

            /* the function below is defined in natcom.c */
            NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
            NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
            NewHeaderInfo.u4Direction = NAT_INBOUND;
            NewHeaderInfo.u1PktType = NAT_UDP;
            NewHeaderInfo.u4InIpAddr =
                pHeaderInfo->pDynamicEntry->u4LocIpAddr,
                NewHeaderInfo.u2InPort = u2LocalPort;

            u4ConnectionStatus =
                NatSearchListAndTable (&NewHeaderInfo,
                                       &u4TransIpAddr, &u2NewPort);

            if (u4ConnectionStatus == NAT_FAILURE)
            {
                NatGetNextFreeGlobalIpPort (pHeaderInfo->pDynamicEntry->
                                            u4LocIpAddr,
                                            NewHeaderInfo.u2InPort,
                                            NewHeaderInfo.u4OutIpAddr,
                                            NewHeaderInfo.u2OutPort,
                                            pHeaderInfo->u4IfNum, &GlobalInfo);
                NewHeaderInfo.u4InIpAddr = pHeaderInfo->u4InIpAddr;
                NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;

                if (GlobalInfo.u2TranslatedLocPort != NAT_ZERO)
                {
                    NewHeaderInfo.u2InPort = GlobalInfo.u2TranslatedLocPort;
                }
                else
                {
                    NewHeaderInfo.u2InPort = u2LocalPort;
                }

                NatAddPartialLinksList (&NewHeaderInfo,
                                        pHeaderInfo->pDynamicEntry->u4LocIpAddr,
                                        u2LocalPort, NAT_NON_PERSISTENT);
                NAT_TRC2 (NAT_TRC_ON, "\n PNA: New entry added to Tmp list."
                          " Port #(In/Out): %u/%u \n",
                          NewHeaderInfo.u2InPort, NewHeaderInfo.u2OutPort);
            }
            else
            {
                NewHeaderInfo.u2InPort = u2NewPort;
                NewHeaderInfo.u4InIpAddr = u4TransIpAddr;
            }
            u2NewPort = OSIX_HTONS (NewHeaderInfo.u2InPort);

            MEMCPY (pTempBuf, &(u2NewPort), NAT_WORD_LEN);
            i1PktChanged = NAT_TRUE;
        }
        pTempBuf += (UINT4) u2MsgLen;
    }

    if (i1PktChanged == NAT_TRUE)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, pPnaBuf, u4PnaOffset,
                                   (u4PktLen - u4PnaOffset));
        /* Recalculating checksum fully if packet is changed */
        u2TransportCksum = NAT_ZERO;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   (UINT4) pHeaderInfo->u1IpHeadLen +
                                   NAT_TCP_CKSUM_OFFSET, NAT_WORD_LEN);
        u2TransportCksum = NatTransportCksumAdjust (pBuf, pHeaderInfo);
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TransportCksum,
                                   (UINT4) pHeaderInfo->u1IpHeadLen +
                                   NAT_TCP_CKSUM_OFFSET, NAT_WORD_LEN);
    }

    NAT_TRC (NAT_TRC_ON, "\n PNA: Outbound Pkt Translation success");
    UNUSED_PARAM (u2OldPort);
    return NAT_SUCCESS;
}

/**************************************************************************
* Name           : NatProcessPNAPktIn 
* Description    : Processes the PNA control packets 
* 
*
* Input (s)   : pBuf - pointer to the packet.
*               pHeaderInfo - pointer to the structure containing information
*                             about the IP and the TCP header.
*
* Output (s)  : The translated PNA control packets.
*
* Returns     : NAT_SUCCESS if the packet is to be forwarded.
*               NAT_FAILURE if the packet is to be dropped.
*
*****************************************************************************/

PRIVATE INT4
NatProcessPNAPktIn (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pPnaBuf,
                    tHeaderInfo * pHeaderInfo, UINT4 u4PktLen)
{
    tHeaderInfo         NewHeaderInfo;
    UINT4               u4PnaOffset = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    UINT4               u4TransIpAddr = NAT_ZERO;
    UINT2               u2MsgId = NAT_ZERO;
    UINT2               u2MsgLen = NAT_ZERO;
    UINT2               u2RemotePort = NAT_ZERO;
    UINT2               u2NewPort = NAT_ZERO;
    UINT2               u2OldPort = NAT_ZERO;
    UINT1              *pTempBuf = NULL;

    UNUSED_PARAM (pBuf);

    MEMSET (&NewHeaderInfo, NAT_ZERO, sizeof (tHeaderInfo));
    u4PnaOffset = (UINT4) (pHeaderInfo->u1IpHeadLen);
    pTempBuf = pPnaBuf + pHeaderInfo->u1TransportHeadLen;
    pTempBuf += NAT_IP_ADDR_LEN + NAT_ONE;

    /* get all the ports and replace them */
    while ((pTempBuf + NAT_IP_ADDR_LEN) < (pPnaBuf + (u4PktLen - u4PnaOffset)))
    {

        MEMCPY (&u2MsgId, pTempBuf, NAT_WORD_LEN);
        u2MsgId = OSIX_NTOHS (u2MsgId);

        if (u2MsgId == NAT_ZERO)
        {
            NAT_TRC (NAT_TRC_ON, "\n PNA: Msg Id 0, No Translation ");
            break;
        }

        pTempBuf += NAT_WORD_LEN;
        MEMCPY (&u2MsgLen, pTempBuf, NAT_WORD_LEN);

        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        pTempBuf += NAT_WORD_LEN;

        if ((u2MsgId == NAT_ONE) || (u2MsgId == NAT_SEVEN))
        {
            MEMCPY (&u2RemotePort, pTempBuf, NAT_WORD_LEN);
            u2OldPort = u2RemotePort;
            u2RemotePort = OSIX_NTOHS (u2RemotePort);

            /* the function below is defined in natcom.c */
            NewHeaderInfo.u4IfNum = pHeaderInfo->u4IfNum;
            NewHeaderInfo.u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
            NewHeaderInfo.u4Direction = NAT_OUTBOUND;
            NewHeaderInfo.u1PktType = NAT_UDP;
            NewHeaderInfo.u4InIpAddr = pHeaderInfo->u4InIpAddr,
                u4ConnectionStatus =
                NatSearchListAndTable (&NewHeaderInfo,
                                       &u4TransIpAddr, &u2NewPort);

            if (u4ConnectionStatus == NAT_FAILURE)
            {
                NatAddPartialLinksList (&NewHeaderInfo,
                                        pHeaderInfo->u4OutIpAddr,
                                        u2RemotePort, NAT_NON_PERSISTENT);
                NAT_TRC2 (NAT_TRC_ON, "\n PNA: New entry added to Tmp list."
                          " Port #(In/Out): %u/%u \n",
                          NewHeaderInfo.u2InPort, NewHeaderInfo.u2OutPort);
            }
            else
            {
                NewHeaderInfo.u2InPort = u2NewPort;
                NewHeaderInfo.u4InIpAddr = u4TransIpAddr;
            }
        }
        if ((pTempBuf + u2MsgLen) < (pPnaBuf + (u4PktLen - u4PnaOffset)))
        {
            pTempBuf += (UINT4) u2MsgLen;
        }
        else
        {
            break;
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n PNA: InBound Pkt Translation success");
    UNUSED_PARAM (u2OldPort);
    return NAT_SUCCESS;
}
