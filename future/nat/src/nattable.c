
/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: nattable.c,v 1.32 2016/03/31 10:53:44 siva Exp $
 *
 * Description:This file contains functions for Nat Table handling
 *             i.e. searching,creating, adding and deleting.
 *
 *******************************************************************/
#ifndef _NATTABLE_C
#define _NATTABLE_C
#include "natinc.h"
static UINT4        gu4NatNextFreeIid;    /* Contains next free IID */
static UINT4        gu4NatNextFreeOid;    /* Contains next free OID */
static UINT4        gu4NatIcmpNewIden;    /* Contains the global identifier 
                                           to be assigned to an ICMP query 
                                           message */
static UINT4        gu4NatPptpNewIden;    /* Contains the global identifier 
                                           to be assigned to a PPTP pkt */
extern UINT4        gu4NatPptpOutCreateFlag;
/* This contains the start of the Translated Local Port */
UINT4               gu4NatInitTranslatedLocPort;
tTMO_HASH_TABLE    *gpNatLocHashList;    /* Pointer to the NatLocHashTable */
tTMO_HASH_TABLE    *gpNatOutHashList;    /* Pointer to the NatOutHashTable */
tTMO_HASH_TABLE    *gpNatGlobalHashList;    /* Pointer to the NatGlobalHashTable */
tTMO_HASH_TABLE    *gpNatOnlyHashList;    /* Pointer to the NatOnlyHashList */
tTMO_HASH_TABLE    *gpNatWanUaHashTbl;    /* Used to store IP,BasePort Passed
                                           During OpenPinHoles */
/* Contains pointer to Dynamic Entries */
/* *INDENT-OFF* */
tTMO_SLL           
    gaNatIidOidArray[NAT_IID_OID_ARRAY_LIMIT][NAT_IID_OID_ARRAY_LIMIT];
/* *INDENT-ON* */
tTMO_SLL            gNatIidFreeList;    /* Maintains freed IID */
tTMO_SLL            gNatOidFreeList;    /* Maintains freed OID */
tTMO_SLL            gNatDnsList;    /* Pointer to DNS Table */
tTMO_SLL            gNatNfsList;    /* Pointer to NFS Table */
tTMO_SLL            gNatNfsIpList;    /* Pointer to NFS IP Table */
tTMO_SLL            gNatUdpList;    /* Pointer to UDP List */
tTMO_SLL            gNatDynamicList;    /* Pointer to Dynamic Table */
tTMO_SLL            gNatTcpDelStack;    /* Pointer to the TCP to be deleted 
                                           Entries */
/* Pointer to the list contain connection information about TFTP and FTP 
 * pasv entry */
tTMO_SLL            gNatPartialLinksList;
/********************************************************************************        FUNCTIONS PROTOTYPE USED BY THE DATABASE MODULE
*******************************************************************************/

PRIVATE UINT4       NatCreateLocalNode
ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 u4LocHashKey, UINT4 *pu4InId,
           UINT4 *pu4InIpAddr, UINT2 *pu2InPort,
           tLocOutHashNode ** pLocOutHashNode, UINT1 *pu1Type,
           UINT1 *pu1AppCallStatus));
PRIVATE UINT4       NatCreateGlobalNode
ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr, UINT2 *pu2InPort,
           tGlobalHashNode * pGlobalNodeEntry,
           tLocOutHashNode ** pLocOutHashNode, UINT1 *pu1Type,
           UINT1 *pu1AppCallStatus));
PRIVATE VOID        NatAssignDynamicNode
ARG_LIST ((tDynamicEntry * pDynamicEntry, tHeaderInfo * pHeaderInfo,
           UINT4 u4IpAddr, UINT2 u2Port, UINT1 u1Type, UINT1 u1AppCallStatus));
PRIVATE UINT4       NatSearchIidOidArray
ARG_LIST ((UINT4 u4Iid, UINT4 u4Oid, tHeaderInfo * pHeaderInfo));
PRIVATE UINT4       NatCreateIidOidArrayDynamicList
ARG_LIST ((UINT4 u4Iid, UINT4 u4Oid, tHeaderInfo * pHeaderInfo, UINT4 u4IpAddr,
           UINT2 u2Port, UINT1 u1Type, UINT1 u1AppCallStatus));
PRIVATE tLocOutHashNode *NatCreateLocOutHashNode
ARG_LIST ((UINT4 u4HashKey, UINT4 u4IpAddr, UINT2 u2Port, UINT4 u4Type,
           UINT4 *pu4Id));
PRIVATE UINT4       NatCreateGlobalHashNode
ARG_LIST ((UINT4 u4TranslatedLocIpAddr, UINT2 u2TranslatedLocPort,
           UINT4 u4Iid));

PRIVATE UINT4 NatCreateNfsTableEntry ARG_LIST ((tHeaderInfo * pHeaderInfo));
PRIVATE UINT4 NatSearchDnsTable ARG_LIST ((UINT4 u4TranslatedLocIp));
PRIVATE UINT4       NatSearchPartialLinksList
ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr, UINT2 *pu2InPort,
           UINT1 *pu1Type, UINT1 *pu1AppCallStatus));
PRIVATE VOID NatCheckForNapt ARG_LIST ((tDynamicEntry * pDynamicListNode));
PRIVATE VOID        NatDeleteNatOnlyNode
ARG_LIST ((UINT4 u4HashIndex, UINT1 u1MulticastFlag, UINT4 u4IpAddr,
           UINT4 u4IfNum, UINT1 u1NaptEnable));

PRIVATE INT4 NatDeletePartialLinksList ARG_LIST ((VOID));
PRIVATE INT4        NatUpdateGlobalHashTable
ARG_LIST ((tHeaderInfo * pHeaderInfo, UINT4 u4InId, UINT4 *pu4InIpAddr,
           UINT2 *pu2InPort));

/***************************************************************************
* Function Name    :  NatDeleteTables
*
* Description      :  Delete all the hash tables used in the Nat module.     
*
* Input (s)        : VOID 
*
* Output (s)       : VOID 
*
* Returns          : VOID 
*
****************************************************************************/

/* All the Hash tables created during initialization should be
 * deleted here. */

VOID
NatDeleteHashTables (VOID)
{
    /* Delete the GlobalHashList. */
    TMO_HASH_Delete_Table (gpNatGlobalHashList, (VOID *) NULL);

    /* 
     * gpNatWanUaHashTbl is used to store NatWanUaHashNode Which Contains IP,
     * BasePort And Parity Of WAN UA, These Informations are added during
     * OpenPinHole Function Call by SIP-Server/SIP-ALG.
     * This Information is stored to Handle Early Data case in NAT_SIP
     */
    TMO_HASH_Delete_Table (gpNatWanUaHashTbl, (VOID *) NULL);

    /* Delete the Outs, this table is searched for the OutId. */
    TMO_HASH_Delete_Table (gpNatOutHashList, (VOID *) NULL);

    /* Delete the Ins list, this table is searched for the InId for a 
     * OutBound packet. */
    TMO_HASH_Delete_Table (gpNatLocHashList, (VOID *) NULL);

    /* Delete the Nat only table which stores the temporary mapping of 
     * the Local Ip address and Global Ip address for sometime. */
    TMO_HASH_Delete_Table (gpNatOnlyHashList, (VOID *) NULL);

    /*Delete the IPSec Inbound, Outbound and Pending hash table */
    TMO_HASH_Delete_Table (gpNatIPSecInHashList, (VOID *) NULL);
    TMO_HASH_Delete_Table (gpNatIPSecOutHashList, (VOID *) NULL);
    TMO_HASH_Delete_Table (gpNatIPSecPendHashList, (VOID *) NULL);

    /*Delete the IKE Hash Table */
    TMO_HASH_Delete_Table (gpNatIKEHashList, (VOID *) NULL);

}

/***************************************************************************
* Function Name    :  NatCleanHashTables
*
* Description      :  Cleans all the hash tables used in the Nat module.     
*                      releases all the nodes used to mempools.
*
* Input (s)        : VOID 
*
* Output (s)       : VOID 
*
* Returns          : VOID 
*
****************************************************************************/
/* Cleaning of hash tables is done here. */

VOID
NatCleanHashTables (VOID)
{
    /* 
     * gpNatGlobalHashList is used to store the InIds. 
     * This table is searched for
     * InId for a InBound packet translation .It uses the Translted Ip address
     * and Port to calculate the hash value.
     */
    TMO_HASH_Clean_Table (gpNatGlobalHashList);
    /* 
     * gpNatOutHashList is used to store the OutIds. This table is searched for
     * the OutId .It uses the Outside Host IP address and Port to calculate the
     * hash value.
     */
    TMO_HASH_Clean_Table (gpNatOutHashList);
    /* 
     * gpNatLocHashList is used to store the Ins. This table is searched for
     * the InId for a OutBound packet.It uses the Inside Host IP address and
     * Port to calculate the hash value.
     */
    TMO_HASH_Clean_Table (gpNatLocHashList);
    /*
     * gpNatOnlyHashList stores the temporary mapping of the Local 
     * Ip address and
     * Global IP address for sometime.This is used for Dynamic Ip address
     * allocation (non-preemptive).
     */
    TMO_HASH_Clean_Table (gpNatOnlyHashList);

    /*Initialise the IPSec Inbound, Outbound and Pending hash table */
    TMO_HASH_Clean_Table (gpNatIPSecInHashList);
    TMO_HASH_Clean_Table (gpNatIPSecOutHashList);
    TMO_HASH_Clean_Table (gpNatIPSecPendHashList);

    /*Initialise the IKE Hash Table */
    TMO_HASH_Clean_Table (gpNatIKEHashList);

}

/***************************************************************************
* Function Name    :  NatInitListsAndTables
* Description    :  Initialises the global variables ,linked lists and hash
*                   tables used in the NAT module.
*
*
* Input (s)   : VOID 
*
* Output (s)  : VOID 
*
* Returns     : VOID 
*
****************************************************************************/
/* Creation of the Hash tables / Lists is done here. All the Hash tables 
 * created, should be deleted also. */

VOID
NatInitListsAndTables (VOID)
{
    UINT4               u4Count1 = NAT_ZERO;
    UINT4               u4Count2 = NAT_ZERO;

    /*
     * gNatDynamicList contains pointer to the nodes storing the session
     * translation information.( All the entries in the Dynamic Translation
     * Table are connected to this linked list).This is used to traverse the
     * entries while deletion.
     */
    TMO_SLL_Init (&gNatDynamicList);
    /*
     * gNatUdpList contains linked list of pointer to UDP session entries in the
     * Dynamic Translation Table.(This make deletion of UDP sessions straight
     * forward.)
     */
    TMO_SLL_Init (&gNatUdpList);
    /*
     * gNatNfsList contains nodes containing session information for NFS
     * sessions
     */
    TMO_SLL_Init (&gNatNfsList);
    /*
     * gNatNfsIpList contains nodes containing the mapping of the Local IP
     * address and the Global Ip address used for NFS sessions.
     */
    TMO_SLL_Init (&gNatNfsIpList);
    /*
     * gNatDnsList conatins the Local Ip address (that has been responded by the
     * DNS server to the Outside Network) and its Translated Ip address ( that
     * will reach the Outside Host which queried) .
     */
    TMO_SLL_Init (&gNatDnsList);
    TMO_SLL_Init (&gNatOidFreeList);
    TMO_SLL_Init (&gNatIidFreeList);
    /*
     * gNatTcpDelStack contains nodes pointing to the TCP session entries in the
     * Dynamic Table which needs to be deleted.
     */
    TMO_SLL_Init (&gNatTcpDelStack);
    /*
     * gNatPartialLinksList conatins the Local IP address,Translated IP address,
     * Local port, Translated Port used for a Ftp/Tftp session going to be
     * established in the near future.
     * (This table acts as a Intermediate table where info is stored for a
     * future session.)
     */
    TMO_SLL_Init (&gNatPartialLinksList);
    /*
     * gNatFreePortList conatins node storing the freed Global Ports that can be
     * used for some future sessions involving port translation 
     */
    TMO_SLL_Init (&gNatFreePortList);

    /* Initialize Policy NAT linked lists. */
    NatPolicyInitLists ();

    gu4NatNextFreeTranslatedLocPort = gu4NatInitTranslatedLocPort;
    gu4NatTcpTimeOut = NAT_TCP_TIME_OUT;
    gu4NatUdpTimeOut = NAT_UDP_TIME_OUT;
    /* 
     * gpNatWanUaHashTbl is used to store NatWanUaHashNode Which Contains IP,
     * BasePort And Parity Of WAN UA, These Informations are added during
     * OpenPinHole Function Call by SIP-Server/SIP-ALG.
     * This Information is stored to Handle Early Data case in NAT_SIP
     */
    gpNatWanUaHashTbl = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL, NAT_ZERO);
    /* 
     * gpNatGlobalHashList is used to store the InIds. This table is searched for
     * InId for a InBound packet translation .It uses the Translted Ip address
     * and Port to calculate the hash value.
     */
    gpNatGlobalHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL,
                                                 NAT_ZERO);
    /* 
     * gpNatOutHashList is used to store the OutIds. This table is searched for
     * the OutId .It uses the Outside Host IP address and Port to calculate the
     * hash value.
     */
    gpNatOutHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL, NAT_ZERO);
    /* 
     * gpNatLocHashList is used to store the Ins. This table is searched for
     * the InId for a OutBound packet.It uses the Inside Host IP address and
     * Port to calculate the hash value.
     */
    gpNatLocHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL, NAT_ZERO);
    /*
     * gpNatOnlyHashList stores the temporary mapping of the Local 
     * Ip address and
     * Global IP address for sometime.This is used for Dynamic Ip address
     * allocation (non-preemptive).
     */
    gpNatOnlyHashList = TMO_HASH_Create_Table (NAT_ONLY_HASH_LIMIT, NULL,
                                               NAT_ZERO);

    /*
     * NatIidOidArray is a 2-D 50x50 array .Each element is the Head of the
     * singly linked list.
     * (This acts as a 2-D hash table which each element acting as a bucket).
     */
    for (u4Count1 = NAT_ZERO; u4Count1 < NAT_IID_OID_ARRAY_LIMIT; u4Count1++)
    {
        for (u4Count2 = NAT_ZERO; u4Count2 < NAT_IID_OID_ARRAY_LIMIT;
             u4Count2++)
        {
            TMO_SLL_Init (&gaNatIidOidArray[u4Count1][u4Count2]);
        }
    }
    /* global counter providing the InId value */
    gu4NatNextFreeIid = NAT_ZERO;
    /* global counter providing the OutId value */
    gu4NatNextFreeOid = NAT_ZERO;
    /* 
     * global counter providing the IcmpId value used to modify the ICMP Id when
     * Port Translation is enabled.
     */
    gu4NatIcmpNewIden = NAT_ICMP_STARTID;
    gu4NatPptpNewIden = NAT_PPTP_STARTID;

    /* Initialise the IPSec session list */
    TMO_SLL_Init (&gNatIPSecListNode);

    /* Initialise the IPSec Pending list */
    TMO_SLL_Init (&gNatIPSecPendListNode);

    /*Initialise the IPSec Inbound, Outbound and Pending hash table */
    gpNatIPSecInHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL,
                                                  NAT_ZERO);
    gpNatIPSecOutHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL,
                                                   NAT_ZERO);
    gpNatIPSecPendHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL,
                                                    NAT_ZERO);

    /*Initialise gu4IPSecTimeout to default value */
    gi4NatIPSecTimeOut = (INT4) (NAT_IPSEC_TIMEOUT);

    /*Initialise gu4IPSecPendTimeout to default value */
    gi4NatIPSecPendingTimeOut = (INT4) (NAT_IPSEC_PENDING_TIMEOUT);

    /*Initialise gu2IPSecMaxRetry to default value */
    gi4NatIPSecMaxRetry = NAT_IPSEC_MAX_RETRY_COUNT;

    /*Initialise the IKE session list */
    TMO_SLL_Init (&gNatIKEList);

    /*Initialise the IKE Hash Table */
    gpNatIKEHashList = TMO_HASH_Create_Table (NAT_HASH_LIMIT, NULL, NAT_ZERO);

    /*Initialise gu4IKETimeout to default value */
    gi4NatIKETimeOut = (INT4) (NAT_IKE_TIMEOUT);

    /*Initialise gu1IKEPortTranslation to default value */
    gi4NatIKEPortTranslation = NAT_DISABLE;

}

/***************************************************************************
* Function Name    :  NatSearchIidOidArray
* Description    :  This function searches the IidOidArray(2-D array) for the
*             presence of dynamic entry whose information are given
*             in pHeaderInfo. If a match is found, NAT_SUCCESS is
*             returned.
*
*
* Input (s)    :  1. u4InId - Identifier assigned to the Local host
*             2. u4OutId - Identifier assigned to the Outside host
*             3. pHeaderInfo - Contains the necessary information of
*            IP and TCP/UDP/ICMP headers.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatSearchIidOidArray (UINT4 u4InId, UINT4 u4OutId, tHeaderInfo * pHeaderInfo)
{
    tIidOidArrayDynamicListNode *pDynamicListNode = NULL;
    tDynamicEntry      *pDynamicEntry = NULL;
    UINT4               u4Status = NAT_FAILURE;
    UINT4               u4CurrTime = NAT_ZERO;

    /*
     * NatIidOidArray is a two dimension array with 50 * 50 elements -each
     * element is a pointer to a singly linked list containing the session
     * entry info.(Its like a 2-D hash table).
     */
    u4InId = u4InId % NAT_IID_OID_ARRAY_LIMIT;
    u4OutId = u4OutId % NAT_IID_OID_ARRAY_LIMIT;

    /*
     * we get the system time to update the time at which the session
     * translation info was last searched for.This is used to delete off
     * the entries which have not been used for a specified period of time.
     */
    NAT_GET_SYS_TIME (&u4CurrTime);

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchIidOidArray \n");
    /* Get the pointer to the dynamic list */
    if (NAT_OUTBOUND == pHeaderInfo->u4Direction)
    {

        /*
         * If it is an outbound packet, scan using the local IP, local Port,
         * Outside IP , Outside Port and Interface Number.
         */
        TMO_SLL_Scan (&gaNatIidOidArray[u4InId][u4OutId], pDynamicListNode,
                      tIidOidArrayDynamicListNode *)
        {
            pDynamicEntry = pDynamicListNode->pDynamicEntry;
            if ((pDynamicEntry->u4LocIpAddr == pHeaderInfo->u4InIpAddr) &&
                (pDynamicEntry->u2LocPort == pHeaderInfo->u2InPort) &&
                (pDynamicEntry->u4OutIpAddr == pHeaderInfo->u4OutIpAddr) &&
                (pDynamicEntry->u2OutPort == pHeaderInfo->u2OutPort) &&
                (pDynamicEntry->u4IfNum == pHeaderInfo->u4IfNum))
            {

                /*
                 * When a match is found, update the pHeaderInfo. In this case
                 * with Global IP and Global Port
                 */
                pDynamicEntry->u4TimeStamp = u4CurrTime;
                NatUpdateHeaderInfo (pHeaderInfo, pDynamicEntry);
                break;
            }
        }
    }
    else
    {

        /*
         * If it is an inbound packet, scan using the Global IP, Global Port,
         * Outside IP , Outside Port and Interface Number.
         */

        TMO_SLL_Scan (&gaNatIidOidArray[u4InId][u4OutId], pDynamicListNode,
                      tIidOidArrayDynamicListNode *)
        {
            pDynamicEntry = pDynamicListNode->pDynamicEntry;
            if ((pDynamicEntry->u4TranslatedLocIpAddr ==
                 pHeaderInfo->u4InIpAddr)
                && (pDynamicEntry->u2TranslatedLocPort == pHeaderInfo->u2InPort)
                && (pDynamicEntry->u4OutIpAddr == pHeaderInfo->u4OutIpAddr)
                && (pDynamicEntry->u2OutPort == pHeaderInfo->u2OutPort)
                && (pDynamicEntry->u4IfNum == pHeaderInfo->u4IfNum))
            {

                /*
                 * When a match is found, update the pHeaderInfo. In this case
                 * with Local IP and Local Port.
                 */
                pDynamicEntry->u4TimeStamp = u4CurrTime;
                NatUpdateHeaderInfo (pHeaderInfo, pDynamicEntry);
                break;
            }
        }
    }

    if (pDynamicListNode != NULL)
    {
        u4Status = NAT_SUCCESS;
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchIidOidArray\n");
    return (u4Status);
}

/***************************************************************************
* Function Name    : NatCreateIidOidArrayDynamicList
* Description    :  This function adds the new connection information to
*                   the dynamic table and add it in the IidOidArray at the
*                   respective place given by u4InId and u4OutId.If its 
*                   the first session in the table then start the Nat Timer.
*
* Input (s)    :  1. u4InId - Local host Identifier used for referencing
*             into the IidOidArray.
*             2. u4OutId - Outside host Identifier used for referencing
*             into the IidOidArray.
*             3. pHeaderInfo - Contains information on IP and TCP/UDP
*             header along with payload information if any.
*             4. u4IpAddr - Depending on direction it is either Global
*             IP address or Local IP address.
*             5. u2Port - Depending on the direction it is either Global
*             port or Local Port.
*
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCreateIidOidArrayDynamicList (UINT4 u4InId, UINT4 u4OutId,
                                 tHeaderInfo * pHeaderInfo, UINT4 u4InIp,
                                 UINT2 u2Port, UINT1 u1Type,
                                 UINT1 u1AppCallStatus)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    tUdpList           *pUdpEntry = NULL;
    UINT4               u4ArrayIid = NAT_ZERO;
    UINT4               u4ArrayOid = NAT_ZERO;
    tIidOidArrayDynamicListNode *pIidOidArrayDynamicListNode = NULL;
    UINT4               u4CreateStatus = NAT_FAILURE;
    /* Get the index of the array */
    u4ArrayIid = u4InId % NAT_IID_OID_ARRAY_LIMIT;
    u4ArrayOid = u4OutId % NAT_IID_OID_ARRAY_LIMIT;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatCreateIidOidArrayDynamicList \n");

    if (NAT_MEM_ALLOCATE (NAT_DYNAMIC_POOL_ID, pDynamicEntry, tDynamicEntry) !=
        NULL)
    {
        MEMSET (pDynamicEntry, NAT_ZERO, sizeof (tDynamicEntry));
        u4CreateStatus = NAT_SUCCESS;
        /* Creating a dynamic entry and entering the information */
        NatAssignDynamicNode (pDynamicEntry, pHeaderInfo, u4InIp, u2Port,
                              u1Type, u1AppCallStatus);
        if (NAT_MEM_ALLOCATE (NAT_ARR_DYNAMIC_NODE_POOL_ID,
                              pIidOidArrayDynamicListNode,
                              tIidOidArrayDynamicListNode) == NULL)
        {
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pDynamicEntry);
            return (NAT_FAILURE);
        }
        pIidOidArrayDynamicListNode->pDynamicEntry = pDynamicEntry;
        pHeaderInfo->pDynamicEntry = pDynamicEntry;
        pDynamicEntry->u1UsedFlag = NAT_FALSE;

        /* Add in IidOidArray list and the pDynamic list */

        TMO_SLL_Add (&gaNatIidOidArray[u4ArrayIid][u4ArrayOid],
                     &(pIidOidArrayDynamicListNode->
                       IidOidArrayDynamicListNode));
        TMO_SLL_Add (&gNatDynamicList, &(pDynamicEntry->DynamicTableEntry));

        /* If it is an UDP packet, then add the pointer into UDP list too */
        if (pHeaderInfo->u1PktType == NAT_UDP)
        {
            NAT_MEM_ALLOCATE (NAT_UDP_POOL_ID, pUdpEntry, tUdpList);
            if (pUdpEntry != NULL)
            {
                pUdpEntry->pDynamicEntry = pDynamicEntry;
                pDynamicEntry->u1UsedFlag = NAT_TRUE;
                TMO_SLL_Add (&gNatUdpList,
                             &(pUdpEntry->IidOidArrayDynamicListNode));
            }
        }
        gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfActiveSessions++;
        /* 
         * Start the timer only once- that is when the first session 
         * entry is created
         */
        if (gu4NatTmrEnable == NAT_DISABLE)
        {
            gu4NatTmrEnable = NAT_ENABLE;
            /* START TIMER */
            if (NatStartTimer () == NAT_FAILURE)
            {
                MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                         "\n Unable to start Timer \n");
            }
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCreateIidOidArrayDynamicList \n");
    return (u4CreateStatus);
}

/***************************************************************************
* Function Name    :  NatAssignDynamicNode
* Description    :  This function updates the dynamic table field with the
*             new connection information.
*
* Input (s)    :  1. pDynamicEntry - The pointer to the dynamic entry.
*             2. pHeaderInfo - Contains updated information about the
*             connection.
*             3. u4IpAddr - Depending upon the direction, this is either
*             Global IP address or Local IP address.
*             4. u2Port - Depending upon the direction, this is either
*             Global Port or Local Port.
*
* Output (s)    :  updates Dynamic Table entry pointed to pDynamicEntry
* Returns      :  None
*
****************************************************************************/

PRIVATE VOID
NatAssignDynamicNode (tDynamicEntry * pDynamicEntry, tHeaderInfo * pHeaderInfo,
                      UINT4 u4IpAddr, UINT2 u2Port, UINT1 u1Type,
                      UINT1 u1AppCallStatus)
{

    UINT4               u4CurrTime = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatAssignDynamicNode \n");

    NAT_GET_SYS_TIME (&u4CurrTime);
    pDynamicEntry->u4TimeStamp = u4CurrTime;
    pDynamicEntry->u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
    pDynamicEntry->u2OutPort = pHeaderInfo->u2OutPort;
    pDynamicEntry->u4IfNum = pHeaderInfo->u4IfNum;

    /* Update the information based on the direction */

    if (NAT_OUTBOUND == pHeaderInfo->u4Direction)
    {
        pDynamicEntry->u4LocIpAddr = pHeaderInfo->u4InIpAddr;
        pDynamicEntry->u2LocPort = pHeaderInfo->u2InPort;
        pDynamicEntry->u4TranslatedLocIpAddr = u4IpAddr;
        pDynamicEntry->u2TranslatedLocPort = u2Port;
        pHeaderInfo->u4InIpAddr = u4IpAddr;
        pHeaderInfo->u2InPort = u2Port;
    }
    else
    {
        pDynamicEntry->u4TranslatedLocIpAddr = pHeaderInfo->u4InIpAddr;
        pDynamicEntry->u2TranslatedLocPort = pHeaderInfo->u2InPort;
        pDynamicEntry->u4LocIpAddr = u4IpAddr;
        pDynamicEntry->u2LocPort = u2Port;
        pHeaderInfo->u4InIpAddr = u4IpAddr;
        pHeaderInfo->u2InPort = u2Port;
    }
    pDynamicEntry->u1PktType = pHeaderInfo->u1PktType;
    pDynamicEntry->u1NaptEnable =
        gapNatIfTable[pHeaderInfo->u4IfNum]->u1NaptEnable;
    pDynamicEntry->u1Direction = (UINT1) pHeaderInfo->u4Direction;

    /* u1AppCallStatus, u1Type: Applicable only for SIP Traffic */
    pDynamicEntry->u1AppCallStatus = u1AppCallStatus;
    pDynamicEntry->u1Type = u1Type;
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatAssignDynamicNode \n");
}

/***************************************************************************
* Function Name    :  NatCreateLocOutHashNode
* Description    :  This creates a bucket either in the Local Hash
*             Table or in the Outside Hash Table depending on u4Type.
*
* Input (s)    :  1. u4HashKey - The key to the Hash Table
*                 2. u4IpAddr - contains the local IP address
*                 3. u2Port - Contains the local port
*                 4. u4Type - Tells whether bucket should be created in
*                    Local Hash Table or Outside Hash Table.
*
*
* Output (s)    :  The inside Identifier (InId).
* Returns      :  The pointer to the local node created.
*
****************************************************************************/

PRIVATE tLocOutHashNode *
NatCreateLocOutHashNode (UINT4 u4HashKey, UINT4 u4IpAddr, UINT2 u2Port,
                         UINT4 u4Type, UINT4 *pu4Id)
{
    tLocOutHashNode    *pNode = NULL;
    tTMO_HASH_TABLE    *pLocOutHashList = NULL;
    tFreeIdNode        *pFreeIdNode = NULL;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatCreateLocOutHashNode \n");

    /*
     * Depending upon the u4Type which can be either Local or Outside
     * update the hash bucket information.
     */
    if (NAT_MEM_ALLOCATE (NAT_HASH_POOL_ID, pNode, tLocOutHashNode) != NULL)
    {
        if (NAT_LOCAL == u4Type)
        {
            pNode->u4IpAddr = u4IpAddr;
            pNode->u2Port = u2Port;
            pFreeIdNode = (tFreeIdNode *) TMO_SLL_Get (&gNatIidFreeList);
            if (pFreeIdNode != NULL)
            {
                pNode->u4Id = pFreeIdNode->u4Id;
            }
            else
            {
                pNode->u4Id = gu4NatNextFreeIid++;
            }
            pLocOutHashList = gpNatLocHashList;
        }
        else
        {
            pNode->u4IpAddr = u4IpAddr;
            pNode->u2Port = u2Port;
            pFreeIdNode = (tFreeIdNode *) TMO_SLL_Get (&gNatOidFreeList);
            if (pFreeIdNode != NULL)
            {
                pNode->u4Id = pFreeIdNode->u4Id;
            }
            else
            {
                pNode->u4Id = gu4NatNextFreeOid++;
            }
            pLocOutHashList = gpNatOutHashList;
        }

        pNode->u4NoOfEntries = NAT_ZERO;
        *pu4Id = pNode->u4Id;

        /* Add the node in the corresponding Hash Table */
        TMO_HASH_Add_Node (pLocOutHashList, &(pNode->LocOutHash), u4HashKey,
                           NULL);
    }
    else
    {
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n No free Memory to create LocOutHash Node \n");
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCreateLocOutHashNode \n");
    return (pNode);
}

/***************************************************************************
* Function Name    :  NatCreateGlobalHashNode
* Description    :  This function creates a bucket in the Global Hash Table.
*
* Input (s)    :
*             1. u4TranslatedLocIpAddr - contains the global IP address.
*             2. u2TranslatedLocPort - contains the global port.
*             3. u4InId - Contains the local host Identifier to which
*             the Global IP address given in pHeaderInfo is assigned.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCreateGlobalHashNode (UINT4 u4TranslatedLocIpAddr, UINT2 u2TranslatedLocPort,
                         UINT4 u4InId)
{

    tGlobalHashNode    *pGlobalNode = NULL;
    tIidListNode       *pIidList = NULL;
    UINT4               u4CreateStatus = NAT_FAILURE;
    UINT4               u4HashKey = NAT_ZERO;

    /*
     * this function creates a new Global Hash node and Updates the Iid List
     * with the Iid given as input.
     */
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatCreateGlobalHashNode \n");
    if (NAT_MEM_ALLOCATE (NAT_GLOBAL_HASH_POOL_ID,
                          pGlobalNode, tGlobalHashNode) != NULL)
    {
        u4CreateStatus = NAT_SUCCESS;
        u4HashKey =
            NatFormHashKey (u4TranslatedLocIpAddr, u2TranslatedLocPort,
                            NAT_GLOBAL);
        /* Update the bucket information */
        pGlobalNode->u4TranslatedLocIpAddr = u4TranslatedLocIpAddr;
        pGlobalNode->u2TranslatedLocPort = u2TranslatedLocPort;

        /* Add the IID in the IidList */
        TMO_SLL_Init (&pGlobalNode->pIidList);
        if (NAT_MEM_ALLOCATE (NAT_IID_LIST_POOL_ID, pIidList, tIidListNode) !=
            NULL)
        {
            u4CreateStatus = NAT_SUCCESS;
            pIidList->u4InId = u4InId;
            pIidList->u4NoOfEntries = NAT_ONE;
            TMO_SLL_Add (&pGlobalNode->pIidList, &(pIidList->GlobalIidList));

            /* Add in the Global Hash Table */
            TMO_HASH_Add_Node (gpNatGlobalHashList,
                               &(pGlobalNode->GlobalHash), u4HashKey, NULL);
        }
        else
        {
            u4CreateStatus = NAT_FAILURE;
            NatMemReleaseMemBlock (NAT_GLOBAL_HASH_POOL_ID,
                                   (UINT1 *) pGlobalNode);
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCreateGlobalHashNode \n");
    return (u4CreateStatus);
}

/***************************************************************************
* Function Name    :  NatCreateLocalNode
* Description    : This function creates the local node in the Local Hash table
*                  and creates a corresponding global node in Global Hash Table.
*
* Input (s)    :  1. pHeaderInfo - contains the connection information.
*                            2. u4LocHashKey - Local Hash Key.
*
* Output (s)    :  1. pu4InId - The local Indentifier.
*                            2. pu4InIpAddr - The Global Ip Address Allocated.
*                            3. pu2InPort - The global port assigned in case of NAPT.
* Returns      :  NAT_SUCCESS if it is able to create local node and a
*                       corresponding globalnode 
*                 else
*                 NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCreateLocalNode (tHeaderInfo * pHeaderInfo, UINT4 u4LocHashKey,
                    UINT4 *pu4InId, UINT4 *pu4InIpAddr, UINT2 *pu2InPort,
                    tLocOutHashNode ** pNode, UINT1 *pu1Type,
                    UINT1 *pu1AppCallStatus)
{
    UINT4               u4ConnectionStatus;
    tGlobalInfo         GlobalInfo;
    tLocOutHashNode    *pLocHashNode = NULL;
    tStaticNaptEntry   *pStaticNaptNode = NULL;
    tNatPartialLinkNode *pInPartialNode = NULL;
    UINT4               u4DummyIp = NAT_ZERO;
    UINT2               u2DummyPort = NAT_ZERO;
    UINT4               u4StaticIp = NAT_ZERO;

    /*
     * Note- This function is called only for "OUTBOUND session creation".
     * First create a local node in the Local hash table and then get the
     * corresponding Trans IP and New Port(for NAPT) .Then create a global node
     * in the Global Hash Table with this TransIP and Port.
     */
    pLocHashNode = NULL;
    u4ConnectionStatus = NAT_SUCCESS;
    GlobalInfo.u4TranslatedLocIpAddr = NAT_ZERO;
    GlobalInfo.u2TranslatedLocPort = NAT_ZERO;

    u4StaticIp = NatSearchStaticTable (pHeaderInfo->u4InIpAddr,
                                       NAT_OUTBOUND, pHeaderInfo->u4IfNum);
    if (*pu4InId == NAT_ZERO)
    {
        pLocHashNode =
            NatCreateLocOutHashNode (u4LocHashKey, pHeaderInfo->u4InIpAddr,
                                     pHeaderInfo->u2InPort, NAT_LOCAL, pu4InId);
        if (pLocHashNode == NULL)
        {
            u4ConnectionStatus = NAT_FAILURE;
        }
    }
    if (u4ConnectionStatus == NAT_SUCCESS)
    {
        /*
         * Search the PartialLinksList .This is because it stores the Trans IP
         * address created during the translation of the control session .
         * And the same IP address mapping should be used for the future
         * data session or any other sessions (related to that control session)
         * starting from the other side of the connection.
         */
        u4ConnectionStatus =
            NatSearchPartialLinksList (pHeaderInfo, pu4InIpAddr, pu2InPort,
                                       pu1Type, pu1AppCallStatus);

        if (((pHeaderInfo->u1PktType != NAT_TCP)
             && (u4ConnectionStatus == NAT_FAILURE))
            || ((pHeaderInfo->u1PktType == NAT_TCP)
                && (pHeaderInfo->u1SynBit == NAT_TRUE)))
        {
            u4DummyIp = pHeaderInfo->u4InIpAddr;
            u2DummyPort = pHeaderInfo->u2InPort;
            /* Search In Partial, if present use that global port  */
            pStaticNaptNode =
                NatGetStaticNaptEntry (&u4DummyIp, &u2DummyPort,
                                       pHeaderInfo->u4Direction,
                                       pHeaderInfo->u4IfNum,
                                       pHeaderInfo->u1PktType);

            /* If it is present in In-Partial List use the 
             * global ip port - TBD */
            if (pStaticNaptNode != NULL)
            {
                GlobalInfo.u4TranslatedLocIpAddr =
                    pStaticNaptNode->u4TranslatedLocIpAddr;
                GlobalInfo.u2TranslatedLocPort =
                    pStaticNaptNode->u2TranslatedLocPort;
            }
            else if ((pInPartialNode = NatGetAppInPartialEntry (u4DummyIp,
                                                                u2DummyPort,
                                                                pHeaderInfo->
                                                                u4Direction,
                                                                pHeaderInfo->
                                                                u1PktType)) !=
                     NULL)
            {
                GlobalInfo.u4TranslatedLocIpAddr =
                    pInPartialNode->u4TranslatedLocIpAddr;
                GlobalInfo.u2TranslatedLocPort =
                    pInPartialNode->u2TranslatedLocPort;
            }
            else
            {
                if ((gu4NatEnable == NAT_DISABLE) ||
                    (NatCheckIfNatEnable (pHeaderInfo->u4IfNum) == NAT_DISABLE))
                {
                    GlobalInfo.u4TranslatedLocIpAddr = pHeaderInfo->u4InIpAddr;
                    GlobalInfo.u2TranslatedLocPort = pHeaderInfo->u2InPort;
                }
                else
                {
                    /* get a Global IP address and Port  */
                    NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                                pHeaderInfo->u2InPort,
                                                pHeaderInfo->u4OutIpAddr,
                                                pHeaderInfo->u2OutPort,
                                                pHeaderInfo->u4IfNum,
                                                &GlobalInfo);
                    if (GlobalInfo.u4TranslatedLocIpAddr != NAT_ZERO)
                    {
                        if (pHeaderInfo->u2OutPort == NAT_RLOGIN_PORT)
                        {
                            if (GlobalInfo.u2TranslatedLocPort != NAT_ZERO)
                            {
                                /*
                                 * We must Release Global port, only when NAPT
                                 * is enabled.
                                 */
                                if (NatCheckIfNaptEnable (pHeaderInfo->u4IfNum)
                                    == NAT_ENABLE)
                                {
                                    if(NatReleaseGlobalPort
                                        (GlobalInfo.u2TranslatedLocPort) == NAT_FAILURE)
                                    {
                                     NAT_DBG1(NAT_DBG_TABLE,
                                              "\nUnable to release the port %d \n",GlobalInfo.u2TranslatedLocPort); 
                                    }    
                                }
                            }

                            /* get a privilaged port (512 - 1023) */
                            GlobalInfo.u2TranslatedLocPort =
                                NatGetPrivilagedGlobalPort ();
                            /* If unable to get, set IP to 0 */
                            if (GlobalInfo.u2TranslatedLocPort == NAT_ZERO)
                            {
                                GlobalInfo.u4TranslatedLocIpAddr = NAT_ZERO;
                                return NAT_FAILURE;
                            }
                        }
                        if ((pHeaderInfo->u1PktType != NAT_ICMP) &&
                            (pHeaderInfo->u1PktType != NAT_PPTP))
                        {
                            if ((GlobalInfo.u2TranslatedLocPort ==
                                 NAT_ZERO)
                                || ((pHeaderInfo->u2InPort == NAT_FTP_DATA_PORT)
                                    && (pHeaderInfo->u1PktType == NAT_TCP))
                                || (pHeaderInfo->u2InPort == NAT_ZERO))
                            {
                                /* Use the Local Port without translation */
                                GlobalInfo.u2TranslatedLocPort =
                                    pHeaderInfo->u2InPort;
                            }
                        }
                        else
                        {
                            /*For ICMP packets modify the ICMP Id if Napt 
                             * is Enabled */
                            if (gapNatIfTable
                                [pHeaderInfo->u4IfNum]->u1NaptEnable
                                == NAT_ENABLE)
                            {
                                if (pHeaderInfo->u1PktType == NAT_ICMP)
                                {
                                    /*We need to chek the implementation on port translation */
                                    if (u4StaticIp != NAT_ZERO)
                                    {
                                        GlobalInfo.u2TranslatedLocPort =
                                            pHeaderInfo->u2InPort;
                                    }
                                    else
                                    {
                                        GlobalInfo.u2TranslatedLocPort =
                                            (UINT2) (gu4NatIcmpNewIden++);
                                    }
                                }
                                else
                                {
                                    if (gu4NatPptpOutCreateFlag != TRUE)
                                    {
                                        GlobalInfo.u2TranslatedLocPort =
                                            (UINT2) (gu4NatPptpNewIden++);
                                    }
                                    else
                                    {
                                        GlobalInfo.u2TranslatedLocPort =
                                            pHeaderInfo->u2InPort;
                                    }
                                }
                            }
                            else
                            {
                                GlobalInfo.u2TranslatedLocPort =
                                    pHeaderInfo->u2InPort;
                            }
                        }
                    }
                }
            }

            if (GlobalInfo.u4TranslatedLocIpAddr != NAT_ZERO)
            {
                *pu4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
                *pu2InPort = GlobalInfo.u2TranslatedLocPort;
                u4ConnectionStatus = NAT_SUCCESS;
            }
            else
            {
                u4ConnectionStatus = NAT_FAILURE;
                MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                         "\n No free Global IP address \n");
            }
        }
        if (u4ConnectionStatus == NAT_SUCCESS)
        {
            /*
             * Local Hash node has been created.
             * A mapping has been obtained for the Local Ip & Port 
             * So create a Global Hash node
             */
            u4ConnectionStatus =
                NatCreateGlobalHashNode (*pu4InIpAddr, *pu2InPort, *pu4InId);

        }
    }
    if ((pLocHashNode != NULL) && (u4ConnectionStatus == NAT_FAILURE))
    {
        /*
         * If unable to get a mapping for Local IP and Port 
         *   or unable to create a Global Hash Node then 
         *   Delete the Local Hash Node already created.
         */
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Unable to create Global Node \n");
        TMO_HASH_Delete_Node (gpNatLocHashList, &(pLocHashNode->LocOutHash),
                              u4LocHashKey);
        NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pLocHashNode);
        if(GlobalInfo.u2TranslatedLocPort != NAT_ZERO)
        {
          if( NatReleaseGlobalPort(GlobalInfo.u2TranslatedLocPort)== NAT_FAILURE)
          {
            /*Since it is a failed condition We need to decrement gu4NatNextFreeTranslatedLocPort 
             * if it is not added to free port pool*/ 
            if(gu4NatNextFreeTranslatedLocPort > gu4NatInitTranslatedLocPort)
            {
                gu4NatNextFreeTranslatedLocPort --;
            }
          }
            
        }
        pLocHashNode = NULL;
    }
    *pNode = pLocHashNode;
    return (u4ConnectionStatus);
}

/***************************************************************************
* Function Name  :  NatCreateGlobalNode
* Description    :  This function creates Global Hash Node.
*
* Input (s)      :  1. pHeaderInfo - Contains the connectio information.
*                   2. pGlobalNodeEntry - Pointer to the global Node.
*
* Output (s)     :  1. pu4InIpAddr - The local IP address corresponding to
*                                    the global IP address.
*                   2. pu2InPort - The port being contacted.
*                   3. pu1AppCallStatus - status of the sip entry
*                      * Applicable only for sip entries *
*
* Returns        :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCreateGlobalNode (tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr,
                     UINT2 *pu2InPort, tGlobalHashNode * pGlobalNodeEntry,
                     tLocOutHashNode ** pNode, UINT1 *pu1Type,
                     UINT1 *pu1AppCallStatus)
{
    UINT4               u4InIpAddr = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_ZERO;
    UINT4               u4LocHashKey = NAT_ZERO;
    UINT4               u4InId = NAT_ZERO;
    UINT2               u2InPort = NAT_ZERO;
    UINT2               u2LocalPort = NAT_ZERO;
    tLocOutHashNode    *pLocNodeEntry = NULL;
    tIidListNode       *pIidNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntry = NULL;

    *pu1Type = NAT_ZERO;
    *pu1AppCallStatus = NAT_ZERO;

    /*
     * Note - this function is called only when session entry needs to be
     * created for "INBOUND session". So some Information should be present in
     * any of the Intermediate Tables ( Dns Table or TftpFtp Table) or in the
     * Static Table.
     * When a new session entry is created 
     * the Local Hash node and the Global Hash node are created 
     * or the usage count is increased for the nodes already existing.
     * This is done only when the Translated IP address is present in 
     * the Tftp-Ftp List ,Dns List or the Static mapping list.
     */
    u4LocHashKey = NAT_ZERO;
    pLocNodeEntry = NULL;
    u4ConnectionStatus =
        NatSearchPartialLinksList (pHeaderInfo, &u4InIpAddr, &u2InPort,
                                   pu1Type, pu1AppCallStatus);

    if (u4ConnectionStatus == NAT_FAILURE)
    {
        if (gapNatIfTable[pHeaderInfo->u4IfNum]->u1TwoWayNatEnable
            == NAT_ENABLE)
        {
            /* No previous presence of TFTP and FTP command */
            u4InIpAddr = NatSearchDnsTable (pHeaderInfo->u4InIpAddr);
            if (u4InIpAddr != NAT_ZERO)
            {
                u2InPort = pHeaderInfo->u2InPort;
                u2LocalPort = u2InPort;
                /*
                 * ICMP packets are not allowed from Outside network to inside
                 * host if there is no static mapping for that inside host.
                 */
                if (pHeaderInfo->u1PktType == NAT_ICMP)
                {
                    u4InIpAddr = NAT_ZERO;
                    u4ConnectionStatus = NAT_FAILURE;
                    MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                             "\n InBound Ping to other than static\
                             mappings is not allowed \n");
                }
            }
            else
            {
                u4InIpAddr = pHeaderInfo->u4InIpAddr;
                u2InPort = pHeaderInfo->u2InPort;
                if (NAT_SUCCESS == NatPolicyGetStaticEntry (&u4InIpAddr,
                                                            pHeaderInfo->
                                                            u4InIpAddr,
                                                            pHeaderInfo->
                                                            u4OutIpAddr,
                                                            pHeaderInfo->
                                                            u2OutPort,
                                                            NAT_INBOUND))
                {
                    if (u4InIpAddr == NAT_ZERO)
                    {
                        u4ConnectionStatus = NAT_FAILURE;
                    }
                }
                else if ((pStaticNaptEntry =
                          NatGetStaticNaptEntry (&u4InIpAddr, &u2InPort,
                                                 pHeaderInfo->u4Direction,
                                                 pHeaderInfo->u4IfNum,
                                                 (UINT2) pHeaderInfo->
                                                 u1PktType)) == NULL)
                {
                    u4InIpAddr = NAT_ZERO;
                    u2InPort = pHeaderInfo->u2InPort;
                    u4InIpAddr = NatSearchStaticTable (pHeaderInfo->u4InIpAddr,
                                                       pHeaderInfo->u4Direction,
                                                       pHeaderInfo->u4IfNum);
                    if (u4InIpAddr == NAT_ZERO)
                    {
                        u4ConnectionStatus = NAT_FAILURE;
                    }
                }
                else
                {
                    /* 
                     * check whether, the entry is based on SIP 
                     * If Yes,  updated the pu1AppCallStatus variable
                     */
                    if ((pStaticNaptEntry->u1AppCallStatus == NAT_ON_HOLD) ||
                        (pStaticNaptEntry->u1AppCallStatus == NAT_NOT_ON_HOLD))
                    {
                        *pu1AppCallStatus = pStaticNaptEntry->u1AppCallStatus;
                        *pu1Type = NAT_IN_PARTIAL;
                    }
                }
                u2LocalPort = u2InPort;
            }
        }
    }
    else
    {
        /* Previous Presence of either FTP or TFTP command */
        u2LocalPort = u2InPort;
    }

    /* Valid Local IP mapping exists for the Global IP in the packet */
    if (u4InIpAddr != NAT_ZERO)
    {
        /*
         * The IP address mapping has been found so create a local node and/or
         * create a global node or increase the usage count if the nodes are
         * already existing.
         */
        u4LocHashKey = NatFormHashKey (u4InIpAddr, u2InPort, NAT_LOCAL);
        TMO_HASH_Scan_Bucket (gpNatLocHashList, u4LocHashKey, pLocNodeEntry,
                              tLocOutHashNode *)
        {
            if ((pLocNodeEntry->u4IpAddr == u4InIpAddr) &&
                (pLocNodeEntry->u2Port == u2InPort))
            {
                u4InId = pLocNodeEntry->u4Id;
                u4ConnectionStatus = NAT_SUCCESS;
                u2InPort = pHeaderInfo->u2InPort;
                if (pGlobalNodeEntry != NULL)
                {
                    /* Not Necessary */
                    u4ConnectionStatus = NAT_FAILURE;
                    TMO_SLL_Scan (&pGlobalNodeEntry->pIidList, pIidNode,
                                  tIidListNode *)
                    {
                        if (u4InId == pIidNode->u4InId)
                        {
                            pIidNode->u4NoOfEntries++;
                            u4ConnectionStatus = NAT_SUCCESS;
                            break;
                        }
                    }

                    if (u4ConnectionStatus != NAT_SUCCESS)
                    {
                        if (NAT_MEM_ALLOCATE (NAT_IID_LIST_POOL_ID,
                                              pIidNode, tIidListNode) != NULL)
                        {
                            u4ConnectionStatus = NAT_SUCCESS;
                            pIidNode->u4InId = u4InId;
                            pIidNode->u4NoOfEntries = NAT_ONE;
                            TMO_SLL_Add (&pGlobalNodeEntry->pIidList,
                                         &(pIidNode->GlobalIidList));
                        }
                    }
                }
                break;
            }
        }
        if (pLocNodeEntry == NULL)
        {
            /* Local Hash node absent so create one */
            if (u2InPort == NAT_ZERO)
            {
                u2InPort = pHeaderInfo->u2InPort;
            }
            if ((pLocNodeEntry =
                 NatCreateLocOutHashNode (u4LocHashKey, u4InIpAddr, u2InPort,
                                          NAT_LOCAL, &u4InId)) != NULL)
            {
                u4ConnectionStatus = NAT_SUCCESS;
            }
            else
            {
                u4ConnectionStatus = NAT_FAILURE;
            }
        }
    }

    if ((pGlobalNodeEntry == NULL) && (u4ConnectionStatus == NAT_SUCCESS))
    {
        /* Global Hash node absent so create one */
        NAT_TRC (NAT_TRC_ON, "\nCreate Global node. ");

        u4ConnectionStatus =
            NatCreateGlobalHashNode (pHeaderInfo->u4InIpAddr,
                                     pHeaderInfo->u2InPort, u4InId);
    }
    *pu4InIpAddr = u4InIpAddr;
    *pu2InPort = u2LocalPort;
    if ((pLocNodeEntry != NULL) && (u4ConnectionStatus == NAT_FAILURE))
    {
        MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                 "\n Unable to create Global Node \n");
        TMO_HASH_Delete_Node (gpNatLocHashList, &(pLocNodeEntry->LocOutHash),
                              u4LocHashKey);
        NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pLocNodeEntry);
        pLocNodeEntry = NULL;
    }
    *pNode = pLocNodeEntry;
    return (u4ConnectionStatus);
}

/***************************************************************************
* Function Name    :  NatGetDynamicEntry
* Description    :  This function searches the various HASH Tables to find
*             the connection information of the arrived packet. If
*             present it updates pHeaderInfo otherwise depending upon
*             the direction creates the entry correspondingly and
*             update the pHeaderInfo. If for a new outbound packet
*             a global IP cannot be assigned, it will result in
*             NAT failure.
*
* Input (s)    : pHeaderInfo - This contains the connection details.
*
* Output (s)    : updated pHeaderInfo
* Returns      : NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

/*
 * To get the connection information from the DYNAMIC TABLE if it exists else
 * to create an entry with the new information.
 */

PUBLIC UINT4
NatGetDynamicEntry (tHeaderInfo * pHeaderInfo, UINT4 u4CreateSearchFlag)
{
    tLocOutHashNode    *pLocNodeEntry = NULL;
    tGlobalHashNode    *pGlobalNodeEntry = NULL;
    tLocOutHashNode    *pOutNodeEntry = NULL;
    tIidListNode       *pIidNode = NULL;
    UINT4               u4InId = NAT_ZERO;
    UINT4               u4OutId = NAT_ZERO;
    UINT4               u4CreateFlag = NAT_FALSE;
    UINT4               u4LocHashKey = NAT_ZERO;
    UINT4               u4OutHashKey = NAT_ZERO;
    UINT4               u4GlobalHashKey = NAT_ZERO;
    UINT4               u4DummyIpAddr = NAT_ZERO;
    UINT2               u2DummyPort = NAT_ZERO;
    UINT2               u2InPort = NAT_ZERO;
    UINT4               u4InIpAddr = NAT_ZERO;
    UINT4               u4ConnectionStatus = NAT_FAILURE;
    UINT1               u1InIdExists = NAT_FALSE;
    UINT1               u1OutIdExists = NAT_FALSE;
    UINT1               u1Type = NAT_ZERO;
    UINT1               u1AppCallStatus = NAT_ZERO;

    /*
     * First search for the entry in the Outside Hash Table (for calculating the
     * hash function here we use the Outside IP address and Outside Port).
     * If the entry is present then get the OutId else create one entry
     * in the Outside Hash Table and get the OutId.
     * After this the flow is dependant on the direction of the packet flow.
     * If the direction is OUTBOUND then 
     *   search for the entry in the Local Hash Table (for hash function we use
     * Local/Src IP +Local/Src Port).If the Entry is present then get the InId
     * else create an entry and get the InId.
     * Use the above OutId and InId to get the session info from the 2-D array
     * NatIidOidArray.
     * 
     * If the direction is INBOUND then
     *   search for the entry in the Global Hash Table (for hash fun we use
     * Translated IP/Dest IP + Dest Port).If an entry is found then we get a
     * list of InIds .Get the first InId and use the above OutId to get the
     * session info from NatIidOidArray.If the session info does not match with
     * our requirement then  we get the next InId from Global Hash node and get
     * the session info from NatIidOidArrray and so on.
     *
     * Note -If the session is new we may need to create a new Hash node (for
     * IPaddr + Port combination) or increment the usage count is the Hash node
     * already exists.
     */

    NAT_TRC (NAT_TRC_ON, "\n Entering NatGetDynamicEntry. ");
    /*
     * Form the hash key for outside IP and port and search in the Outside Hash
     * Table. If entry is found, get the corresponding OID else set the
     * createflag.
     */
    u4OutHashKey =
        NatFormHashKey (pHeaderInfo->u4OutIpAddr, pHeaderInfo->u2OutPort,
                        NAT_OUTSIDE);

    TMO_HASH_Scan_Bucket (gpNatOutHashList, u4OutHashKey, pOutNodeEntry,
                          tLocOutHashNode *)
    {
        if ((pOutNodeEntry->u4IpAddr == pHeaderInfo->u4OutIpAddr) &&
            (pOutNodeEntry->u2Port == pHeaderInfo->u2OutPort))
        {
            u4OutId = pOutNodeEntry->u4Id;
            u1OutIdExists = NAT_TRUE;
            u4ConnectionStatus = NAT_SUCCESS;
            break;
        }
    }
    if (pOutNodeEntry == NULL)
    {
        /* OutHash Node absent so create one */
        /*
         * If a new TCP packet comes with SYN bit "not set" then a new 
         * entry need
         * not be created in NAT.So failure is returned.
         * This is done at this place so as to avoid creation of HashNodes
         * instead of creating and then deleting them.
         */

        if ((pHeaderInfo->u1PktType == NAT_TCP)
            && (pHeaderInfo->u1SynBit != NAT_TRUE))
        {
            return NAT_FAILURE;
        }

        if (u4CreateSearchFlag != NAT_CREATE)
        {
            return NAT_FAILURE;
        }

        NAT_DBG (NAT_DBG_TABLE, "\n Creating new outside node \n");
        NAT_TRC (NAT_TRC_ON, "\n GET NEW OID \n");

        if ((pHeaderInfo->u4Direction == NAT_INBOUND) &&
            (gapNatIfTable[pHeaderInfo->u4IfNum]->u1TwoWayNatEnable
             != NAT_ENABLE))
        {
            u4ConnectionStatus =
                NatScanPartialLinksList (pHeaderInfo, &u4DummyIpAddr,
                                         &u2DummyPort);
            if (u4ConnectionStatus == NAT_FAILURE)
            {
                return NAT_FAILURE;
            }
        }
        if ((pOutNodeEntry =
             NatCreateLocOutHashNode (u4OutHashKey,
                                      pHeaderInfo->u4OutIpAddr,
                                      pHeaderInfo->u2OutPort, NAT_OUTSIDE,
                                      &u4OutId)) != NULL)
        {
            u4ConnectionStatus = NAT_SUCCESS;
            u4CreateFlag = NAT_TRUE;
        }
    }

    if (u4ConnectionStatus == NAT_SUCCESS)
    {
        if (NAT_OUTBOUND == pHeaderInfo->u4Direction)
        {

            /*
             * For outbound packet, form the hash key using local IP and 
             * port and
             * search in the Local Hash Table. If an entry is found, then obtain
             * the corresponding IID.
             * Using the IID and OID (obtained earlier) get the required
             * and assign a new Global IP and set the create flag. If no global 
             * IP address is available set the status to  NAT_FAILURE.
             *
             * Update the pHeaderInfo with the Global IP address if assigned.
             * Hash using the Global IP and   Port (TranslatedPort- if NAPT is
             * enabled otherwise LocalPort)  into the Global Hash Table.
             * If an entry already exists, then append the Iid in the IidList
             * of the Global IP node else  create the global bucket in the 
             * Global Hash Nopde and add  correspondingly.
             */

            u4LocHashKey =
                NatFormHashKey (pHeaderInfo->u4InIpAddr, pHeaderInfo->u2InPort,
                                NAT_LOCAL);

            TMO_HASH_Scan_Bucket (gpNatLocHashList, u4LocHashKey, pLocNodeEntry,
                                  tLocOutHashNode *)
            {
                if ((pLocNodeEntry->u4IpAddr == pHeaderInfo->u4InIpAddr) &&
                    (pLocNodeEntry->u2Port == pHeaderInfo->u2InPort))
                {
                    u4InId = pLocNodeEntry->u4Id;
                    u2InPort = pHeaderInfo->u2InPort;
                    u1InIdExists = NAT_TRUE;
                    u4ConnectionStatus = NAT_SUCCESS;
                    break;
                }
            }
            if (pLocNodeEntry == NULL)
            {
                /*
                 * Note :- This place it is not needed as the check 
                 * is present in the
                 * follwing function called.
                 * If a new TCP packet comes with SYN bit "not set" then a
                 * new entry need
                 * not be created in NAT.So failure is returned.
                 * This is done at this place so as to avoid creation of
                 * HashNodes
                 * instead of creating and then deleting them.
                 */

                if ((pHeaderInfo->u1PktType == NAT_TCP)
                    && (pHeaderInfo->u1SynBit != NAT_TRUE))
                {
                    return NAT_FAILURE;
                }
                if (u4CreateSearchFlag != NAT_CREATE)
                {
                    return NAT_FAILURE;
                }

                u4ConnectionStatus =
                    NatCreateLocalNode (pHeaderInfo, u4LocHashKey, &u4InId,
                                        &u4InIpAddr, &u2InPort, &pLocNodeEntry,
                                        &u1Type, &u1AppCallStatus);

                if (u4ConnectionStatus == NAT_SUCCESS)
                {
                    u4CreateFlag = NAT_TRUE;
                }
            }
            if (u4ConnectionStatus == NAT_SUCCESS)
            {
                /*
                 * Search the Session table (NatIidOid Array) if the session is
                 * an old session i.e. if it was already created
                 * If the session is a new session then it is possible that the
                 * Local/Src IP and Local/Src Port combination of the new session
                 * is same as that of some old session entry.(In this case the
                 * Local Hash Node is same for both the sessions ,but only the
                 * Usage Count increases for the Local Hash Node).In this
                 * situation the same Translated IP + Port combination should be
                 * used for both the sessions.Hence for the second session we
                 * have to get the same Trans IP + Port mapping.
                 * 
                 */
                if (u4CreateFlag != NAT_TRUE)
                {
                    u4ConnectionStatus =
                        NatSearchIidOidArray (u4InId, u4OutId, pHeaderInfo);
                    /* 
                     * In case if we have a valid iid, oid, but the dynamic
                     * entry is not present, then we should create one
                     * instead of returning failure 
                     */
                    if ((u4ConnectionStatus != NAT_SUCCESS) &&
                        (u1InIdExists == NAT_TRUE) &&
                        (u1OutIdExists == NAT_TRUE))
                    {
                        u4ConnectionStatus = (UINT4) NatUpdateGlobalHashTable
                            (pHeaderInfo, u4InId, &u4InIpAddr, &u2InPort);
                        if (u4ConnectionStatus == NAT_SUCCESS)
                        {
                            u4CreateFlag = NAT_TRUE;
                        }
                    }
                }
                else
                {
                    if (u4InIpAddr == NAT_ZERO)
                    {
                        u4ConnectionStatus = (UINT4) NatUpdateGlobalHashTable
                            (pHeaderInfo, u4InId, &u4InIpAddr, &u2InPort);
                    }
                }
            }
        }

        /* for INBOUND packets */

        else
        {

            /*
             * For Inbound packets, hash using Global IP and Global Port into
             * the Global Hash Table .
             *****If entry is found in the Global Hash Table then
             * scan through the IidList and for each IID present check if the
             * dynamic entry (from NatIidOid Array) obtained using this
             * combination of IID and OID is the required one.
             *  If none of the IID matches, check the DNS table using the
             * Global IP address.If entry is present, obtain the corresponding 
             * local IP address .Using local IP and Dest Port create an entry
             * in the Local Hash Table. The IID assigned is updated in the IID
             * list in the Global Hash table (using the Global IP and Dest Port
             * for hash function).
             * If there is no entry in the DNS table then Static table is
             * checked and the same procedure is done.
             * If no mapping is found, NAT_FAILURE is returned.
             ****** If the Global IP and Global Port  doesn't exist in the
             * Global Hash Table then the above procedure is done in followed by
             * the creation of Global hash Node.
             */
            u4ConnectionStatus = NAT_FAILURE;
            u4GlobalHashKey = NatFormHashKey (pHeaderInfo->u4InIpAddr,
                                              pHeaderInfo->u2InPort,
                                              NAT_GLOBAL);

            TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                                  pGlobalNodeEntry, tGlobalHashNode *)
            {
                if ((pGlobalNodeEntry->u4TranslatedLocIpAddr ==
                     pHeaderInfo->u4InIpAddr)
                    && (pGlobalNodeEntry->u2TranslatedLocPort ==
                        pHeaderInfo->u2InPort))
                {
                    TMO_SLL_Scan (&pGlobalNodeEntry->pIidList, pIidNode,
                                  tIidListNode *)
                    {
                        u4InId = pIidNode->u4InId;
                        u4ConnectionStatus =
                            NatSearchIidOidArray (u4InId, u4OutId, pHeaderInfo);
                        if (u4ConnectionStatus == NAT_SUCCESS)
                        {
                            break;
                        }
                    }
                    break;
                }
            }

            /*
             * Either the global node doesn't exist or there is no reference to
             * the present connection in the global node. 
             * If global node doesn't exist, then the global node should be
             * created else the local information is alone updated
             * in the global node in the form of InId.
             */
            if (((pGlobalNodeEntry == NULL) ||
                 (u4ConnectionStatus == NAT_FAILURE)))
            {
                /*
                 * If a new TCP packet comes with SYN bit "not set" then a new entry need
                 * not be created in NAT.So failure is returned.
                 * This is done at this place so as to avoid creation of HashNodes
                 * instead of creating and then deleting them.
                 */

                if ((pHeaderInfo->u1PktType == NAT_TCP)
                    && (pHeaderInfo->u1SynBit != NAT_TRUE))
                {
                    return NAT_FAILURE;
                }

                if (u4CreateSearchFlag != NAT_CREATE)
                {
                    return NAT_FAILURE;
                }

                u4ConnectionStatus =
                    NatCreateGlobalNode (pHeaderInfo, &u4InIpAddr, &u2InPort,
                                         pGlobalNodeEntry, &pLocNodeEntry,
                                         &u1Type, &u1AppCallStatus);
                if ((u4ConnectionStatus == NAT_SUCCESS) && (pLocNodeEntry
                                                            != NULL))
                {
                    u4InId = pLocNodeEntry->u4Id;
                    u4CreateFlag = NAT_TRUE;
                }
            }
        }
          /************************Hash Node LookUp & Creation Ends*********/
        /*
         * Here either we have the pointers to the Out Hash node and Local
         * Hash node or we have u4ConnectionStatus as NAT_FAILURE.
         *
         * Now the session entry is created in the NatIidOidArray and the 
         * NoOfEntries field in each Hash nodes is updated.
         */
        if (u4ConnectionStatus == NAT_SUCCESS)
        {
            if (u4CreateFlag == NAT_TRUE)
            {
                /*
                 * For session entry creation-
                 * In case of INBOUND session TwoWayNat should be enabled 
                 * In case of OUTBOUND session we should have the Translated IP
                 *  and Src Port (New or the same port).
                 */
                if (((NAT_INBOUND == pHeaderInfo->u4Direction)
                     || (NAT_OUTBOUND == pHeaderInfo->u4Direction))
                    && (u4InIpAddr != NAT_ZERO)
                    && ((pHeaderInfo->u1PktType == NAT_PPTP)
                        || (pHeaderInfo->u1PktType == NAT_ICMP)
                        || (pHeaderInfo->u1PktType == NAT_RSVP)
                        || (u2InPort != NAT_ZERO)))
                {
                    if (pHeaderInfo->u1PktType == NAT_TCP)
                    {
                        /*
                         * session entry is created for TCP session only when the
                         * TCP packet contains the Syn Bit
                         */
                        if (pHeaderInfo->u1SynBit == NAT_TRUE)
                        {
                            NAT_DBG (NAT_DBG_TABLE,
                                     "\n New Entry has to be created");
                            u4ConnectionStatus =
                                NatCreateIidOidArrayDynamicList (u4InId,
                                                                 u4OutId,
                                                                 pHeaderInfo,
                                                                 u4InIpAddr,
                                                                 u2InPort,
                                                                 u1Type,
                                                                 u1AppCallStatus);
                        }
                        else
                        {
                            u4ConnectionStatus = NAT_FAILURE;
                            MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                                     "\n Entry not present in dynamic\
                                     table \n");
                        }
                    }
                    else
                    {
                        NAT_DBG (NAT_DBG_TABLE,
                                 "\n New Entry has to be created");
                        u4ConnectionStatus =
                            NatCreateIidOidArrayDynamicList (u4InId, u4OutId,
                                                             pHeaderInfo,
                                                             u4InIpAddr,
                                                             u2InPort,
                                                             u1Type,
                                                             u1AppCallStatus);

                    }
                }
                else
                {
                    u4ConnectionStatus = NAT_FAILURE;
                }
                if (pOutNodeEntry != NULL)
                {
                    pOutNodeEntry->u4NoOfEntries++;
                }
                if (pLocNodeEntry != NULL)
                {
                    pLocNodeEntry->u4NoOfEntries++;
                }
            }
        }
        else
        {
            if (u4CreateFlag == NAT_TRUE)
            {
                TMO_HASH_Delete_Node (gpNatOutHashList,
                                      &(pOutNodeEntry->LocOutHash),
                                      u4OutHashKey);
                NatMemReleaseMemBlock (NAT_HASH_POOL_ID,
                                       (UINT1 *) pOutNodeEntry);

            }
        }
        NAT_TRC1 (NAT_TRC_ON, "\n Out Id is %d \n", u4OutId);
        NAT_TRC1 (NAT_TRC_ON, "\n In Id is %d \n", u4InId);
    }
    else
    {
        if (gapNatIfTable[pHeaderInfo->u4IfNum]->u1TwoWayNatEnable
            != NAT_ENABLE)
        {
            MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                     "\n Either Two Way NAT is not enabled\
                     or Memory not enough\n");
        }
    }
    if (pHeaderInfo->pDynamicEntry == NULL)
    {
        u4ConnectionStatus = NAT_FAILURE;
    }
    if(u4ConnectionStatus == NAT_FAILURE)
    {

        if(u2InPort != NAT_ZERO)
        {
          if( NatReleaseGlobalPort(u2InPort)== NAT_FAILURE)
          {
            /*Since it is a failed condition We need to decrement gu4NatNextFreeTranslatedLocPort 
             * if it is not added to free port pool*/ 
            if(gu4NatNextFreeTranslatedLocPort > gu4NatInitTranslatedLocPort)
            gu4NatNextFreeTranslatedLocPort --;
          }
            
        }
    }	
    NAT_TRC (NAT_TRC_ON, "\n Exiting NatGetDynamicEntry \n");

    return (u4ConnectionStatus);
}

/***************************************************************************
* Function Name  :  NatSearchPartialLinksList
* Description    :  This function searches the TFTP and FTP Table for the
*             presence of information about a  TFTP or FTP
*             connection.
*
* Input (s)    :  1. pHeaderInfo - The header information about the present
*             connection.
*             2. pu4InIpAddr - This IP address contains the modified
*             data.
*             3. pu2InPort - This contains the modified port.
*             4. pu4InId - This contains the inside identifier.
*             * pu1Type, pu1AppCallStatus: Applicable only for sip *
*             5. pu1Type - This contains the Type of the link (staticnapt or partial)
*             6. pu1AppCallStatus - This contains the call status
*
* Output (s)    :  pu4InIpAddr, pu2InPort, pu4InId
*                  pu1Type, pu1AppCallStatus
*
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatSearchPartialLinksList (tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr,
                           UINT2 *pu2InPort, UINT1 *pu1Type,
                           UINT1 *pu1AppCallStatus)
{
    UINT4               u4ConnectionStatus = NAT_FAILURE;
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pInPartialNode = NULL;
    tGlobalInfo         TranslatedIpPort;
    tNatWanUaHashNode  *pNatWanUaHashNode = NULL;
    eParity             eTransBasePortParity = PARITY_ANY;
    UINT2               u2BaseFreeNatPort = NATIPC_PORT_NUM_ZERO;
    UINT2               u2TotalNatPorts = NATIPC_VALUE_ZERO;
    UINT2               u2PortOffset = NATIPC_VALUE_ZERO;
    MEMSET (&TranslatedIpPort, NAT_ZERO, sizeof (tGlobalInfo));
    /*
     * Search for the Trans IP and the Outside IP in the list .If we get the
     * entry then we take the required info and delete the entry so that it
     * cannot be used again.
     */

    if ((pHeaderInfo->u1PktType == NAT_TCP) ||
        (pHeaderInfo->u1PktType == NAT_UDP))
    {
        if (pHeaderInfo->u4Direction == NAT_INBOUND)
        {
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {
                if ((pNatPartialLinkNode->u4TranslatedLocIpAddr ==
                     pHeaderInfo->u4InIpAddr)
                    && (pNatPartialLinkNode->u2TranslatedLocPort ==
                        pHeaderInfo->u2InPort)
                    && (pNatPartialLinkNode->u1Direction ==
                        pHeaderInfo->u4Direction)
                    && (pNatPartialLinkNode->u1PktType ==
                        pHeaderInfo->u1PktType))
                {
                    if ((pNatPartialLinkNode->u4OutIpAddr != NAT_ZERO)
                        && (pNatPartialLinkNode->u4OutIpAddr !=
                            pHeaderInfo->u4OutIpAddr))
                    {
                        continue;
                    }

                    /* 
                     * One more validation matching with Remote Port 
                     * if it is present 
                     */
                    if ((pNatPartialLinkNode->u2OutPort != NAT_ZERO)
                        && (pNatPartialLinkNode->u2OutPort !=
                            pHeaderInfo->u2OutPort))
                    {
                        continue;
                    }

                    *pu4InIpAddr = pNatPartialLinkNode->u4LocIpAddr;
                    *pu2InPort = pNatPartialLinkNode->u2LocPort;

                    /* 
                     * If there is match found for persistent partial link
                     * then update the time stamp to set it again to
                     * NAT_PERSIST_PARTIAL_LINK_TIMEOUT
                     */

                    if (pNatPartialLinkNode->u1PersistFlag == NAT_PERSISTENT)
                    {
                        NAT_GET_SYS_TIME (&(pNatPartialLinkNode->u4TimeStamp));
                    }
                    break;
                }
            }
        }
        else
        {
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {

                if ((pNatPartialLinkNode->u1Direction ==
                     pHeaderInfo->u4Direction) &&
                    (pNatPartialLinkNode->u1PktType == pHeaderInfo->u1PktType))
                {

                    if ((pNatPartialLinkNode->u4OutIpAddr != NAT_ZERO)
                        && (pNatPartialLinkNode->u4OutIpAddr !=
                            pHeaderInfo->u4OutIpAddr))
                    {
                        continue;
                    }

                    if ((pNatPartialLinkNode->u2OutPort != NAT_ZERO)
                        && (pNatPartialLinkNode->u2OutPort !=
                            pHeaderInfo->u2OutPort))
                    {
                        continue;
                    }

                    if ((pNatPartialLinkNode->u2LocPort != NAT_ZERO)
                        && (pNatPartialLinkNode->u2LocPort !=
                            pHeaderInfo->u2InPort))
                    {
                        continue;
                    }

                    /* One more extra validation */
                    if ((pNatPartialLinkNode->u4LocIpAddr != NAT_ZERO)
                        && (pNatPartialLinkNode->u4LocIpAddr !=
                            pHeaderInfo->u4InIpAddr))
                    {
                        continue;
                    }

                    *pu4InIpAddr = pNatPartialLinkNode->u4TranslatedLocIpAddr;
                    *pu2InPort = pNatPartialLinkNode->u2TranslatedLocPort;
                    /* If there is match found for persistent partial link
                     * then update the time stamp to set it again to
                     * NAT_PERSIST_PARTIAL_LINK_TIMEOUT 
                     */
                    if (pNatPartialLinkNode->u1PersistFlag == NAT_PERSISTENT)
                    {
                        NAT_GET_SYS_TIME (&(pNatPartialLinkNode->u4TimeStamp));
                    }

                    break;
                }
            }
        }

        if (pNatPartialLinkNode != NULL)
        {
            /* 
             * check whether, the entry is based on SIP
             */

            if ((pNatPartialLinkNode->u1AppCallStatus == NAT_ON_HOLD) ||
                (pNatPartialLinkNode->u1AppCallStatus == NAT_NOT_ON_HOLD))
            {

                /* NAT Global IP and Port Allocation for Outbound SIP Traffic */
                /* the Local IP/Port would be 0 in OUT partial list */
                if ((pNatPartialLinkNode->u4LocIpAddr == NAT_ZERO) &&
                    (pNatPartialLinkNode->u2LocPort == NAT_ZERO))
                {

                    pInPartialNode =
                        NatGetAppInPartialEntry (pHeaderInfo->u4InIpAddr,
                                                 pHeaderInfo->u2InPort,
                                                 NAT_INBOUND,
                                                 pHeaderInfo->u1PktType);

                    if (pInPartialNode != NULL)
                    {

                        *pu4InIpAddr = pInPartialNode->u4TranslatedLocIpAddr;
                        *pu2InPort = pInPartialNode->u2TranslatedLocPort;

                        *pu1AppCallStatus = pInPartialNode->u1AppCallStatus;
                        *pu1Type = NAT_IN_PARTIAL;

                        /* For outbound SIP traffic, if OUT IP/Port is present
                           in the hash node due to pinhole opened prior to add
                           port mapping ,then delete the redundant hash node as
                           add port mapping is called before the traffic and
                           hence in IN Partial list */
                        if (NatDelWanUaPortHashEntry (pHeaderInfo->u4OutIpAddr,
                                                      pHeaderInfo->u2OutPort,
                                                      NAT_DEL_ALL_HASH_BINDINGS,
                                                      (UINT2) pHeaderInfo->
                                                      u1PktType) != NAT_SUCCESS)
                        {
                            /* No need to throw an error as the hash node
                             * wouldnt have been created & as we have one
                             * more chance while deleting the partial
                             * entry */
                            NAT_TRC2 (NAT_TRC_ON, "Not able to delete the "
                                      "hash node OUT IP %u, OUT  Port %d \n ",
                                      pHeaderInfo->u4OutIpAddr,
                                      pHeaderInfo->u2OutPort);

                            NAT_DBG2 (NAT_DBG_ALL, "Not able to delete the "
                                      "hash node OUT IP %u, OUT Port %d \n ",
                                      pHeaderInfo->u4OutIpAddr,
                                      pHeaderInfo->u2OutPort);
                        }

                        return (NAT_SUCCESS);
                    }

                    /* find the Dst IP/Port is in Hash node and 
                       get No of ports and parity */
                    if ((pNatWanUaHashNode = NatFindWanUaPortHashEntry
                         (pHeaderInfo->u4OutIpAddr,
                          pHeaderInfo->u2OutPort,
                          (UINT2) (pHeaderInfo->u1PktType))) == NULL)
                    {
                        /* if the traffic has already passed with OUT port 
                           in the range of the creation of pinholes.
                           pu4InIpAddr & pu2InPort are filled already.
                           Just return the NAT IP and Port **OR** This is 
                           the pkt from WAN to LAN before sending 200 OK */

                        return NAT_SUCCESS;
                    }

                    /* allocate N bindings with parity */
                    u2TotalNatPorts = pNatWanUaHashNode->u2TotalBindings;
                    eTransBasePortParity =
                        pNatWanUaHashNode->eTransBasePortParity;

                    u2PortOffset =
                        (UINT2) (pHeaderInfo->u2OutPort -
                                 pNatWanUaHashNode->u2OutBasePort);

                    if ((pNatPartialLinkNode->u2TranslatedLocPort ==
                         NAT_ZERO) &&
                        (pNatPartialLinkNode->u4TranslatedLocIpAddr ==
                         NAT_ZERO))
                    {

                        /* Allocating Nat Translated IP Addrses */
                        NatGetNextFreeGlobalIpPort (pHeaderInfo->
                                                    u4InIpAddr,
                                                    pHeaderInfo->
                                                    u2InPort,
                                                    pHeaderInfo->
                                                    u4OutIpAddr,
                                                    pHeaderInfo->
                                                    u2OutPort,
                                                    pHeaderInfo->u4IfNum,
                                                    &TranslatedIpPort);

                        *pu4InIpAddr = TranslatedIpPort.u4TranslatedLocIpAddr;
                        /*
                         * We must Release Global port, only when NAPT
                         * is enabled.
                         */
                        if (NatCheckIfNaptEnable (pHeaderInfo->u4IfNum)
                            == NAT_ENABLE)
                        {

                            /*
                             * we just need NAT IP and not
                             * Port from the above API
                             */
                            if (NatReleaseGlobalPort (TranslatedIpPort.
                                                      u2TranslatedLocPort) !=
                                NAT_SUCCESS)
                            {
                                NAT_TRC (NAT_TRC_ON,
                                         "\n NatReleaseGlobalPort, failed \n");
                            }
                        }

                        if ((gu4NatEnable == NAT_DISABLE) ||
                            (NatCheckIfNatEnable (pHeaderInfo->u4IfNum)
                             == NAT_DISABLE))
                        {
                            u2BaseFreeNatPort = pHeaderInfo->u2InPort;
                        }
                        else
                        {

                            if (NatCheckIfNaptEnable (pHeaderInfo->u4IfNum)
                                == NAT_ENABLE)
                            {
                                /* Bind type has to be updated when the 
                                   support for port allocation based on the 
                                   traffic is in place */
                                if (NatGetFreeGlobalPortGroup
                                    (&u2BaseFreeNatPort, u2TotalNatPorts,
                                     eTransBasePortParity,
                                     BIND_TYPE_MEDIA) != NAT_SUCCESS)
                                {
                                    return NAT_FAILURE;
                                }
                            }
                        }
                    }

                    if (NatCheckIfNaptEnable (pHeaderInfo->u4IfNum) ==
                        NAT_ENABLE)
                    {
                        *pu2InPort = (UINT2) (u2BaseFreeNatPort + u2PortOffset);
                    }
                    else
                    {
                        *pu2InPort = pHeaderInfo->u2InPort;
                        u2BaseFreeNatPort = pHeaderInfo->u2InPort;
                    }

                    /* update the OUT partial list */
                    if (NatUpdatePartialList
                        (pNatWanUaHashNode, pHeaderInfo->u4Direction,
                         *pu4InIpAddr, u2BaseFreeNatPort,
                         pHeaderInfo->u1PktType) == NAT_FAILURE)
                    {
                        return NAT_FAILURE;
                    }

                    /* delete the hash node as the NAT ports are reserved */
                    if (NatDelWanUaPortHashEntry (pHeaderInfo->u4OutIpAddr,
                                                  pHeaderInfo->u2OutPort,
                                                  NAT_DEL_ALL_HASH_BINDINGS,
                                                  (UINT2) pHeaderInfo->
                                                  u1PktType) != NAT_SUCCESS)
                    {
                        /* No need to throw an error as the hash node
                         * wouldnt have been created & as we have one
                         * more chance while deleting the partial
                         * entry */
                        NAT_TRC2 (NAT_TRC_ON, "Not able to delete the "
                                  "hash node OUT IP %u, OUT  Port %d \n ",
                                  pHeaderInfo->u4OutIpAddr,
                                  pHeaderInfo->u2OutPort);

                        NAT_DBG2 (NAT_DBG_ALL, "Not able to delete the "
                                  "hash node OUT IP %u, OUT Port %d \n ",
                                  pHeaderInfo->u4OutIpAddr,
                                  pHeaderInfo->u2OutPort);
                    }

                    *pu1AppCallStatus = pNatPartialLinkNode->u1AppCallStatus;
                    *pu1Type = NAT_OUT_PARTIAL;

                    return (NAT_SUCCESS);

                }
                /* For inbound SIP traffic, if OUT IP/Port is present
                   in the hash node due to pinhole opened and addport
                   binding created then delete it */
                /* The NAT ports are never created for inbound SIP traffic
                   as the traffic would be droped if the IN partial list
                   doesn't have the dest IP/Port as NAT IP/Port */

                /* delete the hash node if exists */
                if (NatDelWanUaPortHashEntry (pHeaderInfo->u4OutIpAddr,
                                              pHeaderInfo->u2OutPort,
                                              NAT_DEL_ALL_HASH_BINDINGS,
                                              (UINT2) pHeaderInfo->u1PktType)
                    != NAT_SUCCESS)
                {
                    /* No need to throw an error as the hash node
                     * wouldnt have been created & as we have one
                     * more chance while deleting the partial
                     * entry */
                    NAT_TRC2 (NAT_TRC_ON, "Not able to delete the "
                              "hash node OUT IP %u, OUT  Port %d \n ",
                              pHeaderInfo->u4OutIpAddr, pHeaderInfo->u2OutPort);

                    NAT_DBG2 (NAT_DBG_ALL, "Not able to delete the "
                              "hash node OUT IP %u, OUT Port %d \n ",
                              pHeaderInfo->u4OutIpAddr, pHeaderInfo->u2OutPort);
                }

                *pu1AppCallStatus = pNatPartialLinkNode->u1AppCallStatus;
                *pu1Type = NAT_IN_PARTIAL;

                /* returning FAILURE, explicitely */
                return NAT_SUCCESS;

            }
        }

        /* If entry is not found in partial table and Nat is disabled, 
         * traslated port and IP should be same as Local Port and IP */
        if (pNatPartialLinkNode == NULL)
        {
            if ((gu4NatEnable == NAT_DISABLE) ||
                (NatCheckIfNatEnable (pHeaderInfo->u4IfNum) == NAT_DISABLE))
            {
                *pu4InIpAddr = pHeaderInfo->u4InIpAddr;
                *pu2InPort = pHeaderInfo->u2InPort;
                return NAT_SUCCESS;
            }
        }

        if ((pNatPartialLinkNode != NULL)
            && (pNatPartialLinkNode->u1PersistFlag == NAT_NON_PERSISTENT))
        {
            TMO_SLL_Delete (&gNatPartialLinksList,
                            &(pNatPartialLinkNode->sNatPartialLinkNode));
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);
            u4ConnectionStatus = NAT_SUCCESS;
        }
    }

    return (u4ConnectionStatus);

}

tNatPartialLinkNode *
NatGetAppInPartialEntry (UINT4 u4LocIpAddr, UINT2 u2LocPort, UINT4 u4Direction,
                         UINT1 u1PktType)
{

    tNatPartialLinkNode *pNatPartialLinkNode = NULL;

    TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                  tNatPartialLinkNode *)
    {
        if ((pNatPartialLinkNode->u4LocIpAddr ==
             u4LocIpAddr)
            && (pNatPartialLinkNode->u2LocPort ==
                u2LocPort)
            && (pNatPartialLinkNode->u1Direction ==
                u4Direction) && (pNatPartialLinkNode->u1PktType == u1PktType))
        {

            /* 
             * If there is match found for persistent partial link
             * then update the time stamp to set it again to
             * NAT_PERSIST_PARTIAL_LINK_TIMEOUT
             */

            if (pNatPartialLinkNode->u1PersistFlag == NAT_PERSISTENT)
            {
                NAT_GET_SYS_TIME (&(pNatPartialLinkNode->u4TimeStamp));
            }

            break;

        }
    }

    return pNatPartialLinkNode;
}

/***************************************************************************
* Function Name:  NatSearchNatOnlyTable
* Description  : This function searches the Nat Only table for the presence
*                of local IP address and Global IP address mapping
*                depending on the interface number. If such an entry is
*                present the corresponding node pointer is returned.
*
* Input (s)    :  1. u4IpAddr - The IP address to be searched.
*                 2. u4IfNum - The interface number on which to be
*                    alloted.
*
* Output (s)   :  None
* Returns      :  Pointer to the Node
*
****************************************************************************/

PUBLIC tNatOnlyNode *
NatSearchNatOnlyTable (UINT4 u4IpAddr, UINT4 u4IfNum)
{
    tNatOnlyNode       *pNatOnlyNode = NULL;
    UINT4               u4HaskKey = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;

    u4TranslatedLocIpAddr = NAT_ZERO;
    pNatOnlyNode = NULL;

    u4HaskKey = (u4IpAddr & NAT_ONLY_HASH_MASK) % NAT_ONLY_HASH_LIMIT;
    TMO_HASH_Scan_Bucket (gpNatOnlyHashList, u4HaskKey, pNatOnlyNode,
                          tNatOnlyNode *)
    {
        if ((pNatOnlyNode->u4IfNum == u4IfNum)
            && (pNatOnlyNode->u4LocIpAddr == u4IpAddr))
        {
            u4TranslatedLocIpAddr = pNatOnlyNode->u4TranslatedLocIpAddr;
            break;
        }
    }
    if (u4TranslatedLocIpAddr != NAT_ZERO)
    {
        u4TranslatedLocIpAddr = NAT_ZERO;
    }

    return (pNatOnlyNode);
}

/***************************************************************************
* Function Name    :  NatAddNatOnlyTable
* Description    :  This functions adds the mapping in the NatOnlyTable.
*
* Input (s)    :  1. u4LocIpAddr - The Local IP address to be added.
*             2. u4TranslatedLocIpAddr - The global address to mapped to the
*            u4LocIpAddr.
*             3. u4IfNum - The interface number on which the
*            connections is existing.
*             4. u1MulticastFlag - States whether the above information
*            is for a multicast packet or not.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC UINT4
NatAddNatOnlyTable (UINT4 u4LocIpAddr, UINT4 u4TranslatedLocIpAddr,
                    UINT4 u4IfNum, UINT1 u1MulticastFlag)
{

    UINT4               u4HashKey = NAT_ZERO;
    tNatOnlyNode       *pNatOnlyNode = NULL;

    if (NAT_MEM_ALLOCATE (NAT_ONLY_POOL_ID, pNatOnlyNode, tNatOnlyNode) != NULL)
    {
        pNatOnlyNode->u4LocIpAddr = u4LocIpAddr;
        pNatOnlyNode->u4TranslatedLocIpAddr = u4TranslatedLocIpAddr;
        pNatOnlyNode->u4IfNum = u4IfNum;
        pNatOnlyNode->u4NoOfConnections = NAT_ONE;
        if (u1MulticastFlag == NAT_IP_MULTICAST)
        {
            pNatOnlyNode->u1MulticastFlag = NAT_TRUE;
            NAT_GET_SYS_TIME (&(pNatOnlyNode->u4TimeStamp));
        }
        else
        {
            pNatOnlyNode->u1MulticastFlag = NAT_ZERO;
            pNatOnlyNode->u4TimeStamp = NAT_ZERO;
        }

        u4HashKey = (u4LocIpAddr & NAT_ONLY_HASH_MASK) % NAT_ONLY_HASH_LIMIT;
        TMO_HASH_Add_Node (gpNatOnlyHashList, &(pNatOnlyNode->NatOnlyNode),
                           u4HashKey, NULL);
        return (NAT_SUCCESS);

    }

    return (NAT_FAILURE);
}

/***************************************************************************
* Function Name    :  NatSearchStaticTable
* Description    :  This function searches the Static table for the presence
*             of local IP address or Global IP address depending on the
*             direction. If it is present, the corresponding mapping
*             is returned.
*
* Input (s)    :  1. u4IpAddr - The IP address to be searched.
*             2. u4Direction - Tells the direction of the packet.
*             Based on this the value stored in the u4IpAddr is changed.
*             3. u4IpAddr - The interface number whose static mapping
*             should be searched.
*
* Output (s)    :  None
* Returns      :  Local IP Address - Direction is NAT_INBOUND
*             Global IP Address - Direction is NAT_OUTBOUND
*             0 - no mapping exists.
****************************************************************************/

PUBLIC UINT4
NatSearchStaticTable (UINT4 u4IpAddr, UINT4 u4Direction, UINT4 u4IfNum)
{
    tStaticEntryNode   *pStaticNode = NULL;
    UINT4               u4StaticIpAddr = NAT_ZERO;
    tInterfaceInfo     *pNode = NULL;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatSearchStaticTable ");

    u4StaticIpAddr = NAT_ZERO;
    if (u4IfNum > (NAT_MAX_NUM_IF))
    {
        return u4StaticIpAddr;
    }

    pNode = gapNatIfTable[u4IfNum];
    if (pNode == NULL)
    {
        return (u4StaticIpAddr);
    }

    NAT_TRC (NAT_TRC_ON, "\n Inside NatSearchStaticTable ");
    if (NAT_OUTBOUND == u4Direction)
    {
        TMO_SLL_Scan (&pNode->StaticList, pStaticNode, tStaticEntryNode *)
        {
            if ((pStaticNode->u4LocIpAddr == u4IpAddr)
                && (pStaticNode->i4RowStatus == NAT_STATUS_ACTIVE))
            {
                u4StaticIpAddr = pStaticNode->u4TranslatedLocIpAddr;
                break;
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&pNode->StaticList, pStaticNode, tStaticEntryNode *)
        {
            if ((pStaticNode->u4TranslatedLocIpAddr == u4IpAddr)
                && (pStaticNode->i4RowStatus == NAT_STATUS_ACTIVE))
            {
                u4StaticIpAddr = pStaticNode->u4LocIpAddr;
                break;
            }
        }
    }
    NAT_DBG2 (NAT_DBG_TABLE, "\n Static Mapping for %x is %x \n", u4IpAddr,
              u4StaticIpAddr);

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatSearchStaticTable \n");
    return (u4StaticIpAddr);
}

/***************************************************************************
* Function Name  :  NatAddPartialLinksList
* Description    :  This function adds partial links needed for dynamic 
*                   data connections to happen.                        
*
* Input (s)    :  1. pHeaderInfo - Contains the required header information
*                 2. u4LocIpAddr - Contains the local IP address of the
*                    connection.
*                 3. u2LocPort - Contains the local port going to be
*                    contacted by the reply packet for this connection.
*
* Output (s)   :  None
* Returns      :  None
*
****************************************************************************/
PUBLIC UINT4
NatAddPartialLinksList (tHeaderInfo * pHeaderInfo, UINT4 u4IpAddr,
                        UINT2 u2Port, UINT1 u1PersistFlag)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT4               u4Status = NAT_ZERO;
    UINT4               u4CurrTime = NAT_ZERO;
#ifdef FIREWALL_WANTED
    tPartialInfo        sPartialInfo;
#endif

    u4Status = NAT_FAILURE;
    NAT_GET_SYS_TIME (&u4CurrTime);

    TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                  tNatPartialLinkNode *)
    {
        if ((pNatPartialLinkNode->u4LocIpAddr == u4IpAddr)
            && (pNatPartialLinkNode->u2LocPort == u2Port)
            && (pNatPartialLinkNode->u4OutIpAddr == pHeaderInfo->u4OutIpAddr))
        {
            if (pNatPartialLinkNode->u2TranslatedLocPort
                != pHeaderInfo->u2InPort)
            {
                /*
                 * We must Release Global port, only when NAPT
                 * is enabled.
                 */
                if (NatCheckIfNaptEnable (pHeaderInfo->u4IfNum) == NAT_ENABLE)
                {
                    if(NatReleaseGlobalPort (pNatPartialLinkNode->
                                          u2TranslatedLocPort) == NAT_FAILURE)
                    {
                        NAT_DBG1(NAT_DBG_TABLE,
                                 "\nUnable to release the port %d \n",
                                 pNatPartialLinkNode->u2TranslatedLocPort);
                    }    
                    pNatPartialLinkNode->u2TranslatedLocPort =
                        pHeaderInfo->u2InPort;
                }
            }
            u4Status = NAT_SUCCESS;
            break;
        }
    }

    if (pNatPartialLinkNode == NULL)
    {

        if (NAT_MEM_ALLOCATE (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                              pNatPartialLinkNode, tNatPartialLinkNode) != NULL)
        {
            u4Status = NAT_SUCCESS;
            MEMSET (pNatPartialLinkNode, NAT_ZERO,
                    sizeof (tNatPartialLinkNode));
            /* Adding partial link for INBOUND direction */
            if (pHeaderInfo->u4Direction == NAT_INBOUND)
            {
                pNatPartialLinkNode->u4TranslatedLocIpAddr =
                    pHeaderInfo->u4InIpAddr;
                pNatPartialLinkNode->u2TranslatedLocPort =
                    pHeaderInfo->u2InPort;
                pNatPartialLinkNode->u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
                pNatPartialLinkNode->u4LocIpAddr = u4IpAddr;
                pNatPartialLinkNode->u2LocPort = u2Port;
                pNatPartialLinkNode->u4TimeStamp = u4CurrTime;
                pNatPartialLinkNode->u1Direction =
                    (UINT1) pHeaderInfo->u4Direction;
                pNatPartialLinkNode->u1PktType = pHeaderInfo->u1PktType;
                pNatPartialLinkNode->u1PersistFlag = u1PersistFlag;
            }
            else if (pHeaderInfo->u4Direction == NAT_OUTBOUND)
            {
                pNatPartialLinkNode->u4LocIpAddr = pHeaderInfo->u4InIpAddr;
                pNatPartialLinkNode->u4OutIpAddr = u4IpAddr;
                pNatPartialLinkNode->u2OutPort = u2Port;
                pNatPartialLinkNode->u1Direction =
                    (UINT1) pHeaderInfo->u4Direction;
                pNatPartialLinkNode->u1PktType = pHeaderInfo->u1PktType;
                pNatPartialLinkNode->u1PersistFlag = u1PersistFlag;
                pNatPartialLinkNode->u4TimeStamp = u4CurrTime;
            }

#ifdef FIREWALL_WANTED
            MEMSET (&sPartialInfo, NAT_ZERO, sizeof (tPartialInfo));

            sPartialInfo.RemoteIP.v4Addr = pNatPartialLinkNode->u4OutIpAddr;
            sPartialInfo.RemoteIP.u4AddrType = IP_VERSION_4;
            sPartialInfo.u2RemotePort = pNatPartialLinkNode->u2OutPort;
            sPartialInfo.LocalIP.v4Addr = pNatPartialLinkNode->u4LocIpAddr;
            sPartialInfo.LocalIP.u4AddrType = IP_VERSION_4;
            sPartialInfo.u2LocalPort = pNatPartialLinkNode->u2LocPort;
            sPartialInfo.u1Direction = pNatPartialLinkNode->u1Direction;
            sPartialInfo.u1Protocol = pNatPartialLinkNode->u1PktType;
            sPartialInfo.u1PersistFlag = pNatPartialLinkNode->u1PersistFlag;
            sPartialInfo.u2InterfaceNum = (UINT2) pHeaderInfo->u4IfNum;
            FwlAddPartialLink (&sPartialInfo);
#endif

            /* Add partial link for NAT only if direction is INBOUND */
            if ((pHeaderInfo->u4Direction == NAT_INBOUND))
            {

                TMO_SLL_Add (&gNatPartialLinksList,
                             &(pNatPartialLinkNode->sNatPartialLinkNode));
            }
            else                /* Delete node created for OUTBOUND link */
            {
                NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                       (UINT1 *) pNatPartialLinkNode);
            }
        }
    }
    return (u4Status);
}

/***************************************************************************
* Function Name    :  NatCreateDnsTable
* Description    :  This function assigns a global IP address for a new
*             local host when the connection is initiated by an Outside
*             host using DNS.
*
* Input (s)    :  1. u4IpAddr - The local host which need a global IP
*            address.
*             2. u4IfNum - The interface number from which the
*            connection is required.
*
*
* Output (s)    :  None
* Returns      :  Global IP address or 0 
*
****************************************************************************/

PUBLIC UINT4
NatCreateDnsTable (UINT4 u4IpAddr, UINT4 u4IfNum)
{
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    tDnsEntryNode      *pDnsNode = NULL;
    tNatOnlyNode       *pNatOnlyNode = NULL;

    /*
     * this function creates an entry in the DNS table .The Entry contains the
     * local IP address of the DNS response from the Inside Network and the
     * corresponding Global IP to which the response Local IP is translated.
     * For a given Local IP address the Trans IP addr is searched for in the
     * Static Table and the Dynamic NatOnlyNat Table.If the mapping is not
     * present in the 2 tables then a new IP addr is assigned to the Local IP.
     * And this Trans IP is stored in the DNS Table.
     */
    u4TranslatedLocIpAddr = NAT_ZERO;
    if ((u4TranslatedLocIpAddr = NatGetTransIpFromStaticNaptTbl
         (u4IfNum, u4IpAddr)) != NAT_ZERO)
    {
        return (u4TranslatedLocIpAddr);
    }
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatCreateDnsTable \n");
    /* Searches the static table for the presence of static mapping */
    if (NAT_MEM_ALLOCATE (NAT_DNS_LIST_POOL_ID, pDnsNode, tDnsEntryNode) !=
        NULL)
    {
        u4TranslatedLocIpAddr =
            NatSearchStaticTable (u4IpAddr, NAT_OUTBOUND, u4IfNum);
        if (u4TranslatedLocIpAddr == NAT_ZERO)
        {
            pNatOnlyNode = NatSearchNatOnlyTable (u4IpAddr, u4IfNum);
            if (pNatOnlyNode != NULL)
            {
                u4TranslatedLocIpAddr = pNatOnlyNode->u4TranslatedLocIpAddr;
                pNatOnlyNode->u4NoOfConnections++;
            }
            /*
             * Assigns a global IP addresses. If 0 is returned
             * it means there are no free Global IP address.
             */
            if (u4TranslatedLocIpAddr == NAT_ZERO)
            {
                u4TranslatedLocIpAddr = NatGetGlobalIpForDns (u4IfNum);
                if (u4TranslatedLocIpAddr != NAT_ZERO)
                {
                    NatAddNatOnlyTable (u4IpAddr, u4TranslatedLocIpAddr,
                                        u4IfNum, NAT_ZERO);
                }
            }

            if (u4TranslatedLocIpAddr != NAT_ZERO)
            {
                pDnsNode->u4LocIpAddr = u4IpAddr;
                pDnsNode->u4TranslatedLocIpAddr = u4TranslatedLocIpAddr;
                TMO_SLL_Add (&gNatDnsList, &(pDnsNode->DnsTable));
            }
            else
            {
                NatMemReleaseMemBlock (NAT_DNS_LIST_POOL_ID,
                                       (UINT1 *) pDnsNode);
            }
        }
        else
        {
            NatMemReleaseMemBlock (NAT_DNS_LIST_POOL_ID, (UINT1 *) pDnsNode);
        }

        NAT_DBG2 (NAT_DBG_TABLE, "\n DNS Mapping for LIP %d is %d \n", u4IpAddr,
                  u4TranslatedLocIpAddr);
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCreateDnsTable \n");
    return (u4TranslatedLocIpAddr);

}

/***************************************************************************
* Function Name    :  NatSearchDnsTable
* Description    :  This functions searches the DNS table for the presence
*             of a mapping for a global IP. If found the corresponding
*             Local IP is returned.
*
* Input (s)    : u4TranslatedLocIp - This is the Global IP address whose mapping
*            is searched for.
*
*
* Output (s)    :  None
* Returns      :  Local IP Address or 0
*
****************************************************************************/

PRIVATE UINT4
NatSearchDnsTable (UINT4 u4TranslatedLocIp)
{
    tDnsEntryNode      *pNode = NULL;
    UINT4               u4LocIpAddr = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchDnsTable \n");
    TMO_SLL_Scan (&gNatDnsList, pNode, tDnsEntryNode *)
    {
        if (pNode->u4TranslatedLocIpAddr == u4TranslatedLocIp)
        {
            u4LocIpAddr = pNode->u4LocIpAddr;
            /*
             * When the required mapping is got the entry is deleted 
             * from the DNS table ensuring that it is not used again by some
             * other INBOUND session.
             */
            TMO_SLL_Delete (&gNatDnsList, &(pNode->DnsTable));
            NatMemReleaseMemBlock (NAT_DNS_LIST_POOL_ID, (UINT1 *) pNode);
            break;
        }
    }

    if (pNode == NULL)
    {
        u4LocIpAddr = NAT_ZERO;
    }
    NAT_DBG2 (NAT_DBG_TABLE, "\n DNS Mapping for GIP %d is %d \n",
              u4TranslatedLocIp, u4LocIpAddr);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchDnsTable \n");
    return (u4LocIpAddr);
}

/***************************************************************************
* Function Name : NatLookUpDnsTable
* Description   : This functions searches the DNS table for the presence
*                 of a mapping for a global IP. If found the timestamp is
*                 checked. If it has expired, then 0 is returned, else the
*                 corresponding local address is returned.
*
* Input (s)    :  u4IpAddr - The IP Address to be checked.
*
* Output (s)    :  None
* Returns      :  Local IP address or 0
*
****************************************************************************/

PUBLIC UINT4
NatLookUpDnsTable (UINT4 u4IpAddr)
{
    UINT4               u4CurrTime = NAT_ZERO;
    UINT4               u4LocIpAddr = NAT_ZERO;
    tDnsEntryNode      *pNode = NULL;
    tDnsEntryNode      *pPrevNode = NULL;
    NAT_GET_SYS_TIME (&u4CurrTime);

    TMO_SLL_Scan (&gNatDnsList, pNode, tDnsEntryNode *)
    {
        if (pNode->u4TranslatedLocIpAddr == u4IpAddr)
        {
            if ((u4CurrTime - pNode->u4BindTimeStamp) > gu4NatUdpTimeOut)
            {
                TMO_SLL_Delete (&gNatDnsList, &(pNode->DnsTable));
                NatMemReleaseMemBlock (NAT_DNS_LIST_POOL_ID, (UINT1 *) pNode);
                pNode = pPrevNode;
            }
            else
            {
                u4LocIpAddr = NAT_ONE;
            }
        }
        pPrevNode = pNode;
    }
    return (u4LocIpAddr);
}

/***************************************************************************
* Function Name    :  NatSearchNfsTable
* Description    :  This function searches for the NFS connection information
*             for the NFS packet arrived. If present, the pHeaderInfo
*             should be updated else an entry should be created.
*
* Input (s)    :  pHeaderInfo - Contains the packet connection information
*
* Output (s)    :  updates teh pHeaderInfo
* Returns      :  NAT_SUCCESS or NAT_FAILUE
*
****************************************************************************/

/*
 *  Note - For NFS sessions the session entries are stored in a seperate linked
 *  list ,not in the dynamic table where other sessions are stored.
 *  This function searches the NFS table to find a match. If a match is found
 *  then it updates the HeaderInfo else creates a new session entry. Also the
 *  search is based on direction. For OUTBOUND packets search is done based 
 *  on LIP, LP ,OIP and OP whereas for INBOUND packets it is done based on 
 *  GIP, GP, OIP and OP
 */
PUBLIC UINT4
NatSearchNfsTable (tHeaderInfo * pHeaderInfo)
{
    tDynamicEntry      *pNfsEntryNode = NULL;
    UINT4               u4NfsStatus = NAT_FAILURE;
    UINT4               u4ConnectionStatus = NAT_FAILURE;

    /* Check for the presence of an entry in the NFS Table for this packet */
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatSearchNfsTable \n");
    if (NAT_OUTBOUND == pHeaderInfo->u4Direction)
    {
        TMO_SLL_Scan (&gNatNfsList, pNfsEntryNode, tDynamicEntry *)
        {
            if ((pNfsEntryNode->u4LocIpAddr == pHeaderInfo->u4InIpAddr) &&
                (pNfsEntryNode->u2LocPort == pHeaderInfo->u2InPort) &&
                (pNfsEntryNode->u4OutIpAddr == pHeaderInfo->u4OutIpAddr) &&
                (pNfsEntryNode->u2OutPort == pHeaderInfo->u2OutPort))
            {

                pHeaderInfo->u4InIpAddr = pNfsEntryNode->u4TranslatedLocIpAddr;
                pHeaderInfo->u2InPort = pNfsEntryNode->u2TranslatedLocPort;
                u4NfsStatus = NAT_SUCCESS;
                break;
            }
        }
    }
    else
    {
        TMO_SLL_Scan (&gNatNfsList, pNfsEntryNode, tDynamicEntry *)
        {
            if ((pNfsEntryNode->u4TranslatedLocIpAddr ==
                 pHeaderInfo->u4InIpAddr)
                && (pNfsEntryNode->u2TranslatedLocPort == pHeaderInfo->u2InPort)
                && (pNfsEntryNode->u4OutIpAddr == pHeaderInfo->u4OutIpAddr)
                && (pNfsEntryNode->u2OutPort == pHeaderInfo->u2OutPort))
            {
                pHeaderInfo->u4InIpAddr = pNfsEntryNode->u4LocIpAddr;
                pHeaderInfo->u2InPort = pNfsEntryNode->u2LocPort;
                u4NfsStatus = NAT_SUCCESS;
                break;
            }
        }
    }

    if (pNfsEntryNode != NULL)
    {
        pHeaderInfo->pDynamicEntry = pNfsEntryNode;
        return (u4NfsStatus);
    }

    if (NatIsNfs (pHeaderInfo) == NAT_FALSE)
    {
        return NAT_FORWARD;
    }

    /* If the destination port is nfs port and entry is not present in
     * nfs list, before creating verify dynamic list to confirm that
     * this packet is not a response of some non-nfs request */
    u4ConnectionStatus = NatGetDynamicEntry (pHeaderInfo, NAT_SEARCH);

    if (u4ConnectionStatus == NAT_SUCCESS)
    {
        return NAT_SUCCESS;
    }

    u4NfsStatus = NatCreateNfsTableEntry (pHeaderInfo);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSearchNfsTable \n");
    return (u4NfsStatus);
}

/***************************************************************************
* Function Name    :  NatCreateNfsTableEntry
* Description    :  This function creates an NFS entry in the NFS table.
*
* Input (s)    :  pHeaderInfo - Contains connection information
*
* Output (s)    :  updates the pHeaderInfo
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE UINT4
NatCreateNfsTableEntry (tHeaderInfo * pHeaderInfo)
{
    tDynamicEntry      *pNfsEntryNode = NULL;
    tGlobalInfo         NfsGlobalInfo;
    tNfsIpNode         *pNfsIpNode = NULL;
    tStaticNaptEntry   *pStaticNaptEntry = NULL;
    tNatPartialLinkNode *pInPartialNode = NULL;
    UINT4               u4IpAddr = NAT_ZERO;
    UINT4               u4CreateStatus = NAT_FAILURE;
    UINT2               u2Port = NAT_ZERO;

    MEMSET (&NfsGlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatCreateNfsTableEntry\n");

    if (NAT_MEM_ALLOCATE (NAT_DYNAMIC_POOL_ID, pNfsEntryNode, tDynamicEntry) !=
        NULL)
    {
        u4CreateStatus = NAT_SUCCESS;
        MEMSET (pNfsEntryNode, NAT_ZERO, sizeof (tDynamicEntry));
        pNfsEntryNode->u4OutIpAddr = pHeaderInfo->u4OutIpAddr;
        pNfsEntryNode->u2OutPort = pHeaderInfo->u2OutPort;
        pNfsEntryNode->u4IfNum = pHeaderInfo->u4IfNum;

        u4IpAddr = pHeaderInfo->u4InIpAddr;
        u2Port = pHeaderInfo->u2InPort;

        if (NAT_OUTBOUND == pHeaderInfo->u4Direction)
        {

            /* For outbound packet, assign a Global IP address & port. */
            pNfsEntryNode->u4LocIpAddr = pHeaderInfo->u4InIpAddr;
            pNfsEntryNode->u2LocPort = pHeaderInfo->u2InPort;

            pStaticNaptEntry = NatGetStaticNaptEntry (&u4IpAddr, &u2Port,
                                                      pHeaderInfo->
                                                      u4Direction,
                                                      pHeaderInfo->u4IfNum,
                                                      (UINT2) pHeaderInfo->
                                                      u1PktType);

            if (pStaticNaptEntry == NULL)
            {

                if ((pInPartialNode = NatGetAppInPartialEntry (u4IpAddr,
                                                               u2Port,
                                                               NAT_INBOUND,
                                                               pHeaderInfo->
                                                               u1PktType))
                    != NULL)

                {
                    NfsGlobalInfo.u4TranslatedLocIpAddr =
                        pInPartialNode->u4TranslatedLocIpAddr;
                    NfsGlobalInfo.u2TranslatedLocPort =
                        pInPartialNode->u2TranslatedLocPort;
                }
                else
                {
                    NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                                pHeaderInfo->u2InPort,
                                                pHeaderInfo->u4OutIpAddr,
                                                pHeaderInfo->u2OutPort,
                                                pHeaderInfo->u4IfNum,
                                                &NfsGlobalInfo);
                }

            }
            else
            {
                NfsGlobalInfo.u4TranslatedLocIpAddr = u4IpAddr;
                NfsGlobalInfo.u2TranslatedLocPort = u2Port;
            }

            if (NfsGlobalInfo.u4TranslatedLocIpAddr != NAT_ZERO)
            {
                pNfsEntryNode->u4TranslatedLocIpAddr =
                    NfsGlobalInfo.u4TranslatedLocIpAddr;
                pHeaderInfo->u4InIpAddr = NfsGlobalInfo.u4TranslatedLocIpAddr;
                if (NfsGlobalInfo.u2TranslatedLocPort != NAT_ZERO)
                {
                    pNfsEntryNode->u2TranslatedLocPort =
                        NfsGlobalInfo.u2TranslatedLocPort;
                    pHeaderInfo->u2InPort = NfsGlobalInfo.u2TranslatedLocPort;
                }
                else
                {
                    pNfsEntryNode->u2TranslatedLocPort = pHeaderInfo->u2InPort;
                }
            }
            else
            {
                u4CreateStatus = NAT_FAILURE;
            }
        }
        else
        {

            /*
             *  For INBOUND packets search the Dns Table and get the mapping.
             *  If entry not present, search the Static Table. If entry is not
             *  present there too, then search the NfsIpList
             *  If entry not present then return NAT_FAILURE.
             *  If entry is present then create the session entry in the NFS
             *  Table.
             */

            pNfsEntryNode->u4TranslatedLocIpAddr = pHeaderInfo->u4InIpAddr;
            pNfsEntryNode->u2TranslatedLocPort = pHeaderInfo->u2InPort;
            pNfsEntryNode->u4LocIpAddr =
                NatSearchDnsTable (pHeaderInfo->u4InIpAddr);

            if (pNfsEntryNode->u4LocIpAddr != NAT_ZERO)
            {
                TMO_SLL_Scan (&gNatNfsIpList, pNfsIpNode, tNfsIpNode *)
                {
                    if (pNfsIpNode->u4TranslatedLocIpAddr ==
                        pNfsEntryNode->u4TranslatedLocIpAddr)
                    {
                        break;
                    }
                }
                if (pNfsIpNode == NULL)
                {
                    if (NAT_MEM_ALLOCATE (NAT_NFS_IP_LIST_POOL_ID,
                                          pNfsIpNode, tNfsIpNode) != NULL)
                    {
                        u4CreateStatus = NAT_SUCCESS;
                        TMO_SLL_Add (&gNatNfsIpList, &(pNfsIpNode->DnsTable));
                        pNfsEntryNode->u2LocPort = pHeaderInfo->u2InPort;
                        pHeaderInfo->u4InIpAddr = pNfsEntryNode->u4LocIpAddr;
                    }
                }
                else
                {
                    u4CreateStatus = NAT_FAILURE;
                }
            }
            else
            {
                if (NatGetStaticNaptEntry (&u4IpAddr, &u2Port,
                                           pHeaderInfo->u4Direction,
                                           pHeaderInfo->u4IfNum,
                                           (UINT2) pHeaderInfo->u1PktType)
                    == NULL)
                {

                    u4IpAddr = NAT_ZERO;
                    u4IpAddr = NatSearchStaticTable (pHeaderInfo->u4InIpAddr,
                                                     pHeaderInfo->u4Direction,
                                                     pHeaderInfo->u4IfNum);
                    if (u4IpAddr == NAT_ZERO)
                    {
                        TMO_SLL_Scan (&gNatNfsIpList, pNfsIpNode, tNfsIpNode *)
                        {
                            if (pNfsIpNode->u4TranslatedLocIpAddr ==
                                pNfsEntryNode->u4TranslatedLocIpAddr)
                            {
                                u4IpAddr = pNfsIpNode->u4LocIpAddr;
                            }
                        }
                    }
                }
                if (u4IpAddr != NAT_ZERO)
                {
                    pNfsEntryNode->u4LocIpAddr = u4IpAddr;
                    pNfsEntryNode->u2LocPort = u2Port;
                    pHeaderInfo->u4InIpAddr = u4IpAddr;
                    pHeaderInfo->u2InPort = u2Port;
                }
                else
                {
                    u4CreateStatus = NAT_FAILURE;
                }
            }
        }
        if (u4CreateStatus == NAT_SUCCESS)
        {
            pHeaderInfo->pDynamicEntry = pNfsEntryNode;
            TMO_SLL_Add (&gNatNfsList, &(pNfsEntryNode->DynamicTableEntry));
            /*
             * Updating the Session Statistics
             */
            gapNatIfTable[pHeaderInfo->u4IfNum]->u4NumOfActiveSessions++;

            /*
             * Start the timer if the Nat timer is not started.This happens when
             * NFS table creats the very first entry for the very first session
             * through NAT after Global NAT Switch is enabled.
             */
            if (gu4NatTmrEnable == NAT_DISABLE)
            {
                gu4NatTmrEnable = NAT_ENABLE;
                /* START TIMER */
                if (NatStartTimer () == NAT_FAILURE)
                {
                    MOD_TRC (gu4NatTrc, ALL_FAILURE_TRC, "NAT",
                             "\n Unable to start Timer \n");
                }
            }
        }
        else
        {
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pNfsEntryNode);
        }
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCreateNfsTableEntry \n");
    return (u4CreateStatus);
}

/***************************************************************************
* Function Name    :  NatFormHashKey
* Description    :  This forms the Hash key depending on the u4Type.
*
* Input (s)    :  1. u4IpAddr - The IP Address of the host
*             2. u2Port - The port used by the host
*             3. u4Type - whether the above two parameters represent
*             Local or Global or Outside IP address and Port.
*
* Output (s)    :  None
* Returns      :  Hash Key
*
****************************************************************************/

PUBLIC UINT4
NatFormHashKey (UINT4 u4IpAddr, UINT2 u2Port, UINT4 u4Type)
{
    UINT4               u4HashValue = NAT_ZERO;

    u4HashValue = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatFormHashKey \n");
    switch (u4Type)
    {

        case NAT_LOCAL:
            /* Hash key formed using 3 bits from IpAddr and 7 from port */
            u2Port = u2Port & NAT_MASK_WITH_VAL_127 /*0x007f */ ;
            u4IpAddr = u4IpAddr & NAT_MASK_WITH_VAL_7 /*0x00000007 */ ;
            u4IpAddr = u4IpAddr << NAT_SEVEN;
            u4HashValue = u2Port | u4IpAddr;
            u4HashValue &= NAT_MASK_WITH_VAL_1023 /*0x000003ff */ ;
            NAT_DBG1 (NAT_DBG_TABLE, "\n LOCAL HASH KEY %d \n", u4HashValue);
            break;
        case NAT_OUTSIDE:
            /* Hash key formed using 5 bits from IpAddr and 5 from port */
            u2Port = u2Port & NAT_MASK_WITH_VAL_031 /*0x001f */ ;
            u4IpAddr = u4IpAddr & NAT_MASK_WITH_VAL_31 /*0x0000001f */ ;
            u4IpAddr = u4IpAddr << NAT_FIVE;
            u4HashValue = u2Port | u4IpAddr;
            u4HashValue &= NAT_MASK_WITH_VAL_1023 /*0x000003ff */ ;
            NAT_DBG1 (NAT_DBG_TABLE, "\n OUTSIDE HASH KEY %d \n", u4HashValue);
            break;
        case NAT_GLOBAL:
            /* Hash key formed using 2 bits from Ipaddr and 8 from port */
            u2Port = u2Port & NAT_MASK_WITH_VAL_255 /*0x00ff */ ;
            u4IpAddr = u4IpAddr & NAT_MASK_WITH_VAL_3 /*0x00000003 */ ;
            u4IpAddr = u4IpAddr << NAT_EIGHT;
            u4HashValue = u2Port | u4IpAddr;
            u4HashValue &= NAT_MASK_WITH_VAL_1023 /*0x000003ff */ ;
            NAT_DBG1 (NAT_DBG_TABLE, "\n GLOBAL HASH KEY %d \n", u4HashValue);
            break;
        default:
            break;
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatFormHashKey \n");
    return u4HashValue;
}

/***************************************************************************
* Function Name    :  NatDeleteEntry
* Description    :  This function deletes the entry which has expired. It
*             also deletes all the references to this entry.
*
* Input (s)    :  pNode - Pointer to the dynamic entry.
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

/*
 * This function is used for deleting the pointers to the given dynamic entry.
 * Only if the No of Entries (usage count) in the hash Nodes is zero the
 * corresponding hash node is deleted. If the IidList in the Global node is
 * empty, the global node is deleted.
 * The following  nodes are deleted (if at all deletion is to be done) -
 * 1.Local Hash node
 * 2.Global Hash node
 * 3.Outside Hash node
 * 4.The node in the gNatDynamicList which points to the session entry.
 * 5.The pointer from NatIidOidArray.
 */
PUBLIC INT4
NatDeleteEntry (tDynamicEntry * pNode)
{
    tLocOutHashNode    *pLocNode = NULL;
    tGlobalHashNode    *pGlobalNode = NULL;
    tLocOutHashNode    *pOutNode = NULL;
    UINT4               u4LocKey = NAT_ZERO;
    UINT4               u4OutKey = NAT_ZERO;
    UINT4               u4GlobalKey = NAT_ZERO;
    UINT4               u4InId = NAT_ZERO;
    UINT4               u4OutId = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    tIidListNode       *pLocIidList = NULL;
    tIidOidArrayDynamicListNode *pDeleteNode = NULL;
    INT4                i4RetVal = NAT_ZERO;
    UINT4               u4PortExists = FALSE;

    u4LocKey = u4OutKey = u4GlobalKey = NAT_ZERO;
    u4InId = u4OutId = NAT_ZERO;
    pDeleteNode = NULL;
    pLocNode = pOutNode = NULL;
    pGlobalNode = NULL;
    pLocIidList = NULL;

    pNode->u1UsedFlag = NAT_FALSE;

    FL_NAT_DEL_HW_ENTRIES (pNode->u4LocIpAddr, pNode->u4OutIpAddr,
                           pNode->u4TranslatedLocIpAddr,
                           pNode->u2LocPort, pNode->u2OutPort,
                           pNode->u2TranslatedLocPort, pNode->u1PktType);

    /* Release the Hash nodes */
    u4LocKey = NatFormHashKey (pNode->u4LocIpAddr, pNode->u2LocPort, NAT_LOCAL);
    u4GlobalKey =
        NatFormHashKey (pNode->u4TranslatedLocIpAddr,
                        pNode->u2TranslatedLocPort, NAT_GLOBAL);
    u4OutKey =
        NatFormHashKey (pNode->u4OutIpAddr, pNode->u2OutPort, NAT_OUTSIDE);

    NAT_DBG (NAT_DBG_EXIT, "\n Entering NatDeleteEntries \n");

    TMO_HASH_Scan_Bucket (gpNatLocHashList, u4LocKey, pLocNode,
                          tLocOutHashNode *)
    {
        if ((pLocNode->u4IpAddr == pNode->u4LocIpAddr) &&
            (pLocNode->u2Port == pNode->u2LocPort))
        {
            pLocNode->u4NoOfEntries--;
            u4InId = pLocNode->u4Id;
            if (pLocNode->u4NoOfEntries == NAT_ZERO)
            {
                TMO_HASH_Delete_Node (gpNatLocHashList,
                                      &(pLocNode->LocOutHash), u4LocKey);
                NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pLocNode);
            }
            break;
        }
    }

    TMO_HASH_Scan_Bucket (gpNatOutHashList, u4OutKey, pOutNode,
                          tLocOutHashNode *)
    {
        if ((pOutNode->u4IpAddr == pNode->u4OutIpAddr) &&
            (pOutNode->u2Port == pNode->u2OutPort))
        {
            pOutNode->u4NoOfEntries--;
            u4OutId = pOutNode->u4Id;
            if (pOutNode->u4NoOfEntries == NAT_ZERO)
            {
                TMO_HASH_Delete_Node (gpNatOutHashList,
                                      &(pOutNode->LocOutHash), u4OutKey);
                NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pOutNode);
                break;
            }
        }
    }

    TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalKey, pGlobalNode,
                          tGlobalHashNode *)
    {
        if ((pGlobalNode->u4TranslatedLocIpAddr == pNode->u4TranslatedLocIpAddr)
            && (pGlobalNode->u2TranslatedLocPort == pNode->u2TranslatedLocPort))
        {
            TMO_SLL_Scan (&pGlobalNode->pIidList, pLocIidList, tIidListNode *)
            {
                if (pLocIidList->u4InId == u4InId)
                {
                    pLocIidList->u4NoOfEntries--;
                    if (pLocIidList->u4NoOfEntries == NAT_ZERO)
                    {
                        TMO_SLL_Delete (&pGlobalNode->pIidList,
                                        &(pLocIidList->GlobalIidList));
                        NatMemReleaseMemBlock (NAT_IID_LIST_POOL_ID,
                                               (UINT1 *) pLocIidList);
                        pLocIidList = NULL;
                    }
                    else
                    {
                        /* Update the flag indicating that some other dynamic 
                         * entry is also using the same translated port */
                        u4PortExists = TRUE;
                    }
                    break;
                }
            }
            if (TMO_SLL_First (&pGlobalNode->pIidList) == NULL)
            {
                TMO_HASH_Delete_Node (gpNatGlobalHashList,
                                      &(pGlobalNode->GlobalHash), u4GlobalKey);
                NatMemReleaseMemBlock (NAT_GLOBAL_HASH_POOL_ID,
                                       (UINT1 *) pGlobalNode);
            }
            break;
        }
    }

    /* Release the Session info from Dynamic Translation Table */
    u4InId = u4InId % NAT_IID_OID_ARRAY_LIMIT;
    u4OutId = u4OutId % NAT_IID_OID_ARRAY_LIMIT;
    /* pHead = ArrayNode.pHead; */
    TMO_SLL_Scan (&gaNatIidOidArray[u4InId][u4OutId], pDeleteNode,
                  tIidOidArrayDynamicListNode *)
    {
        if (pDeleteNode->pDynamicEntry == pNode)
        {
            u4IpAddr = pDeleteNode->pDynamicEntry->u4LocIpAddr;
            u4LocKey = (u4IpAddr & NAT_ONLY_HASH_MASK) % NAT_ONLY_HASH_LIMIT;
            NatDeleteNatOnlyNode (u4LocKey, NAT_FALSE, u4IpAddr,
                                  pDeleteNode->pDynamicEntry->u4IfNum,
                                  pDeleteNode->pDynamicEntry->u1NaptEnable);
            /* There are other dynamic entries using the same port
             * So we should not be freeing it */
            if (u4PortExists == FALSE)
            {
                NatCheckForNapt (pDeleteNode->pDynamicEntry);
            }
            TMO_SLL_Delete (&gNatDynamicList, &(pNode->DynamicTableEntry));
            TMO_SLL_Delete (&gaNatIidOidArray[u4InId][u4OutId],
                            &(pDeleteNode->IidOidArrayDynamicListNode));
            if (pDeleteNode->pDynamicEntry->pAppRec != NULL)
            {
                NatUnRegisterAppln (pDeleteNode->pDynamicEntry->pAppRec);
                pDeleteNode->pDynamicEntry->pAppRec = NULL;
            }

            /* Null check is introduced to fix HDC 45598
             * Done by Rathna Nisha S.S. on 30/9/2008 */
            if (gapNatIfTable[pNode->u4IfNum] != NULL)
            {
                gapNatIfTable[pNode->u4IfNum]->u4NumOfSessionsClosed++;
                gapNatIfTable[pNode->u4IfNum]->u4NumOfActiveSessions--;
            }

            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pDeleteNode->pDynamicEntry);

            NatMemReleaseMemBlock (NAT_ARR_DYNAMIC_NODE_POOL_ID,
                                   (UINT1 *) pDeleteNode);
            break;
        }
    }
    NAT_DBG (NAT_DBG_TABLE,
             "\n Deleted all hash table reference for an Entry\n");

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatDeleteEntry \n");

    return (i4RetVal);

}

/***************************************************************************
* Function Name  :  NatDeleteUdpEntries
* Description    :  This function deletes the expired UDP entries.
*
* Input (s)    :  None
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

/*
 * This functions deletes UDP entries whose timestamp has expired.It deletes the
 * node in the Udp List which points to the session entry being deleted. 
 * For this the current system time is obtained.
 * For deleting the hash nodes corresponding to the session entry
 * DeleteEntry is called.
*/
PUBLIC VOID
NatDeleteUdpEntries (VOID)
{
    tUdpList           *pDeleteNode = NULL;
    tUdpList           *pNextNode = NULL;
    UINT4               u4CurrTime = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatDeleteUdpEntries \n");

    pDeleteNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pDeleteNode != NULL)
    {
        /*if the entry is not used for the last UDP_TIME_OUT units of time then
         * delete the entry.
         */
        NAT_GET_SYS_TIME (&u4CurrTime);

        if ((u4CurrTime - (pDeleteNode->pDynamicEntry)->u4TimeStamp) >=
            gu4NatUdpTimeOut)
        {
            NatDeleteEntry (pDeleteNode->pDynamicEntry);
            pNextNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pDeleteNode->
                                             IidOidArrayDynamicListNode));

            TMO_SLL_Delete (&gNatUdpList,
                            &(pDeleteNode->IidOidArrayDynamicListNode));
            NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pDeleteNode);
        }
        else
        {
            pNextNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pDeleteNode->
                                             IidOidArrayDynamicListNode));
        }
        pDeleteNode = pNextNode;
    }
    NAT_DBG (NAT_DBG_TABLE, "\n Deleted All Expired UDP Entries \n");
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatDeleteUdpEntries \n");

}

/***************************************************************************
* Function Name  :  NatDeleteTcpConnection
* Description    :  This function updates the TCP Deletion Stack and at the
*             same time deletes the expired entries whose FIN bit set
*             packet has arrived.
*
* Input (s)    :  1. pHeaderInfo - Contains the connection information.
*             2. u1AckBit - Whether Ack bit is set or not.
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatDeleteTcpConnection (tHeaderInfo * pHeaderInfo, UINT1 u1AckBit)
{
    UINT4               u4CurrTime = NAT_ZERO;
    tTcpDelNode        *pTcpDelNode = NULL;
    tTcpDelNode        *pNextNode = NULL;
    tDynamicEntry      *pDynamicEntry = NULL;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatDeleteTcpConnection \n");

    pDynamicEntry = NULL;

    NAT_GET_SYS_TIME (&u4CurrTime);
    if (pHeaderInfo != NULL)
    {
        pDynamicEntry = pHeaderInfo->pDynamicEntry;
    }
    if (pDynamicEntry != NULL)
    {
        TMO_SLL_Scan (&gNatTcpDelStack, pTcpDelNode, tTcpDelNode *)
        {
            if (pTcpDelNode->pDynamicEntry == pDynamicEntry)
            {
                if ((pHeaderInfo->u4Direction != pTcpDelNode->u4Direction)
                    && (u1AckBit == NAT_TCP_ACK_BIT_SET))
                {
                    /* According to RFC of TCP/IP MSL can varry
                     * from 15secs-2 minutes, so setting timeout to
                     * 2MS where MSL=15Secs.Timeout is 1 Minute.
                     */
                    pTcpDelNode->u4TimeSpent = u4CurrTime + NAT_IDLE_TIME_OUT;
                    /* Update Time stamp to make sure that the
                       Dynamic entry does not get timed out before 
                       the TCP Del Node */

                }
                break;
            }
        }
        if (pTcpDelNode == NULL)
        {
            NAT_MEM_ALLOCATE (NAT_TCP_STACK_POOL_ID, pTcpDelNode, tTcpDelNode);

            if (pTcpDelNode != NULL)
            {
                pTcpDelNode->pDynamicEntry = pDynamicEntry;

                /* If we only see FIN while closing session mark it to be 
                 * deleted after 4 minutes, this time will be updated to
                 * 1 minute(2MSL) when we see ACK for the FIN in 
                 * reverse direction.
                 */
                pDynamicEntry->u1UsedFlag = NAT_TRUE;

                pTcpDelNode->u4TimeSpent = u4CurrTime + NAT_FIN_TCP_TIME_OUT;
                pTcpDelNode->u4Direction = pHeaderInfo->u4Direction;
                TMO_SLL_Add (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            }
        }
    }
    /* Check for expired entries. If present delete them */
    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);

    while (pTcpDelNode != NULL)
    {
        if ((u4CurrTime > pTcpDelNode->u4TimeSpent) &&
            ((u4CurrTime - pTcpDelNode->u4TimeSpent) > NAT_IDLE_TIME_OUT))
        {
            /*
             * Adds in the NAT Free Global IP List if only NAT was enabled
             * on this connection.
             */
            NatDeleteEntry (pTcpDelNode->pDynamicEntry);

            pNextNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));

            TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));

            NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                   (UINT1 *) pTcpDelNode);

        }
        else
        {
            pNextNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));
        }

        pTcpDelNode = pNextNode;

    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatDeleteTcpConnection \n");

}

/***************************************************************************
* Function Name    :  NatCheckForNapt
* Description    :  This function checks if NAPT is enabled, if yes,
*             the translated port is added in the Free Port List, if not
*             then the Translated IP Address in added in the Free GIP
*             list.
*
* Input (s)    :  pDynamicEntry - Pointer to the dynamic entry.
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/
PRIVATE VOID
NatCheckForNapt (tDynamicEntry * pDynamicEntry)
{

    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    INT4                i4Status = NAT_ZERO;
    UINT4               u4Status = NAT_ZERO;

    /*
     * The translated Port used by UDP or TCP session has to be freed to
     * FreePortList when the UDP/TCP session entry is getting deleted
     * This is done only when NAPT is enabled for that session.
     */
    if ((pDynamicEntry->u1NaptEnable == NAT_ENABLE) &&
        ((pDynamicEntry->u1PktType == NAT_TCP) ||
         (pDynamicEntry->u1PktType == NAT_UDP)))
    {
        if ((NatSearchTransPortInNaptTable (pDynamicEntry->u4IfNum,
                                            pDynamicEntry->
                                            u2TranslatedLocPort) !=
             NAT_SUCCESS) &&
            ((u4Status = NatSearchStaticTable (pDynamicEntry->u4LocIpAddr,
                                               NAT_OUTBOUND,
                                               pDynamicEntry->u4IfNum)) ==
             NAT_ZERO))

        {

            /*
             * Check Whether Translated port is there in Partial List.
             * If it is there, we should not free the Translated port.
             * This Fix is introduced to avoid same Translated port
             * being allocated for two different Partial Entries.
             */
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {
                if (pNatPartialLinkNode->u2TranslatedLocPort ==
                    pDynamicEntry->u2TranslatedLocPort)
                {
                    /* Partial entry is using the same translated port */
                    NAT_TRC1 (NAT_TRC_ON, "\nTransPort is Used In PartialList"
                              "\nUnable To Release Global Port %d\n",
                              pDynamicEntry->u2TranslatedLocPort);
                    NAT_DBG1 (NAT_DBG_TABLE,
                              "\nTransPort is Used In PartialList"
                              "\nUnable To Release Global Port %d\n",
                              pDynamicEntry->u2TranslatedLocPort);
                    return;
                }
            }

            /*
             * changed for inserting free for in the inc.
             * order to the list
             * Check for NAPT enable is already done
             */
            i4Status =
                NatReleaseGlobalPort (pDynamicEntry->u2TranslatedLocPort);
            if (i4Status != NAT_SUCCESS)
            {
                NAT_TRC1 (NAT_TRC_ON, "\n Global Port not released. Port# %u",
                          pDynamicEntry->u2TranslatedLocPort);
            }
        }
    }
}

/***************************************************************************
* Function Name    :  NatDeleteNfsEntry
* Description    :  This function deletes all the expired entries in the
*                   NFS table.
*
* Input (s)    :  None
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/
PUBLIC VOID
NatDeleteNfsEntry (VOID)
{
    tDynamicEntry      *pNfsEntryNode = NULL;
    tDynamicEntry      *pNextNode = NULL;
    UINT4               u4CurrTime = NAT_ZERO;

    pNfsEntryNode = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);

    while (pNfsEntryNode != NULL)
    {
#ifdef FLOWMGR_WANTED
        if (NAT_FL_UTIL_GET_NFS_TIMESTAMP (pNfsEntryNode->u4LocIpAddr,
                                           pNfsEntryNode->u4OutIpAddr,
                                           pNfsEntryNode->u2LocPort,
                                           pNfsEntryNode->u2OutPort)
            == OSIX_SUCCESS)
        {
            NAT_GET_SYS_TIME (&(pNfsEntryNode->u4TimeStamp));
        }
#endif
        pNextNode =
            (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                            &(pNfsEntryNode->
                                              DynamicTableEntry));
        NAT_GET_SYS_TIME (&u4CurrTime);

        if ((u4CurrTime - pNfsEntryNode->u4TimeStamp) >= NAT_NFS_TIME_OUT)
        {
            TMO_SLL_Delete (&gNatNfsList, &(pNfsEntryNode->DynamicTableEntry));

            FL_NAT_DEL_HW_NFS_ENTRIES (pNfsEntryNode->u4LocIpAddr,
                                       pNfsEntryNode->u4OutIpAddr,
                                       pNfsEntryNode->u2LocPort,
                                       pNfsEntryNode->u2OutPort);

            NatCheckForNapt (pNfsEntryNode);
            gapNatIfTable[pNfsEntryNode->u4IfNum]->u4NumOfSessionsClosed++;
            gapNatIfTable[pNfsEntryNode->u4IfNum]->u4NumOfActiveSessions--;
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pNfsEntryNode);

        }
        pNfsEntryNode = pNextNode;
    }
}

/***************************************************************************
* Function Name    :  NatDeleteAllExpiredEntries
* Description    :  This function deletes all the expired entries in the
*                   dynamic table when the Timer goes off.
*
* Input (s)    :  None
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/
PUBLIC VOID
NatDeleteAllExpiredEntries (VOID)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    tDynamicEntry      *pNextNode = NULL;
    UINT4               u4HashIndex = NAT_ZERO;
    UINT4               u4NewTimeout = NAT_ZERO;
    UINT4               u4TimeStamp = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatDeleteAllExpiredEntries \n");

    NAT_GET_SYS_TIME (&u4TimeStamp);
    /*
     * this function is called when the Timer expires.It deletes all the session
     * entries which have not been used for  some pre-specified units of
     * time (this depends on the type of session).
     */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);
    while (pDynamicEntry != NULL)
    {
        pNextNode =
            (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                            &(pDynamicEntry->
                                              DynamicTableEntry));
#ifdef FLOWMGR_WANTED
        if (((pDynamicEntry->u1PktType == NAT_TCP) ||
             (pDynamicEntry->u1PktType == NAT_UDP)) &&
            ((NAT_FL_UTIL_GET_TIMESTAMP (pDynamicEntry->u4LocIpAddr,
                                         pDynamicEntry->u4OutIpAddr,
                                         pDynamicEntry->u2LocPort,
                                         pDynamicEntry->u2OutPort,
                                         pDynamicEntry->u1PktType) ==
              OSIX_SUCCESS) ||
             (NAT_FL_UTIL_GET_TIMESTAMP (pDynamicEntry->u4OutIpAddr,
                                         pDynamicEntry->u4TranslatedLocIpAddr,
                                         pDynamicEntry->u2OutPort,
                                         pDynamicEntry->u2TranslatedLocPort,
                                         pDynamicEntry->u1PktType) ==
              OSIX_SUCCESS)))
        {
            pDynamicEntry->u4TimeStamp = u4TimeStamp;
            pDynamicEntry = pNextNode;
            continue;
        }
        else if ((pDynamicEntry->u1PktType == NAT_PPTP) &&
                 (FL_NAT_GET_PPTP_TIMESTAMP (pDynamicEntry->u4LocIpAddr,
                                             pDynamicEntry->u4OutIpAddr,
                                             pDynamicEntry->
                                             u4TranslatedLocIpAddr,
                                             pDynamicEntry->u2LocPort,
                                             pDynamicEntry->u2OutPort,
                                             pDynamicEntry->
                                             u2TranslatedLocPort) ==
                  OSIX_SUCCESS))
        {
            pDynamicEntry->u4TimeStamp = u4TimeStamp;
            pDynamicEntry = pNextNode;
            continue;
        }
#endif
        pDynamicEntry = pNextNode;
    }

    NatDeleteUdpEntries ();
    NatDeleteNfsEntry ();
    NatDeleteTcpConnection (NULL, NAT_ZERO);
    natDeleteIKEEntries ();
    natDeleteIPSecEntries ();
    NatFreeRsvdTrigInfoEntry ();

    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);
    while (pDynamicEntry != NULL)
    {
        pNextNode =
            (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                            &(pDynamicEntry->
                                              DynamicTableEntry));
#ifdef FLOWMGR_WANTED
        if (((pDynamicEntry->u1PktType == NAT_TCP) ||
             (pDynamicEntry->u1PktType == NAT_UDP)) &&
            ((NAT_FL_UTIL_GET_TIMESTAMP (pDynamicEntry->u4LocIpAddr,
                                         pDynamicEntry->u4OutIpAddr,
                                         pDynamicEntry->u2LocPort,
                                         pDynamicEntry->u2OutPort,
                                         pDynamicEntry->u1PktType) ==
              OSIX_SUCCESS) ||
             (NAT_FL_UTIL_GET_TIMESTAMP (pDynamicEntry->u4OutIpAddr,
                                         pDynamicEntry->u4TranslatedLocIpAddr,
                                         pDynamicEntry->u2OutPort,
                                         pDynamicEntry->u2TranslatedLocPort,
                                         pDynamicEntry->u1PktType) ==
              OSIX_SUCCESS)))
        {
            pDynamicEntry->u4TimeStamp = u4TimeStamp;
            pDynamicEntry = pNextNode;
            continue;
        }
        else if ((pDynamicEntry->u1PktType == NAT_PPTP) &&
                 (FL_NAT_GET_PPTP_TIMESTAMP (pDynamicEntry->u4LocIpAddr,
                                             pDynamicEntry->u4OutIpAddr,
                                             pDynamicEntry->
                                             u4TranslatedLocIpAddr,
                                             pDynamicEntry->u2LocPort,
                                             pDynamicEntry->u2OutPort,
                                             pDynamicEntry->
                                             u2TranslatedLocPort) ==
                  OSIX_SUCCESS))
        {
            pDynamicEntry->u4TimeStamp = u4TimeStamp;
            pDynamicEntry = pNextNode;
            continue;
        }
#endif /* FLOWMGR_WANTED */
        {
            if (pDynamicEntry->u1PktType == NAT_TCP)
            {
                u4NewTimeout = gu4NatTcpTimeOut;
            }
            else
            {
                u4NewTimeout = NAT_PROT_TIME_OUT;
            }
        }

        if ((u4TimeStamp - pDynamicEntry->u4TimeStamp) >= u4NewTimeout)
        {

            if (pDynamicEntry->u1UsedFlag != NAT_TRUE)
            {
                NatDeleteEntry (pDynamicEntry);
            }
        }
        pDynamicEntry = pNextNode;
    }
    /* Deleting entries used for IP Multicast */

    for (u4HashIndex = NAT_ZERO; u4HashIndex < NAT_ONLY_HASH_LIMIT; u4HashIndex++)
    {
        NatDeleteNatOnlyNode (u4HashIndex, NAT_TRUE, NAT_ZERO,
                              NAT_ZERO, NAT_ZERO);
    }

    NatDeletePartialLinksList ();

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatDeleteAllExpiredEntries \n");
}

/***************************************************************************
* Function Name    :  NatDeleteNatOnlyNode
* Description    :  This function deletes all the expired entries in the
*                   NatOnlyTable.
*
* Input (s)    :  1. u4HashIndex - Index in the Hash Table from where the
*            the nodes are to be deleted.
*             2. u1MulticastFlag - Whether the information is for a
*            multicast packet.
*             3. u4LocIpAddr - Depending u1MulticastFlag it contains
*            either the Local IP Address or zero.
*             4. u4IfNum - Interface Number.
*             5. u1NaptEnable - Whether NAPT is enabled or not.
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatDeleteNatOnlyNode (UINT4 u4HashIndex, UINT1 u1MulticastFlag,
                      UINT4 u4LocIpAddr, UINT4 u4IfNum, UINT1 u1NaptEnable)
{
    tNatOnlyNode       *pNatOnlyNode = NULL;
    tNatOnlyNode       *pNextNatOnlyNode = NULL;
    UINT4               u4CurrTime = NAT_ZERO;

    NAT_GET_SYS_TIME (&u4CurrTime);
    pNatOnlyNode =
        (tNatOnlyNode *) TMO_HASH_Get_First_Bucket_Node (gpNatOnlyHashList,
                                                         u4HashIndex);
    while (pNatOnlyNode != NULL)
    {
        /*
         * Checks for either multicast packet in which case the TimeStamp is
         * checked or it should be non-multicast packet with a mapping for
         * LocIpAddr
         */

        if (((u1MulticastFlag == NAT_TRUE)
             && (pNatOnlyNode->u1MulticastFlag == NAT_TRUE)
             && ((u4CurrTime - pNatOnlyNode->u4TimeStamp) >
                 NAT_MULTICAST_TIME_OUT)) || ((u1MulticastFlag == NAT_FALSE)
                                              && (pNatOnlyNode->u4LocIpAddr ==
                                                  u4LocIpAddr)
                                              && (pNatOnlyNode->u4IfNum ==
                                                  u4IfNum)))
        {
            pNextNatOnlyNode =
                (tNatOnlyNode *)
                TMO_HASH_Get_Next_Bucket_Node (gpNatOnlyHashList, u4HashIndex,
                                               &(pNatOnlyNode->NatOnlyNode));
            pNatOnlyNode->u4NoOfConnections--;
            if (pNatOnlyNode->u4NoOfConnections == NAT_ZERO)
            {
                if (u1NaptEnable != NAT_ENABLE)
                {
                    /* Add the free Global Ip to the FreeGipList */
                    NatAddFreeGipList (pNatOnlyNode->u4TranslatedLocIpAddr,
                                       pNatOnlyNode->u4IfNum);
                }
                TMO_HASH_Delete_Node (gpNatOnlyHashList,
                                      &(pNatOnlyNode->NatOnlyNode),
                                      u4HashIndex);
                NatMemReleaseMemBlock (NAT_ONLY_POOL_ID,
                                       (UINT1 *) pNatOnlyNode);
            }
        }
        else
        {
            pNextNatOnlyNode =
                (tNatOnlyNode *)
                TMO_HASH_Get_Next_Bucket_Node (gpNatOnlyHashList, u4HashIndex,
                                               &(pNatOnlyNode->NatOnlyNode));
        }
        pNatOnlyNode = pNextNatOnlyNode;
    }
}

/***************************************************************************
* Function Name    :  NatCheckDynamicList
* Description    :  This function deletes all the  NAT session entries with
*             port number greater than gu4NatInitTranslatedLocPort.
*
* Input (s)    :  u4IfNum  - Interface Number on which NAPT is enabled
*
* Output (s)    :  None
* Returns      :  None
*
****************************************************************************/

PUBLIC VOID
NatCheckDynamicList (UINT4 u4IfNum)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    tUdpList           *pUdpListNode = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    /*
     * Note - This function is called only from the low level routine 
     * which enables  Napt
     */
    /*Delete the UDP which is using Translated Ports session entries */
    pUdpListNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pUdpListNode != NULL)
    {
        if ((pUdpListNode->pDynamicEntry->u4IfNum == u4IfNum) &&
            (pUdpListNode->pDynamicEntry->u2TranslatedLocPort >
             gu4NatInitTranslatedLocPort))
        {
            NatDeleteEntry (pUdpListNode->pDynamicEntry);
            pNextNode =
                TMO_SLL_Next (&gNatUdpList,
                              &(pUdpListNode->IidOidArrayDynamicListNode));
            TMO_SLL_Delete (&gNatUdpList,
                            &(pUdpListNode->IidOidArrayDynamicListNode));
            NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pUdpListNode);
            pUdpListNode = (tUdpList *) pNextNode;
        }
        else
        {
            pUdpListNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pUdpListNode->
                                             IidOidArrayDynamicListNode));
        }
    }
    /*Delete the TCP which is using Translated Ports session entries */
    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);
    while (pTcpDelNode != NULL)
    {
        if ((pTcpDelNode->pDynamicEntry->u4IfNum == u4IfNum) &&
            (pTcpDelNode->pDynamicEntry->u2TranslatedLocPort >
             gu4NatInitTranslatedLocPort))
        {

            NatDeleteEntry (pTcpDelNode->pDynamicEntry);
            pNextNode =
                TMO_SLL_Next (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                   (UINT1 *) pTcpDelNode);
            pTcpDelNode = (tTcpDelNode *) pNextNode;
        }
        else
        {
            pTcpDelNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));
        }
    }

    /*Delete all the dynamic entries which use Translated Port */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {
        if ((pDynamicEntry->u4IfNum == u4IfNum)
            && (pDynamicEntry->u2TranslatedLocPort >
                gu4NatInitTranslatedLocPort))
        {
            pNextNode =
                TMO_SLL_Next (&gNatDynamicList,
                              &(pDynamicEntry->DynamicTableEntry));
            NatDeleteEntry (pDynamicEntry);
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }

    /*Delete all the NFS session entries which use Translated Port */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);

    while (pDynamicEntry != NULL)
    {
        if ((pDynamicEntry->u4IfNum == u4IfNum)
            && (pDynamicEntry->u2TranslatedLocPort >
                gu4NatInitTranslatedLocPort))
        {
            pNextNode =
                TMO_SLL_Next (&gNatNfsList,
                              &(pDynamicEntry->DynamicTableEntry));
            gapNatIfTable[u4IfNum]->u4NumOfSessionsClosed++;
            gapNatIfTable[u4IfNum]->u4NumOfActiveSessions--;
            TMO_SLL_Delete (&gNatNfsList, &(pDynamicEntry->DynamicTableEntry));
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pDynamicEntry);
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }

}

/***************************************************************************
* Function Name  :  NatDeleteDynamicEntry
* Description    :  This function deletes all the entries in the
*             dynamic table for a particular interface if NAT is
*             disabled for that interface.
*
* Input (s)    :  u4IfNum  - Interface Number which has been disabled
*
* Output (s)   :  None
* Returns      :  None
*
***************************************************************************/

PUBLIC VOID
NatDeleteDynamicEntry (UINT4 u4IfNum)
{
    tDynamicEntry      *pDynamicEntry = NULL;
    tUdpList           *pUdpListNode = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    tIPSecListNode     *pDeleteIPSecNode = NULL;
    tIPSecPendListNode *pDeleteIPSecPendNode = NULL;
    tIKEListNode       *pDeleteIKENode = NULL;
    tIPSecListNode     *pNextIPSecNode = NULL;
    tIPSecPendListNode *pNextIPSecPendNode = NULL;
    tIKEListNode       *pNextIKENode = NULL;

    /*
     * This function is called from the low level routine when Global Nat is
     * disabled or Nat is disabled for some interface.
     * This function checks whether the session in this session entry passes 
     * through the given interface .If it is so then delete the session entry.
     */
    /* Delete UDP session entries pertaining to the input interface */
    pUdpListNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pUdpListNode != NULL)
    {
        if (pUdpListNode->pDynamicEntry->u4IfNum == u4IfNum)
        {
            NatDeleteEntry (pUdpListNode->pDynamicEntry);
            pNextNode =
                TMO_SLL_Next (&gNatUdpList,
                              &(pUdpListNode->IidOidArrayDynamicListNode));
            TMO_SLL_Delete (&gNatUdpList,
                            &(pUdpListNode->IidOidArrayDynamicListNode));
            NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pUdpListNode);
            pUdpListNode = (tUdpList *) pNextNode;
        }
        else
        {
            pUdpListNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pUdpListNode->
                                             IidOidArrayDynamicListNode));
        }
    }
/*
 * Here we are adding deletion of TCP nodes using TcpDelStack
 */

    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);
    while (pTcpDelNode != NULL)
    {
        if (pTcpDelNode->pDynamicEntry->u4IfNum == u4IfNum)
        {
            NatDeleteEntry (pTcpDelNode->pDynamicEntry);
            pNextNode =
                TMO_SLL_Next (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                   (UINT1 *) pTcpDelNode);
            pTcpDelNode = (tTcpDelNode *) pNextNode;
        }
        else
        {
            pTcpDelNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));
        }
    }
    /* Delete dynamic session entries pertaining to the input interface */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {
        if (pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatDynamicList,
                              &(pDynamicEntry->DynamicTableEntry));
            NatDeleteEntry (pDynamicEntry);
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }
    /* Delete NFS session entries pertaining to the input interface */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);

    while (pDynamicEntry != NULL)
    {
        if (pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatNfsList,
                              &(pDynamicEntry->DynamicTableEntry));
            /*delete from NfsList */
            TMO_SLL_Delete (&gNatNfsList, &(pDynamicEntry->DynamicTableEntry));

            FL_NAT_DEL_HW_NFS_ENTRIES (pDynamicEntry->u4LocIpAddr,
                                       pDynamicEntry->u4OutIpAddr,
                                       pDynamicEntry->u2LocPort,
                                       pDynamicEntry->u2OutPort);

            gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfSessionsClosed++;
            gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfActiveSessions--;
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pDynamicEntry);
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }

    /*Get the First Node of the IPSec session List */
    pDeleteIPSecNode = (tIPSecListNode *) TMO_SLL_First (&gNatIPSecListNode);
    while (pDeleteIPSecNode != NULL)
    {
        if (pDeleteIPSecNode->pIPSecOutHashNode->u4IfNum == u4IfNum)
            /*
               Get the next node in the IPSec List
               Delete the Hash Node from the IPSec In and Outbound Hash Table
               Delete the Node from the IPSec List
               Release the memory of the IPSec List Node
               Assign the next node of the IPSec List as the current Node    
               Increment number of closed sessions for the interface
               Decrement number of active sessions for the interface
             */
        {
            /*UT_FIX-START */
            /*NAT_UT_IPSEC_FUNC_FLT_5-START */
            gapNatIfTable[pDeleteIPSecNode->pIPSecOutHashNode->u4IfNum]->
                u4NumOfSessionsClosed++;
            gapNatIfTable[pDeleteIPSecNode->pIPSecOutHashNode->u4IfNum]->
                u4NumOfActiveSessions--;
            /*NAT_UT_IPSEC_FUNC_FLT_5-END */
            /*UT_FIX-END */
            pNextIPSecNode =
                (tIPSecListNode *) TMO_SLL_Next (&gNatIPSecListNode,
                                                 &(pDeleteIPSecNode->
                                                   IPSecList));
            natDeleteIPSecInOutHashNode (pDeleteIPSecNode->pIPSecOutHashNode);
            TMO_SLL_Delete (&gNatIPSecListNode, &(pDeleteIPSecNode->IPSecList));
            NatMemReleaseMemBlock (NAT_IPSEC_LIST_POOL_ID,
                                   (UINT1 *) pDeleteIPSecNode);
            pDeleteIPSecNode = pNextIPSecNode;
        }
        else
        {
            pDeleteIPSecNode =
                (tIPSecListNode *) TMO_SLL_Next (&gNatIPSecListNode,
                                                 &(pDeleteIPSecNode->
                                                   IPSecList));
        }
    }                            /* End of while */

    /* Get the First Node of the IPSec Pending List */
    pDeleteIPSecPendNode =
        (tIPSecPendListNode *) TMO_SLL_First (&gNatIPSecPendListNode);
    while (pDeleteIPSecPendNode != NULL /*List is not NULL */ )
    {
        if (pDeleteIPSecPendNode->pIPSecPendHashNode->u4IfNum == u4IfNum)
        {
            /*
               Get the next node in the IPSec Pending List
               Delete the Hash Node from the IPSec Pending Hash Table
               Delete the Node from the IPSec Pending List
               Release the memory of the IPSec Pending List Node
               Assign the next node of the IPSec Pending List as the current Node    
             */
            pNextIPSecPendNode = (tIPSecPendListNode *) TMO_SLL_Next
                (&gNatIPSecPendListNode,
                 &(pDeleteIPSecPendNode->IPSecPendList));
            natDeleteIPSecPendHashNode (pDeleteIPSecPendNode->
                                        pIPSecPendHashNode);
            TMO_SLL_Delete (&gNatIPSecPendListNode,
                            &(pDeleteIPSecPendNode->IPSecPendList));
            NatMemReleaseMemBlock (NAT_IPSEC_PEND_LIST_POOL_ID,
                                   (UINT1 *) pDeleteIPSecPendNode);
            pDeleteIPSecPendNode = pNextIPSecPendNode;
        }
        else
        {
            pDeleteIPSecPendNode = (tIPSecPendListNode *) TMO_SLL_Next
                (&gNatIPSecPendListNode,
                 &(pDeleteIPSecPendNode->IPSecPendList));
        }
    }                            /* End of while */

    /* Get the First Node of the IKE session List */
    pDeleteIKENode = (tIKEListNode *) TMO_SLL_First (&gNatIKEList);
    while (pDeleteIKENode != NULL)
    {
        /* Interface number == u4IfNum */
        if (pDeleteIKENode->pIKEHashNode->u4IfNum == u4IfNum)
        {
            /*
               Get the next node in the IKE List
               Delete the Hash Node from the IKE Hash Table
               Delete the Node from the IKE List
               Release the memory of the IKE List Node
               Assign the next node of the IKE List as the current Node    
               Increment number of closed sessions for the interface
               Decrement number of active sessions for the interface
             */
            /* UT_FIX-START */
            /* NAT_UT_IKE_FUNC_FLT_11-START */
            gapNatIfTable[pDeleteIKENode->pIKEHashNode->u4IfNum]->
                u4NumOfSessionsClosed++;
            gapNatIfTable[pDeleteIKENode->pIKEHashNode->u4IfNum]->
                u4NumOfActiveSessions--;
            /* NAT_UT_IKE_FUNC_FLT_11-END */
            /* UT_FIX-END */
            pNextIKENode =
                (tIKEListNode *) TMO_SLL_Next (&gNatIKEList,
                                               &(pDeleteIKENode->IKEList));
            natDeleteIKEHashNode (pDeleteIKENode->pIKEHashNode);
            TMO_SLL_Delete (&gNatIKEList, &(pDeleteIKENode->IKEList));
            NatMemReleaseMemBlock (NAT_IKE_LIST_POOL_ID,
                                   (UINT1 *) pDeleteIKENode);
            pDeleteIKENode = pNextIKENode;
        }
        else
        {
            pDeleteIKENode = (tIKEListNode *) TMO_SLL_Next (&gNatIKEList,
                                                            &(pDeleteIKENode->
                                                              IKEList));
        }
    }                            /* End of while */

}

/* Added for IP Multicasting */

/***************************************************************************
* Function Name    :  NatAddNatOnlyTableForIpMul
* Description    :  This functions adds in the NatOnlyTable for Ip Multicast
*             packets. If an entry is already there, then only the
*             Multicast Flag is set.
*
* Input (s)    :  1. u4LocIpAddr - The Local IP address to be added.
*             2. u4TranslatedLocIpAddr - The global address to mapped
*              to the u4LocIpAddr.
*             3. u4IfNum - The interface number on which the
*            connections is existing.
*
* Output (s)    :  None
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC UINT4
NatAddNatOnlyTableForIpMul (UINT4 u4LocIpAddr, UINT4 u4TranslatedLocIpAddr,
                            UINT4 u4IfNum)
{

    UINT4               u4Status = NAT_ZERO;
    UINT4               u4HashKey = NAT_ZERO;
    tNatOnlyNode       *pNatOnlyNode = NULL;

    /*
     * This function adds the Trans IP address mapping for IP multicast sessions
     */
    u4Status = NAT_FAILURE;

    u4HashKey = (u4LocIpAddr & NAT_ONLY_HASH_MASK) % NAT_ONLY_HASH_LIMIT;

    TMO_HASH_Scan_Bucket (gpNatOnlyHashList, u4HashKey, pNatOnlyNode,
                          tNatOnlyNode *)
    {
        if ((pNatOnlyNode->u4IfNum == u4IfNum)
            && (pNatOnlyNode->u4LocIpAddr == u4LocIpAddr))
        {
            pNatOnlyNode->u1MulticastFlag = NAT_TRUE;
            NAT_GET_SYS_TIME (&(pNatOnlyNode->u4TimeStamp));
            u4Status = NAT_SUCCESS;
            break;
        }
    }
    if (u4Status == NAT_FAILURE)
    {
        u4Status =
            NatAddNatOnlyTable (u4LocIpAddr, u4TranslatedLocIpAddr, u4IfNum,
                                NAT_IP_MULTICAST);
    }
    return (u4Status);
}

/******************************************************************************
 * Function Name   :  NatReleaseDynamicStructs
 * Description     : This function deletes all the Dynamic Entries left out 
 *                  in the  linked lists after Global NAT is disabled.
 *                  Follwing Lists or Tables are being deleted and initialised.
 *           1)gNatFreePortList - This stores the Translated Ports freed .
 *           2)NatPartialLinksList - This stores info for dynamic cpnnections.
 *           3)DnsTable   - This stores translation involving DNS response.
 *
 * Input (s)  : NONE
 * Output (s) : NONE
 * Returns    : NONE
 ***************************************************************************/
PUBLIC VOID
NatReleaseDynamicStructs (VOID)
{

    NatReleaseFromSLL (&gNatFreePortList, NAT_FREE_PORT_POOL_ID);
    NatReleaseFromSLL (&gNatPartialLinksList, NAT_PARTIAL_LINKS_LIST_POOL_ID);
    NatReleaseFromSLL (&gNatDnsList, NAT_DNS_LIST_POOL_ID);
}

/****************************************************************************
 * Function Name :  NatReleaseFromSLL
 * Description   : This function deletes and releases all the nodes from the
 *                 singly linked list.Finally the list is initialised.
 *                 (Note- Here the data structure is only SLL. This function
 *                 should not be called for other data structures.)
 *                 
 * Input (s)  : 1. pList - The singly linked list from which all the nodes
 *                 are removed.
 *              2. MemPoolId - The pool Id of the pool to which the removed
 *                 nodes are released.
 * Output (s) : NONE
 * Returns    : NONE
 *****************************************************************************/
PUBLIC VOID
NatReleaseFromSLL (tTMO_SLL * pList, UINT4 MemPoolId)
{
    tTMO_SLL_NODE      *pDeleteNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;

    pDeleteNode = TMO_SLL_First (pList);
    while (pDeleteNode != NULL)
    {
        pNextNode = TMO_SLL_Next (pList, pDeleteNode);
        TMO_SLL_Delete (pList, pDeleteNode);
        NatMemReleaseMemBlock (MemPoolId, (UINT1 *) pDeleteNode);
        pDeleteNode = pNextNode;

    }
    TMO_SLL_Init (pList);
}

/* Added while adding H323 ALG to support port neg. applications */

/****************************************************************************
* Function Name :  NatCheckPortNegAppln
* Description   : This function checks whether any application has been 
*                 registered with Negotiated ports
*                 
* Input (s)  : 1. u4Protocol - The Protocol that supports the application
*              2. u2Port - The negotiated port
* Output (s) : NONE
* Returns    : Registered application's Record
*****************************************************************************/

PUBLIC tNatAppDefn *
NatCheckPortNegAppln (UINT4 u4Protocol, UINT2 u2Port)
{
    UINT4               u4AppId = NAT_ZERO;
    INT4                i4Count = NAT_ZERO;
    tNatAppDefn        *pApplRec = NULL;

    u4AppId = (u4Protocol << NAT_SIXTEEN) | u2Port;

    for (i4Count = NAT_ZERO; i4Count < NAT_MAX_PORT_NEG_APPLN; i4Count++)
    {
        pApplRec = gapNatNegPortApplnList[i4Count];
        if (pApplRec != NULL)
        {
            if (pApplRec->u4AppId == u4AppId)
            {
                break;
            }
        }
    }

    return (pApplRec);

}

/****************************************************************************
* Function Name :  NatUnRegisterAppln
* Description   : This function removes the neg. port appln from NegPortApplnList
*                 
* Input (s)  : gNatAppl - The application record
* Output (s) : The Application's Record removed from the NegPortApplnList
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatUnRegisterAppln (tNatAppDefn * gNatAppl)
{
    INT4                i4Count = NAT_ZERO;
    tNatAppDefn        *pTmpApplRec = NULL;

    for (i4Count = NAT_ZERO; i4Count < NAT_MAX_PORT_NEG_APPLN; i4Count++)
    {
        pTmpApplRec = gapNatNegPortApplnList[i4Count];

        if (pTmpApplRec != NULL)
        {
            if (pTmpApplRec->u4AppId == gNatAppl->u4AppId)
            {
                gNatAppl->u4AppUseCount--;
                if (gNatAppl->u4AppUseCount == NAT_ZERO)
                {
                    gapNatNegPortApplnList[i4Count] = NULL;
                }
                break;
            }
        }
    }

    return (NAT_SUCCESS);
}

/****************************************************************************
* Function Name :  NatRegisterAppln
* Description   : This function Adds the neg. port appln to NegPortApplnList
*                 
* Input (s)  : 1. gNatAppl - The application record
*            : 2. u4Protocol - Protocol that supports the appln.
*            : 3. u2Port     - Negotiated Port
* Output (s) : The Application's Record Added to the NegPortApplnList
* Returns    : SUCCESS/FAILURE
*****************************************************************************/

PUBLIC INT4
NatRegisterAppln (tNatAppDefn * gNatAppl, UINT4 u4Protocol, UINT2 u2Port,
                  UINT1 u1App)
{
    gNatAppl->u4AppId = (u4Protocol << NAT_SIXTEEN) | u2Port;
    gNatAppl->u4AppUseCount++;
    gapNatNegPortApplnList[u1App] = gNatAppl;

    return (NAT_SUCCESS);
}

/***************************************************************************
* Function Name  :  NatSearchListAndTable
* Description    :  This function searches the TFTP-FTP Table and
*                   Nat-Translation table for the presence of information 
*                   about a connection. 
*
* Input (s)    :  1. pHeaderInfo - The header information about the present
*             connection.
*             2. pu4InIpAddr - This IP address contains the modified
*             data.
*             3. pu2InPort - This contains the modified port.
*
* Output (s)    :  pu4InIpAddr, pu2InPort
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC UINT4
NatSearchListAndTable (tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr,
                       UINT2 *pu2InPort)
{
    UINT4               u4ConnectionStatus = NAT_FAILURE;

    u4ConnectionStatus = NatScanPartialLinksList (pHeaderInfo,
                                                  pu4InIpAddr, pu2InPort);

    if (u4ConnectionStatus == NAT_FAILURE)
    {
        /* Search the Dynamic Translation Table */
        u4ConnectionStatus = NatGetDynamicEntry (pHeaderInfo, NAT_SEARCH);

        if (u4ConnectionStatus == NAT_SUCCESS)
        {
            *pu4InIpAddr = pHeaderInfo->u4InIpAddr;
            *pu2InPort = pHeaderInfo->u2InPort;
        }
    }
    return (u4ConnectionStatus);
}

/***************************************************************************
* Function Name  :  NatScanPartialLinksList
* Description    :  This function scans the TFTP and FTP Table for the
*             presence of information about a  TFTP or FTP
*             connection.
*
* Input (s)    :  1. pHeaderInfo - The header information about the present
*             connection.
*             2. pu4InIpAddr - This IP address contains the modified
*             data.
*             3. pu2InPort - This contains the modified port.
*
* Output (s)    :  pu4InIpAddr, pu2InPort
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC UINT4
NatScanPartialLinksList (tHeaderInfo * pHeaderInfo, UINT4 *pu4InIpAddr,
                         UINT2 *pu2InPort)
{
    UINT4               u4ConnectionStatus = NAT_ZERO;
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT1               u1Check = NAT_ZERO;

    /*
     * Search for the Trans IP and the Outside IP in the list .If we get the
     * entry then we take the required info and delete the entry so that it
     * cannot be used again.
     */
    u4ConnectionStatus = NAT_FAILURE;
    pNatPartialLinkNode = NULL;

    if ((pHeaderInfo->u1PktType == NAT_TCP) ||
        (pHeaderInfo->u1PktType == NAT_UDP))
    {
        if (pHeaderInfo->u4Direction == NAT_INBOUND)
        {
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {

                if (pNatPartialLinkNode->u1PersistFlag != NAT_SIP_PERSISTENT)
                {
                    u1Check = (UINT1) (pNatPartialLinkNode->u4OutIpAddr
                                       == pHeaderInfo->u4OutIpAddr);
                }
                else
                {
                    u1Check = NAT_ONE;
                }

                if ((pNatPartialLinkNode->u4TranslatedLocIpAddr ==
                     pHeaderInfo->u4InIpAddr)
                    && (u1Check)
                    && (pNatPartialLinkNode->u2TranslatedLocPort ==
                        pHeaderInfo->u2InPort)
                    && (pNatPartialLinkNode->u1Direction ==
                        pHeaderInfo->u4Direction)
                    && (pNatPartialLinkNode->u1PktType ==
                        pHeaderInfo->u1PktType))
                {
                    *pu4InIpAddr = pNatPartialLinkNode->u4LocIpAddr;
                    *pu2InPort = pNatPartialLinkNode->u2LocPort;
                    break;
                }
                /* This handles the case when we have created the partial
                 * link already with the details for which this scan is called
                 * so that we need not create one more link for the same details
                 */
                if ((pNatPartialLinkNode->u4LocIpAddr ==
                     pHeaderInfo->u4InIpAddr)
                    && (pNatPartialLinkNode->u2LocPort ==
                        pHeaderInfo->u2InPort)
                    && (pNatPartialLinkNode->u1Direction ==
                        (UINT1) pHeaderInfo->u4Direction)
                    && (pNatPartialLinkNode->u1PktType ==
                        pHeaderInfo->u1PktType))
                {
                    *pu4InIpAddr = pNatPartialLinkNode->u4TranslatedLocIpAddr;
                    *pu2InPort = pNatPartialLinkNode->u2TranslatedLocPort;
                    break;
                }
            }
        }
        else                    /* NAT_OUTBOUND */
        {
            TMO_SLL_Scan (&gNatPartialLinksList, pNatPartialLinkNode,
                          tNatPartialLinkNode *)
            {
                if (pNatPartialLinkNode->u1PersistFlag != NAT_SIP_PERSISTENT)
                {
                    u1Check = (UINT1) (pNatPartialLinkNode->u4LocIpAddr ==
                                       pHeaderInfo->u4InIpAddr);
                }
                else
                {
                    u1Check = NAT_ONE;
                }

                if ((u1Check) &&
                    (pNatPartialLinkNode->u1Direction ==
                     pHeaderInfo->u4Direction)
                    && (pNatPartialLinkNode->u1PktType ==
                        pHeaderInfo->u1PktType))
                {
                    if ((pNatPartialLinkNode->u4OutIpAddr != NAT_ZERO)
                        && (pNatPartialLinkNode->u4OutIpAddr !=
                            pHeaderInfo->u4OutIpAddr))
                    {
                        continue;
                    }
                    else if ((pNatPartialLinkNode->u2OutPort != NAT_ZERO)
                             && (pNatPartialLinkNode->u2OutPort !=
                                 pHeaderInfo->u2OutPort))
                    {
                        continue;
                    }
                    else if ((pNatPartialLinkNode->u2LocPort != NAT_ZERO)
                             && (pNatPartialLinkNode->u2LocPort !=
                                 pHeaderInfo->u2InPort))
                    {
                        continue;
                    }
                    *pu4InIpAddr = pNatPartialLinkNode->u4TranslatedLocIpAddr;
                    *pu2InPort = pNatPartialLinkNode->u2TranslatedLocPort;
                }

            }
        }
        if (pNatPartialLinkNode != NULL)
        {
            u4ConnectionStatus = NAT_SUCCESS;
        }
    }

    if (((gu4NatEnable == NAT_DISABLE) ||
         (NatCheckIfNatEnable (pHeaderInfo->u4IfNum) == NAT_DISABLE)) &&
        (u4ConnectionStatus != NAT_SUCCESS))
    {
        if (pHeaderInfo->u4Direction == NAT_INBOUND)
        {
            NatAddPartialLinksList (pHeaderInfo,
                                    pHeaderInfo->u4InIpAddr,
                                    pHeaderInfo->u2InPort, NAT_NON_PERSISTENT);
        }
        else
        {
            NatAddPartialLinksList (pHeaderInfo,
                                    pHeaderInfo->u4OutIpAddr,
                                    pHeaderInfo->u2OutPort, NAT_NON_PERSISTENT);
        }

        *pu4InIpAddr = pHeaderInfo->u4InIpAddr;
        *pu2InPort = pHeaderInfo->u2InPort;

        return NAT_SUCCESS;
    }

    return (u4ConnectionStatus);
}

/***************************************************************************
* Function Name  :  NatDeletePartialEntry
* Description    :  This function deletes the Partial Entries, Associated 
                    with Physical Interface. 
                    This Function will release the associated global port of
                    Partial entry, only when the global port is not used in
                    Dynamic entry. 
*
* Input (s)      :  u4IfNum - Interface to which PartialEntry Associated 
*
* Output (s)     :  None 
* Returns        :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PUBLIC INT4
NatDeletePartialEntry (UINT4 u4IfNum)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    UINT2               u2IfFound = FALSE;
    tNatPartialLinkNode *pNextNode = NULL;
    UINT4               u4IntfNum = NATIPC_PORT_NUM_ZERO;
    tGlobalHashNode    *pGlobalNodeEntry = NULL;
    UINT4               u4GlobalHashKey = NAT_ZERO;
    UINT4               u4PortExists = FALSE;

    pNatPartialLinkNode =
        (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

    for (; pNatPartialLinkNode != NULL; pNatPartialLinkNode = pNextNode)
    {
        pNextNode = (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                          (tTMO_SLL_NODE
                                                           *) (VOID *)
                                                          pNatPartialLinkNode);
        u2IfFound = FALSE;
        u4IntfNum = NATIPC_PORT_NUM_ZERO;
        if (SecUtilIpIfGetIfIndexFromIpAddress
            (pNatPartialLinkNode->u4TranslatedLocIpAddr,
             &u4IntfNum) == OSIX_FAILURE)
        {
            if (SecUtilIpIfGetIfIndexFromIpAddress
                (pNatPartialLinkNode->u4OutIpAddr, &u4IntfNum) == OSIX_SUCCESS)
            {
                u2IfFound = TRUE;
            }
        }
        else
        {
            u2IfFound = TRUE;
        }
        if ((u2IfFound != TRUE) || (u4IfNum != u4IntfNum))
        {
            continue;
        }
        if (pNatPartialLinkNode->u2TranslatedLocPort !=
            pNatPartialLinkNode->u2LocPort)
        {
            /* Make sure that translated port is not present in the 
             * Global Hash Node */
            u4GlobalHashKey = NatFormHashKey
                (pNatPartialLinkNode->u4TranslatedLocIpAddr,
                 pNatPartialLinkNode->u2TranslatedLocPort, NAT_GLOBAL);
            u4PortExists = FALSE;

            TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                                  pGlobalNodeEntry, tGlobalHashNode *)
            {
                if ((pGlobalNodeEntry->u4TranslatedLocIpAddr ==
                     pNatPartialLinkNode->u4TranslatedLocIpAddr)
                    && (pGlobalNodeEntry->u2TranslatedLocPort ==
                        pNatPartialLinkNode->u2TranslatedLocPort))

                {
                    /* Update the flag indicating that some other dynamic 
                     * entry is also using the same translated port */
                    u4PortExists = TRUE;
                    break;
                }
            }
            /* There are other dynamic entries using the same port
             * So we should not be freeing it */
            if (u4PortExists == FALSE)
            {
                if(NatReleaseGlobalPort (pNatPartialLinkNode->u2TranslatedLocPort) == NAT_FAILURE)
                {
                   NAT_DBG1(NAT_DBG_TABLE,
                            "\nUnable to release the port %d \n",
                            pNatPartialLinkNode->u2TranslatedLocPort); 

                }    
            }
        }
        TMO_SLL_Delete (&gNatPartialLinksList,
                        (tTMO_SLL_NODE *) (VOID *) pNatPartialLinkNode);
        NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                               (UINT1 *) pNatPartialLinkNode);
    }
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatDeletePartialLinksList
* Description    :  This function deletes the entries in the TFTP and FTP 
                    Table if the entry is idle for sometime. 
*
* Input (s)    : None 
*
* Output (s)    : None 
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/

PRIVATE INT4
NatDeletePartialLinksList (VOID)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pNextNode = NULL;
    tGlobalHashNode    *pGlobalNodeEntry = NULL;
    UINT4               u4CurrTime = NAT_ZERO;
    UINT4               u4TimeOut = NAT_ZERO;
    UINT4               u4NewTimeout = NAT_ZERO;
    UINT4               u4GlobalHashKey = NAT_ZERO;
    UINT4               u4PortExists = FALSE;
    UINT4               u4IntfNum = NATIPC_PORT_NUM_ZERO;
    UINT2               u2IfFound = FALSE;

    NAT_GET_SYS_TIME (&u4CurrTime);

    pNatPartialLinkNode =
        (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

    pNextNode = NULL;

    for (; pNatPartialLinkNode != NULL; pNatPartialLinkNode = pNextNode)
    {
        pNextNode = (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                          &
                                                          (pNatPartialLinkNode->
                                                           sNatPartialLinkNode));
        /* If the partial link is PERSISTENT then just skip 
         * that node as PERSISTENT links can't be deleted 
         */
        u4TimeOut = (pNatPartialLinkNode->u1PersistFlag == NAT_PERSISTENT) ?
            (NAT_PERSIST_PARTIAL_LINK_TIMEOUT) :
            (NAT_NON_PERSIST_PARTIAL_LINK_TIMEOUT);

        if ((pNatPartialLinkNode->u1AppCallStatus == NAT_ON_HOLD) ||
            (pNatPartialLinkNode->u1AppCallStatus == NAT_NOT_ON_HOLD))

        {
            continue;
        }

        u4NewTimeout = u4TimeOut;

        NAT_GET_SYS_TIME (&u4CurrTime);

        if ((u4CurrTime - pNatPartialLinkNode->u4TimeStamp) >= u4NewTimeout)
        {

            if (pNatPartialLinkNode->u2TranslatedLocPort !=
                pNatPartialLinkNode->u2LocPort)
            {
                /* Make sure that translated port is not present in the 
                 * Global Hash Node */
                u4GlobalHashKey = NatFormHashKey
                    (pNatPartialLinkNode->u4TranslatedLocIpAddr,
                     pNatPartialLinkNode->u2TranslatedLocPort, NAT_GLOBAL);

                TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                                      pGlobalNodeEntry, tGlobalHashNode *)
                {
                    if ((pGlobalNodeEntry->u4TranslatedLocIpAddr ==
                         pNatPartialLinkNode->u4TranslatedLocIpAddr)
                        && (pGlobalNodeEntry->u2TranslatedLocPort ==
                            pNatPartialLinkNode->u2TranslatedLocPort))

                    {
                        /* Update the flag indicating that some other dynamic 
                         * entry is also using the same translated port */
                        u4PortExists = TRUE;
                        break;
                    }
                }
                /* There are other dynamic entries using the same port
                 * So we should not be freeing it */
                if ((u4PortExists == FALSE) &&
                    (pNatPartialLinkNode->u2TranslatedLocPort !=
                     pNatPartialLinkNode->u2LocPort))
                {
                    if (SecUtilIpIfGetIfIndexFromIpAddress
                        (pNatPartialLinkNode->u4TranslatedLocIpAddr,
                         &u4IntfNum) == OSIX_FAILURE)
                    {
                        if (SecUtilIpIfGetIfIndexFromIpAddress
                            (pNatPartialLinkNode->u4OutIpAddr,
                             &u4IntfNum) == OSIX_SUCCESS)
                        {
                            u2IfFound = TRUE;
                        }
                    }
                    else
                    {
                        u2IfFound = TRUE;
                    }
                    /*
                     * We must Release Global port, only when NAPT is enabled.
                     */
                    if ((u2IfFound == TRUE) && (NatCheckIfNaptEnable
                                                (u4IntfNum) == NAT_ENABLE))
                    {
                        if(NatReleaseGlobalPort
                            (pNatPartialLinkNode->u2TranslatedLocPort) == NAT_FAILURE)
                        {
                            NAT_DBG1(NAT_DBG_TABLE,
                                     "\nUnable to release the port %d \n",
                                     pNatPartialLinkNode->u2TranslatedLocPort); 

                        }    
                    }
                }
            }
            TMO_SLL_Delete (&gNatPartialLinksList,
                            &(pNatPartialLinkNode->sNatPartialLinkNode));
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);
        }
    }
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  : NatUpdateGlobalHashTable 
* Description    :  This function updates the usage count in Global Hash Node.
*
* Input (s)    : pHeaderInfo - pointer to the Hdr info of the packet.
*                u4InId - Inside Id found from the Local Hash Node.
*
* Output (s)    : pu4InIpAddr - pointer to the Translated IP address
*                 pu2InPort - pointer to the Translated Port. 
*
* Returns      :  NAT_SUCCESS or NAT_FAILURE
*
****************************************************************************/
PRIVATE INT4
NatUpdateGlobalHashTable (tHeaderInfo * pHeaderInfo, UINT4 u4InId,
                          UINT4 *pu4InIpAddr, UINT2 *pu2InPort)
{

    tGlobalInfo         GlobalInfo;
    UINT4               u4GlobalHashKey = NAT_ZERO;
    tIidListNode       *pIidNode = NULL;
    tGlobalHashNode    *pGlobalNodeEntry = NULL;
    UINT4               u4ConnectionStatus = NAT_FAILURE;

    MEMSET (&GlobalInfo, NAT_ZERO, sizeof (tGlobalInfo));
    /* Scan through the complete global hashlist.
     * If the InID is matched, we will get the corresponding translated IP
     * and translated port, for the given Local IP and LocalPort.
     * Increase the usage count of the global HashNode and return the 
     * translated IP and translated Port */
    TMO_HASH_Scan_Table (gpNatGlobalHashList, u4GlobalHashKey)
    {
        TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                              pGlobalNodeEntry, tGlobalHashNode *)
        {
            TMO_SLL_Scan (&pGlobalNodeEntry->pIidList, pIidNode, tIidListNode *)
            {
                if (u4InId == pIidNode->u4InId)
                {
                    /* Increment the usage count and return translated IP 
                     * and port */
                    pIidNode->u4NoOfEntries++;
                    *pu4InIpAddr = pGlobalNodeEntry->u4TranslatedLocIpAddr;
                    *pu2InPort = pGlobalNodeEntry->u2TranslatedLocPort;
                    return NAT_SUCCESS;
                }
            }
        }
    }
    /* TODO: The below code is unnecessary, and should be removed */
    /* get a Global IP address and Port  */
    NatGetNextFreeGlobalIpPort (pHeaderInfo->u4InIpAddr,
                                pHeaderInfo->u2InPort,
                                pHeaderInfo->u4OutIpAddr,
                                pHeaderInfo->u2OutPort,
                                pHeaderInfo->u4IfNum, &GlobalInfo);

    if (GlobalInfo.u4TranslatedLocIpAddr != NAT_ZERO)
    {
        if ((pHeaderInfo->u1PktType != NAT_ICMP) &&
            (pHeaderInfo->u1PktType != NAT_PPTP))
        {
            if (GlobalInfo.u2TranslatedLocPort == NAT_ZERO)
            {
                /* Use the Local Port without translation */
                GlobalInfo.u2TranslatedLocPort = pHeaderInfo->u2InPort;
            }
        }
        else
        {
            /*For ICMP packets modify the ICMP Id if Napt is Enabled */
            if (gapNatIfTable[pHeaderInfo->u4IfNum]->u1NaptEnable == NAT_ENABLE)
            {
                if (pHeaderInfo->u1PktType == NAT_ICMP)
                {
                    GlobalInfo.u2TranslatedLocPort =
                        (UINT2) (gu4NatIcmpNewIden++);
                }
                else
                {
                    if (gu4NatPptpOutCreateFlag != TRUE)
                    {
                        GlobalInfo.u2TranslatedLocPort =
                            (UINT2) (gu4NatPptpNewIden++);
                    }
                    else
                    {
                        GlobalInfo.u2TranslatedLocPort = pHeaderInfo->u2InPort;
                    }
                }
            }
            else
                GlobalInfo.u2TranslatedLocPort = pHeaderInfo->u2InPort;
        }
        *pu4InIpAddr = GlobalInfo.u4TranslatedLocIpAddr;
        *pu2InPort = GlobalInfo.u2TranslatedLocPort;
        if (*pu2InPort != pHeaderInfo->u2InPort)
        {
            u4ConnectionStatus = NatCreateGlobalHashNode (*pu4InIpAddr,
                                                          *pu2InPort, u4InId);
        }
        else
        {
            /*Get the Global Hash Node and update the no of entries used */
            u4GlobalHashKey = NatFormHashKey (*pu4InIpAddr,
                                              *pu2InPort, NAT_GLOBAL);
            TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                                  pGlobalNodeEntry, tGlobalHashNode *)
            {
                if ((pGlobalNodeEntry->u4TranslatedLocIpAddr ==
                     pHeaderInfo->u4InIpAddr)
                    && (pGlobalNodeEntry->u2TranslatedLocPort ==
                        pHeaderInfo->u2InPort))
                {
                    TMO_SLL_Scan (&pGlobalNodeEntry->pIidList, pIidNode,
                                  tIidListNode *)
                    {
                        if (u4InId == pIidNode->u4InId)
                        {
                            pIidNode->u4NoOfEntries++;
                            u4ConnectionStatus = NAT_SUCCESS;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        u4ConnectionStatus = NAT_SUCCESS;
    }
    return ((INT4) u4ConnectionStatus);
}

/***************************************************************************
* Function Name    :  NatSearchStaticNaptTable
* Description    :  This function searches the Static table for the presence
*             of local IP address or Global IP address depending on the
*             direction. If it is present, the corresponding mapping
*             is returned.
*
* Input (s)    : 1. u4IpAddr - The IP address to be searched.
*             2. u4Direction - Tells the direction of the packet.
*             Based on this the value stored in the u4IpAddr is changed.
*             3. u4IpAddr - The interface number whose static mapping
*             should be searched.
* Outputs     :  Local IP Address - Direction is NAT_INBOUND
*             Global IP Address - Direction is NAT_OUTBOUND
*             0 - no mapping exists.
* 
* Returns     : NULL, if no mapping exists
*               Address of Static NAPT entry, if mapping exists.
*                
****************************************************************************/

tStaticNaptEntry   *
NatGetStaticNaptEntry (UINT4 *pu4IpAddr, UINT2 *pu2Port, UINT4 u4Direction,
                       UINT4 u4IfNum, UINT2 u2ProtocolNumber)
{
    UINT2               u2TempProtocol = NAT_ZERO;
    UINT2               u2TempPort = NAT_ZERO;
    tStaticNaptEntry   *pStaticNaptEntry = NULL;
    tInterfaceInfo     *pNode = NULL;
    if (u4IfNum > (NAT_MAX_NUM_IF))
    {
        return NULL;
    }
    pNode = gapNatIfTable[u4IfNum];

    /* If pNode is NULL return NAT_FAILURE */
    if (pNode == NULL)
    {
        return NULL;
    }

    if (NAT_OUTBOUND == u4Direction)
    {
        TMO_SLL_Scan (&pNode->StaticNaptList, pStaticNaptEntry,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntry->u4LocIpAddr == *pu4IpAddr)
                && (pStaticNaptEntry->i4RowStatus == NAT_STATUS_ACTIVE)
                && ((pStaticNaptEntry->u2ProtocolNumber == u2ProtocolNumber)
                    || (pStaticNaptEntry->u2ProtocolNumber == NAT_PROTO_ANY)))
            {
                /*Checking for port range forwarding */
                if ((pStaticNaptEntry->u2StartLocPort <= *pu2Port)
                    && (pStaticNaptEntry->u2EndLocPort >= *pu2Port))
                {
                    *pu4IpAddr = pStaticNaptEntry->u4TranslatedLocIpAddr;
                    break;
                }
                /* For PPTP Call ID Translations, Dynamic Entry to be created with the
                 * outport as 0 */

                else if (*pu2Port == NAT_ZERO)
                {
                    *pu4IpAddr = pStaticNaptEntry->u4TranslatedLocIpAddr;
                    break;
                }
            }
        }
    }
    else
    {

        /* PPTP packets from WAN are allowed if portforwarding is enabled 
         * for the control connection. So query for the control connection
         * ie port = 1723. If the control connection entry is found then
         * allow the packet by translating only the IP. No port translation
         * is needed for the PPTP packets */
        if (u2ProtocolNumber == NAT_PPTP)
        {
            u2TempProtocol = u2ProtocolNumber;
            u2TempPort = *pu2Port;
            u2ProtocolNumber = NAT_TCP;
            *pu2Port = NAT_PPTP_TCP_PORT;
        }
        TMO_SLL_Scan (&pNode->StaticNaptList, pStaticNaptEntry,
                      tStaticNaptEntry *)
        {
            if ((pStaticNaptEntry->u4TranslatedLocIpAddr ==
                 *pu4IpAddr)
                && (pStaticNaptEntry->i4RowStatus ==
                    NAT_STATUS_ACTIVE)
                &&
                ((pStaticNaptEntry->u2ProtocolNumber ==
                  u2ProtocolNumber)
                 || (pStaticNaptEntry->u2ProtocolNumber == NAT_PROTO_ANY)))
            {
                /* Start and End port are same check for translated port and 
                 * set port as Start of range for virtual server which are 
                 * listening on non-standard ports.
                 */
                if (pStaticNaptEntry->u2StartLocPort ==
                    pStaticNaptEntry->u2EndLocPort)
                {
                    if (pStaticNaptEntry->u2TranslatedLocPort == *pu2Port)
                    {
                        *pu2Port = pStaticNaptEntry->u2StartLocPort;
                        *pu4IpAddr = pStaticNaptEntry->u4LocIpAddr;
                        break;
                    }
                }
                /* Checking for port range forwarding, port will be same */
                else if ((pStaticNaptEntry->u2StartLocPort <= *pu2Port)
                         && (pStaticNaptEntry->u2EndLocPort >= *pu2Port))
                {
                    *pu4IpAddr = pStaticNaptEntry->u4LocIpAddr;
                    break;
                }
                /* For PPTP Call ID Translations, Dynamic Entry to be created with the
                 * inport as 0 */
                else if (*pu2Port == NAT_ZERO)
                {
                    *pu4IpAddr = pStaticNaptEntry->u4LocIpAddr;
                    break;
                }
            }
        }

        /* Make sure there is no port translation for PPTP packets */
        if (u2TempProtocol == NAT_PPTP)
        {
            *pu2Port = u2TempPort;
        }
    }

    return (pStaticNaptEntry);
}

/***************************************************************************
* Function Name    :  NatSearchTransPortInStaticNaptTable
* Description    :  This function searches the Static table for the presence
*             of local IP address or Global IP address depending on the
*             direction. If it is present, the corresponding mapping
*             is returned.
*
* Input (s)    : 1. u4IpAddr - The IP address to be searched.
*             2. u4Direction - Tells the direction of the packet.
*             Based on this the value stored in the u4IpAddr is changed.
*             3. u4IpAddr - The interface number whose static mapping
*             should be searched.
*
* Output (s)    :  None
* Returns      :  Local IP Address - Direction is NAT_INBOUND
*             Global IP Address - Direction is NAT_OUTBOUND
*             0 - no mapping exists.
****************************************************************************/

PUBLIC INT4
NatSearchTransPortInNaptTable (UINT4 u4IfNum, UINT2 u2Port)
{
    tStaticNaptEntry   *pStaticNaptEntry = NULL;
    tInterfaceInfo     *pNode = NULL;

    if (u4IfNum > (NAT_MAX_NUM_IF))
    {
        return NAT_FAILURE;
    }
    pNode = gapNatIfTable[u4IfNum];
    if (pNode == NULL)
    {
        return (NAT_FAILURE);
    }

    TMO_SLL_Scan (&pNode->StaticNaptList, pStaticNaptEntry, tStaticNaptEntry *)
    {
        if (((pStaticNaptEntry->u2TranslatedLocPort == u2Port) ||
             ((pStaticNaptEntry->u2StartLocPort >= u2Port) &&
              (pStaticNaptEntry->u2EndLocPort <= u2Port))) &&
            (pStaticNaptEntry->i4RowStatus == NAT_STATUS_ACTIVE))
        {
            return (NAT_SUCCESS);
        }
    }

    return (NAT_FAILURE);
}

/***************************************************************************
* Function Name    :  NatSearchTransIpInStaticNaptTable
* Description    :  This function searches the Static table for the presence
*             of local IP address or Global IP address depending on the
*             direction. If it is present, the corresponding mapping
*             is returned.
*
* Input (s)    : 1. u4IpAddr - The IP address to be searched.
*             2. u4Direction - Tells the direction of the packet.
*             Based on this the value stored in the u4IpAddr is changed.
*             3. u4IpAddr - The interface number whose static mapping
*             should be searched.
*
* Output (s)    :  None
* Returns      :  Local IP Address - Direction is NAT_INBOUND
*             Global IP Address - Direction is NAT_OUTBOUND
*             0 - no mapping exists.
****************************************************************************/

PUBLIC UINT4
NatGetTransIpFromStaticNaptTbl (UINT4 u4IfNum, UINT4 u4IpAddr)
{
    tStaticNaptEntry   *pStaticNaptEntry = NULL;
    tInterfaceInfo     *pNode = NULL;
    UINT4               u4TransIpAddr = NAT_ZERO;
    if (u4IfNum > NAT_MAX_NUM_IF)
    {
        return u4TransIpAddr;
    }
    pNode = gapNatIfTable[u4IfNum];
    if (pNode == NULL)
    {
        return (u4TransIpAddr);
    }

    TMO_SLL_Scan (&pNode->StaticNaptList, pStaticNaptEntry, tStaticNaptEntry *)
    {
        if ((pStaticNaptEntry->u4LocIpAddr == u4IpAddr)
            && (pStaticNaptEntry->i4RowStatus == NAT_STATUS_ACTIVE))
        {
            u4TransIpAddr = pStaticNaptEntry->u4TranslatedLocIpAddr;
            break;
        }
    }

    return (u4TransIpAddr);
}

/***************************************************************************
* Function Name    :  NatInterfaceEntryCreate
* Description      :  This function creates the interface entry and 
*                     enabled NAT by default, if needed.
*
* Input (s)        : 1. u4IfIndex - Interface Index.
*                    2. u4Status - Flag
*                      NAT_ENABLE_ALL- enable NAT, NAPT and two-way NAT
*                      by default
*
* Output (s)       :  None
* Returns          :  NAT_SUCCESS/NAT_FAILURE
****************************************************************************/

INT1
NatInterfaceEntryCreate (UINT4 u4IfIndex, UINT4 u4Status)
{

    if (gapNatIfTable[u4IfIndex] == NULL)
    {
        if (NAT_MEM_ALLOCATE
            (NAT_INTERFACE_INFO_POOL_ID,
             gapNatIfTable[u4IfIndex], tInterfaceInfo) == NULL)
        {
            MOD_TRC (gu4NatTrc, MGMT_TRC, "NAT", "Memory Allocation Failed\n");
            return NAT_FAILURE;
        }

        NATIFTABLE_INITIALIZE (gapNatIfTable[u4IfIndex], NAT_STATUS_NOT_READY);
    }
    else
    {
	/* Return if Entry is alredy Existing*/
	return NAT_SUCCESS;
    }	

    if (u4Status == NAT_ENABLE_ALL)
    {
#if !defined (SECURITY_KERNEL_WANTED) && defined (SNMP_WANTED)  
        if (gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)
#endif
        {
            gapNatIfTable[u4IfIndex]->u1NatEnable = NAT_ENABLE;
            gapNatIfTable[u4IfIndex]->u1NaptEnable = NAT_ENABLE;
            gapNatIfTable[u4IfIndex]->u1TwoWayNatEnable = NAT_ENABLE;
        }
        gapNatIfTable[u4IfIndex]->i4RowStatus = NAT_STATUS_ACTIVE;
    }
    else if (u4Status == NOT_IN_SERVICE)
    {
        gapNatIfTable[u4IfIndex]->u1NatEnable = NAT_DISABLE;
        gapNatIfTable[u4IfIndex]->u1NaptEnable = NAT_DISABLE;
        gapNatIfTable[u4IfIndex]->u1TwoWayNatEnable = NAT_DISABLE;
        gapNatIfTable[u4IfIndex]->i4RowStatus = NAT_STATUS_NOT_IN_SERVICE;
    }

    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  : NatConfigureGlobalIp
* Description    : This function adds/deletes interface IP to global pool.
*
* Input (s)      :1. u4Interface - Interface Index. 
*                 2. u4IpAddress - The interface IP address.
*                 3. u4Status - ADD/DELETE
*
* Output (s)     :  None
* Returns        :  NAT_SUCCESS/NAT_FAILURE
****************************************************************************/

INT1
NatConfigureGlobalIp (UINT4 u4Interface, UINT4 u4IpAddress, UINT4 u4Status)
{
    tTranslatedLocIpAddrNode *pTranslatedLocalIpAddrNode = NULL;
    tInterfaceInfo     *pIfNode = NULL;

    pIfNode = gapNatIfTable[u4Interface];
    if (pIfNode->u1NaptEnable == NAT_ENABLE)
    {
        if ((u4Status == NAT_CREATE))
        {
            /* pIfNode Cannot be NULL */
            /* Row should exist and will always be added as the first member */
            pTranslatedLocalIpAddrNode = (tTranslatedLocIpAddrNode *)
                TMO_SLL_First (&(pIfNode->TranslatedLocIpList));
            if ((pTranslatedLocalIpAddrNode == NULL) ||
                (pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr !=
                 u4IpAddress))
            {
                if (NAT_MEM_ALLOCATE
                    (NAT_GLOBAL_ADDR_POOL_ID,
                     pTranslatedLocalIpAddrNode,
                     tTranslatedLocIpAddrNode) == NULL)
                {
                    return NAT_FAILURE;
                }

                pTranslatedLocalIpAddrNode->u4TranslatedLocIpAddr = u4IpAddress;
                pTranslatedLocalIpAddrNode->u4Mask = NAT_INIT_MASK;
                pTranslatedLocalIpAddrNode->i4RowStatus = NAT_STATUS_ACTIVE;

                TMO_SLL_Insert (&(pIfNode->TranslatedLocIpList),
                                NULL,
                                &(pTranslatedLocalIpAddrNode->
                                  TranslatedLocIpAddrNode));
                /*Nat interface Entry should be initialised */
                NatIfInitFields (u4Interface);
            }
        }
        else if (u4Status == NAT_DELETE)
        {
            if (NatGatDestroy ((INT4) u4Interface, u4IpAddress) == SNMP_FAILURE)
            {
                return NAT_FAILURE;
            }
        }
    }
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatDeleteSpecifiedEntry
* 
* Description    :  This function deletes the dynamic entries corresponding
*                   to given Local IP, Translated IP, Local Port and
*                   Translated Port.
*
* Input (s)      :  u4IfNum - Interface number
*                   u4LocIpAddr - Local Ip Address
*                   u4TransIpAddr - Translated IP address
*                   u2LocPort - Local Port number
*                   u2TransPort - Translated Port Number
* 
* Output (s)     :  None
* 
* Returns        :  NAT_SUCCESS/NAT_FAILURE
***************************************************************************/

INT1
NatDeleteSpecifiedEntry (UINT4 u4IfNum, UINT4 u4LocIpAddr, UINT4 u4TransIpAddr,
                         UINT2 u2LocPort, UINT2 u2TransPort)
{
    tUdpList           *pUdpListNode = NULL;
    tUdpList           *pUdpNextNode = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    tTcpDelNode        *pTcpNextNode = NULL;
    tDynamicEntry      *pNfsEntryNode = NULL;
    tDynamicEntry      *pNextDynamicEntry = NULL;
    tDynamicEntry      *pDynamicEntry = NULL;
    UINT1               u1AppCallStatus = NAT_ZERO;
    NAT_DBG (NAT_DBG_ENTRY, "\n Entering NatDeleteSpecifiedEntry \n");

    /* Delete UDP session entries pertaining to the input interface */
    pUdpListNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pUdpListNode != NULL)
    {
        pUdpNextNode =
            (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                       &(pUdpListNode->
                                         IidOidArrayDynamicListNode));

        if ((pUdpListNode->pDynamicEntry->u4IfNum == u4IfNum) &&
            (pUdpListNode->pDynamicEntry->u4LocIpAddr == u4LocIpAddr) &&
            (pUdpListNode->pDynamicEntry->u4TranslatedLocIpAddr
             == u4TransIpAddr))
        {
            if ((u2LocPort != NAT_ZERO) &&
                (pUdpListNode->pDynamicEntry->u2LocPort != u2LocPort))
            {
                pUdpListNode = pUdpNextNode;
                continue;
            }
            if ((u2TransPort != NAT_ZERO) &&
                (pUdpListNode->pDynamicEntry->u2TranslatedLocPort
                 != u2TransPort))
            {
                pUdpListNode = pUdpNextNode;
                continue;
            }

            TMO_SLL_Delete (&gNatUdpList,
                            &(pUdpListNode->IidOidArrayDynamicListNode));
            NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pUdpListNode);
        }
        pUdpListNode = pUdpNextNode;
    }
    /* deletion of TCP nodes using TcpDelStack */
    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);
    while (pTcpDelNode != NULL)
    {
        pTcpNextNode =
            (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                          &(pTcpDelNode->TcpDelNode));

        if ((pTcpDelNode->pDynamicEntry->u4IfNum == u4IfNum) &&
            (pTcpDelNode->pDynamicEntry->u4LocIpAddr == u4LocIpAddr) &&
            (pTcpDelNode->pDynamicEntry->u4TranslatedLocIpAddr
             == u4TransIpAddr))
        {
            if ((u2LocPort != NAT_ZERO) &&
                (pTcpDelNode->pDynamicEntry->u2LocPort != u2LocPort))
            {
                pTcpDelNode = pTcpNextNode;
                continue;
            }
            if ((u2TransPort != NAT_ZERO) &&
                (pTcpDelNode->pDynamicEntry->u2TranslatedLocPort
                 != u2TransPort))
            {
                pTcpDelNode = pTcpNextNode;
                continue;
            }

            TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                   (UINT1 *) pTcpDelNode);
        }
        pTcpDelNode = pTcpNextNode;
    }

    /* Delete NFS session entries pertaining to the input interface */

    pNfsEntryNode = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);

    while (pNfsEntryNode != NULL)
    {
        pNextDynamicEntry =
            (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                            &(pNfsEntryNode->
                                              DynamicTableEntry));
        if ((pNfsEntryNode->u4IfNum == u4IfNum)
            && (pNfsEntryNode->u4LocIpAddr == u4LocIpAddr)
            && (pNfsEntryNode->u4TranslatedLocIpAddr == u4TransIpAddr))
        {
            if ((u2LocPort != NAT_ZERO) && (pNfsEntryNode->u2LocPort !=
                                            u2LocPort))
            {
                pNfsEntryNode = pNextDynamicEntry;
                continue;
            }
            if ((u2TransPort != NAT_ZERO) &&
                (pNfsEntryNode->u2TranslatedLocPort != u2TransPort))
            {
                pNfsEntryNode = pNextDynamicEntry;
                continue;
            }
            /*delete from NfsList */
            TMO_SLL_Delete (&gNatNfsList, &(pNfsEntryNode->DynamicTableEntry));
        }

        pNfsEntryNode = pNextDynamicEntry;
    }

    /* Delete dynamic session entries pertaining to the input interface */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {
        pNextDynamicEntry =
            (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                            &(pDynamicEntry->
                                              DynamicTableEntry));
        if ((pDynamicEntry->u4IfNum == u4IfNum)
            && (pDynamicEntry->u4LocIpAddr == u4LocIpAddr)
            && (pDynamicEntry->u4TranslatedLocIpAddr == u4TransIpAddr))
        {
            if ((u2LocPort != NAT_ZERO) && (pDynamicEntry->u2LocPort !=
                                            u2LocPort))
            {
                pDynamicEntry = pNextDynamicEntry;
                continue;
            }
            if ((u2TransPort != NAT_ZERO) &&
                (pDynamicEntry->u2TranslatedLocPort != u2TransPort))
            {
                pDynamicEntry = pNextDynamicEntry;
                continue;
            }

            u1AppCallStatus = pDynamicEntry->u1AppCallStatus;
            NatDeleteEntry (pDynamicEntry);
            /* If it the dynamic entry corresponds to a SIP entry,
             * there will be only one dynamic entry associated with
             * it. So we can break away; Otherwise the list will 
             * be corrupted */
            if ((u1AppCallStatus == NAT_ON_HOLD) ||
                (u1AppCallStatus == NAT_NOT_ON_HOLD))
            {
                break;
            }
        }
        pDynamicEntry = pNextDynamicEntry;
    }

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatDeleteSpecifiedEntry \n");
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatRemoveDynamicEntry
* 
* Description    :  This function deletes the given dynamic entry
*
* Input (s)      :  pDynamicEntry  - The Dynamic entry to be deleted
* 
* Output (s)     :  None
* 
* Returns        :  NAT_SUCCESS/NAT_FAILURE
***************************************************************************/

INT1
NatRemoveDynamicEntry (tDynamicEntry * pDynamicEntry)
{
    tLocOutHashNode    *pLocNode = NULL;
    tGlobalHashNode    *pGlobalNode = NULL;
    tLocOutHashNode    *pOutNode = NULL;
    UINT4               u4LocKey = NAT_ZERO;
    UINT4               u4OutKey = NAT_ZERO;
    UINT4               u4GlobalKey = NAT_ZERO;
    UINT4               u4InId = NAT_ZERO;
    UINT4               u4OutId = NAT_ZERO;
    UINT4               u4IpAddr = NAT_ZERO;
    tIidListNode       *pLocIidList = NULL;
    tIidOidArrayDynamicListNode *pDeleteNode = NULL;
    tUdpList           *pUdpListNode = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    UINT4               u4PortExists = FALSE;

    /* pDeleteNode = NULL;
       pLocNode = pOutNode = NULL;
       pGlobalNode = NULL;
       pLocIidList = NULL; */

    NAT_DBG (NAT_DBG_EXIT, "\n Entering NatRemoveDynamicEntry \n");

    /* Delete UDP session entries pertaining to the input interface */
    pUdpListNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pUdpListNode != NULL)
    {
        if (pUdpListNode->pDynamicEntry == pDynamicEntry)
        {
            TMO_SLL_Delete (&gNatUdpList,
                            &(pUdpListNode->IidOidArrayDynamicListNode));
            NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pUdpListNode);
            break;
        }
        else
        {
            pUdpListNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pUdpListNode->
                                             IidOidArrayDynamicListNode));
        }
    }
    /* deletion of TCP nodes using TcpDelStack */
    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);
    while (pTcpDelNode != NULL)
    {
        if (pTcpDelNode->pDynamicEntry == pDynamicEntry)
        {
            TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                   (UINT1 *) pTcpDelNode);
            break;
        }
        else
        {
            pTcpDelNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));
        }
    }

    /* Release the Hash nodes */
    u4LocKey = NatFormHashKey (pDynamicEntry->u4LocIpAddr,
                               pDynamicEntry->u2LocPort, NAT_LOCAL);
    u4GlobalKey =
        NatFormHashKey (pDynamicEntry->u4TranslatedLocIpAddr,
                        pDynamicEntry->u2TranslatedLocPort, NAT_GLOBAL);
    u4OutKey =
        NatFormHashKey (pDynamicEntry->u4OutIpAddr, pDynamicEntry->u2OutPort,
                        NAT_OUTSIDE);

    TMO_HASH_Scan_Bucket (gpNatLocHashList, u4LocKey, pLocNode,
                          tLocOutHashNode *)
    {
        if ((pLocNode->u4IpAddr == pDynamicEntry->u4LocIpAddr) &&
            (pLocNode->u2Port == pDynamicEntry->u2LocPort))
        {
            pLocNode->u4NoOfEntries--;
            u4InId = pLocNode->u4Id;
            if (pLocNode->u4NoOfEntries == NAT_ZERO)
            {
                TMO_HASH_Delete_Node (gpNatLocHashList,
                                      &(pLocNode->LocOutHash), u4LocKey);
                NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pLocNode);
            }
            break;
        }
    }

    TMO_HASH_Scan_Bucket (gpNatOutHashList, u4OutKey, pOutNode,
                          tLocOutHashNode *)
    {
        if ((pOutNode->u4IpAddr == pDynamicEntry->u4OutIpAddr) &&
            (pOutNode->u2Port == pDynamicEntry->u2OutPort))
        {
            pOutNode->u4NoOfEntries--;
            u4OutId = pOutNode->u4Id;
            if (pOutNode->u4NoOfEntries == NAT_ZERO)
            {
                TMO_HASH_Delete_Node (gpNatOutHashList,
                                      &(pOutNode->LocOutHash), u4OutKey);
                NatMemReleaseMemBlock (NAT_HASH_POOL_ID, (UINT1 *) pOutNode);
                break;
            }
        }
    }

    TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalKey, pGlobalNode,
                          tGlobalHashNode *)
    {
        if ((pGlobalNode->u4TranslatedLocIpAddr ==
             pDynamicEntry->u4TranslatedLocIpAddr)
            && (pGlobalNode->u2TranslatedLocPort ==
                pDynamicEntry->u2TranslatedLocPort))
        {
            TMO_SLL_Scan (&pGlobalNode->pIidList, pLocIidList, tIidListNode *)
            {
                if (pLocIidList->u4InId == u4InId)
                {
                    pLocIidList->u4NoOfEntries--;
                    if (pLocIidList->u4NoOfEntries == NAT_ZERO)
                    {
                        TMO_SLL_Delete (&pGlobalNode->pIidList,
                                        &(pLocIidList->GlobalIidList));
                        NatMemReleaseMemBlock (NAT_IID_LIST_POOL_ID,
                                               (UINT1 *) pLocIidList);
                        pLocIidList = NULL;
                    }
                    else
                    {
                        /* Update the flag indicating that some other dynamic 
                         * entry is also using the same translated port */
                        u4PortExists = TRUE;
                    }
                    break;
                }
            }
            if (TMO_SLL_First (&pGlobalNode->pIidList) == NULL)
            {
                TMO_HASH_Delete_Node (gpNatGlobalHashList,
                                      &(pGlobalNode->GlobalHash), u4GlobalKey);
                NatMemReleaseMemBlock (NAT_GLOBAL_HASH_POOL_ID,
                                       (UINT1 *) pGlobalNode);
            }
            break;
        }
    }

    /* Release the Session info from Dynamic Translation Table */
    u4InId = u4InId % NAT_IID_OID_ARRAY_LIMIT;
    u4OutId = u4OutId % NAT_IID_OID_ARRAY_LIMIT;
    /* pHead = ArrayNode.pHead; */
    TMO_SLL_Scan (&gaNatIidOidArray[u4InId][u4OutId], pDeleteNode,
                  tIidOidArrayDynamicListNode *)
    {
        if (pDeleteNode->pDynamicEntry == pDynamicEntry)
        {
            u4IpAddr = pDeleteNode->pDynamicEntry->u4LocIpAddr;
            u4LocKey = (u4IpAddr & NAT_ONLY_HASH_MASK) % NAT_ONLY_HASH_LIMIT;
            NatDeleteNatOnlyNode (u4LocKey, NAT_FALSE, u4IpAddr,
                                  pDeleteNode->pDynamicEntry->u4IfNum,
                                  pDeleteNode->pDynamicEntry->u1NaptEnable);
            /* There are other dynamic entries using the same port
             * So we should not be freeing it */
            if (u4PortExists == FALSE)
            {
                NatCheckForNapt (pDeleteNode->pDynamicEntry);
            }
            TMO_SLL_Delete (&gNatDynamicList,
                            &(pDynamicEntry->DynamicTableEntry));
            TMO_SLL_Delete (&gaNatIidOidArray[u4InId][u4OutId],
                            &(pDeleteNode->IidOidArrayDynamicListNode));
            if (pDeleteNode->pDynamicEntry->pAppRec != NULL)
            {
                NatUnRegisterAppln (pDeleteNode->pDynamicEntry->pAppRec);
                pDeleteNode->pDynamicEntry->pAppRec = NULL;
            }
            gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfSessionsClosed++;
            gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfActiveSessions--;
            NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                   (UINT1 *) pDeleteNode->pDynamicEntry);
            NatMemReleaseMemBlock (NAT_ARR_DYNAMIC_NODE_POOL_ID,
                                   (UINT1 *) pDeleteNode);
            break;
        }
    }

    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatSipDeletePartialEntries                             *  
*                                                                          *
* Description    :  This function deletes the Corresponding RTCP entry,    *
*                   to the given RTP entry.                                * 
*                                                                          * 
* Input (s)      :  pNatPartialLinkNode - RTP Partial entry                *
*                   pDynamicNode - Dynamic Node associated with RTP        *
*                                                                          *
* Output (s)     :  None                                                   * 
*                                                                          *
* Returns        :  NAT_SUCCESS/NAT_FAILURE                                *
***************************************************************************/
INT1
NatSipDeletePartialEntries (tNatPartialLinkNode * pNatPartialRTPLinkNode,
                            tDynamicEntry * pDynamicNode)
{
    tNatPartialLinkNode *pNatPartialLinkNode = NULL;
    tNatPartialLinkNode *pNatPartialNextNode = NULL;
#ifdef FIREWALL_WANTED
    tPartialInfo        fwlPartialInfo;
#endif
    UNUSED_PARAM (pDynamicNode);
    pNatPartialLinkNode =
        (tNatPartialLinkNode *) TMO_SLL_First (&gNatPartialLinksList);

    for (; pNatPartialLinkNode != NULL;
         pNatPartialLinkNode = pNatPartialNextNode)
    {
        pNatPartialNextNode =
            (tNatPartialLinkNode *) TMO_SLL_Next (&gNatPartialLinksList,
                                                  &(pNatPartialLinkNode->
                                                    sNatPartialLinkNode));

        if ((pNatPartialLinkNode->u1AppCallStatus != NAT_ON_HOLD) &&
            (pNatPartialLinkNode->u1AppCallStatus != NAT_NOT_ON_HOLD))
        {
            continue;
        }
        if ((pNatPartialLinkNode->u1Direction ==
             pNatPartialRTPLinkNode->u1Direction) &&
            (pNatPartialLinkNode->u4OutIpAddr ==
             pNatPartialRTPLinkNode->u4OutIpAddr) &&
            ((pNatPartialLinkNode->u2OutPort ==
              pNatPartialRTPLinkNode->u2OutPort)))
        {
            if ((pNatPartialLinkNode->u2LocPort != NAT_ZERO) &&
                (pNatPartialLinkNode->u2LocPort !=
                 pNatPartialRTPLinkNode->u2LocPort))
            {
                continue;
            }

            /* Let us close the PIN Holes opened in firewall as well */

#ifdef FIREWALL_WANTED
            fwlPartialInfo.LocalIP.v4Addr = pNatPartialLinkNode->u4LocIpAddr;
            fwlPartialInfo.RemoteIP.v4Addr = pNatPartialLinkNode->u4OutIpAddr;
            fwlPartialInfo.u1Direction = pNatPartialLinkNode->u1Direction;
            fwlPartialInfo.u1Protocol = pNatPartialLinkNode->u1PktType;
            fwlPartialInfo.u2LocalPort = pNatPartialLinkNode->u2LocPort;
            fwlPartialInfo.u2RemotePort = pNatPartialLinkNode->u2OutPort;
            fwlPartialInfo.u1AppCallStatus =
                pNatPartialLinkNode->u1AppCallStatus;

            if (pNatPartialLinkNode->u1PktType == NAT_ANY)
            {
                fwlPartialInfo.u1Protocol = NAT_TCP;
                FwlClosePinholeEntry (&fwlPartialInfo);
                fwlPartialInfo.u1Protocol = NAT_UDP;
                FwlClosePinholeEntry (&fwlPartialInfo);
            }
            else
            {
                FwlClosePinholeEntry (&fwlPartialInfo);
            }
#endif
            TMO_SLL_Delete (&gNatPartialLinksList,
                            &(pNatPartialLinkNode->sNatPartialLinkNode));
            NatMemReleaseMemBlock (NAT_PARTIAL_LINKS_LIST_POOL_ID,
                                   (UINT1 *) pNatPartialLinkNode);
        }
    }
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatSipDeleteNaptFwlPinhole                             *
*                                                                          *
* Description    :  This function deletes the firewall pinhole of the      *
*                   given NAPT entry.                                      *
*                                                                          * 
* Input (s)      :  pNatStaticNaptEntry - static NAPT Entry                *
*                                                                          *
* Output (s)     :  None                                                   * 
*                                                                          *
* Returns        :  NAT_SUCCESS/NAT_FAILURE                                *
***************************************************************************/
INT1
NatSipDeleteNaptFwlPinhole (tStaticNaptEntry * pNatStaticNaptEntry)
{
#ifdef FIREWALL_WANTED
    tPartialInfo        fwlPartialInfo;
    fwlPartialInfo.LocalIP.v4Addr = pNatStaticNaptEntry->u4LocIpAddr;
    fwlPartialInfo.u2LocalPort = pNatStaticNaptEntry->u2StartLocPort;
    fwlPartialInfo.u1Direction = NAT_INBOUND;
    fwlPartialInfo.RemoteIP.v4Addr = NAT_ZERO;
    fwlPartialInfo.u2RemotePort = NAT_ZERO;
    fwlPartialInfo.u1AppCallStatus = pNatStaticNaptEntry->u1AppCallStatus;
    fwlPartialInfo.u1Protocol = (UINT1) pNatStaticNaptEntry->u2ProtocolNumber;

    if (pNatStaticNaptEntry->u2ProtocolNumber == NAT_PROTO_ANY)
    {
        fwlPartialInfo.u1Protocol = NAT_TCP;
        FwlClosePinholeEntry (&fwlPartialInfo);
        fwlPartialInfo.u1Protocol = NAT_UDP;
        FwlClosePinholeEntry (&fwlPartialInfo);
    }
    else
    {
        FwlClosePinholeEntry (&fwlPartialInfo);
    }
#else
    UNUSED_PARAM (pNatStaticNaptEntry);
#endif
    return NAT_SUCCESS;
}

/******************************************************************************
 *                                                                            *                      * Function     : NatGetHashPortExistSStatus                                  *
 *                                                                            *                      * Description  : This funtion is used to return whether the
 *                ports exist or not.                                         *                      *                                                                            *
 * Input        : u4TransIP   - Translated IP Address                         *                      *                                                                            *
 * Output       : None                                                        *
 *                                                                            *
 * Return       : Port Exist status                                           *
 *                                                                            *
 ******************************************************************************/
UINT4
NatGetHashPortExistsStatus (UINT4 u4TransIP, UINT2 u2TransPort, UINT4 u4Type)
{
    UINT4               u4GlobalHashKey = NAT_ZERO;
    UINT4               u4PortExists = NAT_ZERO;
    tGlobalHashNode    *pGlobalNodeEntry = NULL;

    u4GlobalHashKey = NatFormHashKey (u4TransIP, u2TransPort, u4Type);
    u4PortExists = FALSE;
    TMO_HASH_Scan_Bucket (gpNatGlobalHashList, u4GlobalHashKey,
                          pGlobalNodeEntry, tGlobalHashNode *)
    {
        if ((pGlobalNodeEntry->u4TranslatedLocIpAddr == u4TransIP)
            && (pGlobalNodeEntry->u2TranslatedLocPort == u2TransPort))

        {                        /* Update the flag indicating that some other dynamic
                                 * entry is also using the same translated port */
            u4PortExists = TRUE;
            break;
        }
    }
    return u4PortExists;
}

/***************************************************************************
* Function Name  :  NatDeleteDynamicEntryWithIPandMask
* Description    :  This function deletes all the entries in a given  address
*             pool in the dynamic table for a particular interface.
*
* Input (s)    :  u4IfNum  - Interface Number
*                 u4IpAddr - Start Ip address
*                 u4NetMask - Mask
* Output (s)   :  None
* Returns      :  None
*
***************************************************************************/

PUBLIC VOID
NatDeleteDynamicEntryWithIPAndMask (UINT4 u4IfNum, UINT4 u4IpAddr,
                                    UINT4 u4NetMask)
{
    UINT4               u4Network = NAT_ZERO;
    UINT4               u4StartIp = NAT_ZERO;
    UINT4               u4BroadCastAddr = NAT_ZERO;
    tDynamicEntry      *pDynamicEntry = NULL;
    tUdpList           *pUdpListNode = NULL;
    tTcpDelNode        *pTcpDelNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;

    u4StartIp = u4IpAddr;
    u4Network = u4IpAddr & u4NetMask;
    u4BroadCastAddr = u4Network | (~u4NetMask & NAT_INIT_MASK /*0xffffffff */ );

    /* Delete UDP session entries pertaining to the input interface */
    pUdpListNode = (tUdpList *) TMO_SLL_First (&gNatUdpList);
    while (pUdpListNode != NULL)
    {
        if (pUdpListNode->pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatUdpList,
                              &(pUdpListNode->IidOidArrayDynamicListNode));
            if ((pUdpListNode->pDynamicEntry->u4TranslatedLocIpAddr >=
                 u4StartIp)
                && (pUdpListNode->pDynamicEntry->u4TranslatedLocIpAddr <=
                    u4BroadCastAddr))
            {
                NatDeleteEntry (pUdpListNode->pDynamicEntry);
                TMO_SLL_Delete (&gNatUdpList,
                                &(pUdpListNode->IidOidArrayDynamicListNode));
                NatMemReleaseMemBlock (NAT_UDP_POOL_ID, (UINT1 *) pUdpListNode);
            }
            pUdpListNode = (tUdpList *) pNextNode;
        }
        else
        {
            pUdpListNode =
                (tUdpList *) TMO_SLL_Next (&gNatUdpList,
                                           &(pUdpListNode->
                                             IidOidArrayDynamicListNode));
        }
    }
/*
 * Here we are adding deletion of TCP nodes using TcpDelStack
 */

    pTcpDelNode = (tTcpDelNode *) TMO_SLL_First (&gNatTcpDelStack);
    while (pTcpDelNode != NULL)
    {
        if (pTcpDelNode->pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
            if ((pTcpDelNode->pDynamicEntry->u4TranslatedLocIpAddr >=
                 u4StartIp) &&
                (pTcpDelNode->pDynamicEntry->u4TranslatedLocIpAddr <=
                 u4BroadCastAddr))
            {
                NatDeleteEntry (pTcpDelNode->pDynamicEntry);
                TMO_SLL_Delete (&gNatTcpDelStack, &(pTcpDelNode->TcpDelNode));
                NatMemReleaseMemBlock (NAT_TCP_STACK_POOL_ID,
                                       (UINT1 *) pTcpDelNode);
            }
            pTcpDelNode = (tTcpDelNode *) pNextNode;
        }
        else
        {
            pTcpDelNode =
                (tTcpDelNode *) TMO_SLL_Next (&gNatTcpDelStack,
                                              &(pTcpDelNode->TcpDelNode));
        }
    }
    /* Delete dynamic session entries pertaining to the input interface */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatDynamicList);

    while (pDynamicEntry != NULL)
    {
        if (pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatDynamicList,
                              &(pDynamicEntry->DynamicTableEntry));
            if ((pDynamicEntry->u4TranslatedLocIpAddr >= u4StartIp) &&
                (pDynamicEntry->u4TranslatedLocIpAddr <= u4BroadCastAddr))
            {
                NatDeleteEntry (pDynamicEntry);
            }
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatDynamicList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }
    /* Delete NFS session entries pertaining to the input interface */
    pDynamicEntry = (tDynamicEntry *) TMO_SLL_First (&gNatNfsList);

    while (pDynamicEntry != NULL)
    {
        if (pDynamicEntry->u4IfNum == u4IfNum)
        {
            pNextNode =
                TMO_SLL_Next (&gNatNfsList,
                              &(pDynamicEntry->DynamicTableEntry));
            if ((pDynamicEntry->u4TranslatedLocIpAddr >= u4StartIp)
                && (pDynamicEntry->u4TranslatedLocIpAddr <= u4BroadCastAddr))
            {
                /*delete from NfsList */
                TMO_SLL_Delete (&gNatNfsList,
                                &(pDynamicEntry->DynamicTableEntry));
                gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfSessionsClosed++;
                gapNatIfTable[pDynamicEntry->u4IfNum]->u4NumOfActiveSessions--;
                NatMemReleaseMemBlock (NAT_DYNAMIC_POOL_ID,
                                       (UINT1 *) pDynamicEntry);
            }
            pDynamicEntry = (tDynamicEntry *) pNextNode;
        }
        else
        {
            pDynamicEntry =
                (tDynamicEntry *) TMO_SLL_Next (&gNatNfsList,
                                                &(pDynamicEntry->
                                                  DynamicTableEntry));
        }
    }
}
#endif /* _NATTABLE_C */
