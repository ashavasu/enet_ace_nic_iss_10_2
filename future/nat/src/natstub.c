/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natstub.c,v 1.3 2011/06/14 11:45:07 siva Exp $
 *
 * Description:This file contains main NAT module functions
 *         and other functions needed for simple NAT translation
 *
 *******************************************************************/
#ifndef _NATSTUB_C
#define _NATSTUB_C_
#include "sipalg_inc.h"
#include "natinc.h"

/***************************************************************************
* Function Name:  NatTranslateInBoundPkt                 *
* Description  :  This function translates the Inbound packet if Global 
*                 Nat is enabled and if the packet needs translation.     
*                                     
* Input (s)    :  1. pBuf - This stores the full IP packet along    *
*                 with TCP/UDP header and payload.          
*                 2. u4InIf - This gives the incoming interface number *
*              
* Output (s)   :  Either
*                  modified IP Packet with all references to Global IP  *
*                  and port translated to local IP and port 
*                 Or 
*                  unmodified IP Packet which is to be just forwarded.
*                 and  Variable pi1FrgReass  showing if the fragments 
*                 are reassembled or not, when the fragments are received.
*
* Returns      :  SUCCESS if the packet is to be forwarded to the Inside
*                 Network irrespctive of whether the packet "needs 
*                 translation" or not.
*                 FAILURE if the packet is to be dropped by the calling 
*                 module.This happens when the packet needs to be 
*                 translated but NAT is unable to do it. 
*              
****************************************************************************/

PUBLIC INT4
NatTranslateInBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4InIf,
                        INT1 *pi1FrgReass)
{

    UNUSED_PARAM (pi1FrgReass);
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (u4InIf);
    return SUCCESS;

}

/***************************************************************************
* Function Name  :  NatTranslateOutBoundPkt         
* Description :  This function translates the outbound packet if Global 
*                Nat is enabled and if the packet needs translation.     
* 
* Input (s)   :  1. pBuf - This stores the full IP packet along    
*                 with TCP/UDP header and payload.          
*                 2. u4OutIf - This gives the outgoing interface
*             
* Output (s)  :Either
*               modified IP packet with all references to Local IP   
*               and port(if NAPT enable) modified to Global IP and  
*               port.The flag pi1FrgReass is set to SUCCESS when
*               re-assembling has been done.              
*              Or 
*               unmodified IP Packet which is to be just forwarded.
*               
* Returns     :SUCCESS- if the packet is to be forwarded to the Outside
*              Network irrespective of whether the packet "needs 
*              translation" or not.
*              FAILURE- if the packet is to be dropped by the calling 
*              module.This happens when the packet needs to be 
*              translated but NAT is unable to do it. 
*              FAILURE - when all the fragments are not received
*
****************************************************************************/

PUBLIC INT4
NatTranslateOutBoundPkt (tCRU_BUF_CHAIN_HEADER ** ppBuf, UINT4 u4OutIf,
                         INT1 *pi1FrgReass)
{
    UNUSED_PARAM (pi1FrgReass);
    UNUSED_PARAM (ppBuf);
    UNUSED_PARAM (u4OutIf);
    return SUCCESS;
}

/*****************************************************************************/
/* Function     : NatTmrHandleExpiry                                         */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a timer*/
/*                expiry occurs. This procedure finds the expired timers and */
/*                invokes the corresponding timer routines.                  */
/*                NOTE:                                                      */
/*                -----                                                      */
/*                This function would be for the whole NAT when we migrate   */
/*                to the new timer approach.                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
NatTmrHandleExpiry (VOID)
{
    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name : NaptProcessTimeOut                                        */
/*  Description   :                                                           */
/*                : This procedure calls the appropriate procedures           */
/*                  to handle the timer expiry condition.                     */
/*  Parameter(s)  : None                                                      */
/*  Return Values : VOID                                                      */
/******************************************************************************/

PUBLIC VOID
NaptProcessTimeOut (tTimerListId TimerListId)
{
    UNUSED_PARAM (TimerListId);
    return;
}

INT4
NatSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    UNUSED_PARAM (pu1ModName);
    return NAT_SUCCESS;
}

/***************************************************************************
* Function Name  :  NatDeleteDynamicEntry
* Description    :  This function deletes all the entries in the
*             dynamic table for a particular interface if NAT is
*             disabled for that interface.
*
* Input (s)    :  u4IfNum  - Interface Number which has been disabled
*
* Output (s)   :  None
* Returns      :  None
*
***************************************************************************/

PUBLIC VOID
NatDeleteDynamicEntry (UINT4 u4IfNum)
{
    UNUSED_PARAM (u4IfNum);
    return;
}

/*********************************************************************
*  Function Name : NatProcessTimeOut
*
*  Description   : This procedure takes appropriate action on the expiry
*                  of NAT_TIMER.
*  Parameter(s)    : NONE
*
*  Return Values : VOID
*********************************************************************/

PUBLIC VOID
NatProcessTimeOut (VOID)
{
    return;
}

/*****************************************************************************
 *
 *    Function Name        : NatUtilIsIkeNattPassthroughTraffic
 *
 *    Description          : Checks whether the received packet already has 
 *                           NAT translation in its database or not
 *
 *    Input(s)             : Cru BUf
 *
 *    Output(s)            : None.
 *
 *    Returns              : OSIX_SUCCESS / OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT4
NatUtilIsIkeNattPassthroughTraffic (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (pBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : NatUtilIsVPNPassthroughTraffic
 *
 *    Description          : Checks whether the given initiator cookie is 
 *                           present in NAT database or not
 *
 *    Input(s)             : pu1InitiatorCookie - Pointer to initiator cookie
 *
 *    Output(s)            : None.
 *
 *    Returns              : OSIX_SUCCESS / OSIX_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT4
NatUtilIsVPNPassthroughTraffic (UINT1 *pu1InitiatorCookie)
{
    UNUSED_PARAM (pu1InitiatorCookie);
    return OSIX_SUCCESS;
}

/*********************************************************************
*  Function Name : NatSipAlgProcessTimeOut
*
*  Description   : This procedure takes appropriate action on the expiry
*                  of NAT_SIP_TIMER.
*  Parameter(s)    : NONE
*
*  Return Values : VOID
*********************************************************************/
PUBLIC VOID
NatSipAlgProcessTimeOut (tTimerListId TimerListId)
{
    UNUSED_PARAM (TimerListId);
    return;
}
#endif /* _NATSTUB_C */
