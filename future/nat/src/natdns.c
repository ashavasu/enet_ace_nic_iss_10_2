/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: natdns.c,v 1.6 2013/10/25 10:50:23 siva Exp $
 *
 * Description:This file contains callback routines of dns
 *             ALG. It also contains functions for parsing
 *             the dns packets and modifying the dns payload.
 *
 *******************************************************************/
#ifndef _NATDNS_C
#define _NATDNS_C
#include "natinc.h"

/*****************************************************************************
     PROTOTYPES OF DNS_ALG FUNCTIONS
*****************************************************************************/
PRIVATE UINT4       NatSetDnsCacheTimeOut
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Value, UINT4 u4AnswerOffset,
           UINT4 u4PktSize));
PRIVATE UINT4       NatGetIpAddrFromDnsRR
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4AnswerOffset,
           UINT4 u4PktSize));
PRIVATE UINT4       NatGetDnsAnCount
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType));
PRIVATE UINT4       NatGetDnsArCount
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType));
PRIVATE UINT4       NatGetDnsQdCount
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType));
PRIVATE UINT4       NatGetDnsNsCount
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType));
PRIVATE UINT4       NatConstructDnsResponsePkt
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo));
PRIVATE UINT4       NatChangeDnsPayload
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4TranslatedLocIpAddress,
           UINT4 u4AnswerOffset, UINT4 u4PktSize));
PRIVATE UINT4       NatGetDnsQuerybit
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType));
PRIVATE UINT2       NatGetDnsAnswerOrQueryType
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType, UINT4,
           UINT4 u4PktSize));
PRIVATE UINT4       NatCalculateQuestionOffset
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4QuestionNumber,
           UINT1 u1PktType, UINT4 u4PktSize));
PRIVATE UINT4       NatCalculateAnswerOffset
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4AnswerNo, UINT1 u1PktType,
           UINT4 u4PktSize));

/*****************************************************************************
          DNS_ALG FUNCTIONS
*****************************************************************************/

/********************************************************************************  Function Name    : NatSetDnsCacheTimeOut
*  Description        : This sets the TTL field in the Resource record of the
*             DNS packet.
*
*  Input(s)        : pBuf - pointer to the Dns packet.
*             u4Value - Value of the TTL field.
*             u4AnswerOffset - Starting position of the Dns answer
*             record whose TTL field has to be set.
*  Output(s)        : pBuf - Modified Dns packet.
*  Returns        : NAT_SUCCESS
*******************************************************************************/
PRIVATE UINT4
NatSetDnsCacheTimeOut (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Value,
                       UINT4 u4AnswerOffset, UINT4 u4PktSize)
{
    UINT4               u4ChksumOffset = NAT_ZERO;
    UINT2               u2ChkSum = NAT_ZERO;
    UINT4               u4OldValue = NAT_ZERO;
    UINT1               u1DomainNameDelimiter = NAT_ZERO;
    INT1                ai1TempValue1[NAT_IPADDR_LEN_MAX + NAT_ONE]
        = { NAT_ZERO };
    INT1                ai1TempValue[NAT_IPADDR_LEN_MAX + NAT_ONE]
        = { NAT_ZERO };
    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;

    u1DomainNameDelimiter = NAT_ZERO;
    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatSetDnsCacheTimeOut \n");
    NAT_DBG (NAT_DBG_DNS, "\n Started Parsing the Domain name Section \n");
    while (u4AnswerOffset <= u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, &u1DomainNameDelimiter,
                                   u4AnswerOffset, NAT_ONE_BYTE);
        NAT_DBG1 (NAT_DBG_DNS, "\n DomainNameDelimiter %d \n",
                  u1DomainNameDelimiter);

        /* Scanning for TTL field */
        if ((u1DomainNameDelimiter == NAT_ZERO) ||
            (u1DomainNameDelimiter == NAT_DNS_MESSAGE_COMMPRESS_VALUE))
        {

            if (u1DomainNameDelimiter == NAT_DNS_MESSAGE_COMMPRESS_VALUE)
            {
                u4AnswerOffset = u4AnswerOffset + NAT_TWO;
                NAT_DBG (NAT_DBG_DNS, "\n Message compression is there \n");
            }                    /* This is to take care of message commpression in DNS RRs */

            u4AnswerOffset = u4AnswerOffset + NAT_DNS_RR_TYPE_CLASS_LEN;
            NAT_DBG1 (NAT_DBG_DNS, "\n TTL Offset in the RR is %d \n",
                      u4AnswerOffset);
            break;
        }
        u4AnswerOffset += (UINT4) u1DomainNameDelimiter + NAT_ONE;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4OldValue, u4AnswerOffset,
                               NAT_IP_ADDR_LEN);
    u4ChksumOffset = NatGetIpHeaderLength (pBuf) + NAT_UDP_CKSUM_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ChkSum, u4ChksumOffset,
                               NAT_WORD_LEN);
    u4Value = OSIX_HTONL (u4Value);
    if (u4AnswerOffset % MODULO_DIVISOR_2 != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1TempValue1,
                                   u4AnswerOffset - NAT_ONE,
                                   NAT_IP_ADDR_LEN + NAT_TWO);

        ai1TempValue1[NAT_INDEX_6] = '\0';
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Value, u4AnswerOffset,
                                   NAT_IP_ADDR_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1TempValue,
                                   u4AnswerOffset - NAT_ONE,
                                   NAT_IP_ADDR_LEN + NAT_TWO);
        ai1TempValue[NAT_INDEX_6] = '\0';

        i4NewLen = (INT4) STRLEN (ai1TempValue);
        i4OldLen = (INT4) STRLEN (ai1TempValue1);

        /* For checksum adjustment the old data & new data should be taken 
           from even offset. Also the old & new lengths should be even */
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) ai1TempValue1,
                           NAT_IP_ADDR_LEN + NAT_TWO,
                           (UINT1 *) ai1TempValue, NAT_IP_ADDR_LEN + NAT_TWO);
    }
    else
    {

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Value, u4AnswerOffset,
                                   NAT_IP_ADDR_LEN);
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) &u4OldValue,
                           NAT_IP_ADDR_LEN, (UINT1 *) &u4Value,
                           NAT_IP_ADDR_LEN);
    }
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2ChkSum, u4ChksumOffset,
                               NAT_WORD_LEN);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatSetDnsCacheTimeOut \n")
        UNUSED_PARAM (i4NewLen);
    UNUSED_PARAM (i4OldLen);
    return NAT_SUCCESS;

}

/********************************************************************************  Function Name    : NatCalculateAnswerOffset
*  Description        : Given the AnswerNumber, this function will return its
*             start offset in the DNS packet.
*
*  Input(s)        : pBuf - pointer to Dns packet.
*             u4AnswerNo - serial Number of the answer.
*             u1PktType - signifies TCP or UDP.
*  Output(s)        : None
*  Returns        : AnswerOffset
*******************************************************************************/
PRIVATE UINT4
NatCalculateAnswerOffset (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4AnswerNo,
                          UINT1 u1PktType, UINT4 u4PktSize)
{
    UINT4               u4FirstAnswerOffset = NAT_ZERO;
    UINT4               u4TempAnswerNo = NAT_ZERO;
    UINT2               u2ResourceDataLength = NAT_ZERO;
    UINT4               u4QdCount = NAT_ZERO;
    UINT4               u4AnswerOffset = NAT_ZERO;
    UINT1               u1DomainNameDelimiter = NAT_ZERO;

    u1DomainNameDelimiter = NAT_ZERO;
    u2ResourceDataLength = NAT_ZERO;
    u4FirstAnswerOffset = NAT_ZERO;
    u4TempAnswerNo = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatCalculateAnswerOffset \n");
    u4QdCount = NatGetDnsQdCount (pBuf, u1PktType);
    NAT_DBG1 (NAT_DBG_DNS, "\n Total Number of Questions is %d \n", u4QdCount);
    u4FirstAnswerOffset =
        NatCalculateQuestionOffset
        (pBuf, u4QdCount + NAT_ONE_BYTE, u1PktType, u4PktSize);
    NAT_DBG1 (NAT_DBG_DNS, "\n Offset of the first Answer RR is %d \n",
              u4FirstAnswerOffset);

    if (u4AnswerNo == NAT_ONE)
    {
        u4AnswerOffset = u4FirstAnswerOffset;
    }
    else
    {
        u4AnswerOffset = u4FirstAnswerOffset;
        u4TempAnswerNo = u4AnswerNo;
        u4TempAnswerNo--;
        while (u4TempAnswerNo > NAT_ZERO)
        {

            NAT_DBG (NAT_DBG_DNS, "\n Started Scanning the Answer Section \n");
            while (u4AnswerOffset <= u4PktSize)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, &u1DomainNameDelimiter,
                                           u4AnswerOffset, NAT_ONE_BYTE);
                NAT_DBG1 (NAT_DBG_DNS, "\n DomainNameDelimiter %d \n",
                          u1DomainNameDelimiter);

                if ((u1DomainNameDelimiter == NAT_ZERO) ||
                    (u1DomainNameDelimiter == NAT_DNS_MESSAGE_COMMPRESS_VALUE))
                {

                    if (u1DomainNameDelimiter ==
                        NAT_DNS_MESSAGE_COMMPRESS_VALUE)
                    {
                        u4AnswerOffset = u4AnswerOffset + NAT_TWO;
                        NAT_DBG (NAT_DBG_DNS,
                                 "\n Message Compression is there \n");
                    }
                    /* This is to take care of message commpression in DNS RRs */

                    u4AnswerOffset =
                        u4AnswerOffset + NAT_DNS_RR_TYPE_CLASS_TTL_LEN;
                    CRU_BUF_Copy_FromBufChain (pBuf,
                                               (UINT1 *) &u2ResourceDataLength,
                                               u4AnswerOffset,
                                               NAT_DNS_RDL_FIELD_LEN);
                    u2ResourceDataLength = OSIX_NTOHS (u2ResourceDataLength);

                    u4AnswerOffset =
                        u4AnswerOffset + NAT_DNS_RDL_FIELD_LEN /* 2 */  +
                        u2ResourceDataLength;
                    u4TempAnswerNo--;
                    NAT_DBG2 (NAT_DBG_DNS,
                              "\n Start Offset of the Answer RR given by\
                              ANswer Number %d is %d \n", u4AnswerNo, u4AnswerOffset);
                    break;
                }
                u4AnswerOffset += (UINT4) u1DomainNameDelimiter + NAT_ONE;
            }
        }
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCalculateAnswerOffset \n");
    return u4AnswerOffset;

}

/********************************************************************************  Function Name    : NatCalculateQuestionOffset
*  Description        : Given the QuestionNumber, this function returns
*             its start offset in the DNS packet.
*
*  Input(s)        : pBuf - pointer to Dns packet.
*             u4QuestionNo - serial Number of the Question record.
*             u1PktType - signifies TCP or UDP.
*  Output(s)        : None
*  Returns        : QuestionOffset
*******************************************************************************/
PRIVATE UINT4
NatCalculateQuestionOffset (tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4QuestionNumber, UINT1 u1PktType,
                            UINT4 u4PktSize)
{

    UINT4               u4TempQuestionNumber = NAT_ZERO;
    UINT4               u4TransportHeaderLength = NAT_ZERO;
    UINT4               u4QuestionOffset = NAT_ZERO;
    UINT1               u1DomainNameDelimiter = NAT_ZERO;

    u4TempQuestionNumber = NAT_ZERO;
    u4QuestionOffset = NAT_ZERO;
    u1DomainNameDelimiter = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatCalculateQuestionOffset \n");
    u4TransportHeaderLength = NatGetTransportHeaderLength (pBuf, u1PktType);

    if (u1PktType == NAT_UDP)
    {
        u4QuestionOffset = NatGetIpHeaderLength (pBuf) + NAT_DNS_HEADER_LENGTH +
            u4TransportHeaderLength;
    }
    else
    {
        u4QuestionOffset = NatGetIpHeaderLength (pBuf) + NAT_DNS_HEADER_LENGTH +
            NAT_DNS_TCP_LEN_OFFSET + u4TransportHeaderLength;

    }

    if (u4QuestionNumber == NAT_ONE)
    {
        return u4QuestionOffset;
    }
    else
    {
        u4TempQuestionNumber = u4QuestionNumber;
        u4TempQuestionNumber--;
        while (u4TempQuestionNumber > NAT_ZERO)
        {
            while (u4QuestionOffset <= u4PktSize)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, &u1DomainNameDelimiter,
                                           u4QuestionOffset, NAT_ONE_BYTE);

                if (u1DomainNameDelimiter == NAT_ZERO)
                {
                    u4QuestionOffset = u4QuestionOffset + NAT_FOUR;
                    /* u4QdCount--; */
                    break;
                }
                /*  u4QuestionOffset += 1;  */
                u4QuestionOffset = u4QuestionOffset + u1DomainNameDelimiter +
                    NAT_ONE;
                NAT_DBG1 (NAT_DBG_PKT_FLOW, "Q offset %d", u4QuestionOffset);
            }
            u4TempQuestionNumber--;
        }
    }
    NAT_DBG2 (NAT_DBG_DNS, "\n Start Offset of the question given by \
                                        question Number %d is %d \n", u4QuestionNumber, u4QuestionOffset);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatCalculateQuestionOffset \n");
    return (u4QuestionOffset + NAT_ONE);
}

/********************************************************************************  Function Name    : NatGetIpAddrFromDnsRR
*  Description        : This function gets the IP address from DNS resource
*             record(RR).
*
*  Input(s)        : pBuf - Pointer to the DNS packet.
*             u4AnswerOffset - Starting offset of the RR.
*  Output(s)        : None
*  Returns        : IP address from the DNS RR.
*******************************************************************************/
PRIVATE UINT4
NatGetIpAddrFromDnsRR (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4AnswerOffset,
                       UINT4 u4PktSize)
{
    UINT4               u4IpAddr = NAT_ZERO;
    UINT1               u1DomainNameDelimiter = NAT_ZERO;

    u4IpAddr = NAT_ZERO;
    u1DomainNameDelimiter = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetIpAddrFromDnsRR \n");
    while (u4AnswerOffset <= u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, &u1DomainNameDelimiter,
                                   u4AnswerOffset, NAT_ONE_BYTE);

        if ((u1DomainNameDelimiter == NAT_ZERO) ||
            (u1DomainNameDelimiter == NAT_DNS_MESSAGE_COMMPRESS_VALUE))
        {

            if (u1DomainNameDelimiter == NAT_DNS_MESSAGE_COMMPRESS_VALUE)
            {
                u4AnswerOffset = u4AnswerOffset + NAT_TWO;
                NAT_DBG (NAT_DBG_DNS, "\n Message compression is there \n");
            }
            /* This is to take care of message commpression in DNS RRs */

            u4AnswerOffset = u4AnswerOffset + NAT_DNS_RR_TYPE_CLASS_TTL_LEN;
            u4AnswerOffset = u4AnswerOffset + NAT_DNS_RDL_FIELD_LEN /* 2 */ ;
            break;
        }
        u4AnswerOffset += u1DomainNameDelimiter;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4IpAddr, u4AnswerOffset,
                               NAT_IP_ADDR_LEN);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    NAT_DBG1 (NAT_DBG_DNS,
              "\n IP address from the given Answer offset is  %d \n", u4IpAddr);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetIpAddrFromDnsRR \n");
    return u4IpAddr;
}

/********************************************************************************  Function Name    : NatGetDnsAnCount
*  Description        : This function gets the Total number of answer records
*             in the Dns packet.
*
*  Input(s)        : pBuf - pointer to DNS packet.
*  Output(s)        : None
*  Returns        : Total number of Answer records.
*******************************************************************************/
PRIVATE UINT4
NatGetDnsAnCount (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT2               u2AnCount = NAT_ZERO;
    UINT4               u4DnsAnOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsAnCount \n");

    if (u1PktType == NAT_UDP)
    {
        u4DnsAnOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_AN_OFFSET;
    }
    else
    {
        u4DnsAnOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_AN_OFFSET + NAT_DNS_TCP_LEN_OFFSET;

    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2AnCount, u4DnsAnOffset,
                               NAT_DNS_ANCOUNT_LEN);
    u2AnCount = OSIX_NTOHS (u2AnCount);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsAnCount \n");
    return ((UINT4) u2AnCount);
}

/*******************************************************************************  Function Name    : NatGetDnsArCount
*  Description        : This function gets the Total number of authority
*             records in the Dns packet.
*
*  Input(s)        : pBuf - pointer to DNS packet.
*  Output(s)        : None
*  Returns        : Total number of Authority records.
*******************************************************************************/
PRIVATE UINT4
NatGetDnsArCount (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT2               u2ArCount = NAT_ZERO;
    UINT4               u4DnsArOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsArCount \n");

    if (u1PktType == NAT_UDP)
    {
        u4DnsArOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_AR_OFFSET;
    }
    else
    {
        u4DnsArOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_AR_OFFSET + NAT_DNS_TCP_LEN_OFFSET;

    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ArCount, u4DnsArOffset,
                               NAT_DNS_ARCOUNT_LEN);

    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsArCount \n");
    u2ArCount = OSIX_NTOHS (u2ArCount);
    return ((UINT4) u2ArCount);

}

/*******************************************************************************  Function Name    : NatGetDnsQdCount
*  Description        : This function gets the Total number ofquestion records
*             in the Dns packet.
*
*  Input(s)        : pBuf - pointer to DNS packet.
*  Output(s)        : None
*  Returns        : Total number of Questions.
*******************************************************************************/

PRIVATE UINT4
NatGetDnsQdCount (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT2               u2QdCount = NAT_ZERO;
    UINT4               u4DnsQdOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsQdCount \n");

    if (u1PktType == NAT_UDP)
    {
        u4DnsQdOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_QD_OFFSET;
    }
    else
    {
        u4DnsQdOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_QD_OFFSET + NAT_DNS_TCP_LEN_OFFSET;

    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2QdCount, u4DnsQdOffset,
                               NAT_DNS_QDCOUNT_LEN);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsQdCount \n");
    u2QdCount = OSIX_NTOHS (u2QdCount);
    return ((UINT4) u2QdCount);

}

/******************************************************************************  Function Name    : NatGetDnsNsCount
*  Description        : This function gets the Total number of additional
*             records in the Dns packet.
*
*  Input(s)        : pBuf - pointer to DNS packet.
*  Output(s)        : None
*  Returns        : Total number of additional records.
*******************************************************************************/

PRIVATE UINT4
NatGetDnsNsCount (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT2               u2NsCount = NAT_ZERO;
    UINT4               u4DnsNsOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsNsCount \n");

    if (u1PktType == NAT_UDP)
    {
        u4DnsNsOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_NS_OFFSET;
    }
    else
    {
        u4DnsNsOffset =
            NatGetIpHeaderLength (pBuf) + NatGetTransportHeaderLength (pBuf,
                                                                       u1PktType)
            + NAT_DNS_NS_OFFSET + NAT_DNS_TCP_LEN_OFFSET;

    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2NsCount, u4DnsNsOffset,
                               NAT_DNS_NSCOUNT_LEN);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsNsCount \n");
    u2NsCount = OSIX_NTOHS (u2NsCount);
    return ((UINT4) u2NsCount);
}

/********************************************************************************  Function Name    : NatConstructDnsResponsePkt
*  Description        : This function should swap the src and dest IP address
*             in the packet, set all the above mentioned counts to 0
*             and change the rcode to 5 and change QR bit in the
*             flags field of header section to 1.
*
*  Input(s)        : pBuf - pointer to the DNS packet.
*             u1PktType - TCP or UDP
*  Output(s)        : pBuf - Constructed Response packet.
*  Returns        : NAT_SUCCESS
*******************************************************************************/

PRIVATE UINT4
NatConstructDnsResponsePkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tHeaderInfo * pHeaderInfo)
{
    UINT2               u2DnsFlag = NAT_ZERO;
    UINT2               u2NewDnsFlag = NAT_ZERO;
    UINT4               u4SrcIpAddr = NAT_ZERO;
    UINT4               u4DestIpAddr = NAT_ZERO;
    UINT2               u2SrcPort = NAT_ZERO;
    UINT2               u2DestPort = NAT_ZERO;
    UINT2               u2ChkSum = NAT_ZERO;
    UINT4               u4DnsFlagOffset = NAT_ZERO;
    UINT4               u4AnCount = NAT_ZERO;
    UINT4               u4ArCount = NAT_ZERO;
    UINT4               u4NsCount = NAT_ZERO;
    UINT4               u4Offset = NAT_ZERO;
    UINT4               u4TransportHeaderLength = NAT_ZERO;
    UINT4               u4SrcPortOffset = NAT_ZERO;
    UINT4               u4DestPortOffset = NAT_ZERO;
    UINT4               u4ChksumOffset = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatConstructDnsResponsePkt \n");
    u4SrcIpAddr = NatGetDestIpAddr (pBuf);
    pHeaderInfo->u4InIpAddr = u4SrcIpAddr;
    u4SrcIpAddr = OSIX_HTONL (u4SrcIpAddr);
    u4DestIpAddr = NatGetSrcIpAddr (pBuf);
    pHeaderInfo->u4OutIpAddr = u4DestIpAddr;
    u4DestIpAddr = OSIX_HTONL (u4DestIpAddr);
    u2SrcPort = NatGetDestPort (pBuf);
    pHeaderInfo->u2InPort = u2SrcPort;
    u2SrcPort = OSIX_HTONS (u2SrcPort);
    u2DestPort = (UINT2) NatGetSrcPort (pBuf);
    pHeaderInfo->u2OutPort = u2DestPort;
    u2DestPort = OSIX_HTONS (u2DestPort);
    u4SrcPortOffset = NatGetIpHeaderLength (pBuf);
    u4DestPortOffset = u4SrcPortOffset + NAT_TWO;

    pHeaderInfo->u4Direction = NAT_OUTBOUND;
    u4TransportHeaderLength =
        NatGetTransportHeaderLength (pBuf, pHeaderInfo->u1PktType);
    u4Offset = NatGetIpHeaderLength (pBuf) + u4TransportHeaderLength;
    if (pHeaderInfo->u1PktType == NAT_TCP)
    {
        u4ChksumOffset = NatGetIpHeaderLength (pBuf) + NAT_TCP_CKSUM_OFFSET;
    }
    else
    {
        u4ChksumOffset = NatGetIpHeaderLength (pBuf) + NAT_UDP_CKSUM_OFFSET;
    }
    u4DnsFlagOffset = u4Offset + NAT_DNS_PKT_IDENTIFIER_LENGTH;
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4SrcIpAddr,
                               NAT_IP_SRC_OFFSET /* 12 */ , NAT_IP_ADDR_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4DestIpAddr,
                               NAT_IP_DST_OFFSET /* 16 */ , NAT_IP_ADDR_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2SrcPort,
                               u4SrcPortOffset, NAT_TWO_BYTES);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2DestPort,
                               u4DestPortOffset, NAT_TWO_BYTES);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4AnCount,
                               u4Offset + NAT_DNS_AN_OFFSET,
                               NAT_DNS_ANCOUNT_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4NsCount,
                               u4Offset + NAT_DNS_NS_OFFSET,
                               NAT_DNS_NSCOUNT_LEN);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4ArCount,
                               u4Offset + NAT_DNS_AR_OFFSET,
                               NAT_DNS_ARCOUNT_LEN);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2DnsFlag,
                               u4DnsFlagOffset, NAT_TWO_BYTES);
    u2DnsFlag = OSIX_NTOHS (u2DnsFlag);
    NAT_DBG1 (NAT_DBG_DNS, "\n Dns Flag before setting the MSB and RCODE %d \n",
              u2DnsFlag);

    /* Set the MSB, because it' a response packet */
    u2NewDnsFlag = u2DnsFlag | NAT_DNS_RESPONSE_VALUE;
    /* set the RCODE to 5 because unable to respond due to policy reasons */
    u2NewDnsFlag = (UINT2) ((u2NewDnsFlag & NAT_DNS_RCODE_OFFSET)
                            | NAT_DNS_RCODE_VALUE);
    NAT_DBG1 (NAT_DBG_DNS, "\n Dns Flag After setting the MSB and RCODE %d \n",
              u2DnsFlag);
    u2DnsFlag = (UINT2) OSIX_HTONS (u2DnsFlag);
    u2NewDnsFlag = OSIX_HTONS (u2NewDnsFlag);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2NewDnsFlag, u4DnsFlagOffset,
                               NAT_TWO_BYTES);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ChkSum,
                               u4ChksumOffset, NAT_TWO_BYTES);
    NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) &u2DnsFlag, NAT_TWO,
                       (UINT1 *) &u2NewDnsFlag, NAT_TWO);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2ChkSum,
                               u4ChksumOffset, NAT_TWO_BYTES);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatConstructDnsResponsePkt \n");
    return NAT_SUCCESS;

}

/*******************************************************************************  Function Name    : NatChangeDnsPayload
*  Description        : This function changes the payload of the DNS packet.
*             This replaces the IP address present in the resource
*             record with u4TranslatedLocIpAddress.
*
*  Input(s)        : pBuf - pointer to the DNS packet.
*             u4GlobalIPAddress - this is to be substituted instead *             of the IP address present in the RR.
*             u4AnswerOffset - Starting byte of the Resorce Record.
*  Output(s)        : pBuf - Modified DNS payload.
*  Returns        : NAT_SUCCESS
*******************************************************************************/

PRIVATE UINT4
NatChangeDnsPayload (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT4 u4TranslatedLocIpAddress, UINT4 u4AnswerOffset,
                     UINT4 u4PktSize)
{
    UINT4               u4OldIpAddr = NAT_ZERO;
    UINT4               u4ChksumOffset = NAT_ZERO;
    UINT2               u2ChkSum = NAT_ZERO;
    UINT1               u1Count = NAT_ZERO;
    INT1                ai1TempIpAddr[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT1                ai1TempIpAddr1[NAT_IPADDR_LEN_MAX] = { NAT_ZERO };
    INT4                i4OldLen = NAT_ZERO;
    INT4                i4NewLen = NAT_ZERO;

    u1Count = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatChangeDnsPayload \n");
    while (u4AnswerOffset <= u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Count,
                                   u4AnswerOffset, NAT_ONE);
        if ((u1Count == NAT_ZERO) || (u1Count ==
                                      NAT_DNS_MESSAGE_COMMPRESS_VALUE))
        {

            if (u1Count == NAT_DNS_MESSAGE_COMMPRESS_VALUE)
            {
                u4AnswerOffset = u4AnswerOffset + NAT_TWO;
                NAT_DBG (NAT_DBG_DNS, "\n Message compression is there \n");
            }                    /* This is to take care of message 
                                   commpression in DNS RRs */

            u4AnswerOffset = u4AnswerOffset + NAT_DNS_RR_TYPE_CLASS_TTL_LEN;
            /*CRU_BUF_Copy_FromBufChain( pBuf, (UINT1 *)&u2ResourceDataLength,
               u4AnswerOffset, NAT_DNS_RDL_FIELD_LEN ); */
            /*u2ResourceDataLength = OSIX_NTOHS( u2ResourceDataLength ); */
            u4AnswerOffset = u4AnswerOffset + NAT_DNS_RDL_FIELD_LEN /* 2 */ ;
            break;
        }
        u4AnswerOffset += u1Count;
    }
    /*u4AnswerOffset++; */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4OldIpAddr, u4AnswerOffset,
                               NAT_IP_ADDR_LEN);
    u4ChksumOffset = NatGetIpHeaderLength (pBuf) + NAT_UDP_CKSUM_OFFSET;
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2ChkSum, u4ChksumOffset,
                               NAT_WORD_LEN);

    u4TranslatedLocIpAddress = OSIX_HTONL (u4TranslatedLocIpAddress);
    if (u4AnswerOffset % MODULO_DIVISOR_2 != NAT_ZERO)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1TempIpAddr1,
                                   u4AnswerOffset - NAT_ONE,
                                   NAT_IP_ADDR_LEN + NAT_TWO);
        ai1TempIpAddr1[NAT_INDEX_6] = '\0';
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4TranslatedLocIpAddress,
                                   u4AnswerOffset, NAT_IP_ADDR_LEN);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) ai1TempIpAddr,
                                   u4AnswerOffset - NAT_ONE,
                                   NAT_IP_ADDR_LEN + NAT_TWO);
        ai1TempIpAddr[NAT_INDEX_6] = '\0';

        i4NewLen = (INT4) STRLEN (ai1TempIpAddr);
        i4OldLen = (INT4) STRLEN (ai1TempIpAddr1);
        /* For checksum adjustment the old data & new data should be taken 
           from even offset. Also the old & new lengths should be even */
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) ai1TempIpAddr1,
                           NAT_IP_ADDR_LEN + NAT_TWO,
                           (UINT1 *) ai1TempIpAddr, NAT_IP_ADDR_LEN + NAT_TWO);
    }
    else
    {

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4TranslatedLocIpAddress,
                                   u4AnswerOffset, NAT_IP_ADDR_LEN);
        NatChecksumAdjust ((UINT1 *) &u2ChkSum, (UINT1 *) &u4OldIpAddr,
                           NAT_IP_ADDR_LEN,
                           (UINT1 *) &u4TranslatedLocIpAddress,
                           NAT_IP_ADDR_LEN);
    }
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2ChkSum, u4ChksumOffset,
                               NAT_WORD_LEN);

    /*
     * IP address present in the RRs will be of length 4 bytes( Not a string). 
     * So there will be no change in size of the packet after substitution.
     */
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatChangeDnsPayload \n");
    UNUSED_PARAM (i4NewLen);
    UNUSED_PARAM (i4OldLen);
    return NAT_SUCCESS;
}

/*******************************************************************************
*  Function Name    : NatGetDnsQuerybit
*  Description        : This is to find out whether the DNS packet is a query
*             or response packet.
*
*  Input(s)        : pBuf - pointer to the DNS packet.
*             u1PktType - TCP/UDP packet.
*  Output(s)        : None
*  Returns        : NAT_DNS_QUERY or NAT_DNS_RESPONSE
*******************************************************************************/

PRIVATE UINT4
NatGetDnsQuerybit (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType)
{
    UINT2               u2DnsPktType = NAT_ZERO;
    UINT4               u4FlagOffset = NAT_ZERO;
    UINT4               u4TransportHeaderLength = NAT_ZERO;
    UINT4               u4ReturnVal = NAT_ZERO;

    u2DnsPktType = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsQuerybit \n");
    u4TransportHeaderLength = NatGetTransportHeaderLength (pBuf, u1PktType);

    if (u1PktType == NAT_UDP)
    {
        u4FlagOffset = NatGetIpHeaderLength (pBuf) + u4TransportHeaderLength
            + NAT_DNS_PKT_IDENTIFIER_LENGTH;

    }
    else
    {
        u4FlagOffset = NatGetIpHeaderLength (pBuf) + u4TransportHeaderLength
            + NAT_DNS_PKT_IDENTIFIER_LENGTH + NAT_DNS_TCP_LEN_OFFSET;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2DnsPktType,
                               u4FlagOffset, NAT_TWO_BYTES);
    u2DnsPktType = OSIX_NTOHS (u2DnsPktType);

    /* Take MSB from this two byte */

    u2DnsPktType = (u2DnsPktType & NAT_DNS_RESPONSE_VALUE);

    NAT_DBG1 (NAT_DBG_DNS, "\n Dns Packet type is %d \n", u2DnsPktType);

    /* If MSB is set then it is a response packet */
    if (u2DnsPktType == NAT_DNS_RESPONSE_VALUE)
    {
        u4ReturnVal = NAT_DNS_RESPONSE;
    }
    else
    {
        u4ReturnVal = NAT_DNS_QUERY;
    }
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsQuerybit \n");
    return (u4ReturnVal);
}

/*******************************************************************************
*  Function Name    : NatGetDnsAnswerOrQueryType
*  Description        : This functions finds out the type of the answer or
*             query record.
*
*  Input(s)        : pBuf - pointer to the DNS packet.
*             u1PktType - TCP/UDP packet.
*  Output(s)        : None
*  Returns        : type of the answer or query.( ATYPE or PTR )
*******************************************************************************/

PRIVATE UINT2
NatGetDnsAnswerOrQueryType (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1PktType,
                            UINT4 u4RRNumber, UINT4 u4PktSize)
{
    UINT4               u4QuestionOffset = NAT_ZERO;
    UINT2               u2AnswerOrQueryType = NAT_ZERO;
    UINT1               u1DomainNameDelimiter = NAT_ZERO;

    u1DomainNameDelimiter = NAT_ZERO;
    u2AnswerOrQueryType = NAT_ZERO;

    NAT_DBG (NAT_DBG_ENTRY, "\n Inside NatGetDnsAnswerOrQueryType \n");

    u4QuestionOffset =
        NatCalculateQuestionOffset (pBuf, u4RRNumber, u1PktType, u4PktSize);
    while (u4QuestionOffset <= u4PktSize)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, &u1DomainNameDelimiter,
                                   u4QuestionOffset, NAT_ONE_BYTE);

        if (u1DomainNameDelimiter == NAT_ZERO)
        {
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &u2AnswerOrQueryType,
                                       u4QuestionOffset +
                                       NAT_ONE, NAT_TWO_BYTES);
            u2AnswerOrQueryType = OSIX_NTOHS (u2AnswerOrQueryType);
            break;
        }
        u4QuestionOffset += (UINT4) u1DomainNameDelimiter + NAT_ONE;
    }
    NAT_DBG1 (NAT_DBG_DNS, "\n Type of the Answer or Query is %d \n",
              u2AnswerOrQueryType);
    NAT_DBG (NAT_DBG_EXIT, "\n Exiting NatGetDnsAnswerOrQueryType \n");
    return (u2AnswerOrQueryType);
}

/*******************************************************************************
* Function Name    : NatProcessDnsTcpOrUdp
*  Description        : This function performs the DNS ALG functionality of
*             NAT. This function searches for IP address present
*             in From and Host field of the DNS header and
*             translates those IP addresses, if they are present.
*  Input(s)        : pBuf - Pointer to the DNS packet.
*             pHeaderInfo - Pointer to the structure containing
*             NAT/NAPT entry details corresponding to the session
*             in which the current packet is exchanged.
*  Output(s)        : pBuf - Modified DNS packet.
*  Returns        : i4DnsStatus
*******************************************************************************/
PUBLIC UINT4
NatProcessDnsTcpOrUdp (tCRU_BUF_CHAIN_HEADER * pBuf, tHeaderInfo * pHeaderInfo)
{
    UINT4               u4Direction = NAT_ZERO;
    UINT4               u4DnsPktType = NAT_ZERO;
    UINT2               u2DnsQueryType = NAT_ZERO;
    UINT2               u2DnsAnswerType = NAT_ZERO;
    UINT4               u4AnCount = NAT_ZERO;
    UINT4               u4ArCount = NAT_ZERO;
    UINT4               u4QdCount = NAT_ZERO;
    UINT4               u4NsCount = NAT_ZERO;
    UINT4               u4TempQdCount = NAT_ZERO;
    UINT4               u4TempAnCount = NAT_ZERO;
    UINT4               u4QdNo = NAT_ZERO;
    UINT4               u4AnNo = NAT_ZERO;
    UINT4               u4PrivateAddr = NAT_ZERO;
    UINT4               u4TranslatedLocIpAddr = NAT_ZERO;
    UINT4               u4AnswerOffset = NAT_ZERO;
    INT4                i4DnsStatus = NAT_SUCCESS;
    UINT1               u1PktType = NAT_ZERO;
    UINT4               u4PktSize = NAT_ZERO;

    NAT_TRC (NAT_TRC_ON, "\n Inside NatProcessDnsTcpOrUdp.");

    u4Direction = pHeaderInfo->u4Direction;
    u1PktType = pHeaderInfo->u1PktType;

    /* Bypassing NAT translation for DNS packet which uses tcp and hence we */
    /* are NOT supporting DNS server on TCP,running inside the pvt network */
    if (u1PktType == NAT_TCP)
    {
        return NAT_SUCCESS;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    u4AnCount = NatGetDnsAnCount (pBuf, u1PktType);
    u4ArCount = NatGetDnsArCount (pBuf, u1PktType);
    u4QdCount = NatGetDnsQdCount (pBuf, u1PktType);
    u4NsCount = NatGetDnsNsCount (pBuf, u1PktType);

    NAT_DBG4 (NAT_DBG_DNS, "\n Ancount is %d \n Arcount is %d \n \
             Qdcount is %d \n NsCount is %d \n", u4AnCount, u4ArCount, u4QdCount, u4NsCount);
    UNUSED_PARAM (u4NsCount);
    UNUSED_PARAM (u4ArCount);
    u4DnsPktType = NatGetDnsQuerybit (pBuf, pHeaderInfo->u1PktType);

    if (u4DnsPktType == NAT_DNS_RESPONSE)
    {
        u2DnsAnswerType = NatGetDnsAnswerOrQueryType (pBuf,
                                                      pHeaderInfo->u1PktType,
                                                      NAT_ONE_BYTE, u4PktSize);
    }

    if ((u4DnsPktType == NAT_DNS_QUERY))
    {
        u4TempQdCount = u4QdCount;
        u4QdNo = NAT_ONE;

        while (u4TempQdCount > NAT_ZERO)
        {
            u2DnsQueryType = NatGetDnsAnswerOrQueryType (pBuf,
                                                         pHeaderInfo->u1PktType,
                                                         u4QdNo, u4PktSize);
            if ((u4Direction == NAT_INBOUND)
                && ((u2DnsQueryType == NAT_DNS_PTR_QUERY)
                    || (u2DnsQueryType == NAT_DNS_PTR_QUERY_ALL)))
            {
                /*
                 * construct a response packet with opcode=5(refuse to responsedue
                 * to policy reasons) and set ANCOUNT, NSCOUNT and ARCOUNT to 0 in
                 * the header section and respond.
                 */

                NAT_DBG (NAT_DBG_DNS,
                         "\n Inside PTR query and Inbound packet,"
                         "Construct response\n");
                NAT_TRC (NAT_TRC_ON,
                         "\n Inside PTR query and Inbound packet,"
                         "Construct response\n");
                NatConstructDnsResponsePkt (pBuf, pHeaderInfo);
            }
            u4TempQdCount--;
            u4QdNo++;

        }
    }
    if ((u4Direction == NAT_INBOUND) && (u4DnsPktType == NAT_DNS_RESPONSE))
    {
        NAT_DBG (NAT_DBG_DNS, "\nInside Dns Response and Inbound packet");
        NAT_TRC (NAT_TRC_ON, "\nInside Dns Response and Inbound packet");
        /* Answer */
        if (u4AnCount != NAT_ZERO)
        {
            NAT_DBG (NAT_DBG_DNS,
                     "\n Inside Answer, No Action since no overlapping \n");
            NAT_TRC (NAT_TRC_ON,
                     "\n Inside Answer, No Action since no overlapping \n");
        }
    }
    if ((u4Direction == NAT_OUTBOUND) && (u4DnsPktType == NAT_DNS_RESPONSE))
    {
        NAT_TRC (NAT_TRC_ON, "\n Inside Dns Response and Outbound packet \n");
        NAT_DBG (NAT_DBG_DNS, "\n Inside Dns Response and Outbound packet \n");
        if ((u4QdCount == NAT_ONE) && (u4AnCount >= NAT_ONE) &&
            (u2DnsAnswerType == NAT_DNS_ATYPE_ANSWER))
        {
            u4TempAnCount = u4AnCount;
            u4AnNo = NAT_ONE;
            while (u4TempAnCount > NAT_ZERO)
            {
                u4AnswerOffset = NatCalculateAnswerOffset (pBuf, u4AnNo,
                                                           pHeaderInfo->
                                                           u1PktType,
                                                           u4PktSize);
                u4PrivateAddr =
                    NatGetIpAddrFromDnsRR (pBuf, u4AnswerOffset, u4PktSize);
                u4TranslatedLocIpAddr =
                    NatCreateDnsTable (u4PrivateAddr, pHeaderInfo->u4IfNum);
                if (u4TranslatedLocIpAddr != NAT_ZERO)
                {
                    i4DnsStatus = NAT_SUCCESS;
                    NAT_DBG1 (NAT_DBG_DNS, "\n Dns Status is %d \n",
                              i4DnsStatus);
                    NatChangeDnsPayload (pBuf, u4TranslatedLocIpAddr,
                                         u4AnswerOffset, u4PktSize);

                    /*
                     * set the cache timeout of the resolved global IP
                     * address to zero.
                     */
                    NatSetDnsCacheTimeOut (pBuf, NAT_ZERO,
                                           u4AnswerOffset, u4PktSize);
                }
                u4TempAnCount--;
                u4AnNo++;
            }
        }
    }

    NAT_TRC (NAT_TRC_ON, "\n Exiting NatProcessDnsTcpOrUdp \n");
    return ((UINT4) i4DnsStatus);
}
#endif /* _NATDNS_C */
