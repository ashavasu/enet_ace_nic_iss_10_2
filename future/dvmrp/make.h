#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 02/01/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureDVMRP        |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+

include ../LR/make.h
include ../LR/make.rule

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		= FutureDVMRP
PROJECT_BASE_DIR	= ${BASE_DIR}/dvmrp
PROJECT_MFWD_DIR        = ${BASE_DIR}/mfwd
PROJECT_SOURCE_DIR	= ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	= ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	= ${PROJECT_BASE_DIR}/obj

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = ${PROJECT_INCLUDE_DIR}/dpdata.h \
			 ${PROJECT_INCLUDE_DIR}/dpdef.h \
			 ${PROJECT_INCLUDE_DIR}/dphash.h \
			 ${PROJECT_INCLUDE_DIR}/dpextern.h \
			 ${PROJECT_INCLUDE_DIR}/dpglob.h \
			 ${PROJECT_INCLUDE_DIR}/dpinc.h \
			 ${PROJECT_INCLUDE_DIR}/dproute.h \
			 ${PROJECT_INCLUDE_DIR}/dpport.h \
			 ${PROJECT_INCLUDE_DIR}/dptdfs.h \
			 ${PROJECT_INCLUDE_DIR}/dpmacros.h \
			 ${PROJECT_INCLUDE_DIR}/dpprot.h \
		    	 ${PROJECT_INCLUDE_DIR}/dpnbr.h \
		    	 ${PROJECT_INCLUDE_DIR}/dpsnmp.h \
		   	 ${PROJECT_INCLUDE_DIR}/dpsz.h 
			

ifeq (${SNMP_2}, YES)
PROJECT_INCLUDE_FILES += ${PROJECT_INCLUDE_DIR}/fsdvmrwr.h\
                         ${PROJECT_INCLUDE_DIR}/fsdvmrdb.h
endif

				 
PROJECT_FINAL_INCLUDES_DIRS	= -I$(PROJECT_INCLUDE_DIR) $(COMMON_INCLUDE_DIRS)

ifeq (${MFWD}, YES)
PROJECT_FINAL_INCLUDES_DIRS +=  -I$(PROJECT_MFWD_DIR)
endif

ifeq (${NPAPI}, YES)
PROJECT_FINAL_INCLUDES_DIRS +=  -I$(PROJECT_MFWD_DIR)
endif
	
ifeq (${MBSM}, YES)
PROJECT_INCLUDE_FILES +=  ${BASE_DIR}/inc/mbsm.h
endif
	
PROJECT_FINAL_INCLUDE_FILES	= $(PROJECT_INCLUDE_FILES)


PROJECT_DEPENDENCIES	= $(COMMON_DEPENDENCIES) \
				$(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h


