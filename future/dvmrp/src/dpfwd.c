/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpfwd.c,v 1.19 2014/04/04 10:04:43 siva Exp $
 *
 * Description:Contains functions for handling the Multicast  
 *             datagram from IP module and forward the same  
 *
 *******************************************************************/
#include "dpinc.h"
/* Prototypes */
PRIVATE tForwCacheTbl *DvmrpFmCheckForwCache ARG_LIST ((UINT4, UINT4,
                                                        tDvmrpInterfaceNode *,
                                                        INT4 *));
PRIVATE INT4 DvmrpFmCreateFwdTableEntry ARG_LIST ((UINT4, UINT4, UINT4,
                                                   tDvmrpInterfaceNode *,
                                                   UINT1 *));
PRIVATE void DvmrpFmRemoveFwdCacheEntry ARG_LIST ((tForwCacheTbl **,
                                                   tForwCacheTbl *));
PRIVATE UINT1 DvmrpFmSearchHostTbl ARG_LIST ((UINT4, UINT4, UINT4));
PRIVATE INT4 DvmrpFmCheckForwTable ARG_LIST ((UINT4, UINT4,
                                              tDvmrpInterfaceNode *,
                                              tForwTbl **, tForwCacheTbl **));

/****************************************************************************/
/* Function Name    :  DvmrpFMHandleMcastData                               */
/* Description      :  This function receives the IP Multicast datagrams    */
/*                     from IP and forwards the same on a list of output    */
/*                     interfaces.                                          */
/* Input (s)        :  u4SrcAddr - Source address of the Datagram           */
/*                     u4DestAddr - Multicast group address                 */
/*                     pInterface - Input interface structure               */
/* Output (s)       :  pIntList - Interface list pointer                    */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/

/* changes made in this func - Initialised the output parameter
 * pIntList. 
 * Initiate a Prune upstream if there are no downstream interfaces present
 * to which data can be forwarded.
 */
#ifdef __STDC__
INT4
DvmrpFmHandleMcastData (UINT4 u4SrcAddr, UINT4 u4DestAddr,
                        tDvmrpInterfaceNode * pIfNode, UINT4 *pIntList)
#else
INT4
DvmrpFmHandleMcastData (u4SrcAddr, u4DestAddr, pIfNode, pIntList)
     UINT4               u4SrcAddr;
     UINT4               u4DestAddr;
     tDvmrpInterfaceNode *pIfNode;
     UINT4              *pIntList;
#endif
{
    INT4                i4TblId = 0;
    UINT4               u4PruneMinTime;
    UINT4               u4TempListSize = 2;
    tForwTbl           *pCurFwdEntry;
    tForwCacheTbl      *pFwdCache;
    tOutputInterface   *pOutList;

    if ((gu1DvmrpStatus == DVMRP_DISABLED))
    {
        LOG1 ((CRITICAL, FM, "DVMRP Status Disabled,forwarding not possible"));
        return DVMRP_NOTOK;
    }

    /* Reset the Output interface list */
    MEMSET (pIntList, 0, gu4MaxInterfaces + 1);
    pFwdCache = DvmrpFmCheckForwCache (u4SrcAddr, u4DestAddr,
                                       pIfNode, &i4TblId);

    if ((i4TblId > 0) && (pFwdCache != NULL))
    {
        /* 
         * Normal scenario 
         */
        LOG1 ((INFO, FM, "Forward Cache table entry present"));
        pCurFwdEntry = pFwdCache->pSrcNwInfo;
    }
    else if (i4TblId == 0)
    {
        /*
         * Entry not in Forw Cache Table  
         * Check whether the Source Network information is known to us 
         */
        LOG1 ((INFO, FM, "Forward Cache Table entry not present"));
        if (DvmrpFmCheckForwTable (u4SrcAddr, u4DestAddr,
                                   pIfNode, &pCurFwdEntry, &pFwdCache)
            == DVMRP_NOTOK)
        {
            /* Log */
            return DVMRP_NOTOK;
        }
    }
    else
    {
        /* 
         * Datagram cannot be forwarded. Return DVMRP_NOTOK to the IP module 
         */
        LOG1 ((INFO, FM, "Datagram cannot be forwarded"));
        return DVMRP_NOTOK;
    }

    /* DVMRP_REFRESH_FORW_CACHE (pCurFwdEntry->i4ExpiryTime) ;  */

    /*
     * Entry is available in Forward Cache Table now. Forward the 
     * Data to the list of valid interfaces
     */
    pOutList = pCurFwdEntry->pOutInterface;

    while (pOutList != NULL)
    {
        if (pOutList->u4MinTime == 0)
        {
            pIntList[u4TempListSize++] = pOutList->u4OutInterface;
            LOG2 ((INFO, FM, "Packet forwarded on Interface ",
                   pOutList->u4OutInterface));
        }
        pOutList = pOutList->pNext;
    };
    /* As IP expects the oiflist in the following format:
     * noofOifs - incoming interface - oif1 - oif2 ....
     * incoming interface is also filled here
     */
    pIntList[DVMRP_ZERO] = u4TempListSize - 2;
    pIntList[DVMRP_ONE] = pIfNode->u4IfIndex;

    /* Need to send a Prune upstream if the no downstream nbrs are active */
    if (pIntList[DVMRP_ZERO] == 0)
    {
        u4PruneMinTime = PRUNE_LIFE_TIME;
        DVMRP_FIND_INTERFACE_PRUNETIME (pOutList, u4PruneMinTime);

        if (u4PruneMinTime > 0)
        {
            if ((pCurFwdEntry->u1DeliverFlg == DVMRP_MFWD_DELIVER_MDP) &&
                (pCurFwdEntry->InIface.u1Flag == DATA_RECEIVED_ON_PRUNED_IFACE))
            {
                DvmrpPmSendExponentialPrune (pFwdCache);
            }
            else
            {
                DvmrpPmSendPruneUpstream (pFwdCache, u4PruneMinTime);
                DvmrpMfwdSetDeliverMdpFlag (pCurFwdEntry,
                                            DVMRP_MFWD_DONT_DELIVER_MDP);
            }
        }
        else
        {
            LOG1 ((INFO, FM,
                   "Something wrong. Prune should have been sent up"));
            return DVMRP_NOTOK;
        }
    }
    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmCreateFwdTableEntry                           */
/* Description      :  This function fills a new entry in Forward Table     */
/*                     referring to the appropriate Source network          */
/*                     information in the Route Table                       */
/* Input (s)        :  1. u4RouteTblId - Index into the Route Table         */
/*                     2. u4DestAddr - Destination (Multicast) Group address*/
/*                     3. u4ForwTblEntry - New Table entry which is to be   */
/*                        updated                                           */
/*                     4. u4IfId - Index of the interface table which holds */
/*                        interface on which the datagram is received.      */
/* Output (s)       :  u1OutListFound - DVMRP_TRUE => Output interface list */
/*                     is Not NULL ; DVMRP_FALSE => Output interface NULL   */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/

#ifdef __STDC__
PRIVATE INT4
DvmrpFmCreateFwdTableEntry (UINT4 u4RouteTblId,
                            UINT4 u4DestAddr, UINT4 u4ForwTblEntry,
                            tDvmrpInterfaceNode * pIfNode,
                            UINT1 *u1OutListFound)
#else
PRIVATE INT4
DvmrpFmCreateFwdTableEntry (u4RouteTblId, u4DestAddr,
                            u4ForwTblEntry, pIfNode, u1OutListFound)
     UINT4               u4RouteTblId, u4DestAddr, u4ForwTblEntry, u4IfId;
     UINT1              *u1OutListFound;
#endif
{
    UINT1               u1FoundFlag = DVMRP_FALSE, u1TmpFlag;
    UINT4               u4Index, u4SrcNetwork, u4TmpVal = 0, u4SubnetMask;
    tNextHopType       *pNextList;
    tNextHopTbl        *pNextHopTbl = NULL;
    tForwTbl           *pForwTbl = NULL;
    tRouteTbl          *pRouteTbl = NULL;

    /* All set for adding a new entry */
    pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u4ForwTblEntry);
    DVMRP_FILL_FORWARD_TBL_ENTRY (u4ForwTblEntry, pIfNode->u4IfIndex,
                                  u4DestAddr);

    /* 
     * If the Source network is a directly attached network, set the
     * LOCAL_NETWORK flag 
     */
    if (pForwTbl->u4UpNeighbor == pIfNode->u4IfLocalAddr)
    {
        pForwTbl->InIface.u1State = LOCAL_NETWORK;
    }
    *u1OutListFound = DVMRP_TRUE;

    /* 
     * Now allot Output interfaces list consulting NextHop Table 
     * If there is no entry in the NextHop Table for this Source
     * Network, then create an entry there also
     */
    pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4RouteTblId);
    u4SrcNetwork = pRouteTbl->u4SrcNetwork;
    u4SubnetMask = pRouteTbl->u4SrcMask;

    u1FoundFlag = DvmrpRthSearchNextHopTbl (u4SrcNetwork, u4SubnetMask,
                                            &u4Index);

    /* 
     * Entry found in the Next Hop Table atleast
     */
    if (u1FoundFlag == DVMRP_TRUE)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
        pNextList = pNextHopTbl->pNextHopType;

        while (pNextList != NULL)
        {
            u4TmpVal++;
            LOG2 ((INFO, FM, "Entering for the ..time", u4TmpVal));
            if (pNextList->u1DesigForwFlag == DVMRP_TRUE)
            {
                if (pNextList->u1InType == DVMRP_BRANCH)
                {
                    /* Add another parameter */
                    DvmrpFmAddifList (pForwTbl,
                                      pNextList->u4NHopInterface,
                                      pNextList->pDependentNbrs);
                }
                else
                {

                    /* 
                     * Since it is a leaf, check the Host Membership Table to 
                     * see if any host in the local subnet has subscribed to 
                     * this Multicast Group 
                     */
                    u1TmpFlag = DvmrpFmSearchHostTbl (u4DestAddr,
                                                      pNextList->
                                                      u4NHopInterface,
                                                      u4ForwTblEntry);
                    UNUSED_PARAM (u1TmpFlag);
                }
            }
            pNextList = pNextList->pNextHopTypeNext;
        }                        /* end while */
        /* If no neighbor is added a Prune shd be initiated */
        if (pForwTbl->pOutInterface == NULL)
        {
            *u1OutListFound = DVMRP_FALSE;
        }
    }
    else
    {

        /* 
         * If atleast there are some HOSTS in local network that have
         * subscribed to this Group, add it to the Forward Table
         */

        u1FoundFlag = DvmrpFmSearchHostTbl (u4DestAddr, DVMRP_MAX_INTERFACES,
                                            u4ForwTblEntry);

        /* 
         * since next hop table should not have any
         * entry just for a host's presence in the local subnet
         */
        if (u1FoundFlag == DVMRP_FALSE)
        {
            /* 
             * No entry in HOST MEMBER SHIP TABLE, NEXT HOP TABLE
             * So send a Prune 
             */
            LOG1 ((INFO, FM, "No entry even in Host membership table, "
                   "Send a Prune Upstream"));
            *u1OutListFound = DVMRP_FALSE;
        }
    }
    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmAddifList                                     */
/* Description      :  This function adds an interface to the list of output*/
/*                     interfaces in a Forward Table entry                  */
/* Input (s)        :  1. pForwTbl - Pointer to a Forward Table entry       */
/*                     2. u4IfId - Index of the interface table which holds */
/*                        the interface id.                                 */
/*                     3. pNbrs - List of Downstream dependent Neighbors    */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpFmAddifList (tForwTbl * pForwTbl, UINT4 u4IfId, tNbrAddrList * pNbrs)
#else
INT4
DvmrpFmAddifList (pForwTbl, u4IfId, pNbrs)
     tForwTbl           *pForwTbl;
     UINT4               u4IfId;
     tNbrAddrList       *pNbrs;
#endif
{
    UINT4               u4NbrAddr;
    tNbrPruneList      *pDepNbrs, *pTmpNbr = NULL;
    tOutputInterface   *pOutList, *pTmpList;
    INT1                i1NbrFlg = DVMRP_NOTOK;

    pTmpList = pOutList = pForwTbl->pOutInterface;

    /* 
     * Find the Last node 
     */
    DVMRP_FORW_CHECK_INTERFACE (pTmpList, pOutList, u4IfId);
    if (pTmpList != NULL)
    {
        if ((pNbrs == NULL) && (pTmpList->pDepNbrs != NULL))
        {
            /* Local host coming up */
            pTmpList->u4MinTime = 0;
            return DVMRP_OK;
        }
        else if (!((pNbrs != NULL) && (pTmpList->pDepNbrs == NULL)))
        {

            /* Log entry already present */
            LOG1 ((INFO, FM, "Interface Already present"));
            return DVMRP_NOTOK;
        }
        pOutList = pTmpList;
    }
    else
    {
        /* Allocate a new entry */
        if (pOutList == NULL)
        {
            DVMRP_FORW_ALLOC_NEW_INTERFACE (pOutList, u4IfId);
            pForwTbl->pOutInterface = pOutList;
        }
        else
        {
            DVMRP_FORW_ALLOC_NEW_INTERFACE (pOutList->pNext, u4IfId);
        }

        if (pOutList == NULL)
        {
            LOG1 ((CRITICAL, FM, "Interface block cannot be allocated"));
            return DVMRP_NOTOK;
        }
        LOG1 ((INFO, FM, "Interface block allocated"));
        LOG2 ((INFO, FM, "Interface added in table with Source network",
               pForwTbl->u4SrcNetwork));
        LOG2 ((INFO, FM, "Interface added in table with Group address ",
               pForwTbl->u4GroupAddr));
    }

    pTmpNbr = pOutList->pDepNbrs;
    while (pNbrs != NULL)
    {
        u4NbrAddr = pNbrs->u4NbrAddr;
        DVMRP_CHECK_DEPENDENT_NBR (pOutList, u4NbrAddr, i1NbrFlg);
        if (i1NbrFlg == DVMRP_OK)
        {
            pNbrs = pNbrs->pNbr;
            i1NbrFlg = DVMRP_NOTOK;
            continue;
        }

        DVMRP_FORW_ALLOC_NEW_NEIGHBOR (pDepNbrs);
        if (pDepNbrs == NULL)
        {
            LOG1 ((CRITICAL, FM, "Neighbor block cannot be allocated"));
            return DVMRP_NOTOK;
        }

        LOG2 ((INFO, FM, "Added Neighbor", u4NbrAddr));
        LOG2 ((INFO, FM, " Neighbor Added on interface ", u4IfId));
        if (pOutList->pDepNbrs == NULL)
        {
            pOutList->pDepNbrs = pDepNbrs;
        }
        else
        {
            pTmpNbr->pNbr = pDepNbrs;
        }
        pTmpNbr = pDepNbrs;

        pNbrs = pNbrs->pNbr;
    }
    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmDeleteIfList                                  */
/* Description      :  This function deltes an interface node from a list   */
/*                     of interfaces in the output interface list of a      */
/*                     forward table entry and calculate the minimum Prune  */
/*                     Life time of all the interfaces                      */
/* Input (s)        :  1. pForwTbl - Pointer to a Forward Table entry       */
/*                     2. u4IfId - Index of the interface table which holds */
/*                        the interface id                                  */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
UINT4
DvmrpFmDeleteIfList (tForwTbl * pForwTbl, UINT4 u4IfId, UINT1 u1Flag)
#else
UINT4
DvmrpFmDeleteIfList (pForwTbl, u4IfId, u1Flag)
     tForwTbl           *pForwTbl;
     UINT4               u4IfId;
     UINT1               u1Flag;
#endif
{
    UINT4               u4PruneMinTime;
    tNbrPruneList      *pNbr;
    tOutputInterface   *pOutList, *pTmpList;

    pOutList = pTmpList = pForwTbl->pOutInterface;

    /*
     * Changed Hash define to PRUNE_LIFE_TIME from
     * DEFAULT_PRUNE_TIMER_VAL. Changed in dpfwd.c and dpprune.c wherever
     * this hash was being made used for calculating Prune minimum time
     */
    u4PruneMinTime = PRUNE_LIFE_TIME;

    /* 
     * Check for the matching node 
     */
    while (pTmpList != NULL)
    {
        if (pTmpList->u4OutInterface == u4IfId)
        {

            /* 
             * interface not to be removed either if there are
             * hosts or when there are dependent neighbors
             */
            if ((u1Flag == INT_REMOVE) ||
                ((u1Flag == HOST_LEAVE) && (pTmpList->pDepNbrs == NULL)))
            {
                DvmrpUtilReleaseNbr ((tNbrList *) (pTmpList->pDepNbrs));
                pTmpList->pDepNbrs = NULL;
                LOG2 ((INFO, FM, "All Neighbors Deleted on interface", u4IfId));
                if (pTmpList == pForwTbl->pOutInterface)
                {
                    pForwTbl->pOutInterface = pTmpList->pNext;
                }
                else
                {
                    pOutList->pNext = pTmpList->pNext;
                }
                pTmpList->pNext = NULL;
                DvmrpUtilReleaseIface ((tIfaceList *) pTmpList);
                LOG2 ((INFO, FM, "Deleted Interface ", u4IfId));
                LOG2 ((INFO, FM, "Interface deleted for Source network",
                       pForwTbl->u4SrcNetwork));
                LOG2 ((INFO, FM, "Interface deleted for Group Address",
                       pForwTbl->u4GroupAddr));
            }
            else
            {
                /*  Added Else case */
                pNbr = pTmpList->pDepNbrs;
                DVMRP_FIND_NEIGHBOR_PRUNETIME (pNbr, u4PruneMinTime);
                pTmpList->u4MinTime = u4PruneMinTime;
            }
            break;
        }
        else
        {
            pOutList = pTmpList;
            pTmpList = pTmpList->pNext;
        }
    }

    if (pTmpList == NULL)
    {
        /* Interface not found */
        LOG1 ((INFO, FM, "Interface not present in the output interface list"));
        return 0;
    }
    /* 
     * Calculate the minimum of Prune  Life times of all the nodes 
     * which will be used send the prune to upstream 
     */

    pOutList = pForwTbl->pOutInterface;

    DVMRP_FIND_INTERFACE_PRUNETIME (pOutList, u4PruneMinTime);
    return u4PruneMinTime;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmCheckForwCache                                */
/* Description      :  This function hashes into the Forward Cache Table &  */
/*                     retrieves an entry for a given Source address and    */
/*                     Destination (Multicast) address.                     */
/* Input (s)        :  1. u4SrcAddr - IP address of the Host which          */
/*                        originated multicast datagram                     */
/*                     2. u4DestAddr - Destination Group address            */
/*                     3. pInterface - Ptr to the input interface structure */
/* Output (s)       :  1. i4RetVal = Pointer to return the status of search */
/*                        in the Forward cache table  -1 => invalid packet  */
/*                        1 => entry found 0 => Entry not found             */
/* Returns          :  Pointer to Forward Cache table - On Success          */
/*                     Null - On Failure                                    */
/****************************************************************************/

/* Changes made in this function  - Change the state if there are no 
   downstream routers present */
#ifdef __STDC__
PRIVATE tForwCacheTbl *
DvmrpFmCheckForwCache (UINT4 u4SrcAddr, UINT4 u4DestAddr,
                       tDvmrpInterfaceNode * pIfNode, INT4 *i4RetVal)
#else
PRIVATE tForwCacheTbl *
DvmrpFmCheckForwCache (u4SrcAddr, u4DestAddr, pIfNode, i4RetVal)
     UINT4               u4SrcAddr, u4DestAddr;
     tDvmrpInterfaceNode *pIfNode;
     INT4               *i4RetVal;
#endif
{
    UINT4               u4IfId, u4Index;
    UINT1               u1Flag = DVMRP_FALSE;
    tForwTbl           *pForwTbl;
    tForwCacheTbl      *pFwdCache;

    pFwdCache = (tForwCacheTbl *) (VOID *)
        DvmrpUtilGetCacheEntry (u4SrcAddr, u4DestAddr, DVMRP_FORW_HASH);

    /* 
     * Entry for this Source Address and Dest Group not found 
     */
    if (pFwdCache == NULL)
    {
        *i4RetVal = 0;
        return NULL;
    }

    pForwTbl = pFwdCache->pSrcNwInfo;

    if (pForwTbl == NULL)
    {
        LOG1 ((CRITICAL, FM, "Forward Table entry not present when a Forward"
               " table entry is hit"));
        *i4RetVal = -1;
        return NULL;
    }
    /* 
     * Check for the Upstream neighbor . The check done here can be for an
     * interface also
     */

    u4IfId = pForwTbl->InIface.u4InInterface;

    if (u4IfId != pIfNode->u4IfIndex)
    {
        /* Log Invalid upstream neighbor */
        LOG1 ((CRITICAL, FM, "Packet received from Invalid neighbor "));
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, FM, "Interface Receive Bad packets ",
               pIfNode->u4IfRcvBadPkts));
        *i4RetVal = -1;
        return NULL;
    }
    /* 
     *Very normal scenario 
     */
    LOG1 ((INFO, FM, "Multicast Datagram received From"));
    LOG2 ((INFO, FM, "Source Address    : ", u4SrcAddr));
    LOG2 ((INFO, FM, "Destination Group : ", u4DestAddr));

    /* Expiry time updation */
    DVMRP_REFRESH_FORW_CACHE (pForwTbl->i4ExpiryTime);

    if (pForwTbl->InIface.u1State == DVMRP_NORMAL ||
        pForwTbl->InIface.u1State == LOCAL_NETWORK)
    {
        *i4RetVal = 1;
        return pFwdCache;
    }
    else if (pForwTbl->InIface.u1State == PRUNED)
    {
        if ((pForwTbl->pOutInterface == NULL) &&
            (pForwTbl->InIface.u4PruneRetransTimerVal == 0))
        {
            /* Prune has reached upstream. Reset status to Normal */
            pForwTbl->InIface.u1State = DVMRP_NORMAL;
            *i4RetVal = 1;
            return pFwdCache;
        }
        else
        {
            DVMRP_SEARCH_FOR_HOST (pForwTbl->u4GroupAddr, u4IfId, u1Flag);

            if (u1Flag == DVMRP_FALSE)
            {
                if (pForwTbl->InIface.u2SentPruneTime > 0)
                {
                    pForwTbl->InIface.u1Flag = DATA_RECEIVED_ON_PRUNED_IFACE;
                }
                *i4RetVal = 1;

                LOG1 ((WARNING, FM,
                       "Datagram received from PRUNED upstream interface"));
                return pFwdCache;
            }
        }
    }
    /* 
     * If data is received when we wait for GRAFT_WAITING_ACK, treat is
     * as an ack. and change upstream state to Normal
     */
    else if (pForwTbl->InIface.u1State == GRAFT_WAITING_ACK)
    {
        pForwTbl->InIface.u1State = DVMRP_NORMAL;
        pForwTbl->InIface.u1Flag = DVMRP_NORMAL;
        if (DvmrpUtilStopTimer (DVMRP_GRAFT_RETRANS_TIMER, pFwdCache)
            == DVMRP_NOTOK)
        {
            return NULL;
        }
        *i4RetVal = 1;
        return pFwdCache;
    }

    /* 
     * To discard the invalid Buffer
     */

    *i4RetVal = -1;
    return NULL;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmCacheTimeOutHandler                           */
/* Description      :  This function handles the Age Out timer event for a  */
/*                     Forward Table entry.                                 */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

/* 
 * changes made in this function Decrement Forward table Expiry 
 * time only when the upstream is unpruned.
 * Change the upstream interface state to normal when there is atleast one
 * unpruned interface present in the downstream interface list.
 */
#ifdef __STDC__
void
DvmrpFmCacheTimeOutHandler (tDvmrpTimer * pTimerBlk)
#else
void
DvmrpFmCacheTimeOutHandler (pTimerBlk)
     tDvmrpTimer        *pTimerBlk;
#endif
{
    UINT4               u4Key;
#ifdef MFWD_WANTED
    UINT4               u4LastFwdTime;
    UINT4               u4CrtTime;
    INT4                i4MfwdStatus;
    INT4                i4FwTime;
#endif
#ifdef FS_NPAPI
    INT4                i4HitStatus;
#endif
    tForwTbl           *pForwTbl;
    tForwCacheTbl      *pFwdCache;
    tForwCacheTbl      *pPrev;
    tOutputInterface   *pOutList;

    UNUSED_PARAM (pTimerBlk);

    /* 
     * Decrement timer for all entries in the Forward Table
     * If an entry ages out delete if from teh list and from
     * the hash table if required 
     */

    DVMRP_HASH_SCAN_TABLE (gpForwHashTable, u4Key)
    {

        pPrev = (tForwCacheTbl *) & gpForwHashTable->HashList[u4Key];
        DVMRP_HASH_SCAN_BUCKET (gpForwHashTable, u4Key, pFwdCache,
                                tForwCacheTbl *)
        {

            pForwTbl = pFwdCache->pSrcNwInfo;
            if (pForwTbl == NULL)
            {
                LOG1 ((CRITICAL, FM,
                       "This should not happen Invalid Forward cache entry. "));
                continue;
            }

            if (pForwTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pForwTbl->InIface.u2SentPruneTime > 0)
                {
                    pForwTbl->InIface.u2SentPruneTime -=
                        DEFAULT_CACHE_EXPIRY_TIME;
                }

                if (pForwTbl->i4ExpiryTime > 0)
                {
                    pForwTbl->i4ExpiryTime -= DEFAULT_CACHE_EXPIRY_TIME;
                }
                if (pForwTbl->i4ExpiryTime > 0)
                {
                    /* Decrement prune life time field also */
                    pOutList = pForwTbl->pOutInterface;
                    while (pOutList != NULL)
                    {
                        DVMRP_UPDATE_OIF_NBR_INFO (pOutList);
                        if (pOutList->u4MinTime == 0)
                        {
                            DvmrpMfwdAddOif (pForwTbl,
                                             pOutList->u4OutInterface);
                            pOutList = pOutList->pNext;
                            continue;
                        }
                        if (pOutList->u4MinTime > 0)
                        {
                            pOutList->u4MinTime -= DEFAULT_PRUNE_EXPIRY_TIME;
                        }

                        if (pOutList->u4MinTime == 0)
                        {
                            DvmrpMfwdAddOif (pForwTbl,
                                             pOutList->u4OutInterface);
                        }

                        /*
                         * If there is atleast one interface with Prune time
                         * equal to 0, then the upstream state should not 
                         * longer be Pruned.
                         */

                        if ((pOutList->u4MinTime == 0) &&
                            ((pForwTbl->InIface.u1State == PRUNED) ||
                             (pForwTbl->InIface.u1State == LOCAL_NETWORK)))
                        {
                            if (pForwTbl->InIface.u1State == PRUNED)
                            {
                                pForwTbl->InIface.u1State = DVMRP_NORMAL;
                                pForwTbl->InIface.u1Flag = DVMRP_NORMAL;
                                if (DvmrpUtilStopTimer
                                    (DVMRP_PRUNE_RETRANS_TIMER,
                                     pFwdCache) == DVMRP_OK)
                                {
                                    LOG1 ((INFO, FM,
                                           "Prune Retransmission"
                                           " timer stopped"));
                                }
                                pForwTbl->InIface.u4PruneRetransTimerVal = 0;
                            }

                        }
                        pOutList = pOutList->pNext;
                    }            /* while */
                }
                else
                {
#ifdef MFWD_WANTED
                    DVMRP_GET_MFWD_LAST_FWD_TIME (pFwdCache->u4SrcAddress,
                                                  pFwdCache->u4DestGroup,
                                                  &u4LastFwdTime, i4MfwdStatus);

                    if (i4MfwdStatus == DVMRP_OK)
                    {
                        OsixGetSysTime (&u4CrtTime);
                        i4FwTime = (u4CrtTime - u4LastFwdTime);
                        DVMRP_GET_TIME_IN_SEC (i4FwTime);
                        if (i4FwTime > DVMRP_MAX_CACHE_AGE_OUT_TMR_VAL)
                        {
                            /* Entry Timed out. Remove it from the table */
                            DvmrpFmRemoveFwdCacheEntry (&pFwdCache, pPrev);
                        }
                        else
                        {
                            pForwTbl->i4ExpiryTime =
                                DVMRP_MAX_CACHE_AGE_OUT_TMR_VAL;
                            pOutList = pForwTbl->pOutInterface;
                            while (pOutList != NULL)
                            {
                                DVMRP_UPDATE_OIF_NBR_INFO (pOutList);
                                if (pOutList->u4MinTime == 0)
                                {
                                    DvmrpMfwdAddOif (pForwTbl,
                                                     pOutList->u4OutInterface);
                                    pOutList = pOutList->pNext;
                                    continue;
                                }
                                if (pOutList->u4MinTime > 0)
                                {
                                    pOutList->u4MinTime -=
                                        DEFAULT_PRUNE_EXPIRY_TIME;
                                }

                                if (pOutList->u4MinTime == 0)
                                {
                                    DvmrpMfwdAddOif (pForwTbl,
                                                     pOutList->u4OutInterface);
                                }
                                pOutList = pOutList->pNext;
                            }
                        }
                    }
                    else
                    {
                        /* Entry Timed out. Remove it from the table */
                        DvmrpFmRemoveFwdCacheEntry (&pFwdCache, pPrev);
                    }
#endif
#ifdef FS_NPAPI
                    i4HitStatus = DvmrpNpGetHitBitStatus (pFwdCache);
                    if (i4HitStatus == 0)
                    {
                        DvmrpFmRemoveFwdCacheEntry (&pFwdCache, pPrev);
                    }
                    else
                    {
                        pForwTbl->i4ExpiryTime =
                            DVMRP_MAX_CACHE_AGE_OUT_TMR_VAL;
                        pOutList = pForwTbl->pOutInterface;
                        while (pOutList != NULL)
                        {
                            DVMRP_UPDATE_OIF_NBR_INFO (pOutList);
                            if (pOutList->u4MinTime == 0)
                            {
                                DvmrpMfwdAddOif (pForwTbl,
                                                 pOutList->u4OutInterface);
                                pOutList = pOutList->pNext;
                                continue;
                            }
                            if (pOutList->u4MinTime > 0)
                            {
                                pOutList->u4MinTime -=
                                    DEFAULT_PRUNE_EXPIRY_TIME;
                            }

                            if (pOutList->u4MinTime == 0)
                            {
                                DvmrpMfwdAddOif (pForwTbl,
                                                 pOutList->u4OutInterface);
                            }
                            pOutList = pOutList->pNext;
                        }

                    }
#endif
                }
            }
            else
            {
                /*
                 * Forward Table entry already removed. Now remove the
                 * Forward cache table entry also from the hash.
                 */
                DvmrpFmRemoveFwdCacheEntry (&pFwdCache, pPrev);
            }

            pPrev = pFwdCache;
        }                        /* Scan Bucket */
    }                            /* Scan Table */

    /*
     * Time to start ageout timer
     */
    if (DvmrpUtilStartTimer (DVMRP_CACHE_AGEOUT_TIMER,
                             DVMRP_CACHE_AGE_OUT_TIMER_VAL,
                             NULL) == DVMRP_NOTOK)
    {
        /* Log */
        return;
    }
}

/****************************************************************************/
/* Function Name    :  DvmrpFmSearchRouteTable                              */
/* Description      :  This function searches the Route Table to retrive a  */
/*                     best Route entry for the given Source IP address.    */
/* Input (s)        :  u4SrcAddr - IP address for which the Source network  */
/*                     information to be retrieved from Route table         */
/* Output (s)       :  None                                                 */
/* Returns          :  RouteTable Id                                        */
/****************************************************************************/

#ifdef __STDC__
UINT4
DvmrpFmSearchRouteTable (UINT4 u4SrcAddr)
#else
UINT4
DvmrpFmSearchRouteTable (u4SrcAddr)
     UINT4               u4SrcAddr;
#endif
{
    UINT4               u4BestEntry = MAX_ROUTE_TABLE_ENTRIES,
        u4Count = 0, u4RetVal, u4Index;
    tRouteTbl          *pRouteTbl = NULL;

    for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
    {
        pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);
        /* - Should be checking for NEVER_EXPIRE status also */
        if (((pRouteTbl->u1Status == DVMRP_ACTIVE) ||
             (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT) ||
             (pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE)) &&
            (pRouteTbl->u4SrcNetwork == (u4SrcAddr & pRouteTbl->u4SrcMask)))
        {

            u4RetVal = DvmrpUtilCheckConsOnes (pRouteTbl->u4SrcMask);
            if (u4RetVal > u4Count)
            {
                u4Count = u4RetVal;
                u4BestEntry = u4Index;
            }
        }
    }
    return u4BestEntry;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmDeleteSrcNetInfo                              */
/* Description      :  This function deletes all the entries in Forward     */
/*                     table and forward cache table that corresponds to    */
/*                     a specific source network and subnet mask.           */
/* Input (s)        :  1. u4SrcNetwork - Network IP address of the network  */
/*                        whose Route is deleted from Route Table           */
/*                     2. u4Mask - Subnet mask of the network               */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpFmDeleteSrcNetInfo (UINT4 u4SrcNetwork, UINT4 u4Mask)
#else
void
DvmrpFmDeleteSrcNetInfo (u4SrcNetwork, u4Mask)
     UINT4               u4SrcNetwork, u4Mask;
#endif
{
    UINT4               u4Index;
    tForwTbl           *pForwTbl;
    tForwCacheTbl      *pFwdCache;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pFwdCache = DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index);

        pForwTbl = pFwdCache->pSrcNwInfo;

        if (pForwTbl == NULL)
        {
            continue;
        }
        if (((u4Mask) & (pFwdCache->u4SrcAddress)) == u4SrcNetwork)
        {
            DvmrpFmRemoveFwdCacheEntry (&pFwdCache, pFwdCache);

        }
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmSearchHostTbl                                 */
/* Description      :  This function searches the Host membership table for */
/*                     the given Group address and Interface id and adds an */
/*                     entry for that interface in the Forward Table        */
/* Input (s)        :  1. u4DestGroup - Multicast group on the interface    */
/*                     2. u4IfId - Interface Index                          */
/*                     3. u4ForwTblEntry - Entry id of the Forward Table    */
/*                        which is to be updated                            */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_TRUE - If entry found                          */
/*                     DVMRP_FALSE - Otherwise                              */
/****************************************************************************/
#ifdef __STDC__
PRIVATE UINT1
DvmrpFmSearchHostTbl (UINT4 u4DestGroup, UINT4 u4IfId, UINT4 u4ForwTblEntry)
#else
PRIVATE UINT1
DvmrpFmSearchHostTbl (u4DestGroup, u4IfId, u4ForwTblEntry)
     UINT4               u4DestGroup;
     UINT4               u4IfId;
     UINT4               u4ForwTblEntry;
#endif
{
    UINT4               u4Index, u4IfIndex;
    UINT1               u1Flag = DVMRP_FALSE;
    tDvmrpInterfaceNode *pIfaceTbl = NULL;
    tHostMbrTbl        *pHostTbl = NULL;

    for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
    {
        pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);
        if ((pHostTbl->u1Status == DVMRP_ACTIVE) &&
            (pHostTbl->u4DestGroup == u4DestGroup) &&
            ((pHostTbl->u4LocInterface == u4IfId) ||
             (u4IfId == DVMRP_MAX_INTERFACES)))
        {

            if (u4IfId == DVMRP_MAX_INTERFACES)
            {
                u4IfIndex = pHostTbl->u4LocInterface;
            }
            else
            {
                u4IfIndex = u4IfId;
            }
            /* Do not add Host which is in the upstream interface */
            if (DVMRP_GET_FWDNODE_FROM_INDEX (u4ForwTblEntry)->InIface.
                u4InInterface == u4IfIndex)
            {
                continue;
            }

            pIfaceTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);

            if ((pIfaceTbl == NULL) || !(DVMRP_CHECK_IF_STATUS (pIfaceTbl)))
            {
                return u1Flag;
            }

            /* Entry Found!! */
            DvmrpFmAddifList (DVMRP_GET_FWDNODE_FROM_INDEX (u4ForwTblEntry),
                              pHostTbl->u4LocInterface, NULL);
            /* Should break if the search is made for
             * a given interface alone in the host membership table
             */
            u1Flag = DVMRP_TRUE;
            if (pHostTbl->u4LocInterface == u4IfId)
            {
                break;
            }
        }
    }                            /* end for */
    return u1Flag;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmRemoveFwdCacheEntry                           */
/* Description      :  This function disables a given Forward Table entry   */
/*                     and the corresponding  Forward Table entry           */
/* Input (s)        :  1. pFwdCache - Pointer to Forward Cache table entry  */
/*                     2. pPrev - Pointer to Previous entry in bucket       */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
PRIVATE void
DvmrpFmRemoveFwdCacheEntry (tForwCacheTbl ** pFwdCache, tForwCacheTbl * pPrev)
#else
PRIVATE void
DvmrpFmRemoveFwdCacheEntry (pFwdCache, pPrev)
     tForwCacheTbl     **pFwdCache;
     tForwCacheTbl      *pPrev;
#endif
{
    UINT1               u1Key;
    tForwTbl           *pForwTbl;
    tDpActiveSrcInfo   *pTmpSrcInfo;
    tDpActiveSrcInfo   *pPrevTmpSrcInfo;
    tOutputInterface   *pOutList;

    /* 
     * This function removes the Forward cache table entry specified, from the
     * Hash table and allocates to the Free Pool(gpForwFreePtr).
     * Since we are still in the process of scanning each bucket in the hash
     * table when this function is called, we have to release the current node
     * pFwdCache, the traversing pointer which is deleted now)such that we shd
     * not hamper the further processing of the nodes in teh same bucket. For
     * this purpose we readjust the traversing pointer to point to the Previous
     * node of the deleted node. And so in the next iteration teh node which
     * is next to the deleted node will be processed.
     */

    pForwTbl = (*pFwdCache)->pSrcNwInfo;
    DvmrpMfwdDeleteOneRtEntry (*pFwdCache);
    pPrevTmpSrcInfo = pTmpSrcInfo = pForwTbl->pSrcInfo;
    while (pTmpSrcInfo != NULL)
    {
        if (pTmpSrcInfo->u4SrcAddr == (*pFwdCache)->u4SrcAddress)
        {
            pPrevTmpSrcInfo->pNext = pTmpSrcInfo->pNext;
            pTmpSrcInfo->pNext = NULL;
            DvmrpMemRelease (gDvmrpMemPool.SrcInfoPoolId,
                             (UINT1 *) pTmpSrcInfo);
            pTmpSrcInfo = NULL;
            pForwTbl->u2SrcCnt--;
            break;
        }
        else
        {
            pPrevTmpSrcInfo = pTmpSrcInfo;
            pTmpSrcInfo = pTmpSrcInfo->pNext;
        }
    }
    /* If found in the SrcInfo in the Forw Table, then the SrcInfo is deleted.
     * Now dereference the Forward table from the ForwardCache. 
     */
    (*pFwdCache)->pSrcNwInfo = NULL;
    DVMRP_GET_HASH_INDEX ((*pFwdCache)->u4SrcAddress,
                          (*pFwdCache)->u4DestGroup, u1Key);
    DVMRP_HASH_DELETE_NODE (gpForwHashTable, &((*pFwdCache)->Next),
                            (UINT4) u1Key);

    LOG2 ((INFO, FM, "Removed Forward cache entry with Source Address : ",
           (*pFwdCache)->u4SrcAddress));
    LOG2 ((INFO, FM, "and Group Address : ", (*pFwdCache)->u4DestGroup));
    (*pFwdCache)->u4DestGroup = 0;
    (*pFwdCache)->u4SrcAddress = 0;
    (*pFwdCache)->Next.pNext = (tTMO_SLL_NODE *) (VOID *) gpForwFreePtr;
    gpForwFreePtr = *pFwdCache;
    *pFwdCache = pPrev;

    /*
     * If the status is already inactive, that means that Forward table entry
     *  is already removed and hence we shall just remove the Forward 
     *  cache table entry and quit 
     */
    if (pForwTbl->u2SrcCnt == 0)
    {
        if (pForwTbl->u1Status == DVMRP_ACTIVE)
        {
            pForwTbl->i4ExpiryTime = 0;
            pForwTbl->u1Status = DVMRP_INACTIVE;

            pOutList = pForwTbl->pOutInterface;
            while (pOutList != NULL)
            {
                DvmrpUtilReleaseNbr ((tNbrList *) pOutList->pDepNbrs);
                LOG2 ((INFO, FM, "All Neighbors Deleted on interface",
                       pOutList->u4OutInterface));
                pOutList->pDepNbrs = NULL;
                pOutList = pOutList->pNext;
            }
            DvmrpUtilReleaseIface ((tIfaceList *) pForwTbl->pOutInterface);

            pForwTbl->pOutInterface = NULL;
            pForwTbl->pSrcInfo = NULL;

            LOG1 ((INFO, FM,
                   "All interfaces removed for this forward table entry"));

        }
    }

    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmDeleteIfNbr                                   */
/* Description      :  This function deletes an Neighbor on an interface and*/
/*                     releases the interface node itself if there are no   */
/*                     dependent neighbors on that interface,from the output*/
/*                     interfaces list of a forward table entry and calc    */
/*                     the minimum Prune Life time of all the interfaces    */
/* Input (s)        :  1. pForwTbl - Pointer to a Forward Table entry       */
/*                     2. u4IfId - Index of the interface table which holds */
/*                        the interface id                                  */
/*                     3. u4NbrAddr - Address of the Neighbor which has to  */
/*                        be deleted.                                       */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
UINT4
DvmrpFmDeleteifNbr (tForwTbl * pForwTbl, UINT4 u4IfId, UINT4 u4NbrAddr)
#else
UINT4
DvmrpFmDeleteifNbr (pForwTbl, u4IfId, u4NbrAddr)
     tForwTbl           *pForwTbl;
     UINT4               u4IfId, u4NbrAddr;
#endif
{
    UINT4               u4PruneMinTime, u4Index;
    UINT1               u1Flag = DVMRP_FALSE;
    tNbrPruneList      *pNextNbr, *pTmpNbr = NULL, *pNbr;
    tOutputInterface   *pOutList, *pTmpList;

    pOutList = pTmpList = pForwTbl->pOutInterface;

    u4PruneMinTime = PRUNE_LIFE_TIME;

    /* 
     * Check for the matching node 
     */
    while (pTmpList != NULL)
    {
        if (pTmpList->u4OutInterface == u4IfId)
        {
            pNextNbr = pTmpNbr = pTmpList->pDepNbrs;
            while (pTmpNbr != NULL)
            {
                if (pTmpNbr->u4NbrAddr == u4NbrAddr)
                {
                    if (pTmpNbr == pTmpList->pDepNbrs)
                    {
                        pTmpList->pDepNbrs = pTmpNbr->pNbr;
                    }
                    else
                    {
                        pNextNbr->pNbr = pTmpNbr->pNbr;
                    }
                    pTmpNbr->pNbr = NULL;
                    DvmrpUtilReleaseNbr ((tNbrList *) pTmpNbr);
                    LOG2 ((INFO, FM, "Deleted Neighbor ", u4NbrAddr));
                    LOG2 ((INFO, FM, "Neighbor Deleted on interface", u4IfId));
                    break;
                }
                else
                {
                    pNextNbr = pTmpNbr;
                    pTmpNbr = pTmpNbr->pNbr;
                }
            }                    /* while */

            if (pTmpNbr == NULL)
            {
                LOG1 ((WARNING, FM, "Non existent Neighbor"));
                return 0;
            }

            if (pTmpList->pDepNbrs == NULL)
            {

                /* Added extra check for host */
                DVMRP_SEARCH_FOR_HOST (pForwTbl->u4GroupAddr, u4IfId, u1Flag);

                if (u1Flag == DVMRP_FALSE)
                {
                    if (pTmpList == pForwTbl->pOutInterface)
                    {
                        pForwTbl->pOutInterface = pTmpList->pNext;
                    }
                    else
                    {
                        pOutList->pNext = pTmpList->pNext;
                    }
                    pTmpList->pNext = NULL;
                    DvmrpUtilReleaseIface ((tIfaceList *) pTmpList);
                    LOG2 ((INFO, FM, "Deleted Interface ", u4IfId));
                    LOG2 ((INFO, FM, "Interface deleted for Source network",
                           pForwTbl->u4SrcNetwork));
                    LOG2 ((INFO, FM, "Interface deleted for Group Address",
                           pForwTbl->u4GroupAddr));
                }
                else
                {
                    pTmpList->u4MinTime = 0;
                }
                break;
            }
            DVMRP_FIND_NEIGHBOR_PRUNETIME (pTmpList->pDepNbrs, u4PruneMinTime);
            pTmpList->u4MinTime = u4PruneMinTime;
            break;
        }
        else
        {
            pOutList = pTmpList;
            pTmpList = pTmpList->pNext;
        }
    }                            /* while */

    if (pTmpList == NULL)
    {
        LOG1 ((WARNING, FM, "Non existent interface"));
        return 0;
    }

    /* 
     * Calculate the minimum of Prune  Life times of all the nodes 
     * which will be used send the prune to upstream 
     */

    pOutList = pForwTbl->pOutInterface;

    DVMRP_FIND_INTERFACE_PRUNETIME (pOutList, u4PruneMinTime);

    return u4PruneMinTime;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmAddifNbr                                      */
/* Description      :  This function adds an new Neighbor on an interface to*/
/*                     the list of output interfaces in a Forward Table     */
/*                     entry                                                */
/* Input (s)        :  1. pForwTbl - Pointer to a Forward Table entry       */
/*                     2. u4IfId - Index of the interface table which holds */
/*                        the interface id.                                 */
/*                     3. u4NbrAddr - Address of the new Neighbor on the    */
/*                        downstream interface                              */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpFmAddifNbr (tForwTbl * pForwTbl, UINT4 u4IfId, UINT4 u4NbrAddr)
#else
INT4
DvmrpFmAddifNbr (pForwTbl, u4IfId, u4NbrAddr)
     tForwTbl           *pForwTbl;
     UINT4               u4IfId, u4NbrAddr;
#endif
{
    tNbrPruneList      *pNextNbr;
    tOutputInterface   *pOutList, *pTmpList;
    tOutputInterface   *pChkOutList;
    INT1                i1NbrFlg = DVMRP_NOTOK;

    pTmpList = pOutList = pChkOutList = pForwTbl->pOutInterface;

    /* 
     * Find the Last node 
     */
    DVMRP_FORW_CHECK_INTERFACE (pTmpList, pOutList, u4IfId);

    if (pTmpList != NULL)
    {

        DVMRP_CHECK_DEPENDENT_NBR (pChkOutList, u4NbrAddr, i1NbrFlg);
        if (i1NbrFlg == DVMRP_OK)
        {
            LOG2 ((INFO, FM, "Neighbor Already Added", u4NbrAddr));
            return DVMRP_OK;
        }
        DVMRP_FORW_ALLOC_NEW_NEIGHBOR (pNextNbr);
        if (pNextNbr == NULL)
        {
            LOG1 ((CRITICAL, FM, "Neighbor block cannot be allocated"));
            return DVMRP_NOTOK;
        }
        LOG2 ((INFO, FM, "Added Neighbor", u4NbrAddr));
        LOG2 ((INFO, FM, " Neighbor Added on interface ", u4IfId));
        pNextNbr->pNbr = pTmpList->pDepNbrs;
        pOutList->pDepNbrs = pNextNbr;

        pTmpList->u4MinTime = 0;
    }
    else
    {
        /* Outlist passing to the macro changed */
        /* Allocate a new entry */
        if (pOutList == NULL)
        {
            DVMRP_FORW_ALLOC_NEW_INTERFACE (pOutList, u4IfId);
            pForwTbl->pOutInterface = pOutList;
        }
        else
        {
            DVMRP_FORW_ALLOC_NEW_INTERFACE (pOutList->pNext, u4IfId);
        }

        if (pOutList == NULL)
        {
            LOG1 ((CRITICAL, FM, "Interface block cannot be allocated"));
            return DVMRP_NOTOK;
        }

        LOG1 ((INFO, FM, "Interface block allocated"));
        LOG2 ((INFO, FM, "Interface added in table with Source network",
               pForwTbl->u4SrcNetwork));
        LOG2 ((INFO, FM, "Interface added in table with Group address ",
               pForwTbl->u4GroupAddr));

        DVMRP_FORW_ALLOC_NEW_NEIGHBOR (pOutList->pDepNbrs);
        if (pOutList->pDepNbrs == NULL)
        {
            LOG1 ((CRITICAL, FM, "Neighbor block cannot be allocated"));
            return DVMRP_NOTOK;
        }

        LOG2 ((INFO, FM, "Added Neighbor", u4NbrAddr));
        LOG2 ((INFO, FM, " Neighbor Added on interface ", u4IfId));
        pOutList->u4MinTime = 0;
    }
    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmResetPruneStatus                              */
/* Description      :  This function cancels the Prunes sent by a downstream*/
/*                     Neighbor which has expressed a change in Generation  */
/*                     Id and sends a Graft to the upstream if required     */
/* Input(s)         :  1. u4IfId - Index of the interface table which holds */
/*                        the interface id.                                 */
/*                     2. u4NbrAddr - Address of the Neighbor which         */
/*                        indicated a change in generation id               */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpFmResetPruneStatus (UINT4 u4IfId, UINT4 u4NbrAddr)
#else
void
DvmrpFmResetPruneStatus (u4IfId, u4NbrAddr)
     UINT4               u4IfId, u4NbrAddr;
#endif
{
    UINT4               u4Index;
    tForwTbl           *pForwTbl;
    tNbrPruneList      *pNbr, *pTmpNbr;
    tOutputInterface   *pOutList;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }
        pOutList = pForwTbl->pOutInterface;

        while (pOutList != NULL)
        {

            if (pOutList->u4OutInterface == u4IfId)
            {
                pNbr = pOutList->pDepNbrs;
                DVMRP_SET_NEIGHBOR_PRUNETIME (pNbr, u4NbrAddr, 0, FM);
                pOutList->u4MinTime = 0;
                if ((DvmrpMfwdAddOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
                {
                    LOG1 ((INFO, GM, "Unable to Add Oif in MFWD"));
                }
                break;
            }
            pOutList = pOutList->pNext;
        }

        if (pOutList == NULL)
        {
            /* log interface not present */
            LOG1 ((WARNING, FM, "Non existent Interface"));
            return;
        }
        DvmrpGmSendGraftIfIfacePruned (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                       (u4Index), u4IfId);
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpFmCheckForwTable                                */
/* Description      :  This function checks the Forward table entry for the */
/*                     incoming Source and Destination addresses and if not */
/*                     found then create one if possible and return the     */
/*                     pointer in the output parameter list                 */
/* Input(s)         :  1. u4SrcAddr - IP Address of the Host                */
/*                     2. u4DestAddr - Multicast Group Address              */
/*                     3. pInterface - Pointer to the incoming interface    */
/*                        record                                            */
/* Output (s)       :  1. pFwdEntry - Pointer to Forward Table entry if     */
/*                        entry found or created , otherwise NULL           */
/*                     2. pFwdCache - Pointer to Forward Cache Table entry  */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
PRIVATE INT4
DvmrpFmCheckForwTable (UINT4 u4SrcAddr, UINT4 u4DestAddr,
                       tDvmrpInterfaceNode * pIfNode, tForwTbl ** pFwdEntry,
                       tForwCacheTbl ** pFwdCache)
#else
PRIVATE INT4
DvmrpFmCheckForwTable (u4SrcAddr, u4DestAddr, pIfNode, pFwdEntry, pFwdCache)
     UINT4               u4SrcAddr, u4DestAddr;
     tForwTbl          **pFwdEntry;
     tForwCacheTbl     **pFwdCache;
     tDvmrpInterfaceNode *pIfNode;
#endif
{
    UINT1               u1FoundFlag;
    UINT1               u1Key;
    UINT1               u1OutlistFound;
    UINT4               u4SrcNetwork;
    UINT4               u4IfId = 0;
    UINT4               u4FreeForwTblEntry;
    UINT4               u4RouteTblId;
    UINT4               u4Index;
    tDpActiveSrcInfo   *pSrcNode = NULL;
    tDpActiveSrcInfo   *pSrcInfoNode = NULL;
    tOutputInterface   *pOutList = NULL;

    if (gpForwHashTable == NULL)
    {
        LOG1 ((CRITICAL, FM,
               "Forward Hash table is NULL so exiting DvmrpFmCheckForwTable"));
        return DVMRP_NOTOK;
    }
    if (gpForwFreePtr == NULL)
    {
        LOG1 ((CRITICAL, FM, "Forward cache table entry cannot be allocated"));
        return DVMRP_NOTOK;
    }
    u4IfId = pIfNode->u4IfIndex;
    u4RouteTblId = DvmrpFmSearchRouteTable (u4SrcAddr);

    if (u4RouteTblId == MAX_ROUTE_TABLE_ENTRIES)
    {
        LOG1 ((CRITICAL, FM,
               "No Route Entry so Forward table entry not created"));
        pIfNode->u4IfRcvBadPkts++;
        return DVMRP_NOTOK;
    }
    /*
     * Packet received from invalid upstream interface
     * return NOTOK
     */
    if ((DVMRP_GET_ROUTENODE_FROM_INDEX (u4RouteTblId))->u4IfaceIndex != u4IfId)
    {
        LOG1 ((CRITICAL, FM, "Packet received from Invalid neighbor "));
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, FM, "Interface Receive Bad packets ",
               pIfNode->u4IfRcvBadPkts));
        return DVMRP_NOTOK;
    }

    u4SrcNetwork =
        (DVMRP_GET_ROUTENODE_FROM_INDEX (u4RouteTblId))->u4SrcNetwork;
    u1FoundFlag = DVMRP_FALSE;
    u4FreeForwTblEntry = MAX_FORW_TABLE_ENTRIES;

    for (u4Index = 0; u4Index < MAX_FORW_TABLE_ENTRIES; u4Index++)
    {
        /* 
         * While searching for the Source Network, Get the First available
         * free entry also .
         */
        if (((DVMRP_GET_FWDNODE_FROM_INDEX (u4Index))->u1Status
             == DVMRP_INACTIVE) &&
            (u4FreeForwTblEntry == MAX_FORW_TABLE_ENTRIES))
        {
            u4FreeForwTblEntry = u4Index;
        }
        else if
            (((DVMRP_GET_FWDNODE_FROM_INDEX (u4Index))->u1Status
              == DVMRP_ACTIVE) &&
             (u4SrcNetwork == (DVMRP_GET_FWDNODE_FROM_INDEX
                               (u4Index))->u4SrcNetwork) &&
             (u4DestAddr == (DVMRP_GET_FWDNODE_FROM_INDEX
                             (u4Index))->u4GroupAddr))
        {
            u1FoundFlag = DVMRP_TRUE;
            break;
        }
    }                            /* end for */

    if (u1FoundFlag == DVMRP_FALSE)
    {
        /* 
         * Entry for the Source Network Not found in Forward Table 
         * Check whether it can be created fresh
         */
        if (u4FreeForwTblEntry == MAX_FORW_TABLE_ENTRIES)
        {
            LOG1 ((WARNING, FM, "No Free Forward table Entry available"));
            return DVMRP_NOTOK;
        }

        /* 
         * Now try to create an entry in Forward Table 
         */
        DvmrpFmCreateFwdTableEntry (u4RouteTblId, u4DestAddr,
                                    u4FreeForwTblEntry, pIfNode,
                                    &u1OutlistFound);
        *pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4FreeForwTblEntry);
        LOG2 ((INFO, FM, "Forward Entry created with Source network :",
               (DVMRP_GET_FWDNODE_FROM_INDEX (u4FreeForwTblEntry))->
               u4SrcNetwork));
        LOG2 ((INFO, FM, "and Destination address :",
               (DVMRP_GET_FWDNODE_FROM_INDEX (u4FreeForwTblEntry))->
               u4GroupAddr));

    }
    else
    {
        /* 
         * Entry found in Forward Table 
         */
        *pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4Index);
        LOG2 ((INFO, FM, "Forward Entry present for Source network :",
               (*pFwdEntry)->u4SrcNetwork));
        LOG2 ((INFO, FM, "and Destination address : ",
               (*pFwdEntry)->u4GroupAddr));
        pOutList = (*pFwdEntry)->pOutInterface;
        if ((pOutList == NULL) || (pOutList->u4MinTime > 0))
            u1OutlistFound = DVMRP_FALSE;
        else
            u1OutlistFound = DVMRP_TRUE;

        pSrcNode = (*pFwdEntry)->pSrcInfo;

        if (pSrcNode->u4SrcAddr == u4SrcAddr)
        {
            LOG2 ((INFO, FM, "Source address already Present in the Group",
                   u4SrcAddr));
            return DVMRP_NOTOK;
        }
    }

    /* 
     * Forward Table entry is present. Create Entry in gpForwCacheTable 
     */

    *pFwdCache = gpForwFreePtr;
    gpForwFreePtr = (tForwCacheTbl *) gpForwFreePtr->Next.pNext;

    /* 
     * Fill in fields 
     */
    (*pFwdCache)->u4SrcAddress = u4SrcAddr;
    (*pFwdCache)->u4DestGroup = u4DestAddr;

    (*pFwdCache)->pSrcNwInfo = *pFwdEntry;
    (*pFwdCache)->Next.pNext = NULL;

    /* If the Source Information is not in the list then ...add it at 
     * the head of the list */
    /* Adding the Source Information in a list */
    if (DVMRP_MEM_ALLOCATE (DVMRP_SRC_LIST_PID, pSrcInfoNode, tDpActiveSrcInfo)
        == NULL)
    {
        LOG1 ((WARNING, FM, "Unable to Allocate MemBlock"));
        return DVMRP_NOTOK;
    }
    MEMSET (pSrcInfoNode, 0, sizeof (tDpActiveSrcInfo));
    pSrcInfoNode->u4SrcAddr = u4SrcAddr;
    pSrcInfoNode->u4SrcMask = (DVMRP_GET_ROUTENODE_FROM_INDEX
                               (u4RouteTblId))->u4SrcMask;
    pSrcInfoNode->pNext = NULL;

    pSrcNode = (*pFwdEntry)->pSrcInfo;
    (*pFwdEntry)->pSrcInfo = pSrcInfoNode;
    pSrcInfoNode->pNext = pSrcNode;
    (*pFwdEntry)->u2SrcCnt++;

    if ((DvmrpMfwdCreateRtEntry (*pFwdEntry) == DVMRP_NOTOK))
    {
        LOG1 ((INFO, FM, "Unable to Create Route At MFWD"));
    }

    LOG2 ((INFO, FM, "Added a new Forward Cache Entry with Source address:",
           u4SrcAddr));

    LOG2 ((INFO, FM, "and Group address:", u4DestAddr));
    /* 
     * Add Node to in the appropriate Bucket 
     */
    DVMRP_GET_HASH_INDEX ((*pFwdCache)->u4SrcAddress, (*pFwdCache)->u4DestGroup,
                          u1Key);

    DVMRP_HASH_ADD_NODE (gpForwHashTable, &((*pFwdCache)->Next),
                         (UINT4) u1Key, (UINT1 *) NULL);

    if (u1OutlistFound == DVMRP_FALSE)
    {
        /* 
         * Out interface list not filled . Send a Prune Upstream
         */
        DvmrpPmSendPruneUpstream (*pFwdCache, PRUNE_LIFE_TIME);
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}
