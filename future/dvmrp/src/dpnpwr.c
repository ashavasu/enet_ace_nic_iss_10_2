/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpnpwr.c,v 1.3 2014/12/18 12:12:25 siva Exp $
 *
 * Description:This file contains the wrapper routines in DVMRP that
 *             in turn invoke NPAPIs and NetIp Apis for updating
 *             multicast routes.
 *
 *******************************************************************/

#ifdef FS_NPAPI
#include "dpinc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrAddRoute 
 *
 *    DESCRIPTION      : Adds the multicast route entry to LinuxIp and to NP.
 *
 *    INPUT            :  pIpv4McRouteInfo: structure containing route 
 *                        information  to be updated in LinuxIp and NP.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
DvmrpNpWrAddRoute (tIpv4McRouteInfo * pIpv4McRouteInfo)
{
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pTmpOif = NULL;
    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = pIpv4McRouteInfo->u2NoOfDownStreamIf;

    if (pIpv4McRouteInfo->u2NoOfDownStreamIf != 0)
    {
        NetIpMcRouteInfo.pOIf = MEM_MALLOC
            (sizeof (tNetIpOif) * (pIpv4McRouteInfo->u2NoOfDownStreamIf),
             tNetIpOif);

        if (NetIpMcRouteInfo.pOIf == NULL)
        {
            return OSIX_FAILURE;
        }

        pTmpOif = NetIpMcRouteInfo.pOIf;

        for (u4OifCnt = 0; u4OifCnt < NetIpMcRouteInfo.u4OifCnt; u4OifCnt++)
        {
            pTmpOif->u2TtlThreshold =
                pIpv4McRouteInfo->pDownStreamIf[u4OifCnt].u2TtlThreshold;
            pTmpOif->u4IfIndex =
                pIpv4McRouteInfo->pDownStreamIf[u4OifCnt].u4IfIndex;
            pTmpOif++;
        }
    }

    /* Add the route entry into LinuxIp. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        == NETIPV4_SUCCESS)
    {
        /* Add the route entry into NP */

        if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->u1CallerId,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            /* delete the route added to linuxIp. */
            MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
            NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
            NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
            NetIpMcRouteInfo.u4Iif = u4Iif;

            /* delete route entry from linux ip. */
            if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo,
                                         NETIPV4_DELETE_ROUTE) ==
                NETIPV4_FAILURE)
            {
                i4RetStatus = OSIX_FAILURE;
            }

            i4RetStatus = OSIX_FAILURE;
        }
    }

    if (NetIpMcRouteInfo.pOIf != NULL)
    {
        MEM_FREE (NetIpMcRouteInfo.pOIf);
    }
    return i4RetStatus;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrDelRoute
 *
 *    DESCRIPTION      : Deletes the multicast route entry from LinuxIp and 
 *                       from NP.
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information to be updated in LinuxIp and NP. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
DvmrpNpWrDelRoute (tIpv4McRouteInfo * pIpv4McRouteInfo)
{
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    UINT4               u4Iif = 0;

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;

    /* delete route entry from linux ip. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_DELETE_ROUTE)
        == NETIPV4_SUCCESS)
    {
        /* delete route entry from NP. */
        if (IpmcFsNpIpv4McDelRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrAddOif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to LinuxIp
 *                        and to  adds the new Oif into NP
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route
 *                       information to be updated in LinuxIp and NP.  
 *                       pOifNode: Pointer to the outgoing interfaces 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
DvmrpNpWrAddOif (tIpv4McRouteInfo * pIpv4McRouteInfo,
                 tOutputInterface * pOifNode)
{

    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pTmpDsIf = NULL;
    tOutputInterface   *pOif = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4Iif = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (pOifNode == NULL)
    {
        return OSIX_FAILURE;
    }
    pOif = pOifNode;
    while (pOif != NULL)
    {
        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (u4OifCnt == 0)
    {
        /* We want to add an oif , but the OifCnt is zero.
         * This is inconsistent*/
        return OSIX_FAILURE;
    }

    NetIpMcRouteInfo.pOIf
        = DVMRP_MALLOC (sizeof (tNetIpOif) * u4OifCnt, tNetIpOif);

    if (NetIpMcRouteInfo.pOIf == NULL)
    {
        LOG1 ((WARNING, FM, "Failure allocating memory for installing "
               "entry in NP\n"));
        return OSIX_FAILURE;
    }

    pTmpDsIf = NetIpMcRouteInfo.pOIf;

    while (pOifNode != NULL)
    {
        if ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf
            == pOifNode->u4OutInterface)
        {
            pOifNode = pOifNode->pNext;
            continue;
        }
        u4IfIndex = 0;

        if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                         &u4IfIndex) == NETIPV4_FAILURE)
        {
            LOG2 ((WARNING, FM, "Unable to get Interface index for port %d \n",
                   pOifNode->u4OutInterface));
            DVMRP_MEM_FREE (NetIpMcRouteInfo.pOIf);
            return OSIX_FAILURE;
        }

        pTmpDsIf->u4IfIndex = u4IfIndex;
        pTmpDsIf->u2TtlThreshold = DVMRP_MAX_INTERFACE_TTL;
        pTmpDsIf++;
        pOifNode = pOifNode->pNext;
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;

    /* add the consolidated OifList to LinuxIp */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        == NETIPV4_SUCCESS)
    {
        if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->u1CallerId,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            i4RetStatus = OSIX_FAILURE;
        }
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }

    if (NetIpMcRouteInfo.pOIf != NULL)
    {
        DVMRP_MEM_FREE (NetIpMcRouteInfo.pOIf);
    }

    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrDelOif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to LinuxIp
 *                       and deletes the  Oif from NP
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route 
 *                       information  to be updated in LinuxIp and NP.
 *                       pOifNode: Pointer to the outgoing interfaces 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
DvmrpNpWrDelOif (tIpv4McRouteInfo * pIpv4McRouteInfo,
                 tOutputInterface * pOifNode)
{
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pTmpDsIf = NULL;
    tOutputInterface   *pOif = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4Iif = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4OifPort = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (DVMRP_IP_GET_PORT_FROM_INDEX
        (pIpv4McRouteInfo->pDownStreamIf->u4IfIndex,
         &u4OifPort) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOif = pOifNode;
    while (pOif != NULL)
    {
        if (pOif->u4MinTime != 0)
        {
            /*This is already pruned interface. Should not be in the list */
            pOif = pOif->pNext;
            continue;
        }

        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (pOifNode != NULL)
    {
        NetIpMcRouteInfo.pOIf
            = DVMRP_MALLOC (sizeof (tNetIpOif) * u4OifCnt, tNetIpOif);

        if (NetIpMcRouteInfo.pOIf == NULL)
        {
            LOG1 ((WARNING, FM, "Failure allocating memory for installing "
                   "entry in NP\n"));
            return OSIX_FAILURE;
        }

        pTmpDsIf = NetIpMcRouteInfo.pOIf;

        while (pOifNode != NULL)
        {
            if (((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf
                 == pOifNode->u4OutInterface) || (pOifNode->u4MinTime != 0))
            {
                pOifNode = pOifNode->pNext;
                continue;
            }
            u4IfIndex = 0;

            if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                             &u4IfIndex) == NETIPV4_FAILURE)
            {
                LOG2 ((WARNING, FM,
                       "Unable to get Interface index for port %d \n",
                       pOifNode->u4OutInterface));
                DVMRP_MEM_FREE (NetIpMcRouteInfo.pOIf);
                return OSIX_FAILURE;
            }

            pTmpDsIf->u4IfIndex = u4IfIndex;
            pTmpDsIf->u2TtlThreshold = DVMRP_MAX_INTERFACE_TTL;
            pTmpDsIf++;
            pOifNode = pOifNode->pNext;
        }

    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;

    /* add the consolidated OifList to LinuxIp */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        == NETIPV4_SUCCESS)
    {
        /* delete the Oif from NP. */
        if (IpmcFsNpIpv4McDelRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            i4RetStatus = OSIX_FAILURE;
        }
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }

    if (NetIpMcRouteInfo.pOIf != NULL)
    {
        DVMRP_MEM_FREE (NetIpMcRouteInfo.pOIf);
    }
    return i4RetStatus;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrUpdateIif
 *
 *    DESCRIPTION      : Adds the consolidated multicast route entry to LinuxIp
 *                       and to  adds the new Oif into NP 
 *
 *    INPUT            :  pIpv4McRouteInfo: structure containing route
 *                        information to be updated in LinuxIp and NP.
 *                        pOifNode: Pointer to the outgoing interfaces
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
DvmrpNpWrUpdateIif (tIpv4McRouteInfo * pIpv4McRouteInfo,
                    tOutputInterface * pOifNode)
{
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tOutputInterface   *pOif = NULL;
    tNetIpOif          *pTmpDsIf = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4Iif = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOif = pOifNode;

    while (pOif != NULL)
    {
        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (u4OifCnt != 0)
    {
        NetIpMcRouteInfo.pOIf
            = DVMRP_MALLOC (sizeof (tNetIpOif) * u4OifCnt, tNetIpOif);

        if (NetIpMcRouteInfo.pOIf == NULL)
        {
            LOG1 ((WARNING, FM, "Failure allocating memory for installing "
                   "entry in NP\n"));
            return OSIX_FAILURE;
        }

        pTmpDsIf = NetIpMcRouteInfo.pOIf;

        while (pOifNode != NULL)
        {
            if ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf
                == pOifNode->u4OutInterface)
            {
                pOifNode = pOifNode->pNext;
                continue;
            }
            u4IfIndex = 0;

            if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                             &u4IfIndex) == NETIPV4_FAILURE)
            {
                LOG2 ((WARNING, FM,
                       "Unable to get Interface index for port %d \n",
                       pOifNode->u4OutInterface));
                DVMRP_MEM_FREE (NetIpMcRouteInfo.pOIf);
                return OSIX_FAILURE;
            }

            pTmpDsIf->u4IfIndex = u4IfIndex;
            pTmpDsIf->u2TtlThreshold = DVMRP_MAX_INTERFACE_TTL;
            pTmpDsIf++;
            pOifNode = pOifNode->pNext;
        }
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;

    /* add the consolidated OifList to LinuxIp */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        == NETIPV4_SUCCESS)
    {
        if (IpmcFsNpIpv4McAddRouteEntry (pIpv4McRouteInfo->u4VrId,
                                     pIpv4McRouteInfo->u4GrpAddr,
                                     pIpv4McRouteInfo->u4GrpPrefix,
                                     pIpv4McRouteInfo->u4SrcIpAddr,
                                     pIpv4McRouteInfo->u4SrcIpPrefix,
                                     pIpv4McRouteInfo->u1CallerId,
                                     pIpv4McRouteInfo->rtEntry,
                                     pIpv4McRouteInfo->u2NoOfDownStreamIf,
                                     pIpv4McRouteInfo->pDownStreamIf)
            == FNP_FAILURE)
        {
            i4RetStatus = OSIX_FAILURE;
        }
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DvmrpNpWrUpdateCpuPort
 *
 *    DESCRIPTION      : Adds CPU port to the multicast route entry in LinuxIp
 *                       and in NP; calls the respective NetIp API and NPAPI.
 *
 *    INPUT            : pIpv4McRouteInfo: structure containing route
 *                       information to be updated in LinuxIp and NP.
 *                       u4CpuPortStatus: Contains information whether
 *                       CPU Port is to be added /deleted :
 *                       DVMRP_ADD_CPUPORT/ DVMRP_DELETE_CPUPORT
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
DvmrpNpWrUpdateCpuPort (tIpv4McRouteInfo * pIpv4McRouteInfo,
                        UINT4 u4CpuPortStatus)
{
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pTmpOif = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;

    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (DVMRP_IP_GET_PORT_FROM_INDEX ((UINT4) pIpv4McRouteInfo->rtEntry.u2RpfIf,
                                      &u4Iif) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    NetIpMcRouteInfo.u4SrcAddr = pIpv4McRouteInfo->u4SrcIpAddr;
    NetIpMcRouteInfo.u4GrpAddr = pIpv4McRouteInfo->u4GrpAddr;
    NetIpMcRouteInfo.u4Iif = u4Iif;
    NetIpMcRouteInfo.u4OifCnt = (UINT4) pIpv4McRouteInfo->u2NoOfDownStreamIf;

    if (pIpv4McRouteInfo->u2NoOfDownStreamIf != 0)
    {
        NetIpMcRouteInfo.pOIf = MEM_MALLOC
            (sizeof (tNetIpOif) * (pIpv4McRouteInfo->u2NoOfDownStreamIf),
             tNetIpOif);

        if (NetIpMcRouteInfo.pOIf == NULL)
        {
            return OSIX_FAILURE;
        }

        pTmpOif = NetIpMcRouteInfo.pOIf;
        pTmpDsIf = pIpv4McRouteInfo->pDownStreamIf;

        for (u4OifCnt = 0; u4OifCnt < NetIpMcRouteInfo.u4OifCnt; u4OifCnt++)
        {
            pTmpOif[u4OifCnt].u2TtlThreshold = pTmpDsIf->u2TtlThreshold;
            pTmpOif[u4OifCnt].u4IfIndex = pTmpDsIf->u4IfIndex;
            pTmpDsIf++;
        }
    }

    switch (u4CpuPortStatus)
    {
        case DVMRP_ADD_CPUPORT:

            /* Add Cpu Port to LinuxIp */
            if (NetIpv4McastUpdateRouteCpuPort (&NetIpMcRouteInfo, ENABLED)
                == NETIPV4_SUCCESS)
            {
                /* Add the CPU port to the route entry into NP */
                if (IpmcFsNpIpv4McAddCpuPort (pIpv4McRouteInfo->u4VrId,
                                          pIpv4McRouteInfo->u4GrpAddr,
                                          pIpv4McRouteInfo->u4SrcIpAddr,
                                          pIpv4McRouteInfo->rtEntry)
                    == FNP_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                }
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }

            break;
        case DVMRP_DELETE_CPUPORT:

            /* Delete  Cpu Port from LinuxIp */
            if (NetIpv4McastUpdateRouteCpuPort (&NetIpMcRouteInfo, DISABLED)
                == NETIPV4_SUCCESS)
            {
                /* Add the CPU port to the route entry into NP */
                if (IpmcFsNpIpv4McDelCpuPort (pIpv4McRouteInfo->u4VrId,
                                          pIpv4McRouteInfo->u4GrpAddr,
                                          pIpv4McRouteInfo->u4SrcIpAddr,
                                          pIpv4McRouteInfo->rtEntry)
                    == FNP_FAILURE)
                {
                    i4RetStatus = OSIX_FAILURE;
                }
            }
            else
            {
                i4RetStatus = OSIX_FAILURE;
            }

            break;
        default:
            i4RetStatus = OSIX_FAILURE;
    }

    if (NetIpMcRouteInfo.pOIf != NULL)
    {
        MEM_FREE (NetIpMcRouteInfo.pOIf);
        NetIpMcRouteInfo.pOIf = NULL;
    }
    return i4RetStatus;
}
#endif /*#ifdef FS_NPAPI */
/*-----------------------------------------------------------------------*/
/*                       End of the file dpnpwr.c                        */
/*-----------------------------------------------------------------------*/
