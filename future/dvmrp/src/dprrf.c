/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dprrf.c,v 1.6 2014/04/04 10:04:43 siva Exp $
 *
 * Description:This file has functions which will construct the  
 *             route report packets and send it Output Module.  
 *
 *******************************************************************/
#include "dpinc.h"

/*
 *  Private functions.
 */

PRIVATE UINT4 DvmrpRrfConstructRouteReportPkt ARG_LIST ((UINT4 u4NumRoutes,
                                                         UINT4 u4StartIndex,
                                                         UINT4 u4IfIndex,
                                                         UINT4 *pu4TempNum,
                                                         UINT1 u1Flag));
PRIVATE UINT4       DvmrpRrfChunkRouteReport
ARG_LIST ((UINT4 u4IfaceIndex, UINT4 u4DestAddr, UINT1 u1Flag));

/*
 *  Private variables.
 */

PRIVATE UINT1       gu1RouteSplitTimer;
PRIVATE UINT4       gu4LastSentRouteIndex;    /*  Index of the last entry 
                                               which  was  sent in the 
                                               previous  route report. */

PRIVATE UINT4       gu4NumRoutesToReport;    /*  Number of routes to report 
                                               in   the next route report 
                                               packet.  */
PRIVATE UINT1       gu1NumReportsToSend = 0;    /*  1 or 2 usually as MTU
                                                   will be usually be of 
                                                   fare enough size.  */

PRIVATE UINT4       gu4LastSentRouteIndexNbr;
PRIVATE UINT1       gu1NumReportsToSendNbr = 0;
PRIVATE UINT4       gu4NumRoutesToReportNbr;

/******************************************************************************/
/* Function Name : DvmrpRrfSendRouteTable                                     */
/* Description      : This function will be invoked by the Neighbor discovery    */
/*                 module when it determines that the complete route table    */
/*                 has to be sent to a neighbor.                              */
/* Input(s)         : u4NbrIndex : index in the nbr tbl.                         */
/* Output(s)     : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRrfSendRouteTable (UINT4 u4NbrIndex)
#else

void
DvmrpRrfSendRouteTable (u4NbrIndex)
     UINT4               u4NbrIndex;
#endif
{
    UINT1               u1Flag;
    tNbrTbl            *pNbrTbl;

    pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u4NbrIndex);
    if (gu4NumRoutes == 0)
    {
        LOG1 ((WARNING, RRF, "No Routes to send, returned."));
        return;
    }

    /*
     *  3.2.2 : If a change in generation ID is detected, any prune
     *  information received from the router is no longer valid and
     *  should be flushed.....
     */

    DvmrpFmResetPruneStatus (pNbrTbl->u4NbrIfIndex, pNbrTbl->u4NbrIpAddress);

    /*
     *  I have done some optimization here while sending the route 
     *  reports to a Nbr. by not sending the routes which are in Holdown,
     *  RouteRelearnt Status and the poison metrics on the Nbrs Iface, for this
     *  I may have to set a u1Flag....
     */

    u1Flag = DVMRP_TRUE;
    DvmrpRrfReportNonStop (pNbrTbl->u4NbrIfIndex, pNbrTbl->u4NbrIpAddress,
                           u1Flag);
}

/******************************************************************************/
/* Function Name : DvmrpRrfConstructRouteReportPkt                            */
/* Description      : This function accepts the number of routes to report, the  */
/*                 starting index of the routes in the table as parameters and*/
/*                 will return a route report packet in the chain buffer. It  */
/*                 also returns the index of the entry, which was included as */
/*                 the last entry in the constructed packet.                  */
/* Input(s)         : u4NumRoutes           : Number of routes to send.          */
/*                 u4StartIndex          : Starting index in route table.     */
/*                 u4IfIndex             : Interface index.                   */
/*                 u1Flag                : Set if we are sending optimized    */
/*                                         route reports to Nbr Addr.         */
/* Output(s)      : gpSendBuffer          : May be changed to a new value.     */
/*                 gu4SendBufferSize     : May be changed to a new value.     */
/*                 gu4LastSentRouteIndex : May be changed to a new value.     */
/*                 gu4NumRoutesToReport  : May be changed to a new value.     */
/* Returns         : u4DataLen             : Length of the data.                */
/******************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
DvmrpRrfConstructRouteReportPkt (UINT4 u4NumRoutes,
                                 UINT4 u4StartIndex, UINT4 u4IfIndex,
                                 UINT4 *pu4TempNum, UINT1 u1Flag)
#else

PRIVATE UINT4
DvmrpRrfConstructRouteReportPkt (u4NumRoutes, u4StartIndex,
                                 u4IfIndex, pu4TempNum, u1Flag)
     UINT4               u4NumRoutes;
     UINT4               u4StartIndex;
     UINT4               u4IfIndex;
     UINT4              *pu4TempNum;
     UINT1               u1Flag;
#endif
{
    UINT4               u4DataLen = 0;
    UINT4               u4SrcMask;
    UINT4               u4SrcNetwork;
    UINT1               u1NumBytes;
    UINT1              *pu1Temp;
    UINT4               u4SnmpTmp;
    UINT1               u1Tmp;
    UINT4               u4TmpSrc;
    UINT4               u4TmpMask;
    UINT4               u4TmpRoutes;
    INT4                i4Index;
    tRouteTbl          *pRouteTbl;

    u4TmpRoutes = u4NumRoutes;
    *pu4TempNum = 0;            /* Initialize */
    DVMRP_ALLOC_SEND_BUFFER ();
    if (gpSendBuffer == NULL)
    {
        LOG1 ((CRITICAL, RRF, "Memory failure in send buffer"));
        return u4DataLen;
    }
    pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4StartIndex);

    /* 
     *  Initialize the temp ptr and leave 8 bytes for filling up the IGMP header
     *  later in the output module.
     */

    pu1Temp = gpSendBuffer;
    gpSendBuffer += MAX_IGMP_HDR_LEN;
    while ((u4NumRoutes != 0) && (u4StartIndex < MAX_ROUTE_TABLE_ENTRIES))
    {
        while (pRouteTbl->u1Status == DVMRP_INACTIVE)
        {
            pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (++u4StartIndex);
            (*pu4TempNum)++;
            if (u4StartIndex == MAX_ROUTE_TABLE_ENTRIES)
            {

                /*
                 *  Leave the gpSendBuffer pointing to
                 *  the begining                       
                 */

                gpSendBuffer = pu1Temp;
                return (u4DataLen + MAX_IGMP_HDR_LEN);
            }
        }
        (*pu4TempNum)++;

        /*
         *  No need to report routes to a Neighbor if it is in Holdown
         *  or route relearnt state  
         */

        if ((u1Flag == DVMRP_FALSE) || ((u1Flag == DVMRP_TRUE) &&
                                        (pRouteTbl->u4IfaceIndex != u4IfIndex)
                                        &&
                                        ((pRouteTbl->u1Status == DVMRP_ACTIVE)
                                         || (pRouteTbl->u1Status ==
                                             DVMRP_NEVER_EXPIRE))))
        {

            /*
             * Should be doing 16,8 and 0 bits Right shift to get all the bytes 
             * other than
             * the most significant byte 
             */

            u4TmpMask = u4SrcMask = pRouteTbl->u4SrcMask;
            u1NumBytes = 1;
            if ((*gpSendBuffer++ = (UINT1) (u4TmpMask >> 16)) != 0)
            {
                u1NumBytes = 2;
            }
            u4TmpMask = u4SrcMask;
            if ((*gpSendBuffer++ = (UINT1) (u4TmpMask >> 8)) != 0)
            {
                u1NumBytes = 3;
            }
            u4TmpMask = u4SrcMask;
            if ((*gpSendBuffer++ = (UINT1) (u4TmpMask)) != 0)
            {
                u1NumBytes = 4;
            }
            u4DataLen += 3;
            u4TmpSrc = u4SrcNetwork = pRouteTbl->u4SrcNetwork;
            u1Tmp = 24;
            for (i4Index = 0; i4Index < u1NumBytes; ++i4Index, u1Tmp -= 8)
            {
                *gpSendBuffer++ = (UINT1) (u4TmpSrc >> u1Tmp);
                u4TmpSrc = u4SrcNetwork;
            }

            /*
             *  Assign Metric as it is if the Interface is not an Upstream
             *  Interface , otherwise ADD INFINITY to indicate DEPENDENCY,
             *  and if the entry is in HOLDOWN make the metric INFINITY.
             */

            if (pRouteTbl->u4IfaceIndex == u4IfIndex)
            {
                /* Alas...!!!! dont poison local subnets */
                if (pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE)
                {
                    *gpSendBuffer = pRouteTbl->u1Metric;
                    LOG1 ((INFO, RRF,
                           "Advertising Local subnet without poison metric."));
                }
                else
                {
                    if ((pRouteTbl->u1Status == DVMRP_HOLDOWN) ||
                        (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT))
                    {
                        *gpSendBuffer = DVMRP_INFINITY_METRIC;
                        LOG1 ((INFO, RRF, "Advertising  BAD Route"));
                    }
                    else
                    {
                        *gpSendBuffer =
                            (UINT1) ((pRouteTbl->u1Metric - 1) +
                                     DVMRP_INFINITY_METRIC);
                        /* Advertise Original metric plus infinity */
                        LOG1 ((INFO, RRF,
                               "Expressing Dependancy with poison metric"));
                    }
                }
                /* ST - 02/07 - MSB should be SET always */
                *gpSendBuffer++ |= EIGHTH_BIT_SET;
            }
            else if ((pRouteTbl->u1Status == DVMRP_HOLDOWN) ||
                     (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT))
            {
                *gpSendBuffer++ =
                    (UINT1) (DVMRP_INFINITY_METRIC | EIGHTH_BIT_SET);
                LOG1 ((INFO, RRF, "Advertising  BAD Route"));
            }
            else
            {
                *gpSendBuffer++ =
                    (UINT1) (pRouteTbl->u1Metric | EIGHTH_BIT_SET);
                LOG1 ((INFO, RRF, "Advertising  Normal Route"));
            }
            u4DataLen += u1NumBytes + METRIC_LEN;
            u4SnmpTmp = METRIC_LEN + SIZEOF_MASK + u1NumBytes;
            LOG2 ((INFO, RRF, "Number of bytes Packed in this route is ",
                   u4SnmpTmp));
        }
        pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (++u4StartIndex);

        u4NumRoutes--;
    }                            /* while */
    LOG2 ((INFO, RRF, "Num Routes reported is ", u4TmpRoutes - u4NumRoutes));
    gpSendBuffer = pu1Temp;        /* Leave the gpSendBuffer pointing to the begining */
    return (u4DataLen + MAX_IGMP_HDR_LEN);
}

/******************************************************************************/
/* Function Name : DvmrpRrfRouteReportTimerHandler                            */
/* Description      : This function will be called upon the expiry of a route    */
/*                 report timer, by the Input Handler Module. This function   */
/*                 will send the route report to the nbrs.                    */
/* Input(s)         : None.                                                      */
/* Output(s)      : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRrfRouteReportTimerHandler (tDvmrpTimer * pTimerBlk)
#else

void
DvmrpRrfRouteReportTimerHandler (pTimerBlk)
     tDvmrpTimer        *pTimerBlk;
#endif
{
    UINT1               u1Flag;
    UINT4               u4IfaceIndex;
    UINT4               u4TmpStartIndex = 0;
    UINT4               u4Dummy;
    tDvmrpInterfaceNode *pInterfaceTbl = NULL;
    tTMO_SLL_NODE      *pCurSllNode = NULL;

    UNUSED_PARAM (pTimerBlk);
    if (gu4NumRoutes == 0)
    {
        LOG1 ((WARNING, RRF, "No routes to send"));
        return;
    }
    else
    {
        LOG2 ((INFO, RRF, "routes ACTIVE", gu4NumRoutes));
    }
    LOOP_THROUGH_IFACES;

    /*
     *  After sending the chunk of a route report on each of the active ifaces
     *  start the split timer, so as to report the next chunk when this timer
     *  expires.
     *  Set Route split timer to Default value if it is
     *  zeroi
     */

    if (gu1RouteSplitTimer == 0)
        gu1RouteSplitTimer = DVMRP_ROUTE_REPORT_INTERVAL;
    if ((DvmrpUtilStartTimer (DVMRP_ROUTE_REPORT_TIMER,
                              gu1RouteSplitTimer, NULL)) == DVMRP_NOTOK)
    {
        LOG2 ((INFO, RRF, "SplitTimer Cant be Started for",
               gu1RouteSplitTimer));
    }
    else
    {
        LOG2 ((INFO, RRF, "SplitTimer Started for secs", gu1RouteSplitTimer));
    }
}

/******************************************************************************/
/* Function Name : DvmrpRrfChunkRouteReport                                   */
/* Description      : This function will construct and send the route report to  */
/*                 Nbr or on All DVMRP Routers Addr on either Shutdown evt or */
/*                 Change in GENID or NEW NBR Arrival.                        */
/*                 This function is called locally and from RTH Module.       */
/* Input(s)         : u4IfaceIndex          : Interface index.                   */
/*                 u4DestAddr            : can be either all dvmrp routers or */
/*                                         Nbr Addr                           */
/*                 u1Flag                : Flag for updating                  */
/*                                         gu1NumReportsToSend.               */
/* Output(s)      : gu1NumReportsToSend   : The number of route reports to send*/
/*                 gu4LastSentRouteIndex : Index of in the route table, for   */
/*                                         last route sent out.               */
/*                 gu4NumRoutesToReport  : Number of routes to report in each */
/*                                         route report.                      */
/*                 gu1RouteSplitTimer    : The timer interval between two     */
/*                                         route reports.                     */
/* Returns         : u4Dummy               : Number of routes sent in this chunk*/
/******************************************************************************/

#ifdef __STDC__
PRIVATE UINT4
DvmrpRrfChunkRouteReport (UINT4 u4IfaceIndex, UINT4 u4DestAddr,
                          UINT1 u1IfaceFlag)
#else

PRIVATE UINT4
DvmrpRrfChunkRouteReport (u4IfaceIndex, u4DestAddr, u1IfaceFlag)
     UINT4               u4IfaceIndex;
     UINT4               u4DestAddr;
     UINT1               u1IfaceFlag;
#endif
{
    UINT1               u1Flag;
    UINT1               u1Num;
    UINT4               u4RouteReportSize;
    UINT4               u4DataLen;
    UINT4               u4StartIndex;
    UINT4               u4Dummy;

    /* 
     *  we may not be resonable to all our Nbrs. by having a DVMRP_MIN_MTU, insted of,
     *  u4IfaceMTU = gpIfaceTable[pNbrTbl->u4NbrIfIndex]->gu4IfaceMTU;
     */

    if (u1IfaceFlag == DVMRP_TRUE)
    {
        if (gu1NumReportsToSend == 0)
        {
            gu4LastSentRouteIndex = 0;
            gu4NumRoutesToReport = DVMRP_MIN_MTU / SIZE_OF_EACH_ROUTE;
            LOG2 ((INFO, RRF, "Max routes that can be sent in each chunk",
                   gu4NumRoutesToReport));
            u4RouteReportSize = (UINT4) (gu4NumRoutes * SIZE_OF_EACH_ROUTE);
            DVMRP_MAX_VALUE (u4RouteReportSize, DVMRP_MIN_MTU, u1Num);
            if ((gu4NumRoutesToReport * u1Num) < gu4NumRoutes)
            {
                u1Num++;
            }
            NEAREST_DIVISOR (u1Num);
            gu1NumReportsToSend = u1Num;
            LOG2 ((INFO, RRF, "Reports to send", (UINT4) gu1NumReportsToSend));
            gu1RouteSplitTimer =
                (UINT1) (DVMRP_ROUTE_REPORT_INTERVAL / gu1NumReportsToSend);
        }
        gu1NumReportsToSend--;
        LOG2 ((INFO, RRF, "Remaining Reports to send", gu1NumReportsToSend));
    }
    u4StartIndex = gu4LastSentRouteIndex;
    LOG2 ((INFO, RRF, "Last Sent Route Index", gu4LastSentRouteIndex));

    /*
     *  This Flag is used to Optimize route reports sent to Nbr Addr alone.
     *  So not applicabele here.
     */

    u1Flag = DVMRP_FALSE;

    /*
     *  This check should be there as we may end up looping 
     *  through iface thinking that 10 routes to be sent but when actually 
     *  I have to send just 3 routes alone.
     */

    if (gu4NumRoutes <= gu4NumRoutesToReport)
    {
        gu4NumRoutesToReport = gu4NumRoutes;
    }
    u4DataLen = DvmrpRrfConstructRouteReportPkt (gu4NumRoutesToReport,
                                                 u4StartIndex, u4IfaceIndex,
                                                 &u4Dummy, u1Flag);
    if (u4DataLen == 0)
    {
        LOG1 ((CRITICAL, NDM, "Unable to allocate Mem Block for Send Buffer"));
        return u4Dummy;
    }
    DvmrpOmSendPacket (u4IfaceIndex, (UINT4) u4DestAddr, u4DataLen,
                       DVMRP_REPORT);
    LOG2 ((INFO, RRF, "Message sent to Output Module, size in bytes.",
           u4DataLen));
    return (u4Dummy);
}

/******************************************************************************/
/* Function Name : DvmrpRrfReportNonStop                                      */
/* Description      : This function will construct the route report and send it  */
/*                 to Output Module without splitting the reports over the    */
/*                 time i.e., it sends reports continuesly in chunks.         */
/* Input(s)         : u4IfaceIndex : index to the interface table.               */
/*                 u4DestAddr   : Nbrs addr or All dvmrp Addr.                */
/*                 u1Flag       : This falg will only be set when we report   */
/*                                optimized routes to Nbr Addr alone.         */
/* Output(s)      : Some local global variables will be updated.               */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRrfReportNonStop (UINT4 u4IfaceIndex, UINT4 u4DestAddr, UINT1 u1Flag)
#else

void
DvmrpRrfReportNonStop (u4IfaceIndex, u4DestAddr, u1Flag)
     UINT4               u4IfaceIndex;
     UINT4               u4DestAddr;
     UINT1               u1Flag;
#endif
{
    UINT1               u1Num;
    UINT4               u4RouteReportSizeNbr;
    UINT4               u4StartIndex;
    UINT4               u4DataLen = 0;
    UINT4               u4TempNum;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfaceIndex);

    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, PM, "Invalid Interface Node"));
        return;
    }
    gu4LastSentRouteIndexNbr = 0;
    gu4NumRoutesToReportNbr = (pIfNode->u4IfMTUSize -
                               MAX_IGMP_HDR_LEN -
                               MIN_IP_HDR_LEN) / SIZE_OF_EACH_ROUTE;
    LOG2 ((INFO, RRF, "Routes to report in each chunk",
           gu4NumRoutesToReportNbr));
    u4RouteReportSizeNbr = (UINT4) (gu4NumRoutes * SIZE_OF_EACH_ROUTE);
    DVMRP_MAX_VALUE (u4RouteReportSizeNbr,
                     (pIfNode->u4IfMTUSize -
                      MAX_IGMP_HDR_LEN - MIN_IP_HDR_LEN), u1Num);
    if ((gu4NumRoutesToReportNbr * u1Num) < gu4NumRoutes)
    {
        u1Num++;
    }

    /*
     *  Vishwanath S. & Rajagopalan S.
     *  Need not calculate the nearest divisor here as dont send routes over
     *  the timer expiry.
     *  NEAREST_DIVISOR(u1Num);
     */

    gu1NumReportsToSendNbr = u1Num;
    LOG2 ((INFO, RRF, "Reports to send", gu1NumReportsToSendNbr));
    /* Changed == to > in the exiting condition check */
    for (; gu1NumReportsToSendNbr > 0; gu1NumReportsToSendNbr--)
    {
        /* gu4LastSentRouteIndexNbr += u4TempNum; */
        u4StartIndex = gu4LastSentRouteIndexNbr;
        if (gu4NumRoutes <= gu4NumRoutesToReportNbr)
        {
            gu4NumRoutesToReportNbr = gu4NumRoutes;
        }

        /*
         *  This check should be there as we may end up looping 
         *  through iface thinking that 10 routes to be sent but when actually 
         *  I have to send just 3 routes alone.
         */

        u4DataLen = DvmrpRrfConstructRouteReportPkt (gu4NumRoutesToReportNbr,
                                                     u4StartIndex, u4IfaceIndex,
                                                     &u4TempNum, u1Flag);
        if (u4DataLen == 0)
        {
            LOG1 ((CRITICAL, NDM,
                   "Unable to allocate Mem Block for Send Buffer"));
            return;
        }
        DvmrpOmSendPacket (u4IfaceIndex, (UINT4) u4DestAddr, u4DataLen,
                           DVMRP_REPORT);
        LOG1 ((INFO, RRF, "Message sent to Output Module"));
        /* changed position of the following statement */
        gu4LastSentRouteIndexNbr += u4TempNum;
    }
    LOG2 ((INFO, RRF,
           "Message sent to Output Module, size in bytes.", u4DataLen));
    LOG2 ((INFO, RRF, "Complete Message sent....!!! to %x", u4DestAddr));
}
