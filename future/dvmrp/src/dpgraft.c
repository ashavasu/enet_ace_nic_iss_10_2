/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpgraft.c,v 1.9 2014/04/04 10:04:43 siva Exp $
 *
 * Description:Contains functions for handling incoming Graft 
 *             Messages and generate outgoing Graft messages  
 *
 *******************************************************************/
#include "dpinc.h"

/* Prototypes */

/****************************************************************************/
/* Function Name    :  DvmrpGmHandleGraftPacket                             */
/* Description      :  This function handles the DVMRP Graft                */
/*                     packet ( which is received in a global variable)     */
/*                     and acknowledges it and sends it to the upstream Nbr */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the input interface.    */
/*                     2. u4Len - Size of the received packet               */
/*                     3. u4NbrTbl - Pointer to the Neighbor table entry    */
/*                        which initiated the Graft packet                  */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmHandleGraftPacket (UINT4 u4IfId, UINT4 u4Len, tNbrTbl * pNbrTbl)
#else
void
DvmrpGmHandleGraftPacket (u4IfId, u4Len, pNbrTbl)
     UINT4               u4IfId, u4Len;
     tNbrTbl            *pNbrTbl;
#endif
{
    UINT1              *pDvmrpdata;
    UINT4               u4SrcAddr, u4GroupAddr, u4NbrAddr;
    tNbrPruneList      *pTmpNbr, *pNbr;
    tForwCacheTbl      *pFwdCache;
    tOutputInterface   *pOutList;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfId);
    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, GM, "Invalid Entry"));
        return;
    }

    if (u4Len != GRAFT_PKT_SIZE)
    {
        LOG1 ((WARNING, GM, "Invalid Packet Size"));
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    pDvmrpdata = gpRcvBuffer;

    u4SrcAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) pDvmrpdata));
    if (VALIDATE_UCAST_ADDR (u4SrcAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, GM, "Invalid Source Address"));
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }
    u4GroupAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) (pDvmrpdata + IP_ADDR_LEN)));
    if (VALIDATE_MCAST_ADDR (u4GroupAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, GM, "Invalid Destination Address"));
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    u4NbrAddr = pNbrTbl->u4NbrIpAddress;
    /* 
     * Form and send Graft ack  and forward
     */
    u4Len = GRAFT_PKT_SIZE + MAX_IGMP_HDR_LEN;

    DvmrpGmFormGraft (u4GroupAddr, u4SrcAddr, u4Len);

    if (gpSendBuffer == NULL)
    {
        return;
    }

    DvmrpOmSendPacket (u4IfId, u4NbrAddr, u4Len, DVMRP_GRAFTACK);

    LOG2 ((INFO, GM, "GraftAck sent for Source address ", u4SrcAddr));
    LOG2 ((INFO, GM, "GraftAck sent for Group address", u4GroupAddr));

    pFwdCache =
        (tForwCacheTbl *) (VOID *) DvmrpUtilGetCacheEntry (u4SrcAddr,
                                                           u4GroupAddr,
                                                           DVMRP_FORW_HASH);

    if (pFwdCache == NULL)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((INFO, GM, "Non existing Source and Destination addresses"));
        return;
    }

    if (pFwdCache->pSrcNwInfo == NULL)
    {
        LOG1 ((INFO, GM, "Forward Table entry not present"));
        return;
    }

    /* 
     * Search for the interface for which Graft was received 
     */
    pOutList = pFwdCache->pSrcNwInfo->pOutInterface;

    while (pOutList != NULL)
    {

        if (pOutList->u4OutInterface == u4IfId)
        {
            pNbr = pOutList->pDepNbrs;
            DVMRP_SET_NEIGHBOR_PRUNETIME (pNbr, u4NbrAddr, 0, GM);
            pOutList->u4MinTime = 0;
            break;
        }
        pOutList = pOutList->pNext;
    }

    if (pOutList == NULL)
    {
        LOG1 ((INFO, GM, "Invalid Input interface"));
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    LOG2 ((INFO, GM, "Forward entry with Source Network address updated",
           pFwdCache->pSrcNwInfo->u4SrcNetwork));
    LOG2 ((INFO, GM, "Forward entry with Destination address updated",
           pFwdCache->pSrcNwInfo->u4GroupAddr));
    /* 
     * A Graft has to be sent upstream if required 
     */

    DvmrpGmSendGraftIfIfacePruned (pFwdCache, u4IfId);
    if ((DvmrpMfwdAddOif (pFwdCache->pSrcNwInfo, u4IfId) == DVMRP_NOTOK))
    {
        LOG1 ((INFO, GM, "Unable to Add Oif in MFWD"));
    }

    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmHandleGraftAckPacket                          */
/* Description      :  This function handles the DVMRP Graft Ack            */
/*                     packet ( which is received in a global variable) and */
/*                     updates the Forward Table                            */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the input interface.    */
/*                     2. u4Len - Size of the received packet               */
/*                     3. u4NbrTbl - Pointer to the Neighbor table entry    */
/*                        which initiated the Graft packet                  */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpGmHandleGraftAckPacket (UINT4 u4IfId, UINT4 u4Len, tNbrTbl * pNbrTbl)
#else
void
DvmrpGmHandleGraftAckPacket (u4IfId, u4Len, pNbrTbl)
     UINT4               u4IfId, u4Len;
     tNbrTbl            *pNbrTbl;
#endif
{
    UINT1              *pDvmrpdata;
    UINT4               u4SrcAddr, u4GroupAddr;
    tForwTbl           *pForwTbl;
    tForwCacheTbl      *pFwdCache;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfId);

    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, GM, "Invaild Entry"));
        return;
    }
    if (u4Len != GRAFT_PKT_SIZE)
    {
        LOG1 ((WARNING, GM, "Invalid Packet Size"));
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }
    pDvmrpdata = gpRcvBuffer;

    u4SrcAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) pDvmrpdata));
    u4GroupAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) (pDvmrpdata + IP_ADDR_LEN)));

    if (VALIDATE_UCAST_ADDR (u4SrcAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, GM, "Invalid Source Address"));
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats ", pIfNode->u4IfRcvBadPkts));
        return;
    }
    if (VALIDATE_MCAST_ADDR (u4GroupAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, GM, "Invalid Group Address"));
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats ", pIfNode->u4IfRcvBadPkts));
        return;
    }
    pFwdCache =
        (tForwCacheTbl *) (VOID *) DvmrpUtilGetCacheEntry (u4SrcAddr,
                                                           u4GroupAddr,
                                                           DVMRP_FORW_HASH);

    if (pFwdCache == NULL)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((INFO, GM, "Non existing Source and Destination addresses"));
        return;
    }

    if (pFwdCache->pSrcNwInfo == NULL)
    {
        LOG1 ((INFO, GM, "Forward Table entry not present"));
        return;
    }
    pForwTbl = pFwdCache->pSrcNwInfo;

    LOG2 ((INFO, GM, "GraftAck received for Source address ", u4SrcAddr));
    LOG2 ((INFO, GM, "GraftAck received for Group address", u4GroupAddr));

    if (u4IfId != pForwTbl->InIface.u4InInterface)
    {
        LOG1 ((INFO, GM, "Invalid Input Interface"));
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG2 ((INFO, GM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, GM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    if (pForwTbl->InIface.u1State == GRAFT_WAITING_ACK)
    {
        /* 
         * No use in having GRAFT_ACK_RECEIVED flag.
         * change state to Normal straightaway
         */
        pForwTbl->InIface.u1Flag = DVMRP_NORMAL;
        pForwTbl->InIface.u1State = DVMRP_NORMAL;
        pForwTbl->InIface.u4GraftRetransTimerVal = 0;

        LOG2 ((INFO, GM, "Interface state ", pForwTbl->InIface.u1Flag));
        LOG2 ((INFO, GM, "Graft Retransmit timer value",
               pForwTbl->InIface.u4GraftRetransTimerVal));

        if (DvmrpUtilStopTimer (DVMRP_GRAFT_RETRANS_TIMER, pFwdCache)
            == DVMRP_NOTOK)
        {
            return;
        }

        LOG1 ((INFO, GM, "Graft Retrasmission timer successfully stopped"));
        return;
    }
    LOG1 ((INFO, GM, "Graft Ack received when it is not awaited"));
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmSendGraftOnHMReport                           */
/* Description      :  This function initiates a Graft to the upstream Nbr  */
/*                     if required, when a Host on a new local interface    */
/*                     joins a Multicast Group                              */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the local interface.    */
/*                     2. u4GroupAddr - Multicast Address of the Group to   */
/*                        which a new Host has joined                       */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmSendGraftOnHmReport (UINT4 u4IfId, UINT4 u4GroupAddr)
#else
void
DvmrpGmSendGraftOnHmReport (u4IfId, u4GroupAddr)
     UINT4               u4IfId, u4GroupAddr;
#endif
{
    UINT4               u4Index;
    tForwTbl           *pForwTbl;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }

        if ((DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index))->u4DestGroup
            == u4GroupAddr)
        {

            /* 20/8 Dont add interface block if it is a upstream interface */
            if (pForwTbl->InIface.u4InInterface == u4IfId)
            {
                LOG1 ((INFO, GM, "Host comes up in the upstream interface"));
                continue;
            }

            /* 
             * Add interface to the list of output interfaces 
             */
            if (DvmrpFmAddifList (pForwTbl, u4IfId, NULL) == DVMRP_NOTOK)
            {
                return;
            }
            if ((DvmrpMfwdAddOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
            {
                LOG1 ((INFO, GM, "Unable to Add Oif to MFWD"));
            }

            /* 
             * A Graft has to be sent if required  
             */
            DvmrpGmSendGraftIfIfacePruned (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                           (u4Index), u4IfId);
        }
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmSendGraftOnNbrChange                          */
/* Description      :  This function sends a Graft to the upstream Neighbor */
/*                     whenever there is a new downstream dependency being  */
/*                     expressed by a Neighbor.                             */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id on which there is a route  */
/*                        change.                                           */
/*                     2. u4SrcNetwork - Source network address for which   */
/*                        Route information got changed.                    */
/*                     3. u4NbrAddr - Address of the Neighbor which         */
/*                        expressed downstream dependency                   */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmSendGraftOnNbrChange (UINT4 u4IfId, UINT4 u4SrcNetwork, UINT4 u4NbrAddr)
#else
void
DvmrpGmSendGraftOnNbrChange (u4IfId, u4SrcNetwork, u4NbrAddr)
     UINT4               u4IfId, u4SrcNetwork, u4NbrAddr;
#endif
{
    UINT4               u4Index;
    tForwTbl           *pForwTbl;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }
        if (pForwTbl->u4SrcNetwork == u4SrcNetwork)
        {

            /* 
             * Add interface to the list of output interfaces 
             */
            if (DvmrpFmAddifNbr (pForwTbl, u4IfId, u4NbrAddr) == DVMRP_NOTOK)
            {
                return;
            }
            if ((DvmrpMfwdAddOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
            {
                LOG1 ((INFO, GM, "Unable to Add Oif at MFWD"));
            }

            /* 
             * A Graft has to be sent if required 
             */
            DvmrpGmSendGraftIfIfacePruned (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                           (u4Index), u4IfId);
        }
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmSendGraftIfIfacePruned                        */
/* Description      :  This function initiates a Graft to the upstream Nbr  */
/*                     if the interface is already Pruned.                  */
/* Input (s)        :  pFwdCache - Pointer to a Forward cache Table entry   */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpGmSendGraftIfIfacePruned (tForwCacheTbl * pFwdCache, UINT4 u4IfId)
#else
void
DvmrpGmSendGraftIfIfacePruned (pFwdCache, u4IfId)
     tForwCacheTbl      *pFwdCache;
     UINT4               u4IfId;

#endif
{
    UINT4               u4Len;
    tForwTbl           *pForwTbl;
    tDvmrpInterfaceNode *pNbrIfNode = NULL;
    tNbrTbl            *pNbr = NULL;
    INT1                i1SendFlag = DVMRP_NOTOK;

    u4IfId = u4IfId;
    pForwTbl = pFwdCache->pSrcNwInfo;

    pNbrIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pForwTbl->InIface.u4InInterface);

    if (pNbrIfNode == NULL)
    {
        /* Incomming interface is NULL. May be deleted. */
        LOG1 ((INFO, GM, "Upstream is NULL. Graft not sent on upstream"));
        return;
    }

    if (pForwTbl->InIface.u1State == PRUNED)
    {

        if (DvmrpUtilStopTimer (DVMRP_PRUNE_RETRANS_TIMER, pFwdCache)
            == DVMRP_OK)
        {
            LOG1 ((INFO, GM, "Prune Retransmit timer stopped"));
        }

        /* 
         * A Graft Must be sent 
         */
        pForwTbl->InIface.u1State = GRAFT_WAITING_ACK;
        pForwTbl->InIface.u4GraftRetransTimerVal = DEFAULT_GRAFT_TIMER_VAL;
        pForwTbl->InIface.u4PruneRetransTimerVal = 0;

        /* 
         * Form and Forward the Graft packet 
         */
        u4Len = GRAFT_PKT_SIZE + MAX_IGMP_HDR_LEN;

        DvmrpGmFormGraft (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                          u4Len);

        if (gpSendBuffer == NULL)
        {
            return;
        }

        pNbr = pNbrIfNode->pNbrOnthisIface;
        while (pNbr != NULL)
        {
            if ((pNbr->u4NbrIpAddress == pForwTbl->u4UpNeighbor)
                && ((pNbr->u1NbrCapabilities & DVMRP_CAPABILITIES) ==
                    DVMRP_CAPABILITIES))
            {
                i1SendFlag = DVMRP_OK;
                break;
            }
            pNbr = pNbr->pNbrTblNext;
        }

        if (i1SendFlag == DVMRP_NOTOK)
        {
            LOG1 ((CRITICAL, PM, "UpStream Neighbor capability Failure"));
            return;
        }

        DvmrpOmSendPacket (pForwTbl->InIface.u4InInterface,
                           pForwTbl->u4UpNeighbor, u4Len, DVMRP_GRAFT);

        LOG2 ((INFO, GM, "Graft sent for Source address ",
               pFwdCache->u4SrcAddress));
        LOG2 ((INFO, GM, "Graft sent for Group address",
               pFwdCache->u4DestGroup));

        LOG2 ((INFO, GM, "Value of Graft Retransmit timer is ",
               DEFAULT_GRAFT_TIMER_VAL));
        LOG2 ((INFO, GM, "Upstream Interface  State is ",
               pForwTbl->InIface.u1State));

        if (DvmrpUtilStartTimer (DVMRP_GRAFT_RETRANS_TIMER,
                                 DEFAULT_GRAFT_TIMER_VAL,
                                 pFwdCache) == DVMRP_NOTOK)
        {
            return;
        }
        LOG2 ((INFO, GM, "Graft Retransmit timer started for",
               DEFAULT_GRAFT_TIMER_VAL));
    }
    else
    {
        LOG1 ((INFO, GM, "Graft Not sent Upstream"));
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmFormGraft                                     */
/* Description      :  This function forms a Graft or a Graft Ack packet    */
/*                     with the given Source and Destination address.       */
/* Input (s)        :  1. u4DestAddr - Multicast Group Ip address           */
/*                     2. u4SrcAddr - IP address of the Host which is to be */
/*                        grafted                                           */
/*                     3. u4MaxLength - Length of the Packet to be formed   */
/* Output (s)       :  None                                                 */
/* Returns          :  Number of bytes copied on the Send buffer            */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmFormGraft (UINT4 u4DestAddr, UINT4 u4SrcAddr, UINT4 u4MaxLength)
#else
void
DvmrpGmFormGraft (u4DestAddr, u4SrcAddr, u4MaxLength)
     UINT4               u4DestAddr, u4SrcAddr, u4MaxLength;
#endif
{
    DVMRP_ALLOC_SEND_BUFFER ();

    u4SrcAddr = OSIX_HTONL (u4SrcAddr);
    u4DestAddr = OSIX_HTONL (u4DestAddr);
    MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN], &u4SrcAddr, IP_ADDR_LEN);
    MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN + IP_ADDR_LEN], &u4DestAddr,
            IP_ADDR_LEN);
    u4MaxLength = u4MaxLength;

    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmHandleRetransTimerEvent                       */
/* Description      :  This function handles Graft Retransmission Timer     */
/*                     expiry event and retransmits a Graft if required     */
/* Input (s)        :  pFwdCache - Pointer to a Forward cache table     */
/*                     entry                                                */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmHandleRetransTimerEvent (tDvmrpTimer * pTimerBlk)
#else
void
DvmrpGmHandleRetransTimerEvent (pTimerBlk)
     tDvmrpTimer        *pTimerBlk;
#endif
{
    UINT4               u4Len;
    UINT4               u4Index;
    tForwTbl           *pForwTbl = NULL;
    tForwCacheTbl      *pFwdCache = NULL;

    pForwTbl = DVMRP_GET_BASE_PTR (tForwTbl, AppTimer, pTimerBlk);
    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {
        if ((DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index)) == pForwTbl)
        {
            pFwdCache = DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index);
            break;
        }
    }

    if (pFwdCache == NULL)
    {
        /* Invalid reference block */
        LOG1 ((INFO, GM, "Invalid Reference block passed"));
        return;
    }

    pForwTbl = pFwdCache->pSrcNwInfo;

    if (pForwTbl == NULL)
    {
        LOG1 ((INFO, GM, "Forward cache table entry become invalid"));
        return;
    }

    /* 
     * Upstream is not interested in sending Ack.. so resend graft
     */
    if (pForwTbl->InIface.u1State == GRAFT_WAITING_ACK)
    {

        /*
         * Negative Cache timer value adjusted 
         */
        pForwTbl->InIface.u4GraftRetransTimerVal *= 2;
        LOG2 ((INFO, GM, "Upstream Interface  State is ",
               pForwTbl->InIface.u1State));

        /* 
         * Form and forward the Graft packet 
         */
        u4Len = GRAFT_PKT_SIZE + MAX_IGMP_HDR_LEN;

        DvmrpGmFormGraft (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                          u4Len);

        if (gpSendBuffer == NULL)
        {
            return;
        }

        DvmrpOmSendPacket (pForwTbl->InIface.u4InInterface,
                           pForwTbl->u4UpNeighbor, u4Len, DVMRP_GRAFT);

        LOG2 ((INFO, GM, "Graft sent for Source address ",
               pFwdCache->u4SrcAddress));
        LOG2 ((INFO, GM, "Graft sent for Group address",
               pFwdCache->u4DestGroup));

        if (DvmrpUtilStartTimer
            (DVMRP_GRAFT_RETRANS_TIMER,
             (UINT2) pForwTbl->InIface.u4GraftRetransTimerVal,
             pFwdCache) == DVMRP_NOTOK)
        {
            return;
        }
        LOG2 ((INFO, GM, "Graft Retransmit timer started for seconds",
               pForwTbl->InIface.u4GraftRetransTimerVal));

    }                            /* GRAFT_WAITING_ACK */
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpGmSendGraftOnIfaceChange                        */
/* Description      :  This function sends a Graft to the upstream Neighbor */
/*                     whenever an interface changes its state to become    */
/*                     a Designated Forwarder for a Source network          */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id on which there is a route  */
/*                        change.                                           */
/*                     2. u4SrcNetwork - Source network address for which   */
/*                        Route information got changed.                    */
/*                     3. pNbr - Pointer to the list of downstream dep.     */
/*                        neighbors on that interface                       */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpGmSendGraftOnIfaceChange (UINT4 u4IfId, UINT4 u4SrcNetwork,
                               tNbrAddrList * pNbr)
#else
void
DvmrpGmSendGraftOnIfaceChange (u4IfId, u4SrcNetwork, pNbr)
     UINT4               u4IfId, u4SrcNetwork;
     tNbrAddrList       *pNbr;
#endif
{
    UINT1               u1Flag = DVMRP_FALSE;
    UINT4               u4Index;
    tForwTbl           *pForwTbl;

    /* 
     * Interface shd be added to the forward table only if
     * some hosts are available on that interface
     */

    for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
    {
        if (((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u1Status
             == DVMRP_ACTIVE) &&
            ((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u4LocInterface ==
             u4IfId))
        {
            u1Flag = DVMRP_TRUE;
            break;
        }
    }

    if ((u1Flag == DVMRP_FALSE) && (pNbr == NULL))
    {
        LOG1 ((INFO, GM, "No hosts present in the interface"));
        return;
    }

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }

        if (pForwTbl->u4SrcNetwork == u4SrcNetwork)
        {

            /* 
             * Add interface to the list of output interfaces 
             */
            if (DvmrpFmAddifList (pForwTbl, u4IfId, pNbr) == DVMRP_NOTOK)
            {
                return;
            }
            if ((DvmrpMfwdAddOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
            {
                LOG1 ((INFO, GM, "Unable to Add Oif in MFWD"));
            }

            /* 
             * A Graft has to be sent if required 
             */
            DvmrpGmSendGraftIfIfacePruned (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                           (u4Index), u4IfId);
        }
    }
    return;
}
