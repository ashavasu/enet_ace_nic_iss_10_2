/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: dpport.c,v 1.36 2014/04/04 10:04:43 siva Exp $
*
* Description:This file contains the functions that are to   
*             be ported to the target.                     
*
*******************************************************************/
#include "dpinc.h"

#ifdef LNXIP4_WANTED
#include "chrdev.h"
#endif
#ifdef FS_NPAPI
#include "dvmrp.h"

#endif
extern VOID         RegisterFSDVMR (VOID);
extern UINT4        gu4DvmrpGlobStatus;
PRIVATE void DvmrpHandleIgmpEvent ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pIpPkt));
PRIVATE void        DvmrpHandleIgmpHostEvent
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pIgmpPkt));

VOID                (*gaDvmrpTmrExpHdlr[DVMRP_MAX_TIMERS]) (tDvmrpTimer *) =
{
DvmrpRrfRouteReportTimerHandler,
        DvmrpRthRouteAgeTimerHandler,
        DvmrpFmCacheTimeOutHandler,
        DvmrpNdmNbrTimerExpHandler,
        DvmrpNdmSendProbesOnTimerExpiry,
        DvmrpGmHandleRetransTimerEvent, DvmrpPmHandleRetransTimerEvent};

/****************************************************************************
 * Function Name    :  DvmrpIsDvmrpEnabled
 *
 * Description      :  This function checks if Multicast forwarding status of
 * DVMRP is enabled or disabled
 * 
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         : gu4DvmrpGlobStatus
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  DVMRP_ENABLED or DVMRP_DISABLED
****************************************************************************/
INT4
DvmrpIsDvmrpEnabled ()
{

    if (gu4DvmrpGlobStatus == DVMRP_STATUS_ENABLED)
    {
        return DVMRP_ENABLED;
    }
    else
    {
        return DVMRP_DISABLED;
    }
}

/****************************************************************************/
/* Function Name    :  DvmrpTaskMain                                        */
/* Description      :  This function initializes the CRU and TMO services   */
/*                     and waits for events                                 */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
/***  This is the place of change for common Entry 
 */
#ifdef __STDC__
void
DvmrpTaskMain (INT1 *pi1Dummy)
#else
void
DvmrpTaskMain (INT1 *pi1Dummy)
#endif
{
    UINT4               u4Event = 0;
    UINT4               u4Stat = 0;
    tDvmrpMsgBlock     *pDvmrpMsgBlockPtr = NULL;

    UNUSED_PARAM (pi1Dummy);
    UNUSED_PARAM (pDvmrpMsgBlockPtr);

    gu1DvmrpStatus = DVMRP_DISABLED;

    if (OsixTskIdSelf (&gu4DvmrpTaskId) != OSIX_SUCCESS)
    {
        DVMRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem ((const UINT1 *) DVMRP_MUTEX_SEMA4, 1, OSIX_GLOBAL,
                       &(DVMRP_MUTEX_SEMID)) != OSIX_SUCCESS)
    {
        LOG1 ((WARNING, IHM, "Unable to Create Semaphore"));
        DVMRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }                            /* end of if to see if semaphore is created */
    if (OsixQueCrt ((UINT1 *) DVMRP_Q, OSIX_MAX_Q_MSG_LEN, DVMRP_Q_DEPTH,
                    (tOsixQId *) & gDvIqId) != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, MAIN, "Unable to Create DVMRP Q "));
        /* Delete the Created Semaphore */
        OsixSemDel (DVMRP_MUTEX_SEMID);
        DVMRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef LNXIP4_WANTED
    /* remove DVMRP_DEVICE_FILE_NAME if present in system before creating it  */
    system ("rm -rf " DVMRP_DEVICE_FILE_NAME);
    /* Define the major & minor numbers based on the number of ioctl request */
    system ("/bin/mknod " DVMRP_DEVICE_FILE_NAME " c 103 0");
#endif
    DVMRP_INIT_COMPLETE (OSIX_SUCCESS);
    RegisterFSDVMR ();

    /* 
     * Wait for Events from external Entities
     */
    while (1)
    {
        u4Stat = OsixEvtRecv (gu4DvmrpTaskId, (DVMRP_TIMER_EXPIRED_EVENT |
#ifdef LNXIP4_WANTED
                                               DVMRP_PACKET_ARRIVAL_EVENT |
#endif
#ifdef MFWD_WANTED
                                               DVMRP_MFWD_DISABLE_EVENT |
                                               DVMRP_MFWD_ENABLE_EVENT |
#endif
                                               DVMRP_INPUT_Q_EVENT),
                              OSIX_WAIT | OSIX_EV_ANY, &u4Event);
        DVMRP_MUTEX_LOCK ();
        UNUSED_PARAM (u4Stat);
        if (u4Event & DVMRP_TIMER_EXPIRED_EVENT)
        {
            LOG1 ((INFO, IHM, "timer expiry event received"));
            if (gu4DvmrpGlobStatus != DVMRP_STATUS_DISABLED)
            {
                DvmrpHandleTimers (g1sTimerListId);
            }
        }
        if (u4Event & DVMRP_INPUT_Q_EVENT)
        {
            LOG1 ((INFO, IHM, "INTERFACE UPDATION Event received"));
            DvmrpIhmHandleInputQMsgs ();
        }
#ifdef MFWD_WANTED
        if (u4Event & DVMRP_MFWD_ENABLE_EVENT)
        {
            LOG1 ((INFO, IHM, "DVMRP-MFWD ENABLE Event received"));
            /* Need to call function which will install the ifs and
             * route entries in MFWD */
            DvmrpMfwdHandleEnabling ();
            gDpConfigParams.u1MfwdStatus = MFWD_STATUS_ENABLED;
        }
        if (u4Event & DVMRP_MFWD_DISABLE_EVENT)
        {
            LOG1 ((INFO, IHM, "DVMRP-MFWD DISABLE Event received"));
            /* No Need to call function which will uninstall the ifs and
             *  route entries in MFWD bcoz this Event came from MFWD inself
             *  so MFWD will take care of this. make only status change */
            gDpConfigParams.u1MfwdStatus = MFWD_STATUS_DISABLED;
        }
#endif
#ifdef LNXIP4_WANTED
        if (u4Event & DVMRP_PACKET_ARRIVAL_EVENT)
        {
            DvmrpHandlePacketArrivalEvent ();
            SelAddFd (gDpConfigParams.i4DvmrpSockId,
                      DvmrpNotifyPktArrivalEvent);
        }
#endif

        DVMRP_MUTEX_UNLOCK ();
    }                            /* End of While (1) */
}

#ifdef LNXIP4_WANTED
/***************************************************************************
 * Function Name    : DvmrpHandlePacketArrivalEvent
 *
 * Description      :  This Function is called to receive the DVMRP control
 *                     packets from the socket, calls the function to process
 *                     the contorl messages. 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
DvmrpHandlePacketArrivalEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    struct sockaddr_in  Recv_Node;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[DVMRP_RECV_ANCILLARY_LEN];
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    UINT1              *pRecvPkt = NULL;
    UINT4               u4IfIndex;
    INT4                i4SocketId;
    INT4                i4RecvBytes;
    tIpParms           *pIpParms = NULL;

    MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in));

    if ((pRecvPkt = MEM_MALLOC (DVMRP_RX_MTU, UINT1)) == NULL)
    {
        LOG1 ((WARNING, IHM,
               "Unable to allocate memory, reading from socket failed \n"));
        return;
    }

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &Recv_Node;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pRecvPkt;
    Iov.iov_len = DVMRP_RX_MTU;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4SocketId = gDpConfigParams.i4DvmrpSockId;
    i4RecvBytes = 0;

    while ((i4RecvBytes = recvmsg (i4SocketId, &PktInfo, 0)) > 0)
    {
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

        u4IfIndex = (UINT4) pIpPktInfo->ipi_ifindex;

        /* Copy the recvd linear buf to cru buf */
        if ((pPkt = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            LOG1 ((WARNING, IHM, "CRU Buf Allocation Failed \n"));
            MEM_FREE (pRecvPkt);
            return;
        }

        pIpParms = (tIpParms *) (&pPkt->ModuleData);
        pIpParms->u2Port = u4IfIndex;

        CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes);

        DvmrpHandleIgmpEvent (pPkt);
    }
    MEM_FREE (pRecvPkt);

}
#endif

/****************************************************************************
 * Function Name    :  DvmrpIhmHandleInputQMsgs  
 * Description      :  This function dequeues the messages from the queue
 * Input (s)        :  None
 * Output (s)       :  None                                                 
 * Returns          :  None                                                 
****************************************************************************/

VOID
DvmrpIhmHandleInputQMsgs (VOID)
{
    tDvmrpQMsg         *pQMsg = NULL;

    while ((OsixQueRecv
            (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
             OSIX_NO_WAIT)) == OSIX_SUCCESS)
    {
        if (pQMsg == NULL)
        {
            return;
        }

        switch (pQMsg->u4MsgType)
        {
            case IGMP_HOST_DVMRP_EVENT:
                LOG1 ((INFO, IHM, "IGMP HOST MEMBERSHIP Event Received"));
                DvmrpHandleIgmpHostEvent (pQMsg->DvmrpQMsgParam.DvmrpIfParam);
                break;
            case IGMP_DVMRP_EVENT:
                LOG1 ((INFO, IHM, "IGMP-DVMRP Event received"));
                DvmrpHandleIgmpEvent (pQMsg->DvmrpQMsgParam.DvmrpIfParam);
                break;
            case DVMRP_INTERFACE_EVENT:
                LOG1 ((INFO, IHM, "INTERFACE UPDATION Event Received"));
                DvmrpIhmHandleInterfaceUpdation
                    (&(pQMsg->DvmrpQMsgParam.DvmrpIfStatus));
                break;
            case DVMRP_DATA_EVENT:
                LOG1 ((INFO, IHM, "DATA Packet Event Received"));
                DvmrpHandleDataPkt (pQMsg->DvmrpQMsgParam.DvmrpIfParam);
                break;
#ifdef FS_NPAPI
            case DVMRP_PBMP_CHANGE_EVENT:
                LOG1 ((INFO, IHM, "PBMP Change Event Received to Dvmrp"));
                DvmrpHandlePbmpChgEvent (pQMsg->DvmrpQMsgParam.DvmrpIfParam);
                break;
#endif

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                LOG1 ((INFO, IHM,
                       "Mbsm Line Card Insertion/removal Event Received to Dvmrp"));
                DvmrpMbsmUpdateMrouteTable (pQMsg->DvmrpQMsgParam.DvmrpIfParam,
                                            pQMsg->u4MsgType);
                CRU_BUF_Release_MsgBufChain
                    (pQMsg->DvmrpQMsgParam.DvmrpIfParam, FALSE);
                break;

            case MBSM_MSG_LOAD_SHARING_ENABLE:
                DvmrpMbsmUpdtMrouteTblForLoadSharing (MBSM_LOAD_SHARING_ENABLE);
                break;

            case MBSM_MSG_LOAD_SHARING_DISABLE:
                DvmrpMbsmUpdtMrouteTblForLoadSharing
                    (MBSM_LOAD_SHARING_DISABLE);
                break;

#endif

            default:
                CRU_BUF_Release_MsgBufChain
                    (pQMsg->DvmrpQMsgParam.DvmrpIfParam, FALSE);
        }
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
    }

    return;
}

/*****************************************************************************/
/* Function Name    :  DvmrpTimersAlloc                                      */
/* Description      :  This function initializes the timer block and queue   */
/* Input (s)        :  None                                                  */
/* Output (s)       :  None                                                  */
/* Returns          :  Returns DVMRP_OK on success and DVMRP_NOTOK on failure*/
/*****************************************************************************/

#ifdef __STDC__
INT4
DvmrpTimersAlloc (void)
#else
INT4
DvmrpTimersAlloc (void)
#endif
{
    UINT1               u1TaskName[] = DVMRP_TASK_NAME;
    if (TmrCreateTimerList (u1TaskName, DVMRP_TIMER_EXPIRED_EVENT, NULL,
                            &g1sTimerListId) != TMR_SUCCESS)

    {
        LOG1 ((INFO, IHM, "Timer List Not initialised"));
        return (DVMRP_NOTOK);
    }

    return (DVMRP_OK);
}

/*****************************************************************************/
/* Function Name    : DvmrpSendEvent                                         */
/* Description      : This function sends an event to a particular task      */
/* Input (s)        : u4Event  - Event to be sent to the Task                */
/* Output (s)       : None                                                   */
/* Returns          : Returns DVMRP_OK on success and DVMRP_NOTOK on failure */
/*****************************************************************************/

#ifdef __STDC__
INT4
DvmrpSendEvent (UINT4 u4Event)
#else
INT4
DvmrpSendEvent (u4Event)
     UINT4               u4Event;
#endif
{
    return (OsixEvtSend (gu4DvmrpTaskId, u4Event));
}

/****************************************************************************/
/* Function Name    : DvmrpFreeTimersAndDelayTask                           */
/* Description      : This function frees the timer block and delays task   */
/* Input (s)        : None                                                  */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpFreeTimersAndDelayTask (void)
#else
void
DvmrpFreeTimersAndDelayTask (void)
#endif
{

    OsixDelayTask ((UINT4) (DVMRP_SHUTDOWN_INTERVAL * DVMRP_TICKS_PER_SEC));
    TmrDeleteTimerList (g1sTimerListId);

}

/****************************************************************************/
/* Function Name    :  DvmrpHandleIgmpEvent                                 */
/* Description      :  This function validates the dvmrp protocol packet    */
/*                     queued from IGMP                                     */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpHandleIgmpEvent (tCRU_BUF_CHAIN_HEADER * pIpPkt)
#else
PRIVATE void
DvmrpHandleIgmpEvent (tCRU_BUF_CHAIN_HEADER * pIpPkt)
#endif
{
    tDvmrpInterfaceNode *pIfaceNode = NULL;
    UINT4               u4ValidOffset = 0;
    UINT4               u4Count = 0;
    UINT4               u4IfIndex = 0;
    /* PMT: Fix for Length of tha packet */
    UINT2               u2TotalLength;

    /* 
     * Extract Input Interface and validate it. 
     */
    /* Get the IfIndex from Buffer */
    DVMRP_GET_IFINDEX (pIpPkt, u4IfIndex);    /* This returns the Port No. */

    pIfaceNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);

    if ((pIfaceNode == NULL) || !(DVMRP_CHECK_IF_STATUS (pIfaceNode)))
    {
        LOG1 ((WARNING, IHM, "Interface Not Valid"));
        CRU_BUF_RELEASE_CHAIN (pIpPkt);
        pIpPkt = NULL;
        LOG1 ((INFO, IHM, "IP packet Buffer released"));
        return;
    }
    CRU_BUF_COPY_FROM_CHAIN (pIpPkt,
                             (UINT1 *) &u2TotalLength,
                             IP_PKT_OFFSET_TOTAL_LEN, sizeof (UINT2));

    u4Count = OSIX_NTOHS (u2TotalLength);

    DVMRP_ALLOC_RCV_BUFFER ();
    if (gpRcvBufferRead == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to malloc receive buffer"));
        CRU_BUF_RELEASE_CHAIN (pIpPkt);
        pIpPkt = NULL;
        LOG1 ((INFO, IHM, "IP packet Buffer released"));
        return;
    }

    CRU_BUF_COPY_FROM_CHAIN (pIpPkt, gpRcvBufferRead, 0, u4Count);
    LOG1 ((CRITICAL, IHM, "Normal Release"));
    CRU_BUF_RELEASE_CHAIN (pIpPkt);
    pIpPkt = NULL;
    LOG1 ((INFO, IHM, "IP packet Buffer released"));

    if (IP_OLEN (gpRcvBufferRead[DVMRP_ZERO]) != DVMRP_ZERO)
    {
        u4ValidOffset = IP_OLEN (gpRcvBufferRead[DVMRP_ZERO]) + MIN_IP_HDR_LEN;
        LOG1 ((INFO, IHM, "Extracted the packet with options successfully"));
    }
    else
    {
        u4ValidOffset = MIN_IP_HDR_LEN;
        LOG1 ((INFO, IHM, "Extracted the packet without options successfully"));
    }

    DvmrpIhmProcessProtocolPackets (u4ValidOffset, u4IfIndex);
    if (DvmrpMemRelease (gDvmrpMemPool.MiscPoolId, gpRcvBufferRead)
        == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to release memory block to MemPool"));
        return;
    }

}

/*****************************************************************************/
/* Function Name    :  DvmrpHandleIgmpHostEevent                             */
/* Description      :  This function extracts the Igmp Packet received in the*/
/*                     buffer                                                */
/* Input (s)        :  None                                                  */
/* Output (s)       :  None                                                  */
/* Returns          :  None                                                  */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE void
DvmrpHandleIgmpHostEvent (tCRU_BUF_CHAIN_HEADER * pIgmpPkt)
#else
PRIVATE void
DvmrpHandleIgmpHostEvent (tCRU_BUF_CHAIN_HEADER * pIgmpPkt)
#endif
{
    UINT4               u4Count = 0;

    u4Count = CRU_BUF_Get_ChainValidByteCount (pIgmpPkt);
    DVMRP_ALLOC_RCV_BUFFER ();
    if (gpRcvBuffer == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to malloc received buffer"));
        CRU_BUF_RELEASE_CHAIN (pIgmpPkt);
        LOG1 ((INFO, IHM, "Igmp Pkt Buffer released"));
        pIgmpPkt = NULL;
        return;
    }

    CRU_BUF_COPY_FROM_CHAIN (pIgmpPkt, gpRcvBuffer, 0, u4Count);
    LOG1 ((CRITICAL, IHM, "Normal Release"));
    CRU_BUF_RELEASE_CHAIN (pIgmpPkt);
    LOG1 ((INFO, IHM, "Igmp Pkt Buffer released"));
    pIgmpPkt = NULL;

    DvmrpIhmProcessHostPackets ();
    if (DvmrpMemRelease (gDvmrpMemPool.MiscPoolId, gpRcvBufferRead)
        == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to release memory block to MemPool"));
        return;
    }

    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmUpdateIfStatus                               */
/* Description      :  This function is invoked whenever IP wants to        */
/*                     initmate DVMRP regarding the change of any interface */
/* Input (s)        :  u4BitMap  - This indicates the parameters that have  */
/*                                 changed                                  */
/*                     u4IfIndex - This contains the Interface Id           */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpUpdateIfStatus (tNetIpv4IfInfo * pNetIpv4Info, UINT4 u4BitMap)
#else
void
DvmrpUpdateIfStatus (pNetIpv4Info, u4BitMap)
     tNetIpv4IfInfo     *pNetIpv4Info;
     UINT4               u4BitMap;
#endif
{
    tDvmrpQMsg         *pQMsg = NULL;
    /* 
     * The Bitmap received indicates the parameters that have changed.
     * Maximum bits that could be set is 7
     */
    if (u4BitMap > 127)
    {
        LOG1 ((WARNING, IHM, "Invalid Bitmap received"));
        return;
    }

    if (DVMRP_MEM_ALLOCATE (DVMRP_Q_PID, pQMsg, tDvmrpQMsg) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for Q"));
        return;
    }

    pQMsg->u4MsgType = DVMRP_INTERFACE_EVENT;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4IfBitMap = u4BitMap;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4IfIndex = pNetIpv4Info->u4IfIndex;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4Addr = pNetIpv4Info->u4Addr;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4NetMask = pNetIpv4Info->u4NetMask;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4Mtu = pNetIpv4Info->u4Mtu;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4Admin = pNetIpv4Info->u4Admin;
    pQMsg->DvmrpQMsgParam.DvmrpIfStatus.u4Oper = pNetIpv4Info->u4Oper;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return;
    }

    OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT);
    LOG1 ((INFO, IHM, "Packet enqueued successfully"));
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpHandleProtocolPackets                           */
/* Description      :  This function is invoked whenever IGMP recevies any  */
/*                     DVMRP Protocol Packet.                               */
/* Input (s)        :  pPacket  - Pointer to the Dvmrp Protocol packet      */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpHandleProtocolPackets (tCRU_BUF_CHAIN_HEADER * pPacket)
#else
void
DvmrpHandleProtocolPackets (pPacket)
     tCRU_BUF_CHAIN_HEADER *pPacket;
#endif
{
    tDvmrpQMsg         *pQMsg = NULL;

    if (gu1DvmrpStatus == DVMRP_DISABLED)
    {
        LOG1 ((WARNING, IHM, "DVMRP Disabled : Something is wrong!!! "
               " IGMP De-registration not successful"));
        CRU_BUF_RELEASE_CHAIN (pPacket);
        return;
    }

    if (pPacket == NULL)
    {
        LOG1 ((WARNING, IHM, "Received NULL Packet"));
        return;
    }

    if (DVMRP_MEM_ALLOCATE (DVMRP_Q_PID, pQMsg, tDvmrpQMsg) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for Q"));
        CRU_BUF_RELEASE_CHAIN (pPacket);
        return;
    }
    pQMsg->u4MsgType = IGMP_DVMRP_EVENT;
    pQMsg->DvmrpQMsgParam.DvmrpIfParam = pPacket;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        CRU_BUF_RELEASE_CHAIN (pPacket);
        pPacket = NULL;
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return;
    }

    OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT);
    LOG1 ((INFO, IHM, "Packet enqueued successfully"));
    return;

}

/****************************************************************************
* Function Name    :  DvmrpHandleProtocolPackets                           
* Description      :  This function handles the when a multicast data packet 
*                     is received
* Input (s)        :  pBuf  - Pointer to the CRU Buffer
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/

#ifdef __STDC__
VOID
DvmrpHandleDataPkt (tCRU_BUF_CHAIN_DESC * pBuf)
#else
VOID
DvmrpHandleDataPkt (pBuf)
     tCRU_BUF_CHAIN_DESC *pBuf;

#endif
{
    tDvmrpInterfaceNode *pIfNode = NULL;
    t_IP                IpHdr;
    UINT4               u4SrcAddr;
    UINT4               u4DestAddr;
    UINT4               u4IfIndex;
    UINT4               u4BufLen;
#ifdef MFWD_WANTED
    INT4                i4Status;
    tMfwdMrpMDPMsg      MfwdMsgHdr;
#endif

    if (pBuf != NULL)
    {
#ifdef MFWD_WANTED
        DVMRP_EXTRACT_MFWD_HEADER ((UINT1 *) &MfwdMsgHdr, pBuf, i4Status);
        if (i4Status == DVMRP_NOTOK)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            LOG1 ((INFO, IHM, "Extracting MFWD MSG header - FAILED "));
            return;
        }

        DVMRP_SKIP_MFWD_MSG_HEADER (pBuf);
        u4IfIndex = MfwdMsgHdr.u4Iif;
#else
        DVMRP_GET_IFINDEX (pBuf, u4IfIndex);
#endif
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
        u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if ((pIfNode == NULL) || (!DVMRP_CHECK_IF_STATUS (pIfNode)))
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            LOG1 ((INFO, IHM, "Mcast Data Pkt Rcvd on wrong Iface "));
            return;
        }
        /* Extract the IP header from the buffer */
        if ((u4BufLen < MIN_IP_HDR_LEN)
            || (IP_EXTRACT_HEADER (&IpHdr, pBuf) == DVMRP_NOTOK))
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            LOG1 ((CRITICAL, IHM, "Unable extract IP header from Buffer "));
            return;
        }

        /* Get the source Address and Group Address */
        u4SrcAddr = IpHdr.u4Src;
        u4DestAddr = IpHdr.u4Dest;

        DvmrpHandleDatagram (pIfNode, pBuf, u4SrcAddr, u4DestAddr);
    }
    else
    {
        LOG1 ((CRITICAL, IHM, "Null Buffer from IP"));
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpProcessMcastDataPkt                             */
/* Description      :  This function process the datagram received from IP  */
/*                     to extract Source and  Destination IP addr and also  */
/*                     Interface ID                                         */
/* Input (s)        :  pBuf         - Pointer to the Multicast datagram     */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
VOID
DvmrpProcessMcastDataPkt (tCRU_BUF_CHAIN_DESC * pBuf)
#else
VOID
DvmrpProcessMcastDataPkt (pBuf)
     tCRU_BUF_CHAIN_DESC *pBuf;

#endif
{
    tDvmrpQMsg         *pQMsg = NULL;

    if (gu1DvmrpStatus == DVMRP_DISABLED)
    {
        LOG1 ((WARNING, IHM, "DVMRP Disabled : Something is wrong!!! "
               " IGMP De-registration not successful"));
        CRU_BUF_RELEASE_CHAIN (pBuf);
        return;
    }

    if (DVMRP_MEM_ALLOCATE (DVMRP_Q_PID, pQMsg, tDvmrpQMsg) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for Q"));
        CRU_BUF_RELEASE_CHAIN (pBuf);
        return;
    }
    pQMsg->u4MsgType = DVMRP_DATA_EVENT;
    pQMsg->DvmrpQMsgParam.DvmrpIfParam = pBuf;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        CRU_BUF_RELEASE_CHAIN (pBuf);
        pBuf = NULL;
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return;
    }

    OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT);
    LOG1 ((INFO, IHM, "Packet enqueued successfully"));
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpHandleDatagram                                  */
/* Description      :  This function process the datagram received from IP  */
/* Input (s)        :  pBuf         - Pointer to the Multicast datagram     */
/*                     u4SrcAddr    - Source IP Addr                        */
/*                     u4DestAddr   - Destination IP addr                   */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpHandleDatagram (tDvmrpInterfaceNode * pIfNode, tCRU_BUF_CHAIN_DESC * pBuf,
                     UINT4 u4SrcAddr, UINT4 u4DestAddr)
#else
INT4
DvmrpHandleDatagram (pIfNode, pBuf, u4SrcAddr, u4DestAddr)
     tDvmrpInterfaceNode *pIfNode;
     tCRU_BUF_CHAIN_DESC *pBuf;
     UINT4               u4SrcAddr;
     UINT4               u4DestAddr;

#endif
{
    INT4                i4RetVal;
    UINT4              *pIntList;

    pIntList = UtlShMemAllocOifList ();
    if (pIntList == NULL)
    {
        i4RetVal = DVMRP_NOTOK;
        CRU_BUF_RELEASE_CHAIN (pBuf);
        return i4RetVal;
    }

    i4RetVal = DvmrpFmHandleMcastData (u4SrcAddr, u4DestAddr,
                                       pIfNode, pIntList);
    if (i4RetVal != DVMRP_NOTOK)
    {
        if (DvmrpEnqueueMcastDataPktToIp (pBuf, u4SrcAddr, u4DestAddr, pIntList)
            == DVMRP_NOTOK)
        {
            i4RetVal = DVMRP_NOTOK;
            CRU_BUF_RELEASE_CHAIN (pBuf);
            UtlShMemFreeOifList (pIntList);
        }
    }
    else
    {
        CRU_BUF_RELEASE_CHAIN (pBuf);
        UtlShMemFreeOifList (pIntList);
    }
    return i4RetVal;
}

/****************************************************************************/
/* Function Name    :  DvmrpEnqueueMcastDataPktToIp                         */
/* Description      :  This function Enqueues data packet to IP along with  */
/*                     interface list in which packet needs to be sent      */
/* Input (s)        :  pBuf         - Pointer to the Multicast datagram     */
/*                     u4SrcAddr    - Source IP Addr                        */
/*                     u4DestAddr   - Destination IP addr                   */
/*                     pOifList     - pointer to outgoing interface list    */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK                                             */
/*                     DVMRP_NOTOK                                          */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpEnqueueMcastDataPktToIp (tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT4 u4SrcAddr, UINT4 u4DestAddr,
                              UINT4 *pOifList)
#else
INT4
DvmrpEnqueueMcastDataPktToIp (pBuf, u4SrcAddr, u4DestAddr, pOifList)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4SrcAddr;
     UINT4               u4DestAddr;
     UINT4              *pOifList;
#endif
{
#ifndef LNXIP4_WANTED
    tMcastIpSendParams *pIpMcastParams;    /* IP's structure For multicast data */
    pIpMcastParams = (tMcastIpSendParams *) IP_GET_MODULE_DATA_PTR (pBuf);

    /* Packet type identifies whether it is
     * a Multicast control packet or multicast
     * data packet.
     */
    pIpMcastParams->u1Cmd = DVMRP_MULTICAST_DATA;
    pIpMcastParams->u4SrcAddr = u4SrcAddr;
    pIpMcastParams->u4DestAddr = u4DestAddr;
    pIpMcastParams->pu4OIfList = pOifList;

    if (IpEnquePktToIpFromHL (pBuf) == IP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
#else
    struct sockaddr_in  DestAddr;
    struct in_addr      IfAddr;
    tDvmrpInterfaceNode *pIfNode = NULL;
    UINT4               u4OifCnt;
    UINT4               u4Count;
    INT4                i4SendBytes;
    INT4                i4OptVal = 0;
    UINT1              *pRawPkt;
    UINT2               u2Len = 0;

    UNUSED_PARAM (u4SrcAddr);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Len, 2, 2);
    u2Len = OSIX_NTOHS (u2Len);

    if ((pRawPkt = (UINT1 *) MEM_MALLOC (u2Len, UINT1 *)) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return DVMRP_NOTOK;
    }
    if (CRU_BUF_Copy_FromBufChain (pBuf, pRawPkt, 0, u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        MEM_FREE (pRawPkt);
        return DVMRP_NOTOK;
    }
    /* Once the packet is copied from CRU buffer to Linier buffer,
       release CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, 0);

    MEMSET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);

    /* it is a multicast data packet oif list scan the given oifs and 
       send out the packet on all the Oifs, choose the interface address 
       based on the interface on which of the packet is sending out */
    u4OifCnt = *pOifList;
    for (u4Count = 0; u4Count < u4OifCnt; u4Count++)
    {
        /* pOifList contains first as Oif count, second as Incoming 
           interface, so skip both thn we get the first Oif */
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pOifList[u4Count + 2]);
        IfAddr.s_addr = OSIX_HTONL (pIfNode->u4IfLocalAddr);

        i4OptVal = 1;
        if (setsockopt (gDpConfigParams.i4DvmrpSockId,
                        IPPROTO_IP, IP_HDRINCL, &i4OptVal,
                        sizeof (i4OptVal)) < 0)
        {
            LOG1 ((CRITICAL, IHM, "setsockopt failed  IP_HDR_INCL\n"));
        }

        if (setsockopt (gDpConfigParams.i4DvmrpSockId,
                        IPPROTO_IP, IP_MULTICAST_IF,
                        (char *) &(IfAddr), sizeof (IfAddr)) < 0)
        {
            LOG1 ((CRITICAL, IHM, "setsockopt failed  IP_MULTICAST_IF\n"));
        }

        if ((i4SendBytes = sendto ((gDpConfigParams.i4DvmrpSockId), pRawPkt,
                                   u2Len, MSG_DONTWAIT,
                                   ((struct sockaddr *) &DestAddr),
                                   (sizeof (struct sockaddr_in)))) < 0)
        {
            LOG2 ((CRITICAL, IHM, "Multicast data packet forwarding on"
                   "interface %d failed \n", pOifList[u4Count + 2]));
        }
    }
    i4OptVal = 0;
    if (setsockopt (gDpConfigParams.i4DvmrpSockId,
                    IPPROTO_IP, IP_HDRINCL, &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        LOG1 ((CRITICAL, IHM, "setsockopt failed  IP_HDR_INCL\n"));
    }

    MEM_FREE (pRawPkt);
#endif

    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpCurrentTime                                     */
/* Description      :  This function gets the Current time                  */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  Current Timer                                        */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpCurrentTime (void)
#else
INT4
DvmrpCurrentTime (void)
#endif
{
    tOsixSysTime        SysTime;

    if (OsixGetSysTime (&SysTime) == OSIX_SUCCESS)
    {
        return (SysTime);
    }

    else
    {
        return (DVMRP_NOTOK);
    }
}

/****************************************************************************/
/* Function Name    :  DvmrpStartTimer                                      */
/* Description      :  This function starts a Timer for specified duration  */
/* Input (s)        :  u4TmrRef - Timer Reference block with info about     */
/*                     Timer which will be started.                         */
/*                     u2Duration - Duration for which the timer will be    */
/*                     started                                              */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK on Success and DVMRP_NOTOK on Failure       */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpStartTimer (tTmrAppTimer * pTimerRef, UINT2 u2Duration)
#else
INT4
DvmrpStartTimer (tTmrAppTimer * pTimerRef, u2Duration)
     tTmrAppTimer       *pTimerRef;
     UINT2               u2Duration;
#endif
{

    /* 
     * As the timer needs to be started according to the granularity of system
     * timer, here the value in sec is converted to sys ticks
     */
    DVMRP_GET_SYS_TIME (u2Duration);

    if (TmrStartTimer
        (g1sTimerListId, pTimerRef, (UINT4) u2Duration) != TMR_SUCCESS)
    {
        return (DVMRP_NOTOK);
    }
    else
    {
        return (DVMRP_OK);
    }
}

/****************************************************************************/
/* Function Name    :  DvmrpStopTimer                                       */
/* Description      :  This function stops a Timer which is running        */
/* Input (s)        :  u4TmrRef - Timer Reference block with info about     */
/*                     Timer which will be started.                         */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK on Success and DVMRP_NOTOK on Failure       */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpStopTimer (tTmrAppTimer * pTimerRef)
#else
INT4
DvmrpStopTimer (tTmrAppTimer * pTimerRef)
     tTmrAppTimer       *pTimerRef;
#endif
{

    if (TmrStopTimer (g1sTimerListId, pTimerRef) != TMR_SUCCESS)
    {
        return (DVMRP_NOTOK);
    }
    else
    {
        return (DVMRP_OK);
    }
}

/****************************************************************************/
/* Function Name    :  DvmrpHandleTimers                                    */
/* Description      :  This function is invoked whenever a timer event is   */
/*                     received. Based on the Timers the                */
/*                     corresponding routines are called                    */
/* Input (s)        :  timerListId - Id of the Timer List in which a timer  */
/*               has expired.                        */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
void
DvmrpHandleTimers (tTimerListId timerListId)
{
    UINT1               u1TimerId = 0;
    tTmrAppTimer       *pAppTimer = NULL;
    tDvmrpTimer        *pDvmrpTimer;
    while ((pAppTimer = TmrGetNextExpiredTimer (timerListId)) != NULL)
    {
        pDvmrpTimer = (tDvmrpTimer *) pAppTimer;
        u1TimerId = pDvmrpTimer->u1TimerType;
        if (u1TimerId > 0)
        {
            (*gaDvmrpTmrExpHdlr[u1TimerId - 1]) ((tDvmrpTimer *) (pAppTimer));
        }
    }
}

/****************************************************************************
* Function Name    :  DvmrpGlobalMemInit                           
* Description      :  This function initialises the Global Memory
* Input (s)        :  None
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/

INT4
DvmrpGlobalMemInit ()
{

    if (DvmrpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        LOG1 ((WARNING, IHM, "Unable to Create MemPools"));
        return DVMRP_NOTOK;
    }

    gDvmrpMemPool.ForwTblPoolId = DVMRPMemPoolIds
        [MAX_DP_FWD_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.ForwCacheTblPoolId = DVMRPMemPoolIds
        [MAX_DP_FWD_CACHE_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.HostMbrTblPoolId = DVMRPMemPoolIds
        [MAX_DP_HOST_MBR_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.NbrTblPoolId = DVMRPMemPoolIds
        [MAX_DP_NBR_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.RouteTblPoolId = DVMRPMemPoolIds
        [MAX_DP_ROUTE_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.NextHopTblPoolId = DVMRPMemPoolIds
        [MAX_DP_NEXT_HOP_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.IfaceListPoolId = DVMRPMemPoolIds
        [MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.NbrListPoolId = DVMRPMemPoolIds
        [MAX_DP_NBR_FREE_POOL_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.SrcInfoPoolId = DVMRPMemPoolIds
        [MAX_DP_SRC_FREE_POOL_TBL_ENTRIES_SIZING_ID];
    gDvmrpMemPool.IfaceTblPoolId = DVMRPMemPoolIds[MAX_DP_MAX_IFACES_SIZING_ID];
    gDvmrpMemPool.QPoolId = DVMRPMemPoolIds[MAX_DP_Q_DEPTH_SIZING_ID];
    gDvmrpMemPool.MiscPoolId = DVMRPMemPoolIds[MAX_DP_MISC_BLOCKS_SIZING_ID];
    gDvmrpMemPool.ForwTblPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_FWD_TBL_PTR_SIZING_ID];
    gDvmrpMemPool.ForwCachePtrPoolId = DVMRPMemPoolIds
        [MAX_DP_FWD_CACHE_PTR_SIZING_ID];
    gDvmrpMemPool.HostMbrPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_HOST_MBR_PTR_SIZING_ID];
    gDvmrpMemPool.NbrTblPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_NBR_TBL_PTR_SIZING_ID];
    gDvmrpMemPool.RouteTblPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_ROUTE_TBL_PTR_SIZING_ID];
    gDvmrpMemPool.NextHopPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_NEXT_HOP_PTR_SIZING_ID];
    gDvmrpMemPool.IfaceListPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_IFACE_LIST_PTR_SIZING_ID];
    gDvmrpMemPool.NbrListPtrPoolId = DVMRPMemPoolIds
        [MAX_DP_NBR_LIST_PTR_SIZING_ID];

    return (DVMRP_OK);
}

/****************************************************************************
* Function Name    :  DvmrpGlobalMemAllocate                           
* Description      :  This function initialises the Global Memory
* Input (s)        :  None
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/

INT4
DvmrpGlobalMemAllocate ()
{
    tNbrTbl            *pNbrTbl;
    tNextHopTbl        *pNextHopTbl;
    tRouteTbl          *pRouteTbl, *pPrevRouteTbl = NULL;
    tForwCacheTbl      *pForwCacheTbl, *pPrevForwCacheTbl = NULL;
    tIfaceList         *pIfaceFreePool, *pPrevIfaceFreePool = NULL;
    tNbrList           *pNbrFreePool, *pPrevNbrFreePool = NULL;
    UINT4               u4Var = 0;
    UINT4               u4TblSize = 0;

    /* 
     * Initialization of ForwTable
     */
    u4TblSize = FsDVMRPSizingParams[MAX_DP_FWD_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_FWD_TBL_PID,
                                 ((UINT1 **) (gpForwTable + u4Var))) ==
            MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for ForwTable"));
            return (DVMRP_NOTOK);
        }

        MEMSET (*(gpForwTable + u4Var), 0, sizeof (tForwTbl));
        ((tForwTbl *) (*(gpForwTable + u4Var)))->u1Status = DVMRP_INACTIVE;
    }
    LOG1 ((INFO, IHM, "Forward Table Initialised"));

    /* 
     * Initialization of ForwCache Table
     */

    u4TblSize = FsDVMRPSizingParams[MAX_DP_FWD_CACHE_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_FWD_CACHE_TBL_PID,
                                 ((UINT1 **) (gpForwCacheTable + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for ForwCacheTable"));
            return (DVMRP_NOTOK);
        }
        pForwCacheTbl = (tForwCacheTbl *) (*(gpForwCacheTable + u4Var));
        MEMSET (pForwCacheTbl, 0, sizeof (tForwCacheTbl));
        pForwCacheTbl->Next.pNext = NULL;
        if (u4Var != 0)
        {
            pPrevForwCacheTbl->Next.pNext =
                (tTMO_SLL_NODE *) (VOID *) pForwCacheTbl;
        }
        pPrevForwCacheTbl = pForwCacheTbl;

    }
    LOG1 ((INFO, IHM, "Forward Cache Table MemBlock Initialised"));
    gpForwFreePtr = *gpForwCacheTable;

    /* 
     * Initialization of Host Member Table
     */

    u4TblSize = FsDVMRPSizingParams[MAX_DP_HOST_MBR_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_HOST_MBR_TBL_PID,
                                 ((UINT1 **) (gpHostMbrTable + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for HostMbrTable"));
            return (DVMRP_NOTOK);
        }

        ((tHostMbrTbl *) (*(gpHostMbrTable + u4Var)))->u1Status =
            DVMRP_INACTIVE;
    }
    LOG1 ((INFO, IHM, "Host Member Table Initialised"));

    /* 
     * Initialization of Neighbor Table Entries
     */
    u4TblSize = FsDVMRPSizingParams[MAX_DP_NBR_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_NBR_TBL_PID,
                                 ((UINT1 **) (gpNeighborTable + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for NBRTable"));
            return (DVMRP_NOTOK);
        }
        pNbrTbl = (tNbrTbl *) (*(gpNeighborTable + u4Var));
        MEMSET (pNbrTbl, 0, sizeof (tNbrTbl));
        pNbrTbl->u1Status = DVMRP_INACTIVE;
        pNbrTbl->u1SeenByNbrFlag = DVMRP_FALSE;
        pNbrTbl->pNbrTblNext = NULL;
    }
    LOG1 ((INFO, IHM, "Neighbor Table Initialised"));

    /* 
     * Initialization of Route Table
     */

    u4TblSize = FsDVMRPSizingParams[MAX_DP_ROUTE_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < 100; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_ROUTE_TBL_PID,
                                 ((UINT1 **) (gpRouteTable + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for RouteTable"));
            return (DVMRP_NOTOK);
        }
        pRouteTbl = (tRouteTbl *) (*(gpRouteTable + u4Var));
        MEMSET (pRouteTbl, 0, sizeof (tRouteTbl));
        pRouteTbl->u1Status = DVMRP_INACTIVE;
        pRouteTbl->RouteEntryNext.pNext = NULL;
        if (u4Var != 0)
        {
            pPrevRouteTbl->RouteEntryNext.pNext =
                (tTMO_SLL_NODE *) (VOID *) pRouteTbl;
        }
        pPrevRouteTbl = pRouteTbl;
    }
    LOG1 ((INFO, IHM, "Route Table Initialised"));
    gpRouteFreePtr = *gpRouteTable;

    /*
     * Initialization of Route Next Hop Table 
     */
    u4TblSize = FsDVMRPSizingParams[MAX_DP_NEXT_HOP_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_NEXT_HOP_TBL_PID,
                                 ((UINT1 **) (gpNextHopTable + u4Var))) ==
            MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for RouteNextHopTable"));
            return (DVMRP_NOTOK);
        }
        pNextHopTbl = (tNextHopTbl *) (*(gpNextHopTable + u4Var));
        MEMSET (pNextHopTbl, 0, sizeof (tNextHopTbl));
        pNextHopTbl->pNextHopType = NULL;
        pNextHopTbl->u1Status = DVMRP_INACTIVE;
    }
    LOG1 ((INFO, IHM, "Route Next Hop Table Initialised"));

    /* 
     * Initialization of Interface Free Pool
     */
    u4TblSize = FsDVMRPSizingParams
        [MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES_SIZING_ID].u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_IFACE_LIST_PID,
                                 ((UINT1 **) (gpGlobIfacePtr + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for IFaceList"));
            return (DVMRP_NOTOK);
        }

        pIfaceFreePool = (tIfaceList *) (*(gpGlobIfacePtr + u4Var));
        MEMSET (pIfaceFreePool, 0, sizeof (tIfaceList));
        pIfaceFreePool->pNext = NULL;
        if (u4Var != 0)
        {
            pPrevIfaceFreePool->pNext = pIfaceFreePool;
        }
        pPrevIfaceFreePool = pIfaceFreePool;
    }
    LOG1 ((INFO, IHM, "Interface Free Pool Initialised"));
    gpFreeIfacePtr = *gpGlobIfacePtr;

    /*
     * Initialization of Neighbor Free Pool
     */

    u4TblSize = FsDVMRPSizingParams[MAX_DP_NBR_FREE_POOL_TBL_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;

    for (u4Var = 0; u4Var < u4TblSize; u4Var++)
    {
        if (MemAllocateMemBlock (DVMRP_NBR_LIST_PID,
                                 ((UINT1 **) (gpGlobNbrPtr + u4Var)))
            == MEM_FAILURE)
        {
            LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
                   " for NBRList"));
            return (DVMRP_NOTOK);
        }
        pNbrFreePool = (tNbrList *) (*(gpGlobNbrPtr + u4Var));
        MEMSET (pNbrFreePool, 0, sizeof (tNbrList));
        pNbrFreePool->pNext = NULL;
        if (u4Var != 0)
        {
            pPrevNbrFreePool->pNext = pNbrFreePool;
        }
        pPrevNbrFreePool = pNbrFreePool;
    }
    LOG1 ((INFO, IHM, "Neighbor Free Pool Initialised"));
    gpFreeNbrPtr = *gpGlobNbrPtr;
    return (DVMRP_OK);
}

/****************************************************************************
* Function Name    :  DvmrpInitIfaceTimersAndBuffer
* Description      :  This function initialises the Interface and Timer buffers
*                     is received
* Input (s)        :  None
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/

INT4
DvmrpInitIfaceTimersAndBuffer ()
{
    DvmrpInitInterfaceInfo ();

    /* Timers to be created */
    if (DvmrpTimersAlloc () == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to create Timer List"));
        return DVMRP_NOTOK;
    }

    if (DvmrpIhmStartTimers () == DVMRP_NOTOK)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;

}

/****************************************************************************
* Function Name    :  DvmrpMemAllocate                           
* Description      :  This function allocates the memory
*                     is received
* Input (s)        :  MemPoold - Memory Pool Id
* Output (s)       :  Pointer to the Memory Pool
* Returns          :  None                                                 
****************************************************************************/

INT4
DvmrpMemAllocate (tMemPoolId MemPoolId, UINT1 **ppu1Block)
{

    INT4                i4MemAllocateStatus;
    i4MemAllocateStatus = DVMRP_OK;

    /* Allocate a block from the MemPool.
     */
    if (MemAllocateMemBlock (MemPoolId, ppu1Block) == MEM_FAILURE)

    {

        i4MemAllocateStatus = DVMRP_NOTOK;
    }

    return i4MemAllocateStatus;
}

/****************************************************************************
* Function Name    :  DvmrpMemRelease                           
* Description      :  This function releases the memory
*                     is received
* Input (s)        :  MemPoold - Memory Pool Id
* Output (s)       :  Pointer to the Memory Pool
* Returns          :  None                                                 
****************************************************************************/

INT4
DvmrpMemRelease (tMemPoolId MemPoolId, UINT1 *pu1Block)
{

    INT4                i4Status;
    UINT4               u4MemReleaseStatus;

    i4Status = DVMRP_OK;

    /* Release the block to the MemPool.
     */
    u4MemReleaseStatus = MemReleaseMemBlock (MemPoolId, pu1Block);

    if (u4MemReleaseStatus == MEM_FAILURE)
    {
        i4Status = DVMRP_NOTOK;
    }

    return i4Status;
}

/****************************************************************************/
/* Function Name    : Log1                                                  */
/* Description      : This function logs messages onto the screen and/or    */
/*                    console.                                              */
/* Input (s)        : Message level - Warning/Info/Critical                 */
/*                    Module name                                           */
/*                    Message string                                        */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
Log1 (UINT1 u1Level, UINT4 u4Module, const CHR1 * pMsg)
#else

void
Log1 (u1Level, u4Module, pMsg)
     UINT1               u1Level;
     UINT4               u4Module;
     const CHR1         *pMsg;
#endif
{
    UINT1               pLine[512];

    u1Level = 0;                /* Just to remove warning */

    UNUSED_PARAM (u1Level);

    SPRINTF ((CHR1 *) pLine, "%s \n", pMsg);
    UtlTrcLog (u4Module, gu4DvmrpLogMask, "DVMRP: ", (const CHR1 *) pLine);

    return;
}

/****************************************************************************/
/* Function Name    : Log2                                                  */
/* Description      : This function logs messages onto the screen and/or    */
/*                    console.                                              */
/* Input (s)        : Message level - Warning/Info/Critical                 */
/*                    Module name                                           */
/*                    Message string                                        */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
Log2 (UINT1 u1Level, UINT4 u4Module, const CHR1 * pMsg, UINT4 u4Value)
#else

void
Log2 (u1Level, u4Module, pMsg, u4Value)
     UINT1               u1Level;
     UINT4               u4Module;
     const CHR1         *pMsg;
     UINT4               u4Value;
#endif
{
    UINT1               pLine[512];
    u1Level = u1Level;

    SPRINTF ((CHR1 *) pLine, "%s Hex 0x%x \n", pMsg, u4Value);
    UtlTrcLog (u4Module, gu4DvmrpLogMask, "DVMRP: ", (const CHR1 *) pLine);
    return;
}

 /***************************************************************************
 * Function Name    :  DvmrpMfwdHandleStatus
 *
 * Description      :  This function is used to post the status change info
 *                     about the multicast forwarding module (MFWD), this
 *                     function is registered with MFWD.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  pBuf      - Pointer to the Buffer
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
DvmrpMfwdHandleStatus (UINT4 u4Event)
{
    /* Send a EVENT to DVMRP */
    OsixEvtSend (gu4DvmrpTaskId, u4Event);
    return SUCCESS;
}

/****************************************************************************
* Function Name    :  DvmrpGlobalTableInit                           
* Description      :  This function initialises the Global Tables used in DVMRP
* Input (s)        :  None
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/
VOID
DvmrpGlobalTableInit ()
{
    gpForwTable = (tForwTbl **) MemAllocMemBlk (DVMRP_FWD_TBL_PTR_PID);

    gpForwCacheTable =
        (tForwCacheTbl **) MemAllocMemBlk (DVMRP_FWD_CACHE_PTR_PID);

    gpHostMbrTable = (tHostMbrTbl **) MemAllocMemBlk (DVMRP_HOST_MBR_PTR_PID);

    gpNextHopTable = (tNextHopTbl **) MemAllocMemBlk (DVMRP_NEXT_HOP_PTR_PID);

    gpNeighborTable = (tNbrTbl **) MemAllocMemBlk (DVMRP_NBR_TBL_PTR_PID);

    gpRouteTable = (tRouteTbl **) MemAllocMemBlk (DVMRP_ROUTE_TBL_PTR_PID);

    gpGlobIfacePtr = (tIfaceList **) MemAllocMemBlk (DVMRP_IFACE_LIST_PTR_PID);

    gpGlobNbrPtr = (tNbrList **) MemAllocMemBlk (DVMRP_NBR_LIST_PTR_PID);
    return;

}

/****************************************************************************
* Function Name    :  DvmrpGlobalTableFree                           
* Description      :  This function frees the Global Tables used in DVMRP
* Input (s)        :  None
* Output (s)       :  None                                                 
* Returns          :  None                                                 
****************************************************************************/

VOID
DvmrpGlobalTableFree ()
{
    /* Free all the global pointers */

    DVMRP_MEM_FREE (gpForwTable);
    gpForwTable = NULL;

    DVMRP_MEM_FREE (gpForwCacheTable);
    gpForwCacheTable = NULL;

    DVMRP_MEM_FREE (gpHostMbrTable);
    gpHostMbrTable = NULL;

    DVMRP_MEM_FREE (gpNextHopTable);
    gpNextHopTable = NULL;

    DVMRP_MEM_FREE (gpNeighborTable);
    gpNeighborTable = NULL;

    DVMRP_MEM_FREE (gpRouteTable);
    gpRouteTable = NULL;

    DVMRP_MEM_FREE (gpGlobIfacePtr);
    gpGlobIfacePtr = NULL;

    DVMRP_MEM_FREE (gpGlobNbrPtr);
    gpGlobNbrPtr = NULL;
    return;
}

/****************************************************************************
 * Function Name    :  DvmrpVlanPortBmpChgNotify
 *
 * Description      :  This function posts an event to the DVMRP task for the
 *                     change in the port bit map of a VLan for a group.
 *                     
 * Input(s)         :  MacAddr - Multicast Mac Address from Vlan
 *                     VlanId  - The Vlan Id of the interface for which a join or leave for the multicast
 *                               mac address was received
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :   
 *
 * Global Variables
 * Modified         :  None
 *
 * Return(s)        :  None 
 ****************************************************************************/
#ifdef FS_NPAPI
VOID
DvmrpVlanPortBmpChgNotify (tMacAddr MacAddr, tDvmrpVlanId VlanId)
{
    tIpmcMcastVlanPbmpData DvmrpVlanMcastData;
    tDvmrpQMsg         *pQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    LOG1 ((INFO, IHM, "Entering Function DvmrpVlanPortBmpChgNotify\n"));

    if (CfaGetVlanInterfaceIndex (VlanId) == CFA_INVALID_INDEX)
    {
        LOG1 ((INFO, IHM, "Invalid VLANID passed for the PBMP change\n"));
        return;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tIpmcMcastVlanPbmpData), 0);

    if (pBuf == NULL)
    {
        LOG1 ((INFO, IHM,
               "CRU allocation for Vlan PBMP Change notification to DVMRP - FAILED \n"));
        return;
    }
/* Get the Vlan related information */
    MEMCPY (DvmrpVlanMcastData.MacAddr, MacAddr, (sizeof (tMacAddr)));
    DvmrpVlanMcastData.VlanId = VlanId;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &DvmrpVlanMcastData, 0,
                               sizeof (tIpmcMcastVlanPbmpData));

    if ((pQMsg = (tDvmrpQMsg *) MemAllocMemBlk (DVMRP_Q_PID)) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for Q"));
        CRU_BUF_RELEASE_CHAIN (pBuf);
        return;
    }

    pQMsg->u4MsgType = DVMRP_PBMP_CHANGE_EVENT;
    pQMsg->DvmrpQMsgParam.DvmrpIfParam = pBuf;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        CRU_BUF_RELEASE_CHAIN (pBuf);
        pBuf = NULL;
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return;
    }

    if (OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {

        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        return;
    }
    LOG1 ((INFO, IHM, "Packet enqueued successfully"));
    return;

}
#endif

/*-------------------------------------------------------------------+
 *
 * Function           : DvmrpExtractIpHdr
 *
 * Input(s)           : pIp, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : DVMRP_OK, DVMRP_NOTOK
 *
 * Action :
 * This routine is used to get the IP header in the packet.
 *
+-------------------------------------------------------------------*/
INT4
DvmrpExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp;
    UINT1              *pu1Options;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, 0, IP_HDR_LEN);

    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */

        pIpHdr = &TmpIpHdr;

        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    u1Tmp = pIpHdr->u1Ver_hdrlen;

    pIp->u1Version = (u1Tmp >> 4);
    pIp->u1Hlen = (u1Tmp & 0x0f) << 2;
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Len = CRU_NTOHS (pIpHdr->u2Totlen);

    if (pIp->u1Hlen < IP_HDR_LEN || pIp->u2Len < pIp->u1Hlen)
    {
        LOG2 ((CRITICAL, IHM, "Discarding pkt rcvd with header length %d.\n",
               pIp->u1Hlen));

        return DVMRP_NOTOK;
    }

    pIp->u2Olen = (((u1Tmp & 0x0f) * 4) - IP_HDR_LEN);
    pIp->u2Id = CRU_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = CRU_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = CRU_NTOHS (pIpHdr->u2Cksum);

    MEMCPY (&u4TempSrc, &pIpHdr->u4Src, sizeof (UINT4));
    MEMCPY (&u4TempDest, &pIpHdr->u4Dest, sizeof (UINT4));

    u4TempSrc = CRU_NTOHL (u4TempSrc);
    u4TempDest = CRU_NTOHL (u4TempDest);

    MEMCPY (&pIp->u4Src, &u4TempSrc, sizeof (UINT4));
    MEMCPY (&pIp->u4Dest, &u4TempDest, sizeof (UINT4));

    if (pIp->u2Olen)
    {
        pu1Options = CRU_BUF_Get_DataPtr_IfLinear (pBuf, IP_HDR_LEN,
                                                   pIp->u2Olen);
        if (pu1Options == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIp->au1Options,
                                       IP_HDR_LEN, pIp->u2Olen);
        }
        else
        {
            MEMCPY (pIp->au1Options, pu1Options, pIp->u2Olen);
        }
    }
    return DVMRP_OK;
}
