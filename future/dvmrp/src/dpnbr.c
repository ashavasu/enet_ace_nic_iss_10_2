/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpnbr.c,v 1.10 2014/04/04 10:04:43 siva Exp $
 *
 * Description:This file contains the functions for Neighbor  
 *             Discovery Module                              
 *
 *******************************************************************/
#include "dpinc.h"

/****************************************************************************/
/* Function Name    :  DvmrpNdmSendProbes                                   */
/* Description      :  This function decides the interface on which the     */
/*                     probe message has to be forwarded                    */
/* Input (s)        :  u1GenFlag - Indicates whether it is startup Event or */
/*                                 Probe Timer Expiry Event                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpNdmSendProbes (UINT1 u1GenFlag)
#else
void
DvmrpNdmSendProbes (u1GenFlag)
     UINT1               u1GenFlag;
#endif
{
    tDvmrpInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    INT1                i1First = DVMRP_NOTOK;

    if (u1GenFlag == DVMRP_NEW_GENID)
    {
        gu4GenerationId = DvmrpCurrentTime ();
        LOG2 ((INFO, NDM, "Generation ID is Updated with new value:",
               gu4GenerationId));
    }

    pSllNode = TMO_SLL_First (&gDvmrpIfInfo.IfGetNextList);

    i1First = (pSllNode != NULL) ? DVMRP_OK : DVMRP_NOTOK;

    do
    {
        if (i1First == DVMRP_NOTOK)
        {
            break;
        }

        pIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                      IfGetNextLink, pSllNode);
        if (DVMRP_CHECK_IF_STATUS (pIfNode))
        {
            DvmrpNdmSndProbeOnIface (pIfNode);
        }
    }
    while ((pSllNode = TMO_SLL_Next (&gDvmrpIfInfo.IfGetNextList,
                                     pSllNode)) != NULL);
    /* 
     * Start the Probe Timer irrespective of whether the interfaces are    
     * active or not                                                      
     */

    if (DvmrpUtilStartTimer (DVMRP_PROBE_TIMER,
                             DVMRP_PROBE_INTERVAL, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, NDM, "Unable to start Probe Timer"));
        return;
    }
    else
    {
        LOG1 ((INFO, NDM, "Started Probe Timer"));
    }
    return;
}

/****************************************************************************/
/* Function Name    : DvmrpNdmSndProbeOnIface                               */
/* Description      : This function forwards the Probe Message to the Output*/
/*                    Module                                                */
/* Input (s)        : u4IfIndex - Index into the Interface Table            */
/* Output (s)       : The Probe packet is formed and sent to Output Module  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpNdmSndProbeOnIface (tDvmrpInterfaceNode * pIfNode)
#else
void
DvmrpNdmSndProbeOnIface (u4IfIndex)
     UINT4               u4IfIndex;
#endif
{
    tNbrTbl            *pNbrTbl = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4GenId = 0, u4Count = 0, u4NbrIpAddr = 0, u4Temp = 0;

    /* 
     * Immeterial of whether we have nbr on this iface we are gonna send the 
     * probes
     */
    u4IfIndex = pIfNode->u4IfIndex;
    pNbrTbl = pIfNode->pNbrOnthisIface;
    LOG2 ((INFO, NDM, "Nbr on this interface is :", CLI_PTR_TO_U4 (pNbrTbl)));

    /* 
     * The size of the packet to be forwarded is calculated
     */
    while (pNbrTbl)
    {
        u4Count++;
        pNbrTbl = pNbrTbl->pNbrTblNext;
    }

    u4Count = (u4Count * IP_ADDR_LEN) + MAX_IGMP_HDR_LEN +
        DVMRP_GENERATION_ID_LEN;
    LOG2 ((INFO, NDM, "Buffer Size is :", u4Count));

    DVMRP_ALLOC_SEND_BUFFER ();
    if (gpSendBuffer == NULL)
    {
        LOG1 ((CRITICAL, NDM, "Unable to allocate Mem Block for Send Buffer"));
        return;
    }

    /* 
     * Get the current system time and copy it into the SendBuffer as the  
     * router has restarted                                               
     */

    u4GenId = OSIX_HTONL (gu4GenerationId);
    MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN], &u4GenId, DVMRP_GENERATION_ID_LEN);

    /* 
     * Fill in the IP address of the neighbors on this interface in the Buffer
     */

    pNbrTbl = pIfNode->pNbrOnthisIface;
    while (pNbrTbl)
    {
        u4NbrIpAddr = OSIX_HTONL (pNbrTbl->u4NbrIpAddress);
        MEMCPY (&gpSendBuffer
                [MAX_IGMP_HDR_LEN + DVMRP_GENERATION_ID_LEN + u4Temp],
                &u4NbrIpAddr, IP_ADDR_LEN);
        u4Temp += IP_ADDR_LEN;
        pNbrTbl = pNbrTbl->pNbrTblNext;
    }

    DvmrpOmSendPacket (u4IfIndex, ALL_DVMRP_ROUTERS, u4Count, DVMRP_PROBE);
    return;
}

/****************************************************************************/
/* Function Name    : DvmrpNdmhandleProbePacket                             */
/* Description      : This function processes an incoming Neighbor Probe    */
/*                    Message                                               */
/* Input (s)        : u4DataLen  - Length of the Probe Packet               */
/*                    u4IfIndex  - Interface Index on which this packet     */
/*                                 is received                              */
/*                    u4SrcAddr  - Neighbor address from whom this packet   */
/*                                 has been received                        */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpNdmHandleProbePacket (UINT4 u4DataLen, UINT4 u4IfIndex, UINT4 u4SrcAddr,
                           UINT1 u1NbrCap)
#else
void
DvmrpNdmHandleProbePacket (u4DataLen, u4IfIndex, u4SrcAddr, u1NbrCap)
     UINT4               u4DataLen;
     UINT4               u4IfIndex;
     UINT4               u4SrcAddr;
     UINT1               u1NbrCap;
#endif
{
    tDvmrpInterfaceNode *pIfTbl = NULL;
    tNbrTbl            *pNbrTbl = NULL,
        *pCurNbr = NULL, *pPrevNbrOnIface = NULL, *pNbrOnIface = NULL;
    UINT4               u4GenId = 0, u4NbrIpAddr = 0, u4Temp = 0, u4Time =
        0, u4Var = 0;
    UINT1              *pDvmrpData = NULL, u1NbrRebooted =
        DVMRP_FALSE, u1SeenByNbr = DVMRP_FALSE;

    pIfTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
    if (NULL == pIfTbl)
    {
        LOG1 ((WARNING, NDM, "Probe Msg Due to Invalid Interface Node!!!"));
        return;
    }
    if ((u4SrcAddr & pIfTbl->u4IfMask) !=
        (pIfTbl->u4IfLocalAddr & pIfTbl->u4IfMask))
    {
        LOG1 ((WARNING, NDM, "Probe Msg with Different Network !!!"));
        return;
    }
    pNbrOnIface = pIfTbl->pNbrOnthisIface;
    pDvmrpData = gpRcvBuffer;

    /*
     * need not check for NULL probe and Valid Host, as Validating 
     * the host will be done by IP.
     */

    /*
     * The Probe Packet must atleast have the GenId eventhough the list  
     * of IP address is not there. Store the GenId as it is required to check 
     * if the nbr got rebooted
     */

    if (u4DataLen < DVMRP_GENERATION_ID_LEN)
    {
        LOG1 ((WARNING, NDM, "Invalid Data Length"));
        pIfTbl->u4IfRcvBadPkts++;
        LOG1 ((WARNING, NDM, "Interface Statistics Updated"));
        return;
    }

    u4GenId = OSIX_NTOHL (*((UINT4 *) (VOID *) pDvmrpData));
    u4DataLen -= DVMRP_GENERATION_ID_LEN;

    /* 
     * Check if this Nbr is already in the Interface Table's Nbr List  
     */

    for (; pNbrOnIface != NULL; pNbrOnIface = pNbrOnIface->pNbrTblNext)
    {

        pPrevNbrOnIface = pNbrOnIface;
        if (u4SrcAddr == pNbrOnIface->u4NbrIpAddress)
        {
            if (pNbrOnIface->u1Status == DVMRP_ACTIVE)
            {
                /* 
                 * The Entry is refreshed  
                 */
                pNbrOnIface->u4ExpiryTime = DVMRP_NBR_TIME_OUT_TIMER_VAL;
                LOG2 ((INFO, NDM,
                       "Neighbor Expiry Time updated with the value",
                       pNbrOnIface->u4ExpiryTime));
                pCurNbr = pNbrOnIface;
                if (pNbrOnIface->u4NbrGenId != u4GenId)
                {
                    LOG1 ((INFO, NDM, "Generation ID changed"));

                    /* 
                     * Just set a flag now so that route reports can be sent 
                     * later below, because genid has changed 
                     */
                    u1NbrRebooted = DVMRP_TRUE;
                    /* 
                     * Make a fresh entry for genid and update entry 
                     * creation time
                     */
                    pNbrOnIface->u4NbrGenId = u4GenId;
                    pNbrOnIface->u4NbrUpTime = DvmrpCurrentTime ();
                }
                break;
            }
        }
    }

    /* 
     * Add this New Neighbor in the Nbr Table and Interface Table  
     */

    /* 
     * Check for an INVALID Status in the Neighbor Table, and install this nbr 
     * there. Well this is a new nbr on this iface, so must send route reports
     * to this neighbor.
     */

    if (pNbrOnIface == NULL)
    {
        for (u4Var = 0; u4Var < MAX_NBR_TABLE_ENTRIES; u4Var++)
        {
            if (DVMRP_GET_NBR_STATUS (u4Var) == DVMRP_INACTIVE)
                break;
        }

        if (u4Var == MAX_NBR_TABLE_ENTRIES)
        {
            LOG1 ((WARNING, NDM,
                   "No Entries can be created in Nbr Tbl, memory exhausted"));
            return;
        }

        pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u4Var);
        u4Time = DvmrpCurrentTime ();
        DVMRP_FILL_NBR_TBL_ENTRY (pNbrTbl, u4IfIndex, u4SrcAddr, u4Time,
                                  u4GenId, u1NbrCap);
        LOG1 ((INFO, NDM, "Seen By Neighbor Flag is set to FALSE"));

        /* 
         * Install this nbr as a last nbr on this iface
         */

        if (pPrevNbrOnIface == NULL)
        {
            pIfTbl->pNbrOnthisIface = pNbrTbl;
        }
        else
        {
            pPrevNbrOnIface->pNbrTblNext = pNbrTbl;
        }

        pCurNbr = pNbrTbl;
        u1NbrRebooted = DVMRP_TRUE;
        LOG1 ((INFO, NDM, "Neighbor added on this interface"));
    }

    /* 
     * Check If this router's address is present in the Probe Packet
     */

    u4Temp = 0;

    while (u4DataLen > 0)
    {
        if (u4DataLen < IP_ADDR_LEN)
        {
            LOG1 ((CRITICAL, NDM, "Invalid IP address Field"));
            LOG1 ((INFO, NDM, "Interface and Neighbor Statistics updated"));
            pIfTbl->u4IfRcvBadPkts++;
            pCurNbr->u4NbrRcvBadPkts++;
            return;
        }

        /* 
         * Check if this router's IP addr exists in the nbr report. If not, 
         * all dvmrp protocol packets from this nbr has to be ignored until
         * it takes this router's address in its probes
         */

        u4NbrIpAddr = OSIX_NTOHL (*((UINT4 *) (VOID *)
                                    &pDvmrpData[DVMRP_GENERATION_ID_LEN +
                                                u4Temp]));
        u4DataLen -= IP_ADDR_LEN;
        u4Temp += IP_ADDR_LEN;
        if (u4NbrIpAddr == pIfTbl->u4IfLocalAddr)
        {
            u1SeenByNbr = DVMRP_TRUE;
            break;
        }
        /* 
         * Not interested in the other grub (IP addrs) sent by my nbr as it will
         * be soon received from these nbrs. see the draft v3.5 [3.2.3]
         */
    }

    /* 
     * If seen_by_nbrouter still being FALSE.. Then this nbr will be in trouble..    * As all the dvmrp protocol packets from this nbr gonna be discarded....in 
     * the input module itself. [3.2] 1.
     */

    if ((u1NbrRebooted == DVMRP_TRUE) ||
        ((pCurNbr->u1SeenByNbrFlag == DVMRP_FALSE) &&
         (u1SeenByNbr == DVMRP_TRUE)))
    {
        /*
         * Search needed as DvmrpRrfSendRouteTable needs the neighbor index
         */
        for (u4Var = 0; u4Var < MAX_NBR_TABLE_ENTRIES; u4Var++)
        {
            if (pCurNbr->u4NbrIpAddress == DVMRP_GET_NBR_ADDRESS (u4Var))
            {
                DvmrpRrfSendRouteTable (u4Var);
                break;
            }
        }
    }
    pCurNbr->u1SeenByNbrFlag = u1SeenByNbr;
    return;
}

/****************************************************************************/
/* Function Name    : DvmrpNdmNbrTimerExpHandler                            */
/* Description      : This function on expiry of the Neighbor Timer sets the*/
/*                    status of the neighbor to INVALID and calls route Hdlr*/
/*                    when the expiry time of the neighbor has reached zero */
/* Input (s)        : None                                                  */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpNdmNbrTimerExpHandler (tDvmrpTimer * pTimerBlk)
#else
void
DvmrpNdmNbrTimerExpHandler (pTimerBlk)
     tDvmrpTimer        *pTimerBlk;
#endif
{
    tNbrTbl            *pNbrTbl = NULL, *pPrevNbrOnIface = NULL, *pTmpNbr =
        NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tDvmrpInterfaceNode *pIfaceNode = NULL;
    UINT4               u4Var;

    UNUSED_PARAM (pTimerBlk);

    for (u4Var = 0; u4Var < MAX_NBR_TABLE_ENTRIES; u4Var++)
    {
        pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u4Var);
        if (NULL == pNbrTbl)
        {
            continue;
        }
        if (pNbrTbl->u1Status == DVMRP_ACTIVE)
        {
            pNbrTbl->u4ExpiryTime--;
            if (pNbrTbl->u4ExpiryTime == 0)
            {
                pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pNbrTbl->u4NbrIfIndex);
                if (NULL == pIfNode)
                {
                    continue;
                }
                pTmpNbr = pIfNode->pNbrOnthisIface;
                pPrevNbrOnIface = pTmpNbr;
                while (pTmpNbr != NULL)
                {
                    if (pTmpNbr->u4NbrIpAddress == pNbrTbl->u4NbrIpAddress)
                    {
                        pTmpNbr->u1Status = DVMRP_INACTIVE;
                        LOG1 ((INFO, NDM,
                               "Neighbor on this interface is made inactive"));
                        LOG1 ((INFO, NDM,
                               "And removed from the interface list"));
                        pIfaceNode = (DVMRP_GET_IFNODE_FROM_INDEX
                                      (pNbrTbl->u4NbrIfIndex));
                        if ((pIfaceNode != NULL)
                            && (pTmpNbr == pIfaceNode->pNbrOnthisIface))
                        {

                            pIfaceNode->pNbrOnthisIface = pTmpNbr->pNbrTblNext;
                        }
                        else
                        {
                            pPrevNbrOnIface->pNbrTblNext = pTmpNbr->pNbrTblNext;
                        }
                        /* 
                         * Inactivated Nbr should be removed from the
                         * Neighbor list for that interface
                         */
                        pTmpNbr->pNbrTblNext = NULL;
                        DvmrpRthCancelTimedoutNbr (pNbrTbl->u4NbrIpAddress,
                                                   pNbrTbl->u4NbrIfIndex);
                        break;
                    }
                    else
                    {
                        pPrevNbrOnIface = pTmpNbr;
                        pTmpNbr = pTmpNbr->pNbrTblNext;
                    }
                }
            }
        }
    }

    /* Timer duration changed */
    if (DvmrpUtilStartTimer (DVMRP_NEIGHBOR_AGEOUT_TIMER,
                             DVMRP_NBR_AGE_OUT_TIMER_VAL, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, NDM, "Unable to start Neighbor Ageout Timer"));
    }
    return;
}

/****************************************************************************/
/* Function Name    : DvmrpNdmHandleIfaceDown                               */
/* Description      : This function deletes the list of neighbors on a      */
/*                    particular interface when that interface goes down.   */
/* Input (s)        : u4IfId - Neighbor Interface Id                        */
/* Output (s)       : None                                                  */
/* Returns          : None                                                  */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpNdmHandleIfaceDown (UINT4 u4IfIndex)
#else
void
DvmrpNdmHandleIfaceDown (u4IfIndex)
     UINT4               u4IfIndex;
#endif
{
    tDvmrpInterfaceNode *pIfTbl = NULL;
    tNbrTbl            *pNbrTbl = NULL, *pPrevNbrTbl = NULL;

    pIfTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
    if (NULL == pIfTbl)
    {
        LOG1 ((INFO, NDM, "Invalid Interface Node"));
        return;
    }
    pNbrTbl = pIfTbl->pNbrOnthisIface;

    while (pNbrTbl != NULL)
    {
        pNbrTbl->u1Status = DVMRP_INACTIVE;
        LOG1 ((INFO, NDM,
               "Status of Neighbors on this interface are made INACTIVE"));
        pPrevNbrTbl = pNbrTbl;
        pNbrTbl = pNbrTbl->pNbrTblNext;
        pPrevNbrTbl->pNbrTblNext = NULL;
    }
    pIfTbl->pNbrOnthisIface = NULL;
    return;
}

VOID
DvmrpNdmSendProbesOnTimerExpiry (tDvmrpTimer * pTimerBlk)
{
    UNUSED_PARAM (pTimerBlk);
    DvmrpNdmSendProbes (DVMRP_OLD_GENID);
    return;
}
