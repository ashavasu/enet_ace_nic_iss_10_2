/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpif.c,v 1.10 2015/11/05 13:21:45 siva Exp $
 *
 *******************************************************************/
#include "dpinc.h"

tDvmrpInterfaceNode *
DvmrpCreateInterfaceNode (UINT4 u4IfIndex, UINT1 u1IfStatus)
{

    tDvmrpInterfaceNode *pIfNode = NULL;

    UINT4               u4HashIndex = DVMRP_ZERO;

    /* Create the Interface Node and initialize the node with the default
     * value.  */
    if (DVMRP_MEM_ALLOCATE (DVMRP_IFACE_TBL_PID, pIfNode, tDvmrpInterfaceNode)
        == NULL)

    {

        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for IfaceNode"));

        return NULL;

    }

    /* Memory Block for the Interface node allocated successfully.
     * Lets proceed with the initialisation and add the newly
     * created interface node into the interface table.
     */

    MEMSET (pIfNode, DVMRP_ZERO, sizeof (tDvmrpInterfaceNode));

    /* Initialise and add the Interface node to the Interface list of
     * Instance Node
     */
    DvmrpSetDefaultIfaceNodeValue (u4IfIndex, u1IfStatus, pIfNode);

    DVMRP_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gDvmrpIfInfo.IfHashTbl, &(pIfNode->IfHashLink),
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    if (DVMRP_CHECK_IF_STATUS (pIfNode))

    {

        DvmrpRthLocIfaceUpdate (pIfNode->u4IfLocalAddr,
                                pIfNode->u4IfMask,
                                pIfNode->u1IfMetric, u4IfIndex, DVMRP_ACTIVE);

        gu4GenerationId = DvmrpCurrentTime ();

        LOG2 ((INFO, NDM, "Generation ID is Updated with new value:",
               gu4GenerationId));

        DvmrpNdmSndProbeOnIface (pIfNode);

        if (DvmrpUtilStartTimer (DVMRP_PROBE_TIMER,
                                 DVMRP_PROBE_INTERVAL, NULL) == DVMRP_NOTOK)

        {

            LOG1 ((WARNING, NDM, "Unable to start Probe Timer"));

        }

        else

        {

            LOG1 ((INFO, NDM, "Started Probe Timer"));

        }

    }

    return pIfNode;

}

INT4
DvmrpDeleteInterfaceNode (tDvmrpInterfaceNode * pIfaceNode)
{

    UINT4               u4HashIndex = DVMRP_ZERO;

    UINT4               u4IfIndex;

    u4IfIndex = pIfaceNode->u4IfIndex;

    if (DVMRP_CHECK_IF_STATUS (pIfaceNode))

    {

        LOG1 ((INFO, MOD_SNMP, "The Interface Index status changed"));

        LOG1 ((INFO, MOD_SNMP, "Interface is to be Destroyed"));

        DvmrpNdmHandleIfaceDown (u4IfIndex);

        DvmrpRthLocIfaceUpdate (pIfaceNode->u4IfLocalAddr, 0,
                                DEFAULT_METRIC, u4IfIndex, DVMRP_INACTIVE);

        pIfaceNode->u1IfStatus = DVMRP_INACTIVE;

        gu4NumActiveIfaces--;

    }

    /* Now Release the InterfaceNode */
    TMO_SLL_Delete (&(gDvmrpIfInfo.IfGetNextList), &pIfaceNode->IfGetNextLink);

    DVMRP_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Delete_Node (gDvmrpIfInfo.IfHashTbl,
                          &(pIfaceNode->IfHashLink), u4HashIndex);

    /* Now Release the InterfaceNode */
    DvmrpMemRelease (gDvmrpMemPool.IfaceTblPoolId, (UINT1 *) pIfaceNode);

    return DVMRP_OK;

}

VOID
DvmrpSetDefaultIfaceNodeValue (UINT4 u4IfIndex, UINT1 u1IfStatus,
                               tDvmrpInterfaceNode * pIfNode)
{

    tTMO_SLL_NODE      *pSllNode = NULL;

    tTMO_SLL_NODE      *pPrevSllNode = NULL;

    tDvmrpInterfaceNode *pCurIfNode = NULL;
    tNetIpv4IfInfo      IpInfo;
    UINT4               u4CfaIfIndex;
    UINT2               u2VlanId;

    TMO_SLL_Init_Node (&(pIfNode->IfHashLink));

    TMO_SLL_Init_Node (&(pIfNode->IfGetNextLink));

    if (DVMRP_GET_IFINFO (u4IfIndex, &IpInfo) != NETIPV4_SUCCESS)
    {
        LOG2 ((CRITICAL, IHM, "Unable to get Interface info from Ip"
               "for index %d \n", u4IfIndex));
        return;
    }

    if ((IpInfo.u4Admin == IPIF_ADMIN_ENABLE) &&
        (IpInfo.u4Oper == IPIF_OPER_ENABLE))

    {

        gu4NumActiveIfaces++;

        pIfNode->u1IfStatus = (UINT1) IpInfo.u4Oper;
        pIfNode->u4IfMTUSize = IpInfo.u4Mtu;
        pIfNode->u4IfLocalAddr = IpInfo.u4Addr;
        pIfNode->u4IfMask = IpInfo.u4NetMask;
        pIfNode->u1IfMetric = DEFAULT_METRIC;
        pIfNode->u4IfRcvBadPkts = 0;
        pIfNode->u4IfRcvBadRoutes = 0;
        pIfNode->pNbrOnthisIface = NULL;
    }
    else
    {
        pIfNode->u1IfStatus = DVMRP_INACTIVE;
    }
    /* Get the vlan id for this interface index */
    if (DVMRP_GET_IFINDEX_FROM_PORT (u4IfIndex,
                                     &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
         LOG1 ((WARNING, IHM, "Unable to get Interface index from port \n"));
         return;
    }

    if (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS)
    {
        pIfNode->u2VlanId = u2VlanId;
    }

    pIfNode->u1IfRowStatus = u1IfStatus;
    pIfNode->u4IfIndex = u4IfIndex;

    TMO_SLL_Scan (&gDvmrpIfInfo.IfGetNextList, pSllNode, tTMO_SLL_NODE *)
    {

        pCurIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode, IfGetNextLink,
                                         pSllNode);

        if (pCurIfNode->u4IfIndex > pIfNode->u4IfIndex)

        {
            break;
        }
        pPrevSllNode = pSllNode;
    }

    TMO_SLL_Insert (&gDvmrpIfInfo.IfGetNextList, pPrevSllNode,
                    (tTMO_SLL_NODE *) & pIfNode->IfGetNextLink);

    /*
     * Mtu and Route table updations shd not be done
     * for Inactive interfaces
     */
    if (gu4MinMTU == 0)
    {
        gu4MinMTU = pIfNode->u4IfMTUSize;
        LOG1 ((INFO, IHM, "MinMtu initalised"));
    }
    else
    {
        if (pIfNode->u4IfMTUSize < gu4MinMTU)
        {
            gu4MinMTU = pIfNode->u4IfMTUSize;
        }
        LOG1 ((INFO, IHM, "MinMtu updated"));
    }

    return;

}

tDvmrpInterfaceNode *
DvmrpGetInterfaceNode (UINT4 u4IfIndex)
{

    tDvmrpInterfaceNode *pIfaceNode = NULL;

    UINT4               u4HashIndex = DVMRP_ZERO;

    DVMRP_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gDvmrpIfInfo.IfHashTbl, u4HashIndex, pIfaceNode,
                          tDvmrpInterfaceNode *)
    {

        if (pIfaceNode->u4IfIndex == u4IfIndex)

        {

            break;

        }

        else if (pIfaceNode->u4IfIndex > u4IfIndex)

        {

            pIfaceNode = NULL;

            break;

        }

    }

    return pIfaceNode;

}

/****************************************************************************/
/* Function Name             : DvmrpGetVlanIdFromIfIndex                    */
/*                                                                          */
/* Description               : This function returns the VLAN ID for the    */
/*                             given IP interface index                     */
/*                                                                          */
/* Input (s)                 : u4Port   - IP Interface index                */
/*                                                                          */
/* Output (s)                : pu2VlanId - VLAN ID of the IP interface      */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : DVMRP_OK/DVMRP_NOTOK                         */
/****************************************************************************/
INT4
DvmrpGetVlanIdFromIfIndex (UINT4 u4Port, UINT2 *pu2VlanId)
{
    tDvmrpInterfaceNode *pIfaceTbl = NULL;

    pIfaceTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4Port);
    if (pIfaceTbl == NULL)
    {
        return DVMRP_NOTOK;
    }
    *pu2VlanId = pIfaceTbl->u2VlanId;
    return DVMRP_OK;
}
