/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpsnmp.c,v 1.11 2014/12/18 12:12:25 siva Exp $
 *
 * Description:This file contains the source code for the SNMP  
 *             Low Level Routines                              
 *
 *******************************************************************/
#include "dpinc.h"
#include "fssocket.h"

/*            
 * GLOBAL DECLARATIONS 
 */

UINT1               au1Version[5];
INT4                gi4SnmpReturnFlag = DVMRP_TRUE;
INT4                gi4GetFirstFlag = DVMRP_TRUE;
UINT4               gu4DvmrpGlobStatus = DVMRP_STATUS_DISABLED;

/***************************************************************************** */
/* Function Name: DvmrpTestStatus                                              */
/*                                                                             */
/* Description    : This function checks if the given value is valid           */
/* Input(s)    : The value whose validity must be checked                      */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpTestStatus (UINT1 u1Value)
#else
INT4
DvmrpTestStatus (u1Value)
     UINT1               u1Value;
#endif
{
    if ((u1Value == DVMRP_ENABLED) || (u1Value == DVMRP_DISABLED))
    {
        LOG1 ((INFO, MOD_SNMP, "Value within Range"));
        return (SNMP_SUCCESS);
    }

    LOG1 ((INFO, MOD_SNMP, "Value Out Of Range"));
    return (SNMP_FAILURE);
}

/*                Functions for Scalar Values                                  */
/***************************************************************************** */
/* Function Name: DvmrpSetStatus                                               */
/*                                                                             */
/* Description    : This function sets DvmrpStatus to the value specified      */
/* Input(s)    : The value to be assigned to dvmrpStatus                       */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpSetStatus (UINT1 u1Status)
#else
INT4
DvmrpSetStatus (u1Status)
     UINT1               u1Status;
#endif
{
    tNetIpMcRegInfo     NetIpMcRegInfo;
    if (DvmrpTestStatus (u1Status) == SNMP_SUCCESS)
    {
        if (u1Status == DVMRP_ENABLED)
        {
            if (gu1DvmrpStatus == DVMRP_ENABLED)
            {
                LOG1 ((WARNING, MOD_SNMP, "Dvmrp already up"));
                return (SNMP_SUCCESS);
            }
            if (DvmrpIhmHandleStartup () == DVMRP_NOTOK)
            {
                LOG1 ((CRITICAL, MAIN,
                       "Failed in function DvmrpIhmHandleStartup"));
                return SNMP_FAILURE;
            }

            /* Register for Multicast data packets from Linux IP */
            MEMSET (&NetIpMcRegInfo, 0, sizeof (tNetIpMcRegInfo));
            NetIpMcRegInfo.u4ProtocolId = DVMRP_ID;
            NetIpMcRegInfo.pDataPktRcv = DvmrpProcessMcastDataPkt;
            NetIpv4RegisterMcPacket (&NetIpMcRegInfo);
#ifdef FS_NPAPI
            /* Set the IPMC bit in hardware */
            if (IpmcFsDvmrpNpInitHw () == FNP_FAILURE)
            {
                LOG1 ((CRITICAL, MAIN, "Failed to sartup router in Hardware"));
                DvmrpIhmFreeDataStructures ();
                DvmrpIhmHandleShutdown ();
                return SNMP_FAILURE;
            }
#endif
            DvmrpMfwdHandleEnabling ();
#ifdef MFWD_WANTED
            gDpConfigParams.u1MfwdStatus = MFWD_STATUS_ENABLED;
#endif
            gu4DvmrpGlobStatus = DVMRP_STATUS_ENABLED;
            LOG1 ((INFO, MOD_SNMP, "Startup of Router"));
        }
        else if (u1Status == DVMRP_DISABLED)
        {
            if (gu1DvmrpStatus == DVMRP_DISABLED)
            {
                LOG1 ((WARNING, MOD_SNMP, "Dvmrp is already down"));
                return (SNMP_SUCCESS);
            }

            /* Deregister for Multicast data packets from Linux IP */
            NetIpv4DeRegisterMcPacket ((UINT4) DVMRP_ID);
#ifdef FS_NPAPI
            if (IpmcFsDvmrpNpDeInitHw () == FNP_FAILURE)
            {
                LOG1 ((CRITICAL, MOD_SNMP,
                       "Failed to Shutdown Router in Hardware"));
                return SNMP_FAILURE;
            }
#endif
            DvmrpMfwdHandleDisabling ();
#ifdef MFWD_WANTED
            gDpConfigParams.u1MfwdStatus = MFWD_STATUS_DISABLED;
#endif
            gu4DvmrpGlobStatus = DVMRP_STATUS_DISABLED;
            DvmrpIhmHandleShutdown ();

            LOG1 ((CRITICAL, MOD_SNMP, "Shutdown of Router"));
        }
    }
    return (SNMP_SUCCESS);
}

/***************************************************************************** */
/* Function Name: DvmrpGetStatus                                               */
/*                                                                             */
/* Description    :Returns the value of the object dvmrpStatus                 */
/* Input(s)    : None                                                          */
/* Output(s)    : None                                                         */
/* Returns    : The value of the object dvmrpStatus                            */
/***************************************************************************** */
INT4
DvmrpGetStatus (void)
{
    LOG2 ((INFO, MOD_SNMP, "DvmrpStatus : ", gu1DvmrpStatus));
    return ((INT4) gu1DvmrpStatus);
}

/***************************************************************************** */
/* Function Name: DvmrpTestLogEnable                                           */
/*                                                                             */
/* Description    : This function checks the validity of the value given       */
/* Input(s)    : The value whose value must be checked                         */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */

#ifdef __STDC__
INT4
DvmrpTestLogEnabled (UINT1 u1Value)
#else
INT4
DvmrpTestLogEnabled (u1Value)
     UINT1               u1Value;
#endif
{
    if ((u1Value <= 5) && (u1Value >= 1))
    {
        LOG1 ((INFO, MOD_SNMP, "Value within range"));
        return (SNMP_SUCCESS);
    }
    else
    {
        LOG1 ((WARNING, MOD_SNMP, "Value out of range"));
        return (SNMP_FAILURE);
    }
}

/***************************************************************************** */
/* Function Name: DvmrpTestLogMask (UINT4 u4Value)                             */
/*                                                                             */
/* Description    : This function checks the validity of the value given       */
/* Input(s)    : The value whose value must be checked (valid range is 10      */
/*                  bits set                                                   */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */

#ifdef __STDC__
INT4
DvmrpTestLogMask (UINT4 u4Value)
#else
INT4
DvmrpTestLogMask (u4Value)
     UINT4               u4Value;
#endif
{
    if ((u4Value < 1024) && (u4Value > 0))
    {
        LOG1 ((INFO, MOD_SNMP, "value within range"));
        return (SNMP_SUCCESS);
    }
    else
    {
        LOG1 ((WARNING, MOD_SNMP, "value out of range"));
        return (SNMP_FAILURE);
    }
}

/***************************************************************************** */
/* Function Name: DvmrpTestMaxSrcs (UINT4 u4Value)                             */
/*                                                                             */
/* Description    : This function checks the validity of the value given       */
/* Input(s)    : The value whose value must be checked                         */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpTestMaxSrcs (UINT4 u4Value)
#else
INT4
DvmrpTestMaxSrcs (u4Value)
     UINT4               u4Value;
#endif
{
    if ((u4Value <= 100) && (u4Value > 0))
    {
        LOG1 ((INFO, MOD_SNMP, "value within range"));
        return (SNMP_SUCCESS);
    }
    else
    {
        LOG1 ((WARNING, MOD_SNMP, "value out of range"));
        return (SNMP_FAILURE);
    }
}

/***************************************************************************** */
/* Function Name: DvmrpTestPruneLifeTime (UINT4 u4Value)                     */
/*                                                                             */
/* Description    : This function checks the validity of the value given       */
/* Input(s)    : The value whose value must be checked                         */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpTestPruneLifeTime (UINT4 u4Value)
#else
INT4
DvmrpTestPruneLifeTime (u4Value)
     UINT4               u4Value;
#endif
{
    if ((u4Value <= 7200) && (u4Value > 0))
    {
        LOG1 ((INFO, MOD_SNMP, "value within range"));
        return (SNMP_SUCCESS);
    }
    else
    {
        LOG1 ((WARNING, MOD_SNMP, "value out of range"));
        return (SNMP_FAILURE);
    }
}

/*****************************************************************************   */
/* Function Name: DvmrpSetLogMask (UINT4 u4Value)                                */
/*                                                                               */
/* Description    : This function assigns value given to the object dvmrpLogMask */
/* Input(s)    : The value                                                       */
/* Output(s)    : None                                                           */
/* Returns    : SNMP_SUCCESS                                                     */
/*                or SNMP_FAILURE                                                */
/*****************************************************************************   */

#ifdef __STDC__
INT4
DvmrpSetLogMask (UINT4 u4Value)
#else
INT4
DvmrpSetLogMask (u4Value)
     UINT4               u4Value;
#endif
{
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " Dvmrp disabled. Cannot set!"));
        return (SNMP_FAILURE);
    }
    else
    {
        gu4DvmrpLogMask = u4Value;
        LOG1 ((INFO, MOD_SNMP, " set successful!"));
        return (SNMP_SUCCESS);
    }
}

/***************************************************************************** */
/* Function Name: DvmrpSetLogEnabled                                           */
/*                                                                             */
/* Description    : This function assigns value to object dvmrpLogEnabled      */
/* Input(s)    : The value of LogEnabled 1, 2, 3 or 4                          */
/* Output(s)    : None                                                         */
/* Returns    : SNMP_SUCCESS                                                   */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */

#ifdef __STDC__
INT4
DvmrpSetLogEnabled (UINT1 u1Value)
#else
INT4
DvmrpSetLogEnabled (u1Value)
     UINT1               u1Value;
#endif
{
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " Dvmrp disabled. Cannot set!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DvmrpTestLogEnabled (u1Value) == SNMP_FAILURE)
        {
            LOG1 ((INFO, MOD_SNMP, " set unsuccessful!"));
            return (SNMP_FAILURE);
        }
        else
        {
            gu1DvmrpLogEnabled = u1Value;
            LOG1 ((INFO, MOD_SNMP, " set successful!"));
            return (SNMP_SUCCESS);
        }
    }
}

/***************************************************************************** */
/* Function Name: DvmrpGetLogEnabled                                           */
/*                                                                             */
/* Description    : This function returns value of the object dvmrpLogEnabled  */
/* Input(s)    : None                                                          */
/* Output(s)    : None                                                         */
/* Returns    : value of the object dvmrpLogEnabled                            */
/***************************************************************************** */
UINT1
DvmrpGetLogEnabled (void)
{
    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        LOG2 ((INFO, MOD_SNMP, "DvmrpLogEnabled: ", gu1DvmrpLogEnabled));
        return (gu1DvmrpLogEnabled);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetLogMask                                             */
/*                                                                            */
/* Description    : This function returns value of the object dvmrpLogMask      */
/* Input(s)    : None                                                        */
/* Output(s)    : None                                                        */
/* Returns    : value of the object dvmrpLogMask                            */
/******************************************************************************/
UINT4
DvmrpGetLogMask (void)
{
    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        LOG2 ((INFO, MOD_SNMP, "DvmrpLogMask : ", gu4DvmrpLogMask));
        return ((INT4) gu4DvmrpLogMask);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetVersionString                                       */
/*                                                                            */
/* Description    : This function returns the value of the object               */
/*                dvmrpVersionString                                          */
/* Input(s)    : None                                                        */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpVersionString                                 */
/******************************************************************************/
UINT1              *
DvmrpGetVersionString (void)
{

    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        SPRINTF ((CHR1 *) au1Version, "%x.%x", DVMRP_MAJOR_VERSION,
                 DVMRP_MINOR_VERSION);
        LOG1 ((INFO, MOD_SNMP, "Version : "));
        LOG1 ((INFO, MOD_SNMP, (const CHR1 *) au1Version));
        return (au1Version);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (NULL);
}

/******************************************************************************/
/* Function Name: DvmrpGetGenerationId                                        */
/*                                                                            */
/* Description    : This function returns the value of the object               */
/*                 dvmrpGenerationId                                          */
/* Input(s)    : None                                                        */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpGenerationId                                  */
/******************************************************************************/
INT4
DvmrpGetGenerationId (void)
{
    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        LOG2 ((INFO, MOD_SNMP, "Generation Id : ", gu4GenerationId));
        return ((INT4) gu4GenerationId);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetNumRoutes                                           */
/*                                                                            */
/* Description    : This function returns the value  of the object              */
/*                dvmrpNumRoutes                                              */
/* Input(s)    : None                                                        */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNumRoutes                                     */
/******************************************************************************/
INT4
DvmrpGetNumRoutes (void)
{
    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        LOG2 ((INFO, MOD_SNMP, "NumRoutes : ", gu4NumRoutes));
        return ((INT4) gu4NumRoutes);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetReachableRoutes                                     */
/*                                                                            */
/* Description    : This function returns the value  of the object              */
/*                dvmrpReachableRoutes                                        */
/* Input(s)    : None                                                        */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpReachableRoutes                               */
/******************************************************************************/
INT4
DvmrpGetReachableRoutes (void)
{
    if (gu1DvmrpStatus == DVMRP_ENABLED)
    {
        LOG2 ((INFO, MOD_SNMP, "Reachabl Routes : ", gu4NumReachableRoutes));
        return ((INT4) gu4NumReachableRoutes);
    }
    LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
    gi4SnmpReturnFlag = DVMRP_FALSE;
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: TwoIndexCompare                                             */
/*                                                                            */
/* Description    : This function compares the given two groups of two indices. */
/*                                                                            */
/* Input(s)    : Two groups of two indices                                   */
/*                                                                            */
/* Output(s)    : none                                                        */
/*                                                                            */
/* Returns    : VAR1_LT_VAR2 or VAR1_GT_VAR2 or VAR1_EQ_VAR2                      */
/******************************************************************************/
#ifdef __STDC__
INT1
TwoIndexCompare (UINT4 u4RouteSource1, UINT4 u4RouteSource2,
                 UINT4 u4RouteMask1, UINT4 u4RouteMask2)
#else
INT1
TwoIndexCompare (u4RouteSource1, u4RouteSource2, u4RouteMask1, u4RouteMask2)
     UINT4               u4RouteSource1;
     UINT4               u4RouteSource2;
     UINT4               u4RouteMask1;
     UINT4               u4RouteMask2;
#endif
{
    if (u4RouteSource1 > u4RouteSource2)
    {
        return (VAR1_GT_VAR2);
    }
    if (u4RouteSource1 < u4RouteSource2)
    {
        return (VAR1_LT_VAR2);
    }
    if (u4RouteMask1 > u4RouteMask2)
    {
        return (VAR1_GT_VAR2);
    }
    if (u4RouteMask1 < u4RouteMask2)
    {
        return (VAR1_LT_VAR2);
    }
    return (VAR1_EQ_VAR2);
}

/******************************************************************************/
/* Function Name: ThreeIndexCompare                                           */
/*                                                                            */
/* Description    : This function compares two sets of three indices            */
/*                                                                            */
/* Input(s)    : Two sets of three indices                                   */
/*                                                                            */
/* Output(s)    : None                                                        */
/*                                                                            */
/* Returns    : VAR1_LT_VAR2 or VAR1_GT_VAR2 or VAR1_EQ_VAR2                      */
/******************************************************************************/
#ifdef __STDC__
INT1
NeighborIndexCompare (UINT4 u4NbrAddr1, UINT4 u4NbrAddr2)
#else
INT1
NeighborIndexCompare (u4NbrAddr1, u4NbrAddr2)
     UINT4               u4NbrAddr1;
     UINT4               u4NbrAddr2;
#endif
{
    if (u4NbrAddr1 > u4NbrAddr2)
    {
        return (VAR1_GT_VAR2);
    }
    if (u4NbrAddr1 < u4NbrAddr2)
    {
        return (VAR1_LT_VAR2);
    }
    return (VAR1_EQ_VAR2);
}

/*                      Functions for DVMRP ROUTE TABLE                       */
/******************************************************************************/
/* Function Name: DvmrpGetNextRouteIndex                                      */
/*                                                                            */
/* Description    : This function fills the parameters with the index value of  */
/*                the entry next to the value passed to it as parameter       */
/* Input(s)    : Pointers to the indices to the RouteTable                   */
/*                                                                            */
/* Output(s)    : Pointers to the indices of the lexicographically the next   */
/*                entry to the entry whose index is passed as parameter.      */
/* Returns    : SNMP_SUCCESS or SNMP_FAILURE                                */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetNextRouteIndex (UINT4 *pu4RouteAddress, UINT4 *pu4RouteMask)
#else
INT4
DvmrpGetNextRouteIndex (pu4RouteAddress, pu4RouteMask)
     UINT4              *pu4RouteAddress;
     UINT4              *pu4RouteMask;
#endif
{
    UINT4               u4NextRouteAddress = 0;
    UINT4               u4NextRouteMask = 0;
    UINT2               u2Index;
    tRouteTbl          *pRouteTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        gi4SnmpReturnFlag = DVMRP_FALSE;
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_ROUTE_TBL_PID == 0)
        {
            LOG1 ((INFO, MOD_SNMP, " Table Empty"));
            gi4SnmpReturnFlag = DVMRP_FALSE;
            return (SNMP_FAILURE);
        }

        /*
         * This loop initialises the NextRouteAddress and NextRouteMask with the 
         * first eligible entry from the array of entries.
         */

        for (u2Index = 0; u2Index < MAX_ROUTE_TABLE_ENTRIES; u2Index++)
        {
            pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u2Index);
            if (pRouteTbl->u1Status != DVMRP_INACTIVE)
            {
                if (TwoIndexCompare (*pu4RouteAddress,
                                     pRouteTbl->u4SrcNetwork,
                                     *pu4RouteMask,
                                     pRouteTbl->u4SrcMask) == VAR1_LT_VAR2)
                {
                    u4NextRouteAddress = pRouteTbl->u4SrcNetwork;
                    u4NextRouteMask = pRouteTbl->u4SrcMask;
                    LOG1 ((INFO, MOD_SNMP, " Next entry found "));
                    break;
                }
            }
        }

        /* 
         * The for loop traverses the rest of the array 
         * updating the NextRouteAddress and 
         * NextRouteMask values as required.
         */

        for (++u2Index; u2Index < MAX_ROUTE_TABLE_ENTRIES; u2Index++)
        {
            pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u2Index);
            if (pRouteTbl->u1Status != DVMRP_INACTIVE)
            {
                if (TwoIndexCompare (u4NextRouteAddress,
                                     pRouteTbl->u4SrcNetwork,
                                     u4NextRouteMask,
                                     pRouteTbl->u4SrcMask) == VAR1_GT_VAR2)
                {
                    if (TwoIndexCompare (*pu4RouteAddress,
                                         pRouteTbl->u4SrcNetwork,
                                         *pu4RouteMask,
                                         pRouteTbl->u4SrcMask) == VAR1_LT_VAR2)
                    {
                        u4NextRouteAddress = pRouteTbl->u4SrcNetwork;
                        u4NextRouteMask = pRouteTbl->u4SrcMask;
                    }
                }
            }
        }

        if ((u4NextRouteAddress == 0) && (u4NextRouteMask == 0))
        {
            LOG1 ((INFO, MOD_SNMP, " End Of Table "));
            return (SNMP_FAILURE);
        }
        else
        {
            *pu4RouteAddress = u4NextRouteAddress;
            *pu4RouteMask = u4NextRouteMask;
            gi4GetFirstFlag = FALSE;
            LOG2 ((INFO, MOD_SNMP, "Next Route Address : ",
                   u4NextRouteAddress));
            LOG2 ((INFO, MOD_SNMP, "Next Route Mask : ", u4NextRouteMask));
            return (SNMP_SUCCESS);
        }
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstRouteIndex                                     */
/* Description    : This function fills the parameters with the first entry's */
/*                index in the low level data structure .                     */
/* Input(s)    : Pointers to the indices                                      */
/*                dvmrpRouteSource and dvmrpRouteSourceMask                   */
/* Output(s)    : Pointers to the indices of the lexicographically the first  */
/*                entry in the Route table                                    */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                                */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetFirstRouteIndex (UINT4 *pu4RouteSource, UINT4 *pu4RouteMask)
#else
INT4
DvmrpGetFirstRouteIndex (pu4RouteSource, pu4RouteMask)
     UINT4              *pu4RouteSource;
     UINT4              *pu4RouteMask;
#endif
{
    UINT4               u4TempRouteIndex1 = 0;
    UINT4               u4TempRouteIndex2 = 0;

    INT4                i4GetNextStatus;
    if ((i4GetNextStatus = DvmrpGetNextRouteIndex (&u4TempRouteIndex1,
                                                   &u4TempRouteIndex2)) ==
        SNMP_FAILURE)
    {
        *pu4RouteSource = 0;
        *pu4RouteMask = 0;
        LOG1 ((INFO, MOD_SNMP, " Table Empty "));
        /*
         *   No entry in the table
         */
    }
    else if (i4GetNextStatus == SNMP_SUCCESS)
    {
        *pu4RouteSource = u4TempRouteIndex1;
        *pu4RouteMask = u4TempRouteIndex2;
        LOG2 ((INFO, MOD_SNMP, "First Route Address : ", u4TempRouteIndex1));
        LOG2 ((INFO, MOD_SNMP, "First Route Mask : ", u4TempRouteIndex2));
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/***************************************************************************** */
/* Function Name: DvmrpRouteTableEntryExists                                   */
/*                                                                             */
/* Description  : This function will be called during a get operation to check */
/*               if there is an entry corresponding to the index passed as     */
/*               parameter.                                                    */
/*                                                                             */
/* Input(s)    : Indices to the Route Table, RouteSourceAddress and Mask       */
/*                                                                             */
/* Output(s)    : None                                                         */
/*                                                                             */
/* Returns    : SNMP_SUCCESS or SNMP_FAILURE                                   */
/***************************************************************************** */
#ifdef __STDC__
INT1
DvmrpRouteTableEntryExists (UINT4 u4RouteSource, UINT4 u4RouteMask)
#else
INT1
DvmrpRouteTableEntryExists (u4RouteSource, u4RouteMask)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, " Table Empty"));

            return (SNMP_FAILURE);
        }

/******** Hashing used as per review comment ***********/
        DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);

        DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index,
                                pRouteEntry, tRouteTbl *)
        {
            if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
                (pRouteEntry->u4SrcMask == u4RouteMask))
            {
                LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
                return (SNMP_SUCCESS);
            }
        }
        LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
        return (SNMP_FAILURE);
    }
}

/***************************************************************************** */
/* Function Name : DvmrpGetRouteUpstreamNeighbor                               */
/*                                                                             */
/* Description    : This function returns the value of dvmrpUpstreamNeighbor   */
/*                corresponding to indices, from the protocol data structure   */
/*                                                                             */
/* Input(s)    : Indices to the Route Table                                    */
/*                1) dvmrpRouteSource                                          */
/*                2) dvmrpRouteSourceMask                                      */
/* Output(s)    : None                                                         */
/*                                                                             */
/* Returns    : The value of dvmrpRouteUpstreamNeighbor if successful          */
/*                 or SNMP_FAILURE                                             */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetRouteUpstreamNeighbor (UINT4 u4RouteSource, UINT4 u4RouteMask,
                               UINT4 *pUpStreamNbr)
#else
INT4
DvmrpGetRouteUpstreamNeighbor (u4RouteSource, u4RouteMask, pUpStreamNbr)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
     UINT4              *pUpStreamNbr;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
/******** Hashing used as per review comment ***********/
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            LOG2 ((INFO, MOD_SNMP, "UpstreamNeighbor : ",
                   pRouteEntry->u4UpstreamNbr));
            *pUpStreamNbr = (pRouteEntry->u4UpstreamNbr);
            return (SNMP_SUCCESS);
        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/***************************************************************************** */
/* Function Name: DvmrpGetRouteIfIndex                                    */
/*                                                                             */
/* Description  : This function returns the value of the dvmrpRouteConrolIndex */
/*                from the entry corresponding to the indices, in the          */
/*                protocol data structure                                      */
/* Input(s)    : Indices to the Route Table                                    */
/*                1)dvmrpRouteSource                                           */
/*                2)dvmrpRouteSourceMask                                       */
/* Output(s)    : Pointers to the indices of the lexicographically the first   */
/*                entry in the Table                                           */
/* Returns    : value of dvmrpRouteIfIndex if successful                  */
/*                or SNMP_FAILURE                                              */
/***************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetRouteIfIndex (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *pIfIndex)
#else
INT4
DvmrpGetRouteIfIndex (u4RouteSource, u4RouteMask, pIfIndex)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
     INT4               *pIfIndex;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
/******** Hashing used as per review comment ***********/
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            LOG2 ((INFO, MOD_SNMP, "Interface Index : ",
                   pRouteEntry->u4IfaceIndex));
            *pIfIndex = pRouteEntry->u4IfaceIndex;
            return (SNMP_SUCCESS);
        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteMetric                                         */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the data structure         */
/* Input(s)    : Indices to the Route Table                                  */
/*                1)dvmrpRouteSource                                          */
/*                2)dvmrpRouteSourceMask                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteMetric  if successful or SNMP_FAILURE    */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetRouteMetric (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *pi4Metric)
#else
INT4
DvmrpGetRouteMetric (u4RouteSource, u4RouteMask)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
     INT4               *pi4Metric;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;

/******** Hashing used as per review comment ***********/
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            LOG2 ((INFO, MOD_SNMP, "Metric : ", pRouteEntry->u1Metric));
            *pi4Metric = (INT4) pRouteEntry->u1Metric;
            return (SNMP_SUCCESS);
        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteExpiryTime                                     */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the data structure         */
/* Input(s)    : Indices to the Route Table                                  */
/*                1)dvmrpRouteSource                                          */
/*                2)dvmrpRouteSourceMask                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteExpiryTime if successful or SNMP_FAILURE */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetRouteExpiryTime (UINT4 u4RouteSource, UINT4 u4RouteMask,
                         UINT4 *pExpTime)
#else
INT4
DvmrpGetRouteExpiryTime (u4RouteSource, u4RouteMask, pExpTime)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
     UINT4              *pExpTime;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;

/******** Hashing used as per review comment ***********/
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            LOG2 ((INFO, MOD_SNMP, "Expiry Time : ",
                   pRouteEntry->i4ExpiryTime));
            *pExpTime = pRouteEntry->i4ExpiryTime;
            return (SNMP_SUCCESS);
        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteUpTime                                         */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the data structure         */
/* Input(s)    : Indices to the Route Table                                  */
/*                1)dvmrpRouteSource                                          */
/*                2)dvmrpRouteSourceMask                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteUpTime  if successful or SNMP_FAILURE    */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetRouteUpTime (UINT4 u4RouteSource, UINT4 u4RouteMask, UINT4 *u4UpTime)
#else
INT4
DvmrpGetRouteUpTime (u4RouteSource, u4RouteMask)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
     UINT4               u4UpTime;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;

/******** Hashing used as per review comment ***********/
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            OsixGetSysTime (u4UpTime);
            *u4UpTime = *u4UpTime - pRouteEntry->u4UpTime;
            LOG2 ((INFO, MOD_SNMP, "Up time : ", *u4UpTime));
            return (SNMP_SUCCESS);

        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteStatus                                         */
/*                                                                            */
/* Description    : This function returns the value from the entry            */
/*                corresponding to the indices, in the data structure         */
/* Input(s)    : Indices to the Route Table                                   */
/*                1)dvmrpRouteSource                                          */
/*                2)dvmrpRouteSourceMask                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteStatus  if successful or SNMP_FAILURE      */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetRouteStatus (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *pi4Status)
#else
INT4
DvmrpGetRouteStatus (u4RouteSource, u4RouteMask, *pi4Status)
     UINT4               u4RouteSource;
     UINT4               u4RouteMask;
#endif
{
    UINT1               u1Index = 0;
    tRouteTbl          *pRouteEntry;

/******** Hashing used as per review comment ***********/
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_ROUTE_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }
    DVMRP_GET_HASH_INDEX (u4RouteSource, u4RouteMask, u1Index);
    DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Index, pRouteEntry, tRouteTbl *)
    {
        if ((pRouteEntry->u4SrcNetwork == u4RouteSource) &&
            (pRouteEntry->u4SrcMask == u4RouteMask))
        {
            LOG1 ((INFO, MOD_SNMP, " Active Entry Found "));
            LOG2 ((INFO, MOD_SNMP, "Status : ", pRouteEntry->u1Status));
            *pi4Status = (INT4) pRouteEntry->u1Status;
            return (SNMP_SUCCESS);

        }
    }
    LOG1 ((INFO, MOD_SNMP, " Entry non-existent "));
    return (SNMP_FAILURE);
}

/*                   Functions for dvmrpInterfaceTable                        */
/******************************************************************************/
/* Function Name: DvmrpInterfaceTableEntryExists                              */
/*                                                                            */
/* Description    : This function checks if there is an entry corresponding to  */
/*                the indices, passed as parameter                            */
/*                                                                            */
/* Input(s)    : Indices to the Interface Table                              */
/*                1)dvmrpInterfaceControlIndex                                */
/*                2)dvmrpInterfacePortNum                                     */
/* Output(s)    : None                                                        */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpInterfaceTableEntryExists (UINT4 u4IfIndex)
#else
INT4
DvmrpInterfaceTableEntryExists (u4IfIndex)
     UINT4               u4IfIndex;
#endif
{
    tDvmrpInterfaceNode *pIfNode = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
        if ((pIfNode != NULL) && (pIfNode->u4IfIndex == u4IfIndex))
        {
            LOG1 ((INFO, MOD_SNMP, "Entry Found "));
            return (SNMP_SUCCESS);
        }
    }
    LOG1 ((INFO, MOD_SNMP, "Entry Not Found "));
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstInterfaceIndex                                 */
/*                                                                            */
/* Description    : This function fills the index parameters with the first     */
/*                entry's index in the low level data structure               */
/* Input(s)    : Pointers to Indices to the Interface Table                  */
/*                1)dvmrpInterfaceControlIndex                                */
/*                2)dvmrpInterfacePortNum                                     */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/
#ifdef __STDC__
INT4
DvmrpGetFirstInterfaceIndex (UINT4 *pu4InterfaceIfIndex)
#else
INT4
DvmrpGetFirstInterfaceIndex (pu4InterfaceIfIndex)
     UINT4              *pu4InterfaceIfIndex;
#endif
{
    tDvmrpInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    pIfGetNextLink = TMO_SLL_First (&gDvmrpIfInfo.IfGetNextList);

    if (pIfGetNextLink != NULL)
    {
        pIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode, IfGetNextLink,
                                      pIfGetNextLink);
        *pu4InterfaceIfIndex = pIfNode->u4IfIndex;
        LOG2 ((INFO, MOD_SNMP, "First Idx : ", pIfNode->u4IfIndex));
        return (SNMP_SUCCESS);
    }

    LOG1 ((INFO, MOD_SNMP, " Table Empty "));
    return (SNMP_FAILURE);
}

/*                Functions for NeighborTable                                 */
/******************************************************************************/
/* Function Name: DvmrpGetNextNeighborIndex                                   */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Interface Table                              */
/*                1)dvmrpInterfaceControlIndex                                */
/*                2)dvmrpInterfacePortNum                                     */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpInterfaceLocalAddress if successful           */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNextNeighborIndex (UINT4 *pu4Address)
#else
INT4
DvmrpGetNextNeighborIndex (pu4Address)
     UINT4              *pu4Address;
#endif
{
    UINT4               u4NextAddress = 0;
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }
    else
    {
        /*
         * The for loop initialises the NextAddress with 
         * the first eligible entry from the array of entries. 
         */
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {

                if (NeighborIndexCompare (*pu4Address,
                                          pNbrTbl->u4NbrIpAddress)
                    == VAR1_LT_VAR2)
                {
                    u4NextAddress = pNbrTbl->u4NbrIpAddress;
                    LOG1 ((INFO, MOD_SNMP, "Next Entry Found "));
                    break;
                }
            }
        }
        /* 
         * The for loop traverses the rest of the array updating the 
         * NextAddress values as required.
         */
        pNbrTbl = NULL;
        for (++u2Index; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (NeighborIndexCompare (u4NextAddress,
                                          pNbrTbl->
                                          u4NbrIpAddress) == VAR1_GT_VAR2)
                {
                    if (NeighborIndexCompare (*pu4Address,
                                              pNbrTbl->
                                              u4NbrIpAddress) == VAR1_LT_VAR2)
                    {
                        u4NextAddress = pNbrTbl->u4NbrIpAddress;
                    }
                }
            }
        }
        if (u4NextAddress == 0)
        {
            LOG1 ((INFO, MOD_SNMP, "End of Table  "));
            return (SNMP_FAILURE);
        }
        else
        {
            *pu4Address = u4NextAddress;
            LOG2 ((INFO, MOD_SNMP, "Next Address : ", u4NextAddress));
            return (SNMP_SUCCESS);
        }
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstNeighborIndex                                  */
/*                                                                            */
/* Description    : This function returns the value of the index of the         */
/*                first entry in the dvmrp Interface Table                    */
/*                  data structure                                            */
/* Input(s)    : Indices to the Interface Table                              */
/*                1)dvmrpInterfaceControlIndex                                */
/*                2)dvmrpInterfacePortNum                                     */
/* Output(s)    : None                                                        */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetFirstNeighborIndex (UINT4 *pu4Address)
#else
INT4
DvmrpGetFirstNeighborIndex (pu4Address)
     UINT4              *pu4Address;
#endif
{
    UINT4               u4TempAddress = 0;
    INT4                i4GetNextStatus;

    if ((i4GetNextStatus =
         DvmrpGetNextNeighborIndex (&u4TempAddress)) == SNMP_FAILURE)
    {
        /*
         *   No entry in the table
         */
        pu4Address = (UINT4 *) NULL;
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
    }
    else if (i4GetNextStatus == SNMP_SUCCESS)
    {
        *pu4Address = u4TempAddress;
        LOG2 ((INFO, MOD_SNMP, "First Address : ", u4TempAddress));
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpNeighborTableEntryExists                               */
/*                                                                            */
/* Description    : This function chechks the existence of an entry             */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Interface Table                              */
/*                1)dvmrpInterfaceControlIndex                                */
/*                2)dvmrpInterfacePortNum                                     */
/* Output(s)    : None                                                        */
/* Returns    : SNMP SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpNeighborTableEntryExists (UINT4 u4Address)
#else
INT4
DvmrpNeighborTableEntryExists (u4Address)
     UINT4               u4Address;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG1 ((INFO, MOD_SNMP, " Match Found "));
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "No Match Found  "));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborIfIndex                                     */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborUpTime if successful                  */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborIfIndex (UINT4 u4Address, INT4 *pi4Index)
#else
INT4
DvmrpGetNeighborIfIndex (u4Address, pi4Index)
     UINT4               u4Address;
     INT4               *pi4Index;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Nbr If Index : ",
                           pNbrTbl->u4NbrIfIndex));
                    *pi4Index = pNbrTbl->u4NbrIfIndex;
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborUpTime                                      */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborUpTime if successful                  */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborUpTime (UINT4 u4Address, UINT4 *pu4UpTime)
#else
INT4
DvmrpGetNeighborUpTime (u4Address, pu4UpTime)
     UINT4               u4Address;
     UINT4              *pu4UpTime;
#endif
{
    UINT2               u2Index;
    UINT4               u4UpTime;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    OsixGetSysTime (&u4UpTime);
                    *pu4UpTime = u4UpTime - pNbrTbl->u4NbrUpTime;
                    LOG2 ((INFO, MOD_SNMP, "Up Time : ", *pu4UpTime));
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborExpiryTime                                  */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborExpiryTime if successful              */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborExpiryTime (UINT4 u4Address, UINT4 *pu4ExpTime)
#else
INT4
DvmrpGetNeighborExpiryTime (u4Address, pu4ExpTime)
     UINT4               u4Address;
     UINT4              *pu4ExpTime;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Expiry Time : ",
                           pNbrTbl->u4ExpiryTime));
                    *pu4ExpTime = (INT4) (pNbrTbl->u4ExpiryTime);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborGenerationId                                */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborGenerationId if successful            */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborGenerationId (UINT4 u4Address, INT4 *pi4GenId)
#else
INT4
DvmrpGetNeighborGenerationId (u4Address, pi4GenId)
     UINT4               u4Address;
     INT4               *pi4GenId;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Gen Id : ", pNbrTbl->u4NbrGenId));
                    *pi4GenId = (INT4) (pNbrTbl->u4NbrGenId);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborMajorVersion                                */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborMajorVersion if successful            */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborMajorVersion (UINT4 u4Address, INT4 *pi4MajVersion)
#else
INT4
DvmrpGetNeighborMajorVersion (u4Address, pi4MajVersion)
     UINT4               u4Address;
     INT4               *pi4MajVersion;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Maj Ver : ",
                           pNbrTbl->u1NbrMajorVer));
                    *pi4MajVersion = (INT4) (pNbrTbl->u1NbrMajorVer);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborMinorVer                                    */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborMinorVer  if successful               */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborMinorVer (UINT4 u4Address, INT4 *pi4MinVersion)
#else
INT4
DvmrpGetNeighborMinorVer (u4Address, pi4MinVersion)
     UINT4               u4Address;
     INT4               *pi4MinVersion;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Min Ver : ",
                           pNbrTbl->u1NbrMinorVer));
                    *pi4MinVersion = (INT4) (pNbrTbl->u1NbrMinorVer);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborCapabilities                                */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborCapabilities if successful            */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborCapabilities (UINT4 u4Address, INT4 *pi4NbrCap)
#else
INT4
DvmrpGetNeighborCapabilities (u4Address)
     UINT4               u4Address;
     INT4               *pi4NbrCap;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Capabilities : ",
                           pNbrTbl->u1NbrCapabilities));
                    *pi4NbrCap = (INT4) (pNbrTbl->u1NbrCapabilities);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborRcvRoutes                                   */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborRcvRoutes if successful               */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborRcvRoutes (UINT4 u4Address, UINT4 *pu4RcvRoutes)
#else
INT4
DvmrpGetNeighborRcvRoutes (u4Address, pu4RcvRoutes)
     UINT4               u4Address;
     UINT4              *pu4RcvRoutes;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Rcv Routes : ",
                           pNbrTbl->u4NbrRcvRoutes));
                    *pu4RcvRoutes = (INT4) (pNbrTbl->u4NbrRcvRoutes);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborRcvBadRoutes                                */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborRcvBadRoutes if successful            */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborRcvBadRoutes (UINT4 u4Address, UINT4 *pu4BadRoutes)
#else
INT4
DvmrpGetNeighborRcvBadRoutes (u4Address, pu4BadRoutes)
     UINT4               u4Address;
     UINT4              *pu4BadRoutes;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Rcv Bad Routes : ",
                           pNbrTbl->u4NbrRcvBadRoutes));
                    *pu4BadRoutes = (INT4) (pNbrTbl->u4NbrRcvBadRoutes);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }

}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborAdjFlag                                */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborRcvBadRoutes if successful            */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborAdjFlag (UINT4 u4Address, UINT4 *pi4NbrAdjFlag)
#else
INT4
DvmrpGetNeighborAdjFlag (u4Address, pi4NbrAdjFlag)
     UINT4               u4Address;
     UINT4              *pi4NbrAdjFlag;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "Nbr Adjacency Flag : ",
                           pNbrTbl->u1SeenByNbrFlag));
                    *pi4NbrAdjFlag = (INT4) (pNbrTbl->u1SeenByNbrFlag);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNeighborRcvBadPkts                                  */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpNeighborControlIndex                                 */
/*                2)dvmrpNeighborPortNum                                      */
/*                3)dvmrpNeighborAddress                                      */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpNeighborRcvBadPkts if successful              */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNeighborRcvBadPkts (UINT4 u4Address, UINT4 *pu4BadPkts)
#else
INT4
DvmrpGetNeighborRcvBadPkts (u4Address, pu4BadPkts)
     UINT4               u4Address;
     UINT4              *pu4BadPkts;
#endif
{
    UINT2               u2Index;
    tNbrTbl            *pNbrTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NBR_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u2Index = 0; u2Index < MAX_NBR_TABLE_ENTRIES; u2Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u2Index);
            if (pNbrTbl->u1Status == DVMRP_ACTIVE)
            {
                if (pNbrTbl->u4NbrIpAddress == u4Address)
                {
                    LOG2 ((INFO, MOD_SNMP, "bad pkts : ",
                           pNbrTbl->u4NbrRcvBadPkts));
                    *pu4BadPkts = (INT4) (pNbrTbl->u4NbrRcvBadPkts);
                    return (SNMP_SUCCESS);
                }
            }
        }
        LOG1 ((INFO, MOD_SNMP, "Entry NonExistent"));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: FwdIndexCompare                                            */
/*                                                                            */
/* Description    : This function compares two sets of four indices             */
/* Input(s)    : Two sets of the four indices to the RouteNextHopTable       */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopIfIndex                                 */
/* Output(s)    : None                                                        */
/* Returns    : VAR1_LT_VAR2 or VAR1_GT_VAR2 or VAR1_EQ_VAR2                      */
/******************************************************************************/

#ifdef __STDC__
INT1
FwdIndexCompare (tFwdIndices * pIndex1, tFwdIndices * pIndex2)
#else
INT1
FwdIndexCompare (pIndex1, pIndex2)
     tFwdIndices        *pIndex1;
     tFwdIndices        *pIndex2;
#endif
{
    if (pIndex1->u4SrcNw > pIndex2->u4SrcNw)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4SrcNw < pIndex2->u4SrcNw)
    {
        return (VAR1_LT_VAR2);
    }
    if (pIndex1->u4Mask > pIndex2->u4Mask)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4Mask < pIndex2->u4Mask)
    {
        return (VAR1_LT_VAR2);
    }
    if (pIndex1->u4IfIndex > pIndex2->u4IfIndex)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4IfIndex < pIndex2->u4IfIndex)
    {
        return (VAR1_LT_VAR2);
    }
    if (pIndex1->u4NbrAddr > pIndex2->u4NbrAddr)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4NbrAddr < pIndex2->u4NbrAddr)
    {
        return (VAR1_LT_VAR2);
    }

    return (VAR1_EQ_VAR2);
}

/******************************************************************************/
/* Function Name: FwdIndexCopy                                                   */
/*                                                                            */
/* Description    : This function copies the values of one Index                */
/*                  struct to another                                         */
/* Input(s)    : Pointers to two structures                                  */
/* Output(s)    : None                                                        */
/* Returns    : None                                                        */
/******************************************************************************/

#ifdef __STDC__
void
FwdIndexCopy (tFwdIndices * pIndex1, tFwdIndices * pIndex2)
#else
void
FwdIndexCopy (pIndex1, pIndex2)
     tFwdIndices        *pIndex1;
     tFwdIndices        *pIndex2;
#endif
{
    pIndex2->u4SrcNw = pIndex1->u4SrcNw;
    pIndex2->u4Mask = pIndex1->u4Mask;
    pIndex2->u4IfIndex = pIndex1->u4IfIndex;
    pIndex2->u4NbrAddr = pIndex1->u4NbrAddr;
}

/*                     Functions for dvmrpRouteNextHopTable                   */
/******************************************************************************/
/* Function Name: NextHopIndexCompare                                            */
/*                                                                            */
/* Description    : This function compares two sets of four indices             */
/* Input(s)    : Two sets of the four indices to the RouteNextHopTable       */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopIfIndex                                 */
/* Output(s)    : None                                                        */
/* Returns    : VAR1_LT_VAR2 or VAR1_GT_VAR2 or VAR1_EQ_VAR2                      */
/******************************************************************************/

#ifdef __STDC__
INT1
NextHopIndexCompare (tIndices * pIndex1, tIndices * pIndex2)
#else
INT1
NextHopIndexCompare (pIndex1, pIndex2)
     tIndices           *pIndex1;
     tIndices           *pIndex2;
#endif
{
    if (pIndex1->u4Address > pIndex2->u4Address)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4Address < pIndex2->u4Address)
    {
        return (VAR1_LT_VAR2);
    }
    if (pIndex1->u4Mask > pIndex2->u4Mask)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4Mask < pIndex2->u4Mask)
    {
        return (VAR1_LT_VAR2);
    }
    if (pIndex1->u4IfIndex > pIndex2->u4IfIndex)
    {
        return (VAR1_GT_VAR2);
    }
    if (pIndex1->u4IfIndex < pIndex2->u4IfIndex)
    {
        return (VAR1_LT_VAR2);
    }
    return (VAR1_EQ_VAR2);
}

/******************************************************************************/
/* Function Name: IndexCopy                                                   */
/*                                                                            */
/* Description    : This function copies the values of one Index                */
/*                  struct to another                                         */
/* Input(s)    : Pointers to two structures                                  */
/* Output(s)    : None                                                        */
/* Returns    : None                                                        */
/******************************************************************************/

#ifdef __STDC__
void
IndexCopy (tIndices * pIndex1, tIndices * pIndex2)
#else
void
IndexCopy (pIndex1, pIndex2)
     tIndices           *pIndex1;
     tIndices           *pIndex2;
#endif
{
    pIndex2->u4Address = pIndex1->u4Address;
    pIndex2->u4Mask = pIndex1->u4Mask;
    pIndex2->u4IfIndex = pIndex1->u4IfIndex;
}

/******************************************************************************/
/* Function Name: DvmrpRouteNextHopTableEntryExists                           */
/*                                                                            */
/* Description    : This function checks if there is an entry                   */
/*                corresponding to the indices passed as parameter            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : None                                                        */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpRouteNextHopTableEntryExists (UINT4 u4Address, UINT4 u4Mask,
                                   UINT4 u4IfIndex)
#else
INT4
DvmrpRouteNextHopTableEntryExists (u4Address, u4Mask, u4IfIndex)
     UINT4               u4Address;
     UINT4               u4Mask;
     UINT4               u4IfIndex;
#endif
{
    UINT1               u1Index;
    tNextHopType       *pNextHopType = NULL;
    tNextHopTbl        *pNextHopTbl = NULL;

/* 
 * the for loop checks for an active entry that matches Address and Mask
 */
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NEXT_HOP_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u1Index = 0; u1Index < MAX_NEXT_HOP_TABLE_ENTRIES; u1Index++)
        {
            pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1Index);
            if ((pNextHopTbl->u4NextHopSrc == u4Address) &&
                (pNextHopTbl->u4NextHopSrcMask == u4Mask))
            {
                if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
                {
                    pNextHopType = pNextHopTbl->pNextHopType;
                    break;
                }
                else
                {
                    LOG1 ((INFO, MOD_SNMP, "No Match Found  "));
                    return (SNMP_FAILURE);
                }
            }
        }
        while (pNextHopType != NULL)
        {
            if (pNextHopType->u4NHopInterface == u4IfIndex)
            {
                LOG1 ((INFO, MOD_SNMP, " Match Found  "));
                return (SNMP_SUCCESS);
            }
            pNextHopType = pNextHopType->pNextHopTypeNext;
        }
        LOG1 ((INFO, MOD_SNMP, "No Match Found  "));
        return (SNMP_FAILURE);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteNextHopType                                    */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteNextHopType if successful                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetRouteNextHopType (UINT4 u4Address, UINT4 u4Mask,
                          UINT4 u4IfIndex, INT4 *pi4Type)
#else
INT4
DvmrpGetRouteNextHopType (u4Address, u4Mask, u4IfIndex, pi4Type)
     UINT4               u4Address;
     UINT4               u4Mask;
     UINT4               u4IfIndex;
     INT4               *pi4Type;
#endif
{
    UINT1               u1NxtHpIndex;
    tNextHopType       *pNextHopType = NULL;
    tNextHopTbl        *pNextHopTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_NEXT_HOP_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    /*
     * The for loop gets the entry with the matching Address and Mask
     */

    for (u1NxtHpIndex = 0; u1NxtHpIndex < MAX_NEXT_HOP_TABLE_ENTRIES;
         u1NxtHpIndex++)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1NxtHpIndex);
        if ((pNextHopTbl->u4NextHopSrc == u4Address) &&
            (pNextHopTbl->u4NextHopSrcMask == u4Mask))
        {
            if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
            {
                pNextHopType = pNextHopTbl->pNextHopType;
                break;
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
    }
    /* 
     * the while loop finds the entry with matching interface index 
     */
    while (pNextHopType != NULL)
    {
        if (pNextHopType->u4NHopInterface == u4IfIndex)
        {
            LOG2 ((INFO, MOD_SNMP, "Type : ", pNextHopType->u1InType));
            *pi4Type = (INT4) (pNextHopType->u1InType);
            return (SNMP_SUCCESS);
        }
        pNextHopType = pNextHopType->pNextHopTypeNext;
    }
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteNextHopDesigFowr                               */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteNextHopDesiForw if successful              */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetRouteNextHopDesigFowr (UINT4 u4Address, UINT4 u4Mask,
                               UINT4 u4IfIndex, INT4 *pi4Flag)
#else
INT4
DvmrpGetRouteNextHopDesigFowr (u4Address, u4Mask, u4IfIndex, pi4Flag)
     UINT4               u4Address;
     UINT4               u4Mask;
     UINT4               u4IfIndex;
     INT4               *pi4Flag;
#endif
{
    UINT1               u1NxtHpIndex;
    tNextHopType       *pNextHopType = NULL;
    tNextHopTbl        *pNextHopTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_NEXT_HOP_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    /*
     * The for loop gets the entry with the matching Address and Mask
     */

    for (u1NxtHpIndex = 0; u1NxtHpIndex < MAX_NEXT_HOP_TABLE_ENTRIES;
         u1NxtHpIndex++)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1NxtHpIndex);
        if ((pNextHopTbl->u4NextHopSrc == u4Address) &&
            (pNextHopTbl->u4NextHopSrcMask == u4Mask))
        {
            if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
            {
                pNextHopType = pNextHopTbl->pNextHopType;
                break;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    /* 
     * the while loop finds the entry with matching interface index 
     */
    while (pNextHopType != NULL)
    {
        if (pNextHopType->u4NHopInterface == u4IfIndex)
        {
            LOG2 ((INFO, MOD_SNMP, "Designated Forwarder Flag : ",
                   pNextHopType->u1DesigForwFlag));
            *pi4Flag = (INT4) (pNextHopType->u1DesigForwFlag);
            return (SNMP_SUCCESS);
        }
        pNextHopType = pNextHopType->pNextHopTypeNext;
    }
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetRouteNextHopDepNbrs                               */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopIndex                             */
/*                4)dvmrpRouteNextHopDepNbrs                                  */
/* Output(s)    : None                                                        */
/* Returns    : value of dvmrpRouteNextHopDepNbrs if successful              */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetRouteNextHopDepNbrs (UINT4 u4Address, UINT4 u4Mask,
                             UINT4 u4IfIndex,
                             tSNMP_OCTET_STRING_TYPE * pDepNbrs)
#else
INT4
DvmrpGetRouteNextHopDepNbrs (u4Address, u4Mask, u4IfIndex, pDepNbrs)
     UINT4               u4Address;
     UINT4               u4Mask;
     UINT4               u4IfIndex;
     tSNMP_OCTET_STRING_TYPE *pDepNbrs;
#endif
{
    UINT1               u1NxtHpIndex;
    UINT1               au1DepNbrs[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4NbrCnt = 0;
    CHR1                ai1address[SNMP_MAX_INDICES + 1];
    tNbrAddrList       *pNbr = NULL;
    tNextHopType       *pNextHopType = NULL;
    tNextHopTbl        *pNextHopTbl = NULL;

    MEMSET (ai1address, 0, sizeof (ai1address));
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    if (DVMRP_NEXT_HOP_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    /*
     * The for loop gets the entry with the matching Address and Mask
     */

    for (u1NxtHpIndex = 0; u1NxtHpIndex < MAX_NEXT_HOP_TABLE_ENTRIES;
         u1NxtHpIndex++)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1NxtHpIndex);
        if ((pNextHopTbl->u1Status == DVMRP_ACTIVE) &&
            (pNextHopTbl->u4NextHopSrc == u4Address) &&
            (pNextHopTbl->u4NextHopSrcMask == u4Mask))
        {
            pNextHopType = pNextHopTbl->pNextHopType;
            break;
        }
    }
    /* 
     * the while loop finds the entry with matching interface index 
     */
    while (pNextHopType != NULL)
    {
        if (pNextHopType->u4NHopInterface == u4IfIndex)
        {
            pNbr = pNextHopType->pDependentNbrs;
            while (pNbr != NULL)
            {
                DVMRP_CONVERT_IPADDR_TO_STR1 (ai1address, pNbr->u4NbrAddr);
                i4NbrCnt +=
                    SNPRINTF ((CHR1 *) (au1DepNbrs + i4NbrCnt),
                              SNMP_MAX_OCTETSTRING_SIZE, "%s ",
                              (CHR1 *) ai1address);
                pNbr = pNbr->pNbr;
                if (pNbr != NULL)
                {
                    i4NbrCnt +=
                        SPRINTF ((CHR1 *) (au1DepNbrs + i4NbrCnt), ", ");
                }
                pDepNbrs->i4_Length = i4NbrCnt;
            }
            if (i4NbrCnt == 0)
            {
                pDepNbrs->pu1_OctetList[0] = '\0';
                pDepNbrs->i4_Length = 1;
            }
            else
            {
                STRCPY (pDepNbrs->pu1_OctetList, au1DepNbrs);
            }
            return (SNMP_SUCCESS);
        }
        pNextHopType = pNextHopType->pNextHopTypeNext;
    }
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpGetNextRouteNextHopIndex                               */
/*                                                                            */
/* Description    : This function fills the index parameters with the index of  */
/*                the entry next to the value passed as parameter             */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : the index of the entry, next to the entry whose index is    */
/*                 passed as parameters                                       */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNextRouteNextHopIndex (UINT4 *pu4Address, UINT4 *pu4Mask,
                               UINT4 *pu4IfIndex)
#else
INT4
DvmrpGetNextRouteNextHopIndex (pu4Address, pu4Mask, pu4IfIndex)
     UINT4              *pu4Address;
     UINT4              *pu4Mask;
     UINT4              *pu4IfIndex;
#endif
{
    UINT1               u1Index;
    UINT1               u1Flag = DVMRP_FALSE;
    tIndices            Index;
    tIndices            TempIndex;
    tIndices            NextIndex;
    tNextHopType       *pNextHopType;
    tNextHopTbl        *pNextHopTbl = NULL;

    NextIndex.u4Address = 0;
    NextIndex.u4Mask = 0;
    NextIndex.u4IfIndex = 0;

    Index.u4Address = *pu4Address;
    Index.u4Mask = *pu4Mask;
    Index.u4IfIndex = *pu4IfIndex;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_NEXT_HOP_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u1Index = 0; u1Index < MAX_NEXT_HOP_TABLE_ENTRIES; u1Index++)
        {
            pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1Index);
            pNextHopType = pNextHopTbl->pNextHopType;
            if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
            {
                while (pNextHopType != NULL)
                {
                    TempIndex.u4Address = pNextHopTbl->u4NextHopSrc;
                    TempIndex.u4Mask = pNextHopTbl->u4NextHopSrcMask;
                    TempIndex.u4IfIndex = pNextHopType->u4NHopInterface;
                    if (NextHopIndexCompare (&Index, &TempIndex) ==
                        VAR1_LT_VAR2)
                    {
                        IndexCopy (&TempIndex, &NextIndex);
                        u1Flag = DVMRP_TRUE;
                        break;
                    }
                    pNextHopType = pNextHopType->pNextHopTypeNext;
                }
            }
            if (u1Flag == DVMRP_TRUE)
            {
                u1Flag = DVMRP_FALSE;
                break;
            }
        }

        for (++u1Index; u1Index < MAX_NEXT_HOP_TABLE_ENTRIES; u1Index++)
        {
            pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1Index);
            pNextHopType = pNextHopTbl->pNextHopType;
            if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
            {
                while (pNextHopType != NULL)
                {
                    TempIndex.u4Address = pNextHopTbl->u4NextHopSrc;
                    TempIndex.u4Mask = pNextHopTbl->u4NextHopSrcMask;
                    TempIndex.u4IfIndex = pNextHopType->u4NHopInterface;
                    if ((NextHopIndexCompare (&NextIndex, &TempIndex) ==
                         VAR1_GT_VAR2)
                        && (NextHopIndexCompare (&Index, &TempIndex) ==
                            VAR1_LT_VAR2))
                    {
                        IndexCopy (&TempIndex, &NextIndex);
                    }
                    pNextHopType = pNextHopType->pNextHopTypeNext;
                }
            }
        }
        if ((NextIndex.u4Address == 0) && (NextIndex.u4Mask == 0) &&
            (NextIndex.u4IfIndex == 0))
        {
            LOG1 ((INFO, MOD_SNMP, " End Of Table "));
            return (SNMP_FAILURE);
        }
        else
        {
            *pu4Address = NextIndex.u4Address;
            *pu4Mask = NextIndex.u4Mask;
            *pu4IfIndex = NextIndex.u4IfIndex;
            LOG2 ((INFO, MOD_SNMP, "Next Addr: ", NextIndex.u4Address));
            LOG2 ((INFO, MOD_SNMP, "Next Mask: ", NextIndex.u4Mask));
            LOG2 ((INFO, MOD_SNMP, "Next Interface Idx: ",
                   NextIndex.u4IfIndex));

            return (SNMP_SUCCESS);
        }
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstRouteNextHopIndex                              */
/*                                                                            */
/* Description    : This function returns the value from the entry              */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                               */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : Pointer to the index of the lexicographically first entry   */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetFirstRouteNextHopIndex (UINT4 *pu4Address, UINT4 *pu4Mask,
                                UINT4 *pu4IfIndex)
#else
INT4
DvmrpGetFirstRouteNextHopIndex (pu4Address, pu4Mask, pu4IfIndex)
     UINT4              *pu4Address;
     UINT4              *pu4Mask;
     UINT4              *pu4IfIndex;
#endif
{
    UINT4               u4TempAddress = 0;
    UINT4               u4TempMask = 0;
    UINT4               u4TempIfIndex = 0;

    if (DvmrpGetNextRouteNextHopIndex (&u4TempAddress,
                                       &u4TempMask,
                                       &u4TempIfIndex) == SNMP_FAILURE)
    {
        pu4Address = pu4Mask = pu4IfIndex = (UINT4 *) 0;
        LOG1 ((INFO, MOD_SNMP, " Table Empty "));
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4Address = u4TempAddress;
        *pu4Mask = u4TempMask;
        *pu4IfIndex = u4TempIfIndex;
        LOG2 ((INFO, MOD_SNMP, "First Addr: ", *pu4Address));
        LOG2 ((INFO, MOD_SNMP, "First Mask: ", *pu4Mask));
        LOG2 ((INFO, MOD_SNMP, "First Idx: ", *pu4IfIndex));
        return (SNMP_SUCCESS);
    }
}

/******************************************************************************/
/* Function Name: DvmrpForwardTableEntryExists                                */
/*                                                                            */
/* Description    : This function checks if there is an entry                 */
/*                corresponding to the indices passed as parameter            */
/* Input(s)    : Indices to the Forward Table                                */
/*                1)dvmrpSourceNetwork                                       */
/*                2)dvmrpSourceMask                                          */
/* Output(s)    : None                                                        */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpForwardTableEntryExists (UINT4 u4SrcNetwork, UINT4 u4GrpAddress)
#else
INT4
DvmrpForwardTableEntryExists (UINT4 u4SrcNetwork, UINT4 u4GrpAddress)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
#endif
{
    UINT1               u1Index;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
            if (pForwTbl->u1Status == DVMRP_INACTIVE)
            {
                continue;
            }
            if ((pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
                (pForwTbl->u4GroupAddr == u4GrpAddress))
            {
                return (SNMP_SUCCESS);
            }
        }
        return SNMP_FAILURE;
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstForwardIndex                                   */
/*                                                                            */
/* Description    : This function returns the value from the entry            */
/*                  corresponding to the indices, in the protocol             */
/*                  data structure                                            */
/* Input(s)       : Indices to the Neighbor Table                             */
/*                  1)dvmrpSourceNetwork                                      */
/*                  2)dvmrpGroupAddress                                       */
/* Output(s)    : Pointer to the index of the lexicographically first entry   */
/* Returns      : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetFirstForwardIndex (UINT4 *pu4SrcNetwork, UINT4 *pu4GrpAddress)
#else
INT4
DvmrpGetFirstForwardIndex (pu4SrcNetwork, pu4GrpAddress)
     UINT4              *pu4SrcNetwork;
     UINT4              *pu4GrpAddress;
#endif
{
    UINT4               u4TempSrcNetwork = 0;
    UINT4               u4TempGrpAddress = 0;

    if (DvmrpGetNextForwardIndex (&u4TempSrcNetwork,
                                  &u4TempGrpAddress) == SNMP_FAILURE)
    {
        *pu4SrcNetwork = *pu4GrpAddress = 0;
        LOG1 ((INFO, MOD_SNMP, " Table Empty "));
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4SrcNetwork = u4TempSrcNetwork;
        *pu4GrpAddress = u4TempGrpAddress;
        LOG2 ((INFO, MOD_SNMP, "First Source Addr: ", *pu4SrcNetwork));
        LOG2 ((INFO, MOD_SNMP, "First Group  Addr: ", *pu4GrpAddress));
        return (SNMP_SUCCESS);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNextForwardIndex                                    */
/* Description  : This function fills the index parameters with the index of  */
/*                the entry next to the value passed as parameter             */
/* Input(s)     : Indices to the Neighbor Table                               */
/*                1)dvmrpSrcAddress                                           */
/*                2)dvmrpGroupAddress                                         */
/* Output(s)    : the index of the entry, next to the entry whose index is    */
/*                 passed as parameters                                       */
/* Returns    : SNMP_SUCCESS                                                  */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNextForwardIndex (UINT4 *pu4SrcNetwork, UINT4 *pu4GrpAddress)
#else
INT4
DvmrpGetNextForwardIndex (pu4SrcNetwork, pu4GrpAddress)
     UINT4              *pu4SrcNetwork;
     UINT4              *pu4GrpAddress;
#endif
{
    UINT4               u4NextSrcNetwork = 0;
    UINT4               u4NextGrpAddress = 0;
    UINT2               u2Index;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_FWD_TBL_PID == 0)
        {
            LOG1 ((INFO, MOD_SNMP, " Table Empty"));
            return (SNMP_FAILURE);
        }

        /*
         * This loop initialises the NextSrcAddress and NextGrpAddress with the 
         * first eligible entry from the array of entries.
         */

        for (u2Index = 0; u2Index < MAX_FORW_TABLE_ENTRIES; u2Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u2Index);
            if (pForwTbl->u1Status != DVMRP_INACTIVE)
            {
                if (TwoIndexCompare (*pu4SrcNetwork,
                                     pForwTbl->u4SrcNetwork,
                                     *pu4GrpAddress,
                                     pForwTbl->u4GroupAddr) == VAR1_LT_VAR2)
                {
                    u4NextSrcNetwork = pForwTbl->u4SrcNetwork;
                    u4NextGrpAddress = pForwTbl->u4GroupAddr;
                    LOG1 ((INFO, MOD_SNMP, " Next entry found "));
                    break;
                }
            }
        }

        /* 
         * The for loop traverses the rest of the array 
         * updating the NextRouteAddress and 
         * NextRouteMask values as required.
         */

        for (++u2Index; u2Index < MAX_FORW_TABLE_ENTRIES; u2Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u2Index);
            if (pForwTbl->u1Status != DVMRP_INACTIVE)
            {
                if (TwoIndexCompare (u4NextSrcNetwork,
                                     pForwTbl->u4SrcNetwork,
                                     u4NextGrpAddress,
                                     pForwTbl->u4GroupAddr) == VAR1_GT_VAR2)
                {
                    if (TwoIndexCompare (*pu4SrcNetwork,
                                         pForwTbl->u4SrcNetwork,
                                         *pu4GrpAddress,
                                         pForwTbl->u4GroupAddr) == VAR1_LT_VAR2)
                    {
                        u4NextSrcNetwork = pForwTbl->u4SrcNetwork;
                        u4NextGrpAddress = pForwTbl->u4GroupAddr;
                    }
                }
            }
        }

        if ((u4NextSrcNetwork == 0) && (u4NextGrpAddress == 0))
        {
            LOG1 ((INFO, MOD_SNMP, " End Of Table "));
            return (SNMP_FAILURE);
        }
        else
        {
            *pu4SrcNetwork = u4NextSrcNetwork;
            *pu4GrpAddress = u4NextGrpAddress;
            gi4GetFirstFlag = FALSE;
            LOG2 ((INFO, MOD_SNMP, "Next Source Address : ", u4NextSrcNetwork));
            LOG2 ((INFO, MOD_SNMP, "Next Group Address : ", u4NextGrpAddress));
            return (SNMP_SUCCESS);
        }
    }
}

/******************************************************************************/
/* Function Name : DvmrpGetForwardUpstreamNeighbor                            */
/* Description   : This function returns the value of dvmrpUpstreamNeighbor   */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpUpstreamNeighbor if successful           */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardUpstreamNeighbor (UINT4 u4SrcNetwork,
                                 UINT4 u4GrpAddress, UINT4 *pUpStreamNbr)
#else
INT4
DvmrpGetForwardUpstreamNeighbor (u4SrcNetwork, u4GrpAddress, pUpStreamNbr)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4              *pUpStreamNbr;
#endif
{
    UINT1               u1Index = 0;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            LOG2 ((INFO, MOD_SNMP, "Upstream Neighbor : ",
                   pForwTbl->u4UpNeighbor));
            *pUpStreamNbr = (INT4) (pForwTbl->u4UpNeighbor);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/******************************************************************************/
/* Function Name : DvmrpGetForwardExpiryTime                                  */
/* Description   : This function returns the value of dvmrpUpstreamNeighbor   */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpForwardExpiryTime if successful          */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardExpiryTime (UINT4 u4SrcNetwork,
                           UINT4 u4GrpAddress, UINT4 *pExpTime)
#else
INT4
DvmrpGetForwardExpiryTime (u4SrcNetwork, u4GrpAddress, pExpTime)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4              *pExpTime;
#endif
{
    UINT1               u1Index = 0;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            LOG2 ((INFO, MOD_SNMP, "Expiry Time : ", pForwTbl->i4ExpiryTime));
            *pExpTime = (INT4) (pForwTbl->i4ExpiryTime);

            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/******************************************************************************/
/* Function Name : DvmrpGetForwardTblStatus                                   */
/* Description   : This function returns the value of dvmrpUpstreamNeighbor   */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpForwardTblStatus if successful           */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardTblStatus (UINT4 u4SrcNetwork,
                          UINT4 u4GrpAddress, INT4 *pTblStatus)
#else
INT4
DvmrpGetForwardTblStatus (u4SrcNetwork, u4GrpAddress, pTblStatus)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4              *pTblStatus;
#endif
{
    UINT1               u1Index = 0;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            LOG2 ((INFO, MOD_SNMP, "Expiry Time : ", pForwTbl->u1Status));
            *pTblStatus = (INT4) (pForwTbl->u1Status);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/******************************************************************************/
/* Function Name :DvmrpGetForwardInIfIndex                                   */
/* Description   : This function returns the value of dvmrp Incoming Index    */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpForwardTblStatus if successful           */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardInIfIndex (UINT4 u4SrcNetwork,
                          UINT4 u4GrpAddress, INT4 *pInIfIndex)
#else
INT4
DvmrpGetForwardInIfIndex (u4SrcNetwork, u4GrpAddress, pInIfIndex)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4              *pInIfIndex;
#endif
{
    UINT1               u1Index = 0;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            LOG2 ((INFO, MOD_SNMP, "RPF Interface : ",
                   pForwTbl->InIface.u4InInterface));

            *pInIfIndex = (INT4) (pForwTbl->InIface.u4InInterface);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/******************************************************************************/
/* Function Name :DvmrpGetForwardInIfState                                   */
/* Description   : This function returns the value of dvmrp IIf State         */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpForwardTblStatus if successful           */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardInIfState (UINT4 u4SrcNetwork,
                          UINT4 u4GrpAddress, INT4 *pInIfState)
#else
INT4
DvmrpGetForwardInIfIndex (u4SrcNetwork, u4GrpAddress, pInIfState)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4              *pInIfState;
#endif
{
    UINT1               u1Index = 0;
    tForwTbl           *pForwTbl = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, "Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            LOG2 ((INFO, MOD_SNMP, "Rpf Interface State : ",
                   pForwTbl->InIface.u1State));

            *pInIfState = (INT4) (pForwTbl->InIface.u1State);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/******************************************************************************/
/* Function Name: DvmrpForwardTableEntryExists                                */
/*                                                                            */
/* Description    : This function checks if there is an entry                 */
/*                corresponding to the indices passed as parameter            */
/* Input(s)    : Indices to the Forward Table                                */
/*                1)dvmrpSourceNetwork                                       */
/*                2)dvmrpSourceMask                                          */
/* Output(s)    : None                                                        */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpForwardPruneNbrTableEntryExists (UINT4 u4SrcNetwork,
                                      UINT4 u4GrpAddress,
                                      UINT4 u4IfIndex, UINT4 u4NbrAddr)
#else
INT4
DvmrpForwardPruneNbrTableEntryExists (u4SrcNetwork,
                                      u4GrpAddress, u4IfIndex, u4NbrAddr)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4               u4IfIndex;
     UINT4               u4NbrAddr;
#endif
{
    UINT1               u1Index;
    tForwTbl           *pForwTbl = NULL;
    tOutputInterface   *pOutputInterface = NULL;
    tNbrPruneList      *pDepNbrs = NULL;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
            if (pForwTbl->u1Status == DVMRP_INACTIVE)
            {
                continue;
            }
            if ((pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
                (pForwTbl->u4GroupAddr == u4GrpAddress))
            {
                pOutputInterface = pForwTbl->pOutInterface;
                while (pOutputInterface != NULL)
                {
                    if (pOutputInterface->u4OutInterface == u4IfIndex)
                    {
                        pDepNbrs = pOutputInterface->pDepNbrs;
                        while (pDepNbrs != NULL)
                        {
                            if (pDepNbrs->u4NbrAddr == u4NbrAddr)
                            {
                                return (SNMP_SUCCESS);
                            }
                            pDepNbrs = pDepNbrs->pNbr;
                        }
                    }
                    pOutputInterface = pOutputInterface->pNext;
                }
            }
        }
        return SNMP_FAILURE;
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetFirstForwardPruneNbrIndex                           */
/*                                                                            */
/* Description    : This function returns the value from the entry            */
/*                corresponding to the indices, in the protocol               */
/*                  data structure                                            */
/* Input(s)    : Indices to the Neighbor Table                                */
/*                1)dvmrpRouteNextHopSource                                   */
/*                2)dvmrpRouteNextHopMask                                     */
/*                3)dvmrpRouteNextHopControlIndex                             */
/*                4)dvmrpRouteNextHopPortNum                                  */
/* Output(s)    : Pointer to the index of the lexicographically first entry   */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetFirstForwardPruneNbrIndex (UINT4 *pu4SourceNetwork,
                                   UINT4 *pu4GroupAddress,
                                   UINT4 *pu4IfIndex, UINT4 *pu4NbrAddr)
#else
INT4
DvmrpGetFirstForwardPruneNbrIndex (pu4SourceNetwork, pu4GroupAddress,
                                   pu4IfIndex, pu4NbrAddr)
     UINT4              *pu4SourceNetwork;
     UINT4              *pu4GroupAddress;
     UINT4              *pu4IfIndex;
     UINT4              *pu4NbrAddr;
#endif
{
    UINT4               u4TempNetwork = 0;
    UINT4               u4TempGroup = 0;
    UINT4               u4TempIfIndex = 0;
    UINT4               u4TempNbrAddr = 0;

    if (DvmrpGetNextForwardPruneNbrIndex (&u4TempNetwork,
                                          &u4TempGroup,
                                          &u4TempIfIndex,
                                          &u4TempNbrAddr) == SNMP_FAILURE)
    {
        pu4SourceNetwork = pu4GroupAddress = pu4IfIndex = pu4NbrAddr =
            (UINT4 *) 0;
        LOG1 ((INFO, MOD_SNMP, " Table Empty "));
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4SourceNetwork = u4TempNetwork;
        *pu4GroupAddress = u4TempGroup;
        *pu4IfIndex = u4TempIfIndex;
        *pu4NbrAddr = u4TempNbrAddr;
        LOG2 ((INFO, MOD_SNMP, "First Network Addr: ", *pu4SourceNetwork));
        LOG2 ((INFO, MOD_SNMP, "First Gropup Addr: ", *pu4GroupAddress));
        LOG2 ((INFO, MOD_SNMP, "First Idx: ", *pu4IfIndex));
        LOG2 ((INFO, MOD_SNMP, "First Prune Nbr: ", *pu4NbrAddr));
        return (SNMP_SUCCESS);
    }
}

/******************************************************************************/
/* Function Name: DvmrpGetNextForwardPruneNbrIndex                            */
/*                                                                            */
/* Description    : This function fills the index parameters with the index of*/
/*                the entry next to the value passed as parameter             */
/* Input(s)    : Indices to the Neighbor Tab le                               */
/*                1)dvmrpSourceNetwork                                        */
/*                2)dvmrpGroupAddress                                         */
/*                3)dvmrpIfIndex                                              */
/* Output(s)    : the index of the entry, next to the entry whose index is    */
/*                 passed as parameters                                       */
/* Returns    : SNMP_SUCCESS                                                */
/*                or SNMP_FAILURE                                             */
/******************************************************************************/

#ifdef __STDC__
INT4
DvmrpGetNextForwardPruneNbrIndex (UINT4 *pu4Network, UINT4 *pu4Group,
                                  UINT4 *pu4IfIndex, UINT4 *pu4NbrAddr)
#else
INT4
DvmrpGetNextForwardPruneNbrIndex (pu4Network, pu4Group, pu4IfIndex, pu4NbrAddr)
     UINT4              *pu4Network;
     UINT4              *pu4Group;
     UINT4              *pu4IfIndex;
     UINT4              *pu4NbrAddr;
#endif
{

    UINT1               u1Index;
    UINT1               u1Flag = DVMRP_FALSE;
    tFwdIndices         Index;
    tFwdIndices         TempIndex;
    tFwdIndices         NextIndex;
    tForwTbl           *pForwTbl = NULL;
    tOutputInterface   *pOutputInterface = NULL;
    tNbrPruneList      *pDepNbrs = NULL;

    NextIndex.u4SrcNw = 0;
    NextIndex.u4Mask = 0;
    NextIndex.u4IfIndex = 0;
    NextIndex.u4NbrAddr = 0;

    Index.u4SrcNw = *pu4Network;
    Index.u4Mask = *pu4Group;
    Index.u4IfIndex = *pu4IfIndex;
    Index.u4NbrAddr = *pu4NbrAddr;

    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }
    else
    {
        if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
        {
            LOG1 ((INFO, MOD_SNMP, "Table Empty"));
            return (SNMP_FAILURE);
        }
        for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
            pOutputInterface = pForwTbl->pOutInterface;

            if (pForwTbl->u1Status == DVMRP_ACTIVE)
            {
                while (pOutputInterface != NULL)
                {
                    TempIndex.u4SrcNw = pForwTbl->u4SrcNetwork;
                    TempIndex.u4Mask = pForwTbl->u4GroupAddr;
                    TempIndex.u4IfIndex = pOutputInterface->u4OutInterface;
                    pDepNbrs = pOutputInterface->pDepNbrs;
                    while (pDepNbrs != NULL)
                    {
                        TempIndex.u4NbrAddr = pDepNbrs->u4NbrAddr;
                        if (FwdIndexCompare (&Index, &TempIndex) ==
                            VAR1_LT_VAR2)
                        {
                            FwdIndexCopy (&TempIndex, &NextIndex);
                            u1Flag = DVMRP_TRUE;
                            if (pDepNbrs->pNbr == NULL)
                            {
                                LOG1 ((INFO, MOD_SNMP, "Next Entry Found  "));
                                break;
                            }
                        }
                        pDepNbrs = pDepNbrs->pNbr;
                    }
                    if ((pOutputInterface->pNext == NULL)
                        || (u1Flag == DVMRP_TRUE))
                    {
                        LOG1 ((INFO, MOD_SNMP, "Next Entry Found"));
                        break;
                    }
                    pOutputInterface = pOutputInterface->pNext;
                }
            }
            if (u1Flag == DVMRP_TRUE)
            {
                u1Flag = DVMRP_FALSE;
                break;
            }
        }

        for (++u1Index; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
        {
            pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
            pOutputInterface = pForwTbl->pOutInterface;
            if (pForwTbl->u1Status == DVMRP_ACTIVE)
            {
                while (pOutputInterface != NULL)
                {
                    TempIndex.u4SrcNw = pForwTbl->u4SrcNetwork;
                    TempIndex.u4Mask = pForwTbl->u4GroupAddr;
                    TempIndex.u4IfIndex = pOutputInterface->u4OutInterface;
                    pDepNbrs = pOutputInterface->pDepNbrs;
                    while (pDepNbrs != NULL)
                    {
                        TempIndex.u4NbrAddr = pDepNbrs->u4NbrAddr;
                        if ((FwdIndexCompare (&NextIndex, &TempIndex) ==
                             VAR1_GT_VAR2)
                            && (FwdIndexCompare (&Index, &TempIndex) ==
                                VAR1_LT_VAR2))
                        {
                            FwdIndexCopy (&TempIndex, &NextIndex);
                        }
                        pDepNbrs = pDepNbrs->pNbr;
                    }
                    pOutputInterface = pOutputInterface->pNext;
                }
            }
        }
        if ((NextIndex.u4SrcNw == 0) && (NextIndex.u4Mask == 0) &&
            (NextIndex.u4IfIndex == 0) && (NextIndex.u4NbrAddr == 0))
        {
            LOG1 ((INFO, MOD_SNMP, " End Of Table "));
            return (SNMP_FAILURE);
        }
        else
        {
            *pu4Network = NextIndex.u4SrcNw;
            *pu4Group = NextIndex.u4Mask;
            *pu4IfIndex = NextIndex.u4IfIndex;
            *pu4NbrAddr = NextIndex.u4NbrAddr;
            return (SNMP_SUCCESS);
        }
    }
}

/******************************************************************************/
/* Function Name : DvmrpGetForwardNbrPruneTime                                */
/* Description   : This function returns the value of dvmrpUpstreamNeighbor   */
/*                 corresponding to indices, from the protocol data structure */
/* Input(s)      : Indices to the Route Table                                 */
/*                1) dvmrpSourceNetwork                                       */
/*                2) dvmrpGroupAddress                                        */
/*                3) dvmrpIfIndex                                             */
/* Output(s)     : None                                                       */
/*                                                                            */
/* Returns       : The value of dvmrpUpstreamNeighbor if successful           */
/*                 or SNMP_FAILURE                                            */
/**************************************************************************** */
#ifdef __STDC__
INT4
DvmrpGetForwardNbrPruneTime (UINT4 u4SrcNetwork, UINT4 u4GrpAddress,
                             UINT4 u4IfIndex, UINT4 u4NbrAddr,
                             UINT4 *pu4PruneTime)
#else
INT4
DvmrpGetForwardNbrPruneTime (u4SrcNetwork, u4GrpAddress, u4IfIndex,
                             u4NbrAddr, pu4PruneTime)
     UINT4               u4SrcNetwork;
     UINT4               u4GrpAddress;
     UINT4               u4IfIndex;
     UINT4               u4NbrAddr;
     UINT4              *pu4PruneTime;
#endif
{
    UINT1               u1Index;
    tOutputInterface   *pOutputInterface = NULL;
    tForwTbl           *pForwTbl = NULL;
    tNbrPruneList      *pPruneNbr = NULL;

    /* 
     * the for loop checks for an active entry that matches Address and Mask
     */
    if (gu1DvmrpStatus != DVMRP_ENABLED)
    {
        LOG1 ((INFO, MOD_SNMP, " DVMRP is down!"));
        return (SNMP_FAILURE);
    }

    if (DVMRP_FWD_TBL_PID == DVMRP_ZERO)
    {
        LOG1 ((INFO, MOD_SNMP, " Table Empty"));
        return (SNMP_FAILURE);
    }

    for (u1Index = 0; u1Index < MAX_FORW_TABLE_ENTRIES; u1Index++)
    {
        pForwTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u1Index);
        if ((pForwTbl->u1Status == DVMRP_ACTIVE) &&
            (pForwTbl->u4SrcNetwork == u4SrcNetwork) &&
            (pForwTbl->u4GroupAddr == u4GrpAddress))
        {
            pOutputInterface = pForwTbl->pOutInterface;
            break;
        }
    }

    while (pOutputInterface != NULL)
    {
        if (pOutputInterface->u4OutInterface == u4IfIndex)
        {
            pPruneNbr = pOutputInterface->pDepNbrs;
            while (pPruneNbr != NULL)
            {
                if (pPruneNbr->u4NbrAddr == u4NbrAddr)
                {
                    LOG2 ((INFO, MOD_SNMP, "Prune Life Time : ",
                           pPruneNbr->u4PruneLifeTime));
                    *pu4PruneTime = (INT4) (pPruneNbr->u4PruneLifeTime);
                    return (SNMP_SUCCESS);
                }
                pPruneNbr = pPruneNbr->pNbr;
            }
        }
        pOutputInterface = pOutputInterface->pNext;
    }
    LOG1 ((INFO, MOD_SNMP, "No Match Found  "));
    return (SNMP_FAILURE);
}
