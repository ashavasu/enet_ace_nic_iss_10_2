/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dvmrplnx.c,v 1.9 2012/12/05 15:10:10 siva Exp $ 
 *
 * Description:This file holds the functions to for poting DVMRP on linux
 *             IP.
 *
 *******************************************************************/

#include "dpinc.h"
#include "fssocket.h"

#ifdef BSDCOMP_SLI_WANTED
#include "chrdev.h"
#endif

#define DVMRP_MDP_STACK_SIZE    OSIX_DEFAULT_STACK_SIZE
#define DVMRP_MDP_TASK_PRIORITY 60
#define DVMRP_MDP_MAX_BYTES     1600
#define DVMRP_MDP_TASK_NAME     (UINT1*)"DMDP"

#ifdef NP_KERNEL_WANTED
INT4                gi4DvmrpDevFd = -1;
#endif
tOsixTaskId         gu4DvmrpMdpTaskId;
/***************************************************************************
 * Function Name    : DvmrpCreateSocket 
 *
 * Description      :  This function creates socket for receiving DVMRP control
 *                     packets.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         : gDpConfigParams.i4DvmrpSockId - created socket id is 
 *                    stored.
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK, if socket creation is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpCreateSocket (VOID)
{
    INT4                i4SocketId = 0;

    i4SocketId = socket (AF_INET, SOCK_RAW, IPPROTO_IGMP);

    if (i4SocketId < 0)
    {
        LOG1 ((CRITICAL, IHM, "DVMRP Socket creation - FAILED \n"));
        return DVMRP_NOTOK;
    }
    gDpConfigParams.i4DvmrpSockId = i4SocketId;

    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpJoinMcastGroup 
 *
 * Description      :  This function is used to join the standard DVMRP 
 *                     multicast group on a interface to receive the DVMRP 
 *                     control packets.
 *
 * Global Variables
 * Referred         : gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK, if setsockopt is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpJoinMcastGroup (UINT4 u4IfAddr)
{
    struct ip_mreq      mreq;

    MEMSET (&mreq, 0, sizeof (mreq));
    mreq.imr_multiaddr.s_addr = OSIX_HTONL (ALL_DVMRP_ROUTERS);
    mreq.imr_interface.s_addr = OSIX_HTONL (u4IfAddr);

    if (setsockopt (gDpConfigParams.i4DvmrpSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
    {
        LOG1 ((CRITICAL, IHM,
               "Unable to Join DVMRP group 224.0.0.13: setsockopt failed\n"));
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpLeaveMcastGroup
 *
 * Description      :  This function is used to leave the standard DVMRP 
 *                     multicast group on a interfacei. which will block 
 *                     DVMRP control packet reception on the given interface. 
 *
 * Global Variables
 * Referred         : gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK, if setsockopt is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpLeaveMcastGroup (UINT4 u4IfAddr)
{
    struct ip_mreq      mreq;

    MEMSET (&mreq, 0, sizeof (mreq));
    mreq.imr_multiaddr.s_addr = OSIX_HTONL (ALL_DVMRP_ROUTERS);
    mreq.imr_interface.s_addr = OSIX_HTONL (u4IfAddr);

    if (setsockopt (gDpConfigParams.i4DvmrpSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
    {
        LOG1 ((CRITICAL, IHM,
               "Unable to leave DVMRP group 224.0.0.13: setsockopt failed\n"));
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpSetSocketOption
 *
 * Description      :  This function is used to set the default socket option
 *                     for opened DVMRP socket. 
 *
 * Global Variables
 * Referred         : gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u4IdAddr - Interface address on which to join for 
 *                                Multicast address.
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK, if setsockopt is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpSetSocketOption (VOID)
{
    INT4                i4Option = 0;
    UINT1               u1Option = 0;

    if ((fcntl (gDpConfigParams.i4DvmrpSockId, F_SETFL, O_NONBLOCK)) < 0)
    {
        LOG1 ((CRITICAL, IHM, "Unable set socket NON-BLOCKING \n"));
        return DVMRP_NOTOK;
    }
    u1Option = TRUE;
    if (setsockopt (gDpConfigParams.i4DvmrpSockId, IPPROTO_IP,
                    IP_MULTICAST_TTL, &u1Option, sizeof (u1Option)) < 0)
    {
        LOG1 ((CRITICAL, IHM,
               "Unable to set IP_MULTICAST_TTL: socket option failed\n"));
        return DVMRP_NOTOK;
    }

    if (setsockopt (gDpConfigParams.i4DvmrpSockId, IPPROTO_IP,
                    IP_MULTICAST_LOOP, &i4Option, sizeof (i4Option)) < 0)
    {
        LOG1 ((CRITICAL, IHM,
               "Unable to set IP_MULTICAST_LOOP: socket option failed\n"));
        return DVMRP_NOTOK;
    }
    /* using this socket option we can use recvmsg to receive required packet 
     * with additional packet information, In this case this option is used to
     * get packet interface information, to get the interface index on which 
     * the packet has received, see DvmrpHandlePacketArrivalEvent */
    if (setsockopt (gDpConfigParams.i4DvmrpSockId, IPPROTO_IP, IP_PKTINFO,
                    &u1Option, sizeof (u1Option)) < 0)
    {
        LOG1 ((CRITICAL, IHM,
               "Unable to set IP_PKTINFO: socket option failed\n"));
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpCloseSocket
 *
 * Description      :  This function is used to close the socket for opened 
 *                     DVMRP socket.  
 *                     
 * Global Variables
 * Referred         :  gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         :  gDpConfigParams.i4DvmrpSockId 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
DvmrpCloseSocket (VOID)
{
    close (gDpConfigParams.i4DvmrpSockId);
    gDpConfigParams.i4DvmrpSockId = -1;
}

/***************************************************************************
 * Function Name    :  DvmrpNotifyPktArrivalEvent
 *
 * Description      :  This function is called from Select utility, used 
 *                     send an packet arrival indication to DVMRP, this function
 *                     will not receive any packet, just gives an indication.
 *                     This function will be called from Select utility 
 *                     context.  
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  i4Sock - currently not used 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
DvmrpNotifyPktArrivalEvent (INT4 i4Sock)
{
    UNUSED_PARAM (i4Sock);
    /* Send a PACKET_ARRIVAL_EVENT to DVMRP */
    if (OsixEvtSend (gu4DvmrpTaskId,
                     DVMRP_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, IHM, "Sending a event from IP to DVMRP - FAILED \n"));
        return;
    }
}

/***************************************************************************
 * Function Name    :  DvmrpRegisterForMDP 
 *
 * Description      :  This function spawns the task to receive multicast 
 *                     data packets,
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

INT4
DvmrpRegisterForMDP (VOID)
{
#ifdef NP_KERNEL_WANTED
    gi4DvmrpDevFd = FileOpen ((const UINT1 *) DVMRP_DEVICE_FILE_NAME,
                              OSIX_FILE_RO);

    if (gi4DvmrpDevFd < 0)
    {
        LOG1 ((CRITICAL, IHM, "Unable to open DVMRP char device file\n"));
        return DVMRP_NOTOK;
    }

    if (OsixTskCrt (DVMRP_MDP_TASK_NAME, DVMRP_MDP_TASK_PRIORITY,
                    DVMRP_MDP_STACK_SIZE, (OsixTskEntry) DvmrpMdpTaskMain, 0,
                    &gu4DvmrpMdpTaskId) != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, IHM, "DVMRP-MDP task creation FAILED \n"));
        return DVMRP_NOTOK;
    }
#endif
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpDeRegisterForMDP 
 *
 * Description      :  This function closes the opened charecter device and 
 *                     deletes the task spawned for receiving multicast data 
 *                     packets 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  None 
 *
 * Input (s)        :  u1Id - currently not used. 
 *
 * Output (s)       :  None
 *
 * Returns          :  DVMRP_OK if MDP dtask deletion is success
 *                     else DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpDeRegisterForMDP ()
{
#ifdef NP_KERNEL_WANTED
    /* close the charecter device opend */
    FileClose (gi4DvmrpDevFd);
    /* Delete the task spawned for receiving multicast data from charecter 
     * device */
    OsixTskDel (gu4DvmrpMdpTaskId);
#endif
    return DVMRP_OK;
}

#ifdef NP_KERNEL_WANTED
/***************************************************************************
 * Function Name    :  DvmrpMdpTaskMain 
 *
 * Description      :  This function creates the node and opens the char 
 *                     device for receiving multicast data packets. it reads 
 *                     the data from char device and calls the callback 
 *                     function provided by DVMRP for receiving multicast data 
 *                     packets. This task will be in while (1) waiting for 
 *                     the data packets on char device. 
 *                     
 * Global Variables
 * Referred         :  None 
 *
 * Global Variables
 * Modified         :  gi4DvmrpDevFd - opened char dev id. 
 *
 * Input (s)        :  None 
 *
 * Output (s)       :  None
 *
 * Returns          :  None 
 ****************************************************************************/

VOID
DvmrpMdpTaskMain (VOID)
{
    PRIVATE UINT1       au1Buf[DVMRP_MDP_MAX_BYTES];
    tHeader            *Header = NULL;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tHandlePacketRxCallBack *pData = NULL;
    tIpParms           *pIpParms = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PktLen;
    UINT1              *pu1PktData = NULL;
    INT4                i4PktDataOffset;
    INT4                i4Len = 0;

    /* 
       read packets from charecter device and call the callback function 
       to enqueue the received data packet  
     */

    while (1)
    {
        MEMSET (au1Buf, 0, DVMRP_MDP_MAX_BYTES);

        if ((i4Len = read (gi4DvmrpDevFd, au1Buf, DVMRP_MDP_MAX_BYTES)) > 0)
        {
            Header = (tHeader *) au1Buf;
        }
        else
        {
            Header = NULL;
        }

        if (Header != NULL)
        {
            i4PktDataOffset = sizeof (tHeader) +
                sizeof (tHandlePacketRxCallBack);
            pData = (tHandlePacketRxCallBack *) (au1Buf + sizeof (tHeader));
            if (!pData)
            {
                continue;
            }

            u4IfIndex = pData->u4IfIndex;
            u4PktLen = pData->u4PktLen;
            pu1PktData = (UINT1 *) (au1Buf + i4PktDataOffset);

            /* Copy the recvd linear buf to cru buf */

            if ((pPkt = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
            {
                LOG1 ((CRITICAL, IHM,
                       "Unable to Allocate CRU buffer for MDP \n"));
                continue;
            }

            pIpParms = (tIpParms *) (&pPkt->ModuleData);
            pIpParms->u2Port = u4IfIndex;

            CRU_BUF_Copy_OverBufChain (pPkt, pu1PktData + 14, 0, u4PktLen);

            DvmrpProcessMcastDataPkt (pPkt);
        }
    }
}
#endif
