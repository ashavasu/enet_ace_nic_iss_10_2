/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dphost.c,v 1.4 2007/02/01 14:46:57 iss Exp $
 *
 * Description:Contains function for handling Host Membership 
 *             Information from IGMP module                   
 *
 *******************************************************************/
#include "dpinc.h"

/****************************************************************************/
/* Function Name    :  DvmrpHmHandleIgmpHostInfo                            */
/* Description      :  This function handles the host join or leave info    */
/*                     from the IGMP module and sends a Graft or Prune to   */
/*                     the upstream Neighbor required                       */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the local interface.    */
/*                     2. u4GroupAddr - Multicast Group address to which a  */
/*                        Host has joined or left from.                     */
/*                     3. u1Flag - Flag to indicate whether it is a Join    */
/*                        or a Leave report                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpHmHandleIgmpHostInfo (UINT4 u4IfId, UINT4 u4GroupAddr, UINT1 u1Flag)
#else
void
DvmrpHmHandleIgmpHostInfo (u4IfId, u4GroupAddr, u1Flag)
     UINT4               u4IfId, u4GroupAddr;
     UINT1               u1Flag;
#endif
{
    UINT4               u4Index, u4FreeHmEntry;
    tDvmrpInterfaceNode *pIfaceTbl = NULL;
    tHostMbrTbl        *pHostTbl = NULL;

    pIfaceTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfId);

    if (u1Flag == DVMRP_JOIN)
    {                            /* Host Join Report */

        u4FreeHmEntry = MAX_HOST_MBR_TABLE_ENTRIES;

        for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
        {
            if (((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u1Status
                 == DVMRP_INACTIVE) &&
                (u4FreeHmEntry == MAX_HOST_MBR_TABLE_ENTRIES))
            {
                u4FreeHmEntry = u4Index;
                break;
            }
            else if (((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u1Status
                      == DVMRP_ACTIVE) &&
                     ((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u4DestGroup
                      == u4GroupAddr) &&
                     ((DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index))->u4LocInterface
                      == u4IfId))
            {
                LOG1 ((INFO, HM, "Entry already present"));
                return;
            }
        }

        if (u4FreeHmEntry == MAX_HOST_MBR_TABLE_ENTRIES)
        {
            LOG1 ((CRITICAL, HM, "Host membership table Full"));
            return;
        }
        pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);
        pHostTbl->u4LocInterface = u4IfId;
        pHostTbl->u4DestGroup = u4GroupAddr;

        if ((pIfaceTbl == NULL) || !(DVMRP_CHECK_IF_STATUS (pIfaceTbl)))
        {
            LOG1 ((INFO, IHM, "Igmp Join Rcvd on Invalid or Dvmrp Disabled"
                   "Iface"));
            pHostTbl->u1PendFlg = DVMRP_PENDING;
            pHostTbl->u1Status = DVMRP_ACTIVE;
            return;
        }
        pHostTbl->u1Status = DVMRP_ACTIVE;

        /* 
         * A Graft will be sent to the upstream if required 
         */
        DvmrpGmSendGraftOnHmReport (u4IfId, u4GroupAddr);

    }
    else if (u1Flag == DVMRP_LEAVE)
    {
        /* 
         * Host Leave Report 
         */
        for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
        {
            pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);
            if ((pHostTbl->u4DestGroup == u4GroupAddr) &&
                (pHostTbl->u4LocInterface == u4IfId))
            {
                if ((pHostTbl->u1Status == DVMRP_ACTIVE) &&
                    (pHostTbl->u1PendFlg != DVMRP_PENDING))
                {
                    pHostTbl->u1Status = DVMRP_INACTIVE;

                    /* 
                     * A Prune will be sent to the upstream if required 
                     */
                    DvmrpPmSendPruneOnHmReport (u4IfId, u4GroupAddr);
                }
                if (pHostTbl->u1PendFlg == DVMRP_PENDING)
                {
                    pHostTbl->u4DestGroup = 0;
                    pHostTbl->u4LocInterface = 0;
                    pHostTbl->u1PendFlg = 0;
                    pHostTbl->u1Status = DVMRP_INACTIVE;
                }
                return;
            }
        }                        /* end for */
        /* Log entry not found */
    }
    else
    {
        /* Invalid Flag value */
        LOG1 ((WARNING, HM, "Invalid Message or v3 Report not supported"));
        return;
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpProcessPendingGroups                            */
/* Description      :  This function handles the pending host join or leave */
/*                     info from the IGMP module and sends a Graft or Prune to
 */
/*                     the upstream Neighbor required                       */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the local interface.    */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpHmProcessPendingGroups (tDvmrpInterfaceNode * pIfaceTbl)
#else
void
DvmrpHmProcessPendingGroups (*pIfaceTbl)
     tDvmrpInterfaceNode *pIfaceTbl;
#endif
{
    UINT4               u4Index;
    tHostMbrTbl        *pHostTbl = NULL;

    for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
    {
        pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);
        if (pHostTbl->u4LocInterface == pIfaceTbl->u4IfIndex)
        {
            if ((pHostTbl->u1PendFlg == DVMRP_PENDING) &&
                (pHostTbl->u1Status == DVMRP_ACTIVE))
            {
                DvmrpGmSendGraftOnHmReport (pIfaceTbl->u4IfIndex,
                                            pHostTbl->u4DestGroup);
                pHostTbl->u1PendFlg = 0;
            }
            else if (pHostTbl->u1Status == DVMRP_ACTIVE)
            {
                pHostTbl->u1PendFlg = DVMRP_PENDING;
            }
        }
    }
    return;
}

/****************************************************************************/
