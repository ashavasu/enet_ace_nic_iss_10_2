/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *  $Id: dputil.c,v 1.11 2012/03/06 11:27:20 siva Exp $
 *    
 *  Description:This file contains the utility functions as needed
 *              by DVMRP protocol. As such these functions can   
 *              be used for other protocol aspects as there are  
 *              generic to any TCP/IP protocol. Mainly we shall  
 *              deal here with validating a subnet, mask, IP addr
 *              formating the IP addr for printing it out, etc.
 *            
 ********************************************************************/
#include "dpinc.h"

/******************************************************************************/
/* Function Name: DvmrpUtilValidMask                                          */
/* Description   :This will check wether the mask is left contiguous, as      */
/*                required by CIDR. As such rfc-930 doesent pose any rule on  */
/*                representing a mask, but CIDR does.                         */
/* Input(s)    :  u4Mask : mask.                                              */
/* Output(s)    : None.                                                       */
/* Returns    :   ok if valid mask                                            */
/*                not ok if invalid mask.                                     */
/******************************************************************************/

#ifdef __STDC__
INT1
DvmrpUtilValidMask (UINT4 u4Mask)
#else

INT1
DvmrpUtilValidMask (u4Mask)
     UINT4               u4Mask;

#endif
{

    if (~(((u4Mask & (-((INT4) u4Mask))) - 1) | u4Mask) != 0)

    {

        /*
         *          *  Mask is not Left contiguous
         *                   */

        return (DVMRP_NOTOK);

    }

    return (DVMRP_OK);

}

/******************************************************************************/
/* Function Name: DvmrpUtilValidSubnet                                        */
/* Description    : Verify that a given subnet number and mask pair are credible*/
/*                (With CIDR, almost any subnet and mask are credible.)       */
/* Input(s)    : u4Subnet : Subnet number.                                   */
/*                u4Mask   : Subnet Mask.                                     */
/* Output(s)    : None.                                                       */
/* Returns    : DVMRP_OK : If the subnet is valid.                          */
/*                DVMRP_NOTOK : If the subnet is Invalid.                     */
/******************************************************************************/

#ifdef __STDC__
INT1
DvmrpUtilValidSubnet (UINT4 u4Subnet, UINT4 u4Mask)
#else

INT1
DvmrpUtilValidSubnet (u4Subnet, u4Mask)
     UINT4               u4Subnet;

     UINT4               u4Mask;

#endif
{

    /*
     *        u4Subnet = ntohl (u4Subnet);
     *               u4Mask = ntohl (u4Mask);
     *                    */

    if ((u4Subnet & u4Mask) != u4Subnet)

    {

        return (DVMRP_NOTOK);

    }

    if ((u4Subnet == 0) && (u4Mask == 0))

    {

        return (DVMRP_OK);

    }

    if (DVMRP_IN_CLASSA (u4Subnet))

    {

        if ((u4Mask < 0xff000000) || ((u4Subnet & 0xff000000) == 0x7f000000) ||
            ((u4Subnet & 0xff000000) == 0x00000000))

        {

            return (DVMRP_NOTOK);

        }

    }

    else if ((DVMRP_IN_CLASSD (u4Subnet)) || (DVMRP_IN_BADCLASS (u4Subnet)))

    {

        /*
         *          *  Above Class C address space
         *                   */

        return (DVMRP_NOTOK);

    }

    if (u4Subnet & ~u4Mask)

    {

        /*
         *          *  Host bits are set in the subnet 
         *                   */

        return (DVMRP_NOTOK);

    }

    if (DvmrpUtilValidMask (u4Mask) == DVMRP_NOTOK)

    {

        /*
         *          *  Netmask is not contiguous
         *                   */

        return (DVMRP_NOTOK);

    }

    return (DVMRP_OK);

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilGetFreeIface                                */
/* Description      :  This function gets a interface block from the Free   */
/*                     Pool                                                 */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  Pointer to Interface block                           */
/****************************************************************************/
#ifdef __STDC__
tIfaceList         *
DvmrpUtilGetFreeIface ()
#else
tIfaceList         *
DvmrpUtilGetFreeIface ()
#endif
{

    tIfaceList         *pIfaceBuffer;

    if (gpFreeIfacePtr == NULL)

    {

        return NULL;

    }

    pIfaceBuffer = gpFreeIfacePtr;

    gpFreeIfacePtr = pIfaceBuffer->pNext;

    pIfaceBuffer->pNext = NULL;

    return (pIfaceBuffer);

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilReleaseIface                                */
/* Description      :  This function releases an interface block to the Free*/
/*                     Pool.                                                */
/* Input (s)        :  pInterface - Pointer to the Interface block to be    */
/*                     released                                             */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpUtilReleaseIface (tIfaceList * pInterface)
#else
void
DvmrpUtilReleaseIface (pInterface)
     tIfaceList         *pInterface;

#endif
{

    tIfaceList         *pTmpIface, *pNextIface;

    if (pInterface == NULL)

    {

        return;

    }

    pTmpIface = pInterface;

    while (pInterface)

    {

        pInterface->pNext = (tIfaceList *) ((tOutputInterface *)
                                            pInterface)->pNext;

        ((tOutputInterface *) pInterface)->pNext = NULL;

        pNextIface = pInterface;

        pInterface = pInterface->pNext;

    }

    pNextIface->pNext = gpFreeIfacePtr;

    gpFreeIfacePtr = pTmpIface;

    return;

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilGetNbr                                      */
/* Description      :  This function gets a Neighbor block from the Free    */
/*                     Pool                                                 */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  Pointer to Neighbor block                            */
/****************************************************************************/
#ifdef __STDC__
tNbrList           *
DvmrpUtilGetNbr ()
#else
tNbrList           *
DvmrpUtilGetNbr ()
#endif
{

    tNbrList           *pNbrPtr;

    if (gpFreeNbrPtr == NULL)

    {

        return NULL;

    }

    pNbrPtr = gpFreeNbrPtr;

    gpFreeNbrPtr = pNbrPtr->pNext;

    pNbrPtr->pNext = NULL;

    return pNbrPtr;

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilReleaseNbr                                  */
/* Description      :  This function gets a interface block from the Free   */
/*                     Pool                                                 */
/* Input (s)        :  pNbr - Pointer to the Nbr which has to be released   */
/* Output (s)       :  None                                                 */
/* Returns          :  NULL                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpUtilReleaseNbr (tNbrList * pNeighbor)
#else
void
DvmrpUtilReleaseNbr (pNeighbor)
     tNbrList           *pNeighbor;

#endif
{

    tNbrList           *pTmpNbr, *pNextNbr;

    if (pNeighbor == NULL)

    {

        return;

    }

    pTmpNbr = pNeighbor;

    while (pNeighbor)

    {

        pNeighbor->pNext = (tNbrList *) ((tNbrPruneList *) pNeighbor)->pNbr;

        ((tNbrPruneList *) pNeighbor)->pNbr = NULL;

        pNextNbr = pNeighbor;

        pNeighbor = pNeighbor->pNext;

    }

    pNextNbr->pNext = gpFreeNbrPtr;

    gpFreeNbrPtr = pTmpNbr;

    return;

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilGetCacheEntry                               */
/* Description      :  This function hashes into the Forward Cache table to */
/*                     get the entry with given SrcAddress and GroupAddress */
/* Input (s)        :  1. u4SrcAddress - IP address of the Host             */
/*                     2. u4GroupAddr - Multicast Group address             */
/* Output (s)       :  None                                                 */
/* Returns          :  Pointer to the Forward Cache Table entry             */
/****************************************************************************/
#ifdef __STDC__
UINT1              *
DvmrpUtilGetCacheEntry (UINT4 u4Value1, UINT4 u4Value2, UINT1 u1Flag)
#else
UINT1              *
DvmrpUtilGetCacheEntry (u4Value1, u4Value2, u1Flag)
     UINT4               u4Value1;

     UINT4               u4Value2;

     UINT1               u1Flag;

#endif
{

    UINT1               u1Key;

    tForwCacheTbl      *pFwdCacheEntry;

    tRouteTbl          *pRouteEntry;

    DVMRP_GET_HASH_INDEX (u4Value1, u4Value2, u1Key);

    if (u1Flag == DVMRP_FORW_HASH)

    {

        DVMRP_HASH_SCAN_BUCKET (gpForwHashTable, u1Key, pFwdCacheEntry,
                                tForwCacheTbl *)
        {

            /* Search for SrcAddr and DestGroup by following the Hash Bucket links */
            if ((pFwdCacheEntry->u4SrcAddress == u4Value1) &&
                (pFwdCacheEntry->u4DestGroup == u4Value2))

                break;

        }

        return (UINT1 *) pFwdCacheEntry;

    }

    else

    {

        DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Key,
                                pRouteEntry, tRouteTbl *)
        {

            /* Search for SrcNw and SrcMask by following the Hash Bucket links */
            if ((pRouteEntry->u4SrcNetwork == u4Value1) &&
                (pRouteEntry->u4SrcMask == u4Value2))

                break;

        }

        return (UINT1 *) pRouteEntry;

    }

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilCheckConsOnes                               */
/* Description      :  This function returns the Number of Consecutive      */
/*                     'Ones' in a given 4 byte value.                      */
/* Input (s)        :  u4Value - Number for which ones has to be calculated */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
UINT4
DvmrpUtilCheckConsOnes (UINT4 u4Value)
#else
UINT4
DvmrpUtilCheckConsOnes (u4Value)
     UINT4               u4Value;

#endif
{

    UINT4               u4Count = 0, u4Index;

    for (u4Index = 0; u4Index < (8 * sizeof (UINT4)); u4Index++)

    {

        if (u4Value & 0x80000000)

        {

            u4Count++;

            u4Value = u4Value << 1;

        }

        else

        {

            break;

        }

    }

    return u4Count;

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilStartTimer                                  */
/* Description      :  This function starts a user timer, on the request    */
/*                     by a particulare module, for a particular duration.  */
/*                     TMO Timer Library routines are called                */
/* Input (s)        :  pPtrRef     - Pointer to the Table entry             */
/*                     u1TimerType - Type of the Timer Started              */
/*                     u2Duration  - Duration of the timer                  */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK on Success and DVMRP_NOTOK on Failure       */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpUtilStartTimer (UINT1 u1TimerType, UINT2 u2Duration,
                     tForwCacheTbl * pPtrRef)
#else
INT4
DvmrpUtilStartTimer (u1TimerType, u2Duration, pPtrRef)
     UINT1               u1TimerType;

     UINT2               u2Duration;

     tForwCacheTbl      *pPtrRef;

#endif
{

    tTmrAppTimer       *pTimerRef;

    tDvmrpTimer        *pDvmrpTimer = NULL;

    if ((u1TimerType == DVMRP_GRAFT_RETRANS_TIMER) ||
        (u1TimerType == DVMRP_PRUNE_RETRANS_TIMER))

    {

        pTimerRef = &((pPtrRef)->pSrcNwInfo->AppTimer.AppTmr);

        pDvmrpTimer = &(pPtrRef->pSrcNwInfo->AppTimer);

        pDvmrpTimer->u1TimerType = u1TimerType;

    }

    else

    {

        if ((u1TimerType > DVMRP_GRAFT_RETRANS_TIMER) ||
            (u1TimerType <= DVMRP_ZERO))
        {
            return (DVMRP_NOTOK);
        }
        pTimerRef = &((gTimerList[u1TimerType - 1]).AppTmr);

        pDvmrpTimer = &gTimerList[u1TimerType - 1];

        pDvmrpTimer->u1TimerType = u1TimerType;

    }

    if (DvmrpStartTimer (pTimerRef, u2Duration) == DVMRP_NOTOK)

    {

        LOG1 ((CRITICAL, IHM, "Unable to start CRU Timer "));

        return (DVMRP_NOTOK);

    }

    return (DVMRP_OK);

}

/****************************************************************************/
/* Function Name    : DvmrpUtilStopTimer                                    */
/* Description      : This function stops a timer of a specific type by     */
/*                    calling TMO Library routines                          */
/* Input (s)        : pPtrRef     - Pointer to an entry in the table        */
/*                    u1TimerType - Type of the Timer Started               */
/* Output (s)       : None                                                  */
/* Returns          : DVMRP_OK on Success and DVMRP_NOTOK on Failure        */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpUtilStopTimer (UINT1 u1TimerType, tForwCacheTbl * pPtrRef)
#else
INT4
DvmrpUtilStopTimer (u1TimerType, pPtrRef)
     UINT1               u1TimerType;

     UINT4              *pPtrRef;

#endif
{

    tTmrAppTimer       *pTimerRef;

    if (pPtrRef != NULL)

    {

        pTimerRef = &(((tForwCacheTbl *) pPtrRef)->pSrcNwInfo->AppTimer.AppTmr);

        if (pTimerRef == NULL)

        {

            /* Just to avoid stopping timers which has not been started */
            return (DVMRP_NOTOK);

        }

        else

        {

            if (DvmrpStopTimer (pTimerRef) == DVMRP_NOTOK)

            {

                LOG1 ((CRITICAL, IHM, "Unable to stop CRU Timer "));

                return (DVMRP_NOTOK);

            }

        }

    }

    else

    {
        if ((u1TimerType > DVMRP_MAX_TABLE_TIMERS) ||
            (u1TimerType <= DVMRP_ZERO))
        {
            return (DVMRP_NOTOK);
        }

        pTimerRef = &((gTimerList[u1TimerType - 1]).AppTmr);

        if (pTimerRef == NULL)

        {

            /* Just to avoid stopping timers which has not been started */
            return (DVMRP_NOTOK);

        }

        if (DvmrpStopTimer (pTimerRef) == DVMRP_NOTOK)

        {

            LOG1 ((CRITICAL, IHM, "Unable to stop CRU Timer "));

            return (DVMRP_NOTOK);

        }

    }

    return (DVMRP_OK);

}

/****************************************************************************/
/* Function Name    :  DvmrpUtilCheckSum                                    */
/* Description      :  This function calculates the checksum for the dvmrp  */
/*                     Packet                                               */
/* Input (s)        :  pBuf     - Pointer to the dvmrp Pkt                  */
/*                     i2Length - Pkt size                                  */
/* Output (s)       :  Checksum of the packet is calculted                  */
/* Returns          :  Checksum  of 2 Bytes is returned.                    */
/****************************************************************************/
#ifdef __STDC__
UINT4
DvmrpUtilCheckSum (UINT1 *pBuf, INT2 i2Length)
#else

UINT4
DvmrpUtilCheckSum (pBuf, i2Length)
     UINT1              *pBuf;

     INT2                i2Length;

#endif
{

    UINT4               u4Sum = 0;

    UINT2               u2Tmp = 0, u2Val = i2Length;

    while (u2Val > 1)

    {

        u4Sum += *((UINT2 *) (VOID *) (pBuf));

        pBuf += 2;

        u2Val -= 2;

    }

    if (u2Val > 0)

    {

        u2Tmp = 0;

        *((UINT1 *) &u2Tmp) = *pBuf;

        u4Sum += u2Tmp;

    }

    while (u4Sum >> 16)

    {

        u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);

    }

    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return u2Tmp;

}

VOID
DvmrpGetTimeString (UINT4 u4Time, INT1 *Time)
{

    UINT4               u4Days, u4Hrs, u4Mins, u4Secs, u4CentiSecs;

    MEMSET (Time, 0, 16);

    if (u4Time == 0)

    {

        SPRINTF ((CHR1 *) Time, "[0d 00:00:00.00]");

        return;

    }

    u4CentiSecs = u4Time % 100;

    u4Time = u4Time / 100;

    u4Secs = u4Time % 60;

    u4Time = u4Time / 60;

    u4Mins = u4Time % 60;

    u4Time = u4Time / 60;

    u4Hrs = u4Time % 24;

    u4Days = u4Time / 24;

    SPRINTF ((CHR1 *) Time, "[%ud %02u:%02u:%02u.%02u]", u4Days, u4Hrs,
             u4Mins, u4Secs, u4CentiSecs);

}
