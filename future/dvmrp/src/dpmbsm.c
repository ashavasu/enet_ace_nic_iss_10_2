/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: dpmbsm.c,v 1.19 2014/12/18 12:12:25 siva Exp $
 **
 ** Description: 
 **             
 **             
 **                                   
 **
 ********************************************************************/

#ifdef MBSM_WANTED
#include "dpinc.h"
extern UINT4        gu4DvmrpGlobStatus;

/*****************************************************************************/
/*                                                                           */
/* Function     : DvmrpMbsmUpdateMrouteTable                                 */
/*                                                                           */
/* Description  : Updates the Multicast Route Table in the NP based on the   */
/*                Line card status received                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u1Status - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
DvmrpMbsmUpdateMrouteTable (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Cmd)
{
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmProtoMsg       ProtoMsg;
    tMcRtEntry          rtEntry;
    tForwTbl           *pFwdEntry = NULL;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tOutputInterface   *pOifNode = NULL;
    tOutputInterface   *pOif = NULL;
    INT4                i4RetStatus = MBSM_SUCCESS;
    UINT4               u4OifCnt = 0;
    UINT4               u4Index;
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    /* Perform CRU Buf Copy of the pBuf to Structure tMbsmProtoMsg */
    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                  "CRU Buf Copy From Buf Chain Failed\n");
        return;
    }

    pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

    if (u4Cmd == MBSM_MSG_CARD_INSERT)
    {
        /* Check port/card message */
        if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
        {
            if (IpmcFsDvmrpMbsmNpInitHw (pSlotInfo) != FNP_SUCCESS)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                          "Card Insertion: FsDvmrpMbsmNpInitHw Failed \n");
                i4RetStatus = MBSM_FAILURE;
            }

            FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

            for (u4Index = 0; u4Index < MAX_FORW_TABLE_ENTRIES; u4Index++)
            {
                pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4Index);

                if (pFwdEntry == NULL)
                {
                    continue;
                }
                rtEntry.u2RpfIf = (UINT2) pFwdEntry->InIface.u4InInterface;
                pActSrcNode = pFwdEntry->pSrcInfo;

                if (pActSrcNode == NULL)
                {
                    continue;
                }
                if (u4Cmd == MBSM_MSG_CARD_INSERT)
                {
                    pOifNode = pFwdEntry->pOutInterface;
                    pOif = pOifNode;

                    while (pOif != NULL)
                    {
                        u4OifCnt++;
                        pOif = pOif->pNext;
                    }

                    if (pOifNode != NULL)
                    {
                        pDsIf =
                            DVMRP_MALLOC (sizeof (tMcDownStreamIf) * u4OifCnt,
                                          tMcDownStreamIf);
                        if (pDsIf == NULL)
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                                      "Failure allocating memory for installing "
                                      "entry in NP\n");
                            i4RetStatus = MBSM_FAILURE;
                            break;
                        }
                    }

                    pTmpDsIf = pDsIf;

                    while (pOifNode != NULL)
                    {
                        if (rtEntry.u2RpfIf == pOifNode->u4OutInterface)
                        {
                            pOifNode = pOifNode->pNext;
                            continue;
                        }
                        pTmpDsIf->u4IfIndex = pOifNode->u4OutInterface;
                        pIfNode =
                            DVMRP_GET_IFNODE_FROM_INDEX (pOifNode->
                                                         u4OutInterface);
                        pTmpDsIf->u2TtlThreshold = 255;
                        pTmpDsIf->u4Mtu = pIfNode->u4IfMTUSize;

                        if (CFA_FAILURE == CfaGetVlanId (pTmpDsIf->u4IfIndex,
                                                         &pTmpDsIf->u2VlanId))
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                                      "Card Insertion: "
                                      "DvmrpMbsmUpdateMrouteTable Failed "
                                      "in retrieving VlanId for DsIf \n");

                            i4RetStatus = MBSM_FAILURE;
                            continue;
                        }

                        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                                  pActSrcNode->u4SrcAddr,
                                                  pTmpDsIf->u2VlanId,
                                                  pTmpDsIf->McFwdPortList,
                                                  pTmpDsIf->UntagPortList);

                        pTmpDsIf++;
                        pOifNode = pOifNode->pNext;
                    }

                    pDsIf = (u4OifCnt == 0) ? NULL : pDsIf;

                    if (CFA_FAILURE == CfaGetVlanId (rtEntry.u2RpfIf,
                                                     &rtEntry.u2VlanId))
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                                  "Card Insertion: "
                                  "DvmrpMbsmUpdateMrouteTable Failed "
                                  "in retrieving VlanId for rtEntry \n");

                        i4RetStatus = MBSM_FAILURE;
                        continue;
                    }

#ifdef IGS_WANTED
                    if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts
                        (rtEntry.u2VlanId,
                         pFwdEntry->u4GroupAddr,
                         pActSrcNode->u4SrcAddr,
                         rtEntry.McFwdPortList, rtEntry.UntagPortList))
#else
                    if (VLAN_FAILURE ==
                        VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList))
#endif
                    {
                        i4RetStatus = MBSM_FAILURE;
                        continue;
                    }

                    if (FNP_FAILURE ==
                        (IpmcFsNpIpv4MbsmMcAddRouteEntry (DVMRP_RTR_ID,
                                                      pFwdEntry->u4GroupAddr,
                                                      FNP_ALL_BITS_SET,
                                                      pActSrcNode->u4SrcAddr,
                                                      FNP_ALL_BITS_SET,
                                                      IPMC_MRP, rtEntry,
                                                      u4OifCnt, pDsIf,
                                                      pSlotInfo)))
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                                  "Failure installing entry in NP\n");
                        i4RetStatus = MBSM_FAILURE;
                    }
                    else
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                                  "Installed Entry in Hardware successfully\n");
                    }
                    if (pDsIf != NULL)
                    {
                        DVMRP_MEM_FREE (pDsIf);
                    }
                }
            }
        }
    }
    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    MbsmSendAckFromProto (&protoAckMsg);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DvmrpMbsmPostMessage                                       */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the DVMRP Task                            */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                pProtoMsg will be NULL for LOAD_SHARING event              */
/*                i1Status  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    tDvmrpQMsg         *pQMsg = NULL;

    if (gu4DvmrpGlobStatus == DVMRP_STATUS_DISABLED)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP, "Dvmrp is Globally Disabled\n");

        if ((i4Event == MBSM_MSG_LOAD_SHARING_ENABLE) ||
            (i4Event == MBSM_MSG_LOAD_SHARING_DISABLE))
        {
            /* Ack should not be sent for load-sharing messages when DVMRP 
             * is disabled */
            return MBSM_SUCCESS;
        }

        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if (pProtoMsg != NULL)
    {
        pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg), 0);
        if (pBuf == NULL)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                      "CRU Buf Allocation Failed\n");
            return MBSM_FAILURE;
        }

        /* Copy the message to the CRU buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                                   sizeof (tMbsmProtoMsg));
    }

    if (DVMRP_MEM_ALLOCATE (DVMRP_Q_PID, pQMsg, tDvmrpQMsg) == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP,
                  "Unable to Allocate MemBlock from MemPool for Timer");
        if (pBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        }
        return MBSM_FAILURE;
    }

    pQMsg->u4MsgType = (UINT4) i4Event;
    pQMsg->DvmrpQMsgParam.DvmrpIfParam = pBuf;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP, "Unable to Enqueue the message");
        if (pBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
            pBuf = NULL;
        }
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return MBSM_FAILURE;
    }

    /* send an explicit event to the task */
    if (OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_DVMRP, "Unable to send the event");
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************
 * Function Name : DvmrpMbsmUpdtMrouteTblForLoadSharing 
 * Description   : Function to program IP multicast table when load-sharing is 
 *                 enabled.
 * Input(s)      : u1Flag - LoadSharingFlag 
 * Output(s)     : None  
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
DvmrpMbsmUpdtMrouteTblForLoadSharing (UINT1 u1Flag)
{
    UNUSED_PARAM (u1Flag);
    return (MBSM_SUCCESS);
}
#endif
