
/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : dvmrpcli.c                                       |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                                     |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : DVMRP                                            |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI DVMRP Commands           |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 3rd Nov 2004    |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*                                                                             *
* $Id: dvmrpcli.c,v 1.16                                                      *
*                                                                             *
*/

#ifndef __DVMRPCLI_C__
#define __DVMRPCLI_C__
#include "dpinc.h"
#include "dvmrpcli.h"
#include "fsdvmrcli.h"
#include "fsdvmrwr.h"
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_dvmrp_cmd                              */
/*                                                                           */
/*     DESCRIPTION      : This function processes all the DVMRP commands     */
/*                                                                           */
/*     INPUT            : CliHandle-Handle to  the CLI Context               */
/*                          u4Command- Command Identifier to be passed       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_dvmrp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT1              *args[DVMRP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4CmdType = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if ((gu1DvmrpStatus == DVMRP_DISABLED) && (u4Command != CLI_DVMRP_ENABLE))
    {
        CliPrintf (CliHandle, "\r%% DVMRP has been disabled\r\n ");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == DVMRP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_DVMRP_ENABLE:
            i4RetVal =
                DvmrpSetModuleStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_DVMRP_PRUNE_LIFE_TIME:
            i4RetVal = DvmrpSetPruneLifeTime (CliHandle, *(INT4 *) (VOID *)
                                              (args[0]));
            break;

        case CLI_DVMRP_NO_PRUNE_LIFE_TIME:
            i4RetVal =
                DvmrpSetPruneLifeTime (CliHandle,
                                       DVMRP_DEFAULT_PRUNE_LIFE_TIME);
            break;
        case CLI_DVMRP_IFACE_STATUS_ENABLE:
            i4RetVal = DvmrpSetIfaceStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_DVMRP_TRACE:
            i4RetVal = DvmrpSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_DVMRP_NO_TRACE:
            i4RetVal = DvmrpSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_DVMRP_SHOW_ALL:

            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == SHOW_DVMRP_ROUTE)
            {
                i4RetVal = DvmrpShowRouteInterface (CliHandle, u4Index);
            }
            else if (u4CmdType == SHOW_MROUTE)
            {
                i4RetVal = DvmrpShowMulticastRoute (CliHandle);
            }
            else if (u4CmdType == SHOW_NEIGHBOR)
            {
                i4RetVal = DvmrpShowNeighbor (CliHandle);
            }
            else if (u4CmdType == SHOW_NEXTHOP)
            {
                i4RetVal = DvmrpShowNextHop (CliHandle);
            }
            else if (u4CmdType == SHOW_INFO)
            {
                i4RetVal = DvmrpShowInfo (CliHandle);
            }
            else if (u4CmdType == SHOW_PRUNE)
            {
                i4RetVal = DvmrpShowMulticastPrune (CliHandle);
            }
            break;

    }
    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {

        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DVMRP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", DvmrpCliErrString[u4ErrCode]);
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Command Failed\r\n");
        }
        CLI_SET_ERR (0);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpSetModuleStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function enables or disables the DVMRP module */
/*                                                                           */
/*     INPUT            :  CliHandle- Handle to the Cli Context              */
/*                         u1Status -  Enable/disable                        */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2DvmrpStatus (&u4ErrCode, i4Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetDvmrpStatus (i4Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpSetPruneLifeTime                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the prune life time             */
/*                                                                           */
/*     INPUT            :  CliHandle- Handle to the Cli Context              */
/*                         u4Timer -  prune life time value                  */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpSetPruneLifeTime (tCliHandle CliHandle, INT4 i4Timer)
{
    UINT4               u4ErrCode;

    if (nmhTestv2DvmrpPruneLifeTime (&u4ErrCode, i4Timer) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetDvmrpPruneLifeTime (i4Timer) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpSetIfaceStatus                                */
/*                                                                           */
/*     DESCRIPTION      : This function enables or disables the DVMRP module */
/*                        on a VLAN interface                                */
/*                                                                           */
/*     INPUT            :  CliHandle- Handle to the Cli Context              */
/*                         u1Status -  Enable/disable                        */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpSetIfaceStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrCode;
    UINT4               u4IfIndex;
    INT4                i4RowStatus;
    INT4                i4RetVal;

    u4IfIndex = CLI_GET_VLANID ();

    if (nmhGetDvmrpInterfaceStatus (u4IfIndex, &i4RetVal) == SNMP_FAILURE)
    {
        i4RowStatus = (i4Status == 1) ? DVMRP_ROW_STATUS_CREATE_AND_GO :
            DVMRP_ROW_STATUS_DESTROY;

        if (nmhTestv2DvmrpInterfaceStatus
            (&u4ErrCode, (INT4) u4IfIndex, i4RowStatus) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        if (i4Status == 1)
        {
            if (nmhSetDvmrpInterfaceStatus
                ((INT4) u4IfIndex, i4RowStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        if (i4RetVal == i4Status)
        {
            return (CLI_SUCCESS);
        }
        i4RowStatus = (i4Status == 1) ? DVMRP_ROW_STATUS_ACTIVE :
            DVMRP_ROW_STATUS_DESTROY;
        if (nmhTestv2DvmrpInterfaceStatus
            (&u4ErrCode, (INT4) u4IfIndex, i4RowStatus) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetDvmrpInterfaceStatus ((INT4) u4IfIndex, i4RowStatus) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpSetTrace                                      */
/*                                                                           */
/*     DESCRIPTION      : This function enables /disables debugging of       */
/*                        DVMRP module. It also sets or resets the debug mask*/
/*                                                                           */
/*     INPUT            :  CliHandle -Handle to the Cli Context              */
/*                         i4Value   - Debugging bits                        */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
DvmrpSetTrace (tCliHandle CliHandle, INT4 i4Value)
{
    if (i4Value >= 0)
    {
        gu4DvmrpLogMask |= (UINT4) i4Value;
    }
    else
    {
        i4Value &= DVMRP_MAX_INT4;
        gu4DvmrpLogMask &= (~i4Value);
    }
    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowInfo                                      */
/*                                                                           */
/*     DESCRIPTION      : This function DVMRP information                    */
/*                                                                           */
/*     INPUT            :  CliHandle- Handle to the Cli Context              */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpShowInfo (tCliHandle CliHandle)
{
    INT4                i4RetVal;
    INT4                i4IfIndex;
    INT4                i4PrevIfIndex;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4LocalAddress;
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    const CHR1         *au1AdminStatus[] = {
        "DVMRP_ENABLED",
        "DVMRP_DISABLED"
    };

    nmhGetDvmrpStatus (&i4RetVal);
    if (i4RetVal == DVMRP_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nDVMRP is enabled in the switch\r\n");
    }
    else
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "Dvmrp Version : 0x%x (major) 0x%x (minor)\r\n",
               DVMRP_MAJOR_VERSION, DVMRP_MINOR_VERSION);

    nmhGetDvmrpGenerationId (&i4RetVal);
    CliPrintf (CliHandle, "GenerationId : %d,  ", i4RetVal);

    nmhGetDvmrpNumRoutes ((UINT4 *) &i4RetVal);
    CliPrintf (CliHandle, "Total Routes : %d,  ", i4RetVal);

    nmhGetDvmrpReachableRoutes ((UINT4 *) &i4RetVal);
    CliPrintf (CliHandle, "Reachable Routes : %d\r\n", i4RetVal);

    nmhGetDvmrpPruneLifeTime (&i4RetVal);
    CliPrintf (CliHandle, "Prune Life Time : %d\r\n", i4RetVal);

    if (nmhGetFirstIndexDvmrpInterfaceTable (&i4IfIndex) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nInterface Information\r\n");
        CliPrintf (CliHandle, "------------------------\r\n");

        CliPrintf (CliHandle, "%14s%16s%8s%12s\r\n", "IfaceName/Id",
                   "Address", "Metric", "AdminStatus");

        CliPrintf (CliHandle, "%14s%16s%8s%12s\r\n", "--------------",
                   "---------------", "------", "-----------");
        do
        {
            MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

            CfaGetInterfaceNameFromIndex (i4IfIndex,
                                          (UINT1 *) au1InterfaceName);
            CliPrintf (CliHandle, "%7s/%-5d ", au1InterfaceName, i4IfIndex);

            nmhGetDvmrpInterfaceLocalAddress (i4IfIndex, &u4LocalAddress);
            DVMRP_CONVERT_IPADDR_TO_STR (ai1address, u4LocalAddress);
            CliPrintf (CliHandle, "%16s", ai1address);

            nmhGetDvmrpInterfaceMetric (i4IfIndex, &i4RetVal);
            CliPrintf (CliHandle, " %6d ", i4RetVal);

            nmhGetDvmrpInterfaceStatus (i4IfIndex, &i4RetVal);

            if (i4RetVal == DVMRP_ENABLED)
            {
                CliPrintf (CliHandle, "%12s\r\n", au1AdminStatus[0]);
            }
            else
            {
                CliPrintf (CliHandle, "%12s\r\n", au1AdminStatus[1]);
            }

            i4PrevIfIndex = i4IfIndex;
        }
        while (nmhGetNextIndexDvmrpInterfaceTable (i4PrevIfIndex, &i4IfIndex) ==
               SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowRouteInterface                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays DVMRP routes                */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the Cli Context               */
/*                        u4Index- Specific interface for which the route    */
/*                        be displayed                                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpShowRouteInterface (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1TmStr[MAX_TIME_STRING];
    UINT4               u4DvmrpRouteSource;
    UINT4               u4DvmrpRouteSourceMask;
    UINT4               u4PrevDvmrpRouteSource;
    UINT4               u4PrevDvmrpRouteSourceMask;
    INT4                i4RetVal;
    INT4                i4Status;
    UINT4               u4Mask;
    UINT4               u4DvmrpRouteUpstreamNeighbor;
    UINT1               u1Flag = 0;
    UINT4               u4IfaceIndex;
    const CHR1         *au1RouteStatus[] = {
        "Active",
        "Inactive",
        "Holdown",
        "Relearnt",
        "Local/NeverExpire"
    };
    INT4                i4Quit = CLI_SUCCESS;

    CliPrintf (CliHandle, "\r\nDvmrp Routing Table\r\n");
    CliPrintf (CliHandle, "-------------------\r\n");

    if (nmhGetFirstIndexDvmrpRouteTable
        (&u4DvmrpRouteSource, &u4DvmrpRouteSourceMask) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        nmhGetDvmrpRouteStatus (u4DvmrpRouteSource, u4DvmrpRouteSourceMask,
                                &i4Status);
        if (i4Status != DVMRP_INACTIVE)
        {
            nmhGetDvmrpRouteIfIndex (u4DvmrpRouteSource, u4DvmrpRouteSourceMask,
                                     (INT4 *) &u4IfaceIndex);

            if (u4IfIndex != 0)
            {
                if (u4IfaceIndex == u4IfIndex)
                {
                    u1Flag = 1;
                    /* This flag is being set to indicate that the user
                     * has specified the interface for which route details 
                     * must be displayed */
                }
            }
            else
            {
                u1Flag = 1;
            }

            if (u1Flag)
            {
                DVMRP_CONVERT_IPADDR_TO_STR (ai1address, u4DvmrpRouteSource);
                u4Mask = CliGetMaskBits (u4DvmrpRouteSourceMask);
                CliPrintf (CliHandle, "%s/%d", ai1address, u4Mask);

                nmhGetDvmrpRouteMetric (u4DvmrpRouteSource,
                                        u4DvmrpRouteSourceMask, &i4RetVal);
                CliPrintf (CliHandle, "[%d] ", i4RetVal);

                nmhGetDvmrpRouteUpTime (u4DvmrpRouteSource,
                                        u4DvmrpRouteSourceMask,
                                        (UINT4 *) &i4RetVal);
                DvmrpGetTimeString (i4RetVal, ai1TmStr);
                CliPrintf (CliHandle, "uptime %s, ", ai1TmStr);

                nmhGetDvmrpRouteExpiryTime (u4DvmrpRouteSource,
                                            u4DvmrpRouteSourceMask,
                                            (UINT4 *) &i4RetVal);
                DvmrpGetTimeString (i4RetVal, ai1TmStr);
                CliPrintf (CliHandle, "expires %s\r\n", ai1TmStr);

                CliPrintf (CliHandle, "Status: %s\r\n",
                           au1RouteStatus[i4Status - 1]);
                nmhGetDvmrpRouteUpTime (u4DvmrpRouteSource,
                                        u4DvmrpRouteSourceMask,
                                        (UINT4 *) &i4RetVal);

                nmhGetDvmrpRouteUpstreamNeighbor (u4DvmrpRouteSource,
                                                  u4DvmrpRouteSourceMask,
                                                  &u4DvmrpRouteUpstreamNeighbor);
                DVMRP_CONVERT_IPADDR_TO_STR
                    (ai1address, u4DvmrpRouteUpstreamNeighbor);

                MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
                CfaGetInterfaceNameFromIndex (u4IfaceIndex,
                                              (UINT1 *) au1InterfaceName);
                i4Quit =
                    CliPrintf (CliHandle, "via %s, %s\r\n\r\n", ai1address,
                               au1InterfaceName);

                /* i4Quit gets the paging status */

                if (u4IfIndex != 0)
                {
                    break;
                }

                if (i4Quit == CLI_FAILURE)
                {
                    break;
                }

            }
        }

        u4PrevDvmrpRouteSource = u4DvmrpRouteSource;
        u4PrevDvmrpRouteSourceMask = u4DvmrpRouteSourceMask;
    }
    while (nmhGetNextIndexDvmrpRouteTable (u4PrevDvmrpRouteSource,
                                           &u4DvmrpRouteSource,
                                           u4PrevDvmrpRouteSourceMask,
                                           &u4DvmrpRouteSourceMask) ==
           SNMP_SUCCESS);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowMulticastRoute                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays DVMRP multicast routes      */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the Cli Context               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpShowMulticastRoute (tCliHandle CliHandle)
{
    INT4                i4Quit = CLI_SUCCESS;
    INT4                i4RetVal;
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                ai2address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    const CHR1         *au1IntStatus[] = {
        "GRAFT_ACK_RCVD",
        "WAITING_GRAFT_ACK",
        "NORMAL",
        "PRUNED",
        "DATA_ON_PRUNED_IFACE",
        "LOCAL_NETWORK"
    };
    UINT4               u4DvmrpSourceNetwork;
    UINT4               u4DvmrpGroupAddress;

    UINT4               u4PrevDvmrpSourceNetwork;
    UINT4               u4PrevDvmrpGroupAddress;

    CliPrintf (CliHandle, "\r\nDvmrp Forward Information\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");

    if (nmhGetFirstIndexDvmrpForwardTable (&u4DvmrpSourceNetwork,
                                           &u4DvmrpGroupAddress) !=
        SNMP_FAILURE)
    {
        do
        {

            nmhGetDvmrpForwardTblStatus (u4DvmrpSourceNetwork,
                                         u4DvmrpGroupAddress, &i4RetVal);
            if (i4RetVal != DVMRP_INACTIVE)
            {

                DVMRP_CONVERT_IPADDR_TO_STR (ai1address, u4DvmrpSourceNetwork);
                DVMRP_CONVERT_IPADDR_TO_STR (ai2address, u4DvmrpGroupAddress);

                CliPrintf (CliHandle, "\r\n(%s, %s)\r\n", ai1address,
                           ai2address);

                nmhGetDvmrpForwardUpstreamNeighbor (u4DvmrpSourceNetwork,
                                                    u4DvmrpGroupAddress,
                                                    (UINT4 *) &i4RetVal);
                DVMRP_CONVERT_IPADDR_TO_STR (ai1address, i4RetVal);
                CliPrintf (CliHandle,
                           "Reverse Path Forwarding Neighbor/Interface : %s/",
                           ai1address);

                nmhGetDvmrpForwardInIfIndex (u4DvmrpSourceNetwork,
                                             u4DvmrpGroupAddress, &i4RetVal);

                MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
                CfaGetInterfaceNameFromIndex (i4RetVal,
                                              (UINT1 *) au1InterfaceName);
                CliPrintf (CliHandle, "(%s)\r\n", au1InterfaceName);

                nmhGetDvmrpForwardInIfState (u4DvmrpSourceNetwork,
                                             u4DvmrpGroupAddress, &i4RetVal);

                CliPrintf (CliHandle,
                           "Interface State of Upstream neighbor : %s  ",
                           au1IntStatus[i4RetVal - 5]);

                nmhGetDvmrpForwardExpiryTime (u4DvmrpSourceNetwork,
                                              u4DvmrpGroupAddress,
                                              (UINT4 *) &i4RetVal);
                CliPrintf (CliHandle, "Expiry Time : %d\r\n", i4RetVal);

            }

            i4Quit = CliPrintf (CliHandle, "\r");
            /* Get the paging status */

            if (i4Quit == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }

            u4PrevDvmrpSourceNetwork = u4DvmrpSourceNetwork;
            u4PrevDvmrpGroupAddress = u4DvmrpGroupAddress;

        }
        while (nmhGetNextIndexDvmrpForwardTable (u4PrevDvmrpSourceNetwork,
                                                 &u4DvmrpSourceNetwork,
                                                 u4PrevDvmrpGroupAddress,
                                                 &u4DvmrpGroupAddress) !=
               SNMP_FAILURE);

    }
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

 /*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowMulticastPrune                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays DVMRP multicast routes prune*/
/*                        list                                               */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the Cli Context               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpShowMulticastPrune (tCliHandle CliHandle)
{
    tForwTbl           *pFwdEntry = NULL;
    tOutputInterface   *pOutlist = NULL;
    INT4                i4Quit = CLI_SUCCESS;
    UINT4               u4Index = 0;
    tNbrPruneList      *pPList = NULL;
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                ai1pruneexp[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (ai1pruneexp, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMCPY (ai1pruneexp, DVMRP_PRUNE_TIMER_EXP, STRLEN (DVMRP_PRUNE_TIMER_EXP));

    for (u4Index = 0; u4Index < MAX_FORW_TABLE_ENTRIES; u4Index++)
    {
        pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4Index);
        pOutlist = pFwdEntry->pOutInterface;
        while (pOutlist != NULL)
        {
            pPList = pOutlist->pDepNbrs;

            CliPrintf (CliHandle, "\r\nPrune List :");

            while (pPList != NULL)
            {
                if (pPList->u4PruneLifeTime != 0)
                {
                    DVMRP_CONVERT_IPADDR_TO_STR (ai1address, pPList->u4NbrAddr);
                    CliPrintf (CliHandle,
                               "\r\n  NbrAddress/PruneTime : %s/%d   ",
                               ai1address, pPList->u4PruneLifeTime);
                }
                else
                {
                    DVMRP_CONVERT_IPADDR_TO_STR (ai1address, pPList->u4NbrAddr);
                    CliPrintf (CliHandle,
                               "\r\n  NbrAddress : %s / %s   ",
                               ai1address, ai1pruneexp);
                }

                pPList = pPList->pNbr;
            }
            CliPrintf (CliHandle, "\r\n");

            pOutlist = pOutlist->pNext;
        }
        i4Quit = CliPrintf (CliHandle, "\r");

        /* Get the paging status */

        if (i4Quit == CLI_FAILURE)
        {
            break;
        }
    }
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowNeighbor                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays DVMRP neighbors             */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the Cli Context               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DvmrpShowNeighbor (tCliHandle CliHandle)
{
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1TmStr[MAX_TIME_STRING];
    INT4                i4RetVal;
    UINT4               u4DvmrpNeighborAddress;
    UINT4               u4PrevDvmrpNeighborAddress;
    const CHR1         *au1Adj[] = {
        "ESTABLISHED",
        "NOT ESTABLISHED"
    };
    INT4                i4Quit = CLI_SUCCESS;

    if (nmhGetFirstIndexDvmrpNeighborTable (&u4DvmrpNeighborAddress) ==
        SNMP_FAILURE)
    {
        return (CLI_SUCCESS);
    }
    CliPrintf (CliHandle, "\r\nNeighbour Information\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    CliPrintf (CliHandle, "%15s%12s%19s%8s%11s%12s\r\n",
               "NeighborAddress", "Interface", "      UpTime     ", "ExpTime",
               "  GenId  ", "Adjacency");

    CliPrintf (CliHandle, "%17s%12s%17s%8s%11s%12s\r\n",
               "-----------------", "-----------", "----------------",
               "-------", "-------", "----------");
    do
    {
        DVMRP_CONVERT_IPADDR_TO_STR (ai1address, u4DvmrpNeighborAddress);
        CliPrintf (CliHandle, "%-17s", ai1address);

        nmhGetDvmrpNeighborIfIndex (u4DvmrpNeighborAddress, &i4RetVal);
        MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
        CfaGetInterfaceNameFromIndex (i4RetVal, (UINT1 *) au1InterfaceName);
        CliPrintf (CliHandle, "  %-11s", au1InterfaceName);

        nmhGetDvmrpNeighborUpTime (u4DvmrpNeighborAddress, (UINT4 *) &i4RetVal);

        DvmrpGetTimeString ((UINT4) i4RetVal, ai1TmStr);
        CliPrintf (CliHandle, "%-17s", ai1TmStr);

        nmhGetDvmrpNeighborExpiryTime (u4DvmrpNeighborAddress,
                                       (UINT4 *) &i4RetVal);
        CliPrintf (CliHandle, " %-6d ", i4RetVal);
        nmhGetDvmrpNeighborGenerationId (u4DvmrpNeighborAddress, &i4RetVal);

        CliPrintf (CliHandle, "   %-5d", i4RetVal);
        nmhGetDvmrpNeighborAdjFlag (u4DvmrpNeighborAddress, &i4RetVal);

        i4Quit = CliPrintf (CliHandle, "%15s\r\n", au1Adj[i4RetVal]);

        /* Get the paging status */

        if (i4Quit == CLI_FAILURE)
        {
            break;
        }
        u4PrevDvmrpNeighborAddress = u4DvmrpNeighborAddress;

    }
    while (nmhGetNextIndexDvmrpNeighborTable (u4PrevDvmrpNeighborAddress,
                                              &u4DvmrpNeighborAddress) !=
           SNMP_FAILURE);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowNextHop                                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays DVMRP nexthop information   */
/*                                                                           */
/*     INPUT            : CliHandle- Handle to the Cli Context               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DvmrpShowNextHop (tCliHandle CliHandle)
{
    tNextHopTbl        *pNextHop = NULL;
    tNextHopType       *pTmpNextHopType = NULL;
    tNbrAddrList       *pDepNbr = NULL;
    UINT4               u4Index = 0;
    CHR1                ai1address[CFA_MAX_PORT_NAME_LENGTH];
    CHR1                ai2address[CFA_MAX_PORT_NAME_LENGTH];
    const CHR1         *au1Type[] = { "Leaf", "Branch" };
    const CHR1         *au1Df[] = { "True", "False" };

    UINT4               u4DvmrpRouteNextHopSource;
    UINT4               u4DvmrpRouteNextHopSourceMask;
    INT4                i4DvmrpRouteNextHopIfIndex;
    INT4                i4Quit = CLI_SUCCESS;
    UINT1               u1BranchFound = DVMRP_FALSE;
    UINT2               u2CommaCount;

    CliPrintf (CliHandle, "\r\nDvmrp NextHop Information\r\n");
    CliPrintf (CliHandle, "---------------------------\r\n");

    if (nmhGetFirstIndexDvmrpRouteNextHopTable (&u4DvmrpRouteNextHopSource,
                                                &u4DvmrpRouteNextHopSourceMask,
                                                &i4DvmrpRouteNextHopIfIndex) ==
        SNMP_FAILURE)
    {
        return (CLI_SUCCESS);
    }

    for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
    {
        pNextHop = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
        if (pNextHop->u1Status == DVMRP_INACTIVE)
        {
            continue;
        }

        pTmpNextHopType = pNextHop->pNextHopType;
        while (pTmpNextHopType != NULL)
        {
            if (pTmpNextHopType->u1InType == DVMRP_BRANCH)
            {
                u1BranchFound = DVMRP_TRUE;
                break;
            }
            pTmpNextHopType = pTmpNextHopType->pNextHopTypeNext;

        }
        if (u1BranchFound == DVMRP_TRUE)
        {
            u1BranchFound = DVMRP_FALSE;
            DVMRP_CONVERT_IPADDR_TO_STR (ai1address, pNextHop->u4NextHopSrc);
            DVMRP_CONVERT_IPADDR_TO_STR (ai2address,
                                         pNextHop->u4NextHopSrcMask);
            CliPrintf (CliHandle, "\r\nSrcAddress/Mask : %s/%s",
                       ai1address, ai2address);

            pTmpNextHopType = pNextHop->pNextHopType;
            while (pTmpNextHopType != NULL)
            {
                if (pTmpNextHopType->u1InType == LEAF)
                {
                    pTmpNextHopType = pTmpNextHopType->pNextHopTypeNext;
                    continue;
                }
                CfaGetInterfaceNameFromIndex ((INT4) pTmpNextHopType->
                                              u4NHopInterface,
                                              (UINT1 *) ai2address);
                CliPrintf (CliHandle, "\r\nNextHopIndex : %d (%s), ",
                           pTmpNextHopType->u4NHopInterface, ai2address);

                CliPrintf (CliHandle, "IfType : %s, ",
                           au1Type[pTmpNextHopType->u1InType - 1]);

                CliPrintf (CliHandle, "DF: %s", au1Df[pTmpNextHopType->
                                                      u1DesigForwFlag]);

                if (pTmpNextHopType->pDependentNbrs == NULL)
                {
                    CliPrintf (CliHandle, "\r\nDependent Nbr : NIL\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\nDependent Nbrs :");
                }

                pDepNbr = pTmpNextHopType->pDependentNbrs;

                u2CommaCount = 0;
                while (pDepNbr != NULL)
                {
                    DVMRP_CONVERT_IPADDR_TO_STR (ai1address,
                                                 pDepNbr->u4NbrAddr);
                    if (u2CommaCount == 0)
                    {
                        CliPrintf (CliHandle, "%s", ai1address);
                    }
                    else
                    {
                        CliPrintf (CliHandle, ", %s", ai1address);
                    }

                    u2CommaCount++;

                    pDepNbr = pDepNbr->pNbr;
                }

                pTmpNextHopType = pTmpNextHopType->pNextHopTypeNext;
            }
            i4Quit = CliPrintf (CliHandle, "\r\n");
        }
        /* Get paging status */
        if (i4Quit == CLI_FAILURE)
        {
            break;
        }
    }

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : DvmrpShowRunningConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        DVMRP module                                       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified module (dvmrp/all), for       */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DvmrpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4RetVal = 0;
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    nmhGetDvmrpStatus (&i4RetVal);

    if (i4RetVal != DVMRP_DISABLED)
    {
        CliPrintf (CliHandle, "set ip dvmrp enable\r\n");

        nmhGetDvmrpPruneLifeTime (&i4RetVal);

        if (i4RetVal != DVMRP_DEFAULT_PRUNE_LIFE_TIME)
        {
            CliPrintf (CliHandle, "ip dvmrp prune-life-time %d\r\n", i4RetVal);
        }

        if (u4Module == ISS_DVMRP_SHOW_RUNNING_CONFIG)
        {
            if (nmhGetFirstIndexDvmrpInterfaceTable (&i4Index) == SNMP_SUCCESS)
            {
                do
                {
                    MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);

                    CfaCliConfGetIfName ((UINT4) i4Index,
                                         (INT1 *) &au1IfName[0]);

                    CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

                    DvmrpShowRunningConfigInterfaceDetails (CliHandle, i4Index);

                    CliPrintf (CliHandle, "!\r\n");

                    i4PrevIndex = i4Index;
                }
                while (nmhGetNextIndexDvmrpInterfaceTable (i4PrevIndex,
                                                           &i4Index)
                       == SNMP_SUCCESS);
            }
        }
    }
    else
    {
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DvmrpShowRunningConfigInterfaceDetails             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the currently operating     */
/*                        DVMRP configurations for a particular interface.   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
DvmrpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4RowStatus = 0;

    nmhGetDvmrpInterfaceStatus (i4Index, &i4RowStatus);

    if (i4RowStatus == DVMRP_ROW_STATUS_ACTIVE)
    {
        CliPrintf (CliHandle, " set ip dvmrp enable\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssDvmrpShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DVMRP debug level         */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssDvmrpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    if (gu1DvmrpStatus == DVMRP_DISABLED)
    {
        UNUSED_PARAM (CliHandle);
        return;
    }

    i4DbgLevel = gu4DvmrpLogMask;
    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rDVMRP :");

    if ((i4DbgLevel & DVMRP_NBR_DEBUG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DVMRP Neighbor debugging is on");
    }
    if ((i4DbgLevel & DVMRP_GRP_DEBUG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DVMRP Groups debugging is on");
    }
    if ((i4DbgLevel & DVMRP_JP_DEBUG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DVMRP Join/Prune debugging is on");
    }
    if ((i4DbgLevel & DVMRP_IO_DEBUG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DVMRP Input/Output debugging is on");
    }
    if ((i4DbgLevel & DVMRP_MRT_DEBUG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  DVMRP Multicast routing table debugging is on");
    }
    if ((i4DbgLevel & DVMRP_MDH_DEBUG) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  DVMRP Multicast data handling debugging is on");
    }
    if ((i4DbgLevel & DVMRP_MGMT_DEBUG) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DVMRP Management debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

#endif
