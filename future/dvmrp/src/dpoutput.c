/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpoutput.c,v 1.7 2010/01/07 12:42:56 prabuc Exp $
 *
 * Description:This file contains the function  to send the   
 *             Dvmrp packets to IP                           
 *
 *******************************************************************/
#include "dpinc.h"

/****************************************************************************/
/* Function Name    :  DvmrpOmSendPacket                                    */
/* Description      :  This function Validates the dvmrp packet and sends it*/
/*                     to lower layer                                       */
/* Input (s)        :  u4IfaceIndex - Index into the Interface Table        */
/*                     u4DestAddr   - Destination IP addr                   */
/*                     u4Length     - Length of the packet received         */
/*                     u1Code       - Type of the Protocol Packet           */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#if __STDC__
VOID
DvmrpOmSendPacket (UINT4 u4IfaceIndex, UINT4 u4DestAddr,
                   UINT4 u4Length, UINT1 u1Code)
#else
VOID
DvmrpOmSendPacket (u4IfaceIndex, u4DestAddr, u4Length, u1Code)
     UINT4               u4IfaceIndex;
     UINT4               u4DestAddr;
     UINT4               u4Length;
     UINT1               u1Code;
#endif
{
    UINT2               u2CheckSum = 0;
    UINT2               u2Cap = DVMRP_CAPABILITIES;
    tDvmrpInterfaceNode *pIfNode = NULL;

    /* 
     * Fill the Dvmrp Header 
     */
    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfaceIndex);
    if (NULL == pIfNode)
    {
        LOG1 ((INFO, OM, "Invalid Interface Node"));
        return;
    }
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u1Type = DVMRP_TYPE;
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u1Code = u1Code;
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u2CheckSum = 0;
    /* For Probe packets Capabilities shd be set */
    /* ((tDvmrpHeader *) gpSendBuffer)->u2Reserved = OSIX_HTONS (
       ((u1Code == DVMRP_PROBE) ? DVMRP_CAPABILITIES : 0));      */
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u2Reserved = OSIX_HTONS (u2Cap);
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u1MinorVer = DVMRP_MINOR_VERSION;
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u1MajorVer = DVMRP_MAJOR_VERSION;

    /* 
     * Checksum is calculated for the data bytes in Network order
     * which is again converted to Network order and filled in the
     * outgoing buffer
     */
    u2CheckSum = (UINT2) DvmrpUtilCheckSum (gpSendBuffer, ((INT2) u4Length));
    ((tDvmrpHeader *) (VOID *) gpSendBuffer)->u2CheckSum = (u2CheckSum);

    LOG1 ((INFO, OM, "Dvmrp Header formed successfully"));

    DvmrpIpSendPacket (u4Length, pIfNode->u4IfLocalAddr,
                       u4DestAddr, u4IfaceIndex);

    if (DvmrpMemRelease (gDvmrpMemPool.MiscPoolId, gpSendBuffer) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to release memory block to MemPool"));
    }
    return;
}
