
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: dpnpapi.c,v 1.22 2014/12/18 12:12:25 siva Exp $
 *
 * Description:This file contains the wrapper routines in DVMRP that
 *             in turn invoke NPAPIs and NetIp Apis for updating
 *             multicast routes.
 *
 *******************************************************************/
#ifdef FS_NPAPI
#include "dpinc.h"
#include "nat.h"

INT4
DvmrpNpDeleteRoute (tForwTbl * pFwdEntry)
{
    tMcRtEntry          rtEntry;
    tDpActiveSrcInfo   *pSrcNode = NULL;
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4IfIndex;

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface, &u4IfIndex);

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    pSrcNode = pFwdEntry->pSrcInfo;

    if (DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    /* OwnerId can be used to get the VR_ID. This is used for supporting
     * VR.*/
    while (pSrcNode != NULL)
    {
        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
        Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
        Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u4SrcIpAddr = pSrcNode->u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = 0;
        Ipv4McRouteInfo.pDownStreamIf = NULL;

        if (DvmrpNpWrDelRoute (&Ipv4McRouteInfo) == OSIX_FAILURE)
        {
            LOG1 ((WARNING, FM,
                   "Failure in deleting the Multicast Route in the NP\n"));
            i4Status = DVMRP_NOTOK;
        }
        else
        {
            LOG2 ((INFO, FM, "Deleted Route from the NP\n",
                   pSrcNode->u4SrcAddr));
            LOG2 ((INFO, FM, "Deleted Route from the NP\n",
                   pFwdEntry->u4GroupAddr));
        }
        pSrcNode = pSrcNode->pNext;
    }
    return i4Status;
}

INT4
DvmrpNpDeleteOneRoute (tForwCacheTbl * pFwdCache)
{
    tMcRtEntry          rtEntry;
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tDpActiveSrcInfo   *pSrcNode = NULL;
    tForwTbl           *pFwdEntry = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4IfIndex;

    pFwdEntry = pFwdCache->pSrcNwInfo;
    pSrcNode = pFwdEntry->pSrcInfo;

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    /* OwnerId can be used to get the VR_ID. This is used for supporting
     * VR.*/
    while (pSrcNode != NULL)
    {
        if (pFwdCache->u4SrcAddress == pSrcNode->u4SrcAddr)
        {
            MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
            MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
            Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
            Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
            Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.u4SrcIpAddr = pSrcNode->u4SrcAddr;
            Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
            Ipv4McRouteInfo.u2NoOfDownStreamIf = 0;
            Ipv4McRouteInfo.pDownStreamIf = NULL;

            if (DvmrpNpWrDelRoute (&Ipv4McRouteInfo) == OSIX_FAILURE)

            {
                LOG1 ((WARNING, FM, "Failure in deleting the Multicast Route "
                       "in the NP\n"));
                i4Status = DVMRP_NOTOK;
            }
            else
            {
                LOG2 ((INFO, FM, "Deleted Route (0x%x, 0x%x) from the NP\n",
                       pSrcNode->u4SrcAddr));
                LOG2 ((INFO, FM, " ", pFwdEntry->u4GroupAddr));
            }
            break;
        }
        pSrcNode = pSrcNode->pNext;
    }
    return i4Status;
}

INT4
DvmrpNpAddRoute (tForwTbl * pFwdEntry)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tOutputInterface   *pOifNode = NULL;
    tOutputInterface   *pOif = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4OifCnt = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4IfIndex = 0;
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    pActSrcNode = pFwdEntry->pSrcInfo;
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    rtEntry.u2RpfIf = (UINT2) pFwdEntry->InIface.u4InInterface;
    pOifNode = pFwdEntry->pOutInterface;
    pOif = pOifNode;
    /* check if entry is to be created in HW
     * for NAT MCAST support
     * If NAT_FAILURE (2) is returned pkt should be
     * routed/switched by control plane
     */
    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface, &u4InIndex);
#ifdef NAT_WANTED
    if (NatCheckIfAddMcastEntry (u4InIndex) == NAT_FAILURE)
        return DVMRP_OK;
#endif

    while (pOif != NULL)
    {
        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (pOifNode != NULL)
    {
        pDsIf = DVMRP_MALLOC (sizeof (tMcDownStreamIf) * u4OifCnt,
                              tMcDownStreamIf);
        if (pDsIf == NULL)
        {
            LOG1 ((WARNING, FM, "Failure allocating memory for installing "
                   "entry in NP\n"));
            return DVMRP_NOTOK;
        }
    }

    pTmpDsIf = pDsIf;

    while (pOifNode != NULL)
    {
        if (rtEntry.u2RpfIf == pOifNode->u4OutInterface)
        {
            pOifNode = pOifNode->pNext;
            continue;
        }
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pOifNode->u4OutInterface);
        pTmpDsIf->u2TtlThreshold = 255;
        pTmpDsIf->u4Mtu = pIfNode->u4IfMTUSize;
        u4IfIndex = 0;
        if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                         &u4IfIndex) == NETIPV4_FAILURE)
        {
            LOG2 ((WARNING, FM, "Unable to get Interface index for port %d \n",
                   pOifNode->u4OutInterface));

            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }
        pTmpDsIf->u4IfIndex = u4IfIndex;
        if (DvmrpGetVlanIdFromIfIndex
            (pOifNode->u4OutInterface, &pTmpDsIf->u2VlanId) == DVMRP_NOTOK)
        {
            LOG1 ((WARNING, FM,
                   "Failure in getting VlanId of the interface\n"));
            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }

        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                  pActSrcNode->u4SrcAddr, pTmpDsIf->u2VlanId,
                                  pTmpDsIf->McFwdPortList,
                                  pTmpDsIf->UntagPortList);
        pTmpDsIf++;
        pOifNode = pOifNode->pNext;
    }

    pDsIf = (u4OifCnt == 0) ? NULL : pDsIf;
    if (DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                     &u4IfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId))
    {
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

#ifdef IGS_WANTED
    if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts (rtEntry.u2VlanId,
                                                    pFwdEntry->u4GroupAddr,
                                                    pActSrcNode->u4SrcAddr,
                                                    rtEntry.McFwdPortList,
                                                    rtEntry.UntagPortList))
#else
    if (VLAN_FAILURE == VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList))
#endif
    {
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
    Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
    Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
    Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
    Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u2NoOfDownStreamIf = u4OifCnt;
    Ipv4McRouteInfo.pDownStreamIf = pDsIf;
    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
    if (DvmrpNpWrAddRoute (&Ipv4McRouteInfo) == OSIX_FAILURE)
    {
        LOG1 ((WARNING, FM, "Failure installing entry in NP.\n"));
        i4Status = DVMRP_NOTOK;
    }
    else
    {
        LOG2 ((INFO, FM, "Installed entry (0x%x)", pActSrcNode->u4SrcAddr));
        LOG2 ((INFO, FM, "(0x%x) in NP.\n", pFwdEntry->u4GroupAddr));
    }

    if (pDsIf != NULL)
    {
        DVMRP_MEM_FREE (pDsIf);
    }
    return i4Status;
}

INT4
DvmrpNpDeleteOif (tForwTbl * pFwdEntry, UINT4 u4OifIndex)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf     DsIf;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4InIfIndex = 0;
    UINT4               u4OutIndex = 0;

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&DsIf, 0, sizeof (tMcDownStreamIf));

    pActSrcNode = pFwdEntry->pSrcInfo;
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */

    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                 &u4InIfIndex);
    DVMRP_GET_IFINDEX_FROM_PORT (u4OifIndex, &u4OutIndex);

    rtEntry.u2RpfIf = (UINT2) u4InIfIndex;
    DsIf.u4IfIndex = u4OutIndex;

    if (DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }
    if (DvmrpGetVlanIdFromIfIndex (u4OifIndex, &DsIf.u2VlanId) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    /* Update the portlist for this MC group address */

    while (pActSrcNode != NULL)
    {
        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                  pActSrcNode->u4SrcAddr, DsIf.u2VlanId,
                                  DsIf.McFwdPortList, DsIf.UntagPortList);

        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
        Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
        Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
        Ipv4McRouteInfo.pDownStreamIf = &DsIf;

        if (DvmrpNpWrDelOif (&Ipv4McRouteInfo, pFwdEntry->pOutInterface)
            == OSIX_FAILURE)
        {
            LOG1 ((WARNING, FM, "Failure Deleting Oif From NP.\n"));
            i4Status = DVMRP_NOTOK;
        }
        pActSrcNode = pActSrcNode->pNext;
    }
    return i4Status;
}

INT4
DvmrpNpAddOif (tForwTbl * pFwdEntry, UINT4 u4OifIndex)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tDvmrpInterfaceNode *pIfNode;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf     DsIf;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4InIfIndex = 0;
    UINT4               u4OutIndex = 0;

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&DsIf, 0, sizeof (DsIf));

    if (pFwdEntry->InIface.u4InInterface == u4OifIndex)
    {
        return i4Status;
    }

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4OifIndex);
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    if (DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                     &u4InIfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        return DVMRP_NOTOK;
    }
    rtEntry.u2RpfIf = (UINT2) u4InIfIndex;
    if (DVMRP_GET_IFINDEX_FROM_PORT (u4OifIndex,
                                     &u4OutIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        return DVMRP_NOTOK;
    }

    DsIf.u4IfIndex = u4OutIndex;
    DsIf.u2TtlThreshold = 255;
    DsIf.u4Mtu = pIfNode->u4IfMTUSize;
    if ((DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
         (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId)) ||
        (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex (u4OifIndex, &DsIf.u2VlanId)))
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    pActSrcNode = pFwdEntry->pSrcInfo;

    while (pActSrcNode != NULL)
    {
#ifdef IGS_WANTED
        if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts (rtEntry.u2VlanId,
                                                        pFwdEntry->
                                                        u4GroupAddr,
                                                        pActSrcNode->
                                                        u4SrcAddr,
                                                        rtEntry.
                                                        McFwdPortList,
                                                        rtEntry.UntagPortList))
#else
        if (VLAN_FAILURE == VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                    rtEntry.McFwdPortList,
                                                    rtEntry.UntagPortList))
#endif
        {
            return DVMRP_NOTOK;
        }

        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                  pActSrcNode->u4SrcAddr,
                                  DsIf.u2VlanId, DsIf.McFwdPortList,
                                  DsIf.UntagPortList);

        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
        Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
        Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
        Ipv4McRouteInfo.pDownStreamIf = &DsIf;
        Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
        if (DvmrpNpWrAddOif (&Ipv4McRouteInfo, pFwdEntry->pOutInterface)
            == OSIX_FAILURE)
        {
            LOG1 ((WARNING, FM, "Failure installing entry in NP.\n"));
            i4Status = DVMRP_NOTOK;
        }
        pActSrcNode = pActSrcNode->pNext;
    }

    return i4Status;
}

INT4
DvmrpNpUpdateIif (tForwTbl * pFwdEntry)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    tMcRtEntry          rtEntry;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4IfIndex = 0;

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    if (DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                     &u4IfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        return DVMRP_NOTOK;
    }
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId))
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    pActSrcNode = pFwdEntry->pSrcInfo;

    while (pActSrcNode != NULL)
    {
#ifdef IGS_WANTED
        if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts (rtEntry.u2VlanId,
                                                        pFwdEntry->
                                                        u4GroupAddr,
                                                        pActSrcNode->
                                                        u4SrcAddr,
                                                        rtEntry.
                                                        McFwdPortList,
                                                        rtEntry.UntagPortList))
#else
        if (VLAN_FAILURE == VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                    rtEntry.McFwdPortList,
                                                    rtEntry.UntagPortList))
#endif
        {
            PRINTF (" DvmrpNpUpdateIif: Not able to get FwdPorts for "
                    "VlanId %d GrpAddr %x SrcAddr %x in NP.\n",
                    rtEntry.u2VlanId, pFwdEntry->u4GroupAddr,
                    pActSrcNode->u4SrcAddr);
        }

        MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
        MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
        Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
        Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
        Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
        Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
        Ipv4McRouteInfo.u2NoOfDownStreamIf = 1;
        Ipv4McRouteInfo.pDownStreamIf = NULL;
        Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
        if (DvmrpNpWrUpdateIif (&Ipv4McRouteInfo, pFwdEntry->pOutInterface)
            == OSIX_FAILURE)
        {
            LOG1 ((WARNING, FM, "Failure installing entry in NP.\n"));
            i4Status = DVMRP_NOTOK;
        }
        pActSrcNode = pActSrcNode->pNext;
    }

    return i4Status;
}

INT4
DvmrpNpAddCpuPort (tForwTbl * pFwdEntry)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tOutputInterface   *pOifNode = NULL;
    tOutputInterface   *pOif = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4OifCnt = 0;
    UINT4               u4IfIndex = 0;

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    pActSrcNode = pFwdEntry->pSrcInfo;
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    rtEntry.u2RpfIf = (UINT2) pFwdEntry->InIface.u4InInterface;
    pOifNode = pFwdEntry->pOutInterface;
    pOif = pOifNode;

    while (pOif != NULL)
    {
        if (pOifNode->u4MinTime != 0)
        {
            pOif = pOif->pNext;
            continue;
        }
        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (pOifNode != NULL)
    {
        pDsIf = DVMRP_MALLOC (sizeof (tMcDownStreamIf) * u4OifCnt,
                              tMcDownStreamIf);
        if (pDsIf == NULL)
        {
            LOG1 ((WARNING, FM, "Failure allocating memory for installing "
                   "entry in NP\n"));
            return DVMRP_NOTOK;
        }
    }

    pTmpDsIf = pDsIf;

    while (pOifNode != NULL)
    {
        if ((rtEntry.u2RpfIf == pOifNode->u4OutInterface) ||
            (pOifNode->u4MinTime != 0))
        {
            pOifNode = pOifNode->pNext;
            continue;
        }
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pOifNode->u4OutInterface);
        pTmpDsIf->u2TtlThreshold = 255;
        pTmpDsIf->u4Mtu = pIfNode->u4IfMTUSize;
        u4IfIndex = 0;
        if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                         &u4IfIndex) == NETIPV4_FAILURE)
        {
            LOG2 ((WARNING, FM, "Unable to get Interface index for port %d \n",
                   pOifNode->u4OutInterface));

            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }
        pTmpDsIf->u4IfIndex = u4IfIndex;
        if (DvmrpGetVlanIdFromIfIndex
            (pOifNode->u4OutInterface, &pTmpDsIf->u2VlanId) == DVMRP_NOTOK)
        {
            LOG1 ((WARNING, FM,
                   "Failure in getting VlanId of the interface\n"));
            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }

        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                  pActSrcNode->u4SrcAddr, pTmpDsIf->u2VlanId,
                                  pTmpDsIf->McFwdPortList,
                                  pTmpDsIf->UntagPortList);
        pTmpDsIf++;
        pOifNode = pOifNode->pNext;
    }

    if (DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                     &u4IfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId))
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

#ifdef IGS_WANTED
    if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts (rtEntry.u2VlanId,
                                                    pFwdEntry->u4GroupAddr,
                                                    pActSrcNode->u4SrcAddr,
                                                    rtEntry.McFwdPortList,
                                                    rtEntry.UntagPortList))
#else
    if (VLAN_FAILURE == VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList))
#endif
    {
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
    Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
    Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
    Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
    Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u2NoOfDownStreamIf = u4OifCnt;
    Ipv4McRouteInfo.pDownStreamIf = pDsIf;
    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
    if (DvmrpNpWrUpdateCpuPort (&Ipv4McRouteInfo, DVMRP_ADD_CPUPORT)
        == OSIX_FAILURE)
    {
        LOG1 ((WARNING, FM, "Failure in Adding CPU port in NP.\n"));
        i4Status = DVMRP_NOTOK;
    }
    else
    {
        LOG2 ((INFO, FM, "Installed CPU port entry (0x%x)",
               pActSrcNode->u4SrcAddr));
        LOG2 ((INFO, FM, "(0x%x) in NP.\n", pFwdEntry->u4GroupAddr));
    }

    if (pDsIf != NULL)
    {
        DVMRP_MEM_FREE (pDsIf);
    }
    return i4Status;
}

INT4
DvmrpNpDeleteCpuPort (tForwTbl * pFwdEntry)
{
    tIpv4McRouteInfo    Ipv4McRouteInfo;
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tDpActiveSrcInfo   *pActSrcNode = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tOutputInterface   *pOifNode = NULL;
    tOutputInterface   *pOif = NULL;
    INT4                i4Status = DVMRP_OK;
    UINT4               u4OifCnt = 0;
    UINT4               u4IfIndex = 0;

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    pActSrcNode = pFwdEntry->pSrcInfo;
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);
    rtEntry.u2RpfIf = (UINT2) pFwdEntry->InIface.u4InInterface;
    pOifNode = pFwdEntry->pOutInterface;
    pOif = pOifNode;

    while (pOif != NULL)
    {
        if (pOifNode->u4MinTime != 0)
        {
            pOif = pOif->pNext;
            continue;
        }
        u4OifCnt++;
        pOif = pOif->pNext;
    }

    if (pOifNode != NULL)
    {
        pDsIf = DVMRP_MALLOC (sizeof (tMcDownStreamIf) * u4OifCnt,
                              tMcDownStreamIf);
        if (pDsIf == NULL)
        {
            LOG1 ((WARNING, FM, "Failure allocating memory for installing "
                   "entry in NP\n"));
            return DVMRP_NOTOK;
        }
    }

    pTmpDsIf = pDsIf;

    while (pOifNode != NULL)
    {
        if ((rtEntry.u2RpfIf == pOifNode->u4OutInterface) ||
            (pOifNode->u4MinTime != 0))
        {
            pOifNode = pOifNode->pNext;
            continue;
        }
        pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pOifNode->u4OutInterface);
        pTmpDsIf->u2TtlThreshold = 255;
        pTmpDsIf->u4Mtu = pIfNode->u4IfMTUSize;
        u4IfIndex = 0;
        if (DVMRP_GET_IFINDEX_FROM_PORT (pOifNode->u4OutInterface,
                                         &u4IfIndex) == NETIPV4_FAILURE)
        {
            LOG2 ((WARNING, FM, "Unable to get Interface index for port %d \n",
                   pOifNode->u4OutInterface));
            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }
        pTmpDsIf->u4IfIndex = u4IfIndex;
        if (DvmrpGetVlanIdFromIfIndex
            (pOifNode->u4OutInterface, &pTmpDsIf->u2VlanId) == DVMRP_NOTOK)
        {
            LOG1 ((WARNING, FM,
                   "Failure in getting VlanId of the interface\n"));
            if (pDsIf != NULL)
            {
                DVMRP_MEM_FREE (pDsIf);
            }
            return DVMRP_NOTOK;
        }

        DvmrpGetMcastFwdPortList (pFwdEntry->u4GroupAddr,
                                  pActSrcNode->u4SrcAddr, pTmpDsIf->u2VlanId,
                                  pTmpDsIf->McFwdPortList,
                                  pTmpDsIf->UntagPortList);
        pTmpDsIf++;
        pOifNode = pOifNode->pNext;
    }

    if (DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface,
                                     &u4IfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

    rtEntry.u2RpfIf = (UINT2) u4IfIndex;
    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
        (pFwdEntry->InIface.u4InInterface, &rtEntry.u2VlanId))
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

#ifdef IGS_WANTED
    if (SNOOP_FAILURE == SnoopUtilGetMcIgsFwdPorts (rtEntry.u2VlanId,
                                                    pFwdEntry->u4GroupAddr,
                                                    pActSrcNode->u4SrcAddr,
                                                    rtEntry.McFwdPortList,
                                                    rtEntry.UntagPortList))
#else
    if (VLAN_FAILURE == VlanGetVlanMemberPorts (rtEntry.u2VlanId,
                                                rtEntry.McFwdPortList,
                                                rtEntry.UntagPortList))
#endif
    {
        if (pDsIf != NULL)
        {
            DVMRP_MEM_FREE (pDsIf);
        }
        return DVMRP_NOTOK;
    }

    MEMSET (&Ipv4McRouteInfo, 0, sizeof (Ipv4McRouteInfo));
    MEMCPY (&Ipv4McRouteInfo.rtEntry, &rtEntry, sizeof (tMcRtEntry));
    Ipv4McRouteInfo.u4VrId = DVMRP_RTR_ID;
    Ipv4McRouteInfo.u4GrpAddr = pFwdEntry->u4GroupAddr;
    Ipv4McRouteInfo.u4GrpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u4SrcIpAddr = pActSrcNode->u4SrcAddr;
    Ipv4McRouteInfo.u4SrcIpPrefix = FNP_ALL_BITS_SET;
    Ipv4McRouteInfo.u2NoOfDownStreamIf = u4OifCnt;
    Ipv4McRouteInfo.pDownStreamIf = pDsIf;
    Ipv4McRouteInfo.u1CallerId = IPMC_MRP;
    if (DvmrpNpWrUpdateCpuPort (&Ipv4McRouteInfo, DVMRP_DELETE_CPUPORT)
        == OSIX_FAILURE)
    {
        LOG1 ((WARNING, FM, "Failure in deleting CPU port in NP.\n"));
        i4Status = DVMRP_NOTOK;
    }
    else
    {
        LOG2 ((INFO, FM, "Removed CPU port entry (0x%x)",
               pActSrcNode->u4SrcAddr));
        LOG2 ((INFO, FM, "(0x%x) in NP.\n", pFwdEntry->u4GroupAddr));
    }

    if (pDsIf != NULL)
    {
        DVMRP_MEM_FREE (pDsIf);
    }
    return i4Status;
}

INT4
DvmrpNpGetHitBitStatus (tForwCacheTbl * pFwdCache)
{
    UINT4               i4HitStatus = DVMRP_NOTOK;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Iif = 0;
    UINT4               u4IfIndex = 0;
    UINT2               u2VlanId;
    tForwTbl           *pFwdTbl = NULL;

    u4SrcAddr = pFwdCache->u4SrcAddress;
    u4GrpAddr = pFwdCache->u4DestGroup;
    pFwdTbl = pFwdCache->pSrcNwInfo;
    u4Iif = pFwdTbl->InIface.u4InInterface;

    if (DVMRP_GET_IFINDEX_FROM_PORT (u4Iif, &u4IfIndex) == NETIPV4_FAILURE)
    {
        LOG1 ((WARNING, FM, "Unable to get Interface index from port \n"));
        return DVMRP_NOTOK;
    }
    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex (u4Iif, &u2VlanId))
    {
        LOG1 ((WARNING, FM, "Failure in getting VlanId of the interface\n"));
        return DVMRP_NOTOK;
    }

    IpmcFsNpIpv4McGetHitStatus (u4SrcAddr, u4GrpAddr, u4Iif, u2VlanId,
                            &i4HitStatus);
    return i4HitStatus;
}

VOID
DvmrpHandlePbmpChgEvent (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIpmcMcastVlanPbmpData PbmpData;
    tMcRtEntry          rtEntry;
    UINT4               u4VlanIfIndex;
    tMcDownStreamIf     downStreamIf;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tForwCacheTbl      *pFwdCache = NULL;
    tForwTbl           *pForwTbl = NULL;
    tMacAddr            MacAddr;
    tOutputInterface   *pOutList = NULL;
    UINT4               u4Key = 0;

    MEMSET (&downStreamIf, 0, sizeof (tMcDownStreamIf));

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PbmpData, 0,
                               sizeof (tIpmcMcastVlanPbmpData));

    CRU_BUF_RELEASE_CHAIN (pBuf);
    pBuf = NULL;
    u4VlanIfIndex = CfaGetVlanInterfaceIndex (PbmpData.VlanId);
    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        LOG1 ((INFO, IHM, "Invalid VLANID passed for the PBMP change "));
        return;
    }
    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4VlanIfIndex);
    DVMRP_HASH_SCAN_TABLE (gpForwHashTable, u4Key)
    {

        DVMRP_HASH_SCAN_BUCKET (gpForwHashTable, u4Key, pFwdCache,
                                tForwCacheTbl *)
        {

            UtilConvMcastIP2Mac (pFwdCache->u4DestGroup, MacAddr);
            if (MEMCMP (MacAddr, PbmpData.MacAddr, MAC_ADDR_LEN) == 0)
            {
                pForwTbl = pFwdCache->pSrcNwInfo;
                if (pForwTbl == NULL)
                {
                    LOG1 ((CRITICAL, FM,
                           "This should not happen Invalid Forward cache entry. "));
                    continue;
                }
                FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

                if (pForwTbl->InIface.u4InInterface == u4VlanIfIndex)
                {
                    rtEntry.u2RpfIf = (UINT2) u4VlanIfIndex;
                    if (DVMRP_NOTOK == DvmrpGetVlanIdFromIfIndex
                        (pForwTbl->InIface.u4InInterface, &rtEntry.u2VlanId))
                    {
                        LOG1 ((WARNING, FM,
                               "Failure in getting VlanId of the interface\n"));
                        return;
                    }

                    DvmrpGetMcastFwdPortList (pFwdCache->u4DestGroup,
                                              pFwdCache->u4SrcAddress,
                                              rtEntry.u2VlanId,
                                              rtEntry.McFwdPortList,
                                              rtEntry.UntagPortList);
                    L2IwfGetVlanEgressPorts (rtEntry.u2VlanId,
                                             rtEntry.VlanEgressPortList);

                    IpmcFsNpIpv4McUpdateIifVlanEntry (0, pFwdCache->u4DestGroup,
                                                  pFwdCache->u4SrcAddress,
                                                  rtEntry);
                }
                else
                {
                    pOutList = pForwTbl->pOutInterface;
                    while (pOutList != NULL)
                    {
                        if (pOutList->u4OutInterface == u4VlanIfIndex)
                        {
                            rtEntry.u2RpfIf = pForwTbl->InIface.u4InInterface;
                            downStreamIf.u4IfIndex = u4VlanIfIndex;

                            if ((DvmrpGetVlanIdFromIfIndex
                                 (pForwTbl->InIface.u4InInterface,
                                  &rtEntry.u2VlanId) == DVMRP_NOTOK) ||
                                (DvmrpGetVlanIdFromIfIndex
                                 (pOutList->u4OutInterface,
                                  &downStreamIf.u2VlanId) == DVMRP_NOTOK))
                            {
                                LOG1 ((WARNING, FM,
                                       "Failure in getting VlanId of the interface\n"));
                                return;
                            }

                            DvmrpGetMcastFwdPortList (pFwdCache->u4DestGroup,
                                                      pFwdCache->u4SrcAddress,
                                                      downStreamIf.u2VlanId,
                                                      downStreamIf.
                                                      McFwdPortList,
                                                      downStreamIf.
                                                      UntagPortList);
                            L2IwfGetVlanEgressPorts (downStreamIf.u2VlanId,
                                                     downStreamIf.
                                                     VlanEgressPortList);
                            downStreamIf.u2TtlThreshold = 255;
                            downStreamIf.u4Mtu = pIfNode->u4IfMTUSize;

                            IpmcFsNpIpv4McUpdateOifVlanEntry (0,
                                                          pFwdCache->
                                                          u4DestGroup,
                                                          pFwdCache->
                                                          u4SrcAddress, rtEntry,
                                                          downStreamIf);
                            break;

                        }
                        pOutList = pOutList->pNext;
                    }

                }

            }

        }
    }
    return;
}

UINT4
DvmrpGetMcastFwdPortList (UINT4 u4GrpAddr, UINT4 u4SrcAddr, tVlanId u2VlanId,
                          tPortList McFwdPortList, tPortList UntagPortList)
{
    tMacAddr            au1MacAddr;

    /* Convert GroupAddr to MacAddr */
    UtilConvMcastIP2Mac (u4GrpAddr, au1MacAddr);

    MEMSET (McFwdPortList, 0, sizeof (tPortList));
    MEMSET (UntagPortList, 0, sizeof (tPortList));

#ifdef IGS_WANTED
    /* Call IGS func here to update PortList. It will inturn call
     * VlanIgsGetTxPortList based on Igs forwarding mode
     */
    if (SNOOP_FAILURE ==
        SnoopUtilGetMcIgsFwdPorts (u2VlanId, u4GrpAddr, u4SrcAddr,
                                   McFwdPortList, UntagPortList))
#else
    UNUSED_PARAM (u4SrcAddr);
    if (VLAN_FAILURE == VlanGetVlanMemberPorts (u2VlanId,
                                                McFwdPortList, UntagPortList))
#endif
    {
        return DVMRP_NOTOK;
    }

    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMRoutePktCount                 */
/*  Description     : The function get the number of packets   */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pFwdCache - Route node                   */
/*  Output(s)       : pu4IpMRoutePkts - packets received       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMRoutePktCount (tForwCacheTbl * pFwdCache, UINT4 *pu4IpMRoutePkts)
{

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetRouteStats (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                                NETIPMC_ROUTE_PKTS, pu4IpMRoutePkts)
        == NETIPV4_SUCCESS)
    {
        return DVMRP_OK;
    }
    else
    {
        return DVMRP_NOTOK;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (DVMRP_RTR_ID, pFwdCache->u4DestGroup,
                                pFwdCache->u4SrcAddress, IPV4MROUTE_PKTS,
                                pu4IpMRoutePkts) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMRouteDifferentInIfPktCount    */
/*  Description     : This function gets the packets which     */
/*                    router has received from source and      */
/*                    multicast group which were dropped       */
/*                    because they were not received on the    */
/*                    expected incomming interface             */
/*  Input(s)        : pFwdCache - Route node                   */
/*  Output(s)       : pu4DifferentInIfPackets -                */
/*                    Diffrent if in pkts.                     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMRouteDifferentInIfPktCount (tForwCacheTbl * pFwdCache,
                                       UINT4 *pu4DifferentInIfPackets)
{
#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetRouteStats (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                                NETIPMC_ROUTE_DIFF_IF_IN_PKTS,
                                pu4DifferentInIfPackets) == NETIPV4_SUCCESS)
    {
        return DVMRP_OK;
    }
    else
    {
        return DVMRP_NOTOK;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (DVMRP_RTR_ID, pFwdCache->u4DestGroup,
                                pFwdCache->u4SrcAddress,
                                IPV4MROUTE_DIFF_IF_IN_PKTS,
                                pu4DifferentInIfPackets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMRouteOctetCount               */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pFwdCache - Route node                   */
/*  Output(s)       : pu4IpMRouteOctets - Octets received      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMRouteOctetCount (tForwCacheTbl * pFwdCache, UINT4 *pu4IpMRouteOctets)
{

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetRouteStats (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                                NETIPMC_ROUTE_OCTETS, pu4IpMRouteOctets)
        == NETIPV4_SUCCESS)
    {
        return DVMRP_OK;
    }
    else
    {
        return DVMRP_NOTOK;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (DVMRP_RTR_ID, pFwdCache->u4DestGroup,
                                pFwdCache->u4SrcAddress,
                                IPV4MROUTE_OCTETS, pu4IpMRouteOctets)
        == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMRouteHCOctetCount             */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group. This is 64-bit counter  */
/*  Input(s)        : pFwdCache - Route node                   */
/*  Output(s)       : pu4IpMRouteHCOctets - Octets received.   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMRouteHCOctetCount (tForwCacheTbl * pFwdCache,
                              tSNMP_COUNTER64_TYPE * pu8IpMRouteHCOctets)
{
    if (IpmcFsNpIpv4GetMRouteHCStats (DVMRP_RTR_ID, pFwdCache->u4DestGroup,
                                  pFwdCache->u4SrcAddress, IPV4MROUTE_HCOCTETS,
                                  pu8IpMRouteHCOctets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMNextHopPktCount               */
/*  Description     : The function gets number of packets      */
/*                    which have been forwarded on this route  */
/*                    on particular interface                  */
/*  Input(s)        : pFwdCache - Route node                   */
/*                    i4OifIndex -Out interface index          */
/*  Output(s)       : pu4NextHopPkts - Packets received.       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMNextHopPktCount (tForwCacheTbl * pFwdCache, INT4 i4OifIndex,
                            UINT4 *pu4NextHopPkts)
{
    INT4                i4IfIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4OifIndex, (UINT4 *) &i4IfIndex);

    if (IpmcFsNpIpv4GetMNextHopStats (DVMRP_RTR_ID, pFwdCache->u4DestGroup,
                                  pFwdCache->u4SrcAddress, i4IfIndex,
                                  IPV4MNEXTHOP_PKTS, pu4NextHopPkts)
        == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMIfInOctetCount                */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have arrived on the interface       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4InOctets - Octets received on Iface   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMIfInOctetCount (INT4 i4IfIndex, UINT4 *pu4InOctets)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_IN_OCTETS,
                                pu4InOctets) == NETIPV4_SUCCESS)
    {
        return DVMRP_OK;
    }
    else
    {
        return DVMRP_NOTOK;
    }
#endif

    if (IpmcFsNpIpv4GetMIfaceStats (DVMRP_RTR_ID, i4CfaIndex,
                                IPV4MIFACE_IN_MCAST_PKTS,
                                pu4InOctets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMIfOutOctetCount               */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have sent on the interface          */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4OutOctets - Octets sent on Iface      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMIfOutOctetCount (INT4 i4IfIndex, UINT4 *pu4OutOctets)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_OUT_OCTETS,
                                pu4OutOctets) == NETIPV4_SUCCESS)
    {
        return DVMRP_OK;
    }
    else
    {
        return DVMRP_NOTOK;
    }
#endif

    if (IpmcFsNpIpv4GetMIfaceStats (DVMRP_RTR_ID, i4CfaIndex,
                                IPV4MIFACE_OUT_MCAST_PKTS,
                                pu4OutOctets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMIfHCInOctetCount              */
/*  Description     : Gets number of octets of  multicast pkts */
/*                    that have arrived on the interface       */
/*                    This is 64-bit counter.                  */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4InHCOctets - Octets received on Iface */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMIfHCInOctetCount (INT4 i4IfIndex,
                             tSNMP_COUNTER64_TYPE * pu8HCInOctets)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4GetMIfaceHCStats (DVMRP_RTR_ID, i4CfaIndex,
                                  IPV4MIFACE_HCIN_MCAST_PKTS,
                                  pu8HCInOctets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpGetMIfHCOutOctetCount             */
/*  Description     : The function get number of octets of     */
/*                    multicast packets that have sent on the  */
/*                    interface. This is 64-bit counter.       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4OutHCOctets - Octets sent on interface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpGetMIfHCOutOctetCount (INT4 i4IfIndex,
                              tSNMP_COUNTER64_TYPE * pu8HCOutOctets)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4GetMIfaceHCStats (DVMRP_RTR_ID, i4CfaIndex,
                                  IPV4MIFACE_HCOUT_MCAST_PKTS,
                                  pu8HCOutOctets) == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpSetMIfaceTtlTreshold              */
/*  Description     : This function sets the multicast TTL     */
/*                    threshold of the interface               */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : i4TtlTreshold -TTL threshold of the Iface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpSetMIfaceTtlTreshold (INT4 i4IfIndex, INT4 i4TtlTreshold)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4SetMIfaceTtlTreshold (DVMRP_RTR_ID, i4CfaIndex, i4TtlTreshold)
        == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************/
/*  Function Name   : DvmrpNpSetMIfaceRateLimit                */
/*  Description     : This function sets the multicast rate    */
/*                    limit value for interface                */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : i4RateLimit - Rate limit value.          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : DVMRP_OK/DVMRP_NOTOK                     */
/***************************************************************/
INT4
DvmrpNpSetMIfaceRateLimit (INT4 i4IfIndex, INT4 i4RateLimit)
{
    INT4                i4CfaIndex;

    DVMRP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4SetMIfaceRateLimit (DVMRP_RTR_ID, i4CfaIndex, i4RateLimit)
        == FNP_FAILURE)
    {
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

#endif
