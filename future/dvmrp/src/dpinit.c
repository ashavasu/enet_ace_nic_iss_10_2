/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpinit.c,v 1.13 2013/12/16 15:27:56 siva Exp $
 *
 * Description:Contains function for handling Host Membership 
 *             Information from IGMP module                   
 *
 *******************************************************************/
#include "dpinc.h"
#include "dpglob.h"

VOID                TaskInit (VOID);

#ifdef FUTURE_SNMP_WANTED
#include "include.h"
#include "fsdvmrp_snmpmbdb.h"
INT4 RegisterFSDVMRPwithFutureSNMP PROTO ((void));
extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));
#endif /* FUTURE_SNMP_WANTED */

/***************************************************************************
 * Function Name    :  DvmrpSpawnTask
 *
 * Description      :  This function creates a DVMRP task
 *
 * Global Variables
 * Referred         :  None
 *
 * Gobal Variables
 * Modified         :  gu4DvmrpTaskId
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  DVMRP_OK
 *                     DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpSpawnTask (void)
{
    if (OsixTskCrt ((UINT1 *) DVMRP_TASK_NAME, DVMRP_TASK_PRIORITY,
                    DVMRP_STACK_SIZE, (OsixTskEntry) DvmrpTaskMain,
                    0, &gu4DvmrpTaskId) != OSIX_SUCCESS)
    {
        LOG1 ((WARNING, IHM, "Unable to Create DVMRP Task"));
        return DVMRP_NOTOK;
    }
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    : DvmrpInit
 *
 * Description      : Creates Global memory pool ,
 *                    Creates the memory pool for the first component, 
 *                    registers with the lower layer protocol IP and IGMP, 
 *                    loads the default configuration  
 *
 * Global Variables 
 * Referred         : None
 *
 * Global Variables 
 * Modified         : None
 *              
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : DVMRP_OK
                      DVMRP_NOTOK
 ****************************************************************************/
INT4
DvmrpInit ()
{

    DvmrpLoadDefaultConfigs ();

    if (DvmrpGlobalMemInit () == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to Create MemPools"));
        gu1DvmrpStatus = DVMRP_DISABLED;
        return (DVMRP_NOTOK);

    }
    DvmrpGlobalTableInit ();

    if (DvmrpGlobalMemAllocate () == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to Create MemPools"));
        gu1DvmrpStatus = DVMRP_DISABLED;
        return (DVMRP_NOTOK);
    }

    if (DvmrpInitHashTables () == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to Initialize the Hash Tables"));
        return (DVMRP_NOTOK);
    }

    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    : DvmrpInitHashTables
 *
 * Description      : This function intialises the Foward and Route HashTables
 *
 * Global Variables 
 * Referred         : None
 *
 * Global Variables 
 * Modified         : gpForwHashTable and gpRouteHashTable
 *              
 * Input (s)        : None
 *
 * Output (s)       : None
 *
 * Returns          : DVMRP_OK
                      DVMRP_NOTOK
 ****************************************************************************/
INT4
DvmrpInitHashTables ()
{
    /* Initialization of Forward Hash Table */

    if ((gpForwHashTable = (tDVMRP_HASH_TABLE *)
         DVMRP_HASH_CREATE_TABLE (MAX_HASH_TABLE_ENTRIES, NULL, TRUE)) == NULL)
    {

        LOG1 ((WARNING, IHM, "Unable to create ForwHashTable"));
        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Forward Hash Table Initialized"));

    /* Initialization of Route Hash Table */

    if ((gpRouteHashTable = (tDVMRP_HASH_TABLE *)
         DVMRP_HASH_CREATE_TABLE (MAX_HASH_TABLE_ENTRIES, NULL, TRUE)) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to create RouteHashTable"));
        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Route Hash Table Initialized"));
    return (DVMRP_OK);
}

/***************************************************************************
 * Function Name    :  DvmrpLoadDefaultConfigs
 *
 * Description      :  Loads DVMRP configurable objects with default values 
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
DvmrpLoadDefaultConfigs (VOID)
{

    /* Initialize the global variables */

    gpSendBuffer = NULL;
    gpRcvBuffer = NULL;
    gpRcvBufferRead = NULL;

    gu4GenerationId = 0;
    gu4NumRoutes = 0;
    gu4NumReachableRoutes = 0;
    gDpConfigParams.u2PruneLifeTime = DVMRP_DEFAULT_PRUNE_LIFE_TIME;
    gDvmrpSystemSize.u4DvmrpMaxSources = DVMRP_MAX_SOURCES;
#ifdef LNXIP4_WANTED
    gDpConfigParams.i4DvmrpSockId = -1;
#endif
    return;
}

/***************************************************************************
 * Function Name    :  DvmrpInitInterfaceInfo 
 *
 * Description      :  Loads DVMRP configurable objects with default values
 *
 * Global Variables
 * Referred         :  gDvmrpInterfaceTbl 
 *                     gDpConfigParams.u4IfHashSize
 *
 * Global Variables
 * Modified         :  gDvmrpInterfaceTbl
 *                     gDpConfigParams.u4IfHashSize
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
DvmrpInitInterfaceInfo (VOID)
{
    UINT4               u4HashTableSize = DVMRP_ZERO;

    if (DVMRP_MAX_INTERFACES > DVMRP_MAX_IFACE_HASH_BUCKETS)
    {
        u4HashTableSize = DVMRP_MAX_IFACE_HASH_BUCKETS;
    }
    else
    {
        u4HashTableSize = DVMRP_MAX_INTERFACES;
    }
    gDvmrpIfInfo.u4HashSize = u4HashTableSize;

    gDvmrpIfInfo.IfHashTbl = TMO_HASH_Create_Table (u4HashTableSize,
                                                    DvmrpIfNodeAddCriteria,
                                                    TRUE);
    if (gDvmrpIfInfo.IfHashTbl == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Create Hash Table for interface"));
        gDvmrpIfInfo.u4HashSize = 0;
        return;
    }
    TMO_SLL_Init (&gDvmrpIfInfo.IfGetNextList);

    return;
}

/****************************************************************************
 * Function Name         : DvmrpIfNodeAddCriteria
 *                                        
 * Description           : This function is the call back function provided
 *                         to FSAP2 for inserting the interface node bucket 
 *                         list of the hash table in the assending order.
 *                         
 *                                        
 * Input (s)             : pCurNode - The Current node in the hash table with 
 *                         which the Interface Index is to be compared.
 *                         pu1IfIndex - The index of the interface that is to
 *                         be newly added to the bucket.
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : return INSERT_PRIORTO if the current node ifindex
 *                         value is greater than the value to be inserted.
 *                         returns MATCH_NOT_FOUND other wise
 ****************************************************************************/

UINT4
DvmrpIfNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex)
{
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = (tDvmrpInterfaceNode *) (VOID *) pCurNode;
    if (pIfNode->u4IfIndex > (*((UINT4 *) (VOID *) pu1IfIndex)))
    {
        return INSERT_PRIORTO;
    }
    return MATCH_NOT_FOUND;
}

#ifdef FUTURE_SNMP_WANTED
INT4
RegisterFSDVMRPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & fsdvmrp_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & fsdvmrp_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & fsdvmrp_FMAS_MIBObjectTable,
         fsdvmrp_FMAS_Global_data, (INT4) fsdvmrp_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

#endif /* FUTURE_SNMP_WANTED */

/*****************************************************************************/
/* Function Name      : DvmrpMutexLock                                       */
/*                                                                           */
/* Description        : This function is used to take the DVMRP mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
DvmrpMutexLock (VOID)
{
    if (OsixSemTake (DVMRP_MUTEX_SEMID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DvmrpUnMutexLock                                     */
/*                                                                           */
/* Description        : This function is used to give the DVMRP mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
DvmrpMutexUnLock (VOID)
{
    OsixSemGive (DVMRP_MUTEX_SEMID);
    return SNMP_SUCCESS;
}
