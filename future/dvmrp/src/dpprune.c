/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpprune.c,v 1.8 2014/04/04 10:04:43 siva Exp $
 *
 * Description:Contains functions for handling incoming DVMRP 
 *             Prune Messages and generate outgoing Prune     
 *             Messages                                       
 *
 *******************************************************************/
#include "dpinc.h"

/* Prototypes  */
PRIVATE void DvmrpPmFormPrune ARG_LIST ((UINT4, UINT4, UINT4, UINT4));

/****************************************************************************/
/* Function Name    :  DvmrpPmHandlePrunePacket                             */
/* Description      :  This function handles the DVMRP Prune                */
/*                     packet ( which is received in a global variable)     */
/*                     and sends it to the upstream Neighbor if required    */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the input interface.    */
/*                     2. u4Len - Size of the received packet               */
/*                     3. u4NbrTbl - Pointer to the Neighbor table entry    */
/*                        which initiated the Graft packet                  */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpPmHandlePrunePacket (UINT4 u4IfId, UINT4 u4Len, tNbrTbl * pNbrTbl)
#else
void
DvmrpPmHandlePrunePacket (u4IfId, u4Len, pNbrTbl)
     UINT4               u4IfId, u4Len;
     tNbrTbl            *pNbrTbl;
#endif
{

    UINT1              *pDvmrpdata, u1Flag = DVMRP_FALSE;
    UINT4               u4SrcAddr,
        u4GroupAddr, u4NbrAddr, u4Index, u4PruneLifeTime, u4PruneMinTime = 0;
    tForwCacheTbl      *pFwdCache;
    tNbrPruneList      *pTmpNbr, *pNbr;
    tOutputInterface   *pOutList = NULL;
    tOutputInterface   *pTempOutList = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfId);
    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, PM, "Invalid Interface Node"));
        return;
    }
    if (u4Len != PRUNE_PKT_SIZE)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, PM, "Invalid Packet Size"));
        LOG2 ((INFO, PM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, PM, "Interface Stats ", pIfNode->u4IfRcvBadPkts));
        return;
    }

    pDvmrpdata = gpRcvBuffer;

    u4SrcAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) pDvmrpdata));
    u4GroupAddr = OSIX_NTOHL (*((UINT4 *) (VOID *) (pDvmrpdata + IP_ADDR_LEN)));
    u4PruneLifeTime = OSIX_NTOHL (*((UINT4 *) (VOID *)
                                    (pDvmrpdata + IP_ADDR_LEN * 2)));
    u4NbrAddr = pNbrTbl->u4NbrIpAddress;

    if (VALIDATE_UCAST_ADDR (u4SrcAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, PM, "Invalid Source Address"));
        LOG2 ((INFO, PM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, PM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    if (VALIDATE_MCAST_ADDR (u4GroupAddr) == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, PM, "Invalid Group Address"));
        LOG2 ((INFO, PM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, PM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    if (u4PruneLifeTime == 0)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((WARNING, PM, "Invalid PruneLifeTime "));
        LOG2 ((INFO, PM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, PM, "Interface Stats", pIfNode->u4IfRcvBadPkts));
        return;
    }

    pFwdCache =
        (tForwCacheTbl *) (VOID *) DvmrpUtilGetCacheEntry (u4SrcAddr,
                                                           u4GroupAddr,
                                                           DVMRP_FORW_HASH);

    if (pFwdCache == NULL)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((INFO, PM, "Non existing Source and Destination addresses"));
        return;
    }

    if (pFwdCache->pSrcNwInfo == NULL)
    {
        LOG1 ((INFO, PM, "Forward Table entry not present"));
        return;
    }
    /* Changed Hash define to PRUNE_LIFE_TIME from
     * DEFAULT_PRUNE_TIMER_VAL. Changed in dpfwd.c and dpprune.c wherever
     * this hash was being made used for calculating Prune minimum time
     */
    u4PruneMinTime = PRUNE_LIFE_TIME;
    pOutList = pFwdCache->pSrcNwInfo->pOutInterface;

    while (pOutList != NULL)
    {

        if (pOutList->u4OutInterface == u4IfId)
        {
            pNbr = pOutList->pDepNbrs;
            DVMRP_SET_NEIGHBOR_PRUNETIME (pNbr, u4NbrAddr, u4PruneLifeTime, PM);

            DVMRP_FIND_NEIGHBOR_PRUNETIME (pNbr, u4PruneMinTime);
            DVMRP_SEARCH_FOR_HOST (pFwdCache->pSrcNwInfo->u4GroupAddr, u4IfId,
                                   u1Flag);

            if (u1Flag == DVMRP_FALSE)
            {
                pOutList->u4MinTime = u4PruneMinTime;
            }
            break;
        }
        pOutList = pOutList->pNext;
    }

    if (pOutList == NULL)
    {
        pNbrTbl->u4NbrRcvBadPkts++;
        pIfNode->u4IfRcvBadPkts++;
        LOG1 ((INFO, PM, "Invalid Input interface"));
        LOG2 ((INFO, PM, "Neighbor Stats ", pNbrTbl->u4NbrRcvBadPkts));
        LOG2 ((INFO, PM, "Interface Stats ", pIfNode->u4IfRcvBadPkts));
        return;
    }
    LOG2 ((INFO, PM,
           "Forward entry with Source Network address updated",
           pFwdCache->pSrcNwInfo->u4SrcNetwork));
    LOG2 ((INFO, PM, "Forward Entry with Destination address updated",
           pFwdCache->pSrcNwInfo->u4GroupAddr));

    pTempOutList = pOutList;
    u4PruneMinTime = PRUNE_LIFE_TIME;
    pOutList = pFwdCache->pSrcNwInfo->pOutInterface;

    /*  
     * Calculate the Prunelife time to be sent to upstream if required  
     */
    DVMRP_FIND_INTERFACE_PRUNETIME (pOutList, u4PruneMinTime);

    /*  
     * A Prune has to be sent Upstream  
     */
    if (u4PruneMinTime > 0)
    {
        DvmrpPmSendPruneUpstream (pFwdCache, u4PruneMinTime);
    }
    else
    {
        LOG1 ((INFO, PM, "Prune Not Sent Upstream"));
    }
    if (pTempOutList->u4MinTime != 0)
    {
        DvmrpMfwdDeleteOif (pFwdCache->pSrcNwInfo,
                            pTempOutList->u4OutInterface);
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmSendPruneOnHMReport                           */
/* Description      :  This function sends a Prune to the upstream Neighbor */
/*                     if required based on the Host Leave report received  */
/*                     for a Multicast group.                               */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id of the local interface.    */
/*                     2. u4GroupAddr - Multicast Address of the Group for  */
/*                        which there are no hosts.                         */
/* Output (s)       :  None                                                 */
/* Returns          :  DVMRP_OK - On Success                                */
/*                     DVMRP_NOTOK - On Failure                             */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpPmSendPruneOnHmReport (UINT4 u4IfId, UINT4 u4GroupAddr)
#else
void
DvmrpPmSendPruneOnHmReport (u4IfId, u4GroupAddr)
     UINT4               u4IfId, u4GroupAddr;
#endif
{
    UINT4               u4Index, u4PruneLifeTime;
    tForwTbl           *pForwTbl;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }

        if ((DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index))->u4DestGroup
            == u4GroupAddr)
        {

            /*  
             * Remove interface in the Output interface list  
             */
            /* Added last parameter */
            u4PruneLifeTime =
                DvmrpFmDeleteIfList (pForwTbl, u4IfId, HOST_LEAVE);
            if ((DvmrpMfwdDeleteOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
            {
                LOG1 ((INFO, PM, "Uanble to Delete Oif From MFWD"));
            }
            if (u4PruneLifeTime > 0)
            {
                /*  
                 * No other go. A Prune has to be sent upstream  
                 */
                DvmrpPmSendPruneUpstream (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                          (u4Index), u4PruneLifeTime);
            }
            else
            {
                LOG1 ((INFO, PM, "Prune Not Sent Upstream"));
            }
        }
    }                            /* end for */
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmSendPruneOnIfaceChange                        */
/* Description      :  This function sends a Prune to the upstream Neighbor */
/*                     if required, based on change in Route information for*/
/*                     a Source network or when interface goes down.        */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id on which Route change has  */
/*                        changed                                           */
/*                     2. u4SrcNetwork - Source Network address for which   */
/*                        the Route information has changed.                */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
PUBLIC void
DvmrpPmSendPruneOnIfaceChange (UINT4 u4IfId, UINT4 u4SrcNetwork)
#else
PUBLIC void
DvmrpPmSendPruneOnIfaceChange (u4IfId, u4SrcNetwork)
     UINT4               u4IfId, u4SrcNetwork;
#endif
{
    UINT4               u4Index, u4PruneLifeTime;
    tForwTbl           *pForwTbl;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }

        /* 
         * Source network = 0 implies that the Interface has gone down and 
         * it has to be removed from all the Forward table entries. A Valid  
         * Source Network implies that the Designated Forwarder status is 
         * changed to False and hence we wont be forwarding on this interface 
         * So remove interface from all the Source network entries in Forward 
         * Table 
         */

        if ((u4SrcNetwork == 0) || (pForwTbl->u4SrcNetwork == u4SrcNetwork))
        {

            /*  
             * Remove interface in the Output interface list  
             */
            /* Added Flag as the last parameter */
            u4PruneLifeTime =
                DvmrpFmDeleteIfList (pForwTbl, u4IfId, INT_REMOVE);
            if ((DvmrpMfwdDeleteOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
            {
                LOG1 ((INFO, PM, "Unable to Delete Oif from MFWD"));
            }
            if (u4PruneLifeTime > 0)
            {
                /*  
                 * No other go. A Prune has to be sent upstream  
                 */
                DvmrpPmSendPruneUpstream (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                          (u4Index), u4PruneLifeTime);
            }
            else
            {
                LOG1 ((INFO, PM, "Prune Not Sent Upstream"));
            }
        }
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmSendPruneOnNbrChange                          */
/* Description      :  This function sends a Prune to the upstream Neighbor */
/*                     if required, based on change in Route information for*/
/*                     a Source network or when Neighbor goes down.         */
/* Input (s)        :  1. u4IfId - Index into the Interface Table which     */
/*                        holds the interface id on which Route change has  */
/*                        changed                                           */
/*                     2. u4SrcNetwork - Source Network address for which   */
/*                        the Route information has changed.                */
/*                     3. u4NbrAddr - Address of the Neighbor which         */
/*                        cancelled downstream dependency                   */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
PUBLIC void
DvmrpPmSendPruneOnNbrChange (UINT4 u4IfId, UINT4 u4SrcNetwork, UINT4 u4NbrAddr)
#else
PUBLIC void
DvmrpPmSendPruneOnNbrChange (u4IfId, u4SrcNetwork, u4NbrAddr)
     UINT4               u4IfId, u4SrcNetwork, u4NbrAddr;
#endif
{
    UINT4               u4Index, u4PruneLifeTime;
    tForwTbl           *pForwTbl;

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {

        pForwTbl = DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index);

        if (pForwTbl == NULL)
        {
            /* Entry Invalid */
            continue;
        }

        /* 
         * Source network = 0 implies that the Neighbor has gone down and has 
         * to be removed from all the Forward table entries. A Valid Source  
         * Network implies that the Neighbor has cancelled his downstream 
         * dependency for the source network alone. 
         */

        if ((u4SrcNetwork == 0) || (pForwTbl->u4SrcNetwork == u4SrcNetwork))
        {

            /*  
             * Remove interface in the Output interface list  
             */
            u4PruneLifeTime = DvmrpFmDeleteifNbr (pForwTbl, u4IfId, u4NbrAddr);

            if (u4PruneLifeTime > 0)
            {
                if ((DvmrpMfwdDeleteOif (pForwTbl, u4IfId) == DVMRP_NOTOK))
                {
                    LOG1 ((INFO, PM, "Uanble to Delete Oif From MFWD"));
                }
                /*  
                 * No other go. A Prune has to be sent upstream  
                 */
                DvmrpPmSendPruneUpstream (DVMRP_GET_FWD_CACHENODE_FROM_INDEX
                                          (u4Index), u4PruneLifeTime);
            }
            else
            {
                LOG1 ((INFO, PM, "Prune Not Sent Upstream"));
            }
        }
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmSendPruneUpstream                             */
/* Description      :  This function forms and sends a Prune packet to the  */
/*                     upstream Neighbor                                    */
/* Input (s)        :  1. pFwdCache - Pointer to the Forward cache     */
/*                        Table entry                                       */
/*                     2. u4PruneMinTime - Prune life time value.           */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpPmSendPruneUpstream (tForwCacheTbl * pFwdCache, UINT4 u4PruneMinTime)
#else
void
DvmrpPmSendPruneUpstream (pFwdCache, u4PruneMinTime)
     tForwCacheTbl      *pFwdCache;
     UINT4               u4PruneMinTime;
#endif
{
    UINT4               u4Len, u4SrcAddr, u4DestAddr;
    tForwTbl           *pForwTbl;
    tDvmrpInterfaceNode *pNbrIfNode = NULL;
    tNbrTbl            *pNbr = NULL;
    INT1                i1SendFlag = DVMRP_NOTOK;

    pForwTbl = pFwdCache->pSrcNwInfo;

    u4SrcAddr = pFwdCache->u4SrcAddress;
    u4DestAddr = pFwdCache->u4DestGroup;

    if (pForwTbl->InIface.u1State == LOCAL_NETWORK)
    {
        LOG1 ((INFO, PM, "Prune not sent upstream - Source is directly"
               " connected"));
        return;
    }
    if (pForwTbl->InIface.u1State == PRUNED)
    {
        if (DvmrpUtilStopTimer (DVMRP_PRUNE_RETRANS_TIMER, pFwdCache)
            == DVMRP_OK)
        {
            LOG1 ((INFO, PM, "Prune Retransmission timer stopped"));
        }
    }
    else if (pForwTbl->InIface.u1State == GRAFT_WAITING_ACK)
    {
        if (DvmrpUtilStopTimer (DVMRP_GRAFT_RETRANS_TIMER, pFwdCache)
            == DVMRP_OK)
        {
            LOG1 ((INFO, PM, "Graft Retransmission timer stopped"));
        }
    }

    /*  
     * Update Forward Table entry  
     */
    pForwTbl->InIface.u1Flag = DVMRP_NORMAL;
    pForwTbl->InIface.u1State = PRUNED;
    pForwTbl->InIface.u4PruneRetransTimerVal = DEFAULT_PRUNE_TIMER_VAL;
    pForwTbl->InIface.u2SentPruneTime = (UINT2) u4PruneMinTime;

    LOG2 ((INFO, PM, "Value of Prune Retransmit timer is \
   ", DEFAULT_PRUNE_TIMER_VAL));
    LOG2 ((INFO, PM, "Upstream Interface State is ",
           pForwTbl->InIface.u1State));
    /*  
     * Form and forward a Prune packet  
     */
    u4Len = PRUNE_PKT_SIZE + MAX_IGMP_HDR_LEN;
    pNbrIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pForwTbl->InIface.u4InInterface);
    if (pNbrIfNode == NULL)
    {
        return;
    }
    pNbr = pNbrIfNode->pNbrOnthisIface;
    DvmrpPmFormPrune (u4DestAddr, u4SrcAddr, u4PruneMinTime, u4Len);

    if (gpSendBuffer == NULL)
    {
        return;
    }
    while (pNbr != NULL)
    {
        if ((pNbr->u4NbrIpAddress == pForwTbl->u4UpNeighbor) &&
            ((pNbr->u1NbrCapabilities & DVMRP_CAPABILITIES)
             == DVMRP_CAPABILITIES))
        {
            i1SendFlag = DVMRP_OK;
            break;
        }
        pNbr = pNbr->pNbrTblNext;
    }
    if (i1SendFlag == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, PM, "UpStream Neighbor capability Failure"));
        return;
    }

    DvmrpOmSendPacket (pForwTbl->InIface.u4InInterface, pForwTbl->u4UpNeighbor,
                       u4Len, DVMRP_PRUNE);

    LOG2 ((INFO, PM, "Prune sent for Source address", u4SrcAddr));

    LOG2 ((INFO, PM, "Prune sent for Group address", u4DestAddr));
    if (DvmrpUtilStartTimer (DVMRP_PRUNE_RETRANS_TIMER, DEFAULT_PRUNE_TIMER_VAL,
                             pFwdCache) == DVMRP_NOTOK)
    {
        LOG2 ((INFO, PM, "Prune Retransmit timer not started for",
               DEFAULT_PRUNE_TIMER_VAL));
    }
    LOG2 ((INFO, PM, "Prune Retransmit timer started for",
           DEFAULT_PRUNE_TIMER_VAL));
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmFormPrune                                     */
/* Description      :  This function forms a Prune packet for the given     */
/*                     Source Destination and Prune life time values.       */
/* Input (s)        :  1. u4DestAddr - Multicast Group address              */
/*                     2. u4SrcAddress - IP Address of the Host which has   */
/*                        to be Pruned                                      */
/*                     3. u4PruneMinTime - Time for which the interface has */
/*                        to be Pruned.                                     */
/*                     4. u4MaxLength - Maximum packet size                 */
/* Output (s)       :  None                                                 */
/* Returns          :  Number of bytes copied on to the Send Buffer         */
/****************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpPmFormPrune (UINT4 u4DestAddr, UINT4 u4SrcAddress, UINT4
                  u4PruneMinTime, UINT4 u4MaxLength)
#else
PRIVATE void
DvmrpPmFormPrune (u4DestAddr, u4SrcAddress, u4PruneMinTime, u4MaxLength)
     UINT4               u4DestAddr, u4SrcAddress, u4PruneMinTime, u4MaxLength;
#endif
{
    DVMRP_ALLOC_SEND_BUFFER ();
    if (gpSendBuffer != NULL)
    {
        u4SrcAddress = OSIX_HTONL (u4SrcAddress);
        u4DestAddr = OSIX_HTONL (u4DestAddr);
        u4PruneMinTime = OSIX_HTONL (u4PruneMinTime);
        MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN], &u4SrcAddress, IP_ADDR_LEN);
        MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN + IP_ADDR_LEN], &u4DestAddr,
                IP_ADDR_LEN);
        MEMCPY (&gpSendBuffer[MAX_IGMP_HDR_LEN + 2 * IP_ADDR_LEN],
                &u4PruneMinTime, IP_ADDR_LEN);
        u4MaxLength = u4MaxLength;
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpPmHandleRetransTimerEvent                       */
/* Description      :  This function handles the expiry of the Prune        */
/*                     Retransmission Timer event.                          */
/* Input (s)        :  pFwdCache - Pointer to the Forward Cache table   */
/*                     entry                                                */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpPmHandleRetransTimerEvent (tDvmrpTimer * pTimerBlk)
#else
void
DvmrpPmHandleRetransTimerEvent (pFwdCache)
     tForwCacheTbl      *pFwdCache;
#endif
{
    UINT4               u4Index;
    tForwTbl           *pForwTbl = NULL;
    tForwCacheTbl      *pFwdCache = NULL;

    pForwTbl = DVMRP_GET_BASE_PTR (tForwTbl, AppTimer, pTimerBlk);

    for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
    {
        if ((DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX (u4Index)) == pForwTbl)
        {
            pFwdCache = DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index);
            break;
        }
    }

    if (pFwdCache == NULL)
    {
        /* Invalid reference block */
        LOG1 ((WARNING, PM, "Null Pointer passed as input"));
        return;
    }

    pForwTbl = pFwdCache->pSrcNwInfo;

    if (pForwTbl == NULL)
    {
        /* Log  Entry removed */
        LOG1 ((WARNING, PM, "Forward Table entry already deleted"));
        return;
    }

    /* Set DeliverMDP Flag and return */
    DvmrpMfwdSetDeliverMdpFlag (pForwTbl, DVMRP_MFWD_DELIVER_MDP);
}

/****************************************************************************/
/* Function Name    :  DvmrpPmSendExponentialPrune                          */
/* Description      :  This function Sends Prune   exponentially            */
/*                     Retransmission Timer event.                          */
/* Input (s)        :  pFwdCache - Pointer to the Forward Cache table   */
/*                     entry                                                */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

VOID
DvmrpPmSendExponentialPrune (tForwCacheTbl * pFwdCache)
{

    UINT4               u4PruneMinTime = 0;
    UINT4               u4Len = 0;
    tOutputInterface   *pOutList = NULL;
    tForwTbl           *pForwTbl = NULL;

    pForwTbl = pFwdCache->pSrcNwInfo;

    if ((pForwTbl->InIface.u1Flag == DATA_RECEIVED_ON_PRUNED_IFACE) &&
        (pForwTbl->InIface.u1State == PRUNED))
    {
        /*  
         * Negative cache Timer value adjusted  
         */
        u4PruneMinTime = PRUNE_LIFE_TIME;
        u4PruneMinTime -= pForwTbl->InIface.u4PruneRetransTimerVal;
        pForwTbl->InIface.u4PruneRetransTimerVal *= 2;
        if (pForwTbl->InIface.u4PruneRetransTimerVal > PRUNE_LIFE_TIME)
        {
            pForwTbl->InIface.u4PruneRetransTimerVal = DEFAULT_PRUNE_TIMER_VAL;
        }
        LOG2 ((INFO, PM, "Upstream Interface  State is ",
               pForwTbl->InIface.u1State));

        pOutList = pForwTbl->pOutInterface;
        DVMRP_FIND_INTERFACE_PRUNETIME (pOutList, u4PruneMinTime);

        /*  
         * Form and forward the Prune packet  
         */
        u4Len = PRUNE_PKT_SIZE + MAX_IGMP_HDR_LEN;

        DvmrpPmFormPrune (pFwdCache->u4DestGroup, pFwdCache->u4SrcAddress,
                          u4PruneMinTime, u4Len);

        if (gpSendBuffer == NULL)
        {
            return;
        }

        DvmrpOmSendPacket (pForwTbl->InIface.u4InInterface,
                           pForwTbl->u4UpNeighbor, u4Len, DVMRP_PRUNE);
        LOG2 ((INFO, PM, "Prune sent for Source address ",
               pFwdCache->u4SrcAddress));
        LOG2 ((INFO, PM, "Prune sent for Group address",
               pFwdCache->u4DestGroup));

        if (DvmrpUtilStartTimer
            (DVMRP_PRUNE_RETRANS_TIMER,
             (UINT2) pForwTbl->InIface.u4PruneRetransTimerVal,
             pFwdCache) == DVMRP_NOTOK)
        {
            /* Log timer failed */
            return;
        }

        LOG2 ((INFO, PM, "Prune Retransmit timer started for seconds",
               pForwTbl->InIface.u4PruneRetransTimerVal));

        /* Reset the Flag */
        pForwTbl->InIface.u1Flag = DVMRP_NORMAL;
        DvmrpMfwdSetDeliverMdpFlag (pForwTbl, DVMRP_MFWD_DONT_DELIVER_MDP);
    }
    else
    {
        pForwTbl->InIface.u4PruneRetransTimerVal = 0;
        LOG1 ((INFO, PM, "Invalid Prune retransmission timer expiry event"));
    }
    return;

}
