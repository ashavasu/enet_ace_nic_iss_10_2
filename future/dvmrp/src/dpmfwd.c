/* * $Id: dpmfwd.c,v 1.16 2014/08/28 11:15:07 siva Exp $*/
#include "dpinc.h"
#include "nat.h"
/****************************************************************************
 * Function Name         :    DvmrpMfwdCreateRtEntry                    *
 *                                                                          *
 * Description           :  This function creates a message buffer for the  *
 *                          entry creation and posts it to MFWD for the     *
 *                          for the creation.                               *
 *                                                                          *
 * Input (s)             : pRtEntry   - The entry that is to be created     *
 *                         at the MFWD.                                     *
 *                                                                          *
 * Output (s)            :    None                                          *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : DVMRP_OK - successful creation of entry       *
 *                       DVMRP_NOTOK - failure cases                      *
 ****************************************************************************/

INT4
DvmrpMfwdCreateRtEntry (tForwTbl * pFwdEntry)
{
    INT4                i4Status = DVMRP_OK;
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    tOutputInterface   *pOutIf = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT4               u4OifCtr = 0;
    UINT4               u4Cnt = 0;
    UINT4               u4TempAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4NbrAddr = 0;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdCreateRtEntry"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_CREATE_CMD;
    MsgHdr.u1NumEntries = 1;

    /* Fill basic data required for the creation of entry */
    u4SrcAddr = OSIX_NTOHL (pFwdEntry->pSrcInfo->u4SrcAddr);
    u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);
    u4NbrAddr = OSIX_NTOHL (pFwdEntry->u4UpNeighbor);
    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4SrcAddr));
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4GrpAddr));
    IPVX_ADDR_INIT_IPV4 (RtData.UpStrmNbr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4NbrAddr));
    DVMRP_MASK_TO_MASKLEN (pFwdEntry->pSrcInfo->u4SrcMask, RtData.u4SrcMaskLen);

    RtData.u1ChkRpfFlag = 1;
    RtData.u4Iif = pFwdEntry->InIface.u4InInterface;
    RtData.u4GrpMaskLen = IPVX_IPV4_ADDR_LEN;

    pOutIf = pFwdEntry->pOutInterface;
    RtData.u4OifCnt = 0;

    for (u4Cnt = 1; pOutIf != NULL; u4Cnt++)
    {
        if (pOutIf->u4OutInterface != pFwdEntry->InIface.u4InInterface)
        {
            RtData.u4OifCnt++;
        }
        pOutIf = pOutIf->pNext;
    }
    RtData.u1DeliverMDP = DVMRP_MFWD_DONT_DELIVER_MDP;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (RtData.u4OifCnt *
                                                sizeof (tMrpOifNode)), 0);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM, "Failure in MFWD MRP Update Message buffer "
               "Allocation"));

        /* Failure in Update Message buffer allocation, exit */
        LOG1 ((INFO, IHM, "Exiting PimMfwdCreateRtEntry"));
        return (DVMRP_NOTOK);
    }

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* add all the oifs into the update message for the MFWD */
    pOutIf = pFwdEntry->pOutInterface;

    for (pOutIf = pFwdEntry->pOutInterface; pOutIf != NULL;
         pOutIf = pOutIf->pNext)
    {
        if (pOutIf->u4OutInterface == pFwdEntry->InIface.u4InInterface)
        {
            continue;
        }

        IPVX_ADDR_INIT_IPV4 (UpdOifNode.NextHopAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4TempAddr));
        UpdOifNode.u4OifIndex = pOutIf->u4OutInterface;
        UpdOifNode.u4NextHopState = 2;
        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                                   MFWD_OIFINFO_OFFSET (u4OifCtr),
                                   sizeof (tMrpOifNode));
        u4OifCtr++;
    }

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    pFwdEntry->u1DeliverFlg = RtData.u1DeliverMDP;

    LOG1 ((INFO, IHM, "Exiting DvmrpMfwdCreateRtEntry"));
#endif
#ifdef FS_NPAPI
    i4Status = DvmrpNpAddRoute (pFwdEntry);
#endif
    UNUSED_PARAM (pFwdEntry);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdDeleteRtEntry                        */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          entry deletion and posts it to MFWD for the     */
/*                          for the deletion.                               */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/
INT4
DvmrpMfwdDeleteRtEntry (tForwTbl * pFwdEntry)
{
    INT4                i4Status = 0;
#ifdef MFWD_WANTED
    tMrtUpdData         RtData;
    tMrpUpdMesgHdr      MsgHdr;
    tDpActiveSrcInfo   *pSrcInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT1               u1EntryCnt = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdDeleteRtEntry"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    pSrcInfo = pFwdEntry->pSrcInfo;

    while (pSrcInfo != NULL)
    {
        u1EntryCnt++;
        pSrcInfo = pSrcInfo->pNext;
    }
    if (u1EntryCnt == 0)
    {
        LOG1 ((INFO, FM, "Forward Entry with no Source Info...Some"
               " thing wrong"));
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_DELETE_CMD;
    MsgHdr.u1NumEntries = u1EntryCnt;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               (sizeof (tMrtUpdData) *
                                                u1EntryCnt), 0);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM, "Failure in MFWD MRP Update Message buffer "
               "Allocation"));
        /* Failure in Update Message buffer allocation, exit */
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteRtEntry"));
        return (DVMRP_NOTOK);
    }

    /* Copy the Message Header in to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    u1EntryCnt = 0;
    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        u4SrcAddr = OSIX_NTOHL (pSrcInfo->u4SrcAddr);
        u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);
        IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));

        IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4GrpAddr));
        DVMRP_MASK_TO_MASKLEN (pSrcInfo->u4SrcMask, RtData.u4SrcMaskLen);

        RtData.u4Iif = pFwdEntry->InIface.u4InInterface;

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf,
                                   (UINT1 *) &RtData,
                                   sizeof (tMrpUpdMesgHdr) +
                                   (sizeof (tMrtUpdData) * u1EntryCnt),
                                   sizeof (tMrtUpdData));
        pSrcInfo = pSrcInfo->pNext;
    }

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteRtEntry"));
#endif
#ifdef FS_NPAPI
    i4Status = DvmrpNpDeleteRoute (pFwdEntry);
#endif
    UNUSED_PARAM (pFwdEntry);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdDeleteOneRtEntry                     */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          entry deletion and posts it to MFWD for the     */
/*                          for the deletion.                               */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/
INT4
DvmrpMfwdDeleteOneRtEntry (tForwCacheTbl * pFwdCache)
{
    INT4                i4Status = 0;
#ifdef MFWD_WANTED
    tMrtUpdData         RtData;
    tMrpUpdMesgHdr      MsgHdr;
    tDpActiveSrcInfo   *pSrcInfo = NULL;
    tForwTbl           *pFwdEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT1               u1EntryCnt = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdDeleteOneRtEntry"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    pFwdEntry = pFwdCache->pSrcNwInfo;
    pSrcInfo = pFwdEntry->pSrcInfo;

    while (pSrcInfo != NULL)
    {
        if (pSrcInfo->u4SrcAddr == pFwdCache->u4SrcAddress)
        {
            u1EntryCnt = 1;
            break;
        }
        pSrcInfo = pSrcInfo->pNext;
    }

    if (u1EntryCnt != 1)
    {
        return i4Status;
    }
    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_DELETE_CMD;
    MsgHdr.u1NumEntries = u1EntryCnt;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               (sizeof (tMrtUpdData) *
                                                u1EntryCnt), 0);
    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM, "Failure in MFWD MRP Update Message buffer "
               "Allocation"));
        /* Failure in Update Message buffer allocation, exit */
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteOneRtEntry"));
        return (DVMRP_NOTOK);
    }

    /* Copy the Message Header in to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));
    u4SrcAddr = OSIX_NTOHL (pFwdCache->u4SrcAddress);
    u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);

    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4SrcAddr));
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &(u4GrpAddr));
    DVMRP_MASK_TO_MASKLEN (pSrcInfo->u4SrcMask, RtData.u4SrcMaskLen);

    RtData.u4Iif = pFwdEntry->InIface.u4InInterface;

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf,
                               (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteOneRtEntry"));
#endif
#ifdef FS_NPAPI
    i4Status = DvmrpNpDeleteOneRoute (pFwdCache);
#endif
    UNUSED_PARAM (pFwdCache);
    return (i4Status);
}

/****************************************************************************
 * Function Name         :    DvmrpMfwdSetDeliverMdpFlag                *
 *                                                                          *
 * Description           :  This function creates a message buffer for the  *
 *                          entry deletion and posts it to MFWD for the     *
 *                          for the deletion.                               *
 *                                                                          *
 * Input (s)             : pRtEntry   - The entry that is to be created     *
 *                         at the MFWD.                                     *
 *                                                                          *
 * Output (s)            :    None                                          *
 *                                                                          *
 * Global Variables Referred : None                                         *
 *                                                                          *
 * Global Variables Modified : None                                         *
 *                                                                          *
 * Returns             : DVMRP_OK - successful creation of entry       *
 *                       DVMRP_NOTOK - failure cases                      *
 ****************************************************************************/
INT4
DvmrpMfwdSetDeliverMdpFlag (tForwTbl * pFwdEntry, UINT1 u1DeliverMdp)
{
    INT4                i4Status = DVMRP_OK;
#ifdef MFWD_WANTED
    /* The following variables' type defs are common to MRP and MFWD */
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tDpActiveSrcInfo   *pSrcInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT1               u1EntryCnt = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdSetDeliverMdpFlag"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    if (pFwdEntry->u1DeliverFlg == u1DeliverMdp)
    {
        return DVMRP_OK;
    }
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));
    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        u1EntryCnt++;
        pSrcInfo = pSrcInfo->pNext;
    }
    if (u1EntryCnt == 0)
    {
        LOG1 ((INFO, FM, "Forward Entry with no Source Info...Some"
               " thing wrong"));
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_UPD_DELIVERMDP_FLAG;
    MsgHdr.u1NumEntries = u1EntryCnt;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               (sizeof (tMrtUpdData) *
                                                u1EntryCnt), 0);

    if (pMrtUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM, "Failure in MFWD MRP Update Message buffer "
               "Allocation"));
        /* Failure in Update Message buffer allocation, exit */
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteRtEntry"));
        return (DVMRP_NOTOK);
    }

    /* Copy the Message Header in to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));
    u1EntryCnt = 0;
    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        MEMSET (&RtData, 0, sizeof (tMrtUpdData));

        u4SrcAddr = OSIX_NTOHL (pSrcInfo->u4SrcAddr);
        u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);
        /* Fill basic data required for the creation of entry */
        IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4GrpAddr));

        /* Fill basic data required for the creation of entry */
        RtData.u4Iif = pFwdEntry->InIface.u4InInterface;
        RtData.u1DeliverMDP = u1DeliverMdp;

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf,
                                   (UINT1 *) &RtData,
                                   sizeof (tMrpUpdMesgHdr) +
                                   (sizeof (tMrtUpdData) * u1EntryCnt),
                                   sizeof (tMrtUpdData));

        pSrcInfo = pSrcInfo->pNext;
        u1EntryCnt++;
    }

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
    pFwdEntry->u1DeliverFlg = u1DeliverMdp;
    LOG1 ((INFO, IHM, "Exiting DvmrpMfwdSetDeliverMdpFlag"));
#endif
#ifdef FS_NPAPI
    if (pFwdEntry->u1DeliverFlg == u1DeliverMdp)
    {
        return DVMRP_OK;
    }

    if (u1DeliverMdp == DVMRP_MFWD_DELIVER_MDP)
    {
        i4Status = DvmrpNpAddCpuPort (pFwdEntry);
    }
    else
    {
        i4Status = DvmrpNpDeleteCpuPort (pFwdEntry);
    }
    pFwdEntry->u1DeliverFlg = u1DeliverMdp;
#endif
    UNUSED_PARAM (pFwdEntry);
    UNUSED_PARAM (u1DeliverMdp);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdAddOif                               */
/*                                                                          */
/* Description           :  This function calls the update oif for the      */
/*                          addition of the Oif with required parameters    */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         whcih is to be added                     */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/

INT4
DvmrpMfwdAddOif (tForwTbl * pFwdEntry, UINT4 u4OifIndex)
{
    INT4                i4Status = DVMRP_OK;
    tOutputInterface   *pOutList = NULL;
    UINT4               u4OifCnt = 0;
#ifdef FS_NPAPI
#ifdef NAT_WANTED
    UINT4               u4InIndex = 0;
#endif
#endif

#ifdef MFWD_WANTED
    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdAddOif"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }
    if (pFwdEntry->InIface.u4InInterface == u4OifIndex)
    {
        return i4Status;
    }
    i4Status = DvmrpMfwdUpdateOif (pFwdEntry, u4OifIndex,
                                   MFWD_ENTRY_ADD_OIF_CMD);

#endif
#ifdef FS_NPAPI
    /* check if entry is to be created in HW
     * for NAT MCAST support
     * If NAT_SUCCESS (1) is returned pkt should be
     * routed/switched by Data plane
     * else pkt should be switched by control plane,
     * hence do not add Hw entry
     */
#ifdef NAT_WANTED

    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface, &u4InIndex);
    if (NatCheckIfAddMcastEntry (u4InIndex) != NAT_FAILURE)
#endif

        DvmrpNpAddOif (pFwdEntry, u4OifIndex);
#endif
    pOutList = pFwdEntry->pOutInterface;
    while (pOutList != NULL)
    {
        u4OifCnt++;
        pOutList = pOutList->pNext;
    }
    if (u4OifCnt == 1)
    {
        DvmrpMfwdSetDeliverMdpFlag (pFwdEntry, DVMRP_MFWD_DONT_DELIVER_MDP);
    }
#ifndef FS_NPAPI
    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdAddOif"));
#endif
    UNUSED_PARAM (pFwdEntry);
    UNUSED_PARAM (u4OifIndex);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdDeleteOif                              */
/*                                                                          */
/* Description           :  This function calls the update oif with the     */
/*                          witht the required parameters for the deletion  */
/*                           of the oif                                     */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         which is be deleted                              */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/

INT4
DvmrpMfwdDeleteOif (tForwTbl * pFwdEntry, UINT4 u4OifIndex)
{
    INT4                i4Status = DVMRP_OK;

#ifdef MFWD_WANTED
    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdDeleteOif"));

    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return i4Status;
    }

    i4Status = DvmrpMfwdUpdateOif (pFwdEntry, u4OifIndex,
                                   MFWD_ENTRY_DELETE_OIF_CMD);
#endif
#ifdef FS_NPAPI
    DvmrpNpDeleteOif (pFwdEntry, u4OifIndex);
#endif
#ifndef FS_NPAPI
    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteOif"));
#endif
    UNUSED_PARAM (pFwdEntry);
    UNUSED_PARAM (u4OifIndex);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdUpdateOif                              */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          Oif update and posts the update message to MFWD */
/*                          for the update.                                 */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u4OifIndex:- The oif index of the interfaces     */
/*                         whose state is to be changed                     */
/*                         u4NextHopAddr:- The next hop address for which   */
/*                         the oif state is to be updated                   */
/*                         u1OifState   :- The state of the next hop        */
/*                         u1InstanceId :- The instance id of pim           */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/
INT4
DvmrpMfwdUpdateOif (tForwTbl * pFwdEntry, UINT4 u4OifIndex, UINT1 u1UpdCmd)
{
    INT4                i4Status = DVMRP_OK;
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    tDpActiveSrcInfo   *pSrcInfo = NULL;
    UINT1               u1EntryCnt = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;

    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdUpdateOif"));

    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));
    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        u1EntryCnt++;
        pSrcInfo = pSrcInfo->pNext;
    }
    if (u1EntryCnt == 0)
    {
        LOG1 ((INFO, FM, "Forward Entry with no Source Info...Some"
               " thing wrong"));
        return i4Status;
    }
    /* fill the update message header */

    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = u1UpdCmd;
    MsgHdr.u1NumEntries = 1;

    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        MEMSET (&RtData, 0, sizeof (tMrtUpdData));
        MEMSET (&UpdOifNode, 0, sizeof (tMrpOifNode));

        u4SrcAddr = OSIX_NTOHL (pSrcInfo->u4SrcAddr);
        u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);
        IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4GrpAddr));

        /*  need to update
           RtData.u4RtAddr = pRtEntry->u4SrcAddr; */
        RtData.u4Iif = pFwdEntry->InIface.u4InInterface;
        RtData.u4OifCnt = 1;

        pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                                   sizeof (tMrtUpdData) +
                                                   (RtData.u4OifCnt *
                                                    sizeof (tMrpOifNode)), 0);
        if (pMrtUpdBuf == NULL)
        {
            /* Check if Allocation success */
            LOG1 ((WARNING, IHM,
                   "Failure in MFWD MRP Update Message buffer Allocation"));
            /* Failure in Update Message buffer allocation, exit */
            LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdUpdateOif"));
            return (DVMRP_NOTOK);
        }

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                                   sizeof (tMrpUpdMesgHdr));

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                                   sizeof (tMrpUpdMesgHdr),
                                   sizeof (tMrtUpdData));

        /* update Oif params */
        /* Need to update
           UpdOifNode.u4NextHopAddr = u4NextHopAddr; */
        if (u1UpdCmd == MFWD_ENTRY_ADD_OIF_CMD)
        {
            UpdOifNode.u4NextHopState = 2;
        }
        else
        {
            UpdOifNode.u4NextHopState = 1;
        }

        UpdOifNode.u4OifIndex = u4OifIndex;

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                                   MFWD_OIFINFO_OFFSET (0),
                                   sizeof (tMrpOifNode));

        MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
        pSrcInfo = pSrcInfo->pNext;
    }
    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdUpdateOif"));
#endif
#ifdef FS_NPAPI
    pFwdEntry = pFwdEntry;
    u4OifIndex = u4OifIndex, u1UpdCmd = u1UpdCmd;
    i4Status = DVMRP_OK;
#endif
    UNUSED_PARAM (pFwdEntry);
    UNUSED_PARAM (u4OifIndex);
    UNUSED_PARAM (u1UpdCmd);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :    DvmrpMfwdUpdateIif                            */
/*                                                                          */
/* Description           :  This function creates a message buffer for the  */
/*                          Iif update and posts the update message to MFWD */
/*                          for the update.                                 */
/*                                                                          */
/* Input (s)             : pRtEntry   - The entry that is to be created     */
/*                         at the MFWD.                                     */
/*                         u1InstanceId :- The instance id of pim           */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : DVMRP_OK - successful creation of entry         */
/*                       DVMRP_NOTOK - failure cases                        */
/****************************************************************************/
INT4
DvmrpMfwdUpdateIif (tForwTbl * pFwdEntry, UINT4 u4OldSrc, UINT4 u4PrevIif)
{
    INT4                i4Status = DVMRP_OK;
#ifdef FS_NPAPI
#ifdef NAT_WANTED
    UINT4               u4InIndex;
#endif
#endif
#ifdef MFWD_WANTED
    UINT4               u4Cnt = 0;
    tOutputInterface   *pOutIf = NULL;
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrtIifUpdData      IifData;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    tDpActiveSrcInfo   *pSrcInfo = NULL;
    UINT1               u1EntryCnt = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4TempAddr = 0;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdUpdateIif"));
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdUpdateIif"));
        return i4Status;
    }

    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));
    pSrcInfo = pFwdEntry->pSrcInfo;
    while (pSrcInfo != NULL)
    {
        u1EntryCnt++;
        pSrcInfo = pSrcInfo->pNext;
    }
    if (u1EntryCnt == 0)
    {
        LOG1 ((INFO, FM, "Forward Entry with no Source Info...Some"
               " thing wrong"));
        return i4Status;
    }
    /* fill the update message header */

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_UPDATE_IIF_CMD;
    MsgHdr.u1NumEntries = 1;

    pSrcInfo = pFwdEntry->pSrcInfo;

    while (pSrcInfo != NULL)
    {
        u4SrcAddr = OSIX_NTOHL (pSrcInfo->u4SrcAddr);
        u4GrpAddr = OSIX_NTOHL (pFwdEntry->u4GroupAddr);
        IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4GrpAddr));
        IPVX_ADDR_INIT_IPV4 (RtData.RtAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));

        RtData.u4Iif = u4PrevIif;

        /* fill the data required for the IIF update */

        u4SrcAddr = 0;
        u4SrcAddr = OSIX_NTOHL (pSrcInfo->u4SrcAddr);
        u4TempAddr = 0;
        u4TempAddr = OSIX_NTOHL (pFwdEntry->u4UpNeighbor);

        IPVX_ADDR_INIT_IPV4 (IifData.NewSrcAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4SrcAddr));
        IifData.u4Iif = pFwdEntry->InIface.u4InInterface;

        IPVX_ADDR_INIT_IPV4 (RtData.UpStrmNbr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4TempAddr));
        IPVX_ADDR_INIT_IPV4 (IifData.UpStrmNewNbr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &(u4TempAddr));

        RtData.u1ChkRpfFlag = MFWD_MRP_CHK_RPF_IF;

        RtData.u4OifCnt = 0;

        pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                                   sizeof (tMrtUpdData) +
                                                   sizeof (tMrtIifUpdData), 0);
        if (pMrtUpdBuf == NULL)
        {
            /* Check if Allocation success */
            LOG1 ((WARNING, IHM, "Failure in MFWD MRP Update Message buffer "
                   "Allocation"));
            /* Failure in Update Message buffer allocation, exit */
            LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdUpdateIif"));
            return (DVMRP_NOTOK);
        }

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                                   sizeof (tMrpUpdMesgHdr));
        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                                   sizeof (tMrpUpdMesgHdr),
                                   sizeof (tMrtUpdData));

        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &IifData,
                                   sizeof (tMrpUpdMesgHdr) +
                                   sizeof (tMrtUpdData),
                                   sizeof (tMrtIifUpdData));

        MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);
        pSrcInfo = pSrcInfo->pNext;
    }

    if (u4PrevIif != pFwdEntry->InIface.u4InInterface)
    {
        /* 1. Here we need to Scan the OifList and if Old Iif was present in 
         *    the OifList now add that as it is no longer Iif.
         * 2. If new Oif is in the OifList, delete that from the Mfwd     
         */
        pOutIf = pFwdEntry->pOutInterface;
        for (u4Cnt = 0; pOutIf != NULL; u4Cnt++)
        {
            if (pOutIf->u4OutInterface == u4PrevIif)
            {
                DvmrpMfwdAddOif (pFwdEntry, pOutIf->u4OutInterface);
            }
            else if (pOutIf->u4OutInterface == pFwdEntry->InIface.u4InInterface)
            {
                DvmrpMfwdDeleteOif (pFwdEntry, pOutIf->u4OutInterface);
            }
            pOutIf = pOutIf->pNext;
        }
    }

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdUpdateIif"));
#endif
#ifdef FS_NPAPI
    u4OldSrc = u4OldSrc;
    u4PrevIif = u4PrevIif;
#ifdef NAT_WANTED
    DVMRP_GET_IFINDEX_FROM_PORT (pFwdEntry->InIface.u4InInterface, &u4InIndex);
    if (NatCheckIfAddMcastEntry (u4InIndex) != NAT_FAILURE)
#endif
        i4Status = DvmrpNpUpdateIif (pFwdEntry);
#endif
    UNUSED_PARAM (pFwdEntry);
    UNUSED_PARAM (u4OldSrc);
    UNUSED_PARAM (u4PrevIif);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :   DvmrpMfwdHandleEnabling                        */
/*                                                                          */
/* Description           :  This function Reinstates all the route entries  */
/*                          at the MFWD after it is reenabled from the      */
/*                          disabled state.                                 */
/*                                                                          */
/* Input (s)             : Nonde                                            */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/
VOID
DvmrpMfwdHandleEnabling ()
{
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4Offset;
    UINT4               u4IfCount;
    INT4                i4Status;
    UINT4               u4Var;
    tTMO_SLL_NODE      *pInstIfNode = NULL;
    tDvmrpInterfaceNode *pInstNode = NULL;
    tForwTbl           *pFwdEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    /* UINT1               u1DeliverMdp = 0;THIS VALUE NEED TO BE CHANGED */

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdHandleEnabling"));
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_ENABLED)
    {
        return;
    }

    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;

    u4IfCount = TMO_SLL_Count (&gDvmrpIfInfo.IfGetNextList);

    if (u4IfCount == 0)
    {
        /* Failure trace */
        return;
    }

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) + sizeof (UINT4) *
                                              u4IfCount, 0);
    if (pIfUpdBuf != NULL)
    {
        CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, 0,
                                   sizeof (tMrpUpdMesgHdr));
        CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                                   sizeof (tMrpUpdMesgHdr), sizeof (UINT4));

        u4Offset = sizeof (tMrpUpdMesgHdr) + sizeof (UINT4);
        TMO_SLL_Scan (&(gDvmrpIfInfo.IfGetNextList), pInstIfNode,
                      tTMO_SLL_NODE *)
        {
            pInstNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                            IfGetNextLink, pInstIfNode);

            CRU_BUF_Copy_OverBufChain (pIfUpdBuf,
                                       (UINT1 *) &pInstNode->u4IfIndex,
                                       u4Offset, sizeof (UINT4));
            u4Offset += sizeof (UINT4);
        }
        MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);
    }

    for (u4Var = 0; u4Var < MAX_FORW_TABLE_ENTRIES; u4Var++)
    {

        pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4Var);
        if (pFwdEntry->u1Status != DVMRP_INACTIVE)
        {
            DvmrpMfwdCreateRtEntry (pFwdEntry);
        }
    }

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdHandleEnabling"));
#endif
    return;
}

/****************************************************************************/
/* Function Name         :   DvmrpMfwdHandleDisabling                       */
/*                                                                          */
/* Description           :  This function uninstalls all the route and      */
/*                          interface  information at MFWD after it is      */
/*                          disabled from the enabled state.                */
/*                                                                          */
/* Input (s)             : Nonde                                            */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/
VOID
DvmrpMfwdHandleDisabling ()
{
    tTMO_SLL_NODE      *pInstIfNode = NULL;
    tDvmrpInterfaceNode *pInstNode = NULL;
#ifndef MFWD_WANTED
    tNetIpMcastInfo     NetIpMcastInfo;
#endif

#ifdef MFWD_WANTED
    tForwTbl           *pFwdEntry = NULL;
    UINT4               u4Var;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdHandleDisabling"));
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        return;
    }

    /* The Route Entries are removed from mfwd before deleting 
     * the interface */
    for (u4Var = 0; u4Var < MAX_FORW_TABLE_ENTRIES; u4Var++)
    {
        pFwdEntry = DVMRP_GET_FWDNODE_FROM_INDEX (u4Var);
        if (pFwdEntry->u1Status != DVMRP_INACTIVE)
        {
            /* Delete All the installed entries */
            DvmrpMfwdDeleteRtEntry (pFwdEntry);
        }
    }

    TMO_SLL_Scan (&(gDvmrpIfInfo.IfGetNextList), pInstIfNode, tTMO_SLL_NODE *)
    {
        pInstNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                        IfGetNextLink, pInstIfNode);
        DvmrpMfwdDeleteIface (pInstNode->u4IfIndex);
    }

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdHandleDisabling"));
#else
    /* Disable Multicasting in VIFs added in NetIp. */
    TMO_SLL_Scan (&(gDvmrpIfInfo.IfGetNextList), pInstIfNode, tTMO_SLL_NODE *)
    {
        pInstNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                        IfGetNextLink, pInstIfNode);

        MEMSET (&NetIpMcastInfo, 0, sizeof (NetIpMcastInfo));

        NetIpMcastInfo.u4IpPort = pInstNode->u4IfIndex;
        NetIpMcastInfo.u1McastProtocol = DVMRP_ID;
        if (NetIpv4SetMcStatusOnPort (&NetIpMcastInfo, DISABLED)
            == NETIPV4_FAILURE)
        {
            return;
        }
    }
#endif
    return;
}

/****************************************************************************/
/* Function Name         :   DvmrpMfwdAddIface                                */
/*                                                                          */
/* Description           :  This function creates a update buffer for adding*/
/*                          an interface to the interface list of the       */
/*                          MRP at the MFWD.                                */
/*                                                                          */
/* Input (s)             : u4IfIndex :- the interface index value of the       */
/*                         interface to be added                            */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/

INT4
DvmrpMfwdAddIface (UINT4 u4IfIndex)
{
    INT4                i4Status = DVMRP_OK;
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4IfCount;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdAddIface"));
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdAddIface"));
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;
    u4IfCount = 1;

    /* Fill basic data required for the IF Update */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount, 0);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM,
               "Failure in MFWD MRP Update Message buffer Allocation"));
        /* Failure in Update Message buffer allocation, exit */
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdAddIface"));
        return (DVMRP_NOTOK);
    }

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdAddIface"));
#endif
#ifdef FS_NPAPI
    u4IfIndex = u4IfIndex;
    i4Status = DVMRP_OK;
#endif
    UNUSED_PARAM (u4IfIndex);
    return (i4Status);
}

/****************************************************************************/
/* Function Name         :  DvmrpMfwdDeleteIface                        */
/*                                                                          */
/* Description           :  This function Deletes the specified interface   */
/*                          from the interface list of the specified MRP    */
/*                                                                          */
/* Input (s)             : u4IfIndex :- the interface index value of the       */
/*                         interface to be deleted                          */
/*                                                                          */
/* Output (s)            :    None                                          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : None                                               */
/****************************************************************************/
INT4
DvmrpMfwdDeleteIface (UINT4 u4IfIndex)
{
    INT4                i4Status = DVMRP_OK;
#ifdef MFWD_WANTED
    tMrpUpdMesgHdr      MsgHdr;
    UINT4               u4IfCount;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;

    LOG1 ((INFO, IHM, "Entering function DvmrpMfwdDeleteIface"));
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdMfwdDeleteIface"));
        return i4Status;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = DVMRP_RTR_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_DELETE_OWNERIF_CMD;
    u4IfCount = 1;

    /* Fill basic data required for the OIF Update */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount, 0);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        LOG1 ((WARNING, IHM,
               "Failure in MFWD MRP Update Message buffer Allocation"));
        LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdMfwdDeleteIface"));
        return (DVMRP_NOTOK);
    }

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    LOG1 ((INFO, IHM, "Exiting function DvmrpMfwdDeleteIface"));
#endif
#ifdef FS_NPAPI
    u4IfIndex = u4IfIndex;
    i4Status = DVMRP_OK;
#endif
    UNUSED_PARAM (u4IfIndex);
    return (i4Status);
}

/****************************************************************************/
