# include  "include.h"
# include  "fsdvmmid.h"
# include  "fsdvmlow.h"
# include  "fsdvmcon.h"
# include  "fsdvmogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsdvmrcli.h"

/****************************************************************************
 Function   : dvmrpInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
dvmrpInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
dvmrpInterfaceEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dvmrpInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_dvmrpInterfaceControlIndex = FALSE;
    INT4                i4_next_dvmrpInterfaceControlIndex = FALSE;

    INT4                i4_dvmrpInterfacePortNum = FALSE;
    INT4                i4_next_dvmrpInterfacePortNum = FALSE;

    UINT4               u4_addr_ret_val_dvmrpInterfaceLocalAddress;
   /*** $$TRACE_LOG (ENTRY,"dvmrpInterfaceTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    u8_counter_val.msn = 0;
    u8_counter_val.lsn = 0;

    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dvmrpInterfaceTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + INTEGER_LEN;

            if (LEN_dvmrpInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dvmrpInterfaceControlIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_dvmrpInterfacePortNum =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDvmrpInterfaceTable
                     (i4_dvmrpInterfaceControlIndex,
                      i4_dvmrpInterfacePortNum)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpInterfaceControlIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpInterfacePortNum;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDvmrpInterfaceTable
                     (&i4_dvmrpInterfaceControlIndex,
                      &i4_dvmrpInterfacePortNum)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpInterfaceControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpInterfacePortNum;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpInterfaceControlIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpInterfacePortNum =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDvmrpInterfaceTable
                     (i4_dvmrpInterfaceControlIndex,
                      &i4_next_dvmrpInterfaceControlIndex,
                      i4_dvmrpInterfacePortNum,
                      &i4_next_dvmrpInterfacePortNum)) == SNMP_SUCCESS)
                {
                    i4_dvmrpInterfaceControlIndex =
                        i4_next_dvmrpInterfaceControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpInterfaceControlIndex;
                    i4_dvmrpInterfacePortNum = i4_next_dvmrpInterfacePortNum;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpInterfacePortNum;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DVMRPINTERFACECONTROLINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpInterfaceControlIndex;
            }
            else
            {
                i4_return_val = i4_next_dvmrpInterfaceControlIndex;
            }
            break;
        }
        case DVMRPINTERFACEPORTNUM:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpInterfacePortNum;
            }
            else
            {
                i4_return_val = i4_next_dvmrpInterfacePortNum;
            }
            break;
        }
        case DVMRPINTERFACELOCALADDRESS:
        {
            i1_ret_val =
                nmhGetDvmrpInterfaceLocalAddress (i4_dvmrpInterfaceControlIndex,
                                                  i4_dvmrpInterfacePortNum,
                                                  &u4_addr_ret_val_dvmrpInterfaceLocalAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_ret_val_dvmrpInterfaceLocalAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPINTERFACEMETRIC:
        {
            i1_ret_val =
                nmhGetDvmrpInterfaceMetric (i4_dvmrpInterfaceControlIndex,
                                            i4_dvmrpInterfacePortNum,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPINTERFACERCVBADPKTS:
        {
            i1_ret_val =
                nmhGetDvmrpInterfaceRcvBadPkts (i4_dvmrpInterfaceControlIndex,
                                                i4_dvmrpInterfacePortNum,
                                                &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPINTERFACERCVBADROUTES:
        {
            i1_ret_val =
                nmhGetDvmrpInterfaceRcvBadRoutes (i4_dvmrpInterfaceControlIndex,
                                                  i4_dvmrpInterfacePortNum,
                                                  &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dvmrpNeighborEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
dvmrpNeighborEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
dvmrpNeighborEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dvmrpNeighborTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    INT4                i4_dvmrpNeighborControlIndex = FALSE;
    INT4                i4_next_dvmrpNeighborControlIndex = FALSE;

    INT4                i4_dvmrpNeighborPortNum = FALSE;
    INT4                i4_next_dvmrpNeighborPortNum = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dvmrpNeighborAddress = FALSE;
    UINT4               u4_addr_next_dvmrpNeighborAddress = FALSE;
    UINT1               u1_addr_dvmrpNeighborAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_dvmrpNeighborAddress[ADDR_LEN] =
        NULL_STRING;

   /*** $$TRACE_LOG (ENTRY,"dvmrpNeighborTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    u8_counter_val.msn = 0;
    u8_counter_val.lsn = 0;

    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dvmrpNeighborTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + INTEGER_LEN + ADDR_LEN;

            if (LEN_dvmrpNeighborTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_dvmrpNeighborControlIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_dvmrpNeighborPortNum =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dvmrpNeighborAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dvmrpNeighborAddress =
                    CRU_BMC_DWFROMPDU (u1_addr_dvmrpNeighborAddress);

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDvmrpNeighborTable
                     (i4_dvmrpNeighborControlIndex, i4_dvmrpNeighborPortNum,
                      u4_addr_dvmrpNeighborAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpNeighborControlIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpNeighborPortNum;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dvmrpNeighborAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDvmrpNeighborTable
                     (&i4_dvmrpNeighborControlIndex, &i4_dvmrpNeighborPortNum,
                      &u4_addr_dvmrpNeighborAddress)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpNeighborControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpNeighborPortNum;
                    CRU_BMC_DWTOPDU (u1_addr_dvmrpNeighborAddress,
                                     u4_addr_dvmrpNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dvmrpNeighborAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpNeighborControlIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpNeighborPortNum =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dvmrpNeighborAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dvmrpNeighborAddress =
                        CRU_BMC_DWFROMPDU (u1_addr_dvmrpNeighborAddress);

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDvmrpNeighborTable
                     (i4_dvmrpNeighborControlIndex,
                      &i4_next_dvmrpNeighborControlIndex,
                      i4_dvmrpNeighborPortNum, &i4_next_dvmrpNeighborPortNum,
                      u4_addr_dvmrpNeighborAddress,
                      &u4_addr_next_dvmrpNeighborAddress)) == SNMP_SUCCESS)
                {
                    i4_dvmrpNeighborControlIndex =
                        i4_next_dvmrpNeighborControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpNeighborControlIndex;
                    i4_dvmrpNeighborPortNum = i4_next_dvmrpNeighborPortNum;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpNeighborPortNum;
                    u4_addr_dvmrpNeighborAddress =
                        u4_addr_next_dvmrpNeighborAddress;
                    CRU_BMC_DWTOPDU (u1_addr_next_dvmrpNeighborAddress,
                                     u4_addr_dvmrpNeighborAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_dvmrpNeighborAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DVMRPNEIGHBORCONTROLINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpNeighborControlIndex;
            }
            else
            {
                i4_return_val = i4_next_dvmrpNeighborControlIndex;
            }
            break;
        }
        case DVMRPNEIGHBORPORTNUM:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpNeighborPortNum;
            }
            else
            {
                i4_return_val = i4_next_dvmrpNeighborPortNum;
            }
            break;
        }
        case DVMRPNEIGHBORADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                CRU_BMC_DWTOPDU (u1_octet_string, u4_addr_dvmrpNeighborAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_next_dvmrpNeighborAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DVMRPNEIGHBORUPTIME:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborUpTime (i4_dvmrpNeighborControlIndex,
                                           i4_dvmrpNeighborPortNum,
                                           u4_addr_dvmrpNeighborAddress,
                                           &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBOREXPIRYTIME:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborExpiryTime (i4_dvmrpNeighborControlIndex,
                                               i4_dvmrpNeighborPortNum,
                                               u4_addr_dvmrpNeighborAddress,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORGENERATIONID:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborGenerationId (i4_dvmrpNeighborControlIndex,
                                                 i4_dvmrpNeighborPortNum,
                                                 u4_addr_dvmrpNeighborAddress,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORMAJORVERSION:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborMajorVersion (i4_dvmrpNeighborControlIndex,
                                                 i4_dvmrpNeighborPortNum,
                                                 u4_addr_dvmrpNeighborAddress,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORMINORVERSION:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborMinorVersion (i4_dvmrpNeighborControlIndex,
                                                 i4_dvmrpNeighborPortNum,
                                                 u4_addr_dvmrpNeighborAddress,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORCAPABILITIES:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborCapabilities (i4_dvmrpNeighborControlIndex,
                                                 i4_dvmrpNeighborPortNum,
                                                 u4_addr_dvmrpNeighborAddress,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORRCVROUTES:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborRcvRoutes (i4_dvmrpNeighborControlIndex,
                                              i4_dvmrpNeighborPortNum,
                                              u4_addr_dvmrpNeighborAddress,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORRCVBADPKTS:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborRcvBadPkts (i4_dvmrpNeighborControlIndex,
                                               i4_dvmrpNeighborPortNum,
                                               u4_addr_dvmrpNeighborAddress,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNEIGHBORRCVBADROUTES:
        {
            i1_ret_val =
                nmhGetDvmrpNeighborRcvBadRoutes (i4_dvmrpNeighborControlIndex,
                                                 i4_dvmrpNeighborPortNum,
                                                 u4_addr_dvmrpNeighborAddress,
                                                 &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dvmrpRouteEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
dvmrpRouteEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
dvmrpRouteEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dvmrpRouteTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dvmrpRouteSource = FALSE;
    UINT4               u4_addr_next_dvmrpRouteSource = FALSE;
    UINT1               u1_addr_dvmrpRouteSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_dvmrpRouteSource[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dvmrpRouteSourceMask = FALSE;
    UINT4               u4_addr_next_dvmrpRouteSourceMask = FALSE;
    UINT1               u1_addr_dvmrpRouteSourceMask[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_dvmrpRouteSourceMask[ADDR_LEN] =
        NULL_STRING;

    UINT4               u4_addr_ret_val_dvmrpRouteUpstreamNeighbor;
   /*** $$TRACE_LOG (ENTRY,"dvmrpRouteTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    u8_counter_val.msn = 0;
    u8_counter_val.lsn = 0;

    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dvmrpRouteTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + ADDR_LEN;

            if (LEN_dvmrpRouteTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dvmrpRouteSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dvmrpRouteSource =
                    CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteSource);

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dvmrpRouteSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dvmrpRouteSourceMask =
                    CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteSourceMask);

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDvmrpRouteTable
                     (u4_addr_dvmrpRouteSource,
                      u4_addr_dvmrpRouteSourceMask)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dvmrpRouteSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dvmrpRouteSourceMask[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDvmrpRouteTable (&u4_addr_dvmrpRouteSource,
                                                      &u4_addr_dvmrpRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    CRU_BMC_DWTOPDU (u1_addr_dvmrpRouteSource,
                                     u4_addr_dvmrpRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dvmrpRouteSource[i4_count];

                    CRU_BMC_DWTOPDU (u1_addr_dvmrpRouteSourceMask,
                                     u4_addr_dvmrpRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dvmrpRouteSourceMask[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dvmrpRouteSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dvmrpRouteSource =
                        CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteSource);

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dvmrpRouteSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dvmrpRouteSourceMask =
                        CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteSourceMask);

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDvmrpRouteTable (u4_addr_dvmrpRouteSource,
                                                     &u4_addr_next_dvmrpRouteSource,
                                                     u4_addr_dvmrpRouteSourceMask,
                                                     &u4_addr_next_dvmrpRouteSourceMask))
                    == SNMP_SUCCESS)
                {
                    u4_addr_dvmrpRouteSource = u4_addr_next_dvmrpRouteSource;
                    CRU_BMC_DWTOPDU (u1_addr_next_dvmrpRouteSource,
                                     u4_addr_dvmrpRouteSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_dvmrpRouteSource[i4_count];
                    u4_addr_dvmrpRouteSourceMask =
                        u4_addr_next_dvmrpRouteSourceMask;
                    CRU_BMC_DWTOPDU (u1_addr_next_dvmrpRouteSourceMask,
                                     u4_addr_dvmrpRouteSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_dvmrpRouteSourceMask[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DVMRPROUTESOURCE:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                CRU_BMC_DWTOPDU (u1_octet_string, u4_addr_dvmrpRouteSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_next_dvmrpRouteSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DVMRPROUTESOURCEMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                CRU_BMC_DWTOPDU (u1_octet_string, u4_addr_dvmrpRouteSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_next_dvmrpRouteSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DVMRPROUTEUPSTREAMNEIGHBOR:
        {
            i1_ret_val =
                nmhGetDvmrpRouteUpstreamNeighbor (u4_addr_dvmrpRouteSource,
                                                  u4_addr_dvmrpRouteSourceMask,
                                                  &u4_addr_ret_val_dvmrpRouteUpstreamNeighbor);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_ret_val_dvmrpRouteUpstreamNeighbor);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPROUTECONTROLINDEX:
        {
            i1_ret_val =
                nmhGetDvmrpRouteControlIndex (u4_addr_dvmrpRouteSource,
                                              u4_addr_dvmrpRouteSourceMask,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPROUTEPORTNUM:
        {
            i1_ret_val =
                nmhGetDvmrpRoutePortNum (u4_addr_dvmrpRouteSource,
                                         u4_addr_dvmrpRouteSourceMask,
                                         &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPROUTEMETRIC:
        {
            i1_ret_val =
                nmhGetDvmrpRouteMetric (u4_addr_dvmrpRouteSource,
                                        u4_addr_dvmrpRouteSourceMask,
                                        &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPROUTEEXPIRYTIME:
        {
            i1_ret_val =
                nmhGetDvmrpRouteExpiryTime (u4_addr_dvmrpRouteSource,
                                            u4_addr_dvmrpRouteSourceMask,
                                            &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPROUTEUPTIME:
        {
            i1_ret_val =
                nmhGetDvmrpRouteUpTime (u4_addr_dvmrpRouteSource,
                                        u4_addr_dvmrpRouteSourceMask,
                                        &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dvmrpRouteNextHopEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
dvmrpRouteNextHopEntryGet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
dvmrpRouteNextHopEntryGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_dvmrpRouteNextHopTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dvmrpRouteNextHopSource = FALSE;
    UINT4               u4_addr_next_dvmrpRouteNextHopSource = FALSE;
    UINT1               u1_addr_dvmrpRouteNextHopSource[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_dvmrpRouteNextHopSource[ADDR_LEN] =
        NULL_STRING;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_dvmrpRouteNextHopSourceMask = FALSE;
    UINT4               u4_addr_next_dvmrpRouteNextHopSourceMask = FALSE;
    UINT1               u1_addr_dvmrpRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;
    UINT1               u1_addr_next_dvmrpRouteNextHopSourceMask[ADDR_LEN] =
        NULL_STRING;

    INT4                i4_dvmrpRouteNextHopControlIndex = FALSE;
    INT4                i4_next_dvmrpRouteNextHopControlIndex = FALSE;

    INT4                i4_dvmrpRouteNextHopPortNum = FALSE;
    INT4                i4_next_dvmrpRouteNextHopPortNum = FALSE;

   /*** $$TRACE_LOG (ENTRY,"dvmrpRouteNextHopTableGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;

    u8_counter_val.msn = 0;
    u8_counter_val.lsn = 0;

    switch (u1_search_type)
    {

        case EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_dvmrpRouteNextHopTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + ADDR_LEN + INTEGER_LEN +
                INTEGER_LEN;

            if (LEN_dvmrpRouteNextHopTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dvmrpRouteNextHopSource[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dvmrpRouteNextHopSource =
                    CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteNextHopSource);

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_dvmrpRouteNextHopSourceMask[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_dvmrpRouteNextHopSourceMask =
                    CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteNextHopSourceMask);

                /* Extracting The Integer Index. */
                i4_dvmrpRouteNextHopControlIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_dvmrpRouteNextHopPortNum =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceDvmrpRouteNextHopTable
                     (u4_addr_dvmrpRouteNextHopSource,
                      u4_addr_dvmrpRouteNextHopSourceMask,
                      i4_dvmrpRouteNextHopControlIndex,
                      i4_dvmrpRouteNextHopPortNum)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dvmrpRouteNextHopSource[i4_count];

                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_dvmrpRouteNextHopSourceMask[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpRouteNextHopControlIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_dvmrpRouteNextHopPortNum;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexDvmrpRouteNextHopTable
                     (&u4_addr_dvmrpRouteNextHopSource,
                      &u4_addr_dvmrpRouteNextHopSourceMask,
                      &i4_dvmrpRouteNextHopControlIndex,
                      &i4_dvmrpRouteNextHopPortNum)) == SNMP_SUCCESS)
                {
                    CRU_BMC_DWTOPDU (u1_addr_dvmrpRouteNextHopSource,
                                     u4_addr_dvmrpRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dvmrpRouteNextHopSource[i4_count];

                    CRU_BMC_DWTOPDU (u1_addr_dvmrpRouteNextHopSourceMask,
                                     u4_addr_dvmrpRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_dvmrpRouteNextHopSourceMask[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpRouteNextHopControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_dvmrpRouteNextHopPortNum;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dvmrpRouteNextHopSource[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dvmrpRouteNextHopSource =
                        CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteNextHopSource);

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_dvmrpRouteNextHopSourceMask[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_dvmrpRouteNextHopSourceMask =
                        CRU_BMC_DWFROMPDU (u1_addr_dvmrpRouteNextHopSourceMask);

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpRouteNextHopControlIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_dvmrpRouteNextHopPortNum =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexDvmrpRouteNextHopTable
                     (u4_addr_dvmrpRouteNextHopSource,
                      &u4_addr_next_dvmrpRouteNextHopSource,
                      u4_addr_dvmrpRouteNextHopSourceMask,
                      &u4_addr_next_dvmrpRouteNextHopSourceMask,
                      i4_dvmrpRouteNextHopControlIndex,
                      &i4_next_dvmrpRouteNextHopControlIndex,
                      i4_dvmrpRouteNextHopPortNum,
                      &i4_next_dvmrpRouteNextHopPortNum)) == SNMP_SUCCESS)
                {
                    u4_addr_dvmrpRouteNextHopSource =
                        u4_addr_next_dvmrpRouteNextHopSource;
                    CRU_BMC_DWTOPDU (u1_addr_next_dvmrpRouteNextHopSource,
                                     u4_addr_dvmrpRouteNextHopSource);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_dvmrpRouteNextHopSource[i4_count];
                    u4_addr_dvmrpRouteNextHopSourceMask =
                        u4_addr_next_dvmrpRouteNextHopSourceMask;
                    CRU_BMC_DWTOPDU (u1_addr_next_dvmrpRouteNextHopSourceMask,
                                     u4_addr_dvmrpRouteNextHopSourceMask);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_dvmrpRouteNextHopSourceMask[i4_count];
                    i4_dvmrpRouteNextHopControlIndex =
                        i4_next_dvmrpRouteNextHopControlIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpRouteNextHopControlIndex;
                    i4_dvmrpRouteNextHopPortNum =
                        i4_next_dvmrpRouteNextHopPortNum;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_dvmrpRouteNextHopPortNum;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case DVMRPROUTENEXTHOPSOURCE:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_dvmrpRouteNextHopSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_next_dvmrpRouteNextHopSource);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DVMRPROUTENEXTHOPSOURCEMASK:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_dvmrpRouteNextHopSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                CRU_BMC_DWTOPDU (u1_octet_string,
                                 u4_addr_next_dvmrpRouteNextHopSourceMask);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case DVMRPROUTENEXTHOPCONTROLINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpRouteNextHopControlIndex;
            }
            else
            {
                i4_return_val = i4_next_dvmrpRouteNextHopControlIndex;
            }
            break;
        }
        case DVMRPROUTENEXTHOPPORTNUM:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == EXACT) || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_dvmrpRouteNextHopPortNum;
            }
            else
            {
                i4_return_val = i4_next_dvmrpRouteNextHopPortNum;
            }
            break;
        }
        case DVMRPROUTENEXTHOPTYPE:
        {
            i1_ret_val =
                nmhGetDvmrpRouteNextHopType (u4_addr_dvmrpRouteNextHopSource,
                                             u4_addr_dvmrpRouteNextHopSourceMask,
                                             i4_dvmrpRouteNextHopControlIndex,
                                             i4_dvmrpRouteNextHopPortNum,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dvmrpScalarGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
#ifdef __STDC__
tSNMP_VAR_BIND     *
dvmrpScalarGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                UINT1 u1_arg, UINT1 u1_search_type)
#else
tSNMP_VAR_BIND     *
dvmrpScalarGet (p_in_db, p_incoming, u1_arg, u1_search_type)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     UINT1               u1_search_type;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_dvmrpScalar_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val;
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

    tSNMP_OCTET_STRING_TYPE *poctet_retval_dvmrpVersionString = NULL;
   /*** $$TRACE_LOG (ENTRY,"dvmrpScalarGet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_search_type = %d\n",u1_search_type); ***/

/*** DECLARATION_END ***/

    u8_counter_val.msn = 0;
    u8_counter_val.lsn = 0;

    LEN_dvmrpScalar_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_dvmrpScalar_INDEX++;
    if (u1_search_type == EXACT)
    {
        if ((LEN_dvmrpScalar_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_dvmrpScalar_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case DVMRPVERSIONSTRING:
        {
            poctet_retval_dvmrpVersionString =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (65537);
            if (poctet_retval_dvmrpVersionString == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetDvmrpVersionString (poctet_retval_dvmrpVersionString);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_dvmrpVersionString;
            }
            else
            {
                free_octetstring (poctet_retval_dvmrpVersionString);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPGENERATIONID:
        {
            i1_ret_val = nmhGetDvmrpGenerationId (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPNUMROUTES:
        {
            i1_ret_val = nmhGetDvmrpNumRoutes (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPREACHABLEROUTES:
        {
            i1_ret_val = nmhGetDvmrpReachableRoutes (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPSTATUS:
        {
            i1_ret_val = nmhGetDvmrpStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPLOGENABLED:
        {
            i1_ret_val = nmhGetDvmrpLogEnabled (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPLOGMASK:
        {
            i1_ret_val = nmhGetDvmrpLogMask (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case DVMRPMAXSRCS:
        {
            i1_ret_val = nmhGetDvmrpMaxSrcs (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = DVMRP_ZERO;
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i2_type); ***/
   /*** $$TRACE_LOG (EXIT,"  u4_counter_val = %u\n",u4_counter_val); ***/
   /*** $$TRACE_LOG (EXIT,"  i2_type = %d\n",i4_return_val); ***/

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : dvmrpScalarSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
#ifdef __STDC__
INT4
dvmrpScalarSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
dvmrpScalarSet (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;
     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
   /*** $$TRACE_LOG (ENTRY,"dvmrpScalarSet Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case DVMRPSTATUS:
        {
            i1_ret_val = nmhSetDvmrpStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DVMRPLOGENABLED:
        {
            i1_ret_val = nmhSetDvmrpLogEnabled (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DVMRPLOGMASK:
        {
            i1_ret_val = nmhSetDvmrpLogMask (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case DVMRPMAXSRCS:
        {
            i1_ret_val = nmhSetDvmrpMaxSrcs (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {

                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case DVMRPVERSIONSTRING:
            /*  Read Only Variables. */
        case DVMRPGENERATIONID:
            /*  Read Only Variables. */
        case DVMRPNUMROUTES:
            /*  Read Only Variables. */
        case DVMRPREACHABLEROUTES:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : dvmrpScalarTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

#ifdef __STDC__
INT4
dvmrpScalarTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                 UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
#else
INT4
dvmrpScalarTest (p_in_db, p_incoming, u1_arg, p_value)
     tSNMP_OID_TYPE     *p_in_db;
     tSNMP_OID_TYPE     *p_incoming;
     UINT1               u1_arg;

     tSNMP_MULTI_DATA_TYPE *p_value;
#endif
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */
   /*** $$TRACE_LOG (ENTRY,"dvmrpScalarTest Routine\n"); ***/
   /*** $$TRACE_LOG (ENTRY,"u1_arg = %d\n",u1_arg); ***/

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_INCONSISTENT_NAME);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case DVMRPSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DvmrpStatus (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DVMRPLOGENABLED:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DvmrpLogEnabled (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DVMRPLOGMASK:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DvmrpLogMask (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case DVMRPMAXSRCS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2DvmrpMaxSrcs (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case DVMRPVERSIONSTRING:
        case DVMRPGENERATIONID:
        case DVMRPNUMROUTES:
        case DVMRPREACHABLEROUTES:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
