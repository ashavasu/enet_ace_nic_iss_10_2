/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpinput.c,v 1.24 2014/01/24 12:24:55 siva Exp $
 *
 * Description:This file contains the various functions to    
 *             handle Events from IP , IGMP and TimerExpiry   
 *             Event                                        
 *
 *******************************************************************/
#include "dpinc.h"
#include "fssocket.h"

INT4                gi4DvmrpRegId;
/****************************************************************************/
/* Function Name    :  DvmrpIhmMainInit                                     */
/* Description      :  This function initializes all the global data        */
/*                     structures and initializes the required timers       */
/* Input (s)        :  None                                                 */
/* Output (s)       :  All global data structures and timers initialized    */
/* Returns          :  Returns DVMRP_OK on allocating memory for global     */
/*                     data structures and DVMRP_NOTOK on failure.          */
/****************************************************************************/
#ifdef __STDC__
INT4
DvmrpIhmMainInit (void)
#else
INT4
DvmrpIhmMainInit (void)
#endif
{
    if (DvmrpSpawnTask () == (UINT1) DVMRP_NOTOK)
    {
        return (DVMRP_NOTOK);
    }

    return (DVMRP_OK);
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmStartTimers                                  */
/* Description      :  This function starts the CacheAge out timer, Route   */
/*                     age out timer, Neighbor Age out and Route Report     */
/*                     Timer.                                               */
/* Input (s)        :  None                                                 */
/* Output (s)       :  The ageout timers and route report timers are started*/
/* Returns          :  Returns DVMRP_OK on Success & DVMRP_NOTOK on failure */
/****************************************************************************/

#ifdef __STDC__
INT4
DvmrpIhmStartTimers (void)
#else
INT4
DvmrpIhmStartTimers (void)
#endif
{

    if (DvmrpUtilStartTimer (DVMRP_ROUTE_REPORT_TIMER,
                             DVMRP_ROUTE_REPORT_INTERVAL, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to start Route Report Timer"));

        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Started Route Report Timer successfully"));

    if (DvmrpUtilStartTimer (DVMRP_ROUTE_AGEOUT_TIMER, DVMRP_ADVANCE_TIMER_VAL,
                             NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Route Report Age out Start Failure"));
        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Started Route Ageout Timer successfully"));

    if (DvmrpUtilStartTimer (DVMRP_CACHE_AGEOUT_TIMER,
                             DVMRP_CACHE_AGE_OUT_TIMER_VAL,
                             NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Cache age out timer start Failure "));
        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Started Cache Ageout Timer successfully"));

    if (DvmrpUtilStartTimer (DVMRP_NEIGHBOR_AGEOUT_TIMER,
                             DVMRP_NBR_AGE_OUT_TIMER_VAL, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Neighbor age out timer start Failure "));
        return (DVMRP_NOTOK);
    }
    LOG1 ((INFO, IHM, "Started Neighbor Ageout Timer successfully"));

    return (DVMRP_OK);
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmHandleStartup                                */
/* Description      :  This function initializes the timers, registers dvmrp*/
/*                     IP and IGMP and initalizes the Iface list and table  */
/* Input (s)        :  None                                                 */
/* Output (s)       :  DvmrpRegisters in IP,IGMP and initializes the Iface  */
/*                     Table and array                                      */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
INT4
DvmrpIhmHandleStartup (void)
#else
INT4
DvmrpIhmHandleStartup (void)
#endif
{

    if (DvmrpInit () == DVMRP_NOTOK)
    {
        DvmrpIhmFreeDataStructures ();
        LOG1 ((CRITICAL, MAIN, "Unable to Initialize Dvmrp Tables"));
        return DVMRP_NOTOK;
    }

    if (DvmrpInitIfaceTimersAndBuffer () == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to init Timers \n"));
        DvmrpIhmFreeDataStructures ();
        DvmrpIhmHandleShutdown ();
        return DVMRP_NOTOK;
    }

#ifdef LNXIP4_WANTED
    /* Create Socket for IGMP packets and set the socket options */
    if (DvmrpInitAndAddSocket () == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to init socket \n"));
        DvmrpIhmFreeDataStructures ();
        DvmrpIhmHandleShutdown ();
        return DVMRP_NOTOK;
    }
#endif
    if (DvmrpRegisterWithExtModules () == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable register with external interface \n"));
        DvmrpIhmFreeDataStructures ();
        DvmrpIhmHandleShutdown ();
        return DVMRP_NOTOK;
    }
    gu1DvmrpStatus = DVMRP_ENABLED;
    return DVMRP_OK;
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmHandleShutdown                               */
/* Description      :  This function stops all the timers, deregisters dvmrp*/
/*                     with IP & IGMP and frees all the global tables       */
/* Input (s)        :  None                                                 */
/* Output (s)       :  DOWN event is sent to IGMP and dvmrp deregisters from*/
/*                     IP and frees all the tables allocated                */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpIhmHandleShutdown (void)
#else
void
DvmrpIhmHandleShutdown (void)
#endif
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tDvmrpInterfaceNode *pIfNode = NULL;
    gu1DvmrpStatus = DVMRP_DISABLED;

    DvmrpIhmHandleInputQMsgs ();
    /* 
     * Added another function Pointer to deregister with
     * IGMP for receiving Host Membership packets 
     */

    /* Stop all the timers  */
    if (DvmrpUtilStopTimer (DVMRP_ROUTE_REPORT_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop Route Report Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_ROUTE_AGEOUT_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop route ageout Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_CACHE_AGEOUT_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop cache ageout Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_NEIGHBOR_AGEOUT_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop Nbr ageout Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_PROBE_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop the probe Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_GRAFT_RETRANS_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop graft retransmission Timer"));
    }

    if (DvmrpUtilStopTimer (DVMRP_PRUNE_RETRANS_TIMER, NULL) == DVMRP_NOTOK)
    {
        LOG1 ((WARNING, IHM, "Unable to stop prune retransmission Timer"));
    }

    if (g1sTimerListId != 0)
    {
        TmrDeleteTimerList (g1sTimerListId);
        g1sTimerListId = 0;
    }

#ifdef LNXIP4_WANTED
    /* reset all the socket information */
    DvmrpDeInitAndRemoveSocket ();
#endif

    /*Deregister with all the external module  */
    DvmrpDeRegisterWithExtModules ();

    DvmrpRthHandleShutdownEvent ();

    if (DVMRP_IFACE_TBL_PID != 0)
    {
        while ((pSllNode = (tTMO_SLL_NODE *)
                TMO_SLL_First (&(gDvmrpIfInfo.IfGetNextList))) != NULL)
        {
            pIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                          IfGetNextLink, pSllNode);

            DvmrpDeleteInterfaceNode (pIfNode);
        }
        MemDeleteMemPool (DVMRP_IFACE_TBL_PID);
        gDvmrpMemPool.IfaceTblPoolId = 0;
        LOG1 ((INFO, IHM, "Iface Table Freed Successfully"));
    }
    if (gDvmrpIfInfo.IfHashTbl != NULL)
    {
        DVMRP_HASH_DELETE_TABLE (gDvmrpIfInfo.IfHashTbl, NULL);
        gDvmrpIfInfo.IfHashTbl = NULL;
    }

    DvmrpIhmFreeDataStructures ();
    DvmrpGlobalTableFree ();

    if (DVMRP_TIMER_PID != 0)
    {
        MemDeleteMemPool (DVMRP_TIMER_PID);
        gDvmrpMemPool.TimerPoolId = 0;
        LOG1 ((INFO, IHM, "Timer Pool Freed Successfully"));
    }

    if (DVMRP_Q_PID != 0)
    {
        MemDeleteMemPool (DVMRP_Q_PID);
        gDvmrpMemPool.QPoolId = 0;
        LOG1 ((INFO, IHM, "Misc Pool Freed Successfully"));
    }
    if (DVMRP_MISC_PID != 0)
    {
        MemDeleteMemPool (DVMRP_MISC_PID);
        gDvmrpMemPool.MiscPoolId = 0;
        LOG1 ((INFO, IHM, "Misc Pool Freed Successfully"));
    }

    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmFreeDataStructures                           */
/* Description      :  This function frees all the tables that are malloced.*/
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

/* changes made in this function - 15/7 - Iface free pool and Nbr free pool
 * Freed, using the Global pointers' values
 */
#ifdef __STDC__
void
DvmrpIhmFreeDataStructures (void)
#else
void
DvmrpIhmFreeDataStructures (void)
#endif
{
    UINT4               u4Index;
    UINT1               u1Key;
    tForwCacheTbl      *pFwdCacheTbl = NULL;
    tForwTbl           *pFwdTbl = NULL;
    tHostMbrTbl        *pHostTbl = NULL;
    tNbrTbl            *pNbrTbl = NULL;
    tRouteTbl          *pRouteTbl = NULL;
    tNextHopTbl        *pNextHopTbl = NULL;

    if (DVMRP_FWD_CACHE_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
        {
            pFwdCacheTbl = DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index);
            if ((pFwdCacheTbl != NULL) && (pFwdCacheTbl->pSrcNwInfo != NULL))
            {
                DVMRP_GET_HASH_INDEX (pFwdCacheTbl->u4SrcAddress,
                                      pFwdCacheTbl->u4DestGroup, u1Key);
                DVMRP_HASH_DELETE_NODE (gpForwHashTable,
                                        &pFwdCacheTbl->Next, u1Key);
            }
        }
    }                            /* if( pFwdCacheTbl */

    if (DVMRP_FWD_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_FORW_TABLE_ENTRIES; u4Index++)
        {
            pFwdTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u4Index);
            if (pFwdTbl != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.ForwTblPoolId,
                                 (UINT1 *) pFwdTbl);
                pFwdTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_FWD_TBL_PID);
        gDvmrpMemPool.ForwTblPoolId = 0;
        LOG1 ((INFO, IHM, "ForwTable Freed Successfully"));
    }

    if (DVMRP_FWD_CACHE_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_FORW_CACHE_TABLE_ENTRIES; u4Index++)
        {
            pFwdCacheTbl = DVMRP_GET_FWD_CACHENODE_FROM_INDEX (u4Index);
            if (pFwdCacheTbl != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.ForwCacheTblPoolId,
                                 (UINT1 *) pFwdCacheTbl);
                pFwdCacheTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_FWD_CACHE_TBL_PID);
        gDvmrpMemPool.ForwCacheTblPoolId = 0;
        LOG1 ((INFO, IHM, "Forward Cache Table Freed Successfully"));
        gpForwFreePtr = NULL;
    }

    if (DVMRP_HOST_MBR_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++)
        {
            pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);
            if (pHostTbl != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.HostMbrTblPoolId,
                                 (UINT1 *) pHostTbl);
                pHostTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_HOST_MBR_TBL_PID);
        gDvmrpMemPool.HostMbrTblPoolId = 0;
        LOG1 ((INFO, IHM, "Host member Table Freed Successfully"));
    }

    if (DVMRP_NBR_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_NBR_TABLE_ENTRIES; u4Index++)
        {
            pNbrTbl = DVMRP_GET_NBRNODE_FROM_INDEX (u4Index);
            if (pNbrTbl != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.NbrTblPoolId, (UINT1 *) pNbrTbl);
                pNbrTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_NBR_TBL_PID);
        gDvmrpMemPool.NbrTblPoolId = 0;
        LOG1 ((INFO, IHM, "Neighbor Table Freed Successfully"));
    }

    if (DVMRP_ROUTE_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
        {
            pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);
            if (pRouteTbl != NULL)
            {
                if (pRouteTbl->u1Status != DVMRP_INACTIVE)
                {
                    DVMRP_GET_HASH_INDEX (pRouteTbl->u4SrcNetwork,
                                          pRouteTbl->u4SrcMask, u1Key);
                    DVMRP_HASH_DELETE_NODE (gpRouteHashTable,
                                            &pRouteTbl->RouteEntryNext, u1Key);
                }
                DvmrpMemRelease (gDvmrpMemPool.RouteTblPoolId,
                                 (UINT1 *) pRouteTbl);
                pRouteTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_ROUTE_TBL_PID);
        gDvmrpMemPool.RouteTblPoolId = 0;
        LOG1 ((INFO, IHM, "Route Table Freed Successfully"));
        gpRouteFreePtr = NULL;
    }

    if (DVMRP_NEXT_HOP_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
        {
            pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
            if (pNextHopTbl != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.NextHopTblPoolId,
                                 (UINT1 *) pNextHopTbl);
                pNextHopTbl = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_NEXT_HOP_TBL_PID);
        gDvmrpMemPool.NextHopTblPoolId = 0;
        LOG1 ((INFO, IHM, "NextHop Table Freed Successfully"));
    }

    if (DVMRP_IFACE_LIST_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_IFACE_FREE_POOL_TABLE_ENTRIES;
             u4Index++)
        {
            if ((*(gpGlobIfacePtr + u4Index)) != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.IfaceListPoolId,
                                 (UINT1 *) (*(gpGlobIfacePtr + u4Index)));
                (*(gpGlobIfacePtr + u4Index)) = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_IFACE_LIST_PID);
        gDvmrpMemPool.IfaceListPoolId = 0;
        LOG1 ((INFO, IHM, "Interface Free pool Table Freed Successfully"));
        gpFreeIfacePtr = NULL;
    }

    if (DVMRP_NBR_LIST_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_NBR_FREE_POOL_TABLE_ENTRIES; u4Index++)
        {
            if (*(gpGlobNbrPtr + u4Index) != NULL)
            {
                DvmrpMemRelease (gDvmrpMemPool.NbrListPoolId,
                                 (UINT1 *) (*(gpGlobNbrPtr + u4Index)));
                (*(gpGlobNbrPtr + u4Index)) = NULL;
            }
        }
        MemDeleteMemPool (DVMRP_NBR_LIST_PID);
        gDvmrpMemPool.NbrListPoolId = 0;
        LOG1 ((INFO, IHM, "Nbr Free pool Table Freed Successfully"));
        gpFreeNbrPtr = NULL;
    }

    if (gpRouteHashTable != NULL)
    {
        DVMRP_HASH_DELETE_TABLE (gpRouteHashTable, NULL);
        gpRouteHashTable = NULL;
    }
    if (gpForwHashTable != NULL)
    {
        DVMRP_HASH_DELETE_TABLE (gpForwHashTable, NULL);
        gpForwHashTable = NULL;
    }

    return;
}

/*****************************************************************************/
/* Function Name    :  DvmrpIhmProcessProtocolPackets                        */
/* Description      :  This function validates the dvmrp protocol packet     */
/*                     and forwards the data to the destined module          */
/* Input (s)        :  u4ValidOffset - Indicates the start of dvmrp packet   */
/*                     u4IfIndex     - Index into Interface Table            */
/* Output (s)       :  None                                                  */
/* Returns          :  None                                                  */
/*****************************************************************************/

#ifdef __STDC__
void
DvmrpIhmProcessProtocolPackets (UINT4 u4ValidOffset, UINT4 u4IfIndex)
#else
void
DvmrpIhmProcessProtocolPackets (u4ValidOffset, u4IfIndex)
     UINT4               u4ValidOffset;
     UINT4               u4IfIndex;
#endif
{
    tDvmrpInterfaceNode *pIfaceTbl = NULL;
    tNbrTbl            *pNbrOnIface = NULL;
    UINT4               u4SrcAddr = 0, u4DataLen = 0;
    UINT2               u2CheckSum, u2CalCheckSum;
    UINT2               u2Len = 0;
    UINT1               u1Flag = DVMRP_FALSE, u1Code = 0;
    UINT1               u1NbrCap = 0;

    u4SrcAddr = OSIX_NTOHL (*((UINT4 *) (VOID *)
                              &gpRcvBufferRead[SRC_IP_ADDR_OFFSET]));

    if (VALIDATE_UCAST_ADDR (u4SrcAddr) == 0)
    {
        LOG1 ((WARNING, IHM, "ZERO Source address received"));
        return;
    }

    /*
     * While sending probe and route reports the destination address
     * is 224.0.0.4, in which case, this router may receive the packet
     * which was sent by itself. Hence doing this check.
     */

    pIfaceTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);

    if ((NULL == pIfaceTbl) || (u4SrcAddr == pIfaceTbl->u4IfLocalAddr))
    {
        LOG1 ((INFO, IHM, "Loop around address received"));
        return;
    }

    u2Len = *(UINT2 *) (VOID *) &gpRcvBufferRead[2];
    u2Len = OSIX_NTOHS (u2Len);
    u4DataLen = (UINT4) (u2Len - ((UINT2) u4ValidOffset));

    /*
     * Check sum validation ::
     * The incoming Network Order checksum is converted to Host order value
     * and validated against the checksum that is calculated using the 
     * below function, which calculates the checksum of the incoming data
     * in Host order.
     */

    gpRcvBuffer = &gpRcvBufferRead[u4ValidOffset];
    u2CheckSum = *(UINT2 *) (VOID *) &gpRcvBuffer[2];
    gpRcvBuffer[2] = 0;
    gpRcvBuffer[3] = 0;

    u2CalCheckSum = (UINT2) DvmrpUtilCheckSum (gpRcvBuffer, (INT2) u4DataLen);

    /*
     * Added a switch here for UNIT Testing, as the packets
     * received are GPS packets
     */

    if (u2CheckSum != u2CalCheckSum)
    {
        LOG1 ((WARNING, IHM, "Invalid Checksum"));
        pIfaceTbl->u4IfRcvBadPkts++;
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }
    LOG1 ((INFO, IHM, "Valid Checksum"));

    if (gpRcvBuffer[4] == DVMRP_RESERVED)
    {
        LOG1 ((INFO, IHM, "Valid Reserved"));
    }
    else
    {
        LOG1 ((WARNING, IHM, "Invalid Reserved"));
        pIfaceTbl->u4IfRcvBadPkts++;
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }

    u1NbrCap = gpRcvBuffer[5];

    if (((gpRcvBuffer[6] == DVMRP_MINOR_VERSION) &&
         (gpRcvBuffer[7] == DVMRP_MAJOR_VERSION))
#ifdef INTEROP
        || ((gpRcvBuffer[6] == INTEROP_MINOR_VERSION) &&
            (gpRcvBuffer[7] == INTEROP_MAJOR_VERSION))
#endif
        )
    {
        LOG1 ((INFO, IHM, "Valid Version"));
    }
    else
    {
        LOG1 ((WARNING, IHM, "Invalid version"));
        pIfaceTbl->u4IfRcvBadPkts++;
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }
    LOG1 ((INFO, IHM, "Valid Version"));

    u1Code = gpRcvBuffer[1];

    /* 
     * Copy the Data Portion to the Global Receive Buffer
     */

    u4DataLen = u4DataLen - MAX_IGMP_HDR_LEN;

    if (u4DataLen == 0)
    {
        pIfaceTbl->u4IfRcvBadPkts++;
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }
    gpRcvBuffer = &gpRcvBufferRead[u4ValidOffset + MAX_IGMP_HDR_LEN];
    if (u1Code == DVMRP_PROBE)
    {
        DvmrpNdmHandleProbePacket (u4DataLen, u4IfIndex, u4SrcAddr, u1NbrCap);
        return;
    }

    /* 
     * Check if the packet is received from a valid neighbor
     */

    u1Flag = DVMRP_FALSE;
    pNbrOnIface = pIfaceTbl->pNbrOnthisIface;

    for (; pNbrOnIface != NULL; pNbrOnIface = pNbrOnIface->pNbrTblNext)
    {
        if (pNbrOnIface->u4NbrIpAddress == u4SrcAddr)
        {
            if ((pNbrOnIface->u1Status == DVMRP_ACTIVE) &&
                (pNbrOnIface->u1SeenByNbrFlag == DVMRP_TRUE))
            {
                u1Flag = DVMRP_TRUE;
                break;
            }
        }
    }

    if (u1Flag == DVMRP_FALSE)
    {
        LOG1 ((WARNING, IHM, "Invalid Neighbor"));
        pIfaceTbl->u4IfRcvBadPkts++;
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }

    switch (u1Code)
    {

        case DVMRP_REPORT:
            DvmrpRthHandleRouteReport (u4DataLen, u4IfIndex, pNbrOnIface);
            break;

        case DVMRP_PRUNE:
            DvmrpPmHandlePrunePacket (u4IfIndex, u4DataLen, pNbrOnIface);
            break;

        case DVMRP_GRAFT:
            DvmrpGmHandleGraftPacket (u4IfIndex, u4DataLen, pNbrOnIface);
            break;

        case DVMRP_GRAFTACK:
            DvmrpGmHandleGraftAckPacket (u4IfIndex, u4DataLen, pNbrOnIface);
            break;
        default:
            LOG1 ((WARNING, IHM, "Invalid Code"));
            pIfaceTbl->u4IfRcvBadPkts++;
            pNbrOnIface->u4NbrRcvBadPkts++;
            LOG1 ((INFO, IHM, "Interface Stats and Neighbor Stats updated"));
            break;
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmProcessHostPackets                       */
/* Description      :  This function extracts the interface, destination    */
/*                     group and flag from the Igmp Packet                  */
/* Input (s)        :  None                                                 */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpIhmProcessHostPackets (void)
#else
void
DvmrpIhmProcessHostPackets (void)
#endif
{
    tIgmpHost           IgmpHost;

    MEMSET (&IgmpHost, 0, sizeof (tIgmpHost));
    MEMCPY (&IgmpHost, (tIgmpHost *) (VOID *) gpRcvBufferRead,
            sizeof (tIgmpHost));

    if (VALIDATE_MCAST_ADDR (IgmpHost.u4DestGroup) == 0)
    {
        LOG1 ((WARNING, IHM,
               "Invalid Destination Group in IGMP Host Membership Packet"));
        LOG1 ((INFO, IHM, "Interface Statistics updated"));
        return;
    }

    DvmrpHmHandleIgmpHostInfo (IgmpHost.u4IfIndex, IgmpHost.u4DestGroup,
                               IgmpHost.u1Flag);
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpIhmHandleInterfaceUpdation                      */
/* Description      :  This function is invoked whenever an Interface       */
/*                     Event is received Locally                            */
/* Input (s)        :  None                                                 */
/* Output (s)       :  Interface Updated                                    */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpIhmHandleInterfaceUpdation (tDvmrpIfStatus * pDvmrpIfStatus)
#else
void
DvmrpIhmHandleInterfaceUpdation (tDvmrpIfStatus * pDvmrpIfStatus)
#endif
{
    tNetIpMcastInfo     NetIpMcastInfo;
    tDvmrpInterfaceNode *pIfaceTbl = NULL;
    tDvmrpInterfaceNode *pCurIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT4               u4Var, u4BitMap;
    UINT4               u4IfIndex = 0;
    UINT4               u4HashIndex;
    UINT1               u1IfaceInactive = DVMRP_FALSE;
    UINT1               u1FieldUpdated = DVMRP_FALSE;
    UINT1               u1OperFlag = DVMRP_ZERO;
    UINT1               u1AdminFlag = DVMRP_ZERO;
    UINT1               u1Value;

    LOG1 ((INFO, IHM, "Interface Update Pkt Buffer released"));

    u4BitMap = pDvmrpIfStatus->u4IfBitMap;

    if (u4BitMap & DEL_BIT_SET)
    {
        TMO_SLL_Scan (&gDvmrpIfInfo.IfGetNextList, pSllNode, tTMO_SLL_NODE *)
        {
            pCurIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode, IfGetNextLink,
                                             pSllNode);
            if (pCurIfNode->u4IfIndex == pDvmrpIfStatus->u4IfIndex)
            {
                pIfaceTbl = pCurIfNode;
                u4IfIndex = pIfaceTbl->u4IfIndex;
                break;
            }
        }
    }
    else
    {
        u4IfIndex = pDvmrpIfStatus->u4IfIndex;
        pIfaceTbl = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
    }

    if ((pIfaceTbl == NULL) || (!(DVMRP_CHECK_IF_ROW_STATUS (pIfaceTbl))))
    {
        LOG1 ((INFO, IHM, "Interface Update Rcvd for Iface Table - NULL"));
        return;
    }

    u1OperFlag = (UINT1) pDvmrpIfStatus->u4Oper;
    u1AdminFlag = (UINT1) pDvmrpIfStatus->u4Admin;

    for (u4Var = 0; u4Var < 7; u4Var++)
    {
        u1Value = (UINT1) ((u4BitMap >> u4Var) & LAST_BIT_SET);
        if (u1Value == 0)
        {
            continue;
        }

        switch (u4Var)
        {
/* Code changed so as to treat Admin or Oper status' notification
 * in a similar manner and Dvmrp's interface status will be enabled only
 * if both of them are made up. But dvmrp's interface status will be made
 * down if either of them is made down 
 */
            case BIT_ADMIN_STATUS:
            case BIT_OPER_STATUS:

                if ((u1AdminFlag == IPIF_ADMIN_ENABLE) &&
                    (u1OperFlag == IPIF_OPER_ENABLE))
                {

                    if (pIfaceTbl->u1IfStatus == DVMRP_INACTIVE)
                    {
                        pIfaceTbl->u1IfStatus = DVMRP_ACTIVE;
                        pIfaceTbl->u1IfMetric = DEFAULT_METRIC;
                        pIfaceTbl->u4IfLocalAddr = pDvmrpIfStatus->u4Addr;
                        pIfaceTbl->u4IfMTUSize = pDvmrpIfStatus->u4Mtu;
                        pIfaceTbl->u4IfMask = pDvmrpIfStatus->u4NetMask;
                        pIfaceTbl->u4IfRcvBadPkts = 0;
                        pIfaceTbl->u4IfRcvBadRoutes = 0;
                        pIfaceTbl->pNbrOnthisIface = NULL;
                        gu4NumActiveIfaces++;
                        u1FieldUpdated = DVMRP_TRUE;
                    }
                    else
                    {
                        LOG2 ((INFO, IHM,
                               "Interface already active. Dvmrp's table not"
                               " updated", u4IfIndex));
                        return;
                    }
                }
                else
                {
                    if (pIfaceTbl->u1IfStatus == DVMRP_ACTIVE)
                    {
                        u1IfaceInactive = DVMRP_TRUE;
                        u1FieldUpdated = DVMRP_TRUE;
                    }
                }
                break;

            case BIT_IP_ADDR:
                pIfaceTbl->u4IfLocalAddr = pDvmrpIfStatus->u4Addr;
                u1FieldUpdated = DVMRP_TRUE;
                break;

            case BIT_MASK:
                LOG1 ((INFO, IHM, "Subnet mask has changed "));
                u1FieldUpdated = DVMRP_TRUE;
                break;

            case BIT_IFACE_DEL:
                u1IfaceInactive = DVMRP_TRUE;
                u1FieldUpdated = DVMRP_TRUE;

                MEMSET (&NetIpMcastInfo, 0, sizeof (NetIpMcastInfo));
                NetIpMcastInfo.u4IpPort = pIfaceTbl->u4IfIndex;
                NetIpMcastInfo.u1McastProtocol = DVMRP_ID;
                if (NetIpv4SetMcStatusOnPort (&NetIpMcastInfo, DISABLED)
                    == NETIPV4_FAILURE)
                {
                    return;
                }
                break;

            case BIT_IFACE_MTU:
                pIfaceTbl->u4IfMTUSize = pDvmrpIfStatus->u4Mtu;
                if ((pIfaceTbl->u4IfMTUSize < gu4MinMTU) || (gu4MinMTU == 0))
                {
                    gu4MinMTU = pIfaceTbl->u4IfMTUSize;
                }
                LOG1 ((INFO, IHM, "Mtu changed"));
                break;

            default:
                break;
        }                        /* switch */

        if (u1IfaceInactive == DVMRP_TRUE)
        {
            LOG2 ((INFO, IHM, "Interface made inactive", u4IfIndex));
            break;
        }

    }                            /* for */

    if (u1FieldUpdated == DVMRP_TRUE)
    {
        if (u1IfaceInactive == DVMRP_TRUE)
        {
            if ((pIfaceTbl->u1IfStatus == DVMRP_ACTIVE) ||
                (pIfaceTbl->u1IfRowStatus == DVMRP_ROW_STATUS_ACTIVE))
            {
                LOG1 ((INFO, IHM, "The Interface Index status changed"));
                LOG1 ((INFO, IHM, "Interface Changed from ACTIVE to INACTIVE"));
                DvmrpNdmHandleIfaceDown (u4IfIndex);
                DvmrpRthLocIfaceUpdate (pIfaceTbl->u4IfLocalAddr, 0,
                                        DEFAULT_METRIC,
                                        u4IfIndex, DVMRP_INACTIVE);
                pIfaceTbl->u1IfStatus = DVMRP_INACTIVE;

                if (u4Var == BIT_IFACE_DEL)
                {
                    /* Now Release the InterfaceNode */
                    TMO_SLL_Delete (&(gDvmrpIfInfo.IfGetNextList),
                                    &pIfaceTbl->IfGetNextLink);

                    DVMRP_GET_IF_HASHINDEX (pIfaceTbl->u4IfIndex, u4HashIndex);
                    TMO_HASH_Delete_Node (gDvmrpIfInfo.IfHashTbl,
                                          &(pIfaceTbl->IfHashLink),
                                          u4HashIndex);
                    /* Now Release the InterfaceNode */
                    DvmrpMemRelease (gDvmrpMemPool.IfaceTblPoolId,
                                     (UINT1 *) pIfaceTbl);
                }
                gu4NumActiveIfaces--;

            }
            /*
             * Since the interface is already INACTIVE need not do anything
             */
        }
        else if (pIfaceTbl->u1IfStatus == DVMRP_ACTIVE)
        {
            gu4GenerationId = DvmrpCurrentTime ();
            LOG2 ((INFO, NDM, "Generation ID is Updated with new value:",
                   gu4GenerationId));
            DvmrpNdmSndProbeOnIface (pIfaceTbl);
            if (DvmrpUtilStartTimer (DVMRP_PROBE_TIMER,
                                     DVMRP_PROBE_INTERVAL, NULL) == DVMRP_NOTOK)
            {
                LOG1 ((WARNING, NDM, "Unable to start Probe Timer"));
            }
            else
            {
                LOG1 ((INFO, NDM, "Started Probe Timer"));
            }
            DvmrpRthLocIfaceUpdate (pIfaceTbl->u4IfLocalAddr,
                                    pDvmrpIfStatus->u4NetMask,
                                    pIfaceTbl->u1IfMetric,
                                    u4IfIndex, pIfaceTbl->u1IfStatus);
            LOG1 ((INFO, IHM, "The Interface Index status changed"));
            LOG1 ((INFO, IHM, "Interface Changed from INACTIVE to ACTIVE"));
        }
    }
    return;
}

#ifdef LNXIP4_WANTED
/***************************************************************************
 * Function Name    :  DvmrpInitAndAddSocket 
 *
 * Description      :  This function creates socket for receiving DVMRP 
 *                     control packets, sets default socket options needed 
 *                     and adds the Fd to Select utility.
 *
 * Global Variables
 * Referred         : gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         : gDpConfigParams.i4DvmrpSockId - created socket id is 
 *                    stored.
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK if socket creation is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpInitAndAddSocket ()
{
    if (DvmrpCreateSocket () == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to create a socket for packets \n"));
        return DVMRP_NOTOK;
    }

    if (DvmrpSetSocketOption () == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to set send socket options \n"));
        /* Socket has to be closed */
        DvmrpCloseSocket ();
        return DVMRP_NOTOK;
    }
    /* Add to FD_SET */
    SelAddFd (gDpConfigParams.i4DvmrpSockId, DvmrpNotifyPktArrivalEvent);
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpDeInitAndRemoveSocket 
 *
 * Description      :  This function removes the DVMRP socket from Select 
 *                     utility and closes the socket. 
 *
 * Global Variables
 * Referred         : gDpConfigParams.i4DvmrpSockId 
 *
 * Global Variables
 * Modified         : gDpConfigParams.i4DvmrpSockId - created socket id is 
 *                    stored.
 *
 * Input (s)        : None  
 *
 * Output (s)       : None
 *
 * Returns          : None 
 ****************************************************************************/

VOID
DvmrpDeInitAndRemoveSocket ()
{
    if (gDpConfigParams.i4DvmrpSockId != -1)
    {
        /* remove to Fd from FD_SET */
        SelRemoveFd (gDpConfigParams.i4DvmrpSockId);
        DvmrpCloseSocket ();
    }
}
#endif
/***************************************************************************
 * Function Name    :  DvmrpRegisterWithExtModules 
 *
 * Description      :  This function registers with the external modules like 
 *                     IGMP - currently IGMP gives IgmpHost packets, DVMRP 
 *                     control packet and Interface status change info to 
 *                     DVMRP,  with Linux IP DVMRP gets the Control packets 
 *                     directly from socket, MFWD or with char device for 
 *                     receiving multicast data packets    
 *
 * Global Variables
 * Referred         : None 
 *
 * Input (s)        :  None  
 *
 * Output (s)       :  None
 *
 * Returns          : DVMRP_OK if registration is success else 
 *                    DVMRP_NOTOK
 ****************************************************************************/

INT4
DvmrpRegisterWithExtModules ()
{
#ifdef MFWD_WANTED
    INT4                i4Status;
#endif /* MFWD_WANTED */
#ifdef IGMP_WANTED
#ifdef LNXIP4_WANTED
    /* With Linux IP register with IGMP to get IGMP report information, 
       to get Interface change information, DVMRP control packets will
       be received through socket  */
    if (DVMRP_REGISTER_WITH_IGMP (DVMRP_PROTOCOL_ID,
                                  NULL,
                                  DvmrpUpdateIfStatus, NULL,
                                  DvmrpHandleHostPkt) != IGMP_OK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to register with IGMP"));
        return DVMRP_NOTOK;
    }
#else
    /* With FS IP register wiht IGMP for receiving DVMRP protocol 
       packets, to get IGMP report information, to get Interface 
       change information */
    if (DVMRP_REGISTER_WITH_IGMP (DVMRP_PROTOCOL_ID,
                                  DvmrpHandleProtocolPackets,
                                  DvmrpUpdateIfStatus, NULL,
                                  DvmrpHandleHostPkt) != IGMP_OK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to register with IGMP"));
        return DVMRP_NOTOK;
    }
#endif
#endif

#ifdef MFWD_WANTED
    DVMRP_MFWD_REGISTER_MRP (DVMRP_RTR_ID, i4Status);
    if (i4Status == DVMRP_OK)
    {
        LOG1 ((CRITICAL, IHM, "Registration with MFWD Succesfull"));
    }
    else
    {
        LOG1 ((CRITICAL, IHM, "Failure in registering with MFWD"));
        return DVMRP_NOTOK;
    }
#else
    if (DVMRP_REGISTER_WITH_IP_FOR_MDP (gDpConfigParams.i4IpRegnId)
        == DVMRP_NOTOK)
    {
        return DVMRP_NOTOK;
    }
#endif
    return DVMRP_OK;
}

/***************************************************************************
 * Function Name    :  DvmrpDeRegisterWithExtModules 
 *
 * Description      :  This function Deregisters with the external modules 
 *                     like IGMP - IGMP gives IgmpHost packets, MFWD or with 
 *                     char device.    
 *
 * Global Variables
 * Modified         : gDpConfigParams.i4IpRegnId - An Id returned from IP 
 *                    after registration. 
 *
 * Input (s)        : None  
 *
 * Output (s)       : None
 *
 * Returns          : None 
 ****************************************************************************/

VOID
DvmrpDeRegisterWithExtModules ()
{
#ifdef IGMP_WANTED
    DVMRP_DEREGISTER_WITH_IGMP (DVMRP_PROTOCOL_ID);
#endif
#ifdef MFWD_WANTED
    DVMRP_MFWD_DEREGISTER_MRP (DVMRP_RTR_ID);
#else
    DVMRP_DEREGISTER_WITH_IP_FOR_MDP (gDpConfigParams.i4IpRegnId);
    gDpConfigParams.i4IpRegnId = -1;
#endif
}
