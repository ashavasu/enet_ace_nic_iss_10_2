/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpsz.c,v 1.5 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DVMRPSZ_C
#include "dpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DvmrpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DVMRP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsDVMRPSizingParams[i4SizingId].u4StructSize,
                              FsDVMRPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(DVMRPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DvmrpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DvmrpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDVMRPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DVMRPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DvmrpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DVMRP_MAX_SIZING_ID; i4SizingId++)
    {
        if (DVMRPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DVMRPMemPoolIds[i4SizingId]);
            DVMRPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
