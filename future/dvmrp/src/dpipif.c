/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpipif.c,v 1.12 2010/01/07 12:42:56 prabuc Exp $
 *
 * Description:This file contains the functions that are to   
 *             be ported to the target.                      
 *
 *******************************************************************/
#include "dpinc.h"

/* FOR OUTPUT MODULE */
/****************************************************************************/
/* Function Name    :  DvmrpIpSendPacket                                    */
/* Description      :  This function Enqueues the dvmrp Packet to DVMRP_IP  */
/*                     Queue.                                               */
/* Input (s)        :  u4DvmrpLen   - Dvmrp Data length                     */
/*                     u4SrcAddr    - Source IP address                     */
/*                     u4DestAddr   - Destination IP address                */
/* Output (s)       :  u4IfIndex    - Output Interface Index                */
/* Returns          :  None                                                 */
/****************************************************************************/
#ifdef __STDC__
void
DvmrpIpSendPacket (UINT4 u4DvmrpLen, UINT4 u4SrcAddr,
                   UINT4 u4DestAddr, UINT4 u4IfIndex)
#else
void
DvmrpIpSendPacket (u4DvmrpLen, u4SrcAddr, u4DestAddr, u4IfIndex)
     UINT4               u4DvmrpLen, u4SrcAddr, u4DestAddr, u4IfIndex;
#endif
{
#ifndef LNXIP4_WANTED
    tCRU_BUF_CHAIN_HEADER *pBufChain = NULL;
    t_IP_SEND_PARMS    *pParams = NULL;
    UINT4               u4ValidOffset = 0, u4Size = 0;

    u4ValidOffset = MIN_IP_HDR_LEN + LOWER_LAYER_HDR;
    u4Size = u4DvmrpLen + u4ValidOffset;

    if ((pBufChain = (tCRU_BUF_CHAIN_HEADER *)
         CRU_BUF_ALLOCATE_CHAIN (u4Size, u4ValidOffset)) == NULL)
    {
        LOG1 ((CRITICAL, OM, "Unable to allocate CRU Buffer"));
        LOG1 ((INFO, OM, "Packet Not Enqueued"));
        return;
    }

    if (CRU_BUF_COPY_OVER_CHAIN (pBufChain, gpSendBuffer, 0,
                                 u4DvmrpLen) == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, OM, "Unable to copy over Chain"));
        LOG1 ((INFO, OM, "Packet Not Enqueued"));
        CRU_BUF_RELEASE_CHAIN (pBufChain);
        pBufChain = NULL;
        return;
    }

    /* 
     * Fill the IP header 
     */

    pParams = (t_IP_SEND_PARMS *) IP_GET_MODULE_DATA_PTR (pBufChain);

    /* 
     * Set the IP Params 
     */
    /*
     * Variables filled in Params structure
     * should not be converted to network order
     */

    pParams->u1Cmd = IP_LAYER4_DATA;
    pParams->u2Len = (UINT2) u4DvmrpLen;
    pParams->u4Src = u4SrcAddr;
    pParams->u4Dest = u4DestAddr;
    pParams->u2Id = IP_IDENTIFICATION;
    pParams->u1Olen = DEFAULT_OPT_LEN;
    pParams->u1Proto = IGMP_TYPE;
    pParams->u1Tos = DVMRP_DEFAULT_TOS;
    pParams->u1Ttl = DVMRP_DEFAULT_TTL;
    pParams->u1Df = DEFAULT_DF;
    pParams->u2Port = (UINT2) u4IfIndex;

    /* 
     * Give other general information 
     */

    LOG1 ((INFO, OM, "IP Parameters set successfully in CRU buffer"));

    if (IpEnquePktToIpFromHL (pBufChain) == IP_FAILURE)
    {
        return;
    }
#else
    struct sockaddr_in  DestAddr;
    struct in_addr      IfAddr;
    INT4                i4SendBytes = 0;

    /* index is not used */
    u4IfIndex = u4IfIndex;
    MEMSET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);

    IfAddr.s_addr = OSIX_HTONL (u4SrcAddr);

    if (setsockopt (gDpConfigParams.i4DvmrpSockId,
                    IPPROTO_IP, IP_MULTICAST_IF,
                    (char *) &(IfAddr), sizeof (IfAddr)) < 0)
    {
        LOG1 ((CRITICAL, IHM, "setsockopt failed  IP_MULTICAST_IF\n"));
        return;
    }

    if ((i4SendBytes = sendto ((gDpConfigParams.i4DvmrpSockId), gpSendBuffer,
                               u4DvmrpLen, MSG_DONTWAIT,
                               ((struct sockaddr *) &DestAddr),
                               (sizeof (struct sockaddr_in)))) < 0)
    {
        LOG1 ((CRITICAL, IHM, "sendto failed \n"));
        return;
    }
#endif
    return;
}

/*****************************************************************************/
/*    Function Name : DvmrpHandleHostPkt                               */
/*      Description   : Call back function for IGMP to deliver DVMRP Host    */
/*            packets                             */
/*      Input         : Pointer to the Interface Message received from IGMP  */
/*      Output          : None                             */
/*      Returns       : None                             */
/*****************************************************************************/
void
DvmrpHandleHostPkt (tGrpNoteMsg GrpMsg)
{
    tIgmpHost           HostPkt;
    tDvmrpQMsg         *pQMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufChain = NULL;

    LOG1 ((INFO, IHM, "DVMRP Host Packet"));
    HostPkt.u4IfIndex = GrpMsg.u4IfIndex;
    HostPkt.u4DestGroup = GrpMsg.u4GrpAddr;
    HostPkt.u1Flag = GrpMsg.u1Flag;

    if ((pBufChain = (tCRU_BUF_CHAIN_HEADER *)
         CRU_BUF_ALLOCATE_CHAIN (HOST_PACKET_SIZE, 0)) == NULL)
    {
        LOG1 ((CRITICAL, IHM, "Unable to allocate CRU Buffer"));
        return;
    }

    if (CRU_BUF_COPY_OVER_CHAIN (pBufChain, (UINT1 *) &HostPkt, 0,
                                 HOST_PACKET_SIZE) == DVMRP_NOTOK)
    {
        LOG1 ((CRITICAL, IHM, "Unable to copy over Chain"));
        CRU_BUF_RELEASE_CHAIN (pBufChain);
        pBufChain = NULL;
        return;
    }
    if (DVMRP_MEM_ALLOCATE (DVMRP_Q_PID, pQMsg, tDvmrpQMsg) == NULL)
    {
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"
               " for Timer"));
        CRU_BUF_RELEASE_CHAIN (pBufChain);
        return;
    }
    pQMsg->u4MsgType = IGMP_HOST_DVMRP_EVENT;
    pQMsg->DvmrpQMsgParam.DvmrpIfParam = pBufChain;

    if (OsixQueSend (gDvIqId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LOG1 ((CRITICAL, OM, "Unable to Enqueue the message"));
        CRU_BUF_RELEASE_CHAIN (pBufChain);
        pBufChain = NULL;
        DvmrpMemRelease (gDvmrpMemPool.QPoolId, (UINT1 *) (pQMsg));
        return;
    }

    OsixEvtSend (gu4DvmrpTaskId, DVMRP_INPUT_Q_EVENT);
    LOG1 ((INFO, IHM, "Packet enqueued successfully"));
    return;
}
