/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dprth.c,v 1.9 2014/04/04 10:04:43 siva Exp $
 *
 * Description:This module constructs the DVMRP routing table    
 *             based on the route reports sent by its neighbors. 
 *             It then appropriately makes updates to the        
 *             tForwTbl tNbrTbl and tNextHopTbl. It also handles 
 *             the Shutdown Event and Route Report Timer Event. 
 *
 *******************************************************************/
#include "dpinc.h"

/* 
 *  Private functions.  
 */

PRIVATE void DvmrpRthUpdateRt ARG_LIST ((UINT4 u4SrcNetwork, UINT4 u4SrcMask,
                                         UINT1 u1Metric, tNbrTbl * pNbrTbl,
                                         UINT4 u4IfIndex));
PRIVATE void        DvmrpRthMoveToHoldown
ARG_LIST ((tRouteTbl * pRouteEntryToHoldown));
PRIVATE void DvmrpRthDelRouteEntry ARG_LIST ((tRouteTbl * pRouteEntryToDel));

PRIVATE tRouteTbl  *DvmrpRthAddNewEntry
ARG_LIST ((UINT4 u4SrcNetwork, UINT4 u4SrcMask, UINT4 u1Metric,
           UINT4 u4UpstreamNbr, UINT4 u4IfIndex, UINT1 u1Status));
PRIVATE void        DvmrpRthSkipInvalidRoutes
ARG_LIST ((UINT4 *pDataLen, UINT1 u1NumBytes, UINT1 *pDvmrpData,
           tNbrTbl * pNbrTbl));
PRIVATE void        DvmrpRthUpdateDesigForw
ARG_LIST ((tNextHopTbl * pNextHopTbl, UINT4 u4IfIndex, UINT1 u1Flag));

/******************************************************************************/
/* Function Name: DvmrpRthUpdateRt                                            */
/* Description     : This function processes the route report for single origin  */
/*                and updates/creates a new entry in route tbl if necessary.  */
/* Input(s)        : u4SrcNetwork : src net of multicast.                        */
/*                u4SrcMask    : src net mask of multicast.                   */
/*                u1Metric     : cost to reach src net.                       */
/*                pNbrTbl      : received this report from the nbr.           */
/*                u4IfIndex    : interface index.                             */
/* Output(s)    : None.                                                        */
/* Returns       : None.                                                        */
/******************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpRthUpdateRt (UINT4 u4SrcNetwork, UINT4 u4SrcMask,
                  UINT1 u1Metric, tNbrTbl * pNbrTbl, UINT4 u4IfIndex)
#else

PRIVATE void
DvmrpRthUpdateRt (u4SrcNetwork, u4SrcMask, u1Metric, pNbrTbl, u4IfIndex)
     UINT4               u4SrcNetwork;
     UINT4               u4SrcMask;
     UINT1               u1Metric;
     tNbrTbl            *pNbrTbl;
     UINT4               u4IfIndex;
#endif
{
    UINT1               u1Msg = 0;
    UINT1               u1NbrFound;
    UINT1               u1Key;
    UINT1               u1Flag;
    UINT1               u1NewMetric;
    UINT1               u1AllLeaf;
    UINT4               u4NbrAddr = pNbrTbl->u4NbrIpAddress;
    UINT4               u4Entry;
    UINT4               u4Index;
    tRouteTbl          *pRouteTbl;
    tNextHopTbl        *pNextHopTbl;
    tNextHopType       *pNextHopType;
    tNextHopType       *pTmpIface;
    tNbrAddrList       *pNbrAddrList = NULL;
    tNbrAddrList       *pTmpNbr;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);

    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, PM, "Invalid Interface Node"));
        return;
    }
    if (DvmrpUtilValidSubnet (u4SrcNetwork, u4SrcMask) == DVMRP_NOTOK)
    {
        pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
        LOG2 ((WARNING, RTH, "Bad route because of Invalid Subnet : ",
               pNbrTbl->u4NbrRcvBadRoutes));
        pIfNode->u4IfRcvBadRoutes++;
        LOG2 ((WARNING, RTH,
               "Number of bad routes received on this iface: ",
               pIfNode->u4IfRcvBadRoutes));
        return;
    }
    else
    {
        pNbrTbl->u4NbrRcvRoutes++;    /* for SNMP */
        LOG2 ((INFO, RTH, "Number of Valid routes received by this Nbr. : ",
               pNbrTbl->u4NbrRcvRoutes));
    }

    if (u1Metric < DVMRP_INFINITY_METRIC)
    {
        u1NewMetric = (UINT1) (u1Metric + pIfNode->u1IfMetric);
        if (u1NewMetric >= DVMRP_INFINITY_METRIC)
        {
            u1NewMetric = DVMRP_INFINITY_METRIC;
        }
    }
    else
    {
        u1NewMetric = u1Metric;
    }

    /*
     *  3.4.6 (A) If the route is new and the metric is less than infinity,
     *  the route should be added.
     *  Now we need to lookup the reported origin, in our routing table
     *  So, calc # index to get a ptr to route table in # table, we pass
     *  origin_prefix, mask as the key to get this.
     *  If NULL is returned then this is a new entry to be added to our
     *  routing table.
     *  So, get the free index ptr and add the entry there and leave the
     *  free index pointing to next free index in this new entry.
     */

    pRouteTbl = (tRouteTbl *) (VOID *) DvmrpUtilGetCacheEntry (u4SrcNetwork,
                                                               u4SrcMask,
                                                               DVMRP_ROUTE_HASH);
    if (pRouteTbl == NULL)
    {                            /* New Route */
        if (u1NewMetric < DVMRP_INFINITY_METRIC)
        {

            /*  Well We can create a new entry now. The function AddNewEntry
             *  returns a ptr to the added entry into the route tbl, and this
             *  has to be inserted into the # bucket, pointed by HashIndex. 
             */

            pRouteTbl =
                DvmrpRthAddNewEntry (u4SrcNetwork, u4SrcMask, u1NewMetric,
                                     u4NbrAddr, u4IfIndex, DVMRP_ACTIVE);
            if (pRouteTbl != NULL)
            {
                DVMRP_GET_HASH_INDEX (u4SrcNetwork, u4SrcMask, u1Key);
                DVMRP_HASH_ADD_NODE (gpRouteHashTable,
                                     &pRouteTbl->RouteEntryNext, u1Key, NULL);
            }
            else
            {
                LOG1 ((CRITICAL, RTH, "The route table is full cant Add."));
                return;            /* Dont remove.. */
            }
        }
        else
        {

            /*
             *  Discard the route as the route is not of any Use for Me.
             */

            LOG1 ((INFO, RTH, "The route was new, But the metric was Bad."));
        }
        return;                    /* Dont remove */
    }

    /*
     * Placed the following in a common place 
     */
    u1Flag = DvmrpRthSearchNextHopTbl (pRouteTbl->u4SrcNetwork,
                                       pRouteTbl->u4SrcMask, &u4Entry);

    /* 
     *  If we are here => we got a hit in # table. So we need to just update
     *  our routing table, so get the ptr to route tbl stored in # bucket. 
     *  Now we are at the right place to update our route entry.
     */

    if (u1NewMetric < DVMRP_INFINITY_METRIC)
    {
        u1Msg = NEWMETRIC_LESSTHAN_INF;
    }
    else if (u1NewMetric == DVMRP_INFINITY_METRIC)
    {
        u1Msg = NEWMETRIC_EQL_INF;
    }
    else if ((u1NewMetric > DVMRP_INFINITY_METRIC) &&
             (u1NewMetric < 2 * DVMRP_INFINITY_METRIC))
    {
        u1Msg = NEWMETRIC_GR_INF_LESSTHAN_TWICE_INF;
    }
    else if ((u1NewMetric < 1) || (u1NewMetric >= 2 * DVMRP_INFINITY_METRIC))
    {
        pNbrTbl->u4NbrRcvBadRoutes++;
        LOG1 ((WARNING, RTH, "Bad Metric Advertised by Nbr."));
        LOG2 ((WARNING, RTH, "Number of Bad Routes advertised by Nbr.",
               pNbrTbl->u4NbrRcvBadRoutes));
        pIfNode->u4IfRcvBadRoutes++;
        LOG2 ((WARNING, RTH,
               "Number of bad routes received on this iface: ",
               pIfNode->u4IfRcvBadRoutes));
    }

    switch (u1Msg)
    {
        case NEWMETRIC_LESSTHAN_INF:

            /* 
             *  If the nbr is downstream dependent nbr (check for a match in 
             *  tNextHopTbl), the nbr is now learning the route from another
             *  source. Cancel the downstream dependancey. 
             *  pRouteTbl is right now pointing to an existing entry. Take
             *  that entries  src_prefix and mask (opt : and upstream iface) and
             *  call the func  which gets the ptr to the entry hit.
             */

            if (u1Flag == DVMRP_TRUE)
            {
                if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) ==
                    DVMRP_NOTOK)
                {
                    pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);

                    /* 
                     * now pNextHopTbl is pointing to the entry hit change 
                     * it toleaf.  */

                    u1NbrFound = DVMRP_FALSE;
                    DVMRP_ROUTE_DELETE_NEIGHBOR (pNextHopTbl, u4IfIndex,
                                                 u4NbrAddr, pTmpNbr,
                                                 pNbrAddrList, u1NbrFound,
                                                 u1AllLeaf);

                    /* If all the neighbors have been removed then there is no
                     *  point in retaining this next hop table entry.
                     *  Call Datagram handler to remove from the forward table 
                     *  also.
                     */

                    if (u1NbrFound == DVMRP_TRUE)
                    {
                        DvmrpPmSendPruneOnNbrChange (u4IfIndex,
                                                     pNextHopTbl->u4NextHopSrc,
                                                     pNbrTbl->u4NbrIpAddress);
                        LOG2 ((INFO, RTH,
                               "Down stream dependancy is canceled for Nbr.",
                               u4NbrAddr));
                    }
                }
            }
            else
            {
                LOG1 ((INFO, RTH,
                       "There is no entry for this Source in the NextHop.."));
            }

            /*
             * Update the Route Table with the Incoming Values for the Route 
             */

            if (u1NewMetric > pRouteTbl->u1Metric)
            {
                u1Msg = NEW_METRIC_GR_EXISTING;
            }
            else if (u1NewMetric < pRouteTbl->u1Metric)
            {
                u1Msg = NEW_METRIC_LESS_EXISTING;
            }
            else
            {                    /* Route received from Upstream and probable not from a
                                 * multi access network..!! */
                u1Msg = NEW_METRIC_EQL_EXISTING;
            }

            switch (u1Msg)
            {
                case NEW_METRIC_GR_EXISTING:
                    if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) ==
                        DVMRP_OK)
                    {
                        if (u4NbrAddr == pRouteTbl->u4UpstreamNbr)
                        {
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {
                                pRouteTbl->u1Metric = u1NewMetric;
                                pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                                LOG1 ((INFO, RTH,
                                       "The status was HOLDOWN made "
                                       "DVMRP_ROUTE_RELEARNT ..!!"));
                            }
                            else
                            {    /* Not in Holdown */
                                /* SRG - Metric assignment shd happen always */
                                pRouteTbl->u1Metric = u1NewMetric;
                                DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                                  pNbrTbl, u1NewMetric,
                                                  DVMRP_FLASH);
                                LOG1 ((INFO, RTH,
                                       "Flash Update Sent for DownStream Nbrs"));

                                if (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT)
                                {
                                    LOG1 ((INFO, RTH,
                                           "The route status is relearnt,"
                                           "Expiry Time not updated."));
                                }
                                else
                                {
                                    pRouteTbl->i4ExpiryTime =
                                        DVMRP_BEGIN_HOLDOWN_AFTER;
                                    LOG1 ((INFO, RTH,
                                           "The route relearnt from SAME gw "
                                           "with different metric..!!"));
                                }
                            }
                        }
                        else
                        {        /* Not an upstream Nbr. */
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {
                                pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                                pRouteTbl->u1Metric = u1NewMetric;
                                pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                LOG1 ((INFO, RTH,
                                       "The route was in Holdown and made"
                                       "DVMRP_ROUTE_RELEARNT..!!"));
                            }
                            else
                            {    /*route is either in Active or Relearnt State */
                                LOG1 ((INFO, RTH,
                                       "The route is discarded as it is of"
                                       "no use..!!"));
                            }
                        }        /* else */
                    }
                    else
                    {            /* Downstream iface */
                        if (u1Metric == pRouteTbl->u1Metric)
                        {
                            if (u4NbrAddr > pIfNode->u4IfLocalAddr)
                            {
                                if (u1Flag == DVMRP_TRUE)
                                {
                                    pNextHopTbl =
                                        DVMRP_GET_NEXTHOPNODE_FROM_INDEX
                                        (u4Entry);
                                    DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                             u4IfIndex,
                                                             DVMRP_TRUE);
                                }
                                else
                                {
                                    DvmrpGmSendGraftOnIfaceChange (u4IfIndex,
                                                                   pRouteTbl->
                                                                   u4SrcNetwork,
                                                                   NULL);
                                }
                            }
                            else
                            {
                                if (u1Flag == DVMRP_TRUE)
                                {
                                    pNextHopTbl =
                                        DVMRP_GET_NEXTHOPNODE_FROM_INDEX
                                        (u4Entry);
                                    DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                             u4IfIndex,
                                                             DVMRP_FALSE);
                                }
                                else
                                {
                                    DvmrpPmSendPruneOnIfaceChange (u4IfIndex,
                                                                   pRouteTbl->
                                                                   u4SrcNetwork);
                                }
                            }
                        }
                        if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                        {
                            pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                            pRouteTbl->u1Metric = u1NewMetric;
                            pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                            pRouteTbl->u4IfaceIndex = u4IfIndex;

                            LOG1 ((INFO, RTH,
                                   "The route relearnt from one of our"
                                   "downstream ifaces "));

                            LOG1 ((INFO, RTH,
                                   "is now placed in DVMRP_ROUTE_RELEARNT"
                                   "state.."));
                        }
                        else
                        {        /* status may be, Active or Relearnt */
                            LOG1 ((INFO, RTH, "The route is of No use."));
                        }
                    }            /* Downstream interface */
                    break;

                    /*
                     *  check [3.4.6] B. 1. a. done
                     */

                case NEW_METRIC_LESS_EXISTING:
                    if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) ==
                        DVMRP_OK)
                    {
                        if (pRouteTbl->u4UpstreamNbr == u4NbrAddr)
                        {
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {
                                pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                                pRouteTbl->u1Metric = u1NewMetric;
                                LOG1 ((INFO, RTH, "The route was in HOLDOWN now"
                                       "made DVMRP_ROUTE_RELEARNT."));
                            }
                            else
                            {    /* Not in Holdown */
                                pRouteTbl->u1Metric = u1NewMetric;
                                if (pRouteTbl->u1Status != DVMRP_ROUTE_RELEARNT)
                                {
                                    pRouteTbl->i4ExpiryTime =
                                        DVMRP_BEGIN_HOLDOWN_AFTER;
                                    LOG1 ((INFO, RTH,
                                           "Same Nbr. is reporting this"
                                           "route on same iface, with"
                                           "lesser metric"));
                                    return;    /* Dont remove */
                                }
                                LOG1 ((INFO, RTH,
                                       "Same Nbr is reporting this route on"
                                       "same iface, with lesser metric"));

                                LOG1 ((INFO, RTH,
                                       "While route was in Relearnt Status"));
                            }
                        }
                        else
                        {        /* Different Upsteream Nbr. */
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {

                                /*
                                 *  v3.7 One of the upstreams subordinate is helping
                                 *  us out.
                                 */

                                pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                                pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                pRouteTbl->u1Metric = u1NewMetric;
                                LOG1 ((INFO, RTH,
                                       "The route was in HOLDOWN now made"
                                       "DVMRP_ROUTE_RELEARNT."));
                            }
                            else
                            {
                                pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                pRouteTbl->u1Metric = u1NewMetric;
                                DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                                  pNbrTbl, u1NewMetric,
                                                  DVMRP_FLASH);
                                LOG1 ((INFO, RTH,
                                       "Flash Update Sent for DownStream Nbrs"));
                                /* Not in Holdown */
                                if (pRouteTbl->u1Status != DVMRP_ROUTE_RELEARNT)
                                {
                                    pRouteTbl->i4ExpiryTime =
                                        DVMRP_BEGIN_HOLDOWN_AFTER;
                                    LOG1 ((INFO, RTH,
                                           "Diff Nbr is reporting this route"
                                           "on same iface with better metric"));
                                    return;
                                }

                                LOG1 ((INFO, RTH,
                                       "Diff Nbr is reporting this route on"
                                       "same iface with a better metric"));

                                LOG1 ((INFO, RTH,
                                       "while route was in Relearnt Status."));

                                /*
                                 * The forwarding cache table has to be
                                 * updated with this new nbr.
                                 */

                                DVMRP_UPDATE_UPSTREAM_NBR (pRouteTbl);
                            }
                        }
                    }            /* Upstream Interface */
                    else
                    {            /* Downstream Iface */
                        if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                        {

                            /*
                             *  Not in Draft. (v3.7)
                             */

                            pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                            pRouteTbl->u4IfaceIndex = u4IfIndex;
                            pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                            pRouteTbl->u1Metric = u1NewMetric;

                            LOG1 ((INFO, RTH,
                                   "The route was in HOLDOWN now made"
                                   "DVMRP_ROUTE_RELEARNT."));
                        }
                        else
                        {        /* Not in Holdown */

                            /*
                             * If our previous upstream iface for this route 
                             * changes to a new iface, then the new iface 
                             * which is now our upstream iface for this route
                             * which was previously our downstream iface, for
                             * which we might have set the designated 
                             * forwarder flag will now has to be reset.
                             *  if we dont find a next hop table entry we 
                             *  should still be updating the next hop table 
                             *  entry.
                             */

                            if (u1Flag == DVMRP_TRUE)
                            {
                                pNextHopTbl =
                                    DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);

                                DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfIndex,
                                                         DVMRP_FALSE);

                                /*
                                 *  and the previous upstream iface which now 
                                 *  becomes our downstream iface weill be any 
                                 *  way LEAF for this particular src, all we 
                                 *  have to do is set the designated forwarder
                                 *  flag for this.
                                 */

                                DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                         pRouteTbl->
                                                         u4IfaceIndex,
                                                         DVMRP_TRUE);
                            }
                            else
                            {
                                LOG1 ((INFO, RTH,
                                       "There is no entry for this Source"
                                       "in the NextHop.."));

                                /* 
                                 *  Forward table may have to be updated by
                                 *  adding this interface, if it doesent i
                                 *  exist.
                                 */

                                DvmrpGmSendGraftOnIfaceChange (u4IfIndex,
                                                               pRouteTbl->
                                                               u4SrcNetwork,
                                                               NULL);
                            }

                            /* 
                             *  If the upstream interface changed.
                             *  A flash poison update should be sent upstream
                             *  indicating a change in the downstream dependancy.
                             *  (All the above will happen in our Next route report
                             *  expiry, so just forget it for now).
                             */

                            pRouteTbl->u4IfaceIndex = u4IfIndex;
                            pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                            pRouteTbl->u1Metric = u1NewMetric;
                            if (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT)
                            {
                                LOG1 ((INFO, RTH,
                                       "Diff Nbr. is reporting this"
                                       "route now on diff iface"));

                                LOG1 ((INFO, RTH,
                                       "while the route was"
                                       "in Relearnt Status."));
                            }
                            else
                            {
                                pRouteTbl->i4ExpiryTime =
                                    DVMRP_BEGIN_HOLDOWN_AFTER;
                                LOG1 ((INFO, RTH,
                                       "Diff Nbr. is reporting this"
                                       "route now on different iface."));
                            }

                            /* 
                             * The forwarding cache table has to be
                             * updated with this new nbr.
                             */

                            DVMRP_UPDATE_UPSTREAM_NBR (pRouteTbl);
                            DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                              pNbrTbl, u1NewMetric,
                                              DVMRP_FLASH);
                            LOG1 ((INFO, RTH,
                                   "Flash Update Sent for DownStream Nbrs"));

                            DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                              pNbrTbl, u1NewMetric,
                                              DVMRP_POISSON_FLASH);
                            LOG1 ((INFO, RTH,
                                   "Poisson Flash update sent for upstream"
                                   "interface indicating the change in"
                                   "downstream dependency"));
                        }
                    }            /* Downstream interface */
                    break;

                    /*
                     *  check [3.4.6] B. 1. b. done
                     */

                case NEW_METRIC_EQL_EXISTING:
                    if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) ==
                        DVMRP_OK)
                    {
                        if (u4NbrAddr == pRouteTbl->u4UpstreamNbr)
                        {
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {

                                /* 
                                 *  Prematuarly take the route, out of holdown &
                                 *  begin advertising it with a non-infinity
                                 *  metric. A flash update may need to be sent 
                                 *  to all the ifaces, except the one over which
                                 *  it was received. (v3.7)
                                 */

                                pRouteTbl->u1Status = DVMRP_ACTIVE;
                                ++gu4NumReachableRoutes;
                                LOG1 ((INFO, RTH, "The route was in HOLDOWN now"
                                       "made ACTIVE....."));
                            }
                            else
                            {    /* Not in HOLDOWN */

                                /* 
                                 *  simply refresh the route 
                                 */

                                if (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT)
                                {
                                    LOG1 ((INFO, RTH,
                                           "The Route is in Relearnt Status"));
                                }
                                else
                                {    /* Active */
                                    pRouteTbl->i4ExpiryTime =
                                        DVMRP_BEGIN_HOLDOWN_AFTER;
                                    LOG1 ((INFO, RTH,
                                           "Just Refreshed the Route "));
                                }
                            }
                        }
                        else
                        {        /* Different Upstream Nbr. */
                            if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                            {
                                pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                                pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                LOG1 ((INFO, RTH, "The route was in HOLDOWN now"
                                       "made DVMRP_ROUTE_RELEARNT."));
                            }
                            else
                            {    /* Not in Holdown */
                                /* Performance issue...
                                 * Even if the route state is in route relearnt
                                 * state update the upstream if he is a better
                                 * guy..
                                 */
                                if (u4NbrAddr < pRouteTbl->u4UpstreamNbr)
                                {
                                    /*
                                     *  upstream toggles. 
                                     */
                                    pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                    /* The forwarding cache table
                                     * has to be updated with this new nbr.
                                     */
                                    DVMRP_UPDATE_UPSTREAM_NBR (pRouteTbl);
                                    DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                                      pNbrTbl, u1NewMetric,
                                                      DVMRP_POISSON_FLASH);
                                    LOG1 ((INFO, RTH,
                                           "Poisson Flash Update Sent for"
                                           "UpStream Nbrs"));

                                    if (pRouteTbl->u1Status !=
                                        DVMRP_ROUTE_RELEARNT)
                                    {
                                        pRouteTbl->i4ExpiryTime =
                                            DVMRP_BEGIN_HOLDOWN_AFTER;
                                        LOG1 ((INFO, RTH,
                                               "Updated the Togglling"
                                               "Upstream Neighbor"));
                                        return;    /* Dont remove */
                                    }
                                    LOG1 ((INFO, RTH,
                                           "Updated the Togglling Upstream"
                                           "Neighbor, While the route was"
                                           "in Relearnt Status."));
                                }
                                else
                                {    /*Nbr IP Addr is greater than existing */
                                    LOG1 ((INFO, RTH,
                                           "The Nbr addr is greater than"
                                           "what is Route Table "));

                                    LOG1 ((INFO, RTH,
                                           "The route is a directly attached"
                                           "Subnet,So Route is of No use."));
                                }
                            }    /* Not in Holdown */
                        }        /* Different Upstream Nbr. */
                    }
                    else
                    {            /* down stream iface */
                        if (pRouteTbl->u1Status == DVMRP_HOLDOWN)
                        {
                            pRouteTbl->u1Status = DVMRP_ROUTE_RELEARNT;
                            pRouteTbl->u4IfaceIndex = u4IfIndex;
                            pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                            LOG1 ((INFO, RTH,
                                   "The route was in HOLDOWN now made"
                                   "DVMRP_ROUTE_RELEARNT."));
                        }
                        else
                        {        /* Status may be Active OR in Relearnt */
                            /* u4NbrAddr should have been
                             * checked with the CURRENT route table pointer
                             * instead of gpRouteTable
                             */

                            if ((pRouteTbl->u1Status != DVMRP_NEVER_EXPIRE) &&
                                (u4NbrAddr < pRouteTbl->u4UpstreamNbr))
                            {

                                /*
                                 *  The the Nbrs IP addr is Better than the 
                                 *  route table Upstream Ip addr => the 
                                 *  interface will now be changed and the route
                                 *  table has to be updated with this Nbr as 
                                 *  Upstream and Iface.
                                 *  And downstream becomes upstream and
                                 *  upstream becomes my downstream.(this will
                                 *  apply fo route in Relearnt status also).
                                 */

                                if (u1Flag == DVMRP_FALSE)
                                {

                                    /* 
                                     *  Forward table may have to be updated by
                                     *  adding this interface, if it doesent
                                     *  exist.
                                     */

                                    DvmrpPmSendPruneOnIfaceChange (u4IfIndex,
                                                                   pRouteTbl->
                                                                   u4SrcNetwork);
                                    LOG1 ((INFO, RTH,
                                           "There is no entry for this"
                                           "Source in the NextHop.."));
                                    /*   return;  */
                                }
                                else
                                {
                                    pNextHopTbl = gpNextHopTable[u4Entry];
                                    DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                             u4IfIndex,
                                                             DVMRP_FALSE);
                                    DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                             pRouteTbl->
                                                             u4IfaceIndex,
                                                             DVMRP_TRUE);
                                }

                                /* 
                                 *  If the upstream interface changed.
                                 *  A flash poison update should be sent
                                 *  upstream indicating a change in the
                                 *  downstream dependancy.
                                 *  (All the above will happen in our Next 
                                 *  route report expiry, so just forget it for
                                 *  now).
                                 */

                                pRouteTbl->u4IfaceIndex = u4IfIndex;
                                pRouteTbl->u4UpstreamNbr = u4NbrAddr;
                                if (pRouteTbl->u1Status != DVMRP_ROUTE_RELEARNT)
                                {
                                    pRouteTbl->i4ExpiryTime =
                                        DVMRP_BEGIN_HOLDOWN_AFTER;
                                    LOG1 ((INFO, RTH,
                                           "Different Nbr. whose IP addr is"
                                           "less than than Upstream Nbr"));

                                    LOG1 ((INFO, RTH,
                                           "is reporting this route now on"
                                           "downstream iface"));
                                }
                                else
                                {
                                    LOG1 ((INFO, RTH,
                                           "Diff Nbr whose IP addr is less"
                                           "than than Route table Upstream"
                                           "Nbr "));
                                    LOG1 ((INFO, RTH,
                                           "is reporting this routenow on"
                                           "downstream iface, "));
                                    LOG1 ((INFO, RTH,
                                           "while the route is in Route"
                                           "Relearnt State."));
                                }
                                /* The forwarding cache table
                                 * has to be updated with this new nbr.
                                 */
                                DVMRP_UPDATE_UPSTREAM_NBR (pRouteTbl);
                            }
                            else
                            {
                                /* Nbr IP addr is greater than route table
                                 * Upstrem Nbr IP addr.
                                 */
                                if (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT)
                                {
                                    LOG1 ((INFO, RTH, "The Route is in Relearnt"
                                           "status, So returning."));
                                    return;    /* Dont remove */
                                }
                                else
                                {    /* Not Relearnt */
                                    if (u4NbrAddr < pIfNode->u4IfLocalAddr)
                                    {
                                        if (u1Flag == DVMRP_FALSE)
                                        {
                                            /* 
                                             *  Forward table may have to be
                                             *  updated by adding this interface,
                                             *  if it doesent exist.
                                             */
                                            DvmrpPmSendPruneOnIfaceChange
                                                (u4IfIndex,
                                                 pRouteTbl->u4SrcNetwork);
                                            LOG1 ((INFO, RTH,
                                                   "There is no entry for this"
                                                   "Source in the NextHop.."));
                                            return;    /* Dont remove */
                                        }
                                        pNextHopTbl =
                                            DVMRP_GET_NEXTHOPNODE_FROM_INDEX
                                            (u4Entry);
                                        DvmrpRthUpdateDesigForw (pNextHopTbl,
                                                                 u4IfIndex,
                                                                 DVMRP_FALSE);
                                        LOG1 ((INFO, RTH,
                                               "Designated forwarder flag RESET"
                                               "and called a function in"
                                               "forwarding Module."));
                                    }
                                    else
                                    {    /* My IP addr is less than Nbrs. */
                                        if (u1Flag == DVMRP_TRUE)
                                        {
                                            pNextHopTbl =
                                                DVMRP_GET_NEXTHOPNODE_FROM_INDEX
                                                (u4Entry);

                                            /* 
                                             *  DF flag has to be updated.
                                             */

                                            if (u1Metric < pRouteTbl->u1Metric)
                                            {
                                                DvmrpRthUpdateDesigForw
                                                    (pNextHopTbl, u4IfIndex,
                                                     DVMRP_FALSE);
                                            }
                                            else
                                            {
                                                DvmrpRthUpdateDesigForw
                                                    (pNextHopTbl, u4IfIndex,
                                                     DVMRP_TRUE);
                                            }
                                        }
                                        else
                                        {

                                            /* 
                                             *  Forward table may have to be
                                             *  updated by adding this 
                                             *  interface,
                                             *  if it doesent exist.
                                             */

                                            DvmrpGmSendGraftOnIfaceChange
                                                (u4IfIndex,
                                                 pRouteTbl->u4SrcNetwork, NULL);
                                            LOG1 ((INFO, RTH,
                                                   "There is no entry for this"
                                                   "Source in the NextHop.."));
                                        }
                                    }    /* My Ip addr is lessthan Nbrs */
                                }    /* Not Relearnt */
                            }    /* Nbr Addr is greater than route table Upstream Nbr */
                        }        /* Not holddown */
                    }            /* Downstream Iface */
                    break;
            }                    /* 2nd switch */

            /*
             *  check [3.4.6] B. 1. c. done
             */

            break;

        case NEWMETRIC_EQL_INF:

            /*
             *  a. Next hop = existing next hop
             */

            if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) == DVMRP_OK)
            {
                /* 
                 * If the route is allready in Holdown, leave it as it is, But
                 * if it were to be in route relearnt and the new guy whoom we
                 * relearnt this route is itself starts advertising the bad
                 * metric, because of some delay in propagation of routes, then
                 * we should be making the status back into Holdown.
                 * And if the route advertised is our own directly attached
                 * subnet then dont make it holdown as we are the Upstream 
                 * for this.
                 */
                DvmrpFlashUpdate (u4SrcNetwork, u4SrcMask,
                                  pNbrTbl, u1NewMetric, DVMRP_FLASH);
                LOG1 ((INFO, RTH, "Flash Update Sent for DownStream Nbrs"));

                if (pRouteTbl->u1Status == DVMRP_ACTIVE)
                {
                    DvmrpRthMoveToHoldown (pRouteTbl);
                }
            }
            else
            {

                /*
                 *  b. Next hop != existing next hop
                 */

                if (u1Flag == DVMRP_FALSE)
                {
                    /* 
                     *  Forward table may have to be updated by adding this
                     *  interface, if it doesent exist, So that if it were to be
                     *  the Nbr who went down we should be forwarding for this
                     *  subnet now.
                     */
                    DvmrpGmSendGraftOnIfaceChange (u4IfIndex,
                                                   pRouteTbl->u4SrcNetwork,
                                                   NULL);
                    LOG1 ((INFO, RTH,
                           "There is no entry for this Source in"
                           "the NextHop.."));
                    return;
                }
                pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);

                /* 
                 *  If the nbr ..was.. considered to be the designated
                 *  forwarder.. the receiving router(i.e., we) should now
                 *  become the designated forwarder for the src n/w on this
                 *  iface....
                 */

                DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfIndex, DVMRP_TRUE);

                /* 
                 *  pNextHopTbl is still pointing to the entry hit make it LEAF.
                 */

                u1NbrFound = DVMRP_FALSE;

                DVMRP_ROUTE_DELETE_NEIGHBOR (pNextHopTbl, u4IfIndex, u4NbrAddr,
                                             pTmpNbr, pNbrAddrList, u1NbrFound,
                                             u1AllLeaf);

                /*
                 *  Call Datagram handler to remove from the forward table also
                 */

                if (u1NbrFound == DVMRP_TRUE)
                    DvmrpPmSendPruneOnNbrChange (u4IfIndex,
                                                 pNextHopTbl->u4NextHopSrc,
                                                 pNbrTbl->u4NbrIpAddress);
            }

            /*
             *  check [3.4.6] B. 2. done
             */

            break;

        case NEWMETRIC_GR_INF_LESSTHAN_TWICE_INF:
            /* 
             *  If the route is currently in Holdown state, then there is no
             *  point in registering this nbr as downstream dependent, as there
             *  wont be any next hop table entry will be existing.  So just
             *  return.
             */

            if ((pRouteTbl->u1Status == DVMRP_HOLDOWN) ||
                (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT))
            {
                return;
            }

            pRouteTbl->i4ExpiryTime = DVMRP_DELETE_ROUTE_AFTER;

            /*
             *  3.4.6. B. 3. a. Nbr on down stream interface.
             *  If the sending nbr is considered to be on a downstream iface 
             *  for that route, then the nbr should be made as donstream dep 
             *  nbr for that route. 
             */

            if (DVMRP_COMPARE (pRouteTbl->u4IfaceIndex, u4IfIndex) ==
                DVMRP_NOTOK)
            {

                /* 
                 *  If we are here => nbr is not on my upstream iface...
                 *  => he is on my down stream iface for this particular route.
                 */

                if (u1Flag == DVMRP_TRUE)
                {
                    pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);

                    /* 
                     *  If the nbr was considered to be the designated forwarder
                     *  (i.e., my subordinate router not the downstream router)
                     *  Subordinate Router : is the one, who advertises the 
                     *  same metric as we do, on a particular interface, but
                     *  whose ip addr is better than ours.....
                     *  ...the receiving router(i.e., we) should now become the
                     *  designated forwarder for the src n/w on this iface.
                     */

                    /* 
                     *  pNextHopTbl will now be pointing to a hit entry,
                     *  now search for a match of the iface and make that
                     *  as the BRANCH.
                     */

                    for (pNextHopType = pNextHopTbl->pNextHopType;
                         pNextHopType != NULL; pNextHopType =
                         pNextHopType->pNextHopTypeNext)
                    {
                        if (DVMRP_COMPARE
                            (pNextHopType->u4NHopInterface,
                             u4IfIndex) == DVMRP_OK)
                        {
                            /*  
                             *  Check wether the neighbor is allready existing
                             *  as a downstream dependent neighbor, if so dont
                             *  make a new entry else create a new entry for 
                             *  this neighbor.
                             */
                            pNbrAddrList = pNextHopType->pDependentNbrs;
                            while (pNbrAddrList)
                            {
                                if (pNbrAddrList->u4NbrAddr == u4NbrAddr)
                                {
                                    LOG1 ((INFO, RTH,
                                           "Neighbor Allready Registered as"
                                           "Downstream Dependent Nbr."));
                                    return;
                                }
                                pNbrAddrList = pNbrAddrList->pNbr;
                            }

                            /* 
                             *  Add this Nbr as a Downstream dependent Nbr for
                             *  this particular Src network in Next hop table.
                             */

                            pNbrAddrList = (tNbrAddrList *) DvmrpUtilGetNbr ();
                            if (pNbrAddrList == NULL)
                            {
                                LOG1 ((CRITICAL, RTH,
                                       "No free memory for this Nbr"));
                                return;
                            }

                            /*  
                             *  we should now implicitly become desig forwarder.
                             */

                            DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfIndex,
                                                     DVMRP_TRUE);
                            pNbrAddrList->u4NbrAddr = u4NbrAddr;
                            pNbrAddrList->pNbr = pNextHopType->pDependentNbrs;
                            pNextHopType->pDependentNbrs = pNbrAddrList;
                            pNextHopType->u1InType = DVMRP_BRANCH;
                            DvmrpGmSendGraftOnNbrChange (u4IfIndex,
                                                         u4SrcNetwork,
                                                         u4NbrAddr);
                            break;
                        }
                    }            /* for */
                }
                else
                {

                    /*
                     *  As per our design we shall add the Nbr to the next hop
                     *  only if a dependancy is expressed, thats why we dont
                     *  add an entry here when we add an new entry in route
                     *  table.....
                     *  We dont have a hit in next hop => this is the first
                     *  time the Nbr has indicated the downstream dependancy
                     *  for this src...... create a new entry.
                     */

                    if (u4Entry == MAX_NEXT_HOP_TABLE_ENTRIES)
                    {
                        LOG1 ((RTH, CRITICAL,
                               "No memory so we may over write the"
                               "last entry, so returning."));
                        return;
                    }
                    /* No need to recalculate the first free nexthop entry */
                    DvmrpRthCreateNodeinNexthopTbl (pRouteTbl, u4IfIndex,
                                                    u4Entry);
                    /*
                     *  Add the Nbr to this Iface as a dependent Nbr.
                     */
                    pTmpIface =
                        (DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry))->
                        pNextHopType;
                    while (pTmpIface)
                    {
                        if (pTmpIface->u4NHopInterface == u4IfIndex)
                        {
                            pTmpIface->pDependentNbrs =
                                (tNbrAddrList *) DvmrpUtilGetNbr ();
                            if (pTmpIface->pDependentNbrs == NULL)
                            {
                                LOG1 ((CRITICAL, RTH,
                                       "No free memory for this Nbr"));
                                return;
                            }
                            /* Nbr address shd be assigned
                             * to the newly alloted neighbor */
                            pTmpIface->pDependentNbrs->u4NbrAddr = u4NbrAddr;
                            break;
                        }
                        pTmpIface = pTmpIface->pNextHopTypeNext;
                    }
                    if (pTmpIface == NULL)
                    {
                        LOG1 ((CRITICAL, RTH, "Inactive interface "));
                        return;
                    }
                    LOG1 ((INFO, RTH,
                           "New NextHop table entry created for a Src"
                           "Net with dependent Nbr."));

                    /* 
                     *  .... if one or more prunes have been sent upstream
                     *  containing this source network, then Graft messages
                     *  will need to be sent upstream in the direction of
                     *  the source network ..for each  group.. with existing
                     *  prune state.
                     */

                    DvmrpGmSendGraftOnNbrChange (u4IfIndex, u4SrcNetwork,
                                                 u4NbrAddr);
                }
            }

            /*
             *  check [3.4.6] 3. a. done
             */

            else
            {
                /*
                 *  If upstream is expressing dependancy on downstream then just
                 *  ignore it.
                 */
                LOG1 ((INFO, RTH,
                       "Upstream is Expressing Dependancy, IGNORED."));
                return;
            }
            /*
             *  check [3.4.6] 3. b. done
             */
            break;

        default:
            LOG1 ((WARNING, RTH, "UNKNOWN ERROR IN ROUTE REPORT"));
            break;
    }                            /* 1st switch */
}

/******************************************************************************/
/* Function Name : DvmrpRthHandleRouteReport                                  */
/* Description      : An Incomming route report from an IP multicast capable     */
/*                 interface. This functionality is split into 2 functions,   */
/*                 one for extractiong the mask, src_prefix and metric from   */
/*                 the reported route, and the other for updating the route   */
/*                 table accordingly.                                         */
/*                 This function processes the incomming Route Report and     */
/*                 updates the tRouteTbl and the tNextHopTbl in DVMRP. This   */
/*                 function will be called by the Input Handler Module, as    */
/*                 and when a Route Report is received from a nbr.            */
/* Input(s)         : u4DataLen  : Length of the linear buffer.                  */
/*                 u4IfIndex  : Index to the interface table.                 */
/*                 pNbrTbl    : Ptr to Nbr Table.                             */
/* Output(s)      : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRthHandleRouteReport (UINT4 u4DataLen, UINT4 u4IfIndex, tNbrTbl * pNbrTbl)
#else

void
DvmrpRthHandleRouteReport (u4DataLen, u4IfIndex, pNbrTbl)
     UINT4               u4DataLen;
     UINT4               u4IfIndex;
     tNbrTbl            *pNbrTbl;
#endif
{
    UINT1               u1NumBytes;
    UINT4               u4SrcNetwork;
    UINT4               u4SrcMask = 0;
    UINT1               u1Metric;
    UINT1              *pDvmrpData;
    UINT4               u4Tmp;
    UINT4               u4TmpSrc;
    UINT1               u1Tmp;
    INT1                i1MetricTestBitSet;
    INT4                i4Index;
    tDvmrpInterfaceNode *pIfNode = NULL;

    /* 
     *  Extract all the fields in the rote report packet 
     */
    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (u4IfIndex);
    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, PM, "Invalid Interface Node"));
        return;
    }
    pDvmrpData = gpRcvBuffer;
    while (u4DataLen > 0)
    {                            /* Index by mask. */
        if (u4DataLen < 3)
        {                        /* Process for mask alone first */
            pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
            LOG1 ((WARNING, RTH, "Datalength is Incorrect"));
            LOG2 ((WARNING, RTH, "Number of Bad Routes received are",
                   pNbrTbl->u4NbrRcvBadRoutes));
            pIfNode->u4IfRcvBadRoutes++;
            LOG2 ((WARNING, RTH,
                   "Number of bad routes received on this iface: ",
                   pIfNode->u4IfRcvBadRoutes));
            return;                /* break point */
        }
        u1NumBytes = 1;
        u4SrcMask = 0xff;
        u4SrcMask = u4SrcMask << 24;
        u4Tmp = *pDvmrpData;
        u4SrcMask |= u4Tmp << 16;
        if (*pDvmrpData++ != 0)
        {
            u1NumBytes = 2;
        }
        u4Tmp = 0;
        u4Tmp = *pDvmrpData;
        u4SrcMask |= u4Tmp << 8;
        if (*pDvmrpData++ != 0)
        {
            u1NumBytes = 3;
        }
        u4SrcMask |= *pDvmrpData;
        if (*pDvmrpData++ != 0)
        {
            u1NumBytes = 4;
        }
        u4DataLen -= SIZEOF_MASK;
        if (DvmrpUtilValidMask (u4SrcMask) == DVMRP_NOTOK)
        {
            LOG1 ((WARNING, RTH, "Bogus netmask reported, more than one routes"
                   "will be effected "));
            LOG1 ((WARNING, RTH, "if we receive a route sorted by mask."));

            /* 
             *  If this is the invalid mask then we have to discard all
             *  orgin_prefix and metrics indexed by this 1/2/3 byte mask and
             *  move the ptr appropriately to the next mask index. 
             */

            DvmrpRthSkipInvalidRoutes (&u4DataLen, u1NumBytes, pDvmrpData,
                                       pNbrTbl);
            continue;
        }
        do
        {
            if (u4DataLen < (UINT1) (u1NumBytes + SIZEOF_METRIC))
            {
                pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
                LOG1 ((WARNING, RTH, "bad route because of improper size."));
                LOG2 ((WARNING, RTH,
                       "Number of Bad routes received by this Nbr.",
                       pNbrTbl->u4NbrRcvBadRoutes));
                pIfNode->u4IfRcvBadRoutes++;
                LOG2 ((WARNING, RTH,
                       "Number of bad routes received on this iface:",
                       pIfNode->u4IfRcvBadRoutes));
                return;
            }

            /*
             *  Capture source prefix from the data buffer. 
             */

            u4SrcNetwork = 0;
            u1Tmp = 24;
            for (i4Index = 0; i4Index < u1NumBytes; ++i4Index, u1Tmp -= 8)
            {
                /* Sendbuffer changed to RcvBuffer */
                u4TmpSrc = *(pDvmrpData++);
                u4SrcNetwork |= (u4TmpSrc << u1Tmp);
                u4TmpSrc = 0;
            }

            /* 
             *  Capture metric from the data buffer. 
             */

            u1Metric = *(pDvmrpData++);
            u4DataLen -= u1NumBytes + SIZEOF_METRIC;

            /* 
             *  check if the last bit is set 
             */

            i1MetricTestBitSet = u1Metric;
            u1Metric = (u1Metric & SEVEN_BITS_SET);
            if (u1Metric == 0)
            {
                pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
                LOG1 ((WARNING, RTH, "Bad Metric Reported By Nbr."));
                LOG2 ((WARNING, RTH,
                       "Number of bad routes Received from this Nbr.",
                       pNbrTbl->u4NbrRcvBadRoutes));
                pIfNode->u4IfRcvBadRoutes++;
                LOG2 ((WARNING, RTH,
                       "Number of bad routes received on this iface:",
                       pIfNode->u4IfRcvBadRoutes));
            }
            else if (u4SrcNetwork == 0)
            {
                /* 
                 *  If we have a default route then the mask is
                 *  set to 0. (ref : 3.4.2) 
                 */
                u4SrcMask = 0;
                LOG1 ((CRITICAL, RTH, "Received a default route. Contact Your"
                       "Sys Admin.  Returning.....!!!!"));
            }
            else
            {
                DvmrpRthUpdateRt (u4SrcNetwork, u4SrcMask, u1Metric, pNbrTbl,
                                  u4IfIndex);
            }
        }
        while (!(i1MetricTestBitSet & EIGHTH_BIT_SET));
    }                            /* WHILE ends here */
}

/******************************************************************************/
/* Function Name : DvmrpRthRouteAgeTimerHandler                               */
/* Description      : On receipt of timer events advance the timer in the route  */
/*                 table.  This function will be called when the Route Age    */
/*                 Timer had expired, by the Input Handler Module.            */
/* Input(s)         : None.                                                      */
/* Output(s)      : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRthRouteAgeTimerHandler (tDvmrpTimer * pTimerBlk)
#else

void
DvmrpRthRouteAgeTimerHandler (pTimerBlk)
     tTimerBlk          *pTimerBlk;
#endif
{
    UINT4               u4Index;
    tRouteTbl          *pRouteTbl;

    UNUSED_PARAM (pTimerBlk);
    for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
    {
        pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);

        /*
         *  for efficiency age route entry after 5 secs
         */

        if ((pRouteTbl->u1Status == DVMRP_INACTIVE) ||
            (pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE))
        {
            continue;
        }
        pRouteTbl->i4ExpiryTime -= DVMRP_ADVANCE_TIMER_VAL;

        /*
         *  Holdown will begin after 140 secs to 260 secs
         */

        if ((pRouteTbl->i4ExpiryTime <= 0) &&
            (pRouteTbl->u1Status == DVMRP_HOLDOWN))
        {
            DvmrpRthDelRouteEntry (pRouteTbl);
        }
        else if ((pRouteTbl->i4ExpiryTime <= 0) &&
                 (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT))
        {
            pRouteTbl->u1Status = DVMRP_ACTIVE;
            pRouteTbl->i4ExpiryTime = DVMRP_BEGIN_HOLDOWN_AFTER;
            ++gu4NumReachableRoutes;
            LOG1 ((INFO, RTH, "Route which was relearnt but was in Holdown"
                   "is now made Actve."));
        }
        else if ((pRouteTbl->i4ExpiryTime <= 0) &&
                 (pRouteTbl->u1Status == DVMRP_ACTIVE))
        {

            /*
             *  Call the below func as lot more things have to done
             */

            DvmrpRthMoveToHoldown (pRouteTbl);
        }
    }
    if ((DvmrpUtilStartTimer (DVMRP_ROUTE_AGEOUT_TIMER, DVMRP_ADVANCE_TIMER_VAL,
                              NULL)) == DVMRP_NOTOK)
    {
        LOG2 ((INFO, RTH, "Route Ageout Timer Cant be Started for",
               DVMRP_ADVANCE_TIMER_VAL));
    }
    else
    {
        LOG2 ((INFO, RTH, "Route Ageout Timer Started for secs",
               DVMRP_ADVANCE_TIMER_VAL));
    }
}

/******************************************************************************/
/* Function Name: DvmrpRthMoveToHoldown                                       */
/* Description     : After route holdown timer expiry this func will be called.  */
/*                [3.2.4.] 1. When a nbr timesout, All routes learned by this */
/*                neighbor should be immediately place in holdown. Forwarding */
/*                cache entries may need to be updated2. All downstream       */
/*                dependencies by this neighbor should be canceled. This may  */
/*                trigger prunes to be sent.                                  */
/* Input(s)        : pRouteTblHoldown : ptr to the route entry to be placed in   */
/*                                   HOLDOWN state.                           */
/* Output(s)     : None.                                                       */
/* Returns        : None.                                                       */
/******************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpRthMoveToHoldown (tRouteTbl * pRouteTblHoldown)
#else

PRIVATE void
DvmrpRthMoveToHoldown (pRouteTblHoldown)
     tRouteTbl          *pRouteTblHoldown;
#endif
{
    UINT1               u1Flag;
    UINT4               u4Entry;
    tNextHopTbl        *pNextHopTbl;
    tNextHopType       *pNextHopType;

    if ((pRouteTblHoldown->u1Status == DVMRP_ACTIVE) ||
        (pRouteTblHoldown->u1Status == DVMRP_NEVER_EXPIRE))
    {
        gu4NumReachableRoutes--;
        LOG2 ((INFO, RTH, "Num Reachable route Now is.",
               gu4NumReachableRoutes));
    }
    /* 
     *  Just place the entry in HOLDOWN the rest will be taken care when
     *  we advertise the routes.
     *  DO NOT MAKE THE METRIC = INFINITY in the routing table...!!
     */

    pRouteTblHoldown->u1Status = DVMRP_HOLDOWN;
    LOG1 ((INFO, RTH, "Route entry placed in HOLDOWN state"));

    /*
     *  Now this route has to be advertised for another 120 secs 
     */
    pRouteTblHoldown->i4ExpiryTime = DVMRP_DELETE_ROUTE_AFTER;

    /*
     *  Entries in Forward Cache has to be deleted.
     */
    /* Last two params are removed */
    DvmrpFmDeleteSrcNetInfo (pRouteTblHoldown->u4SrcNetwork,
                             pRouteTblHoldown->u4SrcMask);

    /*
     *  Delete all entries in tNextHopTbl, with the source Address
     *  field = Source n/w in the route.
     */
    u1Flag = DvmrpRthSearchNextHopTbl (pRouteTblHoldown->u4SrcNetwork,
                                       pRouteTblHoldown->u4SrcMask, &u4Entry);

    if (u1Flag == DVMRP_FALSE)
    {
        LOG1 ((INFO, RTH,
               "There is no entry for this Source in the NextHop.."));
    }
    else
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);
        pNextHopTbl->u1Status = DVMRP_INACTIVE;

        pNextHopType = pNextHopTbl->pNextHopType;
        while (pNextHopType)
        {
            DvmrpUtilReleaseNbr ((tNbrList *) pNextHopType->pDependentNbrs);
            pNextHopType->pDependentNbrs = NULL;
            pNextHopType = pNextHopType->pNextHopTypeNext;
        }
        LOG1 ((INFO, RTH, "All Nbrs on this iface released."));
        DvmrpUtilReleaseIface ((tIfaceList *) pNextHopTbl->pNextHopType);
        LOG1 ((INFO, RTH, "All interfaces released."));
        pNextHopTbl->pNextHopType = NULL;
        LOG1 ((INFO, RTH, "Entry for this Source in NextHop deleted"));
        LOG1 ((INFO, RTH, "interfaces released to free pool.."));
    }
}

/******************************************************************************/
/* Function Name : DvmrpRthDelRouteEntry                                      */
/* Description      : delete an entry in the route table. Here we need not touch */
/*                 tForwTbl as we have updated it when this entry went in     */
/*                 Holdown itself. All we need to do here is make the entry   */
/*                 inactive and update the free ptr.                          */
/* Input(s)         : pRouteTblDel : ptr to the route entry to be deleated.      */
/* Output(s)      : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpRthDelRouteEntry (tRouteTbl * pRouteTblDel)
#else

PRIVATE void
DvmrpRthDelRouteEntry (pRouteTblDel)
     tRouteTbl          *pRouteTblDel;
#endif
{
    UINT1               u1Key;
    pRouteTblDel->u1Status = DVMRP_INACTIVE;

    /* 
     *  release the free entry to the SLL of Route Free Entries Ptr.
     */

    DVMRP_GET_HASH_INDEX (pRouteTblDel->u4SrcNetwork, pRouteTblDel->u4SrcMask,
                          u1Key);
    DVMRP_HASH_DELETE_NODE (gpRouteHashTable,
                            &pRouteTblDel->RouteEntryNext, u1Key);
    pRouteTblDel->RouteEntryNext.pNext =
        (tTMO_SLL_NODE *) (VOID *) gpRouteFreePtr;
    gpRouteFreePtr = pRouteTblDel;
    gu4NumRoutes--;
    LOG2 ((INFO, RTH, "The route is deleted , number of Routes Now is",
           gu4NumRoutes));
    /* 
     *  do not decrement gu4NumReachableRoutes as we have done it in 
     *  Holdown() func itself.
     */

}

/******************************************************************************/
/* Function Name : DvmrpRthHandleShutdownEvent                                */
/* Description      : On receopt of graceful shutdown tell good by to neighbors. */
/*                This function will be called by the Input Handler Module,   */
/*                when shutdown event occurs.                                 */
/* Input(s)        : None.                                                       */
/* Output(s)     : None.                                                       */
/* Returns        : None.                                                       */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRthHandleShutdownEvent (void)
#else

void
DvmrpRthHandleShutdownEvent (void)
#endif
{
    UINT1               u1Flag;
    UINT4               u4Index;
    tRouteTbl          *pRouteTbl;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    INT1                i1First = DVMRP_NOTOK;

    if (DVMRP_ROUTE_TBL_PID != 0)
    {
        for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
        {
            pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);
            if ((pRouteTbl->u1Status == DVMRP_ACTIVE) ||
                (pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE))
            {
                /* 
                 * Inorder to advertise this Route as a 'Bad one' we change 
                 * its status to Holdown
                 */
                pRouteTbl->u1Status = DVMRP_HOLDOWN;
            }
        }
        LOG1 ((WARNING, RTH,
               "All the Active routes are Now Placed in Holdown"));

        LOG1 ((WARNING, RTH, "So as to Advertise the Bad Metric"));

    }                            /*  if( gpRouteTable != NULL ) */
    LOG1 ((WARNING, RTH, "No Active routes to place in Holdown"));

    /*
     *  This Flag is used to Optimize route reports sent to Nbr Addr alone.
     *  So not applicabele here.
     */
    if (DVMRP_IFACE_TBL_PID != 0)
    {
        u1Flag = DVMRP_FALSE;
        pSllNode = TMO_SLL_First (&gDvmrpIfInfo.IfGetNextList);
        i1First = (pSllNode != NULL) ? DVMRP_OK : DVMRP_NOTOK;

        do
        {
            if (i1First == DVMRP_NOTOK)
            {
                break;
            }
            pIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                          IfGetNextLink, pSllNode);
            if (DVMRP_CHECK_IF_STATUS (pIfNode) &&
                (pIfNode->pNbrOnthisIface != NULL))
            {
                DvmrpRrfReportNonStop (pIfNode->u4IfIndex,
                                       (UINT4) ALL_DVMRP_ROUTERS, u1Flag);
            }
            else
            {
                LOG1 ((WARNING, RTH,
                       "Interface is either DOWN or no Nbrs."
                       " So, can't send report"));
            }
        }
        while ((pSllNode = TMO_SLL_Next (&gDvmrpIfInfo.IfGetNextList,
                                         pSllNode)) != NULL);

    }
}

/******************************************************************************/
/* Function Name : DvmrpRthAddNewEntry                                        */
/* Description      : Add a new entry in the routing table. This function returns*/
/*                 a pointer to the entry created in the routing table which  */
/*                 has to go inside the # bucket.                             */
/* Input(s)        : u4SrcNetwork  : src net of multicast.                       */
/*                u4SrcMask     : src net mask of multicast.                  */
/*                u1Metric      : cost to reach src net.                      */
/*                u4UpstreamNbr : Upstream nbr for this route entry.          */
/*                u4IfIndex     : interface index.                            */
/*                u1Status      : Status of the route, (Active/Never Expire). */
/* Output(s)     : gu4NumRoutes  : This global ptr will be incremented.        */
/* Returns        : pRouteTbl     : Ptr to the added entry in the route table.  */
/******************************************************************************/

PRIVATE tRouteTbl  *
#ifdef __STDC__
DvmrpRthAddNewEntry (UINT4 u4SrcNetwork, UINT4 u4SrcMask, UINT4 u1Metric,
                     UINT4 u4UpstreamNbr, UINT4 u4IfIndex, UINT1 u1Status)
#else
DvmrpRthAddNewEntry (u4SrcNetwork, u4SrcMask, u1Metric, u4UpstreamNbr,
                     u4IfIndex, u1Status)
     UINT4               u4SrcNetwork;
     UINT4               u4SrcMask;
     UINT4               u1Metric;
     UINT4               u4UpstreamNbr;
     UINT4               u4IfIndex;
     UINT1               u1Status;
#endif
{
    tRouteTbl          *pRouteTbl;

    /*
     *  Get the Free Index ptr which holds the ptr to an invalid entry in
     *  tRouteTbl.
     */

    if (gpRouteFreePtr == NULL)
    {
        LOG1 ((CRITICAL, RTH, "Route Table Full"));
        return (NULL);
    }
    pRouteTbl = gpRouteFreePtr;

    /*
     *  Then leave the free index ptr pointing to the next free index, so that
     *  we can easily get the free index, next time when we need to add an entry.
     *  An entry in tRouteTbl is either A/I/H so we can have SLL of Active route
     *  entries and SLL of Inactive i.e., Free Indexes withe just one ptr in
     *  tRouteTbl. gpRouteFreePtr will always be pointing to head of free
     *  indexes SLL.
     */

    gpRouteFreePtr =
        (tRouteTbl *) (VOID *) gpRouteFreePtr->RouteEntryNext.pNext;
    pRouteTbl->RouteEntryNext.pNext = NULL;
    pRouteTbl->u4SrcNetwork = u4SrcNetwork;
    pRouteTbl->u4SrcMask = u4SrcMask;
    pRouteTbl->u4UpstreamNbr = u4UpstreamNbr;
    pRouteTbl->u4IfaceIndex = u4IfIndex;
    pRouteTbl->u1Metric = (UINT1) u1Metric;
    pRouteTbl->u4UpTime = DvmrpCurrentTime ();
    pRouteTbl->i4ExpiryTime = DVMRP_DELETE_ROUTE_AFTER;
    pRouteTbl->u1Status = u1Status;
    ++gu4NumRoutes;
    ++gu4NumReachableRoutes;
    LOG1 ((INFO, RTH, "New Route Added in Route table "));
    LOG2 ((INFO, RTH, "Number of Current Known Routes are ", gu4NumRoutes));
    LOG2 ((INFO, RTH, "Number of Current Active Routes are",
           gu4NumReachableRoutes));
    return (pRouteTbl);
}

/******************************************************************************/
/* Function Name : DvmrpRthCancelTimedoutNbr                                  */
/* Description   : This function updates the tRouteTbl by cancelling the      */
/*                 dependancy of a particular nbr who has timed out. This func*/
/*                 will be called by the Nbr. Disc. module, when a nbr times  */
/*                 out (35 secs).                                             */
/* Input(s)      : u4NbrAddr : neighbor's ip addr.                            */
/*                 u4IfIndex : interface index to reach this nbr.             */
/* Output(s)      : None.                                                      */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRthCancelTimedoutNbr (UINT4 u4NbrAddr, UINT4 u4IfIndex)
#else

void
DvmrpRthCancelTimedoutNbr (u4NbrAddr, u4IfIndex)
     UINT4               u4NbrAddr;
     UINT4               u4IfIndex;
#endif
{
    UINT1               u1NbrFound;
    UINT1               u1AllLeaf;
    UINT1               u1Flag;
    UINT4               u4Entry;
    UINT4               u4Index;
    tRouteTbl          *pRouteTbl;
    tNextHopTbl        *pNextHopTbl;
    tNextHopType       *pNextHopType;
    tNbrAddrList       *pNbrAddrList;
    tNbrAddrList       *pTmpNbr;

    /*
     *  If the timedout Nbr is upstream Nbr then all the routes learned by him
     *  are placed in holdown. This will take care of updating Forwarding cache 
     *  entries.
     */

    u1NbrFound = DVMRP_FALSE;
    for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
    {
        pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);
        /* 
         * check for an inactive entry or else causes infinite loop in HASH 
         */
        if (pRouteTbl->u1Status != DVMRP_INACTIVE)
        {
            if ((pRouteTbl->u4UpstreamNbr == u4NbrAddr) &&
                (pRouteTbl->u4IfaceIndex == u4IfIndex))
            {
                DvmrpRthMoveToHoldown (pRouteTbl);
                LOG2 ((INFO, RTH, "This route is now placed in Holdown,"
                       "which was learnt by..", u4NbrAddr));
                LOG1 ((INFO, RTH, "Who Timedout"));
                u1NbrFound = DVMRP_TRUE;
            }
            else if (pRouteTbl->u4IfaceIndex != u4IfIndex)
            {
                /* 
                 * If there are local hosts, then I should be forwarding the
                 * datagrams for all the source networks for which the 
                 * neighbor use to forward, when the nbr goes down so, I
                 * should set my Designated forwarder flag for each of those
                 * routes on this ifce.
                 */

                u1Flag = DvmrpRthSearchNextHopTbl (pRouteTbl->u4SrcNetwork,
                                                   pRouteTbl->u4SrcMask,
                                                   &u4Entry);
                if (u1Flag == DVMRP_TRUE)
                {
                    pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Entry);
                    DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfIndex,
                                             DVMRP_TRUE);
                }
                else
                {
                    DvmrpGmSendGraftOnIfaceChange (u4IfIndex,
                                                   pRouteTbl->u4SrcNetwork,
                                                   NULL);
                }

                /* 
                 *  Note: If the above doesent work out we should have a 
                 *  datastruct to cache the subordinate router's addr, iface 
                 *  and source network so, that when this guy goes down we 
                 *  can use this struct to make ourselves the designated 
                 *  forwarder for all those source networks on this interface.
                 */

            }
        }
    }
    if (u1NbrFound == DVMRP_FALSE)
    {
        LOG1 ((WARNING, RTH, "We Did not Learn Any Route from this Nbr..!!"));
        LOG1 ((WARNING, RTH, "Nbr is not advertising its own Subnet Also.. "));
        LOG1 ((WARNING, RTH, "But this scenario is quite Possible!!"));
    }

    /*
     *  All the downstream dependancy by this Nbr should be canceled. This 
     *  may trigger a prune to be sent upstream if it was the last downstream
     *  dependent Nbr.
     */

    u1NbrFound = DVMRP_FALSE;
    for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
        if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
        {
            /* 
             * Local variable should be passed and not
             * the pointer to the Nexthop type to the macro
             */

            DVMRP_ROUTE_DELETE_NEIGHBOR (pNextHopTbl, u4IfIndex, u4NbrAddr,
                                         pTmpNbr, pNbrAddrList, u1NbrFound,
                                         u1AllLeaf);
        }
    }                            /* for */

    /* 
     *  The Nbr has to be removed from Forward cache also.
     *  But if there are any hosts locally We should start forwarding
     *  blindly if we are not currently doing so.
     */

    if (u1NbrFound == DVMRP_TRUE)
    {
        DvmrpPmSendPruneOnNbrChange (u4IfIndex, 0, u4NbrAddr);
        LOG2 ((INFO, RTH, "Down stream dependancy is canceled for Nbr..",
               u4NbrAddr));
    }
}

/******************************************************************************/
/* Function Name : DvmrpRthLocIfaceUpdate                                     */
/* Description   : ThIs function updates the tRouteTbl, whenever there is a   */
/*                 state change in local interface. This function will be     */
/*                 called when a local interface goes down or comes up, by the*/
/*                 Input Handler Module.                                      */
/* Input(s)      : u4UpstreamNbr : upstream nbr addr.                         */
/*                 u4Mask        : mask associated with nbr addr.             */
/*                 u1Metric      : iface metric.                              */
/*                 u4IfId        : iface index.                               */
/*                 u1Status      : iface status, can be ACTIVE/INACTIVE       */
/* Output(s)      : None                                                       */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
void
DvmrpRthLocIfaceUpdate (UINT4 u4UpstreamNbr, UINT4 u4Mask,
                        UINT1 u1Metric, UINT4 u4IfId, UINT1 u1Status)
#else

void
DvmrpRthLocIfaceUpdate (u4UpstreamNbr, u4Mask, u1Metric, u4IfId, u1Status)
     UINT4               u4UpstreamNbr;
     UINT4               u4Mask;
     UINT1               u1Metric;
     UINT4               u4IfId;
     UINT1               u1Status;
#endif
{
    UINT1               u1Flag;
    UINT4               u4Index;
    UINT4               u4SrcNetwork = u4UpstreamNbr & u4Mask;
    UINT1               u1Key;
    tRouteTbl          *pRouteTbl;
    tNextHopTbl        *pNextHopTbl;
    tNextHopType       *pNextHopType;
    tNextHopType       *pTmpList;

    if (gpRouteHashTable == NULL)
    {
        LOG1 ((INFO, RTH,
               "Route Hash table is NULL so exiting DvmrpRthLocIfaceUpdate"));
        return;
    }

    switch (u1Status)
    {
        case DVMRP_INACTIVE:
            for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
            {
                pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);

                /*
                 *  If the interface is connected to a WAN link then there
                 *  is no local subnet info in my route table, But I might have
                 *  learnt many routes from this interface as my upstream
                 *  interface, so I need not be bothered to check wether it is
                 *  a LAN or WAN interfaace when an interface goes down.
                 */

                if (((pRouteTbl->u1Status == DVMRP_ACTIVE) ||
                     (pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE)) &&
                    (pRouteTbl->u4IfaceIndex == u4IfId))
                {
                    DvmrpRthMoveToHoldown (pRouteTbl);
                    LOG1 ((INFO, RTH, "The route which was dependent"
                           "on this iface is placed Holdown"));
                }
            }

            /*
             *  release the iface from each of the Next hop table entry.....
             *  Again we need not bother wether the iface is WAN or LAN here too.
             */

            for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
            {
                pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
                if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
                {
                    pTmpList = pNextHopType = pNextHopTbl->pNextHopType;
                    while (pNextHopType)
                    {
                        if (pNextHopType->u4NHopInterface == u4IfId)
                        {
                            DvmrpUtilReleaseNbr ((tNbrList *)
                                                 pNextHopType->pDependentNbrs);
                            LOG1 ((INFO, RTH, "The Nbr is Released"));
                            pNextHopType->pDependentNbrs = NULL;
                            if (pNextHopType == pNextHopTbl->pNextHopType)
                            {
                                pNextHopTbl->pNextHopType =
                                    pNextHopType->pNextHopTypeNext;
                            }
                            else
                            {
                                pTmpList->pNextHopTypeNext =
                                    pNextHopType->pNextHopTypeNext;
                            }
                            pNextHopType->pNextHopTypeNext = NULL;
                            DvmrpUtilReleaseIface ((tIfaceList *) pNextHopType);
                            LOG1 ((INFO, RTH, "The Iface is Released"));
                            if (pNextHopTbl->pNextHopType == NULL)
                                pNextHopTbl->u1Status = DVMRP_INACTIVE;
                            break;
                        }
                        else
                        {
                            pTmpList = pNextHopType;
                            pNextHopType = pNextHopType->pNextHopTypeNext;
                        }
                    }            /* while */
                }
            }

            /*
             *  Forward cache table might have this iface still in its 
             *  some of the entries, we need to remove this iface from 
             *  there also.
             *  Interface id should be passed 
             *  instead of u4Index 
             */

            DvmrpPmSendPruneOnIfaceChange (u4IfId, 0);
            break;

        case DVMRP_ACTIVE:
            /*
             *  There are 2 cases, where in this case will fall in,
             *  one is when the interface which had gone down comes up, and the
             *  other is the interfaces was up, but the parameters of the
             *  interface got changed.
             */

            u1Flag = DVMRP_FALSE;
            for (u4Index = 0; u4Index < MAX_ROUTE_TABLE_ENTRIES; u4Index++)
            {
                pRouteTbl = DVMRP_GET_ROUTENODE_FROM_INDEX (u4Index);
                if (pRouteTbl->u4IfaceIndex == u4IfId)
                {
                    if ((pRouteTbl->u1Status == DVMRP_NEVER_EXPIRE) ||
                        ((((pRouteTbl->u4UpstreamNbr & pRouteTbl->u4SrcMask) ==
                           pRouteTbl->u4SrcNetwork) &&
                          ((pRouteTbl->u1Status == DVMRP_HOLDOWN) ||
                           (pRouteTbl->u1Status == DVMRP_ROUTE_RELEARNT)))))
                    {
                        /*  the next hop table entry and forward cache entries
                         *  for this source network will be deleted and now we 
                         *  can over write the Holdown status back to Never 
                         *  expire and update this entry with reported 
                         *  parameters.
                         */
                        DvmrpRthMoveToHoldown (pRouteTbl);
                        DVMRP_GET_HASH_INDEX (pRouteTbl->u4SrcNetwork,
                                              pRouteTbl->u4SrcMask, u1Key);
                        DVMRP_HASH_DELETE_NODE (gpRouteHashTable,
                                                &pRouteTbl->RouteEntryNext,
                                                u1Key);
                        pRouteTbl->u4SrcNetwork = u4SrcNetwork;
                        pRouteTbl->u4SrcMask = u4Mask;
                        pRouteTbl->u4UpstreamNbr = u4UpstreamNbr;
                        pRouteTbl->u1Status = DVMRP_NEVER_EXPIRE;
                        DVMRP_GET_HASH_INDEX (pRouteTbl->u4SrcNetwork,
                                              pRouteTbl->u4SrcMask, u1Key);
                        DVMRP_HASH_ADD_NODE (gpRouteHashTable,
                                             &pRouteTbl->RouteEntryNext,
                                             u1Key, NULL);
                        u1Flag = DVMRP_TRUE;
                        ++gu4NumReachableRoutes;
                        break;
                    }
                    else
                    {
                        /* 
                         * We need not make the status active as, there may be
                         * other routes which were in holdown because of some 
                         * other reason and not because the interface went 
                         * down. Let the route be relearnt, any way if it were
                         * to learn it from the same iface and gateway with 
                         * same metric, it will be placed in Active state any
                         * way.
                         */
                        continue;
                    }
                }
            }                    /* for */
            if (u1Flag == DVMRP_FALSE)
            {
                /* 
                 * Checking for possible duplicate entries getting added in to
                 * the route table.
                 */
                DVMRP_GET_HASH_INDEX (u4SrcNetwork, u4Mask, u1Key);
                DVMRP_HASH_SCAN_BUCKET (gpRouteHashTable, u1Key, pRouteTbl,
                                        tRouteTbl *)
                {
                    if ((pRouteTbl->u4SrcNetwork == u4SrcNetwork) &&
                        (pRouteTbl->u4SrcMask == u4Mask) &&
                        (pRouteTbl->u1Metric > u1Metric))
                    {
                        pRouteTbl->u1Status = DVMRP_NEVER_EXPIRE;
                        pRouteTbl->u4UpTime = DvmrpCurrentTime ();
                        pRouteTbl->i4ExpiryTime = DVMRP_DELETE_ROUTE_AFTER;
                        pRouteTbl->u1Metric = u1Metric;
                        pRouteTbl->u4UpstreamNbr = u4UpstreamNbr;
                        pRouteTbl->u4IfaceIndex = u4IfId;
                        ++gu4NumReachableRoutes;
                        u1Flag = DVMRP_TRUE;
                        break;
                    }
                }
                if (u1Flag == DVMRP_FALSE)
                {
                    pRouteTbl =
                        DvmrpRthAddNewEntry (u4SrcNetwork, u4Mask, u1Metric,
                                             u4UpstreamNbr, u4IfId,
                                             DVMRP_NEVER_EXPIRE);
                    if (pRouteTbl != NULL)
                    {
                        DVMRP_HASH_ADD_NODE (gpRouteHashTable,
                                             &pRouteTbl->RouteEntryNext,
                                             u1Key, NULL);
                    }
                    else
                    {
                        LOG1 ((CRITICAL, RTH,
                               "The route table is full cant Add."));
                        return;
                    }
                }
            }

            /*
             *  Now I have to add this interface to each of the Active NextHop
             *  table entries and make it as LEAF and set the Designated 
             *  Forwarder Flag to TRUE implicitly.
             */
            for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
            {
                pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
                if (pNextHopTbl->u1Status == DVMRP_ACTIVE)
                {
                    pTmpList = pNextHopType = pNextHopTbl->pNextHopType;
                    while (pTmpList)
                    {
                        pNextHopType = pTmpList;
                        pTmpList = pTmpList->pNextHopTypeNext;
                    }
                    if ((pNextHopType->pNextHopTypeNext =
                         (tNextHopType *) DvmrpUtilGetFreeIface ()) == NULL)
                    {
                        LOG1 ((CRITICAL, RTH,
                               "No free memory for this interface"));
                        return;
                    }
                    pNextHopType = pNextHopType->pNextHopTypeNext;
                    pNextHopType->u4NHopInterface = u4IfId;
                    pNextHopType->u1InType = LEAF;
                    DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfId, DVMRP_TRUE);
                    LOG1 ((INFO, RTH, "New iface added on all nexthop table"
                           "entries and Desig Forw flag set."));
                }
            }                    /* for */
            break;

        default:
            LOG1 ((WARNING, RTH, "Unknown iface status"));
            break;
    }
    return;
}

/****************************************************************************/
/* Function Name    :  DvmrpRthSearchNextHopTbl                             */
/* Description      :  This function searches the NextHopTable for a given  */
/*                     source network and source mask address. This returns */
/*                     the first free entry if there is no Hit              */
/* Input (s)        :  u4SrcNetwork : Source network address which is       */
/*                                    searched for                          */
/*                     u4Mask       : Subnet Mask of the Source network     */
/* Output (s)       :  pu4Entry     : Entry corresponding to the source     */
/*                                    and mask if there is a hit. otherwise */
/*                                    the first available free entry.       */
/* Returns          :  DVMRP_TRUE   : If Entry is found                     */
/*                     DVMRP_FALSE  : Otherwise.                            */
/****************************************************************************/

#ifdef __STDC__
UINT1
DvmrpRthSearchNextHopTbl (UINT4 u4SrcNetwork, UINT4 u4Mask, UINT4 *pu4Entry)
#else

UINT1
DvmrpRthSearchNextHopTbl (u4SrcNetwork, u4Mask, pu4Entry)
     UINT4               u4SrcNetwork;
     UINT4               u4Mask;
     UINT4              *pu4Entry;
#endif
{
    UINT4               u4Index;
    UINT4               u4FreeNexthopEntry = MAX_NEXT_HOP_TABLE_ENTRIES;
    tNextHopTbl        *pNextHopTbl = NULL;

    for (u4Index = 0; u4Index < MAX_NEXT_HOP_TABLE_ENTRIES; u4Index++)
    {

        /* 
         * While searching for the Source Network, Get the First available
         * free entry also .
         */
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4Index);
        if ((pNextHopTbl->u1Status == DVMRP_INACTIVE) &&
            (u4FreeNexthopEntry == MAX_NEXT_HOP_TABLE_ENTRIES))
        {
            u4FreeNexthopEntry = u4Index;
        }
        else if ((pNextHopTbl->u1Status == DVMRP_ACTIVE) &&
                 (pNextHopTbl->u4NextHopSrc == u4SrcNetwork) &&
                 (pNextHopTbl->u4NextHopSrcMask == u4Mask))
        {
            *pu4Entry = u4Index;
            return DVMRP_TRUE;
        }
    }                            /* end for */
    *pu4Entry = u4FreeNexthopEntry;
    return DVMRP_FALSE;
}

/****************************************************************************/
/* Function Name    :  DvmrpRthCreateNodeinNexthopTbl                       */
/* Description      :  This function Fills in new entry in NexthopTable for */
/*                     the specified Source network, mask and the interface.*/
/* Input (s)        :  pRouteTblPtr       : Pointer to a Route Table entry  */
/*                     u4IfId             : Index of the interface table,   */
/*                                          which holds the Next hop        */
/*                                          interface Id                    */
/*                     u4FreeNexthopEntry : Free next hop table entry.      */
/* Output (s)       :  None                                                 */
/* Returns          :  None                                                 */
/****************************************************************************/

#ifdef __STDC__
void
DvmrpRthCreateNodeinNexthopTbl (tRouteTbl * pRouteTblPtr,
                                UINT4 u4IfId, UINT4 u4FreeNexthopEntry)
#else

void
DvmrpRthCreateNodeinNexthopTbl (pRouteTblPtr, u4IfId, u4FreeNexthopEntry)
     tRouteTbl          *pRouteTblPtr;
     UINT4               u4IfId;
     UINT4               u4FreeNexthopEntry;
#endif
{
    tNextHopTbl        *pNextHopPtr;
    tNextHopType       *pNextHop;
    tNextHopType       *pTempPtr;
    tDvmrpInterfaceNode *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    INT1                i1First;

    pNextHopPtr = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u4FreeNexthopEntry);
    pNextHopPtr->u4NextHopSrc = pRouteTblPtr->u4SrcNetwork;
    pNextHopPtr->u4NextHopSrcMask = pRouteTblPtr->u4SrcMask;

    /* 
     * Status of the newly allocated next hop table entry 
     * should be made active 
     */

    pNextHopPtr->u1Status = DVMRP_ACTIVE;
    pTempPtr = pNextHop = pNextHopPtr->pNextHopType;

    pSllNode = TMO_SLL_First (&gDvmrpIfInfo.IfGetNextList);
    i1First = (pSllNode != NULL) ? DVMRP_OK : DVMRP_NOTOK;
    do
    {
        if (i1First == DVMRP_NOTOK)
        {
            break;
        }
        pIfNode = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,
                                      IfGetNextLink, pSllNode);
        if (DVMRP_CHECK_IF_STATUS (pIfNode))
        {
            /*
             * Add only active interfaces 
             * Even upstream interface shd be
             * present in the interface list 
             */
            if ((pNextHop = (tNextHopType *) DvmrpUtilGetFreeIface ()) == NULL)
            {
                LOG1 ((CRITICAL, RTH, "No free memory for this interface"));
                return;
            }
            pNextHop->u4NHopInterface = pIfNode->u4IfIndex;
            pNextHop->u1DesigForwFlag = DVMRP_TRUE;
            if (pIfNode->u4IfIndex == u4IfId)
            {
                pNextHop->u1InType = DVMRP_BRANCH;
            }
            else
            {
                pNextHop->u1InType = LEAF;
            }
            /* Newly formed node to be linked with
             * the list
             */
            if (pNextHopPtr->pNextHopType == NULL)
            {
                pNextHopPtr->pNextHopType = pNextHop;
            }
            else
            {
                pTempPtr->pNextHopTypeNext = pNextHop;
            }
            pTempPtr = pNextHop;

        }
    }
    while ((pSllNode = TMO_SLL_Next (&gDvmrpIfInfo.IfGetNextList,
                                     pSllNode)) != NULL);
}

/******************************************************************************/
/* Function Name : DvmrpRthSkipInvalidRoutes                                  */
/* Description      : Skips the routes received if the mask is invalid.          */
/* Input(s)         : pDataLen   : data length.                                  */
/*                 u1NumBytes : number of non zero bytes in the mask.         */
/*                 pDvmrpData : ptr to the route report data.                 */
/*                 pNbrTbl    : ptr to the Nbr Table.                         */
/* Output(s)      : pDataLen   : changed depending on no. of routes indexed by */
/*                              mask in the received route report.            */
/*                 pDvmrpData : changed depending on no. of routes indexed by */
/*                              mask in the received route report.            */
/* Returns         : None.                                                      */
/******************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpRthSkipInvalidRoutes (UINT4 *pDataLen, UINT1 u1NumBytes,
                           UINT1 *pDvmrpData, tNbrTbl * pNbrTbl)
#else

PRIVATE void
DvmrpRthSkipInvalidRoutes (pDataLen, u1NumBytes, pDvmrpData, pNbrTbl)
     UINT4              *pDataLen;
     UINT1               u1NumBytes;
     UINT1              *pDvmrpData;
     tNbrTbl            *pNbrTbl;
#endif
{
    UINT1               u1Metric;
    tDvmrpInterfaceNode *pIfNode = NULL;

    pIfNode = DVMRP_GET_IFNODE_FROM_INDEX (pNbrTbl->u4NbrIfIndex);

    if (NULL == pIfNode)
    {
        LOG1 ((WARNING, PM, "Invalid Interface Node"));
        return;
    }

    do
    {
        if (*pDataLen < (UINT4) (u1NumBytes + SIZEOF_METRIC))
        {
            pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
            LOG1 ((WARNING, RTH, "Improper Datalength"));
            LOG2 ((WARNING, RTH, "Number of bad routes Reported by this Nbr.",
                   pNbrTbl->u4NbrRcvBadRoutes));
            pIfNode->u4IfRcvBadRoutes++;
            LOG2 ((WARNING, RTH,
                   "Number of bad routes received on this iface: ",
                   pIfNode->u4IfRcvBadRoutes));
            return;
        }

        /*
         *  Skip howmuchever source prefix bytes, from the data buffer. 
         */

        if (u1NumBytes == 1)
        {
            pDvmrpData++;
        }
        if (u1NumBytes == 2)
        {
            pDvmrpData += u1NumBytes;
        }
        if (u1NumBytes == 3)
        {
            pDvmrpData += u1NumBytes;
        }
        if (u1NumBytes == 4)
        {
            pDvmrpData += u1NumBytes;
        }

        /* 
         *  Extract and Skip the metric size from the data buffer. 
         */

        u1Metric = *pDvmrpData++;
        *pDataLen -= u1NumBytes + SIZEOF_METRIC;
        pNbrTbl->u4NbrRcvBadRoutes++;    /* for SNMP */
        LOG1 ((WARNING, RTH, " bad mask and thats why, bad route"));
        LOG2 ((WARNING, RTH, "Number of Bad routes received from this Nbr.",
               pNbrTbl->u4NbrRcvBadRoutes));
        pIfNode->u4IfRcvBadRoutes++;
        LOG2 ((WARNING, RTH, "Number of bad routes received on this iface: ",
               pIfNode->u4IfRcvBadRoutes));

        /* 
         *  check if the last bit is set 
         */

    }
    while ((u1Metric & EIGHTH_BIT_SET) == EIGHTH_BIT_SET);
}

/******************************************************************************/
/* Function Name: DvmrpRthUpdateDesigForw                                     */
/* Description  : This function will change the designated forwarder status   */
/*                and accordingly calls the forwarding module to send a prune */
/*                or a graft.                                                 */
/* Input(s)        : pNextHopTbl : Pointer to the next hop table entry.          */
/*                u4IfIndex   : Interface index whose designated forwarder    */
/*                              is to be updated.                             */
/*                u1Flag      : Flag to be assigned to the interface.         */
/*                              (DVMRP_TRUE = Forward on this interface.      */
/*                              DVMRP_FALSE = Dont Forward on this interface) */
/* Output(s)    :  None.                                                       */
/* Returns       :  None.                                                       */
/******************************************************************************/

#ifdef __STDC__
PRIVATE void
DvmrpRthUpdateDesigForw (tNextHopTbl * pNextHopTbl, UINT4 u4IfIndex,
                         UINT1 u1Flag)
#else
PRIVATE void
DvmrpRthUpdateDesigForw (pNextHopTbl, u4IfIndex, u1Flag)
     tNextHopTbl        *pNextHopTbl;
     UINT4               u4IfIndex;
     UINT1               u1Flag;
#endif
{
    tNextHopType       *pNextHopType;

    pNextHopType = pNextHopTbl->pNextHopType;
    for (; pNextHopType != NULL; pNextHopType = pNextHopType->pNextHopTypeNext)
    {
        if (pNextHopType->u4NHopInterface == u4IfIndex)
        {
            if (u1Flag == DVMRP_FALSE)
            {
                pNextHopType->u1DesigForwFlag = u1Flag;
                DvmrpPmSendPruneOnIfaceChange (u4IfIndex,
                                               pNextHopTbl->u4NextHopSrc);
                LOG2 ((INFO, RTH, "Designated forwarder flag disabled no"
                       "forwarding on this iface.", u4IfIndex));
                break;
            }
            else if ((pNextHopType->u1DesigForwFlag == DVMRP_FALSE) &&
                     (u1Flag == DVMRP_TRUE))
            {
                pNextHopType->u1DesigForwFlag = u1Flag;
                DvmrpGmSendGraftOnIfaceChange (u4IfIndex,
                                               pNextHopTbl->u4NextHopSrc,
                                               pNextHopType->pDependentNbrs);
                LOG2 ((INFO, RTH,
                       "Designated forwarder flag enabled on this" "iface now.",
                       u4IfIndex));
                break;
            }
        }
    }
}

/******************************************************************************/
/* Function Name: DvmrpFlashUpdate                                           */
/* Description  : This function is used to send Flash and poisson 
 *                flash updates                                               */
/* Input(s)     : u4SrcNetwork : Source network                              */
/*                u4SrcMask    : Source Mask                                 */
/*                pNbrTbl      : pointer to neighbor table                   */
/*                u1Metric     : Metric                                      */
/*                u1FlashType  : FLASH UPDATE or POISSON FLASH UPDATE        */
/* Output(s)    :  None.                                                       */
/* Returns       :  None.                                                       */
/******************************************************************************/

void
DvmrpFlashUpdate (UINT4 u4SrcNetwork, UINT4 u4SrcMask, tNbrTbl * pNbrTbl,
                  UINT1 u1Metric, UINT1 u1FlashType)
{

    UINT1               u1Index = 0;
    UINT1               u1ChkStatus = DVMRP_FALSE;
    UINT4               u4NbrAddr;
    UINT4               u4NbrIdx;
    tNextHopTbl        *pNextHopTbl = NULL;
    tNextHopType       *pNextHopType = NULL;
    tNbrAddrList       *pNbrList = NULL;

    if (u1FlashType == DVMRP_POISSON_FLASH)
    {
        u1Metric += DVMRP_INFINITY_METRIC;
        DvmrpSendFlashRoute (u4SrcNetwork, u4SrcMask, u1Metric,
                             pNbrTbl->u4NbrIpAddress, pNbrTbl->u4NbrIfIndex);
        return;
    }

    for (u1Index = 0; u1Index < MAX_NEXT_HOP_TABLE_ENTRIES; u1Index++)
    {
        pNextHopTbl = DVMRP_GET_NEXTHOPNODE_FROM_INDEX (u1Index);
        if ((u4SrcNetwork == pNextHopTbl->u4NextHopSrc) &&
            (u4SrcMask == pNextHopTbl->u4NextHopSrcMask))
        {
            u1ChkStatus = DVMRP_TRUE;
            break;
        }
    }

    if (u1ChkStatus == DVMRP_FALSE)
    {
        LOG1 ((CRITICAL, RRF, "Next Hop Table is Null "));
        return;
    }

    pNextHopType = pNextHopTbl->pNextHopType;

    while (pNextHopType != NULL)
    {
        pNbrList = pNextHopType->pDependentNbrs;

        while (pNbrList != NULL)
        {
            u4NbrAddr = pNbrList->u4NbrAddr;
            u4NbrIdx = pNextHopType->u4NHopInterface;
            DvmrpSendFlashRoute (u4SrcNetwork, u4SrcMask, u1Metric, u4NbrAddr,
                                 u4NbrIdx);
            pNbrList = pNbrList->pNbr;
        }
        pNextHopType = pNextHopType->pNextHopTypeNext;
    }
}

/******************************************************************************/
/* Function Name: DvmrpSendFlashRoute                                        */
/* Description  : This function is used to send Flash and poisson            */
/*                flash updates                                              */
/* Input(s)     : u4SrcNetwork : Source network                              */
/*                u4SrcMask    : Source Mask                                 */
/*                u1Metric     : Metric                                      */
/*                u4NbrAddr    : neighbor address                            */
/*                u4NbrIdx     : Interface Index                             */
/* Output(s)    :  None.                                                     */
/* Returns      :  None.                                                     */
/******************************************************************************/
void
DvmrpSendFlashRoute (UINT4 u4SrcNetwork, UINT4 u4SrcMask,
                     UINT1 u1Metric, UINT4 u4NbrAddr, UINT4 u4NbrIdx)
{
    UINT4               u4DataLen = 0;
    UINT1               u1NumBytes;
    UINT1              *pu1Temp;
    UINT4               u4SnmpTmp;
    UINT1               u1Tmp;
    UINT4               u4TmpSrc;
    UINT4               u4TmpMask;
    INT4                i4Index;

    DVMRP_ALLOC_SEND_BUFFER ();
    if (gpSendBuffer == NULL)
    {
        LOG1 ((CRITICAL, RRF, "Memory failure in send buffer"));
        return;
    }

    pu1Temp = gpSendBuffer;
    gpSendBuffer += MAX_IGMP_HDR_LEN;

    u4TmpMask = u4SrcMask;
    u1NumBytes = 1;
    if ((*gpSendBuffer++ = (UINT1) (u4TmpMask >> 16)) != 0)
    {
        u1NumBytes = 2;
    }
    u4TmpMask = u4SrcMask;
    if ((*gpSendBuffer++ = (UINT1) (u4TmpMask >> 8)) != 0)
    {
        u1NumBytes = 3;
    }
    u4TmpMask = u4SrcMask;
    if ((*gpSendBuffer++ = (UINT1) (u4TmpMask)) != 0)
    {
        u1NumBytes = 4;
    }
    u4DataLen += 3;
    u4TmpSrc = u4SrcNetwork;
    u1Tmp = 24;
    for (i4Index = 0; i4Index < u1NumBytes; ++i4Index, u1Tmp -= 8)
    {
        *gpSendBuffer++ = (UINT1) (u4TmpSrc >> u1Tmp);
        u4TmpSrc = u4SrcNetwork;
    }

    /* Fill the Metric */
    *gpSendBuffer = u1Metric;

    *gpSendBuffer |= EIGHTH_BIT_SET;

    u4DataLen += u1NumBytes + METRIC_LEN + MAX_IGMP_HDR_LEN;
    u4SnmpTmp = METRIC_LEN + SIZEOF_MASK + u1NumBytes;
    LOG2 ((INFO, RRF, "Number of bytes Packed in this route is ", u4SnmpTmp));

    gpSendBuffer = pu1Temp;
    /* Leave the gpSendBuffer pointing to the begining */

    /* Send the Flash Update as a normal Route Update with only one route
     * so that there will be no need to handle when receiving the Flash
     * Update 
     */

    DvmrpOmSendPacket (u4NbrIdx, u4NbrAddr, u4DataLen, DVMRP_REPORT);

    return;
}
