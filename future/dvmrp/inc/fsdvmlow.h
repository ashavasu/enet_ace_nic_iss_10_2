/* $Id: fsdvmlow.h,v 1.4 2011/09/24 06:51:06 siva Exp $*/
/* Proto Validate Index Instance for DvmrpInterfaceTable. */
INT1
nmhValidateIndexInstanceDvmrpInterfaceTable ARG_LIST((INT4  , INT4 ));


/* Proto Type for Low Level GET FIRST fn for DvmrpInterfaceTable  */

INT1
nmhGetFirstIndexDvmrpInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpInterfaceLocalAddress ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceMetric ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceRcvBadPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceRcvBadRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));



/* Proto Validate Index Instance for DvmrpNeighborTable. */
INT1
nmhValidateIndexInstanceDvmrpNeighborTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpNeighborTable  */

INT1
nmhGetFirstIndexDvmrpNeighborTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpNeighborTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpNeighborUpTime ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborExpiryTime ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborGenerationId ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborMajorVersion ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborMinorVersion ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborCapabilities ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborRcvRoutes ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborRcvBadPkts ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborRcvBadRoutes ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for DvmrpRouteTable. */

/* Proto Type for Low Level GET FIRST fn for DvmrpRouteTable  */


/* Proto type for GET_NEXT Routine.  */


/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhGetDvmrpRouteControlIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpRoutePortNum ARG_LIST((UINT4  , UINT4 ,INT4 *));


/* Proto Validate Index Instance for DvmrpRouteNextHopTable. */
INT1
nmhValidateIndexInstanceDvmrpRouteNextHopTable ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpRouteNextHopTable  */

INT1
nmhGetFirstIndexDvmrpRouteNextHopTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpRouteNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpRouteNextHopType ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ,INT4 *));


INT1
nmhGetDvmrpMaxSrcs ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */



INT1
nmhSetDvmrpMaxSrcs ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */



INT1
nmhTestv2DvmrpMaxSrcs ARG_LIST((UINT4 *  ,INT4 ));


INT1 
nmhValidateIndexInstanceDvmrpForwardTable (UINT4 u4DvmrpSourceNetwork , 
                                           UINT4 u4DvmrpGroupAddress);
INT1 
nmhGetFirstIndexDvmrpForwardTable (UINT4 *pu4DvmrpSourceNetwork , 
                                   UINT4 *pu4DvmrpGroupAddress);
INT1 
nmhGetNextIndexDvmrpForwardTable (UINT4 u4DvmrpSourceNetwork ,
                                  UINT4 *pu4NextDvmrpSourceNetwork , 
                                  UINT4 u4DvmrpGroupAddress ,
                                  UINT4 *pu4NextDvmrpGroupAddress);
INT1 
nmhGetDvmrpForwardUpstreamNeighbor (UINT4 u4DvmrpSourceNetwork , 
                                    UINT4 u4DvmrpGroupAddress , 
                                    UINT4 *pu4RetValDvmrpForwardUpstreamNeighbor);
INT1 
nmhGetDvmrpForwardExpiryTime (UINT4 u4DvmrpSourceNetwork , 
                              UINT4 u4DvmrpGroupAddress , 
                              UINT4 *pu4RetValDvmrpForwardExpiryTime);
INT1 
nmhGetDvmrpForwardTblStatus (UINT4 u4DvmrpSourceNetwork , 
                             UINT4 u4DvmrpGroupAddress , 
                             INT4 *pi4RetValDvmrpForwardTblStatus);


INT1 
nmhValidateIndexInstanceDvmrpForwardPruneNbrTable (UINT4 u4DvmrpForwardSourceNetwork , 
                                     UINT4 u4DvmrpForwardGroupAddress , 
         INT4 i4DvmrpForwardIfIndex);

INT1 
nmhGetFirstIndexDvmrpForwardPruneNbrTable (UINT4 *pu4DvmrpForwardSourceNetwork , 
                             UINT4 *pu4DvmrpForwardGroupAddress , 
        INT4 *pi4DvmrpForwardIfIndex);

INT1 
nmhGetNextIndexDvmrpForwardPruneNbrTable (UINT4 u4DvmrpForwardSourceNetwork ,
                            UINT4 *pu4NextDvmrpForwardSourceNetwork , 
       UINT4 u4DvmrpForwardGroupAddress ,
       UINT4 *pu4NextDvmrpForwardGroupAddress , 
       INT4 i4DvmrpForwardIfIndex ,
       INT4 *pi4NextDvmrpForwardIfIndex);

INT1 
nmhGetDvmrpForwardPruneNeighbor (UINT4 u4DvmrpForwardSourceNetwork , 
                   UINT4 u4DvmrpForwardGroupAddress , 
     INT4 i4DvmrpForwardIfIndex , 
     UINT4 *pu4RetValDvmrpForwardPruneNeighbor);

INT1 
nmhGetDvmrpForwardNbrPruneTime (UINT4 u4DvmrpForwardSourceNetwork , 
                  UINT4 u4DvmrpForwardGroupAddress , 
    INT4 i4DvmrpForwardIfIndex , 
    INT4 *pi4RetValDvmrpForwardNbrPruneTime);

