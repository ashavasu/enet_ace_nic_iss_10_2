/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpinc.h,v 1.16 2014/12/18 12:12:24 siva Exp $
 *
 * Description:This file contains DVMRP and other system      
 *             include files.                            
 *
 *******************************************************************/
#ifndef   _DPINC_H
#define   _DPINC_H

/* Global/library include files. */

#include "lr.h"
#include "ip.h"
#include "utilipvx.h"
#include "igmp.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "snp.h"
#include "utlshinc.h"

#ifdef MFWD_WANTED
#include "mfmrp.h"
#endif

#ifdef MRI_WANTED
#include "mri.h"
#endif

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ipnp.h"
#include "ipmcnp.h"
#include "ipmcnpwr.h"
#endif 

#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"

#include "dpdata.h"
#include "dphash.h"
#include "dpdef.h"
#include "dptdfs.h"
#include "dpport.h"
#include "dpmacros.h"
#include "dpprot.h"

#ifndef _DPGLOB_H
#include "dpextern.h"
#endif

#include "dpnbr.h"
#include "dproute.h"
#include "dpsnmp.h"

#include "fsdvmrlw.h"

#include "dvmrp.h"
#include "dvmrpcli.h"

#include "dpsz.h"
#endif /* _DPINC_H */
