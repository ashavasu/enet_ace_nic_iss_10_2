/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpmacros.h,v 1.11 2014/01/24 12:24:54 siva Exp $
 *
 * Description:This file contains all the macros related      
 *             to the (FSAP2) BUFFER calls               
 *
 *******************************************************************/
#include "fssocket.h"
#define CRU_BUF_RELEASE_CHAIN(pChainHdr) \
        CRU_BUF_Release_MsgBufChain(pChainHdr,FALSE)

#define CRU_BUF_COPY_OVER_CHAIN(pChainHdr,pu1_Data,u4_Offset,u4_Size) \
        CRU_BUF_Copy_OverBufChain(pChainHdr,pu1_Data,u4_Offset,u4_Size)

#define CRU_BUF_COPY_FROM_CHAIN(pChainHdr,pu1_Data,u4_Offset,u4_Size) \
        CRU_BUF_Copy_FromBufChain(pChainHdr,pu1_Data,u4_Offset,u4_Size)

#define CRU_BUF_ALLOCATE_CHAIN(u4_DataSize,u4_DataOffset) \
        CRU_BUF_Allocate_MsgBufChain(u4_DataSize,u4_DataOffset)

#define CRU_BUF_Set_DataLength(pBufChain,u4_Length) \
        (pBufChain)->ModuleData.u4_DataLength = (u4_Length)

#define   CRU_GET_INTERFACE_TYPE(IfId)    CRU_BUF_Get_Interface_Type(IfId)   
#define   CRU_GET_INTERFACE_NUM(IfId)     CRU_BUF_Get_Interface_Num(IfId)    
#define   CRU_GET_INTERFACE_VCNUM(IfId)   CRU_BUF_Get_Interface_SubRef(IfId) 



/* Macro to get the Interface node from the IfIndex */
#define DVMRP_GET_IFNODE_FROM_INDEX(IfIndex) DvmrpGetInterfaceNode(IfIndex)

#define DVMRP_CHECK_IF_STATUS(pIfNode) \
        ((pIfNode->u1IfRowStatus == DVMRP_ACTIVE) &&         \
        (pIfNode->u1IfStatus == DVMRP_ACTIVE))               

#define DVMRP_CHECK_IF_ROW_STATUS(pIfNode) \
        (pIfNode->u1IfRowStatus == DVMRP_ACTIVE)

#define DVMRP_GET_IF_STATUS(pIfNode)          \
                pIfNode->u1Status

#define DVMRP_GET_NBR_PTR_FROM_IFNODE(pIfNode) \
                pIfNode->pNbrOnthisIface


/*------------------------------------------------------------------*/

#define DVMRP_GET_IF_HASHINDEX(u4IfIndex, u4HashIndex) \
     u4HashIndex = (u4IfIndex % gDvmrpIfInfo.u4HashSize)

/* macro to find base address of structure given the member address */
#define DVMRP_GET_BASE_PTR(type, memberName,pMember) \
              (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName) )

/* ------------------------------------------------------------------*/
/* All the Get Routines to access the global tables */
#define DVMRP_GET_NBRNODE_FROM_INDEX(u4Index)  \
                 ((tNbrTbl *)(*(gpNeighborTable + u4Index)))

#define DVMRP_GET_NBR_STATUS(u4Index)          \
                ((tNbrTbl *)(*(gpNeighborTable + u4Index)))->u1Status

#define DVMRP_GET_NBR_ADDRESS(u4Index)         \
                ((tNbrTbl *)(*(gpNeighborTable + u4Index)))->u4NbrIpAddress

#define DVMRP_GET_FWD_CACHENODE_FROM_INDEX(u4Index)      \
                ((tForwCacheTbl *)(*(gpForwCacheTable + u4Index)))

#define DVMRP_GET_FWDNODE_FROM_INDEX(u4Index)      \
                ((tForwTbl *)(*(gpForwTable + u4Index)))

#define DVMRP_GET_FWD_TBL_FROM_FWD_CACHE_INDEX(u4Index)  \
                ((tForwCacheTbl *)(*(gpForwCacheTable + u4Index)))->pSrcNwInfo

#define DVMRP_GET_ROUTENODE_FROM_INDEX(u4Index)          \
                ((tRouteTbl *)(*(gpRouteTable + u4Index)))

#define DVMRP_GET_NEXTHOPNODE_FROM_INDEX(u4Index)          \
                ((tNextHopTbl *)(*(gpNextHopTable + u4Index)))

#define DVMRP_GET_HOSTNODE_FROM_INDEX(u4Index)              \
                ((tHostMbrTbl *)(*(gpHostMbrTable + u4Index)))



#define DVMRP_UPDATE_OIF_NBR_INFO(pOutList)\
{\
     tNbrPruneList   *pNextNbr = NULL;\
     pNextNbr = pOutList->pDepNbrs;\
     while (pNextNbr != NULL)\
     {\
         if (pNextNbr->u4PruneLifeTime > 0)\
         {\
             pNextNbr->u4PruneLifeTime -= DEFAULT_PRUNE_EXPIRY_TIME;\
         }\
         pNextNbr = pNextNbr->pNbr;\
     }\
}
#define DVMRP_INET_NTOA(ipaddr)    UtlInetNtoa(ipaddr)

#define DVMRP_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
  tUtlInAddr  InAddr;\
         InAddr.u4Addr = OSIX_NTOHL (u4Value);\
         STRNCPY (pString, (CHR1 *)DVMRP_INET_NTOA(InAddr), CFA_MAX_PORT_NAME_LENGTH);\
\
}
   
#define DVMRP_CONVERT_IPADDR_TO_STR1(pString, u4Value)\
{\
  tUtlInAddr  InAddr;\
         InAddr.u4Addr = OSIX_NTOHL (u4Value);\
         STRNCPY (pString, (CHR1 *)DVMRP_INET_NTOA(InAddr), SNMP_MAX_INDICES + 1);\
\
}

/* macro to convert mask to mask length */
#define DVMRP_MASK_TO_MASKLEN(mask, masklen)  \
{ \
    UINT4 u4TmpMask  = (mask); \
    UINT4 u4TmpMasklen = sizeof((mask)) << 3;      \
    for ( ; u4TmpMasklen > 0; u4TmpMasklen--, u4TmpMask >>= 1) { \
         if (u4TmpMask & 0x1) { \
             break; \
         } \
    }\
   (masklen) = (UINT1) u4TmpMasklen; \
}

#define DVMRP_MEM_ALLOCATE(PoolId,pu1Block,type) \
            (pu1Block = (type *)(MemAllocMemBlk ((tMemPoolId)PoolId)))
    
