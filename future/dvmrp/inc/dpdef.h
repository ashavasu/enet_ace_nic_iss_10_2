/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpdef.h,v 1.14 2014/04/04 10:04:42 siva Exp $
 *
 * Description: This file contains all the #definitions
 *
 *******************************************************************/
#ifndef _DPDEF_H
#define _DPDEF_H

#define   DVMRP_BRANCH                             0x02
#define   LEAF                                     0x01

#define   IHM                                         1
#define   OM                                          2
#define   NDM                                         4
#define   RRF                                         8
#define   RTH                                        16
#define   FM                                         32
#define   PM                                         64
#define   GM                                        128
#define   HM                                        256
#define   MOD_SNMP                                  512
#define   MAIN                                     1024

#define   CRU_DVMRP_MODULE                         0x44


#define   BIT_OPER_STATUS                             0
#define   BIT_ADMIN_STATUS                            1
#define   BIT_IP_ADDR                                 2
#define   BIT_MASK                                    3
#define   BIT_IFACE_DEL                               5
#define   BIT_IFACE_MTU                               6
#define   LAST_BIT_SET                             0x01
#define   DEL_BIT_SET                              0x20
#define   DVMRP_SHUTDOWN_INTERVAL                     3
#define   DVMRP_TICKS_PER_SEC                         1

#ifdef INTEROP
#define   INTEROP_MAJOR_VERSION                     0xB
#define   INTEROP_MINOR_VERSION                     0x2
#endif

#define   DVMRP_MAJOR_VERSION                      0x03
#define   DVMRP_MINOR_VERSION                      0xFF

#define   DVMRP_TYPE                               0x13

#define   DVMRP_ROUTE_REPORT_INTERVAL                60
#define   DVMRP_PROBE_INTERVAL                       10
#define   DVMRP_BEGIN_HOLDOWN_AFTER                 140
#define   DVMRP_DELETE_ROUTE_AFTER                  120
#define   DVMRP_ADVANCE_TIMER_VAL                     5
#define   DVMRP_NBR_TIME_OUT_TIMER_VAL               35
#define   DVMRP_CACHE_AGE_OUT_TIMER_VAL               1
#define   DVMRP_NBR_AGE_OUT_TIMER_VAL                 1
#define   DVMRP_MAX_CACHE_AGE_OUT_TMR_VAL           100

#define   DVMRP_INFINITY_METRIC                      32
#define   NEWMETRIC_LESSTHAN_INF                   0x01
#define   NEWMETRIC_EQL_INF                        0x02
#define   NEWMETRIC_GR_INF_LESSTHAN_TWICE_INF      0x03

#define   DVMRP_NEW_GENID                             0
#define   DVMRP_OLD_GENID                             1

#define   DVMRP_JOIN                                  1
#define   DVMRP_LEAVE                                 2
#define   DVMRP_FORW_HASH                             2
#define   DVMRP_ROUTE_HASH                            1

#define   DVMRP_PROBE                                 1
#define   DVMRP_REPORT                                2
#define   DVMRP_PRUNE                                 7
#define   DVMRP_GRAFT                                 8
#define   DVMRP_GRAFTACK                              9

#define   DVMRP_RESERVED                              0
#define   DVMRP_CAPABILITIES                     0x000E

#define   METRIC_LEN                                  1
#define   SIZEOF_EACH_ROUTE                           8

#define   DVMRP_COMPARE(x, y)   ((x == y ? DVMRP_OK : DVMRP_NOTOK)) 
#define   DVMRP_MAX_SOURCES                   20
#define   DVMRP_MAX_IFACE_HASH_BUCKETS          256
#define   MAX_HASH_TABLE_ENTRIES                256
 
#define   DVMRP_MAX_INTERFACES            FsDVMRPSizingParams\
                                          [MAX_DP_MAX_IFACES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_NBR_TABLE_ENTRIES           FsDVMRPSizingParams\
                                          [MAX_DP_NBR_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_ROUTE_TABLE_ENTRIES         FsDVMRPSizingParams\
                                         [MAX_DP_ROUTE_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_HOST_MBR_TABLE_ENTRIES      FsDVMRPSizingParams\
                                     [MAX_DP_HOST_MBR_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_NEXT_HOP_TABLE_ENTRIES      FsDVMRPSizingParams\
                                      [MAX_DP_NEXT_HOP_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_FORW_TABLE_ENTRIES          FsDVMRPSizingParams\
                                          [MAX_DP_FWD_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_NBR_FREE_POOL_TABLE_ENTRIES  FsDVMRPSizingParams\
                                 [MAX_DP_NBR_FREE_POOL_TBL_ENTRIES_SIZING_ID].\
                                           u4PreAllocatedUnits

#define   MAX_SRC_FREE_POOL_TABLE_ENTRIES   FsDVMRPSizingParams \
                                 [MAX_DP_SRC_FREE_POOL_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   MAX_FORW_CACHE_TABLE_ENTRIES     FsDVMRPSizingParams\
                                   [MAX_DP_FWD_CACHE_TBL_ENTRIES_SIZING_ID].\
                                   u4PreAllocatedUnits

#define   MAX_IFACE_FREE_POOL_TABLE_ENTRIES    FsDVMRPSizingParams\
                               [MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES_SIZING_ID].\
                                          u4PreAllocatedUnits

#define   LAN                                     1

#define   DVMRP_ACTIVE                         0x01
#define   DVMRP_INACTIVE                       0x02
#define   DVMRP_HOLDOWN                        0x03
#define   DVMRP_ROUTE_RELEARNT                 0x04
#define   DVMRP_NEVER_EXPIRE                   0x05
#define   DVMRP_SET                            0x01
#define   DVMRP_RESET                          0x02

#define   DVMRP_PENDING                        0x06

/* Definition for the RowStatus values */
#define   DVMRP_ROW_STATUS_ACTIVE                 1
#define   DVMRP_ROW_STATUS_NOT_IN_SERVICE         2
#define   DVMRP_ROW_STATUS_NOT_READY              3
#define   DVMRP_ROW_STATUS_CREATE_AND_GO          4
#define   DVMRP_ROW_STATUS_CREATE_AND_WAIT        5
#define   DVMRP_ROW_STATUS_DESTROY                6

#define   DVMRP_ENABLED                           1
#define   DVMRP_DISABLED                          2

#define   DVMRP_ROUTE_REPORT_TIMER                1
#define   DVMRP_ROUTE_AGEOUT_TIMER                2
#define   DVMRP_CACHE_AGEOUT_TIMER                3
#define   DVMRP_NEIGHBOR_AGEOUT_TIMER             4
#define   DVMRP_PROBE_TIMER                       5
#define   DVMRP_GRAFT_RETRANS_TIMER               6
#define   DVMRP_PRUNE_RETRANS_TIMER               7

#define   DVMRP_MAX_TIMERS                        7
#define   DVMRP_MAX_TABLE_TIMERS                  5
#define   MAX_TIMER_BLOCKS    (MAX_FORW_CACHE_TABLE_ENTRIES + DVMRP_MAX_TABLE_TIMERS)

#define   MAX_MISC_BLOCKS   FsDVMRPSizingParams\
                            [MAX_DP_MISC_BLOCKS_SIZING_ID].\
                            u4PreAllocatedUnits

#define   MAX_MISC_BLOCK_SIZE                    2048
#define   DVMRP_TRUE                              0
#define   DVMRP_FALSE                             1
#define   DVMRP_ONE                               1

#define   LOG_NONE                             0x04
#define   ALL                                  0x05

#define   INFO                                 0x01
#define   WARNING                              0x02
#define   CRITICAL                             0x03

#define   ALL_DVMRP_ROUTERS      (UINT4)0xe0000004                        
#define   DVMRP_IN_CLASSD(i)     (((INT4)(i) & 0xf0000000) == 0xe0000000) 
#define   DVMRP_IN_BADCLASS(i)   (((INT4)(i) & 0xf0000000) == 0xf0000000) 
#define   DVMRP_IN_CLASSA(i)     (((INT4)(i) & 0x80000000) == 0)          

#define   DVMRP_ALLOC_SEND_BUFFER() { \
    if (DVMRP_MEM_ALLOCATE(DVMRP_MISC_PID, gpSendBuffer,UINT1)        \
                    == NULL)                                      \
    {                                                                     \
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"   \
                             " for Sending Messages"));                   \
        gpSendBuffer = NULL;                                              \
    }                                                                     \
    else {\
    MEMSET (gpSendBuffer, 0, MAX_MISC_BLOCK_SIZE);                        \
    }\
  }



/* 
 * Updated with another pointer gpRcvBufferRead 
 */

#define   DVMRP_ALLOC_RCV_BUFFER() { \
    if (DVMRP_MEM_ALLOCATE (DVMRP_MISC_PID, gpRcvBufferRead,UINT1)     \
                    == NULL)                                      \
    {                                                                     \
        LOG1 ((WARNING, IHM, "Unable to Allocate MemBlock from MemPool"   \
                             " for Copying Received DVMRP Messages"));    \
        gpRcvBufferRead = NULL;                                           \
    }                                                                     \
    else {\
        MEMSET (gpRcvBufferRead, 0, MAX_MISC_BLOCK_SIZE);                     \
    }\
    gpRcvBuffer = gpRcvBufferRead;                                        \
  }

#define DVMRP_GET_HASH_INDEX(u4Key1, u4Key2, u1HashIndex)               \
   {                                                                    \
      UINT1 *pTmp;                                                      \
      UINT4 u4Tmp;                                                     \
      u4Tmp = (u4Key1 ^ u4Key2);                                        \
      pTmp = (UINT1 *)&u4Tmp;                                           \
      u1HashIndex = (pTmp[0] ^ pTmp[1]) ^ (pTmp[2] ^ pTmp[3]);          \
   }

#define   LOG1(x)   Log1  x 
#define   LOG2(x)   Log2  x 

#define   VALIDATE_MCAST_ADDR(u4GroupAddr)                                   \
                       ((u4GroupAddr == 0) ? 0 :                             \
                         ((DVMRP_IN_CLASSD(u4GroupAddr)) ? 1 : 0 ) )

#define   VALIDATE_UCAST_ADDR(u4SrcAddr)                                     \
                     ( (u4SrcAddr == 0) ? 0 :                                \
                       ( (DVMRP_IN_BADCLASS(u4SrcAddr)) ? 0 :                \
                         ( (VALIDATE_MCAST_ADDR(u4SrcAddr)) ? 0 : 1 ) ) )


#define DVMRP_FWD_TBL_PID          gDvmrpMemPool.ForwTblPoolId
#define DVMRP_FWD_CACHE_TBL_PID    gDvmrpMemPool.ForwCacheTblPoolId
#define DVMRP_HOST_MBR_TBL_PID     gDvmrpMemPool.HostMbrTblPoolId
#define DVMRP_NBR_TBL_PID          gDvmrpMemPool.NbrTblPoolId
#define DVMRP_ROUTE_TBL_PID        gDvmrpMemPool.RouteTblPoolId
#define DVMRP_NEXT_HOP_TBL_PID     gDvmrpMemPool.NextHopTblPoolId
#define DVMRP_IFACE_TBL_PID        gDvmrpMemPool.IfaceTblPoolId
#define DVMRP_NBR_LIST_PID         gDvmrpMemPool.NbrListPoolId
#define DVMRP_IFACE_LIST_PID       gDvmrpMemPool.IfaceListPoolId
#define DVMRP_MISC_PID             gDvmrpMemPool.MiscPoolId
#define DVMRP_TIMER_PID            gDvmrpMemPool.TimerPoolId
#define DVMRP_Q_PID                gDvmrpMemPool.QPoolId
#define DVMRP_SRC_LIST_PID         gDvmrpMemPool.SrcInfoPoolId
#define DVMRP_FWD_TBL_PTR_PID      gDvmrpMemPool.ForwTblPtrPoolId
#define DVMRP_FWD_CACHE_PTR_PID    gDvmrpMemPool.ForwCachePtrPoolId
#define DVMRP_HOST_MBR_PTR_PID     gDvmrpMemPool.HostMbrPtrPoolId
#define DVMRP_NBR_TBL_PTR_PID      gDvmrpMemPool.NbrTblPtrPoolId
#define DVMRP_ROUTE_TBL_PTR_PID    gDvmrpMemPool.RouteTblPtrPoolId
#define DVMRP_NEXT_HOP_PTR_PID     gDvmrpMemPool.NextHopPtrPoolId
#define DVMRP_IFACE_LIST_PTR_PID   gDvmrpMemPool.IfaceListPtrPoolId
#define DVMRP_NBR_LIST_PTR_PID     gDvmrpMemPool.NbrListPtrPoolId


#define DVMRP_MAX_INT4                 0x7fffffff
#define DVMRP_MAX_UINT4            0xFFFFFFFF
#define DVMRP_DEFAULT_SOURCE_MASK  0xFFFFFFFF

#define DVMRP_ZERO       0
/* Defines for memory pool access */
#define  DVMRP_MEM_INIT     DvmrpMemInit
#define  DVMRP_MEM_RELEASE  DvmrpMemRelease
#define  DVMRP_MEM_CLEAR    DvmrpMemClear
#define  DVMRP_MEM_FREE     MEM_FREE
#define  DVMRP_MALLOC       MEM_MALLOC
#define  DVMRP_MEMCPY       MEMCPY

#define  DVMRP_MUTEX_SEMA4             "DVP"
#define  DVMRP_MUTEX_SEMID             gDpConfigParams.DpMutex.semId
#define DVMRP_MAX_TIMER   10
#define DVMRP_IANA_PROTOCOL_ID  4
#define DVMRP_MAX_GROUPS        10
#define DVMRP_FLASH         1
#define DVMRP_POISSON_FLASH 2

    /* Added for BCM */

#define  DVMRP_PROTOCOL_ID             0x13
 
#define  MAX_TIME_STRING  17
#define  DVMRP_RTR_ID     256
 
#define  DVMRP_IANA_UCAST_PROTOCOL_ID  0x11

#define DVMRP_PRUNE_TIMER_EXP  "Prune Timer Expries"

#ifdef MRI_WANTED

#define  DVMRP_MIN_INTERFACE_TTL   MRI_MIN_INTERFACE_TTL
#define  DVMRP_MAX_INTERFACE_TTL   MRI_MAX_INTERFACE_TTL

#define  DVMRP_MIN_RATELIMIT_IN_KBPS    MRI_MIN_RATELIMIT_IN_KBPS 
#define  DVMRP_MAX_RATELIMIT_IN_KBPS    MRI_MAX_RATELIMIT_IN_KBPS 

#define  DVMRP_ROUTE_MULTICAST     MRI_ROUTE_TYPE_MULTICAST
/* Out Interface status */
#define  DVMRP_OIF_STATE_PRUNED              MRI_OIF_STATE_PRUNED
#define  DVMRP_OIF_STATE_FORWARDING          MRI_OIF_STATE_FORWARDING

#else

#define  DVMRP_MIN_INTERFACE_TTL  0 
#define  DVMRP_MAX_INTERFACE_TTL  255 

#define  DVMRP_MIN_RATELIMIT_IN_KBPS  0 
#define  DVMRP_MAX_RATELIMIT_IN_KBPS  0

#define  DVMRP_ROUTE_MULTICAST    2 
/* Out Interface status */
#define  DVMRP_OIF_STATE_PRUNED          1
#define  DVMRP_OIF_STATE_FORWARDING      2


#endif /* MRI_WANTED */
 
#endif /* _DPDEF_H */
