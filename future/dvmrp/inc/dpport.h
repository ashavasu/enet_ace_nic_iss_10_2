/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpport.h,v 1.14 2010/07/30 13:17:00 prabuc Exp $
 *
 * Description: This file contains all the porting related 
 *              stuff   
 *
 *******************************************************************/

#ifndef _DPPORT_H
#define _DPPORT_H
INT4
DvmrpMfwdHandleStatus ARG_LIST ((UINT4 u4Event));

/*
 * Macros, to set IP send Params
 */
#define  CRU_MEM_REL_BUF(u2QueId,pParams)  \
                            CRU_BUF_RELEASE_FREE_OBJ(u2QueId,pParams)

#define  CRU_MEM_GET_BUF(u2QueId)  CRU_BUF_ALLOCATE_FREE_OBJ(u2QueId)


#define   DVMRP_NOTOK   -1            

 /*
 * Local Queue Events
 */
#define   DVMRP_STARTUP_EVENT         0x00000001 
#define   DVMRP_SHUTDOWN_EVENT        0x00000002 
#define   DVMRP_INTERFACE_EVENT       0x00000004 

/*
 * Task Queue Events
 */
#define   IGMP_HOST_DVMRP_EVENT       0x00000008 
#define   IGMP_DVMRP_EVENT            0x00000010 

/*
 * Timer Event
 */
#define  DVMRP_TIMER_EXPIRED_EVENT   0x00000020 

#define   DVMRP_INPUT_Q_EVENT         0x00000100
#define   DVMRP_DATA_EVENT            0x00000200

/* Port bit map Change Event  */

#ifdef FS_NPAPI   
#define DVMRP_PBMP_CHANGE_EVENT       0x00001000
#endif
   
#ifdef LNXIP4_WANTED
#define DVMRP_PACKET_ARRIVAL_EVENT    0x00000040
#endif
 
#ifdef MFWD_WANTED
#define  DVMRP_MFWD_DISABLE_EVENT     MFWD_TO_MRP_STATUS_DISABLED_EVENT
#define  DVMRP_MFWD_ENABLE_EVENT      MFWD_TO_MRP_STATUS_ENABLED_EVENT
#define  DVMRP_MFWD_DELIVER_MDP       0x01
#define  DVMRP_MFWD_DONT_DELIVER_MDP  0x03
#define  DVMRP_MFWD_DELIVER_FULL_MDP  MFWD_MRP_DELIVER_FULLMDP
#else
#define  DVMRP_MFWD_DELIVER_MDP       1
#define  DVMRP_MFWD_DELIVER_FULL_MDP  2
#define  DVMRP_MFWD_DONT_DELIVER_MDP  3
#endif

#ifdef LNXIP4_WANTED
#define  DVMRP_RECV_ANCILLARY_LEN    24
#define  DVMRP_RX_MTU              1500
#endif

#define   NUM_1S_TIMER_BLOCKS         100        

/*
 * Queue Numbers
 */
#define   IGMP_DVMRP_QUEUE        TMO_TASK_QUE_00               
#define   IGMP_HOST_DVMRP_QUEUE   TMO_TASK_QUE_02               
#define   DVMRP_IP_QUEUE          CRU_FORW_RT_TASK_INPUT_TQUEUE 

#define   SRC_IP_ADDR_OFFSET     12
#define   DEST_IP_ADDR_OFFSET    16

#define   MAX_IP_HDR_LEN         60
#define   MIN_IP_HDR_LEN         20
#define   MAX_IGMP_HDR_LEN        8
#define   IP_ADDR_LEN             4
#define   LOWER_LAYER_HDR        14
#define   IP_IDENTIFICATION       0

 /* 
 * Size of Host Membership packet received from IGMP 
 */
#define   HOST_PACKET_SIZE       sizeof(tIgmpHost) 
#define   IGMP_TYPE            0x02

#define   DVMRP_DEFAULT_TOS    0xc0
#define   DVMRP_DEFAULT_TTL       1
#define   DEFAULT_DF              0
#define   DEFAULT_OPT_LEN         0
#define   DEFAULT_METRIC          1

/* 
 * Added this macro as hashing requires it 
 */
#define   DVMRP_TASK_PRIORITY        60                            
#define   DVMRP_MAX_OIFLIST_SIZE     IPIF_MAX_LOGICAL_IFACES+2     
#define   DVMRP_MULTICAST_DATA       IP_MCAST_DATA_PACKET_FROM_MRM 
#define   IP_PKT_OFFSET_TOTAL_LEN    2                             

#define   DVMRP_GET_SYS_TIME(time) \
          time = (UINT2)(time * SYS_TIME_TICKS_IN_A_SEC)

#define  DVMRP_REGISTER_WITH_IGMP       IgmpRegisterMRP
#define  DVMRP_DEREGISTER_WITH_IGMP     IgmpDeRegisterMRP
#define  DVMRP_IP_GET_METRIC_PREFERENCE IpGetProtocolPreference
#define  DVMRP_IP_GET_PORT_FROM_INDEX   NetIpv4GetPortFromIfIndex        
#define  DVMRP_GET_IFINDEX_FROM_PORT    NetIpv4GetCfaIfIndexFromPort 
#define  DVMRP_GET_IFINFO               NetIpv4GetIfInfo

#define  DVMRP_STACK_SIZE             OSIX_DEFAULT_STACK_SIZE
#define  DVMRP_TASK_NAME             "DVM"                        

/* Structure for the PIM Q Message */
/* --------------------------------*/

typedef struct _DvmrpQMsg {
        UINT4  u4MsgType ;
        union _DvmrpQMsgParam{
                tCRU_BUF_CHAIN_HEADER   *DvmrpIfParam;
                tDvmrpIfStatus          DvmrpIfStatus;
        }DvmrpQMsgParam;
}tDvmrpQMsg;


typedef struct _DvmrpMsgBlock{
    UINT1  au1Buf[MAX_DP_MISC_BLOCK_SIZE];
}tDvmrpMsgBlock;

/* Macro to extract the IfIndex from the message buffer */
/* ---------------------------------------------------- */
#define DVMRP_GET_IFINDEX(pBuf, u4IfIndex) \
{ \
   tIpParms *pIpParms = NULL;\
   pIpParms = (tIpParms *)(&pBuf->ModuleData);\
   u4IfIndex = pIpParms->u2Port; \
}

#define IP_EXTRACT_HEADER    DvmrpExtractIpHdr 

INT4
DvmrpExtractIpHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);

INT4 DvmrpEnqueueMcastDataPktToIp ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                             UINT4 u4SrcAddr,
                                             UINT4 u4DestAddr,
                                             UINT4 * pOifList));


void      DvmrpUpdateIfStatus (tNetIpv4IfInfo *, UINT4);

void      DvmrpHandleProtocolPackets (tCRU_BUF_CHAIN_HEADER *);
void      DvmrpHandleHostPkt (tGrpNoteMsg);
void      DvmrpHandleHostPackets (tIgmpHost Packet);

INT4
DvmrpHandleDatagram (tDvmrpInterfaceNode *pIfNode, tCRU_BUF_CHAIN_DESC * pBuf,
                     UINT4 u4SrcAddr, UINT4 u4DestAddr);
INT4
DvmrpMemAllocate (tMemPoolId MemPoolId, UINT1 **ppu1Block);
INT4
DvmrpMemRelease (tMemPoolId MemPoolId, UINT1 *pu1Block);

#ifdef LNXIP4_WANTED
VOID DvmrpHandlePacketArrivalEvent (VOID);
INT4 DvmrpCreateSocket (VOID);
INT4 DvmrpSetSocketOption (VOID);
VOID DvmrpCloseSocket (VOID);
VOID DvmrpNotifyPktArrivalEvent (INT4);
INT4 DvmrpRegisterForMDP (VOID); 
INT4 DvmrpDeRegisterForMDP (VOID); 
VOID DvmrpMdpTaskMain (VOID);
INT4 DvmrpJoinMcastGroup (UINT4 u4IfAddr);
INT4 DvmrpLeaveMcastGroup (UINT4 u4IfAddr);
INT4 DvmrpInitAndAddSocket (VOID);
VOID DvmrpDeInitAndRemoveSocket (VOID);
#endif  /* LNXIP4_WANTED */

#define DVMRP_MRP_POST_MRTUPDATE_TO_MFWD      MfwdPostMrpMsgToMfwd
#define  DVMRP_MFWD_REGISTER_MRP(u2RtrId, i4Status) \
{ \
    tMrpRegnInfo  regninfo; \
    regninfo.u2OwnerId = u2RtrId; \
    regninfo.u4MaxGroups = DVMRP_MAX_GROUPS; \
    regninfo.u4MaxSrcs = DVMRP_MAX_SOURCES;\
    regninfo.u1Mode = 1;\
    regninfo.u4MaxIfaces = DVMRP_MAX_INTERFACES; \
    regninfo.u1ProtoId = DVMRP_IANA_PROTOCOL_ID; \
    regninfo.MrpHandleMcastDataPkt = (VOID (*) (struct CRU_BUF_CHAIN_DESC *))DvmrpProcessMcastDataPkt;\
    regninfo.MrpHandleV6McastDataPkt = NULL;\
    regninfo.MrpInformMfwdStatus = (INT4 (*) (UINT4))DvmrpMfwdHandleStatus; \
    i4Status = MfwdInputHandleRegistration (regninfo); \
    i4Status = (i4Status == MFWD_SUCCESS)?DVMRP_OK:DVMRP_NOTOK; \
}

#define  DVMRP_MFWD_DEREGISTER_MRP(u2RtrId) \
{ \
    MfwdInputHandleDeRegistration (u2RtrId); \
}
/* Enqueue the message to MFWD module */ 
#define  MFWD_ENQUEUE_DVMRP_UPDATE_TO_MFWD(pMrtUpdBuf, i4Status) \
{ \
    i4Status = DVMRP_MRP_POST_MRTUPDATE_TO_MFWD(pMrtUpdBuf);\
    if (i4Status == MFWD_FAILURE) \
    { \
        LOG1 ((WARNING, IHM, "Failure in Posting the update message to MFWD"));\
        i4Status = DVMRP_NOTOK; \
    } \
    else \
    { \
         LOG1 ((WARNING, IHM, "Successfully enqueued a MRT update message to MFWD"));\
         i4Status = DVMRP_OK;\
    } \
}

#define  DVMRP_EXTRACT_MFWD_HEADER(MsgHdr, pBuffer, i4Status) \
{ \
      i4Status = CRU_BUF_Copy_FromBufChain (pBuffer, MsgHdr,0 , \
                                           sizeof (tMfwdMrpMDPMsg)); \
      i4Status = (i4Status == CRU_FAILURE)?DVMRP_NOTOK:DVMRP_OK; \
}

#define  DVMRP_SKIP_MFWD_MSG_HEADER(pBuffer) \
     CRU_BUF_Move_ValidOffset (pBuffer, sizeof (tMfwdMrpMDPMsg));

#define  DVMRP_GET_MFWD_LAST_FWD_TIME(u4Src, u4Grp, pu4FwdTime, Status)  \
{ \
    tIPvXAddr SrcAddr; \
    tIPvXAddr GrpAddr; \
    if (gDpConfigParams.u1MfwdStatus == MFWD_STATUS_DISABLED) {\
             *pu4FwdTime = 0; \
             Status = DVMRP_NOTOK;\
     }\
     else \
     { \
         u4Src = OSIX_NTOHL(u4Src); \
         u4Grp = OSIX_NTOHL(u4Grp); \
         IPVX_ADDR_INIT(SrcAddr, IPVX_ADDR_FMLY_IPV4, (UINT1 *) &(u4Src)); \
         IPVX_ADDR_INIT(GrpAddr, IPVX_ADDR_FMLY_IPV4, (UINT1 *) &(u4Grp)); \
         MfwdMrtGetLastFwdTime (DVMRP_RTR_ID, SrcAddr, GrpAddr, pu4FwdTime);\
         Status = DVMRP_OK; \
     } \
}

/*------------------------------------------------------------------*/
/* Macros for setting/getting time in sec */
#define DVMRP_GET_TIME_IN_SEC(Time) { \
	     (Time) = ((Time) / (SYS_TIME_TICKS_IN_A_SEC)); \
}
/*------------------------------------------------------------------*/


#ifdef LNXIP4_WANTED
#define  DVMRP_REGISTER_WITH_IP_FOR_MDP(status) \
     ((status = DvmrpRegisterForMDP() == DVMRP_NOTOK)?DVMRP_NOTOK:DVMRP_OK)
#else
#define  DVMRP_REGISTER_WITH_IP_FOR_MDP(status) \
 (((status = IpRegHLProtocolForMCastPkts(DvmrpProcessMcastDataPkt)) == IP_FAILURE)?DVMRP_NOTOK:status)
#endif
		  
#ifdef LNXIP4_WANTED
#define  DVMRP_DEREGISTER_WITH_IP_FOR_MDP(status) DvmrpDeRegisterForMDP();
#else
#define  DVMRP_DEREGISTER_WITH_IP_FOR_MDP(status) \
 IpDeRegHLProtocolForMCastPkts(status);
#endif
		  
#define DVMRP_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)
		  
#endif
