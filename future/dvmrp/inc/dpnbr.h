/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpnbr.h,v 1.3 2007/02/01 14:46:55 iss Exp $
 *
 * Description:This file contains all the definitions, macros 
 *             and prototypes related to Neighbor Discovery   
 *             Module                                         
 *
 *******************************************************************/
#ifndef _DPNBR_H
#define _DPNBR_H

#define   DVMRP_GENERATION_ID_LEN   4 

#define DVMRP_FILL_NBR_TBL_ENTRY(pNbrTbl, IfaceId, u4SrcAddr, value, u4GenId, u1NbrCap) \
           {                                                                  \
           pNbrTbl->u4NbrIfIndex       = IfaceId;                             \
           pNbrTbl->u4NbrIpAddress     = u4SrcAddr;                           \
           pNbrTbl->u4NbrUpTime        = value;                               \
           pNbrTbl->u4ExpiryTime       = DVMRP_NBR_TIME_OUT_TIMER_VAL;        \
           pNbrTbl->u4NbrGenId         = u4GenId;                             \
           pNbrTbl->u1NbrMajorVer      = DVMRP_MAJOR_VERSION;                 \
           pNbrTbl->u1NbrMinorVer      = DVMRP_MINOR_VERSION;                 \
           pNbrTbl->u1NbrCapabilities  = u1NbrCap;                                   \
           pNbrTbl->u4NbrRcvBadPkts    = 0;                                   \
           pNbrTbl->u4NbrRcvBadRoutes  = 0;                                   \
           pNbrTbl->u4NbrRcvRoutes     = 0;                                   \
           pNbrTbl->u1Status           = DVMRP_ACTIVE;                        \
           pNbrTbl->u1SeenByNbrFlag    = DVMRP_FALSE;                         \
           pNbrTbl->pNbrTblNext        = NULL;                                \
          }

#endif /* _DPNBR_H */
