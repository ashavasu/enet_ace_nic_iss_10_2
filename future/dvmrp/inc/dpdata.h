/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpdata.h,v 1.6 2010/07/27 10:32:16 prabuc Exp $
 *
 * Description:This file contains all the #definitions        
 *             with respect to the Datagram handler module 
 *
 *******************************************************************/
#ifndef _DATA_H
#define _DATA_H

#define   DVMRP_Q_MODE                OSIX_LOCAL                    
#define   DEFAULT_CACHE_EXPIRY_TIME   DVMRP_CACHE_AGE_OUT_TIMER_VAL 
#define   DEFAULT_PRUNE_EXPIRY_TIME   DVMRP_CACHE_AGE_OUT_TIMER_VAL 

#define   DVMRP_Q                     "DVMQ"                           
#define   DVMRP_Q_SIZE                4

#define   DVMRP_Q_DEPTH              FsDVMRPSizingParams\
                                     [MAX_DP_Q_DEPTH_SIZING_ID].\
                                     u4PreAllocatedUnits

#define   REFRESH_CACHE_TIME          100                           
#define   SRC_ADDR_OFFSET             12                            
#define   DEST_ADDR_OFFSET            16                            

#define   DEFAULT_PRUNE_TIMER_VAL     3                             
#define   DEFAULT_GRAFT_TIMER_VAL     5                             

#define   DVMRP_DEFAULT_PRUNE_LIFE_TIME     50 
#define   PRUNE_LIFE_TIME     gDpConfigParams.u2PruneLifeTime 

#define   GRAFT_ACK_RECEIVED                 5
#define   GRAFT_WAITING_ACK                  6
#define   DVMRP_NORMAL                             7
#define   PRUNED                             8
#define   DATA_RECEIVED_ON_PRUNED_IFACE      9


#define   LOCAL_NETWORK                     10

#define   PRUNE_PKT_SIZE                    12
#define   GRAFT_PKT_SIZE                     8

#define   MAX_REFRESH_ALLOWED               60

#define   INT_REMOVE                         1
#define   HOST_LEAVE                         2

#define DVMRP_REFRESH_FORW_CACHE(u2Value)                             \
        {                                                             \
         if (( u2Value + REFRESH_CACHE_TIME) > (MAX_REFRESH_ALLOWED)) \
            u2Value = MAX_REFRESH_ALLOWED;                            \
          else                                                        \
            u2Value += REFRESH_CACHE_TIME;                            \
         }

#define  DVMRP_FILL_FORWARD_TBL_ENTRY(u4ForwTblEntry, u4IfId, u4DestAddr)   \
        {                                                                   \
        pForwTbl->u1Status = DVMRP_ACTIVE;                \
        pForwTbl->u4SrcNetwork =                          \
              ((tRouteTbl *)*(gpRouteTable + u4RouteTblId))->u4SrcNetwork; \
        pForwTbl->u4UpNeighbor =                          \
              ((tRouteTbl *)*(gpRouteTable + u4RouteTblId))->u4UpstreamNbr; \
        pForwTbl->u4GroupAddr = u4DestAddr;               \
        pForwTbl->u1DeliverFlg = 0;                       \
        pForwTbl->u2SrcCnt = 0;                           \
        pForwTbl->InIface.u4InInterface = u4IfId ;        \
        pForwTbl->InIface.u1State = DVMRP_NORMAL;               \
        pForwTbl->InIface.u1Flag  = DVMRP_NORMAL;               \
        pForwTbl->InIface.u2SentPruneTime = 0;               \
        pForwTbl->InIface.u4GraftRetransTimerVal = 0;     \
        pForwTbl->InIface.u4PruneRetransTimerVal = 0;     \
        pForwTbl->pOutInterface = NULL;                   \
        pForwTbl->i4ExpiryTime = REFRESH_CACHE_TIME;      \
        OsixGetSysTime (&(pForwTbl->u4UpTime));                       \
        }

#define DVMRP_FORW_CHECK_INTERFACE(pTmpList, pOutList, u4IfId)      \
    {                                                               \
      while (pTmpList != NULL) {                                    \
        if (pTmpList->u4OutInterface == u4IfId)                     \
            break;                                                  \
        pOutList = pTmpList;                                        \
        pTmpList = pTmpList->pNext;                                 \
      }                                                             \
    }

#define DVMRP_FORW_ALLOC_NEW_INTERFACE(pTmp, u4IfId)                \
    {                                                               \
      pTmp = (tOutputInterface *)DvmrpUtilGetFreeIface();           \
      pOutList = pTmp;                                              \
      if (pOutList != NULL) {                                       \
         pOutList->u4OutInterface = u4IfId;                         \
         pOutList->u4MinTime = 0;                                   \
         OsixGetSysTime (&(pOutList->u4UpTime));                    \
         pOutList->pNext = NULL;                                    \
     }                                                              \
   }

#define DVMRP_FORW_ALLOC_NEW_NEIGHBOR(pNextNbr)                     \
     {                                                              \
         pNextNbr = (tNbrPruneList *)DvmrpUtilGetNbr();             \
         if (pNextNbr != NULL) {                                    \
            pNextNbr->u4NbrAddr = u4NbrAddr;                        \
            pNextNbr->u4PruneLifeTime = 0;                          \
         }                                                          \
     }
#define DVMRP_CHECK_DEPENDENT_NBR(pOutList, u4NbrAddr, u1NbrFlg)        \
{                                                                       \
        tNbrPruneList *pChkNbr = NULL;                                  \
    	pChkNbr = pOutList->pDepNbrs;                                   \
	while (pChkNbr != NULL)                                         \
	{                                                               \
        if (pChkNbr->u4NbrAddr == u4NbrAddr)                            \
        {                                                               \
             u1NbrFlg = DVMRP_OK;                                       \
             break;                                                     \
        }                                                               \
        else                                                            \
        {                                                               \
            pChkNbr = pChkNbr->pNbr;                                    \
        }                                                               \
	}                                                               \
}
/* The following 2 macros have not been used so far */
#define DVMRP_FORW_DELETE_ALL_NEIGHBORS(pTmpNbr, pNextNbr)          \
     {                                                              \
           while (pTmpNbr) {                                        \
              pNextNbr = pTmpNbr->pNbr;                             \
              pTmpNbr->pNbr = NULL;                                 \
              DvmrpUtilReleaseNbr((tNbrPruneList *)pTmpNbr);        \
              pTmpNbr = pNextNbr;                                   \
           }                                                        \
     }
#define DVMRP_FORW_DELETE_INTERFACE(pTmpList, pOutList)             \
     {                                                              \
           pOutList = pTmpList->pNext;                              \
           pTmpList->pNext = NULL;                                  \
           pTmpNbr = pTmpList->pDepNbrs;                            \
           DVMRP_FORW_DELETE_ALL_NEIGHBORS(pTmpNbr, pNextNbr);            \
           DvmrpUtilReleaseIface ((tIfaceFreePool *)pTmpList);      \
     }

#define DVMRP_FIND_INTERFACE_PRUNETIME(pOutList,u4PruneMinTime)     \
    {                                                               \
   while (pOutList != NULL) {                                       \
      if (pOutList->u4MinTime == 0) {                               \
            u4PruneMinTime = 0;                                     \
            break;                                                  \
      }                                                             \
      else {                                                        \
         if (pOutList->u4MinTime < u4PruneMinTime)                  \
            u4PruneMinTime = pOutList->u4MinTime;                   \
      }                                                             \
   pOutList = pOutList->pNext;                                      \
   }                                                                \
    }

#define  DVMRP_FIND_NEIGHBOR_PRUNETIME(pDepNbrs, u4PruneMinTime)    \
     {                                                              \
         for (pNbr=pDepNbrs;pNbr;pNbr= pNbr->pNbr) {                \
            if (pNbr->u4PruneLifeTime == 0) {                       \
               u4PruneMinTime = 0;                                  \
               break;                                               \
            }                                                       \
            else                                                    \
               if (pNbr->u4PruneLifeTime < u4PruneMinTime)          \
                  u4PruneMinTime = pNbr->u4PruneLifeTime;           \
         }                                                          \
     }

#define DVMRP_SET_NEIGHBOR_PRUNETIME(pNbr,u4NbrAddr, u4Value, u1Module)       \
     {                                                                        \
     for (pTmpNbr=pNbr;pTmpNbr;pTmpNbr=pTmpNbr->pNbr) {                       \
        if (pTmpNbr->u4NbrAddr == u4NbrAddr)    {                             \
           if (pTmpNbr->u4PruneLifeTime != u4Value) {                         \
               pTmpNbr->u4PruneLifeTime = u4Value;                            \
               LOG2((INFO, u1Module, "Value of Prunelifetime field changed",  \
                     pTmpNbr->u4PruneLifeTime));                              \
           }                                                                  \
           else {                                                             \
              if (u4Value == 0) {                                             \
                 LOG1((INFO, u1Module, "Unexpected Downstream Neighbor "));   \
                 return;                                                      \
              }                                                               \
           }                                                                  \
           break;                                                             \
         }                                                                    \
     }                                                                        \
     if (pTmpNbr == NULL) {                                                   \
         LOG1((INFO, u1Module, "Invalid Downstream Neighbor"));               \
         return;                                                              \
     }                                                                        \
     }

#define DVMRP_UPDATE_UPSTREAM_NBR(pRouteTbl)                                 \
{                                                                            \
  UINT4      u4PrevIndex = 0;                                                \
  tForwTbl  *pFwdTbl = NULL;                                                 \
  for (u4Index = 0; u4Index < MAX_FORW_TABLE_ENTRIES; u4Index++)   {         \
    pFwdTbl = DVMRP_GET_FWDNODE_FROM_INDEX (u4Index);                        \
    if ((pFwdTbl->u1Status == DVMRP_ACTIVE) &&                   \
        (pRouteTbl->u4SrcNetwork == pFwdTbl->u4SrcNetwork)) {    \
       pFwdTbl->u4UpNeighbor = pRouteTbl->u4UpstreamNbr;         \
       u4PrevIndex = pFwdTbl->InIface.u4InInterface;             \
       pFwdTbl->InIface.u4InInterface = pRouteTbl->u4IfaceIndex; \
       DvmrpMfwdUpdateIif (pFwdTbl, 0, u4PrevIndex);             \
    }                                                                        \
  }                                                                          \
}

#define DVMRP_SEARCH_FOR_HOST(u4Group, u4IfId,u1Flag)                    \
{                                                                        \
  tHostMbrTbl  *pHostTbl = NULL;                                         \
  for (u4Index=0; u4Index < MAX_HOST_MBR_TABLE_ENTRIES; u4Index++) {     \
      pHostTbl = DVMRP_GET_HOSTNODE_FROM_INDEX (u4Index);               \
      if ((pHostTbl->u1Status == DVMRP_ACTIVE) &&         \
           (pHostTbl->u4DestGroup == u4Group) &&           \
           (pHostTbl->u4LocInterface == u4IfId))          \
      {\
              u1Flag = DVMRP_TRUE;                                       \
              break;                                                     \
      }\
  }                                                                       \
}

#endif
