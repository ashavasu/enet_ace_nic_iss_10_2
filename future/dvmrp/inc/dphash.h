/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dphash.h,v 1.3 2007/02/01 14:46:55 iss Exp $
 *
 * Description:This file contains hashing related definitions 
 *             macros and function prototypes                 
 *
 *******************************************************************/
#ifndef   _DPHASH_H
#define   _DPHASH_H

typedef tTMO_HASH_TABLE  tDVMRP_HASH_TABLE;
typedef tTMO_HASH_NODE   tDVMRP_HASH_NODE;

#define   DVMRP_HASH_ADD            TMO_HASH_ADD_NODE     
#define   DVMRP_HASH_DELETE         TMO_HASH_DELETE_NODE  

#define   DVMRP_HASH_ADD_NODE       TMO_HASH_Add_Node     
#define   DVMRP_HASH_DELETE_NODE    TMO_HASH_Delete_Node  

#define   DVMRP_HASH_CREATE_TABLE   TMO_HASH_Create_Table 
#define   DVMRP_HASH_DELETE_TABLE   TMO_HASH_Delete_Table 

#define   DVMRP_HASH_SCAN_TABLE     TMO_HASH_Scan_Table   
#define   DVMRP_HASH_SCAN_BUCKET    TMO_HASH_Scan_Bucket  

#endif /* _DPHASH_H */
