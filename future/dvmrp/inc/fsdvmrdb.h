/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdvmrdb.h,v 1.6 2009/02/26 16:11:44 nswamy-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDVMRDB_H
#define _FSDVMRDB_H

UINT1 DvmrpInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 DvmrpNeighborTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DvmrpRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DvmrpRouteNextHopTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 DvmrpForwardTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DvmrpForwardPruneNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DvmrpIpMRTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 DvmrpIpMNextHopTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsdvmr [] ={1,3,6,1,4,1,2076,60};
tSNMP_OID_TYPE fsdvmrOID = {8, fsdvmr};


UINT4 DvmrpVersionString [ ] ={1,3,6,1,4,1,2076,60,1,1};
UINT4 DvmrpGenerationId [ ] ={1,3,6,1,4,1,2076,60,1,2};
UINT4 DvmrpNumRoutes [ ] ={1,3,6,1,4,1,2076,60,1,3};
UINT4 DvmrpReachableRoutes [ ] ={1,3,6,1,4,1,2076,60,1,4};
UINT4 DvmrpStatus [ ] ={1,3,6,1,4,1,2076,60,1,5};
UINT4 DvmrpLogEnabled [ ] ={1,3,6,1,4,1,2076,60,1,6};
UINT4 DvmrpLogMask [ ] ={1,3,6,1,4,1,2076,60,1,7};
UINT4 DvmrpPruneLifeTime [ ] ={1,3,6,1,4,1,2076,60,1,8};
UINT4 DvmrpInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,9,1,1};
UINT4 DvmrpInterfaceStatus [ ] ={1,3,6,1,4,1,2076,60,2,9,1,2};
UINT4 DvmrpInterfaceLocalAddress [ ] ={1,3,6,1,4,1,2076,60,2,9,1,3};
UINT4 DvmrpInterfaceMetric [ ] ={1,3,6,1,4,1,2076,60,2,9,1,4};
UINT4 DvmrpInterfaceRcvBadPkts [ ] ={1,3,6,1,4,1,2076,60,2,9,1,5};
UINT4 DvmrpInterfaceRcvBadRoutes [ ] ={1,3,6,1,4,1,2076,60,2,9,1,6};
UINT4 DvmrpInterfaceTtl [ ] ={1,3,6,1,4,1,2076,60,2,9,1,7};
UINT4 DvmrpInterfaceProtocol [ ] ={1,3,6,1,4,1,2076,60,2,9,1,8};
UINT4 DvmrpInterfaceRateLimit [ ] ={1,3,6,1,4,1,2076,60,2,9,1,9};
UINT4 DvmrpInterfaceInMcastOctets [ ] ={1,3,6,1,4,1,2076,60,2,9,1,10};
UINT4 DvmrpInterfaceOutMcastOctets [ ] ={1,3,6,1,4,1,2076,60,2,9,1,11};
UINT4 DvmrpInterfaceHCInMcastOctets [ ] ={1,3,6,1,4,1,2076,60,2,9,1,12};
UINT4 DvmrpInterfaceHCOutMcastOctets [ ] ={1,3,6,1,4,1,2076,60,2,9,1,13};
UINT4 DvmrpNeighborIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,10,1,1};
UINT4 DvmrpNeighborAddress [ ] ={1,3,6,1,4,1,2076,60,2,10,1,2};
UINT4 DvmrpNeighborUpTime [ ] ={1,3,6,1,4,1,2076,60,2,10,1,3};
UINT4 DvmrpNeighborExpiryTime [ ] ={1,3,6,1,4,1,2076,60,2,10,1,4};
UINT4 DvmrpNeighborGenerationId [ ] ={1,3,6,1,4,1,2076,60,2,10,1,5};
UINT4 DvmrpNeighborMajorVersion [ ] ={1,3,6,1,4,1,2076,60,2,10,1,6};
UINT4 DvmrpNeighborMinorVersion [ ] ={1,3,6,1,4,1,2076,60,2,10,1,7};
UINT4 DvmrpNeighborCapabilities [ ] ={1,3,6,1,4,1,2076,60,2,10,1,8};
UINT4 DvmrpNeighborRcvRoutes [ ] ={1,3,6,1,4,1,2076,60,2,10,1,9};
UINT4 DvmrpNeighborRcvBadPkts [ ] ={1,3,6,1,4,1,2076,60,2,10,1,10};
UINT4 DvmrpNeighborRcvBadRoutes [ ] ={1,3,6,1,4,1,2076,60,2,10,1,11};
UINT4 DvmrpNeighborAdjFlag [ ] ={1,3,6,1,4,1,2076,60,2,10,1,12};
UINT4 DvmrpRouteSource [ ] ={1,3,6,1,4,1,2076,60,2,11,1,1};
UINT4 DvmrpRouteSourceMask [ ] ={1,3,6,1,4,1,2076,60,2,11,1,2};
UINT4 DvmrpRouteUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,60,2,11,1,3};
UINT4 DvmrpRouteIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,11,1,4};
UINT4 DvmrpRouteMetric [ ] ={1,3,6,1,4,1,2076,60,2,11,1,5};
UINT4 DvmrpRouteExpiryTime [ ] ={1,3,6,1,4,1,2076,60,2,11,1,6};
UINT4 DvmrpRouteUpTime [ ] ={1,3,6,1,4,1,2076,60,2,11,1,7};
UINT4 DvmrpRouteStatus [ ] ={1,3,6,1,4,1,2076,60,2,11,1,8};
UINT4 DvmrpRouteNextHopSource [ ] ={1,3,6,1,4,1,2076,60,2,12,1,1};
UINT4 DvmrpRouteNextHopSourceMask [ ] ={1,3,6,1,4,1,2076,60,2,12,1,2};
UINT4 DvmrpRouteNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,12,1,3};
UINT4 DvmrpRouteNextHopType [ ] ={1,3,6,1,4,1,2076,60,2,12,1,4};
UINT4 DvmrpRouteNextHopDesigForw [ ] ={1,3,6,1,4,1,2076,60,2,12,1,5};
UINT4 DvmrpRouteNextHopDepNbrs [ ] ={1,3,6,1,4,1,2076,60,2,12,1,6};
UINT4 DvmrpSourceNetwork [ ] ={1,3,6,1,4,1,2076,60,2,13,1,1};
UINT4 DvmrpGroupAddress [ ] ={1,3,6,1,4,1,2076,60,2,13,1,2};
UINT4 DvmrpForwardUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,60,2,13,1,3};
UINT4 DvmrpForwardInIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,13,1,4};
UINT4 DvmrpForwardInIfState [ ] ={1,3,6,1,4,1,2076,60,2,13,1,5};
UINT4 DvmrpForwardExpiryTime [ ] ={1,3,6,1,4,1,2076,60,2,13,1,6};
UINT4 DvmrpForwardTblStatus [ ] ={1,3,6,1,4,1,2076,60,2,13,1,7};
UINT4 DvmrpForwardSourceNetwork [ ] ={1,3,6,1,4,1,2076,60,2,14,1,1};
UINT4 DvmrpForwardGroupAddress [ ] ={1,3,6,1,4,1,2076,60,2,14,1,2};
UINT4 DvmrpForwardIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,14,1,3};
UINT4 DvmrpForwardPruneNeighbor [ ] ={1,3,6,1,4,1,2076,60,2,14,1,4};
UINT4 DvmrpForwardNbrPruneTime [ ] ={1,3,6,1,4,1,2076,60,2,14,1,5};
UINT4 DvmrpIpMRGroup [ ] ={1,3,6,1,4,1,2076,60,2,15,1,1};
UINT4 DvmrpIpMRSource [ ] ={1,3,6,1,4,1,2076,60,2,15,1,2};
UINT4 DvmrpIpMRSourceMask [ ] ={1,3,6,1,4,1,2076,60,2,15,1,3};
UINT4 DvmrpIpMRUpstreamNeighbor [ ] ={1,3,6,1,4,1,2076,60,2,15,1,4};
UINT4 DvmrpIpMRInIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,15,1,5};
UINT4 DvmrpIpMRUpTime [ ] ={1,3,6,1,4,1,2076,60,2,15,1,6};
UINT4 DvmrpIpMRExpiryTime [ ] ={1,3,6,1,4,1,2076,60,2,15,1,7};
UINT4 DvmrpIpMRPkts [ ] ={1,3,6,1,4,1,2076,60,2,15,1,8};
UINT4 DvmrpIpMRDifferentInIfPackets [ ] ={1,3,6,1,4,1,2076,60,2,15,1,9};
UINT4 DvmrpIpMROctets [ ] ={1,3,6,1,4,1,2076,60,2,15,1,10};
UINT4 DvmrpIpMRProtocol [ ] ={1,3,6,1,4,1,2076,60,2,15,1,11};
UINT4 DvmrpIpMRRtProto [ ] ={1,3,6,1,4,1,2076,60,2,15,1,12};
UINT4 DvmrpIpMRRtAddress [ ] ={1,3,6,1,4,1,2076,60,2,15,1,13};
UINT4 DvmrpIpMRRtMask [ ] ={1,3,6,1,4,1,2076,60,2,15,1,14};
UINT4 DvmrpIpMRRtType [ ] ={1,3,6,1,4,1,2076,60,2,15,1,15};
UINT4 DvmrpIpMRHCOctets [ ] ={1,3,6,1,4,1,2076,60,2,15,1,16};
UINT4 DvmrpIpMNextHopGroup [ ] ={1,3,6,1,4,1,2076,60,2,16,1,1};
UINT4 DvmrpIpMNextHopSource [ ] ={1,3,6,1,4,1,2076,60,2,16,1,2};
UINT4 DvmrpIpMNextHopSourceMask [ ] ={1,3,6,1,4,1,2076,60,2,16,1,3};
UINT4 DvmrpIpMNextHopIfIndex [ ] ={1,3,6,1,4,1,2076,60,2,16,1,4};
UINT4 DvmrpIpMNextHopAddress [ ] ={1,3,6,1,4,1,2076,60,2,16,1,5};
UINT4 DvmrpIpMNextHopState [ ] ={1,3,6,1,4,1,2076,60,2,16,1,6};
UINT4 DvmrpIpMNextHopUpTime [ ] ={1,3,6,1,4,1,2076,60,2,16,1,7};
UINT4 DvmrpIpMNextHopExpiryTime [ ] ={1,3,6,1,4,1,2076,60,2,16,1,8};
UINT4 DvmrpIpMNextHopProtocol [ ] ={1,3,6,1,4,1,2076,60,2,16,1,9};
UINT4 DvmrpIpMNextHopPkts [ ] ={1,3,6,1,4,1,2076,60,2,16,1,10};


tMbDbEntry fsdvmrMibEntry[]= {

{{10,DvmrpVersionString}, NULL, DvmrpVersionStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DvmrpGenerationId}, NULL, DvmrpGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DvmrpNumRoutes}, NULL, DvmrpNumRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DvmrpReachableRoutes}, NULL, DvmrpReachableRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,DvmrpStatus}, NULL, DvmrpStatusGet, DvmrpStatusSet, DvmrpStatusTest, DvmrpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DvmrpLogEnabled}, NULL, DvmrpLogEnabledGet, DvmrpLogEnabledSet, DvmrpLogEnabledTest, DvmrpLogEnabledDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DvmrpLogMask}, NULL, DvmrpLogMaskGet, DvmrpLogMaskSet, DvmrpLogMaskTest, DvmrpLogMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,DvmrpPruneLifeTime}, NULL, DvmrpPruneLifeTimeGet, DvmrpPruneLifeTimeSet, DvmrpPruneLifeTimeTest, DvmrpPruneLifeTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{12,DvmrpInterfaceIfIndex}, GetNextIndexDvmrpInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceStatus}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceStatusGet, DvmrpInterfaceStatusSet, DvmrpInterfaceStatusTest, DvmrpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DvmrpInterfaceTableINDEX, 1, 0, 1, NULL},

{{12,DvmrpInterfaceLocalAddress}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceLocalAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceMetric}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, "1"},

{{12,DvmrpInterfaceRcvBadPkts}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceRcvBadPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceRcvBadRoutes}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceTtl}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceTtlGet, DvmrpInterfaceTtlSet, DvmrpInterfaceTtlTest, DvmrpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceProtocol}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceRateLimit}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceRateLimitGet, DvmrpInterfaceRateLimitSet, DvmrpInterfaceRateLimitTest, DvmrpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DvmrpInterfaceTableINDEX, 1, 0, 0, "0"},

{{12,DvmrpInterfaceInMcastOctets}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceOutMcastOctets}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceHCInMcastOctets}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceHCInMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpInterfaceHCOutMcastOctets}, GetNextIndexDvmrpInterfaceTable, DvmrpInterfaceHCOutMcastOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DvmrpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborIfIndex}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborAddress}, GetNextIndexDvmrpNeighborTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborUpTime}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborExpiryTime}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborGenerationId}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborGenerationIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborMajorVersion}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborMajorVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborMinorVersion}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborMinorVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborCapabilities}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborRcvRoutes}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborRcvRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborRcvBadPkts}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborRcvBadPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborRcvBadRoutes}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpNeighborAdjFlag}, GetNextIndexDvmrpNeighborTable, DvmrpNeighborAdjFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpNeighborTableINDEX, 1, 0, 0, NULL},

{{12,DvmrpRouteSource}, GetNextIndexDvmrpRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteSourceMask}, GetNextIndexDvmrpRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteUpstreamNeighbor}, GetNextIndexDvmrpRouteTable, DvmrpRouteUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteIfIndex}, GetNextIndexDvmrpRouteTable, DvmrpRouteIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteMetric}, GetNextIndexDvmrpRouteTable, DvmrpRouteMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteExpiryTime}, GetNextIndexDvmrpRouteTable, DvmrpRouteExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteUpTime}, GetNextIndexDvmrpRouteTable, DvmrpRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteStatus}, GetNextIndexDvmrpRouteTable, DvmrpRouteStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DvmrpRouteTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpRouteNextHopSource}, GetNextIndexDvmrpRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpRouteNextHopSourceMask}, GetNextIndexDvmrpRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpRouteNextHopIfIndex}, GetNextIndexDvmrpRouteNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpRouteNextHopType}, GetNextIndexDvmrpRouteNextHopTable, DvmrpRouteNextHopTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpRouteNextHopDesigForw}, GetNextIndexDvmrpRouteNextHopTable, DvmrpRouteNextHopDesigForwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpRouteNextHopDepNbrs}, GetNextIndexDvmrpRouteNextHopTable, DvmrpRouteNextHopDepNbrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DvmrpRouteNextHopTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpSourceNetwork}, GetNextIndexDvmrpForwardTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpGroupAddress}, GetNextIndexDvmrpForwardTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardUpstreamNeighbor}, GetNextIndexDvmrpForwardTable, DvmrpForwardUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardInIfIndex}, GetNextIndexDvmrpForwardTable, DvmrpForwardInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardInIfState}, GetNextIndexDvmrpForwardTable, DvmrpForwardInIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardExpiryTime}, GetNextIndexDvmrpForwardTable, DvmrpForwardExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardTblStatus}, GetNextIndexDvmrpForwardTable, DvmrpForwardTblStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpForwardTableINDEX, 2, 0, 0, NULL},

{{12,DvmrpForwardSourceNetwork}, GetNextIndexDvmrpForwardPruneNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpForwardPruneNbrTableINDEX, 4, 0, 0, NULL},

{{12,DvmrpForwardGroupAddress}, GetNextIndexDvmrpForwardPruneNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpForwardPruneNbrTableINDEX, 4, 0, 0, NULL},

{{12,DvmrpForwardIfIndex}, GetNextIndexDvmrpForwardPruneNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DvmrpForwardPruneNbrTableINDEX, 4, 0, 0, NULL},

{{12,DvmrpForwardPruneNeighbor}, GetNextIndexDvmrpForwardPruneNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpForwardPruneNbrTableINDEX, 4, 0, 0, NULL},

{{12,DvmrpForwardNbrPruneTime}, GetNextIndexDvmrpForwardPruneNbrTable, DvmrpForwardNbrPruneTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpForwardPruneNbrTableINDEX, 4, 0, 0, NULL},

{{12,DvmrpIpMRGroup}, GetNextIndexDvmrpIpMRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRSource}, GetNextIndexDvmrpIpMRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRSourceMask}, GetNextIndexDvmrpIpMRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRUpstreamNeighbor}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRUpstreamNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRInIfIndex}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRInIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRUpTime}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRExpiryTime}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRPkts}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRDifferentInIfPackets}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRDifferentInIfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMROctets}, GetNextIndexDvmrpIpMRTable, DvmrpIpMROctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRProtocol}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRRtProto}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRRtProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRRtAddress}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRRtAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRRtMask}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRRtMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRRtType}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRRtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMRHCOctets}, GetNextIndexDvmrpIpMRTable, DvmrpIpMRHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DvmrpIpMRTableINDEX, 3, 0, 0, NULL},

{{12,DvmrpIpMNextHopGroup}, GetNextIndexDvmrpIpMNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopSource}, GetNextIndexDvmrpIpMNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopSourceMask}, GetNextIndexDvmrpIpMNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopIfIndex}, GetNextIndexDvmrpIpMNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopAddress}, GetNextIndexDvmrpIpMNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopState}, GetNextIndexDvmrpIpMNextHopTable, DvmrpIpMNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopUpTime}, GetNextIndexDvmrpIpMNextHopTable, DvmrpIpMNextHopUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopExpiryTime}, GetNextIndexDvmrpIpMNextHopTable, DvmrpIpMNextHopExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopProtocol}, GetNextIndexDvmrpIpMNextHopTable, DvmrpIpMNextHopProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},

{{12,DvmrpIpMNextHopPkts}, GetNextIndexDvmrpIpMNextHopTable, DvmrpIpMNextHopPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DvmrpIpMNextHopTableINDEX, 5, 0, 0, NULL},
};
tMibData fsdvmrEntry = { 85, fsdvmrMibEntry };
#endif /* _FSDVMRDB_H */

