/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dproute.h,v 1.4 2007/04/02 06:38:57 iss Exp $
 *
 * Description:This file contains all the definitions related 
 *             to the Route handler module                    
 *
 *******************************************************************/
#ifndef _DPROUTE_H_
#define _DPROUTE_H_

#define   NEW_METRIC_GR_EXISTING      0x04
#define   NEW_METRIC_LESS_EXISTING    0x05
#define   NEW_METRIC_EQL_EXISTING     0x06

#define   SIZEOF_METRIC                  1
#define   SIZEOF_MASK                    3
#define   SEVEN_BITS_SET              0x7f
#define   EIGHTH_BIT_SET              0x80
#define   SIZE_OF_EACH_ROUTE             8

#define DVMRP_MIN_MTU    (gu4MinMTU - MAX_IGMP_HDR_LEN - MIN_IP_HDR_LEN)

#define LOOP_THROUGH_IFACES {                                                \
   u1Flag = DVMRP_TRUE;                                                      \
    pCurSllNode = TMO_SLL_First (&gDvmrpIfInfo.IfGetNextList);               \
    while (pCurSllNode != NULL)                                              \
    {                                                                        \
        pInterfaceTbl = DVMRP_GET_BASE_PTR (tDvmrpInterfaceNode,             \
                                IfGetNextLink, pCurSllNode);                 \
       u4IfaceIndex = pInterfaceTbl->u4IfIndex;                              \
       if ((DVMRP_CHECK_IF_STATUS (pInterfaceTbl)) &&                        \
                              pInterfaceTbl->pNbrOnthisIface != NULL) {      \
           u4Dummy = DvmrpRrfChunkRouteReport(u4IfaceIndex,                  \
                                  (UINT4) ALL_DVMRP_ROUTERS, u1Flag);        \
           if ( u1Flag == DVMRP_TRUE ) {                                     \
               u4TmpStartIndex = u4Dummy;                                    \
           }                                                                 \
           u1Flag = DVMRP_FALSE;                                             \
       } else {                                                              \
                LOG1((WARNING, RRF,"Interface is either DOWN, or no Nbrs."));\
                LOG2((WARNING, RRF,"So cant send report",u4IfaceIndex));     \
         }                                                                   \
       pCurSllNode = TMO_SLL_Next (&gDvmrpIfInfo.IfGetNextList,             \
                                   &(pInterfaceTbl->IfGetNextLink));         \
    }                                                                         \
    gu4LastSentRouteIndex += u4TmpStartIndex;                                 \
}

#define DVMRP_ROUTE_DELETE_NEIGHBOR(pNextHopTbl, u4IfIndex,                    \
                                    u4NbrAddr, pTmpNbr,                        \
                                    pNbrAddrList, u1NbrFound, u1AllLeaf) {     \
           u1AllLeaf = DVMRP_FALSE;                                            \
           pNextHopType = pNextHopTbl->pNextHopType;                           \
           while (pNextHopType) {                                              \
              if (pNextHopType->u4NHopInterface == u4IfIndex) {                \
                  pTmpNbr = pNbrAddrList = pNextHopType->pDependentNbrs;       \
                  LOG1((INFO, RTH, "Interface found"));                        \
                  while (pNbrAddrList) {                                       \
                      if (pNbrAddrList->u4NbrAddr == u4NbrAddr) {         \
                         LOG1((INFO, RTH, "Neighbor found"));                  \
                         if (pNbrAddrList == pNextHopType->pDependentNbrs) {   \
                             pNextHopType->pDependentNbrs = pNbrAddrList->pNbr;\
                         }                                                     \
                         else  {                                               \
                            pTmpNbr->pNbr = pNbrAddrList->pNbr;                \
                         }                                                     \
                         pNbrAddrList->pNbr = NULL;                            \
                         DvmrpUtilReleaseNbr((tNbrList *)pNbrAddrList);        \
                         LOG1((INFO, RTH, "Neighbor removed"));                \
                         u1NbrFound = DVMRP_TRUE;                              \
                         break;                                                \
                      }                                                        \
                      pTmpNbr = pNbrAddrList;                                  \
                      pNbrAddrList = pNbrAddrList->pNbr;                       \
                  }                                                            \
                  if (pNextHopType->pDependentNbrs == NULL) {                  \
                     pNextHopType->u1InType = LEAF;                            \
                  }                                                            \
                  break;                                                       \
              }                                                                \
              else {                                                           \
                 if ((u1AllLeaf == DVMRP_FALSE) &&                             \
                    (pNextHopType->u1InType == DVMRP_BRANCH)) {                \
                     u1AllLeaf = DVMRP_TRUE;                                   \
                 }                                                             \
                 pNextHopType = pNextHopType->pNextHopTypeNext;                \
              }                                                                \
           }                                                                   \
           while (pNextHopType) {                                              \
              if (pNextHopType->u1InType == DVMRP_BRANCH) {                    \
                  u1AllLeaf = DVMRP_TRUE;                                      \
              }                                                                \
              pNextHopType = pNextHopType->pNextHopTypeNext;                   \
           }                                                                   \
           if (u1AllLeaf == DVMRP_FALSE) {                                     \
               pNextHopTbl->u1Status = DVMRP_INACTIVE;                         \
               pNextHopType = pNextHopTbl->pNextHopType;                       \
               DvmrpUtilReleaseIface((tIfaceList *)pNextHopTbl->pNextHopType); \
               LOG1((INFO, RTH, "All interfaces released."));                  \
               pNextHopTbl->pNextHopType = NULL;                               \
               LOG1((INFO, RTH, "entry for this Source in the NextHop,         \
               Deleated, and interfaces released to free pool.."));            \
           }                                                                   \
}

/*
 * Added by Rajagopalan 19/5
 */
#define NEAREST_DIVISOR(u1n) {                         \
   if (u1n == 0)                                       \
      u1n = 1;                                         \
   else if (u1n > DVMRP_ROUTE_REPORT_INTERVAL)         \
      u1n = DVMRP_ROUTE_REPORT_INTERVAL;               \
   while ((DVMRP_ROUTE_REPORT_INTERVAL % u1n) != 0) {  \
       u1n++;                                          \
   }                                                   \
}

#define DVMRP_MAX_VALUE(u1x, u2y, u1Num)  \
   u1Num = (u1x >= u2y) ? 1 : 0

#endif
