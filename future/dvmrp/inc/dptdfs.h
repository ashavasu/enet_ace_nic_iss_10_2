/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dptdfs.h,v 1.10 2011/12/29 12:46:24 siva Exp $
 *
 * Description: This file contains all the type definitions
 *
 *******************************************************************/
#ifndef _DPTDFS_H
#define _DPTDFS_H

#define  DVMRP_MAX_SEMNAME_LEN         8
typedef struct DpSemDesc {
UINT1      au1SemName[DVMRP_MAX_SEMNAME_LEN];
tOsixSemId semId;
} tDpSemDesc;

typedef struct RouteTable
{
    tTMO_SLL_NODE RouteEntryNext;    /*
                                           * This ptr will be either   
                                           * pointing to a free entry  
                                           * in the route tbl or it can
                                           * hold a route entry in SLL.
                                           * As #ing for a given key   
                                           * may return ptr to more    
                                           * than one entry in route   
                                           * tbl we need to have this  
                                           * SLL.                      
                                         */
    UINT4      u4SrcNetwork;   /* Source N/w of the Route */
    UINT4      u4SrcMask;      /* Subnet Mask of the Source N/w */
    UINT4      u4UpstreamNbr;  /* Previous Hop back to the Source N/w */
    UINT4      u4IfaceIndex;   /* Interface to reach Previous Hop */
    UINT4      u4UpTime;       /* Route Up Time */
    INT4       i4ExpiryTime;   /* Time till this Route is valid */
    UINT1      u1Metric;       /* Metric to reach the Source N/w */
    UINT1      u1Status;       /* State of Route - Active */
    UINT2      u2Reserved;     /* For word alignment      */
} tRouteTbl; 

typedef struct DepNbrs
{
    struct DepNbrs *pNbr;
    UINT4     u4NbrAddr;
} tNbrAddrList;

typedef struct NbrPruneList
{
    struct NbrPruneList *pNbr;
    UINT4     u4NbrAddr;
    UINT4     u4PruneLifeTime;
}
tNbrPruneList;

typedef struct InputInterface
{
    UINT4     u4InInterface;    /* Interface id                       */
    UINT1     u1State;          /* Can be set to DVMRP_NORMAL/PRUNED/       */ 
                                /* GRAFTED_WAITING_ACK/LOCAL_NETWORK  */
    UINT1     u1Flag;           /* Can be set to DATA_RECEIVED_ON_    */
                                /* PRUNE_IFACE/GRAFT_ACK_RECEIVED     */
    UINT2     u2SentPruneTime; /* Prune Time Sent upstream            */
    
    UINT4     u4GraftRetransTimerVal;    /* If > 0 then GraftRetrans timer is */
                                         /*  running  */
    UINT4     u4PruneRetransTimerVal;    /* If > 0 then PruneRetrans timer is */
                                         /* running   */
} tInputInterface;

typedef struct OutputInterface
{
    struct OutputInterface *pNext;
    tNbrPruneList  *pDepNbrs;       /* List of dependent neighbors */
    UINT4          u4OutInterface;  /* Interface id */
    UINT4          u4MinTime;
    UINT4          u4UpTime;
} tOutputInterface;

typedef struct DvmrpTimer
{
    tTmrAppTimer AppTmr; 
    UINT1  u1TimerType;
    UINT1  u1Reserved;
    UINT2  u2Reserved;
} tDvmrpTimer;

typedef struct _DvmrpActiveSrcInfo {
 struct _DvmrpActiveSrcInfo *pNext;
 UINT4  u4SrcAddr;
 UINT4  u4SrcMask;
}tDpActiveSrcInfo; 

typedef struct ForwTable
{
    UINT4            u4SrcNetwork;  /* Source Network Address */
    UINT4            u4GroupAddr;
    UINT4            u4UpNeighbor;  /* Multicast Group */
    tInputInterface  InIface;           /* Next hop (back to source) */
                                        /*  interface information    */
    tOutputInterface *pOutInterface;    /* List of  interfaces to reach the */
                                        /* dependent downstream Neighbors */
    tDpActiveSrcInfo *pSrcInfo;  /* List of Sourcess for that Source
                                  * Network */ 
    INT4          i4ExpiryTime;  /* Age out Time */
    tDvmrpTimer   AppTimer;
    UINT1         u1Status;      /* Entry status Active/Inactive */
    UINT1         u1DeliverFlg;  /* To indicate MFWD regd the deliver flag */
    UINT2         u2SrcCnt;      /* To keep track of No of Active Sources */
    UINT4         u4UpTime;
} tForwTbl;

typedef struct ForwCacheTable
{
    tTMO_SLL_NODE Next;    /* To be used for hashing           */
    UINT4     u4SrcAddress;  /* IP address of the Host */
    UINT4     u4DestGroup;   /* Destination Multicast Group */
    tForwTbl  *pSrcNwInfo;   /* Pointer to entry in Forward Table */
}
tForwCacheTbl;

typedef struct NbrTable
{
    struct NbrTable *pNbrTblNext;
    UINT4  u4NbrIfIndex;       /* Interface to reach the Neighbor */
    UINT4  u4NbrIpAddress;     /* Neighbors IP Address */
    UINT4  u4NbrUpTime;        /* Neighbor Up timer */
    UINT4  u4ExpiryTime;       /* Age Out time for a Neighbor */
    UINT4  u4NbrGenId;         /* Neighbor's Generation ID */
    UINT1  u1NbrMajorVer;      /* Major Version of Neighbor DVMRP */
    UINT1  u1NbrMinorVer;      /* Minor Version of Neighbor DVMRP */
    UINT1  u1NbrCapabilities;  /* Neighbor's Capabilities(flags) */
    UINT1  u1Reserved;
    UINT4  u4NbrRcvBadPkts;    /* Bad packets from Neighbor */
    UINT4  u4NbrRcvBadRoutes;  /* Bad Routes advertised by Neighbor */
    UINT4  u4NbrRcvRoutes;     /* Number of valid routes from Neighbor */
    UINT1  u1Status;           /* Status of this entry  Active/       */
                               /* Inactive                            */
    UINT1  u1SeenByNbrFlag;    /* Will be set TRUE if our IP address  */
                               /* found in Nbr's Probe message        */
    UINT2  u2Reserved;         /* Word alignment                      */
} tNbrTbl;

typedef struct HostMbrTable
{
    UINT4  u4LocInterface;  /* Local Network interface id */
    UINT4  u4DestGroup;     /* Multicast Group address */
    UINT1  u1Status;        /* Entry Status Active/Inactive */
    UINT1  u1PendFlg;
    UINT2  u2Reserved;
} tHostMbrTbl;

typedef struct NextHopType
{
    struct NextHopType *pNextHopTypeNext;
    tNbrAddrList  *pDependentNbrs;  /* List of dependent neighbors */
    UINT4         u4NHopInterface;  /* Next Hop Interface id */
    UINT1         u1InType;         /* Type of interface Leaf/Branch */
    UINT1         u1DesigForwFlag;  /* Designated forwarder or not */
    UINT2         u2Reserved;       /* For word alignment          */
} tNextHopType;

typedef struct NextHopTable
{
    tNextHopType  *pNextHopType;     /* Next Hop Interfaces list */
    UINT4         u4NextHopSrc;      /* Source Network Address */
    UINT4         u4NextHopSrcMask;  /* Subnet mask of the Source network */
    UINT1         u1Status;          /* Status of this entry */
    UINT1         u1Reserved;        /* For word alignment   */
    UINT2         u2Reserved;        /* For word alignment   */

} tNextHopTbl;

typedef struct IfaceFreePool
{
    union Iface
    {
        tOutputInterface OutIface;
        tNextHopType HopType;
    }
    Iface;
    struct IfaceFreePool *pNext;
} tIfaceList;

typedef struct NeighborList
{
    union Nbr
    {
        tNbrAddrList NbrInfo;
        tNbrPruneList NbrPruneInfo;
    }
    NbrInfo;
    struct NeighborList *pNext;
} tNbrList;

typedef struct ForwTablePtr
{
    tForwTbl*  aFwdTbl[MAX_DP_FWD_TBL_ENTRIES];

}tForwTablePtr;

typedef struct ForwCacheTblPtr
{
    tForwCacheTbl*  aFwdCacheTbl[MAX_DP_FWD_CACHE_TBL_ENTRIES];

}tForwCacheTblPtr;

typedef struct HostMbrTblPtr
{
    tHostMbrTbl*  aHostMbr[MAX_DP_HOST_MBR_TBL_ENTRIES];

}tHostMbrTblPtr;

typedef struct NbrTblPtr
{
   tNbrTbl*  aNbrTbl[MAX_DP_NBR_TBL_ENTRIES];

}tNbrTblPtr;

typedef struct RouteTblPtr
{
    tRouteTbl*  aRouteTbl[MAX_DP_ROUTE_TBL_ENTRIES];

}tRouteTblPtr;

typedef struct NextHopTblPtr
{
    tNextHopTbl*  aNextHopTbl[MAX_DP_NEXT_HOP_TBL_ENTRIES]; 
 
}tNextHopTblPtr;

typedef struct IfaceListPtr
{
    tIfaceList*  aIfaceListTbl[MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES];

}tIfaceListPtr;

typedef struct NbrListPtr
{
    tNbrList*  aNbrListTbl[MAX_DP_NBR_FREE_POOL_TBL_ENTRIES];

}tNbrListPtr; 

typedef struct DvmrpHdr
{
    UINT1  u1Type;
    UINT1  u1Code;
    UINT2  u2CheckSum;
    UINT2  u2Reserved;  /* will be filled for Probe Pkts alone - 21 sept */
    UINT1  u1MinorVer;
    UINT1  u1MajorVer;
} tDvmrpHeader;

typedef struct IgmpHost
{
    UINT4         u4IfIndex;
    UINT4         u4DestGroup;
    UINT1         u1Flag;
    UINT1         u1Reserved;
    UINT2         u2Reserved;
}
tIgmpHost;

typedef struct DvmrpIfStatus 
{
    UINT4  u4IfIndex;
    UINT4  u4Addr;
    UINT4  u4Mtu;
    UINT4  u4Admin;
    UINT4  u4Oper;
    UINT4  u4NetMask;
    UINT4  u4IfBitMap;
} tDvmrpIfStatus;

typedef struct DvmrpGlobalMemPoolId
{
    tMemPoolId  ForwTblPoolId;
    tMemPoolId  ForwCacheTblPoolId;
    tMemPoolId  HostMbrTblPoolId;
    tMemPoolId  NbrTblPoolId;
    tMemPoolId  RouteTblPoolId;
    tMemPoolId  NextHopTblPoolId;
    tMemPoolId  IfaceTblPoolId;
    tMemPoolId  NbrListPoolId;
    tMemPoolId  IfaceListPoolId;
    tMemPoolId  SrcInfoPoolId;
    tMemPoolId  TimerPoolId;
    tMemPoolId  MiscPoolId;
    tMemPoolId  QPoolId;
    tMemPoolId  ForwTblPtrPoolId;
    tMemPoolId  ForwCachePtrPoolId;
    tMemPoolId  HostMbrPtrPoolId;
    tMemPoolId  NbrTblPtrPoolId;
    tMemPoolId  RouteTblPtrPoolId;
    tMemPoolId  NextHopPtrPoolId;
    tMemPoolId  IfaceListPtrPoolId;
    tMemPoolId  NbrListPtrPoolId;
    
} tDvmrpGlobalMemPoolId;

typedef struct _DpConfigParam {
   struct DpSemDesc   DpMutex;
   UINT1      u1MfwdStatus;
   UINT1      u1DvmrpStatus;
   UINT2      u2PruneLifeTime;
   INT4       i4IpRegnId;
   UINT4      u4MaxSrcs;
#ifdef LNXIP4_WANTED
   INT4       i4DvmrpSockId;
#endif
} tDpConfigParam;

typedef struct _DvmrpInterfaceNode {
    tTMO_SLL_NODE   IfHashLink; /* This should be the first element 
                                 * Plz do not change the order of this 
                                 */
    tTMO_SLL_NODE   IfGetNextLink;
    UINT4           u4IfIndex;          /* This interface index is IP port 
                                           number*/
    UINT4           u4IfMask;
    UINT1           u1IfStatus;        /* Admin Status of that interface */
    UINT1           u1IfMetric;        /* Interface Metric */
    UINT1           u1IfRowStatus;     /* Interface Row status */
    UINT1           u1Reserved;        /* Reserved Field */
    UINT4           u4IfMTUSize;       /* MTU Size on that interface */
    UINT4           u4IfLocalAddr;     /* IP address on that interface */
    UINT4           u4IfRcvBadPkts;    /* Bad packets rcvd on that interface */
    UINT4           u4IfRcvBadRoutes;  /* Bad Routes rcvd on that interface */
    INT4            i4IfTtlTreshold;
    INT4            i4IfRateLimit;
    tNbrTbl         *pNbrOnthisIface;
    UINT2           u2VlanId;
    UINT2           u2Pad;
} tDvmrpInterfaceNode;

typedef struct _DvmrpInterfaceInfo {
tTMO_HASH_TABLE             *IfHashTbl;
UINT4                        u4HashSize;
tTMO_SLL                     IfGetNextList;
} tDvmrpInterfaceInfo;

/* This enum defines the CpuPortStatus */
typedef enum 
{
   DVMRP_DELETE_CPUPORT,  /* 0 */
   DVMRP_ADD_CPUPORT      /* 1 */
} ePimCpuPortStatus;


#endif /* _DPTDFS_H */
