/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpprot.h,v 1.20 2011/09/24 06:51:06 siva Exp $
 *
 * Description: This file holds Prototypes declarations 
 *
 *******************************************************************/
#ifndef _DPPROTINC_H
#define _DPPROTINC_H

/*  Prototype Declarations  */
INT4      DvmrpTestStatus (UINT1 u1Value);
INT4      DvmrpSetStatus (UINT1 u1Value);
INT4      DvmrpGetStatus (void);
INT4      DvmrpTestLogEnabled (UINT1 u1Value);
INT4      DvmrpTestLogMask (UINT4 u1Value);
INT4      DvmrpSetLogMask (UINT4 u1Value);
INT4      DvmrpSetLogEnabled (UINT1 u1Value);
UINT1     DvmrpGetLogEnabled (void);
UINT4     DvmrpGetLogMask (void);
UINT1    *DvmrpGetVersionString (void);
INT4      DvmrpGetGenerationId (void);

INT4      DvmrpTestMaxSrcs (UINT4 u4Value);
INT4      DvmrpTestPruneLifeTime (UINT4 u4Value);

INT4      DvmrpGetNumRoutes (void);
INT4      DvmrpGetReachableRoutes (void);
INT4      DvmrpGetNextRouteIndex (UINT4 * pu4RouteAddress,
                                  UINT4 * pu4RouteMask);
INT4      DvmrpGetFirstRouteIndex (UINT4 * pu4RouteSource,
                                   UINT4 * pu4RouteMask);
INT1      DvmrpRouteTableEntryExists (UINT4 u4RouteSource, UINT4 u4RouteMask);
INT4      DvmrpGetRouteUpstreamNeighbor (UINT4 u4RouteSource,
                                         UINT4 u4RouteMask, UINT4 *);
INT4      DvmrpGetRouteIfIndex (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *);
INT4      DvmrpGetRouteMetric (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *);
INT4      DvmrpGetRouteExpiryTime (UINT4 u4RouteSource, UINT4 u4RouteMask,UINT4 *);
INT4      DvmrpGetRouteUpTime (UINT4 u4RouteSource, UINT4 u4RouteMask, UINT4 *);
INT4      DvmrpGetRouteStatus (UINT4 u4RouteSource, UINT4 u4RouteMask, INT4 *);

INT4      DvmrpInterfaceTableEntryExists (UINT4 u4IfIndex);
INT4      DvmrpGetFirstInterfaceIndex (UINT4 *pu4InterfaceIfIndex);
INT4 DvmrpForwardTableEntryExists (UINT4 u4SrcAddress, UINT4 u4GrpAddress);
INT4 DvmrpGetFirstForwardIndex (UINT4 *pu4SrcAddress, UINT4 *pu4GrpAddress);
INT4 DvmrpGetNextForwardIndex (UINT4 *pu4SrcAddress, UINT4 *pu4GrpAddress);
INT4 DvmrpGetForwardUpstreamNeighbor (UINT4 u4SrcNetwork, UINT4 u4GrpAddress,
                        UINT4 * pUpStreamNbr);
INT4 DvmrpGetForwardExpiryTime (UINT4 u4SrcNetwork, UINT4 u4GrpAddress,
                  UINT4 * pExpTime);
INT4 DvmrpGetForwardTblStatus  (UINT4 u4SrcNetwork, UINT4 u4GrpAddress,
                         INT4 * pTblStatus);

/* Neighbor Table Prototypes */
INT4      DvmrpGetNextNeighborIndex (UINT4 * pu4Address);
INT4      DvmrpGetFirstNeighborIndex (UINT4 * pu4Address);
INT4      DvmrpNeighborTableEntryExists (UINT4 u4Address);
INT4      DvmrpGetNeighborUpTime (UINT4 u4Address, UINT4 *);
INT4      DvmrpGetNeighborIfIndex (UINT4 u4Address, INT4 *);
INT4      DvmrpGetNeighborExpiryTime (UINT4 u4Address, UINT4 *);
INT4      DvmrpGetNeighborGenerationId (UINT4 u4Address, INT4 *);
INT4      DvmrpGetNeighborMajorVersion (UINT4 u4Address, INT4 *);
INT4      DvmrpGetNeighborMinorVer (UINT4 u4Address, INT4 *);
INT4      DvmrpGetNeighborCapabilities (UINT4 u4Address, INT4 *);
INT4      DvmrpGetNeighborRcvRoutes (UINT4 u4Address, UINT4 *);
INT4      DvmrpGetNeighborRcvBadRoutes (UINT4 u4Address, UINT4 *);
INT4      DvmrpGetNeighborRcvBadPkts (UINT4 u4Address, UINT4 *);


INT4      DvmrpRouteNextHopTableEntryExists (UINT4 u4Address,
                                             UINT4 u4Mask,
                                             UINT4 u4IfIndex);
INT4      DvmrpGetRouteNextHopType (UINT4 u4Address, UINT4 u4Mask,
                                    UINT4 u4IfIndex, INT4 *);
INT4      DvmrpGetNextRouteNextHopIndex (UINT4 * pu4Address,
                                         UINT4 * pu4Mask,
                                         UINT4 * pu4IfIndex);
INT4      DvmrpGetRouteNextHopDesigFowr (UINT4 u4Address, UINT4 u4Mask,
                                         UINT4 u4IfIndex, INT4 *pi4Flag);
INT4      DvmrpGetFirstRouteNextHopIndex (UINT4 * pu4Address,
                                          UINT4 * pu4Mask,
                                          UINT4 * pu4IfIndex);
 /* Forward Prune Table Prototypes */

INT4 
DvmrpGetNeighborAdjFlag (UINT4 u4DvmrpNeighborAddress,
                        UINT4  *pi4RetValDvmrpNeighborAdjFlag);

INT4
DvmrpGetForwardInIfIndex (UINT4 u4SrcNetwork,
                          UINT4 u4GrpAddress, INT4 *pInIfIndex);

INT4
DvmrpGetForwardInIfState (UINT4 u4SrcNetwork,
                          UINT4 u4GrpAddress, INT4 *pInIfState);
INT4
DvmrpForwardPruneNbrTableEntryExists (UINT4 u4SrcNetwork, UINT4 u4GrpAddress, 
                        UINT4 u4IfIndex, UINT4 u4NbrAddr);

INT4
DvmrpGetFirstForwardPruneNbrIndex ( UINT4 *pu4SourceNetwork,
                                    UINT4 *pu4GroupAddress,
        UINT4 *pu4IfIndex,
        UINT4 *pu4NbrAddr);

INT4
DvmrpGetNextForwardPruneNbrIndex (UINT4 *pu4Network, UINT4 *pu4Group, 
                    UINT4 *pu4IfIndex, UINT4 *pu4NbrAddr);


INT4
DvmrpGetForwardNbrPruneTime (UINT4 u4SrcNetwork, UINT4 u4GrpAddress, 
               UINT4 u4IfIndex, UINT4 u4NbrAddr,
               UINT4 *pu4PruneTime);
VOID      
DvmrpGetTimeString (UINT4 u4Time, INT1 *Time);
INT4
DvmrpGetRouteNextHopDepNbrs (UINT4 u4Address, UINT4 u4Mask,
                             UINT4 u4IfIndex, tSNMP_OCTET_STRING_TYPE *pDepNbrs);
VOID
DvmrpPmSendExponentialPrune (tForwCacheTbl *pForwTbl);


void      DvmrpIpSendPacket
ARG_LIST ((UINT4 u4DvmrpLen, UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4 u4IfId));

void DvmrpGetIfaceInfoFromLowerLayer ARG_LIST ((UINT4 u4Var));

void DvmrpGmSendGraftIfIfacePruned (tForwCacheTbl * pFwdCache, UINT4 u4IfId);

VOID
DvmrpIhmHandleInputQMsgs (VOID);


void      DvmrpRthHandleRouteReport (UINT4 u4DataLen, UINT4 u4IfIndex,
                                     tNbrTbl * pNbrTbl);

void DvmrpRthRouteAgeTimerHandler (tDvmrpTimer *pTimerBlk);

void      DvmrpRthHandleShutdownEvent (void);

void      DvmrpRthCancelTimedoutNbr (UINT4 u4NbrAddr, UINT4 u4IfIndex);

void      DvmrpRthLocIfaceUpdate (UINT4 u4UpstreamNbr, UINT4 u4Mask,
                                  UINT1 u1Metric, UINT4 u4IfId, UINT1 u1Status);

UINT1     DvmrpRthSearchNextHopTbl (UINT4 u4SrcNetwork, UINT4 u4Mask,
                                    UINT4 * pu4Entry);

void      DvmrpRthCreateNodeinNexthopTbl (tRouteTbl * pRouteTblPtr,
                                          UINT4 u4IfId,
                                          UINT4 u4FreeNexthopEntry);

INT1      DvmrpUtilValidMask (UINT4 u4Mask);
tIfaceList *DvmrpUtilGetFreeIface (VOID);
void      DvmrpUtilReleaseIface (tIfaceList * pInterface);
tNbrList *DvmrpUtilGetNbr (VOID);
void      DvmrpUtilReleaseNbr (tNbrList * pNeighbor);
UINT1    *DvmrpUtilGetCacheEntry (UINT4 u4Value1, UINT4 u4Value2, UINT1 u1Flag);
UINT4     DvmrpUtilCheckConsOnes (UINT4 u4Value);
INT4      DvmrpUtilStartTimer (UINT1 u1TimerType, UINT2 u2Duration,
                               tForwCacheTbl * pPtrRef);

INT4      DvmrpUtilStopTimer (UINT1 u1TimerType, tForwCacheTbl * pPtrRef);

INT1      DvmrpUtilValidSubnet (UINT4 u4Subnet, UINT4 u4Mask);

INT4 DvmrpInit      ARG_LIST ((void));

INT4
DvmrpGlobalMemInit ARG_LIST ((VOID));

INT4
DvmrpGlobalMemAllocate ARG_LIST ((VOID));

VOID
DvmrpGlobalTableInit ARG_LIST ((VOID));

VOID
DvmrpGlobalTableFree ARG_LIST ((VOID));

INT4
DvmrpInitIfaceTimersAndBuffer ARG_LIST ((VOID));

VOID
DvmrpLoadDefaultConfigs ARG_LIST ((VOID));

VOID      DvmrpHandleDataPkt (tCRU_BUF_CHAIN_DESC * pBuf);

INT4      IpSend (tCRU_BUF_CHAIN_HEADER *);

INT4      DvmrpInitHashTables (VOID);

UINT4
DvmrpIfNodeAddCriteria ARG_LIST ((tTMO_HASH_NODE * pCurNode,
                                  UINT1 *pu1IfIndex));

VOID   DvmrpInitInterfaceInfo ARG_LIST ((VOID));

void DvmrpIhmFreeDataStructures ARG_LIST ((void));



tDvmrpInterfaceNode *
DvmrpGetInterfaceNode (UINT4 u4IfIndex);

INT4 DvmrpIhmStartTimers ARG_LIST ((void));

tDvmrpInterfaceNode *
DvmrpCreateInterfaceNode (UINT4 u4IfIndex, UINT1 u1IfStatus);

INT4
DvmrpDeleteInterfaceNode (tDvmrpInterfaceNode * pIfaceNode);

VOID
DvmrpSetDefaultIfaceNodeValue (UINT4 u4IfIndex, UINT1 u1IfStatus,
                               tDvmrpInterfaceNode * pIfNode);


void DvmrpGmFormGraft ARG_LIST ((UINT4, UINT4, UINT4));

/* dprrf.c */
void DvmrpRrfSendRouteTable ARG_LIST ((UINT4 u4NbrIndex));

void DvmrpRrfSendRouteReport ARG_LIST ((tDvmrpTimer *pTimerBlk));
void
DvmrpRrfRouteReportTimerHandler ARG_LIST ((tDvmrpTimer * pTimerBlk));
void DvmrpRrfReportNonStop ARG_LIST ((UINT4 u4IfaceIndex, UINT4
                                             u4DestAddr, UINT1 u1Flag));

VOID
DvmrpMfwdHandleEnabling ARG_LIST((VOID));
VOID
DvmrpMfwdHandleDisabling ARG_LIST((VOID));
INT4
DvmrpMfwdCreateRtEntry ARG_LIST ((tForwTbl *pFwdEntry));
INT4
DvmrpMfwdDeleteRtEntry ARG_LIST ((tForwTbl *pFwdEntry));
INT4
DvmrpMfwdDeleteOneRtEntry ARG_LIST ((tForwCacheTbl *pFwdCache));
INT4
DvmrpMfwdSetDeliverMdpFlag ARG_LIST ((tForwTbl *pFwdEntry,
                                      UINT1 u1DeliverMdp));
INT4
DvmrpMfwdAddOif ARG_LIST ((tForwTbl * pFwdEntry,
                                     UINT4 u4OifIndex));
INT4
DvmrpMfwdDeleteOif ARG_LIST ((tForwTbl *pFwdEntry, UINT4 u4OifIndex));
INT4
DvmrpMfwdUpdateOif ARG_LIST ((tForwTbl *pFwdEntry, UINT4 u4OifIndex,
                                        UINT1 u1UpdCmd));
INT4
DvmrpMfwdUpdateIif ARG_LIST ((tForwTbl *pFwdEntry, UINT4 u4OldSrc,
                                    UINT4 u4PrevIif));
INT4
DvmrpMfwdAddIface ARG_LIST ((UINT4 u4IfIndex));
INT4
DvmrpMfwdDeleteIface ARG_LIST ((UINT4 u4IfIndex));

void 
DvmrpFlashUpdate (UINT4 u4SrcNetwork, UINT4 u4SrcMask, tNbrTbl *pNbrTbl,
                  UINT1 u1Metric, UINT1 u1FlashType);

void DvmrpSendFlashRoute (UINT4 u4SrcNetwork, UINT4 u4SrcMask,
                          UINT1 u1Metric, UINT4 u4NbrAddr, UINT4 u4NbrIdx);
void
DvmrpHmProcessPendingGroups ARG_LIST((tDvmrpInterfaceNode *pIfNode));

#ifdef FS_NPAPI
INT4 DvmrpNpDeleteOif(tForwTbl *pFwdEntry, UINT4 u4OifIndex);
INT4 DvmrpNpAddOif(tForwTbl *pFwdEntry, UINT4 u4OifIndex);
INT4 DvmrpNpUpdateIif (tForwTbl *pFwdEntry);
INT4 DvmrpNpAddCpuPort (tForwTbl *pFwdEntry);
INT4 DvmrpNpDeleteCpuPort (tForwTbl *pFwdEntry);
INT4 DvmrpNpDeleteRoute(tForwTbl *pFwdEntry);
INT4 DvmrpNpAddRoute(tForwTbl *pFwdEntry);
INT4 DvmrpNpDeleteOneRoute(tForwCacheTbl *pFwdCache);
INT4 DvmrpNpGetHitBitStatus (tForwCacheTbl *pFwdCache);
VOID DvmrpHandlePbmpChgEvent (tCRU_BUF_CHAIN_HEADER * pBuf);
UINT4 DvmrpGetMcastFwdPortList PROTO ((UINT4 u4GrpAddr, 
                                UINT4 u4SrcAddr, tVlanId u2VlanId,
                                tPortList McFwdPortList,
                                tPortList UntagPortList));

INT4  DvmrpNpGetMRoutePktCount (tForwCacheTbl *, UINT4 *);
INT4  DvmrpNpGetMRouteDifferentInIfPktCount (tForwCacheTbl *, UINT4 *);
INT4  DvmrpNpGetMRouteOctetCount (tForwCacheTbl *, UINT4 *);
INT4  DvmrpNpGetMRouteHCOctetCount (tForwCacheTbl *, tSNMP_COUNTER64_TYPE *);
INT4  DvmrpNpGetMNextHopPktCount (tForwCacheTbl *, INT4, UINT4 *);
INT4  DvmrpNpGetMIfInOctetCount (INT4, UINT4 *);
INT4  DvmrpNpGetMIfOutOctetCount (INT4, UINT4 *);
INT4  DvmrpNpGetMIfHCInOctetCount (INT4, tSNMP_COUNTER64_TYPE *);
INT4  DvmrpNpGetMIfHCOutOctetCount (INT4, tSNMP_COUNTER64_TYPE *);
INT4  DvmrpNpSetMIfaceTtlTreshold (INT4, INT4);
INT4  DvmrpNpSetMIfaceRateLimit (INT4, INT4);
INT4  DvmrpNpWrAddRoute(tIpv4McRouteInfo  * pIpv4McRouteInfo);
INT4  DvmrpNpWrDelRoute(tIpv4McRouteInfo  * pIpv4McRouteInfo);
INT4  DvmrpNpWrAddOif(tIpv4McRouteInfo  * pIpv4McRouteInfo, 
                      tOutputInterface  *pOifNode);
INT4  DvmrpNpWrDelOif(tIpv4McRouteInfo  * pIpv4McRouteInfo, 
                      tOutputInterface   *pOifNode);
INT4  DvmrpNpWrUpdateIif(tIpv4McRouteInfo  * pIpv4McRouteInfo, 
                         tOutputInterface   *pOifNode);
INT4  DvmrpNpWrUpdateCpuPort (tIpv4McRouteInfo *pIpv4McRouteInfo,
                              UINT4 u4CpuPortStatus);
#endif /*FS_NPAPI */

#ifdef MBSM_WANTED
INT4                DvmrpMbsmUpdtMrouteTblForLoadSharing (UINT1 u1Flag);
#endif /* MBSM_WANTED */


INT4
DvmrpRegisterWithExtModules (VOID);

VOID
DvmrpDeRegisterWithExtModules (VOID);

INT4
DvmrpGetVlanIdFromIfIndex(UINT4 u4Port, UINT2 *pu2VlanId);

#endif  /* _DPPROTINC_H */
