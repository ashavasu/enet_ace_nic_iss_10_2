/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdvmrlw.h,v 1.6 2009/02/26 16:11:44 nswamy-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpVersionString ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDvmrpGenerationId ARG_LIST((INT4 *));

INT1
nmhGetDvmrpNumRoutes ARG_LIST((UINT4 *));

INT1
nmhGetDvmrpReachableRoutes ARG_LIST((UINT4 *));

INT1
nmhGetDvmrpStatus ARG_LIST((INT4 *));

INT1
nmhGetDvmrpLogEnabled ARG_LIST((INT4 *));

INT1
nmhGetDvmrpLogMask ARG_LIST((INT4 *));

INT1
nmhGetDvmrpPruneLifeTime ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDvmrpStatus ARG_LIST((INT4 ));

INT1
nmhSetDvmrpLogEnabled ARG_LIST((INT4 ));

INT1
nmhSetDvmrpLogMask ARG_LIST((INT4 ));

INT1
nmhSetDvmrpPruneLifeTime ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DvmrpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DvmrpLogEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DvmrpLogMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2DvmrpPruneLifeTime ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DvmrpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DvmrpLogEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DvmrpLogMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2DvmrpPruneLifeTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DvmrpInterfaceTable. */
INT1
nmhValidateIndexInstanceDvmrpInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpInterfaceTable  */

INT1
nmhGetFirstIndexDvmrpInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpInterfaceStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceLocalAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceMetric ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceRcvBadPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceRcvBadRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceTtl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDvmrpInterfaceInMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceOutMcastOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDvmrpInterfaceHCInMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDvmrpInterfaceHCOutMcastOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDvmrpInterfaceStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDvmrpInterfaceTtl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDvmrpInterfaceRateLimit ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DvmrpInterfaceStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DvmrpInterfaceTtl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DvmrpInterfaceRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DvmrpInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DvmrpNeighborTable. */
INT1
nmhValidateIndexInstanceDvmrpNeighborTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpNeighborTable  */

INT1
nmhGetFirstIndexDvmrpNeighborTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpNeighborTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpNeighborIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborUpTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborExpiryTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborGenerationId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborMajorVersion ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborMinorVersion ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborCapabilities ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDvmrpNeighborRcvRoutes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborRcvBadPkts ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborRcvBadRoutes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDvmrpNeighborAdjFlag ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for DvmrpRouteTable. */
INT1
nmhValidateIndexInstanceDvmrpRouteTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpRouteTable  */

INT1
nmhGetFirstIndexDvmrpRouteTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpRouteUpstreamNeighbor ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpRouteIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpRouteMetric ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpRouteExpiryTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpRouteUpTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpRouteStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for DvmrpRouteNextHopTable. */
INT1
nmhValidateIndexInstanceDvmrpRouteNextHopTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpRouteNextHopTable  */

INT1
nmhGetFirstIndexDvmrpRouteNextHopTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpRouteNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpRouteNextHopType ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetDvmrpRouteNextHopDesigForw ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetDvmrpRouteNextHopDepNbrs ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for DvmrpForwardTable. */
INT1
nmhValidateIndexInstanceDvmrpForwardTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpForwardTable  */

INT1
nmhGetFirstIndexDvmrpForwardTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpForwardTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpForwardUpstreamNeighbor ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpForwardInIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpForwardInIfState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpForwardExpiryTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpForwardTblStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for DvmrpForwardPruneNbrTable. */
INT1
nmhValidateIndexInstanceDvmrpForwardPruneNbrTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpForwardPruneNbrTable  */

INT1
nmhGetFirstIndexDvmrpForwardPruneNbrTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpForwardPruneNbrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpForwardNbrPruneTime ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for DvmrpIpMRTable. */
INT1
nmhValidateIndexInstanceDvmrpIpMRTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpIpMRTable  */

INT1
nmhGetFirstIndexDvmrpIpMRTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpIpMRTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpIpMRUpstreamNeighbor ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRInIfIndex ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMRUpTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRPkts ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRDifferentInIfPackets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMROctets ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRProtocol ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMRRtProto ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMRRtAddress ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRRtMask ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMRRtType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMRHCOctets ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for DvmrpIpMNextHopTable. */
INT1
nmhValidateIndexInstanceDvmrpIpMNextHopTable ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DvmrpIpMNextHopTable  */

INT1
nmhGetFirstIndexDvmrpIpMNextHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDvmrpIpMNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDvmrpIpMNextHopState ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMNextHopUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMNextHopExpiryTime ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDvmrpIpMNextHopProtocol ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetDvmrpIpMNextHopPkts ARG_LIST((UINT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));
