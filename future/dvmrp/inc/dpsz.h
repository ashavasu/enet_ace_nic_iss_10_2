/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpsz.h,v 1.3 2011/12/29 12:46:24 siva Exp $
 *
 * Description: This file contains all the sizing Id parameters.
 *
 *******************************************************************/

enum {
    MAX_DP_FWD_CACHE_PTR_SIZING_ID,
    MAX_DP_FWD_CACHE_TBL_ENTRIES_SIZING_ID,
    MAX_DP_FWD_TBL_ENTRIES_SIZING_ID,
    MAX_DP_FWD_TBL_PTR_SIZING_ID,
    MAX_DP_HOST_MBR_PTR_SIZING_ID,
    MAX_DP_HOST_MBR_TBL_ENTRIES_SIZING_ID,
    MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES_SIZING_ID,
    MAX_DP_IFACE_LIST_PTR_SIZING_ID,
    MAX_DP_MAX_IFACES_SIZING_ID,
    MAX_DP_MISC_BLOCKS_SIZING_ID,
    MAX_DP_NBR_FREE_POOL_TBL_ENTRIES_SIZING_ID,
    MAX_DP_NBR_LIST_PTR_SIZING_ID,
    MAX_DP_NBR_TBL_ENTRIES_SIZING_ID,
    MAX_DP_NBR_TBL_PTR_SIZING_ID,
    MAX_DP_NEXT_HOP_PTR_SIZING_ID,
    MAX_DP_NEXT_HOP_TBL_ENTRIES_SIZING_ID,
    MAX_DP_Q_DEPTH_SIZING_ID,
    MAX_DP_ROUTE_TBL_ENTRIES_SIZING_ID,
    MAX_DP_ROUTE_TBL_PTR_SIZING_ID,
    MAX_DP_SRC_FREE_POOL_TBL_ENTRIES_SIZING_ID,
    DVMRP_MAX_SIZING_ID
};


#ifdef  _DVMRPSZ_C
tMemPoolId DVMRPMemPoolIds[ DVMRP_MAX_SIZING_ID];
INT4  DvmrpSizingMemCreateMemPools(VOID);
VOID  DvmrpSizingMemDeleteMemPools(VOID);
INT4  DvmrpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DVMRPSZ_C  */
extern tMemPoolId DVMRPMemPoolIds[ ];
extern INT4  DvmrpSizingMemCreateMemPools(VOID);
extern VOID  DvmrpSizingMemDeleteMemPools(VOID);
#endif /*  _DVMRPSZ_C  */


#ifdef  _DVMRPSZ_C
tFsModSizingParams FsDVMRPSizingParams [] = {
{ "tForwCacheTblPtr", "MAX_DP_FWD_CACHE_PTR", sizeof(tForwCacheTblPtr),MAX_DP_FWD_CACHE_PTR, MAX_DP_FWD_CACHE_PTR,0 },
{ "tForwCacheTbl", "MAX_DP_FWD_CACHE_TBL_ENTRIES", sizeof(tForwCacheTbl),MAX_DP_FWD_CACHE_TBL_ENTRIES, MAX_DP_FWD_CACHE_TBL_ENTRIES,0 },
{ "tForwTbl", "MAX_DP_FWD_TBL_ENTRIES", sizeof(tForwTbl),MAX_DP_FWD_TBL_ENTRIES, MAX_DP_FWD_TBL_ENTRIES,0 },
{ "tForwTablePtr", "MAX_DP_FWD_TBL_PTR", sizeof(tForwTablePtr),MAX_DP_FWD_TBL_PTR, MAX_DP_FWD_TBL_PTR,0 },
{ "tHostMbrTblPtr", "MAX_DP_HOST_MBR_PTR", sizeof(tHostMbrTblPtr),MAX_DP_HOST_MBR_PTR, MAX_DP_HOST_MBR_PTR,0 },
{ "tHostMbrTbl", "MAX_DP_HOST_MBR_TBL_ENTRIES", sizeof(tHostMbrTbl),MAX_DP_HOST_MBR_TBL_ENTRIES, MAX_DP_HOST_MBR_TBL_ENTRIES,0 },
{ "tIfaceList", "MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES", sizeof(tIfaceList),MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES, MAX_DP_IFACE_FREE_POOL_TBL_ENTRIES,0 },
{ "tIfaceListPtr", "MAX_DP_IFACE_LIST_PTR", sizeof(tIfaceListPtr),MAX_DP_IFACE_LIST_PTR, MAX_DP_IFACE_LIST_PTR,0 },
{ "tDvmrpInterfaceNode", "MAX_DP_MAX_IFACES", sizeof(tDvmrpInterfaceNode),MAX_DP_MAX_IFACES, MAX_DP_MAX_IFACES,0 },
{ "tDvmrpMsgBlock", "MAX_DP_MISC_BLOCKS", sizeof(tDvmrpMsgBlock),MAX_DP_MISC_BLOCKS, MAX_DP_MISC_BLOCKS,0 },
{ "tNbrList", "MAX_DP_NBR_FREE_POOL_TBL_ENTRIES", sizeof(tNbrList),MAX_DP_NBR_FREE_POOL_TBL_ENTRIES, MAX_DP_NBR_FREE_POOL_TBL_ENTRIES,0 },
{ "tNbrListPtr", "MAX_DP_NBR_LIST_PTR", sizeof(tNbrListPtr),MAX_DP_NBR_LIST_PTR, MAX_DP_NBR_LIST_PTR,0 },
{ "tNbrTbl", "MAX_DP_NBR_TBL_ENTRIES", sizeof(tNbrTbl),MAX_DP_NBR_TBL_ENTRIES, MAX_DP_NBR_TBL_ENTRIES,0 },
{ "tNbrTblPtr", "MAX_DP_NBR_TBL_PTR", sizeof(tNbrTblPtr),MAX_DP_NBR_TBL_PTR, MAX_DP_NBR_TBL_PTR,0 },
{ "tNextHopTblPtr", "MAX_DP_NEXT_HOP_PTR", sizeof(tNextHopTblPtr),MAX_DP_NEXT_HOP_PTR, MAX_DP_NEXT_HOP_PTR,0 },
{ "tNextHopTbl", "MAX_DP_NEXT_HOP_TBL_ENTRIES", sizeof(tNextHopTbl),MAX_DP_NEXT_HOP_TBL_ENTRIES, MAX_DP_NEXT_HOP_TBL_ENTRIES,0 },
{ "tDvmrpQMsg", "MAX_DP_Q_DEPTH", sizeof(tDvmrpQMsg),MAX_DP_Q_DEPTH, MAX_DP_Q_DEPTH,0 },
{ "tRouteTbl", "MAX_DP_ROUTE_TBL_ENTRIES", sizeof(tRouteTbl),MAX_DP_ROUTE_TBL_ENTRIES, MAX_DP_ROUTE_TBL_ENTRIES,0 },
{ "tRouteTblPtr", "MAX_DP_ROUTE_TBL_PTR", sizeof(tRouteTblPtr),MAX_DP_ROUTE_TBL_PTR, MAX_DP_ROUTE_TBL_PTR,0 },
{ "tDpActiveSrcInfo", "MAX_DP_SRC_FREE_POOL_TBL_ENTRIES", sizeof(tDpActiveSrcInfo),MAX_DP_SRC_FREE_POOL_TBL_ENTRIES, MAX_DP_SRC_FREE_POOL_TBL_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DVMRPSZ_C  */
extern tFsModSizingParams FsDVMRPSizingParams [];
#endif /*  _DVMRPSZ_C  */


