
# ifndef fsdvm_OCON_H
# define fsdvm_OCON_H
/*
 *  The Constant Declarations for
 *  dvmrpInterfaceTable
 */
# define DVMRPINTERFACECONTROLINDEX               (1)
# define DVMRPINTERFACEPORTNUM               (2)
# define DVMRPINTERFACELOCALADDRESS               (3)
# define DVMRPINTERFACEMETRIC               (4)
# define DVMRPINTERFACERCVBADPKTS               (5)
# define DVMRPINTERFACERCVBADROUTES               (6)

/*
 *  The Constant Declarations for
 *  dvmrpNeighborTable
 */
# define DVMRPNEIGHBORCONTROLINDEX               (1)
# define DVMRPNEIGHBORPORTNUM               (2)
# define DVMRPNEIGHBORADDRESS               (3)
# define DVMRPNEIGHBORUPTIME               (4)
# define DVMRPNEIGHBOREXPIRYTIME               (5)
# define DVMRPNEIGHBORGENERATIONID               (6)
# define DVMRPNEIGHBORMAJORVERSION               (7)
# define DVMRPNEIGHBORMINORVERSION               (8)
# define DVMRPNEIGHBORCAPABILITIES               (9)
# define DVMRPNEIGHBORRCVROUTES               (10)
# define DVMRPNEIGHBORRCVBADPKTS               (11)
# define DVMRPNEIGHBORRCVBADROUTES               (12)

/*
 *  The Constant Declarations for
 *  dvmrpRouteTable
 */
# define DVMRPROUTESOURCE               (1)
# define DVMRPROUTESOURCEMASK               (2)
# define DVMRPROUTEUPSTREAMNEIGHBOR               (3)
# define DVMRPROUTECONTROLINDEX               (4)
# define DVMRPROUTEPORTNUM               (5)
# define DVMRPROUTEMETRIC               (6)
# define DVMRPROUTEEXPIRYTIME               (7)
# define DVMRPROUTEUPTIME               (8)

/*
 *  The Constant Declarations for
 *  dvmrpRouteNextHopTable
 */
# define DVMRPROUTENEXTHOPSOURCE               (1)
# define DVMRPROUTENEXTHOPSOURCEMASK               (2)
# define DVMRPROUTENEXTHOPCONTROLINDEX               (3)
# define DVMRPROUTENEXTHOPPORTNUM               (4)
# define DVMRPROUTENEXTHOPTYPE               (5)

/*
 *  The Constant Declarations for
 *  dvmrpScalar
 */
# define DVMRPVERSIONSTRING               (1)
# define DVMRPGENERATIONID               (2)
# define DVMRPNUMROUTES               (3)
# define DVMRPREACHABLEROUTES               (4)
# define DVMRPSTATUS               (5)
# define DVMRPLOGENABLED               (6)
# define DVMRPLOGMASK               (7)
# define DVMRPMAXSRCS               (8)

#endif /*  fsdvmrp_OCON_H  */

