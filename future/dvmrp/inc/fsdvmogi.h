
# ifndef fsdvm_OGP_H
# define fsdvm_OGP_H

 /* The Definitions of the OGP Index Constants.  */
# define SNMP_OGP_INDEX_DVMRPINTERFACETABLE               (0)
# define SNMP_OGP_INDEX_DVMRPNEIGHBORTABLE               (1)
# define SNMP_OGP_INDEX_DVMRPROUTETABLE               (2)
# define SNMP_OGP_INDEX_DVMRPROUTENEXTHOPTABLE               (3)
# define SNMP_OGP_INDEX_DVMRPSCALAR               (4)

#endif /*  fsdvmrp_OGP_H  */

