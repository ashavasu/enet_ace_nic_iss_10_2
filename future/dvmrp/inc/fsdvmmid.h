
/*  Prototype for Get Test & Set for dvmrpInterfaceTable.  */
tSNMP_VAR_BIND*
dvmrpInterfaceEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for dvmrpNeighborTable.  */
tSNMP_VAR_BIND*
dvmrpNeighborEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for dvmrpRouteTable.  */
tSNMP_VAR_BIND*
dvmrpRouteEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for dvmrpRouteNextHopTable.  */
tSNMP_VAR_BIND*
dvmrpRouteNextHopEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for dvmrpScalar.  */
tSNMP_VAR_BIND*
dvmrpScalarGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
dvmrpScalarSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
dvmrpScalarTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));

