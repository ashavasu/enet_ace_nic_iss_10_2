/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpsnmp.h,v 1.3 2007/02/01 14:46:55 iss Exp $
 *
 * Description: #defines used in dpsnmp.c
 *
 *******************************************************************/
#ifndef _DPSNMPINC_H
#define _DPSNMPINC_H

/* 
 * The following structure is used to pass as parameters the four indices
 * of the NextHop table to the FourIndexCompare function
 */

typedef struct Indices
{
    UINT4  u4Address;     /* RouteNextHopSource IP address */
    UINT4  u4Mask;        /* RouteNextHopSource Mask */
    UINT4  u4IfIndex;     /* RouteNextHop IfIndex */
} tIndices;

typedef struct FwdIndices
{
    UINT4  u4SrcNw;     /* FwdPrunenbrSource IP address */
    UINT4  u4Mask;        /*FwdPrunenbrSource Mask */
    UINT4  u4IfIndex;     /* FwdPrunenbr IfIndex */
    UINT4  u4NbrAddr;     /* FwdPrunenbr Addr */
} tFwdIndices;

/* 
 *  The #defines below should be changed as required by SNMP midlevel 
 */
/* 
 * values of SUCCESS & FAILURE must correspond 
 * to the values given in the SNMP Agent used.
 */
#define   VAR1_GT_VAR2                 1                                        
#define   VAR1_LT_VAR2                 2                                        
#define   VAR1_EQ_VAR2                 0

INT1      TwoIndexCompare (UINT4 u4RouteSource1, UINT4 u4RouteSource2,
                           UINT4 u4RouteMask1, UINT4 u4RouteMask2);
INT1      ThreeIndexCompare (UINT2 u2CtrlIndex1, UINT2 u2CtrlIndex2,
                             UINT1 u1Port1, UINT1 u1Port2,
                             UINT4 u4NbrAddr1, UINT4 u4NbrAddr2);
void      IndexCopy (tIndices * pIndex1, tIndices * pIndex2);
void      FwdIndexCopy (tFwdIndices * pIndex1, tFwdIndices * pIndex2);
INT1
FwdIndexCompare (tFwdIndices *pIndex1, tFwdIndices *pIndex2);
INT1
NextHopIndexCompare (tIndices *pIndex1, tIndices *pIndex2);

INT1
NeighborIndexCompare (UINT4 u4NbrAddr1, UINT4 u4NbrAddr2);

#endif
