/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpextern.h,v 1.14 2012/03/01 10:07:02 siva Exp $
 *
 * Description: This file contains externs 
 *
 *******************************************************************/

extern tTimerListId gDpTmrListId;

extern tDvmrpSystemSize   gDvmrpSystemSize;
extern tDvmrpInterfaceInfo gDvmrpIfInfo;
extern tDvmrpGlobalMemPoolId gDvmrpMemPool;

extern tForwTbl       **gpForwTable;
extern tForwCacheTbl  **gpForwCacheTable;
extern tHostMbrTbl    **gpHostMbrTable;
extern tNbrTbl        **gpNeighborTable;
extern tRouteTbl      **gpRouteTable;
extern tNextHopTbl    **gpNextHopTable;
extern tIfaceList     **gpGlobIfacePtr;
extern tNbrList       **gpGlobNbrPtr;

extern tRouteTbl      *gpRouteFreePtr;
extern UINT4           gu4MaxInterfaces;
extern UINT1          *gpSendBuffer;
extern UINT1          *gpRcvBuffer;
extern UINT1          *gpRcvBufferRead;
extern tDvmrpTimer    gTimerList[DVMRP_MAX_TABLE_TIMERS];
extern UINT1           gu1DvmrpLogEnabled;
extern UINT4           gu4DvmrpLogMask;
extern UINT1           gu1DvmrpStatus;      /* DVMRP Status */
extern UINT4           gu4GenerationId;
extern UINT4           gu4NumRoutes;        /* Number of routes in route tbl. */
extern UINT4           gu4NumReachableRoutes;
extern UINT4           gu4MinMTU;
extern tIfaceList     *gpFreeIfacePtr;
extern tNbrList       *gpFreeNbrPtr;
extern tForwCacheTbl  *gpForwFreePtr;

extern tDVMRP_HASH_TABLE  *gpForwHashTable;
extern tDVMRP_HASH_TABLE  *gpRouteHashTable;

extern tDpConfigParam     gDpConfigParams;
extern UINT4              gu4NumActiveIfaces;
extern tTimerListId        g1sTimerListId;
extern INT4                gi4SnmpReturnFlag;
extern INT4                gi4GetFirstFlag;

extern tOsixTaskId   gu4DvmrpTaskId;
extern tOsixQId      gDvIqId;

#ifdef LNXIP4_WANTED
extern INT4         gi4DvmrpDevFd;
#endif

/* dputil.c */
extern INT1 DvmrpUtilValidHost ARG_LIST ((UINT4 u4IpAddr));
extern UINT1 *DvmrpUtilFormat ARG_LIST ((UINT4 u4Addr, UINT1 * a));
extern UINT4 DvmrpUtilCheckSum ARG_LIST ((UINT1 * pBuf, INT2 i2Length));
extern tForwCacheTbl *DvmrpUtilGetForwCacheEntry ARG_LIST ((UINT4, UINT4));
extern void Log1 (UINT1, UINT4, const CHR1 *);
extern void Log2 (UINT1, UINT4, const CHR1 *, UINT4);

/* dpnbr.c */
extern void DvmrpNdmSendProbes ARG_LIST ((UINT1));
extern void DvmrpNdmSndProbeOnIface ARG_LIST ((tDvmrpInterfaceNode *pIfNode));

extern void DvmrpNdmHandleProbePacket ARG_LIST ((UINT4 u4DataLen,
                                                 UINT4 u4IfId,
                                                 UINT4 u4SrcAddr,
                                                 UINT1 u1NbrCap));

extern void DvmrpNdmHandleIfaceDown ARG_LIST ((UINT4 u4IfId));

extern void DvmrpNdmNbrTimerExpHandler ARG_LIST ((tDvmrpTimer *pTimerBlk));

extern void DvmrpNdmSendProbesOnTimerExpiry ARG_LIST ((tDvmrpTimer *pTimerBlk));

/* dpfwd.c */
extern UINT4 DvmrpFmSearchRouteTable ARG_LIST ((UINT4));
extern void DvmrpFmDeleteSrcNetInfo ARG_LIST ((UINT4 u4SrcNetwork, 
                                               UINT4 u4Mask));

extern void DvmrpFmCacheTimeOutHandler ARG_LIST ((tDvmrpTimer *pTimerBlk));

extern UINT4 DvmrpFmDeleteIfList ARG_LIST ((tForwTbl * pForwTbl, 
                                            UINT4 u4IfId, UINT1 u1Flag));

extern INT4 DvmrpFmAddifList ARG_LIST ((tForwTbl * pForwTbl, UINT4 u4IfId,
                                        tNbrAddrList * pNbrs));
extern UINT4 DvmrpFmDeleteifNbr ARG_LIST ((tForwTbl * pForwTbl, UINT4 u4IfId,
                                           UINT4 u4NbrAddr));
extern INT4 DvmrpFmAddifNbr ARG_LIST ((tForwTbl * pForwTbl, UINT4 u4IfId,
                                       UINT4 u4NbrAddr));

extern void DvmrpFmResetPruneStatus ARG_LIST ((UINT4 u4IfId, UINT4 u4NbrAddr));

extern INT4 DvmrpFmHandleMcastData ARG_LIST ((UINT4 u4Srcaddr,
                                              UINT4 u4DestAddr,
                                              tDvmrpInterfaceNode *pIfNode,
                                              UINT4 * pIntList));

/* dpprune.c */

extern void DvmrpPmHandlePrunePacket ARG_LIST ((UINT4 u4IfId, UINT4 u4Len,
                                                tNbrTbl * pNbrTbl));

extern void DvmrpPmSendPruneOnHmReport ARG_LIST ((UINT4 u4IfId,
                                                  UINT4 u4GroupAddr));

extern void DvmrpPmSendPruneOnIfaceChange ARG_LIST ((UINT4 u4IfId, UINT4
                                                     u4SrcNetwork));

extern void DvmrpPmSendPruneOnNbrChange ARG_LIST ((UINT4 u4IfId, UINT4
                                                   u4SrcNetwork,
                                                   UINT4 u4NbrAddr));

extern void DvmrpPmSendPruneUpstream ARG_LIST ((tForwCacheTbl * pFwdCache,
                                                UINT4 u4PruneMinTime));

extern void DvmrpPmHandleRetransTimerEvent ARG_LIST ((tDvmrpTimer *pTimerBlk));

/* dpgraft.c */

extern void DvmrpGmHandleGraftPacket ARG_LIST ((UINT4 u4IfId, UINT4 u4Len,
                                                tNbrTbl * pNbrTbl));

extern void DvmrpGmHandleGraftAckPacket ARG_LIST ((UINT4 u4IfId, UINT4 u4Len,
                                                   tNbrTbl * pNbrTbl));

extern void DvmrpGmSendGraftOnHmReport ARG_LIST ((UINT4 u4IfId,
                                                  UINT4 u4GroupAddr));
extern void DvmrpGmSendGraftOnNbrChange ARG_LIST ((UINT4 u4IfId, UINT4
                                                   u4SrcNetwork,
                                                   UINT4 u4NbrAddr));

extern void DvmrpGmSendGraftOnIfaceChange ARG_LIST ((UINT4 u4IfId,
                                                     UINT4 u4SrcNetwork,
                                                     tNbrAddrList * pNbr));

extern void DvmrpGmHandleRetransTimerEvent ARG_LIST ((tDvmrpTimer *pTimerBlk));


/* dphostmbr.c */

extern void DvmrpHmHandleIgmpHostInfo ARG_LIST ((UINT4 u4IfId, UINT4
                                                 u4GroupAddr, UINT1 u1Flag));

/* dpport.c */

extern INT4 DvmrpTimersAlloc ARG_LIST ((void));

extern INT4 DvmrpSpawnTask ARG_LIST ((void));

extern INT4 DvmrpSendEvent ARG_LIST ((UINT4 u4Event));

extern INT4 DvmrpCurrentTime ARG_LIST ((void));

extern void DvmrpFreeTimersAndDelayTask ARG_LIST ((void));

extern INT4 DvmrpStartTimer ARG_LIST ((tTmrAppTimer *pTimerRef, UINT2 u2Duration));

extern INT4 DvmrpStopTimer ARG_LIST ((tTmrAppTimer *pTimerRef));

extern void DvmrpHandleTimers (tTimerListId);


/* dpinput.c */

extern INT4  DvmrpIhmHandleStartup ARG_LIST ((void));


extern void DvmrpIhmProcessProtocolPackets ARG_LIST ((UINT4 u4ValidOffset,
                                                      UINT4 u4IfIndex));

extern void DvmrpIhmProcessHostPackets ARG_LIST ((void));
extern void DvmrpIhmHandleInterfaceUpdation ARG_LIST 
                                       ((tDvmrpIfStatus *pIfStatus));


/* dpoutput.c */

extern void DvmrpOmSendPacket ARG_LIST ((UINT4 u4IfId, UINT4 u4DestAddr,
                                         UINT4 u4Length, UINT1 u1Code));
