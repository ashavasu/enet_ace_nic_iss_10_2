/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dpglob.h,v 1.5 2007/04/11 12:15:06 iss Exp $
 *
 * Description: This file contains all the global definitions 
 *
 *******************************************************************/
#ifndef _DPGLOB_H
#define _DPGLOB_H

tDvmrpInterfaceInfo gDvmrpIfInfo;

/* Dvmrp Timers list Id */
/* ------------------ */
tTimerListId gDpTmrListId;
tDvmrpSystemSize    gDvmrpSystemSize;

tDvmrpGlobalMemPoolId gDvmrpMemPool;  /* Global Mem PoolId Table       */

tForwTbl       **gpForwTable;
tForwCacheTbl  **gpForwCacheTable;
tHostMbrTbl    **gpHostMbrTable;
tNbrTbl        **gpNeighborTable;
tRouteTbl      **gpRouteTable;
tNextHopTbl    **gpNextHopTable;
tIfaceList     **gpGlobIfacePtr;
tNbrList       **gpGlobNbrPtr;

tRouteTbl      *gpRouteFreePtr; /* Pointer to free entry in route tbl */
tIfaceList     *gpFreeIfacePtr;
tNbrList       *gpFreeNbrPtr;
tForwCacheTbl  *gpForwFreePtr;

UINT4           gu4MaxInterfaces;
UINT1          *gpSendBuffer;
UINT1          *gpRcvBuffer;
UINT1          *gpRcvBufferRead;
tDvmrpTimer    gTimerList[DVMRP_MAX_TABLE_TIMERS];
UINT1           gu1DvmrpLogEnabled;
UINT4           gu4DvmrpLogMask;
UINT1           gu1DvmrpStatus;      /* DVMRP Status */
UINT4           gu4GenerationId;
UINT4           gu4NumRoutes;        /* Number of routes in route tbl. */
UINT4           gu4NumReachableRoutes;
UINT4           gu4MinMTU;

tDVMRP_HASH_TABLE  *gpForwHashTable;
tDVMRP_HASH_TABLE  *gpRouteHashTable;

#ifdef FILE_LOG
FILE     *logfp;
#endif

UINT4                  gu4NumActiveIfaces;

/* 
 *  Added this global variable for porting
 */
tTimerListId  g1sTimerListId;
tOsixTaskId   gu4DvmrpTaskId;
tOsixQId      gDvIqId;
UINT1         gu1TimerFlag;

tDpConfigParam gDpConfigParams;

#endif /* _DPGLOB_H */
