#include "elminc.h"
#include "fselmilw.h"
#include "fselmicli.h"
/*$Id: elmclilow.c,v 1.10 2013/12/16 15:28:04 siva Exp $*/
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetGlobalModuleStatus                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the module status               */
/*                        Enabled) of the ELMIModule.                        */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4ELMModStatus- ELMI Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliSetGlobalModuleStatus (tCliHandle CliHandle, UINT4 u4ElmModStatus)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiModuleStatus (&u4ErrCode, u4ElmModStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsElmiModuleStatus (u4ElmModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliClearStat                                    */
/*                                                                           */
/*     DESCRIPTION      : This function Clear the statistics of all the      */
/*                        Counter to Zero.                                   */
/*                                                                           */
/*     INPUT            :  u24IfIndex - PortNumber                           */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliClearStat (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pElmPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    pElmPortEntry->u4ElmiRxElmiCheckEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxFullStatusEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxFullStatusContdEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxElmiCheckMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxFullStatusMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxFullStatusContdMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxAsyncStatusMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxElmiCheckEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxFullStatEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiTxFullStatContdEnquiryMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxCheckMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxFullStatMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxFullStatContdMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRxAsyncStatusMsg = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiValidMsgRxed = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiInvalidMsgRxed = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRelErrStatusTimeoutCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRelErrInvalidSeqNumCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRelErrInvalidStatusCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiRelErrRxUnsolicitedStatusCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrInvalidProtVerCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrInvalidEvcRefIdCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrInvalidMessageTypeCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrOutOfSequenceInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrDuplicateInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrMandatoryInfoEleMissingCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrInvalidMandatoryInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrInvalidNonMandatoryInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrUnrecognizedInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrUnexpectedInfoEleCount = ELM_INIT_VAL;
    pElmPortEntry->u4ElmiProErrShortMessageCount = ELM_INIT_VAL;

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmSetTrace                                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Trace support               */
/*                        for the ELMI                                       */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  - Handle to the CLI Context            */
/*                        u4ElmDebug  - Trace Value                          */
/*                        u1Action    - Set/No Command                       */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
ElmSetTrace (tCliHandle CliHandle, UINT4 u4ElmTrc, UINT1 u1Action)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;
    UINT4               u4TrcVal = ELM_INIT_VAL;

    if (u1Action == 0)
    {
        /* For Trace Disable, Get the existing Trace Value 
         * and negate that value
         */
        nmhGetFsElmiTraceOption ((INT4 *) &u4TrcVal);
        if (u4ElmTrc == ELM_ALL_TRC)
        {
            u4TrcVal = ELM_INIT_VAL;
        }
        else
        {
            u4TrcVal = u4TrcVal & (~u4ElmTrc);
        }
    }
    else if (u1Action == 1)
    {
        u4TrcVal = (INT4) u4ElmTrc;
    }

    if (nmhTestv2FsElmiTraceOption (&u4ErrCode, (INT4) u4TrcVal) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsElmiTraceOption ((INT4) u4TrcVal) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliDisplayDetails                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface Details      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Index-Interface Index                            */
/*                        u4Value-Interface Info to be displayed             */
/*                          u4Value-                                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliDisplayDetails (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Type)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    INT4                i4StatusCounterValue = ELM_INIT_VAL;
    INT4                i4PollingTimerValue = ELM_INIT_VAL;
    INT4                i4PollingVerificationTimerValue = ELM_INIT_VAL;
    INT4                i4PollingCounterValue = ELM_INIT_VAL;
    INT4                i4ElmiUniSide = ELM_INIT_VAL;
    INT4                i4NoOfConfiguredEvc = ELM_INIT_VAL;
    INT4                i4ElmiCheckRxEnqMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusRxEnqMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusContinuedRxEnqMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiCheckTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusContinuedTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiAsynchronousTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiCheckRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusContinuedRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiAsynchronousRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiCheckTxEnqMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusEnqTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiFullStatusContinuedEnqTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiReceivedValidMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiReceivedInValidMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiErrStatusTimeOutMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInvalidSeqNoTxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInvalidStatusResponceMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiUnsolicatedStatusRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInValidProtVerMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInvalidRefIdRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInvalidMessageTypeMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiOutOfSequenceIeMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiDuplicateIeMsgCount = ELM_INIT_VAL;
    INT4                i4ElmimandoteryIeMissingMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInValidMandoteryIeMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiInvalidNonMandoteryIeMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiUnrecognisedRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiUnexpectedRxMsgCount = ELM_INIT_VAL;
    INT4                i4ElmiShortMsgCount = ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4Index) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (i4Index != ELM_INIT_VAL)
    {
        pElmPortEntry = ELM_GET_PORTENTRY ((UINT2) i4Index);
        if (pElmPortEntry == NULL)
        {

            CliPrintf (CliHandle, "\r%% ELMI Module is not Enabled on "
                       " this Port \r\n");
            return (CLI_FAILURE);
        }
    }

    switch (u4Type)
    {
        case ELMI_EVC_PARAMETER_INFO:
            /*version is remaing */
            /*                u1Version  MEF.16-0106 */
            nmhGetFsElmiStatusCounter (i4Index, &i4StatusCounterValue);
            nmhGetFsElmiPollingVerificationTimerValue (i4Index,
                                                       &i4PollingVerificationTimerValue);
            nmhGetFsElmiPollingTimerValue (i4Index, &i4PollingTimerValue);
            nmhGetFsElmiPollingCounterValue (i4Index, &i4PollingCounterValue);
            nmhGetFsElmiUniSide (i4Index, &i4ElmiUniSide);
            break;

        case ELMI_EVC_STATISTICS_INFO:
            /*API that will give UNI status UNI ID */
            nmhGetFsElmiNoOfConfiguredEvcs (i4Index, &i4NoOfConfiguredEvc);
            nmhGetFsElmiRxElmiCheckEnqMsgCount (i4Index,
                                                (UINT4 *)
                                                &i4ElmiCheckRxEnqMsgCount);
            nmhGetFsElmiRxFullStatusEnqMsgCount (i4Index,
                                                 (UINT4 *)
                                                 &i4ElmiFullStatusRxEnqMsgCount);
            nmhGetFsElmiRxFullStatusContEnqMsgCount (i4Index,
                                                     (UINT4 *)
                                                     &i4ElmiFullStatusContinuedRxEnqMsgCount);
            nmhGetFsElmiTxElmiCheckMsgCount (i4Index,
                                             (UINT4 *) &i4ElmiCheckTxMsgCount);
            nmhGetFsElmiTxFullStatusMsgCount (i4Index,
                                              (UINT4 *)
                                              &i4ElmiFullStatusTxMsgCount);
            nmhGetFsElmiTxFullStatusContMsgCount (i4Index,
                                                  (UINT4 *)
                                                  &i4ElmiFullStatusContinuedTxMsgCount);
            nmhGetFsElmiTxAsyncStatusMsgCount (i4Index,
                                               (UINT4 *)
                                               &i4ElmiAsynchronousTxMsgCount);
            nmhGetFsElmiRxElmiCheckMsgCount (i4Index,
                                             (UINT4 *) &i4ElmiCheckRxMsgCount);
            nmhGetFsElmiRxFullStatusMsgCount (i4Index,
                                              (UINT4 *)
                                              &i4ElmiFullStatusRxMsgCount);
            nmhGetFsElmiRxFullStatusContMsgCount (i4Index,
                                                  (UINT4 *)
                                                  &i4ElmiFullStatusContinuedRxMsgCount);
            nmhGetFsElmiRxAsyncStatusMsgCount (i4Index,
                                               (UINT4 *)
                                               &i4ElmiAsynchronousRxMsgCount);
            nmhGetFsElmiTxElmiCheckEnqMsgCount (i4Index,
                                                (UINT4 *)
                                                &i4ElmiCheckTxEnqMsgCount);
            nmhGetFsElmiTxFullStatusEnqMsgCount (i4Index,
                                                 (UINT4 *)
                                                 &i4ElmiFullStatusEnqTxMsgCount);
            nmhGetFsElmiTxFullStatusContEnqMsgCount (i4Index,
                                                     (UINT4 *)
                                                     &i4ElmiFullStatusContinuedEnqTxMsgCount);
            nmhGetFsElmiRxValidMsgCount (i4Index,
                                         (UINT4 *)
                                         &i4ElmiReceivedValidMsgCount);
            nmhGetFsElmiRxInvalidMsgCount (i4Index,
                                           (UINT4 *)
                                           &i4ElmiReceivedInValidMsgCount);
            nmhGetFsElmiRelErrStatusTimeOutCount (i4Index,
                                                  (UINT4 *)
                                                  &i4ElmiErrStatusTimeOutMsgCount);
            nmhGetFsElmiRelErrInvalidSeqNumCount (i4Index,
                                                  (UINT4 *)
                                                  &i4ElmiInvalidSeqNoTxMsgCount);
            nmhGetFsElmiRelErrInvalidStatusRespCount (i4Index,
                                                      (UINT4 *)
                                                      &i4ElmiInvalidStatusResponceMsgCount);
            nmhGetFsElmiRelErrRxUnSolicitedStatusCount (i4Index,
                                                        (UINT4 *)
                                                        &i4ElmiUnsolicatedStatusRxMsgCount);
            nmhGetFsElmiProErrInvalidProtVerCount (i4Index,
                                                   (UINT4 *)
                                                   &i4ElmiInValidProtVerMsgCount);
            nmhGetFsElmiProErrInvalidEvcRefIdCount (i4Index,
                                                    (UINT4 *)
                                                    &i4ElmiInvalidRefIdRxMsgCount);
            nmhGetFsElmiProErrInvalidMessageTypeCount (i4Index,
                                                       (UINT4 *)
                                                       &i4ElmiInvalidMessageTypeMsgCount);
            nmhGetFsElmiProErrOutOfSequenceInfoEleCount (i4Index,
                                                         (UINT4 *)
                                                         &i4ElmiOutOfSequenceIeMsgCount);
            nmhGetFsElmiProErrDuplicateInfoEleCount (i4Index,
                                                     (UINT4 *)
                                                     &i4ElmiDuplicateIeMsgCount);
            nmhGetFsElmiProErrMandatoryInfoEleMissingCount (i4Index,
                                                            (UINT4 *)
                                                            &i4ElmimandoteryIeMissingMsgCount);
            nmhGetFsElmiProErrInvalidMandatoryInfoEleCount (i4Index,
                                                            (UINT4 *)
                                                            &i4ElmiInValidMandoteryIeMsgCount);
            nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount (i4Index,
                                                               (UINT4 *)
                                                               &i4ElmiInvalidNonMandoteryIeMsgCount);
            nmhGetFsElmiProErrUnrecognizedInfoEleCount (i4Index,
                                                        (UINT4 *)
                                                        &i4ElmiUnrecognisedRxMsgCount);
            nmhGetFsElmiProErrUnexpectedInfoEleCount (i4Index,
                                                      (UINT4 *)
                                                      &i4ElmiUnexpectedRxMsgCount);
            nmhGetFsElmiProErrShortMessageCount (i4Index,
                                                 (UINT4 *)
                                                 &i4ElmiShortMsgCount);

            break;

    }
    switch (u4Type)
    {
        case ELMI_EVC_PARAMETER_INFO:

            if (ELM_GET_PORT_MODE ((UINT2) i4Index) == ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle,
                           "\t\t\tPORT IS IN CUSTOMER MODE\n\n\n\n\n");
                CliPrintf (CliHandle,
                           "\t\t\tELMI-PARAMETER For Interface Ethernet 0/%d\n\n\n",
                           i4Index);
                CliPrintf (CliHandle, "\t\tVersion     :- MEF-16\n");
                CliPrintf (CliHandle, "\t\tMode        :- CE\n");
                CliPrintf (CliHandle, "\t\tt391        :- %dsec\n",
                           i4PollingTimerValue);
                CliPrintf (CliHandle, "\t\tt392        :- NE\n");
                CliPrintf (CliHandle, "\t\tN391        :- %d\n",
                           i4PollingCounterValue);
                CliPrintf (CliHandle, "\t\tN393        :- %d\n",
                           i4StatusCounterValue);
            }
            if (ELM_GET_PORT_MODE ((UINT2) i4Index) == ELM_NETWORK_SIDE)
            {
                CliPrintf (CliHandle,
                           "\t\t\tPORT IS IN NETWORK MODE\n\n\n\n\n");
                CliPrintf (CliHandle,
                           "\t\tELMI-PARAMETER For Interface Ethernet 0/%d\n\n\n",
                           i4Index);
                CliPrintf (CliHandle, "\t\tVersion     :- MEF-16\n");
                CliPrintf (CliHandle, "\t\tMode        :- NM\n");
                CliPrintf (CliHandle, "\t\tt391        :- NE\n");
                CliPrintf (CliHandle, "\t\tt392        :- %dsec\n",
                           i4PollingVerificationTimerValue);
                CliPrintf (CliHandle, "\t\tN391        :- NE\n");
                CliPrintf (CliHandle, "\t\tN393        :- %d\n",
                           i4StatusCounterValue);
            }

            break;
        case ELMI_EVC_STATISTICS_INFO:

            if (ELM_GET_PORT_MODE ((UINT2) i4Index) == ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle,
                           "\t\t\tPORT IS IN CUSTOMER MODE\n\n\n\n\n");
                CliPrintf (CliHandle,
                           "\tRx Elmi Check Message Count \t\t\t%d\n ",
                           i4ElmiCheckRxMsgCount);
                CliPrintf (CliHandle,
                           "\tRx Full Status Message Count\t\t\t%d\n",
                           i4ElmiFullStatusRxMsgCount);
                CliPrintf (CliHandle,
                           "\tRx Full Status Continued Message Count \t\t%d \n",
                           i4ElmiFullStatusContinuedRxMsgCount);
                CliPrintf (CliHandle,
                           "\tRx Asynchronous Message Count\t\t\t%d\n ",
                           i4ElmiAsynchronousRxMsgCount);
                CliPrintf (CliHandle, "\tRx Invalid Message Count \t\t\t%d\n",
                           i4ElmiReceivedInValidMsgCount);
                CliPrintf (CliHandle, "\tRx Valid Message Count \t\t\t\t%d\n ",
                           i4ElmiReceivedValidMsgCount);
                CliPrintf (CliHandle,
                           "\n\tTx Elmi Check Enq Message Count\t\t\t%d\n ",
                           i4ElmiCheckTxEnqMsgCount);
                CliPrintf (CliHandle,
                           "\tTx Full Status Enq Message Count \t\t%d\n",
                           i4ElmiFullStatusEnqTxMsgCount);
                CliPrintf (CliHandle,
                           "\tTx Full Status Continued Enq Message Count \t%d\n ",
                           i4ElmiFullStatusContinuedEnqTxMsgCount);
                CliPrintf (CliHandle,
                           "\n\tNo Of Invalid Status Resp Count \t\t%d\n  ",
                           i4ElmiInvalidStatusResponceMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Unsolicited Message Count \t\t%d\n  ",
                           i4ElmiUnsolicatedStatusRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invalid Sequence Number Count \t\t%d  \n",
                           i4ElmiInvalidSeqNoTxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invaild Protocol Version Count \t\t%d\n",
                           i4ElmiInValidProtVerMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invalid Refrence Id Count\t\t\t%d\n ",
                           i4ElmiInvalidRefIdRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invalid Message Type Count \t\t%d  \n",
                           i4ElmiInvalidMessageTypeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Out Of Sequence Message Count \t\t%d\n",
                           i4ElmiOutOfSequenceIeMsgCount);
                CliPrintf (CliHandle, "\tNo Of Duplicate IE Count \t\t\t%d\n ",
                           i4ElmiDuplicateIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Mandatory IE missing  Count \t%d \n",
                           i4ElmimandoteryIeMissingMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Invalid Mandatory IE Count \t%d\n",
                           i4ElmiInValidMandoteryIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Invalid NonMandatory IE Count  %d\n  ",
                           i4ElmiInvalidNonMandoteryIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message With Unrecognized IE Count \t%d\n",
                           i4ElmiUnrecognisedRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message With Unexpected IE Count \t%d\n ",
                           i4ElmiUnexpectedRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Status Time Out Count \t\t\t%d\n ",
                           i4ElmiErrStatusTimeOutMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Short MessageRx Count \t\t\t%d\n ",
                           i4ElmiShortMsgCount);
                CliPrintf (CliHandle, "\tNo Of EVC Count \t\t\t\t%d \n\n",
                           i4NoOfConfiguredEvc);
            }

            if (ELM_GET_PORT_MODE ((UINT2) i4Index) == ELM_NETWORK_SIDE)
            {

                CliPrintf (CliHandle,
                           "\t\t\tPORT IS IN NETWORK MODE\n\n\n\n\n");
                CliPrintf (CliHandle,
                           "\tRx Elmi Check Enq Message Count \t\t%d\n",
                           i4ElmiCheckRxEnqMsgCount);
                CliPrintf (CliHandle,
                           "\tRx Elmi Full Status Enq Message Count \t\t%d\n ",
                           i4ElmiFullStatusRxEnqMsgCount);
                CliPrintf (CliHandle,
                           "\tRx Elmi Full Status Cont Enq Message Count \t%d\n",
                           i4ElmiFullStatusContinuedRxEnqMsgCount);
                CliPrintf (CliHandle, "\tRx Valid Message Count \t\t\t\t%d\n  ",
                           i4ElmiReceivedValidMsgCount);
                CliPrintf (CliHandle, "\tRx Invalid Message Count \t\t\t%d\n",
                           i4ElmiReceivedInValidMsgCount);
                CliPrintf (CliHandle,
                           "\n\t Tx Elmi Check Message Count \t\t\t%d\n ",
                           i4ElmiCheckTxMsgCount);
                CliPrintf (CliHandle,
                           "\tTx Full Status Message Count \t\t\t%d\n ",
                           i4ElmiFullStatusTxMsgCount);
                CliPrintf (CliHandle,
                           "\tTx Full Status Continued Message Count \t\t%d\n",
                           i4ElmiFullStatusContinuedTxMsgCount);
                CliPrintf (CliHandle,
                           "\tTx Asynchronous Message Count \t\t\t%d\n ",
                           i4ElmiAsynchronousTxMsgCount);
                CliPrintf (CliHandle,
                           "\n\tNo Of Invalid Sequence Number Count \t\t%d\n  ",
                           i4ElmiInvalidSeqNoTxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invaild Protocol Version Count \t\t%d\n",
                           i4ElmiInValidProtVerMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invalid Refrence Id Count \t\t%d \n",
                           i4ElmiInvalidRefIdRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Invalid Message Type Count \t\t%d\n  ",
                           i4ElmiInvalidMessageTypeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Out Of Sequence Message Count \t\t%d\n",
                           i4ElmiOutOfSequenceIeMsgCount);
                CliPrintf (CliHandle, "\tNo Of Duplicate IE Count \t\t\t%d \n",
                           i4ElmiDuplicateIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Mandatory IE missing  Count \t%d\n ",
                           i4ElmimandoteryIeMissingMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Invalid Mandatory IE Count \t%d\n",
                           i4ElmiInValidMandoteryIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message Invalid NonMandatory IE Count \t%d \n",
                           i4ElmiInvalidNonMandoteryIeMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message With Unrecognized IE Count \t%d\n",
                           i4ElmiUnrecognisedRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Rx Message With Unexpected IE Count \t%d\n ",
                           i4ElmiUnexpectedRxMsgCount);
                CliPrintf (CliHandle,
                           "\tNo Of Short MessageRx Count \t\t\t%d\n ",
                           i4ElmiShortMsgCount);
            }
            break;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliShowStatus                                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays ELMI's Status               */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Index-Interface Index                            */
/*                        u4Value-Interface Info to be displayed             */
/*                          u4Value-                                         */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliShowStatus (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Type)
{
    INT4                i4ModStatus = ELM_INIT_VAL;
    INT1                i1OutCome = (INT1) ELM_INIT_VAL;
    INT4                i4NextPort = (INT4) ELM_INIT_VAL;
    INT4                i4CurrentPort = (INT4) ELM_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    /* Module Status */
    nmhGetFsElmiModuleStatus (&i4ModStatus);
    if (i4ModStatus == ELM_ENABLE_GLOBAL)
    {
        CliPrintf (CliHandle, "\r\nELMI Status    : Started, Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nELMI Status    : Started, Disabled\r\n");
    }

    switch (u4Type)
    {

        case ELMI_STATUS_ALL:
            i1OutCome = nmhGetFirstIndexFsElmiPortTable (&i4NextPort);

            while ((i1OutCome != SNMP_FAILURE)
                   && (i4NextPort <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
            {
                if (ElmValidatePortEntry (i4NextPort) != ELM_SUCCESS)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                             "Port Does Not exist!\n");
                    i4CurrentPort = i4NextPort;
                    i1OutCome =
                        nmhGetNextIndexFsElmiPortTable (i4CurrentPort,
                                                        &i4NextPort);
                    continue;
                }

                ELM_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                ElmShowPortStatsDetails (CliHandle, i4NextPort);

                CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
                ELM_LOCK ();

                u4PagingStatus = CliPrintf (CliHandle, "\r\n");

                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User Presses 'q' at the prompt, so break! */
                    break;
                }

                i4CurrentPort = i4NextPort;
                i1OutCome =
                    nmhGetNextIndexFsElmiPortTable (i4CurrentPort, &i4NextPort);

            }

            break;

        case ELMI_STATUS_INTF:
            ELM_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            ElmShowPortStatsDetails (CliHandle, i4Index);

            CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
            ELM_LOCK ();

            break;
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : ElmShowPortStatsDetails                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in Elmi   */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4IfIndex-Interface Index                          */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
ElmShowPortStatsDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4ElmPortElmiStatus = (INT4) ELM_INIT_VAL;
    INT4                i4ElmiUniSide = (INT4) ELM_INIT_VAL;
    INT4                i4StatusCounterValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingTimerValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingCounterValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingVerificationTimerValue = (INT4) ELM_INIT_VAL;
    tElmPortEntry      *pElmPortEntry = NULL;

    /* Needed this check when called from Cfa */

    if (ELM_IS_ELMI_STARTED ())
    {
        CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
        ELM_LOCK ();

        if (nmhValidateIndexInstanceFsElmiPortTable (i4IfIndex) == SNMP_SUCCESS)
        {
            if (i4IfIndex != 0)
            {
                pElmPortEntry = ELM_GET_PORTENTRY (i4IfIndex);
                if (pElmPortEntry == NULL)
                {

                    ELM_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return;
                }
            }

            CliPrintf (CliHandle,
                       "\r\n-------------------------------------------\r\n");
            CliPrintf (CliHandle, "interface gigabitethernet 0/%d ", i4IfIndex);
            CliPrintf (CliHandle,
                       "\r\n-------------------------------------------\r\n");

            /*Module Enable */
            nmhGetFsElmiPortElmiStatus (i4IfIndex, &i4ElmPortElmiStatus);
            if (i4ElmPortElmiStatus == (INT4) ELM_ENABLE_PORT)
            {
                CliPrintf (CliHandle,
                           "ELMI Port Status           : Enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "ELMI Port Status           : Disabled\r\n");
            }

            /*ELMI Uni Side */
            nmhGetFsElmiUniSide (i4IfIndex, &i4ElmiUniSide);
            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle, "ELMI UNI side              : UNI-C\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "ELMI UNI side              : UNI-N\r\n");
            }

            /*ELMI Polling Timer Value */
            nmhGetFsElmiPollingVerificationTimerValue (i4IfIndex,
                                                       &i4PollingVerificationTimerValue);

            if (i4ElmiUniSide == (INT4) ELM_NETWORK_SIDE)
            {
                CliPrintf (CliHandle, "Polling Verification Timer : %d Sec\r\n",
                           i4PollingVerificationTimerValue);
            }

            /*ELMI Polling Timer Value */
            nmhGetFsElmiPollingTimerValue (i4IfIndex, &i4PollingTimerValue);

            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle, "Pooling Timer              : %d Sec\r\n",
                           i4PollingTimerValue);
            }

            /*ELMI Status Counter Value */
            nmhGetFsElmiStatusCounter (i4IfIndex, &i4StatusCounterValue);
            CliPrintf (CliHandle, "Status Counter             : %d\r\n",
                       i4StatusCounterValue);

            /*ELMI Polling Counter Value */
            nmhGetFsElmiPollingCounterValue (i4IfIndex, &i4PollingCounterValue);

            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle, "Pooling Counter:           : %d\r\n",
                           i4PollingCounterValue);
            }
        }
        ELM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetPollingCounter                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--handle to the CLI Context              */
/*                        u4PollingCounterValue                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliSetPollingCounter (tCliHandle CliHandle, INT4 i4Index,
                         UINT4 u4PollingCounterValue)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiPollingCounterValue
        (&u4ErrCode, i4Index, u4PollingCounterValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Polling Counter for Port "
                   "in Network mode\r\n ");
        return (CLI_FAILURE);
    }

    if (nmhSetFsElmiPollingCounterValue (i4Index, u4PollingCounterValue) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Polling Counter for Port\r\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetStatusCounter                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--handle to the CLI Context              */
/*                        u4StatusCounterValue                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliSetStatusCounter (tCliHandle CliHandle, INT4 i4Index,
                        UINT4 u4StatusCounterValue)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiStatusCounter (&u4ErrCode, i4Index, u4StatusCounterValue)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Status Counter for Port\r\n ");
        return (CLI_FAILURE);
    }

    if (nmhSetFsElmiStatusCounter (i4Index, u4StatusCounterValue) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Status Counter for Port\r\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetPollingTimer                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--handle to the CLI Context              */
/*                        u4PollingTimerValue                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliSetPollingTimer (tCliHandle CliHandle, INT4 i4Index,
                       UINT4 u4PollingTimerValue)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiPollingTimerValue
        (&u4ErrCode, i4Index, u4PollingTimerValue) == SNMP_FAILURE)
    {
        if (u4ErrCode == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r%% Value out of range(5-30) \r\n ");
            return (CLI_FAILURE);
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot configure Polling Timer for Port "
                       "in Network mode\r\n ");
            return (CLI_FAILURE);
        }
    }

    if (nmhSetFsElmiPollingTimerValue (i4Index, u4PollingTimerValue) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Polling Timer for Port\r\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetPollingVerificationTimer                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Hello Time for              */
/*                        RSTP module                                        */
/*                                                                           */
/*     INPUT            : tCliHandle--handle to the CLI Context              */
/*                        u4PollingTimerValue                              */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
ElmCliSetPollingVerificationTimer (tCliHandle CliHandle, INT4 i4Index,
                                   UINT4 u4PollingVerificationTimerValue)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiPollingVerificationTimerValue (&u4ErrCode, i4Index,
                                                      u4PollingVerificationTimerValue)
        == SNMP_FAILURE)
    {
        if (u4ErrCode == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle,
                       "\r%% Value out of range, It should be 0 or between 5 and 30 \r\n ");
            return (CLI_FAILURE);
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot Configure Timer, Port configured as "
                       "Customer Edge\n\r");
            return (CLI_FAILURE);
        }
    }

    if (nmhSetFsElmiPollingVerificationTimerValue (i4Index,
                                                   u4PollingVerificationTimerValue)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Cannot Configure Polling Verification Timer for Port\r\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetPortModuleStatus                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the module status               */
/*                        Enabled) of the ELMIModule.                        */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4ELMModStatus- ELMI Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliSetPortModuleStatus (tCliHandle CliHandle, INT4 u4Index,
                           UINT4 u4ElmModStatus)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiPortElmiStatus (&u4ErrCode, u4Index, u4ElmModStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetFsElmiPortElmiStatus (u4Index, u4ElmModStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetCustomerMode                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port in UNI-C Mode.         */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4ELMModStatus- ELMI Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliSetCustomerMode (tCliHandle CliHandle, INT4 u4Index)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;
    UINT4               u4ElmPortStatus = ELM_INIT_VAL;

    u4ElmPortStatus = ELM_CUSTOMER_SIDE;
    if (nmhTestv2FsElmiUniSide (&u4ErrCode, u4Index, u4ElmPortStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhSetFsElmiUniSide (u4Index, u4ElmPortStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetNetworkMode                             */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port in UNI-C Mode.         */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4ELMModStatus- ELMI Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliSetNetworkMode (tCliHandle CliHandle, INT4 u4Index)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;
    UINT4               u4ElmPortStatus = ELM_INIT_VAL;

    u4ElmPortStatus = ELM_NETWORK_SIDE;
    if (nmhTestv2FsElmiUniSide (&u4ErrCode, u4Index, u4ElmPortStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhSetFsElmiUniSide (u4Index, u4ElmPortStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmCliSetSstemControl                             */
/*                                                                           */
/*     DESCRIPTION      : This function is used to start up ELMI   .         */
/*                                                                           */
/*     INPUT            : tCliHandle- handle to the CLI Context              */
/*                        u4ELMModStatus- ELMI Module status                 */
/*                                                                           */
/*     OUTPUT           :  NONE                                              */
/*                                                                           */
/*                                                                           */
/*     RETURNS          :  CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT4
ElmCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ElmSystemControl)
{
    UINT4               u4ErrCode = ELM_INIT_VAL;

    if (nmhTestv2FsElmiSystemControl (&u4ErrCode, u4ElmSystemControl) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhSetFsElmiSystemControl (u4ElmSystemControl) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElmShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  ELMI Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
ElmShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
    ELM_LOCK ();

    if (ElmShowRunningConfigScalars (CliHandle) == CLI_SUCCESS)
    {
        if (u4Module == ISS_ELMI_SHOW_RUNNING_CONFIG)
        {
            ElmShowRunningConfigInterface (CliHandle);
        }
    }

    ELM_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : ElmShowRunningConfigScalars                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in ELMI for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ElmShowRunningConfigScalars (tCliHandle CliHandle)
{
    INT4                i4ModStatus = (INT4) ELM_INIT_VAL;
    INT4                i4SystemControl = (INT4) ELM_INIT_VAL;

    /*SystemControl */
    nmhGetFsElmiSystemControl (&i4SystemControl);

    /*f (i4SystemControl == ELM_SNMP_START)
       {
       return CLI_SUCCESS;
       } */
    if (i4SystemControl == ELM_SNMP_SHUTDOWN)
    {
        return CLI_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "no shutdown ethernet lmi\r\n");
    }

    nmhGetFsElmiModuleStatus (&i4ModStatus);

    /*Module Status */

    if ((i4ModStatus == ELM_DISABLE_GLOBAL)
        && i4SystemControl != ELM_SNMP_SHUTDOWN)
    {
        CliPrintf (CliHandle, "no ethernet lmi global\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : ElmShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface table for  Elmi  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/

VOID
ElmShowRunningConfigInterface (tCliHandle CliHandle)
{

    INT1                i1OutCome = (INT1) ELM_INIT_VAL;
    INT4                i4NextPort = (INT4) ELM_INIT_VAL;
    INT4                i4CurrentPort = (INT4) ELM_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    i1OutCome = nmhGetFirstIndexFsElmiPortTable (&i4NextPort);

    while ((i1OutCome != SNMP_FAILURE)
           && (i4NextPort <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        if (ElmValidatePortEntry (i4NextPort) != ELM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "Port Does Not exist!\n");
            i4CurrentPort = i4NextPort;
            i1OutCome =
                nmhGetNextIndexFsElmiPortTable (i4CurrentPort, &i4NextPort);
            continue;
        }

        CliPrintf (CliHandle, "interface gigabitethernet 0/%d\r\n", i4NextPort);

        ELM_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        ElmShowRunningConfigInterfaceDetails (CliHandle, i4NextPort);

        CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
        ELM_LOCK ();

        CliPrintf (CliHandle, "!");
        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i1OutCome = nmhGetNextIndexFsElmiPortTable (i4CurrentPort, &i4NextPort);

    }

    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : ElmShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in Elmi   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
ElmShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4ElmPortElmiStatus = (INT4) ELM_INIT_VAL;
    INT4                i4ElmiUniSide = (INT4) ELM_INIT_VAL;
    INT4                i4StatusCounterValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingTimerValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingCounterValue = (INT4) ELM_INIT_VAL;
    INT4                i4PollingVerificationTimerValue = (INT4) ELM_INIT_VAL;
    tElmPortEntry      *pElmPortEntry = NULL;

    /* Needed this check when called from Cfa */

    if (ELM_IS_ELMI_STARTED ())
    {
        CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
        ELM_LOCK ();

        if (nmhValidateIndexInstanceFsElmiPortTable (i4IfIndex) == SNMP_SUCCESS)
        {
            if (i4IfIndex != 0)
            {
                pElmPortEntry = ELM_GET_PORTENTRY (i4IfIndex);
                if (pElmPortEntry == NULL)
                {

                    ELM_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return;
                }
            }
            /*Module Enable */
            nmhGetFsElmiPortElmiStatus (i4IfIndex, &i4ElmPortElmiStatus);
            if (i4ElmPortElmiStatus == (INT4) ELM_ENABLE_PORT)
            {
                CliPrintf (CliHandle, "ethernet lmi interface\r\n");
            }

            /*ELMI Uni Side */
            nmhGetFsElmiUniSide (i4IfIndex, &i4ElmiUniSide);
            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                CliPrintf (CliHandle, "ethernet lmi ce\r\n");
            }
            /*ELMI Status Counter Value */
            nmhGetFsElmiStatusCounter (i4IfIndex, &i4StatusCounterValue);
            if (i4StatusCounterValue != (INT4) ELM_DEFAULT_STATUS_COUNTER_VALUE)
            {
                CliPrintf (CliHandle, "ethernet lmi n393 %d\r\n",
                           i4StatusCounterValue);
            }
            /*ELMI Polling Counter Value */
            nmhGetFsElmiPollingCounterValue (i4IfIndex, &i4PollingCounterValue);

            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                if (i4PollingCounterValue !=
                    (INT4) ELM_DEFAULT_POLLING_COUNTER_VALUE)
                {
                    CliPrintf (CliHandle, "ethernet lmi n391 %d\r\n",
                               i4PollingCounterValue);
                }
            }

            /*ELMI Polling Timer Value */
            nmhGetFsElmiPollingTimerValue (i4IfIndex, &i4PollingTimerValue);

            if (i4ElmiUniSide == (INT4) ELM_CUSTOMER_SIDE)
            {
                if (i4PollingTimerValue != (INT4) ELM_DEFAULT_PT_TIMER_VALUE)
                {
                    CliPrintf (CliHandle, "ethernet lmi t391 %d\r\n",
                               i4PollingTimerValue);
                }
            }
            /*ELMI Polling Timer Value */
            nmhGetFsElmiPollingVerificationTimerValue (i4IfIndex,
                                                       &i4PollingVerificationTimerValue);

            if (i4ElmiUniSide == (INT4) ELM_NETWORK_SIDE)
            {
                if (i4PollingVerificationTimerValue !=
                    (INT4) ELM_DEFAULT_PVT_TIMER_VALUE)
                {
                    CliPrintf (CliHandle, "ethernet lmi t392 %d\r\n",
                               i4PollingVerificationTimerValue);
                }
            }
        }
        ELM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
    }
    return;
}
