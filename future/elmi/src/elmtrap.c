/******************************************************************** 
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved 
 * 
 * $Id: elmtrap.c,v 1.12 2014/03/18 11:58:38 siva Exp $ 
 * 
 * Description:This file contains routines for handling        
 *             different traps                             
 * *******************************************************************/

#include "elminc.h"
#include "fselmi.h"
static INT1         ai1TempBuffer[ELM_OBJECT_NAME_MAX_LENGTH + 1];

/*****************************************************************************/
/* Function Name      : ElmInvalidPduRxdTrap                                 */
/*                                                                           */
/* Description        : This routine generates trap message when invalid     */
/*                      ELMI PDU is received by UNI-C or UNI-N.              */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number on which Packet is received   */
/*                      u1ErrType - Type of packet error                     */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmInvalidPduRxdTrap (UINT4 u4IfIndex, UINT1 u1ErrType,
                      INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmInvalidMsgRxdTrap ElmInvalidMsgRxdTrap;

    ELM_MEMSET (&ElmInvalidMsgRxdTrap, (UINT1) ELM_INIT_VAL,
                sizeof (tElmInvalidMsgRxdTrap));

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the Invalid Message Received -Trap Structure information */
        ElmInvalidMsgRxdTrap.u4IfIndex = u4IfIndex;
        ElmInvalidMsgRxdTrap.u1ErrorType = u1ErrType;
        ElmSnmpIfSendTrap (ELM_INVALID_PDU_RXD_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmInvalidMsgRxdTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmMemFailTrap                                       */
/*                                                                           */
/* Description        : This routine generates trap message when memory      */
/*                      allocation Failure occurs                            */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmMemFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    /* Assumptions: 
     * Protocol Lock is already taken.
     */
    tElmBrgErrTraps     ElmBrgErrTraps;

    ELM_MEMSET (&ElmBrgErrTraps, (UINT1) ELM_INIT_VAL,
                sizeof (tElmBrgErrTraps));

    gElmGlobalInfo.u1ElmErrTrapType = ELM_TRAP_MEM_FAIL;

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_MEM_FAILURE_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the MemFailure -Trap Structure information */
        ElmBrgErrTraps.u1ErrTrapType = ELM_TRAP_MEM_FAIL;
        ElmSnmpIfSendTrap (ELM_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmBrgErrTraps);
    }
}

/*****************************************************************************/
/* Function Name      : ElmGlobalMemFailTrap                                 */
/*                                                                           */
/* Description        : This routine generates trap message when memory      */
/*                      allocation Failure occurs                            */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmGlobalMemFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmBrgErrTraps     ElmBrgErrTraps;

    ELM_MEMSET (&ElmBrgErrTraps, (UINT1) ELM_INIT_VAL,
                sizeof (tElmBrgErrTraps));
    gElmGlobalInfo.u1ElmErrTrapType = ELM_TRAP_MEM_FAIL;
    if (IS_ELM_BRG_ERR_EVNT_TRAP ())
    {
        ElmBrgErrTraps.u1ErrTrapType = ELM_TRAP_MEM_FAIL;

        if (ELM_SYSTEM_CONTROL != ELM_START)
        {
            ElmSnmpIfSendTrap (ELM_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                               u1TrapOidLen, (VOID *) &ElmBrgErrTraps);

        }
    }
}

/*****************************************************************************/
/* Function Name      : ElmBufferFailTrap                                    */
/*                                                                           */
/* Description        : This routine generates trap message when buffer      */
/*                      allocation occurs                                    */
/*                                                                           */
/* Input(s)           : pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmBufferFailTrap (INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmBrgErrTraps     ElmBrgErrTraps;

    ELM_MEMSET (&ElmBrgErrTraps, (UINT1) ELM_INIT_VAL,
                sizeof (tElmBrgErrTraps));
    gElmGlobalInfo.u1ElmErrTrapType = ELM_TRAP_BUF_FAIL;

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_MEM_FAILURE_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the Buffer Failure -Trap Structure information */
        ElmBrgErrTraps.u1ErrTrapType = ELM_TRAP_BUF_FAIL;
        ElmSnmpIfSendTrap (ELM_BRG_ERR_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmBrgErrTraps);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiPvtExpiredTrap                                   */
/*                                                                           */
/* Description        : This routine generates trap message when Polling     */
/*                      Verification Timer Expired at UNI-N side.            */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiPvtExpiredTrap (UINT4 u4IfIndex, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmPvtExpiredTrap  ElmPvtExpiredTrap;
    ELM_MEMSET (&ElmPvtExpiredTrap, (UINT1) ELM_INIT_VAL,
                sizeof (tElmPvtExpiredTrap));

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the PVT Expired -Trap Structure information */
        ElmPvtExpiredTrap.u4IfIndex = u4IfIndex;
        ElmPvtExpiredTrap.u1PvtStatus = ELM_PVT_EXPIRED;
        ElmSnmpIfSendTrap (ELM_PVT_EXP_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmPvtExpiredTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiPtExpiredTrap                                   */
/*                                                                           */
/* Description        : This routine generates trap message when Polling     */
/*                      Timer Expired at UNI-c side.                         */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiPtExpiredTrap (UINT4 u4IfIndex, INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmPtExpiredTrap   ElmPtExpiredTrap;
    ELM_MEMSET (&ElmPtExpiredTrap, (UINT1) ELM_INIT_VAL,
                sizeof (tElmPtExpiredTrap));

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the PT expired -Trap Structure information */
        ElmPtExpiredTrap.u4IfIndex = u4IfIndex;
        ElmPtExpiredTrap.u1PtStatus = ELM_PT_EXPIRED;
        ElmSnmpIfSendTrap (ELM_PT_EXP_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmPtExpiredTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiNewEvcCreatedTrap                                */
/*                                                                           */
/* Description        : This routine generates trap message when New Evc     */
/*                      is created at UNI-C side                             */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      *elmiEvcId:- Evc ID                                 */
 /*                      pTrapsOid - OID of the associated Trap Object       */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiNewEvcCreatedTrap (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
                       INT1 *pTrapsOid, UINT1 u1TrapOidLen)
{
    tElmEvcTrap         ElmEvcTrap;

    ELM_MEMSET (&ElmEvcTrap, (UINT1) ELM_INIT_VAL, sizeof (tElmEvcTrap));
    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the Evc Created -Trap Structure information */
        ElmEvcTrap.u4IfIndex = u4IfIndex;
        ElmEvcTrap.u1ElmiEvcStatus = ELM_NEW_EVC_CREATED;

        ELM_MEMCPY (ElmEvcTrap.ElmiEvcId, pelmiEvcId, ELM_MAX_EVC_LENGTH);

        ElmSnmpIfSendTrap (ELM_EVC_TRAP_VAL, pTrapsOid,
                           u1TrapOidLen, (VOID *) &ElmEvcTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiOldEvcDeletedTrap                                */
/*                                                                           */
/* Description        : This routine generates trap message when Evc         */
/*                      is Deleted at UNI-C side                             */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      *elmiEvcId:- Evc ID                                 */
 /*                      pi1TrapsOid - OID of the associated Trap Object     */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiOldEvcDeletedTrap (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
                       INT1 *pTrapsOid, UINT1 u1TrapOidLen)
{
    tElmEvcTrap         ElmEvcTrap;

    ELM_MEMSET (&ElmEvcTrap, (UINT1) ELM_INIT_VAL, sizeof (tElmEvcTrap));
    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the Evc Deleted -Trap Structure information */
        ElmEvcTrap.u4IfIndex = u4IfIndex;
        ElmEvcTrap.u1ElmiEvcStatus = ELM_OLD_EVC_DELETED;

        ELM_MEMCPY (ElmEvcTrap.ElmiEvcId, pelmiEvcId, ELM_MAX_EVC_LENGTH);

        ElmSnmpIfSendTrap (ELM_EVC_TRAP_VAL, pTrapsOid,
                           u1TrapOidLen, (VOID *) &ElmEvcTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiEvcStatusChangedTrap                             */
/*                                                                           */
/* Description        : This routine generates trap message when EVC status  */
/*                      is changed at UNI-C side.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      *elmiEvcId:- Evc ID                                  */
 /*                      pi1TrapsOid - OID of the associated Trap Object     */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiEvcStatusChangedTrap (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
                          INT1 *pTrapsOid, UINT1 u1TrapOidLen)
{
    tElmEvcTrap         ElmEvcTrap;

    ELM_MEMSET (&ElmEvcTrap, (UINT1) ELM_INIT_VAL, sizeof (tElmEvcTrap));
    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the EVC Status Changed -Trap Structure information */
        ElmEvcTrap.u4IfIndex = u4IfIndex;
        ElmEvcTrap.u1ElmiEvcStatus = ELM_EVC_STATUS_CHANGED;

        ELM_MEMCPY (ElmEvcTrap.ElmiEvcId, pelmiEvcId, ELM_MAX_EVC_LENGTH);

        ElmSnmpIfSendTrap (ELM_EVC_TRAP_VAL, pTrapsOid,
                           u1TrapOidLen, (VOID *) &ElmEvcTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiUniStatusChangedTrap                             */
/*                                                                           */
/* Description        : This routine generates trap message when UNI status  */
/*                      is changed at UNI-C side.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiUniStatusChangedTrap (UINT4 u4IfIndex,
                          INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmUniTrap         ElmUniTrap;

    ELM_MEMSET (&ElmUniTrap, (UINT1) ELM_INIT_VAL, sizeof (tElmUniTrap));

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the UNI-Trap Structure information */
        ElmUniTrap.u4IfIndex = u4IfIndex;
        ElmUniTrap.u1ElmiUniStatus = ELM_UNI_STATUS_CHANGED;
        ElmSnmpIfSendTrap (ELM_UNI_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmUniTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmiOperStatusChangedTrap                            */
/*                                                                           */
/* Description        : This routine generates trap message when ELMI        */
/*                      Operational status is changed                        */
/*                      is changed at UNI-C side.                            */
/*                                                                           */
/* Input(s)           : u4IfIndex -port number                               */
/*                      u1ElmOperStatus:-Operational status of ELMI          */
/*                      pi1TrapsOid - OID of the associated Trap Object      */
/*                      u1TrapOidLen - Length of the Trap OID                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmiOperStatusChangedTrap (UINT4 u4IfIndex, UINT1 u1ElmOperStatus,
                           INT1 *pi1TrapsOid, UINT1 u1TrapOidLen)
{
    tElmOperTrap        ElmOperTrap;

    ELM_MEMSET (&ElmOperTrap, (UINT1) ELM_INIT_VAL, sizeof (tElmOperTrap));

    if ((IS_ELM_BRG_ERR_EVNT () == ELM_PROTOCOL_TRAPS)
        || (IS_ELM_BRG_ERR_EVNT () == ELM_ALL_TRAPS))
    {
        /*Fill the OperStaus -Trap Structure information */
        ElmOperTrap.u4IfIndex = u4IfIndex;
        ElmOperTrap.u1ElmiOperStatus = u1ElmOperStatus;
        ElmSnmpIfSendTrap (ELM_OPER_STATUS_CHANGED_TRAP_VAL, pi1TrapsOid,
                           u1TrapOidLen, (VOID *) &ElmOperTrap);
    }
}

/*****************************************************************************/
/* Function Name      : ElmSnmpIfSendTrap                                    */
/*                                                                           */
/* Description        : This routine prepare Trap message by fetching OID   */
/*                      from ELMI MIB & Send to User.                       */
/*                                                                           */
/* Input              : u1TrapId - Trap Identifier                           */
/*                      pi1TrapOid -pointer to Trap OID                      */
/*                      u1OidLen - OID Length                                */
/*                      pTrapInfo - void pointer to the trap information     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                   VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tElmInvalidMsgRxdTrap *pElmInvalidMsgRxdTrap = NULL;
    tElmBrgErrTraps    *pElmBrgErrTraps = NULL;
    tElmPvtExpiredTrap *pElmPvtExpiredTrap = NULL;
    tElmPtExpiredTrap  *pElmPtExpiredTrap = NULL;
    tElmEvcTrap        *pElmEvcTrap = NULL;
    tElmUniTrap        *pElmUniTrap = NULL;
    tElmOperTrap       *pElmOperTrap = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    UINT4               u4GenTrapType = (UINT4) ELM_INIT_VAL;
    UINT4               u4SpecTrapType = (UINT4) ELM_INIT_VAL;
    UINT1               au1Buf[ELM_OBJECT_NAME_MAX_LENGTH];
    UINT1               u1EvcLength = (UINT1) ELM_INIT_VAL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    UNUSED_PARAM (u1OidLen);
    ELM_MEMSET (au1Buf, ELM_INIT_VAL, ELM_OBJECT_NAME_MAX_LENGTH);
    pEnterpriseOid = SNMP_AGT_GetOidFromString (pi1TrapOid);

    if (pEnterpriseOid == NULL)
    {
        return;
    }

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case ELM_INVALID_PDU_RXD_TRAP_VAL:

            pElmInvalidMsgRxdTrap = (tElmInvalidMsgRxdTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_ERR_TRAP_TYPE);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmInvalidMsgRxdTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmInvalidMsgRxdTrap->
                                            u1ErrorType, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Invalid ELMI PDU is received & Trap "
                          " is generated\n "));

            break;

        case ELM_BRG_ERR_TRAP_VAL:

            pElmBrgErrTraps = (tElmBrgErrTraps *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_ERR_TRAP_TYPE);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmBrgErrTraps->
                                            u1ErrTrapType, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Memory Failure Trap is generated\n "));
            break;

        case ELM_PVT_EXP_TRAP_VAL:

            pElmPvtExpiredTrap = (tElmPvtExpiredTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_PVT_STATUS_TRAP_INDEX);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmPvtExpiredTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmPvtExpiredTrap->
                                            u1PvtStatus, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Polling Verification Timer expired  "
                          " PVT Expired Trap is generated\n "));
            break;

        case ELM_PT_EXP_TRAP_VAL:

            pElmPtExpiredTrap = (tElmPtExpiredTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_PT_STATUS_TRAP_INDEX);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmPtExpiredTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmPtExpiredTrap->
                                            u1PtStatus, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Polling Timer is expired  "
                          " PT Expired Trap is generated\n "));
            break;

        case ELM_EVC_TRAP_VAL:

            pElmEvcTrap = (tElmEvcTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_EVC_ID_TRAP_INDEX);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmEvcTrap->u4IfIndex;
                pOid->u4_Length++;
            }
            u1EvcLength = STRLEN ((UINT1 *) pElmEvcTrap->ElmiEvcId);
            pOstring =
                SNMP_AGT_FormOctetString ((UINT1 *) &pElmEvcTrap->ElmiEvcId,
                                          (INT4) u1EvcLength);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                      pOstring, NULL, SnmpCounter64Type);

            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_EVC_STATUS_TRAP_INDEX);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmEvcTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) pElmEvcTrap->
                                      u1ElmiEvcStatus, NULL, NULL,
                                      SnmpCounter64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "EVC Informaion is changed  "
                          " EVC Info Change Trap is generated\n "));

            break;

        case ELM_UNI_TRAP_VAL:

            pElmUniTrap = (tElmUniTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_UNI_STATUS_TRAP_INDEX);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmUniTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmUniTrap->u1ElmiUniStatus,
                                            NULL, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "UNI Informaion is changed  "
                          "UNI Info Change Trap is generated\n "));
            break;
        case ELM_OPER_STATUS_CHANGED_TRAP_VAL:

            pElmOperTrap = (tElmOperTrap *) pTrapInfo;
            SPRINTF ((char *) au1Buf, ELM_MIB_OBJ_OPER_STATUS_TRAP_TYPE);
            pOid = ElmMakeObjIdFromDotNew ((INT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Append the indices for this object
             * Index1 --> Port No.
             * */
            if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
            {
                /* 1. Append the Port Number
                 * */
                pOid->pu4_OidList[pOid->u4_Length] =
                    (UINT4) pElmOperTrap->u4IfIndex;
                pOid->u4_Length++;
            }

            pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                            (INT4) pElmOperTrap->
                                            u1ElmiOperStatus, NULL, NULL,
                                            SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pStartVb = pVbList;

            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "ELMI Operational Status is changed  "
                          "Operational Status Change Trap is generated\n "));

            break;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);
#else /* SNMP_3_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_3_WANTED */
}

/*****************************************************************************/
/* Function Name      : ElmMakeObjIdFromDotNew                               */
/*                                                                           */
/* Description        : This function makes object ID                        */
/*                                                                           */
/* Input(s)           : pi1TextStr -                                        */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : pOidPtr or NULL                                      */
/*****************************************************************************/

tSNMP_OID_TYPE     *
ElmMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pOidPtr;
    UINT2               u2Index = (UINT1) ELM_INIT_VAL;
    UINT2               u2DotCount = (UINT1) ELM_INIT_VAL;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index <
                                                        ELM_OBJECT_NAME_MAX_LENGTH));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; ((u2Index <
                            (sizeof (orig_mib_oid_table) /
                             sizeof (struct MIB_OID)))
                           && (orig_mib_oid_table[u2Index].pName != NULL));
             u2Index++)
        {

            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         orig_mib_oid_table[u2Index].pNumber,
                         STRLEN (orig_mib_oid_table[u2Index].pNumber));
                break;
            }
        }
        ai1TempBuffer[u2Index] = '\0';

        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr) + 1);
    }
    else
    {                            /* is not alpha, so just copy into 
                                   ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */

    u2DotCount = 0;
    for (u2Index = 0; (((u2Index <= ELM_OBJECT_NAME_MAX_LENGTH)) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     ** Object. Tabular Objects needs index to be added at the
     ** end. So, allocating for Maximum OID Length
     ** */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */

    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pOidPtr->pu4_OidList[u2Index] =
             ((UINT4) (ElmParseSubIdNew ((INT1 **) &(pi1TempPtr))))) ==
            (UINT4) -1)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
#else
    UNUSED_PARAM (orig_mib_oid_table);
    UNUSED_PARAM (*pi1TextStr);
    UNUSED_PARAM (ai1TempBuffer);
    return (NULL);
#endif
}

/*****************************************************************************/
/* Function Name      : ElmParseSubIdNew                                     */
/*                                                                           */
/* Description        : This function parse subid                            */
/*                                                                           */
/* Input(s)           : ppu1TempPtr -                                        */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS or ELM_FAILURE                           */
/*****************************************************************************/

INT4
ElmParseSubIdNew (INT1 **pi1TmpPtr)
{
    INT1               *pi1Tmp;
    INT4                i4Value = 0;

    for (pi1Tmp = *pi1TmpPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                               ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                               ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        i4Value = (i4Value * 10) + (*pi1Tmp & 0xf);
    }

    if (*pi1TmpPtr == pi1Tmp)
    {
        i4Value = -1;
    }
    *pi1TmpPtr = pi1Tmp;
    return (i4Value);
}
