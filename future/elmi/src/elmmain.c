/******************************************************************** *  
* Copyright (C) 2006 Aricent Inc . All Rights Reserved]               * 
* $Id: elmmain.c,v 1.9 2012/04/04 14:14:48 siva Exp $                  * 
* Description: This file contains stub routines of Multiple instance  *
* specific functions for backward compatibility.                      * 
************************************************************************/
#include "elminc.h"
#define ELM_TRC_BUF_SIZE    500

/*****************************************************************************/
/* Function Name      : ElmAssignMempoolIds                                  */
/*                                                                           */
/* Description        : This function is called when ELMI Module is STARTED. */
/*                      Assign mempoolIds for mempools created in ELMI module*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
ElmAssignMempoolIds (VOID)
{
    /*tElmQMsg */
    ELM_QMSG_MEMPOOL_ID = ELMIMemPoolIds[MAX_ELMI_Q_MESG_SIZING_ID];

    /*tElmMsgNode */
    ELM_LOCALMSG_MEMPOOL_ID = ELMIMemPoolIds[MAX_ELMI_LOCAL_MESG_SIZING_ID];

    /*tElmQMsg */
    ELM_CFG_QMSG_MEMPOOL_ID = ELMIMemPoolIds[MAX_ELMI_CONFIG_Q_MESG_SIZING_ID];

    /*tElmPortEntryArray */
    ELM_PORT_TBL_MEMPOOL_ID =
        ELMIMemPoolIds[MAX_ELMI_PORT_INFO_ARRAY_SIZING_ID];

    /*tElmPortEntry */
    ELM_PORT_INFO_MEMPOOL_ID = ELMIMemPoolIds[MAX_ELMI_PORT_INFO_SIZING_ID];

    /*tElmTimer */
    ELM_TMR_MEMPOOL_ID = ELMIMemPoolIds[MAX_ELMI_TMR_NODES_SIZING_ID];

    /*tElmAsynchMessageNode */
    ELM_ASYNCH_LIST_MEMPOOL_ID =
        ELMIMemPoolIds[MAX_ELMI_ASYNC_MESG_NODES_SIZING_ID];

    /*tEcmEvcStatusInfo*/
    ELM_EVC_STATUS_MEMPOOL_ID  =
        ELMIMemPoolIds[MAX_ELM_EVC_STATUS_INFO_SIZING_ID];

    /*tEcmEvcVlanInfo*/
    ELM_EVC_VLAN_INFO_MEMPOOL_ID  =
        ELMIMemPoolIds[MAX_ELM_EVC_VLAN_INFO_SIZING_ID];

    /*tCfaUnicInfo*/
    ELM_CFA_UNI_INFO_MEMPOOL_ID  =
        ELMIMemPoolIds[MAX_CFA_UNI_INFO_SIZING_ID];

    /*tLcmEvcInfo*/
    ELM_LCM_EVC_INFO_MEMPOOL_ID =
        ELMIMemPoolIds[MAX_LCM_EVC_INFO_SIZING_ID];

    /*tLcmEvcCeVlanInfo*/
    ELM_LCM_EVC_VLAN_INFO_MEMPOOL_ID =
        ELMIMemPoolIds[MAX_LCM_EVC_CE_VLAN_INFO_SIZING_ID];

    ELM_ETH_FRAME_SIZE_MEMPOOL_ID =
        ELMIMemPoolIds[MAX_ELM_ETH_FRAME_BUF_SIZING_ID];
} 

#ifdef SNMP_2_WANTED
/*****************************************************************************/
/*    Function Name       : ElmiRegisterMIBS                                  */
/*                                                                           */
/*    Description         : This function registers the ELM mibs with        */
/*                          the SNMP agent                                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                      .                      */
/*****************************************************************************/
VOID
ElmiRegisterMIBS (VOID)
{
    RegisterFSELMI ();
}
#endif

VOID
ElmGlobalTrace (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         Str[ELM_TRC_BUF_SIZE];

    if (ELM_IS_ELMI_INITIALISED () == ELM_FALSE)
    {
        return;
    }
    va_start (ap, fmt);
    vsprintf (&Str[0], fmt, ap);
    va_end (ap);

    UtlTrcLog (ELM_TRC_FLAG, u4Flags, ELM_MOD_NAME, Str);
}
