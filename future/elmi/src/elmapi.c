/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmapi.c,v 1.17 2010/07/29 09:20:15 prabuc Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              functionality common to the ELMI-UNI-C and ELMI-UNI-N
 *
 *******************************************************************/

#define _ELMAPI_C_

#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmCreatePort                                        */
/*                                                                           */
/* Description        : This function is called from CFA module through L2IWF*/
/*                      interface to indicate the creation of a new port. It */
/*                      passes on this information to the Message processing */
/*                      module by direct function call.                      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Number of the port that is to  */
/*                                  be created.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmCreatePort (UINT4 u4IfIndex)
{
    tElmMsgNode        *pNode = NULL;

    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }

    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = u4IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_CREATE_PORT_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    L2_SYNC_TAKE_SEM ();

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmDeletePort                                        */
/*                                                                           */
/* Description        : Deletes the port information and releases the memory */
/*                      occupied by it.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number of the port that is to be    */
/*                                  deleted.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmDeletePort (UINT4 u4IfIndex)
{
    tElmMsgNode        *pNode = NULL;

    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = u4IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_DELETE_PORT_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmUpdatePortStatus                                  */
/*                                                                           */
/* Description        : This function is called from the Bridge Module to    */
/*                      indicate the operational status of the physical port */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u2IfIndex - The Global IfIndex of the port to which  */
/*                                  this indication belongs                  */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/*****************************************************************************/
INT4
ElmUpdatePortStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    tElmMsgNode        *pNode = NULL;

    if (ELM_SYSTEM_CONTROL != ELM_START)
    {
        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                        "MGMT: ELM Module has been SHUTDOWN or DISABLED!\n");

        return ELM_SUCCESS;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = u4IfIndex;

    switch (u1Status)
    {
        case ELM_PORT_OPER_UP:
            pNode->MsgType = (tElmLocalMsgType) ELM_ENABLE_PORT_MSG;
            pNode->Msg.u1TrigType = (UINT1) ELM_EXT_PORT_UP;
            break;

        case ELM_PORT_OPER_DOWN:
            pNode->MsgType = (tElmLocalMsgType) ELM_DISABLE_PORT_MSG;
            pNode->Msg.u1TrigType = (UINT1) ELM_EXT_PORT_DOWN;
            break;
        default:
            ELM_RELEASE_LOCALMSG_MEM_BLOCK (pNode);
            return ELM_FAILURE;
    }

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pNode);
        return ELM_FAILURE;
    }

    /* Standby does not receive this indication. 
     * Send it through RM if Node is active node and Stanby node is present
     */
    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        ElmRedOperStatusChangeIndication (u4IfIndex, u1Status);
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleInFrame                                     */
/*                                                                           */
/* Description        : This function is called from the CFA Module to       */
/*                      handover the BPDUs to ELMI Task by posting the PDUs  */
/*                      to the ELMI Queue and sending an event to ELMI Task  */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the PDU CRU Buffer              */
/*                      u2IfIndex - Interface Index of the Port              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE / ELM_DATA                 */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingLayer2Pkt                         */
/*****************************************************************************/
INT4
ElmHandleInFrame (tElmBufChainHeader * pCruBuf, UINT2 u2IfIndex)
{
    tElmInterface       IfaceId;
    INT4                i4RetVal = ELM_SUCCESS;
    tCfaIfInfo          CfaIfInfo;
    tElmQMsg           *pQMsg = NULL;

    ELM_MEMSET (&IfaceId, ELM_INIT_VAL, sizeof (IfaceId));
    ELM_MEMSET (&CfaIfInfo, ELM_INIT_VAL, sizeof (tCfaIfInfo));

    if (!ELM_IS_ELMI_STARTED () || (ELM_RM_GET_NODE_STATE () != RED_ELM_ACTIVE))
    {
        /* If ELM module is not enabled then the packet has to be given to VLAN
         * module*/
        return ELM_DATA;
    }

    if (ELM_GLOBAL_STATUS != (UINT1) ELM_ENABLE_GLOBAL)
    {
        if (ELM_RELEASE_CRU_BUF (pCruBuf, ELM_FALSE) != (INT4) CRU_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC |
                            ELM_ALL_FAILURE_TRC,
                            "ELMI: CRU BUffer Release FAILED!!!\n");
            return ELM_FAILURE;
        }
        return ELM_SUCCESS;
    }

    if (CfaGetIfInfo (u2IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                        "SYS: Cannot Get IfType for Port %u\n", u2IfIndex);

        if (ELM_RELEASE_CRU_BUF (pCruBuf, ELM_FALSE) == ELM_CRU_FAILURE)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                            "MSG: Received Bpdu CRU Buffer -\n");
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                            "Release FAILED!\n");
            return ELM_FAILURE;
        }
        return ELM_SUCCESS;
    }

    IfaceId.u1_InterfaceType = CfaIfInfo.u1IfType;
    if (ELM_ALLOC_QMSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "ELMI: Message Memory Block Allocation FAILED!!!\n");

        if (ELM_RELEASE_CRU_BUF (pCruBuf, ELM_FALSE) != (INT4) CRU_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC |
                            ELM_ALL_FAILURE_TRC,
                            "ELMI: CRU BUffer Release FAILED!!!\n");
            return ELM_FAILURE;
        }
        return ELM_SUCCESS;
    }

    ELM_MEMSET (pQMsg, ELM_MEMSET_VAL, sizeof (tElmQMsg));

    IfaceId.u4IfIndex = (UINT4) u2IfIndex;

    /* set the interface id in the buffer */
    ELM_BUF_SET_INTERFACEID (pCruBuf, IfaceId);
    ELM_QMSG_TYPE (pQMsg) = ELM_PDU_RCVD_QMSG;
    pQMsg->uQMsg.pPduInQ = pCruBuf;

    if (ELM_SEND_TO_QUEUE (ELM_INPUT_QID,
                           (UINT1 *) &pQMsg, ELM_DEF_MSG_LEN) == OSIX_SUCCESS)

    {
        if (ELM_SEND_EVENT (ELM_TASK_ID, ELM_PDU_EVENT) != OSIX_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC |
                            ELM_ALL_FAILURE_TRC,
                            "ELMI: Sending ELM_MSG_EVENT to ELMI Task FAILED!!!\n");
            i4RetVal = ELM_FAILURE;
        }
    }
    else
    {
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC |
                        ELM_ALL_FAILURE_TRC,
                        "ELMI: Posting Message to ELM Queue FAILED!!!\n");
        i4RetVal = ELM_FAILURE;
    }

    if (i4RetVal == ELM_FAILURE)
    {
        if (ELM_RELEASE_CRU_BUF (pCruBuf, ELM_FALSE) == ELM_CRU_FAILURE)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                            "MSG: Received Pdu CRU Buffer -\n");
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                            "Release FAILED!\n");
        }

        if (ELM_RELEASE_QMSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                            "MGMT: Release of Local Msg Memory Block FAILED!\n");
            i4RetVal = ELM_FAILURE;
        }

    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : ElmLock                                              */
/*                                                                           */
/* Description        : This function is used to take the ELM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, ElmTaskMain                                   */
/*****************************************************************************/
INT4
ElmLock (VOID)
{
    if (ELM_TAKE_SEM (gElmGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_OS_RESOURCE_TRC | ELM_ALL_FAILURE_TRC,
                        "MSG: Failed to Take ELM Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the ELM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, ElmTaskMain                                   */
/*****************************************************************************/
INT4
ElmUnLock (VOID)
{
    ELM_GIVE_SEM (gElmGlobalInfo.SemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmGetElmiPortMode                                   */
/*                                                                           */
/* Description        : This function is used to get Elmi Mode on the port   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_CUSTOMER_SIDE or ELM_PROVIDER_SIDE or ELM_FAILURE*/
/*                                                                           */
/* Called By          : External Modules                                     */
/*****************************************************************************/
INT4
ElmGetPortMode (UINT4 u4IfIndex)
{
    if (ElmValidatePortEntry (u4IfIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 " Port Does Not exists For ELMI !\n");
        return (UINT4) ELM_FAILURE;
    }
    return ELM_GET_PORT_MODE (u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : ElmEvcInformationChangeIndication                    */
/*                                                                           */
/* Description        : This routine is called by LCM to indicate any change */
/*                      in status of an EVC to E-LMI UNI-N                   */
/*                                                                           */
/* Input(s)           : EVC Reference Id                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmEvcInformationChangeIndication (UINT2 u2IfIndex)
{
    tElmMsgNode        *pNode = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));
    pNode->u4IfIndex = u2IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_LCM_INFORMATION_UPDATE_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmUniInformationChangeIndication                    */
/*                                                                           */
/* Description        : This routine is called by CFA to indicate change in  */
/*                      UNI Information database                             */
/*                                                                           */
/* Input(s)           : Port Number                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmUniInformationChangeIndication (UINT2 u2IfIndex)
{
    tElmMsgNode        *pNode = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }

    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = (UINT4) u2IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_UNI_INFORMATION_UPDATE_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmMapPort                                           */
/*                                                                           */
/* Description        : This routine is called by VCM to indicate Port       */
/*                      creation in a context                                */
/*                                                                           */
/* Input(s)           : Port Number                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmMapPort (UINT2 u2IfIndex)
{
    tElmMsgNode        *pNode = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = (UINT4) u2IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_MAP_PORT_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmUnMapPort                                         */
/*                                                                           */
/* Description        : This routine is called by LCM to indicate Port       */
/*                      deleted in a context                                 */
/*                                                                           */
/* Input(s)           : Port Number                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmUnMapPort (UINT2 u2IfIndex)
{
    tElmMsgNode        *pNode = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = (UINT4) u2IfIndex;
    pNode->MsgType = (tElmLocalMsgType) ELM_UNMAP_PORT_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmEvcStatusChangeIndication                         */
/*                                                                           */
/* Description        : This routine is called by LCM                        */
/*                      to indicate any change in status of an EVC to E-LMI  */
/*                      UNI-N                                                */
/*                                                                           */
/* Input(s)           : EVC Reference Id                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmEvcStatusChangeIndication (UINT2 u2IfIndex, UINT2 u2EvcRefId, UINT1 u1Status)
{
    tElmMsgNode        *pNode = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_START)
    {
        return ELM_FAILURE;
    }

    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));
        return ELM_FAILURE;
    }

    ELM_MEMSET (pNode, ELM_MEMSET_VAL, sizeof (tElmMsgNode));

    pNode->u4IfIndex = (UINT4) u2IfIndex;
    pNode->u2EvcRefId = u2EvcRefId;
    pNode->Msg.u1EvcStatus = u1Status;
    pNode->MsgType = (tElmLocalMsgType) ELM_EVC_STATUS_CHANGE_MSG;

    if (ElmSendEventToElmTask (pNode) != (INT4) ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmModuleInit                                        */
/*                                                                           */
/* Description        : This is an API to initialise the ELMI module         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmModuleInit (VOID)
{
    ELM_LOCK ();
    if (ElmHandleModuleInit () == ELM_FAILURE)
    {
        ELM_UNLOCK ();
        return ELM_FAILURE;
    }
    ELM_UNLOCK ();
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmModuleShutDown                                    */
/*                                                                           */
/* Description        : This is an API to shutdown the ELMI module           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmModuleShutdown (VOID)
{
    ELM_LOCK ();
    ElmHandleModuleShutdown ();
    ELM_UNLOCK ();
    return;
}
