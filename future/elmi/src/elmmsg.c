/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmmsg.c,v 1.24 2015/10/05 12:21:35 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Message Processing Module which includes enqueued
 *              PDUs and local SNMP messages and Task Initilisation
 *              Routines.
 *
 *******************************************************************/

#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmTaskInit                                          */
/*                                                                           */
/* Description        : This function creates the semaphores, configuration  */
/*                      message queuea and PDU message queue.                */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmTaskInit (VOID)
{
    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC, "MSG: Spawning ELMI Task...\n");
    ELM_MEMSET (&gElmGlobalInfo, ELM_INIT_VAL, sizeof (tElmGlobalInfo));

    /* Initialize task related memory.
     * Create Sems, Queues, and mempools for local messages.
     */
    if (ELM_CREATE_SEM (ELM_MUT_EXCL_SEM_NAME, ELM_SEM_INIT_COUNT, 0,
                        &gElmGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_OS_RESOURCE_TRC | ELM_INIT_SHUT_TRC |
                        ELM_ALL_FAILURE_TRC,
                        "MSG: Failed to Create ELMI Mutual Exclusion Semaphore \n");
        return ELM_FAILURE;
    }
    if (ELM_CREATE_QUEUE (ELM_TASK_INPUT_QNAME, OSIX_MAX_Q_MSG_LEN,
                          ELM_QUEUE_DEPTH, &(ELM_INPUT_QID)) != OSIX_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_OS_RESOURCE_TRC | ELM_INIT_SHUT_TRC |
                        ELM_ALL_FAILURE_TRC,
                        "MSG: Failed To Create Message ELMI Module Input Queue \n");
        return ELM_FAILURE;
    }

    if (ELM_CREATE_QUEUE (ELM_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                          ELM_CFG_Q_DEPTH, &(ELM_CFG_QID)) != OSIX_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_OS_RESOURCE_TRC | ELM_INIT_SHUT_TRC |
                        ELM_ALL_FAILURE_TRC,
                        "MSG: Failed To Create Message ELMI Module CFG Queue \n");
        return ELM_FAILURE;
    }

    gElmGlobalInfo.gu1ElmPdu = gau1ElmPduBuf;
    if (gElmGlobalInfo.gu1ElmPdu == NULL)
    {
        ELM_GLOBAL_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "MSG: Failed to Allocate memory for ELMI pdu!\n");
        return ELM_FAILURE;
    }
    ELM_TRACE_OPTION = (UINT4) 0;

    ELM_SYSTEM_CONTROL = ELM_SHUTDOWN;
    ELM_IS_ELMI_INITIALISED () = ELM_TRUE;

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmTaskDeInit                                        */
/*                                                                           */
/* Description        : This function deletes the semaphores, configuration  */
/*                      message queuea and PDU message queue.                */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmTaskDeInit (VOID)
{
    if (gElmGlobalInfo.SemId != 0)
    {
        ELM_DELETE_SEMAPHORE (gElmGlobalInfo.SemId);
        gElmGlobalInfo.SemId = 0;
    }
    if (ELM_INPUT_QID != 0)
    {
        ELM_DELETE_MSG_QUEUE (ELM_INPUT_QID);
        ELM_INPUT_QID = 0;
    }
    if (ELM_CFG_QID != 0)
    {
        ELM_DELETE_MSG_QUEUE (ELM_CFG_QID);
        ELM_CFG_QID = 0;
    }
    if (gElmGlobalInfo.gu1ElmPdu != NULL)
    {
        ELM_MEMSET (gau1ElmPduBuf, 0, ELM_MAX_ETH_FRAME_SIZE);
        gElmGlobalInfo.gu1ElmPdu = NULL;
    }
}

/*****************************************************************************/
/* Function Name      : ElmSendEventToElmTask                                */
/*                                                                           */
/* Description        : This function receives indications and PDUs from    */
/*                      external modules, forms a Queue message that contains*/
/*                      either the pointer to the indication message received*/
/*                      or the pointer to the CRU buffer i.e. PDU. It then  */
/*                      posts the message into the queue and sends an event  */
/*                      to the Elmp Task.                                    */
/*                                                                           */
/* Input(s)           : pNode - Pointer to the indication message received   */
/*                              from external modules                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmSendEventToElmTask (tElmMsgNode * pNode)
{
    INT4                i4RetVal = ELM_SUCCESS;
    tElmQMsg           *pQMsg = NULL;

    if (ELM_ALLOC_CFGQ_MSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                 "STAP: Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Message Memory Block Allocation FAILED!!!"));

        if (ELM_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }
        return ELM_FAILURE;
    }

    ELM_MEMSET (pQMsg, ELM_MEMSET_VAL, sizeof (tElmQMsg));

    ELM_QMSG_TYPE (pQMsg) = ELM_SNMP_CONFIG_QMSG;
    pQMsg->uQMsg.pMsgNode = pNode;

    if (ELM_SEND_TO_QUEUE (ELM_CFG_QID, (UINT1 *) &pQMsg,
                           ELM_DEF_MSG_LEN) == OSIX_SUCCESS)
    {

        if (ELM_SEND_EVENT (ELM_TASK_ID, ELM_MSG_EVENT) != OSIX_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Sending ELM_MSG_EVENT to ELM Task FAILED!\n");
            i4RetVal = ELM_FAILURE;
        }
    }
    else
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "MGMT: Posting Message to Elm Queue FAILED!\n");
        i4RetVal = ELM_FAILURE;
    }

    if (i4RetVal == ELM_FAILURE)
    {
        if (ELM_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }

        if (ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Release of CFGQ Msg Memory Block FAILED!\n");
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ElmTaskMain                                          */
/*                                                                           */
/* Description        : This is the main entry point function for the ELMI   */
/*                      Task. This waits continuously in a loop to receive   */
/*                      events that are posted to this task and it then calls*/
/*                      the corresponding functions to process the events.   */
/*                                                                           */
/* Input(s)           : pi1Param - Pointer to the parameter value that can be*/
/*                                 passed to this task entry point function. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = ELM_INIT_VAL;
    tElmQMsg           *pQMsg = NULL;
    VOID               *pRmMsg;

    /* The structure tElmPortEntryArray is declared here locally 
     * to ensure that the sizing parameter structure is visible in debugger.
     * This will be removed in the next subsequent changes in the sizing parameters. */
    tElmPortEntryArray *pElmPortEntryPtr = NULL;
    ELM_UNUSED (pElmPortEntryPtr);

    ELM_UNUSED (pi1Param);

    /* Create Sems, Queues, Mempools */
    if (ElmTaskInit () == ELM_FAILURE)
    {
        ElmTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (ELM_GET_TASK_ID (ELM_SELF, (UINT1 *) ELM_TASK_NAME, &(ELM_TASK_ID)) !=
        OSIX_SUCCESS)
    {
        ElmTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    ElmiRegisterMIBS ();
#endif

    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC, "MSG: ELM Task context running ...\n");

    while (ELM_TRUE)
    {
        ELM_GLOBAL_TRC (ELM_EVENT_HANDLING_TRC,
                        "MSG: Waiting for an Event to be received ...\n");

        if (ELM_RECEIVE_EVENT (ELM_TASK_ID, ELM_PDU_EVENT | ELM_MSG_EVENT |
                               ELM_TMR_EXPIRY_EVENT | ELM_RED_BULK_UPD_EVENT,
                               (UINT4) OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & ELM_PDU_EVENT)
            {
                while (ELM_GET_PDU_FROM_ELM_QUEUE (ELM_INPUT_QID,
                                                   (UINT1 *) &pQMsg,
                                                   ELM_DEF_MSG_LEN,
                                                   ELM_OSIX_NO_WAIT)
                       == OSIX_SUCCESS)
                {
                    /* Processing of PDUs */
                    if ((pQMsg == NULL) ||
                        (ELM_QMSG_TYPE (pQMsg) != ELM_PDU_RCVD_QMSG))
                    {
                        continue;
                    }
                    ELM_LOCK ();

                    ElmHandlePduMsg (pQMsg->uQMsg.pPduInQ);

                    ELM_UNLOCK ();

                    if (ELM_RELEASE_QMSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
                    {
                        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                                        "MSG: Release of Local Msg Memory Block"
                                        "FAILED!\n");

                    }

                    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC,
                                    "MSG: PDU Received Event processed.\n");
                }
            }

            if (u4Events & ELM_MSG_EVENT)
            {
                while (ELM_RECV_FROM_QUEUE (ELM_CFG_QID,
                                            (UINT1 *) &pQMsg,
                                            ELM_DEF_MSG_LEN,
                                            ELM_OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    switch (ELM_QMSG_TYPE (pQMsg))
                    {
                        case ELM_SNMP_CONFIG_QMSG:

                            /* Processing of Local Messages */
                            ELM_GLOBAL_TRC (ELM_MGMT_TRC |
                                            ELM_EVENT_HANDLING_TRC,
                                            "MSG: SNMP CONFIG Event received...\n");

                            ELM_LOCK ();

                            ElmHandleSnmpCfgMsg (pQMsg->uQMsg.pMsgNode);

                            ELM_UNLOCK ();

                            if (ELM_RELEASE_LOCALMSG_MEM_BLOCK
                                (pQMsg->uQMsg.pMsgNode) != MEM_SUCCESS)
                            {
                                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                                                ELM_MGMT_TRC,
                                                "MSG: Release of Local Msg Memory "
                                                "Block FAILED!\n");

                            }

                            if (ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                MEM_SUCCESS)
                            {
                                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                                                ELM_MGMT_TRC,
                                                "MSG: Release of Q Msg Memory "
                                                "Block FAILED!\n");

                            }

                            ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC,
                                            "MSG: Message Received Event"
                                            "processed.\n");
                            break;
                        case ELM_RM_QMSG:
                            ELM_GLOBAL_TRC (ELM_MGMT_TRC |
                                            ELM_EVENT_HANDLING_TRC,
                                            "MSG: RM Message received...\n");
                            ELM_LOCK ();
                            pRmMsg = pQMsg->uQMsg.pRmMsg;
                            ElmProcessRmEvent (pRmMsg);
                            ELM_UNLOCK ();
                            if (ELM_RELEASE_LOCALMSG_MEM_BLOCK
                                (pQMsg->uQMsg.pRmMsg) != MEM_SUCCESS)
                            {
                                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                                                ELM_MGMT_TRC,
                                                "MSG: Release of Local Msg Memory "
                                                "Block FAILED!\n");

                            }

                            if (ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) !=
                                MEM_SUCCESS)
                            {
                                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                                                ELM_MGMT_TRC,
                                                "MSG: Release of Q Msg Memory "
                                                "Block FAILED!\n");

                            }

                            ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC,
                                            "MSG: Message Received Event"
                                            "processed.\n");
                            break;
                    }
                }
            }

            /****************************************************/
            /*              ELM_TMR_EXPIRY_EVENT                */
            /****************************************************/
            if (u4Events & ELM_TMR_EXPIRY_EVENT)
            {
                ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC |
                                ELM_TMR_TRC,
                                "MSG: Timer Expiry event obtained ...\n");

                ELM_LOCK ();

                if (ElmTmrExpiryHandler () != ELM_SUCCESS)
                {
                    ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                    "MSG: ElmTmrExpiryHandler function "
                                    "returned FAILURE!\n");
                }

                ELM_UNLOCK ();

                ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC,
                                "MSG: Timer Expiry Event processed...\n");
            }
            if (u4Events & ELM_RED_BULK_UPD_EVENT)
            {
                ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC | ELM_EVENT_HANDLING_TRC |
                                ELM_RED_TRC,
                                "MSG: Bulk Update event obtained ...\n");
                ELM_LOCK ();
                ElmRedHandleBulkUpdateEvent ();
                ELM_UNLOCK ();
                ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC | ELM_EVENT_HANDLING_TRC |
                                ELM_RED_TRC,
                                "MSG: Bulk Update event processed ...\n");
            }

            ELM_GLOBAL_TRC (ELM_EVENT_HANDLING_TRC,
                            "MSG: Completed processing the event(s).\n");

        }                        /* End of Receive Event If Loop */
    }                            /* End of While Loop */

}

/*****************************************************************************/
/* Function Name      : ElmVerifyPdu                                         */
/*                                                                           */
/* Description        : This function checks for the presence and            */
/*                      correctness of the following Information  Elements:  */
/*                            - Protocol Version                             */
/*                            - Message Type                                 */
/*                            - Report Type                                  */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port at which the PDU is receieved        */
/*                                                                           */
/* Output(s)          : *pu1MsgType - Message type of the received PDU       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*                                                                           */
/* Called By          : ElmHandlePduMsg                                      */
/*                                                                           */
/* Calling Function   : ElmRaiseProtocolError                                */
/*****************************************************************************/
INT4
ElmVerifyPdu (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT1 *pu1MsgType)
{
    UINT1               u1ProtocolVersion = ELM_INIT_VAL;
    tElmPortEntry      *pElmPortEntry = NULL;
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    UNUSED_PARAM (pElmPortEntry);
    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             "MSG: Receiving ELMI PDU ...\n");
    ELM_GET_1BYTE (u1ProtocolVersion, pu1Buf);

    if (u1ProtocolVersion != ELM_PROTOCOL_VERSION)
    {
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_INVALID_PROTOCOL_VERSION,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ELM_INCR_ERROR_PROT_VER_RX_COUNT (u4IfIndex);
        return ELM_FAILURE;
    }

    ELM_GET_1BYTE (*pu1MsgType, pu1Buf);
    if ((*pu1MsgType != ELM_PDU_TYPE_STATUS_ENQUIRY) &&
        (*pu1MsgType != ELM_PDU_TYPE_STATUS))
    {
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_INVALID_MSG_TYPE,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ELM_INCR_INVALID_MSG_TYPE_COUNT (u4IfIndex);
        ELM_TRC_ARG1 (ELM_PDU_TRC,
                      "Discarding the PDU due to Message Type = %d in the packet\n",
                      *pu1MsgType);
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmReceivedPdu                                       */
/*                                                                           */
/* Description        : This handles the received Pdu and hands converts the */
/*                      Buffer memory to Linear memory for further           */
/*                      processing                                           */
/*                                                                           */
/* Input(s)           : pBuf - Pointer to the CRU Buffer of the received     */
/*                             Pdu                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmReceivedPdu (tElmBufChainHeader * pBuf, UINT4 u4IfIndex)
{
    tElmPortEntry      *pPortEntry = NULL;
    tElmInterface       IfId;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT4               u4ByteCount = ELM_INIT_VAL;
    UINT2               u2DataLength = ELM_INIT_VAL;
    UINT2               u2EtherType = ELM_INIT_VAL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1IfType = ELM_INIT_VAL;

    /* Assumptions: 
     * 1) ELMI Lock already taken.
     * */

    u2DataLength = (UINT2) ELM_BUF_GET_CHAIN_VALIDBYTECOUNT (pBuf);

    ELM_TRC (ELM_CONTROL_PATH_TRC, "MSG: Receiving PDU ...\n");
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "Valid Bytes in Rxd Packet: %u \n", u2DataLength);
    ELM_PKT_DUMP (ELM_DUMP_TRC, pBuf, u2DataLength,
                  ">************ Dumping received frame **********<\n");
    ELM_TRC (ELM_CONTROL_PATH_TRC,
             ">*******************************************************<\n");

    ELM_MEMSET (&IfId, ELM_INIT_VAL, sizeof (tElmInterface));

    IfId = ELM_BUF_GET_INTERFACEID (pBuf);

    u1IfType = IfId.u1_InterfaceType;

    if (u4IfIndex == ELM_INVALID_PORT_NUM)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "Not processing PDU due to invalid Port in the packet\n");
        return ELM_FAILURE;
    }

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pPortEntry == NULL)
    {
        ELM_TRC_ARG1 (ELM_PDU_TRC | ELM_ALL_FAILURE_TRC,
                      "Port %u DOES NOT exist...Discarding PDU\n", u4IfIndex);
        return ELM_FAILURE;
    }

    if ((pPortEntry->u1PortStatus == ELM_PORT_OPER_DOWN) ||
        (pPortEntry->bLaPort == ELM_TRUE))
    {
        ELM_TRC_ARG1 (ELM_PDU_TRC,
                      "Port %s Not Enabled...Discarding PDU\n",
                      ELM_GET_IFINDEX_STR (u4IfIndex));
        return ELM_SUCCESS;
    }

    if (u1IfType == ELM_ENET_PHYS_INTERFACE_TYPE)
    {
        /* Skipping the Destination and Source Address fields to extract the
         * Length field */
        ELM_BUF_MOVE_VALID_OFFSET (pBuf, ELM_ENET_DEST_SRC_ADDR_SIZE);

        /* Copying the ethertype field */
        u4Offset = 0;
        if (ELM_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &u2EtherType, u4Offset, 2)
            == ELM_CRU_FAILURE)
        {
            ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                     "MSG: Copy From Received Pdu CRU Buffer FAILED!\n");
            return ELM_FAILURE;
        }
        u2EtherType = (UINT2) ELM_NTOHS (u2EtherType);

        if (u2EtherType != ELM_ETHER_TYPE)
        {
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR_TYPE,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
            ELM_TRC_ARG1 (ELM_PDU_TRC,
                          "Discarding the PDU due to EtherType = %d in the packet\n",
                          u2EtherType);
            return ELM_FAILURE;
        }

        /* Subtracting the length of Ethernet Header and Ethertype */
        u2DataLength = (UINT2) (u2DataLength -
                                (ELM_ENET_DEST_SRC_ADDR_SIZE +
                                 ELM_ETHER_TYPE_SIZE));

        /* Now skipping the Length of Ether Type */
        ELM_BUF_MOVE_VALID_OFFSET (pBuf, ELM_ETHER_TYPE_SIZE);

    }
    else
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                      "MSG: Port %s: Not processing PDU due to -\n",
                      ELM_GET_IFINDEX_STR (u4IfIndex));
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "Unknown Physical IfType\n");
        return ELM_FAILURE;
    }

    u4ByteCount = ELM_BUF_GET_CHAIN_VALIDBYTECOUNT (pBuf);
    pu1Pdu = ELM_GET_DATA_PTR_IF_LINEAR (pBuf, u4Offset, u4ByteCount);
    if (pu1Pdu == NULL)
    {
        pu1Pdu = gElmGlobalInfo.gu1ElmPdu;
        ELM_MEMSET (pu1Pdu, ELM_INIT_VAL, ELM_MAX_ETH_FRAME_SIZE);
        if (ELM_COPY_FROM_CRU_BUF (pBuf, pu1Pdu, u4Offset, u4ByteCount)
            == ELM_CRU_FAILURE)
        {
            ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                     "MSG: Received Pdu Copy From CRU Buffer FAILED!\n");
            return ELM_FAILURE;
        }
    }

    /*Verify the valid length of CRU Buffer */
    if (u2DataLength > (UINT2) u4ByteCount)
    {
        ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                 "MSG: Pdu CRU Buffer Length MISMATCH!\n");
        u2DataLength = (UINT2) u4ByteCount;
    }

    if (ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex))
    {
        if (ElmHandleInPdu (pu1Pdu, u4IfIndex, u2DataLength) != ELM_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "MSG: ElmHandleInPdu function FAILED!\n");
            return ELM_FAILURE;
        }
    }
    else
    {
        ELM_TRC (ELM_PDU_TRC | ELM_ALL_FAILURE_TRC,
                 "MSG: ELMI Protocol is Not Enabled On Port\n");
        return ELM_FAILURE;
    }

    ELM_TRC (ELM_PDU_TRC, "MSG: PDU received successfully\n");
    return ELM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : ElmHandleInPdu                                       */
/*                                                                           */
/* Description        : This function handles the PDUs received by E-LMI     */
/*                      task and based on the report type of the received    */
/*                      PDU passes the control to the appropriate function   */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmHandleInPdu (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();

    /* Validate the message received for the Mandatory IEs  and 
     * get the report type present in the message
     */

    if (u2DataLength < ELM_MIN_ETH_FRAME_WITHOUT_HEADERS)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_INVALID_MSG_TYPE,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_INCR_SHORT_MSG_RX_COUNT (u4IfIndex);

        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                 "ELM:ELMI PDU length is less then minimum length .so it is discarded !!!\n");
        return ELM_FAILURE;
    }

    if (ElmVerifyPdu (pu1Buf, u4IfIndex, &u1MsgType) == ELM_FAILURE)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: Verification of the PDU failed!!!\n");
        return ELM_FAILURE;
    }

    /* Skip Protocol and Message type field in PDU */
    pu1Buf = pu1Buf + ELM_PROTO_MSG_TYPE_SIZE;
    u2DataLength = u2DataLength - (UINT2) ELM_PROTO_MSG_TYPE_SIZE;
    /* Depending upon the mode on which ELMI is functioning process the 
     * PDU.
     */
    if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_CUSTOMER_SIDE)
    {
        if (u1MsgType == ELM_PDU_TYPE_STATUS)
        {
            if (ElmHandleCustomerTlv (pu1Buf, u4IfIndex, u2DataLength) ==
                ELM_FAILURE)
            {
                return ELM_FAILURE;
            }
        }
        else
        {
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_INVALID_MSG_TYPE,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                     "SYS: UNI-C side did not receive a Status Message!!!\n");
            return ELM_FAILURE;
        }
    }
    else
    {
        if (u1MsgType == ELM_PDU_TYPE_STATUS_ENQUIRY)
        {
            if (ElmHandleNetworkTlv (pu1Buf, u4IfIndex, u2DataLength) ==
                ELM_FAILURE)
            {
                return ELM_FAILURE;
            }
        }
        else
        {
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_INVALID_MSG_TYPE,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                     "SYS: UNI-N side did not receive a Status Enquiry"
                     "Message!!!\n");
            return ELM_FAILURE;
        }
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandlePduMsg                                      */
/*                                                                           */
/* Description        : It handles the received PDUs  when ELMI Module is    */
/*                      enabled.                                             */
/*                                                                           */
/* Input(s)           : pPdu -     Pointer to the received message Bpdu      */
/*                                 structure                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmHandlePduMsg (tElmBufChainHeader * pPdu)
{
    tElmInterface       IfId;
    UINT4               u4IfIndex = ELM_INIT_VAL;
    UINT2               u2TmpLength = ELM_INIT_VAL;

    IfId = ELM_BUF_GET_INTERFACEID (pPdu);

    u4IfIndex = (UINT2) IfId.u4IfIndex;

    u2TmpLength = (UINT2) ELM_BUF_GET_CHAIN_VALIDBYTECOUNT (pPdu);

    ELM_TRC (ELM_EVENT_HANDLING_TRC | ELM_PDU_TRC,
             "MSG: PDU RCVD Event received ...\n");
    ELM_TRC_ARG1 (ELM_PDU_TRC, "Valid Bytes in Rxd PDU: %u \n", u2TmpLength);

    /* Checking Global Module status, If the Module status is disabled,
     * the discard the PDU*/
    if (ELM_GLOBAL_STATUS == (UINT1) ELM_ENABLE_GLOBAL)
    {
        if (ELM_IS_ELMI_STARTED ())
        {
            if (ElmReceivedPdu (pPdu, (UINT2) u4IfIndex) != ELM_SUCCESS)
            {
                ELM_TRC (ELM_CONTROL_PATH_TRC |
                         ELM_ALL_FAILURE_TRC,
                         "MSG: ElmReceived PDU returned "
                         "failure. DISCARDING PDU !!!\n");

            }
        }
    }
    else
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_PDU_TRC |
                 ELM_EVENT_HANDLING_TRC |
                 ELM_ALL_FAILURE_TRC,
                 "MSG: ELM Module is Not Enabled " "Cannot Process Pdu\n");
    }

    if (ELM_RELEASE_CRU_BUF (pPdu, ELM_FALSE) == ELM_CRU_FAILURE)
    {
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC |
                        ELM_EVENT_HANDLING_TRC,
                        "MSG: Received Pdu CRU Buffer Release " "FAILED!\n");
    }
}

/*****************************************************************************/
/* Function Name      : ElmHandleOutgoingPktOnPort                           */
/*                                                                           */
/* Description        : This function will be called by ELMI module for      */
/*                      sending a PDU on a physical port                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be sended out                       */
/*                      pElmPortInfo - Port Info on which pkt should be send */
/*                      u4PktSize - Size of the Packet                       */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ ELM_FAILURE.                            */
/*****************************************************************************/

INT4
ElmHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tElmPortEntry * pElmPortInfo, UINT4 u4PktSize)
{

    /* Standby Node should not transmit any PDU */
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
    {
        return ELM_FAILURE;
    }

    if (L2IwfHandleOutgoingPktOnPort (pBuf, pElmPortInfo->u4IfIndex,
                                      u4PktSize, ELM_PROT_PDU,
                                      (UINT1) ELM_ENCAP_NONE) == L2IWF_FAILURE)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleSnmpCfgMsg                                  */
/*                                                                           */
/* Description        : It handles the received snmp messages when ELMI      */
/*                      Module is enabled.                                   */
/*                                                                           */
/* Input(s)           : pMsgNode -     Pointer to the received message       */
/*                                 structure                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmHandleSnmpCfgMsg (tElmMsgNode * pMsgNode)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if (!((pMsgNode->MsgType == ELM_CREATE_PORT_MSG) ||
          (pMsgNode->MsgType == ELM_MAP_PORT_MSG)))
    {
        if (pMsgNode->u4IfIndex != ELM_MSG_INVALID_PORT)
        {
            if (ELM_PORT_TBL () != NULL)
            {
                pElmPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
            }

            if (pElmPortEntry != NULL)
            {
                ElmHandleLocalMsgReceived (pMsgNode);
                return;
            }
            else
            {
                /* Should reach here only if the module is shutdown */
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC
                         | ELM_EVENT_HANDLING_TRC,
                         "MSG: Unknown port received in message !!\n");

                ElmDefaultCfgMsgHandler (pMsgNode);
                return;
            }
        }
    }

    if ((pMsgNode->MsgType == ELM_DISABLE_MSG)
        || (pMsgNode->MsgType == ELM_ENABLE_MSG)
        || (pMsgNode->MsgType == ELM_START_ELM_MSG)
        || (pMsgNode->MsgType == ELM_STOP_ELM_MSG)
        || (pMsgNode->MsgType == ELM_CREATE_PORT_MSG)
        || (pMsgNode->MsgType == ELM_MAP_PORT_MSG))
    {

        ElmHandleLocalMsgReceived (pMsgNode);
    }
}

/*****************************************************************************/
/* Function Name      : ElmDefaultCfgMsgHandler                              */
/*                                                                           */
/* Description        : This function does the default processing (like      */
/*                      releasing locks) when the port number                */
/*                      that is encoded in the message is not known to       */
/*                      ELMI                                                 */
/*                                                                           */
/* Input(s)           : pMsgNode - pointer to the message node               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmDefaultCfgMsgHandler (tElmMsgNode * pMsgNode)
{
    ELM_GLOBAL_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                    "MSG: Unkonwn port in message. Message is being"
                    "handled by the default handler!\n");

    switch (pMsgNode->MsgType)
    {

        case ELM_MAP_PORT_MSG:
            L2MI_SYNC_GIVE_SEM();
            break;
        case ELM_CREATE_PORT_MSG:
            L2_SYNC_GIVE_SEM ();
            break;
        case ELM_ENABLE_PORT_MSG:
            break;

        case ELM_DISABLE_PORT_MSG:
            break;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleLocalMsgReceived                            */
/*                                                                           */
/* Description        : This function decodes the type of local message      */
/*                      posted and calls the corresponding message handler.  */
/*                                                                           */
/* Input(s)           : pMsgNode - pointer to the message node               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ElmLocalMsgMemPoolId                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleLocalMsgReceived (tElmMsgNode * pMsgNode)
{
    INT4                i4RetVal = (INT4) ELM_SUCCESS;
    tElmPortEntry      *pPortEntry = NULL;
    /* Assumptions:
     * 1) ELM Lock already taken
     * 2) The u4IfIndex field inside the MsgNode 
     *    refers to the Actual Interface No */

    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             "MSG: Handling received local message ...\n");

    if ((pMsgNode->MsgType != ELM_START_ELM_MSG) &&
        (pMsgNode->MsgType != ELM_STOP_ELM_MSG))
    {
        if (!ELM_IS_ELMI_STARTED ())
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Module not started to Process Local Message!\n");

            ElmDefaultCfgMsgHandler (pMsgNode);
            return (INT4) ELM_SUCCESS;
        }
    }

    switch (pMsgNode->MsgType)
    {
        case ELM_START_ELM_MSG:

            if (ELM_IS_ELMI_STARTED ())
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MGMT_TRC,
                         "MSG: ELM is already Started.\n");
                break;
            }

            /* Starting ELM Module */
            if (ElmHandleModuleInit () != ELM_SUCCESS)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: ElmHandleModuleInit function returned FAILURE!!!\n");
                i4RetVal = (INT4) ELM_FAILURE;
                break;
            }
            ELM_TRC (ELM_INIT_SHUT_TRC,
                     "MSG: ELM Module Initialised Successfully...\n");
            break;

        case ELM_STOP_ELM_MSG:

            if (!ELM_IS_ELMI_STARTED ())
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MGMT_TRC,
                         "MSG: ELM Module not started..No shutdown to perform !\n");
                i4RetVal = ELM_SUCCESS;
                break;
            }
            /* Shutting Down ELM Module */
            ElmHandleModuleShutdown ();

            ELM_TRC (ELM_INIT_SHUT_TRC,
                     "MSG: ELM Module Shutdown Successfully...\n");
            break;

        case ELM_MAP_PORT_MSG:

            if (ElmHandleCreatePort (pMsgNode->u4IfIndex) == ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %s Success...\\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %s FAILED...\\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            L2MI_SYNC_GIVE_SEM ();
            break;

        case ELM_CREATE_PORT_MSG:

            if (ElmHandleCreatePort (pMsgNode->u4IfIndex) == ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %s Success...\\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %s FAILED...\\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            L2_SYNC_GIVE_SEM ();
            break;

        case ELM_UNMAP_PORT_MSG:
            if (ElmHandleDeletePort (pMsgNode->u4IfIndex) == ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Delete Port = %d Success...\\n",
                              pMsgNode->u4IfIndex);
            }
            else
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %d FAILED...\\n",
                              pMsgNode->u4IfIndex);
            }
            L2MI_SYNC_GIVE_SEM ();
            break;

        case ELM_DELETE_PORT_MSG:
            if (ElmHandleDeletePort (pMsgNode->u4IfIndex) == ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Delete Port = %d Success...\\n",
                              pMsgNode->u4IfIndex);
            }
            else
            {
                ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_EVENT_HANDLING_TRC,
                              "MSG: ELM Create Port = %d FAILED...\\n",
                              pMsgNode->u4IfIndex);
            }
            break;

        case ELM_ENABLE_MSG:

            if (!ELM_IS_ELMI_STARTED ())
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: ELM System Shutdown; CANNOT Enable ELM\n");
                i4RetVal = ELM_FAILURE;
                break;
            }

            if (ElmHandleElmiGlobalEnable () != ELM_SUCCESS)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: ElmHandleElmiGlobalEnable function returned "
                         "FAILURE\n");
                i4RetVal = (INT4) ELM_FAILURE;
            }
            else
            {
                ELM_TRC (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                         ELM_MGMT_TRC,
                         "MSG: Management Enabling of ELM Module globally"
                         "Success\n");
            }
            break;

        case ELM_DISABLE_MSG:

            if (!ELM_IS_ELMI_STARTED ())
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: ELM System Shutdown; CANNOT Disable ELM\n");
                i4RetVal = ELM_FAILURE;
                break;
            }

            if (ElmHandleElmiGlobalDisable () != ELM_SUCCESS)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: ElmHandleElmiGlobalDisable function returned "
                         "FAILURE\n");
                i4RetVal = (INT4) ELM_FAILURE;
            }
            else
            {
                ELM_TRC (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                         ELM_MGMT_TRC,
                         "MSG: Management Disabling of ELM Module globally"
                         "Success\n");
            }
            break;

        case ELM_ENABLE_PORT_MSG:

            if (ElmModuleEnablePerPort (pMsgNode->u4IfIndex,
                                        pMsgNode->Msg.u1TrigType) ==
                ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Enabling of Port %s Success\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Enabling of Port %s Failed\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
                i4RetVal = (INT4) ELM_FAILURE;
            }
            break;

        case ELM_DISABLE_PORT_MSG:

            if (ElmModuleDisablePerPort (pMsgNode->u4IfIndex,
                                         pMsgNode->Msg.u1TrigType) ==
                ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Disabling of Port %s Success\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Disabling of Port %s Failed\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
                i4RetVal = (INT4) ELM_FAILURE;
            }
            break;

        case ELM_PORT_ELMI_ENABLE_MSG:

            if (ElmModuleEnablePerPort (pMsgNode->u4IfIndex, ELM_PORT_UP) ==
                ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Enabling of ELMI on Port %s Success\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Enabling of Port %s Failed\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
                i4RetVal = (INT4) ELM_FAILURE;
            }
            break;

        case ELM_PORT_ELMI_DISABLE_MSG:

            if (ElmModuleDisablePerPort (pMsgNode->u4IfIndex, ELM_PORT_DOWN) ==
                ELM_SUCCESS)
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Disabling of ELMI on Port %s Success\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
            }
            else
            {
                ELM_TRC_ARG1 (ELM_EVENT_HANDLING_TRC | ELM_INIT_SHUT_TRC |
                              ELM_MGMT_TRC,
                              "MSG: Management Disabling of Port %s Failed\n",
                              ELM_GET_IFINDEX_STR (pMsgNode->u4IfIndex));
                i4RetVal = (INT4) ELM_FAILURE;
            }
            break;

        case ELM_CONFIGURE_PVT_VALUE_MSG:

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_CUSTOMER_SIDE)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Port configured as Customer port, Cannot "
                                "Configure PVT at Customer side\n");
                i4RetVal = (INT4) ELM_FAILURE;
            }
            else
            {
                pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
                if (pPortEntry == NULL)
                {
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: No Port Information available\n");
                    i4RetVal = (INT4) ELM_FAILURE;
                    break;
                }
                pPortEntry->u1PvtValueConfigured = pMsgNode->Msg.u1PvtValue;
                if (pPortEntry->pPvtTmr != NULL)
                {
                    ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT);
                    if (ElmStartTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT,
                                       pPortEntry->u1PvtValueConfigured) ==
                        ELM_SUCCESS)
                    {
                        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                        "MSG: Polling Verification Timer Re-Started "
                                        "Successfully\n");
                    }
                    else
                    {
                        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                        "MSG: Polling Verification Timer Re-Start "
                                        "FAILED\n");
                        i4RetVal = (INT4) ELM_FAILURE;
                    }
                }
            }
            break;

        case ELM_CONFIGURE_PT_VALUE_MSG:

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_NETWORK_SIDE)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Port configured as Network port, Cannot "
                                "Configure PT at Network side\n");
                i4RetVal = (INT4) ELM_FAILURE;
            }
            else
            {
                pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
                if (pPortEntry == NULL)
                {
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: No Port Information available\n");
                    i4RetVal = (INT4) ELM_FAILURE;
                    break;
                }

                pPortEntry->u1PtValueConfigured = pMsgNode->Msg.u1PtValue;
                if (pPortEntry->pPtTmr != NULL)
                {
                    ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PT);
                    if (ElmStartTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PT,
                                       pPortEntry->u1PtValueConfigured) ==
                        ELM_SUCCESS)
                    {
                        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                        "MSG: Polling Timer Re-Started Successfully\n");
                    }
                    else
                    {
                        ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                        "MSG: Polling Timer Re-Start FAILED\n");
                        i4RetVal = (INT4) ELM_FAILURE;
                    }
                }
            }
            break;

        case ELM_CONFIGURE_POLLING_COUNTER_MSG:

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_NETWORK_SIDE)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Port configured as Network switch, Cannot "
                                "Configure Polling Counter at Network side\n");
                i4RetVal = (INT4) ELM_FAILURE;
            }
            else
            {
                pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
                if (pPortEntry == NULL)
                {
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: No Port Information available\n");
                    i4RetVal = (INT4) ELM_FAILURE;
                    break;
                }

                pPortEntry->u2PollingCounterConfigured =
                    pMsgNode->Msg.u2PollingCounterValue;

                pPortEntry->u2PollingCounter =
                    pMsgNode->Msg.u2PollingCounterValue;

                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Polling Counter Value Configured successfully \n");
            }
            break;

        case ELM_CONFIGURE_STATUS_COUNTER_MSG:

            pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
            if (pPortEntry == NULL)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: No Port Information available\n");
                i4RetVal = (INT4) ELM_FAILURE;
                break;
            }
            pPortEntry->u1StatusCounterConfigured =
                pMsgNode->Msg.u1StatusCounterValue;

            pPortEntry->u1StatusCounter = pMsgNode->Msg.u1StatusCounterValue;

            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                            "MSG: Status Counter Value Configured successfully \n");

            break;

        case ELM_EVC_STATUS_CHANGE_MSG:

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_NETWORK_SIDE)
            {
                if (ELM_IS_ELMI_ENABLED_ON_PORT (pMsgNode->u4IfIndex) ==
                    ELM_TRUE)
                {
                    ElmHandleEvcStatusChangeEvent (pMsgNode->u4IfIndex,
                                                   pMsgNode->u2EvcRefId,
                                                   pMsgNode->Msg.u1EvcStatus);

                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: EVC Status Change Event Processed Successfully\n");
                }
            }
            break;

        case ELM_LCM_INFORMATION_UPDATE_MSG:
        case ELM_UNI_INFORMATION_UPDATE_MSG:

            if ((ELM_IS_ELMI_ENABLED_ON_PORT (pMsgNode->u4IfIndex) == ELM_TRUE)
                && (ELM_GET_IF_STATUS (pMsgNode->u4IfIndex) ==
                    ELM_PORT_OPER_UP))
            {
                if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
                {
                    /* Standby Node should not process this event */
                    ELM_TRC (ELM_CONTROL_PATH_TRC,
                             "LCM Messages not processed by Standby node\n");
                    break;
                }
                pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
                if (pPortEntry != NULL)
                {
                    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                                  "PortNo=%d\n", pMsgNode->u4IfIndex);
                    if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) ==
                        ELM_NETWORK_SIDE)
                    {
                        ELM_INCR_DATA_INSTANCE (pMsgNode->u4IfIndex);
                        if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                            (ELM_IS_STANDBY_UP () != ELM_FALSE))
                        {

                            ElmRedSyncUpDataInstance (pPortEntry);
                        }
                        ElmPreProcessPackets (pMsgNode->u4IfIndex);
                    }
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: Information Change Event Processed Successfully\n");
                    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                                  "Data Instance Updated to : %d\n",
                                  pPortEntry->u4DataInstance);
                }
                else
                {
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "ERR: Port Does NOT Exist!!\n");
                    i4RetVal = (INT4) ELM_FAILURE;
                }
            }
            break;

        case ELM_UNI_NETWORK_ENABLE_MSG:

            pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_NETWORK_SIDE)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Port already configured as Network port\n");
                i4RetVal = (INT4) ELM_SUCCESS;
            }
            else
            {
                if (ELM_IS_ELMI_ENABLED_ON_PORT (pMsgNode->u4IfIndex))
                {

                    ElmModuleDisablePerPort (pMsgNode->u4IfIndex,
                                             ELM_PORT_DOWN);

                    ELM_SET_PORT_MODE (pMsgNode->u4IfIndex, ELM_NETWORK_SIDE);

                    pPortEntry = ELM_GET_PORTENTRY (pMsgNode->u4IfIndex);
                    ElmModuleEnablePerPort (pMsgNode->u4IfIndex, ELM_PORT_UP);
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: Port Mode Configured successfully \n");
                    i4RetVal = (INT4) ELM_SUCCESS;
                }
                else
                {
                    ELM_SET_PORT_MODE (pMsgNode->u4IfIndex, ELM_NETWORK_SIDE);
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: Port Mode Configured successfully \n");
                    i4RetVal = (INT4) ELM_SUCCESS;

                }

            }
            break;

        case ELM_UNI_CUSTOMER_ENABLE_MSG:

            if (ELM_GET_PORT_MODE (pMsgNode->u4IfIndex) == ELM_CUSTOMER_SIDE)
            {
                ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                "MSG: Port already configured as Customer port\n");
                i4RetVal = (INT4) ELM_SUCCESS;
            }
            else
            {
                if (ELM_IS_ELMI_ENABLED_ON_PORT (pMsgNode->u4IfIndex))
                {
                    ElmModuleDisablePerPort (pMsgNode->u4IfIndex,
                                             ELM_PORT_DOWN);

                    ELM_SET_PORT_MODE (pMsgNode->u4IfIndex, ELM_CUSTOMER_SIDE);

                    ElmModuleEnablePerPort (pMsgNode->u4IfIndex, ELM_PORT_UP);

                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: Port Mode Configured successfully \n");
                    i4RetVal = (INT4) ELM_SUCCESS;
                }
                else
                {
                    ELM_SET_PORT_MODE (pMsgNode->u4IfIndex, ELM_CUSTOMER_SIDE);
                    ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                    "MSG: Port Mode Configured successfully \n");
                    i4RetVal = (INT4) ELM_SUCCESS;

                }

            }
            break;

        default:
            ELM_TRC (ELM_EVENT_HANDLING_TRC | ELM_MGMT_TRC |
                     ELM_ALL_FAILURE_TRC, "MSG: Invalid Message Type\n");
            break;

    }                            /* End of Switch Block */

    if (i4RetVal == (INT4) ELM_SUCCESS)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MGMT_TRC | ELM_EVENT_HANDLING_TRC,
                 "MSG: Successfully processed received local message ..\n");
    }

    return (i4RetVal);

}
