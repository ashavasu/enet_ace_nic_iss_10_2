#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmRedOperStatusChangeIndication                     */
/*                                                                           */
/* Description        : This function is invoked whenever port's operational */
/*                      status changes on an active node                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index of a port                */
/*                      u1Status - Port Status                               */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
ElmRedOperStatusChangeIndication (UINT4 u4IfIndex, UINT1 u1Status)
{
    ELM_UNUSED (u4IfIndex);
    ELM_UNUSED (u1Status);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedRmInit                                         */
/*                                                                           */
/* Description        : Initialise the RED Global variables for ELMI         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmRedRmInit (VOID)
{
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Registers ELMI with RM by providing an application   */
/*                      ID for ELMI and a call back function to be called    */
/*                      whenever RM needs to send an event to ELMI           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then ELM_SUCCESS          */
/*                      Otherwise ELM_FAILURE                                */
/*****************************************************************************/
INT4
ElmRedRegisterWithRM (VOID)
{
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmProcessRmEvent                                    */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmProcessRmEvent (tElmRmCtrlMsg * pMsg)
{
    ELM_UNUSED (pMsg);
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleBulkUpdateEvent                          */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      ElmRedHandleBulkRequest is triggered.                */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleBulkUpdateEvent (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedSyncUpDataInstance.                            */
/*                                                                           */
/* Description        : This function calls for synching up of DI of active  */
/*                      and standby when DI changes at the active UNI-N Side */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port entry.           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmRedSyncUpDataInstance (tElmPortEntry * pElmPortEntry)
{
    ELM_UNUSED (pElmPortEntry);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSyncUpPerPortInfo                              */
/*                                                                           */
/* Description        : This function will send the Sync up information for a*/
/*                      particular port                                      */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE.                           */
/*****************************************************************************/
INT4
ElmRedSyncUpPerPortInfo (UINT4 u4PortIndex)
{
    ELM_UNUSED (u4PortIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedFillHeader                                     */
/*                                                                           */
/* Description        : Forms the RM header                                  */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                      ppBuf - Pointer to the allocated buffer              */
/*                      u2DataLength - Length of the PDU                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillHeader (tElmPortEntry * pElmPortEntry, tRmMsg ** ppBuf,
                  UINT2 u2DataLength)
{
    ELM_UNUSED (pElmPortEntry);
    ELM_UNUSED (ppBuf);
    ELM_UNUSED (u2DataLength);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedFillFixedTlv                                   */
/*                                                                           */
/* Description        : This function fills the fixed TLV for RM message     */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                      ppBuf - Pointer to the allocated buffer              */
/*                      u1ReportType - Report Type for the transmitted PDU   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillFixedTlv (tElmPortEntry * pElmPortEntry, tRmMsg ** ppBuf,
                    UINT1 u1ReportType)
{
    ELM_UNUSED (pElmPortEntry);
    ELM_UNUSED (ppBuf);
    ELM_UNUSED (u1ReportType);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmFillUniStatusIe                                   */
/*                                                                           */
/* Description        : This function is called to fill the UNI Status IE.   */
/*                                                                           */
/* Input(s)           : CfaUniInfo - The CFA Port Entry structure            */
/*                      ppBuf - Pointer to the allocated RM buffer           */
/*                      u1UniStatusIeLength - Precalculated length of UNI    */
/*                      Status IE                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillUniStatusIe (tCfaUniInfo CfaUniInfo, tRmMsg ** ppBuf,
                       UINT1 u1UniStatusIeLength)
{
    ELM_UNUSED (CfaUniInfo);
    ELM_UNUSED (ppBuf);
    ELM_UNUSED (u1UniStatusIeLength);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedSendMsgToRm                                    */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if sync msg is sent then ELM_SUCCESS                 */
/*                      Otherwise ELM_FAILURE                                */
/*****************************************************************************/
INT4
ElmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{
    ELM_UNUSED (pMsg);
    ELM_UNUSED (u2BufSize);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSynchupStatusMsg                               */
/*                                                                           */
/* Description        : This function is called whenever Asynchronous Status,*/
/*                      Full Status or Full Status Contd PDUs are required   */
/*                      to be transmitted to the standby node.               */
/*                      This is required so that the LCM database at the     */
/*                      standby node comes in synch with that present at the */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : pu1RedBuf - Pointer to the received PDU              */
/*                      u4IfIndex - Interface Index of a port                */
/*                      u2DataLength - Length of the received PDU            */
/*                      u1MsgType - Message Type                             */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedSynchupStatusMsg (UINT4 u4DataInstance, UINT4 u4IfIndex,
                        UINT2 u2DataLength, UINT1 u1MsgType, UINT1 *pu1RedBuf)
{
    ELM_UNUSED (u4DataInstance);
    ELM_UNUSED (u4IfIndex);
    ELM_UNUSED (u2DataLength);
    ELM_UNUSED (u1MsgType);
    ELM_UNUSED (pu1RedBuf);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedPassCeVlanInfo                                 */
/*                                                                           */
/* Description        : This function is called when the active indicates    */
/*                      that CE-VLAN info has changed and thus the standby   */
/*                      should update its database                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      pLcmCeVlanInfo - Pointer to the Changed CE Vlan Info */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedPassCeVlanInfo (UINT4 u4IfIndex, tLcmEvcCeVlanInfo * pLcmCeVlanInfo)
{
    ELM_UNUSED (u4IfIndex);
    ELM_UNUSED (pLcmCeVlanInfo);
    return;
}
