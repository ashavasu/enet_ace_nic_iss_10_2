/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmsys.c,v 1.21 2011/04/27 11:05:45 siva Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              ELMI Module.
 *
 *******************************************************************/

#include "elminc.h"

INT1                ELM_PORT_TRAPS_OID[256];
extern unsigned int EoidGetEnterpriseOid (void);
/*****************************************************************************/
/* Function Name      : ElmHandleModuleInit                                  */
/*                                                                           */
/* Description        : This function is called at the time of starting up   */
/*                      of E-LMI. This function will reserve the memory pool */
/*                      for Port Entry and EVC information. This function is */
/*                      responsible for initializing the local database and  */
/*                      other protocol specific values                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : The local database gets initialized.                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmHandleModuleInit (VOID)
{
    INT4                i4SysLogId = ELM_INIT_VAL;
    UINT2               u2PrevPort = ELM_INIT_VAL;
    UINT2               u2IfIndex = ELM_INIT_VAL;

    if (ELM_IS_ELMI_STARTED ())
    {
        return ELM_SUCCESS;
    }
    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             " Initializing ELMI Module ... \n");

    MEMSET (&gFsElmSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsElmSizingInfo.ModName, "ELMI", STRLEN ("ELMI"));
    gFsElmSizingInfo.u4ModMemPreAllocated = 0;
    gFsElmSizingInfo.ModSizingParams = gFsElmSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsElmSizingInfo);
#ifdef SNMP_2_WANTED
    SPRINTF ((CHR1 *) ELM_PORT_TRAPS_OID, "1.3.6.1.4.1.%d.159.2.4.1",
             EoidGetEnterpriseOid ());
#endif
    if (ElmiSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: Memory pool creation failed !!!\n");
        ElmHandleModuleShutdown ();
        return ELM_FAILURE;
    }

    /*Assigning respective mempool Id's */
    ElmAssignMempoolIds ();

    /* Initialise the Timer submodule */
    if (ElmTimerInit () != ELM_SUCCESS)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: Timer Module Initialisation failed !!!\n");
        return ELM_FAILURE;
    }

    if (ElmAllocPortInfoBlock () != ELM_SUCCESS)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC |
                 ELM_ALL_FAILURE_TRC,
                 "SYS: Mem alloc failed for PortTable Table "
                 "during Elmi Initialization. \r\n");
        ElmHandleModuleShutdown ();

        return ELM_FAILURE;
    }

    gElmGlobalInfo.u2NoOfElmiEnabledPorts = (UINT2) ELM_INIT_VAL;

    while (L2IwfGetNextValidPort (u2PrevPort, &u2IfIndex) == L2IWF_SUCCESS)
    {
        if (ElmHandleCreatePort ((UINT4) u2IfIndex) != ELM_SUCCESS)
        {
            ElmHandleModuleShutdown ();
            return ELM_FAILURE;
        }
        u2PrevPort = u2IfIndex;
    }

    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "ELMI", SYSLOG_ALERT_LEVEL);
    if (i4SysLogId <= 0)
    {
        ELM_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                 "Registration with Syslog FAILED\n");
        ElmHandleModuleShutdown ();
        return ELM_FAILURE;
    }
    gElmGlobalInfo.u4SysLogId = (UINT4) i4SysLogId;

    /*By default Trap State  is disabled */
    ELM_TRAP_TYPE = ELM_DISABLE_ALL_TRAPS;

    ELM_GLOBAL_TRAP = ELM_DISABLED;

    ELM_SYSTEM_CONTROL = (UINT1) ELM_START;

    ELM_IS_ELMI_INITIALISED () = ELM_TRUE;

    ELM_GLOBAL_STATUS = ELM_ENABLE_GLOBAL;
    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             "SYS: Initialised ELMI Module SUCCESSFULLY... \n");
    if (ElmRedRmInit () != ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }
    /* Register with RM module, to get event and messages from RM. */
    if (ElmRedRegisterWithRM () != ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleModuleShutdown                              */
/*                                                                           */
/* Description        : This function gets the Timer submodule               */
/*                      deinitialization, deletes the memory pools.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1ElmSystemControl                                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmHandleModuleShutdown (VOID)
{
    tElmQMsg           *pQMsg = NULL;
    tElmPortEntry      *pPortEntry = NULL;
    UINT4               u4IfIndex = ELM_INIT_VAL;

    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             "SYS: Performing Shutdown of ELMI Module ... \n");

    if (!ELM_IS_ELMI_STARTED ())
    {
        return;
    }
    ELM_IS_ELMI_INITIALISED () = ELM_FALSE;
    while (ELM_GET_PDU_FROM_ELM_QUEUE (ELM_INPUT_QID,
                                       (UINT1 *) &pQMsg,
                                       ELM_DEF_MSG_LEN,
                                       ELM_OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* Processing of PDUs */
        if ((pQMsg == NULL) || (ELM_QMSG_TYPE (pQMsg) != ELM_PDU_RCVD_QMSG))
        {
            continue;
        }
        if (ELM_RELEASE_CRU_BUF (pQMsg->uQMsg.pPduInQ, ELM_FALSE) ==
            ELM_CRU_FAILURE)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC |
                            ELM_EVENT_HANDLING_TRC,
                            "MSG: Received Pdu CRU Buffer Release "
                            "FAILED!\n");
        }
        if (ELM_RELEASE_QMSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                            "MSG: Release of Local Msg Memory Block"
                            "FAILED!\n");
        }
    }
    while (ELM_GET_PDU_FROM_ELM_QUEUE (ELM_CFG_QID,
                                       (UINT1 *) &pQMsg,
                                       ELM_DEF_MSG_LEN,
                                       ELM_OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* Processing of PDUs */
        if ((pQMsg == NULL) || (ELM_QMSG_TYPE (pQMsg) != ELM_PDU_RCVD_QMSG))
        {
            continue;
        }
        if (ELM_RELEASE_LOCALMSG_MEM_BLOCK
            (pQMsg->uQMsg.pMsgNode) != MEM_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                            ELM_MGMT_TRC,
                            "MSG: Release of Local Msg Memory "
                            "Block FAILED!\n");

        }
        if (ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC |
                            ELM_MGMT_TRC,
                            "MSG: Release of Q Msg Memory " "Block FAILED!\n");
        }
    }

    (VOID) ElmHandleElmiGlobalDisable ();

    for (u4IfIndex = 1; u4IfIndex <= ELM_MAX_NUM_PORTS_SUPPORTED; u4IfIndex++)
    {
        pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
        if (pPortEntry == NULL)
        {
            continue;
        }
        (VOID) ELM_RELEASE_PORT_INFO_MEM_BLOCK (pPortEntry);
        ELM_SET_PORTENTRY (u4IfIndex, NULL);
    }

    gElmGlobalInfo.u4BufferFailureCount = ELM_INIT_VAL;
    gElmGlobalInfo.u4MemoryFailureCount = ELM_INIT_VAL;

    if (ElmTimerDeInit () != ELM_SUCCESS)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: Timer Deinit failed !!!\n");
        return;
    }
    /* Delete all mempools in ELMI module */
    ElmiSizingMemDeleteMemPools ();

    SYS_LOG_DEREGISTER (gElmGlobalInfo.u4SysLogId);
    gElmGlobalInfo.u4SysLogId = 0;
    ELM_SYSTEM_CONTROL = (UINT1) ELM_SHUTDOWN;

    ElmRedRmInit ();
#ifdef L2RED_WANTED
    if (ElmRedDeRegisterWithRM () == ELM_FAILURE)
    {
        return;
    }
#endif
    ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
             "SYS: ELMI Module Shutdown SUCCESSFULLY\n");
}

/*****************************************************************************/
/* Function Name      : ElmInitPortInfo                                      */
/*                                                                           */
/* Description        : Initialises the Spanning Tree Specific Port          */
/*                      Information                                          */
/*                                                                           */
/* Input(s)           : pPortEntry - Port Information                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
ElmInitPortInfo (tElmPortEntry * pPortEntry)
{
    pPortEntry->pPvtTmr = NULL;
    pPortEntry->pPtTmr = NULL;
    pPortEntry->pAsyncMsgTimer = NULL;
    pPortEntry->u4LastReferenceIdSent = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxElmiCheckEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxFullStatusEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxFullStatusContdEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxElmiCheckMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxFullStatusMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxFullStatusContdMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxAsyncStatusMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxElmiCheckEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxFullStatEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiTxFullStatContdEnquiryMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxCheckMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxFullStatMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxFullStatContdMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiRxAsyncStatusMsg = ELM_INIT_VAL;
    pPortEntry->u4ElmiValidMsgRxed = ELM_INIT_VAL;
    pPortEntry->u4ElmiInvalidMsgRxed = ELM_INIT_VAL;
    pPortEntry->u4ElmiRelErrStatusTimeoutCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiRelErrInvalidSeqNumCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiRelErrInvalidStatusCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiRelErrRxUnsolicitedStatusCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrInvalidProtVerCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrInvalidEvcRefIdCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrInvalidMessageTypeCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrOutOfSequenceInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrDuplicateInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrMandatoryInfoEleMissingCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrInvalidMandatoryInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrInvalidNonMandatoryInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrUnrecognizedInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrUnexpectedInfoEleCount = ELM_INIT_VAL;
    pPortEntry->u4ElmiProErrShortMessageCount = ELM_INIT_VAL;
    pPortEntry->u1ElmiMode = ELM_NETWORK_SIDE;
    pPortEntry->u2NoOfEvcConfigured = ELM_INIT_VAL;
    pPortEntry->u1ElmiPortStatus = ELM_PORT_ELMI_DISABLED;
    pPortEntry->u1ElmiPortStatusBackup = ELM_PORT_ELMI_DISABLED;
    pPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
    pPortEntry->u1PvtValueConfigured = ELM_DEFAULT_PVT_TIMER_VALUE;
    pPortEntry->u1PtValueConfigured = ELM_DEFAULT_PT_TIMER_VALUE;
    pPortEntry->u2PollingCounterConfigured = ELM_DEFAULT_POLLING_COUNTER_VALUE;
    pPortEntry->u2PollingCounter = ELM_DEFAULT_POLLING_COUNTER_VALUE;
    pPortEntry->u1StatusCounterConfigured = ELM_CONFIGURE_STATUS_COUNTER_MSG;
    pPortEntry->u1StatusCounter = ELM_CONFIGURE_STATUS_COUNTER_MSG;
    pPortEntry->u1LastReportTypeTxed = ELM_INIT_VAL;
    pPortEntry->u1SendSeqCounter = ELM_INIT_VAL;
    pPortEntry->u1RecvSeqCounter = ELM_INIT_VAL;
    pPortEntry->u1CeVlanInfoRemaining = ELM_INIT_VAL;
    pPortEntry->u1EvcStatusRemaining = ELM_INIT_VAL;
    pPortEntry->u2CeVlanSentInPrevMsg = ELM_INIT_VAL;
    pPortEntry->bRecvdFullStatus = ELM_INIT_VAL;
    pPortEntry->bMsgRcvdInTime = ELM_FALSE;
    pPortEntry->bLaPort = ELM_FALSE;
    pPortEntry->bElmStatusInformationChange = ELM_FALSE;
    pPortEntry->bElmRedBulkUpdInProgress = ELM_FALSE;
    pPortEntry->u1ElmRedRecvSeqCounter = ELM_INIT_VAL;
    pPortEntry->u1ElmRedSendSeqCounter = ELM_INIT_VAL;
}

/*****************************************************************************/
/* Function Name      : ElmHandleElmiGlobalEnable                            */
/*                                                                           */
/* Description        : Called by management when ELMI is enabled globally.  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gElmGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleElmiGlobalEnable (VOID)
{
    tElmPortEntry      *pPortEntry = NULL;
    UINT4               u4IfIndex = ELM_INIT_VAL;

    if (ELM_GLOBAL_STATUS == ELM_ENABLE_GLOBAL)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                 "SYS: ELMI Ports are already enabled globally\n");
        return ELM_SUCCESS;
    }

    ELM_GLOBAL_STATUS = ELM_ENABLE_GLOBAL;
    for (u4IfIndex = 1; u4IfIndex <= ELM_MAX_NUM_PORTS_SUPPORTED; u4IfIndex++)
    {
        if ((pPortEntry = ELM_GET_PORTENTRY (u4IfIndex)) == NULL)
        {
            continue;
        }

        if (pPortEntry->u1ElmiPortStatusBackup == ELM_PORT_ELMI_ENABLED)
        {
            pPortEntry->u1ElmiPortStatusBackup = ELM_PORT_ELMI_DISABLED;

            if (ElmModuleEnablePerPort (u4IfIndex, ELM_PORT_UP) != ELM_SUCCESS)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                         "SYS: ELMI Module Enable Per Port Error\n");
                return ELM_FAILURE;
            }
        }
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleElmiGlobalDisable                           */
/*                                                                           */
/* Description        : Called by management when ELMI is disabled globally. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gElmGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleElmiGlobalDisable (VOID)
{
    tElmPortEntry      *pPortEntry = NULL;
    UINT4               u4IfIndex = ELM_INIT_VAL;

    ELM_GLOBAL_STATUS = ELM_DISABLE_GLOBAL;
    for (u4IfIndex = 1; u4IfIndex <= ELM_MAX_NUM_PORTS_SUPPORTED; u4IfIndex++)
    {
        if ((pPortEntry = ELM_GET_PORTENTRY (u4IfIndex)) == NULL)
        {
            continue;
        }

        if (pPortEntry->u1ElmiPortStatus == ELM_PORT_ELMI_ENABLED)
        {
            pPortEntry->u1ElmiPortStatusBackup = ELM_PORT_ELMI_ENABLED;
        }

        if (ElmModuleDisablePerPort (u4IfIndex, ELM_PORT_DOWN) != ELM_SUCCESS)
        {
            ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                     "SYS: ELMI Module Disable Per Port Error\n");
            return ELM_FAILURE;
        }
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmModuleEnablePerPort                               */
/*                                                                           */
/* Description        : Called by management when ELMI is enabled Per Port   */
/*                                                                           */
/* Input(s)           : Pointer to Port Info structure                       */
/*                      Trigger Type = From CFA or ELMI Module               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmModuleEnablePerPort (UINT4 u4IfIndex, UINT1 u1TriggerType)
{
    tElmPortEntry      *pPortEntry = NULL;
    tElmSll            *pElmList = NULL;

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pPortEntry == NULL)
    {
        return ELM_FAILURE;
    }
    if (u1TriggerType == ELM_EXT_PORT_UP)
    {
        if ((pPortEntry->u1PortStatus == ELM_PORT_OPER_UP)
            && (pPortEntry->bLaPort == ELM_FALSE))
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "Port is Oper up & Elmi is allready enabled on the port ! \n");
            return ELM_SUCCESS;
        }
        pPortEntry->bLaPort = ELM_FALSE;
        pPortEntry->u1PortStatus = ELM_PORT_OPER_UP;

        if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_CUSTOMER_SIDE)
        {
            if ((ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex))
                && (ELM_IS_ELMI_ENABLED ()))
            {
                ElmFlushLcmDataBase (u4IfIndex);
                pPortEntry->u4DataInstance = ELM_INIT_VAL;
                pPortEntry->u1SendSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1RecvSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                pPortEntry->u2PollingCounter =
                    pPortEntry->u2PollingCounterConfigured;
                pPortEntry->u1StatusCounter =
                    pPortEntry->u1StatusCounterConfigured;
                if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
                {
                    /* Standby Node should not transmit the Enquiry messages */
                    return ELM_SUCCESS;
                }
                if (ElmPortTxEnquiryMessage (pPortEntry) != ELM_SUCCESS)
                {
                    return ELM_FAILURE;
                }
            }
            else
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC |
                         ELM_ALL_FAILURE_TRC,
                         "ERR:Either ELMI is down or ELMI is not enabled Globally.\n");
            }
        }
        else if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_NETWORK_SIDE)
        {
            if ((ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex))
                && (ELM_IS_ELMI_ENABLED ()))
            {
                pElmList = ELM_SLL_LIST (u4IfIndex);
                if (pElmList == NULL)
                {
                    return ELM_FAILURE;
                }
                ELM_SLL_INIT (pElmList);
                pPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                pPortEntry->u4DataInstance = DATA_INSTANCE_INTIAL_VALUE;
                pPortEntry->u1SendSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1RecvSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1StatusCounter =
                    pPortEntry->u1StatusCounterConfigured;
                if (ElmStartTimer
                    ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT,
                     pPortEntry->u1PvtValueConfigured) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "PVT Timer NOT Started ! \n");
                    return ELM_FAILURE;
                }
            }
            else
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC |
                         ELM_ALL_FAILURE_TRC,
                         "ERR:Either ELMI is down or ELMI is not enabled Globally.\n");
            }
        }
    }
    else if (u1TriggerType == ELM_PORT_UP)
    {
        if (pPortEntry->u1ElmiPortStatus == ELM_PORT_ELMI_ENABLED)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "Port is Oper up & Elmi is allready enabled on the port ! \n");
            return ELM_SUCCESS;
        }

        pPortEntry->u1ElmiPortStatus = ELM_PORT_ELMI_ENABLED;

        if ((ELM_IS_ELMI_ENABLED ())
            && ((pPortEntry->u1PortStatus) == ELM_PORT_OPER_UP))
        {
            if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_CUSTOMER_SIDE)
            {
                ElmFlushLcmDataBase (u4IfIndex);
                pPortEntry->u4DataInstance = ELM_INIT_VAL;
                pPortEntry->u1SendSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1RecvSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                pPortEntry->u2PollingCounter =
                    pPortEntry->u2PollingCounterConfigured;
                pPortEntry->u1StatusCounter =
                    pPortEntry->u1StatusCounterConfigured;
                if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
                {
                    /* Standby Node should not transmit the Enquiry messages */
                    return ELM_SUCCESS;
                }
                if (ElmPortTxEnquiryMessage (pPortEntry) != ELM_SUCCESS)
                {
                    return ELM_FAILURE;
                }
            }
            if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_NETWORK_SIDE)
            {
                pElmList = ELM_SLL_LIST (u4IfIndex);
                if (pElmList == NULL)
                {
                    return ELM_FAILURE;
                }
                ELM_SLL_INIT (pElmList);
                pPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                pPortEntry->u4DataInstance = DATA_INSTANCE_INTIAL_VALUE;
                pPortEntry->u1StatusCounter =
                    pPortEntry->u1StatusCounterConfigured;
                pPortEntry->u1SendSeqCounter = ELM_INIT_VAL;
                pPortEntry->u1RecvSeqCounter = ELM_INIT_VAL;
                if (ElmStartTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT,
                                   pPortEntry->u1PvtValueConfigured) ==
                    ELM_FAILURE)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "PVT Timer NOT Started ! \n");
                    return ELM_FAILURE;
                }
            }
            ELM_INCR_NO_OF_ELMI_ENABLE ();
        }
        else
        {
            ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC |
                     ELM_ALL_FAILURE_TRC,
                     "ERR:Either Port is down or ELMI is not enabled Globally.\n");
            return ELM_FAILURE;
        }

    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmModuleDisablePerPort                              */
/*                                                                           */
/* Description        : Called by management when ELMI is disabled Per Port  */
/*                                                                           */
/* Input(s)           : Pointer to Port Info structure                       */
/*                      Trigger Type = From CFA or ELMI Module               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmModuleDisablePerPort (UINT4 u4IfIndex, UINT1 u1TriggerType)
{
    tElmPortEntry      *pPortEntry = NULL;

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pPortEntry == NULL)
    {
        return ELM_FAILURE;
    }
    if (u1TriggerType == ELM_EXT_PORT_DOWN)
    {
        pPortEntry->u1PortStatus = ELM_PORT_OPER_DOWN;
        if ((ELM_GET_PORT_MODE (u4IfIndex) == ELM_CUSTOMER_SIDE) &&
            (ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex)))
        {
            pPortEntry->u4DataInstance = ELM_INIT_VAL;
            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PT);
        }
        if ((ELM_GET_PORT_MODE (u4IfIndex) == ELM_NETWORK_SIDE) &&
            (ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex)))
        {
            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT);
            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_ASYNC);

            /* This will free all the packets in this chain */
            while (pPortEntry->pElmFirstPdu != NULL)
            {
                pPortEntry->pElmNextPdu = pPortEntry->pElmFirstPdu->pNextChain;
                ELM_RELEASE_CRU_BUF (pPortEntry->pElmFirstPdu, ELM_FALSE);
                pPortEntry->pElmFirstPdu = pPortEntry->pElmNextPdu;
            }
            pPortEntry->pElmFirstPdu = NULL;
            pPortEntry->pElmNextPdu = NULL;
            pPortEntry->pElmLastPdu = NULL;
        }
    }
    else if (u1TriggerType == ELM_PORT_DOWN)
    {
        if (ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex))
        {
            pPortEntry->u1ElmiPortStatus = ELM_PORT_ELMI_DISABLED;
            if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_CUSTOMER_SIDE)
            {
                pPortEntry->u4DataInstance = ELM_INIT_VAL;
                ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PT);
            }
            if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_NETWORK_SIDE)
            {
                ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT);
                ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_ASYNC);

                /* This will free all the packets in this chain */
                while (pPortEntry->pElmFirstPdu != NULL)
                {
                    pPortEntry->pElmNextPdu =
                        pPortEntry->pElmFirstPdu->pNextChain;
                    ELM_RELEASE_CRU_BUF (pPortEntry->pElmFirstPdu, ELM_FALSE);
                    pPortEntry->pElmFirstPdu = pPortEntry->pElmNextPdu;
                }

                pPortEntry->pElmFirstPdu = NULL;
                pPortEntry->pElmNextPdu = NULL;
                pPortEntry->pElmLastPdu = NULL;
            }
            ELM_DECR_NO_OF_ELMI_ENABLE ();
        }
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleCreatePort                                  */
/*                                                                           */
/* Description        : This function is called from CFA module through L2IWF*/
/*                      interface to indicate the creation of a new port. It */
/*                      passes on this information to the Message processing */
/*                      module by posting a message                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Number of the port that is to  */
/*                                  be created.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ppPortEntry                           */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleCreatePort (UINT4 u4IfIndex)
{
    tElmPortEntry      *pPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfType = CFA_ENET;

    if (!(ELM_IS_ELMI_STARTED () || gElmGlobalInfo.TmrListId != ELM_INIT_VAL))
    {
        return ELM_FAILURE;
    }
    ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                  "SYS: Creating Port: %lu ... \n", u4IfIndex);

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pPortEntry != NULL)
    {
        if (L2IwfIsPortInPortChannel ((UINT2) u4IfIndex) != L2IWF_SUCCESS)
        {
            ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                          "SYS: Port %d: CreatePort: Port Already Created\n",
                          u4IfIndex);
            return ELM_SUCCESS;
        }
        else
        {
            ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                          "SYS: Port %d: Cannot Create Port: Port is member of Port Channel\n",
                          u4IfIndex);
            return ELM_FAILURE;
        }
    }

    /* Check whether the interface type is valid. Currently 
     * ethernet or PPP or ATMVC are valid interfaces.
     */
    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                      "SYS: Cannot Get CFA Information for Port %lu\n",
                      u4IfIndex);
        return ELM_FAILURE;
    }

    u1IfType = CfaIfInfo.u1IfType;

    if ((u1IfType != CFA_ENET) && (u1IfType != CFA_LAGG))
    {
        return ELM_SUCCESS;
    }

    if (ELM_ALLOC_PORT_INFO_MEM_BLOCK (pPortEntry) == NULL)
    {

        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_INIT_SHUT_TRC |
                      ELM_ALL_FAILURE_TRC,
                      "SYS: Port %lu: Port Info Memory Block Allocation failed !!!\n",
                      u4IfIndex);
        return ELM_FAILURE;
    }

    ELM_MEMSET (pPortEntry, ELM_INIT_VAL, sizeof (tElmPortEntry));
    gElmGlobalInfo.ppPortEntry[u4IfIndex - 1] = pPortEntry;

    pPortEntry->u4IfIndex = u4IfIndex;

    pPortEntry->u1PortStatus = CfaIfInfo.u1IfOperStatus;

    ElmInitPortInfo (pPortEntry);

    ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                  "SYS: Port %d: Created Successfully...\n", u4IfIndex);

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleDeletePort                                        */
/*                                                                           */
/* Description        : Deletes the port information and releases the memory */
/*                      occupied by it.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number of the port that is to be    */
/*                                  deleted.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo                                       */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleDeletePort (UINT4 u4IfIndex)
{
    tElmPortEntry      *pPortEntry = NULL;

    if (!(ELM_IS_ELMI_STARTED () || gElmGlobalInfo.TmrListId != ELM_INIT_VAL))
    {
        return ELM_FAILURE;
    }
    ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                  "SYS: Deleting Port %d...\n", u4IfIndex);

    if (ELM_IS_PORT_VALID (u4IfIndex) == ELM_FALSE)
    {
        /* Port number exceeds the allowed range */
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                 "SYS: Delete Port for invalid port number !!!\n");
        return ELM_FAILURE;
    }

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pPortEntry == NULL)
    {
        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: DeletePort: Port is not existing to delete !!!\n");
        return ELM_FAILURE;
    }

    if (ELM_IS_ELMI_ENABLED_ON_PORT (u4IfIndex))
    {
        if (ELM_GET_PORT_MODE (u4IfIndex) == ELM_NETWORK_SIDE)
        {

            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_ASYNC);
            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PVT);
        }
        else
        {
            ElmStopTimer ((VOID *) pPortEntry, ELM_TMR_TYPE_PT);
        }
    }

    u4IfIndex = ELM_IFENTRY_IFINDEX (pPortEntry);

    if (L2IwfIsPortInPortChannel ((UINT2) u4IfIndex) != L2IWF_SUCCESS)
    {
        ELM_RELEASE_PORT_INFO_MEM_BLOCK (pPortEntry);
        ELM_SET_PORTENTRY (u4IfIndex, NULL);
    }
    else
    {
        pPortEntry->bLaPort = ELM_TRUE;
    }

    ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                  "SYS: Port %lu: Deleted Successfully\n", u4IfIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmFlushLcmDataBase                                  */
/*                                                                           */
/* Description        : This routine cleans up the database of LCM           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmFlushLcmDataBase (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tLcmEvcInfo         LcmEvcInfo;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_MGMT_TRC, "DEL:No Port Information present\n");
        return;
    }
    ELM_MEMSET (&LcmEvcInfo, (UINT1) ELM_INIT_VAL, sizeof (tLcmEvcInfo));

    while (LcmGetNextEvcInfo (u4IfIndex, (UINT1) ELM_INIT_VAL,
                              &LcmEvcInfo) != LCM_FAILURE)
    {
        if (LcmEvcInfo.LcmEvcStatusInfo.u2EvcReferenceId == ELM_INIT_VAL)
        {
            break;
        }

        ELM_TRC (ELM_MGMT_TRC, "DEL:Evc is deleted from UNI-C  \n");
        /* Delete the evc From the LCM data base 
         */
        LcmDeleteEvcIndication (u4IfIndex,
                                LcmEvcInfo.LcmEvcStatusInfo.u2EvcReferenceId);
        ELM_MEMSET (&LcmEvcInfo, (UINT1) ELM_INIT_VAL, sizeof (tLcmEvcInfo));
    }
    pElmPortEntry->u2NoOfEvcConfigured = ELM_INIT_VAL;
    return;
}
