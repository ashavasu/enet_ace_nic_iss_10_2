/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elmsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ELMISZ_C
#include "elminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
ElmiSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ELMI_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsELMISizingParams[i4SizingId].u4StructSize,
                              FsELMISizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(ELMIMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            ElmiSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
ElmiSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsELMISizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ELMIMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
ElmiSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ELMI_MAX_SIZING_ID; i4SizingId++)
    {
        if (ELMIMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ELMIMemPoolIds[i4SizingId]);
            ELMIMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
