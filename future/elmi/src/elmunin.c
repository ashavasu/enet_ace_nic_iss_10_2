/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmunin.c,v 1.22 2014/03/18 11:58:38 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              functionality specific to ELMI-UNI-N module
 *
 *******************************************************************/

#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmRcvdCheckEnq                                      */
/*                                                                           */
/* Description        : This function handles the received E-LMI-PDU when the*/
/*                      received report type is E-LMI Check Status Enquiry.  */
/*                      This routine helps in exchange and update of sequence*/
/*                      numbers.                                             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port at which the PDU is receieved        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdCheckEnq (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    ELM_TRC_FN_ENTRY ();

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return ELM_FAILURE;
    }
    /* The PVT timer is stopped, if enabled, on reception of Enquiry Message */
    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT);
    }

    ELM_INCR_RX_ELMI_CHECK_ENQUIRY (u4IfIndex);
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    /* Determine the operational status of ELMI */
    ElmMsgRcvdOperStatusCalc (pElmPortEntry);

    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        /* Synchup the information with standby on reception of an enquiry */
        ElmRedSyncUpPerPortInfo (u4IfIndex);
    }
    /* Prepare and send the ELMI Status Message */
    if (ElmPortTxElmiCheckStatusMessage (pElmPortEntry) != ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }
    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        if (ElmStartTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                           pElmPortEntry->u1PvtValueConfigured) == ELM_FAILURE)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "PVT Timer NOT Started ! \n");
            return ELM_FAILURE;
        }
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdFullStatusEnq                                 */
/*                                                                           */
/* Description        : This function handles the received ELMI PDU when the */
/*                      received report type is Full Status Enquiry          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdFullStatusEnq (UINT4 u4IfIndex)
{
    tElmBufChainHeader *pElmPdu = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               u1ReportType = ELM_INIT_VAL;
    UINT1               u1PktTxed = ELM_INIT_VAL;
    UINT2               u2DataLength = ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "No Port Information present!\n");
        return ELM_FAILURE;
    }

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRcvdFullStatusEnq\n");
    /* The PVT timer is stopped, if enabled, on reception of Enquiry Message */
    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT);
    }

    ELM_INCR_RX_ELMI_FULL_STATUS_ENQUIRY (u4IfIndex);
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    /* Determine the operational status of ELMI */
    ElmMsgRcvdOperStatusCalc (pElmPortEntry);

    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        /* Synchup the information with standby on reception of an enquiry */
        ElmRedSyncUpPerPortInfo (u4IfIndex);
    }
    if (pElmPortEntry->pElmFirstPdu != NULL)
    {
        ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);

        if (pElmPortEntry->pElmNextPdu != NULL)
        {
            if (ELM_COPY_OVER_CRU_BUF
                (pElmPortEntry->pElmNextPdu, &(pElmPortEntry->u1SendSeqCounter),
                 21, 1) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                         "ELMI PDU Copy Over CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }

            if (ELM_COPY_OVER_CRU_BUF
                (pElmPortEntry->pElmNextPdu, &(pElmPortEntry->u1RecvSeqCounter),
                 22, 1) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                         "ELMI PDU Copy Over CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }
            if (ELM_COPY_FROM_CRU_BUF
                (pElmPortEntry->pElmNextPdu, (UINT1 *) &u1ReportType, 16,
                 1) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: Copy From Received Pdu CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }

            pElmPdu = ELM_DUPLICATE_CRU_BUF (pElmPortEntry->pElmNextPdu);

            if (pElmPdu == NULL)
            {
                ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: Duplicate CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }
            u2DataLength = CRU_BUF_Get_ChainValidByteCount (pElmPdu);
            ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u2DataLength);

            pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmNextPdu->pNextChain;

            if (pElmPortEntry->pElmNextPdu == NULL)
            {
                pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmFirstPdu;
            }

            /*u1ReportType = (UINT1) ELM_NTOHS (u1ReportType); */

            if (u1ReportType == ELM_FULL_STATUS_CONTINUED)
            {
                pElmPortEntry->bFullStatusInProgress = ELM_TRUE;
                ELM_INCR_TX_FULL_STATUS_CONTD (pElmPortEntry->u4IfIndex);
            }
            else if (u1ReportType == ELM_FULL_STATUS)
            {
                pElmPortEntry->bFullStatusInProgress = ELM_FALSE;
                ELM_INCR_TX_FULL_STATUS (pElmPortEntry->u4IfIndex);
            }
        }
        else
        {
            pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmFirstPdu;
        }
    }
    else
    {
        /* Prepare and send the Status Message */
        if (ElmPortTxStatusMessage
            (pElmPortEntry, ELM_RXED_ENQUIRY_PDU, &u1PktTxed) != ELM_SUCCESS)
        {
            return ELM_FAILURE;
        }
    }

    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        if (ElmStartTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                           pElmPortEntry->u1PvtValueConfigured) == ELM_FAILURE)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "PVT Timer NOT Started ! \n");
            return ELM_FAILURE;
        }
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdFullStatusContdEnq                            */
/*                                                                           */
/* Description        : This function handles the received ELMI PDU when the */
/*                      received report type is Full Status Contd Enquiry    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdFullStatusContdEnq (UINT4 u4IfIndex)
{
    tElmBufChainHeader *pElmPdu = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT2               u2ReportType = ELM_INIT_VAL;
    UINT2               u2DataLength = ELM_INIT_VAL;
    UINT1               u1PktTxed = ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "No Port Information present!\n");
        return ELM_FAILURE;
    }
    /* The PVT timer is stopped, if enabled, on reception of Enquiry Message */
    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT);
    }

    ELM_INCR_RX_ELMI_FULL_STATUS_CONTD_ENQUIRY (u4IfIndex);
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    /* Determine the operational status of ELMI */
    ElmMsgRcvdOperStatusCalc (pElmPortEntry);

    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        /* Synchup the information with standby on reception of an enquiry */
        ElmRedSyncUpPerPortInfo (u4IfIndex);
    }
    if (pElmPortEntry->pElmFirstPdu != NULL)
    {
        ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);

        if (pElmPortEntry->pElmNextPdu != NULL)
        {
            if (ELM_COPY_OVER_CRU_BUF
                (pElmPortEntry->pElmNextPdu, &(pElmPortEntry->u1SendSeqCounter),
                 21, 1) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                         "ELMI PDU Copy Over CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }

            if (ELM_COPY_OVER_CRU_BUF
                (pElmPortEntry->pElmNextPdu, &(pElmPortEntry->u1RecvSeqCounter),
                 22, 1) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                         "ELMI PDU Copy Over CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }
            if (ELM_COPY_FROM_CRU_BUF
                (pElmPortEntry->pElmNextPdu, (UINT1 *) &u2ReportType, 16,
                 2) == ELM_CRU_FAILURE)
            {
                ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: Copy From Received Pdu CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }

            pElmPdu = ELM_DUPLICATE_CRU_BUF (pElmPortEntry->pElmNextPdu);

            if (pElmPdu == NULL)
            {
                ELM_TRC (ELM_PDU_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                         "MSG: Duplicate CRU Buffer FAILED!\n");
                return ELM_FAILURE;
            }

            u2DataLength = CRU_BUF_Get_ChainValidByteCount (pElmPdu);
            ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u2DataLength);

            pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmNextPdu->pNextChain;

            if (pElmPortEntry->pElmNextPdu == NULL)
            {
                pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmFirstPdu;
            }

            u2ReportType = (UINT2) ELM_NTOHS (u2ReportType);

            if (u2ReportType == ELM_FULL_STATUS_CONTINUED)
            {
                pElmPortEntry->bFullStatusInProgress = ELM_TRUE;
                ELM_INCR_TX_FULL_STATUS_CONTD (pElmPortEntry->u4IfIndex);
            }
            else if (u2ReportType == ELM_FULL_STATUS)
            {
                pElmPortEntry->bFullStatusInProgress = ELM_FALSE;
                ELM_INCR_TX_FULL_STATUS (pElmPortEntry->u4IfIndex);
            }
        }
        else
        {
            pElmPortEntry->pElmNextPdu = pElmPortEntry->pElmFirstPdu;
        }
    }
    else
    {
        /* Prepare and send the Status Message */
        if (ElmPortTxStatusMessage
            (pElmPortEntry, ELM_RXED_ENQUIRY_PDU, &u1PktTxed) != ELM_SUCCESS)
        {
            return ELM_FAILURE;
        }
    }

    if (ELM_IS_PVT_ENABLED (u4IfIndex) == ELM_TRUE)
    {
        if (ElmStartTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                           pElmPortEntry->u1PvtValueConfigured) == ELM_FAILURE)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "PVT Timer NOT Started ! \n");
            return ELM_FAILURE;
        }
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmPortTxElmiCheckStatusMessage                      */
/*                                                                           */
/* Description        : This function is called  in order to transmit a      */
/*                      ELMI Check Status Message                            */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmPortTxElmiCheckStatusMessage (tElmPortEntry * pElmPortEntry)
{
    UINT4               u2DataLength = ELM_INIT_VAL;
    ELM_TRC_FN_ENTRY ();

    ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);
    if (ElmFormElmiPdu (pElmPortEntry, ELM_PDU_TYPE_STATUS,
                        ELM_ELMI_CHECK, &u2DataLength) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 " ElmFormElmiPdu function returned FAILURE!\n");

        return ELM_FAILURE;
    }
    if (ElmFillHeaderAndTransmit (pElmPortEntry, u2DataLength) == ELM_FAILURE)
    {
        return ELM_FAILURE;
    }
    else
    {
        ELM_INCR_TX_ELMI_CHECK (pElmPortEntry->u4IfIndex);
        return ELM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : ElmPortTxAsyncStatusMessage                          */
/*                                                                           */
/* Description        : This function is called  in order to transmit an     */
/*                      Asynchronous Message                                 */
/*                                                                           */
/* Input(s)           : u2EvcRefId - Reference Id of the EVC whose status has*/
/*                                   changed                                 */
/*                      u1EvcStatus - Changed Status of EVC in question      */
/*                     u4IfIndex    - Port No at which this EVC is configured*/
/*                     u1EvcStatus - Changed Status of EVC in question       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmPortTxAsyncStatusMessage (UINT2 u2EvcRefId, UINT4 u4IfIndex,
                             UINT1 u1EvcStatus)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT4               u2DataLength = ELM_INIT_VAL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT1               u1ElmPaddingLength = ELM_INIT_VAL;
    UINT1               u1Count = ELM_INIT_VAL;
    UINT1               u1Reserved = ELM_INIT_VAL;
    UINT1               u1AsyncTimerValue = ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "Port Entry does not exist for this port \n");
        return ELM_FAILURE;
    }
    ELM_MEMSET (gElmGlobalInfo.gu1ElmPdu, ELM_MEMSET_VAL,
                sizeof (ELM_MAX_PDU_SIZE));
    /* Set the pointer for global info buffer. Leave space for
     * ELMI header to be filled later.
     */
    pu1PduStart = gElmGlobalInfo.gu1ElmPdu;
    pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;
    pu1PduEnd = pu1PduEnd + ELM_HEADER_SIZE;

    /* The Asynchronous message contains only the EVC Status IE.
     * No Sub Information Elements are present in this message
     */
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_PROTOCOL_VERSION);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_PDU_TYPE_STATUS);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_REPORT_TYPE_IE);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_REPORT_TYPE_LENGTH);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_ASYNCHRONOUS_STATUS);
    /* EVC Status IE */
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_EVC_STATUS_IE);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_ASYNCH_EVC_STATUS_LENGTH);
    ELM_PUT_2BYTE (pu1PduEnd, u2EvcRefId);
    ELM_PUT_1BYTE (pu1PduEnd, u1EvcStatus);
    /* The minimum TLV size is 60 bytes. If the current length is lesser, then
     * pad it with 0x00 to reach the minimum size
     */
    u2DataLength = (UINT4) (pu1PduEnd - pu1PduStart);
    if (u2DataLength < ELM_MIN_PDU_SIZE)
    {
        u1ElmPaddingLength = (UINT1) (ELM_MIN_PDU_SIZE - u2DataLength);
        pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;
        pu1PduEnd = pu1PduEnd + u2DataLength;
        for (u1Count = 0; u1Count < u1ElmPaddingLength; u1Count++)
        {
            ELM_PUT_1BYTE (pu1PduEnd, u1Reserved);
        }
        u2DataLength = u2DataLength + u1ElmPaddingLength;
    }
    if (ElmFillHeaderAndTransmit (pElmPortEntry, u2DataLength) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                      "Asynchronous Message Not sent for port = %s",
                      ELM_GET_IFINDEX_STR (u4IfIndex));
        return ELM_FAILURE;
    }
    else
    {
        ELM_INCR_TX_ASYNCHRONOUS_STATUS (u4IfIndex);
        if (pElmPortEntry->u1PvtValueConfigured < ELM_ASYNCH_TIMER_VAL)
        {
            u1AsyncTimerValue = pElmPortEntry->u1PvtValueConfigured;
        }
        else
        {
            u1AsyncTimerValue = ELM_ASYNCH_TIMER_VAL;
        }
        if (ElmStartTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_ASYNC,
                           u1AsyncTimerValue) == ELM_FAILURE)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "Asynchronous Timer NOT Started ! \n");
            return ELM_FAILURE;
        }
        /* DI should not get incremented if Full Status Procedure is going on */
        if (pElmPortEntry->bFullStatusInProgress != ELM_TRUE)
        {
            ELM_INCR_DATA_INSTANCE (u4IfIndex);

            /* Standby does not receive this indication. 
             * Send it through RM if Node is active node and Stanby node is present
             */
            if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                (ELM_IS_STANDBY_UP () != ELM_FALSE))
            {
                ElmRedSyncUpDataInstance (pElmPortEntry);

            }

            ElmPreProcessPackets (u4IfIndex);
        }
        return ELM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : ElmPortTxStatusMessage                               */
/*                                                                           */
/* Description        : This function is called  in order to transmit a      */
/*                      ELMI Full Status or Full Status Contd Message        */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      u1TriggerType - Indicates whether called an          */
/*                      reception of Status Enquiry or called when EVC/UNI   */
/*                      information is changed                               */
/*                      pu1PktTxed - Pointer to the the packet stored        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmPortTxStatusMessage (tElmPortEntry * pElmPortEntry, UINT1 u1TriggerType,
                        UINT1 *pu1PktTxed)
{
    tCfaUniInfo        *pCfaUniInfo = NULL;
    tElmEvcStatusInfo  *pElmEvcStatusInfo = NULL;
    tElmEvcVlanInfo    *pElmEvcVlanInfo = NULL;
    UINT2               u2DataLength = ELM_INIT_VAL;
    UINT2               u2EvcRefId = (UINT2) ELM_INIT_VAL;
    UINT2               u2ElmSpaceLeft = (UINT2) ELM_INIT_VAL;
    UINT2               u2EvcVlanLastRefId = (UINT2) ELM_INIT_VAL;
    UINT2               u2VlanLastRefIdEvc = (UINT2) ELM_INIT_VAL;
    UINT1               u1UniStatusIeLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1EvcStatusIeLength = (UINT1) ELM_INIT_VAL;
    UINT1              *pu1PduEnd = NULL;
    UINT1               u1NoEvcInfo = (UINT1) ELM_INIT_VAL;
    UINT1               u1Var = (UINT1) ELM_INIT_VAL;

    ELM_MEMSET (gElmGlobalInfo.gu1ElmPdu, ELM_MEMSET_VAL,
                sizeof (ELM_MAX_PDU_SIZE));

    if (ELM_ALLOC_CFA_UNI_INFO (pCfaUniInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "No Memory Available\n");
        return ELM_FAILURE;
    }

    ELM_MEMSET (pCfaUniInfo, ELM_MEMSET_VAL, sizeof (tCfaUniInfo));

    if (u1TriggerType == ELM_RXED_ENQUIRY_PDU)
    {
        /* The send sequence counter should only be incremented if the packet is
         * is sent as a result of enquiry received from UNI-C. 
         * If the packet is getting stored or if the packet is sent from Active node
         * to standby node, it should not be incremented.
         */
        ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);
    }
    pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;

    /* Get the UNI info from CFA and calculate the total size it will take */
    if (ElmGetUniInfo (pElmPortEntry->u4IfIndex, pCfaUniInfo) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                      "UNI information not fetched for port = %s",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));

        ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
        return ELM_FAILURE;
    }
    u1UniStatusIeLength = (UINT1) (STRLEN (pCfaUniInfo->au1UniName) +
                                   ELM_UNI_FIXED_IE_LENGTH);
    u2DataLength = ELM_HEADER_SIZE + ELM_FIXED_TLV_SIZE;
    /* Fetch the EVC Info and CE VLAN ID/EVC Map info from VCM and
     * put it in the Global array. This is done to deduce if UNI-N
     * will send a Full Status or Full Status Continued message
     */
    if (ELM_ALLOC_EVC_STATUS_INFO (pElmEvcStatusInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "No Memory Available\n");
        ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
        return ELM_FAILURE;
    }

    ELM_MEMSET (pElmEvcStatusInfo, ELM_MEMSET_VAL,
                (sizeof (tElmEvcStatusInfo)));

    if (ELM_ALLOC_EVC_VLAN_INFO (pElmEvcVlanInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "pElmEvcVlanInfo:Memory Allocation failed\n");
        ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
        ELM_RELEASE_EVC_STATUS_INFO (pElmEvcStatusInfo);
        return ELM_FAILURE;
    }

    ELM_MEMSET (pElmEvcVlanInfo, ELM_MEMSET_VAL, (sizeof (tElmEvcVlanInfo)));

    ElmGetNextEvcStatusInfo (pElmPortEntry->u4IfIndex,
                             pElmPortEntry->u2LastEvcRefIdSent,
                             pElmEvcStatusInfo->aLcmEvcStatusInfo,
                             &u1NoEvcInfo);
    if (u1NoEvcInfo == ELM_INIT_VAL)
    {
        ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                      "No EVC provisioned on port = %s\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        /* It may happen that all the EVC info has already been sent in the 
         * earlier messages. We need to check if there are some vlan info which has
         * not yet been sent.
         */
        pElmPortEntry->u1EvcStatusRemaining = (UINT1) ELM_FALSE;
        u1NoEvcInfo = ELM_INIT_VAL;
        u2EvcRefId = pElmPortEntry->u2LastEvcRefIdForVlan;

        ElmGetNextEvcCeVlanInfo (pElmPortEntry->u4IfIndex, u2EvcRefId,
                                 pElmEvcVlanInfo->aLcmEvcCeVlanInfo,
                                 &u1NoEvcInfo);
        if (u1NoEvcInfo == ELM_INIT_VAL)
        {
            pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_FALSE;
        }

        while ((u1NoEvcInfo != (UINT1) ELM_INIT_VAL) &&
               (u2DataLength < (UINT2) ELM_MAX_PDU_SIZE))
        {
            for (u1Var = ELM_INIT_VAL;
                 ((u1Var < ELM_MAX_EVC_IN_SINGLE_REQUEST) &&
                  (u1Var < u1NoEvcInfo) &&
                  (u2DataLength < (UINT2) ELM_MAX_PDU_SIZE)); u1Var++)
            {
                /* if ((UINT2) ELM_MAX_PDU_SIZE > u2DataLength) */
                u2ElmSpaceLeft = (UINT2) ELM_MAX_PDU_SIZE - u2DataLength;
                /*  else
                   {
                   u2ElmSpaceLeft = (UINT2) ELM_INIT_VAL;
                   } */
                if (ELM_CE_SINGLE_VLAN_INFO_LENGTH > u2ElmSpaceLeft)
                {
                    /* There is no space left for even a single CE VLAN info to be sent
                     * We will send the Full Status Continued message from here and will fetch the 
                     * information next time
                     */
                    pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_TRUE;
                    u2VlanLastRefIdEvc =
                        pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                        u2EvcReferenceId;
                    break;
                }
                if (pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                    u2NoOfVlansMapped != (UINT2) ELM_INIT_VAL)
                {
                    ElmFillCeVlanMapIe (&pElmEvcVlanInfo->
                                        aLcmEvcCeVlanInfo[u1Var], pElmPortEntry,
                                        &pu1PduEnd, &u2DataLength);
                }
                u2VlanLastRefIdEvc =
                    pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].u2EvcReferenceId;
                if (pElmPortEntry->u1CeVlanInfoRemaining == (UINT1) ELM_FALSE)
                {
                    pElmPortEntry->u2LastEvcRefIdForVlan =
                        pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                        u2EvcReferenceId;
                }
                else
                {
                    /* No more CE VLAN Data can come in this packet */
                    break;
                }
            }
            if (u1Var < u1NoEvcInfo)
            {
                /* Some Vlans fetched earlier were not filled. No need to fetch more */
                break;
            }

            if (pElmPortEntry->u2LastEvcRefIdSent == u2VlanLastRefIdEvc)
            {
                pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_FALSE;
                break;
            }
            ELM_MEMSET (pElmEvcVlanInfo, ELM_MEMSET_VAL,
                        (sizeof (tElmEvcVlanInfo)));

            /* Check If Vlans are remaing in the LCM or Not */
            ElmGetNextEvcCeVlanInfo (pElmPortEntry->u4IfIndex,
                                     u2VlanLastRefIdEvc,
                                     pElmEvcVlanInfo->aLcmEvcCeVlanInfo,
                                     &u1NoEvcInfo);
        }
    }
    else
    {
        /* If a particular EVC has information which cant be accomodated in
         * the current message, the information for that EVC will be fetched 
         * again and send in the next message
         */
        while ((u1NoEvcInfo != (UINT1) ELM_INIT_VAL) &&
               (u2DataLength < (UINT2) ELM_MAX_PDU_SIZE))
        {
            for (u1Var = 0;
                 ((u1Var < ELM_MAX_EVC_IN_SINGLE_REQUEST) &&
                  (u1Var < u1NoEvcInfo) &&
                  (u2DataLength <= (UINT2) ELM_MAX_PDU_SIZE)); u1Var++)
            {
                ElmGetEvcStatusLength ((tLcmEvcStatusInfo *) &
                                       pElmEvcStatusInfo->
                                       aLcmEvcStatusInfo[u1Var],
                                       &u1EvcStatusIeLength);
                if ((UINT2) ELM_MAX_PDU_SIZE > u2DataLength)
                {
                    u2ElmSpaceLeft = (UINT2) ELM_MAX_PDU_SIZE - u2DataLength;
                }
                else
                {
                    u2ElmSpaceLeft = (UINT2) ELM_INIT_VAL;
                }
                if (u1EvcStatusIeLength > u2ElmSpaceLeft)
                {
                    /* There is no further space left for more EVC info to be accomodated
                     * Set the u1EvcStatusRemaining to true so that Full Status Continued can 
                     * be sent and the next time the EVC status info is fetched
                     */
                    pElmPortEntry->u1EvcStatusRemaining = (UINT1) ELM_TRUE;
                    break;
                }
                pElmPortEntry->u1EvcStatusRemaining = (UINT1) ELM_FALSE;
                ElmFillEvcStatusIe ((tLcmEvcStatusInfo *) & pElmEvcStatusInfo->
                                    aLcmEvcStatusInfo[u1Var], &pu1PduEnd,
                                    &u2DataLength);
                pElmPortEntry->u2LastEvcRefIdSent =
                    pElmEvcStatusInfo->aLcmEvcStatusInfo[u1Var].
                    u2EvcReferenceId;
            }
            if (u1Var < u1NoEvcInfo)
            {
                /* Some EVCs fetched earlier were not filled. No need to fetch more */
                break;
            }
            ElmGetNextEvcStatusInfo (pElmPortEntry->u4IfIndex,
                                     pElmPortEntry->u2LastEvcRefIdSent,
                                     pElmEvcStatusInfo->aLcmEvcStatusInfo,
                                     &u1NoEvcInfo);
        }

        /* The message has been filled with EVC status IE.
         * If the space permits, fill the CE VLAN ID/EVC Map IE.
         */
        if ((u2DataLength < ELM_MAX_PDU_SIZE) &&
            (pElmPortEntry->u1EvcStatusRemaining != (UINT1) ELM_TRUE))
        {
            u1NoEvcInfo = ELM_INIT_VAL;
            u2EvcRefId = pElmPortEntry->u2LastEvcRefIdForVlan;
            ElmGetNextEvcCeVlanInfo (pElmPortEntry->u4IfIndex, u2EvcRefId,
                                     pElmEvcVlanInfo->aLcmEvcCeVlanInfo,
                                     &u1NoEvcInfo);
            if (u1NoEvcInfo == ELM_INIT_VAL)
            {
                pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_FALSE;
            }
            while ((u1NoEvcInfo != (UINT1) ELM_INIT_VAL) &&
                   (u2DataLength < (UINT2) ELM_MAX_PDU_SIZE))
            {
                for (u1Var = 0;
                     ((u1Var < ELM_MAX_EVC_IN_SINGLE_REQUEST)
                      && u1Var < u1NoEvcInfo)
                     && (u2DataLength < ELM_MAX_PDU_SIZE); u1Var++)
                {
                    /* if (ELM_MAX_PDU_SIZE > u2DataLength) */
                    u2ElmSpaceLeft = (UINT2) ELM_MAX_PDU_SIZE - u2DataLength;
                    /*  else
                       {
                       u2ElmSpaceLeft = (UINT2) ELM_INIT_VAL;
                       } */
                    if (ELM_CE_SINGLE_VLAN_INFO_LENGTH > u2ElmSpaceLeft)
                    {
                        /* There is no space left for even a single CE VLAN info to be sent
                         * We will send the Full Status Continued message from here and will fetch the 
                         * information next time
                         */
                        pElmPortEntry->u1PktCeVlanInfoRemaining =
                            (UINT1) ELM_TRUE;
                        u2EvcVlanLastRefId =
                            pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                            u2EvcReferenceId;
                        break;
                    }
                    if (pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                        u2NoOfVlansMapped != (UINT2) ELM_INIT_VAL)
                    {
                        ElmFillCeVlanMapIe (&pElmEvcVlanInfo->
                                            aLcmEvcCeVlanInfo[u1Var],
                                            pElmPortEntry, &pu1PduEnd,
                                            &u2DataLength);
                    }
                    u2EvcVlanLastRefId =
                        pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                        u2EvcReferenceId;
                    if (pElmPortEntry->u1CeVlanInfoRemaining ==
                        (UINT1) ELM_FALSE)
                    {
                        pElmPortEntry->u2LastEvcRefIdForVlan =
                            pElmEvcVlanInfo->aLcmEvcCeVlanInfo[u1Var].
                            u2EvcReferenceId;
                    }
                    else
                    {
                        /* No more CE VLAN Data can come in this packet */
                        break;
                    }
                }
                if (u1Var < u1NoEvcInfo)
                {
                    /* Some Vlans fetched earlier were not filled. No need to fetch more */
                    break;
                }
                if (pElmPortEntry->u2LastEvcRefIdSent == u2EvcVlanLastRefId)
                {
                    pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_FALSE;
                    break;
                }

                ELM_MEMSET (pElmEvcVlanInfo, ELM_MEMSET_VAL,
                            (sizeof (tElmEvcVlanInfo)));
                /* Check If Vlans are remaing in the LCM or Not */
                ElmGetNextEvcCeVlanInfo (pElmPortEntry->u4IfIndex,
                                         u2EvcVlanLastRefId,
                                         pElmEvcVlanInfo->aLcmEvcCeVlanInfo,
                                         &u1NoEvcInfo);
            }
        }
    }
    /* The Full Status Continued message will be sent in any of the following cases:
     * 1. There is some EVC Status info which we were not able to fit in.
     * 2. There is some CE VLAN info which we were not able to fit in.
     * 3. There is not enough space left for UNI status IE to be fit in the message.
     * If none of these cases is true, a Full Status message will be send.
     */
    if ((pElmPortEntry->u1EvcStatusRemaining == (UINT1) ELM_TRUE) ||
        (pElmPortEntry->u1PktCeVlanInfoRemaining == (UINT1) ELM_TRUE) ||
        ((u2DataLength + u1UniStatusIeLength) > ELM_MAX_PDU_SIZE))
    {
        if (ElmSendFullStatusContd (pElmPortEntry, u2DataLength, u1TriggerType)
            != ELM_SUCCESS)
        {
            if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                         "Unable to Format Full Status Continued message\n");
            }
            else
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                         "Unable to Send Full Status Continued message\n");
            }
            ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
            ELM_RELEASE_EVC_STATUS_INFO (pElmEvcStatusInfo);
            ELM_RELEASE_EVC_VLAN_INFO (pElmEvcVlanInfo);
            return ELM_FAILURE;
        }
        else
        {
            if (u1TriggerType != ELM_RED_SYNCHUP_DATABASE)
            {
                *pu1PktTxed = ELM_FULL_STATUS_CONTINUED;
            }
            ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
            ELM_RELEASE_EVC_STATUS_INFO (pElmEvcStatusInfo);
            ELM_RELEASE_EVC_VLAN_INFO (pElmEvcVlanInfo);
            return ELM_SUCCESS;
        }
    }
    else
    {
        if (ElmSendFullStatus (pElmPortEntry, *pCfaUniInfo,
                               u1UniStatusIeLength, u2DataLength, u1TriggerType)
            != ELM_SUCCESS)
        {
            if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                         "Unable to Format Full Status Continued message\n");
            }
            else
            {
                ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                         "Unable to Send Full Status Continued message\n");
            }
            ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
            ELM_RELEASE_EVC_STATUS_INFO (pElmEvcStatusInfo);
            ELM_RELEASE_EVC_VLAN_INFO (pElmEvcVlanInfo);
            return ELM_FAILURE;
        }
        if (u1TriggerType != ELM_RED_SYNCHUP_DATABASE)
        {
            *pu1PktTxed = ELM_FULL_STATUS;
        }
        ELM_RELEASE_CFA_UNI_INFO (pCfaUniInfo);
        ELM_RELEASE_EVC_STATUS_INFO (pElmEvcStatusInfo);
        ELM_RELEASE_EVC_VLAN_INFO (pElmEvcVlanInfo);
        return ELM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : ElmSendFullStatus                                    */
/*                                                                           */
/* Description        : This function is called  in order to transmit a      */
/*                      ELMI Full Status Message                             */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      CfaUniInfo - CFA UNI Structure                       */
/*                      u1UniStatusIeLength - Length of the UNI IE           */
/*                      u2DataLength - Length of the PDU to be sent          */
/*                      u1TriggerType - Indicates whether called an          */
/*                      reception of Status Enquiry or called when EVC/UNI   */
/*                      information is changed                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmSendFullStatus (tElmPortEntry * pElmPortEntry, tCfaUniInfo CfaUniInfo,
                   UINT1 u1UniStatusIeLength, UINT4 u2DataLength,
                   UINT1 u1TriggerType)
{
    tElmBufChainHeader *pElmPdu = NULL;
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT1              *pu1PduEnd = NULL;
    UINT1               u1Offset = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmPaddingLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1Reserved = (UINT1) ELM_INIT_VAL;
    UINT1               u1Count = (UINT1) ELM_INIT_VAL;

    u1Offset = (UINT1) ELM_HEADER_SIZE + (UINT1) ELM_FIXED_TLV_SIZE
        + u1UniStatusIeLength;

    u2DataLength = u2DataLength + u1UniStatusIeLength;
    /* The minimum TLV size is 60 bytes. If the current length is lesser, then
     * padding with 0x00, will be required to reach the minimum size
     */
    if (u2DataLength < ELM_MIN_PDU_SIZE)
    {
        u1ElmPaddingLength = (UINT1) ((UINT1) ELM_MIN_PDU_SIZE - (UINT1)
                                      u2DataLength);
        u2DataLength = u2DataLength + u1ElmPaddingLength;
    }

    if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
    {
        /* Allocate the memory from CRU buffer */
        pElmPdu = ELM_CALC_ALLOC_PRE_CRU_BUF (u2DataLength);
        if (pElmPdu != NULL)
        {
            pElmPortEntry->u4NoOfCruBytesAllocated =
                pElmPortEntry->u4NoOfCruBytesAllocated + u2DataLength;
            gElmCruAllocatedMemory = gElmCruAllocatedMemory + u2DataLength;
        }
        else
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "Not Enough CRU Buffer left to pre store packets\n");
            return ELM_FAILURE;
        }
    }
    if (u1TriggerType == ELM_RXED_ENQUIRY_PDU)
    {
        /* Allocate the memory from CRU buffer */
        pElmPdu = ELM_ALLOC_CRU_BUF (u2DataLength, ELM_INIT_VAL);
    }
    if (u1TriggerType == ELM_RED_SYNCHUP_DATABASE)
    {
        /* The LCM Database needs to be synched up between active
         * and standby UNI-C nodes. Allocate RM buffer
         */
        u2BufSize =
            u2DataLength - (UINT2) ELM_HEADER_SIZE +
            (UINT2) ELM_RED_HEADER_SIZE;
        if ((pBuf = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Rm alloc failed"));
            return ELM_FAILURE;
        }
        ElmRedFillHeader (pElmPortEntry, &pBuf, u2DataLength);
        ElmRedFillFixedTlv (pElmPortEntry, &pBuf, ELM_FULL_STATUS);
        ElmRedFillUniStatusIe (CfaUniInfo, &pBuf, u1UniStatusIeLength);
        /* Apply padding if required */
        if (u1ElmPaddingLength > ELM_INIT_VAL)
        {
            pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;
            pu1PduEnd =
                pu1PduEnd + (u2DataLength - (u1Offset + u1ElmPaddingLength));
            for (u1Count = (UINT1) ELM_INIT_VAL; u1Count < u1ElmPaddingLength;
                 u1Count++)
            {
                ELM_PUT_1BYTE (pu1PduEnd, u1Reserved);
            }
        }
        u4Offset =
            ELM_RED_LCM_SYNCH_HEADER_SIZE + ELM_FIXED_TLV_SIZE +
            u1UniStatusIeLength;
        ELM_RM_PUT_N_BYTE (pBuf, gElmGlobalInfo.gu1ElmPdu, &u4Offset,
                           (u2DataLength - u1Offset));

        ElmRedSendMsgToRm (pBuf, u2BufSize);
        pElmPortEntry->bElmRedBulkUpdInProgress = ELM_FALSE;
        pElmPortEntry->u1ElmRedSendSeqCounter = ELM_INIT_VAL;
        ElmResetVarsAfterFullStatus (pElmPortEntry);
        return ELM_SUCCESS;
    }

    if (pElmPdu == NULL)
    {
        ELM_INCR_BUFFER_FAILURE_COUNT ();
        ElmBufferFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_BUFFER_TRC,
                 "ELMI Pdu Allocation of CRU Buffer FAILED!\n");
        ElmResetVarsAfterFullStatus (pElmPortEntry);
        return ELM_FAILURE;
    }
    if (ElmFillFixedTlv (pElmPortEntry, pElmPdu, ELM_FULL_STATUS)
        == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Fixed TLVs FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ElmResetVarsAfterFullStatus (pElmPortEntry);

        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    if (ElmFillUniStatusIe (CfaUniInfo, pElmPdu, u1UniStatusIeLength)
        == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ElmResetVarsAfterFullStatus (pElmPortEntry);
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }

    /* Apply padding if required */
    if (u1ElmPaddingLength > ELM_INIT_VAL)
    {
        pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;
        pu1PduEnd =
            pu1PduEnd + (u2DataLength - (u1Offset + u1ElmPaddingLength));
        for (u1Count = (UINT1) ELM_INIT_VAL; u1Count < u1ElmPaddingLength;
             u1Count++)
        {
            ELM_PUT_1BYTE (pu1PduEnd, u1Reserved);
        }
    }
    if (ElmFillHeaderInCru (pElmPortEntry, pElmPdu) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ElmResetVarsAfterFullStatus (pElmPortEntry);
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, gElmGlobalInfo.gu1ElmPdu,
                               u1Offset, (u2DataLength - u1Offset))
        == (INT4) ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        ElmResetVarsAfterFullStatus (pElmPortEntry);
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
    {
        if (pElmPortEntry->pElmLastPdu == NULL)
        {
            pElmPortEntry->pElmFirstPdu = pElmPdu;
            pElmPortEntry->pElmNextPdu = pElmPdu;
            pElmPortEntry->pElmLastPdu = pElmPdu;
        }
        else
        {
            ELM_LINK_CRU_BUFCHAINS (pElmPortEntry->pElmLastPdu, pElmPdu);
            pElmPortEntry->pElmLastPdu = pElmPdu;
        }
        ElmResetVarsAfterFullStatus (pElmPortEntry);
    }
    else if (u1TriggerType == ELM_RXED_ENQUIRY_PDU)
    {
        ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u2DataLength);
        ELM_INCR_TX_FULL_STATUS (pElmPortEntry->u4IfIndex);
        ElmResetVarsAfterFullStatus (pElmPortEntry);
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmSendFullStatusContd                               */
/*                                                                           */
/* Description        : This function is called  in order to transmit a      */
/*                      ELMI Full Status Continued Message                   */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      u2DataLength - Length of the PDU to be sent          */
/*                      u1TriggerType - Indicates whether called an          */
/*                      reception of Status Enquiry or called when EVC/UNI   */
/*                      information is changed                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmSendFullStatusContd (tElmPortEntry * pElmPortEntry,
                        UINT4 u2DataLength, UINT1 u1TriggerType)
{
    tElmBufChainHeader *pElmPdu = NULL;
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT1               u1Offset = (UINT1) ELM_INIT_VAL;

    u1Offset = ELM_HEADER_SIZE + ELM_FIXED_TLV_SIZE;

    if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
    {
        /* Allocate the memory from CRU buffer */
        pElmPdu = ELM_CALC_ALLOC_PRE_CRU_BUF (u2DataLength);
        if (pElmPdu != NULL)
        {
            pElmPortEntry->u4NoOfCruBytesAllocated =
                pElmPortEntry->u4NoOfCruBytesAllocated + u2DataLength;
            gElmCruAllocatedMemory = gElmCruAllocatedMemory + u2DataLength;
        }
        else
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "Not Enough CRU Buffer left to pre store packets\n");
            return ELM_FAILURE;
        }
    }
    if (u1TriggerType == ELM_RXED_ENQUIRY_PDU)
    {
        /* Allocate the memory from CRU buffer */
        pElmPdu = ELM_ALLOC_CRU_BUF (u2DataLength, (UINT2) ELM_INIT_VAL);
    }
    if (u1TriggerType == ELM_RED_SYNCHUP_DATABASE)
    {
        /* The LCM Database needs to be synched up between active
         * and standby UNI-C nodes. Allocate RM buffer
         */
        u2BufSize = u2DataLength - (UINT2) ELM_HEADER_SIZE +
            (UINT2) ELM_RED_HEADER_SIZE;
        if ((pBuf = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
            SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                          "Rm alloc failed"));
            return ELM_FAILURE;
        }
        ElmRedFillHeader (pElmPortEntry, &pBuf, u2DataLength);
        ElmRedFillFixedTlv (pElmPortEntry, &pBuf, ELM_FULL_STATUS_CONTINUED);
        u4Offset = ELM_RED_LCM_SYNCH_HEADER_SIZE + ELM_FIXED_TLV_SIZE;
        ELM_RM_PUT_N_BYTE (pBuf, gElmGlobalInfo.gu1ElmPdu,
                           &u4Offset, (u2DataLength - u1Offset));

        ElmRedSendMsgToRm (pBuf, u2BufSize);
        return ELM_SUCCESS;
    }

    if (pElmPdu == NULL)
    {
        ELM_INCR_BUFFER_FAILURE_COUNT ();
        ElmBufferFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "ELMI Pdu Allocation of CRU Buffer FAILED!\n");
        return ELM_FAILURE;
    }
    if (ElmFillFixedTlv (pElmPortEntry, pElmPdu, ELM_FULL_STATUS_CONTINUED)
        == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Fixed TLVs FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    if (ElmFillHeaderInCru (pElmPortEntry, pElmPdu) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    /* Copy the information, stored in Global Buffer, in CRU BUffer */
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, gElmGlobalInfo.gu1ElmPdu,
                               u1Offset, (u2DataLength - u1Offset))
        == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }

    if (u1TriggerType == ELM_UPDATE_DATABASE_TRIGGER)
    {
        if (pElmPortEntry->pElmLastPdu == NULL)
        {
            pElmPortEntry->pElmFirstPdu = pElmPdu;
            pElmPortEntry->pElmNextPdu = pElmPdu;
            pElmPortEntry->pElmLastPdu = pElmPdu;
        }
        else
        {
            ELM_LINK_CRU_BUFCHAINS (pElmPortEntry->pElmLastPdu, pElmPdu);
            pElmPortEntry->pElmLastPdu = pElmPdu;
        }
    }
    else
    {
        ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u2DataLength);
        ELM_INCR_TX_FULL_STATUS_CONTD (pElmPortEntry->u4IfIndex);
        /* This variable is set true to indicate that Full Status procedure is in
         * progress. This will help in not incrementing the DI till the procidure
         * gets completed
         */
        pElmPortEntry->bFullStatusInProgress = ELM_TRUE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleNetworkTlv                                  */
/*                                                                           */
/* Description        : This function handles the TLVs received at the UNI-N */
/*                      side                                                 */
/*                                                                           */
/* Input(s)           : pElmPdu - Pointer to the received PDU                */
/*                      u4IfIndex - Port number at which PDU was received    */
/*                      u2DataLength - Datalength of the receivd PDU         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleNetworkTlv (UINT1 *pu1ElmPdu, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT4               u4DataInstanceValue = (UINT4) ELM_INIT_VAL;
    UINT2               u2OrigDataLength = (UINT2) ELM_INIT_VAL;
    UINT1               u1TlvId = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReportTypeValue = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReportTypeLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1SendSequenceNumber = (UINT1) ELM_INIT_VAL;
    UINT1               u1NoOfTlv = (UINT1) ELM_INIT_VAL;
    UINT1               u1LastReceivedTlvId = (UINT1) ELM_INIT_VAL;
    UINT1               u1SequenceNumLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReceiveSequenceNumber = (UINT1) ELM_INIT_VAL;
    UINT1               u1ErrVal = 0;
    UINT1               u1ErrorVal = (UINT1) ELM_SUCCESS;
    UINT1               u1Paddingreceived = (UINT1) ELM_FALSE;
    UINT1               u1UnExpTlvLen = ELM_INIT_VAL;
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR: No port Information available\n");
        return ELM_FAILURE;
    }

    /* The PDU should be parsed only for the TLVs that are expected in 
     * an Enquiry message: Report Type IE, Sequence Numbers IE, Data Instance IE 
     * The parsing of TLVs is stopped on any of the 3 conditions:
     * 1. u1ErrorVal == ELM_FAILURE, this indicates that the message should be 
     * discarded.   
     * 2. u1Paddingreceived == ELM_TRUE, this indicates that the padding bytes
     * are getting parsed.
     * 3. u2DataLength <= 0, this indicates that the complete PDU has been parsed
     * succesfully
     */
    u2OrigDataLength = u2DataLength;
    while ((u1ErrorVal != ELM_FAILURE) &&
           (u1Paddingreceived != (UINT1) ELM_TRUE) && (u2DataLength > 0))
    {
        ELM_GET_1BYTE (u1TlvId, pu1ElmPdu);

        u1ErrVal =
            (UINT1) ELM_DECR_DATA_LENGTH (u2DataLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE);
        UNUSED_PARAM (u1ErrVal);

        /*  {
           ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
           ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
           ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
           ELM_TRC (ELM_ALL_FAILURE_TRC,
           "ERR:ELMI PDU receieved with invalid length\n");
           return ELM_FAILURE;
           } */
        switch (u1TlvId)
        {
            case ELM_REPORT_TYPE_IE:
            {
                /* Report Type should be the first TLV received */
                if (u1LastReceivedTlvId == ELM_INIT_VAL)
                {

                    ELM_GET_1BYTE (u1ReportTypeLength, pu1ElmPdu);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved with invalid length\n");
                        return ELM_FAILURE;
                    }
                    if (u1ReportTypeLength != ELM_REPORT_TYPE_LENGTH)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC |
                                      ELM_ALL_FAILURE_TRC,
                                      "Invalid Length(%d) of Report Type"
                                      "received\n", u1ReportTypeLength);
                        u1ErrorVal = ELM_FAILURE;
                        break;
                    }
                    ELM_GET_1BYTE (u1ReportTypeValue, pu1ElmPdu);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV\n");
                        return ELM_FAILURE;
                    }
                    if ((u1ReportTypeValue == ELM_FULL_STATUS) ||
                        (u1ReportTypeValue == ELM_ELMI_CHECK) ||
                        (u1ReportTypeValue == ELM_FULL_STATUS_CONTINUED))
                    {
                        ELM_INCR_NO_OF_TLV_PARSED (u1NoOfTlv);
                        u1LastReceivedTlvId = ELM_REPORT_TYPE_IE;
                    }
                    else
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                 "SYS: ELMI receieved an invalid report type!!!\n");
                        u1ErrorVal = ELM_FAILURE;
                    }
                }
                else
                {
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "Duplicate Report Type IE received. Ignoring the IE\n");
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) (ELM_REPORT_TYPE_LENGTH +
                                  ELM_DECR_DATALEN_ONE)) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV\n");
                        return ELM_FAILURE;
                    }
                    pu1ElmPdu = pu1ElmPdu + ELM_REPORT_TYPE_LENGTH + 1;
                }
                break;
            }
            case ELM_SEQUENCE_NUMBER_IE:
            {
                /* Sequence Number IE should always come after
                 * Report Type IE
                 */
                if (u1LastReceivedTlvId == ELM_REPORT_TYPE_IE)
                {
                    ELM_GET_1BYTE (u1SequenceNumLength, pu1ElmPdu);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved with invalid length\n");
                        return ELM_FAILURE;
                    }

                    if (u1SequenceNumLength != ELM_SEQUENCE_NUMBER_LENGTH)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_INCR_INVALID_SEQUENCE_NUMBER (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                 "Wrong length of Sequence Numbers received!!\n");
                        u1ErrorVal = ELM_FAILURE;
                        break;
                    }
                    ELM_GET_1BYTE (u1SendSequenceNumber, pu1ElmPdu);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved with invalid length\n");
                        return ELM_FAILURE;
                    }
                    ELM_GET_1BYTE (u1ReceiveSequenceNumber, pu1ElmPdu);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved with invalid length\n");
                        return ELM_FAILURE;
                    }
                    if (u1ReceiveSequenceNumber !=
                        pElmPortEntry->u1SendSeqCounter)
                    {
                        /* The Received Sequence number is wrong. This is
                         * a Reliablity error, but still the PDU should be 
                         * parsed further and UNI-C be replied with the 
                         * requested status
                         */
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_INCR_INVALID_SEQUENCE_NUMBER (u4IfIndex);
                        ELM_TRC_ARG2 (ELM_ALL_FAILURE_TRC,
                                      "Invalid RecvSeqNo(%d,Stored=%d) received\n",
                                      u1ReceiveSequenceNumber,
                                      pElmPortEntry->u1SendSeqCounter);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        if (ElmStartTimer
                            ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                             pElmPortEntry->u1PvtValueConfigured) ==
                            ELM_FAILURE)
                        {
                            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                     "PVT Timer NOT Started ! \n");
                            return ELM_FAILURE;
                        }

                    }
                    pElmPortEntry->u1RecvSeqCounter = u1SendSequenceNumber;
                    ELM_INCR_NO_OF_TLV_PARSED (u1NoOfTlv);
                    u1LastReceivedTlvId = ELM_SEQUENCE_NUMBER_IE;
                }
                else if (u1LastReceivedTlvId == ELM_INIT_VAL)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    /*Check if next TLV is in decrementing order */
                    if ((u2DataLength >= ELM_DECR_DATALEN_THREE) &&
                        (*(pu1ElmPdu + ELM_INCR_BUFF_THREE) < u1TlvId))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    u1ErrorVal = ELM_FAILURE;
                }
                else
                {
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "Duplicate Sequence No IE received. Ignoring the IE\n");
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) (ELM_SEQUENCE_NUMBER_LENGTH +
                                  ELM_DECR_DATALEN_ONE)) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV\n");
                        return ELM_FAILURE;
                    }
                    pu1ElmPdu = pu1ElmPdu + ELM_SEQUENCE_NUMBER_LENGTH
                        + ELM_DECR_DATALEN_ONE;
                }
                break;
            }
            case ELM_DATA_INSTANCE_IE:
            {
                /* Data Instance IE should always come after
                 * Sequence Number IE
                 */
                if (u1LastReceivedTlvId == ELM_SEQUENCE_NUMBER_IE)
                {
                    if (ElmVerifyDataInstance (pu1ElmPdu, u4IfIndex,
                                               &u4DataInstanceValue)
                        == ELM_FAILURE)
                    {
                        if (ELM_DECR_DATA_LENGTH
                            (u2DataLength,
                             (UINT1) (ELM_DATA_INSTANCE_IE_LENGTH +
                                      ELM_DECR_DATALEN_ONE)) == ELM_FAILURE)
                        {
                            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                            ElmInvalidPduRxdTrap (u4IfIndex,
                                                  ELM_PROTOCOL_ERROR,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                            ELM_TRC (ELM_ALL_FAILURE_TRC,
                                     "ERR:ELMI PDU receieved invalid length in TLV\n");
                            return ELM_FAILURE;
                        }

                        u1ErrorVal = ELM_FAILURE;
                    }
                    else
                    {
                        if (ELM_DECR_DATA_LENGTH
                            (u2DataLength,
                             (UINT1) (ELM_DATA_INSTANCE_IE_LENGTH +
                                      ELM_DECR_DATALEN_ONE)) == ELM_FAILURE)
                        {
                            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                            ElmInvalidPduRxdTrap (u4IfIndex,
                                                  ELM_PROTOCOL_ERROR,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                            ELM_TRC (ELM_ALL_FAILURE_TRC,
                                     "ERR:ELMI PDU receieved invalid length in TLV\n");
                            return ELM_FAILURE;
                        }
                        pu1ElmPdu = pu1ElmPdu + ELM_DATA_INSTANCE_IE_LENGTH
                            + ELM_DECR_DATALEN_ONE;
                        ELM_INCR_NO_OF_TLV_PARSED (u1NoOfTlv);
                    }
                    u1LastReceivedTlvId = ELM_DATA_INSTANCE_IE;
                }
                else if (u1LastReceivedTlvId == ELM_DATA_INSTANCE_IE)
                {
                    /* Received Data Instance IE is duplicate */
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    u1ErrorVal = ELM_FAILURE;
                }
                else
                {
                    /* If the Data Instance IE didnot come after Sequence
                     * Number IE, it means that some Mandatory IE is missing
                     */
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    /*Check if next TLV is in decrementing order */
                    if ((u2DataLength >= ELM_DECR_DATALEN_SIX) &&
                        ((*(pu1ElmPdu + ELM_DECR_DATALEN_SIX) ==
                          ELM_REPORT_TYPE_IE) ||
                         (*(pu1ElmPdu + ELM_DECR_DATALEN_SIX) ==
                          ELM_SEQUENCE_NUMBER_IE)) &&
                        (*(pu1ElmPdu + ELM_DECR_DATALEN_SIX) < u1TlvId))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    u1ErrorVal = ELM_FAILURE;
                }
                break;
            }
            case ELM_INIT_VAL:
            {
                /* Check if last received TLV is not DATA_INSTANCE_TLV, then
                 * increment MANDATORY IE Missing counter */
                if (u1LastReceivedTlvId != ELM_DATA_INSTANCE_IE)
                {
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                }
                if ((u2OrigDataLength == ELM_MIN_LENGTH_ENQUIRY_PDU)
                    && (u1NoOfTlv == ELM_MAX_TLV_IN_ENQUIRY_MSG))
                {
                    u1Paddingreceived = (UINT1) ELM_TRUE;
                    /* Padding is received */
                    break;
                }
                else
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                                  "Invalid TLV ID=%d received\n", u1TlvId);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    u1ErrorVal = ELM_FAILURE;
                }
                break;
            }
            default:
                /* Check if last received TLV is not DATA_INSTANCE_TLV, then
                 * increment MANDATORY IE Missing counter */
                if (u1LastReceivedTlvId != ELM_DATA_INSTANCE_IE)
                {
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                }
                ELM_GET_1BYTE (u1UnExpTlvLen, pu1ElmPdu);
                ELM_INCR_PROT_ERR_UNRECOGNISED_IE_RX_COUNT (u4IfIndex);
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length\n");
                    return ELM_FAILURE;
                }
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ELMI PDU receieved with Unexpected TLV !!!\n");
                pu1ElmPdu = pu1ElmPdu + u1UnExpTlvLen;
                u2DataLength = u2DataLength - u1UnExpTlvLen;
                continue;
        }
    }
    if (u1ErrorVal == ELM_SUCCESS)
    {
        switch (u1ReportTypeValue)
        {
            case ELM_FULL_STATUS:
                if (ElmRcvdFullStatusEnq (u4IfIndex) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "Error in handling Full Status Enquiry\n");
                    return ELM_FAILURE;
                }
                break;
            case ELM_ELMI_CHECK:
                if (ElmRcvdCheckEnq (u4IfIndex) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "Error in handling ELMI Check Enquiry\n");
                    return ELM_FAILURE;
                }
                break;
            case ELM_FULL_STATUS_CONTINUED:
                if (ElmRcvdFullStatusContdEnq (u4IfIndex) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "Error in handling Full Status Contd Enquiry\n");
                    return ELM_FAILURE;
                }
                break;
        }
        return ELM_SUCCESS;
    }
    else
    {
        return ELM_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name      : ElmFillHeaderInCru                                   */
/*                                                                           */
/* Description        : Forms the ethernet header and puts it in CRU buffer  */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                      pElmPdu - Pointer to the allocated buffer            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmFillHeaderInCru (tElmPortEntry * pElmPortEntry, tElmBufChainHeader * pElmPdu)
{
    tMacAddr           *pSrcMacAddr = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1Index = (UINT1) ELM_INIT_VAL;
    UINT1               au1ElmiHeader[ELM_HEADER_SIZE];
    ELM_MEMSET (au1ElmiHeader, ELM_MEMSET_VAL, sizeof (au1ElmiHeader));

    au1ElmiHeader[0] = (UINT1) 0x01;
    au1ElmiHeader[1] = (UINT1) 0x80;
    au1ElmiHeader[2] = (UINT1) 0xC2;
    au1ElmiHeader[3] = (UINT1) 0x00;
    au1ElmiHeader[4] = (UINT1) 0x00;
    au1ElmiHeader[5] = (UINT1) 0x07;
    u1Index = 6;
    if (CfaGetIfInfo (ELM_IFENTRY_IFINDEX (pElmPortEntry), &CfaIfInfo) !=
        CFA_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "CfaGetIfInfo call FAILED!\n");
        return ELM_FAILURE;
    }
    pSrcMacAddr = &CfaIfInfo.au1MacAddr;
    ELM_MEMCPY (&au1ElmiHeader[u1Index], pSrcMacAddr, ELM_MAC_ADDR_SIZE);
    u1Index = u1Index + (UINT1) ELM_MAC_ADDR_SIZE;
    au1ElmiHeader[u1Index] = (UINT1) ELM_IST_BYTE_ETHERNET_HEADER;
    ELM_INCR_INDEX (u1Index);
    au1ElmiHeader[u1Index] = (UINT1) ELM_2ND_BYTE_ETHERNET_HEADER;
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, au1ElmiHeader,
                               0, ELM_HEADER_SIZE) == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmFillEvcStatusIe                                   */
/*                                                                           */
/* Description        : This function is called  to fill the EVC status IE in*/
/*                      global buffer                                        */
/*                                                                           */
/* Input(s)           : pLcmEvcStatusInfo - Pointer to the LCM EVC Status    */
/*                                          Info                             */
/*                      ppu1PduEnd - Pointer to the end of global buffer     */
/*                      pu2DataLength - Pointer to Data length of the filled */
/*                      buffer                                               */
/*                                                                           */
/* Output(s)          : pu2DataLength - Data length of the filled buffer     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmFillEvcStatusIe (tLcmEvcStatusInfo * pLcmEvcStatusInfo, UINT1 **ppu1PduEnd,
                    UINT2 *pu2DataLength)
{
    tBwProfile          BwProfile;
    UINT1               u1EvcStatusIeLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmFillByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1EvcIdLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1BwLoopVar = (UINT1) ELM_INIT_VAL;
    UINT1              *pu1ElmStartPtr = NULL;
    BwProfile = pLcmEvcStatusInfo->BwProfile[0];
    u1EvcIdLength = (UINT1) STRLEN (pLcmEvcStatusInfo->au1EvcIdentifier);
    ElmGetEvcStatusLength (pLcmEvcStatusInfo, &u1EvcStatusIeLength);
    pu1ElmStartPtr = *ppu1PduEnd;
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_EVC_STATUS_IE);
    ELM_PUT_1BYTE (*ppu1PduEnd, u1EvcStatusIeLength);
    ELM_PUT_2BYTE (*ppu1PduEnd, pLcmEvcStatusInfo->u2EvcReferenceId);
    ELM_PUT_1BYTE (*ppu1PduEnd, pLcmEvcStatusInfo->u1EvcStatus);
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_EVC_PARAMS_IE);
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_EVC_PARAMS_SUBINFO_LENGTH);
    ELM_PUT_1BYTE (*ppu1PduEnd, pLcmEvcStatusInfo->u1EvcType);
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_EVC_IDENTIFIER_IE);
    ELM_PUT_1BYTE (*ppu1PduEnd, u1EvcIdLength);
    for (u1ElmFillByte = 0; u1ElmFillByte < u1EvcIdLength; u1ElmFillByte++)
    {
        ELM_PUT_1BYTE (*ppu1PduEnd,
                       pLcmEvcStatusInfo->au1EvcIdentifier[u1ElmFillByte]);
    }
    if (pLcmEvcStatusInfo->u1NoOfCos != (UINT1) ELM_INIT_VAL)
    {
        /* The per cos bit is set. The Bandwidth Profile can repeat 8 times,
         * One for each class of service mentioned by User priority bit set.
         */
        for (u1BwLoopVar = 0; u1BwLoopVar < ELM_MAX_BW_PROFILE_IE;
             u1BwLoopVar++)
        {
            BwProfile = pLcmEvcStatusInfo->BwProfile[u1BwLoopVar];
            if (BwProfile.u1PerCosBit != (UINT1) ELM_INIT_VAL)
            {
                ElmFillBwProfile (&BwProfile, &(*ppu1PduEnd));
            }
        }
    }
    else
    {
        ElmFillBwProfile (&BwProfile, &(*ppu1PduEnd));
    }
    *pu2DataLength = *pu2DataLength + (UINT2) (*ppu1PduEnd - pu1ElmStartPtr);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmFillBwProfile                                     */
/*                                                                           */
/* Description        : This function is called  in order fill the Bandwidth */
/*                      Sub Information Element                              */
/*                                                                           */
/* Input(s)           : pBwProfile - Pointer to the Bw Profile structure     */
/*                      ppu1PduEnd - Pointer to the end of global buffer     */
/*                                                                           */
/* Output(s)          : pu2DataLength - Data length of the filled buffer     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmFillBwProfile (tBwProfile * pBwProfile, UINT1 **ppu1PduEnd)
{

    UINT1               u1PriorityBits = (UINT1) ELM_INIT_VAL;
    UINT1               u1BandwidthThirdByte = (UINT1) ELM_INIT_VAL;

    /* Set bits for Colour Mode, Coupling Flag and PerCos bit as received from
     * VCM
     */
    if (pBwProfile->u1PerCosBit != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        ELM_MASK_FIRST_BIT);
    }
    if (pBwProfile->u1Cf != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        ELM_MASK_SECOND_BIT);
    }
    if (pBwProfile->u1Cm != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        ELM_MASK_THIRD_BIT);
    }
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_BW_PROFILE_IE);
    ELM_PUT_1BYTE (*ppu1PduEnd, ELM_BW_PROFILE_IE_LENGTH);
    ELM_PUT_1BYTE (*ppu1PduEnd, u1BandwidthThirdByte);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1CirMagnitude);
    ELM_PUT_2BYTE (*ppu1PduEnd, pBwProfile->u2CirMultiplier);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1CbsMagnitude);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1CbsMultiplier);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1EirMagnitude);
    ELM_PUT_2BYTE (*ppu1PduEnd, pBwProfile->u2EirMultiplier);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1EbsMagnitude);
    ELM_PUT_1BYTE (*ppu1PduEnd, pBwProfile->u1EbsMultiplier);
    u1PriorityBits = 1;
    u1PriorityBits = u1PriorityBits << pBwProfile->u1PriorityBits;
    ELM_PUT_1BYTE (*ppu1PduEnd, u1PriorityBits);
}

/*****************************************************************************/
/* Function Name      : ElmFillCeVlanMapIe                                   */
/*                                                                           */
/* Description        : This function is called  in order fill the CE VLAN   */
/*                      ID/EVC Map Information Element                       */
/*                                                                           */
/* Input(s)           : *pLcmEvcCeVlanInfo - CE VLAN structure pointer       */
/*                      pu2DataLength - Data length of the filled buffer     */
/*                      ppu1PduEnd - Pointer to the end of global buffer      */
/*                                                                           */
/* Output(s)          : pu2DataLength - Data length of the filled buffer     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmFillCeVlanMapIe (tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo,
                    tElmPortEntry * pElmPortEntry, UINT1 **ppu1PduEnd,
                    UINT2 *pu2DataLength)
{
    UINT2               u2NoOfVlansRemaining = (UINT2) ELM_INIT_VAL;
    UINT2               u2NoOfPossibleVlanInfo = (UINT2) ELM_INIT_VAL;
    UINT2               u2VlanId = (UINT2) ELM_INIT_VAL;
    UINT2               u2TotalVlans = (UINT2) ELM_INIT_VAL;
    UINT2               u2LocalLastVlanSent = (UINT2) ELM_INIT_VAL;
    UINT1               u1CeVlanInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanInfoFifthByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanInfoSixthByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanSeqNo = (UINT1) ELM_INIT_VAL;
    UINT1               u1EvcMapEntryLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1VlanCounter = (UINT1) ELM_INIT_VAL;
    UINT1              *pu1StartAddr = NULL;
    BOOL1               bResult = OSIX_FALSE;

    pu1StartAddr = *ppu1PduEnd;
    /* u2NoOfVlansRemaining: No of Vlans whose information has not yet been sent
     * u2LastVlanIdStored: The Vlan ID whose information was last stored in 
     *                     global buffer
     * u2NoOfPossibleVlanInfo: No of Vlans whose information can be sent in the
     *                         current message. This value is calculated keeping
     *                         the current space left in the message.
     * u2NoOfVlansMapped: Total No of Vlans configured on the EVC
     */
    u2TotalVlans = pLcmEvcCeVlanInfo->u2NoOfVlansMapped;
    if (pElmPortEntry->u1CeVlanInfoRemaining == (UINT1) ELM_TRUE)
    {
        /* This is the case when some vlans for an evc were not sent in the
         * previous message. Now the remaining info should be filled and sent
         */
        u2LocalLastVlanSent = pElmPortEntry->u2LastVlanIdSent;
        u2TotalVlans = u2TotalVlans - pElmPortEntry->u2CeVlanSentInPrevMsg;
        pElmPortEntry->u1CeVlanInfoRemaining = (UINT1) ELM_FALSE;
    }
    else
    {
        u2LocalLastVlanSent = pLcmEvcCeVlanInfo->u2FirstVlanId;
    }

    ElmGetVlanCapacity (*pu2DataLength, &u2NoOfPossibleVlanInfo);
    if (u2NoOfPossibleVlanInfo > u2TotalVlans)
    {
        u2NoOfVlansRemaining = u2TotalVlans;
    }
    else
    {
        u2NoOfVlansRemaining = u2NoOfPossibleVlanInfo;
    }
    if (u2TotalVlans > u2NoOfVlansRemaining)
    {
        pElmPortEntry->u1CeVlanInfoRemaining = (UINT1) ELM_TRUE;
        pElmPortEntry->u2CeVlanSentInPrevMsg =
            pElmPortEntry->u2CeVlanSentInPrevMsg + u2NoOfVlansRemaining;
    }
    else
    {
        pElmPortEntry->u2CeVlanSentInPrevMsg = (UINT2) ELM_INIT_VAL;
    }
    if (pLcmEvcCeVlanInfo->u1UntagPriTag == ELM_TRUE)
    {
        u1CeVlanInfoSixthByte = (UINT1) (u1CeVlanInfoSixthByte |
                                         ELM_MASK_SECOND_BIT);
    }
    if (pLcmEvcCeVlanInfo->u1DefaultEvc == ELM_TRUE)
    {
        u1CeVlanInfoSixthByte = (UINT1) (u1CeVlanInfoSixthByte |
                                         ELM_MASK_FIRST_BIT);
    }
    u1CeVlanSeqNo = (UINT1) (u1CeVlanInfoFifthByte & ELM_MASK_SIX_BITS);
    while (u2NoOfVlansRemaining != (UINT2) ELM_INIT_VAL)
    {
        u1CeVlanInfoFifthByte = ELM_INIT_VAL;
        /* Length of the CE Vlan ID/EVC Map Info cannot exceed 255.
         * Moreover it is dependent on the number of Vlans remaining whose 
         * information is yet to be sent
         */
        if (u2NoOfVlansRemaining > ELM_MAX_VLANS_PER_CE_VLAN_INFO)
        {
            u1EvcMapEntryLength = ELM_MAX_VLANS_PER_CE_VLAN_INFO * 2;
        }
        else
        {
            u1EvcMapEntryLength = (UINT1) ((u2NoOfVlansRemaining) * 2);
        }
        u1CeVlanInfoLength = (UINT1) ELM_CE_VLAN_INFO_FIXED_LENGTH +
            u1EvcMapEntryLength;

        ELM_PUT_1BYTE (*ppu1PduEnd, (UINT1) ELM_CE_VLAN_EVC_MAP_IE);
        ELM_PUT_1BYTE (*ppu1PduEnd, u1CeVlanInfoLength);
        ELM_PUT_2BYTE (*ppu1PduEnd, pLcmEvcCeVlanInfo->u2EvcReferenceId);
        ELM_INCR_CE_VLAN_SEQ_NUMBER (u1CeVlanSeqNo);
        u1CeVlanInfoFifthByte = u1CeVlanInfoFifthByte | u1CeVlanSeqNo;
        /* If all the remaining Vlans can be accomodated in single IE then
         * this is the last IE. Set the Last IE bit to 1 in this case
         */
        if (u2NoOfVlansRemaining <= ELM_MAX_VLANS_PER_CE_VLAN_INFO)
        {
            u1CeVlanInfoFifthByte |= ELM_MASK_SEVENTH_BIT;
        }
        ELM_PUT_1BYTE (*ppu1PduEnd, u1CeVlanInfoFifthByte);
        ELM_PUT_1BYTE (*ppu1PduEnd, u1CeVlanInfoSixthByte);
        ELM_PUT_1BYTE (*ppu1PduEnd, ELM_EVC_MAP_ENTRY_IE);
        ELM_PUT_1BYTE (*ppu1PduEnd, u1EvcMapEntryLength);

        for (u2VlanId = u2LocalLastVlanSent, u1VlanCounter = ELM_INIT_VAL;
             ((u2VlanId <= pLcmEvcCeVlanInfo->u2LastVlanId) &&
              (u2NoOfVlansRemaining != 0)); u2VlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (pLcmEvcCeVlanInfo->CeVlanMapInfo, u2VlanId,
                                     LCM_CE_VLAN_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                ELM_PUT_2BYTE (*ppu1PduEnd, u2VlanId);
                u2NoOfVlansRemaining =
                    u2NoOfVlansRemaining - (UINT2) ELM_DECR_VLAN_COUNTER_ONE;
                u1VlanCounter =
                    u1VlanCounter + (UINT1) ELM_INCR_VLAN_COUNTER_ONE;
            }
            if (u1VlanCounter == (UINT1) ELM_MAX_VLANS_PER_CE_VLAN_INFO)
            {
                /* A maximum of 124 CE Vlans can be put in one IE.
                 * Rest should be put in next CE VLAN MAP IE(s)
                 */
                break;
            }
        }
        pElmPortEntry->u2LastVlanIdSent = u2VlanId;
        u2LocalLastVlanSent = u2VlanId + (UINT2) ELM_INCR_VLAN_COUNTER_ONE;

    }

    if (u2NoOfPossibleVlanInfo >= u2TotalVlans)
    {
        pElmPortEntry->u2LastVlanIdSent = (UINT2) ELM_INIT_VAL;
        pElmPortEntry->u2CeVlanSentInPrevMsg = (UINT2) ELM_INIT_VAL;
    }
    *pu2DataLength = *pu2DataLength + (UINT2) (*ppu1PduEnd - pu1StartAddr);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmFillFixedTlv                                      */
/*                                                                           */
/* Description        : This function is used to fill the following IE in the*/
/*                      message:                                             */
/*                      1) Protocol Version                                  */
/*                      2) Message Type                                      */
/*                      3) Report Type                                       */
/*                      4) Sequence Number                                   */
/*                      5) Data Instance                                     */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      pElmPdu - Pointer to the allocated CRU buffer        */
/*                      u1ReportType - Indicates the report type of ELMI PDU */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmFillFixedTlv (tElmPortEntry * pElmPortEntry, tElmBufChainHeader * pElmPdu,
                 UINT1 u1ReportType)
{

    UINT1               au1FixedLengthTlv[ELM_FIXED_TLV_SIZE];
    ELM_MEMSET (au1FixedLengthTlv, ELM_MEMSET_VAL, sizeof (au1FixedLengthTlv));
    au1FixedLengthTlv[0] = (UINT1) ELM_PROTOCOL_VERSION;
    au1FixedLengthTlv[1] = (UINT1) ELM_PDU_TYPE_STATUS;
    au1FixedLengthTlv[2] = (UINT1) ELM_REPORT_TYPE_IE;
    au1FixedLengthTlv[3] = (UINT1) ELM_REPORT_TYPE_LENGTH;
    au1FixedLengthTlv[4] = u1ReportType;
    au1FixedLengthTlv[5] = (UINT1) ELM_SEQUENCE_NUMBER_IE;
    au1FixedLengthTlv[6] = (UINT1) ELM_SEQUENCE_NUMBER_LENGTH;
    au1FixedLengthTlv[7] = pElmPortEntry->u1SendSeqCounter;
    au1FixedLengthTlv[8] = pElmPortEntry->u1RecvSeqCounter;
    au1FixedLengthTlv[9] = (UINT1) ELM_DATA_INSTANCE_IE;
    au1FixedLengthTlv[10] = (UINT1) ELM_DATA_INSTANCE_IE_LENGTH;
    au1FixedLengthTlv[11] = (UINT1) ELM_INIT_VAL;
    pElmPortEntry->u4DataInstance = ELM_HTONL (pElmPortEntry->u4DataInstance);
    ELM_MEMCPY (&au1FixedLengthTlv[12], &(pElmPortEntry->u4DataInstance),
                ELM_DATA_INSTANCE_LENGTH);
    pElmPortEntry->u4DataInstance = ELM_NTOHL (pElmPortEntry->u4DataInstance);
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, au1FixedLengthTlv,
                               ELM_HEADER_SIZE, ELM_FIXED_TLV_SIZE)
        == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmFillUniStatusIe                                   */
/*                                                                           */
/* Description        : This function is called to fill the UNI Status IE.   */
/*                      After filling the IE, it is copied to CRU Buffer     */
/*                                                                           */
/* Input(s)           : ppElmUniInfo - The pointer to the CFA Port Entry     */
/*                      structure                                            */
/*                      pElmPdu - Pointer to the allocated CRU buffer        */
/*                      u1UniStatusIeLength - Precalculated length of UNI    */
/*                      Status IE                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmFillUniStatusIe (tCfaUniInfo CfaUniInfo, tElmBufChainHeader * pElmPdu,
                    UINT1 u1UniStatusIeLength)
{
    tCfaUniBwProfile    BwProfile;
    UINT1               au1UniInfo[ELM_MAX_UNI_INFO_SIZE + 1];
    UINT1               u1BandwidthThirdByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1UniIdLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1UniFillByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1Offset = (UINT1) ELM_INIT_VAL;
    UINT1               u1Index = (UINT1) ELM_INIT_VAL;

    BwProfile = CfaUniInfo.UniBwProfile;
    u1UniIdLength = u1UniStatusIeLength - (UINT1) ELM_UNI_FIXED_IE_LENGTH;
    u1Offset = ELM_HEADER_SIZE + ELM_FIXED_TLV_SIZE;
    ELM_MEMSET (au1UniInfo, ELM_MEMSET_VAL, sizeof (au1UniInfo));
    if (BwProfile.u1Cf != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        (UINT1) ELM_MASK_SECOND_BIT);
    }
    if (BwProfile.u1Cm != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        (UINT1) ELM_MASK_THIRD_BIT);
    }
    au1UniInfo[0] = (UINT1) ELM_UNI_STATUS_IE;
    au1UniInfo[1] = u1UniStatusIeLength - (UINT1) ELM_PROTO_MSG_TYPE_SIZE;
    au1UniInfo[2] = CfaUniInfo.u1CeVlanMapType;
    au1UniInfo[3] = (UINT1) ELM_UNI_IE;
    au1UniInfo[4] = u1UniIdLength;
    u1Index = 5;
    for (u1UniFillByte = 0; ((u1UniFillByte < CFA_MAX_LENGTH_UNI_NAME) &&
                             (u1UniFillByte < u1UniIdLength)); u1UniFillByte++)
    {
        if (u1Index > ELM_MAX_UNI_INFO_SIZE)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_BUFFER_TRC,
                     "UniInfo Array Index out of bound!\n");
            return ELM_FAILURE;
        }
        au1UniInfo[u1Index] = CfaUniInfo.au1UniName[u1UniFillByte];
        ELM_INCR_INDEX (u1Index);
    }
    au1UniInfo[u1Index] = (UINT1) ELM_BW_PROFILE_IE;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = (UINT1) ELM_BW_PROFILE_IE_LENGTH;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = u1BandwidthThirdByte;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1CirMagnitude;
    ELM_INCR_INDEX (u1Index);
    ELM_MEMCPY (&au1UniInfo[u1Index], &BwProfile.u2CirMultiplier,
                ELM_CIR_MULTIPLIER_LENGTH);
    ELM_INCR_INDEX (u1Index);
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1CbsMagnitude;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1CbsMultiplier;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1EirMagnitude;
    ELM_INCR_INDEX (u1Index);
    ELM_MEMCPY (&au1UniInfo[u1Index], &BwProfile.u2EirMultiplier,
                ELM_EIR_MULTIPLIER_LENGTH);
    ELM_INCR_INDEX (u1Index);
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1EbsMagnitude;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1EbsMultiplier;
    ELM_INCR_INDEX (u1Index);
    au1UniInfo[u1Index] = BwProfile.u1PriorityBits;

    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, au1UniInfo,
                               u1Offset, u1UniStatusIeLength)
        == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_BUFFER_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmMsgRcvdOperStatusCalc                             */
/*                                                                           */
/* Description        : This function determines the operational status of   */
/*                      ELMI, when a Status Enquiry message is received.     */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port entry structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmMsgRcvdOperStatusCalc (tElmPortEntry * pElmPortEntry)
{
    /* Operational Status of ELMI can only be calulated when PVT is enabled */
    if (ELM_IS_PVT_ENABLED (pElmPortEntry->u4IfIndex) == ELM_TRUE)
    {
        if (pElmPortEntry->u1ElmiOperStatus == ELM_OPER_ENABLED)
        {
            /* When ELMI is opertional, reset the Status Counter to N393 on
             * reception of a Status Enquiry message
             */
            pElmPortEntry->u1StatusCounter =
                pElmPortEntry->u1StatusCounterConfigured;
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                          "Status Counter restarted with value=%d\n",
                          pElmPortEntry->u1StatusCounter);
        }
        else
        {
            ELM_DECR_STATUS_COUNTER (pElmPortEntry->u4IfIndex);
            if (pElmPortEntry->u1StatusCounter == ELM_INIT_VAL)
            {
                /* ELMI was not operational. ELMI has received N393 
                 * consecutive Status Enquiries. This indicates that ELMI has 
                 * got operational.
                 * Reset Status counter to N393 and change the operational 
                 * status of ELMI.
                 */
                pElmPortEntry->u1StatusCounter =
                    pElmPortEntry->u1StatusCounterConfigured;
                pElmPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                ElmElmiOperStatusIndication (pElmPortEntry->u4IfIndex,
                                             ELM_OPER_ENABLED);
                ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                              "ELMI ENABLED with Status Counter=%d\n",
                              pElmPortEntry->u1StatusCounter);
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : ElmPvtExpired                                        */
/*                                                                           */
/* Description        : This function handles the expiry of PVT.             */
/*                      It calculates the operational status of ELMI         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number at which the timer has expired*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
INT4
ElmPvtExpired (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    ELM_TRC_FN_ENTRY ();
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return ELM_FAILURE;
    }
    /* Expiry of PVT indicates that ELMI UNI-N has not received any 
     * Status Enquiry in the time interval when PVT was running.
     * This is an abnormal case. Check if ELMI has gone Non-operational
     */
    ElmiPvtExpiredTrap (u4IfIndex, ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        /* Synchup with the standby */
        ElmRedSyncUpPerPortInfo (u4IfIndex);
    }
    if (pElmPortEntry->u1ElmiOperStatus == ELM_OPER_ENABLED)
    {
        ELM_DECR_STATUS_COUNTER (pElmPortEntry->u4IfIndex);
        if (pElmPortEntry->u1StatusCounter == (UINT1) ELM_INIT_VAL)
        {
            /* ELMI has not received N393 consecutive Status Enquiries.
             * This indicates that ELMI has gone not operational.
             * Reset Status counter to N393 and change the operational 
             * status of ELMI.
             */
            pElmPortEntry->u1StatusCounter =
                pElmPortEntry->u1StatusCounterConfigured;
            pElmPortEntry->u1ElmiOperStatus = ELM_OPER_DISABLED;
            ElmElmiOperStatusIndication (u4IfIndex, ELM_OPER_DISABLED);
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                          "ELMI DISABLED with Status Counter=%d\n",
                          pElmPortEntry->u1StatusCounter);
        }
    }
    else
    {
        /* The most recent consecutive N393 PVT timer expiries were abnormal 
         * so elmi is considered non operational and elmi oper status is 
         * made down
         */
        pElmPortEntry->u1StatusCounter =
            pElmPortEntry->u1StatusCounterConfigured;
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                      "Status Counter reset to value = %d\n",
                      pElmPortEntry->u1StatusCounter);
    }
    if (ElmStartTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                       pElmPortEntry->u1PvtValueConfigured) == ELM_FAILURE)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "PVT Timer NOT Started ! \n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmAsynchTimerExpired                                */
/*                                                                           */
/* Description        : This function handles the expiry of Asynchronous     */
/*                      Timer.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number at which the timer has expired*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
INT4
ElmAsynchTimerExpired (UINT4 u4IfIndex)
{
    tElmAsynchMessageNode *pElmAsynchMessageNode = NULL;
    tElmSll            *pElmList = NULL;

    ELM_UNUSED (u4IfIndex);
    ELM_TRC_FN_ENTRY ();

    pElmList = ELM_SLL_LIST (u4IfIndex);
    if (pElmList == NULL)
    {
        return ELM_FAILURE;
    }
    pElmAsynchMessageNode = (tElmAsynchMessageNode *) ELM_SLL_FIRST (pElmList);
    if (pElmAsynchMessageNode != NULL)
    {
        /* Send Asynchronous message for this information now */
        if (ElmPortTxAsyncStatusMessage (pElmAsynchMessageNode->u2EvcRefId,
                                         pElmAsynchMessageNode->u4IfIndex,
                                         pElmAsynchMessageNode->u1EvcStatus)
            != ELM_SUCCESS)
        {
            /* Asynchronous message not sent */
            return ELM_FAILURE;
        }
        /* Delete the node whose information has been sent */
        ELM_SLL_DELETE (pElmList, &(pElmAsynchMessageNode->ElmAsynchNextNode));
        ELM_RELEASE_ASYNCH_LIST_MEM_BLOCK (pElmAsynchMessageNode);
    }
    /* If the list is empty when the  timer expired, then there
     * is no information pending at the UNI-N side that needs to be send
     */
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleEvcStatusChangeEvent                        */
/*                                                                           */
/* Description        : This function handles the event that the EVC status  */
/*                      has changed                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port No of the status changed EVC         */
/*                      u2EvcRefId - Reference ID of the EVC whose status has*/
/*                      changed                                              */
/*                      u1EvcStatus - Changed status of the EVC              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
INT4
ElmHandleEvcStatusChangeEvent (UINT4 u4IfIndex, UINT2 u2EvcRefId,
                               UINT1 u1EvcStatus)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tElmAsynchMessageNode *pElmAsynchMessageNode = NULL;
    tElmSll            *pElmList = NULL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return ELM_FAILURE;
    }

    /* The difference between sending of 2 Asynchronous messages
     * should be greater than Maximum Value of Polling Timer.
     * If UNI-N gets an indication of an EVC status change in that time 
     * interval, the message should be stored and then sent on Asynchronous
     * Timer expiry
     */
    if (pElmPortEntry->pAsyncMsgTimer != NULL)
    {
        /* Store this message in a linked list and return */
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "Asynchronous Timer running. Send the message on Timer"
                 " Expiry ! \n");
        if (ELM_ALLOC_ASYNCH_LIST_MEM_BLOCK (pElmAsynchMessageNode) == NULL)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "No Memory Available\n");
            return ELM_FAILURE;
        }
        if (pElmAsynchMessageNode == NULL)
        {
            return ELM_FAILURE;
        }
        ELM_MEMSET (pElmAsynchMessageNode, (UINT1) ELM_INIT_VAL,
                    sizeof (tElmAsynchMessageNode));
        ELM_SLL_INIT_NODE (&(pElmAsynchMessageNode->ElmAsynchNextNode));
        pElmAsynchMessageNode->u2EvcRefId = u2EvcRefId;
        pElmAsynchMessageNode->u4IfIndex = u4IfIndex;
        pElmAsynchMessageNode->u1EvcStatus = u1EvcStatus;

        pElmList = ELM_SLL_LIST (u4IfIndex);
        if (pElmList == NULL)
        {
            ELM_RELEASE_ASYNCH_LIST_MEM_BLOCK (pElmAsynchMessageNode);
            return ELM_FAILURE;
        }
        ELM_SLL_ADD (pElmList, &(pElmAsynchMessageNode->ElmAsynchNextNode));
        return ELM_SUCCESS;
    }
    else if (ElmPortTxAsyncStatusMessage (u2EvcRefId, u4IfIndex,
                                          u1EvcStatus) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "Asynchronous EVC Status Messsage transmission failed! \n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmGetEvcStatusLength                                */
/*                                                                           */
/* Description        : This function calcualtes the length of the EVC status*/
/*                      IE                                                   */
/*                                                                           */
/* Input(s)           : pElmEvcInfo - Pointer to the VCM EVC structure       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : u1EvcStatusLength - Length of EVC Status IE          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
VOID
ElmGetEvcStatusLength (tLcmEvcStatusInfo * pLcmEvcStatusInfo,
                       UINT1 *pu1EvcStatusLength)
{
    *pu1EvcStatusLength = (UINT1) (STRLEN (pLcmEvcStatusInfo->au1EvcIdentifier)
                                   + (UINT1) ELM_EVC_FIXED_IE_LENGTH);

    if (pLcmEvcStatusInfo->u1NoOfCos == 0)
    {
        *pu1EvcStatusLength += ELM_BW_PROFILE_TOTAL_LENGTH;
    }
    else
    {
        *pu1EvcStatusLength +=
            (UINT1) (pLcmEvcStatusInfo->u1NoOfCos *
                     ELM_BW_PROFILE_TOTAL_LENGTH);
    }
}

/*****************************************************************************/
/* Function Name      : ElmFillHeaderAndTransmit                             */
/*                                                                           */
/* Description        : This function fills the ELMI header for the PDU to be*/
/*                      sent and sends the PDU                               */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the Port Entry structure  */
/*                      u2DataLength - Length of the PDU to be sent          */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
INT4
ElmFillHeaderAndTransmit (tElmPortEntry * pElmPortEntry, UINT4 u2DataLength)
{
    tElmBufChainHeader *pElmPdu = NULL;
    if (ElmFillHeader (pElmPortEntry) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: Fill Ethernet Header FAILED!."
                      "Unable to transmit packet\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        return ELM_FAILURE;
    }
    /* Allocate the Memory for CRU Buffer */
    pElmPdu = ELM_ALLOC_CRU_BUF (u2DataLength, ELM_INIT_VAL);
    if (pElmPdu == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        ELM_INCR_BUFFER_FAILURE_COUNT ();
        /*generate the Buffer Failure Trap */
        ElmBufferFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_BUFFER_TRC,
                 "ELMI Pdu Allocation of CRU Buffer FAILED!\n");
        return ELM_FAILURE;
    }
    /*Copy the ELMI Pdu to CRU Buffur */
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, gElmGlobalInfo.gu1ElmPdu,
                               0, u2DataLength) == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ELMI PDU Copy Over CRU Buffer FAILED!\n");
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }
    ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u2DataLength);
    ELM_TRC (ELM_CONTROL_PATH_TRC, " Packet transmitted successfully\n");
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmResetVarsAfterFullStatus                          */
/*                                                                           */
/* Description        : This function resets the variables after Full Status */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the Port Entry Structure  */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmResetVarsAfterFullStatus (tElmPortEntry * pElmPortEntry)
{
    pElmPortEntry->bFullStatusInProgress = ELM_FALSE;
    pElmPortEntry->u2LastVlanIdSent = (UINT2) ELM_INIT_VAL;
    pElmPortEntry->u2LastEvcRefIdForVlan = (UINT2) ELM_INIT_VAL;
    pElmPortEntry->u2LastEvcRefIdSent = (UINT2) ELM_INIT_VAL;
    pElmPortEntry->u2CeVlanSentInPrevMsg = (UINT2) ELM_INIT_VAL;
    pElmPortEntry->u1PktCeVlanInfoRemaining = (UINT1) ELM_INIT_VAL;
    pElmPortEntry->u1CeVlanInfoRemaining = (UINT1) ELM_INIT_VAL;
    pElmPortEntry->u1EvcStatusRemaining = (UINT1) ELM_INIT_VAL;
}

/*****************************************************************************/
/* Function Name      : ElmPreProcessPackets                                 */
/*                                                                           */
/* Description        : This function processes the packet even before the   */
/*                      enquiry for them is received.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port                      */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmPreProcessPackets (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    INT4                i4TxRetValue = ELM_SUCCESS;
    UINT1               u1PktTransmitted = ELM_FULL_STATUS_CONTINUED;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "MSG: No Port Information present \n");
        return;
    }
    if (pElmPortEntry->bFullStatusInProgress != ELM_TRUE)
    {
        /* This will free all the packets in this chain */
        while (pElmPortEntry->pElmFirstPdu != NULL)
        {
            pElmPortEntry->pElmNextPdu =
                pElmPortEntry->pElmFirstPdu->pNextChain;
            ELM_RELEASE_CRU_BUF (pElmPortEntry->pElmFirstPdu, ELM_FALSE);
            pElmPortEntry->pElmFirstPdu = pElmPortEntry->pElmNextPdu;
        }

        pElmPortEntry->pElmFirstPdu = NULL;
        pElmPortEntry->pElmNextPdu = NULL;
        pElmPortEntry->pElmLastPdu = NULL;

        if (gElmCruAllocatedMemory >= pElmPortEntry->u4NoOfCruBytesAllocated)
        {
            gElmCruAllocatedMemory =
                gElmCruAllocatedMemory - pElmPortEntry->u4NoOfCruBytesAllocated;
            pElmPortEntry->u4NoOfCruBytesAllocated = ELM_INIT_VAL;

            LCM_LOCK ();
            while ((u1PktTransmitted == (UINT1) ELM_FULL_STATUS_CONTINUED)
                   && (i4TxRetValue == (INT4) ELM_SUCCESS))
            {
                i4TxRetValue = ElmPortTxStatusMessage (pElmPortEntry,
                                                       ELM_UPDATE_DATABASE_TRIGGER,
                                                       &u1PktTransmitted);
            }
            LCM_UNLOCK ();

            if (i4TxRetValue == ELM_FAILURE)
            {
                /* This will free all the packets in this chain */
                while (pElmPortEntry->pElmFirstPdu != NULL)
                {
                    pElmPortEntry->pElmNextPdu =
                        pElmPortEntry->pElmFirstPdu->pNextChain;
                    ELM_RELEASE_CRU_BUF (pElmPortEntry->pElmFirstPdu,
                                         ELM_FALSE);
                    pElmPortEntry->pElmFirstPdu = pElmPortEntry->pElmNextPdu;
                }

                pElmPortEntry->pElmFirstPdu = NULL;
                pElmPortEntry->pElmNextPdu = NULL;
                pElmPortEntry->pElmLastPdu = NULL;
                gElmCruAllocatedMemory =
                    gElmCruAllocatedMemory -
                    pElmPortEntry->u4NoOfCruBytesAllocated;
                pElmPortEntry->u4NoOfCruBytesAllocated = ELM_INIT_VAL;
            }
        }
        else
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "MSG: CRU Memory Corrupted by ELMI \n");
            return;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ElmGetVlanCapacity                                   */
/*                                                                           */
/* Description        : This function calculates the number of Vlans that can*/
/*                      be accomodated in the space left for the PDU         */
/*                                                                           */
/* Input(s)           : u2DataLength = Length already filled in the PDU      */
/*                      pu2NoOfPossibleVlanInfo = No of Possible Vlans       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmGetVlanCapacity (UINT2 u2DataLength, UINT2 *pu2NoOfPossibleVlanInfo)
{
    UINT2               u2RemainingLength = (UINT2) ELM_INIT_VAL;
    UINT2               u2SpaceLeftForVlans = (UINT2) ELM_INIT_VAL;
    UINT2               u2NoOfIe = (UINT2) ELM_INIT_VAL;
    u2RemainingLength = (UINT2) ELM_MAX_PDU_SIZE - u2DataLength;
    u2NoOfIe =
        u2RemainingLength / (UINT2) ((UINT2) ELM_MAX_CE_VLAN_INFO +
                                     (UINT2) ELM_SINGLE_VLAN_SIZE);
    if ((u2RemainingLength % (ELM_MAX_CE_VLAN_INFO + ELM_SINGLE_VLAN_SIZE)) !=
        ELM_INIT_VAL)
    {
        u2NoOfIe = u2NoOfIe + (UINT2) ELM_INCR_NO_IE_ONE;
    }
    u2SpaceLeftForVlans =
        u2RemainingLength - (u2NoOfIe * (UINT2) ELM_CE_VLAN_IE_FIXED_SIZE);
    *pu2NoOfPossibleVlanInfo =
        u2SpaceLeftForVlans / (UINT2) ELM_SINGLE_VLAN_SIZE;
    return;
}
