/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : elmcli.c                                       |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : ELMI                                     |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* 
*  $Id: elmcli.c,v 1.5 2012/08/14 08:52:33 siva Exp $
*
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI RSTP/MSTP Commands       |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 25th Jun 2007   |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/
#ifndef __ELMICLI_C__
#define __ELMICLI_C__

#include "elminc.h"
#include "elmcli.h"

INT4
cli_process_elm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT1              *args[ELM_CLI_MAX_ARGS];
    INT1                argno = (UINT1) ELM_INIT_VAL;
    UINT4               u4Value = ELM_INIT_VAL;
    UINT4               u4Index = ELM_INIT_VAL;
    UINT4               u4ErrCode = ELM_INIT_VAL;
    UINT4               u4CmdType = ELM_INIT_VAL;
    UINT1               u1Action = (UINT1) ELM_INIT_VAL;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4Type = ELM_INIT_VAL;
    UINT4               u4ElmSystemControl = ELM_INIT_VAL;
    UINT4               u4IfIndex = ELM_INIT_VAL;
    tElmPortEntry      *pElmPortEntry = NULL;

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == ELM_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, ElmLock, ElmUnLock);

    ELM_LOCK ();
    switch (u4Command)
    {
        case CLI_ELMI_GLOB_MOD_STATUS:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Value = ELM_ENABLE_GLOBAL;
                i4RetVal = ElmCliSetGlobalModuleStatus (CliHandle, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_NO_GLOB_MOD_STATUS:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Value = ELM_DISABLE_GLOBAL;
                i4RetVal = ElmCliSetGlobalModuleStatus (CliHandle, u4Value);
            }

            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;

        case CLI_ELMI_CLEAR_STAT:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                if (u4Index == ELM_INIT_VAL)
                {
                    for (u4IfIndex = 1;
                         u4IfIndex <= ELM_MAX_NUM_PORTS_SUPPORTED; u4IfIndex++)
                    {
                        if ((pElmPortEntry =
                             ELM_GET_PORTENTRY (u4IfIndex)) == NULL)
                        {
                            continue;
                        }
                        i4RetVal = ElmCliClearStat (u4IfIndex);
                    }
                }
                else
                {
                    i4RetVal = ElmCliClearStat (u4Index);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_TRACE_ENABLE:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u1Action = 1;

                u4Value = CLI_PTR_TO_U4 (args[0]);
                i4RetVal = ElmSetTrace (CliHandle, u4Value, u1Action);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_TRACE_DISABLE:
            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u1Action = 0;

                u4Value = CLI_PTR_TO_U4 (args[0]);

                i4RetVal = ElmSetTrace (CliHandle, u4Value, u1Action);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_SHOW_INFO:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {

                u4Type = CLI_PTR_TO_U4 (args[0]);

                i4RetVal = ElmCliDisplayDetails (CliHandle, u4Index, u4Type);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_SHOW_STATUS:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {

                u4Type = CLI_PTR_TO_U4 (args[0]);

                i4RetVal = ElmCliShowStatus (CliHandle, u4Index, u4Type);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_PORT_PROPERTIES:

            u4Index = CLI_GET_IFINDEX ();

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                /*args[0]-> type of command  */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == ELMI_POLLING_COUNTER)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    i4RetVal =
                        ElmCliSetPollingCounter (CliHandle, u4Index, u4Value);

                }
                else if (u4CmdType == ELMI_STATUS_COUNTER)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    i4RetVal =
                        ElmCliSetStatusCounter (CliHandle, u4Index, u4Value);
                }
                else if (u4CmdType == ELMI_POLLING_TIMER)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    i4RetVal =
                        ElmCliSetPollingTimer (CliHandle, u4Index, u4Value);
                }
                else if (u4CmdType == ELMI_POLLING_VERIFICATION_TIMER)
                {
                    u4Value = CLI_PTR_TO_U4 (args[1]);
                    i4RetVal = ElmCliSetPollingVerificationTimer (CliHandle,
                                                                  u4Index,
                                                                  u4Value);
                }

            }

            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_NO_PORT_PROPERTIES:

            u4Index = CLI_GET_IFINDEX ();

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                /*args[0]-> type of command  */

                u4CmdType = CLI_PTR_TO_U4 (args[0]);

                if (u4CmdType == ELMI_POLLING_COUNTER)
                {
                    u4Value = ELM_DEFAULT_POLLING_COUNTER_VALUE;
                    i4RetVal =
                        ElmCliSetPollingCounter (CliHandle, u4Index, u4Value);

                }
                else if (u4CmdType == ELMI_STATUS_COUNTER)
                {
                    u4Value = ELM_DEFAULT_STATUS_COUNTER_VALUE;
                    i4RetVal =
                        ElmCliSetStatusCounter (CliHandle, u4Index, u4Value);
                }
                else if (u4CmdType == ELMI_POLLING_TIMER)
                {
                    u4Value = ELM_DEFAULT_PT_TIMER_VALUE;
                    i4RetVal =
                        ElmCliSetPollingTimer (CliHandle, u4Index, u4Value);
                }
                else if (u4CmdType == ELMI_POLLING_VERIFICATION_TIMER)
                {
                    u4Value = ELM_DEFAULT_PVT_TIMER_VALUE;
                    i4RetVal = ElmCliSetPollingVerificationTimer (CliHandle,
                                                                  u4Index,
                                                                  u4Value);
                }

            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;

        case CLI_ELMI_PORT_MOD_STATUS:

            u4Index = CLI_GET_IFINDEX ();

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Value = ELM_ENABLE_PORT;

                i4RetVal = ElmCliSetPortModuleStatus
                    (CliHandle, u4Index, u4Value);
            }

            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }

            break;
        case CLI_ELMI_NO_PORT_MOD_STATUS:

            u4Index = CLI_GET_IFINDEX ();

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Value = ELM_DISABLE_PORT;
                i4RetVal = ElmCliSetPortModuleStatus
                    (CliHandle, u4Index, u4Value);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;
        case CLI_ELMI_CUSTOMER_MODE:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Index = CLI_GET_IFINDEX ();
                i4RetVal = ElmCliSetCustomerMode (CliHandle, u4Index);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }

            break;
        case CLI_ELMI_NETWORK_MODE:

            if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
            {
                u4Index = CLI_GET_IFINDEX ();
                i4RetVal = ElmCliSetNetworkMode (CliHandle, u4Index);
            }
            else
            {
                CliPrintf (CliHandle, "\r%% ELMI Module is not Started\r\n ");
            }
            break;

        case CLI_ELMI_START_UP:
            u4ElmSystemControl = ELM_SNMP_START;
            i4RetVal = ElmCliSetSystemControl (CliHandle, u4ElmSystemControl);
            break;
        case CLI_ELMI_SHUT_DOWN:

            u4ElmSystemControl = ELM_SNMP_SHUTDOWN;
            i4RetVal = ElmCliSetSystemControl (CliHandle, u4ElmSystemControl);
            break;

        default:
            /* Given command does not match with any of the SET or SHOW 
             * commands */
            CliPrintf (CliHandle, "\r%% Invalid Command !\r\n ");
            i4RetVal = CLI_FAILURE;
            break;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ELM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s\r\n", ElmCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    ELM_UNLOCK ();

    return i4RetVal;
}

#endif
