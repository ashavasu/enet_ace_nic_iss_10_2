/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmutil.c,v 1.7 2012/04/04 14:14:48 siva Exp $
 *
 * Description: This file contains all utility routines used by the
 *              ELMI Module.
 *
 *******************************************************************/

#include "elminc.h"

#ifdef ELMI_TRACE_WANTED
/*****************************************************************************/
/* Function Name      : ElmPktDump                                           */
/*                                                                           */
/* Description        : Prints Packet contents.                              */
/*                      This routine is used for Trace.                      */
/*                                                                           */
/* Input(s)           : pBuf - CRU Buffer pointer.                           */
/*                      u2Length - Length of valid data.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
ElmPktDump (tElmBufChainHeader * pMsg, UINT2 u2Length)
{
    UINT2               u2Cnt = (UINT2) ELM_INIT_VAL;
    UINT1               *pu1Buf =  NULL;
    
    if (ELM_ALLOC_ELM_ETH_FRAME_BUF (pu1Buf) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                   "pu1Buf:Memory Allocation failed\n");
         
        return;
    }

    ELM_MEMSET (pu1Buf, ELM_MEMSET_VAL,
                        ELM_MAX_ETH_FRAME_SIZE);
    
    ELM_COPY_FROM_CRU_BUF (pMsg, pu1Buf, 0, u2Length);
    for (u2Cnt = 1; ((u2Cnt < ELM_MAX_ETH_FRAME_SIZE) && (u2Cnt <= u2Length));
         u2Cnt++)
    {
        ELM_PRINT (ELM_ALL_FLAG, ELM_ALL_FLAG, " ", "%2x", pu1Buf[u2Cnt - 1]);
        if ((u2Cnt % 8) == 0)
        {
            ELM_PRINT (ELM_ALL_FLAG, ELM_ALL_FLAG, " ", "\n");
        }
    }
    ELM_PRINT (ELM_ALL_FLAG, ELM_ALL_FLAG, " ", "\n");
    ELM_RELEASE_ELM_ETH_FRAME_BUF(pu1Buf);
    return;
}
#endif /* ELM_TRACE_WANTED */

/****************************************************************************
 * Function    :  ElmSnmpLowValidatePortIndex
 * Input       :  i4ElmiPortIndex
 *
 * Output      :  None
 * Returns     :  ELM_SUCCESS / ELM_FAILURE
 *****************************************************************************/

INT4
ElmSnmpLowValidatePortIndex (INT4 i4ElmiPortIndex)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return ELM_FAILURE;
    }

    if (i4ElmiPortIndex < ELM_MIN_NUM_PORTS
        || i4ElmiPortIndex > ELM_MAX_NUM_PORTS_SUPPORTED)
    {
        return ELM_FAILURE;
    }

    if (L2IwfIsPortInPortChannel ((UINT2) i4ElmiPortIndex) == L2IWF_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/****************************************************************************
 * Function    :  ElmSnmpLowGetNextValidIndex
 * Input       :  i4ElmiPortIndex
 *
 * Output      :  pi4NextElmiPortIndex
 * Returns     :  ELM_SUCCESS / ELM_FAILURE
 *****************************************************************************/

INT4
ElmSnmpLowGetNextValidIndex (INT4 i4ElmiPortIndex, INT4 *pi4NextElmiPortIndex)
{
    UINT4               u4Count = ELM_INIT_VAL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return ELM_FAILURE;
    }

    for (u4Count = (UINT2) (i4ElmiPortIndex + 1); u4Count <=
         ELM_MAX_NUM_PORTS_SUPPORTED; u4Count++)
    {
        if (L2IwfIsPortInPortChannel (u4Count) == L2IWF_SUCCESS)
        {
            continue;
        }
        if ((ELM_GET_PORTENTRY (u4Count)) != NULL)
        {
            *pi4NextElmiPortIndex = u4Count;
            return ELM_SUCCESS;
        }
    }
    return ELM_FAILURE;
}

/****************************************************************************
 * Function    :  ElmSnmpLowGetFirstValidIndex
 * Input       :  None
 *
 * Output      :  i4ElmiPortIndex
 * Returns     :  ELM_SUCCESS / ELM_FAILURE
 *****************************************************************************/

INT4
ElmSnmpLowGetFirstValidIndex (INT4 *i4ElmiPortIndex)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return ELM_FAILURE;
    }

    if (ElmSnmpLowGetNextValidIndex (0, i4ElmiPortIndex) != ELM_SUCCESS)
    {
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/****************************************************************************
 * Function    :  ElmValidatePortEntry
 * Input       :  i4ElmiPortIndex
 *
 * Output      :  None
 * Returns     :  ELM_SUCCESS / ELM_FAILURE
 *****************************************************************************/

INT4
ElmValidatePortEntry (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return ELM_FAILURE;
    }

    if (u4IfIndex < ELM_MIN_NUM_PORTS
        && u4IfIndex > ELM_MAX_NUM_PORTS_SUPPORTED)
    {
        return ELM_FAILURE;
    }

    if (L2IwfIsPortInPortChannel ((UINT2) u4IfIndex) == L2IWF_SUCCESS)
    {
        return ELM_FAILURE;
    }

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pElmPortEntry != NULL)
    {
        return ELM_SUCCESS;
    }
    else
    {
        return ELM_FAILURE;
    }
}

INT4
ElmProcessSnmpRequest (tElmMsgNode * pNode)
{
    INT4                i4RetVal = ELM_SUCCESS;

    ElmHandleSnmpCfgMsg (pNode);

    if (ELM_RELEASE_LOCALMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "MGMT: Release of Local Msg Memory Block FAILED!\n");
        i4RetVal = ELM_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ElmAllocPortTblInfoBlock                             */
/*                                                                           */
/* Description        : This functions allocates the memory block for the    */
/*                      PortEntry Array                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
ElmAllocPortInfoBlock (VOID)
{

    /* Allocate memory for port table */
    if (ELM_ALLOC_PORT_TBL_BLOCK (gElmGlobalInfo.ppPortEntry) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);

        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "SYS: Memory Allocation for Global Port Table failed !!!\n");
        return ELM_FAILURE;
    }

    ELM_MEMSET (ELM_PORT_TBL (), (UINT1) ELM_INIT_VAL,
                (ELM_MAX_NUM_PORTS_SUPPORTED * sizeof (tElmPortEntry *)));

    return ELM_SUCCESS;
}
