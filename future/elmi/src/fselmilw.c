/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselmilw.c,v 1.15 2012/08/14 08:52:33 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fselmilw.h"
# include  "elminc.h"

extern UINT4        fselmi[8];
extern UINT4        FsElmiSystemControl[10];
PRIVATE VOID        ElmiNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElmiSystemControl
 Input       :  The Indices

                The Object 
                retValFsElmiSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiSystemControl (INT4 *pi4RetValFsElmiSystemControl)
{
    if (ELM_IS_ELMI_STARTED () == (UINT1) ELM_STARTED)
    {
        *pi4RetValFsElmiSystemControl = (INT4) ELM_SNMP_START;
    }
    else
    {
        *pi4RetValFsElmiSystemControl = (INT4) ELM_SNMP_SHUTDOWN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiModuleStatus
 Input       :  The Indices

                The Object 
                retValFsElmiModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiModuleStatus (INT4 *pi4RetValFsElmiModuleStatus)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        *pi4RetValFsElmiModuleStatus = (UINT1) ELM_DISABLE_GLOBAL;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsElmiModuleStatus = (INT4) ELM_GLOBAL_STATUS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiActivePortCount
 Input       :  The Indices

                The Object 
                retValFsElmiActivePortCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiActivePortCount (UINT4 *pu4RetValFsElmiActivePortCount)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        *pu4RetValFsElmiActivePortCount = (UINT1) ELM_INIT_VAL;

        return SNMP_SUCCESS;
    }

    *pu4RetValFsElmiActivePortCount = gElmGlobalInfo.u2NoOfElmiEnabledPorts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTraceOption
 Input       :  The Indices

                The Object 
                retValFsElmiTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTraceOption (INT4 *pi4RetValFsElmiTraceOption)
{
    *pi4RetValFsElmiTraceOption = ELM_TRACE_OPTION;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiBufferOverFlowCount
 Input       :  The Indices

                The Object 
                retValFsElmiBufferOverFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiBufferOverFlowCount (UINT4 *pu4RetValFsElmiBufferOverFlowCount)
{
    *pu4RetValFsElmiBufferOverFlowCount = gElmGlobalInfo.u4BufferFailureCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiMemAllocFailureCount
 Input       :  The Indices

                The Object 
                retValFsElmiMemAllocFailureCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiMemAllocFailureCount (UINT4 *pu4RetValFsElmiMemAllocFailureCount)
{
    *pu4RetValFsElmiMemAllocFailureCount = gElmGlobalInfo.u4MemoryFailureCount;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElmiSystemControl
 Input       :  The Indices

                The Object 
                setValFsElmiSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiSystemControl (INT4 i4SetValFsElmiSystemControl)
{

    if (i4SetValFsElmiSystemControl == (INT4) ELM_SNMP_START)
    {
        if (ELM_IS_ELMI_STARTED () == ELM_TRUE)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "LW: ELMI Module is already started !!!\n");
            return SNMP_SUCCESS;
        }
        if (ElmHandleModuleInit () != ELM_SUCCESS)
        {
            ELM_TRC (ELM_INIT_SHUT_TRC | ELM_ALL_FAILURE_TRC,
                     "MSG:nmhSetFsElmiSystemControl function returned FAILURE!!!\n");
            return (INT1) SNMP_FAILURE;
        }
    }
    else
    {
        if (ELM_IS_ELMI_STARTED () != ELM_TRUE)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "LW: ELMI Module is not started to shut down !!!\n");
            return SNMP_SUCCESS;
        }
        /* Shutting Down ELM Module */
        ElmHandleModuleShutdown ();

        /* Notify MSR with the Elmi oids */
        ElmiNotifyProtocolShutdownStatus ();
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiModuleStatus
 Input       :  The Indices

                The Object 
                setValFsElmiModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiModuleStatus (INT4 i4SetValFsElmiModuleStatus)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (i4SetValFsElmiModuleStatus == ELM_ENABLE_GLOBAL)
    {
        if (ELM_GLOBAL_STATUS == (UINT1) ELM_ENABLE_GLOBAL)
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: ELMI Module is Already Enabled\n");
            return SNMP_SUCCESS;
        }
        u1MsgType = ELM_ENABLE_MSG;
    }
    else
    {
        if (ELM_GLOBAL_STATUS == (UINT1) ELM_DISABLE_GLOBAL)
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: ELMI Module is Already Disabled\n");
            return SNMP_SUCCESS;
        }
        u1MsgType = ELM_DISABLE_MSG;
    }

    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));

        return SNMP_FAILURE;
    }
    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    pMsgNode->MsgType = u1MsgType;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiTraceOption
 Input       :  The Indices

                The Object 
                setValFsElmiTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiTraceOption (INT4 i4SetValFsElmiTraceOption)
{
    ELM_TRACE_OPTION = (UINT4) i4SetValFsElmiTraceOption;
    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                  "LW: Management SET of Trace Option as %d Success\n",
                  i4SetValFsElmiTraceOption);
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElmiSystemControl
 Input       :  The Indices

                The Object 
                testValFsElmiSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsElmiSystemControl)
{
    if ((i4TestValFsElmiSystemControl != ELM_SNMP_START) &&
        (i4TestValFsElmiSystemControl != ELM_SNMP_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiModuleStatus
 Input       :  The Indices

                The Object 
                testValFsElmiModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiModuleStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsElmiModuleStatus)
{
    if ((i4TestValFsElmiModuleStatus != ELM_ENABLE_GLOBAL) &&
        (i4TestValFsElmiModuleStatus != ELM_DISABLE_GLOBAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiTraceOption
 Input       :  The Indices

                The Object 
                testValFsElmiTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsElmiTraceOption)
{
    if ((i4TestValFsElmiTraceOption < ELM_MIN_TRACE_VAL) ||
        (i4TestValFsElmiTraceOption >= ELM_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElmiSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElmiModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiModuleStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElmiTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElmiPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElmiPortTable
 Input       :  The Indices
                FsElmiPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElmiPortTable (INT4 i4FsElmiPort)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "ELMI System Control is Shutdown!\n");
        return SNMP_FAILURE;
    }

    if (ElmSnmpLowValidatePortIndex (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "MGMT: Invalid Port Index!\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElmiPortTable
 Input       :  The Indices
                FsElmiPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElmiPortTable (INT4 *pi4FsElmiPort)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "ELMI Module is not Enabled!\n");
        return SNMP_FAILURE;
    }

    if (ElmSnmpLowGetFirstValidIndex (pi4FsElmiPort) != ELM_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElmiPortTable
 Input       :  The Indices
                FsElmiPort
                nextFsElmiPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElmiPortTable (INT4 i4FsElmiPort, INT4 *pi4NextFsElmiPort)
{
    if (i4FsElmiPort < (INT4) ELM_INIT_VAL)
    {
        return SNMP_FAILURE;
    }

    if (ElmSnmpLowGetNextValidIndex (i4FsElmiPort, pi4NextFsElmiPort)
        != ELM_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElmiPortElmiStatus
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiPortElmiStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPortElmiStatus (INT4 i4FsElmiPort,
                            INT4 *pi4RetValFsElmiPortElmiStatus)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);
    if (pElmPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsElmiPortElmiStatus = pElmPortEntry->u1ElmiPortStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiUniSide
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiUniSide
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiUniSide (INT4 i4FsElmiPort, INT4 *pi4RetValFsElmiUniSide)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    *pi4RetValFsElmiUniSide = pElmPortEntry->u1ElmiMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiOperStatus
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiOperStatus (INT4 i4FsElmiPort, INT4 *pi4RetValFsElmiOperStatus)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    *pi4RetValFsElmiOperStatus = pElmPortEntry->u1ElmiOperStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiStatusCounter
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiStatusCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiStatusCounter (INT4 i4FsElmiPort,
                           INT4 *pi4RetValFsElmiStatusCounter)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED () == (UINT1) ELM_STOPPED))
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);
    *pi4RetValFsElmiStatusCounter =
        (INT4) pElmPortEntry->u1StatusCounterConfigured;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiPollingVerificationTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiPollingVerificationTimerValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPollingVerificationTimerValue (INT4 i4FsElmiPort,
                                           INT4
                                           *pi4RetValFsElmiPollingVerificationTimerValue)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "Port Does Not  exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);
    if (((ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort)) != ELM_NETWORK_SIDE))
    {
        *pi4RetValFsElmiPollingVerificationTimerValue = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValFsElmiPollingVerificationTimerValue =
        (INT4) pElmPortEntry->u1PvtValueConfigured;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiPollingTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiPollingTimerValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPollingTimerValue (INT4 i4FsElmiPort,
                               INT4 *pi4RetValFsElmiPollingTimerValue)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pi4RetValFsElmiPollingTimerValue = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pi4RetValFsElmiPollingTimerValue = pElmPortEntry->u1PtValueConfigured;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiPollingCounterValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiPollingCounterValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPollingCounterValue (INT4 i4FsElmiPort,
                                 INT4 *pi4RetValFsElmiPollingCounterValue)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "Port Does Not  exist for this instance!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pi4RetValFsElmiPollingCounterValue = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pi4RetValFsElmiPollingCounterValue =
        pElmPortEntry->u2PollingCounterConfigured;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiNoOfConfiguredEvcs
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiNoOfConfiguredEvcs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiNoOfConfiguredEvcs (INT4 i4FsElmiPort,
                                INT4 *pi4RetValFsElmiNoOfConfiguredEvcs)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pi4RetValFsElmiNoOfConfiguredEvcs = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    *pi4RetValFsElmiNoOfConfiguredEvcs =
        (INT4) pElmPortEntry->u2NoOfEvcConfigured;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxElmiCheckEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxElmiCheckEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxElmiCheckEnqMsgCount (INT4 i4FsElmiPort,
                                    UINT4
                                    *pu4RetValFsElmiRxElmiCheckEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiRxElmiCheckEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxElmiCheckEnqMsgCount =
        pElmPortEntry->u4ElmiRxElmiCheckEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxFullStatusEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxFullStatusEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxFullStatusEnqMsgCount (INT4 i4FsElmiPort,
                                     UINT4
                                     *pu4RetValFsElmiRxFullStatusEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiRxFullStatusEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxFullStatusEnqMsgCount =
        pElmPortEntry->u4ElmiRxFullStatusEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxFullStatusContEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxFullStatusContEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxFullStatusContEnqMsgCount (INT4 i4FsElmiPort,
                                         UINT4
                                         *pu4RetValFsElmiRxFullStatusContEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiRxFullStatusContEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxFullStatusContEnqMsgCount =
        pElmPortEntry->u4ElmiRxFullStatusContdEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxElmiCheckMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxElmiCheckMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxElmiCheckMsgCount (INT4 i4FsElmiPort,
                                 UINT4 *pu4RetValFsElmiTxElmiCheckMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist !\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);
    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiTxElmiCheckMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxElmiCheckMsgCount = pElmPortEntry->u4ElmiTxElmiCheckMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxFullStatusMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxFullStatusMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxFullStatusMsgCount (INT4 i4FsElmiPort,
                                  UINT4 *pu4RetValFsElmiTxFullStatusMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);
    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiTxFullStatusMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxFullStatusMsgCount = pElmPortEntry->u4ElmiTxFullStatusMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxFullStatusContMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxFullStatusContMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxFullStatusContMsgCount (INT4 i4FsElmiPort,
                                      UINT4
                                      *pu4RetValFsElmiTxFullStatusContMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiTxFullStatusContMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxFullStatusContMsgCount =
        pElmPortEntry->u4ElmiTxFullStatusContdMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxAsyncStatusMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxAsyncStatusMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxAsyncStatusMsgCount (INT4 i4FsElmiPort,
                                   UINT4 *pu4RetValFsElmiTxAsyncStatusMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        *pu4RetValFsElmiTxAsyncStatusMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxAsyncStatusMsgCount =
        pElmPortEntry->u4ElmiTxAsyncStatusMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxElmiCheckMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxElmiCheckMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxElmiCheckMsgCount (INT4 i4FsElmiPort,
                                 UINT4 *pu4RetValFsElmiRxElmiCheckMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);
    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiRxElmiCheckMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxElmiCheckMsgCount = pElmPortEntry->u4ElmiRxCheckMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxFullStatusMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxFullStatusMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxFullStatusMsgCount (INT4 i4FsElmiPort,
                                  UINT4 *pu4RetValFsElmiRxFullStatusMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiRxFullStatusMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }

    *pu4RetValFsElmiRxFullStatusMsgCount = pElmPortEntry->u4ElmiRxFullStatMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxFullStatusContMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxFullStatusContMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxFullStatusContMsgCount (INT4 i4FsElmiPort,
                                      UINT4
                                      *pu4RetValFsElmiRxFullStatusContMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiRxFullStatusContMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxFullStatusContMsgCount =
        pElmPortEntry->u4ElmiRxFullStatContdMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxAsyncStatusMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxAsyncStatusMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxAsyncStatusMsgCount (INT4 i4FsElmiPort,
                                   UINT4 *pu4RetValFsElmiRxAsyncStatusMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiRxAsyncStatusMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRxAsyncStatusMsgCount =
        pElmPortEntry->u4ElmiRxAsyncStatusMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxElmiCheckEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxElmiCheckEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxElmiCheckEnqMsgCount (INT4 i4FsElmiPort,
                                    UINT4
                                    *pu4RetValFsElmiTxElmiCheckEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiTxElmiCheckEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxElmiCheckEnqMsgCount =
        pElmPortEntry->u4ElmiTxElmiCheckEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxFullStatusEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxFullStatusEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxFullStatusEnqMsgCount (INT4 i4FsElmiPort,
                                     UINT4
                                     *pu4RetValFsElmiTxFullStatusEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiTxFullStatusEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxFullStatusEnqMsgCount =
        pElmPortEntry->u4ElmiTxFullStatEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiTxFullStatusContEnqMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiTxFullStatusContEnqMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiTxFullStatusContEnqMsgCount (INT4 i4FsElmiPort,
                                         UINT4
                                         *pu4RetValFsElmiTxFullStatusContEnqMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4RetValFsElmiTxFullStatusContEnqMsgCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiTxFullStatusContEnqMsgCount =
        pElmPortEntry->u4ElmiTxFullStatContdEnquiryMsg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxValidMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxValidMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxValidMsgCount (INT4 i4FsElmiPort,
                             UINT4 *pu4RetValFsElmiRxValidMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiRxValidMsgCount = pElmPortEntry->u4ElmiValidMsgRxed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRxInvalidMsgCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRxInvalidMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRxInvalidMsgCount (INT4 i4FsElmiPort,
                               UINT4 *pu4RetValFsElmiRxInvalidMsgCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiRxInvalidMsgCount = pElmPortEntry->u4ElmiInvalidMsgRxed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRelErrStatusTimeOutCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRelErrStatusTimeOutCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRelErrStatusTimeOutCount (INT4 i4FsElmiPort,
                                      UINT4
                                      *pu4RetValFsElmiRelErrStatusTimeOutCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (((ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort)) != ELM_CUSTOMER_SIDE))
    {
        *pu4RetValFsElmiRelErrStatusTimeOutCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRelErrStatusTimeOutCount =
        pElmPortEntry->u4ElmiRelErrStatusTimeoutCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRelErrInvalidSeqNumCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRelErrInvalidSeqNumCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRelErrInvalidSeqNumCount (INT4 i4FsElmiPort,
                                      UINT4
                                      *pu4RetValFsElmiRelErrInvalidSeqNumCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiRelErrInvalidSeqNumCount =
        pElmPortEntry->u4ElmiRelErrInvalidSeqNumCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRelErrInvalidStatusRespCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRelErrInvalidStatusRespCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRelErrInvalidStatusRespCount (INT4 i4FsElmiPort,
                                          UINT4
                                          *pu4RetValFsElmiRelErrInvalidStatusRespCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (((ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort)) != ELM_CUSTOMER_SIDE))
    {
        *pu4RetValFsElmiRelErrInvalidStatusRespCount = (INT4) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRelErrInvalidStatusRespCount =
        pElmPortEntry->u4ElmiRelErrInvalidStatusCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiRelErrRxUnSolicitedStatusCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiRelErrRxUnSolicitedStatusCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiRelErrRxUnSolicitedStatusCount (INT4 i4FsElmiPort,
                                            UINT4
                                            *pu4RetValFsElmiRelErrRxUnSolicitedStatusCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    if (((ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort)) != ELM_CUSTOMER_SIDE))
    {
        *pu4RetValFsElmiRelErrRxUnSolicitedStatusCount = (INT1) ELM_INIT_VAL;
        return (INT1) SNMP_SUCCESS;
    }
    *pu4RetValFsElmiRelErrRxUnSolicitedStatusCount =
        pElmPortEntry->u4ElmiRelErrRxUnsolicitedStatusCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrInvalidProtVerCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrInvalidProtVerCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrInvalidProtVerCount (INT4 i4FsElmiPort,
                                       UINT4
                                       *pu4RetValFsElmiProErrInvalidProtVerCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrInvalidProtVerCount =
        pElmPortEntry->u4ElmiProErrInvalidProtVerCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrInvalidEvcRefIdCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrInvalidEvcRefIdCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrInvalidEvcRefIdCount (INT4 i4FsElmiPort,
                                        UINT4
                                        *pu4RetValFsElmiProErrInvalidEvcRefIdCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrInvalidEvcRefIdCount =
        pElmPortEntry->u4ElmiProErrInvalidEvcRefIdCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrInvalidMessageTypeCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrInvalidMessageTypeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrInvalidMessageTypeCount (INT4 i4FsElmiPort,
                                           UINT4
                                           *pu4RetValFsElmiProErrInvalidMessageTypeCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrInvalidMessageTypeCount =
        pElmPortEntry->u4ElmiProErrInvalidMessageTypeCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrOutOfSequenceInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrOutOfSequenceInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrOutOfSequenceInfoEleCount (INT4 i4FsElmiPort,
                                             UINT4
                                             *pu4RetValFsElmiProErrOutOfSequenceInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrOutOfSequenceInfoEleCount =
        pElmPortEntry->u4ElmiProErrOutOfSequenceInfoEleCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrDuplicateInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrDuplicateInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrDuplicateInfoEleCount (INT4 i4FsElmiPort,
                                         UINT4
                                         *pu4RetValFsElmiProErrDuplicateInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrDuplicateInfoEleCount =
        pElmPortEntry->u4ElmiProErrDuplicateInfoEleCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrMandatoryInfoEleMissingCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrMandatoryInfoEleMissingCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrMandatoryInfoEleMissingCount (INT4 i4FsElmiPort,
                                                UINT4
                                                *pu4RetValFsElmiProErrMandatoryInfoEleMissingCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrMandatoryInfoEleMissingCount =
        pElmPortEntry->u4ElmiProErrMandatoryInfoEleMissingCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrInvalidMandatoryInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrInvalidMandatoryInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrInvalidMandatoryInfoEleCount (INT4 i4FsElmiPort,
                                                UINT4
                                                *pu4RetValFsElmiProErrInvalidMandatoryInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrInvalidMandatoryInfoEleCount =
        pElmPortEntry->u4ElmiProErrInvalidMandatoryInfoEleCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrInvalidNonMandatoryInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount (INT4 i4FsElmiPort,
                                                   UINT4
                                                   *pu4RetValFsElmiProErrInvalidNonMandatoryInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrInvalidNonMandatoryInfoEleCount =
        pElmPortEntry->u4ElmiProErrInvalidNonMandatoryInfoEleCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrUnrecognizedInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrUnrecognizedInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrUnrecognizedInfoEleCount (INT4 i4FsElmiPort,
                                            UINT4
                                            *pu4RetValFsElmiProErrUnrecognizedInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrUnrecognizedInfoEleCount =
        pElmPortEntry->u4ElmiProErrUnrecognizedInfoEleCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrUnexpectedInfoEleCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrUnexpectedInfoEleCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrUnexpectedInfoEleCount (INT4 i4FsElmiPort,
                                          UINT4
                                          *pu4RetValFsElmiProErrUnexpectedInfoEleCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrUnexpectedInfoEleCount =
        pElmPortEntry->u4ElmiProErrUnexpectedInfoEleCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiProErrShortMessageCount
 Input       :  The Indices
                FsElmiPort

                The Object 
                retValFsElmiProErrShortMessageCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiProErrShortMessageCount (INT4 i4FsElmiPort,
                                     UINT4
                                     *pu4RetValFsElmiProErrShortMessageCount)
{
    tElmPortEntry      *pElmPortEntry = NULL;

    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Started!\n");
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (i4FsElmiPort);

    *pu4RetValFsElmiProErrShortMessageCount =
        pElmPortEntry->u4ElmiProErrShortMessageCount;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElmiPortElmiStatus
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiPortElmiStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiPortElmiStatus (INT4 i4FsElmiPort,
                            INT4 i4SetValFsElmiPortElmiStatus)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (i4SetValFsElmiPortElmiStatus == ELM_ENABLE_PORT)
    {
        if (ELM_IS_ELMI_ENABLED_ON_PORT ((UINT4) i4FsElmiPort))
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: ELMI Module is Already Enabled on the Port \n");
            return SNMP_SUCCESS;
        }
        u1MsgType = ELM_PORT_ELMI_ENABLE_MSG;
    }
    else
    {
        if (!ELM_IS_ELMI_ENABLED_ON_PORT ((UINT4) i4FsElmiPort))
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: ELMI Module Already Disabled on the Port \n");
            return SNMP_SUCCESS;
        }
        u1MsgType = ELM_PORT_ELMI_DISABLE_MSG;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));

        return SNMP_FAILURE;
    }

    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pMsgNode);
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiUniSide
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiUniSide
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiUniSide (INT4 i4FsElmiPort, INT4 i4SetValFsElmiUniSide)
{
    tElmMsgNode        *pMsgNode = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    pElmPortEntry = ELM_GET_PORTENTRY ((UINT4) i4FsElmiPort);

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if (i4SetValFsElmiUniSide == ELM_CUSTOMER_SIDE)
    {
        if (pElmPortEntry->u1ElmiMode == ELM_CUSTOMER_SIDE)
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: Port Already configured as Customer edge \n");
            return SNMP_SUCCESS;
        }
        else
        {
            u1MsgType = ELM_UNI_CUSTOMER_ENABLE_MSG;
        }
    }
    else
    {
        if (pElmPortEntry->u1ElmiMode == ELM_NETWORK_SIDE)
        {
            ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                     "LW: Port Already Configured as Network edge \n");
            return SNMP_SUCCESS;
        }
        else
        {
            u1MsgType = ELM_UNI_NETWORK_ENABLE_MSG;
        }
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));

        return SNMP_FAILURE;
    }
    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;
    pMsgNode->Msg.u1ElmUniMode = (UINT1) i4SetValFsElmiUniSide;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiStatusCounter
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiStatusCounter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiStatusCounter (INT4 i4FsElmiPort, INT4 i4SetValFsElmiStatusCounter)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));
        return SNMP_FAILURE;
    }

    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    u1MsgType = ELM_CONFIGURE_STATUS_COUNTER_MSG;
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;
    pMsgNode->Msg.u1StatusCounterValue = (UINT1) i4SetValFsElmiStatusCounter;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pMsgNode);
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiPollingVerificationTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiPollingVerificationTimerValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiPollingVerificationTimerValue (INT4 i4FsElmiPort,
                                           INT4
                                           i4SetValFsElmiPollingVerificationTimerValue)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));
        return SNMP_FAILURE;
    }

    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    u1MsgType = ELM_CONFIGURE_PVT_VALUE_MSG;
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;
    pMsgNode->Msg.u1PvtValue =
        (UINT1) i4SetValFsElmiPollingVerificationTimerValue;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pMsgNode);
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiPollingTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiPollingTimerValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiPollingTimerValue (INT4 i4FsElmiPort,
                               INT4 i4SetValFsElmiPollingTimerValue)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));
        return SNMP_FAILURE;
    }
    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    u1MsgType = ELM_CONFIGURE_PT_VALUE_MSG;
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;
    pMsgNode->Msg.u1PtValue = (UINT1) i4SetValFsElmiPollingTimerValue;
    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pMsgNode);
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiPollingCounterValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                setValFsElmiPollingCounterValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiPollingCounterValue (INT4 i4FsElmiPort,
                                 INT4 i4SetValFsElmiPollingCounterValue)
{
    tElmMsgNode        *pMsgNode = NULL;
    UINT1               u1MsgType = (UINT1) ELM_INIT_VAL;

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if (ELM_ALLOC_LOCALMSG_MEM_BLOCK (pMsgNode) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INC_MEM_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Local Message Memory Allocation Fail!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "LW: Local Message Memory Allocation Fail!"));
        return SNMP_FAILURE;
    }

    ELM_MEMSET (pMsgNode, (UINT1) ELM_INIT_VAL, sizeof (tElmMsgNode));
    u1MsgType = ELM_CONFIGURE_POLLING_COUNTER_MSG;
    pMsgNode->u4IfIndex = (UINT4) i4FsElmiPort;
    pMsgNode->MsgType = u1MsgType;
    pMsgNode->Msg.u2PollingCounterValue =
        (UINT2) i4SetValFsElmiPollingCounterValue;

    if (ElmProcessSnmpRequest (pMsgNode) != ELM_SUCCESS)
    {
        ELM_RELEASE_LOCALMSG_MEM_BLOCK (pMsgNode);
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElmiPortElmiStatus
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiPortElmiStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiPortElmiStatus (UINT4 *pu4ErrorCode, INT4 i4FsElmiPort,
                               INT4 i4TestValFsElmiPortElmiStatus)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ELM_IS_ELMI_ENABLED () == (UINT1) ELM_DISABLED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "LW: ELMI Module is not Enabled Globally !\n");
	CLI_SET_ERR(CLI_ELMI_NO_GLOB_MOD_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
	CLI_SET_ERR(CLI_ELMI_NO_PORT_ERR);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsElmiPortElmiStatus != ELM_PORT_ENABLED) &&
        (i4TestValFsElmiPortElmiStatus != ELM_PORT_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiUniSide
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiUniSide
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiUniSide (UINT4 *pu4ErrorCode, INT4 i4FsElmiPort,
                        INT4 i4TestValFsElmiUniSide)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsElmiUniSide != ELM_CUSTOMER_SIDE) &&
        (i4TestValFsElmiUniSide != ELM_NETWORK_SIDE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiStatusCounter
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiStatusCounter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiStatusCounter (UINT4 *pu4ErrorCode, INT4 i4FsElmiPort,
                              INT4 i4TestValFsElmiStatusCounter)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValFsElmiStatusCounter < ELM_STATUS_COUNTER_MIN_VALUE) ||
        (i4TestValFsElmiStatusCounter > ELM_STATUS_COUNTER_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiPollingVerificationTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiPollingVerificationTimerValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiPollingVerificationTimerValue (UINT4 *pu4ErrorCode,
                                              INT4 i4FsElmiPort,
                                              INT4
                                              i4TestValFsElmiPollingVerificationTimerValue)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsElmiPollingVerificationTimerValue < ELM_PVT_MIN_VALUE) ||
        (i4TestValFsElmiPollingVerificationTimerValue > ELM_PVT_MAX_VALUE))
    {
        if (i4TestValFsElmiPollingVerificationTimerValue != ELM_INIT_VAL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (INT1) SNMP_FAILURE;
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC, "PVT Disabled\n");
        }
    }

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_NETWORK_SIDE)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: Cannot Configure Timer, Port configured as Customer Edge\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiPollingTimerValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiPollingTimerValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiPollingTimerValue (UINT4 *pu4ErrorCode, INT4 i4FsElmiPort,
                                  INT4 i4TestValFsElmiPollingTimerValue)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsElmiPollingTimerValue < (INT4) ELM_PT_MIN_VALUE) ||
        (i4TestValFsElmiPollingTimerValue > (INT4) ELM_PT_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "ERR: Cannot configure Polling Timer for Port in Network Mode\n");
        return (INT1) SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiPollingCounterValue
 Input       :  The Indices
                FsElmiPort

                The Object 
                testValFsElmiPollingCounterValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiPollingCounterValue (UINT4 *pu4ErrorCode, INT4 i4FsElmiPort,
                                    INT4 i4TestValFsElmiPollingCounterValue)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPort) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValFsElmiPollingCounterValue <
         (INT4) ELM_POLLING_COUNTER_MIN_VALUE)
        || (i4TestValFsElmiPollingCounterValue >
            (INT4) ELM_POLLING_COUNTER_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ELM_GET_PORT_MODE ((UINT4) i4FsElmiPort) != ELM_CUSTOMER_SIDE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "ERR:Cannot Configure Pollling Counter for Port in  Network mode\n");
        return (INT1) SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElmiPortTable
 Input       :  The Indices
                FsElmiPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElmiSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                retValFsElmiSetGlobalTrapOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiSetGlobalTrapOption (INT4 *pi4RetValFsElmiSetGlobalTrapOption)
{
    *pi4RetValFsElmiSetGlobalTrapOption = ELM_GLOBAL_TRAP;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiSetTraps
 Input       :  The Indices

                The Object 
                retValFsElmiSetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiSetTraps (INT4 *pi4RetValFsElmiSetTraps)
{
    *pi4RetValFsElmiSetTraps = ELM_TRAP_TYPE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiErrTrapType
 Input       :  The Indices

                The Object 
                retValFsElmiErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiErrTrapType (INT4 *pi4RetValFsElmiErrTrapType)
{
    *pi4RetValFsElmiErrTrapType = ELM_ERR_TRAP;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElmiSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                setValFsElmiSetGlobalTrapOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiSetGlobalTrapOption (INT4 i4SetValFsElmiSetGlobalTrapOption)
{
    ELM_GLOBAL_TRAP = (UINT4) i4SetValFsElmiSetGlobalTrapOption;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElmiSetTraps
 Input       :  The Indices

                The Object 
                setValFsElmiSetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElmiSetTraps (INT4 i4SetValFsElmiSetTraps)
{
    ELM_TRAP_TYPE = (UINT4) i4SetValFsElmiSetTraps;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElmiSetGlobalTrapOption
 Input       :  The Indices

                The Object 
                testValFsElmiSetGlobalTrapOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsElmiSetGlobalTrapOption)
{
    if ((i4TestValFsElmiSetGlobalTrapOption < 0) ||
        (i4TestValFsElmiSetGlobalTrapOption > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElmiSetTraps
 Input       :  The Indices

                The Object 
                testValFsElmiSetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElmiSetTraps (UINT4 *pu4ErrorCode, INT4 i4TestValFsElmiSetTraps)
{
    if ((i4TestValFsElmiSetTraps < 0)
        || (i4TestValFsElmiSetTraps > ELM_TRAP_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElmiSetGlobalTrapOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiSetGlobalTrapOption (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElmiSetTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElmiSetTraps (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElmiPortTrapNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElmiPortTrapNotificationTable
 Input       :  The Indices
                FsElmiPortTrapIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElmiPortTrapNotificationTable (INT4
                                                         i4FsElmiPortTrapIndex)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "ELMI System Control is shutdown!\n");
        return SNMP_FAILURE;
    }

    if (ElmSnmpLowValidatePortIndex (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, "Port Does Not Exist\n");
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElmiPortTrapNotificationTable
 Input       :  The Indices
                FsElmiPortTrapIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElmiPortTrapNotificationTable (INT4 *pi4FsElmiPortTrapIndex)
{
    return nmhGetNextIndexFsElmiPortTrapNotificationTable
        (0, pi4FsElmiPortTrapIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElmiPortTrapNotificationTable
 Input       :  The Indices
                FsElmiPortTrapIndex
                nextFsElmiPortTrapIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElmiPortTrapNotificationTable (INT4 i4FsElmiPortTrapIndex,
                                                INT4
                                                *pi4NextFsElmiPortTrapIndex)
{
    if (i4FsElmiPortTrapIndex < (INT4) ELM_INIT_VAL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (ElmSnmpLowGetNextValidIndex
        (i4FsElmiPortTrapIndex, pi4NextFsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElmiPvtExpired
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiPvtExpired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPvtExpired (INT4 i4FsElmiPortTrapIndex,
                        INT4 *pi4RetValFsElmiPvtExpired)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiPvtExpired);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiPtExpired
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiPtExpired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiPtExpired (INT4 i4FsElmiPortTrapIndex,
                       INT4 *pi4RetValFsElmiPtExpired)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiPtExpired);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiEvcStatus
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiEvcStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiEvcStatus (INT4 i4FsElmiPortTrapIndex,
                       INT4 *pi4RetValFsElmiEvcStatus)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiEvcStatus);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiUniStatus
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiUniStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiUniStatus (INT4 i4FsElmiPortTrapIndex,
                       INT4 *pi4RetValFsElmiUniStatus)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiUniStatus);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiEvcId
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiEvcId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiEvcId (INT4 i4FsElmiPortTrapIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValFsElmiEvcId)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pRetValFsElmiEvcId);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiErrType
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiErrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiErrType (INT4 i4FsElmiPortTrapIndex, INT4 *pi4RetValFsElmiErrType)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiErrType);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElmiOperStatusStatus
 Input       :  The Indices
                FsElmiPortTrapIndex

                The Object 
                retValFsElmiOperStatusStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElmiOperStatusStatus (INT4 i4FsElmiPortTrapIndex,
                              INT4 *pi4RetValFsElmiOperStatusStatus)
{
    if ((ELM_IS_ELMI_STARTED ()) == (UINT1) ELM_STOPPED)
    {
        ELM_TRC (ELM_MGMT_TRC | ELM_ALL_FAILURE_TRC,
                 "LW: ELMI Module is not Started\n");
        return SNMP_FAILURE;
    }
    if (ElmValidatePortEntry (i4FsElmiPortTrapIndex) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC, " Port Does Not exist!\n");
        return (INT1) SNMP_FAILURE;
    }

    ELM_UNUSED (pi4RetValFsElmiOperStatusStatus);
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  ElmiNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
ElmiNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fselmi, (sizeof (fselmi) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsElmiSystemControl,
                      (sizeof (FsElmiSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the elmi shutdown, with 
     * elmi oids and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MsrNotifyProtocolShutdown (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}
