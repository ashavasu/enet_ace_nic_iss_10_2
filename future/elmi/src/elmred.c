/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmred.c,v 1.25 2014/03/18 11:58:38 siva Exp $
 *
 * Description: This file contains routines to implement High Availability
 *              in ELMI Module
 *
 *******************************************************************/

#include "elminc.h"

tElmRedGlobalInfo   gElmRedGlobalInfo;

/*****************************************************************************/
/* Function Name      : ElmRedRmInit                                         */
/*                                                                           */
/* Description        : Initialise the RED Global variables for ELMI         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmRedRmInit (VOID)
{
    ELM_BULK_REQ_RECD () = ELM_FALSE;
    ELM_RM_GET_NODE_STATE () = RED_ELM_IDLE;
    ELM_NUM_STANDBY_NODES () = 0;
    gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_MIN_NUM_PORTS;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedRmInit\n");
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRestartModule                                     */
/*                                                                           */
/* Description        : This function will restart ELMI module.              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE.                           */
/*****************************************************************************/
INT4
ElmRestartModule (VOID)
{

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRestartModule\n");
    ELM_LOCK ();
    /* Shutdown ELMI module and then restart it */

    ElmHandleModuleShutdown ();

    if (ElmHandleModuleInit () != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Module Init Failed during restart..\n");
        ELM_UNLOCK ();
        return ELM_FAILURE;
    }
    ELM_UNLOCK ();
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSyncUpPerPortInfo                              */
/*                                                                           */
/* Description        : This function will send the Sync up information for a*/
/*                      particular port                                      */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE.                           */
/*****************************************************************************/
INT4
ElmRedSyncUpPerPortInfo (UINT4 u4PortIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    tElmTmrListId       TmrListId;
    tElmTimer          *pElmTimer = NULL;
    UINT4               u4TimerVal = ELM_INIT_VAL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedSyncUpPerPortInfo\n");
    /* The fetched pointer should not be NULL in this case.
     * The NULL check was present in the thread. If we receive NULL here,
     * Let a crash happen!
     */
    pElmPortEntry = ELM_GET_PORTENTRY (u4PortIndex);

    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "No Port Information available\n");
        return ELM_FAILURE;
    }
    if (ELM_RM_GET_NODE_STATE () != RED_ELM_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        if (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE)
        {
            pElmPortEntry->bElmRedBulkUpdInProgress = ELM_FALSE;
        }

        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        ElmRmHandleProtocolEvent (&ProtoEvt);

        return ELM_FAILURE;
    }

    if (ELM_IS_STANDBY_UP () == ELM_FALSE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, " Red sync up msgs can not be sent if"
                 " the standby node is down.\n");
        if (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE)
        {
            pElmPortEntry->bElmRedBulkUpdInProgress = ELM_FALSE;
        }
        /* Standby node is not present, so no need to send the
         * sync up message. */
        return ELM_SUCCESS;
    }

    u2BufSize = ELM_RED_SYNC_UP_MSG_SIZE;
    u2MsgLen = ELM_RED_SYNC_UP_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        ElmRmHandleProtocolEvent (&ProtoEvt);
        return (ELM_FAILURE);
    }
    if (pElmPortEntry->u1ElmiMode == ELM_CUSTOMER_SIDE)
    {
        TmrListId = ELM_TMR_LIST_ID;
        pElmTimer = pElmPortEntry->pPtTmr;
        if (pElmTimer != NULL)
        {
            if (ELM_GET_REMAINING_TIME (TmrListId, &(pElmTimer->ElmAppTimer),
                                        &u4TimerVal) == ELM_TMR_FAILURE)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_CONTROL_PATH_TRC,
                         " TMR: GetRemainingTime failed\n");
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                ElmRmHandleProtocolEvent (&ProtoEvt);
                RM_FREE (pMsg);
                return ELM_FAILURE;
            }
        }
    }
    /* Information common to both UNI-N and UNI-C */
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_RED_SYNC_UP_MSG);
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, pElmPortEntry->u4IfIndex);
    if ((pElmPortEntry->bElmStatusInformationChange == ELM_TRUE) &&
        (pElmPortEntry->bMsgExpected == ELM_TRUE))
    {
        /* Received Status message had some changed information and UNI-C
         * has requested an enquiry. This is the scenario in which the PDU
         * will be passed to the standby. Adjust the Send Seq No for this case
         */
        ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, pElmPortEntry->u1SendSeqCounter -
                           (UINT1) ELM_DECR_DATALEN_ONE);
    }
    else
    {
        ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, pElmPortEntry->u1SendSeqCounter);
    }
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, pElmPortEntry->u1RecvSeqCounter);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, pElmPortEntry->u4DataInstance);
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, pElmPortEntry->u1StatusCounter);
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, pElmPortEntry->u1ElmiOperStatus);
    /* Information specific to UNI-C */
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, pElmPortEntry->u2PollingCounter);
    if (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE)
    {
        ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_FULL_STATUS_CONTINUED);
        ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, ELM_INIT_VAL);
    }
    else
    {
        ELM_RM_PUT_1_BYTE (pMsg, &u4Offset,
                           pElmPortEntry->u1LastReportTypeTxed);
        if (pElmPortEntry->bElmStatusInformationChange == ELM_TRUE)
        {
            ELM_RM_PUT_2_BYTE (pMsg, &u4Offset,
                               pElmPortEntry->u2LastParsedEvcRefId);
            pElmPortEntry->u2LastParsedEvcRefId = ELM_INIT_VAL;
        }
        else
        {
            ELM_RM_PUT_2_BYTE (pMsg, &u4Offset,
                               pElmPortEntry->u2LastReceivedRefrenceID);
        }
    }
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimerVal);

    if (ElmRedSendMsgToRm (pMsg, u2BufSize) == ELM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        ElmRmHandleProtocolEvent (&ProtoEvt);
    }
    /* The dynamic Synchup message was sent to the standby. If this is due to 
     * a bulk request, LCM database also needs to be synched up. Form ELMI PDUs
     * and send them to the standby. This is required only for UNI-C side.
     */
    if (pElmPortEntry->u1ElmiMode == ELM_CUSTOMER_SIDE)
    {
        while (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE)
        {
            ElmPortTxStatusMessage (pElmPortEntry, ELM_RED_SYNCHUP_DATABASE,
                                    NULL);
        }
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSendBulkRequest                                */
/*                                                                           */
/* Description        : This function sends a bulk request to the active     */
/*                      node in the system.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE.                            */
/*****************************************************************************/
INT4
ElmRedSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ELM_RM_OFFSET;
    UINT2               u2BufSize = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedSendBulkRequest\n");
    u2BufSize = ELM_BULK_REQ_MSG_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        return ELM_FAILURE;
    }
    /* Fill the message type. */
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_BULK_REQ_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, ELM_BULK_REQ_MSG);

    ElmRedSendMsgToRm (pMsg, u2BufSize);

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Registers ELMI with RM by providing an application   */
/*                      ID for ELMI and a call back function to be called    */
/*                      whenever RM needs to send an event to ELMI           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then ELM_SUCCESS          */
/*                      Otherwise ELM_FAILURE                                */
/*****************************************************************************/
INT4
ElmRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedRegisterWithRM\n");
    RmRegParams.u4EntId = RM_ELMI_APP_ID;
    RmRegParams.pFnRcvPkt = ElmRedRcvPktFromRm;
    if (ElmRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ElmRedRegisterWithRM: Registration with RM FAILED\n");
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedDeRegisterWithRM                               */
/*                                                                           */
/* Description        : Deregisters ELMI with RM                             */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then ELM_SUCCESS        */
/*                      Otherwise ELM_FAILURE                                */
/*****************************************************************************/
INT4
ElmRedDeRegisterWithRM (VOID)
{
    if (ElmRmDeRegisterProtocols () == RM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ElmRedDeregisterWithRM: Deregistration with RM FAILED\n");
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSendMsgToRm                                    */
/*                                                                           */
/* Description        : This function constructs the RM message from the     */
/*                      given linear buf and sends it to Redundancy Manager. */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the RM input buffer.               */
/*                      u2BufSize  - Size of the given input buffer.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if sync msg is sent then ELM_SUCCESS                 */
/*                      Otherwise ELM_FAILURE                                */
/*****************************************************************************/
INT4
ElmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedSendMsgToRm\n");
    /* Call the API provided by RM to send the data to RM */
    if (ElmRmEnqMsgToRmFromAppl
        (pMsg, u2BufSize, RM_ELMI_APP_ID, RM_ELMI_APP_ID) == RM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Enq to RM from appl failed\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);

        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedRcvPktFromRm                                   */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the ELMI*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RM_FAILURE/RM_SUCCESS                                */
/*****************************************************************************/
INT4
ElmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tElmQMsg           *pQMsg = NULL;
    tElmRmCtrlMsg      *pCtrlMsg = NULL;
    INT4                i4RetVal = RM_SUCCESS;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedRcvPktFromRm\n");
    /* If RM_MESSAGE/RM_STANDBY_UP/RM_STANDBY_DOWN, pData cannot be NULL */
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent */
        return RM_FAILURE;
    }
    if (ELM_IS_ELMI_INITIALISED () == ELM_FALSE)
    {
        /* ELMI is not initialized so the Mempools and the Queue 
         * for the messages will not be present
         */
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    if (ELM_ALLOC_CFGQ_MSG_MEM_BLOCK (pQMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        ELM_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                 "STAP: Configuration Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Configuration Message Memory Block Allocation FAILED!!"));
        return RM_FAILURE;
    }
    if (ELM_ALLOC_CTRLMSG_MEM_BLOCK (pCtrlMsg) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmGlobalMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg);
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                        "STAP: Local Message Memory Block Allocation FAILED!!!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "STAP: Local Message Memory Block Allocation FAILED!!!"));
        return RM_FAILURE;
    }

    ELM_MEMSET (pCtrlMsg, ELM_MEMSET_VAL, sizeof (tElmRmCtrlMsg));
    ELM_MEMSET (pQMsg, ELM_MEMSET_VAL, sizeof (tElmQMsg));

    ELM_QMSG_TYPE (pQMsg) = ELM_RM_QMSG;

    pQMsg->uQMsg.pRmMsg = pCtrlMsg;
    pQMsg->uQMsg.pRmMsg->u1Event = u1Event;
    pQMsg->uQMsg.pRmMsg->pFrame = pData;
    pQMsg->uQMsg.pRmMsg->u2Length = u2DataLen;

    if (ELM_SEND_TO_QUEUE (ELM_CFG_QID, (UINT1 *) &pQMsg,
                           ELM_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC, "Message Successfully send to queue\n");
        if (ELM_SEND_EVENT (ELM_TASK_ID, ELM_MSG_EVENT) != OSIX_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Sending Bulk Update Event to ELM Task "
                     "FAILED!\n");
            i4RetVal = RM_FAILURE;
        }
        ELM_TRC (ELM_CONTROL_PATH_TRC, "Event Successfully Posted\n");
    }
    else
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "MGMT: Posting Message to Elm Queue FAILED!\n");
        i4RetVal = RM_FAILURE;
    }

    if (i4RetVal == RM_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        if (ELM_RELEASE_LOCALMSG_MEM_BLOCK (pCtrlMsg) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Release of Local Msg Memory Block FAILED!\n");
        }
        else
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "Release of Local Msg Memory Block SUCCESS!\n");
        }
        if (ELM_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                     "MGMT: Release of CFGQ Msg Memory Block FAILED!\n");
        }
        else
        {
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "Release of CFGQ Msg Memory Block SUCCESS!\n");
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ElmRedSendBulkUpdateTailMsg                          */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE.                           */
/*****************************************************************************/
INT4
ElmRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ELM_RM_OFFSET;
    UINT2               u2BufSize;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedSendBulkUpdateTailMsg\n");
    if (ELM_RM_GET_NODE_STATE () != RED_ELM_ACTIVE)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC,
                 " Node is not active. Bulk Update tail msg " "not sent.\n");
        return ELM_SUCCESS;
    }

    u2BufSize = ELM_BULK_UPD_TAIL_MSG_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        ElmRmHandleProtocolEvent (&ProtoEvt);
        return ELM_FAILURE;
    }

    /* Form a bulk update tail message. 

     *        <--------1 Byte--------><--------2 Bytes------>
     ******************************************************* 
     *        *                      *                     *
     * RM Hdr * ELM_BULK_UPD_TAIL_MSG * Msg Length         *
     *        *                      *                     * 
     *******************************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs. */
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, ELM_BULK_UPD_TAIL_MSG);
    if (ElmRedSendMsgToRm (pMsg, u2BufSize) == ELM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        ElmRmHandleProtocolEvent (&ProtoEvt);
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleBulkRequest                              */
/*                                                                           */
/* Description        : This function is invoked whenever ELMI module        */
/*                      requests for Bulk Update                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : Constructs Bulk Update                               */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleBulkRequest (VOID)
{

    tElmPortEntry      *pElmPortEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4PortIndex = ELM_INIT_VAL;
    UINT2               u2BulkUpdPortCnt = ELM_INIT_VAL;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    ELM_TRC (ELM_CONTROL_PATH_TRC, " Received Bulk Req\n");
    if ((ELM_SYSTEM_CONTROL == ELM_SHUTDOWN) ||
        (ELM_GLOBAL_STATUS == ELM_DISABLE_GLOBAL))
    {
        /* ELMI completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        ElmRmSetBulkUpdatesStatus (RM_ELMI_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        ElmRedSendBulkUpdateTailMsg ();

        return;
    }
    u2BulkUpdPortCnt = ELM_RED_NO_OF_PORTS_PER_SUB_UPDATE;
    for (u4PortIndex = gElmRedGlobalInfo.u4BulkUpdNextPort;
         u4PortIndex <= ELM_MAX_NUM_PORTS_SUPPORTED
         && u2BulkUpdPortCnt > ELM_INIT_VAL; u4PortIndex++)
    {
        /* Update u4BulkUpdNextPort, to resume the next sub bulk update from
         * where the previous sub bulk update left it out.
         */
        gElmRedGlobalInfo.u4BulkUpdNextPort++;
        pElmPortEntry = ELM_GET_PORTENTRY (u4PortIndex);
        if (pElmPortEntry == NULL)
        {
            continue;
        }
        pElmPortEntry->bElmRedBulkUpdInProgress = ELM_TRUE;
        if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
            (ELM_IS_STANDBY_UP () != ELM_FALSE))
        {
            ElmRedSyncUpPerPortInfo (u4PortIndex);
        }
        u2BulkUpdPortCnt--;
    }
    if (gElmRedGlobalInfo.u4BulkUpdNextPort <= ELM_MAX_NUM_PORTS_SUPPORTED)
    {
        /* Send an event to start the next sub bulk update.
         */
        if (ELM_SEND_EVENT (ELM_TASK_ID, ELM_RED_BULK_UPD_EVENT)
            != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            ElmRmHandleProtocolEvent (&ProtoEvt);
            ELM_TRC (ELM_ALL_FAILURE_TRC, "ElmRedHandleBulkUpdateEvent:"
                     " ELM_RED_BULK_UPD_EVENT SendEvent FAILED\n");
        }
    }
    else
    {
        /* ELMI completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        ElmRmSetBulkUpdatesStatus (RM_ELMI_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        ElmRedSendBulkUpdateTailMsg ();

        gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_INIT_VAL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleBulkUpdateEvent                          */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      ElmRedHandleBulkRequest is triggered.                */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleBulkUpdateEvent (VOID)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleBulkUpdateEvent\n");
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE)
    {
        ElmRedHandleBulkRequest ();
    }
    else
    {
        ELM_BULK_REQ_RECD () = ELM_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedMakeNodeActiveFromIdle                         */
/*                                                                           */
/* Description        : This function is invoked whenever the ELMI module is */
/*                      in Idle state and is requested to go to active state */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : ELMI Module Node status .                            */
/* Global Variables                                                          */
/* Modified           : ELMI Module Node status .                            */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedMakeNodeActiveFromIdle (VOID)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedMakeNodeActiveFromIdle\n");
    ELM_RM_GET_NODE_STATE () = RED_ELM_ACTIVE;
    if (ELM_IS_ELMI_STARTED () == ELM_STARTED)
    {
        ElmHandleElmiGlobalEnable ();
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedMakeNodeActiveFromStandby                      */
/*                                                                           */
/* Description        : This function is invoked whenever the ELMI module is */
/*                      in Standby Mode and is requested to go to ACTIVE     */
/*                      mode                                                 */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : ELMI Module Node Status.                             */
/* Global Variables                                                          */
/* Modified           : ELMI Module Node Status.                             */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedMakeNodeActiveFromStandby (VOID)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tTimeStamp          StoredTime = ELM_INIT_VAL;
    tTimeStamp          CurrentTime = ELM_INIT_VAL;
    UINT4               u4PortIndex = ELM_INIT_VAL;
    UINT2               u2Duration = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedMakeNodeActiveFromStandby\n");
    ELM_RM_GET_NODE_STATE () = RED_ELM_ACTIVE;
    if (ELM_IS_ELMI_STARTED () != ELM_STARTED)
    {
        /* ELMI is not started, there will not be any ports created */
        return;
    }
    /* Start Polling Timer for UNI-C ports ,for the remaining duration */
    for (u4PortIndex = ELM_MIN_NUM_PORTS;
         u4PortIndex <= ELM_MAX_NUM_PORTS_SUPPORTED; u4PortIndex++)
    {
        pElmPortEntry = ELM_GET_PORTENTRY (u4PortIndex);
        if (pElmPortEntry == NULL)
        {
            continue;
        }
        if ((ELM_IS_ELMI_ENABLED () == (UINT1) ELM_ENABLED) &&
            (pElmPortEntry->u1ElmiPortStatus == ELM_PORT_ELMI_ENABLED) &&
            (pElmPortEntry->u1PortStatus == ELM_PORT_OPER_UP))
        {
            if (pElmPortEntry->u1ElmiMode == ELM_CUSTOMER_SIDE)
            {
                StoredTime = pElmPortEntry->u4PollingTimerExpiryTime;
                ELM_GET_TIME_STAMP (&CurrentTime);
                if (StoredTime > CurrentTime)
                {
                    u2Duration = (UINT2) (StoredTime - CurrentTime) /
                        (UINT2) ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
                    if (u2Duration > (UINT2) ELM_INIT_VAL)
                    {
                        if (ElmStartTimer
                            ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PT,
                             u2Duration) != ELM_SUCCESS)
                        {
                            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                            "MSG: Polling Timer NOT Started \n");
                        }
                    }
                    else
                    {
                        ElmPtExpired (pElmPortEntry);
                    }
                }
                else
                {
                    /* The timer expiry time has crossed the current time.
                     * Send the timer expiry to UNI-C
                     */
                    ElmPtExpired (pElmPortEntry);
                }
            }
            else
            {
                StoredTime = pElmPortEntry->u4PollingVeriTimerExpiryTime;
                ELM_GET_TIME_STAMP (&CurrentTime);
                if (StoredTime > CurrentTime)
                {
                    u2Duration = (UINT2) (StoredTime - CurrentTime) /
                        (UINT2) ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
                    if (u2Duration > (UINT2) ELM_INIT_VAL)
                    {
                        if (ElmStartTimer
                            ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT,
                             u2Duration) != ELM_SUCCESS)
                        {
                            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                                            "MSG: Polling Verification Timer NOT Started \n");
                        }
                    }
                    else
                    {
                        ElmPvtExpired (pElmPortEntry->u4IfIndex);
                    }
                }
                else
                {
                    ElmPvtExpired (pElmPortEntry->u4IfIndex);
                }
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleGoActive                                 */
/*                                                                           */
/* Description        : This function is invoked whenever the ELM module is  */
/*                      given a GO_ACTIVE trigger                            */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : ELM Module Status .                                  */
/* Global Variables                                                          */
/* Modified           : ELM Module Status .                                  */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleGoActive (VOID)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleGoActive\n");
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_IDLE)
    {
        ElmRedMakeNodeActiveFromIdle ();
    }
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
    {
        ElmRedMakeNodeActiveFromStandby ();
    }

    ELM_NUM_STANDBY_NODES () = ElmRmGetStandbyNodeCount ();

    if (ELM_BULK_REQ_RECD () == ELM_TRUE)
    {
        ELM_BULK_REQ_RECD () = ELM_FALSE;
        gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_MIN_NUM_PORTS;
        ElmRedHandleBulkUpdateEvent ();
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleGoStandby                                */
/*                                                                           */
/* Description        : This function is invoked whenever the ELMI module is */
/*                      has got a GO_STANDBY indication                      */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : ELMI Module Status.                                  */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleGoStandby (VOID)
{
    UINT4               u4PortIndex = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleGoStandby\n");
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE)
    {
        ELM_RM_GET_NODE_STATE () = RED_ELM_FORCE_SWITCHOVER_INPROGRESS;
        ELM_NUM_STANDBY_NODES () = ELM_INIT_VAL;

        /* Stop the protocol timers running on the ports */
        for (u4PortIndex = ELM_MIN_NUM_PORTS;
             (u4PortIndex <= ELM_MAX_NUM_PORTS_SUPPORTED); u4PortIndex++)
        {
            ElmStopAllRunningTimers (u4PortIndex);
            ElmRedClearTxRxCounters (u4PortIndex);
        }
        if (ELM_IS_ELMI_ENABLED () != ELM_DISABLED)
        {
            if (ElmHandleElmiGlobalDisable () != ELM_SUCCESS)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ElmHandleElmiGlobalDisable returned FAILURE!!\n");
            }
            if (ElmHandleElmiGlobalEnable () != ELM_SUCCESS)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ElmHandleElmiGlobalDisable returned FAILURE!!\n");
            }
        }
        ELM_RM_GET_NODE_STATE () = RED_ELM_STANDBY;
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedClearTxRxCounters                              */
/*                                                                           */
/* Description        : This function is invoked whenever the ELMI module    */
/*                      has got a GO_STANDBY indication                     */
/* Input(s)           : u4IfIndex                                            */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
ElmRedClearTxRxCounters (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_STARTED)
    {
        return;
    }

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return;
    }

    if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) == ELM_NETWORK_SIDE)
    {
        pElmPortEntry->u4ElmiTxElmiCheckMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiTxFullStatusMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiTxFullStatusContdMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiTxAsyncStatusMsg = ELM_INIT_VAL;

        pElmPortEntry->u4ElmiValidMsgRxed = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxElmiCheckEnquiryMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxFullStatusEnquiryMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxFullStatusContdEnquiryMsg = ELM_INIT_VAL;
    }
    else if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) == ELM_CUSTOMER_SIDE)
    {
        pElmPortEntry->u4ElmiTxElmiCheckEnquiryMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiTxFullStatEnquiryMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiTxFullStatContdEnquiryMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxCheckMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxFullStatMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxFullStatContdMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiRxAsyncStatusMsg = ELM_INIT_VAL;
        pElmPortEntry->u4ElmiValidMsgRxed = ELM_INIT_VAL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleRestoreComplete                          */
/*                                                                           */
/* Description        : This function is invoked whenever the ELMI module    */
/*                      has got a CONFIG_RESTORE_COMPLETE indication         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : ELMI Module Status.                                  */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleRestoreComplete (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleRestoreComplete\n");
    if (ELM_RM_GET_NODE_STATE () == RED_ELM_IDLE)
    {
        if (ElmRmGetNodeState () == RM_STANDBY)
        {
            /* Recieved GO_STANDBY event earlier */
            ELM_RM_GET_NODE_STATE () = RED_ELM_STANDBY;
            ElmRedMakeNodeStandbyFromIdle ();
        }

        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        ElmRmHandleProtocolEvent (&ProtoEvt);
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedTrigHigherLayer                                */
/*                                                                           */
/* Description        : This function will send the given event to the next  */
/*                      higher layer (can be STP or VLAN).                   */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type. L2_INITIATE_BULK_UPDATES  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedTrigHigherLayer (UINT1 u1TrigType)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedTrigHigherLayer\n");
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    ElmSnoopRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
#ifdef LLDP_WANTED
    ElmLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        ((u1TrigType == RM_COMPLETE_SYNC_UP) ||
         (u1TrigType == L2_COMPLETE_SYNC_UP)))
    {
        /* Send an event to RM to notify that sync up is completed */
        if (ElmRmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "ELMI send event L2_SYNC_UP_COMPLETED to " "RM failed\n");
        }
    }
#endif
#endif
}

/*****************************************************************************/
/* Function Name      : ElmProcessRmEvent                                    */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmProcessRmEvent (tElmRmCtrlMsg * pMsg)
{
    tElmRedStates       ElmPrevNodeState = RED_ELM_IDLE;
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmProcessRmEvent\n");
    switch (pMsg->u1Event)
    {
        case GO_ACTIVE:
            ELM_TRC (ELM_CONTROL_PATH_TRC, "Received GO_ACTIVE EVENT\n");

            if (ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE)
            {
                break;
            }
            ElmPrevNodeState = ELM_RM_GET_NODE_STATE ();
            ElmRedHandleGoActive ();
            ELM_NUM_STANDBY_NODES () = ElmRmGetStandbyNodeCount ();

            if (ELM_BULK_REQ_RECD () == ELM_TRUE)
            {
                ELM_BULK_REQ_RECD () = ELM_FALSE;
                gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_MIN_NUM_PORTS;
                ElmRedHandleBulkUpdateEvent ();
            }

            if (ElmPrevNodeState == RED_ELM_IDLE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            ElmRmHandleProtocolEvent (&ProtoEvt);
            break;

        case GO_STANDBY:
            if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
            {
                break;
            }
            ElmRedHandleGoStandby ();
            ELM_BULK_REQ_RECD () = ELM_FALSE;
            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            ElmRmHandleProtocolEvent (&ProtoEvt);
            break;

        case RM_INIT_HW_AUDIT:
            /* HW Audit is implemented for ELMI */
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pMsg->pFrame;
            ELM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (ELM_BULK_REQ_RECD () == ELM_TRUE)
            {
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) && \
    !defined (RSTP_WANTED) && !defined (VLAN_WANTED) && !defined (ECFM_WANTED)
                ELM_BULK_REQ_RECD () = ELM_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_MIN_NUM_PORTS;
                ElmRedHandleBulkUpdateEvent ();
#endif
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsg->pFrame;
            ELM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            ElmRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->pFrame, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->pFrame, pMsg->u2Length);

            ProtoAck.u4AppId = RM_ELMI_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (ELM_RM_GET_NODE_STATE () != RED_ELM_IDLE)
            {
                ElmRedHandleSyncUpMessage (pMsg);
            }
            /* Processing of message is over, hence free the RM message. */
            RM_FREE (pMsg->pFrame);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);

            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            ElmRedHandleRestoreComplete ();
            break;

        case L2_INITIATE_BULK_UPDATES:
            if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
            {
                ElmRedSendBulkRequest ();
            }
            break;

        case RM_COMPLETE_SYNC_UP:
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED)&& \
    !defined (RSTP_WANTED) && !defined (VLAN_WANTED) && !defined (ECFM_WANED)
            ElmRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
#else
            /* Ignoring this Event */
            ELM_GLOBAL_TRC (ELM_ALL_FAILURE_TRC,
                            "Ignoring RM_COMPLETE_SYNC_UP if lower layers are"
                            "present \n");
#endif
            break;

        case L2_COMPLETE_SYNC_UP:
            ELM_TRC (ELM_CONTROL_PATH_TRC, "RECEIVED L2_COMPLETE_SYNC_UP\n");
            ElmRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : ElmRedMakeNodeStandbyFromIdle.                       */
/*                                                                           */
/* Description        : This function makes the node Standby from Idle state */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS                                          */
/*****************************************************************************/
INT4
ElmRedMakeNodeStandbyFromIdle (VOID)
{
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedMakeNodeStandbyFromIdle\n");
    if (ELM_IS_ELMI_STARTED () != ELM_STARTED)
    {
        ELM_INIT_NUM_PEERS ();
        ELM_TRC (ELM_CONTROL_PATH_TRC,
                 "ELM Node made standby and ELM is not started.\n");
        return ELM_SUCCESS;
    }

    ELM_INIT_NUM_PEERS ();

    /* Note: Even when the node is in Idle state, configurations
     * can be made. Framework has to take care of it.
     * Hence check the protocol admin status and enable/disable 
     * the ELMI module accordingly.
     */
    if (ELM_IS_ELMI_ENABLED () != ELM_DISABLED)
    {
        /* According to configurations ELM module is enabled,
         * hence enable ELM here. 
         */
        ELM_TRC (ELM_CONTROL_PATH_TRC, "Enabling ELMI Globally\n");
        ElmHandleElmiGlobalEnable ();
    }
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ELM Node made STANDBY from IDLE state.\n");
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSyncUpDataInstance.                            */
/*                                                                           */
/* Description        : This function calls for synching up of DI of active  */
/*                      and standby when DI changes at the active UNI-N Side */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port entry.           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS/ELM_FAILURE                              */
/*****************************************************************************/
INT4
ElmRedSyncUpDataInstance (tElmPortEntry * pElmPortEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ELM_RM_OFFSET;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedSyncUpDataInstance\n");
    u2BufSize = ELM_DATA_INSTANCE_SYNC_MSG_SIZE;
    u2MsgLen = ELM_DATA_INSTANCE_SYNC_MSG_LEN;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        return ELM_FAILURE;
    }
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "Sending DI = %d\n", pElmPortEntry->u4DataInstance);

    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_DATA_INSTANCE_SYNC_MSG);
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, pElmPortEntry->u4IfIndex);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, pElmPortEntry->u4DataInstance);

    ElmRedSendMsgToRm (pMsg, u2BufSize);

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleDiSynchUpMsg                             */
/*                                                                           */
/* Description        : This function is invoked when DI synch up message is */
/*                      received by the standby node.                        */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                      pu4Offset - Offset in the message received           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleDiSynchUpMsg (tRmMsg * pData, UINT4 *pu4Offset)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT4               u4IfIndex = ELM_INIT_VAL;
    UINT4               u4DataInstance = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleDiSynchUpMsg\n");
    if (ELM_RM_GET_NODE_STATE () != RED_ELM_STANDBY)
    {
        /* This node is not a standby node
         * Dont process the message. Return from here.
         */
        return;
    }
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4IfIndex);
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return;
    }
    if (pElmPortEntry->u1ElmiMode == ELM_CUSTOMER_SIDE)
    {
        /* DI synch happens only at Network Side.
         * The customer side will receive the updated DI in the
         * next ELMI Check Status PDU
         */
        return;
    }
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4DataInstance);
    pElmPortEntry->u4DataInstance = u4DataInstance;
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "RCVD DI = %d\n", pElmPortEntry->u4DataInstance);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandlePortSynchUpMsg                           */
/*                                                                           */
/* Description        : This function is invoked when a port synchup PDU is  */
/*                      received by the standby node                         */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                      pu4Offset - Offset in the message received           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandlePortSynchUpMsg (tRmMsg * pData, UINT4 *pu4Offset)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tTimeStamp          TimeStamp;
    UINT4               u4IfIndex = ELM_INIT_VAL;
    UINT4               u4DataInstance = ELM_INIT_VAL;
    UINT4               u4PollingTimerVal = ELM_INIT_VAL;
    UINT2               u2PollingCounter = ELM_INIT_VAL;
    UINT2               u2LastReceivedReferenceId = ELM_INIT_VAL;
    UINT1               u1LastReportTypeTxed = ELM_INIT_VAL;
    UINT1               u1SendSeqCounter = ELM_INIT_VAL;
    UINT1               u1RecvSeqCounter = ELM_INIT_VAL;
    UINT1               u1StatusCounter = ELM_INIT_VAL;
    UINT1               u1ElmiOperStatus = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandlePortSynchUpMsg\n");
    if (ELM_RM_GET_NODE_STATE () != RED_ELM_STANDBY)
    {
        /* This node is not a standby node
         * Dont process the message. Return from here.
         */
        ELM_TRC (ELM_CONTROL_PATH_TRC, "Not a Standby Node. Return\n");
        return;
    }
    /* Parse the Message to fetch the values */
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4IfIndex);

    if (ELM_PORT_TBL () == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC, "Port table is NULL. Return\n");
        return;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return;
    }
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1SendSeqCounter);
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1RecvSeqCounter);
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4DataInstance);
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1StatusCounter);
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1ElmiOperStatus);
    ELM_RM_GET_2_BYTE (pData, pu4Offset, u2PollingCounter);
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1LastReportTypeTxed);
    ELM_RM_GET_2_BYTE (pData, pu4Offset, u2LastReceivedReferenceId);
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4PollingTimerVal);
    /* Synch Up the database */
    pElmPortEntry->u1SendSeqCounter = u1SendSeqCounter;
    pElmPortEntry->u1RecvSeqCounter = u1RecvSeqCounter;
    pElmPortEntry->u4DataInstance = u4DataInstance;
    pElmPortEntry->u1StatusCounter = u1StatusCounter;
    pElmPortEntry->u1ElmiOperStatus = u1ElmiOperStatus;
    pElmPortEntry->u2PollingCounter = u2PollingCounter;
    pElmPortEntry->u1LastReportTypeTxed = u1LastReportTypeTxed;
    pElmPortEntry->u2LastReceivedRefrenceID = u2LastReceivedReferenceId;
    ELM_GET_TIME_STAMP (&TimeStamp);

    /* Synch up the timers information */
    /* Store the remaining timer value of Polling Timer.
     * First clear the value stored earlier.*/
    pElmPortEntry->u4PollingTimerExpiryTime = ELM_INIT_VAL;
    if (u4PollingTimerVal == ELM_INIT_VAL)
    {
        /* The received Polling Timer value is ZERO. The synchup PDU was sent when
         * PT expired. Since PT always restarts on expiry, store the next expiry time
         * after the configured timer value
         */
        pElmPortEntry->u4PollingTimerExpiryTime =
            pElmPortEntry->u1PtValueConfigured *
            ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    }
    else
    {
        pElmPortEntry->u4PollingTimerExpiryTime = u4PollingTimerVal;
    }
    ELM_ADD_TIME_STAMP (pElmPortEntry->u4PollingTimerExpiryTime, TimeStamp);
    /* Polling Verification timer is restarted immidiately after sending 
     * status message. The status message is sent whenever an enquiry is received.
     * Synch up the value of PVT
     */
    pElmPortEntry->u4PollingVeriTimerExpiryTime =
        pElmPortEntry->u1PvtValueConfigured *
        ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    ELM_ADD_TIME_STAMP (pElmPortEntry->u4PollingVeriTimerExpiryTime, TimeStamp);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleSyncUpMessage                            */
/*                                                                           */
/* Description        : This function is invoked whenever ELMI module        */
/*                      receives a sync up request                           */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedHandleSyncUpMessage (tElmRmCtrlMsg * pMsg)
{
    tElmPortEntry      *pPortEntry = NULL;
    VOID               *pData = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT4               u4PortIndex = ELM_INIT_VAL;
    UINT2               u2Len = ELM_INIT_VAL;
    UINT1               u1MessageType = ELM_INIT_VAL;
    UINT1               u1PortStatus = ELM_INIT_VAL;
    UINT1               u1ProtVersion = ELM_INIT_VAL;
    UINT1               u1Type = ELM_INIT_VAL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedHandleSyncUpMessage\n");
    pData = (VOID *) pMsg->pFrame;
    ELM_RM_GET_1_BYTE ((tRmMsg *) pData, &u4Offset, u1MessageType);
    /* Length of the Individual Message */
    ELM_RM_GET_2_BYTE ((tRmMsg *) pData, &u4Offset, u2Len);

    switch (u1MessageType)
    {
        case ELM_DATA_INSTANCE_SYNC_MSG:
            if (u2Len != ELM_DATA_INSTANCE_SYNC_MSG_LEN)
            {
                ELM_TRC (ELM_CONTROL_PATH_TRC,
                         "Incorrect Message Length Received\n");
                break;
            }
            ELM_TRC (ELM_CONTROL_PATH_TRC, "Received DI Synch Up msg. \n");
            ElmRedHandleDiSynchUpMsg (pData, &u4Offset);
            break;

        case ELM_RED_SYNC_UP_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC, "Received Synch Up msg. \n");
            if (u2Len == ELM_RED_SYNC_UP_MSG_LEN)
            {
                ElmRedHandlePortSynchUpMsg (pData, &u4Offset);
            }
            else
            {
                /* Incorrect message length for this message type
                 * Discard the Packet and do not process further
                 */
                ELM_TRC (ELM_CONTROL_PATH_TRC,
                         " Incorrect message Length Received\n");
            }
            break;

        case ELM_BULK_REQ_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC, "Received Bulk Request Message\n");
            if (ELM_IS_STANDBY_UP () == ELM_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                ELM_BULK_REQ_RECD () = ELM_TRUE;
                break;
            }

            ELM_BULK_REQ_RECD () = ELM_FALSE;
            /* On recieving ELM_BULK_REQ_MSG, Bulk updation process should be
             * restarted.
             */
            gElmRedGlobalInfo.u4BulkUpdNextPort = ELM_MIN_NUM_PORTS;
            ElmRedHandleBulkUpdateEvent ();
            break;

        case ELM_BULK_UPD_TAIL_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC, "Received Bulk Update tail msg. \n");
            /* On receiving ELM_BULK_UPD_TAIL_MSG, give indication to RM
             * RM gives the trigger to higher layers
             * (STP/VLAN) inorder to send the Bulk Req msg. \n");
             */
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ElmRmHandleProtocolEvent (&ProtoEvt);
            break;

        case ELM_PORT_OPER_STATUS_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "Received Port Oper Status Change msg. \n");
            ELM_RM_GET_4_BYTE ((tRmMsg *) pData, &u4Offset, u4PortIndex);
            ELM_RM_GET_1_BYTE ((tRmMsg *) pData, &u4Offset, u1PortStatus);
            ElmUpdatePortStatus (u4PortIndex, u1PortStatus);
            break;

        case ELM_RED_LCM_DATABASE_SYNCHUP_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "RM:Received LCM Database Synchup PDU.\n");
            /* These messages are received as part of Bulk Updates.
             * The Bulk update that starts here will end after the parsing of
             * a Full Status Message.
             */
            pu1Pdu = gElmGlobalInfo.gu1ElmPdu;
            ELM_MEMSET (pu1Pdu, ELM_INIT_VAL, ELM_MAX_ETH_FRAME_SIZE);
            ELM_RM_GET_N_BYTE ((tRmMsg *) pData, pu1Pdu, &u4Offset, u2Len);
            ELM_GET_4BYTE (u4PortIndex, pu1Pdu);

            pPortEntry = ELM_GET_PORTENTRY (u4PortIndex);
            if (pPortEntry == NULL)
            {
                ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                         "No Port Information present!!!\n");
                return;
            }
            pPortEntry->bElmRedBulkUpdInProgress = ELM_TRUE;
            /* Standby never sends any request so it will discard these messages. 
             * Simulating the situation that Standby had requested for message.
             */
            pPortEntry->bMsgExpected = ELM_TRUE;
            ELM_GET_1BYTE (u1ProtVersion, pu1Pdu);
            ELM_GET_1BYTE (u1Type, pu1Pdu);
            if (ElmHandleCustomerTlv (pu1Pdu, u4PortIndex,
                                      (u2Len -
                                       (ELM_INTERFACE_INDEX_SIZE +
                                        ELM_PROTO_MSG_TYPE_SIZE)))
                != ELM_SUCCESS)
            {
                /* The received PDU was not parsed correctly. The remaining PDUs
                 * will be discarded due to this and the database will not be in synch.
                 * Request again for bulk update
                 */
                ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                         "ElmHandleCustomerTLV failed!!!\n");
                ProtoEvt.u4Event = RM_INITIATE_BULK_UPDATE;
                ElmRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case ELM_RED_CHANGED_STATUS_SYNCHUP:
            ElmRedHandleChangedStatus (pData, &u4Offset);
            break;

        default:
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                          "Unexpected Message Type: %d received\n",
                          u1MessageType);
            break;
    }
    UNUSED_PARAM (u1ProtVersion);
    UNUSED_PARAM (u1Type);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedOperStatusChangeIndication                     */
/*                                                                           */
/* Description        : This function is invoked whenever port's operational */
/*                      status changes on an active node                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index of a port                */
/*                      u1Status - Port Status                               */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
ElmRedOperStatusChangeIndication (UINT4 u4IfIndex, UINT1 u1Status)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ELM_RM_OFFSET;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedOperStatusChangeIndication\n");
    u2BufSize = ELM_PORT_OPER_STATUS_MSG_SIZE;
    u2MsgLen = ELM_PORT_OPER_STATUS_MSG_LEN;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting 
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        return ELM_FAILURE;
    }

    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_PORT_OPER_STATUS_MSG);
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1Status);

    ElmRedSendMsgToRm (pMsg, u2BufSize);

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRedSynchupStatusMsg                               */
/*                                                                           */
/* Description        : This function is called whenever Asynchronous Status,*/
/*                      Full Status or Full Status Contd PDUs are required   */
/*                      to be transmitted to the standby node.               */
/*                      This is required so that the LCM database at the     */
/*                      standby node comes in synch with that present at the */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : pu1RedBuf - Pointer to the received PDU              */
/*                      u4IfIndex - Interface Index of a port                */
/*                      u2DataLength - Length of the received PDU            */
/*                      u1MsgType - Message Type                             */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElmRedSynchupStatusMsg (UINT4 u4DataInstance, UINT4 u4IfIndex,
                        UINT2 u2DataLength, UINT1 u1MsgType, UINT1 *pu1RedBuf)
{
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;

    /* Encapsulate the received Status message in RM Frame */
    u2BufSize = u2DataLength + (UINT2) ELM_RED_STATUS_MSG_HEADER_SIZE;
    u2MsgLen = u2BufSize - (UINT2) ELM_RED_MSG_TYPE_SIZE;
    if ((pBuf = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR: RM Allocation Failed\n");
        return;
    }

    ELM_RM_PUT_1_BYTE (pBuf, &u4Offset, ELM_RED_CHANGED_STATUS_SYNCHUP);
    ELM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (pBuf, &u4Offset, u4IfIndex);
    /* Form the RM PDU and send it */
    ELM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MsgType);
    ELM_RM_PUT_2_BYTE (pBuf, &u4Offset,
                       (u2DataLength + ELM_DATA_INSTANCE_LENGTH));
    /* Data Instance is significant only for Full Status Message */
    ELM_RM_PUT_4_BYTE (pBuf, &u4Offset, u4DataInstance);
    ELM_RM_PUT_N_BYTE (pBuf, pu1RedBuf, &u4Offset, u2DataLength);

    ElmRedSendMsgToRm (pBuf, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedFillHeader                                     */
/*                                                                           */
/* Description        : Forms the RM header                                  */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                      ppBuf - Pointer to the allocated buffer              */
/*                      u2DataLength - Length of the PDU                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillHeader (tElmPortEntry * pElmPortEntry, tRmMsg ** ppBuf,
                  UINT2 u2DataLength)
{

    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedFillHeader\n");
    u2MsgLen = u2DataLength - (UINT2) ELM_HEADER_SIZE +
        (UINT2) ELM_INTERFACE_INDEX_SIZE;

    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_RED_LCM_DATABASE_SYNCHUP_MSG);
    ELM_RM_PUT_2_BYTE (*ppBuf, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (*ppBuf, &u4Offset, pElmPortEntry->u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedFillFixedTlv                                   */
/*                                                                           */
/* Description        : This function fills the fixed TLV for RM message     */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                      ppBuf - Pointer to the allocated buffer              */
/*                      u1ReportType - Report Type for the transmitted PDU   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillFixedTlv (tElmPortEntry * pElmPortEntry, tRmMsg ** ppBuf,
                    UINT1 u1ReportType)
{
    UINT4               u4Offset = ELM_INIT_VAL;

    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedFillFixedTlv\n");
    u4Offset = ELM_RED_LCM_SYNCH_HEADER_SIZE;
    ELM_RED_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);

    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_PROTOCOL_VERSION);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_PDU_TYPE_STATUS);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_REPORT_TYPE_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_REPORT_TYPE_LENGTH);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, u1ReportType);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_SEQUENCE_NUMBER_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_SEQUENCE_NUMBER_LENGTH);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset,
                       pElmPortEntry->u1ElmRedSendSeqCounter);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_INIT_VAL);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_DATA_INSTANCE_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_DATA_INSTANCE_IE_LENGTH);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_INIT_VAL);
    ELM_RM_PUT_4_BYTE (*ppBuf, &u4Offset, pElmPortEntry->u4DataInstance);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmFillUniStatusIe                                   */
/*                                                                           */
/* Description        : This function is called to fill the UNI Status IE.   */
/*                                                                           */
/* Input(s)           : CfaUniInfo - The CFA Port Entry structure            */
/*                      ppBuf - Pointer to the allocated RM buffer           */
/*                      u1UniStatusIeLength - Precalculated length of UNI    */
/*                      Status IE                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedFillUniStatusIe (tCfaUniInfo CfaUniInfo, tRmMsg ** ppBuf,
                       UINT1 u1UniStatusIeLength)
{
    tCfaUniBwProfile    BwProfile;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT1               u1BandwidthThirdByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1UniIdLength = (UINT1) ELM_INIT_VAL;

    u4Offset = ELM_RED_LCM_SYNCH_HEADER_SIZE + ELM_FIXED_TLV_SIZE;
    ELM_TRC (ELM_CONTROL_PATH_TRC, "ElmRedFillUniStatusIe\n");
    BwProfile = CfaUniInfo.UniBwProfile;
    u1UniIdLength = u1UniStatusIeLength - (UINT1) ELM_UNI_FIXED_IE_LENGTH;

    if (BwProfile.u1Cf != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        (UINT1) ELM_MASK_SECOND_BIT);
    }
    if (BwProfile.u1Cm != (UINT1) ELM_INIT_VAL)
    {
        u1BandwidthThirdByte = (UINT1) (u1BandwidthThirdByte |
                                        (UINT1) ELM_MASK_THIRD_BIT);
    }
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_UNI_STATUS_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset,
                       (u1UniStatusIeLength - (UINT1) ELM_PROTO_MSG_TYPE_SIZE));
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, CfaUniInfo.u1CeVlanMapType);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_UNI_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, u1UniIdLength);
    ELM_RM_PUT_N_BYTE (*ppBuf, &(CfaUniInfo.au1UniName), &u4Offset,
                       u1UniIdLength);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_BW_PROFILE_IE);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, ELM_BW_PROFILE_IE_LENGTH);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, u1BandwidthThirdByte);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1CirMagnitude);
    ELM_RM_PUT_2_BYTE (*ppBuf, &u4Offset, BwProfile.u2CirMultiplier);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1CbsMagnitude);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1CbsMultiplier);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1EirMagnitude);
    ELM_RM_PUT_2_BYTE (*ppBuf, &u4Offset, BwProfile.u2EirMultiplier);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1EbsMagnitude);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1EbsMultiplier);
    ELM_RM_PUT_1_BYTE (*ppBuf, &u4Offset, BwProfile.u1PriorityBits);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedPassCeVlanInfo                                 */
/*                                                                           */
/* Description        : This function is called when the active indicates    */
/*                      that CE-VLAN info has changed and thus the standby   */
/*                      should update its database                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      pLcmCeVlanInfo - Pointer to the Changed CE Vlan Info */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedPassCeVlanInfo (UINT4 u4IfIndex, tLcmEvcCeVlanInfo * pLcmCeVlanInfo)
{
    tRmMsg             *pMsg = NULL;
    tElmPortEntry      *pPortEntry = NULL;
    UINT4               u4Offset = ELM_INIT_VAL;
    UINT2               u2BufSize = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;
    UINT2               u2SpaceForVlans = ELM_INIT_VAL;
    UINT2               u2CeVlanMsgLen = ELM_INIT_VAL;
    UINT2               u2VlanId = ELM_INIT_VAL;
    UINT1               u1CeVlanInfoFifthByte = ELM_INIT_VAL;
    BOOL1               bResult = OSIX_FALSE;

    pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);

    if (pPortEntry == NULL)
    {
        return;
    }

    u2SpaceForVlans =
        (UINT2) ((pPortEntry->u2NoOfVlansMapped) *
                 (UINT2) ELM_SINGLE_VLAN_SIZE);
    u2BufSize =
        (UINT2) ELM_RED_HEADER_SIZE + (UINT2) ELM_RED_CE_VLAN_INFO_FIXED_SIZE +
        u2SpaceForVlans;
    u2MsgLen = u2BufSize - (UINT2) ELM_RED_MSG_TYPE_SIZE;
    u2CeVlanMsgLen =
        u2SpaceForVlans + (UINT2) ELM_RED_CE_VLAN_INFO_FIXED_SIZE -
        (UINT2) ELM_RED_MSG_TYPE_SIZE;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting 
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        return;
    }

    if (pLcmCeVlanInfo->u1UntagPriTag == ELM_TRUE)
    {
        u1CeVlanInfoFifthByte = (UINT1) (u1CeVlanInfoFifthByte |
                                         ELM_MASK_SECOND_BIT);
    }
    if (pLcmCeVlanInfo->u1DefaultEvc == ELM_TRUE)
    {
        u1CeVlanInfoFifthByte = (UINT1) (u1CeVlanInfoFifthByte |
                                         ELM_MASK_FIRST_BIT);
    }
    /* Fill the RM Common Header */
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_RED_CHANGED_STATUS_SYNCHUP);
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    ELM_RM_PUT_4_BYTE (pMsg, &u4Offset, u4IfIndex);
    /* Fill the CE Vlan Info */
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, ELM_RED_CE_VLAN_INFO_MSG);
    ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2CeVlanMsgLen);
    ELM_RM_PUT_1_BYTE (pMsg, &u4Offset, u1CeVlanInfoFifthByte);

    for (u2VlanId = pLcmCeVlanInfo->u2FirstVlanId;
         u2VlanId <= pLcmCeVlanInfo->u2LastVlanId; u2VlanId++)
    {
        OSIX_BITLIST_IS_BIT_SET (pLcmCeVlanInfo->CeVlanMapInfo, u2VlanId,
                                 LCM_CE_VLAN_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            ELM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2VlanId);
        }
    }

    /* Send the formed message */
    ElmRedSendMsgToRm (pMsg, u2BufSize);
    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleCeVlanInfo                               */
/*                                                                           */
/* Description        : This functions parses the CE Vlan info passed to     */
/*                      standby frpm active. After parsing, the information  */
/*                      is passed to LCM for database updation.              */
/*                                                                           */
/* Input(s)           : pData - Pointer to the received message              */
/*                      pu4Offset - Offset for the message                   */
/*                      u4IfIndex - Port index                               */
/*                      u2MsgLen - Length of the message to be parsed        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedHandleCeVlanInfo (UINT1 *pu1Pdu, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tLcmEvcCeVlanInfo  *pLcmEvcCeVlanInfo = NULL;
    UINT2               u2CeVlanId = ELM_INIT_VAL;
    UINT1               u1DefaultEvc = (UINT1) ELM_INIT_VAL;
    UINT1               u1UntagPriTag = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanIeFifthByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanSubInfoLength = ELM_INIT_VAL;

    if (ELM_ALLOC_LCM_EVC_VLAN_INFO (pLcmEvcCeVlanInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "pLcmCeVlanInfo:Memory Allocation failed\n");
        return;
    }

    ELM_MEMSET (pLcmEvcCeVlanInfo, ELM_INIT_VAL, sizeof (tLcmEvcCeVlanInfo));
    u1CeVlanSubInfoLength = u2DataLength - (UINT2) ELM_RED_MSG_TYPE_LENGTH_SIZE;

    ELM_GET_2BYTE ((pLcmEvcCeVlanInfo->u2EvcReferenceId), pu1Pdu);
    ELM_GET_1BYTE (u1CeVlanIeFifthByte, pu1Pdu);
    u1DefaultEvc = u1CeVlanIeFifthByte;
    u1DefaultEvc = u1DefaultEvc & (UINT1) ELM_DEFAULT_EVC_FINDER;
    u1UntagPriTag = u1CeVlanIeFifthByte & (UINT1) ELM_TAGGED_BIT_FINDER;

    while (u1CeVlanSubInfoLength != (UINT1) ELM_INIT_VAL)
    {
        ELM_GET_2BYTE (u2CeVlanId, pu1Pdu);
        pLcmEvcCeVlanInfo->u2NoOfVlansMapped =
            pLcmEvcCeVlanInfo->u2NoOfVlansMapped + (UINT2) ELM_INCR_DATALEN_ONE;
        OSIX_BITLIST_SET_BIT (pLcmEvcCeVlanInfo->CeVlanMapInfo, u2CeVlanId,
                              LCM_CE_VLAN_SIZE);
        if (pLcmEvcCeVlanInfo->u2FirstVlanId == (UINT1) ELM_INIT_VAL)
        {
            pLcmEvcCeVlanInfo->u2FirstVlanId = u2CeVlanId;
        }
        pLcmEvcCeVlanInfo->u2LastVlanId = u2CeVlanId;
        u1CeVlanSubInfoLength =
            u1CeVlanSubInfoLength - (UINT1) ELM_SINGLE_VLAN_SIZE;
    }
    pLcmEvcCeVlanInfo->u1DefaultEvc = u1DefaultEvc;
    pLcmEvcCeVlanInfo->u1UntagPriTag = u1UntagPriTag;

    /* Update the received information to LCM for updation */
    ElmPassCeVlanInfo (u4IfIndex, pLcmEvcCeVlanInfo);
    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmEvcCeVlanInfo);

    return;
}

/*****************************************************************************/
/* Function Name      : ElmRedHandleChangedStatus                            */
/*                                                                           */
/* Description        : This function handles the PDUs from Active which have*/
/*                      the changed LCM database information.                */
/* Input(s)           : pData - Pointer to the received PDU                  */
/*                      pu4Offset - Offset in the message received           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmRedHandleChangedStatus (tRmMsg * pData, UINT4 *pu4Offset)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT4               u4IfIndex = ELM_INIT_VAL;
    UINT4               u4DataInstance = ELM_INIT_VAL;
    UINT2               u2MsgLen = ELM_INIT_VAL;
    UINT2               u2DataLength = ELM_INIT_VAL;
    UINT1               u1MsgType = ELM_INIT_VAL;
    UINT1              *pu1Pdu = NULL;

    if (ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE)
    {
        /* This message is not for active to parse */
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR: Active received changed status message\n");
        return;
    }
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4IfIndex);
    ELM_RM_GET_1_BYTE (pData, pu4Offset, u1MsgType);
    ELM_RM_GET_2_BYTE (pData, pu4Offset, u2MsgLen);
    ELM_RM_GET_4_BYTE (pData, pu4Offset, u4DataInstance);

    u2DataLength = u2MsgLen - (UINT2) ELM_DATA_INSTANCE_LENGTH;
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR: No Port Information present\n");
        return;
    }
    pu1Pdu = gElmGlobalInfo.gu1ElmPdu;
    ELM_MEMSET (pu1Pdu, ELM_INIT_VAL, ELM_MAX_ETH_FRAME_SIZE);
    ELM_RM_GET_N_BYTE (pData, pu1Pdu, pu4Offset, u2DataLength);
    switch (u1MsgType)
    {

        case ELM_RED_ASYNCH_STATUS_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "RM:Received Asynchronous Status Message.\n");
            ElmRcvdAsynStatus (pu1Pdu, u4IfIndex, u2DataLength);
            break;

        case ELM_RED_FULL_STATUS_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "RM:Received Full Status Message.\n");
            ElmRcvdFullStatus (pu1Pdu, u4IfIndex, u2DataLength);
            pElmPortEntry->u4DataInstance = u4DataInstance;
            pElmPortEntry->u2LastReceivedRefrenceID = (UINT1) ELM_INIT_VAL;
            pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
            pElmPortEntry->u1ElmCeTlvCheck = (UINT1) ELM_INIT_VAL;
            break;

        case ELM_RED_FULL_STATUS_CONTD_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "RM:Received Full Status Continued Message.\n");
            ElmRcvdFullStatusContinued (pu1Pdu, u4IfIndex, u2DataLength);
            break;

        case ELM_RED_CE_VLAN_INFO_MSG:
            ELM_TRC (ELM_CONTROL_PATH_TRC,
                     "RM:Received Changed CE VLAN Info.\n");
            ElmRedHandleCeVlanInfo (pu1Pdu, u4IfIndex, u2DataLength);
            break;

        default:
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "RM:Incorrect Status Change Message Received\n");
    }
    return;
}
