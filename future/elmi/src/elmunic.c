/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmunic.c,v 1.25 2014/03/18 11:58:38 siva Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              functionality specific to ELMI-UNI-C module
 *
 *******************************************************************/

#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmPtExpired                                         */
/*                                                                           */
/* Description        : This is the main routine for UNI-C in order to       */
/*                      transmit Enquiry Message.This function is            */
/*                      called whenever PT Timer Expired & Timer Expiry      */
/*                      Event is generated.                                  */
/*                                                                           */
/* Input(s)           : pElmPortEntry:-Pointer to Port Structure             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmPtExpired (tElmPortEntry * pElmPortEntry)
{
    ELM_TRC_FN_ENTRY ();

    /* Calculate the operational Status of ELMI When Ever Polling Timer Expired 
     */

    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC | ELM_TMR_TRC,
             "TMR:Pollling Timer is Expired,Polling Timer Expired Trap"
             "is generated.Operetional Status is calculated \n");
    ElmPtExpiredOperStatusCalc (pElmPortEntry);
    /* Genrate the PT Expired Trap 
     */
    ElmiPtExpiredTrap (pElmPortEntry->u4IfIndex, ELM_PORT_TRAPS_OID,
                       ELM_PORT_TRAPS_OID_LEN);
    /* PT expired abnormally and Full Status Enquiry Process has not 
     * completed due to some error to send the Full Sttaus Enquiry Message 
     * Again set polling couter to configured value.
     */
    if (pElmPortEntry->bMsgRcvdInTime == ELM_FALSE)
    {
        if ((pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS_CONTINUED)
            || (pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS))
        {
            pElmPortEntry->u2PollingCounter =
                pElmPortEntry->u2PollingCounterConfigured;

            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC | ELM_TMR_TRC,
                          "TMR:Polling Timer is expired abnormally . Polling counter"
                          "is set to Configured value of Polling counter ."
                          "current value is %d\n",
                          pElmPortEntry->u2PollingCounter);
        }
        /* Increment the value of Status Time Out Count Because UNI-C has not 
         * received Status Message or PT is expired abnormally.
         */

        ELM_INCR_REL_ERR_STATUS_TIME_OUT_COUNT (pElmPortEntry->u4IfIndex);
        /* Synchup with the standby */
        if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
            (ELM_IS_STANDBY_UP () != ELM_FALSE))
        {
            ElmRedSyncUpPerPortInfo (pElmPortEntry->u4IfIndex);
        }
    }
    /* when UNI-C transmit the Full status continued enquiry message & Full   
     * status is received & parsed completly then decrement the PC value by   
     * one.
     */
    else if (pElmPortEntry->bMsgRcvdInTime == ELM_TRUE)
    {
        /* Decrement the Polling counter Value when ever Full status or Full 
         * status continued  message is received and Pt expired normally.
         */

        if ((pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS_CONTINUED)
            || (pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS))
        {
            ELM_DECR_POLLING_COUNTER_VALUE (pElmPortEntry->u4IfIndex);
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC | ELM_TMR_TRC,
                          "TMR:Polling Timer is expired normally & the"
                          "current value of Polling counter is %d\n",
                          pElmPortEntry->u2PollingCounter);

        }
    }

    if (pElmPortEntry->u1LastReportTypeTxed == ELM_ELMI_CHECK)
    {
        /* Decrement the Polling counter Value when ever Elmi chck enquiry 
         * message is transmitted & PT expired normally or abnormally.
         */
        ELM_DECR_POLLING_COUNTER_VALUE (pElmPortEntry->u4IfIndex);
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                      "The current value of Polling counter is %d\n",
                      pElmPortEntry->u2PollingCounter);
    }

    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                  "The Current value of Polling counter is %d\n",
                  pElmPortEntry->u2PollingCounter);

    /*Transmit the Enquiry message */
    if (ElmPortTxEnquiryMessage (pElmPortEntry) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmPortTxEnquiryMessage function has returned FAILURE!\n");
        return ELM_FAILURE;
    }
    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
             "Status Enquiry Message is transmitted by UNI-C!\n");

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmPortTxEnquiryMessage                              */
/*                                                                           */
/* Description        : This function is called by ElmPtExpired function    */
/*                      whenever Polling timer expires.and Timer Expiry Event*/
/*                      is generated then UNI-C  UNI-C will send the Full    */
/*                      Status or ELMI Check ENQUIRY                         */
/*                      message depending upon the current value of PC.      */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmPortTxEnquiryMessage (tElmPortEntry * pElmPortEntry)
{
    UINT1               u1ReportType = (UINT1) ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();

    /*check wheter pElmPortEntry is NULL or not  */
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmPortTxEnquiryMessage Function rcecived argument"
                 "Event Invalid Port Pointer\n");
        return ELM_FAILURE;
    }
    if (ELM_IS_ELMI_ENABLED () == (UINT1) ELM_DISABLED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI Module is not enabled globally  so"
                 "ELMI PDU can not be transmitted \n");
        return ELM_FAILURE;
    }
    if (!ELM_IS_ELMI_ENABLED_ON_PORT (pElmPortEntry->u4IfIndex))
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI Module is not enabled on the Port so"
                 "ELMI PDU can not be transmitted \n");
        return ELM_FAILURE;
    }

    if (pElmPortEntry->u1ElmiPortStatus != ELM_PORT_ELMI_ENABLED ||
        pElmPortEntry->u1PortStatus != ELM_PORT_OPER_UP)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:Port is eithrer operationally down"
                 "or ELMI is disabled on port\n");

        return ELM_SUCCESS;
    }

    /* check the current value with configured Polling Counter value if both are 
     * equal send the Full status enquiry message else Elmi check enquiry 
     * message.
     */

    if (pElmPortEntry->u2PollingCounter ==
        pElmPortEntry->u2PollingCounterConfigured)
    {
        u1ReportType = (UINT1) ELM_FULL_STATUS;

        if (ElmPortTxElmiPdu (pElmPortEntry, u1ReportType) != ELM_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "ERR:ElmPortTxElmiPdu function has returned FAILURE!\n");
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ElmPortTxElmiPdu function has returned FAILURE!\n");
            return ELM_FAILURE;
        }
        /* Increment the Full Status Enquiry Message Count */
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                 "Full Status Enquiry Message is transmitted from Port!\n");

        ELM_INCR_TX_FULL_STATUS_ENQUIRY_COUNT (pElmPortEntry->u4IfIndex);
    }
    else
    {
        u1ReportType = (UINT1) ELM_ELMI_CHECK;

        if (ElmPortTxElmiPdu (pElmPortEntry, u1ReportType) != ELM_SUCCESS)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                     "ERR:ElmPortTxElmiPdu function has returned FAILURE!\n");
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ElmPortTxElmiPdu function has returned FAILURE!\n");
            return ELM_FAILURE;
        }

        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                 "ELMI Check Status Enquiry Message is transmitted from Port!\n");
        /* Increment the ELMI Check Enquiry Message Count */
        ELM_INCR_TX_ELMI_CHECK_ENQUIRY_COUNT (pElmPortEntry->u4IfIndex);
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmPortTxElmiPdu                                     */
/*                                                                           */
/* Description        : This function is called by ElmPortTxEnquiryMessage   */
/*                      Message function in order to transmit a Status       */
/*                      ENQUIRY Message.                                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      u1ReportType - Indicates the Report type of ELMI PDU */
/*                      to be formed                                         */
/*                              1.ELM_FULL_STATUS                            */
/*                              2.ELM_ELMI_CHECK                             */
/*                              3.ELM_FULL_STATUS_CONTINUED                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmPortTxElmiPdu (tElmPortEntry * pElmPortEntry, UINT1 u1ReportType)
{
    tElmBufChainHeader *pElmPdu = NULL;
    UINT4               u4Length = ELM_INIT_VAL;

    if (ELM_IS_ELMI_ENABLED () == (UINT1) ELM_DISABLED)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI Module is not enabled globally  so"
                 "ELMI PDU can not be transmitted \n");
        return ELM_FAILURE;
    }

    if (!ELM_IS_ELMI_ENABLED_ON_PORT (pElmPortEntry->u4IfIndex))
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI Module is DISABLED on the Port so"
                 "ELMI PDU can not be transmitted \n");
        return ELM_FAILURE;
    }
    if (ELM_RM_GET_NODE_STATE () != RED_ELM_ACTIVE)
    {
        /* The Enquiry messages should not be sent for the non-active node.
         * But still return success, so as not to affect the existing functionality
         */
        return ELM_SUCCESS;
    }

    /* Increment the value of Send Sequence Counter */
    ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE (pElmPortEntry->u4IfIndex);

    ELM_TRC_FN_ENTRY ();

    /* Fill the Ethernet Hedder of 14 Byte first 
     * 6 Bytes for destinatin Address,next 6 Bytes for source address and next 2 
     * Bytes for Ethernet Hedder.
     */
    if (ElmFillHeader (pElmPortEntry) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC |
                      ELM_BUFFER_TRC,
                      "Port %s: ElmFillHedder Function has returne Failure!."
                      "So UNI-C is Unable to transmit Status Enquiry Message\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                      " Port %s: ElmFillHedder Function Has returned Failure !."
                      "So Unable to transmit From this Port\n",
                      ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
        return ELM_FAILURE;
    }

    /* Form the ELMI Pdu depending upon report Type */
    if (ElmFormElmiPdu (pElmPortEntry, ELM_PDU_TYPE_STATUS_ENQUIRY,
                        u1ReportType, &u4Length) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 " ERR:ElmFormElmiPdu function returned FAILURE!\n");
        return ELM_FAILURE;
    }

    pElmPortEntry->u1LastReportTypeTxed = u1ReportType;
    /* Allocate the Memory for CRU Buffer */
    pElmPdu = ELM_ALLOC_CRU_BUF (u4Length, ELM_INIT_VAL);

    if (pElmPdu == NULL)
    {
        /* Incrementing the Buffer Failure Count */
        ELM_INCR_BUFFER_FAILURE_COUNT ();
        /*generate the Buffer Failure Trap */
        ElmBufferFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC | ELM_BUFFER_TRC,
                 "ERR:ELMI Pdu Allocation of CRU Buffer FAILED!\n");
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MEM_TRC,
                 "ERR:ELMI Pdu Allocation of CRU Buffer FAILED!\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gElmGlobalInfo.u4SysLogId,
                      "ERR:ELMI Pdu Allocation of CRU Buffer FAILED!"));
        return ELM_FAILURE;
    }
    /*Copy the ELMI Pdu to CRU Buffur */
    if (ELM_COPY_OVER_CRU_BUF (pElmPdu, gElmGlobalInfo.gu1ElmPdu,
                               0, u4Length) == ELM_CRU_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU Copy Over CRU Buffer FAILED!\n");
        ELM_RELEASE_CRU_BUF (pElmPdu, ELM_FALSE);
        return ELM_FAILURE;
    }

    ElmHandleOutgoingPktOnPort (pElmPdu, pElmPortEntry, u4Length);

    /* send packet on Port */
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  " Port %s: ELMI PDU is transmitted from Port >>>>>\n",
                  ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
    /*Start the PT */
    if (ElmStartTimer (pElmPortEntry, ELM_TMR_TYPE_PT,
                       pElmPortEntry->u1PtValueConfigured) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "ERR;Polling Timer is not started on this port \n");
        return ELM_FAILURE;
    }
    /* When UNI-C send the Enquiry Message set the bMsgExpected to True means  
     *  UNI-C is now expecting responce for this enquiry message & set
     *  bMsgRcvdInTime to ELM_FALSE.
     */
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                  "TMR:Current Value of Polling Timer is %d &"
                  " Polling Timer is started on the port \n",
                  pElmPortEntry->u1PtValueConfigured);

    pElmPortEntry->bMsgExpected = ELM_TRUE;
    pElmPortEntry->bMsgRcvdInTime = ELM_FALSE;

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmFillHeader                                        */
/*                                                                           */
/* Description        : This function Forms the ethernet headerfor ELMI PDU  */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port information.     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS (or) ELM_FAILURE                         */
/*****************************************************************************/
INT4
ElmFillHeader (tElmPortEntry * pElmPortEntry)
{
    UINT1               au1ElmiHeader[ELM_HEADER_SIZE];
    tCfaIfInfo          CfaIfInfo;
    INT4                i4Index = (UINT1) ELM_INIT_VAL;
    tMacAddr           *pSrcMacAddr = NULL;

    ELM_MEMSET (au1ElmiHeader, ELM_MEMSET_VAL, sizeof (au1ElmiHeader));
    /*First copy the 6 Bytes fixed Destination Address */
    au1ElmiHeader[0] = (UINT1) 0x01;
    au1ElmiHeader[1] = (UINT1) 0x80;
    au1ElmiHeader[2] = (UINT1) 0xC2;
    au1ElmiHeader[3] = (UINT1) 0x00;
    au1ElmiHeader[4] = (UINT1) 0x00;
    au1ElmiHeader[5] = (UINT1) 0x07;
    i4Index = 6;

    /*get the 6 Bytes Source Mac Address of the Port from CFA */
    if (CfaGetIfInfo (ELM_IFENTRY_IFINDEX (pElmPortEntry), &CfaIfInfo) !=
        CFA_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:CfaGetIfInfo call FAILED!\n");
        return ELM_FAILURE;
    }

    pSrcMacAddr = &CfaIfInfo.au1MacAddr;

    ELM_MEMCPY (&au1ElmiHeader[i4Index], pSrcMacAddr, ELM_MAC_ADDR_SIZE);
    i4Index = i4Index + ELM_MAC_ADDR_SIZE;
    au1ElmiHeader[i4Index] = (UINT1) ELM_IST_BYTE_ETHERNET_HEADER;
    i4Index = i4Index + 1;
    au1ElmiHeader[i4Index] = (UINT1) ELM_2ND_BYTE_ETHERNET_HEADER;
    ELM_MEMCPY (gElmGlobalInfo.gu1ElmPdu, au1ElmiHeader, ELM_HEADER_SIZE);

    return ELM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : ElmFormElmiPdu                                       */
/*                                                                           */
/* Description        : This function is called by ElmFormElmiPdu            */
/*                      Message function in                                  */
/*                      order to form a Full Status,Full Status Continued    */
/*                      & ELMI Check STATUS ENQUIRY Messages.                */
/*                                                                           */
/* Input(s)           : pElmPortEntry - The pointer to the Port Entry        */
/*                      u1ReportType - Indicates the Report type of ELMI PDU */
/*                         to be formed                                      */
/*                              1.ELM_FULL_STATUS                            */
/*                              2.ELM_ELMI_CHECK                             */
/*                              3.ELM_FULL_STATUS_CONTINUED                  */
/*                      pu4DataLength - The number of bytes that have been   */
/*                                      filled in the BPDU                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
ElmFormElmiPdu (tElmPortEntry * pElmPortEntry, UINT1 u1PduType,
                UINT1 u1ReportType, UINT4 *pu4DataLength)
{
    UINT1               u1ReserveValue = (UINT1) ELM_INIT_VAL;
    UINT1              *pu1PduStart = NULL;
    UINT1              *pu1PduEnd = NULL;
    UINT1               u1ElmPaddingLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1Reserved = (UINT1) ELM_INIT_VAL;
    UINT1               u1Count = (UINT1) ELM_INIT_VAL;

    pu1PduStart = gElmGlobalInfo.gu1ElmPdu;
    pu1PduEnd = gElmGlobalInfo.gu1ElmPdu;
    pu1PduEnd = pu1PduEnd + ELM_HEADER_SIZE;

    pElmPortEntry->u1LastReportTypeTxed = u1ReportType;
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_PROTOCOL_VERSION);
    ELM_PUT_1BYTE (pu1PduEnd, u1PduType);

    /*Fill the Report Type Information Element in Elmi Pdu */
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_REPORT_TYPE_IE);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_REPORT_TYPE_LENGTH);
    ELM_PUT_1BYTE (pu1PduEnd, u1ReportType);
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "The Report Type of ELMI Pdu Going to transmit is %d\n",
                  u1ReportType);

    /*Fill the Sequence Number Information Element in Elmi Pdu */
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_SEQUENCE_NUMBER_IE);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_SEQUENCE_NUMBER_LENGTH);
    ELM_PUT_1BYTE (pu1PduEnd, pElmPortEntry->u1SendSeqCounter);
    ELM_PUT_1BYTE (pu1PduEnd, pElmPortEntry->u1RecvSeqCounter);
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "The Send Sequence Number of ELMI Pdu Going to transmit is %d\n",
                  pElmPortEntry->u1SendSeqCounter);
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "The Receive Sequence Number of ELMI Pdu Going to transmit is %d\n",
                  pElmPortEntry->u1RecvSeqCounter);

    /*Fill the Data Instance Information Element in Elmi Pdu */
    ELM_PUT_1BYTE (pu1PduEnd, ELM_DATA_INSTANCE_IE);
    ELM_PUT_1BYTE (pu1PduEnd, (UINT1) ELM_DATA_INSTANCE_IE_LENGTH);
    ELM_PUT_1BYTE (pu1PduEnd, u1ReserveValue);
    ELM_PUT_4BYTE (pu1PduEnd, pElmPortEntry->u4DataInstance);
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC,
                  "The Data instance of ELMI Pdu Going to transmit is %d\n",
                  pElmPortEntry->u4DataInstance);

    *pu4DataLength = (UINT4) (pu1PduEnd - pu1PduStart);
    /*Find the Excat length of ELMI PDU by Subtracting start Pointer from End 
       Pointer */
    /*Put the 30 more Bytes to Elmi Pdu & Make it to 46 bytes Minimum */

    if (ELM_MIN_PDU_SIZE > (*pu4DataLength))
    {
        u1ElmPaddingLength =
            (UINT1) ELM_MIN_PDU_SIZE - (UINT1) (*pu4DataLength);
    }
    else
    {
        return ELM_SUCCESS;
    }

    for (u1Count = ELM_INIT_VAL; u1Count < u1ElmPaddingLength; u1Count++)
    {
        ELM_PUT_1BYTE (pu1PduEnd, u1Reserved);
    }
    *pu4DataLength = (UINT4) (pu1PduEnd - pu1PduStart);

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmPtExpiredOperStatusCalc                           */
/*                                                                           */
/* Description        : This function determines the operational status of   */
/*                      ELMI, when a PT is Expired      .                    */
/*                                                                           */
/* Input(s)           : pElmPortEntry - Pointer to the port entry structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmPtExpiredOperStatusCalc (tElmPortEntry * pElmPortEntry)
{

    ELM_TRC_FN_ENTRY ();
    if (pElmPortEntry->u1ElmiOperStatus == ELM_OPER_ENABLED)
    {
        if (pElmPortEntry->bMsgRcvdInTime == ELM_TRUE)
        {
            /* When ELMI is opertional, reset the Status Counter to N393 on
             * normal expiry of PT.
             */
            pElmPortEntry->u1StatusCounter =
                pElmPortEntry->u1StatusCounterConfigured;
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                     "The ELMI Operational Status is Enabled\n");
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                          "The Status Counter is set to Default/Configured"
                          "value.Current Value of Status Counter is %d \n",
                          pElmPortEntry->u1StatusCounter);

        }
        else
        {
            /* Decrement the Status Counter Value when Elmi Operation Status is 
             * enabled & PT expired abnormally.
             */
            ELM_DECR_STATUS_COUNTER (pElmPortEntry->u4IfIndex);
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                     "The ELMI Operational Status of ELMI is Enabled\n");
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                          "The Status Counter is Decremented by one"
                          "value. Current Value of Status Counter is %d \n",
                          pElmPortEntry->u1StatusCounter);

            if (pElmPortEntry->u1StatusCounter == (UINT1) ELM_INIT_VAL)
            {
                /* when Status Counter is Zero ELMI Operation Status set 
                 * Disabled.
                 */
                pElmPortEntry->u1ElmiOperStatus = ELM_OPER_DISABLED;
                pElmPortEntry->u1StatusCounter =
                    pElmPortEntry->u1StatusCounterConfigured;
                ElmElmiOperStatusIndication (pElmPortEntry->u4IfIndex,
                                             pElmPortEntry->u1ElmiOperStatus);
                ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                         "The ELMI Operational Status is Changed to Disabled\n");
                ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                              "The Status Counter is set to Default/Configured"
                              " value.Current Value of Status Counter is %d\n",
                              pElmPortEntry->u1StatusCounter);

                ElmiOperStatusChangedTrap (pElmPortEntry->u4IfIndex,
                                           ELM_OPER_STATUS_ENABLED,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
            }
        }
    }
    else
    {
        if (pElmPortEntry->bMsgRcvdInTime == ELM_TRUE)
        {
            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                     "The ELMI Operational Status is Disabled\n");

            ELM_DECR_STATUS_COUNTER (pElmPortEntry->u4IfIndex);
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                          "The Status Counter is Decremented by one "
                          "Current Value of Status Counter is %d\n",
                          pElmPortEntry->u1StatusCounter);

            if (pElmPortEntry->u1StatusCounter == (UINT1) ELM_INIT_VAL)
            {
                /* when Status Counter is Zero ELMI Operation Status set          
                 * Enabled & set Status counter to Configured value.
                 */
                pElmPortEntry->u1StatusCounter =
                    pElmPortEntry->u1StatusCounterConfigured;
                ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                              "The Status Counter is set to Default/Configured"
                              "value.Current Value of Status Counter is %d\n",
                              pElmPortEntry->u1StatusCounter);
                ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                         "The ELMI Operational Status is changed to ELMI Enabled\n");
                pElmPortEntry->u1ElmiOperStatus = ELM_OPER_ENABLED;
                ElmElmiOperStatusIndication (pElmPortEntry->u4IfIndex,
                                             pElmPortEntry->u1ElmiOperStatus);
                ElmiOperStatusChangedTrap (pElmPortEntry->u4IfIndex,
                                           ELM_OPER_STATUS_DISABLED,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
            }
        }
        else
        {
            /* Set the Status Counter to Configured value when PT expired       
             * abnormally & ELMI is not Operational. 
             */

            pElmPortEntry->u1StatusCounter =
                pElmPortEntry->u1StatusCounterConfigured;
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_MGMT_TRC,
                          "ELMI is operationally disabled ."
                          "The Current Value of Status Counter is set to configured value %d\n",
                          pElmPortEntry->u1StatusCounter);
        }
    }
}

/*****************************************************************************/
/* Function Name      : ElmHandleCustomerTlv                                 */
/*                                                                           */
/* Description        : This function handles the TLVs received at the UNI-C */
/*                      side.                                                */
/*                                                                           */
/* Input(s)           : pu1Buf - Pointer to the received PDU                 */
/*                      u4IfIndex - Port number at which PDU was received     */
/*                      u2DataLength- length od Data Packet                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmHandleCustomerTlv (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1              *pu1StartTlv = NULL;
    UINT1               u1ReportTypeId = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReportTypeLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReportTypeValue = (UINT1) ELM_INIT_VAL;
    UINT4               u4CurrentDataInstance = ELM_INIT_VAL;
    UINT1               u1UnExpReportLen = (UINT1) ELM_INIT_VAL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return ELM_FAILURE;
    }
    while (u2DataLength != ELM_INIT_VAL)
    {
        ELM_GET_1BYTE (u1ReportTypeId, pu1Buf);

        if (u1ReportTypeId != ELM_REPORT_TYPE_IE)
        {
            ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
            ELM_GET_1BYTE (u1UnExpReportLen, pu1Buf);
            if (ELM_DECR_DATA_LENGTH
                (u2DataLength, (UINT1) ELM_DECR_DATALEN_TWO) == ELM_FAILURE)
            {
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ELMI PDU receieved with invalid length\n");
                return ELM_FAILURE;
            }
            if (ELM_DECR_DATA_LENGTH (u2DataLength, u1UnExpReportLen)
                == ELM_FAILURE)
            {
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ELMI PDU receieved invalid length in TLV\n");
                return ELM_FAILURE;
            }
            pu1Buf = pu1Buf + u1UnExpReportLen;
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ELMI PDU receieved with uexpected IE \n");
            continue;
        }
        break;
    }
    ELM_GET_1BYTE (u1ReportTypeLength, pu1Buf);

    /* Validate the length of Report Type IE. */
    if (u1ReportTypeLength != ELM_REPORT_TYPE_LENGTH)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with error in Report Type IE Length\n");
        return ELM_FAILURE;
    }

    ELM_GET_1BYTE (u1ReportTypeValue, pu1Buf);
    pu1StartTlv = pu1Buf;

    /* Decrement the Data Length by 3.
     */
    if (ELM_DECR_DATA_LENGTH (u2DataLength, (UINT1) ELM_DECR_DATALEN_THREE) ==
        ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with invalid length!!!\n");
        return ELM_FAILURE;
    }

    if (u1ReportTypeValue != ELM_ASYNCHRONOUS_STATUS &&
        pElmPortEntry->bMsgExpected == ELM_FALSE)
    {
        /* ELMI-UNI was not waiting for any message also Message rxed is not a 
         * Asyncronous message.
         */
        ELM_INCR_RX_UNSOLICITED_STATUS_COUNT (u4IfIndex);
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT (u4IfIndex);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI UNI-C has received Unsolicited message\n");
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        return ELM_FAILURE;
    }

    switch (u1ReportTypeValue)
    {
        case ELM_ELMI_CHECK:

            ELM_TRC (ELM_MGMT_TRC, "ELMI Check Status Message is received\n");

            if (pElmPortEntry->u1LastReportTypeTxed ==
                ELM_FULL_STATUS_CONTINUED)
            {

                /* Receiced Packet must be discarded because UNI-C 
                 * was expecting the Full status Continued or Full 
                 * Status Message but UNI-C has received Elmi Check 
                 * Status Message.
                 */

                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT (u4IfIndex);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR: ELMI Check Status Message is received "
                         "in responce to Full Status Continued Enquiry.\n"
                         "Message discarded\n");
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                return ELM_FAILURE;
            }

            if (pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS)
            {

                /* Receiced Packet must be discarded because UNI-C 
                 * was expecting the Full status Continued or Full 
                 * Status Message but UNI-C has received Elmi Check 
                 * Status Message.
                 */
                ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT (u4IfIndex);
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR: ELMI Check Status Message is received "
                         "in responce to Full Status Enquiry. \n"
                         "Message discarded\n");
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                return ELM_FAILURE;
            }
            /* In Elmi Check Status Message two more Tlvs Sequence 
             * Number and Data Instance must be present.
             */
            if (ElmRcvdCheckStatus (pu1Buf, u4IfIndex, u2DataLength) !=
                ELM_SUCCESS)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ElmRcvdCheckStatus function has returned failure\n");
                return ELM_FAILURE;
            }

            ELM_TRC (ELM_MGMT_TRC,
                     "ELM:Elmi Check status message has been parsed successfully\n");
            break;

        case ELM_ASYNCHRONOUS_STATUS:

            ELM_TRC (ELM_MGMT_TRC,
                     "Single EVC Asyncronous Status Message is received\n");
            /* Single Evc Asynchronous Status Messgae contains only Evc staus IE
             */

            if (ElmRcvdAsynStatus (pu1Buf, u4IfIndex, u2DataLength) !=
                ELM_SUCCESS)
            {
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ElmRcvdAsynStatus function has returned"
                         "failure Message\n");
                return ELM_FAILURE;
            }
            else
            {
                /* The received Asynchronous status should be passed to the standby for
                 * LCM Update.
                 */
                if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                    (ELM_IS_STANDBY_UP () != ELM_FALSE))
                {
                    ElmRedSynchupStatusMsg (ELM_INIT_VAL, u4IfIndex,
                                            u2DataLength,
                                            ELM_RED_ASYNCH_STATUS_MSG, pu1Buf);
                }
            }

            ELM_TRC (ELM_MGMT_TRC,
                     "Single EVC Asynchronous message has been parsed successfully\n");
            break;

        case ELM_FULL_STATUS:

            ELM_TRC (ELM_MGMT_TRC,
                     "ELMI Full Status Message is received on Port \n");
            /* when the Fulll Status Message is parsed completly then local DI will 
             * be updated with Value of DI received in the packet.
             */
            pu1StartTlv = pu1StartTlv + (UINT1) ELM_INCR_BUFF_SEVEN;
            ELM_GET_4BYTE (u4CurrentDataInstance, pu1StartTlv);

            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                          "The value of Data Instance in received  UNI-C is  %d\n",
                          u4CurrentDataInstance);
            if (pElmPortEntry->u1LastReportTypeTxed == ELM_ELMI_CHECK)
            {

                /* Receiced ELMI PDU must be discarded because UNI-C 
                 * was expecting Elmi Check Status Message but 
                 * received Full status Message.
                 */
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT (u4IfIndex);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ELM: Full status Message is received & ignored"
                         "in responce to Elmi check Enquiry Message\n");
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                return ELM_FAILURE;
            }

            /* Full Status Message contains 5 more TLVs which will be parse here.
             */
            if (ElmRcvdFullStatus (pu1Buf, u4IfIndex, u2DataLength) !=
                ELM_SUCCESS)
            {

                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ElmRcvdFullStatus function has returned failure"
                         "Message\n");
                pElmPortEntry->u2PollingCounter =
                    pElmPortEntry->u2PollingCounterConfigured;
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                pElmPortEntry->u2LastReceivedRefrenceID = (UINT1) ELM_INIT_VAL;
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                pElmPortEntry->u1ElmCeTlvCheck = (UINT1) ELM_INIT_VAL;
                return ELM_FAILURE;
            }
            else
            {
                /* The received FULL status should be passed to the standby for
                 * LCM Update.
                 */
                if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                    (ELM_IS_STANDBY_UP () != ELM_FALSE))
                {
                    /* Send the synhup information. If the information in the
                     * PDU has changed the complete PDU will be required to be sent
                     */
                    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                        (ELM_IS_STANDBY_UP () != ELM_FALSE))
                    {
                        ElmRedSyncUpPerPortInfo (pElmPortEntry->u4IfIndex);
                    }
                    if ((pElmPortEntry->bElmStatusInformationChange ==
                         ELM_TRUE))
                    {
                        ELM_TRC (ELM_CONTROL_PATH_TRC,
                                 "Received PDU has some new information. Pass it to standby\n");
                        ElmRedSynchupStatusMsg (u4CurrentDataInstance,
                                                u4IfIndex, u2DataLength,
                                                ELM_RED_FULL_STATUS_MSG,
                                                pu1Buf);
                        pElmPortEntry->bElmStatusInformationChange = ELM_FALSE;
                    }
                }
            }

            pElmPortEntry->u2LastReceivedRefrenceID = (UINT1) ELM_INIT_VAL;
            pElmPortEntry->u4DataInstance = u4CurrentDataInstance;
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                          "The value of Data Instance at UNI-C is  %d\n",
                          pElmPortEntry->u4DataInstance);
            ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                          "The value of Data Instance in received  UNI-C is  %d\n",
                          u4CurrentDataInstance);
            pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
            pElmPortEntry->u1ElmCeTlvCheck = (UINT1) ELM_INIT_VAL;
            pElmPortEntry->u1ElmRedRecvSeqCounter = (UINT1) ELM_INIT_VAL;
            ELM_TRC (ELM_MGMT_TRC,
                     "ELMI Full Status message has been parsed successfully\n");
            break;

        case ELM_FULL_STATUS_CONTINUED:

            ELM_TRC (ELM_MGMT_TRC,
                     "Full Status Continued Message is received\n");
            if (pElmPortEntry->u1LastReportTypeTxed == ELM_ELMI_CHECK)
            {

                /* Receiced ELMI PDU must be discarded because UNI-C 
                 * was expecting Elmi Check Status Message but 
                 * received Full status continued Status Message 
                 */
                pElmPortEntry->u2LastReceivedRefrenceID = (UINT1) ELM_INIT_VAL;
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT (u4IfIndex);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:Full status continued Status Message is"
                         "received in responce to Elmi check Enquiry Message"
                         " Message Discarded\n");
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                return ELM_FAILURE;
            }

            if (ElmRcvdFullStatusContinued (pu1Buf, u4IfIndex, u2DataLength)
                != ELM_SUCCESS)
            {
                pElmPortEntry->u2LastReceivedRefrenceID = (UINT1) ELM_INIT_VAL;
                pElmPortEntry->u2LastCeVlanEvcRefId = (UINT1) ELM_INIT_VAL;
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ElmRcvdFullStatusContd  function has"
                         "returned failure Message\n");
                pElmPortEntry->u2PollingCounter =
                    pElmPortEntry->u2PollingCounterConfigured;
                pElmPortEntry->u1ElmCeTlvCheck = (UINT1) ELM_INIT_VAL;
                return ELM_FAILURE;
            }
            else
            {
                if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                    (ELM_IS_STANDBY_UP () != ELM_FALSE))
                {
                    /* Send the synhup information. If the information in the
                     * PDU has changed the complete PDU will be required to be sent
                     */
                    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                        (ELM_IS_STANDBY_UP () != ELM_FALSE))
                    {
                        ElmRedSyncUpPerPortInfo (pElmPortEntry->u4IfIndex);
                    }
                    if (pElmPortEntry->bElmStatusInformationChange == ELM_TRUE)
                    {
                        ElmRedSynchupStatusMsg (ELM_INIT_VAL, u4IfIndex,
                                                u2DataLength,
                                                ELM_RED_FULL_STATUS_CONTD_MSG,
                                                pu1Buf);
                        pElmPortEntry->bElmStatusInformationChange = ELM_FALSE;
                    }
                }
            }
            ELM_TRC (ELM_MGMT_TRC,
                     "Full Status continued message is received & parsed completely\n");
            break;

        default:

            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ELMI PDU receieved has an invalid report type IE !!!\n");
            return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdCheckStatus                                   */
/*                                                                           */
/* Description        : This function handles the received E-LMI-PDU when the*/
/*                      received report type is E-LMI Check Status Message.  */
/*                      This routine helps in exchange and update of sequence*/
/*                      numbers & data instance.                             */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port at which the PDU is receieved        */
/*                      u2DataLength - Length of the remaaning PDU           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdCheckStatus (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               au1ElmiCheckTLV[NUM_OF_MANDATORY_TLV_ELMI_CHECK_MSG] =
        { 0x02, 0x03 };
    UINT1               u1NumOfMandatoryTlv = (UINT1) ELM_INIT_VAL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present\n");
        return ELM_FAILURE;
    }
    u1NumOfMandatoryTlv = NUM_OF_MANDATORY_TLV_ELMI_CHECK_MSG;

    /* validate the sequence number ane Data Instance IE received in ELMI PDU
     */
    if (ElmParseRcvdTLV (pu1Buf, u4IfIndex, u2DataLength, au1ElmiCheckTLV,
                         u1NumOfMandatoryTlv, ELM_ELMI_CHECK) == ELM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseRcvdTlv function has returned Failure\n");
        return ELM_FAILURE;
    }
    /* Synchup with the standby */
    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
        (ELM_IS_STANDBY_UP () != ELM_FALSE))
    {
        ElmRedSyncUpPerPortInfo (pElmPortEntry->u4IfIndex);
    }
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    ELM_INCR_RX_VALID_ELMI_CHECK_STATUS_COUNT (u4IfIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdAsynStatus                                    */
/*                                                                           */
/* Description        : This function handles the received ELMI PDU when the */
/*                      received report type is Single EVC Asyncronous Status*/
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port number                               */
/*                      u2DataLength - Length of the remaaning PDU           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdAsynStatus (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    UINT1               au1AsyncStatusTLV[NUM_OF_MANDATORY_TLV_ASYNC_STATUS_MSG]
        = { 0x21 };
    UINT1               u1NumOfMandatoryTlv = (UINT1) ELM_INIT_VAL;

    u1NumOfMandatoryTlv = NUM_OF_MANDATORY_TLV_ASYNC_STATUS_MSG;

    if (ElmParseRcvdTLV (pu1Buf, u4IfIndex, u2DataLength, au1AsyncStatusTLV,
                         u1NumOfMandatoryTlv, ELM_ASYNCHRONOUS_STATUS) ==
        ELM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseRcvdTlv function has returned Failure!!!\n");
        return ELM_FAILURE;
    }
    /* Asynchronous status Message is received and Parsed 
     * properly so Increment the No of valid message received 
     * count & no of received Asynchronous status Message 
     * count.
     */
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    ELM_INCR_RX_VALID_ASYN_STATUS_COUNT (u4IfIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdFullStatusContinued                           */
/*                                                                           */
/* Description        : This function handles the received ELMI PDU when the */
/*                      received report type is Full Status Continued        */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to ELMI -PDU                       */
/*                      u4IfIndex - Port number                               */
/*                      u2DataLength - Length of the remaaning PDU           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdFullStatusContinued (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1
         
         
         
         
         
         
         
        au1FullStatusContTlv[NUM_OF_MANDATORY_TLV_FULL_STATUS_CONTINUED_MSG] =
        { 0x02, 0x03, 0x21, 0x22 };
    UINT1               u1NumOfMandatoryTlv = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReportTypeValue = (UINT1) ELM_INIT_VAL;

    u1NumOfMandatoryTlv =
        (UINT1) NUM_OF_MANDATORY_TLV_FULL_STATUS_CONTINUED_MSG;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present\n");
        return ELM_FAILURE;
    }

    if (ElmParseRcvdTLV (pu1Buf, u4IfIndex, u2DataLength,
                         au1FullStatusContTlv, u1NumOfMandatoryTlv,
                         ELM_FULL_STATUS_CONTINUED) == ELM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseRcvdTLV function has returned failure Message\n");
        return ELM_FAILURE;
    }
    /* Full staus Continued Message is received and Parsed 
     * properly so Increment the No of valid messagreceived 
     * count & no of received full status continued message 
     * count.
     */
    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    ELM_INCR_RX_FULL_STATUS_CONTINUED_COUNT (u4IfIndex);
    ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PT);

    /* when the full status continued message is recived and 
     * parsed by UNI-C and Information of EVC & CE-VLAN ID
     * has  been stored then UNI-C must transmit the full 
     * status enquiry message.
     */
    u1ReportTypeValue = (UINT1) ELM_FULL_STATUS_CONTINUED;

    if (ElmPortTxElmiPdu (pElmPortEntry, u1ReportTypeValue) != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmPortTxElmiPdu function has returned failure Message\n");
        return ELM_FAILURE;
    }
    ELM_INCR_TX_FULL_STATUS_CONTINUED_ENQ_MSG_COUNT (u4IfIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmRcvdFullStatus                                    */
/*                                                                           */
/* Description        : This function handles the received ELMI PDU when the */
/*                      received report type is Full Status.                  */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to ELMI -PDU                       */
/*                      u4IfIndex - Port number                               */
/*                      u2DataLength - Length of the remaaning PDU           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmRcvdFullStatus (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1
         
         
         
         
         
         
         
        au1FullStatusContTlv[NUM_OF_MANDATORY_TLV_FULL_STATUS_MSG] =
        { 0x02, 0x03, 0x11, 0x21, 0x22 };
    UINT1               u1NumOfMandatoryTlv = (UINT1) ELM_INIT_VAL;

    u1NumOfMandatoryTlv = NUM_OF_MANDATORY_TLV_FULL_STATUS_MSG;
    if (ElmParseRcvdTLV (pu1Buf, u4IfIndex, u2DataLength,
                         au1FullStatusContTlv, u1NumOfMandatoryTlv,
                         ELM_FULL_STATUS) == ELM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseRcvdTlv function has returned failure Message\n");
        return ELM_FAILURE;
    }
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present\n");
        return ELM_FAILURE;
    }
    /* Full staus Message is received and Parsed 
     * properly so Increment the No of valid messag received 
     * count & no of received full status continued message 
     * count 
     */

    ELM_INCR_RX_VALID_MSG_COUNT (u4IfIndex);
    ELM_INCR_RX_FULL_STATUS_COUNT (u4IfIndex);

    /* Full Status message is received and Parsed 
     * correctly now set the bMsgRcvdInTime to True.
     */
    pElmPortEntry->bMsgRcvdInTime = ELM_TRUE;
    pElmPortEntry->bMsgExpected = ELM_FALSE;
    if ((pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE) &&
        (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY))
    {
        pElmPortEntry->bElmRedBulkUpdInProgress = ELM_FALSE;
        pElmPortEntry->bMsgRcvdInTime = ELM_FALSE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmParseRcvdTLV                                      */
/*                                                                           */
/* Description        : This function handles the received E-LMI-PDU when the*/
/*                      received report type is E-LMI Check Status Message.  */
/*                      This routine helps in exchange and update of sequence*/
/*                      numbers & data instance.                             */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port at which the PDU is receieved        */
/*                      u2DataLength - Length of the remaning PDU           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmParseRcvdTLV (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT2 u2DataLength, UINT1
                 *au1PacketTlv, UINT1 u1NoOfTlv, UINT1 u1ReportType)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    tLcmEvcCeVlanInfo  *pLcmCeVlanInfo = NULL;
    tLcmEvcCeVlanInfo  *pLcmCeVlanInfoInDb = NULL;
    tLcmEvcInfo        *pLcmEvcInfo = NULL;
    tLcmEvcInfo        *pLcmEvcCeVlanInfo = NULL;
    INT4                i4RetVal = ELM_FAILURE;
    UINT4               u4DataInstanceValue = (UINT4) ELM_INIT_VAL;
    UINT1               au1ElmiTlvRcvd[NUM_OF_MANDATORY_TLV_STATUS_MSG] =
        { 0x00, 0x00, 0x00, 0x00, 0x00 };
    UINT1               u1LocalTlvIndex = (UINT1) ELM_INIT_VAL;
    UINT1               u1RcvdTlvType = (UINT1) ELM_INIT_VAL;
    UINT1               u1EvcInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1LastIeBitFinder = (UINT1) ELM_INIT_VAL;
    UINT1               u1DefaultEvcBit = (UINT1) ELM_INIT_VAL;
    UINT1               u1UntagPriTagBit = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanIeFifthByte = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmUniInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1UnexpTlvLen = (UINT1) ELM_INIT_VAL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present!!!\n");
        return ELM_FAILURE;
    }
    if (ELM_ALLOC_LCM_EVC_INFO (pLcmEvcInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "pLcmEvcInfo:Memory Allocation failed\n");
        return ELM_FAILURE;
    }
    ELM_MEMSET (pLcmEvcInfo, (UINT1) ELM_INIT_VAL, sizeof (tLcmEvcInfo));
    /* If the received PDU has some new information, it will be required
     * to send the PDU to standby. Store the last parsed EVC ref id for proper
     * parsing.
     */
    pElmPortEntry->u2LastParsedEvcRefId =
        pElmPortEntry->u2LastReceivedRefrenceID;
    while (u2DataLength != ELM_INIT_VAL)
    {
        ELM_GET_1BYTE (u1RcvdTlvType, pu1Buf);

        if (ELM_DECR_DATA_LENGTH (u2DataLength, (UINT1) ELM_DECR_DATALEN_ONE) ==
            ELM_FAILURE)
        {
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ELMI PDU receieved with invalid length!!!\n");
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            return ELM_FAILURE;
        }
        switch (u1RcvdTlvType)
        {

            case ELM_REPORT_TYPE_IE:

                /*Increment Duplicate IE received Counter */
                /*Skip Duplicated IE */
                ELM_INCR_DUPLICATE_IE (u4IfIndex);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "DUP:ELMI PDU receieved Contains Duplicate Report"
                         "Type IE!!!\n");

                pu1Buf = pu1Buf + ELM_INCR_BUFF_TWO;
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_TWO) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                continue;

            case ELM_SEQUENCE_NUMBER_IE:

                /* ELMI PDU will be Discarded when Single Evc Asynchronous 
                 * Status Message is received & It contains Sequence Number IE.
                 */
                if (u1ReportType == ELM_ASYNCHRONOUS_STATUS)
                {
                    ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR: Asynchronous Status Message received with Unexpected"
                             " Sequence Number Information Element \n");

                    /* Discard this TLV and parse further */
                    ELM_GET_1BYTE (u1UnexpTlvLen, pu1Buf);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    pu1Buf = pu1Buf + u1UnexpTlvLen;
                    u2DataLength = u2DataLength - u1UnexpTlvLen;
                    continue;
                }

                /* Check for sequence number TLV is repeated in ELMI PDU then   
                 * discard the TLV only.
                 */
                if (au1ElmiTlvRcvd[0] == ELM_TRUE)
                {
                    /*Increment Duplicate IE received Counter */
                    /*Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "DUP:ELMI PDU receieved Contains Duplicate Sequence"
                             "Number IE!!!\n");

                    pu1Buf = pu1Buf + ELM_INCR_BUFF_THREE;
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_THREE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                        ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                        return ELM_FAILURE;
                    }
                    continue;
                }

                /* Expected Mandatory IE is not received in the packet hence 
                 * Mandatory information is missing but expected IE is received 
                 * in the packet hence information is not in sequence.
                 */
                if (au1PacketTlv[u1LocalTlvIndex] != u1RcvdTlvType &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Mandatory IE missing Count */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);

                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= ELM_DECR_DATALEN_THREE) &&
                        (*(pu1Buf + ELM_INCR_BUFF_THREE) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                if (ElmVerifySequenceNumber (pu1Buf, u4IfIndex) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ElmVerifySequenceNumber function has returned Failure\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }

                pu1Buf = pu1Buf + ELM_INCR_BUFF_THREE;
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength,
                     (UINT1) ELM_DECR_DATALEN_THREE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                /* Update IE received */
                au1ElmiTlvRcvd[0] = ELM_TRUE;
                break;

            case ELM_DATA_INSTANCE_IE:

                /* ELMI PDU will be Discarded when Single Evc Asynchronous 
                 * Status Message is received & It contains Data Instance IE.
                 */
                if (u1ReportType == ELM_ASYNCHRONOUS_STATUS)
                {
                    ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR: Asynchronous Status Message received with Unexpected"
                             " Data Instance Information Element \n");

                    /* Discard this TLV and parse further */
                    ELM_GET_1BYTE (u1UnexpTlvLen, pu1Buf);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    pu1Buf = pu1Buf + u1UnexpTlvLen;
                    u2DataLength = u2DataLength - u1UnexpTlvLen;
                    continue;
                }
                /* Check for Data Instance TLV is repeated in ELMI PDU then   
                 * discard the TLV only.
                 */
                if (au1ElmiTlvRcvd[1] == ELM_TRUE)
                {
                    /*Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "DUP:ELMI PDU contains duplicate data Instance"
                             "IE!!!\n");

                    pu1Buf = pu1Buf + ELM_INCR_BUFF_SIX;
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_SIX) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                        ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                        return ELM_FAILURE;
                    }
                    continue;
                }

                /* Expected Mandatory IE is not received in the packet hence 
                 * Mandatory information is missing but expected IE is received 
                 * in the packet hence information is not in sequence.
                 */
                if ((au1PacketTlv[u1LocalTlvIndex] != u1RcvdTlvType) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= ELM_DECR_DATALEN_SIX) &&
                        (*(pu1Buf + ELM_DECR_DATALEN_SIX) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                /* Verify the Data Instance Value Received In ELMi PDU */
                if (ElmVerifyDataInstance
                    (pu1Buf, u4IfIndex, &u4DataInstanceValue) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ElmVerify DataInstance function has returned failure"
                             "!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                pu1Buf = pu1Buf + ELM_INCR_BUFF_SIX;
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_SIX) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }

                if (pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS)
                {
                    pElmPortEntry->u4FullStatusConDataInstance =
                        u4DataInstanceValue;
                }
                if (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE)
                {
                    /* During Bulk Update process, the messages are received from
                     * the active node. Set the Data Instance correctly
                     */
                    pElmPortEntry->u4FullStatusConDataInstance =
                        u4DataInstanceValue;
                }
                if (pElmPortEntry->u4DataInstance != u4DataInstanceValue)
                {
                    if (u1ReportType == ELM_ELMI_CHECK)
                    {
                        /* if the Local DI value at UNI-C is diffrent from the DI    
                         * value of recived message then first stop PT Timer & send  
                         * the full status enquiry message. Configure current Polling 
                         * Counter Value to Configured Value 
                         */
                        ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PT);
                        pElmPortEntry->u2PollingCounter =
                            pElmPortEntry->u2PollingCounterConfigured;

                        if (ElmPortTxEnquiryMessage (pElmPortEntry) !=
                            ELM_SUCCESS)
                        {
                            ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                     "ERR:ElmPortTxStatusEnquiryMessage function has "
                                     "returned FAILURE!\n");
                            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                            return ELM_FAILURE;
                        }
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    }
                }
                if (u1ReportType == ELM_FULL_STATUS_CONTINUED)
                {
                    /* if the node is standby, there is a possibility that
                     * it received an intermittent PDU. This check is not required for standby
                     */
                    if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
                    {
                        au1ElmiTlvRcvd[1] = ELM_TRUE;
                        break;
                    }
                    if (pElmPortEntry->u4FullStatusConDataInstance !=
                        u4DataInstanceValue)
                    {
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_INCR_INVALID_NON_MANDATORY_IE (u4IfIndex);
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:DI value error in FULL Status Continued Message \n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                }
                else
                {
                    if (u1ReportType == ELM_ELMI_CHECK)
                    {
                        pElmPortEntry->bMsgExpected = ELM_FALSE;
                        pElmPortEntry->bMsgRcvdInTime = ELM_TRUE;
                    }
                }
                /* Update IE received */
                au1ElmiTlvRcvd[1] = ELM_TRUE;

                break;

            case ELM_UNI_STATUS_IE:

                ELM_GET_1BYTE (u1ElmUniInfoLength, pu1Buf);

                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                /* ELMI PDU will be Discarded when Single Evc Asynchronous 
                 * Statsus Message,ELMI Check ,Full Status Continued status message
                 * contains UNI Status IE.
                 */
                if (u1ReportType == ELM_ELMI_CHECK ||
                    u1ReportType == ELM_FULL_STATUS_CONTINUED ||
                    u1ReportType == ELM_ASYNCHRONOUS_STATUS)
                {
                    ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR: Message received with Unexpected"
                             " UNI Status Information Element \n");

                    /* Discard this TLV and parse further */
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    pu1Buf = pu1Buf + u1UnexpTlvLen;
                    u2DataLength = u2DataLength - u1UnexpTlvLen;
                    continue;
                }
                /* Check for UNI STATUS IE is repeated in ELMI PDU then   
                 * discard the TLV only.
                 */
                if (au1ElmiTlvRcvd[2] == ELM_TRUE)
                {
                    /*Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "DUP:ELMI PDU Contains Duplicate UNI Status "
                             "IE!!!\n");

                    pu1Buf = pu1Buf + u1ElmUniInfoLength;

                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) u1ElmUniInfoLength) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex,
                                              ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    continue;
                }

                /* Expected Mandatory IE is not received in the packet hence 
                 * Mandatory information is missing but expected IE is received 
                 * in the packet hence information is not in sequence.
                 */
                if (au1PacketTlv[u1LocalTlvIndex] != u1RcvdTlvType &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= u1ElmUniInfoLength) &&
                        (*(pu1Buf + u1ElmUniInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                /*Parse the UNI Status Tlv */
                if (ElmHandleUniStatusTlv
                    (pu1Buf, u4IfIndex, u1ElmUniInfoLength) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU is received with error in UNI status IE \n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                pu1Buf = pu1Buf + u1ElmUniInfoLength;
                if (ELM_DECR_DATA_LENGTH (u2DataLength, u1ElmUniInfoLength) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                if (u2DataLength == (UINT2) ELM_INIT_VAL)
                {
                    if ((pElmPortEntry->u1LastReportTypeTxed ==
                         (UINT1) ELM_FULL_STATUS)
                        && (u1ReportType) == (UINT1) ELM_FULL_STATUS)
                    {
                        while (ElmGetNextEvcInfo
                               (u4IfIndex,
                                pElmPortEntry->u2LastReceivedRefrenceID,
                                pLcmEvcInfo) != LCM_FAILURE)
                        {
                            if (pLcmEvcInfo->LcmEvcStatusInfo.
                                u2EvcReferenceId == ELM_INIT_VAL)
                            {
                                break;
                            }

                            ELM_TRC (ELM_MGMT_TRC,
                                     "DEL:Evc is deleted from UNI-C  \n");
                            /* Delete the evc From the LCM data base 
                             */
                            pElmPortEntry->bElmStatusInformationChange =
                                ELM_TRUE;
                            ElmiOldEvcDeletedTrap (u4IfIndex,
                                                   pLcmEvcInfo->
                                                   LcmEvcStatusInfo.
                                                   au1EvcIdentifier,
                                                   ELM_PORT_TRAPS_OID,
                                                   ELM_PORT_TRAPS_OID_LEN);
                            ElmDeleteEvcIndication (u4IfIndex,
                                                    pLcmEvcInfo->
                                                    LcmEvcStatusInfo.
                                                    u2EvcReferenceId);
                            ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                          "DEL:The EVC Refrance Id %d is Deleted From LCM DataBase \n",
                                          pLcmEvcInfo->LcmEvcStatusInfo.
                                          u2EvcReferenceId);
                            ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                            pElmPortEntry->u2LastReceivedRefrenceID =
                                pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;

                        }

                    }
                }
                au1ElmiTlvRcvd[2] = ELM_TRUE;

                break;

            case ELM_EVC_STATUS_IE:

                ELM_GET_1BYTE (u1EvcInfoLength, pu1Buf);
                if (u1ReportType == ELM_ASYNCHRONOUS_STATUS)
                {
                    /* Check for EVC STATUS IE is repeated in ELMI PDU then   
                     * discard the TLV only.
                     */
                    if (au1ElmiTlvRcvd[3] == ELM_TRUE)
                    {
                        /* Increment Duplicate IE received Counter */
                        /* Skip Duplicated IE */
                        ELM_INCR_DUPLICATE_IE (u4IfIndex);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "DUP:Asynchronous Status Message Contains Duplicate "
                                 "Evc IE!!!\n");

                        u1EvcInfoLength =
                            u1EvcInfoLength + (UINT1) ELM_INCR_DATALEN_ONE;
                        pu1Buf = pu1Buf + u1EvcInfoLength;

                        if (ELM_DECR_DATA_LENGTH (u2DataLength, u1EvcInfoLength)
                            == ELM_FAILURE)
                        {
                            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                            ElmInvalidPduRxdTrap (u4IfIndex,
                                                  ELM_PROTOCOL_ERROR,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                            ELM_TRC (ELM_ALL_FAILURE_TRC,
                                     "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                            return ELM_FAILURE;
                        }
                        continue;

                    }
                }
                /* ELMI PDU will be Discarded when Elmi Check  
                 * Statsus Message contains EVC Status IE.
                 */
                if (u1ReportType == ELM_ELMI_CHECK)
                {
                    ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR: ELMI Check Message received with Unexpected"
                             " UNI Status Information Element \n");

                    /* Discard this TLV and parse further */
                    ELM_GET_1BYTE (u1UnexpTlvLen, pu1Buf);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    pu1Buf = pu1Buf + u1UnexpTlvLen;
                    u2DataLength = u2DataLength - u1UnexpTlvLen;
                    continue;
                }

                /* Expected Mandatory IE is not received in the packet hence 
                 * Mandatory information is missing but expected IE is received 
                 * in the packet hence information is not in sequence.
                 */
                if (au1PacketTlv[u1LocalTlvIndex] != u1RcvdTlvType &&
                    (u1ReportType == ELM_FULL_STATUS) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= u1EvcInfoLength) &&
                        (*(pu1Buf + u1EvcInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }

                if ((au1ElmiTlvRcvd[2] != ELM_TRUE)
                    && (u1ReportType == ELM_FULL_STATUS))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= u1EvcInfoLength) &&
                        (*(pu1Buf + u1EvcInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;

                }
                if ((au1ElmiTlvRcvd[1] != ELM_TRUE)
                    && (u1ReportType == ELM_FULL_STATUS_CONTINUED))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u2DataLength >= u1EvcInfoLength) &&
                        (*(pu1Buf + u1EvcInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }

                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                /*Parse the EVC Status IE further */
                if (ElmHandleEvcStatusTlv (pu1Buf, u4IfIndex, u1EvcInfoLength,
                                           u1ReportType) == ELM_FAILURE)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:Error in EVC Info received \n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }

                pu1Buf = pu1Buf + u1EvcInfoLength;
                if (ELM_DECR_DATA_LENGTH (u2DataLength, u1EvcInfoLength) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                /* When next Tlv Type is CE-Vlan then parse the CE-Valn IE.
                 */

                if (u1ReportType == ELM_ASYNCHRONOUS_STATUS ||
                    *pu1Buf == ELM_CE_VLAN_EVC_MAP_IE)
                {
                    au1ElmiTlvRcvd[3] = ELM_TRUE;
                    break;
                }
                else
                {
                    continue;
                }
                break;

            case ELM_CE_VLAN_EVC_MAP_IE:

                /* When Elmi Check or Single EVC Asynchronous Staus message 
                 * Contrains CE-VLAN IE then LMI PDU will be discarded
                 */
                if ((u1ReportType == ELM_ELMI_CHECK) ||
                    (u1ReportType == ELM_ASYNCHRONOUS_STATUS))
                {
                    ELM_INCR_INVALID_UNEXPECTED_IE (u4IfIndex);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR: Message received with Unexpected"
                             " UNI Status Information Element \n");

                    /* Discard this TLV and parse further */
                    ELM_GET_1BYTE (u1UnexpTlvLen, pu1Buf);
                    if (ELM_DECR_DATA_LENGTH
                        (u2DataLength,
                         (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                    }
                    pu1Buf = pu1Buf + u1UnexpTlvLen;
                    u2DataLength = u2DataLength - u1UnexpTlvLen;
                    continue;
                }
                if (pElmPortEntry->u1ElmCeTlvCheck == (UINT1) ELM_INIT_VAL)
                {
                    while (ElmGetNextEvcInfo
                           (u4IfIndex, pElmPortEntry->u2LastReceivedRefrenceID,
                            pLcmEvcInfo) != LCM_FAILURE)
                    {
                        if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId ==
                            ELM_INIT_VAL)
                        {
                            break;
                        }

                        ELM_TRC (ELM_MGMT_TRC,
                                 "DEL:Evc is deleted from UNI-C  \n");
                        /* Delete the evc From the LCM data base 
                         */
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ElmiOldEvcDeletedTrap (u4IfIndex,
                                               pLcmEvcInfo->LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmDeleteEvcIndication (u4IfIndex,
                                                pLcmEvcInfo->LcmEvcStatusInfo.
                                                u2EvcReferenceId);
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "DEL:The EVC Refrance Id %d is Deleted From LCM DataBase \n",
                                      pLcmEvcInfo->LcmEvcStatusInfo.
                                      u2EvcReferenceId);
                        ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        pElmPortEntry->u2LastReceivedRefrenceID =
                            pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;

                    }
                }

                /* Expected IE is not received and still TLV's are to be parsed in 
                 * the packet hence Mandatory information is missing 
                 * Secondly expected IE in the packet is received hence information 
                 * is not in sequence.
                 */
                if (pElmPortEntry->u1LastReportTypeTxed == ELM_FULL_STATUS)
                {
                    if ((au1ElmiTlvRcvd[3] != ELM_TRUE)
                        && (u1ReportType == ELM_FULL_STATUS))
                    {
                        /* Increment Invalid PDU received and Mandatory IE missing */
                        /* Increment Info Out of Sequence */
                        ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                        return ELM_FAILURE;
                    }
                }

                if ((au1ElmiTlvRcvd[1] != ELM_TRUE)
                    && (u1ReportType == ELM_FULL_STATUS_CONTINUED))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                if (ELM_ALLOC_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo) == NULL)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "pLcmCeVlanInfo:Memory Allocation failed\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    return ELM_FAILURE;
                }

                ELM_MEMSET (pLcmCeVlanInfo, ELM_INIT_VAL,
                            sizeof (tLcmEvcCeVlanInfo));

                ELM_GET_1BYTE (u1CeVlanInfoLength, pu1Buf);
                ELM_GET_2BYTE (pLcmCeVlanInfo->u2EvcReferenceId, pu1Buf);
                ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                if (ELM_ALLOC_LCM_EVC_INFO (pLcmEvcCeVlanInfo) == NULL)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "pLcmEvcCeVlanInfo:Memory Allocation failed\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                ELM_MEMSET (pLcmEvcCeVlanInfo, (UINT1) ELM_INIT_VAL,
                            sizeof (tLcmEvcCeVlanInfo));

                if (ElmGetEvcInfo
                    (u4IfIndex, pLcmCeVlanInfo->u2EvcReferenceId,
                     pLcmEvcCeVlanInfo) != LCM_SUCCESS)
                {
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "ERR:The EVC Refrence ID is no currently present in LCM %d \n",
                                  pLcmCeVlanInfo->u2EvcReferenceId);
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:Vlans are map to those EVC which is not"
                             "currently present in LCM|||\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }

                ELM_GET_1BYTE (u1LastIeBitFinder, pu1Buf);
                ELM_GET_1BYTE (u1CeVlanIeFifthByte, pu1Buf);

                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength,
                     (UINT1) ELM_DECR_DATALEN_FIVE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                /* Parse the CE-VLAN Tlv */
                if (ElmHandleCeVlanTlv
                    (pu1Buf, u4IfIndex,
                     (u1CeVlanInfoLength - (UINT1) ELM_DECR_DATALEN_FOUR)) ==
                    ELM_FAILURE)
                {
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ElmHandleCeVlanTlv has returned Failure \n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                u1LastIeBitFinder =
                    u1LastIeBitFinder & (UINT1) ELM_LAST_BIT_FINDER;
                /* Get the default EVC bit and Untagged/Priority Tagged bit */
                u1DefaultEvcBit = u1CeVlanIeFifthByte;
                u1DefaultEvcBit =
                    u1DefaultEvcBit & (UINT1) ELM_DEFAULT_EVC_FINDER;
                u1UntagPriTagBit =
                    u1CeVlanIeFifthByte & (UINT1) ELM_TAGGED_BIT_FINDER;

                pu1Buf =
                    pu1Buf + (u1CeVlanInfoLength -
                              (UINT1) ELM_DECR_DATALEN_FOUR);
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength,
                     (u1CeVlanInfoLength - (UINT1) 4)) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    return ELM_FAILURE;
                }
                pElmPortEntry->u1ElmCeTlvCheck = (UINT1) ELM_TRUE;
                if (u1LastIeBitFinder != (UINT1) ELM_INIT_VAL)
                {
                    pElmPortEntry->u2LastCeVlanEvcRefId =
                        pLcmCeVlanInfo->u2EvcReferenceId;
                    ELM_MEMCPY ((pLcmCeVlanInfo->CeVlanMapInfo),
                                &(pElmPortEntry->CeVlanMapInfo),
                                LCM_CE_VLAN_SIZE);
                    pLcmCeVlanInfo->u2FirstVlanId =
                        pElmPortEntry->u2FirstVlanId;
                    pLcmCeVlanInfo->u2LastVlanId = pElmPortEntry->u2LastVlanId;
                    pLcmCeVlanInfo->u2NoOfVlansMapped =
                        pElmPortEntry->u2NoOfVlansMapped;
                    pLcmCeVlanInfo->u1DefaultEvc = u1DefaultEvcBit;
                    pLcmCeVlanInfo->u1UntagPriTag = u1UntagPriTagBit;
                    /* The complete CE-VLAN info for an EVC has been parsed.
                     * If the received information is different than the one 
                     * present in the database then this information has to be passed to 
                     * LCM for updation. In case of HA, all the Vlans will be sent to the 
                     * standby
                     */
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                    if (ELM_ALLOC_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb) ==
                        NULL)
                    {
                        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                                 "pLcmCeVlanInfoInDb:Memory Allocation failed\n");

                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                        return ELM_FAILURE;
                    }

                    ELM_MEMSET (pLcmCeVlanInfoInDb, (UINT1) ELM_INIT_VAL,
                                sizeof (tLcmEvcCeVlanInfo));

                    ElmGetEvcCeVlanInfo (u4IfIndex,
                                         pLcmCeVlanInfo->u2EvcReferenceId,
                                         pLcmCeVlanInfoInDb);
                    if (MEMCMP
                        (pLcmCeVlanInfo, pLcmCeVlanInfoInDb,
                         sizeof (tLcmEvcCeVlanInfo)) != ELM_INIT_VAL)
                    {
                        ElmPassCeVlanInfo (u4IfIndex, pLcmCeVlanInfo);
                        if ((ELM_RM_GET_NODE_STATE () == RED_ELM_ACTIVE) &&
                            (ELM_IS_STANDBY_UP () != ELM_FALSE))
                        {
                            ElmRedPassCeVlanInfo (u4IfIndex, pLcmCeVlanInfo);
                        }
                    }
                    pElmPortEntry->u2FirstVlanId = (UINT2) ELM_INIT_VAL;
                    pElmPortEntry->u2LastVlanId = (UINT2) ELM_INIT_VAL;
                    pElmPortEntry->u2NoOfVlansMapped = (UINT2) ELM_INIT_VAL;
                    ELM_MEMSET (pElmPortEntry->CeVlanMapInfo,
                                (UINT1) ELM_INIT_VAL, LCM_CE_VLAN_SIZE);
                    au1ElmiTlvRcvd[4] = ELM_TRUE;
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    continue;
                }
                else
                {
                    ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                    continue;
                }
                ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfo);
                ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                break;
            case ELM_INIT_VAL:

                while (1)
                {
                    if (u1ReportType == (UINT1) ELM_FULL_STATUS)
                    {
                        if (u1LocalTlvIndex >= (UINT1) ELM_THREE_TLV_PARSED)
                        {
                            if ((pElmPortEntry->u1LastReportTypeTxed ==
                                 (UINT1) ELM_FULL_STATUS)
                                && (u1ReportType) == (UINT1) ELM_FULL_STATUS)
                            {
                                while (ElmGetNextEvcInfo
                                       (u4IfIndex,
                                        pElmPortEntry->u2LastReceivedRefrenceID,
                                        pLcmEvcInfo) != LCM_FAILURE)
                                {
                                    if (pLcmEvcInfo->LcmEvcStatusInfo.
                                        u2EvcReferenceId == ELM_INIT_VAL)
                                    {
                                        break;
                                    }

                                    ELM_TRC (ELM_MGMT_TRC,
                                             "DEL:Evc is deleted from UNI-C  \n");
                                    /* Delete the evc From the LCM data base 
                                     */
                                    pElmPortEntry->bElmStatusInformationChange =
                                        ELM_TRUE;
                                    ElmiOldEvcDeletedTrap (u4IfIndex,
                                                           pLcmEvcInfo->
                                                           LcmEvcStatusInfo.
                                                           au1EvcIdentifier,
                                                           ELM_PORT_TRAPS_OID,
                                                           ELM_PORT_TRAPS_OID_LEN);
                                    ElmDeleteEvcIndication (u4IfIndex,
                                                            pLcmEvcInfo->
                                                            LcmEvcStatusInfo.
                                                            u2EvcReferenceId);
                                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                                  "DEL:The EVC Refrance Id %d is Deleted From LCM DataBase \n",
                                                  pLcmEvcInfo->LcmEvcStatusInfo.
                                                  u2EvcReferenceId);
                                    ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT
                                        (u4IfIndex);
                                    pElmPortEntry->u2LastReceivedRefrenceID =
                                        pLcmEvcInfo->LcmEvcStatusInfo.
                                        u2EvcReferenceId;

                                }

                            }
                            i4RetVal = ELM_SUCCESS;
                            break;
                        }
                    }
                    if (u1ReportType == (UINT1) ELM_ELMI_CHECK)
                    {
                        if (u1LocalTlvIndex == (UINT1) ELM_TWO_TLV_PARSED)
                        {
                            i4RetVal = ELM_SUCCESS;
                            break;
                        }
                    }
                    if (u1ReportType == (UINT1) ELM_FULL_STATUS_CONTINUED)
                    {
                        if (u1LocalTlvIndex >= (UINT1) ELM_TWO_TLV_PARSED)
                        {
                            i4RetVal = ELM_SUCCESS;
                            break;
                        }
                    }
                    if (u1ReportType == (UINT1) ELM_ASYNCHRONOUS_STATUS)
                    {
                        if (u1LocalTlvIndex == (UINT1) ELM_ONE_TLV_PARSED)
                        {
                            i4RetVal = ELM_SUCCESS;
                            break;
                        }
                    }
                    if (i4RetVal == ELM_SUCCESS)
                    {
                        /* Check if all the remaining bytes are 0, i.e it is
                         * padding
                         */
                        while (u2DataLength != ELM_INIT_VAL)
                        {
                            if ((*pu1Buf) == ELM_INIT_VAL)
                            {
                                pu1Buf = pu1Buf + 1;
                                u2DataLength = u2DataLength - 1;
                                continue;
                            }
                            else
                            {
                                /* Some Value is there, that means it is a
                                 * Invalid PDU
                                 */
                                ELM_TRC (ELM_MGMT_TRC,
                                         "ELM: Invalid Elmi  message is received."
                                         " So ELMI PDU is Dropped\n");
                                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                                ElmInvalidPduRxdTrap (u4IfIndex,
                                                      ELM_PROTOCOL_ERROR,
                                                      ELM_PORT_TRAPS_OID,
                                                      ELM_PORT_TRAPS_OID_LEN);
                                ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                                ELM_RELEASE_LCM_EVC_VLAN_INFO
                                    (pLcmCeVlanInfoInDb);
                                return ELM_FAILURE;
                            }
                        }
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    }
                    else
                    {
                        /* Number of Mandatory TLV parsed in the received message
                         * are less than expected hence Discard packet*/
                        ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved with missing mandatory IE!!!\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
                        ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
                        return ELM_FAILURE;
                    }
                }
                break;
            default:

                ELM_GET_1BYTE (u1UnexpTlvLen, pu1Buf);
                ELM_INCR_PROT_ERR_UNRECOGNISED_IE_RX_COUNT (u4IfIndex);
                if (ELM_DECR_DATA_LENGTH
                    (u2DataLength, (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                pu1Buf = pu1Buf + u1UnexpTlvLen;
                u2DataLength = u2DataLength - u1UnexpTlvLen;
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ELMI PDU receieved with unrecognized IE!!!\n");
                continue;
        }                        /*End Switch */
        /*Increment the No Of TLV Parsed Counter */
        ELM_INCR_NO_OF_LOCAL_TLV_PARSED (u1LocalTlvIndex);

        if ((u1LocalTlvIndex == u1NoOfTlv)
            && (u2DataLength == (UINT1) ELM_INIT_VAL))
        {
            break;
        }
    }                            /*End While */

    if (u1ReportType == (UINT1) ELM_FULL_STATUS)
    {
        if (u1LocalTlvIndex >= (UINT1) ELM_THREE_TLV_PARSED)
        {
            if (au1ElmiTlvRcvd[4] != ELM_TRUE)
            {
                while (ElmGetNextEvcInfo
                       (u4IfIndex, pElmPortEntry->u2LastReceivedRefrenceID,
                        pLcmEvcInfo) != LCM_FAILURE)
                {
                    if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId ==
                        ELM_INIT_VAL)
                    {
                        break;
                    }

                    ELM_TRC (ELM_MGMT_TRC, "DEL:Evc is deleted from UNI-C  \n");
                    /* Delete the evc From the LCM data base 
                     */
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ElmiOldEvcDeletedTrap (u4IfIndex,
                                           pLcmEvcInfo->LcmEvcStatusInfo.
                                           au1EvcIdentifier, ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmDeleteEvcIndication (u4IfIndex,
                                            pLcmEvcInfo->LcmEvcStatusInfo.
                                            u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "DEL:The EVC Refrance Id %d is Deleted From LCM DataBase \n",
                                  pLcmEvcInfo->LcmEvcStatusInfo.
                                  u2EvcReferenceId);
                    ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    pElmPortEntry->u2LastReceivedRefrenceID =
                        pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;

                }
            }

            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            return ELM_SUCCESS;
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC,
                     "ERR:Mandatory IE missing in received Full Status Message \n");
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
            return ELM_FAILURE;

        }
    }

    if (u1ReportType == (UINT1) ELM_ELMI_CHECK)
    {
        if (u1LocalTlvIndex == (UINT1) ELM_TWO_TLV_PARSED)
        {
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            return ELM_SUCCESS;
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC,
                     "ERR:Mandatory IE missing in received Elmi Check Status Message \n");
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
            return ELM_FAILURE;

        }
    }
    if (u1ReportType == (UINT1) ELM_FULL_STATUS_CONTINUED)
    {
        if (u1LocalTlvIndex >= (UINT1) ELM_TWO_TLV_PARSED)
        {
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
            return ELM_SUCCESS;
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC,
                     "ERR:Mandatory IE missing in received Full Status Continued Message \n");
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcCeVlanInfo);
            ELM_RELEASE_LCM_EVC_VLAN_INFO (pLcmCeVlanInfoInDb);
            return ELM_FAILURE;

        }
    }
    if (u1ReportType == (UINT1) ELM_ASYNCHRONOUS_STATUS)
    {
        if (u1LocalTlvIndex == (UINT1) ELM_ONE_TLV_PARSED)
        {
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            return ELM_SUCCESS;
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC,
                     "ERR:Mandatory IE missing in received Single Evc Asynchronous Status Message \n");
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
            return ELM_FAILURE;

        }
    }
    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmVerifySequenceNumber                              */
/*                                                                           */
/* Description        : This function checks for the presence and            */
/*                      correctness of the sequence numbers in the received  */
/*                      E-LMI-PDU. This function then calls the routine for  */
/*                      updation of the sequence counters                    */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port at which the PDU is receieved        */
/*                                                                           */
/* Output(s)          : pu1SendSequenceNumber � Send Sequence Number as      */
/*                      received in the PDU                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmVerifySequenceNumber (UINT1 *pu1Buf, UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               u1SequenceNumLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1SendSequenceNumber = (UINT1) ELM_INIT_VAL;
    UINT1               u1ReceiveSequenceNumber = (UINT1) ELM_INIT_VAL;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return ELM_FAILURE;
    }

    ELM_TRC_FN_ENTRY ();

    /* Fetching the value of Length of Sequence Number, Send Sequence Number 
     * and Recieve Sequence Number from the message.
     */
    ELM_GET_1BYTE (u1SequenceNumLength, pu1Buf);
    ELM_GET_1BYTE (u1SendSequenceNumber, pu1Buf);
    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                  "The SendSequence Number is  %d in Sequence IE\n",
                  u1SendSequenceNumber);

    ELM_GET_1BYTE (u1ReceiveSequenceNumber, pu1Buf);
    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                  "The ReceiveSequence Number is  %d in Sequence IE\n",
                  u1ReceiveSequenceNumber);

    /* Validate the values */
    if (u1SequenceNumLength != ELM_SEQUENCE_NUMBER_LENGTH)
    {
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                      "ERR:Error in Length = %d of Received Sequence IE\n",
                      u1SequenceNumLength);
        return ELM_FAILURE;
    }
    /* If the node is standby, then it is parsing the message that 
     * Active has send to it. In that case it should keep track of the 
     * sequence number of the messages that it has received in the case of
     * Bulk Update.
     */
    if ((ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY) &&
        (pElmPortEntry->bElmRedBulkUpdInProgress == ELM_TRUE))
    {
        /* Check if no packet has been lost */
        if (u1SendSequenceNumber !=
            ((pElmPortEntry->u1ElmRedRecvSeqCounter) + 1))
        {
            ELM_TRC_ARG2 (ELM_ALL_FAILURE_TRC,
                          "ERR:RM:u1SendSequenceNumber=%d,pElmPortEntry->u1ElmRedRecvSeqCounter=%d\n",
                          u1SendSequenceNumber,
                          pElmPortEntry->u1ElmRedRecvSeqCounter);
            return ELM_FAILURE;
        }
        else
        {
            pElmPortEntry->u1ElmRedRecvSeqCounter = u1SendSequenceNumber;
            return ELM_SUCCESS;
        }
    }
    /* Compare the Value of Receive Sequence Number with Send Sequence Counter 
     */
    if (u1ReceiveSequenceNumber != pElmPortEntry->u1SendSeqCounter)
    {
        ELM_INCR_INVALID_SEQUENCE_NUMBER (u4IfIndex);
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_RELIABILITY_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                      "ERR:Discarding the ELMI PDU due to Mismatch in RcvSeqNum = %d in the packet\n",
                      u1ReceiveSequenceNumber);
        return ELM_FAILURE;
    }
    /* update the Receive sequence Counter with Send Sequence Number 
     */
    pElmPortEntry->u1RecvSeqCounter = u1SendSequenceNumber;
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmVerifyDataInstance                                */
/*                                                                           */
/* Description        : This function checks for the correctness of the      */
/*                      Data Instance IE. It then compares the value of the  */
/*                      received DI with the locally stored value            */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      pElmPortEntry - Port Entry pointer                   */
/*                      pu4DataInstanceValue= data Instance Value            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmVerifyDataInstance (UINT1 *pu1Buf, UINT4 u4IfIndex,
                       UINT4 *pu4DataInstanceValue)
{
    UINT1               u1DataInstanceLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1Reserved = (UINT1) ELM_INIT_VAL;

    ELM_TRC_FN_ENTRY ();

    /* Fetch the Data Instance value and verify its correctness */
    ELM_GET_1BYTE (u1DataInstanceLength, pu1Buf);
    ELM_GET_1BYTE (u1Reserved, pu1Buf);
    ELM_GET_4BYTE (*pu4DataInstanceValue, pu1Buf);
    ELM_TRC_ARG1 (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                  "The value of DI in received ELMI PDU is %d\n",
                  *pu4DataInstanceValue);

    /* Validate the values */
    if ((u1DataInstanceLength != ELM_LENGTH_OF_DATA_INSTANCE) ||
        (u1Reserved != ELM_INIT_VAL))
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:Error in Data Instance IE received \n");
        return ELM_FAILURE;
    }

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleCeVlanTlv                                   */
/*                                                                           */
/* Description        : This Function handle CE-VLAN Tlv & this is called by */
/*                      ElmParseRcvdTLV function                             */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port Number                               */
/*                      u1CeVlanInfoLength= CeVlan Info Length               */
/*                      pElmCeVlanInfo = Pointer to Ce Vlan Structure        */
/*                      pu1CeVlanCount = Pointer to CE Vlan Count            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmHandleCeVlanTlv (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT1 u1CeVlanInfoLength)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT2               u2CeVlanId = (UINT2) ELM_INIT_VAL;
    UINT1               u1CeVlanSubInfoId = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1CeVlanSubLength = (UINT1) ELM_INIT_VAL;

    ELM_GET_1BYTE (u1CeVlanSubInfoId, pu1Buf);
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present !!!\n");
        return ELM_FAILURE;
    }

    if (ELM_DECR_DATA_LENGTH (u1CeVlanInfoLength, (UINT1) 1) == ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with invalid length!!!\n");
        return ELM_FAILURE;
    }
    /* Validate CE-VLAN ID */
    if (u1CeVlanSubInfoId != ELM_EVC_MAP_ENTRY_IE)
    {
        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                 "ERR:ELMI Pdu Contains invalid CeVlanSubInfo SubinfoId\n");
        return ELM_FAILURE;
    }

    ELM_GET_1BYTE (u1CeVlanSubInfoLength, pu1Buf);

    if (ELM_DECR_DATA_LENGTH (u1CeVlanInfoLength, (UINT1) 1) == ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with invalid length!!!\n");
        return ELM_FAILURE;
    }
    u1CeVlanSubLength = u1CeVlanSubInfoLength;
    while (u1CeVlanSubInfoLength != (UINT1) ELM_INIT_VAL)
    {
        ELM_GET_2BYTE (u2CeVlanId, pu1Buf);
        ELM_INCR_VLANS_MAPPED (u4IfIndex);
        OSIX_BITLIST_SET_BIT (pElmPortEntry->CeVlanMapInfo, u2CeVlanId,
                              LCM_CE_VLAN_SIZE);
        if (pElmPortEntry->u2FirstVlanId == (UINT1) ELM_INIT_VAL)
        {
            pElmPortEntry->u2FirstVlanId = u2CeVlanId;
        }
        pElmPortEntry->u2LastVlanId = u2CeVlanId;
        if (ELM_DECR_DATA_LENGTH (u1CeVlanSubInfoLength, (UINT1) 2) ==
            ELM_FAILURE)
        {
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ELMI PDU receieved with invalid length!!!\n");
            return ELM_FAILURE;
        }
    }

    if (ELM_DECR_DATA_LENGTH (u1CeVlanInfoLength, u1CeVlanSubLength) ==
        ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
        return ELM_FAILURE;
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleUniStatusTlv                                */
/*                                                                           */
/* Description        : This Function handle UNI Status Tlv & this is called */
/*                      by ElmParseRcvdTLV function                          */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port Number                               */
/*                      u1ElmUniInfoLength = Length of UNI Status            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmHandleUniStatusTlv (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT1 u1ElmUniInfoLength)
{
    tCfaUniInfo         CfaUniInfo;
    tCfaUniInfo         CfaUniInfoPrevious;
    tElmPortEntry      *pPortEntry = NULL;

    ELM_MEMSET (&CfaUniInfo, (UINT1) ELM_INIT_VAL, sizeof (tCfaUniInfo));
    ELM_MEMSET (&CfaUniInfoPrevious, (UINT1) ELM_INIT_VAL,
                sizeof (tCfaUniInfo));
    CfaUniInfo.u4IfIndex = (UINT4) u4IfIndex;
    ELM_GET_1BYTE (CfaUniInfo.u1CeVlanMapType, pu1Buf);

    if (ELM_DECR_DATA_LENGTH (u1ElmUniInfoLength, (UINT1) 1) == ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with invalid length!!!\n");
        return ELM_FAILURE;
    }
    /* Parse the UNI SubInformation Element */
    if (ElmParseUniSubInfo (pu1Buf, u4IfIndex, u1ElmUniInfoLength, &CfaUniInfo)
        != ELM_SUCCESS)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseUniSubInfo function has returned failure!!!\n");
        return ELM_FAILURE;
    }
    /* After Passing the Complete Tlv OF UNI Pass the UNI Information to CFA */

    if (ElmGetUniInfo (u4IfIndex, &CfaUniInfoPrevious) == ELM_FAILURE)
    {
        ELM_TRC_ARG1 (ELM_INIT_SHUT_TRC | ELM_CONTROL_PATH_TRC,
                      "UNI information not fetched for port = %s",
                      ELM_GET_IFINDEX_STR (u4IfIndex));
        return ELM_FAILURE;
    }
    if (CfaUniInfoPrevious.u1CeVlanMapType != CfaUniInfo.u1CeVlanMapType)
    {
        ELM_TRC (ELM_MGMT_TRC,
                 "UNI IE is parsed from ELMI PDU & UNI Information is passed to CFA!!!\n");
        pPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
        if (pPortEntry == NULL)
        {
            ELM_TRC (ELM_MGMT_TRC,
                     "UNI IE is parsed from ELMI PDU & UNI Information is passed to CFA!!!\n");
            return ELM_FAILURE;
        }

        pPortEntry->bElmStatusInformationChange = ELM_TRUE;

        ElmUpdateUniInfo (CfaUniInfo);
        ElmiUniStatusChangedTrap (u4IfIndex, ELM_PORT_TRAPS_OID,
                                  ELM_PORT_TRAPS_OID_LEN);
    }
    else
    {
        ELM_TRC (ELM_MGMT_TRC,
                 "UNI Status is Same as in received ELMI PDU!!!\n");
    }
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmHandleEvcStatusTlv                                */
/*                                                                           */
/* Description        : This Function handle EVC Status Tlv & this is called */
/*                      by ElmParseRcvdTLV function                          */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port Number                               */
/*                      u1EvcInfoLength = Length of EVC Status               */
/*                      u1ReportType  Report type value                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT4
ElmHandleEvcStatusTlv (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT1 u1EvcInfoLength,
                       UINT1 u1ReportType)
{
    tLcmEvcStatusInfo   LcmEvcStatusInfo;
    tLcmEvcStatusInfo   LcmEvcStatusInfoInDb;
    tElmPortEntry      *pElmPortEntry = NULL;
    tLcmEvcInfo        *pLcmEvcInfo = NULL;
    tLcmEvcInfo        *pElmNextEvcInfo = NULL;

    ELM_TRC (ELM_MGMT_TRC, "ElmHandleEvcStatusTlv!!!\n");
    ELM_MEMSET (&LcmEvcStatusInfo, (UINT1) ELM_INIT_VAL,
                sizeof (tLcmEvcStatusInfo));
    ELM_MEMSET (&LcmEvcStatusInfoInDb, (UINT1) ELM_INIT_VAL,
                sizeof (tLcmEvcStatusInfo));
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_MGMT_TRC, "No Port Information present\n");
        return ELM_FAILURE;
    }

    ELM_GET_2BYTE (LcmEvcStatusInfo.u2EvcReferenceId, pu1Buf);
    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                  "The ELMI PDU Contains the EVC with Refrence ID %d \n",
                  LcmEvcStatusInfo.u2EvcReferenceId);

    ELM_GET_1BYTE (LcmEvcStatusInfo.u1EvcStatus, pu1Buf);
    /* when the EVC with lower Refrence ID is received after the higer then ELMI 
     * PDU is discarded.
     */

    if (LcmEvcStatusInfo.u2EvcReferenceId == ELM_INIT_VAL)
    {

        ELM_TRC (ELM_MGMT_TRC,
                 "ERR:ELMI PDU is Received with Refrence Id"
                 "ZERO.This EVC is Discarded \n");

        ELM_INCR_INVALID_EVC_REF_ID_COUNT (u4IfIndex);
        if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength, (UINT1) u1EvcInfoLength) ==
            ELM_FAILURE)
        {
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_TRC (ELM_ALL_FAILURE_TRC,
                     "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
            return ELM_FAILURE;
        }
        return ELM_SUCCESS;
    }
    if ((u1ReportType == ELM_FULL_STATUS) ||
        (u1ReportType == ELM_FULL_STATUS_CONTINUED))
    {
        if (LcmEvcStatusInfo.u2EvcReferenceId <
            pElmPortEntry->u2LastReceivedRefrenceID)
        {
            ElmInvalidPduRxdTrap (u4IfIndex,
                                  ELM_PROTOCOL_ERROR_TYPE,
                                  ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
            ELM_INCR_INVALID_EVC_REF_ID_COUNT (u4IfIndex);
            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
            ELM_TRC (ELM_MGMT_TRC,
                     "ERR:ELMI PDU is Discarded due to Lower EVC"
                     "refrence ID received after higher EVC ID \n");
            return ELM_FAILURE;
        }
        if (LcmEvcStatusInfo.u2EvcReferenceId ==
            pElmPortEntry->u2LastReceivedRefrenceID)
        {

            ELM_TRC (ELM_MGMT_TRC,
                     "ELMI PDU is Received with Refrence Id"
                     "same as last received EVC refrence ID \n");

            if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength, u1EvcInfoLength) ==
                ELM_FAILURE)
            {
                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                return ELM_FAILURE;
            }
            return ELM_SUCCESS;
        }
    }

    if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength, (UINT1) ELM_DECR_DATALEN_THREE)
        == ELM_FAILURE)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ELMI PDU receieved with invalid length!!!\n");
        return ELM_FAILURE;
    }
    /* When the report type is single Evc asyhronous then pass the EVC status to 
     * LCM module.
     */
    if (ELM_ALLOC_LCM_EVC_INFO (pLcmEvcInfo) == NULL)
    {
        ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                 "pLcmEvcInfo:Memory Allocation failed\n");

        return ELM_FAILURE;
    }
    ELM_MEMSET (pLcmEvcInfo, (UINT1) ELM_INIT_VAL, sizeof (tLcmEvcInfo));

    if (u1ReportType == ELM_ASYNCHRONOUS_STATUS)
    {
        switch (LcmEvcStatusInfo.u1EvcStatus)
        {
            case ELM_EVC_NOT_ACTIVE:

                /* Get evc info from LCM if this EVC is present there changed 
                 * the  Evc Status and store new info in LCM 
                 */
                if ((ElmGetEvcInfo
                     (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                      pLcmEvcInfo)) != LCM_FAILURE)
                {
                    if (pLcmEvcInfo->LcmEvcStatusInfo.u1EvcStatus !=
                        LcmEvcStatusInfo.u1EvcStatus)
                    {
                        ElmElmiEvcStatusChangeIndication (u4IfIndex,
                                                          LcmEvcStatusInfo.
                                                          u2EvcReferenceId,
                                                          LcmEvcStatusInfo.
                                                          u1EvcStatus);
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "The Status of EVC Refrance Id %d is Changed \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_TRC (ELM_MGMT_TRC, "Evc Status is changed \n");
                        ElmiEvcStatusChangedTrap (u4IfIndex,
                                                  LcmEvcStatusInfo.
                                                  au1EvcIdentifier,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                    }
                    else
                    {
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "The Status of EVC Refrance Id %d is same as the status in LCM  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                    }
                }
                else
                {
                    ELM_TRC (ELM_MGMT_TRC,
                             "The status of EVC is ELM_EVC_NOT_ACTIVE \n");
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC Refrance Id %d is not present in LCM Database \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                }

                break;

            case ELM_EVC_ACTIVE_EVC:
            case ELM_EVC_PARTIALLY_ACTIVE:
                /* Get evc info from LCM if this EVC is present there changed 
                 * the  Evc Status and store new info in LCM 
                 */
                if ((ElmGetEvcInfo
                     (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                      pLcmEvcInfo)) != LCM_FAILURE)
                {
                    if (pLcmEvcInfo->LcmEvcStatusInfo.u1EvcStatus !=
                        LcmEvcStatusInfo.u1EvcStatus)
                    {
                        ElmElmiEvcStatusChangeIndication (u4IfIndex,
                                                          LcmEvcStatusInfo.
                                                          u2EvcReferenceId,
                                                          LcmEvcStatusInfo.
                                                          u1EvcStatus);
                        ELM_TRC (ELM_MGMT_TRC, "Evc Status is changed \n");
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "The Status of EVC Refrance Id %d is Changed \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ElmiEvcStatusChangedTrap (u4IfIndex,
                                                  LcmEvcStatusInfo.
                                                  au1EvcIdentifier,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                    }
                    else
                    {
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "The Status of EVC Refrance Id %d is same as the status in LCM  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                    }

                }
                else
                {
                    /* generate an error and add the EVC in LCM data base. */
                    ElmInvalidPduRxdTrap (u4IfIndex,
                                          ELM_PROTOCOL_ERROR_TYPE,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "currently not present in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);

                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                }
                break;

            case ELM_EVC_NEW_ACTIVE:

                /* New Evc is created which is active so generate the trap 
                 * message new evc created & add this evc to LCM data base 
                 */
                if ((ElmGetEvcInfo
                     (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                      pLcmEvcInfo)) != LCM_FAILURE)
                {
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ElmDeleteEvcIndication (u4IfIndex,
                                            LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "EVC is already Present in LCM data base. \n");
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "EVC with Refrence ID %d is Deleted From LCM data base. \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "added in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                }
                else
                {

                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "currently not present in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "New Evc Info with Refrence ID %d is added to LCM Database \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "New EVC is created which is Active");
                }
                break;

            case ELM_EVC_NEW_NOT_ACTIVE:

                /* New Evc is created which is not active so generate the
                 * trap message new evc created & add this evc to LCM data 
                 * base.
                 */
                if ((ElmGetEvcInfo
                     (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                      pLcmEvcInfo)) != LCM_FAILURE)
                {
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ElmDeleteEvcIndication (u4IfIndex,
                                            LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "EVC is already Present in LCM data base. \n");
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "EVC with Refrence ID %d is Deleted From LCM data base. \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "added in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                }
                else
                {

                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "currently not present in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "New Evc Info is with Evc Refrence ID %d is added to LCM Database  \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "New Evc is created which is Not Active\n");
                }
                break;

            case ELM_EVC_NEW_PARTIALLY_ACTIVE:

                /* New Evc is created which is partially active so generate  
                 * the trap message new evc created & add this evc to LCM    
                 * data base.
                 */
                if ((ElmGetEvcInfo
                     (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                      pLcmEvcInfo)) != LCM_FAILURE)
                {
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ElmDeleteEvcIndication (u4IfIndex,
                                            LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "EVC is already Present in LCM data base. \n");
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "EVC is with Refrence ID %d is Deleted From LCM data base. \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "added in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                }
                else
                {

                    ElmiNewEvcCreatedTrap (u4IfIndex,
                                           LcmEvcStatusInfo.au1EvcIdentifier,
                                           ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmCreateNewEvcIndication (u4IfIndex, &LcmEvcStatusInfo);
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "The EVC reference ID %d received in Asynchronous Message is"
                                  "currently not present in LCM Database.\n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                  LcmEvcStatusInfo.u2EvcReferenceId);
                    ELM_TRC (ELM_MGMT_TRC,
                             "New EVC is created which is Partially Active");
                }
                break;

            default:

                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                      ELM_PORT_TRAPS_OID,
                                      ELM_PORT_TRAPS_OID_LEN);
                ELM_TRC (ELM_MGMT_TRC,
                         "ERR:Evc Status Type Element is wrong in Asynchronous Status Message\n");
                ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                return ELM_FAILURE;

        }
        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
        /* In Single EVC asynchronous Status Messsage only one EVC information  
         * is present so after getting this Information Pass this to LCM.
         */
        return ELM_SUCCESS;
    }
    /* if the Length is zero For Full Status  Mandatory Subinfo elements are 
     * missing in ELMI PDU and PDU is discarded.
     */
    if (u1EvcInfoLength == (UINT1) ELM_INIT_VAL)
    {
        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
        ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                              ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC (ELM_MGMT_TRC,
                 "ERR:Mandatory Sub Info Elemnt missing in EVC Status IE\n");
        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
        return ELM_FAILURE;
    }

    /* Fetch the information of the EVC, with the received reference ID,
     * from the LCM database. This will help in finding out any change in the 
     * status. If there is any change in the information then the database update
     * indication is required to be sent. In the case of HA the PDU with the changed 
     * information will be required to sent to the standby
     */
    ElmGetEvcStatusInfo (u4IfIndex, LcmEvcStatusInfo.u2EvcReferenceId,
                         &LcmEvcStatusInfoInDb);

    /* EVC status IE in Full Status or Full Status Continued contains 3 subinfo 
     * TLVs 
     */
    if (ElmParseEvcSubInfo
        (pu1Buf, u4IfIndex, u1EvcInfoLength, &LcmEvcStatusInfo) == ELM_FAILURE)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC,
                 "ERR:ElmParseEvcSubInfo function has returned failure \n");
        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
        return ELM_FAILURE;
    }

    if ((u1ReportType == ELM_FULL_STATUS) ||
        (u1ReportType == ELM_FULL_STATUS_CONTINUED))
    {
        /* get the EVC info from Lcm data base */
        if (ElmGetNextEvcInfo
            (u4IfIndex, pElmPortEntry->u2LastReceivedRefrenceID,
             pLcmEvcInfo) != LCM_FAILURE)
        {
            if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId == ELM_INIT_VAL)
            {
                switch (LcmEvcStatusInfo.u1EvcStatus)
                {
                    case ELM_EVC_ACTIVE_EVC:
                    case ELM_EVC_PARTIALLY_ACTIVE:
                        /* When the Active Bit is set & this EvC is 
                         * Currently not present in the LCM data base  
                         * then Raise an error and add the EVC to LCM .
                         * data base.
                         */
                        ElmInvalidPduRxdTrap (u4IfIndex,
                                              ELM_PROTOCOL_ERROR_TYPE,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is received with Active bit" "set\n");
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is not present in LCM data base. \n");
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        break;
                    case ELM_EVC_NEW_ACTIVE:
                    case ELM_EVC_NEW_PARTIALLY_ACTIVE:
                    case ELM_EVC_NEW_NOT_ACTIVE:
                        /* When the Received EVC is already present in the  
                         * LCM data base and new bit is set one then 
                         * delete the EVC first from LCM & then ADd the 
                         * EVC information received in STATUS message.
                         */

                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is added in LCM data base. \n");
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        break;
                    case ELM_EVC_NOT_ACTIVE:
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);

                        ELM_TRC (ELM_MGMT_TRC,
                                 "New EVC is created which is"
                                 "Not Active::\n");
                        break;
                    default:

                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EvcStatus Type Element is wrong \n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;

                }

            }
            else if (LcmEvcStatusInfo.u2EvcReferenceId <
                     pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId)
            {
                switch (LcmEvcStatusInfo.u1EvcStatus)
                {
                    case ELM_EVC_ACTIVE_EVC:
                    case ELM_EVC_PARTIALLY_ACTIVE:
                        /* When the Active Bit is set & this EvC is 
                         * Currently not present in the LCM data base  
                         * then Raise an error and add the EVC to LCM .
                         * data base.
                         */
                        ElmInvalidPduRxdTrap (u4IfIndex,
                                              ELM_PROTOCOL_ERROR_TYPE,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is received with Active bit" "set\n");
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is not present in LCM data base. \n");
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        pElmPortEntry->u2LastReceivedRefrenceID =
                            LcmEvcStatusInfo.u2EvcReferenceId;
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    case ELM_EVC_NEW_ACTIVE:
                    case ELM_EVC_NEW_PARTIALLY_ACTIVE:
                    case ELM_EVC_NEW_NOT_ACTIVE:
                        /* When the Received EVC is already present in the  
                         * LCM data base and new bit is set one then 
                         * delete the EVC first from LCM & then ADd the 
                         * EVC information received in STATUS message.
                         */

                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is added in LCM data base. \n");
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        pElmPortEntry->u2LastReceivedRefrenceID =
                            LcmEvcStatusInfo.u2EvcReferenceId;
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    case ELM_EVC_NOT_ACTIVE:

                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                        pElmPortEntry->u2LastReceivedRefrenceID =
                            LcmEvcStatusInfo.u2EvcReferenceId;
                        ELM_TRC (ELM_MGMT_TRC,
                                 "New EVC is created which is"
                                 "Not Active::\n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    default:

                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "ERR:EvcStatus Type Element is wrong \n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;

                }

            }
            else if (LcmEvcStatusInfo.u2EvcReferenceId ==
                     pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId)
            {
                switch (LcmEvcStatusInfo.u1EvcStatus)
                {
                    case ELM_EVC_NOT_ACTIVE:
                    case ELM_EVC_ACTIVE_EVC:
                    case ELM_EVC_PARTIALLY_ACTIVE:
                        /* When the Received EVC is already present in the  
                         * LCM data base then compare the status & give the 
                         * status change indication.
                         */
                        if (pLcmEvcInfo->LcmEvcStatusInfo.u1EvcStatus !=
                            LcmEvcStatusInfo.u1EvcStatus)
                        {
                            ELM_TRC (ELM_MGMT_TRC, "Evc Status is changed \n");
                            ElmiEvcStatusChangedTrap (u4IfIndex,
                                                      LcmEvcStatusInfo.
                                                      au1EvcIdentifier,
                                                      ELM_PORT_TRAPS_OID,
                                                      ELM_PORT_TRAPS_OID_LEN);
                            ElmElmiEvcStatusChangeIndication (u4IfIndex,
                                                              LcmEvcStatusInfo.
                                                              u2EvcReferenceId,
                                                              LcmEvcStatusInfo.
                                                              u1EvcStatus);
                        }
                        break;
                    case ELM_EVC_NEW_ACTIVE:
                    case ELM_EVC_NEW_PARTIALLY_ACTIVE:
                    case ELM_EVC_NEW_NOT_ACTIVE:
                        /* When the Received EVC is already present in the  
                         * LCM data base and new bit is set one then 
                         * delete the EVC first from LCM & then ADd the 
                         * EVC information received in STATUS message.
                         */
                        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                        ElmDeleteEvcIndication (u4IfIndex,
                                                LcmEvcStatusInfo.
                                                u2EvcReferenceId);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "EVC is already Present in LCM data base. \n");
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "Evc Info with Refrence ID %d is deleted from LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        ElmiNewEvcCreatedTrap (u4IfIndex,
                                               LcmEvcStatusInfo.
                                               au1EvcIdentifier,
                                               ELM_PORT_TRAPS_OID,
                                               ELM_PORT_TRAPS_OID_LEN);
                        ElmCreateNewEvcIndication (u4IfIndex,
                                                   &LcmEvcStatusInfo);
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                      LcmEvcStatusInfo.u2EvcReferenceId);
                        break;
                    default:

                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_MGMT_TRC,
                                 "ERR:EvcStatus Type Element is wrong \n");
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_FAILURE;
                }
            }
            else
            {
                if (ELM_ALLOC_LCM_EVC_INFO (pElmNextEvcInfo) == NULL)
                {
                    ELM_TRC (ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                             "pElmNextEvcInfo:Memory Allocation failed\n");
                    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                    return ELM_FAILURE;
                }
                ELM_MEMSET (pElmNextEvcInfo, ELM_MEMSET_VAL,
                            sizeof (tLcmEvcInfo));

                pElmNextEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
                    pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;

                while (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId <
                       LcmEvcStatusInfo.u2EvcReferenceId)
                {
                    /* get the EVC info from Lcm data base */
                    if (ElmGetNextEvcInfo
                        (u4IfIndex,
                         pElmNextEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId,
                         pElmNextEvcInfo) != LCM_SUCCESS)
                    {
                        ELM_RELEASE_LCM_EVC_INFO (pElmNextEvcInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        /* Invalid condition */
                        return ELM_FAILURE;
                    }

                    ELM_TRC (ELM_MGMT_TRC, "Evc is deleted from UNI-C  \n");
                    /* Delete the evc From the LCM data base 
                     */
                    pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
                    ElmiOldEvcDeletedTrap (u4IfIndex,
                                           pLcmEvcInfo->LcmEvcStatusInfo.
                                           au1EvcIdentifier, ELM_PORT_TRAPS_OID,
                                           ELM_PORT_TRAPS_OID_LEN);
                    ElmDeleteEvcIndication (u4IfIndex,
                                            pLcmEvcInfo->LcmEvcStatusInfo.
                                            u2EvcReferenceId);
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "Evc Info with Refrence ID %d is deleted from LCM DataBase  \n",
                                  pLcmEvcInfo->LcmEvcStatusInfo.
                                  u2EvcReferenceId);
                    ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                    pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId =
                        pElmNextEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;
                    pElmPortEntry->u2LastReceivedRefrenceID =
                        pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;
                    ELM_MEMCPY (pLcmEvcInfo->LcmEvcStatusInfo.au1EvcIdentifier,
                                pElmNextEvcInfo->LcmEvcStatusInfo.
                                au1EvcIdentifier, ELM_MAX_EVC_LENGTH);

                    if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId ==
                        ELM_INIT_VAL)
                    {
                        switch (LcmEvcStatusInfo.u1EvcStatus)
                        {
                            case ELM_EVC_ACTIVE_EVC:
                            case ELM_EVC_PARTIALLY_ACTIVE:
                                /* When the Active Bit is set & this EvC is 
                                 * Currently not present in the LCM data base  
                                 * then Raise an error and add the EVC to LCM .
                                 * data base.
                                 */
                                ElmInvalidPduRxdTrap (u4IfIndex,
                                                      ELM_PROTOCOL_ERROR_TYPE,
                                                      ELM_PORT_TRAPS_OID,
                                                      ELM_PORT_TRAPS_OID_LEN);
                                ELM_TRC (ELM_MGMT_TRC,
                                         "EVC is received with Active bit"
                                         "set\n");
                                ELM_TRC (ELM_MGMT_TRC,
                                         "EVC is not present in LCM data base. \n");
                                ElmiNewEvcCreatedTrap (u4IfIndex,
                                                       LcmEvcStatusInfo.
                                                       au1EvcIdentifier,
                                                       ELM_PORT_TRAPS_OID,
                                                       ELM_PORT_TRAPS_OID_LEN);
                                ElmCreateNewEvcIndication (u4IfIndex,
                                                           &LcmEvcStatusInfo);
                                ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                              "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                              LcmEvcStatusInfo.
                                              u2EvcReferenceId);
                                ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                                break;
                            case ELM_EVC_NEW_ACTIVE:
                            case ELM_EVC_NEW_PARTIALLY_ACTIVE:
                            case ELM_EVC_NEW_NOT_ACTIVE:
                                /* When the Received EVC is already present in the  
                                 * LCM data base and new bit is set one then 
                                 * delete the EVC first from LCM & then ADd the 
                                 * EVC information received in STATUS message.
                                 */

                                ELM_TRC (ELM_MGMT_TRC,
                                         "EVC is added in LCM data base. \n");
                                ElmiNewEvcCreatedTrap (u4IfIndex,
                                                       LcmEvcStatusInfo.
                                                       au1EvcIdentifier,
                                                       ELM_PORT_TRAPS_OID,
                                                       ELM_PORT_TRAPS_OID_LEN);
                                ElmCreateNewEvcIndication (u4IfIndex,
                                                           &LcmEvcStatusInfo);
                                ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                              "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                              LcmEvcStatusInfo.
                                              u2EvcReferenceId);
                                ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);
                                break;
                            case ELM_EVC_NOT_ACTIVE:
                                ELM_TRC (ELM_MGMT_TRC,
                                         "New EVC is created which is"
                                         "Not Active::\n");
                                ElmiNewEvcCreatedTrap (u4IfIndex,
                                                       LcmEvcStatusInfo.
                                                       au1EvcIdentifier,
                                                       ELM_PORT_TRAPS_OID,
                                                       ELM_PORT_TRAPS_OID_LEN);
                                ElmCreateNewEvcIndication (u4IfIndex,
                                                           &LcmEvcStatusInfo);
                                ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                              "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                              LcmEvcStatusInfo.
                                              u2EvcReferenceId);
                                ELM_INC_NO_OF_EVC_CONFIGURED_COUNT (u4IfIndex);

                                break;
                            default:

                                ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                                ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                                ElmInvalidPduRxdTrap (u4IfIndex,
                                                      ELM_PROTOCOL_ERROR,
                                                      ELM_PORT_TRAPS_OID,
                                                      ELM_PORT_TRAPS_OID_LEN);
                                ELM_TRC (ELM_MGMT_TRC,
                                         "ERR:EvcStatus Type Element is wrong \n");
                                ELM_RELEASE_LCM_EVC_INFO (pElmNextEvcInfo);
                                ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                                return ELM_FAILURE;
                        }
                        pElmPortEntry->u2LastReceivedRefrenceID =
                            LcmEvcStatusInfo.u2EvcReferenceId;
                        ELM_RELEASE_LCM_EVC_INFO (pElmNextEvcInfo);
                        ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                        return ELM_SUCCESS;
                    }
                }
                if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId ==
                    LcmEvcStatusInfo.u2EvcReferenceId)
                {
                    switch (LcmEvcStatusInfo.u1EvcStatus)
                    {
                        case ELM_EVC_NOT_ACTIVE:
                        case ELM_EVC_ACTIVE_EVC:
                        case ELM_EVC_PARTIALLY_ACTIVE:
                            /* When the Received EVC is already present in the  
                             * LCM data base then compare the status & give the 
                             * status change indication.
                             */
                            if (pLcmEvcInfo->LcmEvcStatusInfo.u1EvcStatus !=
                                LcmEvcStatusInfo.u1EvcStatus)
                            {
                                ELM_TRC (ELM_MGMT_TRC,
                                         "Evc Status is changed \n");
                                ElmiEvcStatusChangedTrap (u4IfIndex,
                                                          LcmEvcStatusInfo.
                                                          au1EvcIdentifier,
                                                          ELM_PORT_TRAPS_OID,
                                                          ELM_PORT_TRAPS_OID_LEN);
                                ElmElmiEvcStatusChangeIndication (u4IfIndex,
                                                                  LcmEvcStatusInfo.
                                                                  u2EvcReferenceId,
                                                                  LcmEvcStatusInfo.
                                                                  u1EvcStatus);
                            }
                            break;
                        case ELM_EVC_NEW_ACTIVE:
                        case ELM_EVC_NEW_PARTIALLY_ACTIVE:
                        case ELM_EVC_NEW_NOT_ACTIVE:
                            /* When the Received EVC is already present in the  
                             * LCM data base and new bit is set one then 
                             * delete the EVC first from LCM & then ADd the 
                             * EVC information received in STATUS message.
                             */
                            pElmPortEntry->bElmStatusInformationChange =
                                ELM_TRUE;
                            ElmDeleteEvcIndication (u4IfIndex,
                                                    LcmEvcStatusInfo.
                                                    u2EvcReferenceId);
                            ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                          "Evc Info with Refrence ID %d is deleted from LCM DataBase  \n",
                                          LcmEvcStatusInfo.u2EvcReferenceId);
                            ELM_TRC (ELM_MGMT_TRC,
                                     "EVC is already Present in LCM data base. \n");
                            ElmiNewEvcCreatedTrap (u4IfIndex,
                                                   LcmEvcStatusInfo.
                                                   au1EvcIdentifier,
                                                   ELM_PORT_TRAPS_OID,
                                                   ELM_PORT_TRAPS_OID_LEN);
                            ElmCreateNewEvcIndication (u4IfIndex,
                                                       &LcmEvcStatusInfo);
                            ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                          "New Evc Info with Refrence ID %d is added to LCM DataBase  \n",
                                          LcmEvcStatusInfo.u2EvcReferenceId);
                            break;
                        default:

                            ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                            ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                                  ELM_PORT_TRAPS_OID,
                                                  ELM_PORT_TRAPS_OID_LEN);
                            ELM_TRC (ELM_MGMT_TRC,
                                     "ERR:EvcStatus Type Element is wrong \n");
                            ELM_RELEASE_LCM_EVC_INFO (pElmNextEvcInfo);
                            ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
                            return ELM_FAILURE;
                    }
                }
            }
        }
        else
        {
            ELM_TRC (ELM_MGMT_TRC, "ERR:Invalid Condition in UNI-C  \n");

        }

        if (pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId == ELM_INIT_VAL)
        {
            pElmPortEntry->u2LastReceivedRefrenceID =
                LcmEvcStatusInfo.u2EvcReferenceId;
        }
        else
        {
            pElmPortEntry->u2LastReceivedRefrenceID =
                pLcmEvcInfo->LcmEvcStatusInfo.u2EvcReferenceId;
        }
    }

    /* The EVC info has been parsed. Compare it with the
     * info present in the database
     */
    if (MEMCMP
        (&LcmEvcStatusInfo, &LcmEvcStatusInfoInDb,
         sizeof (tLcmEvcStatusInfo)) != ELM_INIT_VAL)
    {
        /* Received information differs from what is stored in the database */
        ELM_TRC (ELM_CONTROL_PATH_TRC, "EVC Status Info changed\n");
        pElmPortEntry->bElmStatusInformationChange = ELM_TRUE;
        if ((LcmEvcStatusInfoInDb.u1EvcStatus & ELM_MASK_FIRST_BIT) != ELM_TRUE)
        {
            ElmDatabaseUpdateIndication (u4IfIndex, &LcmEvcStatusInfo);
        }
    }
    ELM_RELEASE_LCM_EVC_INFO (pElmNextEvcInfo);
    ELM_RELEASE_LCM_EVC_INFO (pLcmEvcInfo);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmParseUniSubInfo                                   */
/*                                                                           */
/* Description        : This Function handle Uni Sub Info Tlv & this is      */
/*                      called by ElmHandleUniStatusTlv function.            */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port Number                               */
/*                      u1ElmUniInfoLength = Length of  Status               */
/*                      u1ReportType  Report type value                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE, ELM_SUCCESS                             */
/*****************************************************************************/
INT1
ElmParseUniSubInfo (UINT1 *pu1Buf, UINT4 u4IfIndex,
                    UINT1 u1ElmUniInfoLength, tCfaUniInfo * pElmUniInfo)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               au1ElmUniSubInfo
        [MANDATORY_SUB_INFO_TLV_UNI_STATUS_IE] = { 0x51, 0x71 };
    UINT1               au1ElmUniTlvRcvd
        [MANDATORY_SUB_INFO_TLV_UNI_STATUS_IE] = { 0x00, 0x00 };
    UINT1               u1RcvdTlvType = (UINT1) ELM_INIT_VAL;
    UINT1               u1LocalTlvIndex = (UINT1) ELM_INIT_VAL;
    UINT1               u1NoOfTlv = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmUniIdSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmUniBwSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmUnRecInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1CmCfPerCosBitFinder = (UINT1) ELM_INIT_VAL;
    UINT1               u1CurrentCmCfPerCosBitFinder = (UINT1) ELM_INIT_VAL;
    UINT1               u1ErrVal = 0;
    u1NoOfTlv = MANDATORY_SUB_INFO_TLV_UNI_STATUS_IE;
    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Information present!!!\n");
        return ELM_FAILURE;
    }

    while (u1ElmUniInfoLength != ELM_INIT_VAL)
    {
        ELM_GET_1BYTE (u1RcvdTlvType, pu1Buf);
        u1ErrVal = (UINT1) ELM_DECR_DATA_LENGTH
            (u1ElmUniInfoLength, (UINT1) ELM_DECR_DATALEN_ONE);
        UNUSED_PARAM (u1ErrVal);
        /*  {
           ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
           ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
           ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
           ELM_TRC (ELM_ALL_FAILURE_TRC,
           "ERR:ELMI PDU receieved with invalid length!!!\n");
           return ELM_FAILURE;
           } */
        switch (u1RcvdTlvType)
        {
            case ELM_UNI_IE:

                ELM_GET_1BYTE (u1ElmUniIdSubInfoLength, pu1Buf);
                if (ELM_DECR_DATA_LENGTH (u1ElmUniInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    return ELM_FAILURE;
                }

                if (au1ElmUniTlvRcvd[0] == ELM_TRUE)
                {
                    /* Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU Contains Duplicate UNI Idetifier"
                             "sub Information element!!!\n");
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    u1ElmUniInfoLength = u1ElmUniInfoLength -
                        u1ElmUniIdSubInfoLength;
                    continue;
                }

                if (((u1LocalTlvIndex > 0) && (u1LocalTlvIndex < 2)) &&
                    (au1ElmUniSubInfo[u1LocalTlvIndex] != u1RcvdTlvType) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u1ElmUniInfoLength >= u1ElmUniIdSubInfoLength) &&
                        (*(pu1Buf + u1ElmUniIdSubInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }

                    return ELM_FAILURE;
                }

                /* Length of UNI ID can be maxmium 64 if it is more then 64    
                 * then  only first 64 character will be considered as UNI ID
                 */
                if (u1ElmUniIdSubInfoLength >= ELM_MAX_LENGTH_UNI_NAME)
                {
                    ELM_MEMCPY (pElmUniInfo->au1UniName, pu1Buf,
                                ELM_MAX_LENGTH_UNI_NAME - 1);
                    pElmUniInfo->au1UniName[ELM_MAX_LENGTH_UNI_NAME - 1] = '\0';
                }
                else
                {
                    ELM_MEMCPY (pElmUniInfo->au1UniName, pu1Buf,
                                u1ElmUniIdSubInfoLength);
                    pElmUniInfo->au1UniName[u1ElmUniIdSubInfoLength] = '\0';
                }

                pu1Buf = pu1Buf + u1ElmUniIdSubInfoLength;
                if (ELM_DECR_DATA_LENGTH (u1ElmUniInfoLength,
                                          u1ElmUniIdSubInfoLength) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    return ELM_FAILURE;
                }
                au1ElmUniTlvRcvd[0] = ELM_TRUE;
                break;

            case ELM_BW_PROFILE_IE:

                ELM_GET_1BYTE (u1ElmUniBwSubInfoLength, pu1Buf);
                /*validate bandwidth profile subinfo Length */
                if (u1ElmUniBwSubInfoLength != ELM_BW_PROFILE_IE_LENGTH)
                {
                    ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                             "ERR:Received ELMI PDU Contains invalid Bandwidth"
                             "Profile Subinfolength\n");
                    return ELM_FAILURE;
                }
                if (ELM_DECR_DATA_LENGTH (u1ElmUniInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    return ELM_FAILURE;
                }

                if (au1ElmUniTlvRcvd[1] == ELM_TRUE)
                {
                    /* Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    u1ElmUniInfoLength = u1ElmUniInfoLength -
                        u1ElmUniBwSubInfoLength;
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU Contains Duplicate Band Width Profile"
                             "sub Information element!!!\n");
                    continue;
                }

                if (((u1LocalTlvIndex > 0) && (u1LocalTlvIndex < 2)) &&
                    (au1ElmUniSubInfo[u1LocalTlvIndex] != u1RcvdTlvType &&
                     (u1LocalTlvIndex < u1NoOfTlv)))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u1ElmUniInfoLength >= u1ElmUniBwSubInfoLength) &&
                        (*(pu1Buf + u1ElmUniBwSubInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }

                    return ELM_FAILURE;
                }

                ELM_GET_1BYTE (u1CmCfPerCosBitFinder, pu1Buf);
                u1CurrentCmCfPerCosBitFinder = u1CmCfPerCosBitFinder;
                u1CmCfPerCosBitFinder = u1CmCfPerCosBitFinder &
                    (UINT1) ELM_PER_COS_BIT_FINDER;
                pElmUniInfo->UniBwProfile.u1PerCosBit = u1CmCfPerCosBitFinder;
                u1CmCfPerCosBitFinder = u1CurrentCmCfPerCosBitFinder;
                u1CmCfPerCosBitFinder = u1CmCfPerCosBitFinder &
                    (UINT1) ELM_PER_CF_BIT_FINDER;
                pElmUniInfo->UniBwProfile.u1Cf = u1CmCfPerCosBitFinder;

                u1CmCfPerCosBitFinder = u1CurrentCmCfPerCosBitFinder;
                u1CmCfPerCosBitFinder = u1CmCfPerCosBitFinder &
                    (UINT1) ELM_PER_CM_BIT_FINDER;
                pElmUniInfo->UniBwProfile.u1Cm = u1CmCfPerCosBitFinder;

                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1CirMagnitude,
                               pu1Buf);

                ELM_GET_2BYTE (pElmUniInfo->UniBwProfile.u2CirMultiplier,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1CbsMagnitude,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1CbsMultiplier,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1EirMagnitude,
                               pu1Buf);
                ELM_GET_2BYTE (pElmUniInfo->UniBwProfile.u2EirMultiplier,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1EbsMagnitude,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1EbsMultiplier,
                               pu1Buf);
                ELM_GET_1BYTE (pElmUniInfo->UniBwProfile.u1PriorityBits,
                               pu1Buf);

                u1ElmUniInfoLength = u1ElmUniInfoLength -
                    u1ElmUniBwSubInfoLength;
                au1ElmUniTlvRcvd[1] = ELM_TRUE;
                break;

            default:

                ELM_GET_1BYTE (u1ElmUnRecInfoLength, pu1Buf);
                ELM_INCR_PROT_ERR_UNRECOGNISED_IE_RX_COUNT (u4IfIndex);
                if (ELM_DECR_DATA_LENGTH (u1ElmUniInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    return ELM_FAILURE;
                }

                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ELMI PDU Contains Unrecognized UNI Idetifier"
                         "sub Information element!!!\n");
                /* Increment the buffer pointer and decrement the total length
                 * to be parsed
                 */
                pu1Buf = pu1Buf + u1ElmUnRecInfoLength;
                u1ElmUniInfoLength = u1ElmUniInfoLength - u1ElmUnRecInfoLength;
                continue;
        }
        /*Increment the No Of TLV Parsed Counter */
        ELM_INCR_NO_OF_LOCAL_TLV_PARSED (u1LocalTlvIndex);

        if (u1LocalTlvIndex == u1NoOfTlv)
        {
            if (u1ElmUniInfoLength == ELM_INIT_VAL)
            {
                return ELM_SUCCESS;
            }
        }
    }                            /* while End */

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmParseEvcSubInfo                                   */
/*                                                                           */
/* Description        : This Function handle Evc Sub Info Tlv & This is      */
/*                      called by ElmHandleEvcStatusTlv function.            */
/*                                                                           */
/* Input(s)           : pu1Buf  - Pointer to E-LMI-PDU                       */
/*                      u4IfIndex - Port Number                               */
/*                      u1EvcInfoLength = Length of EVC Status               */
/*                      pLcmEvcStatusInfo  Pointer to Evc Status info        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_FAILURE/ELM_SUCCESS                              */
/*****************************************************************************/
INT1
ElmParseEvcSubInfo (UINT1 *pu1Buf, UINT4 u4IfIndex,
                    UINT1 u1EvcInfoLength,
                    tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    UINT1               au1ElmEvcSubInfo[MANDATORY_SUB_INFO_TLV_EVC_STATUS_IE] =
        { 0x61, 0x62, 0x71 };
    UINT1               au1ElmiTlvRcvd[MANDATORY_SUB_INFO_TLV_EVC_STATUS_IE]
        = { 0x00, 0x00, 0x00 };
    UINT1               u1RcvdTlvType = (UINT1) ELM_INIT_VAL;
    UINT1               u1LocalTlvIndex = (UINT1) ELM_INIT_VAL;
    UINT1               u1NoOfTlv = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmEvcIdSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmEvcParSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmEvcBandSubInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1Count = (UINT1) ELM_INIT_VAL;
    UINT1               u1CmCfPerCosBitFinder = (UINT1) ELM_INIT_VAL;
    UINT1               u1CirMagnitude = (UINT1) ELM_INIT_VAL;
    UINT2               u2CirMultiplier = (UINT2) ELM_INIT_VAL;
    UINT1               u1CbsMagnitude = (UINT1) ELM_INIT_VAL;
    UINT1               u1CbsMultiplier = (UINT1) ELM_INIT_VAL;
    UINT1               u1EirMagnitude = (UINT1) ELM_INIT_VAL;
    UINT2               u2EirMultiplier = (UINT2) ELM_INIT_VAL;
    UINT1               u1EbsMagnitude = (UINT1) ELM_INIT_VAL;
    UINT1               u1EbsMultiplier = (UINT1) ELM_INIT_VAL;
    UINT1               u1PriorityBits = (UINT1) ELM_INIT_VAL;
    UINT1               u1NoOfCos = (UINT1) ELM_INIT_VAL;
    UINT1               u1ElmUnRecInfoLength = (UINT1) ELM_INIT_VAL;
    UINT1               u1Index = (UINT1) ELM_INIT_VAL;
    UINT1               u1Cm = (UINT1) ELM_INIT_VAL;
    UINT1               u1Cf = (UINT1) ELM_INIT_VAL;
    UINT1               u1PerCosBit = (UINT1) ELM_INIT_VAL;
    UINT1               u1ErrVal = 0;

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        ELM_TRC (ELM_ALL_FAILURE_TRC, "ERR:No Port Entry Present!!!\n");
        return ELM_FAILURE;
    }

    /* The EVC SubInfo element should be parsed & ELMI PDU should be discarded  
     * when There is any error in SubInfo element then this function return     
     * ELM_FAILURE,when the complete subinfo will be parsed then function wil   
     * return ELM_SUCCESS.
     */
    u1NoOfTlv = (UINT1) MANDATORY_SUB_INFO_TLV_EVC_STATUS_IE;

    while (u1EvcInfoLength != ELM_INIT_VAL)
    {
        ELM_GET_1BYTE (u1RcvdTlvType, pu1Buf);

        u1ErrVal =
            (UINT1) ELM_DECR_DATA_LENGTH (u1EvcInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE);
        UNUSED_PARAM (u1ErrVal);
        /*  {
           ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
           ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
           ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
           ELM_TRC (ELM_ALL_FAILURE_TRC,
           "ERR:ELMI PDU receieved with invalid length!!!\n");
           return ELM_FAILURE;
           } */

        /* In EVC Status IE must have 3 SubInfo element 
         * 1. EVC parameter subinfo Element.
         * 2. EVC Reference ID SubInfo Element.
         * 3. Bandwidth Profile SubInfo Element.
         */
        switch (u1RcvdTlvType)
        {
            case ELM_EVC_PARAMS_IE:

                ELM_GET_1BYTE (u1ElmEvcParSubInfoLength, pu1Buf);
                ELM_GET_1BYTE (pLcmEvcStatusInfo->u1EvcType, pu1Buf);
                if (ELM_DECR_DATA_LENGTH
                    (u1EvcInfoLength,
                     (UINT1) ELM_DECR_DATALEN_TWO) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    return ELM_FAILURE;
                }

                if (au1ElmiTlvRcvd[0] == ELM_TRUE)
                {
                    /*Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    u1EvcInfoLength = u1EvcInfoLength -
                        u1ElmEvcParSubInfoLength;
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU Contains Evc Parameter Duplicate Sub "
                             "Information Element in EVC STTAUS IE !!!\n");
                    continue;
                }

                if (((u1LocalTlvIndex > 0) && (u1LocalTlvIndex < 3)) &&
                    (au1ElmEvcSubInfo[u1LocalTlvIndex] != u1RcvdTlvType) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u1EvcInfoLength >= u1ElmEvcParSubInfoLength) &&
                        (*(pu1Buf + u1ElmEvcParSubInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }

                    return ELM_FAILURE;
                }

                if (pLcmEvcStatusInfo->u1EvcType ==
                    ELM_EVC_TYPE_POINT2MULTIPOINT)
                {
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "Evc Type With Refrence ID %d is Point To MultiPoint\n",
                                  pLcmEvcStatusInfo->u2EvcReferenceId);
                }
                if (pLcmEvcStatusInfo->u1EvcType == ELM_EVC_TYPE_POINT2POINT)
                {
                    ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                  "Evc Type With Refrence ID %d is Point To Point\n",
                                  pLcmEvcStatusInfo->u2EvcReferenceId);
                }
                au1ElmiTlvRcvd[0] = ELM_TRUE;
                break;

            case ELM_EVC_IDENTIFIER_IE:

                ELM_GET_1BYTE (u1ElmEvcIdSubInfoLength, pu1Buf);
                if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    return ELM_FAILURE;
                }

                if (au1ElmiTlvRcvd[1] == ELM_TRUE)
                {
                    /*Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    u1EvcInfoLength = u1EvcInfoLength - u1ElmEvcIdSubInfoLength;

                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU Contains Evc Idetnifier Duplicate Sub "
                             "Information Element in EVC STTAUS IE !!!\n");
                    continue;
                }

                if (((u1LocalTlvIndex > 0) && (u1LocalTlvIndex < 3)) &&
                    (au1ElmEvcSubInfo[u1LocalTlvIndex] != u1RcvdTlvType) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */
                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    /* Check next TLV type for possible Out of Sequance IEs */
                    if ((u1EvcInfoLength >= u1ElmEvcIdSubInfoLength) &&
                        (*(pu1Buf + u1ElmEvcIdSubInfoLength) < u1RcvdTlvType))
                    {
                        ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT (u4IfIndex);
                    }

                    return ELM_FAILURE;
                }

                /* Length of EVC ID can be maxmium 100 if it is more then 100   
                 * then  only first 100 will be consider as ID
                 */
                if (u1ElmEvcIdSubInfoLength >= ELM_MAX_LENGTH_EVC_NAME)
                {
                    ELM_MEMCPY (pLcmEvcStatusInfo->au1EvcIdentifier, pu1Buf,
                                ELM_MAX_LENGTH_EVC_NAME - 1);
                    pLcmEvcStatusInfo->
                        au1EvcIdentifier[ELM_MAX_LENGTH_EVC_NAME - 1] = '\0';
                }
                else
                {
                    ELM_MEMCPY (pLcmEvcStatusInfo->au1EvcIdentifier, pu1Buf,
                                u1ElmEvcIdSubInfoLength);
                    pLcmEvcStatusInfo->au1EvcIdentifier[u1ElmEvcIdSubInfoLength]
                        = '\0';
                }

                pu1Buf = pu1Buf + u1ElmEvcIdSubInfoLength;
                if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength,
                                          u1ElmEvcIdSubInfoLength) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    return ELM_FAILURE;
                }
                au1ElmiTlvRcvd[1] = ELM_TRUE;

                break;

            case ELM_BW_PROFILE_IE:

                if (u1NoOfCos > (UINT1) ELM_MAX_BW_PROFILE_IE)
                {
                    /*Increment Duplicate IE received Counter */
                    /* Skip Duplicated IE */
                    ELM_INCR_DUPLICATE_IE (u4IfIndex);
                    ELM_GET_1BYTE (u1ElmEvcBandSubInfoLength, pu1Buf);
                    u1EvcInfoLength = u1EvcInfoLength -
                        (u1ElmEvcBandSubInfoLength + (UINT1) 1);
                    ELM_TRC_ARG1 (ELM_ALL_FAILURE_TRC,
                                  "ERR:EVC with refrence ID %d Contains more then 8 Evc BandWidth Profile Sub "
                                  "Information Element in EVC STTAUS IE !!!\n",
                                  pLcmEvcStatusInfo->u2EvcReferenceId);
                    continue;
                }
                if (((u1LocalTlvIndex > 0) && (u1LocalTlvIndex < 3)) &&
                    (au1ElmEvcSubInfo[u1LocalTlvIndex] != u1RcvdTlvType) &&
                    (u1LocalTlvIndex < u1NoOfTlv))
                {
                    /* Increment Invalid PDU received and Mandatory IE missing */
                    /* Increment Info Out of Sequence */

                    ELM_INCR_MISSING_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);

                    return ELM_FAILURE;
                }
                ELM_GET_1BYTE (u1ElmEvcBandSubInfoLength, pu1Buf);
                /* validate bandwidth profile subinfo Length */
                if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength,
                                          (UINT1) ELM_DECR_DATALEN_ONE) ==
                    ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved with invalid length!!!\n");
                    return ELM_FAILURE;
                }

                if (u1ElmEvcBandSubInfoLength != ELM_BW_PROFILE_IE_LENGTH)
                {
                    ELM_INCR_INVALID_MANDATORY_IE (u4IfIndex);
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC | ELM_MGMT_TRC,
                             "ERR:ELMI Pdu Contains invalid Bandwidth Profile"
                             "Subinfolength in EVC Status IE\n");
                    return ELM_FAILURE;
                }

                ELM_GET_1BYTE (u1CmCfPerCosBitFinder, pu1Buf);
                u1PerCosBit =
                    u1CmCfPerCosBitFinder & (UINT1) ELM_PER_COS_BIT_FINDER;
                u1Cf = u1CmCfPerCosBitFinder & (UINT1) ELM_PER_CF_BIT_FINDER;
                u1Cm = u1CmCfPerCosBitFinder & (UINT1) ELM_PER_CM_BIT_FINDER;

                ELM_GET_1BYTE (u1CirMagnitude, pu1Buf);
                ELM_GET_2BYTE (u2CirMultiplier, pu1Buf);
                ELM_GET_1BYTE (u1CbsMagnitude, pu1Buf);
                ELM_GET_1BYTE (u1CbsMultiplier, pu1Buf);
                ELM_GET_1BYTE (u1EirMagnitude, pu1Buf);
                ELM_GET_2BYTE (u2EirMultiplier, pu1Buf);
                ELM_GET_1BYTE (u1EbsMagnitude, pu1Buf);
                ELM_GET_1BYTE (u1EbsMultiplier, pu1Buf);
                ELM_GET_1BYTE (u1PriorityBits, pu1Buf);
                for (u1Index = (UINT1) ELM_MIN_PRIORITY;
                     u1Index <= (UINT1) ELM_MAX_PRIORITY; u1Index++)
                {
                    if (((u1PriorityBits >> u1Index) & 0x01) == 0x01)
                    {
                        u1PriorityBits = u1Index;
                        break;
                    }
                }
                /* If Per Cos Bit is set to Zero then only one bandwidth profile 
                 * TLV is present if Per Cos Bit is set to 1 then 7 More TLVs   
                 * will be present in ELMI PDU..
                 */

                if (u1PerCosBit == (UINT1) ELM_INIT_VAL)
                {
                    /* pu1Buf = pu1Buf + u1ElmEvcBandSubInfoLength; */
                    if (ELM_DECR_DATA_LENGTH (u1EvcInfoLength,
                                              u1ElmEvcBandSubInfoLength) ==
                        ELM_FAILURE)
                    {
                        ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                        ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                              ELM_PORT_TRAPS_OID,
                                              ELM_PORT_TRAPS_OID_LEN);
                        ELM_TRC (ELM_ALL_FAILURE_TRC,
                                 "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                        return ELM_FAILURE;
                    }
                    if ((u1Count == (UINT1) ELM_INIT_VAL))
                    {
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "Evc With Refrence ID %d contains single Bandwidth Profile IE\n",
                                      pLcmEvcStatusInfo->u2EvcReferenceId);
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1PerCosBit =
                            u1PerCosBit;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1Cf = u1Cf;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1Cm = u1Cm;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1CirMagnitude =
                            u1CirMagnitude;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u2CirMultiplier =
                            u2CirMultiplier;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1CbsMagnitude =
                            u1CbsMagnitude;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1CbsMultiplier =
                            u1CbsMultiplier;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1EirMagnitude =
                            u1EirMagnitude;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u2EirMultiplier =
                            u2EirMultiplier;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1EbsMagnitude =
                            u1EbsMagnitude;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1EbsMultiplier =
                            u1EbsMultiplier;
                        pLcmEvcStatusInfo->BwProfile[u1Count].u1PriorityBits =
                            u1PriorityBits;
                        ELM_INCR_NO_OF_BANDWIDTH_COUNT (u1Count);
                        pLcmEvcStatusInfo->u1NoOfCos = u1Count;
                        return ELM_SUCCESS;
                    }
                    if ((u1Count != (UINT1) ELM_INIT_VAL))
                    {
                        ELM_TRC_ARG1 (ELM_MGMT_TRC,
                                      "Evc With Refrence ID %d contains Multiple"
                                      " Bandwidth Profile IE with PCOS Bit ZERO\n",
                                      pLcmEvcStatusInfo->u2EvcReferenceId);
                        continue;
                    }
                }
                else
                {
                    u1EvcInfoLength =
                        u1EvcInfoLength - u1ElmEvcBandSubInfoLength;
                    u1Count = u1PriorityBits;
                    if ((u1Count >= ELM_NUM_BW_ELEMENTS))
                    {
                        break;
                    }
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1PerCosBit =
                        u1PerCosBit;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1Cf = u1Cf;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1Cm = u1Cm;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1CirMagnitude =
                        u1CirMagnitude;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u2CirMultiplier =
                        u2CirMultiplier;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1CbsMagnitude =
                        u1CbsMagnitude;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1CbsMultiplier =
                        u1CbsMultiplier;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1EirMagnitude =
                        u1EirMagnitude;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u2EirMultiplier =
                        u2EirMultiplier;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1EbsMagnitude =
                        u1EbsMagnitude;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1EbsMultiplier =
                        u1EbsMultiplier;
                    pLcmEvcStatusInfo->BwProfile[u1Count].u1PriorityBits =
                        u1PriorityBits;
                    ELM_INCR_NO_OF_BANDWIDTH_COUNT (u1Count);
                    u1NoOfCos = u1NoOfCos + (UINT1) 1;
                    pLcmEvcStatusInfo->u1NoOfCos = u1NoOfCos;
                    continue;
                }

                au1ElmiTlvRcvd[2] = ELM_TRUE;
                break;
            default:

                ELM_GET_1BYTE (u1ElmUnRecInfoLength, pu1Buf);
                ELM_INCR_PROT_ERR_UNRECOGNISED_IE_RX_COUNT (u4IfIndex);
                if (ELM_DECR_DATA_LENGTH
                    (u1EvcInfoLength,
                     (UINT1) ELM_DECR_DATALEN_ONE) == ELM_FAILURE)
                {
                    ELM_INCR_INVALID_MSG_RX_COUNT (u4IfIndex);
                    ElmInvalidPduRxdTrap (u4IfIndex, ELM_PROTOCOL_ERROR,
                                          ELM_PORT_TRAPS_OID,
                                          ELM_PORT_TRAPS_OID_LEN);
                    ELM_TRC (ELM_ALL_FAILURE_TRC,
                             "ERR:ELMI PDU receieved invalid length in TLV!!!\n");
                    return ELM_FAILURE;
                }
                /* Increment the buffer pointer and decrement the total length
                 * to be parsed
                 */
                pu1Buf = pu1Buf + u1ElmUnRecInfoLength;
                u1EvcInfoLength = u1EvcInfoLength - u1ElmUnRecInfoLength;
                ELM_TRC (ELM_ALL_FAILURE_TRC,
                         "ELMI PDU Contains Unrecognized Sub "
                         "Information Element in EVC STATUS IE !!!\n");
                continue;
        }
        /* Increment the No Of TLV Parsed Counter */
        ELM_INCR_NO_OF_LOCAL_TLV_PARSED (u1LocalTlvIndex);
        if (u1LocalTlvIndex == u1NoOfTlv)
        {
            if (u1EvcInfoLength == ELM_INIT_VAL)
            {
                return ELM_SUCCESS;
            }
        }
    }
    return ELM_SUCCESS;
}
