/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmport.c,v 1.8 2008/10/13 14:01:21 prabuc-iss Exp $
 *
 * Description: This file contains wrapper functions defined to call the
 *              external apis by UNI-C & UNI-N
 * 
 *******************************************************************/

#define _ELMIPORT_C_
#include "elminc.h"

#ifdef LLDP_WANTED
#include "lldp.h"
#endif

/*****************************************************************************/
/* Function Name      : ElmGetNextEvcStatusInfo                              */
/*                                                                           */
/* Description        : This function is called to fetch the EVC status info */
/*                      from LCM of the EVC next to the one whose ref id is  */
/*                      provided                                             */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - EVC Ref ID                              */
/*                      pLcmEvcStatusInfo - EVC status info structure        */
/*                      pu1NoEvcInfo - No of EVCs whose status is passed     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetNextEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcStatusInfo * pLcmEvcStatusInfo,
                         UINT1 *pu1NoEvcInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetNextEvcStatusInfo (u2PortNo, u2EvcRefId,
                                        pLcmEvcStatusInfo, pu1NoEvcInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetEvcStatusInfo                                  */
/*                                                                           */
/* Description        : This function is called to fetch the EVC status info */
/*                      from LCM of the EVC whose ref id is provided         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - EVC Ref ID                              */
/*                      pLcmEvcStatusInfo - EVC status info structure        */
/*                      pu1NoEvcInfo - No of EVCs whose status is passed     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetEvcStatusInfo (u2PortNo, u2EvcRefId, pLcmEvcStatusInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetEvcCeVlanInfo                                  */
/*                                                                           */
/* Description        : This function is called to fetch the CE vlan info of */
/*                      VLANS mapped on the EVC provided                     */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - EVC Ref ID                              */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetEvcCeVlanInfo (u2PortNo, u2EvcRefId, pLcmEvcCeVlanInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetNextEvcCeVlanInfo                              */
/*                                                                           */
/* Description        : This function is called to fetch the CE vlan info of */
/*                      VLANS mapped on next the EVC provided                */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - EVC Ref ID                              */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure           */
/*                      pu1NoEvcInfo- No of EVCs for which VLAN info is given*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetNextEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo,
                         UINT1 *pu1NoEvcInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetNextEvcCeVlanInfo (u2PortNo, u2EvcRefId,
                                        pLcmEvcCeVlanInfo, pu1NoEvcInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetUniInfo                                        */
/*                                                                           */
/* Description        : This function is called to fetch the UNI Info        */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pCfaUniInfo - UNI info structure                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetUniInfo (UINT2 u2PortNo, tCfaUniInfo * pCfaUniInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = CfaGetUniInfo (u2PortNo, pCfaUniInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetEvcInfo                                        */
/*                                                                           */
/* Description        : This function is called to fetch the EVC Info        */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC reference ID                  */
/*                      pLcmEvcInfo - EVC info structure                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
               tLcmEvcInfo * pLcmEvcInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetEvcInfo (u2PortNo, u2EvcReferenceId, pLcmEvcInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmGetNextEvcInfo                                    */
/*                                                                           */
/* Description        : This function is called to fetch the EVC Info for the*/
/*                      EVC next to the EVC provided                         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC reference ID                  */
/*                      pLcmEvcInfo - EVC info structure                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : INT4                                                 */
/*****************************************************************************/
INT4
ElmGetNextEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                   tLcmEvcInfo * pLcmEvcInfo)
{
    INT4                i4Return = (INT4) ELM_INIT_VAL;
    i4Return = LcmGetNextEvcInfo (u2PortNo, u2EvcReferenceId, pLcmEvcInfo);
    return (i4Return);
}

/*****************************************************************************/
/* Function Name      : ElmUpdateUniInfo                                     */
/*                                                                           */
/* Description        : This function is called to update the UNI Info       */
/*                                                                           */
/* Input(s)           : CfaUniInfo - UNI info structure                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmUpdateUniInfo (tCfaUniInfo CfaUniInfo)
{
    CfaUpdateUniInfo (CfaUniInfo);
}

/*****************************************************************************/
/* Function Name      : ElmPassCeVlanInfo                                    */
/*                                                                           */
/* Description        : This function is called to pass the CE Vlan Info     */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pElmCeVlanInfo - Pointer to CE Vlan info structure   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmPassCeVlanInfo (UINT2 u2PortNo, tLcmEvcCeVlanInfo * pElmCeVlanInfo)
{
    LcmPassCeVlanInfo (u2PortNo, pElmCeVlanInfo);
}

/*****************************************************************************/
/* Function Name      : ElmDatabaseUpdateIndication                          */
/*                                                                           */
/* Description        : This function is to give the indication that LCM     */
/*                      database has been updated                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - Pointer to EVC status info       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmDatabaseUpdateIndication (UINT2 u2PortNo,
                             tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    LcmDatabaseUpdateIndication (u2PortNo, pLcmEvcStatusInfo);
}

/*****************************************************************************/
/* Function Name      : ElmCreateNewEvcIndication                            */
/*                                                                           */
/* Description        : This function is to give the indication that a new   */
/*                      EVC is created                                       */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - Pointer to EVC status info       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmCreateNewEvcIndication (UINT2 u2PortNo,
                           tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    LcmCreateNewEvcIndication (u2PortNo, pLcmEvcStatusInfo);
}

/*****************************************************************************/
/* Function Name      : ElmDeleteEvcIndication                               */
/*                                                                           */
/* Description        : This function is to give the indication that an EVC  */
/*                      is deleted                                           */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - Id of the deleted EVC             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmDeleteEvcIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId)
{
    LcmDeleteEvcIndication (u2PortNo, u2EvcReferenceId);
}

/*****************************************************************************/
/* Function Name      : ElmElmiEvcStatusChangeIndication                     */
/*                                                                           */
/* Description        : This function is to give the indication that status  */
/*                      an EVC has changed                                   */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - Id of the deleted EVC             */
/*                      u1EvcStatus - Changed Status                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmElmiEvcStatusChangeIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                                  UINT1 u1EvcStatus)
{
    LcmEvcStatusChangeIndication (u2PortNo, u2EvcReferenceId, u1EvcStatus);
}

/*****************************************************************************/
/* Function Name      : ElmElmiOperStatusIndication                          */
/*                                                                           */
/* Description        : This function is to give the indication that         */
/*                      operational status of ELMI has changed               */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u1ElmOperStatus - Changed operational status         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ElmElmiOperStatusIndication (UINT2 u2PortNo, UINT1 u1ElmOperStatus)
{
    LcmElmiOperStatusIndication (u2PortNo, u1ElmOperStatus);
}

#ifdef L2RED_WANTED
/******************************************************************************/
/*  Function Name       : ElmRmRegisterProtcols                               */
/*  Description         : Routine used by ELMI to register with RM.           */
/*  Input(s)            : tRmRegParams - Reg. params to be provided by        */
/*                        protocols.                                          */
/* Output(s)            : None.                                               */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None                                                  */
/*                                                                            */
/* Returns              : RM_SUCCESS/RM_FAILURE.                              */
/******************************************************************************/
UINT4
ElmRmRegisterProtocols (tRmRegParams * pRmRegParams)
{
    return (RmRegisterProtocols (pRmRegParams));
}

/******************************************************************************/
/*  Function Name       : ElmRmDeRegisterProtcols                             */
/*  Description         : Routine used by ELMI to deregister with RM.         */
/*  Input(s)            : tRmRegParams - Reg. params to be provided by        */
/*                        protocols.                                          */
/* Output(s)            : None.                                               */
/*                                                                            */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None                                                  */
/*                                                                            */
/* Returns              : RM_SUCCESS/RM_FAILURE.                              */
/******************************************************************************/
UINT4
ElmRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_ELMI_APP_ID));
}

/******************************************************************************/
/* Function Name          : ElmRmEnqMsgToRmFromAppl                           */
/* Description            : Routine to enq the msg from ELMI to RM            */
/* Input(s)               : pRmMsg - msg from ELMI                            */
/*                          u2DataLen - Len of msg                            */
/*                          u4SrcEntId - EntId from which the msg has arrived */
/*                          u4DestEntId - EntId to which the msg has to be    */
/*                            sent                                            */
/* Output(s)              :  None                                             */
/*                                                                            */
/* Global Variables                                                           */
/* Referred               : None                                              */
/*                                                                            */
/* Global Variables                                                           */
/* Modified               : None                                              */
/*                                                                            */
/* Returns                : RM_SUCCESS/RM_FAILURE.                            */
/* Action                 : Routine to enq the msg from applications to RM    */
/*                          task.                                             */
/******************************************************************************/
UINT4
ElmRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/******************************************************************************/
/* Function Name          : ElmRmSetBulkUpdatesStatus                         */
/* Description            : This function calls the RM module to set the      */
/*                          Status of the bulk updates                        */
/* Input(s)               : u4AppId - Application Identifier.                 */
/* Output(s)              :  None                                             */
/*                                                                            */
/* Global Variables                                                           */
/* Referred               : None                                              */
/*                                                                            */
/* Global Variables                                                           */
/* Modified               : None                                              */
/*                                                                            */
/* Returns                : RM_SUCCESS/RM_FAILURE.                            */
/******************************************************************************/
INT4
ElmRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}

/*****************************************************************************/
/* Function Name      : ElmRmGetStandbyNodeCount                             */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : No.of standby nodes                                  */
/*                                                                           */
/*****************************************************************************/
UINT1
ElmRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : ElmRmGetNodeState                                    */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Node state                                           */
/*                                                                           */
/*****************************************************************************/
UINT4
ElmRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
/*****************************************************************************/
/* Function Name      : ElmSnoopRedRcvPktFromRm                              */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to SNOOP*/
/*                      task. For other events this function enqueues the    */
/*                      given data to the queue and sends the corresponding  */
/*                      event to SNOOP task.                                 */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event is sent then return     */
/*                      SNOOP_SUCCESS otherwise return SNOOP_FAILURE         */
/*****************************************************************************/
INT4
ElmSnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (SnoopRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : ElmLldpRedRcvPktFromRm                               */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to LLDP */
/*                      task. For other events this function enqueues the    */
/*                      given data to the queue and sends the corresponding  */
/*                      event to LLDP task.                                  */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
INT4
ElmLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif

/******************************************************************************/
/* Function Name      : ElmRmSendEventToRmTask                                */
/* Description        : ELMI calls this function to post any event to RM      */
/* Input(s)           : u4Event - Event to be send                            */
/* Output(s)          : None                                                  */
/* Global Variables                                                           */
/* Referred           : None                                                  */
/*                                                                            */
/* Global Variables                                                           */
/* Modified           : None                                                  */
/* Returns            : RM_SUCCESS/RM_FAILURE                                 */
/******************************************************************************/
UINT4
ElmRmSendEventToRmTask (UINT4 u4Event)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ELMI_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
    ProtoEvt.u4SendEvent = u4Event;

    return RmApiHandleProtocolEvent (&ProtoEvt);
}

/*****************************************************************************/
/* Function Name      : ElmRmHandleProtocolEvent                          */
/*                                                                            */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                            */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_INITIATE_BULK_UPDATE /            */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
ElmRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : ElmRmReleaseMemoryForMsg                             */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
ElmRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

#endif
