/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmtmr.c,v 1.10 2010/11/15 13:36:01 prabuc Exp $
 *
 * Description: This file contains functions pertaining to the 
 *              Timer Module and other timer functionalities of
 *              ELMI Module.
 *
 *******************************************************************/

#include "elminc.h"

/*****************************************************************************/
/* Function Name      : ElmTimerInit                                         */
/*                                                                           */
/* Description        : This function is called by the initialisation module */
/*                      inorder to initialise all the timer related entities.*/
/*                      This function creates a Memory Pool for all the      */
/*                      module timers and also creates a Timer List.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ElmTmrMemPoolId,                      */
/*                      gElmGlobalInfo.ElmTmrListId                          */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmTimerInit (VOID)
{

    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                    ELM_CONTROL_PATH_TRC,
                    "TMR: Timer Memory Pool created successfully\n");

    /* Creating the Timer List */
    if (ELM_CREATE_TMR_LIST
        ((UINT1 *) ELM_TASK_NAME, ELM_TMR_EXPIRY_EVENT, NULL,
         &ELM_TMR_LIST_ID) != TMR_SUCCESS)
    {
        ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                        ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                        "TMR: Timer List Creation FAILED!\n");

        /* Deleting the Memory Pool that was created */
        if (ELM_DELETE_TMR_MEM_POOL (ELM_TMR_MEMPOOL_ID) != MEM_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_MEM_TRC | ELM_TMR_TRC |
                            ELM_ALL_FAILURE_TRC,
                            "TMR: Timer Memory Pool Deletion FAILED!\n");
        }
        else
        {
            ELM_TMR_MEMPOOL_ID = ELM_INIT_VAL;
        }

        return ELM_FAILURE;
    }

    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                    ELM_CONTROL_PATH_TRC,
                    "TMR: Timer List created successfully\n");

    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                    ELM_CONTROL_PATH_TRC,
                    "TMR: Timer Module Initialised successfully\n");

    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmTimerDeInit                                       */
/*                                                                           */
/* Description        : This function is called by the shutdown module in    */
/*                      order to de-initialise all the timer related         */
/*                      entities.This function deletes the Memory Pool and   */
/*                      the Timer List. This routine is also called when any */
/*                      failure occurs during Timer Initialisation.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gElmGlobalInfo.ElmTmrMemPoolId,                      */
/*                      gElmGlobalInfo.ElmTmrListId                          */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmTimerDeInit (VOID)
{
    INT4                i4RetVal = ELM_SUCCESS;

    /* Deleting the Timer List */
    if (ELM_TMR_LIST_ID != ELM_INIT_VAL)
    {
        if (ELM_DELETE_TMR_LIST (ELM_TMR_LIST_ID) != TMR_SUCCESS)
        {
            ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                            ELM_CONTROL_PATH_TRC | ELM_ALL_FAILURE_TRC,
                            "TMR: Timer List Deletion FAILED!\n");

            i4RetVal = ELM_FAILURE;
        }
        ELM_TMR_LIST_ID = ELM_INIT_VAL;
    }

    ELM_GLOBAL_TRC (ELM_INIT_SHUT_TRC | ELM_OS_RESOURCE_TRC |
                    ELM_CONTROL_PATH_TRC,
                    "TMR: Timer Module De-Initialised successfully\n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ElmStartTimer                                        */
/*                                                                           */
/* Description        : This function is called whenever any timer needs to  */
/*                      be started by the Module(s). This function allocates */
/*                      a memory block for the timer node and then starts    */
/*                      the timer of the specified type for the specified    */
/*                      duration.                                            */
/*                                                                           */
/* Input(s)           : pPortPtr - A Void Pointer which may point to the Port*/
/*                                Entry structure or to the Per Instance Port*/
/*                                Info structure.                            */
/*                      u1TimerType - The type of the timer that is to be    */
/*                                    started                                */
/*                      u2Duration - The duration for which the timer needs  */
/*                                   to be started                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ElmTmrMemPoolId                       */
/*                      gElmGlobalInfo.ElmTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmStartTimer (VOID *pPortPtr, UINT1 u1TimerType, UINT2 u2Duration)
{
    tElmTimer          *pElmTimer = NULL;
    tElmTimer         **ppTimer = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
    INT4                i4RetVal = ELM_SUCCESS;
    UINT4               u4IfIndex = ELM_INIT_VAL;

#ifdef ELM_DEBUG

    UINT1               aau1TimerName[4][20] = { " ",
        "POLLINGTIMER", "POLLINGVERITIMER", "ASYNCTIMER"
    };

#endif

    if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
    {
        /* Timer should not start on a Standby Node */
        return ELM_SUCCESS;
    }
    if (pPortPtr == NULL)
    {

        ELM_TRC_ARG2 (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                      "TMR: Duration %d: ElmStartTimer for timer %s"
                      " called with Null Pointer\n",
                      u2Duration, aau1TimerName[u1TimerType]);

        return ELM_FAILURE;
    }

    if (u2Duration == (UINT2) ELM_INIT_VAL)
    {
        ELM_TRC_ARG2 (ELM_TMR_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                      "TMR: Port %d: Trying to Start Timer %s for Zero"
                      " Duration!!\n", u4IfIndex, aau1TimerName[u1TimerType]);
        return ELM_FAILURE;
    }

    /* Allocate the timer node from the Timer Memory Pool */
    if (ELM_ALLOC_TMR_MEM_BLOCK (pElmTimer) == NULL)
    {
        /* Incrementing the Memory Failure Count */
        ELM_INCR_MEMORY_FAILURE_COUNT ();
        ElmMemFailTrap (ELM_PORT_TRAPS_OID, ELM_PORT_TRAPS_OID_LEN);
        ELM_TRC_ARG3 (ELM_TMR_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                      "TMR: Port %d: Duration %d: Timer Memory Block"
                      "Allocation for timer %s FAILED!\n",
                      u4IfIndex, u2Duration, aau1TimerName[u1TimerType]);
        return ELM_FAILURE;
    }

    ELM_MEMSET (pElmTimer, ELM_INIT_VAL, sizeof (tElmTimer));

    pElmTimer->pEntry = (VOID *) pPortPtr;

    pElmTimer->u1TimerType = u1TimerType;

    /* Store the pointer to the timer node in the corresponding timer pointer
     * and also validate the Timer Type */

    switch (u1TimerType)
    {

        case ELM_TMR_TYPE_PVT:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;

            u4IfIndex = pElmPortEntry->u4IfIndex;
            if (pElmPortEntry->pPvtTmr != NULL)
            {
                if (ElmStopTimer (pElmPortEntry, u1TimerType) != ELM_SUCCESS)
                {
                    ppTimer = &(pElmPortEntry->pPvtTmr);
                    ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                             "TMR: Unable to stop the running PVT Timer!\n");
                    i4RetVal = ELM_FAILURE;
                    break;
                }
            }
            ppTimer = &(pElmPortEntry->pPvtTmr);
            ELM_TRC_ARG3 (ELM_TMR_TRC,
                          "TMR: Starting Timer %s for port %s "
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          ELM_GET_IFINDEX_STR
                          (pElmPortEntry->u4IfIndex), u2Duration);
            break;

        case ELM_TMR_TYPE_PT:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;

            u4IfIndex = pElmPortEntry->u4IfIndex;
            if (pElmPortEntry->pPtTmr != NULL)
            {
                if (ElmStopTimer (pElmPortEntry, u1TimerType) != ELM_SUCCESS)
                {
                    ppTimer = &(pElmPortEntry->pPtTmr);
                    ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                             "TMR: Unable to stop the running PT Timer!\n");
                    i4RetVal = ELM_FAILURE;
                    break;
                }
            }
            ppTimer = &(pElmPortEntry->pPtTmr);
            ELM_TRC_ARG3 (ELM_TMR_TRC,
                          "TMR: Starting Timer %s for port %s "
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          ELM_GET_IFINDEX_STR
                          (pElmPortEntry->u4IfIndex), u2Duration);
            break;

        case ELM_TMR_TYPE_ASYNC:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;

            u4IfIndex = pElmPortEntry->u4IfIndex;
            if (pElmPortEntry->pAsyncMsgTimer != NULL)
            {
                if (ElmStopTimer (pElmPortEntry, u1TimerType) != ELM_SUCCESS)
                {
                    ppTimer = &(pElmPortEntry->pAsyncMsgTimer);
                    ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                             "TMR: Unable to stop the running ASYNC Timer!\n");
                    i4RetVal = ELM_FAILURE;
                    break;
                }
            }
            ppTimer = &(pElmPortEntry->pAsyncMsgTimer);
            ELM_TRC_ARG3 (ELM_TMR_TRC,
                          "TMR: Starting Timer %s for port %s "
                          " for duration %u\n",
                          aau1TimerName[u1TimerType],
                          ELM_GET_IFINDEX_STR
                          (pElmPortEntry->u4IfIndex), u2Duration);
            break;

        default:
            ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                     "TMR: Invalid Timer Type\n");
            i4RetVal = ELM_FAILURE;
            break;
    }

    if (i4RetVal == ELM_FAILURE)
    {
        if (ELM_RELEASE_TMR_MEM_BLOCK (pElmTimer) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                     "TMR: Memory Block Release FAILED!\n");
            i4RetVal = ELM_FAILURE;
        }

        if (ppTimer != NULL)
        {
            if (ELM_RELEASE_TMR_MEM_BLOCK (*ppTimer) != MEM_SUCCESS)
            {
                ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                         "TMR: Memory Block Release FAILED!\n");
            }
        }
        return i4RetVal;
    }

    u2Duration = (UINT2) (u2Duration * ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    if (ELM_START_TIMER (ELM_TMR_LIST_ID, &(pElmTimer->ElmAppTimer),
                         u2Duration) != TMR_SUCCESS)
    {
        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                 "TMR: Starting Timer FAILED!\n");
        i4RetVal = ELM_FAILURE;

        if (ELM_RELEASE_TMR_MEM_BLOCK (pElmTimer) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                     "TMR: Memory Block Release FAILED!\n");
            i4RetVal = ELM_FAILURE;
        }
        return i4RetVal;
    }

    *ppTimer = pElmTimer;

    ELM_TRC_ARG3 (ELM_TMR_TRC,
                  "TMR: Port %s:  Started Timer %s for duration %u\n",
                  ELM_GET_IFINDEX_STR (u4IfIndex),
                  aau1TimerName[u1TimerType],
                  (u2Duration / ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : ElmStopTimer                                         */
/*                                                                           */
/* Description        : This function is called whenever any timer needs to  */
/*                      be stopped by the Module(s). This function stops the */
/*                      timer of the specified duration.                     */
/*                                                                           */
/* Input(s)           : pPortPtr - A Void Pointer which may point to the Port */
/*                                Entry structure or to the Per Instance Port*/
/*                                Info structure.                            */
/*                      u1TimerType - The type of the timer that is to be    */
/*                                    started                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ElmTmrMemPoolId                       */
/*                      gElmGlobalInfo.ElmTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmStopTimer (VOID *pPortPtr, UINT1 u1TimerType)
{

    tElmTimer         **ppElmTimer = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
#ifdef ELM_DEBUG
    UINT1               aau1TimerName[4][20] = { " ",
        "POLLINGTIMER", "POLLINGVERITIMER", "ASYNCTIMER"
    };
#endif

    if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
    {
        /* Timer should not have started on a Standby Node */
        return ELM_SUCCESS;
    }

    if (pPortPtr == NULL)
    {
        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                 "TMR: ElmStopTimer called with Null Pointer\n");
        return ELM_FAILURE;
    }

    switch (u1TimerType)
    {

        case ELM_TMR_TYPE_PVT:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;
            ppElmTimer = &(pElmPortEntry->pPvtTmr);
            ELM_TRC_ARG2 (ELM_TMR_TRC,
                          "TMR: Port %s: Stopping Timer %s \n",
                          ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex),
                          aau1TimerName[u1TimerType]);
            break;

        case ELM_TMR_TYPE_PT:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;
            ppElmTimer = &(pElmPortEntry->pPtTmr);
            ELM_TRC_ARG2 (ELM_TMR_TRC,
                          "TMR: Port %s: Stopping Timer %s \n",
                          ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex),
                          aau1TimerName[u1TimerType]);
            break;

        case ELM_TMR_TYPE_ASYNC:
            pElmPortEntry = (tElmPortEntry *) pPortPtr;
            ppElmTimer = &(pElmPortEntry->pAsyncMsgTimer);
            ELM_TRC_ARG2 (ELM_TMR_TRC,
                          "TMR: Port %s: Stopping Timer %s \n",
                          ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex),
                          aau1TimerName[u1TimerType]);
            break;

        default:
            ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                     "TMR: Invalid Timer Type\n");
            return ELM_FAILURE;
    }

    if (*ppElmTimer == NULL)
    {
        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                 "TMR: Timer not running, cannot stop\n");
        return ELM_FAILURE;
    }

    if (ELM_STOP_TIMER (ELM_TMR_LIST_ID, &((*ppElmTimer)->ElmAppTimer))
        == TMR_FAILURE)
    {
        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                 "TMR: Stopping Timer FAILED!\n");
        return ELM_FAILURE;
    }

    /* Free the Timer Memory Block to the Memory Pool */
    if (ELM_RELEASE_TMR_MEM_BLOCK (*ppElmTimer) != MEM_SUCCESS)
    {
        ELM_TRC (ELM_TMR_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                 "TMR: Memory Block Release FAILED!\n");
        return ELM_FAILURE;
    }

    *ppElmTimer = NULL;

    ELM_TRC (ELM_TMR_TRC, "TMR: Timer Stopped successfully\n");
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmTmrExpiryHandler                                  */
/*                                                                           */
/* Description        : This function is called whenever any timer expires.  */
/*                      This extracts all the expired timers at any instant  */
/*                      of time and depending on the type of timer, it       */
/*                      performs the necessary processing.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gElmGlobalInfo.ElmTmrListId                          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/

INT4
ElmTmrExpiryHandler (VOID)
{
    tElmTimer          *pElmTimer = NULL;
    tElmPortEntry      *pElmPortEntry = NULL;
    INT4                i4RetVal = ELM_SUCCESS;
    UINT1               u1TimerType = ELM_INIT_VAL;

    ELM_GLOBAL_TRC (ELM_CONTROL_PATH_TRC,
                    "TMR: Handling Timer Expiry event obtained ...\n");

    if ((ELM_IS_ELMI_INITIALISED () != TRUE) ||
        (ELM_TMR_LIST_ID == ELM_INIT_VAL))
    {
        ELM_GLOBAL_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                        "TMR: Invalid Timer List\n");
        return ELM_FAILURE;
    }

    while ((pElmTimer =
            (tElmTimer *) ELM_GET_NEXT_EXPIRED_TMR (ELM_TMR_LIST_ID)) != NULL)
    {
        u1TimerType = pElmTimer->u1TimerType;
        pElmPortEntry = (tElmPortEntry *) pElmTimer->pEntry;

        if (ELM_RM_GET_NODE_STATE () == RED_ELM_STANDBY)
        {
            /* Standby Node should not process this event */
            return ELM_SUCCESS;
        }

        if (ELM_IS_ELMI_ENABLED () && (pElmPortEntry != NULL))
        {
            if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) ==
                ELM_NETWORK_SIDE)
            {
                /* 
                 * Calling the respective Timer expiry handler 
                 */

                switch (u1TimerType)
                {
                    case ELM_TMR_TYPE_PVT:

                        pElmPortEntry->pPvtTmr = NULL;
                        ELM_TRC_ARG1 (ELM_TMR_TRC,
                                      "TMR: Port %s: PVT Timer EXPIRED \n",
                                      ELM_GET_IFINDEX_STR
                                      (pElmPortEntry->u4IfIndex));

                        ElmPvtExpired (pElmPortEntry->u4IfIndex);
                        break;

                    case ELM_TMR_TYPE_ASYNC:

                        pElmPortEntry->pAsyncMsgTimer = NULL;
                        ELM_TRC_ARG1 (ELM_TMR_TRC,
                                      "TMR: Port %s: ASYNC Timer EXPIRED \n",
                                      ELM_GET_IFINDEX_STR
                                      (pElmPortEntry->u4IfIndex));
                        ElmAsynchTimerExpired (pElmPortEntry->u4IfIndex);
                        break;

                    default:
                        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                                 "TMR: Invalid Timer Type\n");
                        i4RetVal = ELM_FAILURE;

                }                /* End of switch */
            }
            else if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) ==
                     ELM_CUSTOMER_SIDE)
            {
                /* 
                 * Calling the respective Timer expiry handler 
                 */

                switch (u1TimerType)
                {
                    case ELM_TMR_TYPE_PT:

                        pElmPortEntry->pPtTmr = NULL;
                        ELM_TRC_ARG1 (ELM_TMR_TRC,
                                      "TMR: Port %s: PT Timer EXPIRED \n",
                                      ELM_GET_IFINDEX_STR
                                      (pElmPortEntry->u4IfIndex));

                        ElmPtExpired (pElmPortEntry);
                        break;

                    default:
                        ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                                 "TMR: Invalid Timer Type\n");
                        i4RetVal = ELM_FAILURE;

                }                /* End of switch */
            }
        }
        /* Free the Timer Memory Block to the Memory Pool */
        if (ELM_RELEASE_TMR_MEM_BLOCK (pElmTimer) != MEM_SUCCESS)
        {
            ELM_TRC (ELM_TMR_TRC | ELM_MEM_TRC | ELM_ALL_FAILURE_TRC,
                     "TMR: Memory Block Release FAILED!\n");
            return ELM_FAILURE;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : ElmStopAllRunningTimers                              */
/*                                                                           */
/* Description        : This function is called whenever any port is deleted */
/*                      or when the Module is disabled. This function will   */
/*                      stop all the running timers for this port.           */
/*                                                                           */
/* Input(s)           : u4PortIndex - Port Index at which timers have to be  */
/*                      stopped                                              */
 /*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
ElmStopAllRunningTimers (UINT4 u4IfIndex)
{
    tElmPortEntry      *pElmPortEntry = NULL;
    if (ELM_IS_ELMI_STARTED () != ELM_STARTED)
    {
        return;
    }

    pElmPortEntry = ELM_GET_PORTENTRY (u4IfIndex);
    if (pElmPortEntry == NULL)
    {
        return;
    }

    if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) == ELM_NETWORK_SIDE)
    {
        if (pElmPortEntry->pPvtTmr != NULL)
        {
            if (ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PVT)
                != ELM_SUCCESS)
            {
                ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                         "TMR: ElmStopTimer for Polling Verification Timer"
                         " FAILED!\n");
            }
        }
        if (pElmPortEntry->pAsyncMsgTimer != NULL)
        {
            if (ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_ASYNC)
                != ELM_SUCCESS)
            {
                ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                         "TMR: ElmStopTimer for Asynchronous Timer"
                         " FAILED!\n");
            }
        }
    }
    else if (ELM_GET_PORT_MODE (pElmPortEntry->u4IfIndex) == ELM_CUSTOMER_SIDE)
    {
        if (pElmPortEntry->pPtTmr != NULL)
        {
            if (ElmStopTimer ((VOID *) pElmPortEntry, ELM_TMR_TYPE_PT)
                != ELM_SUCCESS)
            {
                ELM_TRC (ELM_TMR_TRC | ELM_ALL_FAILURE_TRC,
                         "TMR: ElmStopTimer for Polling Timer" " FAILED!\n");
            }
        }
    }

    ELM_TRC_ARG1 (ELM_TMR_TRC | ELM_INIT_SHUT_TRC,
                  "TMR: Port %s: Stopped All Running Timers... \n",
                  ELM_GET_IFINDEX_STR (pElmPortEntry->u4IfIndex));
    return;
}

/* End of file */
