/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmtdfs.h,v 1.9 2012/04/04 14:14:46 siva Exp $
 *
 * Description: This file contains all the typedefs used by the ELMI
 * module.
 *
 *******************************************************************/


#ifndef _ELMTDFS_H_
#define _ELMTDFS_H_

/******************************************************************************/
/*                             Global Structure                               */
/******************************************************************************/
typedef tCRU_BUF_CHAIN_HEADER     tElmBufChainHeader;
typedef tCRU_INTERFACE            tElmInterface;
typedef tTMO_SLL                  tElmSll;
typedef tTMO_SLL_NODE             tElmSllNode;
typedef UINT4                     tElmLocalMsgType;

typedef struct ElmTimer
{
   tElmAppTimer             ElmAppTimer;    /* The FSAP2 Application Timer
                                             * node */
   VOID                     *pEntry;        /* A void pointer in which the
                                             * corresponding port entry
                                             * pointer for which the timer
                                             * was started can be stored 
                                             */
   UINT1                    u1TimerType;    /* The type of the timer that
                                             * this timer node represents */
   UINT1                    au1Reserved[3];
}tElmTimer;


typedef struct ElmPortEntry
{
   tElmTimer            *pPvtTmr;
   tElmTimer            *pPtTmr;
   tElmTimer            *pAsyncMsgTimer;
   tElmSll              ElmAsynchList;
   tElmBufChainHeader   *pElmFirstPdu;/* Chain of Pre allocated/formatted Packets */
   tElmBufChainHeader   *pElmNextPdu; /* Next PDU to be transmitted in case of
                                       * transmitting pre formed packets */
   tElmBufChainHeader   *pElmLastPdu; /* Last PDU in the prepocessed PDU list */
   tIssVlanList         CeVlanMapInfo;
   UINT4                u4NoOfCruBytesAllocated;/* No of Bytes allocated from CRU buffer 
                                                 * to store Pre formed packets */
   UINT4                u4LastReferenceIdSent;
   UINT4                u4IfIndex;          /* Actual Interface No on th switch
                                             */
   UINT4                u4ElmiRxElmiCheckEnquiryMsg;
   UINT4                u4ElmiRxFullStatusEnquiryMsg;
   UINT4                u4ElmiRxFullStatusContdEnquiryMsg;
   UINT4                u4ElmiTxElmiCheckMsg;
   UINT4                u4ElmiTxFullStatusMsg;
   UINT4                u4ElmiTxFullStatusContdMsg;
   UINT4                u4ElmiTxAsyncStatusMsg;
   UINT4                u4ElmiTxElmiCheckEnquiryMsg;
   UINT4                u4ElmiTxFullStatEnquiryMsg;
   UINT4                u4ElmiTxFullStatContdEnquiryMsg;
   UINT4                u4ElmiRxCheckMsg;
   UINT4                u4ElmiRxFullStatMsg;
   UINT4                u4ElmiRxFullStatContdMsg;
   UINT4                u4ElmiRxAsyncStatusMsg;
   UINT4                u4ElmiValidMsgRxed;
   UINT4                u4ElmiInvalidMsgRxed;
   UINT4                u4ElmiRelErrStatusTimeoutCount;
   UINT4                u4ElmiRelErrInvalidSeqNumCount;
   UINT4                u4ElmiRelErrInvalidStatusCount;
   UINT4                u4ElmiRelErrRxUnsolicitedStatusCount;
   UINT4                u4ElmiProErrInvalidProtVerCount;
   UINT4                u4ElmiProErrInvalidEvcRefIdCount;
   UINT4                u4ElmiProErrInvalidMessageTypeCount;
   UINT4                u4ElmiProErrOutOfSequenceInfoEleCount;
   UINT4                u4ElmiProErrDuplicateInfoEleCount;
   UINT4                u4ElmiProErrMandatoryInfoEleMissingCount;
   UINT4                u4ElmiProErrInvalidMandatoryInfoEleCount;
   UINT4                u4ElmiProErrInvalidNonMandatoryInfoEleCount;
   UINT4                u4ElmiProErrUnrecognizedInfoEleCount;
   UINT4                u4ElmiProErrUnexpectedInfoEleCount;
   UINT4                u4ElmiProErrShortMessageCount;
   UINT4                u4DataInstance;
   UINT4                u4FullStatusConDataInstance; 
   UINT4                u4PollingTimerExpiryTime;
   UINT4                u4PollingVeriTimerExpiryTime;
   UINT2                u2NoOfEvcConfigured;
   UINT2                u2LastEvcRefIdSent;
   UINT2                u2FirstVlanId;
   UINT2                u2LastVlanId;
   UINT2                u2NoOfVlansMapped;
   UINT2                u2LastCeVlanEvcRefId;
   UINT2                u2LastReceivedRefrenceID;
   UINT2                u2LastEvcRefIdForVlan;
   UINT2                u2LastVlanIdSent;
   UINT2                u2PollingCounterConfigured;
   UINT2                u2PollingCounter;
   UINT2                u2CeVlanSentInPrevMsg;
   UINT1                au1StrIfIndex [ELM_IFINDEX_STR_LEN]; /* Global IfIndex
                                   in string form */

   UINT2                u2LastParsedEvcRefId;

   UINT1                u1PortStatus;       /* Status of this port 
                                             * INVALID / OPER_DOWN /
                                             * OPER_UP 
                                             */
   UINT1                u1ElmiPortStatus;
   UINT1                u1ElmiPortStatusBackup;
   UINT1                u1ElmiOperStatus;
   UINT1                u1PvtValueConfigured;
   UINT1                u1PtValueConfigured;
   UINT1                u1StatusCounterConfigured;
   UINT1                u1StatusCounter;
   UINT1                u1LastReportTypeTxed;
   UINT1                u1SendSeqCounter;
   UINT1                u1RecvSeqCounter;
   UINT1                u1ElmRedSendSeqCounter;
   UINT1                u1ElmRedRecvSeqCounter;
   UINT1                u1ElmCeTlvCheck; 
   UINT1                u1ElmiMode;
   UINT1                u1PktCeVlanInfoRemaining;
   UINT1                u1CeVlanInfoRemaining;
   UINT1                u1EvcStatusRemaining;
   BOOL1                bRecvdFullStatus;
   BOOL1                bMsgRcvdInTime;
   BOOL1                bFullStatusInProgress;
   BOOL1                bMsgExpected;
   BOOL1                bLaPort;
   BOOL1                bElmRedBulkUpdInProgress;
   BOOL1                bElmStatusInformationChange;
   UINT1                u1Reserved;
}tElmPortEntry;

typedef struct ElmEvcStatusInfo {
   tLcmEvcStatusInfo    aLcmEvcStatusInfo[ELM_MAX_EVC_IN_SINGLE_REQUEST];
}tElmEvcStatusInfo;

typedef struct ElmEvcVlanInfo {
   tLcmEvcCeVlanInfo   aLcmEvcCeVlanInfo[ELM_MAX_EVC_IN_SINGLE_REQUEST];
}tElmEvcVlanInfo;

typedef struct ElmGlobalInfo {
   tElmTmrListId      TmrListId;  /* Id of the Timer
                                  * List used by the
                                  * Timer Module */

   tElmQId            InputQId;   /* QueueId of the ELMI Task */

   tElmQId            CfgQId;     /* QueueId of the ELMI Configuration
                                  * Queue */
   tElmQId            AsynchListQId;


   tElmTaskId         TaskId;     /* ELMI Task Id */
   tElmOsixSemId      SemId;      /* ELMI Sem4 Id */
   
    tElmMemPoolId     TmrMemPoolId;     /* Timer Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * block of size
                                          * tElmTimer */
 
   tElmMemPoolId     PortTableMemPoolId;  /* Memory Pool Id of
                                          * the pool used for 
                                          * allocating memory 
                                          * block for PortEntryTable
                                          */
   tElmMemPoolId     PortInfoMemPoolId;  /* Memory Pool Id of
                                          * the pool used for 
                                          * allocating memory 
                                          * block for PortEntry
                                          */
   tElmMemPoolId     LocalMsgMemPoolId;  /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Local
                                          * SNMP messages 
                                          */
   tElmMemPoolId     QMsgMemPoolId;      /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Messages 
                                          * posted to ELMI Q 
                                          */
   tElmMemPoolId     CfgQMsgMemPoolId;   /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Messages
                                          * posted to ELMI CFG Q 
                                          */
   tElmMemPoolId     EvcStatusPoolId;    /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Tx
                                          * Status Info
                                          */
   tElmMemPoolId     EvcVlanInfoPoolId;   /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Tx
                                          * Vlan Info
                                          */
   tElmMemPoolId     CfaUniInfoPoolId;   /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Cfa
                                          * Unic Info
                                          */
										  
   tElmMemPoolId     LcmEvcInfoPoolId;    /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for LCM
                                          * EVC Info
                                          */

   tElmMemPoolId     LcmEvcVlanInfoPoolId; /* Memory Pool Id of
                                            * the pool used for
                                            * allocating memory
                                            * blocks for LCM
                                            * EVC CE VLAN INFO Info
                                            */
   tElmMemPoolId     FrameSizePoolId;  /* Memory Pool Id of
                                        * the pool used for
                                        * allocating memory
                                        * blocks for ELMI FRAME
                                        * SIZE
                                        */

   tElmPortEntry     **ppPortEntry;
                                /* Pointer to array of pointers to Port 
                                 * Information */
   tElmMemPoolId     ElmAsynchListMemPoolId;

   tElmSll           ElmAsynchListHead;
   UINT4             u4GlobalTraceOption;      /* For enabling traces 
                                             */
   UINT4             u4GlobalTrapOption;       /* For enabling Global Trap 
                                             */

   UINT4             u4ElmTrapOption;           /* For enabling Trap */
   UINT4             u4BufferFailureCount;
                                             /* Number of times Buffer 
                                              * Failure has occured 
                                              */
   
   UINT4             u4MemoryFailureCount;
                                             /* Number of times Memory 
                                              * Failure has occured 
                                              */
   UINT4             u4SysLogId;
   UINT2             u2NoOfElmiEnabledPorts;
   UINT1             u1ElmGenTrapType;
                                            /* Indicate the general trap set 
                                             */

   UINT1             u1GlobalModuleStatus;  
                                            /* Stores the protocol Admin status 
                                             */ 
   UINT1             u1SystemControl; 
   

   UINT1            u1ElmErrTrapType;   /* Indicate the error trap set 
                                             */
   UINT1            au1Reserved [2]; 
   UINT1            *gu1ElmPdu;
}tElmGlobalInfo;

typedef enum
{
    ELM_EVC_NOT_ACTIVE = 0,
    ELM_EVC_NEW_NOT_ACTIVE,
    ELM_EVC_ACTIVE_EVC,
    ELM_EVC_NEW_ACTIVE,
    ELM_EVC_PARTIALLY_ACTIVE,
    ELM_EVC_NEW_PARTIALLY_ACTIVE
}tElmEvcStatus;

typedef enum
{
    FULL_STATUS = 0,
    ELMI_CHECK,
    ASYNC_STATUS,
    FULL_STATUS_CONTINUED
}tElmReportType;

typedef enum
{
    ELM_EVC_TYPE_POINT2POINT = 0,
    ELM_EVC_TYPE_POINT2MULTIPOINT = 1
}tElmEvcType;

/* Peer node Id. */
typedef VOID * tElmRedPeerId;

/* To handle message/events given by Redundancy Manager. */
typedef struct {
#ifdef L2RED_WANTED
    tRmMsg          *pFrame;     /* Message given by RM module. */
#else
    VOID            *pFrame;
#endif
    tElmRedPeerId   PeerId;
    UINT2           u2Length;   /* Length of message given by RM module. */
    UINT1           u1Event;    /* Event given by RM module. */
    UINT1           u1Reserved;
}tElmRmCtrlMsg;


typedef struct ElmMsgNode
{
    tElmLocalMsgType    MsgType;
    UINT4               u4IfIndex;
    UINT2               u2EvcRefId;
    UINT1              au1Reserved[2];
    union
    {
        tElmRmCtrlMsg   *pRmCtrlMsg;  
        UINT2 u2PollingCounterValue;
        UINT1 u1EvcStatus;
        UINT1 u1PvtValue;
        UINT1 u1PtValue;
        UINT1 u1StatusCounterValue;
        UINT1 u1TrigType;
        UINT1 u1ElmUniMode;
        BOOL1 bStatus;
    }Msg;
}tElmMsgNode;

typedef struct ElmQMsg {
   tElmLocalMsgType      LocalMsgType;
   union {
      tElmMsgNode        *pMsgNode;
      tElmBufChainHeader *pPduInQ;
      tElmRmCtrlMsg      *pRmMsg;  

   }uQMsg;

}tElmQMsg;

typedef struct ElmBrgErrTraps{
  UINT1             u1ErrTrapType;
  UINT1             au1Reserved[3];
}tElmBrgErrTraps;

typedef struct ElmInvalidMsgRxdTrap
{
      UINT4         u4IfIndex;
      UINT1         u1ErrorType;
      UINT1         au1Reserved[3];
}tElmInvalidMsgRxdTrap;

typedef struct ElmPvtExpiredTrap
{     
      UINT4         u4IfIndex;
      UINT1         u1PvtStatus;
      UINT1         u1Reserved[3];
}tElmPvtExpiredTrap;

typedef struct ElmPtExpiredTrap
{
      UINT4         u4IfIndex;
      UINT1         u1PtStatus;
      UINT1         u1Reserved[3];
}tElmPtExpiredTrap;

typedef struct ElmEvcTrap
{
      UINT1                      ElmiEvcId[100];
      UINT4                      u4IfIndex;
      UINT1                      u1ElmiEvcStatus;
      UINT1                      au1Reserved[3];
}tElmEvcTrap;

typedef struct ElmUniTrap
{
      UINT4         u4IfIndex;
      UINT1         u1ElmiUniStatus;
      UINT1         u1Reserved[3];
}tElmUniTrap;


typedef struct ElmAsynchMessageNode
{
    tElmSllNode           ElmAsynchNextNode;
    UINT4                 u4IfIndex;
    UINT2                 u2EvcRefId;
    UINT1                 u1EvcStatus;
    UINT1                 u1Reserved;
}tElmAsynchMessageNode;

typedef struct ElmOperTrap
{
      UINT4         u4IfIndex;
      UINT1         u1ElmiOperStatus;
      UINT1         u1Reserved[3];
}tElmOperTrap;

/* BELOW STRUCTURE IS USED FOR MEMPOOL CREATION ONLY*/
/* START */
typedef struct ElmPortEntryArray{
 /*This structure will be used only for "u4BlockSize" of MemCreateMemPool API.
   This structure should NOT be used in the protocol code.*/
    tElmPortEntry *apPortEntry[ELM_MAX_PORT_INFO];
}tElmPortEntryArray;
/*END */

#endif
