
#ifndef _ELMIGLOB_H_
#define _ELMIGLOB_H_


#ifdef _ELMAPI_C_
UINT4                           gElmCruAllocatedMemory;
UINT1                           gu1IsElmInitialised = ELM_FALSE;
UINT1                           gu1ElmSystemControl ;
tElmGlobalInfo                  gElmGlobalInfo;
UINT1                           gau1ElmPduBuf[ELM_MAX_ETH_FRAME_SIZE];

/* Sizing Params structure.*/
tFsModSizingParams gFsElmSizingParams [] =
{
    {"ELM_QMSG_MEMBLK_SIZE", "ELM_QMSG_MEMBLK_COUNT", 
     ELM_QMSG_MEMBLK_SIZE, ELM_QMSG_MEMBLK_COUNT, 
     ELM_QMSG_MEMBLK_COUNT, 0},

    {"ELM_LOCAL_MSG_MEMBLK_SIZE", "ELM_LOCAL_MSG_MEMBLK_COUNT", 
     ELM_LOCAL_MSG_MEMBLK_SIZE, ELM_LOCAL_MSG_MEMBLK_COUNT, 
     ELM_LOCAL_MSG_MEMBLK_COUNT, 0},

    {"ELM_CFGQ_MSG_MEMBLK_SIZE", "ELM_CFGQ_MSG_MEMBLK_COUNT", 
     ELM_CFGQ_MSG_MEMBLK_SIZE, ELM_CFGQ_MSG_MEMBLK_COUNT, 
     ELM_CFGQ_MSG_MEMBLK_COUNT, 0},

    {"((ELM_MAX_PORT_INFO) * sizeof (tElmPortEntry *))", 
     "ELM_PORT_TBL_MEMBLK_COUNT", 
     ((ELM_MAX_PORT_INFO) * sizeof (tElmPortEntry *)), 
     ELM_PORT_TBL_MEMBLK_COUNT, 
     ELM_PORT_TBL_MEMBLK_COUNT, 0},

    {"ELM_PORT_INFO_MEMBLK_SIZE", "ELM_MAX_PORT_INFO", 
     ELM_PORT_INFO_MEMBLK_SIZE, ELM_MAX_PORT_INFO, 
     ELM_MAX_PORT_INFO, 0},

    {"ELM_ASYNCH_LIST_MEMBLK_SIZE", "ELM_MAX_ASYNCH_LIST", 
     ELM_ASYNCH_LIST_MEMBLK_SIZE, ELM_MAX_ASYNCH_LIST, 
     ELM_MAX_ASYNCH_LIST, 0},

    {"ELM_TMRNODE_MEMBLK_SIZE", "ELM_MAX_NUM_TMRNODES", 
     ELM_TMRNODE_MEMBLK_SIZE, ELM_MAX_NUM_TMRNODES, 
     ELM_MAX_NUM_TMRNODES, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsElmSizingInfo;

/************************************************************/

#else
extern UINT4                           gElmCruAllocatedMemory;
extern UINT1                           gu1IsElmInitialised;
extern UINT1                           gu1ElmSystemControl ;
extern tElmGlobalInfo                  gElmGlobalInfo;
/* Sizing Params Structure */
extern tFsModSizingParams gFsElmSizingParams []; 
extern tFsModSizingInfo gFsElmSizingInfo;
extern UINT1                    gau1ElmPduBuf[ELM_MAX_ETH_FRAME_SIZE];

/************************************************************/


#endif

#endif
