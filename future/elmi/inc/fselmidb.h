/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselmidb.h,v 1.6 2008/08/20 14:11:51 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSELMIDB_H
#define _FSELMIDB_H

UINT1 FsElmiPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsElmiPortTrapNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fselmi [] ={1,3,6,1,4,1,2076,159};
tSNMP_OID_TYPE fselmiOID = {8, fselmi};


UINT4 FsElmiSystemControl [ ] ={1,3,6,1,4,1,2076,159,1,1};
UINT4 FsElmiModuleStatus [ ] ={1,3,6,1,4,1,2076,159,1,2};
UINT4 FsElmiActivePortCount [ ] ={1,3,6,1,4,1,2076,159,1,3};
UINT4 FsElmiTraceOption [ ] ={1,3,6,1,4,1,2076,159,1,4};
UINT4 FsElmiBufferOverFlowCount [ ] ={1,3,6,1,4,1,2076,159,1,5};
UINT4 FsElmiMemAllocFailureCount [ ] ={1,3,6,1,4,1,2076,159,1,6};
UINT4 FsElmiPort [ ] ={1,3,6,1,4,1,2076,159,1,7,1,1};
UINT4 FsElmiPortElmiStatus [ ] ={1,3,6,1,4,1,2076,159,1,7,1,2};
UINT4 FsElmiUniSide [ ] ={1,3,6,1,4,1,2076,159,1,7,1,3};
UINT4 FsElmiOperStatus [ ] ={1,3,6,1,4,1,2076,159,1,7,1,4};
UINT4 FsElmiStatusCounter [ ] ={1,3,6,1,4,1,2076,159,1,7,1,5};
UINT4 FsElmiPollingVerificationTimerValue [ ] ={1,3,6,1,4,1,2076,159,1,7,1,6};
UINT4 FsElmiPollingTimerValue [ ] ={1,3,6,1,4,1,2076,159,1,7,1,7};
UINT4 FsElmiPollingCounterValue [ ] ={1,3,6,1,4,1,2076,159,1,7,1,8};
UINT4 FsElmiNoOfConfiguredEvcs [ ] ={1,3,6,1,4,1,2076,159,1,7,1,9};
UINT4 FsElmiRxElmiCheckEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,10};
UINT4 FsElmiRxFullStatusEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,11};
UINT4 FsElmiRxFullStatusContEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,12};
UINT4 FsElmiTxElmiCheckMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,13};
UINT4 FsElmiTxFullStatusMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,14};
UINT4 FsElmiTxFullStatusContMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,15};
UINT4 FsElmiTxAsyncStatusMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,16};
UINT4 FsElmiRxElmiCheckMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,17};
UINT4 FsElmiRxFullStatusMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,18};
UINT4 FsElmiRxFullStatusContMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,19};
UINT4 FsElmiRxAsyncStatusMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,20};
UINT4 FsElmiTxElmiCheckEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,21};
UINT4 FsElmiTxFullStatusEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,22};
UINT4 FsElmiTxFullStatusContEnqMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,23};
UINT4 FsElmiRxValidMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,24};
UINT4 FsElmiRxInvalidMsgCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,25};
UINT4 FsElmiRelErrStatusTimeOutCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,26};
UINT4 FsElmiRelErrInvalidSeqNumCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,27};
UINT4 FsElmiRelErrInvalidStatusRespCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,28};
UINT4 FsElmiRelErrRxUnSolicitedStatusCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,29};
UINT4 FsElmiProErrInvalidProtVerCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,30};
UINT4 FsElmiProErrInvalidEvcRefIdCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,31};
UINT4 FsElmiProErrInvalidMessageTypeCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,32};
UINT4 FsElmiProErrOutOfSequenceInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,33};
UINT4 FsElmiProErrDuplicateInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,34};
UINT4 FsElmiProErrMandatoryInfoEleMissingCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,35};
UINT4 FsElmiProErrInvalidMandatoryInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,36};
UINT4 FsElmiProErrInvalidNonMandatoryInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,37};
UINT4 FsElmiProErrUnrecognizedInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,38};
UINT4 FsElmiProErrUnexpectedInfoEleCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,39};
UINT4 FsElmiProErrShortMessageCount [ ] ={1,3,6,1,4,1,2076,159,1,7,1,40};
UINT4 FsElmiSetGlobalTrapOption [ ] ={1,3,6,1,4,1,2076,159,2,1};
UINT4 FsElmiSetTraps [ ] ={1,3,6,1,4,1,2076,159,2,2};
UINT4 FsElmiErrTrapType [ ] ={1,3,6,1,4,1,2076,159,2,3};
UINT4 FsElmiPortTrapIndex [ ] ={1,3,6,1,4,1,2076,159,2,4,1,1};
UINT4 FsElmiPvtExpired [ ] ={1,3,6,1,4,1,2076,159,2,4,1,2};
UINT4 FsElmiPtExpired [ ] ={1,3,6,1,4,1,2076,159,2,4,1,3};
UINT4 FsElmiEvcStatus [ ] ={1,3,6,1,4,1,2076,159,2,4,1,4};
UINT4 FsElmiUniStatus [ ] ={1,3,6,1,4,1,2076,159,2,4,1,5};
UINT4 FsElmiEvcId [ ] ={1,3,6,1,4,1,2076,159,2,4,1,6};
UINT4 FsElmiErrType [ ] ={1,3,6,1,4,1,2076,159,2,4,1,7};
UINT4 FsElmiOperStatusStatus [ ] ={1,3,6,1,4,1,2076,159,2,4,1,8};


tMbDbEntry fselmiMibEntry[]= {

{{10,FsElmiSystemControl}, NULL, FsElmiSystemControlGet, FsElmiSystemControlSet, FsElmiSystemControlTest, FsElmiSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsElmiModuleStatus}, NULL, FsElmiModuleStatusGet, FsElmiModuleStatusSet, FsElmiModuleStatusTest, FsElmiModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsElmiActivePortCount}, NULL, FsElmiActivePortCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsElmiTraceOption}, NULL, FsElmiTraceOptionGet, FsElmiTraceOptionSet, FsElmiTraceOptionTest, FsElmiTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsElmiBufferOverFlowCount}, NULL, FsElmiBufferOverFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsElmiMemAllocFailureCount}, NULL, FsElmiMemAllocFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsElmiPort}, GetNextIndexFsElmiPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiPortElmiStatus}, GetNextIndexFsElmiPortTable, FsElmiPortElmiStatusGet, FsElmiPortElmiStatusSet, FsElmiPortElmiStatusTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "2"},

{{12,FsElmiUniSide}, GetNextIndexFsElmiPortTable, FsElmiUniSideGet, FsElmiUniSideSet, FsElmiUniSideTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "2"},

{{12,FsElmiOperStatus}, GetNextIndexFsElmiPortTable, FsElmiOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, "2"},

{{12,FsElmiStatusCounter}, GetNextIndexFsElmiPortTable, FsElmiStatusCounterGet, FsElmiStatusCounterSet, FsElmiStatusCounterTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "4"},

{{12,FsElmiPollingVerificationTimerValue}, GetNextIndexFsElmiPortTable, FsElmiPollingVerificationTimerValueGet, FsElmiPollingVerificationTimerValueSet, FsElmiPollingVerificationTimerValueTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "15"},

{{12,FsElmiPollingTimerValue}, GetNextIndexFsElmiPortTable, FsElmiPollingTimerValueGet, FsElmiPollingTimerValueSet, FsElmiPollingTimerValueTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "10"},

{{12,FsElmiPollingCounterValue}, GetNextIndexFsElmiPortTable, FsElmiPollingCounterValueGet, FsElmiPollingCounterValueSet, FsElmiPollingCounterValueTest, FsElmiPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsElmiPortTableINDEX, 1, 0, 0, "360"},

{{12,FsElmiNoOfConfiguredEvcs}, GetNextIndexFsElmiPortTable, FsElmiNoOfConfiguredEvcsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxElmiCheckEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxElmiCheckEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxFullStatusEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxFullStatusEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxFullStatusContEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxFullStatusContEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxElmiCheckMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxElmiCheckMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxFullStatusMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxFullStatusMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxFullStatusContMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxFullStatusContMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxAsyncStatusMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxAsyncStatusMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxElmiCheckMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxElmiCheckMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxFullStatusMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxFullStatusMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxFullStatusContMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxFullStatusContMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxAsyncStatusMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxAsyncStatusMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxElmiCheckEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxElmiCheckEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxFullStatusEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxFullStatusEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiTxFullStatusContEnqMsgCount}, GetNextIndexFsElmiPortTable, FsElmiTxFullStatusContEnqMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxValidMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxValidMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRxInvalidMsgCount}, GetNextIndexFsElmiPortTable, FsElmiRxInvalidMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRelErrStatusTimeOutCount}, GetNextIndexFsElmiPortTable, FsElmiRelErrStatusTimeOutCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRelErrInvalidSeqNumCount}, GetNextIndexFsElmiPortTable, FsElmiRelErrInvalidSeqNumCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRelErrInvalidStatusRespCount}, GetNextIndexFsElmiPortTable, FsElmiRelErrInvalidStatusRespCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiRelErrRxUnSolicitedStatusCount}, GetNextIndexFsElmiPortTable, FsElmiRelErrRxUnSolicitedStatusCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrInvalidProtVerCount}, GetNextIndexFsElmiPortTable, FsElmiProErrInvalidProtVerCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrInvalidEvcRefIdCount}, GetNextIndexFsElmiPortTable, FsElmiProErrInvalidEvcRefIdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrInvalidMessageTypeCount}, GetNextIndexFsElmiPortTable, FsElmiProErrInvalidMessageTypeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrOutOfSequenceInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrOutOfSequenceInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrDuplicateInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrDuplicateInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrMandatoryInfoEleMissingCount}, GetNextIndexFsElmiPortTable, FsElmiProErrMandatoryInfoEleMissingCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrInvalidMandatoryInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrInvalidMandatoryInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrInvalidNonMandatoryInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrInvalidNonMandatoryInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrUnrecognizedInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrUnrecognizedInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrUnexpectedInfoEleCount}, GetNextIndexFsElmiPortTable, FsElmiProErrUnexpectedInfoEleCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiProErrShortMessageCount}, GetNextIndexFsElmiPortTable, FsElmiProErrShortMessageCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElmiPortTableINDEX, 1, 0, 0, NULL},

{{10,FsElmiSetGlobalTrapOption}, NULL, FsElmiSetGlobalTrapOptionGet, FsElmiSetGlobalTrapOptionSet, FsElmiSetGlobalTrapOptionTest, FsElmiSetGlobalTrapOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsElmiSetTraps}, NULL, FsElmiSetTrapsGet, FsElmiSetTrapsSet, FsElmiSetTrapsTest, FsElmiSetTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsElmiErrTrapType}, NULL, FsElmiErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsElmiPortTrapIndex}, GetNextIndexFsElmiPortTrapNotificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiPvtExpired}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiPvtExpiredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiPtExpired}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiPtExpiredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiEvcStatus}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiEvcStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiUniStatus}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiUniStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiEvcId}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiEvcIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiErrType}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},

{{12,FsElmiOperStatusStatus}, GetNextIndexFsElmiPortTrapNotificationTable, FsElmiOperStatusStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElmiPortTrapNotificationTableINDEX, 1, 0, 0, NULL},
};
tMibData fselmiEntry = { 57, fselmiMibEntry };
#endif /* _FSELMIDB_H */

