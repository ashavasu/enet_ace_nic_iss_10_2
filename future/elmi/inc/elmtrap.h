/******************************************************************** 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved] 
 * * $Id: elmtrap.h,v 1.6 2011/04/28 11:42:49 siva Exp $
 *
 * Description: This file contains all macros and structures used
 *              for traps by the ELMI module.
 * *******************************************************************/
#ifndef _ELM_TRAP_H_
#define _ELM_TRAP_H_

extern INT1  ELM_PORT_TRAPS_OID[256];

/* BRG */
#define ELM_TRAP_TYPE gElmGlobalInfo.u4ElmTrapOption
#define ELM_GEN_TRAP gElmGlobalInfo.u1ElmGenTrapType
#define ELM_ERR_TRAP (gElmGlobalInfo.u1ElmErrTrapType)
#define ELM_GLOBAL_TRAP (gElmGlobalInfo.u4GlobalTrapOption)
#define ELM_PKT_ERR_VAL(u4IfIndex) \
        gElmGlobalInfo.ppPortEntry[u4IfIndex]->u2InvalidPktValue
#define IS_ELM_BRG_ERR_EVNT() \
    (gElmGlobalInfo.u4ElmTrapOption & 0x03) 
#define IS_ELM_BRG_ERR_EVNT_TRAP() \
    (gElmGlobalInfo.u4GlobalTrapOption & 0x01) 



/* Type code for normal trap type */
#define ELM_UP_TRAP 1
#define ELM_DOWN_TRAP 2

/* Type code for abnormal trap type */
#define ELM_TRAP_MEM_FAIL 1
#define ELM_TRAP_BUF_FAIL 2

/* Type code for INVALID BPDU_TRXD */
#define ELM_INVALID_PROTOCOL_ID 1
#define ELM_INVALID_BPDU_TYPE  2
#define ELM_TRAP_MAX  3

#define ELM_TRAP_SNMP_VERSION1  0
#define ELM_PORT_TRAPS_OID_LEN  10
#define ELM_OBJECT_NAME_MAX_LENGTH     256

#define ELM_DISABLE_ALL_TRAPS  0
#define ELM_MEM_FAILURE_TRAPS  1
#define ELM_PROTOCOL_TRAPS     2
#define ELM_ALL_TRAPS          3


#define ELM_MIB_OBJ_ERR_TYPE          "fsElmiErrType"
#define ELM_MIB_OBJ_ERR_TYPE_TRAP_INDEX "fsElmiErrTrapType"
#define ELM_MIB_OBJ_EVC_STATUS_TRAP_INDEX "fsElmiEvcStatus"
#define ELM_MIB_OBJ_UNI_STATUS_TRAP_INDEX "fsElmiUniStatus"
#define ELM_MIB_OBJ_EVC_ID_TRAP_INDEX "fsElmiEvcId"
#define ELM_MIB_OBJ_PT_STATUS_TRAP_INDEX "fsElmiPtExpired"
#define ELM_MIB_OBJ_PVT_STATUS_TRAP_INDEX "fsElmiPvtExpired"
#define ELM_MIB_OBJ_ERR_TRAP_TYPE    "fsElmiErrType"
#define ELM_MIB_OBJ_OPER_STATUS_TRAP_TYPE    "fsElmiOperStatusStatus"
#define ELM_SWITCH_ALIAS_LEN                 32


typedef struct _ELMBrgInvalidBpduRxdTrap{
  tMacAddr BrgAddr;
  UINT2   u2Pad;
  UINT4   u4IfIndex;
  UINT2   u2ErrVal;
  UINT1   u1ErrTrapType;
  UINT1   u1Reserved;
} tELMBrgInvalidBpduRxdTrap;    

VOID ElmiNewEvcCreatedTrap  (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
                       INT1 *pTrapsOid, UINT1 u1TrapOidLen);

VOID ElmiOldEvcDeletedTrap  (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
                       INT1 *pTrapsOid, UINT1 u1TrapOidLen);
VOID ElmiEvcStatusChangedTrap  (UINT4 u4IfIndex, UINT1 *pelmiEvcId,
               INT1 *pTrapsOid, UINT1 u1TrapOidLen);

VOID ElmiUniStatusChangedTrap (UINT4 u4IfIndex,
                               INT1 *pTrapsOid,
                               UINT1 u1TrapOidLen);
VOID ElmInvalidPduRxdTrap (UINT4 u4IfIndex,
                           UINT1 u1ErrType,
                           INT1 *pTrapsOid,
                           UINT1 u1TrapOidLen);
VOID ElmMemFailTrap (INT1 *pTrapsOid,
                     UINT1 u1TrapOidLen);
VOID ElmGlobalMemFailTrap (INT1 *pTrapsOid,
                           UINT1 u1TrapOidLen);
VOID ElmBufferFailTrap (INT1 *pTrapsOid,
                        UINT1 u1TrapOidLen);
VOID ElmiPvtExpiredTrap (UINT4 u4IfIndex,
                         INT1 *pTrapsOid,
                         UINT1 u1TrapOidLen);
VOID ElmiPtExpiredTrap (UINT4 u4IfIndex,
                        INT1 *pTrapsOid,
                        UINT1 u1TrapOidLen);

VOID ElmSnmpIfSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                       VOID *pTrapInfo);
VOID
ElmiOperStatusChangedTrap(UINT4 u4IfIndex,UINT1 u1ElmOperStatus,
                           INT1 *pi1TrapsOid, UINT1 u1TrapOidLen);

tSNMP_OID_TYPE * ElmMakeObjIdFromDotNew (INT1 *pi1TextStr);
INT4 ElmParseSubIdNew (INT1 **ppi1TempPtr);
#endif
