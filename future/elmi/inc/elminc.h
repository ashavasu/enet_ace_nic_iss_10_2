/********************************************************************g 
* Copyright (C) 2006 Aricent Inc . All Rights Reserved] * 
* $Id: elminc.h,v 1.7 2011/09/24 06:56:44 siva Exp $ * 
* Description: This file contains all the header files to be 
*              included and used by ELMI Module. * 
*******************************************************************/
#ifndef _ELMHDRS_H_
#define _ELMHDRS_H_
#include "lr.h"
#include "cfa.h"
#include "uni.h"
#include "snmctdfs.h"
#include "snmccons.h" 
#include "fssyslog.h"
#include "snmputil.h" 
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "iss.h"
#include "cli.h"
#include "vcm.h"
#include "evcpro.h"
#include "lcm.h"
#include "elmconst.h"
#include "elmmacr.h"
#include "elmtdfs.h"
#include "elm.h"
#include "elmtrc.h"
#include "elmdbg.h"
#include "elmtrc.h"
#include "elmprot.h"
#include "elmtrap.h"
#include "elmglob.h"
#include "fselmiwr.h"
#include "elmcli.h"
#include "elmclilow.h"
#include "elmport.h"
#include "elmsz.h"
#ifdef L2RED_WANTED
#include "rmgr.h"
#include "rstp.h"
#include "elmred.h"
#include "bridge.h"
#include "snp.h"
#else
#include "elmrdstb.h"
#endif
#endif /* _ELMHDRS_H_ */
