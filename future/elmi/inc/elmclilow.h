/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmclilow.h,v 1.4 2008/08/26 04:18:49 premap-iss Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the CLI Routines for ELMI functionality.
 *
 *******************************************************************/

#ifndef _ELMCLILOW_H
#define _ELMCLILOW_H_


INT4
ElmCliSetGlobalModuleStatus (tCliHandle CliHandle, UINT4 u4ElmModStatus);
INT4
ElmCliClearStat (UINT4 u4IfIndex);
INT4
ElmSetTrace (tCliHandle CliHandle, UINT4 u4ElmTrc, UINT1 u1Action);
INT4
ElmCliDisplayDetails (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Type);
INT4
ElmCliSetPollingCounter (tCliHandle CliHandle,INT4 i4Index, UINT4 u4PollingCounterValue);
INT4
ElmCliSetStatusCounter (tCliHandle CliHandle, INT4 i4Index,UINT4 u4StatusCounterValue);
INT4
ElmCliSetPollingTimer (tCliHandle CliHandle, INT4 i4Index,UINT4 u4PollingTimerValue);
INT4
ElmCliSetPollingVerificationTimer (tCliHandle CliHandle,INT4 i4Index, UINT4 u4PollingVerificationTimerValue);

INT4 ElmCliSetSystemControl(tCliHandle CliHandle,UINT4 u4ElmSystemControl);
INT4 ElmCliSetNetworkMode (tCliHandle CliHandle,INT4 i4Index);
INT4 ElmCliSetCustomerMode (tCliHandle CliHandle,INT4 i4Index );
INT4 ElmCliSetPortModuleStatus (tCliHandle CliHandle,INT4 i4Index,UINT4 u4ElmModStatus);
    
INT4 ElmShowRunningConfigScalars(tCliHandle);


VOID ElmShowRunningConfigInterface(tCliHandle);
INT4 ElmShowRunningConfig(tCliHandle,UINT4);
VOID ElmShowRunningConfigInterfaceDetails(tCliHandle,INT4);
INT4
ElmCliShowStatus (tCliHandle CliHandle, INT4 i4Index, UINT4 u4Type);
VOID ElmShowPortStatsDetails(tCliHandle,INT4);


#endif
