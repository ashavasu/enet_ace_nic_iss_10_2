#ifndef _FSELMIWR_H
#define _FSELMIWR_H

VOID RegisterFSELMI(VOID);

VOID UnRegisterFSELMI(VOID);
INT4 FsElmiSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiActivePortCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiBufferOverFlowCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiMemAllocFailureCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsElmiModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsElmiTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);



INT4 GetNextIndexFsElmiPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsElmiPortElmiStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiUniSideGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiStatusCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingVerificationTimerValueGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingTimerValueGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingCounterValueGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiNoOfConfiguredEvcsGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxElmiCheckEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxFullStatusEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxFullStatusContEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxElmiCheckMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxFullStatusMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxFullStatusContMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxAsyncStatusMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxElmiCheckMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxFullStatusMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxFullStatusContMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxAsyncStatusMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxElmiCheckEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxFullStatusEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiTxFullStatusContEnqMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxValidMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRxInvalidMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRelErrStatusTimeOutCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRelErrInvalidSeqNumCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRelErrInvalidStatusRespCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiRelErrRxUnSolicitedStatusCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrInvalidProtVerCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrInvalidEvcRefIdCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrInvalidMessageTypeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrOutOfSequenceInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrDuplicateInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrMandatoryInfoEleMissingCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrInvalidMandatoryInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrInvalidNonMandatoryInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrUnrecognizedInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrUnexpectedInfoEleCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiProErrShortMessageCountGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPortElmiStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiUniSideSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiStatusCounterSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingVerificationTimerValueSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingTimerValueSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingCounterValueSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPortElmiStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiUniSideTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiStatusCounterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingVerificationTimerValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingTimerValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiPollingCounterValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 FsElmiSetGlobalTrapOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSetTrapsGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSetGlobalTrapOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSetTrapsSet(tSnmpIndex *, tRetVal *);
INT4 FsElmiSetGlobalTrapOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiSetTrapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsElmiSetGlobalTrapOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsElmiSetTrapsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFsElmiPortTrapNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsElmiPvtExpiredGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiPtExpiredGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiEvcStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiUniStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiEvcIdGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiErrTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsElmiOperStatusStatusGet(tSnmpIndex *, tRetVal *);
#endif /* _FSELMIWR_H */
