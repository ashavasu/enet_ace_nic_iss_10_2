/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmprot.h,v 1.10 2011/09/24 06:56:44 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the ELMI Module.
 *
 *******************************************************************/


#ifndef _ELMPROT_H_
#define _ELMPROT_H_

/***************** COMMON FUNCTIONALITY **********/
INT4    ElmTaskInit(VOID);
VOID    ElmTaskDeInit (VOID);
INT4    ElmHandleModuleInit (VOID);
VOID    ElmHandleModuleShutdown (VOID);
VOID    ElmiRegisterMIBS (VOID);
INT4    ElmHandleElmiGlobalEnable(VOID);
INT4    ElmHandleElmiGlobalDisable(VOID);
INT4    ElmModuleEnablePerPort (UINT4 u4IfIndex, UINT1 u1TriggerType);
INT4    ElmModuleDisablePerPort (UINT4 u4IfIndex, UINT1 u1TriggerType);

INT4    ElmReceivedPdu (tElmBufChainHeader * pBuf, UINT4 u4IfIndex);
INT4    ElmVerifyPdu (UINT1 *pu1Buf,
                      UINT4              u4IfIndex,
                      UINT1              *pu1MsgType);
VOID    ElmHandlePduMsg(tElmBufChainHeader  *pPdu);
INT4    ElmFormElmiPdu (tElmPortEntry *pElmPortEntry,UINT1 u1PduType,
                        UINT1 u1ReportType,UINT4 *pu4DataLength);
INT4    ElmHandleInPdu (UINT1 *pu1Buf, UINT4 u4IfIndex,UINT2 u2DataLength);
VOID    ElmFillEnquiry(tElmPortEntry *pElmPortEntry,UINT1 u1MsgType, 
                       UINT1 u1ReportType,UINT4 *pu4DataLength);
VOID    ElmPtExpiredOperStatusCalc (tElmPortEntry *pPortEntry);
VOID    ElmRaiseReliabilityError(VOID);
VOID    ElmRaiseProtocolError(VOID);
VOID    ElmHandleSnmpCfgMsg(tElmMsgNode *pMsgNode);
INT4    ElmHandleEvcStatusChangeEvent (UINT4 u4IfIndex, UINT2 u2EvcRefId, 
                               UINT1 u1EvcStatus);
VOID    ElmHandleEvcInformationChangeEvent (UINT2 u2EvcRefId);
INT4    ElmSendEventToElmTask(tElmMsgNode *pNode);
INT4    ElmProcessMessage(tElmMsgNode *pNode);
INT4    ElmTimerInit(VOID);
INT4    ElmTimerDeInit(VOID);
INT4    ElmStartTimer (VOID *pPortPtr, UINT1 u1TimerType, UINT2 u2Duration);
INT4    ElmStopTimer (VOID *pPortPtr, UINT1 u1TimerType);
INT4    ElmTmrExpiryHandler (VOID);

INT4    ElmValidatePortEntry(UINT4 u4IfIndex);
INT4    ElmProcessSnmpRequest (tElmMsgNode *pBuf);
INT4    ElmGetLocalPortIdFromIfIndex (UINT4 u4IfIndex, UINT2 *pU2LocalPort);
VOID    ElmOperStatusCalc(UINT4 u4IfIndex);
INT4
ElmVerifyDataInstance (UINT1 *pu1Buf,UINT4 u4IfIndex,
                       UINT4 *pu4DataInstanceValue);
INT4    ElmSeqNumAndDiFn (UINT1 *pBuf,
                          tElmPortEntry *pPortEntry);

INT4    ElmSnmpLowValidatePortIndex (INT4 i4ElmiPortIndex);
INT4    ElmSnmpLowGetNextValidIndex (INT4 i4ElmiPortIndex,
                                     INT4 *pi4NextElmiPortIndex);
INT4    ElmSnmpLowGetFirstValidIndex (INT4 *i4ElmiPortIndex);
VOID    ElmStopAllRunningTimers (UINT4 u4IfIndex);
INT4    ElmAddToIfIndexTable (tElmPortEntry * pElmPortEntry);
VOID    ElmInitPortInfo (tElmPortEntry *pPortEntry);
INT4    ElmRemoveFromIfIndexTable (tElmPortEntry *pElmPortEntry);
INT4    ElmDefaultCfgMsgHandler (tElmMsgNode * pMsgNode);
VOID    ElmPktDump (tElmBufChainHeader * pMsg, UINT2 u2Length);
INT4    ElmHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tElmPortEntry * pElmPortInfo, UINT4 u4PktSize);
INT4    ElmHandleLocalMsgReceived (tElmMsgNode * pMsgNode);
VOID    ElmAssignMempoolIds (VOID);
INT4    ElmAllocPortInfoBlock (VOID);
INT4    ElmHandleCreatePort (UINT4 u4IfIndex);
INT4    ElmHandleDeletePort (UINT4 u4IfIndex);

/***************** ELMI UNI-C FUNCTIONALITY **********/

INT4    ElmSendFullStatusEnqMessage (tElmPortEntry    *pElmPortEntry, 
                                     tElmBufChainHeader    *pBuf, 
                                     UINT4 *pu4DataLength);
INT4    ElmSendElmiCheckEnqMessage (tElmPortEntry    *pElmPortEntry, 
                                    tElmBufChainHeader    *pBuf, 
                                    UINT4 *pu4DataLength);

INT4    ElmStartPollingTimer (VOID    *pPortPtr, UINT2    u2Duration);
INT4    ElmStopPollingTimer (VOID    *pPortPtr);
INT4    ElmPtExpired(tElmPortEntry      *pElmPortEntry);
INT4    ElmFillHeader (tElmPortEntry * pElmPortEntry);

INT4    ElmRcvdCheckStatus (UINT1 *pBuf, UINT4 u4IfIndex, UINT2 u2DataLength);
INT4    ElmRcvdFullStatus (UINT1 *pBuf, UINT4 u4IfIndex,UINT2 u2DataLength);
INT4    ElmRcvdFullStatusContd (UINT1 *pBuf, UINT4 u4IfIndex,UINT2 u2DataLength);
INT4    ElmRcvdAsynStatus (UINT1 *pBuf, UINT4 u4IfIndex, UINT2 u2DataLength);
INT4    ElmPortTxStatusEnquiryInit (UINT1 u1Event, 
                                    tElmPortEntry * pElmPortEntry);
INT4    ElmPortTxEnquiryMessage (tElmPortEntry * pElmPortEntry);
INT4    ElmPortTxElmiPdu (tElmPortEntry * pElmPortEntry,
                                    UINT1 u1ReportType);
INT4    ElmHandleCustomerTlv (UINT1 *pBuf, UINT4 u4IfIndex,UINT2 u2DataLength);
INT4    ElmVerifySequenceNumber (UINT1 *pu1Buf, UINT4 u4IfIndex);
INT4    ElmRcvdFullStatusContinued (UINT1 *pu1Buf, UINT4 u4IfIndex, 
                                    UINT2 u2DataLength);
INT4    ElmParseRcvdTLV (UINT1 *pu1Buf, UINT4 u4IfIndex,UINT2 u2DataLength, 
                         UINT1 *au1PacketTLV, UINT1 u1NoOfTlv, UINT1 u1ReportType);
INT4    ElmHandleUniStatusTlv(UINT1 *pu1Buf, UINT4 u4IfIndex,
                              UINT1 u1ElmUniInfoLength);
INT4    ElmHandleEvcStatusTlv(UINT1 *pu1Buf, UINT4 u4IfIndex,
                              UINT1 u1EvcInfoLength, UINT1 u1ReportType);
INT4    ElmHandleCeVlanTlv(UINT1 *pu1Buf,UINT4  u4IfIndex,
                           UINT1 u1CeVlanInfoLength);
INT1    ElmParseEvcSubInfo(UINT1 *pu1Buf, UINT4 u4IfIndex,
                           UINT1 u1EvcInfoLength,
                           tLcmEvcStatusInfo *pLcmEvcStatusInfo);
INT1    ElmParseUniSubInfo(UINT1 *pu1Buf, UINT4 u4IfIndex,
                           UINT1 u1ElmUniInfoLength,tCfaUniInfo *pCfaUniInfo);
VOID ElmFlushLcmDataBase(UINT4 u4IfIndex);
/***************** ELMI UNI-N FUNCTIONALITY **********/

INT4    ElmProtocolInit(VOID);
INT4    ElmRcvdFullStatusContdEnq (UINT4 u4IfIndex);
VOID    ElmAsyncTmrExpired(tElmPortEntry      *pElmPortEntry);
INT4    ElmRcvdFullStatusEnq (UINT4 u4IfIndex);
INT4    ElmSendAsynStatus (tElmPortEntry *pElmPortEntry,
                           tElmBufChainHeader *pBuf, UINT4 *pu4DataLength);
INT4    ElmHandleNetworkTlv (UINT1 *pElmPdu, UINT4 u4IfIndex, UINT2 u2DataLength);
INT4    ElmPortTxElmiCheckStatusMessage (tElmPortEntry * pElmPortEntry);
INT4    ElmPortTxAsyncStatusMessage (UINT2 u2EvcRefId, UINT4 u4IfIndex,
                                     UINT1 u1EvcStatus);
INT4    ElmPortTxStatusMessage (tElmPortEntry * pElmPortEntry,UINT1 u1TriggerType,
                           UINT1 *pu1PktTxed);
INT4    ElmSendFullStatus (tElmPortEntry *pElmPortEntry, tCfaUniInfo CfaUniInfo, 
                           UINT1 u1UniStatusIeLength,UINT4 u2DataLength,
                           UINT1 u1TriggerType);
INT4    ElmSendFullStatusContd (tElmPortEntry *pElmPortEntry, 
                                UINT4 u4DataLength,UINT1 u1TriggerType);
INT4    ElmFillHeaderInCru (tElmPortEntry * pElmPortEntry, 
                            tElmBufChainHeader *pElmPdu);
VOID    ElmFillEvcStatusIe (tLcmEvcStatusInfo *pLcmEvcStatusInfo,UINT1 **ppu1PduEnd,
                            UINT2 *pu2DataLength );
VOID    ElmFillBwProfile (tBwProfile *pBwProfile, UINT1 **ppu1PduEnd);
VOID    ElmFillCeVlanMapIe (tLcmEvcCeVlanInfo *pLcmEvcCeVlanInfo,
                            tElmPortEntry *pElmPortEntry,  
                            UINT1 **ppu1PduEnd,
                            UINT2 *pu2DataLength);
INT4    ElmFillFixedTlv(tElmPortEntry *pElmPortEntry,
                        tElmBufChainHeader *pElmPdu,
                    UINT1 u1ReportType);
INT4    ElmFillUniStatusIe (tCfaUniInfo CfaUniInfo, 
                            tElmBufChainHeader *pElmPdu,
                        UINT1 u1UniStatusIeLength);
VOID    ElmMsgRcvdOperStatusCalc(tElmPortEntry *pElmPortEntry);
INT4    ElmPvtExpired(UINT4 u4IfIndex);
INT4    ElmAsynchTimerExpired (UINT4 u4IfIndex);
VOID    ElmGetEvcStatusLength(tLcmEvcStatusInfo *pLcmEvcStatusInfo, 
                              UINT1 *u1EvcStatusLength);
INT4    ElmRcvdCheckEnq (UINT4 u4IfIndex);
INT4    ElmFillHeaderAndTransmit (tElmPortEntry *pElmPortEntry, 
                                  UINT4 u4DataLength);
VOID    ElmResetVarsAfterFullStatus(tElmPortEntry *pElmPortEntry);
VOID    ElmPreProcessPackets(UINT4 u4IfIndex);
VOID    ElmGetVlanCapacity(UINT2 u2DataLength, UINT2 *pu2NoOfPossibleVlanInfo);
#endif /* _ELMPROT_H_ */
