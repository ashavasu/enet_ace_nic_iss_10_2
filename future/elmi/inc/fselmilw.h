/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselmilw.h,v 1.5 2008/08/20 14:11:51 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElmiSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsElmiModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsElmiActivePortCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElmiTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsElmiBufferOverFlowCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElmiMemAllocFailureCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElmiSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsElmiModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsElmiTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElmiSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsElmiModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsElmiTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElmiSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsElmiModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsElmiTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElmiPortTable. */
INT1
nmhValidateIndexInstanceFsElmiPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElmiPortTable  */

INT1
nmhGetFirstIndexFsElmiPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElmiPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElmiPortElmiStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiUniSide ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiStatusCounter ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiPollingVerificationTimerValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiPollingTimerValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiPollingCounterValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiNoOfConfiguredEvcs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiRxElmiCheckEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxFullStatusEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxFullStatusContEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxElmiCheckMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxFullStatusMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxFullStatusContMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxAsyncStatusMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxElmiCheckMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxFullStatusMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxFullStatusContMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxAsyncStatusMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxElmiCheckEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxFullStatusEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiTxFullStatusContEnqMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxValidMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRxInvalidMsgCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRelErrStatusTimeOutCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRelErrInvalidSeqNumCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRelErrInvalidStatusRespCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiRelErrRxUnSolicitedStatusCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrInvalidProtVerCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrInvalidEvcRefIdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrInvalidMessageTypeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrOutOfSequenceInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrDuplicateInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrMandatoryInfoEleMissingCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrInvalidMandatoryInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrUnrecognizedInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrUnexpectedInfoEleCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsElmiProErrShortMessageCount ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElmiPortElmiStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsElmiUniSide ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsElmiStatusCounter ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsElmiPollingVerificationTimerValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsElmiPollingTimerValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsElmiPollingCounterValue ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElmiPortElmiStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsElmiUniSide ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsElmiStatusCounter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsElmiPollingVerificationTimerValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsElmiPollingTimerValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsElmiPollingCounterValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElmiPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElmiSetGlobalTrapOption ARG_LIST((INT4 *));

INT1
nmhGetFsElmiSetTraps ARG_LIST((INT4 *));

INT1
nmhGetFsElmiErrTrapType ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElmiSetGlobalTrapOption ARG_LIST((INT4 ));

INT1
nmhSetFsElmiSetTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElmiSetGlobalTrapOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsElmiSetTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElmiSetGlobalTrapOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsElmiSetTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElmiPortTrapNotificationTable. */
INT1
nmhValidateIndexInstanceFsElmiPortTrapNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElmiPortTrapNotificationTable  */

INT1
nmhGetFirstIndexFsElmiPortTrapNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElmiPortTrapNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElmiPvtExpired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiPtExpired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiEvcStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiUniStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiEvcId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsElmiErrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsElmiOperStatusStatus ARG_LIST((INT4 ,INT4 *));
