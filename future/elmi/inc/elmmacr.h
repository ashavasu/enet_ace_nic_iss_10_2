/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmmacr.h,v 1.13 2012/04/04 14:14:46 siva Exp $
 *
 * Description: This file contains all the macros used by ELMI Module.
 *
 *******************************************************************/


#ifndef _ELMMACR_H_
#define _ELMMACR_H_

/******************************************************************************/
/*                             MACROS                                         */
/******************************************************************************/

#define     ELM_IS_ELMI_INITIALISED()            gu1IsElmInitialised

#define ELM_IS_ELMI_ENABLED()    \
           ((ELM_IS_ELMI_INITIALISED() == ELM_TRUE) && \
            (ELM_SYSTEM_CONTROL == (UINT1)ELM_START) && \
            (ELM_GLOBAL_STATUS == (UINT1)ELM_ENABLE_GLOBAL)) 

#define ELM_IS_ELMI_STARTED()    \
           ((ELM_IS_ELMI_INITIALISED() == ELM_TRUE) && \
            (ELM_SYSTEM_CONTROL == (UINT1)ELM_START)) 
#define     ELM_SET_PORT_MODE(u4IfIndex,u1Mode)\
do {\
    if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
    {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmiMode=u1Mode; \
    }\
}while(0)

#define ELM_GET_PORT_MODE(u4IfIndex) \
        ((ELM_GET_PORTENTRY(u4IfIndex) != NULL)?(gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmiMode:0)

#define     ELM_IS_ELMI_ENABLED_ON_PORT(u4IfIndex) \
            ((ELM_GET_PORTENTRY(u4IfIndex))!=NULL?\
            (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmiPortStatus == ELM_PORT_ELMI_ENABLED:0)

#define     ELM_INC_MEM_FAILURE_COUNT() \
                (gElmGlobalInfo.u4MemoryFailureCount)++    

#define     ELM_GLOBAL_IFINDEX_TREE() \
                gElmGlobalInfo.GlobalIfIndexTable

#define     ELM_SYSTEM_CONTROL  gu1ElmSystemControl

#define     ELM_GLOBAL_STATUS \
                (gElmGlobalInfo.u1GlobalModuleStatus)

#define     ELM_TRACE_OPTION \
                (gElmGlobalInfo.u4GlobalTraceOption)

#define     ELM_GET_IF_STATUS(u4IfIndex) \
            (ELM_GET_PORTENTRY(u4IfIndex)!=NULL?(gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1PortStatus:\
             ELM_PORT_OPER_DOWN)


#define     ELM_GET_PORTENTRY(u4IfIndex) \
            (((u4IfIndex <=0)||(u4IfIndex >ELM_MAX_NUM_PORTS_SUPPORTED))? NULL\
            :(gElmGlobalInfo.ppPortEntry[u4IfIndex-1]))

#define     ELM_SET_PORTENTRY(u4IfIndex,pPortEntry) \
do {\
   if ((u4IfIndex > 0) && (u4IfIndex <=ELM_MAX_NUM_PORTS_SUPPORTED))\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1]) = pPortEntry;\
   }\
}while(0)

#define     ELM_PORT_TBL() \
                (gElmGlobalInfo.ppPortEntry)

#define     ELM_IS_PVT_ENABLED(u4IfIndex) \
      ((ELM_GET_PORTENTRY(u4IfIndex)!=NULL)?\
       ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1PvtValueConfigured>0 ?ELM_TRUE:ELM_FALSE):ELM_FALSE)

#define     ELM_INCR_MISSING_MANDATORY_IE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrMandatoryInfoEleMissingCount++;\
   }\
}while(0)

#define     ELM_INCR_DUPLICATE_IE(u4IfIndex) \
do {\
    if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
    {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrDuplicateInfoEleCount++;\
    }\
}while(0)

#define     ELM_INCR_INVALID_SEQUENCE_NUMBER(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRelErrInvalidSeqNumCount++;\
   }\
}while(0)

#define     ELM_INCR_INVALID_MANDATORY_IE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrInvalidMandatoryInfoEleCount++;\
   }\
}while(0)

#define     ELM_INCR_INVALID_NON_MANDATORY_IE(u4IfIndex) \
do {\
    if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
    {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrInvalidNonMandatoryInfoEleCount++;\
    }\
}while(0)

#define     ELM_INCR_OUT_OF_SEQUENCE_IE_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrOutOfSequenceInfoEleCount++;\
   }\
}while (0)

#define ELM_INCR_INVALID_MSG_RX_COUNT(u4IfIndex) \
do {\
   if(ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiInvalidMsgRxed++;\
   }\
}while (0)

#define ELM_INCR_REALIBLITY_ERR_INVALID_STATUS_MSG_RX_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRelErrInvalidStatusCount++;\
   }\
}while (0)

#define ELM_INCR_PROT_ERR_UNEXPECTD_IE_RX_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex)!= NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrUnexpectedInfoEleCount++;\
   }\
}while (0)

#define ELM_INCR_PROT_ERR_UNRECOGNISED_IE_RX_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrUnrecognizedInfoEleCount++;\
   }\
}while (0)

#define ELM_INCR_REL_ERR_STATUS_TIME_OUT_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRelErrStatusTimeoutCount++;\
   }\
}while (0)

#define ELM_INCR_RX_VALID_MSG_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiValidMsgRxed++;\
   }\
}while (0)

#define ELM_INCR_RX_VALID_ELMI_CHECK_STATUS_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex)!=NULL) \
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxCheckMsg++;\
   }\
}while (0)

#define ELM_INCR_RX_UNSOLICITED_STATUS_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRelErrRxUnsolicitedStatusCount++;\
   }\
}while (0)

#define ELM_INCR_SHORT_MSG_RX_COUNT(u4IfIndex) \
do {\
    if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
    {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrShortMessageCount++;\
    }\
}while (0)

#define ELM_INCR_ERROR_PROT_VER_RX_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrInvalidProtVerCount++;\
   }\
}while (0)

#define ELM_INCR_INVALID_MSG_TYPE_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrInvalidMessageTypeCount++;\
   }\
}while (0)

#define ELM_INCR_INVALID_EVC_REF_ID_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrInvalidEvcRefIdCount++;\
   }\
}while (0)

#define     ELM_DECR_STATUS_COUNTER(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       if (((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1StatusCounter) > 0)\
       {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1StatusCounter --;\
       }\
   }\
}while (0)

#define     ELM_INCR_BUFFER_FAILURE_COUNT() \
                (gElmGlobalInfo.u4BufferFailureCount)++

#define     ELM_INCR_MEMORY_FAILURE_COUNT() \
                (gElmGlobalInfo.u4MemoryFailureCount)++

#define     ELM_INCR_TX_FULL_STATUS_ENQUIRY_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxFullStatEnquiryMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_ELMI_CHECK_ENQUIRY_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxElmiCheckEnquiryMsg++;\
   }\
}while (0)
#define     ELM_INCR_SEND_SEQUENCE_COUNTER_VALUE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      if ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter == 255)\
      {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter =1;\
      }\
      else \
      {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter++;\
      }\
    }\
}while (0) 

#define     ELM_INCR_DATA_INSTANCE(u4IfIndex)\
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      if ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4DataInstance == 0xffffffff)\
      {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4DataInstance =1;\
      }\
      else \
      {\
        (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4DataInstance++;\
      }\
    }\
}while (0) 

#define     ELM_INCR_POLLING_COUNTER_VALUE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2PollingCounter++;\
   }\
}while (0)

#define     ELM_INCR_TX_ELMI_CHECK(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxElmiCheckMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_FULL_STATUS(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxFullStatusMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_FULL_STATUS_CONTD(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxFullStatusContdMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_ASYNCHRONOUS_STATUS(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxAsyncStatusMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_DATA_INSTANCE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4DataInstance++;\
   }\
}while (0)

#define     ELM_INCR_RX_ELMI_CHECK_ENQUIRY(u4IfIndex)\
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxElmiCheckEnquiryMsg++;\
   }\
}while (0)

#define     ELM_INCR_RX_ELMI_FULL_STATUS_ENQUIRY(u4IfIndex)\
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxFullStatusEnquiryMsg++;\
   }\
}while (0)

#define     ELM_INCR_RX_ELMI_FULL_STATUS_CONTD_ENQUIRY(u4IfIndex)\
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxFullStatusContdEnquiryMsg++;\
   }\
}while (0)

#define     ELM_INCR_CE_VLAN_SEQ_NUMBER(u1CeVlanSeqNo) \
                ((u1CeVlanSeqNo == 63)? (u1CeVlanSeqNo = 0):(u1CeVlanSeqNo++))

#define     ELM_INCR_INVALID_UNEXPECTED_IE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiProErrUnexpectedInfoEleCount++;\
   }\
}while (0)

#define     ELM_INC_NO_OF_EVC_CONFIGURED_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2NoOfEvcConfigured++;\
   }\
}while (0)

#define     ELM_DECR_NO_OF_EVC_CONFIGURED_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2NoOfEvcConfigured--;\
   }\
}while (0)

#define ELM_INCR_VLANS_MAPPED(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex)!= NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2NoOfVlansMapped++;\
   }\
}while (0)

#define     ELM_DECR_POLLING_COUNTER_VALUE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      if ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2PollingCounter == ELM_INIT_VAL)\
      {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2PollingCounter = \
               ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2PollingCounterConfigured);\
      }\
      else\
      {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u2PollingCounter--;\
      }\
   }\
}while (0)

#define     ELM_DECR_SEND_SEQUENCE_COUNTER_VALUE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
      if ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter == 1 ) \
      {\
         (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter = 255; \
      }\
      else\
      {\
          (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1SendSeqCounter--;\
      }\
}while (0)

#define     ELM_INCR_RX_VALID_ASYN_STATUS_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxAsyncStatusMsg++;\
   }\
}while (0)

#define     ELM_INCR_RX_FULL_STATUS_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxFullStatMsg++;\
   }\
}while (0)

#define     ELM_INCR_RX_FULL_STATUS_CONTINUED_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex)  != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiRxFullStatContdMsg++;\
   }\
}while (0)

#define     ELM_INCR_TX_FULL_STATUS_CONTINUED_ENQ_MSG_COUNT(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u4ElmiTxFullStatContdEnquiryMsg++;\
   }\
}while (0)

#define     ELM_IFENTRY_IFINDEX(pPortEntry)\
            (pPortEntry)->u4IfIndex
#define    ELM_INCR_NO_OF_ELMI_ENABLE()\
           (gElmGlobalInfo.u2NoOfElmiEnabledPorts)++ 	
#define    ELM_DECR_NO_OF_ELMI_ENABLE()\
           (gElmGlobalInfo.u2NoOfElmiEnabledPorts)-- 	

/* Calculated for 2-per-port timers */
#define     ELM_TMR_LIST_ID             gElmGlobalInfo.TmrListId  
#define     ELM_MSG_LIST_ID             gElmGlobalInfo.MsgSllId

#define     ELM_CREATE_TMR_LIST         TmrCreateTimerList
#define     ELM_DELETE_TMR_LIST         TmrDeleteTimerList
#define     ELM_START_TIMER             TmrStartTimer
#define     ELM_STOP_TIMER              TmrStopTimer
#define     ELM_GET_NEXT_EXPIRED_TMR    TmrGetNextExpiredTimer
#define     ELM_GET_SYS_TIME            OsixGetSysTime
#define     ELM_GET_REMAINING_TIME      TmrGetRemainingTime

/******************************************************************************/
/* Semaphore Related Macros */
/******************************************************************************/

#define     ELM_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
                OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))
  
#define     ELM_DELETE_SEM  OsixDeleteSem

#define     ELM_TAKE_SEM  OsixSemTake

#define     ELM_GIVE_SEM  OsixSemGive

#define     ELM_UNUSED(x)                   {x = x;}
#define     ELM_CREATE_QUEUE                OsixQueCrt
#define     ELM_DELETE_QUEUE                OsixQueDel
#define     ELM_GET_TASK_ID                 OsixGetTaskId
#define     ELM_SEND_TO_QUEUE               OsixQueSend
#define     ELM_RECV_FROM_QUEUE             OsixQueRecv
#define     ELM_RECEIVE_EVENT               OsixEvtRecv
#define     ELM_OSIX_EV_ANY                 OSIX_EV_ANY
#define     ELM_OSIX_NO_WAIT                OSIX_NO_WAIT
#define     ELM_DEF_MSG_LEN                 OSIX_DEF_MSG_LEN 

#define     ELM_HTONS                       OSIX_HTONS
#define     ELM_HTONL                       OSIX_HTONL
#define     ELM_NTOHS                       OSIX_NTOHS
#define     ELM_NTOHL                       OSIX_NTOHL

#define     ELM_SEND_EVENT                  OsixEvtSend

#define     ELM_GET_PDU_FROM_ELM_QUEUE      ELM_RECV_FROM_QUEUE
#define     ELM_OSIX_MSG_NORMAL             OSIX_MSG_NORMAL
#define     ELM_SELF                        SELF
#define     ELM_QMSG_TYPE(pMsg)             pMsg->LocalMsgType
#define     ELM_MEMCPY                      MEMCPY
#define     ELM_MEMCMP                      MEMCMP
#define     ELM_MEMSET                      MEMSET
#define     ELM_SPRINTF                     SPRINTF
#define     ELM_TMR_SUCCESS                 TMR_SUCCESS
#define     ELM_TMR_FAILURE                 TMR_FAILURE
/*
#define     SET_VLAN_ID(Arr,VlanId) \
                Arr[VlanId/8] = Arr[(VlanId/8)] | (0x01 << (VlanId%8))

#define     IS_VLAN_ID_CONFIGURED(Arr,VlanId) \
                (((Arr[(VlanId/8)] & (0x01 << (VlanId%8))) == 0)? ELM_FAILURE:ELM_SUCCESS)
*/
#define     ELM_CREATE_LOCALMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(ELM_MEMORY_TYPE),(tElmMemPoolId*)(pPoolId))
#define     ELM_CREATE_QMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(ELM_MEMORY_TYPE),(tElmMemPoolId*)(pPoolId))
#define     ELM_CREATE_CFGQ_MSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(ELM_MEMORY_TYPE),(tElmMemPoolId*)(pPoolId))
#define     ELM_CREATE_ASYNCH_LIST_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(ELM_MEMORY_TYPE),(tElmMemPoolId*)(pPoolId))

#define     ELM_RELEASE_LOCALMSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(ELM_LOCALMSG_MEMPOOL_ID, (UINT1 *)pNode)
#define     ELM_RELEASE_QMSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(ELM_QMSG_MEMPOOL_ID, (UINT1 *)pNode)
#define     ELM_RELEASE_CFGQ_MSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(ELM_CFG_QMSG_MEMPOOL_ID, (UINT1 *)pNode)

#define     ELM_CREATE_MEM_POOL(BlockSize, NumOfBlocks, MemoryType, pPoolId) \
                MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                MemoryType, (tElmMemPoolId *)(pPoolId))
#define     ELM_DELETE_MEM_POOL(pPoolId)    MemDeleteMemPool(pPoolId)
#define     ELM_DELETE_MSG_QUEUE(QId)      OsixQueDel(QId)
#define     ELM_DELETE_SEMAPHORE(SemId)      OsixSemDel(SemId)

#define     ELM_CREATE_TMR_MEM_POOL(BlockSize,NumOfBlocks, \
            MemoryType, pPoolId) \
            MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
            MemoryType, (tElmMemPoolId*)(pPoolId))
#define     ELM_DELETE_TMR_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)

#define     ELM_DELETE_LOCALMSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)
#define     ELM_DELETE_QMSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)
#define     ELM_DELETE_CFGQ_MSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)
#define     ELM_DELETE_ASYNCH_LIST_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)

#define     ELM_ALLOC_PORT_TBL_BLOCK(pPortEntry) \
            (pPortEntry = (tElmPortEntry **) \
            MemAllocMemBlk(ELM_PORT_TBL_MEMPOOL_ID))

#define     ELM_RELEASE_PORT_TBL_BLOCK(pPortEntry) \
             MemReleaseMemBlock(ELM_PORT_TBL_MEMPOOL_ID, (UINT1 *)pPortEntry)
#define     ELM_ALLOC_PORT_INFO_MEM_BLOCK(pPortEntry) \
            (pPortEntry = (tElmPortEntry *) \
             MemAllocMemBlk(ELM_PORT_INFO_MEMPOOL_ID))

#define     ELM_RELEASE_PORT_INFO_MEM_BLOCK(pPortEntry) \
                MemReleaseMemBlock(ELM_PORT_INFO_MEMPOOL_ID, (UINT1*)pPortEntry)

#define     ELM_ALLOC_EVC_STATUS_INFO(pElmEvcStatusInfo) \
              (pElmEvcStatusInfo = (tElmEvcStatusInfo *) \
                MemAllocMemBlk(ELM_EVC_STATUS_MEMPOOL_ID))
  
#define     ELM_RELEASE_EVC_STATUS_INFO(pElmEvcStatusInfo) \
                MemReleaseMemBlock(ELM_EVC_STATUS_MEMPOOL_ID, (UINT1 *)pElmEvcStatusInfo)

#define     ELM_ALLOC_EVC_VLAN_INFO(pElmEvcVlanInfo) \
              (pElmEvcVlanInfo = (tElmEvcVlanInfo *) \
                MemAllocMemBlk(ELM_EVC_VLAN_INFO_MEMPOOL_ID))

#define     ELM_RELEASE_EVC_VLAN_INFO(pElmEvcVlanInfo) \
                MemReleaseMemBlock(ELM_EVC_VLAN_INFO_MEMPOOL_ID, (UINT1 *)pElmEvcVlanInfo)

#define     ELM_ALLOC_CFA_UNI_INFO(pCfaUniInfo) \
              (pCfaUniInfo = (tCfaUniInfo *) \
                MemAllocMemBlk(ELM_CFA_UNI_INFO_MEMPOOL_ID))

#define     ELM_RELEASE_CFA_UNI_INFO(pCfaUniInfo) \
                MemReleaseMemBlock(ELM_CFA_UNI_INFO_MEMPOOL_ID, (UINT1 *)pCfaUniInfo)

#define     ELM_ALLOC_LCM_EVC_INFO(pLcmEvcInfo) \
              (pLcmEvcInfo = (tLcmEvcInfo *) \
                MemAllocMemBlk(ELM_LCM_EVC_INFO_MEMPOOL_ID))

#define     ELM_RELEASE_LCM_EVC_INFO(pLcmEvcInfo) \
                MemReleaseMemBlock(ELM_LCM_EVC_INFO_MEMPOOL_ID, (UINT1 *)pLcmEvcInfo)

#define     ELM_ALLOC_LCM_EVC_VLAN_INFO(pLcmCeVlanInfoInDb) \
              (pLcmCeVlanInfoInDb = (tLcmEvcCeVlanInfo *) \
                MemAllocMemBlk(ELM_LCM_EVC_VLAN_INFO_MEMPOOL_ID))

#define     ELM_RELEASE_LCM_EVC_VLAN_INFO(pLcmCeVlanInfoInDb) \
                MemReleaseMemBlock(ELM_LCM_EVC_VLAN_INFO_MEMPOOL_ID, (UINT1 *)pLcmCeVlanInfoInDb)

#define     ELM_ALLOC_ELM_ETH_FRAME_BUF(pu1Buf) \
            (pu1Buf = (UINT1 *) \
             MemAllocMemBlk(ELM_ETH_FRAME_SIZE_MEMPOOL_ID))

#define     ELM_RELEASE_ELM_ETH_FRAME_BUF(pu1Buf) \
                MemReleaseMemBlock(ELM_ETH_FRAME_SIZE_MEMPOOL_ID, (UINT1 *)pu1Buf)

#define     ELM_ALLOC_LOCALMSG_MEM_BLOCK(pNode) \
            (pNode = (tElmMsgNode *) \
             MemAllocMemBlk(ELM_LOCALMSG_MEMPOOL_ID))

#define     ELM_ALLOC_CTRLMSG_MEM_BLOCK(pNode) \
            (pNode = (tElmRmCtrlMsg *) \
             MemAllocMemBlk(ELM_LOCALMSG_MEMPOOL_ID))

#define     ELM_ALLOC_QMSG_MEM_BLOCK(pNode) \
            (pNode = (tElmQMsg *) \
             MemAllocMemBlk(ELM_QMSG_MEMPOOL_ID))

#define     ELM_ALLOC_CFGQ_MSG_MEM_BLOCK(pNode) \
            (pNode = (tElmQMsg *) \
             MemAllocMemBlk(ELM_CFG_QMSG_MEMPOOL_ID))

#define     ELM_ALLOC_TMR_MEM_BLOCK(pNode) \
            (pNode = (tElmTimer *) \
             MemAllocMemBlk(ELM_TMR_MEMPOOL_ID))

#define     ELM_RELEASE_TMR_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(ELM_TMR_MEMPOOL_ID, (UINT1 *)pNode)

#define     ELM_ALLOC_ASYNCH_LIST_MEM_BLOCK(pNode) \
            (pNode = (tElmAsynchMessageNode *) \
             MemAllocMemBlk(ELM_ASYNCH_LIST_MEMPOOL_ID))

#define     ELM_RELEASE_ASYNCH_LIST_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(ELM_ASYNCH_LIST_MEMPOOL_ID, (UINT1 *)pNode)

#define     ELM_CALC_ALLOC_PRE_CRU_BUF(u2DataLength) \
                ((u2DataLength + gElmCruAllocatedMemory)> (256*SYS_MAX_BUF_BLOCK_SIZE))? \
                NULL:ELM_ALLOC_CRU_BUF(u2DataLength,0)

#define     ELM_IS_PORT_VALID(PortId) \
            (((PortId == 0) || (PortId > ELM_MAX_NUM_PORTS_SUPPORTED))? \
        ELM_FALSE:ELM_TRUE)

#define     ELM_TRC_FN_ENTRY()   

/******************************************************************************/
/* Other Memory Related Defines */
/******************************************************************************/

#define     ELM_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

/******************************************************************************/
/* CRU Buffer related definitions */
/******************************************************************************/

#define ELM_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define ELM_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define ELM_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define ELM_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define ELM_CRU_BUF_Get_ChainValidByteCount  CRU_BUF_Get_ChainValidByteCount
#define ELM_BUF_MOVE_VALID_OFFSET            CRU_BUF_Move_ValidOffset 
#define ELM_IS_CRU_BUF_LINEAR                CRU_BUF_Get_DataPtr_IfLinear
#define ELM_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain
#define ELM_LINK_CRU_BUFCHAINS               CRU_BUF_Link_BufChains

#define ELM_BUF_GET_CHAIN_VALIDBYTECOUNT(pBuf) \
             CRU_BUF_Get_ChainValidByteCount(pBuf)
#define  ELM_BUF_SET_INTERFACEID(pBuf,IfaceId) \
            CRU_BUF_Set_InterfaceId(pBuf,IfaceId) 
#define  ELM_BUF_GET_INTERFACEID(pBuf) \
            CRU_BUF_Get_InterfaceId(pBuf)

#define  ELM_CRU_FALSE  FALSE

#define  ELM_GET_DATA_PTR_IF_LINEAR(pBuf, u4Offset, u4NumBytes)    \
         CRU_BUF_Get_DataPtr_IfLinear((pBuf), (u4Offset), (u4NumBytes))
    
/******************************************************************************/
/* CFA related macros */
/******************************************************************************/

#define ELM_PROT_PDU                    CFA_PROT_BPDU
#define ELM_ENCAP_NONE                  CFA_ENCAP_NONE
#define ELM_ENET_PHYS_INTERFACE_TYPE    CRU_ENET_PHYS_INTERFACE_TYPE
#define ELM_ENET_HEADER_SIZE            CRU_ENET_HEADER_SIZE

/******************************************************************************/
/* Macros for getting data from linear buffer */
/******************************************************************************/
#define ELM_GET_1BYTE(u1Val, pu1PktBuf) \
   do {                             \
      u1Val = *pu1PktBuf;           \
         pu1PktBuf += 1;        \
   } while(0)

#define ELM_GET_2BYTE(u2Val, pu1PktBuf)            \
do {                                        \
   ELM_MEMCPY (&u2Val, pu1PktBuf, 2);\
      u2Val = (UINT2)ELM_NTOHS(u2Val);               \
      pu1PktBuf += 2;                   \
} while(0)

#define ELM_GET_4BYTE(u4Val, pu1PktBuf)              \
do {                                          \
   ELM_MEMCPY (&u4Val, pu1PktBuf, 4); \
      u4Val = ELM_NTOHL(u4Val);                 \
      pu1PktBuf += 4;                    \
} while(0)


#define ELM_GET_MAC_ADDR(Value, pu1PktBuf)  \
        do {                                       \
           ELM_MEMCPY (&Value, (pu1PktBuf), sizeof(tMacAddr));    \
           (pu1PktBuf) += 6;                       \
        }while(0)

/******************************************************************************/
/* Encode Module Related Macros used to assign fields to buffer */
/******************************************************************************/
#define ELM_PUT_1BYTE(pu1PktBuf, u1Val) \
        do {                             \
           *pu1PktBuf = u1Val;           \
           pu1PktBuf += 1;        \
        } while(0)

#define ELM_PUT_2BYTE(pu1PktBuf, u2Val)        \
        do {                                    \
            u2Val = (UINT2)ELM_HTONS(u2Val);                  \
            ELM_MEMCPY (pu1PktBuf,&u2Val, 2); \
            u2Val = (UINT2)ELM_NTOHS(u2Val);                  \
            pu1PktBuf += 2;              \
        } while(0)

#define ELM_PUT_4BYTE(pu1PktBuf, u4Val)       \
        do {                                   \
           u4Val = ELM_HTONL(u4Val);   \
           ELM_MEMCPY (pu1PktBuf,&u4Val, 4); \
           u4Val = ELM_NTOHL(u4Val); \
           pu1PktBuf += 4;             \
        }while(0)

#define ELM_PUT_MAC_ADDR(pu1PktBuf, Value)   \
        do {                                      \
           ELM_MEMCPY (pu1PktBuf, &Value, sizeof(tMacAddr));     \
           pu1PktBuf += 6;                        \
        }while(0)


/******************************************************************************/
/* Macro for UINT8 counters */
/******************************************************************************/

#define   ELM_UINT8_ADD(varA, value) \
   if ((0xffffffff - varA.u4LoWord) >= value){ \
      (varA.u4LoWord)+= value; \
   }                           \
   else {                      \
      varA.u4LoWord = value - (0xffffffff - varA.u4LoWord) - 1;\
      (varA.u4HiWord)++; \
   } 

#define ELM_UINT8_INC(varA) \
   if (varA.u4HiWord == 0xffffffff) \
      (varA.u4LoWord)++; \
   (varA.u4HiWord)++; 
   
#define ELM_UINT8_RESET(varA) \
   varA.u4HiWord = 0; \
   varA.u4LoWord = 0;

#define ELM_MAC_ADDR_TO_ARRAY(pu1Array, pMacAddr) \
        { \
          tMacAddress TmpMacAddr; \
          \
          TmpMacAddr.u4Dword = (pMacAddr)->u4Dword; \
          TmpMacAddr.u2Word = (pMacAddr)->u2Word;   \
          \
          TmpMacAddr.u4Dword = OSIX_HTONL(TmpMacAddr.u4Dword); \
          TmpMacAddr.u2Word = (UINT2)(OSIX_HTONS(TmpMacAddr.u2Word));   \
          \
          MEMCPY ((pu1Array),(&TmpMacAddr.u4Dword),(sizeof(TmpMacAddr.u4Dword))); \
          MEMCPY ((pu1Array+(sizeof(TmpMacAddr.u4Dword))), (&TmpMacAddr.u2Word), \
                   (sizeof(TmpMacAddr.u2Word))); \
        }
/* Macros for SLL Implementation*/
#define ELM_SLL_INIT(pList) TMO_SLL_Init(pList)

#define ELM_SLL_INIT_NODE(pNode)        TMO_SLL_Init_Node(pNode)
#define ELM_SLL_ADD(pList, pNode) \
        ((pList!=NULL)?TMO_SLL_Add(pList, pNode):NULL)
#define ELM_SLL_DELETE(pList, pNode)\
do {\
   if (pList != NULL)\
   {\
      TMO_SLL_Delete (pList, pNode);\
   }\
}while (0)

#define ELM_SLL_COUNT(pList)            TMO_SLL_Count(pList)
#define ELM_SLL_NEXT(pList, pNode)      TMO_SLL_Next(pList,pNode)

#define ELM_SLL_FIRST(pList) \
        (pList!=NULL?TMO_SLL_First(pList):NULL)

#define ELM_SLL_GET(pList)              TMO_SLL_Get(pList)
#define ELM_SLL_LAST(pList)             TMO_SLL_Last(pList)
#define ELM_SLL_IS_NODE_IN_LIST(pNode)  TMO_SLL_Is_Node_In_List(pNode)

#define ELM_SLL_LIST(u4IfIndex) \
        (ELM_GET_PORTENTRY(u4IfIndex)!=NULL?(&(gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->ElmAsynchList):NULL)

#define  ELM_INCR_NO_OF_TLV_PARSED(u1NoOfTlv) u1NoOfTlv++ 
#define  ELM_INCR_NO_OF_LOCAL_TLV_PARSED(u1LocalTlvIndex) u1LocalTlvIndex++ 
#define  ELM_INCR_INDEX(u1Index)   u1Index++
#define  ELM_INCR_NO_OF_CE_VLAN_COUNT(u1Count) u1Count++ 
#define  ELM_INCR_NO_OF_BANDWIDTH_COUNT(u1Count) u1Count++ 
#define  ELM_DECR_DATA_LENGTH(u2DataLength,u1NoDec) \
                        ((u2DataLength >= u1NoDec)? \
                         ((u2DataLength =( u2DataLength - u1NoDec) )  ? \
                        ELM_SUCCESS:ELM_SUCCESS):ELM_FAILURE)


#endif
