/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmconst.h,v 1.13 2012/04/04 14:14:46 siva Exp $
 *
 * Description: This file contains all the constants used by
 *              ELMI module.
 *
 *******************************************************************/

#ifndef _ELMCONST_H_
#define _ELMCONST_H_

/******************************************************************************/
/*                              General Constants                             */
/******************************************************************************/

/* Number Of ELMI Events */
#define     ELM_PDU_EVENT                      0x0001
#define     ELM_MSG_EVENT                       0x0002
#define     ELM_TMR_EXPIRY_EVENT                0x0004
#define     ELM_RED_BULK_UPD_EVENT              0x0008


#define     ELM_INIT_VAL                        0
#define     ELM_FALSE                           0
#define     ELM_TRUE                            1
#define     ELM_ENABLED                         1
#define     ELM_DISABLED                        0
#define     ELM_PORT_ENABLED                    1
#define     ELM_PORT_DISABLED                   2
#define     ELM_OPER_ENABLED                    1
#define     ELM_OPER_DISABLED                   2
#define     ELM_STARTED                         1
#define     ELM_STOPPED                         0
#define     ELM_PORT_ELMI_ENABLED               1 
#define     ELM_PORT_ELMI_DISABLED              2 
#define     ELM_EXT_PORT_UP                     3
#define     ELM_PORT_UP                         4
#define     ELM_EXT_PORT_DOWN                   5
#define     ELM_PORT_DOWN                       6
#define     ELM_ENET_DEST_SRC_ADDR_SIZE         12
#define     ELM_ENABLE_GLOBAL                   1
#define     ELM_DISABLE_GLOBAL                  2
#define     ELM_ENABLE_PORT                     1
#define     ELM_DISABLE_PORT                    2

#define     ELM_DEFAULT_POLLING_COUNTER_VALUE        360

#define     ELM_SEM_INIT_COUNT                  1
#define     ELM_SYS_NUM_OF_TIME_UNITS_IN_A_SEC  SYS_NUM_OF_TIME_UNITS_IN_A_SEC

/*ELMI  - PDU Type*/
#define     ELM_PDU_TYPE_STATUS_ENQUIRY        0x75
#define     ELM_PDU_TYPE_STATUS                0x7d

/* QMSG types */
#define     ELM_SNMP_CONFIG_QMSG                1
#define     ELM_PDU_RCVD_QMSG                   2
#define     ELM_RM_QMSG                         3

/* ELMI  Constants -- Message Type*/
#define     ELM_CONFIGURE_PVT_VALUE_MSG         1
#define     ELM_CONFIGURE_PT_VALUE_MSG          2
#define     ELM_CONFIGURE_POLLING_COUNTER_MSG   3
#define     ELM_CONFIGURE_STATUS_COUNTER_MSG    4
#define     ELM_ENABLE_PORT_MSG                 5   /*Hardware Port Status*/
#define     ELM_DISABLE_PORT_MSG                6   /*Hardware Port Status*/
#define     ELM_CREATE_PORT_MSG                 9   /*Interface Port Create*/
#define     ELM_DELETE_PORT_MSG                 10  /*Interface Port Delete*/
#define     ELM_EVC_STATUS_CHANGE_MSG           11
#define     ELM_UNI_INFORMATION_UPDATE_MSG      12
#define     ELM_LCM_INFORMATION_UPDATE_MSG      13
#define     ELM_ENABLE_MSG                      14  /* ELMI Enable on All Ports 
                                                     */
#define     ELM_DISABLE_MSG                     17
                                                  /* ELMI Disable on All Ports  
*/
#define     ELM_PORT_ELMI_ENABLE_MSG            19
#define     ELM_PORT_ELMI_DISABLE_MSG           20
#define     ELM_UNI_NETWORK_ENABLE_MSG          21
#define     ELM_UNI_CUSTOMER_ENABLE_MSG         22
#define     ELM_START_ELM_MSG                   27
#define     ELM_STOP_ELM_MSG                    28
#define     ELM_MAP_PORT_MSG                    29
#define     ELM_UNMAP_PORT_MSG                  30

#define     ELM_MIN_LEN_PDU                     41
/*ELMI -  Protocol Constants*/
#define     ELM_PT_MIN_VALUE                    5
#define     ELM_PT_MAX_VALUE                    30
#define     ELM_PVT_MIN_VALUE                   5
#define     ELM_PVT_MAX_VALUE                   30
#define     ELM_STATUS_COUNTER_MIN_VALUE        2
#define     ELM_STATUS_COUNTER_MAX_VALUE        10
#define     ELM_POLLING_COUNTER_MAX_VALUE       65000
#define     ELM_POLLING_COUNTER_MIN_VALUE       1
#define     ELM_MAX_DATA_INSTANCE               65000
#define     ELM_ASYNCH_TIMER_VAL                (ELM_PT_MAX_VALUE/10)

#define     ELM_MAX_EVC_SUPPORTED               128
#define     ELM_NUM_BW_ELEMENTS                 8
#define     ELM_MAX_LENGTH_UNI_NAME             64
#define     ELM_MAX_LENGTH_EVC_NAME             100
#define     ELM_MIN_NUM_PORTS                   1
#define     ELM_PORT_TBL_MEMBLK_COUNT           1
#define     ELM_IFINDEX_STR_LEN                 8
#define     ELM_MIN_TRACE_VAL                   0
#define     ELM_MAX_TRACE_VAL                   459008 
#define     ELM_MIN_DEBUG_VAL                   0
#define     ELM_MAX_DEBUG_VAL                   262143
#define     ELM_PORT_OPER_UP                    CFA_IF_UP
#define     ELM_PORT_OPER_DOWN                  CFA_IF_DOWN
#define     ELM_PROTOCOL_VERSION                1
#define     ELM_32BIT_MAX                       4294967295U  /* 0xffffffff */
#define     ELM_REPORT_TYPE_LENGTH              1
#define     ELM_SEQUENCE_NUMBER_LENGTH          2
#define     ELM_DATA_INSTANCE_LENGTH            4
#define     ELM_ASYNCH_EVC_STATUS_LENGTH        3
#define     ELM_EVC_PARAMS_SUBINFO_LENGTH       1
#define     ELM_MAX_EVC_LENGTH                  100
#define     ELM_BW_PROFILE_IE_LENGTH            12
#define     ELM_BW_PROFILE_TOTAL_LENGTH         14
#define     ELM_UNTAGGED_PRIORITY_BIT           0x02
#define     ELM_EVC_DEFAULT_BIT                 0x01
#define     ELM_MAX_VLAN_ID                     4096
#define     ELM_RXED_ENQUIRY_PDU                1
#define     ELM_UPDATE_DATABASE_TRIGGER         2
#define     ELM_RED_SYNCHUP_DATABASE            3

#define     ELM_PROTO_MSG_TYPE_SIZE             2
#define     ELM_MAX_ETH_FRAME_SIZE              1514 /*Excluding CRC */
#define     ELM_MAX_PDU_SIZE                    1500
#define     ELM_HEADER_SIZE                     14
#define     ELM_MIN_ETH_FRAME_SIZE              64
#define     ELM_ETH_FRAME_CRC_SIZE              4
#define     ELM_MIN_ETH_FRAME_WITHOUT_HEADERS   (ELM_MIN_ETH_FRAME_SIZE - ELM_HEADER_SIZE - ELM_ETH_FRAME_CRC_SIZE)
#define     ELM_MIN_PDU_SIZE                    60
#define     ELM_FIXED_TLV_SIZE                  16
#define     ELM_MAC_ADDR_SIZE                   6
#define     ELM_UNI_FIXED_IE_LENGTH             19
#define     ELM_MAX_UNI_INFO_SIZE               83
#define     ELM_CE_VLAN_INFO_FIXED_LENGTH       6
#define     ELM_CE_SINGLE_VLAN_INFO_LENGTH      10  /* Length to send 1 Vlan */
#define     ELM_EVC_FIXED_IE_LENGTH             8
#define     ELM_MAX_TLV_IN_ENQUIRY_MSG          3
#define     ELM_MAX_EVC_IN_SINGLE_REQUEST       10
#define     ELM_CE_VLAN_IE_FIXED_SIZE           8
#define     ELM_SINGLE_VLAN_SIZE                2

#define     ELM_RELIABILITY_ERROR               0
#define     ELM_PROTOCOL_ERROR                  1

#define     ELM_LAST_CE_VLAN_ID_IE              0x70
#define     ELM_MAX_CEVLAN                      124
#define     ELM_MEMSET_VAL                      0
#define     ELM_ETH_DEST_SRC_ADDR_SIZE          12
#define     ELM_TYPE_LEN_SIZE                   2
#define     ELM_RELIABILITY_ERROR_TYPE          0 
#define     ELM_PROTOCOL_ERROR_TYPE             1
#define     ELM_MAX_CE_VLAN_INFO                255
#define     ELM_MAX_VLANS_PER_CE_VLAN_INFO      124
#define     ELM_FULL_STATUS                     0
#define     ELM_ELMI_CHECK                      1
#define     ELM_ASYNCHRONOUS_STATUS             2
#define     ELM_FULL_STATUS_CONTINUED           3
#define     DATA_INSTANCE_INTIAL_VALUE          1

/*Information Element Identifiers*/
#define     ELM_REPORT_TYPE_IE                     0x01
#define     ELM_SEQUENCE_NUMBER_IE                 0x02
#define     ELM_DATA_INSTANCE_IE                   0x03
#define     ELM_UNI_STATUS_IE                      0x11
#define     ELM_EVC_STATUS_IE                      0x21
#define     ELM_CE_VLAN_EVC_MAP_IE                 0x22

/*Sub Information Element Identifiers*/
#define     ELM_UNI_IE                             0x51
#define     ELM_EVC_PARAMS_IE                      0x61
#define     ELM_EVC_IDENTIFIER_IE                  0x62
#define     ELM_EVC_MAP_ENTRY_IE                   0x63
#define     ELM_BW_PROFILE_IE                      0x71

/*Define Masks Here If Required*/
#define     ELM_MASK_FIRST_BIT                      0x01 /*00000001*/
#define     ELM_MASK_SECOND_BIT                     0x02 /*00000010*/
#define     ELM_MASK_THIRD_BIT                      0x04 /*00000100*/
#define     ELM_MASK_SIXTH_BIT                      0x32 /*00100000*/
#define     ELM_MASK_SEVENTH_BIT                    0x40 /*01000000*/
#define     ELM_MASK_EIGTH_BIT                      0x80 /*10000000*/
#define     ELM_MASK_SIX_BITS                       0x3f /*00111111*/

/******************************************************************************/
/*                             Timer Module Constants                         */
/******************************************************************************/
#define     ELM_TMR_TYPE_PT                     1
#define     ELM_TMR_TYPE_PVT                    2
#define     ELM_TMR_TYPE_ASYNC                  3

#define     ELM_DEFAULT_ASYNC_TIMER_VALUE       1
#define     ELM_DEFAULT_PVT_TIMER_VALUE         15
#define     ELM_DEFAULT_PT_TIMER_VALUE          10
#define     ELM_DEFAULT_STATUS_COUNTER_VALUE    4
#define     ELM_START                           1 
#define     ELM_SHUTDOWN                        2
#define     ELM_DEST_SRC_ADDR_SIZE              12
#define     ELM_ETHER_TYPE_SIZE                 2
#define     ELM_ETHER_TYPE                      0x88ee
#define     ELM_INVALID_ETHER_TYPE              1
#define     ELM_INVALID_PROTOCOL_VERSION        2
#define     ELM_INVALID_MSG_TYPE                3
#define     ELM_INVALID_REPORT_TYPE             4
#define     ELM_INVALID_SEQUENCE_NUM            5
#define     ELM_INVALID_DATA_INSTANCE           6
#define     ELM_INVALID_PDU_RXD_TRAP_VAL        1
#define     ELM_BRG_ERR_TRAP_VAL                2
#define     ELM_PVT_EXP_TRAP_VAL                3
#define     ELM_PT_EXP_TRAP_VAL                 4
#define     ELM_EVC_TRAP_VAL                    5
#define     ELM_UNI_TRAP_VAL                    6
#define     ELM_PVT_EXPIRED                     0
#define     ELM_PT_EXPIRED                      0
#define     PT_EXPIRED_NORMALLY                 1
#define     PT_EXPIRED_ABNORMALLY               0
#define     ELM_STR_ADDR_SIZE                   8
#define     ELM_NEW_EVC_CREATED                 0
#define     ELM_OLD_EVC_DELETED                 1
#define     ELM_EVC_STATUS_CHANGED              2
#define     ELM_UNI_STATUS_CHANGED              0
#define     ELM_LENGTH_OF_DATA_INSTANCE         5

#define     ELM_MUT_EXCL_SEM_NAME               (const UINT1 *) "ELMI" 

#define     ELM_QMSG_MEMBLK_SIZE                (sizeof(tElmQMsg))
#define     ELM_LOCAL_MSG_MEMBLK_SIZE           (sizeof(tElmMsgNode))
#define     ELM_CFGQ_MSG_MEMBLK_SIZE            (sizeof(tElmQMsg))

#define     ELM_AVG_LOCAL_MSGS_PER_PORT         10
#define     ELM_QMSG_MEMBLK_COUNT               (ELM_QUEUE_DEPTH)
#define     ELM_CFGQ_MSG_MEMBLK_COUNT           (ELM_CFG_Q_DEPTH)
#define     ELM_LOCAL_MSG_MEMBLK_COUNT          (ELM_MAX_NUM_PORTS_SUPPORTED_IN_SYSTEM \
                                                 * ELM_AVG_LOCAL_MSGS_PER_PORT )

#define     ELM_TMR_MEMPOOL_ID                  gElmGlobalInfo.TmrMemPoolId
#define     ELM_PORT_TBL_MEMPOOL_ID             gElmGlobalInfo.PortTableMemPoolId
#define     ELM_PORT_INFO_MEMPOOL_ID            gElmGlobalInfo.PortInfoMemPoolId
#define     ELM_QMSG_MEMPOOL_ID                 gElmGlobalInfo.QMsgMemPoolId
#define     ELM_LOCALMSG_MEMPOOL_ID             gElmGlobalInfo.LocalMsgMemPoolId
#define     ELM_CFG_QMSG_MEMPOOL_ID             gElmGlobalInfo.CfgQMsgMemPoolId
#define     ELM_ASYNCH_LIST_MEMPOOL_ID          gElmGlobalInfo.ElmAsynchListMemPoolId
#define     ELM_EVC_STATUS_MEMPOOL_ID           gElmGlobalInfo.EvcStatusPoolId
#define     ELM_EVC_VLAN_INFO_MEMPOOL_ID        gElmGlobalInfo.EvcVlanInfoPoolId
#define     ELM_CFA_UNI_INFO_MEMPOOL_ID         gElmGlobalInfo.CfaUniInfoPoolId
#define     ELM_LCM_EVC_INFO_MEMPOOL_ID         gElmGlobalInfo.LcmEvcInfoPoolId
#define     ELM_LCM_EVC_VLAN_INFO_MEMPOOL_ID    gElmGlobalInfo.LcmEvcVlanInfoPoolId
#define     ELM_ETH_FRAME_SIZE_MEMPOOL_ID       gElmGlobalInfo.FrameSizePoolId
#define     ELM_INPUT_QID                       gElmGlobalInfo.InputQId
#define     ELM_CFG_QID                         gElmGlobalInfo.CfgQId
#define     ELM_TASK_ID                         gElmGlobalInfo.TaskId

#define     ELM_TASK_INPUT_QNAME                ELM_QUEUE_NAME
#define     ELM_QUEUE_MODE                      0

#define     ELM_CRU_SUCCESS                     CRU_SUCCESS
#define     ELM_CRU_FAILURE                     CRU_FAILURE

#define     ELM_TMRNODE_MEMBLK_SIZE             (sizeof(tElmTimer))
#define     ELM_PORT_INFO_MEMBLK_SIZE           (sizeof(tElmPortEntry))
#define     ELM_ASYNCH_LIST_MEMBLK_SIZE         (sizeof(tElmAsynchMessageNode))
#define     ELM_INVALID_PORT_NUM                0  
#define     ELM_MSG_INVALID_PORT                0 /* The value that is filled in 
                                                  the IfIndex field of queue 
                                                  messages if it is not a port 
                                                  based message */

#define     tElmAppTimer                        tTmrAppTimer
#define     tElmTimerCfg                        tTimerCfg
#define     tElmTmrListId                       tTimerListId
#define     tElmTaskId                          tOsixTaskId
#define     tElmMemPoolId                       tMemPoolId
#define     tElmOsixSemId                       tOsixSemId
#define     tElmQId                             tOsixQId

#define     NUM_OF_MANDATORY_TLV_ELMI_CHECK_MSG                    2 
#define     NUM_OF_MANDATORY_TLV_ASYNC_STATUS_MSG                  1 
#define     NUM_OF_MANDATORY_TLV_FULL_STATUS_CONTINUED_MSG         4 
#define     NUM_OF_MANDATORY_TLV_FULL_STATUS_MSG                   5 
#define     NUM_OF_MANDATORY_TLV_STATUS_MSG                        5 
#define     MANDATORY_SUB_INFO_TLV_EVC_STATUS_IE                   3
#define     MANDATORY_SUB_INFO_TLV_UNI_STATUS_IE                   2

#define     ELM_OPER_STATUS_CHANGED_TRAP_VAL                       7            
        
#define     ELM_OPER_STATUS_ENABLED                                0
#define     ELM_OPER_STATUS_DISABLED                               1 

#define     ELM_PER_COS_BIT_FINDER                  0x01
#define     ELM_PER_CF_BIT_FINDER                   0x02
#define     ELM_PER_CM_BIT_FINDER                   0x04
#define     ELM_LAST_BIT_FINDER                     0x40
#define     ELM_DEFAULT_EVC_FINDER                  0x01
#define     ELM_TAGGED_BIT_FINDER                   0x02

#define ELM_IST_BYTE_ETHERNET_HEADER                0x88
#define ELM_2ND_BYTE_ETHERNET_HEADER                0xEE
#define ELM_MAX_UNI_NAME                            64
#define ELM_MAX_EVC_NAME                            100
#define ELM_DECR_DATALEN_ONE                        1
#define ELM_DECR_DATALEN_TWO                        2
#define ELM_DECR_DATALEN_THREE                      3
#define ELM_DECR_DATALEN_FOUR                       4
#define ELM_DECR_DATALEN_FIVE                       5
#define ELM_DECR_DATALEN_SIX                        6
#define ELM_INCR_BUFF_ONE                           1
#define ELM_INCR_BUFF_TWO                           2
#define ELM_INCR_BUFF_THREE                         3
#define ELM_INCR_BUFF_SIX                           6
#define ELM_INCR_BUFF_SEVEN                         7
#define ELM_INCR_DATALEN_ONE                        1
#define ELM_MAX_BW_PROFILE_IE                       8



#define ELM_ONE_TLV_PARSED                          1
#define ELM_TWO_TLV_PARSED                          2
#define ELM_THREE_TLV_PARSED                        3


#define ELM_INCR_NO_IE_ONE                          1
#define ELM_DECR_VLAN_COUNTER_ONE                   1
#define ELM_INCR_VLAN_COUNTER_ONE                   1
#define ELM_DATA_INSTANCE_IE_LENGTH                 5
#define ELM_MIN_LENGTH_ENQUIRY_PDU                  44
#define ELM_CIR_MULTIPLIER_LENGTH                   2
#define ELM_EIR_MULTIPLIER_LENGTH                   2
#define ELM_MIN_PRIORITY                            0
#define ELM_MAX_PRIORITY                            7


#endif
