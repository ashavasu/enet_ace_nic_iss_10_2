/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmred.h,v 1.12 2011/09/24 06:56:44 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for ELMI module.   
 *
 ******************************************************************/
#ifndef ELM_RED_H
#define ELM_RED_H

typedef UINT4  tElmRedStates;
typedef tOsixSysTime tTimeStamp;


#define     RED_ELM_IDLE                             RM_INIT
#define     RED_ELM_ACTIVE                           RM_ACTIVE
#define     RED_ELM_STANDBY                          RM_STANDBY
#define     RED_ELM_FORCE_SWITCHOVER_INPROGRESS      4

#define     ELM_RED_LCM_DATABASE_SYNCHUP_MSG    1
#define     ELM_BULK_REQ_MSG                    2
#define     ELM_BULK_UPD_TAIL_MSG               3
#define     ELM_DATA_INSTANCE_SYNC_MSG          4
#define     ELM_PORT_OPER_STATUS_MSG            5
#define     ELM_RED_SYNC_UP_MSG                 6
#define     ELM_RED_CHANGED_STATUS_SYNCHUP      7

/* Message Types for Changed Status Messages */
#define     ELM_RED_ASYNCH_STATUS_MSG           1
#define     ELM_RED_FULL_STATUS_MSG             2
#define     ELM_RED_FULL_STATUS_CONTD_MSG       3
#define     ELM_RED_CE_VLAN_INFO_MSG            4

#define     ELM_RED_NO_OF_PORTS_PER_SUB_UPDATE        10
#define     ELM_RED_SYNC_UP_MSG_SIZE                  24
#define     ELM_DATA_INSTANCE_SYNC_MSG_SIZE           11
#define     ELM_BULK_UPD_TAIL_MSG_SIZE                3
#define     ELM_BULK_REQ_MSG_SIZE                     3
#define     ELM_PORT_OPER_STATUS_MSG_SIZE             8
#define     ELM_RED_STATUS_MSG_HEADER_SIZE            14 
#define     ELM_RED_HEADER_SIZE                       ELM_INTERFACE_INDEX_SIZE + 3 
                                                            /*Type(1) + Length(2) */
#define     ELM_DATA_INSTANCE_SYNC_MSG_LEN            (ELM_DATA_INSTANCE_SYNC_MSG_SIZE - 3)
#define     ELM_RED_SYNC_UP_MSG_LEN                   (ELM_RED_SYNC_UP_MSG_SIZE - 3)
#define     ELM_PORT_OPER_STATUS_MSG_LEN              (ELM_PORT_OPER_STATUS_MSG_SIZE - 3)
#define     ELM_RED_CE_VLAN_INFO_FIXED_SIZE            6 /*Type(1) + Length(2)
                                                          *EvcId(2) + Default Bit/Tagged Bit (1)
                                                          */
#define     ELM_RED_MSG_TYPE_SIZE                      1
#define     ELM_RED_MSG_TYPE_LENGTH_SIZE               3
#define     ELM_RED_LCM_SYNCH_HEADER_SIZE              7
                                                    /* Type(1) + Length(2) + ELMI HEADER */
#define     ELM_INTERFACE_INDEX_SIZE                  4 /* Interface index is 4 bytes long */
#define     ELM_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */

#define     ELM_RED_TRC                   0x00000100


typedef enum {
   RED_ELM_PORT_INFO =1,
   RED_ELM_PDU,
   RED_ELM_OPER_STATUS,
   RED_ELM_BULK_REQ,
   RED_ELM_BULK_UPD_TAIL_MSG
}tElmRmMsgTypes;

typedef struct ElmRedGlobalInfo 
{
    tElmTimer            ElmRedSynchUpTimer;
    UINT4                u4BulkUpdNextPort;
    UINT1                u1ElmNodeStatus;
    UINT1                u1NumPeerPresent;
    BOOL1                bBulkReqRcvd;
    UINT1                u1Reserved; 
}tElmRedGlobalInfo;

#define     ELM_RM_GET_NODE_STATE()  \
             gElmRedGlobalInfo.u1ElmNodeStatus
#define     ELM_BULK_REQ_RECD()            gElmRedGlobalInfo.bBulkReqRcvd
#define     ELM_NUM_STANDBY_NODES() \
             (gElmRedGlobalInfo.u1NumPeerPresent)
#define     ELM_IS_STANDBY_UP() \
          ((gElmRedGlobalInfo.u1NumPeerPresent > 0) ? ELM_TRUE : ELM_FALSE)
#define     ELM_INIT_NUM_PEERS() \
           (gElmRedGlobalInfo.u1NumPeerPresent = 0)
#define     ELM_RM_GET_STATIC_CONFIG_STATUS()  RmGetStaticConfigStatus ()

#define     ELM_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define     ELM_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define     ELM_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define    ELM_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define    ELM_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define     ELM_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define     ELM_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ELM_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define     ELM_GET_TIME_STAMP(pTime) \
             (OsixGetSysTime(pTime))

#define     ELM_ADD_TIME_STAMP(time,TimeStamp) \
              (time = time + TimeStamp)

#define     ELM_RED_INCR_SEND_SEQUENCE_COUNTER_VALUE(u4IfIndex) \
do {\
   if (ELM_GET_PORTENTRY(u4IfIndex) != NULL)\
   {\
     if ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmRedSendSeqCounter ==255)\
     {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmRedSendSeqCounter = 1;\
     }\
     else \
     {\
       (gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->u1ElmRedSendSeqCounter++;\
     }\
   }\
}while (0)

INT4 ElmRedRegisterWithRM (VOID);
UINT4 ElmRmDeRegisterProtocols (VOID);
INT4 ElmRedDeRegisterWithRM (VOID);
VOID ElmRedHandleBulkRequest (VOID);
VOID ElmRedHandleBulkUpdateEvent (VOID);
VOID ElmProcessRmEvent (tElmRmCtrlMsg * pMsg);
VOID ElmRedHandleGoActive (VOID);
VOID ElmRedMakeNodeActiveFromIdle (VOID);
VOID ElmRedMakeNodeActiveFromStandby (VOID);
VOID ElmRedHandleRestoreComplete (VOID);
VOID ElmRedTrigHigherLayer (UINT1 u1TrigType);
INT4 ElmRedMakeNodeStandbyFromIdle (VOID);
INT4 ElmRedSendBulkUpdateTailMsg (VOID);
INT4 ElmRedRmInit (VOID);
INT4 ElmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize);
INT4 ElmRedSyncUpPerPortInfo (UINT4 u4PortIndex);
VOID ElmRedHandleGoStandby (VOID);
VOID ElmRedHandleSyncUpMessage (tElmRmCtrlMsg *pMsg);
INT4 ElmRedSendBulkRequest (VOID);
VOID ElmRedTriggerHigherLayer (UINT1 u1Trigger);
INT4 ElmRedSyncUpDataInstance(tElmPortEntry *pElmPortEntry);
VOID ElmRedHandleDiSynchUpMsg(tRmMsg *pData,UINT4 *pu4Offset);
VOID ElmRedHandlePortSynchUpMsg(tRmMsg *pData, UINT4 *pu4Offset);
VOID ElmRedHandlePeriodicUpdate (VOID);
INT4 ElmRedOperStatusChangeIndication (UINT4 u4IfIndex,UINT1 u1Status);
VOID ElmRedSynchupStatusMsg (UINT4 u4DataInstance, UINT4 u4IfIndex,UINT2 u2DataLength,
                              UINT1 u1MsgType,UINT1 *pu1RedBuf);
VOID ElmRedFillHeader (tElmPortEntry *pElmPortEntry,tRmMsg **ppBuf, UINT2 u2DataLength);
VOID ElmRedFillFixedTlv(tElmPortEntry *pElmPortEntry,tRmMsg **ppBuf, UINT1 u1ReportType);
VOID ElmRedFillUniStatusIe (tCfaUniInfo CfaUniInfo, tRmMsg **ppBuf, UINT1 u1UniStatusIeLength);
VOID ElmRedPassCeVlanInfo(UINT4 u4IfIndex,tLcmEvcCeVlanInfo *pLcmCeVlanInfo);
VOID ElmRedHandleChangedStatus (tRmMsg *pData,UINT4 *pu4Offset);
VOID ElmRedHandleCeVlanInfo (UINT1 *pu1Pdu, UINT4 u4IfIndex,UINT2 u2DataLength);
UINT4 ElmRmRegisterProtocols (tRmRegParams * pRmRegParams);
UINT4 ElmRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, 
                               UINT4 u4SrcEntId,UINT4 u4DestEntId);
INT4 ElmRmSetBulkUpdatesStatus (UINT4 u4AppId);
UINT1 ElmRmGetStandbyNodeCount (VOID);
UINT4 ElmRmGetNodeState (VOID);
INT4 ElmSnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID ElmRedClearTxRxCounters (UINT4 u4PortIndex);
#ifdef LLDP_WANTED
INT4 ElmLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif

UINT4 ElmRmSendEventToRmTask (UINT4 u4Event);
UINT1 ElmRmHandleProtocolEvent (tRmProtoEvt *pEvt);
UINT4 ElmRmReleaseMemoryForMsg (UINT1 *);

extern tElmRedGlobalInfo gElmRedGlobalInfo;

#endif

