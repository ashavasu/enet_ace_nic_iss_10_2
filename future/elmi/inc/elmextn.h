/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmextn.h,v 1.2 2007/11/19 07:21:37 iss Exp $
 * $Id: elmextn.h,v 1.2 2007/11/19 07:21:37 iss Exp $
 *
 * Description: This file contains all the external declarations for
 *              the ELMI Module. 
 *
 *******************************************************************/


#ifndef _ELMEXTN_H_
#define _ELMEXTN_H_

tSNMP_VAR_BIND     * SNMP_AGT_FormVarBind (tSNMP_OID_TYPE * oid,
                      INT2 i2_type,
                      UINT4 u4_ul_value,
                      INT4 i4_sl_value,
                      tSNMP_OCTET_STRING_TYPE * os_value,
                      tSNMP_OID_TYPE * oid_value,
                      tSNMP_COUNTER64_TYPE u8_Counter64Value);

VOID SNMP_AGT_RIF_Notify_V1_Or_V2_Trap (UINT4 u4_Version,
                                   tSNMP_OID_TYPE * pEnterpriseOid,
                                   UINT4 u4_gen_trap_type,
                                   UINT4 u4_spec_trap_type,
                                   tSNMP_VAR_BIND * vb_list);
tSNMP_OID_TYPE     *SNMP_AGT_GetOidFromString (INT1 *pi1_str);
tSNMP_OCTET_STRING_TYPE *
SNMP_AGT_FormOctetString (UINT1 *u1_string, INT4 i4_length);

#endif /* _ELMEXTN_H_ */
