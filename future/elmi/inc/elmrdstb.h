/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmrdstb.h,v 1.2 2008/06/18 13:22:13 prabuc-iss Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the ELMI Module.
 *
 *******************************************************************/


#ifndef _ELMRDSTB_H_
#define _ELMRDSTB_H_


VOID ElmRedFillFixedTlv (tElmPortEntry * pElmPortEntry, tRmMsg ** ppBuf,
                         UINT1 u1ReportType);
INT4 ElmRedOperStatusChangeIndication (UINT4 u4IfIndex,UINT1 u1Status);
INT4 ElmRedRmInit (VOID);
INT4 ElmRedRegisterWithRM (VOID);
VOID ElmProcessRmEvent (tElmRmCtrlMsg * pMsg);
VOID ElmRedHandleBulkUpdateEvent (VOID);
INT4 ElmRedSyncUpDataInstance(tElmPortEntry *pElmPortEntry);
INT4 ElmRedSyncUpPerPortInfo (UINT4 u4PortIndex);
VOID ElmRedFillHeader (tElmPortEntry *pElmPortEntry,tRmMsg **ppBuf, UINT2
u2DataLength);
VOID ElmRedFillUniStatusIe (tCfaUniInfo CfaUniInfo, tRmMsg **ppBuf, UINT1
u1UniStatusIeLength);
INT4 ElmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize);
VOID ElmRedSynchupStatusMsg (UINT4 u4DataInstance, UINT4 u4IfIndex,UINT2 u2DataLength,
                              UINT1 u1MsgType,UINT1 *pu1RedBuf);
VOID ElmRedPassCeVlanInfo(UINT4 u4IfIndex,tLcmEvcCeVlanInfo *pLcmCeVlanInfo);

#define     ELM_RM_GET_NODE_STATE()   RED_ELM_ACTIVE
#define     ELM_IS_STANDBY_UP()       ELM_FALSE
#define     RED_ELM_ACTIVE            RM_ACTIVE
#define     RED_ELM_STANDBY           RM_STANDBY
#define     ELM_RED_HEADER_SIZE                       ELM_INTERFACE_INDEX_SIZE + 3
#define     ELM_RED_LCM_SYNCH_HEADER_SIZE              7
#define     ELM_INTERFACE_INDEX_SIZE                  4 /* Interface index is 4 bytes long */
#define     ELM_RED_TRC                   0x00000100

#define     ELM_RED_ASYNCH_STATUS_MSG           1
#define     ELM_RED_FULL_STATUS_MSG             2
#define     ELM_RED_FULL_STATUS_CONTD_MSG       3
#define     ELM_RED_CE_VLAN_INFO_MSG            4


#define    ELM_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)


#endif 
