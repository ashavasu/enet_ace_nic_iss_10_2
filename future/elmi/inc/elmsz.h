/*$Id: elmsz.h,v 1.4 2012/04/04 14:14:46 siva Exp $*/
enum {
    MAX_CFA_UNI_INFO_SIZING_ID,
    MAX_ELM_ETH_FRAME_BUF_SIZING_ID,
    MAX_ELM_EVC_STATUS_INFO_SIZING_ID,
    MAX_ELM_EVC_VLAN_INFO_SIZING_ID,
    MAX_ELMI_ASYNC_MESG_NODES_SIZING_ID,
    MAX_ELMI_CONFIG_Q_MESG_SIZING_ID,
    MAX_ELMI_LOCAL_MESG_SIZING_ID,
    MAX_ELMI_PORT_INFO_ARRAY_SIZING_ID,
    MAX_ELMI_PORT_INFO_SIZING_ID,
    MAX_ELMI_Q_MESG_SIZING_ID,
    MAX_ELMI_TMR_NODES_SIZING_ID,
    MAX_LCM_EVC_CE_VLAN_INFO_SIZING_ID,
    MAX_LCM_EVC_INFO_SIZING_ID,
    ELMI_MAX_SIZING_ID
};

#ifdef  _ELMISZ_C
tMemPoolId ELMIMemPoolIds[ ELMI_MAX_SIZING_ID];
INT4  ElmiSizingMemCreateMemPools(VOID);
VOID  ElmiSizingMemDeleteMemPools(VOID);
INT4  ElmiSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _ELMISZ_C  */
extern tMemPoolId ELMIMemPoolIds[ ];
extern INT4  ElmiSizingMemCreateMemPools(VOID);
extern VOID  ElmiSizingMemDeleteMemPools(VOID);
extern INT4  ElmiSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _ELMISZ_C  */


#ifdef  _ELMISZ_C
tFsModSizingParams FsELMISizingParams [] = {
{ "tCfaUniInfo", "MAX_CFA_UNI_INFO", sizeof(tCfaUniInfo),MAX_CFA_UNI_INFO, MAX_CFA_UNI_INFO,0 },
{ "UINT1[ELM_MAX_ETH_FRAME_SIZE]", "MAX_ELM_ETH_FRAME_BUF", sizeof(UINT1[ELM_MAX_ETH_FRAME_SIZE]),MAX_ELM_ETH_FRAME_BUF, MAX_ELM_ETH_FRAME_BUF,0 },
{ "tElmEvcStatusInfo", "MAX_ELM_EVC_STATUS_INFO", sizeof(tElmEvcStatusInfo),MAX_ELM_EVC_STATUS_INFO, MAX_ELM_EVC_STATUS_INFO,0 },
{ "tElmEvcVlanInfo", "MAX_ELM_EVC_VLAN_INFO", sizeof(tElmEvcVlanInfo),MAX_ELM_EVC_VLAN_INFO, MAX_ELM_EVC_VLAN_INFO,0 },
{ "tElmAsynchMessageNode", "MAX_ELMI_ASYNC_MESG_NODES", sizeof(tElmAsynchMessageNode),MAX_ELMI_ASYNC_MESG_NODES, MAX_ELMI_ASYNC_MESG_NODES,0 },
{ "tElmQMsg", "MAX_ELMI_CONFIG_Q_MESG", sizeof(tElmQMsg),MAX_ELMI_CONFIG_Q_MESG, MAX_ELMI_CONFIG_Q_MESG,0 },
{ "tElmMsgNode", "MAX_ELMI_LOCAL_MESG", sizeof(tElmMsgNode),MAX_ELMI_LOCAL_MESG, MAX_ELMI_LOCAL_MESG,0 },
{ "tElmPortEntryArray", "MAX_ELMI_PORT_INFO_ARRAY", sizeof(tElmPortEntryArray),MAX_ELMI_PORT_INFO_ARRAY, MAX_ELMI_PORT_INFO_ARRAY,0 },
{ "tElmPortEntry", "MAX_ELMI_PORT_INFO", sizeof(tElmPortEntry),MAX_ELMI_PORT_INFO, MAX_ELMI_PORT_INFO,0 },
{ "tElmQMsg", "MAX_ELMI_Q_MESG", sizeof(tElmQMsg),MAX_ELMI_Q_MESG, MAX_ELMI_Q_MESG,0 },
{ "tElmTimer", "MAX_ELMI_TMR_NODES", sizeof(tElmTimer),MAX_ELMI_TMR_NODES, MAX_ELMI_TMR_NODES,0 },
{ "tLcmEvcCeVlanInfo", "MAX_LCM_EVC_CE_VLAN_INFO", sizeof(tLcmEvcCeVlanInfo),MAX_LCM_EVC_CE_VLAN_INFO, MAX_LCM_EVC_CE_VLAN_INFO,0 },
{ "tLcmEvcInfo", "MAX_LCM_EVC_INFO", sizeof(tLcmEvcInfo),MAX_LCM_EVC_INFO, MAX_LCM_EVC_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _ELMISZ_C  */
extern tFsModSizingParams FsELMISizingParams [];
#endif /*  _ELMISZ_C  */


