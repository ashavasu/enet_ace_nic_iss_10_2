/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmtrc.h,v 1.4 2010/04/01 14:12:05 prabuc Exp $
 *
 * Description: This file contains declarations of traces used by  
 *              the ELMI module.
 *
 *******************************************************************/


#ifndef _ELMTRC_H_
#define _ELMTRC_H_

/* Trace and debug flags */
#define   ELM_TRC_FLAG     gElmGlobalInfo .u4GlobalTraceOption

#ifdef TRACE_WANTED
#define ELM_GET_IFINDEX_STR(u4IfIndex) \
        (ELM_GET_PORTENTRY(u4IfIndex)!=NULL?\
         ((gElmGlobalInfo.ppPortEntry[u4IfIndex-1])->au1StrIfIndex):NULL)
#else
#define ELM_GET_IFINDEX_STR(u4IfIndex) ""
#endif

/* Trace definitions */
#ifdef  TRACE_WANTED

#define ELMI_TRACE_WANTED

#define ELM_PKT_DUMP(TraceType, pBuf, Length, Str) \
   UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str); \
   if (ELM_TRC_FLAG & ELM_DUMP_TRC) \
   {                                \
      ElmPktDump(pBuf, Length);     \
   }

#define ELM_GLOBAL_TRC  ElmGlobalTrace


#define ELM_TRC(TraceType, Str)                                \
        UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str)
        
#define ELM_TRC_ARG1(TraceType, Str, Arg1)                     \
        UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str, \
        Arg1)

#define ELM_TRC_ARG2(TraceType, Str, Arg1, Arg2)               \
        UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str, \
        Arg1, Arg2)

#define ELM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)         \
        UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str, \
        Arg1, Arg2, Arg3)
   
#define ELM_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)   \
        UtlTrcLog(ELM_TRC_FLAG, TraceType, ELM_MOD_NAME, Str, \
        Arg1, Arg2, Arg3, Arg4)

#define   ELM_DEBUG(x) {x}

#ifndef ELMI_DEBUG
#define ELM_PRINT  UtlTrcLog
#endif  /* ELMI_DEBUG */

#else  /* TRACE_WANTED */

#define ELM_GLOBAL_TRC  ElmGlobalTrace
#define ELM_PKT_DUMP(TraceType, pBuf, Length, Str)
#define ELM_TRC(TraceType, Str)
#define ELM_TRC_ARG1(TraceType, Str, Arg1)\
{\
		UNUSED_PARAM(Arg1);\
}
#define ELM_TRC_ARG2(TraceType, Str, Arg1, Arg2)\
{\
		UNUSED_PARAM(Arg1);\
		UNUSED_PARAM(Arg2);\
}
#define ELM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)\
{\
		UNUSED_PARAM(Arg1);\
		UNUSED_PARAM(Arg2);\
		UNUSED_PARAM(Arg3);\
}
#define ELM_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)\
{\
		UNUSED_PARAM(Arg1);\
		UNUSED_PARAM(Arg2);\
		UNUSED_PARAM(Arg3);\
		UNUSED_PARAM(Arg4);\
}

#define   ELM_DEBUG(x)

#endif /* TRACE_WANTED */
VOID ElmGlobalTrace (UINT4 u4Flags, const char *fmt, ...);
VOID ElmGlobalDebug (UINT4 u4Flags, const char *fmt, ...);

#endif/* _ELMTRC_H_ */

