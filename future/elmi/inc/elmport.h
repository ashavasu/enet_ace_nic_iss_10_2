/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmport.h,v 1.3 2007/11/19 07:21:37 iss Exp $
 *
 * Description: This file contains all the prototypes of wrapper
 *              functions used by the ELMI Module to call the 
 *              external apis.
 *
 *******************************************************************/


#ifndef _ELMIPORT_H_
#define _ELMIPORT_H_

INT4 
ElmGetNextEvcStatusInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
                        tLcmEvcStatusInfo *pLcmEvcStatusInfo,
                        UINT1 *pu1NoEvcInfo);
INT4 
ElmGetEvcStatusInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
                        tLcmEvcStatusInfo *pLcmEvcStatusInfo);
INT4 
ElmGetEvcCeVlanInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
                    tLcmEvcCeVlanInfo *pLcmEvcCeVlanInfo);
INT4 
ElmGetNextEvcCeVlanInfo(UINT2 u2PortNo, UINT2 u2EvcRefId, 
                        tLcmEvcCeVlanInfo *pLcmEvcCeVlanInfo,
                        UINT1 *pu1NoEvcInfo);

INT4 ElmGetUniInfo(UINT2 u2PortNo,tCfaUniInfo *pCfaUniInfo);

INT4 ElmGetEvcInfo(UINT2 u2PortNo,UINT2 u2EvcReferenceId,
                   tLcmEvcInfo *pLcmEvcInfo);

INT4 ElmGetNextEvcInfo(UINT2 u2PortNo,UINT2 u2EvcReferenceId,
                       tLcmEvcInfo *pLcmEvcInfo);
 
VOID ElmUpdateUniInfo(tCfaUniInfo CfaUniInfo);

VOID ElmPassCeVlanInfo(UINT2 u2PortNo,
                       tLcmEvcCeVlanInfo *pElmCeVlanInfo);

VOID ElmDatabaseUpdateIndication(UINT2 u2PortNo,
                                 tLcmEvcStatusInfo *pLcmEvcStatusInfo);

VOID ElmCreateNewEvcIndication (UINT2 u2PortNo,
                                tLcmEvcStatusInfo *pLcmEvcStatusInfo);

VOID ElmDeleteEvcIndication(UINT2 u2PortNo,UINT2 u2EvcReferenceId);

VOID ElmElmiEvcStatusChangeIndication(UINT2 u2PortNo,UINT2 u2EvcReferenceId,
                                  UINT1 u1EvcStatus);

VOID ElmElmiOperStatusIndication (UINT2 u2PortNo,UINT1 u1ElmOperStatus);
#endif /* _ELMIPORT_H_ */
