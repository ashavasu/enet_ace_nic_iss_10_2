/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: elmdbg.h,v 1.2 2007/11/19 07:21:37 iss Exp $
 *
 * Description: This file contains declarations of various Debug 
 *              Options used in ELMI Module.
 *
 *******************************************************************/


#ifndef _ELMDBG_H_
#define _ELMDBG_H_

/* Module names */
#define     ELM_MOD_NAME                 "ELM"

/* Trace definitions */
#ifdef      ELMI_DEBUG  
#define     ELM_GLOBAL_DBG  ElmGlobalDebug
	
#define     ELM_DBG(DebugType, Str) \
                UtlTrcLog(ELM_DBG_FLAG, DebugType, ELM_MOD_NAME)
        
#define     ELM_DBG_ARG1(DebugType, Str, Arg1) \
                UtlTrcLog(ELM_DBG_FLAG, DebugType, ELM_MOD_NAME, Arg1)

#define     ELM_DBG_ARG2(DebugType, Str, Arg1, Arg2) \
                UtlTrcLog(ELM_DBG_FLAG, DebugType, ELM_MOD_NAME, Arg1, Arg2)

#define     ELM_DBG_ARG3(DebugType, Str, Arg1, Arg2, Arg3) \
                UtlTrcLog(ELM_DBG_FLAG, DebugType, ELM_MOD_NAME, Arg1, Arg2, Arg3)

#define     ELM_DBG_ARG4(DebugType, Str, Arg1, Arg2, Arg3, Arg4) \
                UtlTrcLog(ELM_DBG_FLAG, DebugType, ELM_MOD_NAME, \
                  Arg1, Arg2, Arg3, Arg4)

#define     ELM_PRINT       UtlTrcLog

#define     ELM_PRINTF(fmt) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, fmt)
#define     ELM_PRINTF1(fmt,x1) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, fmt,x1)
#define     ELM_PRINTF2(fmt,x1,x2) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, fmt,x1,x2)
#define     ELM_PRINTF3(fmt,x1,x2,x3) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, fmt,x1, x2, x3)
#define     ELM_PRINTF4(fmt,x1,x2,x3,x4) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, fmt,x1,x2,x3,x4)
#define     ELM_PRINTF5(fmt,x1,x2,x3,x4,x5) \
                UtlTrcLog((UINT4)255, (UINT4)255, ELM_MOD_NAME, \
                fmt,x1,x2,x3,x4,x5)

#else  /* ELMI_DEBUG */

#define     ELM_DBG(DebugType, Str)
#define     ELM_DBG_ARG1(DebugType, Str, Arg1)
#define     ELM_DBG_ARG2(DebugType, Str, Arg1, Arg2)
#define     ELM_DBG_ARG3(DebugType, Str, Arg1, Arg2, Arg3)
#define     ELM_DBG_ARG4(DebugType, Str, Arg1, Arg2, Arg3, Arg4)
   
#ifndef     ELMI_TRACE_WANTED
#define     ELM_PRINT
#endif      /* ELMI_TRACE_WANTED */
#define     ELM_PRINTF(x)
#define     ELM_PRINTF1(fmt,x1)
#define     ELM_PRINTF2(fmt,x1,x2)
#define     ELM_PRINTF3(fmt,x1,x2,x3)
#define     ELM_PRINTF4(fmt,x1,x2,x3,x4)
#define     ELM_PRINTF5(fmt,x1,x2,x3,x4,x5)

#endif /* ELMI_DEBUG */

#endif/* _ELMDBG_H_ */
