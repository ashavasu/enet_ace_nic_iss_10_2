#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 10th Aug 2007                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

ELMI_SWITCHES = -DELMI_DEBUG -DELMI_TRAP_WANTED -DTRACE_WANTED


TOTAL_OPNS = ${ELMI_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

ELMI_BASE_DIR   = ${BASE_DIR}/elmi

ELMI_SRC_DIR       = ${ELMI_BASE_DIR}/src
ELMI_INC_DIR       = ${ELMI_BASE_DIR}/inc
ELMI_OBJ_DIR       = ${ELMI_BASE_DIR}/obj
SNMP_INC_DIR       = ${BASE_DIR}/inc/snmp
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
FSAP_INC_DIR       = ${BASE_DIR}/fsap2/linux
# VCM Module should be included here #

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${ELMI_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${SNMP_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
