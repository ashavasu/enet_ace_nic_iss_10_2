 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapmain.c,v 1.6 2013/06/26 12:08:31 siva Exp $
 *
 * Description:This file contains the Init, route map creation 
               routines for Route Map Module.
 *
 *******************************************************************/

#include "rmapinc.h"
#include "fsrmapwr.h"

UINT4               gu4RMapGlobalTrc;
UINT4               gu4RMapGlobalDbg;
tRMapGlobalInfo     gRMapGlobalInfo;

/**************************************************************************/
/*   Function Name   : RMapInit                                           */
/*   Description     : This function creates Semaphore, allocates memory  */
/*                     and  Embedded RBTree to store the RouteMap Entry   */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapInit (VOID)
{
    /* Create the semaphore identifier to access the RMAP datastructures */
    if (OsixSemCrt
        (RMAP_RW_SEM_NAME, &(gRMapGlobalInfo.RMapRwSemId)) != OSIX_SUCCESS)
    {
        return RMAP_FAILURE;
    }
    OsixSemGive (gRMapGlobalInfo.RMapRwSemId);

    if (OsixSemCrt
        (RMAP_INT_SEM_NAME, &(gRMapGlobalInfo.RMapIntSemId)) != OSIX_SUCCESS)
    {
        return RMAP_FAILURE;
    }
    OsixSemGive (gRMapGlobalInfo.RMapIntSemId);
    gRMapGlobalInfo.u4ReaderCount = 0;

    /* Create the RBTree for RouteMap Table */
    gRMapGlobalInfo.RMapRoot =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tRMapNode, RBRMapNode)),
                              RMapCompareMapEntry);

    if (gRMapGlobalInfo.RMapRoot == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "Route Map RB Tree creation failed.\n");
        return RMAP_FAILURE;
    }

    /* Create the RBTree for IP Prefix Table */
    gRMapGlobalInfo.IpPrefixRoot =
        RBTreeCreateEmbedded (0, RMapIpPrefixCompareEntry);

    if (gRMapGlobalInfo.IpPrefixRoot == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "IP Prefix RB Tree creation failed.\n");
        return RMAP_FAILURE;
    }

    if (RmapSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "Mempool Allocation for RMAP failed.\n");
        return RMAP_FAILURE;
    }
    gRMapGlobalInfo.RMapSetPoolId =
        RMAPMemPoolIds[MAX_ROUTEMAP_SET_NODES_SIZING_ID];
    gRMapGlobalInfo.RMapMatchListPoolId =
        RMAPMemPoolIds[MAX_ROUTEMAP_MATCH_LISTS_SIZING_ID];
    gRMapGlobalInfo.RMapMatchPoolId =
        RMAPMemPoolIds[MAX_ROUTEMAP_MATCH_NODES_SIZING_ID];
    gRMapGlobalInfo.RMapPoolId = RMAPMemPoolIds[MAX_ROUTEMAP_NODES_SIZING_ID];
    gRMapGlobalInfo.IpPrefixPoolId =
        RMAPMemPoolIds[MAX_IP_PREFIX_NODES_SIZING_ID];

    gRMapGlobalInfo.u4RMapTrapEnabled = RMAP_TRAP_DEFAULT;

#if defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    RegisterFSRMAP ();
#endif
    return RMAP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RMapDeInit                                         */
/*   Description     : This function releases the semaphore, releases the */
/*                     memory allocated and deletes Route Map RBTree      */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapDeInit (VOID)
{
    OsixSemDel (gRMapGlobalInfo.RMapRwSemId);
    OsixSemDel (gRMapGlobalInfo.RMapIntSemId);

    if (gRMapGlobalInfo.RMapRoot != NULL)
        RBTreeDelete (gRMapGlobalInfo.RMapRoot);

    if (gRMapGlobalInfo.IpPrefixRoot != NULL)
        RBTreeDelete (gRMapGlobalInfo.IpPrefixRoot);

    RmapSizingMemDeleteMemPools ();

    gRMapGlobalInfo.RMapSetPoolId = 0;
    gRMapGlobalInfo.RMapMatchListPoolId = 0;
    gRMapGlobalInfo.RMapMatchPoolId = 0;
    gRMapGlobalInfo.RMapPoolId = 0;
    gRMapGlobalInfo.IpPrefixPoolId = 0;

    return RMAP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RMapLockWrite                                      */
/*   Description     : This function locks WRITE access to rmap           */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapLockWrite (VOID)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (OsixSemTake (gRMapGlobalInfo.RMapRwSemId) == OSIX_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "RouteMap Semaphore creation failed.\n");
    }
    else
    {
        i4RetVal = SNMP_SUCCESS;
    }

    return i4RetVal;
}

/**************************************************************************/
/*   Function Name   : RMapUnLockWrite                                    */
/*   Description     : This function unlocks WRITE access to rmap         */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapUnLockWrite (VOID)
{
    OsixSemGive (gRMapGlobalInfo.RMapRwSemId);
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RMapLockRead                                       */
/*   Description     : This function locks READ access to rmap            */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapLockRead (VOID)
{
    /* lock internal data */
    if (OsixSemTake (gRMapGlobalInfo.RMapIntSemId) == OSIX_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "RouteMap Semaphore creation failed.\n");
        return SNMP_FAILURE;
    }

    /* increment reader counter */
    gRMapGlobalInfo.u4ReaderCount++;

    /* if this reader is 1st - lock r/w */
    if (gRMapGlobalInfo.u4ReaderCount == 1)
    {
        if (OsixSemTake (gRMapGlobalInfo.RMapRwSemId) == OSIX_FAILURE)
        {
            /* decrement reader counter */
            gRMapGlobalInfo.u4ReaderCount--;

            /* unlock internal data */
            OsixSemGive (gRMapGlobalInfo.RMapIntSemId);

            RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                      "RouteMap Semaphore creation failed.\n");

            return SNMP_FAILURE;
        }
    }

    /* unlock internal data */
    OsixSemGive (gRMapGlobalInfo.RMapIntSemId);

    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RMapUnLockRead                                     */
/*   Description     : This function unlocks READ access to rmap          */
/*   Return Value    : RMAP_SUCCESS or RMAP_FAILURE                       */
/**************************************************************************/
INT4
RMapUnLockRead (VOID)
{
    /* lock internal data */
    if (OsixSemTake (gRMapGlobalInfo.RMapIntSemId) == OSIX_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_INIT_TRC, RMAP_MODULE_NAME,
                  "RouteMap Semaphore creation failed.\n");
        return SNMP_FAILURE;
    }

    /* decrement reader counter */
    gRMapGlobalInfo.u4ReaderCount--;

    /* if this reader is last - unlock r/w */
    if (gRMapGlobalInfo.u4ReaderCount == 0)
    {
        OsixSemGive (gRMapGlobalInfo.RMapRwSemId);
    }

    /* unlock internal data */
    OsixSemGive (gRMapGlobalInfo.RMapIntSemId);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : RMapSendUpdateMsg                                          */
/*                                                                           */
/* Description  : This function sends Route Map Update Message to RTM/RTM6   */
/*                It is invoked when the MAP/MATCH/SET entry                 */
/*                updated (Deleted/Added)                                    */
/*                                                                           */
/* Input        : pu1RMapName - Route Map name                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RMapSendUpdateMsg (UINT1 *pu1RMapName)
{
    /* RTM4 always should register with rmap !!! */
    RMAP_SEND_UPDATE_MSG_CALLBACK (RMAP_APP_RTM) (pu1RMapName, 0);

    if (RMAP_SEND_UPDATE_MSG_CALLBACK (RMAP_APP_RTM6) != NULL)
    {
        RMAP_SEND_UPDATE_MSG_CALLBACK (RMAP_APP_RTM6) (pu1RMapName, 0);
    }
}

/*****************************************************************************/
/* Function     : RMapSendStatusUpdateMsg                                    */
/*                                                                           */
/* Description  : This function sends Route Map Update Message to protocols  */
/*                It is invoked when the MAP entry                           */
/*                updated (Deleted/Added)                                    */
/*                                                                           */
/* Input        : pu1RMapName - Route Map name                               */
/*                u4Status - route map status, one bit per MAP node          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RMapSendStatusUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    UINT1               u1AppId;

    for (u1AppId = RMAP_APP_RIP; u1AppId < RMAP_MAX_APPLNS; u1AppId++)
    {
        if (RMAP_SEND_UPDATE_MSG_CALLBACK (u1AppId) != NULL)
        {
            RMAP_SEND_UPDATE_MSG_CALLBACK (u1AppId) (pu1RMapName, u4Status);
        }

    }
}
