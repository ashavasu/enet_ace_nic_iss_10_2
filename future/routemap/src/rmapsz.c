/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RMAPSZ_C
#include "rmapinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RmapSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMAP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsRMAPSizingParams[i4SizingId].u4StructSize,
                              FsRMAPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RMAPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RmapSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RmapSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRMAPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RMAPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RmapSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RMAP_MAX_SIZING_ID; i4SizingId++)
    {
        if (RMAPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RMAPMemPoolIds[i4SizingId]);
            RMAPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
