/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmaputils.c,v 1.19 2016/12/27 12:36:00 siva Exp $
 *
 * Description:This file contains the Init, route map creation 
               routines for Route Map Module.
 *
 *******************************************************************/

#include "rmapinc.h"
#include "utilcli.h"
extern UINT4        gu4RMapGlobalTrc;
extern UINT4        gu4RMapGlobalDbg;
extern tRMapGlobalInfo gRMapGlobalInfo;

/*****************************************************************************/
/* Function     : ValidateXAddr                                              */
/*                                                                           */
/* Description  : Validate tIPvXAddr                                         */
/*                                                                           */
/* Input        : pXAddr   Pointer to tIPvXAddr                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS - tIPvXAddr is valid                          */
/*                RMAP_FAILURE - tIPvXAddr is not valid                      */
/*****************************************************************************/
INT4
ValidateXAddr (tIPvXAddr * pXAddr)
{
    UINT4               u4IpAddr;
    tIp6Addr            Ip6Addr;
    INT4                i4Res = RMAP_FAILURE;

    if (pXAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, pXAddr->au1Addr, IPVX_IPV4_ADDR_LEN);
        if ((u4IpAddr != 0) &&
            (u4IpAddr != IP_GEN_BCAST_ADDR) &&
            (u4IpAddr != IP_LOOPBACK_ADDRESS))
        {
            i4Res = RMAP_SUCCESS;
        }
        else
        {
            i4Res = RMAP_FAILURE;
        }

    }

    else if (pXAddr->u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr, pXAddr->au1Addr, IPVX_IPV6_ADDR_LEN);
        if (IS_ADDR_UNSPECIFIED (Ip6Addr) ||
            IS_ADDR_MULTI (Ip6Addr) || IS_ADDR_LOOPBACK (Ip6Addr))
        {
            i4Res = RMAP_FAILURE;
        }
        else
        {
            i4Res = RMAP_SUCCESS;
        }
    }

    return i4Res;
}

/*****************************************************************************/
/* Function     : ValidateIpv4Addr                                              */
/*                                                                           */
/* Description  : Validate tIPvXAddr                                         */
/*                                                                           */
/* Input        : pIpAddr   Pointer to tIPvXAddr                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS - tIPvXAddr is valid                          */
/*                RMAP_FAILURE - tIPvXAddr is not valid                      */
/*****************************************************************************/
INT4
ValidateIpv4Addr (tIPvXAddr * pIpAddr, UINT4 i4PrefixLen)
{
    UINT4              *pu4IpAddr = NULL;
    UINT4               u4NetAddr;
    UINT4               u4SubnetAddr;
    UINT4               u4BroadcastAddr;
    tUtlInAddr          pAddr;
    UINT1               IpAddr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4DestMask = 0;
    u4SubnetAddr =
        (IP_GEN_BCAST_ADDR << (IPVX_IPV4_MAX_MASK_LEN - (i4PrefixLen)));
    u4DestMask = u4SubnetAddr;
    pu4IpAddr = (UINT4 *) (VOID *) (pIpAddr->au1Addr);
    u4NetAddr = u4SubnetAddr & *pu4IpAddr;
    u4SubnetAddr = u4SubnetAddr ^ IP_GEN_BCAST_ADDR;
    u4BroadcastAddr = u4SubnetAddr | u4NetAddr;

    if (pIpAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if((*pu4IpAddr & u4DestMask)  != *pu4IpAddr)
        {
            return RMAP_FAILURE;
        }
        pAddr.u4Addr = OSIX_HTONL (*pu4IpAddr);
        MEMCPY (&IpAddr, &pAddr, sizeof (UINT4));
        /* Avoid 0.x.y.z network */
        if (IpAddr[0] == 0)
        {
            return RMAP_FAILURE;
        }
        /* Avoid network broadcast address */
        else if (IpAddr[0] == 255)
        {
            return RMAP_FAILURE;
        }
        else if((i4PrefixLen != 31) && (i4PrefixLen != 32))
        {
            if (u4BroadcastAddr == *pu4IpAddr)
            {
                return RMAP_FAILURE;
            }
        }
    }

    if (pIpAddr->u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
       if(i4PrefixLen > 128)
       {
          return RMAP_FAILURE;
       }
    }
    return RMAP_SUCCESS;
}

/*****************************************************************************/
/* Function     : ValidateXAddr                                              */
/*                                                                           */
/* Description  : Validate IPv4/IPv6 prefix length                           */
/*                                                                           */
/* Input        : u4Prefix   -prefix length                                  */
/*                u1Family   -address family                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS - prefix length is valid                      */
/*                RMAP_FAILURE - prefix length is not valid                  */
/*****************************************************************************/
INT4
ValidateXAddrPrefix (UINT4 u4Prefix, UINT1 u1Family)
{
    INT4                i4Res = RMAP_FAILURE;

    if (u1Family == 0)
    {
        i4Res = RMAP_SUCCESS;
    }
    else if (u1Family == IPVX_ADDR_FMLY_IPV4)
    {
        if (u4Prefix == 0)
        {
            i4Res = RMAP_FAILURE;
        }
        else
        {
            i4Res = RMAP_SUCCESS;
        }

    }
    else if (u1Family == IPVX_ADDR_FMLY_IPV6)
    {
        if ((u4Prefix > 0) && (u4Prefix <= 128))
        {
            i4Res = RMAP_SUCCESS;
        }
        else
        {
            i4Res = RMAP_FAILURE;
        }
    }

    return i4Res;
}

/*****************************************************************************/
/* Function     : CmpIp6Addr                                                 */
/*                                                                           */
/* Description  : Compare tIp6Addr with prefix                               */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - addresses are equal                                    */
/*                other - the same as memcmp                                 */
/*****************************************************************************/
INT1
CmpIp6Addr (tIp6Addr * pa1, UINT1 preflen1, tIp6Addr * pa2, UINT1 preflen2)
{
    UINT1               blen, bitlen, mask, t1, t2;
    INT4                i4MemRes = 0;
    INT1                res = 0;

    if (preflen1 != preflen2)
    {
        return 1;
    }

    blen = (preflen1 >> 3);
    bitlen = preflen1 - (blen << 3);

    /* compare bytes */
    if (blen != 0)
    {
        i4MemRes = MEMCMP (pa1, pa2, blen);
        res = (INT1) i4MemRes;
    }
    if (res != 0)
    {
        return res;
    }

    /* compare last byte */
    if (bitlen)
    {
        mask = 0xff << (8 - bitlen);
        t1 = (((UINT1 *) pa1)[blen] & mask);
        t2 = (((UINT1 *) pa2)[blen] & mask);
        res = t1 ^ t2;
    }

    return res;
}

/*****************************************************************************/
/* Function     : CmpIpvxAddrWithPrefix                                      */
/*                                                                           */
/* Description  : Compare tIPvXAddr with prefix                              */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - addresses are equal                                    */
/*                other - the same as memcmp                                 */
/*****************************************************************************/
INT1
CmpIpvxAddrWithPrefix (tIPvXAddr * pa1, UINT1 preflen1,
                       tIPvXAddr * pa2, UINT1 preflen2)
{
    INT1                r = 1;
    UINT4               u4Addr1, u4Addr2, u4Mask1, u4Mask2;

    if ((pa1->u1Afi == IPVX_ADDR_FMLY_IPV4) &&
        (pa2->u1Afi == IPVX_ADDR_FMLY_IPV4))
    {
        MEMCPY (&u4Addr1, pa1->au1Addr, sizeof (UINT4));
        MEMCPY (&u4Addr2, pa2->au1Addr, sizeof (UINT4));

        IPV4_MASKLEN_TO_MASK (u4Mask1, preflen1);
        IPV4_MASKLEN_TO_MASK (u4Mask2, preflen2);

        r = ((u4Addr1 & u4Mask1) == (u4Addr2 & u4Mask2)) ? 0 : 1;
    }
    else if ((pa1->u1Afi == IPVX_ADDR_FMLY_IPV6) &&
             (pa2->u1Afi == IPVX_ADDR_FMLY_IPV6))
    {
        r = CmpIp6Addr ((tIp6Addr *) (VOID *) (pa1->au1Addr), preflen1,
                        (tIp6Addr *) (VOID *) (pa2->au1Addr), preflen2);
    }

    return r;
}

/*****************************************************************************/
/* Function     : CmpIpvxAddr                                                */
/*                                                                           */
/* Description  : Compare tIPvXAddr                                          */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - addresses are equal                                    */
/*                other - the same as memcmp                                 */
/*****************************************************************************/
INT1
CmpIpvxAddr (tIPvXAddr * pa1, tIPvXAddr * pa2)
{
    INT4                r, len;

    if (pa1->u1Afi != pa2->u1Afi)
    {
        return 1;
    }

    IPVX_ADDR_FMLY_TO_LEN (len, pa1->u1Afi);

    r = MEMCMP (pa1->au1Addr, pa2->au1Addr, len);

    return (INT1) r;
}

/*****************************************************************************/
/* Function     : TestZeroIpv6Addr                                           */
/*                                                                           */
/* Description  : Test if tIp6Addr is zero                                   */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - addresse is zero                                       */
/*                other - address is not zero                                */
/*****************************************************************************/
UINT4
TestZeroIpv6Addr (tIp6Addr * pIp6Addr)
{
    /* detect if ip6 addr == 0 */
    if (pIp6Addr == NULL)
    {
        return 0;
    }

    return pIp6Addr->u4_addr[0] |
        pIp6Addr->u4_addr[1] | pIp6Addr->u4_addr[2] | pIp6Addr->u4_addr[3];
}

/*****************************************************************************/
/* Function     : TestZeroIpvxAddr                                           */
/*                                                                           */
/* Description  : Test if tIPvXAddr is zero                                  */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - addresse is zero                                       */
/*                other - address is not zero                                */
/*****************************************************************************/
UINT4
TestZeroIpvxAddr (tIPvXAddr * pa)
{
    UINT4              *pu4;
    UINT4               u4Temp[4] = { 0, 0, 0, 0 };

    if (pa == NULL)
    {
        return 0;
    }

    pu4 = u4Temp;

    if (pa->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pu4, pa->au1Addr, sizeof (IPVX_IPV4_ADDR_LEN));
        return pu4[0];
    }
    else if (pa->u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pu4, pa->au1Addr, sizeof (IPVX_IPV6_ADDR_LEN));
        return pu4[0] | pu4[1] | pu4[2] | pu4[3];
    }

    return 0;
}

/*****************************************************************************/
/* Function     : TestZeroArray                                              */
/*                                                                           */
/* Description  : Test if byte array is zero                                 */
/*                                                                           */
/* TODO         : Move this function to common utilities file                */
/*                                                                           */
/* Returns      : 0 - array is zero                                          */
/*                other - array is not zero                                  */
/*****************************************************************************/
UINT1
TestZeroArray (UINT1 *pu1, UINT4 u4Size)
{
    UINT1               u1Res = 0;

    if (pu1 == NULL)
    {
        return u1Res;
    }

    while (u4Size--)
    {
        u1Res |= *pu1++;
    }

    return u1Res;
}

/*****************************************************************************/
/* Function     : RMapIpPrefixCompareEntry                                   */
/*                                                                           */
/* Description  : Compare the Ip prefix nodes                                */
/*                                                                           */
/* Input        : e1   Pointer to IP prefix node1                            */
/*                e2   Pointer to Ip Prefixe node2                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
RMapIpPrefixCompareEntry (tRBElem * e1, tRBElem * e2)
{
    tRMapIpPrefix      *pIpPrefixEntry1 = e1;
    tRMapIpPrefix      *pIpPrefixEntry2 = e2;
    INT4                i4RetVal = 0;

    if (STRLEN (pIpPrefixEntry1->au1IpPrefixName) >
        STRLEN (pIpPrefixEntry2->au1IpPrefixName))
    {
        return RMAP_RB_GREATER;
    }
    else if (STRLEN (pIpPrefixEntry1->au1IpPrefixName) <
             STRLEN (pIpPrefixEntry2->au1IpPrefixName))
    {
        return RMAP_RB_LESSER;
    }
    else
    {
        i4RetVal =
            MEMCMP (pIpPrefixEntry1->au1IpPrefixName,
                    pIpPrefixEntry2->au1IpPrefixName,
                    STRLEN (pIpPrefixEntry1->au1IpPrefixName));
        if (i4RetVal > 0)
        {
            return RMAP_RB_GREATER;
        }
        else if (i4RetVal < 0)
        {
            return RMAP_RB_LESSER;
        }
        else
        {
            return RMAP_RB_EQUAL;
        }
    }
}

/*****************************************************************************/
/* Function     : RMapCompareMapEntry                                        */
/*                                                                           */
/* Description  : Compare the route maps                                     */
/*                                                                           */
/* Input        : e1   Pointer to RouteMap node  node1                       */
/*                e2   Pointer to RouteMap node  node2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
RMapCompareMapEntry (tRBElem * e1, tRBElem * e2)
{
    tRMapNode          *pRMapEntry1 = e1;
    tRMapNode          *pRMapEntry2 = e2;
    INT4                i4RetVal = 0;

    if (STRLEN (pRMapEntry1->au1RMapName) > STRLEN (pRMapEntry2->au1RMapName))
    {
        return RMAP_RB_GREATER;
    }
    else if (STRLEN (pRMapEntry1->au1RMapName) <
             STRLEN (pRMapEntry2->au1RMapName))
    {
        return RMAP_RB_LESSER;
    }
    else
    {
        i4RetVal = MEMCMP (pRMapEntry1->au1RMapName, pRMapEntry2->au1RMapName,
                           STRLEN (pRMapEntry1->au1RMapName));
        if (i4RetVal > 0)
        {
            return RMAP_RB_GREATER;
        }
        else if (i4RetVal < 0)
        {
            return RMAP_RB_LESSER;
        }
    }
    if (pRMapEntry1->u4SeqNo > pRMapEntry2->u4SeqNo)
    {
        return RMAP_RB_GREATER;
    }
    else if (pRMapEntry1->u4SeqNo < pRMapEntry2->u4SeqNo)
    {
        return RMAP_RB_LESSER;
    }
    else
    {
        return RMAP_RB_EQUAL;
    }
}

/*****************************************************************************/
/* Function     : RMapCompareMatchEntry                                      */
/*                                                                           */
/* Description  : Compare the route map match entries                        */
/*                                                                           */
/* Input        : e1        Pointer to RouteMap Match node  node1            */
/*                e2        Pointer to RouteMap Match  node  node2           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
RMapCompareMatchEntry (tRBElem * e1, tRBElem * e2)
{
    tRMapMatchNode     *pRMapMatchEntry1 = e1;
    tRMapMatchNode     *pRMapMatchEntry2 = e2;
    INT4                i4RetVal = RMAP_RB_EQUAL;

    if (pRMapMatchEntry1->u1Cmd > pRMapMatchEntry2->u1Cmd)
    {
        i4RetVal = RMAP_RB_GREATER;
    }
    else if (pRMapMatchEntry1->u1Cmd < pRMapMatchEntry2->u1Cmd)
    {
        i4RetVal = RMAP_RB_LESSER;
    }

    else
    {
        switch (pRMapMatchEntry1->u1Cmd)
        {
            case RMAP_MATCH_CMD_DEST_IPX:
                if (pRMapMatchEntry1->RMapDestT > pRMapMatchEntry2->RMapDestT)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapDestT <
                         pRMapMatchEntry2->RMapDestT)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                else
                {
                    i4RetVal = IPVX_ADDR_COMPARE (pRMapMatchEntry1->RMapDestX,
                                                  pRMapMatchEntry2->RMapDestX);
                }

                if (i4RetVal == RMAP_RB_EQUAL)
                {
                    if (pRMapMatchEntry1->RMapDestPrefixLen >
                        pRMapMatchEntry2->RMapDestPrefixLen)
                    {
                        i4RetVal = RMAP_RB_GREATER;
                    }
                    else if (pRMapMatchEntry1->RMapDestPrefixLen <
                             pRMapMatchEntry2->RMapDestPrefixLen)
                    {
                        i4RetVal = RMAP_RB_LESSER;
                    }
                }
                break;

            case RMAP_MATCH_CMD_SOURCE_IPX:
                if (pRMapMatchEntry1->RMapSourceT >
                    pRMapMatchEntry2->RMapSourceT)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapSourceT <
                         pRMapMatchEntry2->RMapSourceT)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                else
                {
                    i4RetVal = IPVX_ADDR_COMPARE (pRMapMatchEntry1->RMapSourceX,
                                                  pRMapMatchEntry2->
                                                  RMapSourceX);
                }

                if (i4RetVal == RMAP_RB_EQUAL)
                {
                    if (pRMapMatchEntry1->RMapSourcePrefixLen >
                        pRMapMatchEntry2->RMapSourcePrefixLen)
                    {
                        i4RetVal = RMAP_RB_GREATER;
                    }
                    else if (pRMapMatchEntry1->RMapSourcePrefixLen <
                             pRMapMatchEntry2->RMapSourcePrefixLen)
                    {
                        i4RetVal = RMAP_RB_LESSER;
                    }
                }
                break;

            case RMAP_MATCH_CMD_NEXTHOP_IPX:
                if (pRMapMatchEntry1->RMapNextHopT >
                    pRMapMatchEntry2->RMapNextHopT)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapNextHopT <
                         pRMapMatchEntry2->RMapNextHopT)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                else
                {
                    i4RetVal =
                        IPVX_ADDR_COMPARE (pRMapMatchEntry1->RMapNextHopX,
                                           pRMapMatchEntry2->RMapNextHopX);
                }
                break;

            case RMAP_MATCH_CMD_INTERFACE:
                if ((pRMapMatchEntry1->RMapIfIndex) >
                    (pRMapMatchEntry2->RMapIfIndex))
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if ((pRMapMatchEntry1->RMapIfIndex) <
                         (pRMapMatchEntry2->RMapIfIndex))
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_METRIC:
                if (pRMapMatchEntry1->RMapMetric > pRMapMatchEntry2->RMapMetric)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapMetric <
                         pRMapMatchEntry2->RMapMetric)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_TAG:
                if (pRMapMatchEntry1->RMapTag > pRMapMatchEntry2->RMapTag)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapTag < pRMapMatchEntry2->RMapTag)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_METRIC_TYPE:
                if (pRMapMatchEntry1->RMapMetricType >
                    pRMapMatchEntry2->RMapMetricType)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapMetricType <
                         pRMapMatchEntry2->RMapMetricType)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_ROUTE_TYPE:
                if (pRMapMatchEntry1->RMapRouteType >
                    pRMapMatchEntry2->RMapRouteType)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapRouteType <
                         pRMapMatchEntry2->RMapRouteType)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_ASPATH_TAG:
                if (pRMapMatchEntry1->RMapNextHopAS >
                    pRMapMatchEntry2->RMapNextHopAS)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapNextHopAS <
                         pRMapMatchEntry2->RMapNextHopAS)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_COMMUNITY:
                if (pRMapMatchEntry1->RMapCommunity >
                    pRMapMatchEntry2->RMapCommunity)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapCommunity <
                         pRMapMatchEntry2->RMapCommunity)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_LOCAL_PREF:
                if (pRMapMatchEntry1->RMapLocalPref >
                    pRMapMatchEntry2->RMapLocalPref)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapLocalPref <
                         pRMapMatchEntry2->RMapLocalPref)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_CMD_ORIGIN:
                if (pRMapMatchEntry1->RMapOrigin > pRMapMatchEntry2->RMapOrigin)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->RMapOrigin <
                         pRMapMatchEntry2->RMapOrigin)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                break;

            case RMAP_MATCH_IP_PREFIX_ENTRY:
                if (pRMapMatchEntry1->IpPrefixAddrType >
                    pRMapMatchEntry2->IpPrefixAddrType)
                {
                    i4RetVal = RMAP_RB_GREATER;
                }
                else if (pRMapMatchEntry1->IpPrefixAddrType <
                         pRMapMatchEntry2->IpPrefixAddrType)
                {
                    i4RetVal = RMAP_RB_LESSER;
                }
                else
                {
                    i4RetVal = IPVX_ADDR_COMPARE (pRMapMatchEntry1->IpPrefixA,
                                                  pRMapMatchEntry2->IpPrefixA);
                }

                if (i4RetVal == RMAP_RB_EQUAL)
                {
                    if (pRMapMatchEntry1->IpPrefixLength >
                        pRMapMatchEntry2->IpPrefixLength)
                    {
                        i4RetVal = RMAP_RB_GREATER;
                    }
                    else if (pRMapMatchEntry1->IpPrefixLength <
                             pRMapMatchEntry2->IpPrefixLength)
                    {
                        i4RetVal = RMAP_RB_LESSER;
                    }
                }
                break;
            default:
                RMAP_TRC (RMAP_MOD_TRC, ALL_FAILURE_TRC, RMAP_MODULE_NAME,
                          "Unknown option in Route Map MATCH entry comparison.\n");
                break;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : RMapGetRouteMapNode                                        */
/*                                                                           */
/* Description  : This function returns MAP Node for the given RouteMap      */
/*                name and sequence number                                   */
/*                                                                           */
/* Input        : pu1RMapName - Route Map Name                               */
/*                u4SeqNo - Sequence number                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pRMapNode - Pointer to RouteMap Node                       */
/*****************************************************************************/
tRMapNode          *
RMapGetRouteMapNode (UINT1 *pu1RMapName, UINT4 u4SeqNo)
{
    tRMapNode           RMapNode, *pRMapNode = NULL;

    MEMSET (&RMapNode, 0, sizeof (tRMapNode));

    if (pu1RMapName != NULL)
    {
        STRNCPY (RMapNode.au1RMapName, pu1RMapName,
                 (sizeof (RMapNode.au1RMapName) - 1));
        RMapNode.u4SeqNo = u4SeqNo;

        pRMapNode = (tRMapNode *) RBTreeGet
            (gRMapGlobalInfo.RMapRoot, &RMapNode);
    }
    return pRMapNode;
}

tRMapNode          *
RMapGetRouteMapNodeOct (tSNMP_OCTET_STRING_TYPE * pOct, UINT4 u4SeqNo)
{
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    RMAP_OCT_2_ASCIIZ (au1Name, pOct);
    return RMapGetRouteMapNode (au1Name, u4SeqNo);
}

/*****************************************************************************/
/* Function     : RMapGetNextRouteMapNode                                    */
/*                                                                           */
/* Description  : This function returns MAP Node next to the given RouteMap  */
/*                name and sequence number                                   */
/*                                                                           */
/* Input        : pu1RMapName - Route Map Name                               */
/*                u4SeqNo - Sequence number                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pRMapNode - Pointer to RouteMap Node                       */
/*****************************************************************************/
tRMapNode          *
RMapGetNextRouteMapNode (UINT1 *pu1RMapName, UINT4 u4SeqNo)
{
    tRMapNode           RMapNode, *pRMapNode = NULL;

    MEMSET (&RMapNode, 0, sizeof (tRMapNode));

    if (pu1RMapName != NULL)
    {
        STRNCPY (RMapNode.au1RMapName, pu1RMapName,
                 (sizeof (RMapNode.au1RMapName) - 1));
        RMapNode.u4SeqNo = u4SeqNo;

        pRMapNode = (tRMapNode *) RBTreeGetNext
            (gRMapGlobalInfo.RMapRoot, &RMapNode, NULL);
    }
    return pRMapNode;
}

tRMapNode          *
RMapGetNextRouteMapNodeOct (tSNMP_OCTET_STRING_TYPE * pOct, UINT4 u4SeqNo)
{
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    RMAP_OCT_2_ASCIIZ (au1Name, pOct);
    return RMapGetNextRouteMapNode (au1Name, u4SeqNo);
}

/*****************************************************************************/
/* Function     : RMapAddRouteMapNode                                        */
/*                                                                           */
/* Description  : This function allocates memory for the Route Map node,     */
/*                creates RBTree for RouteMap MATCH entry, adds the Route    */
/*                Map Node RBTree                                            */
/*                                                                           */
/* Input        : pu1RMapName - Route Map Name                               */
/*                u4SeqNo - Sequence number                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pRMapNode - Pointer to the Route Map Node, if the Route    */
/*                Map created successfully                                   */
/*                NULL - otherwise                                           */
/*****************************************************************************/
tRMapNode          *
RMapAddRouteMapNode (UINT1 *pu1RMapName, UINT4 u4SeqNo)
{
    tRMapNode          *pRMapNode = NULL;

    if ((pRMapNode = (tRMapNode *) MemAllocMemBlk (gRMapGlobalInfo.RMapPoolId))
        == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Malloc for Route Map Entry %s failed. \n", pu1RMapName);
        return NULL;
    }

    MEMSET (pRMapNode, 0, sizeof (tRMapNode));

    STRNCPY (pRMapNode->au1RMapName, pu1RMapName,
             (sizeof (pRMapNode->au1RMapName) - 1));

    pRMapNode->u4SeqNo = u4SeqNo;
    pRMapNode->u1Access = RMAP_ROUTE_PERMIT;

    /* Create RBTree for MATCH Entry */
    pRMapNode->RMapMatchRoot = RBTreeCreateEmbedded
        ((FSAP_OFFSETOF (tRMapMatchNode, RBRMapMatchNode)),
         RMapCompareMatchEntry);
    if (pRMapNode->RMapMatchRoot == NULL)
    {
        MemReleaseMemBlock (gRMapGlobalInfo.RMapPoolId, (UINT1 *) (pRMapNode));
        pRMapNode = NULL;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "RBTree creation for Route Map Entry %s failed.\n",
                       pRMapNode->au1RMapName);
    }
    else
    {
        if (RBTreeAdd (gRMapGlobalInfo.RMapRoot, pRMapNode) == RB_FAILURE)
        {
            RBTreeDelete (pRMapNode->RMapMatchRoot);
            pRMapNode->RMapMatchRoot = NULL;
            MemReleaseMemBlock
                (gRMapGlobalInfo.RMapPoolId, (UINT1 *) (pRMapNode));
            pRMapNode = NULL;
            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                           "RBTree Add for Route Map Entry %s failed.\n",
                           pRMapNode->au1RMapName);
        }
    }
    return pRMapNode;
}

/*****************************************************************************/
/* Function     : RMapDeleteRouteMapNode                                     */
/*                                                                           */
/* Description  : This function deletes MATCH node RBTree, releases memory   */
/*                allocated for SET node in the given Route map Node and     */
/*                releases Route Map Node                                    */
/*                                                                           */
/* Input        : pRMapNode  - Route Map Node                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS if Route Map deleted successfully             */
/*                RMAP_FAILUR otherwise                                      */
/*****************************************************************************/
INT4
RMapDeleteRouteMapNode (tRMapNode * pRMapNode)
{
    /* Release the SET node memory */
    if (pRMapNode->pSetNode != NULL)
    {
        MemReleaseMemBlock (gRMapGlobalInfo.RMapSetPoolId,
                            (UINT1 *) (pRMapNode->pSetNode));
        pRMapNode->pSetNode = NULL;
    }

    /* Delete the MATCH node RBTree */
    if (pRMapNode->RMapMatchRoot != NULL)
    {
        RBTreeDelete (pRMapNode->RMapMatchRoot);
    }

    /* Remove the Route Map Node */
    if (RBTreeRemove (gRMapGlobalInfo.RMapRoot, pRMapNode) == RB_FAILURE)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Cannot delete the MAP node %s.\n",
                       pRMapNode->au1RMapName);
        return RMAP_FAILURE;
    }
    MemReleaseMemBlock (gRMapGlobalInfo.RMapPoolId, (UINT1 *) (pRMapNode));
    return RMAP_SUCCESS;
}

/*****************************************************************************/
/* Function     : RMapAddMatchNode                                           */
/*                                                                           */
/* Description  : This function allocates memory for the Route Map node,     */
/*                creates RBTree for RouteMap MATCH entry, adds the Route    */
/*                Map Node RBTree                                            */
/*                                                                           */
/* Input        : pu1RMapName - Route Map Name                               */
/*                u4SeqNo - Sequence number                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pRMapNode - Pointer to the Route Map Node, if the Route    */
/*                Map created successfully                                   */
/*                NULL - otherwise                                           */
/*****************************************************************************/
tRMapMatchNode     *
RMapAddMatchNode (tRMapNode * pRMapNode, tRMapMatchNode * pRMapMatchNode)
{
    tRMapMatchNode     *pOldMLNode = NULL;
    tMatchList         *pOldMLNext = NULL;
    tRMapMatchNode     *pNewMatchNode = NULL;
    tMatchList         *pNewMatchList = NULL;
    tMatchList         *pMatchList = NULL;
    UINT1               u1OpFailed = 0;

    if ((pNewMatchNode = (tRMapMatchNode *)
         MemAllocMemBlk (gRMapGlobalInfo.RMapMatchPoolId)) != NULL)
    {
        MEMSET (pNewMatchNode, 0, sizeof (tRMapMatchNode));
        MEMCPY (pNewMatchNode, pRMapMatchNode, sizeof (tRMapMatchNode));

/* Performance improvement part begin */
        pMatchList = &(pRMapNode->MatchCmdArray[pNewMatchNode->u1Cmd - 1]);
        pOldMLNode = pMatchList->pNode;
        pOldMLNext = pMatchList->pNext;

        /* Add new match list item to the end of list if exist or just assign values otherwise */
        if (pMatchList->pNode != NULL)
        {
            if ((pNewMatchList = (tMatchList *)
                 MemAllocMemBlk (gRMapGlobalInfo.RMapMatchListPoolId)) != NULL)
            {
                pNewMatchList->pNode = pNewMatchNode;
                pNewMatchList->pNext = NULL;
                /* Add item to corresponding match list */
                while (pMatchList->pNext != NULL)
                {
                    pMatchList = pMatchList->pNext;
                }
                pMatchList->pNext = pNewMatchList;
            }
            else
            {
                u1OpFailed = 1;
            }
        }
        else
        {
            pMatchList->pNode = pNewMatchNode;
            pMatchList->pNext = NULL;
        }
/* Performance improvement part end */

        if (RBTreeAdd (pRMapNode->RMapMatchRoot, pNewMatchNode) == RB_FAILURE)
        {
            u1OpFailed = 1;
        }

        if (u1OpFailed)
        {
            if (pNewMatchNode != NULL)
            {
                MemReleaseMemBlock (gRMapGlobalInfo.RMapMatchPoolId,
                                    (UINT1 *) (pNewMatchNode));
                pNewMatchNode = NULL;
            }

            if (pNewMatchList != NULL)
            {
                MemReleaseMemBlock (gRMapGlobalInfo.RMapMatchListPoolId,
                                    (UINT1 *) (pNewMatchList));
            }
            pMatchList->pNode = pOldMLNode;
            pMatchList->pNext = pOldMLNext;
        }
    }

    return pNewMatchNode;
}

/*****************************************************************************/
/* Function     : RMapDeleteMatchNode                                        */
/*                                                                           */
/* Description  : This function deletes MATCH node RBTree, releases memory   */
/*                allocated for SET node in the given Route map Node and     */
/*                releases Route Map Node                                    */
/*                                                                           */
/* Input        : pRMapNode  - Route Map Node                                */
/*                pRMapMatchnode - Route Map MATCH node to be deleted        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS if MATCH node deleted successfully            */
/*                RMAP_FAILUR otherwise                                      */
/*****************************************************************************/
INT4
RMapDeleteMatchNode (tRMapNode * pRMapNode, tRMapMatchNode * pRMapMatchNode)
{
    tMatchList         *pMatchList = NULL;
    if (RBTreeRemove (pRMapNode->RMapMatchRoot, pRMapMatchNode) == RB_FAILURE)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Cannot delete the MATCH node %s.\n",
                       pRMapNode->au1RMapName);

        return RMAP_FAILURE;
    }

/* Performance improvement part begin */
    /* Delete match list item from list */
    pMatchList = &(pRMapNode->MatchCmdArray[pRMapMatchNode->u1Cmd - 1]);
    if (pMatchList->pNode != NULL)
    {
        if (pMatchList->pNode == pRMapMatchNode)
        {
            if (pMatchList->pNext != NULL)
            {
                tMatchList         *pMLDelMe = pMatchList->pNext;

                pMatchList->pNode = pMLDelMe->pNode;
                pMatchList->pNext = pMLDelMe->pNext;

                MemReleaseMemBlock
                    (gRMapGlobalInfo.RMapMatchListPoolId, (UINT1 *) (pMLDelMe));
            }
            else
            {
                pMatchList->pNode = NULL;
            }
        }
        else
        {
            while (pMatchList->pNext != NULL)
            {
                if (pMatchList->pNext->pNode == pRMapMatchNode)
                {
                    tMatchList         *pMLDelMe = pMatchList->pNext;
                    pMatchList->pNext = pMLDelMe->pNext;

                    MemReleaseMemBlock
                        (gRMapGlobalInfo.RMapMatchListPoolId,
                         (UINT1 *) (pMLDelMe));
                    break;
                }
                pMatchList = pMatchList->pNext;
            }
        }
    }
/* Performance improvement part end */

    MemReleaseMemBlock
        (gRMapGlobalInfo.RMapMatchPoolId, (UINT1 *) (pRMapMatchNode));
    return RMAP_SUCCESS;
}

/*****************************************************************************/
/* Function     : RMapParamsToMatchNode                                      */
/*                                                                           */
/* Description  : Convert given parameters to MATCH-node structure           */
/*                                                                           */
/* Input        : pMatchNode  - destination MATCH node                       */
/*                ... - source parameters                                    */
/*****************************************************************************/
VOID
RMapParamsToMatchNode (tRMapMatchNode * pMatchNode,
                       UINT1 u1IsIpPrefixEntry,
                       INT4 i4DestInetType,
                       tSNMP_OCTET_STRING_TYPE * pDestInetAddress,
                       UINT4 u4DestInetPrefix,
                       INT4 i4SourceInetType,
                       tSNMP_OCTET_STRING_TYPE * pSourceInetAddress,
                       UINT4 u4SourceInetPrefix,
                       INT4 i4NextHopInetType,
                       tSNMP_OCTET_STRING_TYPE * pNextHopInetAddr,
                       INT4 i4Interface,
                       INT4 i4Metric,
                       UINT4 u4Tag,
                       INT4 i4MetricType,
                       INT4 i4RouteType,
                       UINT4 u4ASPathTag,
                       UINT4 u4Community, INT4 i4LocalPref, INT4 i4Origin)
{
    tIPvXAddr           DstXAddr, SrcXAddr, NhXAddr;

    IPVX_ADDR_CLEAR (&DstXAddr);
    IPVX_ADDR_CLEAR (&SrcXAddr);
    IPVX_ADDR_CLEAR (&NhXAddr);

    if (pDestInetAddress != NULL)
    {
        IPVX_ADDR_INIT (DstXAddr, i4DestInetType,
                        pDestInetAddress->pu1_OctetList);
    }
    if (pSourceInetAddress != NULL)
    {
        IPVX_ADDR_INIT (SrcXAddr, i4SourceInetType,
                        pSourceInetAddress->pu1_OctetList);
    }
    if (pNextHopInetAddr != NULL)
    {
        IPVX_ADDR_INIT (NhXAddr, i4NextHopInetType,
                        pNextHopInetAddr->pu1_OctetList);
    }

    MEMSET (pMatchNode, 0, sizeof (tRMapMatchNode));

    if (u1IsIpPrefixEntry == TRUE)
    {
        IPVX_ADDR_COPY (&(pMatchNode->IpPrefixA), &DstXAddr);
        pMatchNode->IpPrefixLength = u4DestInetPrefix;
        pMatchNode->u1Cmd = RMAP_MATCH_IP_PREFIX_ENTRY;
    }
    else if (TestZeroIpvxAddr (&DstXAddr) != 0)
    {
        IPVX_ADDR_COPY (&(pMatchNode->RMapDestX), &DstXAddr);
        pMatchNode->RMapDestPrefixLen = u4DestInetPrefix;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_DEST_IPX;
    }
    else if (TestZeroIpvxAddr (&SrcXAddr) != 0)
    {
        IPVX_ADDR_COPY (&(pMatchNode->RMapSourceX), &SrcXAddr);
        pMatchNode->RMapSourcePrefixLen = u4SourceInetPrefix;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_SOURCE_IPX;
    }
    else if (TestZeroIpvxAddr (&NhXAddr) != 0)
    {
        IPVX_ADDR_COPY (&(pMatchNode->RMapNextHopX), &NhXAddr);
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_NEXTHOP_IPX;
    }
    else if (i4Interface != 0)
    {
        pMatchNode->RMapIfIndex = (UINT4)i4Interface;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_INTERFACE;
    }
    else if (i4Metric != 0)
    {
        pMatchNode->RMapMetric = i4Metric;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_METRIC;
    }
    else if (u4Tag != 0)
    {
        pMatchNode->RMapTag = u4Tag;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_TAG;
    }
    else if (i4MetricType != 0)
    {
        pMatchNode->RMapMetricType =(UINT1) i4MetricType;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_METRIC_TYPE;
    }
    else if (i4RouteType != 0)
    {
        pMatchNode->RMapRouteType = (INT2)i4RouteType;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_ROUTE_TYPE;
    }
    else if (u4ASPathTag != 0)
    {
        pMatchNode->RMapNextHopAS = u4ASPathTag;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_ASPATH_TAG;
    }
    else if (u4Community != 0)
    {
        pMatchNode->RMapCommunity = u4Community;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_COMMUNITY;
    }
    else if (i4LocalPref != 0)
    {
        pMatchNode->RMapLocalPref = (UINT4) i4LocalPref;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_LOCAL_PREF;
    }
    else if (i4Origin != 0)
    {
        pMatchNode->RMapOrigin = i4Origin;
        pMatchNode->u1Cmd = RMAP_MATCH_CMD_ORIGIN;
    }
}

/*****************************************************************************/
/* Function     : RMapMatchNodeToParams                                      */
/*                                                                           */
/* Description  : Convert given MATCH-node structure to parameters           */
/*                                                                           */
/* Input        : ... - destination parameters                               */
/*                pMatchNode  - source MATCH node                            */
/*****************************************************************************/
VOID
RMapMatchNodeToParams (INT4 *pi4DestInetType,
                       tSNMP_OCTET_STRING_TYPE * pDestInetAddress,
                       UINT4 *pu4DestInetPrefix,
                       INT4 *pi4SourceInetType,
                       tSNMP_OCTET_STRING_TYPE * pSourceInetAddress,
                       UINT4 *pu4SourceInetPrefix,
                       INT4 *pi4NextHopInetType,
                       tSNMP_OCTET_STRING_TYPE * pNextHopInetAddr,
                       INT4 *pi4Interface,
                       INT4 *pi4Metric,
                       UINT4 *pu4Tag,
                       INT4 *pi4MetricType,
                       INT4 *pi4RouteType,
                       UINT4 *pu4ASPathTag,
                       UINT4 *pu4Community,
                       INT4 *pi4LocalPref,
                       INT4 *pi4Origin, tRMapMatchNode * pRMapMatchNode)
{
    /* clear all vars */
    *pi4DestInetType = 0;
    RMAP_CLEAR_OXADDR (pDestInetAddress);
    *pu4DestInetPrefix = 0;

    *pi4SourceInetType = 0;
    RMAP_CLEAR_OXADDR (pSourceInetAddress);
    *pu4SourceInetPrefix = 0;

    *pi4NextHopInetType = 0;
    RMAP_CLEAR_OXADDR (pNextHopInetAddr);

    *pi4Interface = 0;
    *pi4Metric = 0;
    *pu4Tag = 0;
    *pi4MetricType = 0;
    *pi4RouteType = 0;
    *pu4ASPathTag = 0;
    *pu4Community = 0;
    *pi4LocalPref = 0;
    *pi4Origin = 0;

    switch (pRMapMatchNode->u1Cmd)
    {
        case RMAP_MATCH_CMD_DEST_IPX:
            *pi4DestInetType = pRMapMatchNode->RMapDestT;
            MEMCPY (pDestInetAddress->pu1_OctetList,
                    pRMapMatchNode->RMapDestA, pRMapMatchNode->RMapDestL);
            pDestInetAddress->i4_Length = pRMapMatchNode->RMapDestL;

            *pu4DestInetPrefix = pRMapMatchNode->RMapDestPrefixLen;
            break;

        case RMAP_MATCH_CMD_SOURCE_IPX:
            *pi4SourceInetType = pRMapMatchNode->RMapSourceT;
            MEMCPY (pSourceInetAddress->pu1_OctetList,
                    pRMapMatchNode->RMapSourceA, pRMapMatchNode->RMapSourceL);
            pSourceInetAddress->i4_Length = pRMapMatchNode->RMapSourceL;

            *pu4SourceInetPrefix = pRMapMatchNode->RMapSourcePrefixLen;
            break;

        case RMAP_MATCH_CMD_NEXTHOP_IPX:
            *pi4NextHopInetType = pRMapMatchNode->RMapNextHopT;
            MEMCPY (pNextHopInetAddr->pu1_OctetList,
                    pRMapMatchNode->RMapNextHopA, pRMapMatchNode->RMapNextHopL);
            pNextHopInetAddr->i4_Length = pRMapMatchNode->RMapNextHopL;
            break;

        case RMAP_MATCH_CMD_INTERFACE:
            *pi4Interface = (INT4) pRMapMatchNode->RMapIfIndex;
            break;

        case RMAP_MATCH_CMD_METRIC:
            *pi4Metric = pRMapMatchNode->RMapMetric;
            break;

        case RMAP_MATCH_CMD_TAG:
            *pu4Tag = pRMapMatchNode->RMapTag;
            break;

        case RMAP_MATCH_CMD_METRIC_TYPE:
            *pi4MetricType = pRMapMatchNode->RMapMetricType;
            break;

        case RMAP_MATCH_CMD_ROUTE_TYPE:
            *pi4RouteType = pRMapMatchNode->RMapRouteType;
            break;

        case RMAP_MATCH_CMD_ASPATH_TAG:
            *pu4ASPathTag = pRMapMatchNode->RMapNextHopAS;
            break;

        case RMAP_MATCH_CMD_COMMUNITY:
            *pu4Community = pRMapMatchNode->RMapCommunity;
            break;

        case RMAP_MATCH_CMD_LOCAL_PREF:
            *pi4LocalPref = (INT4) pRMapMatchNode->RMapLocalPref;
            break;

        case RMAP_MATCH_CMD_ORIGIN:
            *pi4Origin = pRMapMatchNode->RMapOrigin;
            break;

        case RMAP_MATCH_IP_PREFIX_ENTRY:
            *pu4DestInetPrefix = pRMapMatchNode->IpPrefixLength;
            *pi4DestInetType = pRMapMatchNode->IpPrefixAddrType;
            MEMCPY (pDestInetAddress->pu1_OctetList,
                    pRMapMatchNode->IpPrefixAddr,
                    pRMapMatchNode->IpPrefixAddrLen);
            pDestInetAddress->i4_Length = pRMapMatchNode->IpPrefixAddrLen;
            break;
        default:
            /*should not come here*/
            break;
    }
}

/*****************************************************************************/
/* Function     : RMapGetMapStatus                                           */
/*                                                                           */
/* Description  : Collect statuses of MAP-nodes, one bit per node            */
/*                                                                           */
/* Input        : pRMapName - Pointer to RouteMap Name                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 0 - all MAP-nodes are not ACTIVE or not exist              */
/*                        route map should NOT be applied                    */
/*                other - at least one MAP-node exists and ACTIVE,           */
/*                        route map should be applied                        */
/*****************************************************************************/
UINT4
RMapGetMapStatus (UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 1;
    UINT4               u4Status = 0;

    if (pu1RMapName == NULL)
    {
        return 0;
    }

    /* as this function is invoked from nmh-, r/w lock is not used */
    for (; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* get MAP node */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            u4Status |= (UINT4) (1 << (u4SeqNo - 1));
        }
    }

    return u4Status;
}

INT1
CmpFilterRMapName (tFilteringRMap * pFilterRMap,
                   tSNMP_OCTET_STRING_TYPE * pRMapName)
{
    if (pFilterRMap != NULL
        && pRMapName != NULL
        && (UINT4) pRMapName->i4_Length ==
        STRLEN (pFilterRMap->au1DistInOutFilterRMapName))
    {
        return STRNCMP (pFilterRMap->au1DistInOutFilterRMapName,
                        pRMapName->pu1_OctetList, pRMapName->i4_Length);
    }
    return 1;
}

tFilteringRMap     *
GetMinFilterRMap (tFilteringRMap * pFilterRMap1, INT1 i1Type1,
                  tFilteringRMap * pFilterRMap2, INT1 i1Type2)
{
    if (pFilterRMap1 == NULL && pFilterRMap2 != NULL)
    {
        return pFilterRMap2;
    }

    if (pFilterRMap1 != NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (pFilterRMap1 == NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) >
        STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap2;
    }
    else if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) <
             STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap1;
    }
    else
    {
        INT4                i4RetVal =
            MEMCMP (pFilterRMap1->au1DistInOutFilterRMapName,
                    pFilterRMap2->au1DistInOutFilterRMapName,
                    STRLEN (pFilterRMap1->au1DistInOutFilterRMapName));
        if (i4RetVal < 0)
        {
            return pFilterRMap1;
        }
        else if (i4RetVal > 0)
        {
            return pFilterRMap2;
        }
    }

    if (i1Type1 > i1Type2)
    {
        return pFilterRMap2;
    }
    return pFilterRMap1;
}

/*****************************************************************************/
/* Function     : RMapUtilIsSameIpPrefixExist                                */
/*                                                                           */
/* Description  : This function is to check whether the configured IP Prefix */
/*                entry is already configured for other address family       */
/*                                                                           */
/* Input        : pIpPrefixNameOct - Pointer to Ip Prefix list Name          */
/*                 u1Afi - Address family of neighbor                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Sequence number if entry present else ZERO                 */
/*****************************************************************************/
UINT4
RMapUtilIsSameIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,  UINT1 u1Afi )    
{
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4SeqNo = 1;
    UINT4               u4SeqNoTemp = 0;

    RMAP_OCT_2_ASCIIZ (au1Name, pIpPrefixNameOct);
  

      for (; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
     {
        /* get MAP node */
        pRMapNode = RMapGetRouteMapNode (au1Name, u4SeqNo);
        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            u4SeqNoTemp = pRMapNode->u4SeqNo;
        }
    

        if (pRMapNode == NULL)
        {
            continue;
        }

        u4SeqNoTemp = pRMapNode->u4SeqNo;

        if (pRMapNode->u1IsIpPrefixInfo != TRUE)
        {
            continue;
        }
        pRMapMatchNode =
            pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;

        if (pRMapMatchNode == NULL)
        {
            continue;
        }
        if (pRMapMatchNode->u1Cmd != RMAP_MATCH_IP_PREFIX_ENTRY)
        {
            continue;
        }
        if (pRMapMatchNode->IpPrefixAddrType != u1Afi)
        {
            return u4SeqNoTemp;
        }
    }
    return RMAP_ZERO;
}

/*****************************************************************************/
/* Function     : RMapUtilIsIpPrefixExist                                    */
/*                                                                           */
/* Description  : This function is to check whether the configured IP Prefix */
/*                entry is already present or not                            */
/*                                                                           */
/* Input        : pIpPrefixNameOct - Pointer to Ip Prefix list Name          */
/*                pIpPrefixInfo    - Pointer to the IP Prefix inifo          */
/*                u1Acess          - PERMIT/DENY                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Sequence number if entry present else ZERO                 */
/*****************************************************************************/
UINT4
RMapUtilIsIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,
                         tIpPrefixInfo * pIpPrefixInfo, UINT1 u1Access)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4SeqNo = 0;

    RMAP_OCT_2_ASCIIZ (au1Name, pIpPrefixNameOct);

    while (1)
    {
        pRMapNode = RMapGetNextRouteMapNode (au1Name, u4SeqNo);

        if (pRMapNode == NULL)
        {
            break;
        }

        if (STRCMP (pRMapNode->au1RMapName, au1Name) != 0)
        {
            break;
        }
        u4SeqNo = pRMapNode->u4SeqNo;

        if (pRMapNode->u1IsIpPrefixInfo != TRUE)
        {
            continue;
        }
        pRMapMatchNode =
            pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;

        if (pRMapMatchNode == NULL)
        {
            continue;
        }
        if (pRMapMatchNode->u1Cmd != RMAP_MATCH_IP_PREFIX_ENTRY)
        {
            continue;
        }
        if (pRMapMatchNode->IpPrefixAddrType !=
            pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi)
        {
            continue;
        }
        if (MEMCMP
            (pRMapMatchNode->IpPrefixAddr,
             pIpPrefixInfo->AddrInfo.InetXAddr.au1Addr,
             pIpPrefixInfo->AddrInfo.InetXAddr.u1AddrLen) != 0)
        {
            continue;
        }
        if (pRMapMatchNode->IpPrefixLength !=
            pIpPrefixInfo->AddrInfo.u4PrefixLen)
        {
            continue;
        }
        if (pRMapMatchNode->IpPrefixMinLen != pIpPrefixInfo->u1MinPrefixLen)
        {
            continue;
        }
        if (pRMapMatchNode->IpPrefixMaxLen != pIpPrefixInfo->u1MaxPrefixLen)
        {
            continue;
        }
        if (pRMapNode->u1Access != u1Access)
        {
            continue;
        }
        return u4SeqNo;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name : MatchIpvxAddrWithPrefix                                   */
/* Description   : compare "prefix length" bits of an address                */
/* Input(s)      : pIpAddr1                                                  */
/*                 pIpAddr2                                                  */
/*                 u4PrefixLen - Prefix length                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : FALSE - Prefix Does not Match                             */
/*                 TRUE  - Prefix Matches                                    */
/*****************************************************************************/
INT4
MatchIpvxAddrWithPrefix (tIPvXAddr * pIpAddr1, tIPvXAddr * pIpAddr2,
                         UINT4 u4PrefixLen)
{
    UINT4               u4Mask = 0;
    UINT4               u4Addr1 = 0;
    UINT4               u4Addr2 = 0;
#ifdef IP6_WANTED
    tIp6Addr            Ip6Addr1;
    tIp6Addr            Ip6Addr2;

    MEMSET (&Ip6Addr1, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6Addr2, 0, sizeof (tIp6Addr));
#endif

    if (pIpAddr1->u1Afi != pIpAddr2->u1Afi)
    {
        return FALSE;
    }
    switch (pIpAddr1->u1Afi)
    {
        case IPVX_ADDR_FMLY_IPV4:
        {
            MEMCPY (&u4Addr1, pIpAddr1->au1Addr, IPVX_IPV4_ADDR_LEN);
            MEMCPY (&u4Addr2, pIpAddr2->au1Addr, IPVX_IPV4_ADDR_LEN);

            if ((u4PrefixLen == 0) || ((u4Addr1 == 0) && (u4Addr2 == 0)))
            {
                return TRUE;
            }

            IPV4_MASKLEN_TO_MASK (u4Mask, u4PrefixLen);
            /* Used to compare Route Prefixes */
            if ((u4Addr1 ^ u4Addr2) & (OSIX_HTONL (u4Mask)))
            {
                return FALSE;
            }
            break;
        }
#ifdef IP6_WANTED
        case IPVX_ADDR_FMLY_IPV6:
        {
            MEMCPY (&Ip6Addr1.u1_addr[0], pIpAddr1->au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            MEMCPY (&Ip6Addr2.u1_addr[0], pIpAddr2->au1Addr,
                    IPVX_IPV6_ADDR_LEN);

            if (Ip6AddrMatch (&Ip6Addr1, &Ip6Addr2, (INT4) u4PrefixLen) != TRUE)
            {
                return FALSE;
            }
            break;
        }
#endif
        default:
            return FALSE;
    }

    return TRUE;
}


/*****************************************************************************/
/* Function Name : RMapIpIsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (AddrPrefix1, AddrPrefix2)                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
RMapIpIsOnSameSubnet (tIPvXAddr * pIncomingRtAddr,
                        tIPvXAddr * pRMapAddr,
                        UINT4 u4IncomingRtPrefixLen,
                        UINT4 u4RMapPrefixLen
                        )
{

    if (0 == CmpIpvxAddrWithPrefix (pRMapAddr,
                            u4RMapPrefixLen,
                            pIncomingRtAddr,
                            u4IncomingRtPrefixLen))
    {
            return TRUE;
    }

    return FALSE;
}
/*****************************************************************************/
/* Function Name : RmapCliParseCommunityStringAsNum                          */
/* Description   : This routine validates the given AS no. string and        */
/*                 converts it to an AS number in decimal format             */
/* Input(s)      : *pAsNoinpToken - Pointer to the AS number string          */
/* Output(s)     : pu4AsNo - Generated AS Number                             */
/* Return(s)     : RMAP_SUCCESS/RMAP_FAILURE                                 */
/*****************************************************************************/
INT4 
RmapCliParseCommunityStringAsNum (CHR1 *pAsNoinpToken, UINT4 *pu4AsNo)
{
    UINT4               u4DigitOne, u4DigitTwo;
    INT4                i4Index = 0;
    INT1                i1ColonFound = RMAP_SUCCESS;
    INT1                i1DigitOne[RMAP_MAX_AS_LEN_WITH_COLON],
                        i1DigitTwo[RMAP_MAX_AS_DIGITS_LEN_IN_STR];
    *pu4AsNo = 0;
    MEMSET (i1DigitOne, 0, sizeof (i1DigitOne));
    MEMSET (i1DigitTwo, 0, sizeof (i1DigitTwo));
    /* If the AS string is in A:B (asdot) format - DigitOne = A, DigitTwo = B
     * If the AS string is in asplain format - DigitOne = A */
    while (*pAsNoinpToken != '\0')
    {
        if (isdigit (*pAsNoinpToken))
            {
            if (i1ColonFound == 0)
                {
                    MEMCPY (i1DigitOne + i4Index, pAsNoinpToken, sizeof (UINT1));
                     if (i1DigitOne[0] == '0')
                        {
                            return  RMAP_FAILURE;
                        }
                        else if (i4Index == RMAP_MAX_AS_LEN)
                         {
                             break;
                         }
                }
                else
                {
                    MEMCPY (i1DigitTwo + i4Index, pAsNoinpToken, sizeof (UINT1));
                     if (i1DigitTwo[0] == '0')
                        {
                            return  RMAP_FAILURE;
                        }
                        else if (i4Index == RMAP_MAX_AS_DIGITS_LEN)
                            {
                                break;
                            }
                }
                i4Index++;
                pAsNoinpToken++;
                if ((*pAsNoinpToken == ':') && (i1ColonFound == 0))
                {
                    if (*(pAsNoinpToken + 1) == '\0')
                        break;
                        i1ColonFound = 1;
                        i4Index = 0;
                        pAsNoinpToken++;
                }
            }
            else
            {
                return  RMAP_FAILURE;
            }
    }
    /* Computing the AS number */
    if (*pAsNoinpToken == '\0')
        {
        if (i1ColonFound == 1)
            {
                u4DigitOne = (UINT4) ATOI (i1DigitOne);
                u4DigitTwo = (UINT4) ATOI (i1DigitTwo);
                if ((u4DigitOne <= RMAP_MAX_TWO_BYTE_AS) &&
                    (u4DigitTwo <= RMAP_MAX_TWO_BYTE_AS))
                    {
                        *pu4AsNo = (u4DigitOne * (RMAP_MAX_TWO_BYTE_AS + 1)) +
                                    u4DigitTwo;
                    }
            } 
            else
                {
                if ((STRLEN (i1DigitOne) >= RMAP_MAX_AS_LEN) &&
                    (STRCMP (i1DigitOne, "4294967295") > 0))
                    {
                        return  RMAP_FAILURE;
                    }
                *pu4AsNo = (UINT4) ATOI (i1DigitOne);
                }
        }
    return  RMAP_SUCCESS;
 }

VOID
PrefixListDefaultRouteHandling(tIPvXAddr InetXAddr,
                               tRtMapInfo *pInputRtInfo,
                               tRMapMatchNode *pTempRMapMatchNode,
                               tRMapNode *pTempRMapNode,
                               INT1 *pi1MatchFound)
{
         tIPvXAddr          ZeroInetXAddr;
         IPVX_ADDR_CLEAR(&ZeroInetXAddr);

        /*Exact match, Min/Max not configured scenario*/
        if ((pTempRMapMatchNode->IpPrefixMinLen == 0) &&
                        (pTempRMapMatchNode->IpPrefixMaxLen == 0))
    {
                if((MEMCMP(InetXAddr.au1Addr,ZeroInetXAddr.au1Addr,InetXAddr.u1AddrLen) == 0)
                       &&((pTempRMapMatchNode->IpPrefixLength == 32)
                          ||(pTempRMapMatchNode->IpPrefixLength == 128)))
                {
                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                        {
                                *pi1MatchFound = PERMIT_PREFIX;
    }
                        else
    {
                                *pi1MatchFound = DENY_PREFIX;
                        }
                }

                /*default prefix with 32(ipv4) and 128(ipv6) mask - default prefix alone
                 * will be considered for
                 * deny and permit*/
                if((MEMCMP(ZeroInetXAddr.au1Addr,InetXAddr.au1Addr,InetXAddr.u1AddrLen)
                                        == 0)&&
                                (pTempRMapMatchNode->IpPrefixLength == pInputRtInfo->u2DstPrefixLen))
                {
                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                        {                                                                                                                                  *pi1MatchFound = PERMIT_PREFIX;
                        }
                        else
                        {
                                *pi1MatchFound = DENY_PREFIX;
                        }
                }

        }
        else
        {
                /*default prefix "LESS THAN/GREAT THAN" with 32/128 mask - it
                 * should consider all the prefixes for
                 * deny and permit*/
                if((MEMCMP(InetXAddr.au1Addr,ZeroInetXAddr.au1Addr,InetXAddr.u1AddrLen)
                                        == 0)                                                                                                        &&((pInputRtInfo->u2DstPrefixLen >=
                                                        pTempRMapMatchNode->IpPrefixMinLen)
                                                ||(pInputRtInfo->u2DstPrefixLen <=
                                                        pTempRMapMatchNode->IpPrefixMaxLen)))
                {
                       /*Greater than/Less than Both configured*/
                        if((pTempRMapMatchNode->IpPrefixMinLen != 0)                                                                                       &&(pTempRMapMatchNode->IpPrefixMaxLen != 0))
                        {
                                if((pInputRtInfo->u2DstPrefixLen
                                                        >=  pTempRMapMatchNode->IpPrefixMinLen)
                                                &&(pInputRtInfo->u2DstPrefixLen
                                                        <=  pTempRMapMatchNode->IpPrefixMaxLen))
                                {
                                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                                        {                                                                                                                                      *pi1MatchFound = PERMIT_PREFIX;
                                        }                                                                                                                              else
                                        {
                                                *pi1MatchFound = DENY_PREFIX;
                                        }
                                }
                        }
                        /*Less than alone configured*/ 
                         if((pTempRMapMatchNode->IpPrefixMinLen != 0)                                                                                       &&(pTempRMapMatchNode->IpPrefixMaxLen == 0))
                         {
                                if(pInputRtInfo->u2DstPrefixLen
                                                        >=  pTempRMapMatchNode->IpPrefixMinLen)
                                {
                                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                                        {                                                                                                                                      *pi1MatchFound = PERMIT_PREFIX;
                                        }                                                                                                                              else
                                        {
                                                *pi1MatchFound = DENY_PREFIX;
                                        }
                                }
                         }
                      /*greater than alone configured*/
                         if((pTempRMapMatchNode->IpPrefixMinLen == 0)                                                                                       &&(pTempRMapMatchNode->IpPrefixMaxLen != 0))
                         {
                                if(pInputRtInfo->u2DstPrefixLen
                                                        <=  pTempRMapMatchNode->IpPrefixMaxLen)
                                {
                                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                                        {                                                                                                                                      *pi1MatchFound = PERMIT_PREFIX;
                                        }                                                                                                                              else
                                        {
                                                *pi1MatchFound = DENY_PREFIX;
                                        }
                                }
                         }


                }
    }

}


