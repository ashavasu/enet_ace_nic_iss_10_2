/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmaplw.c,v 1.27 2016/12/27 12:36:00 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "rmapinc.h"
#include  "fssnmp.h"
#include "rmgr.h"
#include "cfacli.h"
extern UINT4        gu4RMapGlobalTrc;
extern UINT4        gu4RMapGlobalDbg;
extern tRMapGlobalInfo gRMapGlobalInfo;
extern UINT4        FsRMapMatchRowStatus[12];
extern UINT4        FsRMapMatchDestMaxPrefixLen[12];
extern UINT4        FsRMapMatchDestMinPrefixLen[12];

#define CONF_COMM_STD 0
/* LOW LEVEL Routines for Table : FsRMapTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRMapTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsRouteMapName,
                                         UINT4 u4FsRouteMapSeqNum)
{
    /* convert params if need and call appropriate RMap function */
    return nmhValidateIndexInstanceFsRMapTable (pFsRouteMapName,
                                                u4FsRouteMapSeqNum);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRouteMapTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRouteMapTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 *pu4FsRouteMapSeqNum)
{
    /* convert params if need and call appropriate RMap function */
    return (nmhGetFirstIndexFsRMapTable (pFsRouteMapName, pu4FsRouteMapSeqNum));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRouteMapTable
 Input       :  The Indices
                FsRouteMapName
                nextFsRouteMapName
                FsRouteMapSeqNum
                nextFsRouteMapSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRouteMapTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                tSNMP_OCTET_STRING_TYPE * pNextFsRouteMapName,
                                UINT4 u4FsRouteMapSeqNum,
                                UINT4 *pu4NextFsRouteMapSeqNum)
{
    /* convert params if need and call appropriate RMap function */
    return nmhGetNextIndexFsRMapTable (pFsRouteMapName, pNextFsRouteMapName,
                                       u4FsRouteMapSeqNum,
                                       pu4NextFsRouteMapSeqNum);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRouteMapAccess
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapAccess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapAccess (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                        UINT4 u4FsRouteMapSeqNum,
                        INT4 *pi4RetValFsRouteMapAccess)
{
    /* convert params if need and call appropriate RMap function */
    return nmhGetFsRMapAccess (pFsRouteMapName, u4FsRouteMapSeqNum,
                               pi4RetValFsRouteMapAccess);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 *pi4RetValFsRouteMapRowStatus)
{
    /* convert params if need and call appropriate RMap function */
    return nmhGetFsRMapRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  pi4RetValFsRouteMapRowStatus);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRouteMapAccess
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapAccess
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapAccess (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                        UINT4 u4FsRouteMapSeqNum, INT4 i4SetValFsRouteMapAccess)
{
    return nmhSetFsRMapAccess (pFsRouteMapName, u4FsRouteMapSeqNum,
                               i4SetValFsRouteMapAccess);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 i4SetValFsRouteMapRowStatus)
{
    return nmhSetFsRMapRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  i4SetValFsRouteMapRowStatus);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapAccess
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapAccess
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapAccess (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 i4TestValFsRouteMapAccess)
{
    return nmhTestv2FsRMapAccess (pu4ErrorCode, pFsRouteMapName,
                                  u4FsRouteMapSeqNum,
                                  i4TestValFsRouteMapAccess);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapRowStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 i4TestValFsRouteMapRowStatus)
{
    return nmhTestv2FsRMapRowStatus (pu4ErrorCode, pFsRouteMapName,
                                     u4FsRouteMapSeqNum,
                                     i4TestValFsRouteMapRowStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRouteMapTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRouteMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRouteMapMatchTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRouteMapMatchTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRouteMapMatchTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsRouteMapName,
                                              UINT4 u4FsRouteMapSeqNum,
                                              INT4 i4FsRouteMapMatchInterface,
                                              UINT4 u4FsRouteMapMatchIpAddress,
                                              UINT4 u4FsRouteMapMatchIpAddrMask,
                                              UINT4 u4FsRouteMapMatchIpNextHop,
                                              INT4 i4FsRouteMapMatchMetric,
                                              UINT4 u4FsRouteMapMatchTag,
                                              INT4 i4FsRouteMapMatchRouteType,
                                              INT4 i4FsRouteMapMatchMetricType,
                                              UINT4 u4FsRouteMapMatchASPathTag,
                                              UINT4 u4FsRouteMapMatchCommunity,
                                              INT4 i4FsRouteMapMatchOrigin,
                                              INT4
                                              i4FsRouteMapMatchLocalPreference)
{
    UINT4               u4IpAddr;
    INT1                i1RetVal;
    UINT4               u4FsRMapMatchSourceInetPrefix;
    UINT4               u4FsRMapMatchDestInetPrefix;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpAddress);
    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpNextHop);
    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchNextHopInetAddress.i4_Length);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchDestInetPrefix,
                          u4FsRouteMapMatchIpAddrMask);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchSourceInetPrefix, 0);

    i1RetVal = nmhValidateIndexInstanceFsRMapMatchTable (pFsRouteMapName, u4FsRouteMapSeqNum, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchDestInetType */
                                                         &fsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix, 0,    /* i4FsRMapMatchSourceInetType */
                                                         &fsRMapMatchSrcInetAddress, u4FsRMapMatchSourceInetPrefix, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchNextHopInetType */
                                                         &fsRMapMatchNextHopInetAddress,
                                                         i4FsRouteMapMatchInterface,
                                                         i4FsRouteMapMatchMetric,
                                                         u4FsRouteMapMatchTag,
                                                         i4FsRouteMapMatchMetricType,
                                                         i4FsRouteMapMatchRouteType,
                                                         u4FsRouteMapMatchASPathTag,
                                                         u4FsRouteMapMatchCommunity,
                                                         i4FsRouteMapMatchLocalPreference,
                                                         i4FsRouteMapMatchOrigin);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRouteMapMatchTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRouteMapMatchTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                      UINT4 *pu4FsRouteMapSeqNum,
                                      INT4 *pi4FsRouteMapMatchInterface,
                                      UINT4 *pu4FsRouteMapMatchIpAddress,
                                      UINT4 *pu4FsRouteMapMatchIpAddrMask,
                                      UINT4 *pu4FsRouteMapMatchIpNextHop,
                                      INT4 *pi4FsRouteMapMatchMetric,
                                      UINT4 *pu4FsRouteMapMatchTag,
                                      INT4 *pi4FsRouteMapMatchRouteType,
                                      INT4 *pi4FsRouteMapMatchMetricType,
                                      UINT4 *pu4FsRouteMapMatchASPathTag,
                                      UINT4 *pu4FsRouteMapMatchCommunity,
                                      INT4 *pi4FsRouteMapMatchOrigin,
                                      INT4 *pi4FsRouteMapMatchLocalPreference)
{
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];

    UINT4               u4NextFsRMapMatchDestInetPrefix;
    UINT4               u4NextFsRMapMatchSourceInetPrefix;
    INT4                i4NextFsRMapMatchDestInetType;
    INT4                i4NextFsRMapMatchSourceInetType;
    INT4                i4NextFsRMapMatchNextHopInetType;
    UINT4               u4NextFsRouteMapSeqNum;
    INT4                i4NextFsRouteMapMatchInterface;
    INT4                i4NextFsRouteMapMatchMetric;
    UINT4               u4NextFsRouteMapMatchTag;
    INT4                i4NextFsRouteMapMatchMetricType;
    INT4                i4NextFsRouteMapMatchRouteType;
    UINT4               u4NextFsRouteMapMatchASPathTag;
    UINT4               u4NextFsRouteMapMatchCommunity;
    INT4                i4NextFsRouteMapMatchLocalPreference;
    INT4                i4NextFsRouteMapMatchOrigin;
    UINT4               u4FsRMapMatchDestInetPrefix;
    UINT4               u4FsRMapMatchSourceInetPrefix;
    INT1                i1RetVal;
    INT4                i4FsRMapMatchDestInetType = 0;
    INT4                i4FsRMapMatchSourceInetType = 0;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE nextFsRouteMapName;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4FsRMapMatchNextHopInetType = 0;
    tSNMP_OCTET_STRING_TYPE *pNextFsRouteMapName = &nextFsRouteMapName;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddressNext;
    UINT1               au1SrcInetAddrNext[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddressNext;
    UINT1               au1DestInetAddrNext[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddressNext;
    UINT1               au1NextHopInetAddrNext[IPVX_IPV6_ADDR_LEN];
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchDestInetAddress.pu1_OctetList, 0,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;
    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = 0;
    MEMSET (fsRMapMatchNextHopInetAddress.pu1_OctetList, 0,
            fsRMapMatchNextHopInetAddress.i4_Length);

    i1RetVal =
        nmhGetFirstIndexFsRMapMatchTable (pFsRouteMapName,
                                          pu4FsRouteMapSeqNum,
                                          &i4FsRMapMatchDestInetType,
                                          &fsRMapMatchDestInetAddress,
                                          &u4FsRMapMatchDestInetPrefix,
                                          &i4FsRMapMatchSourceInetType,
                                          &fsRMapMatchSrcInetAddress,
                                          &u4FsRMapMatchSourceInetPrefix,
                                          &i4FsRMapMatchNextHopInetType,
                                          &fsRMapMatchNextHopInetAddress,
                                          pi4FsRouteMapMatchInterface,
                                          pi4FsRouteMapMatchMetric,
                                          pu4FsRouteMapMatchTag,
                                          pi4FsRouteMapMatchMetricType,
                                          pi4FsRouteMapMatchRouteType,
                                          pu4FsRouteMapMatchASPathTag,
                                          pu4FsRouteMapMatchCommunity,
                                          pi4FsRouteMapMatchLocalPreference,
                                          pi4FsRouteMapMatchOrigin);
    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4FsRMapMatchDestInetType != IPVX_ADDR_FMLY_IPV6
            && fsRMapMatchSrcInetAddress.i4_Length == 0
            && i4FsRMapMatchNextHopInetType != IPVX_ADDR_FMLY_IPV6)
        {
            *pu4FsRouteMapMatchIpAddress =
                /*OSIX_NTOHL */
                (*(UINT4 *) (VOID *)
                 (fsRMapMatchDestInetAddress.pu1_OctetList));
            IPV4_MASKLEN_TO_MASK (*pu4FsRouteMapMatchIpAddrMask,
                                  u4FsRMapMatchDestInetPrefix);
            *pu4FsRouteMapMatchIpNextHop =
                /*OSIX_NTOHL */
                (*(UINT4 *) (VOID *)
                 (fsRMapMatchNextHopInetAddress.pu1_OctetList));
            return SNMP_SUCCESS;
        }

        /*Next */
        nextFsRouteMapName.pu1_OctetList = au1NextRMapName;

        pNextFsRouteMapName->i4_Length = 0;
        MEMSET (pNextFsRouteMapName->pu1_OctetList, 0, RMAP_MAX_NAME_LEN + 4);

        fsRMapMatchDestInetAddressNext.pu1_OctetList = au1DestInetAddrNext;

        fsRMapMatchSrcInetAddressNext.pu1_OctetList = au1SrcInetAddrNext;

        fsRMapMatchNextHopInetAddressNext.pu1_OctetList =
            au1NextHopInetAddrNext;

        while (i1RetVal != SNMP_FAILURE)
        {
            i1RetVal =
                nmhGetNextIndexFsRMapMatchTable (pFsRouteMapName,
                                                 pNextFsRouteMapName,
                                                 *pu4FsRouteMapSeqNum,
                                                 &u4NextFsRouteMapSeqNum,
                                                 i4FsRMapMatchDestInetType,
                                                 &i4NextFsRMapMatchDestInetType,
                                                 &fsRMapMatchDestInetAddress,
                                                 &fsRMapMatchDestInetAddressNext,
                                                 u4FsRMapMatchDestInetPrefix,
                                                 &u4NextFsRMapMatchDestInetPrefix,
                                                 i4FsRMapMatchSourceInetType,
                                                 &i4NextFsRMapMatchSourceInetType,
                                                 &fsRMapMatchSrcInetAddress,
                                                 &fsRMapMatchSrcInetAddressNext,
                                                 u4FsRMapMatchSourceInetPrefix,
                                                 &u4NextFsRMapMatchSourceInetPrefix,
                                                 i4FsRMapMatchNextHopInetType,
                                                 &i4NextFsRMapMatchNextHopInetType,
                                                 &fsRMapMatchNextHopInetAddress,
                                                 &fsRMapMatchNextHopInetAddressNext,
                                                 *pi4FsRouteMapMatchInterface,
                                                 &i4NextFsRouteMapMatchInterface,
                                                 *pi4FsRouteMapMatchMetric,
                                                 &i4NextFsRouteMapMatchMetric,
                                                 *pu4FsRouteMapMatchTag,
                                                 &u4NextFsRouteMapMatchTag,
                                                 *pi4FsRouteMapMatchMetricType,
                                                 &i4NextFsRouteMapMatchMetricType,
                                                 *pi4FsRouteMapMatchRouteType,
                                                 &i4NextFsRouteMapMatchRouteType,
                                                 *pu4FsRouteMapMatchASPathTag,
                                                 &u4NextFsRouteMapMatchASPathTag,
                                                 *pu4FsRouteMapMatchCommunity,
                                                 &u4NextFsRouteMapMatchCommunity,
                                                 *pi4FsRouteMapMatchLocalPreference,
                                                 &i4NextFsRouteMapMatchLocalPreference,
                                                 *pi4FsRouteMapMatchOrigin,
                                                 &i4NextFsRouteMapMatchOrigin);

            if (i1RetVal == SNMP_SUCCESS)
            {
                if (i4NextFsRMapMatchDestInetType != IPVX_ADDR_FMLY_IPV6
                    && fsRMapMatchSrcInetAddressNext.i4_Length == 0
                    && i4NextFsRMapMatchNextHopInetType != IPVX_ADDR_FMLY_IPV6)
                {
                    *pu4FsRouteMapMatchIpAddress =
                        /*OSIX_NTOHL */
                        (*(UINT4
                           *) (VOID *)
                         (fsRMapMatchDestInetAddressNext.pu1_OctetList));
                    IPV4_MASKLEN_TO_MASK (*pu4FsRouteMapMatchIpAddrMask,
                                          u4NextFsRMapMatchDestInetPrefix);
                    *pu4FsRouteMapMatchIpNextHop =
                        /*OSIX_NTOHL */
                        (*(UINT4
                           *) (VOID *)
                         (fsRMapMatchNextHopInetAddressNext.pu1_OctetList));
                    return SNMP_SUCCESS;
                }
                else
                {
                    pFsRouteMapName->i4_Length = pNextFsRouteMapName->i4_Length;
                    MEMCPY (pFsRouteMapName->pu1_OctetList,
                            pNextFsRouteMapName->pu1_OctetList,
                            pNextFsRouteMapName->i4_Length);
                    pNextFsRouteMapName->i4_Length = 0;
                    MEMSET (pNextFsRouteMapName->pu1_OctetList, 0,
                            RMAP_MAX_NAME_LEN + 4);

                    *pu4FsRouteMapSeqNum = u4NextFsRouteMapSeqNum;
                    i4FsRMapMatchDestInetType = i4NextFsRMapMatchDestInetType;

                    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList,
                            fsRMapMatchDestInetAddressNext.pu1_OctetList,
                            fsRMapMatchDestInetAddressNext.i4_Length);
                    fsRMapMatchDestInetAddress.i4_Length =
                        fsRMapMatchDestInetAddressNext.i4_Length;

                    u4FsRMapMatchDestInetPrefix =
                        u4NextFsRMapMatchDestInetPrefix;
                    i4FsRMapMatchSourceInetType =
                        i4NextFsRMapMatchSourceInetType;

                    MEMCPY (fsRMapMatchSrcInetAddress.pu1_OctetList,
                            fsRMapMatchSrcInetAddressNext.pu1_OctetList,
                            fsRMapMatchSrcInetAddressNext.i4_Length);
                    fsRMapMatchSrcInetAddress.i4_Length =
                        fsRMapMatchSrcInetAddressNext.i4_Length;

                    u4FsRMapMatchSourceInetPrefix =
                        u4NextFsRMapMatchSourceInetPrefix;
                    i4FsRMapMatchNextHopInetType =
                        i4NextFsRMapMatchNextHopInetType;

                    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList,
                            fsRMapMatchNextHopInetAddressNext.pu1_OctetList,
                            fsRMapMatchNextHopInetAddressNext.i4_Length);
                    fsRMapMatchNextHopInetAddress.i4_Length =
                        fsRMapMatchNextHopInetAddressNext.i4_Length;

                    *pi4FsRouteMapMatchInterface =
                        i4NextFsRouteMapMatchInterface;
                    *pi4FsRouteMapMatchMetric = i4NextFsRouteMapMatchMetric;
                    *pu4FsRouteMapMatchTag = u4NextFsRouteMapMatchTag;
                    *pi4FsRouteMapMatchMetricType =
                        i4NextFsRouteMapMatchMetricType;
                    *pi4FsRouteMapMatchRouteType =
                        i4NextFsRouteMapMatchRouteType;
                    *pu4FsRouteMapMatchASPathTag =
                        u4NextFsRouteMapMatchASPathTag;
                    *pu4FsRouteMapMatchCommunity =
                        u4NextFsRouteMapMatchCommunity;
                    *pi4FsRouteMapMatchLocalPreference =
                        i4NextFsRouteMapMatchLocalPreference;
                    *pi4FsRouteMapMatchOrigin = i4NextFsRouteMapMatchOrigin;
                }
            }
        }
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRouteMapMatchTable
 Input       :  The Indices
                FsRouteMapName
                nextFsRouteMapName
                FsRouteMapSeqNum
                nextFsRouteMapSeqNum
                FsRouteMapMatchInterface
                nextFsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                nextFsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                nextFsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                nextFsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                nextFsRouteMapMatchMetric
                FsRouteMapMatchTag
                nextFsRouteMapMatchTag
                FsRouteMapMatchRouteType
                nextFsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                nextFsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                nextFsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                nextFsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                nextFsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference
                nextFsRouteMapMatchLocalPreference
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRouteMapMatchTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsRouteMapName,
                                     UINT4 u4FsRouteMapSeqNum,
                                     UINT4 *pu4NextFsRouteMapSeqNum,
                                     INT4 i4FsRouteMapMatchInterface,
                                     INT4 *pi4NextFsRouteMapMatchInterface,
                                     UINT4 u4FsRouteMapMatchIpAddress,
                                     UINT4 *pu4NextFsRouteMapMatchIpAddress,
                                     UINT4 u4FsRouteMapMatchIpAddrMask,
                                     UINT4 *pu4NextFsRouteMapMatchIpAddrMask,
                                     UINT4 u4FsRouteMapMatchIpNextHop,
                                     UINT4 *pu4NextFsRouteMapMatchIpNextHop,
                                     INT4 i4FsRouteMapMatchMetric,
                                     INT4 *pi4NextFsRouteMapMatchMetric,
                                     UINT4 u4FsRouteMapMatchTag,
                                     UINT4 *pu4NextFsRouteMapMatchTag,
                                     INT4 i4FsRouteMapMatchRouteType,
                                     INT4 *pi4NextFsRouteMapMatchRouteType,
                                     INT4 i4FsRouteMapMatchMetricType,
                                     INT4 *pi4NextFsRouteMapMatchMetricType,
                                     UINT4 u4FsRouteMapMatchASPathTag,
                                     UINT4 *pu4NextFsRouteMapMatchASPathTag,
                                     UINT4 u4FsRouteMapMatchCommunity,
                                     UINT4 *pu4NextFsRouteMapMatchCommunity,
                                     INT4 i4FsRouteMapMatchOrigin,
                                     INT4 *pi4NextFsRouteMapMatchOrigin,
                                     INT4 i4FsRouteMapMatchLocalPreference,
                                     INT4
                                     *pi4NextFsRouteMapMatchLocalPreference)
{
    UINT4               u4IpAddr;

    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddressNext;
    UINT1               au4SrcInetAddrNext[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddressNext;
    UINT1               au4DestInetAddrNext[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;

    UINT4               u4FsRMapMatchDestInetPrefix;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddressNext;
    UINT1               au4NextHopInetAddrNext[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4FsRMapMatchSourceInetPrefix;
    INT4                i4FsRMapMatchDestInetType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4FsRMapMatchSourceInetType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4FsRMapMatchNextHopInetType = IPVX_ADDR_FMLY_IPV4;

    INT1                i1RetVal;
    UINT4               u4NextFsRMapMatchDestInetPrefix;
    UINT4               u4NextFsRMapMatchSourceInetPrefix;
    INT4                i4NextFsRMapMatchDestInetType;
    INT4                i4NextFsRMapMatchSourceInetType;
    INT4                i4NextFsRMapMatchNextHopInetType;
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpAddress);
    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpNextHop);
    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchNextHopInetAddress.i4_Length);

/*Next*/
    fsRMapMatchDestInetAddressNext.pu1_OctetList = au4DestInetAddrNext;
    fsRMapMatchDestInetAddressNext.i4_Length = IPVX_IPV4_ADDR_LEN;

    MEMSET (fsRMapMatchDestInetAddressNext.pu1_OctetList, 0,
            fsRMapMatchDestInetAddressNext.i4_Length);

    fsRMapMatchSrcInetAddressNext.pu1_OctetList = au4SrcInetAddrNext;
    fsRMapMatchSrcInetAddressNext.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMSET (fsRMapMatchSrcInetAddressNext.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddressNext.pu1_OctetList = au4NextHopInetAddrNext;
    fsRMapMatchNextHopInetAddressNext.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMSET (fsRMapMatchNextHopInetAddressNext.pu1_OctetList, 0,
            fsRMapMatchNextHopInetAddressNext.i4_Length);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchDestInetPrefix,
                          u4FsRouteMapMatchIpAddrMask);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchSourceInetPrefix, 0);

    do
    {
        i1RetVal =
            nmhGetNextIndexFsRMapMatchTable (pFsRouteMapName,
                                             pNextFsRouteMapName,
                                             u4FsRouteMapSeqNum,
                                             pu4NextFsRouteMapSeqNum,
                                             i4FsRMapMatchDestInetType,
                                             &i4NextFsRMapMatchDestInetType,
                                             &fsRMapMatchDestInetAddress,
                                             &fsRMapMatchDestInetAddressNext,
                                             u4FsRMapMatchDestInetPrefix,
                                             &u4NextFsRMapMatchDestInetPrefix,
                                             i4FsRMapMatchSourceInetType,
                                             &i4NextFsRMapMatchSourceInetType,
                                             &fsRMapMatchSrcInetAddress,
                                             &fsRMapMatchSrcInetAddressNext,
                                             u4FsRMapMatchSourceInetPrefix,
                                             &u4NextFsRMapMatchSourceInetPrefix,
                                             i4FsRMapMatchNextHopInetType,
                                             &i4NextFsRMapMatchNextHopInetType,
                                             &fsRMapMatchNextHopInetAddress,
                                             &fsRMapMatchNextHopInetAddressNext,
                                             i4FsRouteMapMatchInterface,
                                             pi4NextFsRouteMapMatchInterface,
                                             i4FsRouteMapMatchMetric,
                                             pi4NextFsRouteMapMatchMetric,
                                             u4FsRouteMapMatchTag,
                                             pu4NextFsRouteMapMatchTag,
                                             i4FsRouteMapMatchMetricType,
                                             pi4NextFsRouteMapMatchMetricType,
                                             i4FsRouteMapMatchRouteType,
                                             pi4NextFsRouteMapMatchRouteType,
                                             u4FsRouteMapMatchASPathTag,
                                             pu4NextFsRouteMapMatchASPathTag,
                                             u4FsRouteMapMatchCommunity,
                                             pu4NextFsRouteMapMatchCommunity,
                                             i4FsRouteMapMatchLocalPreference,
                                             pi4NextFsRouteMapMatchLocalPreference,
                                             i4FsRouteMapMatchOrigin,
                                             pi4NextFsRouteMapMatchOrigin);

        if (i1RetVal == SNMP_SUCCESS)
        {
            if (i4NextFsRMapMatchDestInetType != IPVX_ADDR_FMLY_IPV6
                && fsRMapMatchSrcInetAddressNext.i4_Length == 0
                && i4NextFsRMapMatchNextHopInetType != IPVX_ADDR_FMLY_IPV6)
            {
                *pu4NextFsRouteMapMatchIpAddress =
                    /*OSIX_NTOHL */
                    (*(UINT4 *) (VOID *)
                     (fsRMapMatchDestInetAddressNext.pu1_OctetList));
                IPV4_MASKLEN_TO_MASK (*pu4NextFsRouteMapMatchIpAddrMask,
                                      u4NextFsRMapMatchDestInetPrefix);
                *pu4NextFsRouteMapMatchIpNextHop =
                    /*OSIX_NTOHL */
                    (*(UINT4
                       *) (VOID *)
                     (fsRMapMatchNextHopInetAddressNext.pu1_OctetList));
                return SNMP_SUCCESS;
            }
            else
            {
                pFsRouteMapName->i4_Length = pNextFsRouteMapName->i4_Length;
                MEMCPY (pFsRouteMapName->pu1_OctetList,
                        pNextFsRouteMapName->pu1_OctetList,
                        pNextFsRouteMapName->i4_Length);
                pNextFsRouteMapName->i4_Length = 0;
                MEMSET (pNextFsRouteMapName->pu1_OctetList, 0,
                        RMAP_MAX_NAME_LEN + 4);

                u4FsRouteMapSeqNum = *pu4NextFsRouteMapSeqNum;
                i4FsRMapMatchDestInetType = i4NextFsRMapMatchDestInetType;

                MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList,
                        fsRMapMatchDestInetAddressNext.pu1_OctetList,
                        fsRMapMatchDestInetAddressNext.i4_Length);
                fsRMapMatchDestInetAddress.i4_Length =
                    fsRMapMatchDestInetAddressNext.i4_Length;

                u4FsRMapMatchDestInetPrefix = u4NextFsRMapMatchDestInetPrefix;
                i4FsRMapMatchSourceInetType = i4NextFsRMapMatchSourceInetType;

                MEMCPY (fsRMapMatchSrcInetAddress.pu1_OctetList,
                        fsRMapMatchSrcInetAddressNext.pu1_OctetList,
                        fsRMapMatchSrcInetAddressNext.i4_Length);
                fsRMapMatchSrcInetAddress.i4_Length =
                    fsRMapMatchSrcInetAddressNext.i4_Length;

                u4FsRMapMatchSourceInetPrefix =
                    u4NextFsRMapMatchSourceInetPrefix;
                i4FsRMapMatchNextHopInetType = i4NextFsRMapMatchNextHopInetType;

                MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList,
                        fsRMapMatchNextHopInetAddressNext.pu1_OctetList,
                        fsRMapMatchNextHopInetAddressNext.i4_Length);
                fsRMapMatchNextHopInetAddress.i4_Length =
                    fsRMapMatchNextHopInetAddressNext.i4_Length;

                i4FsRouteMapMatchInterface = *pi4NextFsRouteMapMatchInterface;
                i4FsRouteMapMatchMetric = *pi4NextFsRouteMapMatchMetric;
                u4FsRouteMapMatchTag = *pu4NextFsRouteMapMatchTag;
                i4FsRouteMapMatchMetricType = *pi4NextFsRouteMapMatchMetricType;
                i4FsRouteMapMatchRouteType = *pi4NextFsRouteMapMatchRouteType;
                u4FsRouteMapMatchASPathTag = *pu4NextFsRouteMapMatchASPathTag;
                u4FsRouteMapMatchCommunity = *pu4NextFsRouteMapMatchCommunity;
                i4FsRouteMapMatchLocalPreference =
                    *pi4NextFsRouteMapMatchLocalPreference;
                i4FsRouteMapMatchOrigin = *pi4NextFsRouteMapMatchOrigin;
            }
        }
    }
    while (i1RetVal != SNMP_FAILURE);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRouteMapMatchRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference

                The Object 
                retValFsRouteMapMatchRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapMatchRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                UINT4 u4FsRouteMapSeqNum,
                                INT4 i4FsRouteMapMatchInterface,
                                UINT4 u4FsRouteMapMatchIpAddress,
                                UINT4 u4FsRouteMapMatchIpAddrMask,
                                UINT4 u4FsRouteMapMatchIpNextHop,
                                INT4 i4FsRouteMapMatchMetric,
                                UINT4 u4FsRouteMapMatchTag,
                                INT4 i4FsRouteMapMatchRouteType,
                                INT4 i4FsRouteMapMatchMetricType,
                                UINT4 u4FsRouteMapMatchASPathTag,
                                UINT4 u4FsRouteMapMatchCommunity,
                                INT4 i4FsRouteMapMatchOrigin,
                                INT4 i4FsRouteMapMatchLocalPreference,
                                INT4 *pi4RetValFsRouteMapMatchRowStatus)
{
    UINT4               u4IpAddr;
    INT1                i1RetVal;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];

    UINT4               u4FsRMapMatchDestInetPrefix;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4FsRMapMatchSourceInetPrefix;
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpAddress);
    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpNextHop);
    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchNextHopInetAddress.i4_Length);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchDestInetPrefix,
                          u4FsRouteMapMatchIpAddrMask);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchSourceInetPrefix, 0);

    i1RetVal = nmhGetFsRMapMatchRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchDestInetType */
                                           &fsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix, 0,    /* i4FsRMapMatchSourceInetType */
                                           &fsRMapMatchSrcInetAddress, u4FsRMapMatchSourceInetPrefix, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchNextHopInetType */
                                           &fsRMapMatchNextHopInetAddress,
                                           i4FsRouteMapMatchInterface,
                                           i4FsRouteMapMatchMetric,
                                           u4FsRouteMapMatchTag,
                                           i4FsRouteMapMatchMetricType,
                                           i4FsRouteMapMatchRouteType,
                                           u4FsRouteMapMatchASPathTag,
                                           u4FsRouteMapMatchCommunity,
                                           i4FsRouteMapMatchLocalPreference,
                                           i4FsRouteMapMatchOrigin,
                                           pi4RetValFsRouteMapMatchRowStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRouteMapMatchRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference

                The Object 
                setValFsRouteMapMatchRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapMatchRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                UINT4 u4FsRouteMapSeqNum,
                                INT4 i4FsRouteMapMatchInterface,
                                UINT4 u4FsRouteMapMatchIpAddress,
                                UINT4 u4FsRouteMapMatchIpAddrMask,
                                UINT4 u4FsRouteMapMatchIpNextHop,
                                INT4 i4FsRouteMapMatchMetric,
                                UINT4 u4FsRouteMapMatchTag,
                                INT4 i4FsRouteMapMatchRouteType,
                                INT4 i4FsRouteMapMatchMetricType,
                                UINT4 u4FsRouteMapMatchASPathTag,
                                UINT4 u4FsRouteMapMatchCommunity,
                                INT4 i4FsRouteMapMatchOrigin,
                                INT4 i4FsRouteMapMatchLocalPreference,
                                INT4 i4SetValFsRouteMapMatchRowStatus)
{
    UINT4               u4IpAddr;

    UINT4               u4FsRMapMatchDestInetPrefix;
    INT1                i1RetVal;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4FsRMapMatchSourceInetPrefix;
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpAddress);
    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpNextHop);
    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchNextHopInetAddress.i4_Length);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchDestInetPrefix,
                          u4FsRouteMapMatchIpAddrMask);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchSourceInetPrefix, 0);

    i1RetVal = nmhSetFsRMapMatchRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchDestInetType */
                                           &fsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix, 0,    /* i4FsRMapMatchSourceInetType */
                                           &fsRMapMatchSrcInetAddress, u4FsRMapMatchSourceInetPrefix, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchNextHopInetType */
                                           &fsRMapMatchNextHopInetAddress,
                                           i4FsRouteMapMatchInterface,
                                           i4FsRouteMapMatchMetric,
                                           u4FsRouteMapMatchTag,
                                           i4FsRouteMapMatchMetricType,
                                           i4FsRouteMapMatchRouteType,
                                           u4FsRouteMapMatchASPathTag,
                                           u4FsRouteMapMatchCommunity,
                                           i4FsRouteMapMatchLocalPreference,
                                           i4FsRouteMapMatchOrigin,
                                           i4SetValFsRouteMapMatchRowStatus);
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapMatchRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference

                The Object 
                testValFsRouteMapMatchRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapMatchRowStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                   UINT4 u4FsRouteMapSeqNum,
                                   INT4 i4FsRouteMapMatchInterface,
                                   UINT4 u4FsRouteMapMatchIpAddress,
                                   UINT4 u4FsRouteMapMatchIpAddrMask,
                                   UINT4 u4FsRouteMapMatchIpNextHop,
                                   INT4 i4FsRouteMapMatchMetric,
                                   UINT4 u4FsRouteMapMatchTag,
                                   INT4 i4FsRouteMapMatchRouteType,
                                   INT4 i4FsRouteMapMatchMetricType,
                                   UINT4 u4FsRouteMapMatchASPathTag,
                                   UINT4 u4FsRouteMapMatchCommunity,
                                   INT4 i4FsRouteMapMatchOrigin,
                                   INT4 i4FsRouteMapMatchLocalPreference,
                                   INT4 i4TestValFsRouteMapMatchRowStatus)
{
    UINT4               u4IpAddr;
    INT1                i1RetVal;
    UINT4               u4FsRMapMatchSourceInetPrefix;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchNextHopInetAddress;
    UINT4               u4FsRMapMatchDestInetPrefix;
    tSNMP_OCTET_STRING_TYPE fsRMapMatchSrcInetAddress;
    UINT1               au4SrcInetAddr[IPVX_IPV6_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE fsRMapMatchDestInetAddress;
    UINT1               au4DestInetAddr[IPVX_IPV6_ADDR_LEN];
    fsRMapMatchDestInetAddress.pu1_OctetList = au4DestInetAddr;
    fsRMapMatchDestInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpAddress);
    MEMCPY (fsRMapMatchDestInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchDestInetAddress.i4_Length);

    fsRMapMatchSrcInetAddress.pu1_OctetList = au4SrcInetAddr;
    fsRMapMatchSrcInetAddress.i4_Length = 0;

    MEMSET (fsRMapMatchSrcInetAddress.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    fsRMapMatchNextHopInetAddress.pu1_OctetList = au4NextHopInetAddr;
    fsRMapMatchNextHopInetAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = /*OSIX_HTONL */ (u4FsRouteMapMatchIpNextHop);
    MEMCPY (fsRMapMatchNextHopInetAddress.pu1_OctetList, &u4IpAddr,
            fsRMapMatchNextHopInetAddress.i4_Length);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchDestInetPrefix,
                          u4FsRouteMapMatchIpAddrMask);

    IPV4_MASK_TO_MASKLEN (u4FsRMapMatchSourceInetPrefix, 0);

    i1RetVal = nmhTestv2FsRMapMatchRowStatus (pu4ErrorCode, pFsRouteMapName, u4FsRouteMapSeqNum, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchDestInetType */
                                              &fsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix, 0,    /* i4FsRMapMatchSourceInetType */
                                              &fsRMapMatchSrcInetAddress, u4FsRMapMatchSourceInetPrefix, IPVX_ADDR_FMLY_IPV4,    /* i4FsRMapMatchNextHopInetType */
                                              &fsRMapMatchNextHopInetAddress,
                                              i4FsRouteMapMatchInterface,
                                              i4FsRouteMapMatchMetric,
                                              u4FsRouteMapMatchTag,
                                              i4FsRouteMapMatchMetricType,
                                              i4FsRouteMapMatchRouteType,
                                              u4FsRouteMapMatchASPathTag,
                                              u4FsRouteMapMatchCommunity,
                                              i4FsRouteMapMatchLocalPreference,
                                              i4FsRouteMapMatchOrigin,
                                              i4TestValFsRouteMapMatchRowStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRouteMapMatchTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
                FsRouteMapMatchInterface
                FsRouteMapMatchIpAddress
                FsRouteMapMatchIpAddrMask
                FsRouteMapMatchIpNextHop
                FsRouteMapMatchMetric
                FsRouteMapMatchTag
                FsRouteMapMatchRouteType
                FsRouteMapMatchMetricType
                FsRouteMapMatchASPathTag
                FsRouteMapMatchCommunity
                FsRouteMapMatchOrigin
                FsRouteMapMatchLocalPreference
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRouteMapMatchTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRouteMapSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRouteMapSetTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRouteMapSetTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsRouteMapName,
                                            UINT4 u4FsRouteMapSeqNum)
{
    /*[osabo] */
    return nmhValidateIndexInstanceFsRMapSetTable (pFsRouteMapName,
                                                   u4FsRouteMapSeqNum);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRouteMapSetTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRouteMapSetTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                    UINT4 *pu4FsRouteMapSeqNum)
{
    /*[osabo] */
    return nmhGetFirstIndexFsRMapSetTable (pFsRouteMapName,
                                           pu4FsRouteMapSeqNum);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRouteMapSetTable
 Input       :  The Indices
                FsRouteMapName
                nextFsRouteMapName
                FsRouteMapSeqNum
                nextFsRouteMapSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRouteMapSetTable (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsRouteMapName,
                                   UINT4 u4FsRouteMapSeqNum,
                                   UINT4 *pu4NextFsRouteMapSeqNum)
{
    /*[osabo] */
    return nmhGetNextIndexFsRMapSetTable (pFsRouteMapName, pNextFsRouteMapName,
                                          u4FsRouteMapSeqNum,
                                          pu4NextFsRouteMapSeqNum);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetInterface
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetInterface (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 *pi4RetValFsRouteMapSetInterface)
{
    /*[osabo] */
    return nmhGetFsRMapSetInterface (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pi4RetValFsRouteMapSetInterface);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetIpNextHop
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetIpNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetIpNextHop (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              UINT4 *pu4RetValFsRouteMapSetIpNextHop)
{
    /*[osabo] */
    INT4                i4NextHopInetType;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

    if ((SNMP_FAILURE ==
         nmhGetFsRMapSetNextHopInetType (pFsRouteMapName, u4FsRouteMapSeqNum,
                                         &i4NextHopInetType))
        || (SNMP_FAILURE ==
            nmhGetFsRMapSetNextHopInetAddr (pFsRouteMapName, u4FsRouteMapSeqNum,
                                            &NextHopInetAddr)))
    {
        return SNMP_FAILURE;
    }

    if (IPVX_ADDR_FMLY_IPV4 != i4NextHopInetType)
    {
        /*IPv6 case */
        *pu4RetValFsRouteMapSetIpNextHop = 0;
    }
    else
    {
        /*IPv4 case */
        /*tSNMP_OCTET_STRING_TYPE.pu1_OctetList -> UINT4 */
        *pu4RetValFsRouteMapSetIpNextHop =
            /*OSIX_NTOHL */
            (*(UINT4 *) (VOID *) (NextHopInetAddr.pu1_OctetList));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetMetric
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetMetric (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 *pi4RetValFsRouteMapSetMetric)
{
    /*[osabo] */
    return nmhGetFsRMapSetMetric (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  pi4RetValFsRouteMapSetMetric);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetTag (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                        UINT4 u4FsRouteMapSeqNum,
                        UINT4 *pu4RetValFsRouteMapSetTag)
{
    /*[osabo] */
    return nmhGetFsRMapSetTag (pFsRouteMapName, u4FsRouteMapSeqNum,
                               pu4RetValFsRouteMapSetTag);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetMetricType
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetMetricType (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                               UINT4 u4FsRouteMapSeqNum,
                               INT4 *pi4RetValFsRouteMapSetMetricType)
{
    /*[osabo] */
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    *pi4RetValFsRouteMapSetMetricType = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetASPathTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetASPathTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetASPathTag (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              UINT4 *pu4RetValFsRouteMapSetASPathTag)
{
    /*[osabo] */
    return nmhGetFsRMapSetASPathTag (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pu4RetValFsRouteMapSetASPathTag);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetCommunity
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetCommunity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetCommunity (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              tSNMP_OCTET_STRING_TYPE * pRetValFsRouteMapSetCommunity)
{
    /*[osabo] */
    return nmhGetFsRMapSetCommunity (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pRetValFsRouteMapSetCommunity);
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetCommunityAdditive
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object 
                retValFsRMapSetCommunityAdditive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetCommunityAdditive (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                  UINT4 u4FsRMapSeqNum,
                                  INT4 *pi4RetValFsRMapSetCommunityAdditive)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetCommunityAdditive = pRMapNode->pSetNode->u1Addflag;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetCommunity failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetOrigin
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetOrigin (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 *pi4RetValFsRouteMapSetOrigin)
{
    /*[osabo] */
    return nmhGetFsRMapSetOrigin (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  pi4RetValFsRouteMapSetOrigin);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetOriginASNum
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetOriginASNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetOriginASNum (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                UINT4 u4FsRouteMapSeqNum,
                                UINT4 *pu4RetValFsRouteMapSetOriginASNum)
{
    /*[osabo] */
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    *pu4RetValFsRouteMapSetOriginASNum = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetLocalPreference
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetLocalPreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetLocalPreference (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                    UINT4 u4FsRouteMapSeqNum,
                                    INT4 *pi4RetValFsRouteMapSetLocalPreference)
{
    return nmhGetFsRMapSetLocalPref (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pi4RetValFsRouteMapSetLocalPreference);
}

/****************************************************************************
 Function    :  nmhGetFsRouteMapSetRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                retValFsRouteMapSetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRouteMapSetRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 *pi4RetValFsRouteMapSetRowStatus)
{
    /*[osabo] */
    return nmhGetFsRMapSetRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pi4RetValFsRouteMapSetRowStatus);
}

    /* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetInterface
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetInterface (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 i4SetValFsRouteMapSetInterface)
{
    /*[osabo] */
    return nmhSetFsRMapSetInterface (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     i4SetValFsRouteMapSetInterface);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetIpNextHop
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetIpNextHop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetIpNextHop (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              UINT4 u4SetValFsRouteMapSetIpNextHop)
{
    /*[osabo] */
    INT4                i4NextHopInetType;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;
    UINT1               au4NextHopInetAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au4NextHopInetAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

/*    u4SetValFsRouteMapSetIpNextHop = OSIX_HTONL(u4SetValFsRouteMapSetIpNextHop);*/
    NextHopInetAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (NextHopInetAddr.pu1_OctetList, &u4SetValFsRouteMapSetIpNextHop,
            NextHopInetAddr.i4_Length);
    i4NextHopInetType = IPVX_ADDR_FMLY_IPV4;

    if (SNMP_FAILURE ==
        nmhSetFsRMapSetNextHopInetType (pFsRouteMapName, u4FsRouteMapSeqNum,
                                        i4NextHopInetType))
    {
        return SNMP_FAILURE;
    }

    if (SNMP_FAILURE ==
        nmhSetFsRMapSetNextHopInetAddr (pFsRouteMapName, u4FsRouteMapSeqNum,
                                        &NextHopInetAddr))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetMetric
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetMetric (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 i4SetValFsRouteMapSetMetric)
{
    /*[osabo] */
    return nmhSetFsRMapSetMetric (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  i4SetValFsRouteMapSetMetric);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetTag (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                        UINT4 u4FsRouteMapSeqNum,
                        UINT4 u4SetValFsRouteMapSetTag)
{
    /*[osabo] */
    return nmhSetFsRMapSetTag (pFsRouteMapName, u4FsRouteMapSeqNum,
                               u4SetValFsRouteMapSetTag);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetMetricType
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetMetricType (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                               UINT4 u4FsRouteMapSeqNum,
                               INT4 i4SetValFsRouteMapSetMetricType)
{
    /*[osabo] */
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    UNUSED_PARAM (i4SetValFsRouteMapSetMetricType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetASPathTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetASPathTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetASPathTag (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              UINT4 u4SetValFsRouteMapSetASPathTag)
{
    /*[osabo] */
    return nmhSetFsRMapSetASPathTag (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     u4SetValFsRouteMapSetASPathTag);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetCommunity
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetCommunity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetCommunity (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              tSNMP_OCTET_STRING_TYPE * pSetValFsRouteMapSetCommunity)
{
    /*[osabo] */
    return nmhSetFsRMapSetCommunity (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     pSetValFsRouteMapSetCommunity);
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetCommunityAdditive
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object 
                setValFsRMapSetCommunityAdditive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetCommunityAdditive (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                  UINT4 u4FsRMapSeqNum,
                                  INT4 i4SetValFsRMapSetCommunityAdditive)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    
    if(1 == i4SetValFsRMapSetCommunityAdditive)
    {
        return SNMP_SUCCESS;
    }
    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            pRMapNode->pSetNode->u1Addflag = (UINT1) i4SetValFsRMapSetCommunityAdditive;
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetCommunityAdditive failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetCommunity failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetOrigin
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetOrigin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetOrigin (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           INT4 i4SetValFsRouteMapSetOrigin)
{
    /*[osabo] */
    return nmhSetFsRMapSetOrigin (pFsRouteMapName, u4FsRouteMapSeqNum,
                                  i4SetValFsRouteMapSetOrigin);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetOriginASNum
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetOriginASNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetOriginASNum (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                UINT4 u4FsRouteMapSeqNum,
                                UINT4 u4SetValFsRouteMapSetOriginASNum)
{
    /*[osabo] */
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    UNUSED_PARAM (u4SetValFsRouteMapSetOriginASNum);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetLocalPreference
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetLocalPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetLocalPreference (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                    UINT4 u4FsRouteMapSeqNum,
                                    INT4 i4SetValFsRouteMapSetLocalPreference)
{
    /*[osabo] */
    return nmhSetFsRMapSetLocalPref (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     i4SetValFsRouteMapSetLocalPreference);
}

/****************************************************************************
 Function    :  nmhSetFsRouteMapSetRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                setValFsRouteMapSetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRouteMapSetRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 i4SetValFsRouteMapSetRowStatus)
{
    /*[osabo] */
    return nmhSetFsRMapSetRowStatus (pFsRouteMapName, u4FsRouteMapSeqNum,
                                     i4SetValFsRouteMapSetRowStatus);
}

    /* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetInterface
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetInterface (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 u4FsRouteMapSeqNum,
                                 INT4 i4TestValFsRouteMapSetInterface)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetInterface (pu4ErrorCode, pFsRouteMapName,
                                        u4FsRouteMapSeqNum,
                                        i4TestValFsRouteMapSetInterface);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetIpNextHop
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetIpNextHop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetIpNextHop (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 u4FsRouteMapSeqNum,
                                 UINT4 u4TestValFsRouteMapSetIpNextHop)
{

    /*[osabo] */
    INT4                i4NextHopInetType;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddr;
    UINT1               au4NextHopInetAddr[IPVX_IPV6_ADDR_LEN];

    NextHopInetAddr.pu1_OctetList = au4NextHopInetAddr;
    NextHopInetAddr.i4_Length = 0;

/*    u4TestValFsRouteMapSetIpNextHop = OSIX_HTONL(u4TestValFsRouteMapSetIpNextHop);*/
    NextHopInetAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    MEMCPY (NextHopInetAddr.pu1_OctetList, &u4TestValFsRouteMapSetIpNextHop,
            NextHopInetAddr.i4_Length);
    i4NextHopInetType = IPVX_ADDR_FMLY_IPV4;

    if ((SNMP_FAILURE ==
         nmhTestv2FsRMapSetNextHopInetType (pu4ErrorCode, pFsRouteMapName,
                                            u4FsRouteMapSeqNum,
                                            i4NextHopInetType))
        || (SNMP_FAILURE ==
            nmhTestv2FsRMapSetNextHopInetAddr (pu4ErrorCode, pFsRouteMapName,
                                               u4FsRouteMapSeqNum,
                                               &NextHopInetAddr)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetMetric
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetMetric (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 i4TestValFsRouteMapSetMetric)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetMetric (pu4ErrorCode, pFsRouteMapName,
                                     u4FsRouteMapSeqNum,
                                     i4TestValFsRouteMapSetMetric);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetTag (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                           UINT4 u4FsRouteMapSeqNum,
                           UINT4 u4TestValFsRouteMapSetTag)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetTag (pu4ErrorCode, pFsRouteMapName,
                                  u4FsRouteMapSeqNum,
                                  u4TestValFsRouteMapSetTag);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetMetricType
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetMetricType (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                  UINT4 u4FsRouteMapSeqNum,
                                  INT4 i4TestValFsRouteMapSetMetricType)
{
    /*[osabo] */
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    UNUSED_PARAM (i4TestValFsRouteMapSetMetricType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetASPathTag
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetASPathTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetASPathTag (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 u4FsRouteMapSeqNum,
                                 UINT4 u4TestValFsRouteMapSetASPathTag)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetASPathTag (pu4ErrorCode, pFsRouteMapName,
                                        u4FsRouteMapSeqNum,
                                        u4TestValFsRouteMapSetASPathTag);
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetCommunityAdditive
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object 
                testValFsRMapSetCommunityAdditive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetCommunityAdditive (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                     UINT4 u4FsRMapSeqNum,
                                     INT4 i4TestValFsRMapSetCommunityAdditive)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRMapSetCommunityAdditive != RMAP_BGP_COMMUNITY_ADDITIVE) &&
        (i4TestValFsRMapSetCommunityAdditive != RMAP_BGP_COMMUNITY_REPLACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetCommunity
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetCommunity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetCommunity (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 u4FsRouteMapSeqNum,
                                 tSNMP_OCTET_STRING_TYPE * pTestValFsRouteMapSetCommunity)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetCommunity (pu4ErrorCode, pFsRouteMapName,
                                        u4FsRouteMapSeqNum,
                                        pTestValFsRouteMapSetCommunity);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetOrigin
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetOrigin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetOrigin (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                              UINT4 u4FsRouteMapSeqNum,
                              INT4 i4TestValFsRouteMapSetOrigin)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetOrigin (pu4ErrorCode, pFsRouteMapName,
                                     u4FsRouteMapSeqNum,
                                     i4TestValFsRouteMapSetOrigin);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetOriginASNum
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetOriginASNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetOriginASNum (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                   UINT4 u4FsRouteMapSeqNum,
                                   UINT4 u4TestValFsRouteMapSetOriginASNum)
{
    /*[osabo] */
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRouteMapName);
    UNUSED_PARAM (u4FsRouteMapSeqNum);
    UNUSED_PARAM (u4TestValFsRouteMapSetOriginASNum);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetLocalPreference
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetLocalPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetLocalPreference (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsRouteMapName,
                                       UINT4 u4FsRouteMapSeqNum,
                                       INT4
                                       i4TestValFsRouteMapSetLocalPreference)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetLocalPref (pu4ErrorCode, pFsRouteMapName,
                                        u4FsRouteMapSeqNum,
                                        i4TestValFsRouteMapSetLocalPreference);
}

/****************************************************************************
 Function    :  nmhTestv2FsRouteMapSetRowStatus
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum

                The Object
                testValFsRouteMapSetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRouteMapSetRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRouteMapName,
                                 UINT4 u4FsRouteMapSeqNum,
                                 INT4 i4TestValFsRouteMapSetRowStatus)
{
    /*[osabo] */
    return nmhTestv2FsRMapSetRowStatus (pu4ErrorCode, pFsRouteMapName,
                                        u4FsRouteMapSeqNum,
                                        i4TestValFsRouteMapSetRowStatus);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRouteMapSetTable
 Input       :  The Indices
                FsRouteMapName
                FsRouteMapSeqNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRouteMapSetTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhValidateIndexInstanceFsRMapTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                     UINT4 u4FsRMapSeqNum)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((pFsRMapName->i4_Length <= RMAP_MAX_NAME_LEN) &&
        (u4FsRMapSeqNum <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, ALL_FAILURE_TRC, RMAP_MODULE_NAME,
                  " Incorrect Route Map Indices.\n");
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRMapTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRMapTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 *pu4FsRMapSeqNum)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    RMAP_CLEAR_OCT (pFsRMapName, RMAP_MAX_NAME_LEN);
    *pu4FsRMapSeqNum = RMAP_ZERO;

    pRMapNode = RBTreeGetFirst (gRMapGlobalInfo.RMapRoot);
    if (pRMapNode != NULL)
    {
        RMAP_ASCIIZ_2_OCT (pFsRMapName, pRMapNode->au1RMapName);
        *pu4FsRMapSeqNum = pRMapNode->u4SeqNo;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRMapTable
 Input       :  The Indices
                FsRMapName
                nextFsRMapName
                FsRMapSeqNum
                nextFsRMapSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRMapTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            tSNMP_OCTET_STRING_TYPE * pNextFsRMapName,
                            UINT4 u4FsRMapSeqNum, UINT4 *pu4NextFsRMapSeqNum)
{
    tRMapNode           RMapNode, *pRMapNextNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    /* Create local MAP node. It will be safe to use
       input/output params (octet-name, seqnum)the same */
    MEMSET (&RMapNode, 0, sizeof (tRMapNode));
    RMAP_OCT_2_ASCIIZ (RMapNode.au1RMapName, pFsRMapName);
    RMapNode.u4SeqNo = u4FsRMapSeqNum;

    RMAP_CLEAR_OCT (pNextFsRMapName, RMAP_MAX_NAME_LEN);
    *pu4NextFsRMapSeqNum = RMAP_ZERO;

    pRMapNextNode = (tRMapNode *) RBTreeGetNext (gRMapGlobalInfo.RMapRoot,
                                                 &RMapNode, NULL);
    if (pRMapNextNode != NULL)
    {
        RMAP_ASCIIZ_2_OCT (pNextFsRMapName, pRMapNextNode->au1RMapName);
        *pu4NextFsRMapSeqNum = pRMapNextNode->u4SeqNo;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Next Route Map Entry not available.\n");
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRMapAccess
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapAccess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapAccess (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                    UINT4 u4FsRMapSeqNum, INT4 *pi4RetValFsRMapAccess)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        *pi4RetValFsRMapAccess = (INT4) pRMapNode->u1Access;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsRMapRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 *pi4RetValFsRMapRowStatus)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        *pi4RetValFsRMapRowStatus = (INT4) pRMapNode->u1RowStatus;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapIsIpPrefixList
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapIsIpPrefixList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapIsIpPrefixList (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            INT4 *pi4RetValFsRMapIsIpPrefixList)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        *pi4RetValFsRMapIsIpPrefixList = (INT4) pRMapNode->u1IsIpPrefixInfo;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
    }
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRMapAccess
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapAccess
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapAccess (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                    UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapAccess)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        pRMapNode->u1Access = (UINT1) i4SetValFsRMapAccess;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapRowStatus)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapNode          *pTmpRMapNode = NULL;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status;
    UINT4               u4TmpSeqNo = 0;
    UINT4               u4RBretval = 0;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        CLI_SET_ERR (RMAP_CLI_MAX_PREFIX_NAME_LEN);
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    switch (i4SetValFsRMapRowStatus)
    {
        case RMAP_CREATE_AND_WAIT:
        case RMAP_CREATE_AND_GO:

            if (pRMapNode == NULL)
            {
                MEMCPY (&(IpPrefixNode.au1IpPrefixName), au1Name,
                        RMAP_MAX_NAME_LEN);
                /* Create the IP prefix node if not exist */
                pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                           (tRBElem *) & IpPrefixNode);

                if (pIpPrefixNode == NULL)
                {
                    pIpPrefixNode =
                        (tRMapIpPrefix *) MemAllocMemBlk (gRMapGlobalInfo.
                                                          IpPrefixPoolId);
                    if (pIpPrefixNode == NULL)
                    {
                        CLI_SET_ERR (RMAP_CLI_MAX_IP_PREFIX_LIST_RCHD);
                        return SNMP_FAILURE;
                    }
                    MEMSET (pIpPrefixNode, 0, sizeof (tRMapIpPrefix));
                    MEMCPY (pIpPrefixNode->au1IpPrefixName, au1Name,
                            RMAP_MAX_NAME_LEN);
                    u4RBretval = RBTreeAdd (gRMapGlobalInfo.IpPrefixRoot,
                                            (tRBElem *) pIpPrefixNode);
                }
                if (u4FsRMapSeqNum > ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP)
                {
                    CLI_SET_ERR (RMAP_CLI_MAX_SEQ_NUM_EXCEED);
                    return SNMP_FAILURE;
                }

                /* The Row status could be CREATE AND WAIT */
                /* we need to create the node and add it to the Route Map RBTree */
                pRMapNode = RMapAddRouteMapNode (au1Name, u4FsRMapSeqNum);

                if (pRMapNode != NULL)
                {
                    if (i4SetValFsRMapRowStatus == RMAP_CREATE_AND_GO)
                    {
                        pRMapNode->u1RowStatus = RMAP_ACTIVE;

                        /* Send the update message to RTM4/RTM6 */
                        RMapSendUpdateMsg (au1Name);

                        /* Send the update message with status to other applications */
                        u4Status = RMapGetMapStatus (au1Name);
                        RMapSendStatusUpdateMsg (au1Name, u4Status);
                    }
                    else if (i4SetValFsRMapRowStatus == RMAP_CREATE_AND_WAIT)
                    {
                        pRMapNode->u1RowStatus = RMAP_NOT_READY;
                    }
                }
                else
                {
                    RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                              "Error in Route Map Node creation .\n");
                    CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
                    return SNMP_FAILURE;
                }

            }
            else
            {
                RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                          "Error in Route Map RowStatus .\n");
                CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
                return SNMP_FAILURE;
            }
            break;

        case RMAP_ACTIVE:

            if (pRMapNode != NULL)
            {
                /* If it is Ip Prefix entry update the IP Prefix informations */
                if (pRMapNode->u1IsIpPrefixInfo == TRUE)
                {
                    MEMCPY (&(IpPrefixNode.au1IpPrefixName), au1Name,
                            RMAP_MAX_NAME_LEN);
                    if ((pIpPrefixNode =
                         RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                    (tRBElem *) & IpPrefixNode)) != NULL)
                    {
                        pIpPrefixNode->u4Count++;
                        if (u4FsRMapSeqNum > pIpPrefixNode->u4LastUsedSeq)
                        {
                            pIpPrefixNode->u4LastUsedSeq = u4FsRMapSeqNum;
                        }
                    }
                }
                pRMapNode->u1RowStatus = (UINT1) i4SetValFsRMapRowStatus;

                /* Send the update message to RTM4/RTM6 */
                RMapSendUpdateMsg (au1Name);

                /* Send the update message with status to other applications */
                u4Status = RMapGetMapStatus (au1Name);
                RMapSendStatusUpdateMsg (au1Name, u4Status);
            }
            else
            {
                RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                          "Error in Route Map RowStatus .\n");
                CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
                return SNMP_FAILURE;
            }

            break;

        case RMAP_NOT_IN_SERVICE:
            break;

        case RMAP_DESTROY:
            if (pRMapNode != NULL)
            {
                MEMCPY (&(IpPrefixNode.au1IpPrefixName), au1Name,
                        RMAP_MAX_NAME_LEN);
                if ((pIpPrefixNode =
                     RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                (tRBElem *) & IpPrefixNode)) != NULL)
                {
                    if (pIpPrefixNode->u4Count == 0)
                    {
                        RBTreeRem (gRMapGlobalInfo.IpPrefixRoot,
                                   ((tRBElem *) pIpPrefixNode));
                        MemReleaseMemBlock (gRMapGlobalInfo.IpPrefixPoolId,
                                            (UINT1 *) pIpPrefixNode);
                    }
                }
                if ((pRMapNode->u1IsIpPrefixInfo == TRUE)
                    && (pIpPrefixNode != NULL))
                {
                    pIpPrefixNode->u4Count--;
                    /* If the deleted Ip Prefix entry is having the highest seq number, then 
                     * re-calculate the next highest seq number in use*/
                    if (u4FsRMapSeqNum == pIpPrefixNode->u4LastUsedSeq)
                    {
                        for (u4TmpSeqNo = 1; u4TmpSeqNo < u4FsRMapSeqNum;
                             u4TmpSeqNo++)
                        {
                            pTmpRMapNode =
                                RMapGetRouteMapNode (au1Name, u4TmpSeqNo);

                            if ((pTmpRMapNode == NULL)
                                || (pTmpRMapNode->u1IsIpPrefixInfo != TRUE))
                            {
                                continue;
                            }
                            else
                            {
                                pIpPrefixNode->u4LastUsedSeq = u4TmpSeqNo;
                            }
                        }

                        /* Even after scanning the whole RouteMap tree,
                         * if pIpPrefixNode->u4LastUsedSeq is equal to the u4FsRMapSeqNum, 
                         * then there is no more IP Prefix entry present. So assign ZERO
                         */
                        if (pIpPrefixNode->u4LastUsedSeq == u4FsRMapSeqNum)
                        {
                            pIpPrefixNode->u4LastUsedSeq = 0;
                        }
                    }
                    if (pIpPrefixNode->u4Count == 0)
                    {
                        RBTreeRem (gRMapGlobalInfo.IpPrefixRoot,
                                   ((tRBElem *) pIpPrefixNode));
                        MemReleaseMemBlock (gRMapGlobalInfo.IpPrefixPoolId,
                                            (UINT1 *) pIpPrefixNode);
                    }
                }

                /* When the status is in RMAP_NOT_IN_SERVICE, send update msg
                 * registered applications, Applications updates the Routing
                 * protocls  about this change, after this process only
                 * this Route Map has to be deleted. If there is no
                 * application registered with Route Map, make the Row status
                 * to RMAP_NOT_IN_SERVICE */

                pRMapNode->u1RowStatus = (UINT1) i4SetValFsRMapRowStatus;

                /* Send the update message to RTM4/RTM6 */
                RMapSendUpdateMsg (au1Name);

                /* Send the update message with status to other applications */
                u4Status = RMapGetMapStatus (au1Name);
                RMapSendStatusUpdateMsg (au1Name, u4Status);

                RMapProcessRouteMapUpdate (au1Name);
            }
            else
            {
                RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                          "Error in Route Map RowStatus .\n");
                CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
                return SNMP_FAILURE;
            }

            break;
        default:
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Error in Route Map RowStatus .\n");
            CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
            return SNMP_FAILURE;
    }
    /* Added the below line to address coverity warning */
    UNUSED_PARAM (u4RBretval);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRMapIsIpPrefixList
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapIsIpPrefixList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapIsIpPrefixList (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            INT4 i4SetValFsRMapIsIpPrefixList)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pFsRMapName);

    if ((pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                    (tRBElem *) & IpPrefixNode)) == NULL)
    {
        return SNMP_FAILURE;
    }
    pIpPrefixNode->u1IsIpPrefixList = TRUE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        pRMapNode->u1IsIpPrefixInfo = (UINT1) i4SetValFsRMapIsIpPrefixList;
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "IP Prefix Entry not available.\n");
    }
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRMapAccess
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapAccess
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapAccess (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 i4TestValFsRMapAccess)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if ((i4TestValFsRMapAccess != RMAP_ROUTE_PERMIT) &&
            (i4TestValFsRMapAccess != RMAP_ROUTE_DENY))
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Wrong Route Map Access value.\n");

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map Entry %s not available.\n",
                       pFsRMapName->pu1_OctetList);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapRowStatus (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4TestValFsRMapRowStatus)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        /* check whether the Row status is create and wait */
        if (i4TestValFsRMapRowStatus == RMAP_CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                           "Route Map %s doesn't exist.\n", au1Name);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        }
    }
    else
    {
        /* The entry already exists */
        /* either the status should be active or destroy */
        if ((i4TestValFsRMapRowStatus == RMAP_ACTIVE) ||
            (i4TestValFsRMapRowStatus == RMAP_DESTROY) ||
            (i4TestValFsRMapRowStatus == RMAP_NOT_IN_SERVICE))
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else if (i4TestValFsRMapRowStatus == RMAP_CREATE_AND_WAIT)
        {
            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                           "Route Map %s is in create and wait state.\n",
                           au1Name);

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapIsIpPrefixList
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapIsIpPrefixList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapIsIpPrefixList (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                               UINT4 u4FsRMapSeqNum,
                               INT4 i4TestValFsRMapIsIpPrefixList)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT4               u4SeqNo = 0;
    UINT1               u1IsRMap = 0;
    UINT1               u1MatchCmdArrayIndex = 0;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));

    pRMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (i4TestValFsRMapIsIpPrefixList != TRUE)
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Wrong value for IsIpPrefixList.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "IP Prefix entry Entry %s not available.\n",
                       pFsRMapName->pu1_OctetList);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pFsRMapName);
    pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                               (tRBElem *) & IpPrefixNode);
    if (pIpPrefixNode != NULL)
    {
        /*If the Prefix list is already associated with IP Prefix entries,
         * then return SUCCESS*/
        if (pIpPrefixNode->u1IsIpPrefixList == TRUE)
        {
            return SNMP_SUCCESS;
        }
        /* Check if any route map entries are associated with this name */
        else
        {
            do
            {
                pRMapNode = RMapGetNextRouteMapNodeOct (pFsRMapName, u4SeqNo);
                if (pRMapNode == NULL)
                {
                    break;
                }
                if (STRCMP (pFsRMapName->pu1_OctetList, pRMapNode->au1RMapName)
                    != 0)
                {
                    break;
                }

                for (u1MatchCmdArrayIndex = 0;
                     u1MatchCmdArrayIndex < RMAP_MATCH_CMD_ORIGIN;
                     u1MatchCmdArrayIndex++)
                {
                    if (pRMapNode->MatchCmdArray[u1MatchCmdArrayIndex].pNode !=
                        NULL)
                    {
                        u1IsRMap = TRUE;
                        break;
                    }
                }
                u4SeqNo = pRMapNode->u4SeqNo;
            }
            while (1);

            if (u1IsRMap == TRUE)
            {
                /* The given name is associated with other route map entroies.
                 * So Ip prefix entries cannot be created with this name*/
                RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                               "Route map entry Entry is present with the name %s.\n"
                               "So cannot create IP Prefix entry with the same name",
                               pFsRMapName->pu1_OctetList);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (RMAP_CLI_RMAP_EXIST_WITH_GIVEN_NAME);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRMapTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRMapMatchTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRMapMatchTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRMapMatchTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                          UINT4 u4FsRMapSeqNum,
                                          INT4 i4FsRMapMatchDestInetType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsRMapMatchDestInetAddress,
                                          UINT4 u4FsRMapMatchDestInetPrefix,
                                          INT4 i4FsRMapMatchSourceInetType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsRMapMatchSourceInetAddress,
                                          UINT4 u4FsRMapMatchSourceInetPrefix,
                                          INT4 i4FsRMapMatchNextHopInetType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsRMapMatchNextHopInetAddr,
                                          INT4 i4FsRMapMatchInterface,
                                          INT4 i4FsRMapMatchMetric,
                                          UINT4 u4FsRMapMatchTag,
                                          INT4 i4FsRMapMatchMetricType,
                                          INT4 i4FsRMapMatchRouteType,
                                          UINT4 u4FsRMapMatchASPathTag,
                                          UINT4 u4FsRMapMatchCommunity,
                                          INT4 i4FsRMapMatchLocalPref,
                                          INT4 i4FsRMapMatchOrigin)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      RMapMatchNode, *pRMapMatchNode;
    INT1                i1RetVal = SNMP_FAILURE;
    tIPvXAddr           DstXAddr, SrcXAddr, NhXAddr;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    IPVX_ADDR_INIT (DstXAddr,
                    i4FsRMapMatchDestInetType,
                    pFsRMapMatchDestInetAddress->pu1_OctetList);
    IPVX_ADDR_INIT (SrcXAddr,
                    i4FsRMapMatchSourceInetType,
                    pFsRMapMatchSourceInetAddress->pu1_OctetList);
    IPVX_ADDR_INIT (NhXAddr,
                    i4FsRMapMatchNextHopInetType,
                    pFsRMapMatchNextHopInetAddr->pu1_OctetList);

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map %s not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    MEMSET (&RMapMatchNode, 0, sizeof (tRMapMatchNode));

    if (pRMapNode->u1IsIpPrefixInfo == TRUE)
    {
        IPVX_ADDR_COPY (&(RMapMatchNode.RMapDestX), &DstXAddr);
        RMapMatchNode.RMapDestPrefixLen = u4FsRMapMatchDestInetPrefix;
        RMapMatchNode.u1Cmd = RMAP_MATCH_IP_PREFIX_ENTRY;
        i1RetVal = SNMP_SUCCESS;
    }
    else if (TestZeroIpvxAddr (&DstXAddr) != 0)
    {
        if ((ValidateXAddr (&DstXAddr) == RMAP_SUCCESS) &&
            (ValidateXAddrPrefix (u4FsRMapMatchDestInetPrefix,
                                  i4FsRMapMatchDestInetType) == RMAP_SUCCESS))
        {
            IPVX_ADDR_COPY (&(RMapMatchNode.RMapDestX), &DstXAddr);
            RMapMatchNode.RMapDestPrefixLen = u4FsRMapMatchDestInetPrefix;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_DEST_IPX;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (TestZeroIpvxAddr (&SrcXAddr) != 0)
    {
        if ((ValidateXAddr (&SrcXAddr) == RMAP_SUCCESS) &&
            (ValidateXAddrPrefix (u4FsRMapMatchSourceInetPrefix,
                                  i4FsRMapMatchSourceInetType) == RMAP_SUCCESS))
        {
            IPVX_ADDR_COPY (&(RMapMatchNode.RMapSourceX), &SrcXAddr);
            RMapMatchNode.RMapSourcePrefixLen = u4FsRMapMatchSourceInetPrefix;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_SOURCE_IPX;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (TestZeroIpvxAddr (&NhXAddr) != 0)
    {
        if (ValidateXAddr (&NhXAddr) == RMAP_SUCCESS)
        {
            IPVX_ADDR_COPY (&(RMapMatchNode.RMapNextHopX), &NhXAddr);
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_NEXTHOP_IPX;
            i1RetVal = SNMP_SUCCESS;
        }
    }

    else if (i4FsRMapMatchInterface != 0)
    {
        if ((i4FsRMapMatchInterface < RMAP_SYS_DEF_MAX_INTERFACES) ||
            (i4FsRMapMatchInterface > 0))
        {
            RMapMatchNode.RMapIfIndex = i4FsRMapMatchInterface;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_INTERFACE;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4FsRMapMatchMetric != 0)
    {
        if (i4FsRMapMatchMetric > 0)
        {
            RMapMatchNode.RMapMetric = i4FsRMapMatchMetric;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_METRIC;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (u4FsRMapMatchTag != 0)
    {
        if (u4FsRMapMatchTag > 0 && u4FsRMapMatchTag < RMAP_MAX_TAG_VAL)
        {
            RMapMatchNode.RMapTag = u4FsRMapMatchTag;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_TAG;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4FsRMapMatchMetricType != 0)
    {
        if ((i4FsRMapMatchMetricType == RMAP_METRIC_TYPE_INTRA_AREA) ||
            (i4FsRMapMatchMetricType == RMAP_METRIC_TYPE_INTER_AREA) ||
            (i4FsRMapMatchMetricType == RMAP_METRIC_TYPE_TYPE1EXTERNAL) ||
            (i4FsRMapMatchMetricType == RMAP_METRIC_TYPE_TYPE2EXTERNAL))
        {
            RMapMatchNode.RMapMetricType = (UINT1)i4FsRMapMatchMetricType;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_METRIC_TYPE;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4FsRMapMatchRouteType != 0)
    {
        if ((i4FsRMapMatchRouteType == RMAP_ROUTE_TYPE_LOCAL) ||
            (i4FsRMapMatchRouteType == RMAP_ROUTE_TYPE_REMOTE))
        {
            RMapMatchNode.RMapRouteType = i4FsRMapMatchRouteType;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_ROUTE_TYPE;
            i1RetVal = SNMP_SUCCESS;
        }
    }

    else if (u4FsRMapMatchASPathTag != 0)
    {
        RMapMatchNode.RMapNextHopAS = u4FsRMapMatchASPathTag;
        RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_ASPATH_TAG;
        i1RetVal = SNMP_SUCCESS;
    }
    else if (u4FsRMapMatchCommunity != 0)
    {
        if (u4FsRMapMatchCommunity > RMAP_ZERO)
        {
            RMapMatchNode.RMapCommunity = u4FsRMapMatchCommunity;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_COMMUNITY;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4FsRMapMatchLocalPref != 0)
    {
        /* (i4FsRMapMatchLocalPref <= RMAP_MAX_LOCAl_PREF_VAL)
         * removed the above if check to resolve coverity warning 
         * as the value of RMAP_MAX_LOCAl_PREF_VAL is 0x7fffffff 
         * and maximum value of INT4 is 0x7fffffff, the result
         * of the expression will always be true*/
        if (i4FsRMapMatchLocalPref > 0)
        {
            RMapMatchNode.RMapLocalPref = i4FsRMapMatchLocalPref;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_LOCAL_PREF;
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else if (i4FsRMapMatchOrigin != 0)
    {
        if ((i4FsRMapMatchOrigin == RMAP_ORIGIN_IGP) ||
            (i4FsRMapMatchOrigin == RMAP_ORIGIN_EGP) ||
            (i4FsRMapMatchOrigin == RMAP_ORIGIN_INCOMPLETE))
        {
            RMapMatchNode.RMapOrigin = i4FsRMapMatchOrigin;
            RMapMatchNode.u1Cmd = RMAP_MATCH_CMD_ORIGIN;
            i1RetVal = SNMP_SUCCESS;
        }
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        /* Get route map match entry by the given template */
        pRMapMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &RMapMatchNode);
        if (pRMapMatchNode == NULL)
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Route Map MATCH Entry not available.\n");
            i1RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Wrong Indices for Route Map MATCH Table.\n");
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRMapMatchTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRMapMatchTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                  UINT4 *pu4FsRMapSeqNum,
                                  INT4 *pi4FsRMapMatchDestInetType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsRMapMatchDestInetAddress,
                                  UINT4 *pu4FsRMapMatchDestInetPrefix,
                                  INT4 *pi4FsRMapMatchSourceInetType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsRMapMatchSourceInetAddress,
                                  UINT4 *pu4FsRMapMatchSourceInetPrefix,
                                  INT4 *pi4FsRMapMatchNextHopInetType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsRMapMatchNextHopInetAddr,
                                  INT4 *pi4FsRMapMatchInterface,
                                  INT4 *pi4FsRMapMatchMetric,
                                  UINT4 *pu4FsRMapMatchTag,
                                  INT4 *pi4FsRMapMatchMetricType,
                                  INT4 *pi4FsRMapMatchRouteType,
                                  UINT4 *pu4FsRMapMatchASPathTag,
                                  UINT4 *pu4FsRMapMatchCommunity,
                                  INT4 *pi4FsRMapMatchLocalPref,
                                  INT4 *pi4FsRMapMatchOrigin)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapNode          *pRMapNextNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    UINT4               u4RMapCount = 0;
    BOOL1               bFlag = TRUE;

    /* clear rmap name & seq num */
    RMAP_CLEAR_OCT (pFsRMapName, RMAP_MAX_NAME_LEN);
    *pu4FsRMapSeqNum = RMAP_ZERO;

    /* get 1st MAP node */
    pRMapNode = (tRMapNode *) RBTreeGetFirst (gRMapGlobalInfo.RMapRoot);
    if (pRMapNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "No Route Map exists%s\n");
        return SNMP_FAILURE;
    }

    if (RBTreeCount (gRMapGlobalInfo.RMapRoot, &u4RMapCount) == RB_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "No Map Entry in the RB tree .\n");
        return SNMP_FAILURE;
    }

    /* through all MAP nodes find 1st one with existing MATCH node */
    while (u4RMapCount > RMAP_ZERO)
    {
        /* get 1st MATCH node for current MAP node */
        pRMapMatchNode =
            (tRMapMatchNode *) RBTreeGetFirst (pRMapNode->RMapMatchRoot);
        if (pRMapMatchNode == NULL)
        {
            /* if no MATCH node - goto next MAP node */
            pRMapNextNode =
                (tRMapNode *) RBTreeGetNext (gRMapGlobalInfo.RMapRoot,
                                             pRMapNode, NULL);
            if (pRMapNextNode == NULL)
            {
                bFlag = FALSE;
                break;
            }
            pRMapNode = pRMapNextNode;
            u4RMapCount--;
            continue;
        }
        break;
    }

    if (bFlag == FALSE || pRMapMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "No Route Map Match Entry Exists%s\n");
        return SNMP_FAILURE;
    }

    /* here we've found map with MATCH node, store its name */
    RMAP_ASCIIZ_2_OCT (pFsRMapName, pRMapNode->au1RMapName);
    *pu4FsRMapSeqNum = pRMapNode->u4SeqNo;

    /* convert found MATCH node to output params */
    RMapMatchNodeToParams (pi4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           pu4FsRMapMatchDestInetPrefix,
                           pi4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           pu4FsRMapMatchSourceInetPrefix,
                           pi4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           pi4FsRMapMatchInterface,
                           pi4FsRMapMatchMetric,
                           pu4FsRMapMatchTag,
                           pi4FsRMapMatchMetricType,
                           pi4FsRMapMatchRouteType,
                           pu4FsRMapMatchASPathTag,
                           pu4FsRMapMatchCommunity,
                           pi4FsRMapMatchLocalPref,
                           pi4FsRMapMatchOrigin, pRMapMatchNode);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRMapMatchTable
 Input       :  The Indices
                FsRMapName
                nextFsRMapName
                FsRMapSeqNum
                nextFsRMapSeqNum
                FsRMapMatchDestInetType
                nextFsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                nextFsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                nextFsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                nextFsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                nextFsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                nextFsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                nextFsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                nextFsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                nextFsRMapMatchInterface
                FsRMapMatchMetric
                nextFsRMapMatchMetric
                FsRMapMatchTag
                nextFsRMapMatchTag
                FsRMapMatchMetricType
                nextFsRMapMatchMetricType
                FsRMapMatchRouteType
                nextFsRMapMatchRouteType
                FsRMapMatchASPathTag
                nextFsRMapMatchASPathTag
                FsRMapMatchCommunity
                nextFsRMapMatchCommunity
                FsRMapMatchLocalPref
                nextFsRMapMatchLocalPref
                FsRMapMatchOrigin
                nextFsRMapMatchOrigin
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRMapMatchTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsRMapName,
                                 UINT4 u4FsRMapSeqNum,
                                 UINT4 *pu4NextFsRMapSeqNum,
                                 INT4 i4FsRMapMatchDestInetType,
                                 INT4 *pi4NextFsRMapMatchDestInetType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsRMapMatchDestInetAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsRMapMatchDestInetAddress,
                                 UINT4 u4FsRMapMatchDestInetPrefix,
                                 UINT4 *pu4NextFsRMapMatchDestInetPrefix,
                                 INT4 i4FsRMapMatchSourceInetType,
                                 INT4 *pi4NextFsRMapMatchSourceInetType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsRMapMatchSourceInetAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsRMapMatchSourceInetAddress,
                                 UINT4 u4FsRMapMatchSourceInetPrefix,
                                 UINT4 *pu4NextFsRMapMatchSourceInetPrefix,
                                 INT4 i4FsRMapMatchNextHopInetType,
                                 INT4 *pi4NextFsRMapMatchNextHopInetType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsRMapMatchNextHopInetAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsRMapMatchNextHopInetAddr,
                                 INT4 i4FsRMapMatchInterface,
                                 INT4 *pi4NextFsRMapMatchInterface,
                                 INT4 i4FsRMapMatchMetric,
                                 INT4 *pi4NextFsRMapMatchMetric,
                                 UINT4 u4FsRMapMatchTag,
                                 UINT4 *pu4NextFsRMapMatchTag,
                                 INT4 i4FsRMapMatchMetricType,
                                 INT4 *pi4NextFsRMapMatchMetricType,
                                 INT4 i4FsRMapMatchRouteType,
                                 INT4 *pi4NextFsRMapMatchRouteType,
                                 UINT4 u4FsRMapMatchASPathTag,
                                 UINT4 *pu4NextFsRMapMatchASPathTag,
                                 UINT4 u4FsRMapMatchCommunity,
                                 UINT4 *pu4NextFsRMapMatchCommunity,
                                 INT4 i4FsRMapMatchLocalPref,
                                 INT4 *pi4NextFsRMapMatchLocalPref,
                                 INT4 i4FsRMapMatchOrigin,
                                 INT4 *pi4NextFsRMapMatchOrigin)
{
    tRMapNode          *pMapNode;
    tRMapMatchNode      MatchNode, *pNextMatchNode;

    /* get pointer on MAP-node inside tree */
    pMapNode = RMapGetRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);

    if (pMapNode == NULL)
    {
        /* Get the Next Route Map Node */
        pMapNode = RMapGetNextRouteMapNodeOct (pFsRMapName, u4FsRMapSeqNum);
        if (pMapNode == NULL)
        {
            /* no more MAP nodes */
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Get Next Route Map MATCH failed, couldn't get next route map.\n");
            return SNMP_FAILURE;
        }
    }

    /* suppose we won't leave this MAP-node, store map-name, seqnum */
    RMAP_ASCIIZ_2_OCT (pNextFsRMapName, pMapNode->au1RMapName);
    *pu4NextFsRMapSeqNum = pMapNode->u4SeqNo;

    /* fill template MATCH node for given indices */
    RMapParamsToMatchNode (&MatchNode, pMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get the next MATCH-node inside current MAP node. */
    pNextMatchNode = (tRMapMatchNode *) RBTreeGetNext
        (pMapNode->RMapMatchRoot, &MatchNode, NULL);

    /* If no more MATCH-nodes for this MAP-node,
       get the next MAP entry containing MATCH entry in it. */
    while (pNextMatchNode == NULL)
    {
        /* get the next MAP entry */
        pMapNode = (tRMapNode *) RBTreeGetNext
            (gRMapGlobalInfo.RMapRoot, pMapNode, NULL);

        /* If no more MAP-nodes - stop the job */
        if (pMapNode == NULL)
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Get Next Route Map MATCH failed.\n");
            return SNMP_FAILURE;
        }

        /* store new map-name, seqnum */
        RMAP_ASCIIZ_2_OCT (pNextFsRMapName, pMapNode->au1RMapName);
        *pu4NextFsRMapSeqNum = pMapNode->u4SeqNo;

        /* fetch first MATCH entry for this MAP-node */
        pNextMatchNode = RBTreeGetFirst (pMapNode->RMapMatchRoot);
    }

    /* convert found MATCH node to output params */
    RMapMatchNodeToParams (pi4NextFsRMapMatchDestInetType,
                           pNextFsRMapMatchDestInetAddress,
                           pu4NextFsRMapMatchDestInetPrefix,
                           pi4NextFsRMapMatchSourceInetType,
                           pNextFsRMapMatchSourceInetAddress,
                           pu4NextFsRMapMatchSourceInetPrefix,
                           pi4NextFsRMapMatchNextHopInetType,
                           pNextFsRMapMatchNextHopInetAddr,
                           pi4NextFsRMapMatchInterface,
                           pi4NextFsRMapMatchMetric,
                           pu4NextFsRMapMatchTag,
                           pi4NextFsRMapMatchMetricType,
                           pi4NextFsRMapMatchRouteType,
                           pu4NextFsRMapMatchASPathTag,
                           pu4NextFsRMapMatchCommunity,
                           pi4NextFsRMapMatchLocalPref,
                           pi4NextFsRMapMatchOrigin, pNextMatchNode);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRMapMatchRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                retValFsRMapMatchRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapMatchRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            INT4 i4FsRMapMatchDestInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchDestInetAddress,
                            UINT4 u4FsRMapMatchDestInetPrefix,
                            INT4 i4FsRMapMatchSourceInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchSourceInetAddress,
                            UINT4 u4FsRMapMatchSourceInetPrefix,
                            INT4 i4FsRMapMatchNextHopInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchNextHopInetAddr,
                            INT4 i4FsRMapMatchInterface,
                            INT4 i4FsRMapMatchMetric, UINT4 u4FsRMapMatchTag,
                            INT4 i4FsRMapMatchMetricType,
                            INT4 i4FsRMapMatchRouteType,
                            UINT4 u4FsRMapMatchASPathTag,
                            UINT4 u4FsRMapMatchCommunity,
                            INT4 i4FsRMapMatchLocalPref,
                            INT4 i4FsRMapMatchOrigin,
                            INT4 *pi4RetValFsRMapMatchRowStatus)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "GetRouteMapMatchRowStatus failed - couldn't get the"
                       "Route map Entry %s.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node for given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "GetRouteMapMatchRowStatus failed -Route Map MATCH Entry"
                  "not available.\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsRMapMatchRowStatus = pMatchNode->u1RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRMapMatchDestMaxPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                retValFsRMapMatchDestMaxPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapMatchDestMaxPrefixLen (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   INT4 i4FsRMapMatchDestInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchDestInetAddress,
                                   UINT4 u4FsRMapMatchDestInetPrefix,
                                   INT4 i4FsRMapMatchSourceInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchSourceInetAddress,
                                   UINT4 u4FsRMapMatchSourceInetPrefix,
                                   INT4 i4FsRMapMatchNextHopInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchNextHopInetAddr,
                                   INT4 i4FsRMapMatchInterface,
                                   INT4 i4FsRMapMatchMetric,
                                   UINT4 u4FsRMapMatchTag,
                                   INT4 i4FsRMapMatchMetricType,
                                   INT4 i4FsRMapMatchRouteType,
                                   UINT4 u4FsRMapMatchASPathTag,
                                   UINT4 u4FsRMapMatchCommunity,
                                   INT4 i4FsRMapMatchLocalPref,
                                   INT4 i4FsRMapMatchOrigin,
                                   UINT4 *pu4RetValFsRMapMatchDestMaxPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "GetRouteMapMatchRowStatus failed - couldn't get the"
                       "Route map Entry %s.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node for given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map MATCH Entry not available.\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsRMapMatchDestMaxPrefixLen = (UINT4) pMatchNode->IpPrefixMaxLen;

    return SNMP_SUCCESS;
}

/****************************************************************************                                      Function    :  nmhGetFsRMapMatchDestMinPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                retValFsRMapMatchDestMinPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapMatchDestMinPrefixLen (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   INT4 i4FsRMapMatchDestInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchDestInetAddress,
                                   UINT4 u4FsRMapMatchDestInetPrefix,
                                   INT4 i4FsRMapMatchSourceInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchSourceInetAddress,
                                   UINT4 u4FsRMapMatchSourceInetPrefix,
                                   INT4 i4FsRMapMatchNextHopInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchNextHopInetAddr,
                                   INT4 i4FsRMapMatchInterface,
                                   INT4 i4FsRMapMatchMetric,
                                   UINT4 u4FsRMapMatchTag,
                                   INT4 i4FsRMapMatchMetricType,
                                   INT4 i4FsRMapMatchRouteType,
                                   UINT4 u4FsRMapMatchASPathTag,
                                   UINT4 u4FsRMapMatchCommunity,
                                   INT4 i4FsRMapMatchLocalPref,
                                   INT4 i4FsRMapMatchOrigin,
                                   UINT4 *pu4RetValFsRMapMatchDestMinPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "GetRouteMapMatchRowStatus failed - couldn't get the"
                       "Route map Entry %s.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node for given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map MATCH Entry not available.\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsRMapMatchDestMinPrefixLen = (UINT4) pMatchNode->IpPrefixMinLen;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRMapMatchRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                setValFsRMapMatchRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapMatchRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            INT4 i4FsRMapMatchDestInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchDestInetAddress,
                            UINT4 u4FsRMapMatchDestInetPrefix,
                            INT4 i4FsRMapMatchSourceInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchSourceInetAddress,
                            UINT4 u4FsRMapMatchSourceInetPrefix,
                            INT4 i4FsRMapMatchNextHopInetType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsRMapMatchNextHopInetAddr,
                            INT4 i4FsRMapMatchInterface,
                            INT4 i4FsRMapMatchMetric, UINT4 u4FsRMapMatchTag,
                            INT4 i4FsRMapMatchMetricType,
                            INT4 i4FsRMapMatchRouteType,
                            UINT4 u4FsRMapMatchASPathTag,
                            UINT4 u4FsRMapMatchCommunity,
                            INT4 i4FsRMapMatchLocalPref,
                            INT4 i4FsRMapMatchOrigin,
                            INT4 i4SetValFsRMapMatchRowStatus)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE RMapMatchAddress;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "SetRMapMatchRowStatus failed - Route Map Entry %s not available.\n",
                       au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node with given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    /* if no MATCH node found - create it */
    if (((i4SetValFsRMapMatchRowStatus == RMAP_CREATE_AND_GO) ||
         (i4SetValFsRMapMatchRowStatus == RMAP_CREATE_AND_WAIT))
        && (pMatchNode == NULL))
    {
        /* The Row status could be CREATE AND WAIT */
        /* we need to create the node and add it to the Route Map RBTree */
        pMatchNode = RMapAddMatchNode (pRMapNode, &MatchNode);

        if (pMatchNode != NULL)
        {
            if (i4SetValFsRMapMatchRowStatus == RMAP_CREATE_AND_WAIT)
            {
                pMatchNode->u1RowStatus = RMAP_NOT_READY;
            }
            else if (i4SetValFsRMapMatchRowStatus == RMAP_CREATE_AND_GO)
            {
                pMatchNode->u1RowStatus = RMAP_ACTIVE;

                /* Send the update message to RTM4/RTM6 */
                RMapSendUpdateMsg (au1Name);

            }
        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "Set Route Map MATCH Row Status failure - Match Node"
                      "Creation Failed\n");

            return SNMP_FAILURE;
        }
    }

    if ((pMatchNode != NULL) && (i4SetValFsRMapMatchRowStatus == RMAP_ACTIVE))
    {
        pMatchNode->u1RowStatus = (UINT1) i4SetValFsRMapMatchRowStatus;

        /* Send the update message to RTM4/RTM6 */
        RMapSendUpdateMsg (au1Name);
    }

    if ((pMatchNode != NULL) && (i4SetValFsRMapMatchRowStatus == RMAP_DESTROY))
    {
        pMatchNode->u1RowStatus = (UINT1) i4SetValFsRMapMatchRowStatus;

        /* Send the update message to RTM4/RTM6 */
        RMapSendUpdateMsg (au1Name);

        RMapProcessRouteMapUpdate (au1Name);
    }

    if ((pMatchNode != NULL) &&
        (i4SetValFsRMapMatchRowStatus == RMAP_NOT_IN_SERVICE))
    {
        pMatchNode->u1RowStatus = (UINT1) i4SetValFsRMapMatchRowStatus;

        /* Send the update message to RTM4/RTM6 */
        RMapSendUpdateMsg (au1Name);
        RMapProcessRouteMapUpdate (au1Name);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsRMapMatchRowStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsRMapMatchRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RMapLockWrite;
    SnmpNotifyInfo.pUnLockPointer = RMapUnLockWrite;
    SnmpNotifyInfo.u4Indices = 19;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    MEMSET (au1InetAddrBuf, 0, sizeof (au1InetAddrBuf));
    RMAP_INIT_OCT (RMapMatchAddress, au1InetAddrBuf);
    if (i4FsRMapMatchDestInetType == 0)
    {
        pFsRMapMatchDestInetAddress = &RMapMatchAddress;
    }

    if (i4FsRMapMatchSourceInetType == 0)
    {
        pFsRMapMatchSourceInetAddress = &RMapMatchAddress;
    }

    if (i4FsRMapMatchNextHopInetType == 0)
    {
        pFsRMapMatchNextHopInetAddr = &RMapMatchAddress;
    }
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo,
                      "%s %u %i %s %u %i %s %u %i %s %i %i %u %i %i %u %u %i %i %i",
                      pFsRMapName, u4FsRMapSeqNum, i4FsRMapMatchDestInetType,
                      pFsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix,
                      i4FsRMapMatchSourceInetType,
                      pFsRMapMatchSourceInetAddress,
                      u4FsRMapMatchSourceInetPrefix,
                      i4FsRMapMatchNextHopInetType, pFsRMapMatchNextHopInetAddr,
                      i4FsRMapMatchInterface, i4FsRMapMatchMetric,
                      u4FsRMapMatchTag, i4FsRMapMatchMetricType,
                      i4FsRMapMatchRouteType, u4FsRMapMatchASPathTag,
                      u4FsRMapMatchCommunity, i4FsRMapMatchLocalPref,
                      i4FsRMapMatchOrigin, i4SetValFsRMapMatchRowStatus));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRMapMatchDestMaxPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                setValFsRMapMatchDestMaxPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapMatchDestMaxPrefixLen (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   INT4 i4FsRMapMatchDestInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchDestInetAddress,
                                   UINT4 u4FsRMapMatchDestInetPrefix,
                                   INT4 i4FsRMapMatchSourceInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchSourceInetAddress,
                                   UINT4 u4FsRMapMatchSourceInetPrefix,
                                   INT4 i4FsRMapMatchNextHopInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchNextHopInetAddr,
                                   INT4 i4FsRMapMatchInterface,
                                   INT4 i4FsRMapMatchMetric,
                                   UINT4 u4FsRMapMatchTag,
                                   INT4 i4FsRMapMatchMetricType,
                                   INT4 i4FsRMapMatchRouteType,
                                   UINT4 u4FsRMapMatchASPathTag,
                                   UINT4 u4FsRMapMatchCommunity,
                                   INT4 i4FsRMapMatchLocalPref,
                                   INT4 i4FsRMapMatchOrigin,
                                   UINT4 u4SetValFsRMapMatchDestMaxPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE RMapMatchAddress;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map Entry %s not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node with given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Match node not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    pMatchNode->IpPrefixMaxLen = (UINT1) u4SetValFsRMapMatchDestMaxPrefixLen;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsRMapMatchDestMaxPrefixLen;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsRMapMatchDestMaxPrefixLen) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RMapLockWrite;
    SnmpNotifyInfo.pUnLockPointer = RMapUnLockWrite;
    SnmpNotifyInfo.u4Indices = 19;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    MEMSET (au1InetAddrBuf, 0, sizeof (au1InetAddrBuf));
    RMAP_INIT_OCT (RMapMatchAddress, au1InetAddrBuf);

    pFsRMapMatchSourceInetAddress = &RMapMatchAddress;

    pFsRMapMatchNextHopInetAddr = &RMapMatchAddress;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo,
                      "%s %u %i %s %u %i %s %u %i %s %i %i %u %i %i %u %u %i %i %u",
                      pFsRMapName, u4FsRMapSeqNum, i4FsRMapMatchDestInetType,
                      pFsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix,
                      i4FsRMapMatchSourceInetType,
                      pFsRMapMatchSourceInetAddress,
                      u4FsRMapMatchSourceInetPrefix,
                      i4FsRMapMatchNextHopInetType, pFsRMapMatchNextHopInetAddr,
                      i4FsRMapMatchInterface, i4FsRMapMatchMetric,
                      u4FsRMapMatchTag, i4FsRMapMatchMetricType,
                      i4FsRMapMatchRouteType, u4FsRMapMatchASPathTag,
                      u4FsRMapMatchCommunity, i4FsRMapMatchLocalPref,
                      i4FsRMapMatchOrigin,
                      u4SetValFsRMapMatchDestMaxPrefixLen));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRMapMatchDestMinPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                setValFsRMapMatchDestMinPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapMatchDestMinPrefixLen (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   INT4 i4FsRMapMatchDestInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchDestInetAddress,
                                   UINT4 u4FsRMapMatchDestInetPrefix,
                                   INT4 i4FsRMapMatchSourceInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchSourceInetAddress,
                                   UINT4 u4FsRMapMatchSourceInetPrefix,
                                   INT4 i4FsRMapMatchNextHopInetType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsRMapMatchNextHopInetAddr,
                                   INT4 i4FsRMapMatchInterface,
                                   INT4 i4FsRMapMatchMetric,
                                   UINT4 u4FsRMapMatchTag,
                                   INT4 i4FsRMapMatchMetricType,
                                   INT4 i4FsRMapMatchRouteType,
                                   UINT4 u4FsRMapMatchASPathTag,
                                   UINT4 u4FsRMapMatchCommunity,
                                   INT4 i4FsRMapMatchLocalPref,
                                   INT4 i4FsRMapMatchOrigin,
                                   UINT4 u4SetValFsRMapMatchDestMinPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE RMapMatchAddress;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map Entry %s not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* fill template MATCH node with given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Match node not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    pMatchNode->IpPrefixMinLen = (UINT1) u4SetValFsRMapMatchDestMinPrefixLen;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsRMapMatchDestMinPrefixLen;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsRMapMatchDestMinPrefixLen) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RMapLockWrite;
    SnmpNotifyInfo.pUnLockPointer = RMapUnLockWrite;
    SnmpNotifyInfo.u4Indices = 19;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    MEMSET (au1InetAddrBuf, 0, sizeof (au1InetAddrBuf));
    RMAP_INIT_OCT (RMapMatchAddress, au1InetAddrBuf);

    pFsRMapMatchSourceInetAddress = &RMapMatchAddress;
    pFsRMapMatchNextHopInetAddr = &RMapMatchAddress;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo,
                      "%s %u %i %s %u %i %s %u %i %s %i %i %u %i %i %u %u %i %i %u",
                      pFsRMapName, u4FsRMapSeqNum, i4FsRMapMatchDestInetType,
                      pFsRMapMatchDestInetAddress, u4FsRMapMatchDestInetPrefix,
                      i4FsRMapMatchSourceInetType,
                      pFsRMapMatchSourceInetAddress,
                      u4FsRMapMatchSourceInetPrefix,
                      i4FsRMapMatchNextHopInetType, pFsRMapMatchNextHopInetAddr,
                      i4FsRMapMatchInterface, i4FsRMapMatchMetric,
                      u4FsRMapMatchTag, i4FsRMapMatchMetricType,
                      i4FsRMapMatchRouteType, u4FsRMapMatchASPathTag,
                      u4FsRMapMatchCommunity, i4FsRMapMatchLocalPref,
                      i4FsRMapMatchOrigin,
                      u4SetValFsRMapMatchDestMinPrefixLen));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRMapMatchRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                testValFsRMapMatchRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapMatchRowStatus (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                               UINT4 u4FsRMapSeqNum,
                               INT4 i4FsRMapMatchDestInetType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsRMapMatchDestInetAddress,
                               UINT4 u4FsRMapMatchDestInetPrefix,
                               INT4 i4FsRMapMatchSourceInetType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsRMapMatchSourceInetAddress,
                               UINT4 u4FsRMapMatchSourceInetPrefix,
                               INT4 i4FsRMapMatchNextHopInetType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsRMapMatchNextHopInetAddr,
                               INT4 i4FsRMapMatchInterface,
                               INT4 i4FsRMapMatchMetric, UINT4 u4FsRMapMatchTag,
                               INT4 i4FsRMapMatchMetricType,
                               INT4 i4FsRMapMatchRouteType,
                               UINT4 u4FsRMapMatchASPathTag,
                               UINT4 u4FsRMapMatchCommunity,
                               INT4 i4FsRMapMatchLocalPref,
                               INT4 i4FsRMapMatchOrigin,
                               INT4 i4TestValFsRMapMatchRowStatus)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    tCfaIfInfo          CfaIfInfo;
    INT1                i1RetVal = SNMP_FAILURE;
    tIPvXAddr           DstXAddr;
    tIPvXAddr           SrcXAddr;

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhTestv2FsRMapMatchRowStatus failed - Route Map Entry %s not available.\n",
                       au1Name);
        return SNMP_FAILURE;
    }

    if (pRMapNode->u1IsIpPrefixInfo != TRUE)
    {
        /* Validate Destination Address */
        IPVX_ADDR_INIT (DstXAddr,
                        i4FsRMapMatchDestInetType,
                        pFsRMapMatchDestInetAddress->pu1_OctetList);
        if (ValidateIpv4Addr (&DstXAddr, u4FsRMapMatchDestInetPrefix) !=
            RMAP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP_ADDRESS);
            return SNMP_FAILURE;
        }

        /* Validate Source Address */
        IPVX_ADDR_INIT (SrcXAddr,
                        i4FsRMapMatchSourceInetType,
                        pFsRMapMatchSourceInetAddress->pu1_OctetList);
        if (ValidateIpv4Addr (&SrcXAddr, u4FsRMapMatchSourceInetPrefix) !=
            RMAP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP_ADDRESS);
            return SNMP_FAILURE;
        }
    }

    /* test interface index */
    if (i4FsRMapMatchInterface != 0)
    {
        CfaGetIfInfo ((UINT4) i4FsRMapMatchInterface, &CfaIfInfo);
        if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
              ((CfaIfInfo.u1IfType == CFA_ENET) &&
               (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (RMAP_CLI_ERR_INVALID_INTERFACE_TYPE);
            return SNMP_FAILURE;
        }
    }

    /* fill template MATCH node with given indices */
    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode != NULL)
    {
        if ((pRMapNode->u1IsIpPrefixInfo == TRUE)
            && (i4TestValFsRMapMatchRowStatus == RMAP_ACTIVE))
        {
            if ((pMatchNode->IpPrefixMinLen != 0)
                && (pMatchNode->IpPrefixMaxLen != 0))
            {
                if (pMatchNode->IpPrefixMinLen > pMatchNode->IpPrefixMaxLen)
                {
                    CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
                    return SNMP_FAILURE;
                }
            }
        }

        if (((i4TestValFsRMapMatchRowStatus > RMAP_ACTIVE) ||
             (i4TestValFsRMapMatchRowStatus < RMAP_DESTROY))
            && (i4TestValFsRMapMatchRowStatus != RMAP_CREATE_AND_WAIT))
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhTestv2FsRMapMatchRowStatus failed -Incorrect Route Map"
                      "MATCH Row Status \n");
        }
    }
    else if ((pMatchNode == NULL) &&
             (i4TestValFsRMapMatchRowStatus == RMAP_CREATE_AND_WAIT))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "nmhTestv2FsRMapMatchRowStatus failed - RMap MATCH entry not"
                  " available.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapMatchDestMaxPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                testValFsRMapMatchDestMaxPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapMatchDestMaxPrefixLen (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                      UINT4 u4FsRMapSeqNum,
                                      INT4 i4FsRMapMatchDestInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchDestInetAddress,
                                      UINT4 u4FsRMapMatchDestInetPrefix,
                                      INT4 i4FsRMapMatchSourceInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchSourceInetAddress,
                                      UINT4 u4FsRMapMatchSourceInetPrefix,
                                      INT4 i4FsRMapMatchNextHopInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchNextHopInetAddr,
                                      INT4 i4FsRMapMatchInterface,
                                      INT4 i4FsRMapMatchMetric,
                                      UINT4 u4FsRMapMatchTag,
                                      INT4 i4FsRMapMatchMetricType,
                                      INT4 i4FsRMapMatchRouteType,
                                      UINT4 u4FsRMapMatchASPathTag,
                                      UINT4 u4FsRMapMatchCommunity,
                                      INT4 i4FsRMapMatchLocalPref,
                                      INT4 i4FsRMapMatchOrigin,
                                      UINT4
                                      u4TestValFsRMapMatchDestMaxPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map Entry %s not available.\n", au1Name);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pRMapNode->u1IsIpPrefixInfo != TRUE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Max Prefix length should be configured only for IP Prefix entry\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Match node not exists\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsRMapMatchDestMaxPrefixLen <= u4FsRMapMatchDestInetPrefix)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Max Prefix length should be greater than Prefix length\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsRMapMatchDestInetType == IPVX_ADDR_FMLY_IPV4) &&
        (u4TestValFsRMapMatchDestMaxPrefixLen > RMAP_MAX_IPV4_PREFIX_LEN))
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Max Prefix length should not be greater than maximum prefix"
                  "length for the particular address family\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4FsRMapMatchDestInetType == IPVX_ADDR_FMLY_IPV6) &&
        (u4TestValFsRMapMatchDestMaxPrefixLen > RMAP_MAX_IPV6_PREFIX_LEN))
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Max Prefix length should not be greater than maximum prefix"
                  "length for the particular address family\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapMatchDestMinPrefixLen
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin

                The Object
                testValFsRMapMatchDestMinPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapMatchDestMinPrefixLen (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                      UINT4 u4FsRMapSeqNum,
                                      INT4 i4FsRMapMatchDestInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchDestInetAddress,
                                      UINT4 u4FsRMapMatchDestInetPrefix,
                                      INT4 i4FsRMapMatchSourceInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchSourceInetAddress,
                                      UINT4 u4FsRMapMatchSourceInetPrefix,
                                      INT4 i4FsRMapMatchNextHopInetType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRMapMatchNextHopInetAddr,
                                      INT4 i4FsRMapMatchInterface,
                                      INT4 i4FsRMapMatchMetric,
                                      UINT4 u4FsRMapMatchTag,
                                      INT4 i4FsRMapMatchMetricType,
                                      INT4 i4FsRMapMatchRouteType,
                                      UINT4 u4FsRMapMatchASPathTag,
                                      UINT4 u4FsRMapMatchCommunity,
                                      INT4 i4FsRMapMatchLocalPref,
                                      INT4 i4FsRMapMatchOrigin,
                                      UINT4
                                      u4TestValFsRMapMatchDestMinPrefixLen)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode      MatchNode, *pMatchNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Route Map Entry %s not available.\n", au1Name);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pRMapNode->u1IsIpPrefixInfo != TRUE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Min Prefix length should be configured only for IP Prefix entry\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RMapParamsToMatchNode (&MatchNode, pRMapNode->u1IsIpPrefixInfo,
                           i4FsRMapMatchDestInetType,
                           pFsRMapMatchDestInetAddress,
                           u4FsRMapMatchDestInetPrefix,
                           i4FsRMapMatchSourceInetType,
                           pFsRMapMatchSourceInetAddress,
                           u4FsRMapMatchSourceInetPrefix,
                           i4FsRMapMatchNextHopInetType,
                           pFsRMapMatchNextHopInetAddr,
                           i4FsRMapMatchInterface,
                           i4FsRMapMatchMetric,
                           u4FsRMapMatchTag,
                           i4FsRMapMatchMetricType,
                           i4FsRMapMatchRouteType,
                           u4FsRMapMatchASPathTag,
                           u4FsRMapMatchCommunity,
                           i4FsRMapMatchLocalPref, i4FsRMapMatchOrigin);

    /* Get MATCH entry */
    pMatchNode = RBTreeGet (pRMapNode->RMapMatchRoot, &MatchNode);

    if (pMatchNode == NULL)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Match node not exists\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsRMapMatchDestMinPrefixLen <= u4FsRMapMatchDestInetPrefix)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Min Prefix length should be greater than Prefix length\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RMAP_CLI_MIN_PREFIX_LEN_INVALID);
        return SNMP_FAILURE;
    }

    if ((i4FsRMapMatchDestInetType == IPVX_ADDR_FMLY_IPV4) &&
        (u4TestValFsRMapMatchDestMinPrefixLen > RMAP_MAX_IPV4_PREFIX_LEN))
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Min Prefix length should not be greater than maximum prefix"
                  "length for the particular address family\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RMAP_CLI_MIN_PREFIX_LEN_INVALID);
        return SNMP_FAILURE;
    }
    if ((i4FsRMapMatchDestInetType == IPVX_ADDR_FMLY_IPV6) &&
        (u4TestValFsRMapMatchDestMinPrefixLen > RMAP_MAX_IPV6_PREFIX_LEN))
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Min Prefix length should not be greater than maximum prefix"
                  "length for the particular address family\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RMAP_CLI_MIN_PREFIX_LEN_INVALID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRMapMatchTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
                FsRMapMatchDestInetType
                FsRMapMatchDestInetAddress
                FsRMapMatchDestInetPrefix
                FsRMapMatchSourceInetType
                FsRMapMatchSourceInetAddress
                FsRMapMatchSourceInetPrefix
                FsRMapMatchNextHopInetType
                FsRMapMatchNextHopInetAddr
                FsRMapMatchInterface
                FsRMapMatchMetric
                FsRMapMatchTag
                FsRMapMatchMetricType
                FsRMapMatchRouteType
                FsRMapMatchASPathTag
                FsRMapMatchCommunity
                FsRMapMatchLocalPref
                FsRMapMatchOrigin
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRMapMatchTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRMapSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRMapSetTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRMapSetTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                        UINT4 u4FsRMapSeqNum)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((pFsRMapName->i4_Length <= RMAP_MAX_NAME_LEN) &&
        (u4FsRMapSeqNum <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "ValidateIndexRMapSetTable failed - Error in Route Map"
                  "Indices\n");
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRMapSetTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRMapSetTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                UINT4 *pu4FsRMapSeqNum)
{
    tRMapNode          *pRMapNode;
    tRMapNode          *pRMapNextNode;
    UINT4               u4RMapCount = 0;
    BOOL1               bFlag = TRUE;

    /* clear output params */
    RMAP_CLEAR_OCT (pFsRMapName, RMAP_MAX_NAME_LEN);
    *pu4FsRMapSeqNum = RMAP_ZERO;

    /* get 1st MAP node */
    pRMapNode = (tRMapNode *) RBTreeGetFirst (gRMapGlobalInfo.RMapRoot);
    if ((pRMapNode == NULL))
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
        return SNMP_FAILURE;
    }

    if (RBTreeCount (gRMapGlobalInfo.RMapRoot, &u4RMapCount) == RB_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "No Map Entry in the RB tree .\n");
        return SNMP_FAILURE;
    }

    /* through all MAP nodes find 1st one with SET node */
    while (u4RMapCount > RMAP_ZERO)
    {
        if (pRMapNode->pSetNode == NULL)
        {
            pRMapNextNode =
                (tRMapNode *) RBTreeGetNext (gRMapGlobalInfo.RMapRoot,
                                             pRMapNode, NULL);
            if (pRMapNextNode == NULL)
            {
                bFlag = FALSE;
                break;
            }
            pRMapNode = pRMapNextNode;
            u4RMapCount--;
            continue;
        }
        break;
    }

    if (bFlag == FALSE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "Route Map Entry not available.\n");
        return SNMP_FAILURE;
    }

    /* store output params */
    RMAP_ASCIIZ_2_OCT (pFsRMapName, pRMapNode->au1RMapName);
    *pu4FsRMapSeqNum = pRMapNode->u4SeqNo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRMapSetTable
 Input       :  The Indices
                FsRMapName
                nextFsRMapName
                FsRMapSeqNum
                nextFsRMapSeqNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRMapSetTable (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                               tSNMP_OCTET_STRING_TYPE * pNextFsRMapName,
                               UINT4 u4FsRMapSeqNum, UINT4 *pu4NextFsRMapSeqNum)
{
    tRMapNode          *pRMapNextNode = NULL;
    tRMapNode          *pRMapTempNode = NULL;
    UINT4               u4RMapCount = 0;
    BOOL1               bFlag = TRUE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get next MAP node */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNextNode = RMapGetNextRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNextNode == NULL)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Next Route Map Entry for Route Map %s"
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    if (RBTreeCount (gRMapGlobalInfo.RMapRoot, &u4RMapCount) == RB_FAILURE)
    {
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "No Map Entry in the RB tree .\n");
        return SNMP_FAILURE;
    }

    /* through all MAP nodes find next one with SET node */
    while (u4RMapCount > RMAP_ZERO)
    {
        if (pRMapNextNode->pSetNode == NULL)
        {
            pRMapTempNode = (tRMapNode *)
                RBTreeGetNext (gRMapGlobalInfo.RMapRoot, pRMapNextNode, NULL);

            if (pRMapTempNode == NULL)
            {
                bFlag = FALSE;
                break;
            }
            pRMapNextNode = pRMapTempNode;
            u4RMapCount--;
            continue;
        }
        break;
    }

    if (bFlag == FALSE)
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "Next Route Map Entry for Route Map %s"
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* store output params */
    RMAP_ASCIIZ_2_OCT (pNextFsRMapName, pRMapNextNode->au1RMapName);
    *pu4NextFsRMapSeqNum = pRMapNextNode->u4SeqNo;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRMapSetNextHopInetType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetNextHopInetType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetNextHopInetType (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                UINT4 u4FsRMapSeqNum,
                                INT4 *pi4RetValFsRMapSetNextHopInetType)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetNextHopInetType =
            pRMapNode->pSetNode->NextHopX.u1Afi;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetNextHopInetType failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetNextHopInetAddr
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetNextHopInetAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetNextHopInetAddr (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                UINT4 u4FsRMapSeqNum,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsRMapSetNextHopInetAddr)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        MEMCPY (pRetValFsRMapSetNextHopInetAddr->pu1_OctetList,
                pRMapNode->pSetNode->NextHopX.au1Addr,
                pRMapNode->pSetNode->NextHopX.u1AddrLen);
        pRetValFsRMapSetNextHopInetAddr->i4_Length =
            (INT4) pRMapNode->pSetNode->NextHopX.u1AddrLen;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetNextHopInetType failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetInterface
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetInterface (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          INT4 *pi4RetValFsRMapSetInterface)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetInterface = pRMapNode->pSetNode->u4IfIndex;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "GetRMapSetIface failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetMetric
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetMetric (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 *pi4RetValFsRMapSetMetric)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetMetric = pRMapNode->pSetNode->i4Metric;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetMetric failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                    UINT4 u4FsRMapSeqNum, UINT4 *pu4RetValFsRMapSetTag)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetTag = pRMapNode->pSetNode->u4Tag;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetRouteType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetRouteType (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          INT4 *pi4RetValFsRMapSetRouteType)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetRouteType = pRMapNode->pSetNode->i2RouteType;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetRouteType failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetASPathTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetASPathTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetASPathTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          UINT4 *pu4RetValFsRMapSetASPathTag)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetASPathTag = pRMapNode->pSetNode->u4NextHopAS;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetASPathTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetCommunity
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetCommunity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetCommunity (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsRMapSetCommunity)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
	    /*store the octect representation in rmapSetNode*/

	    MEMCPY (pRetValFsRMapSetCommunity->pu1_OctetList,
                           pRMapNode->pSetNode->au1RMapCommunityList,
			   STRLEN(pRMapNode->pSetNode->au1RMapCommunityList));
	    pRetValFsRMapSetCommunity->i4_Length =
		    STRLEN(pRMapNode->pSetNode->au1RMapCommunityList);

            i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetCommunity failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetLocalPref
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetLocalPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetLocalPref (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          INT4 *pi4RetValFsRMapSetLocalPref)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetLocalPref = pRMapNode->pSetNode->u4LocalPref;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetLocalPref failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetOrigin
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetOrigin (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 *pi4RetValFsRMapSetOrigin)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetOrigin = pRMapNode->pSetNode->i4Origin;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetOrigin failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetWeight
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetWeight (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, UINT4 *pu4RetValFsRMapSetWeight)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetWeight = pRMapNode->pSetNode->u2Weight;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetWeight failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetEnableAutoTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetEnableAutoTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetEnableAutoTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                              UINT4 u4FsRMapSeqNum,
                              INT4 *pi4RetValFsRMapSetEnableAutoTag)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetEnableAutoTag = pRMapNode->pSetNode->u1AutoTagEna;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetEnableAutoTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetLevel
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetLevel (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                      UINT4 u4FsRMapSeqNum, INT4 *pi4RetValFsRMapSetLevel)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetLevel = pRMapNode->pSetNode->u1Level;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetLevel failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          INT4 *pi4RetValFsRMapSetRowStatus)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    /* get MAP node for given rmap name & seq num */
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pi4RetValFsRMapSetRowStatus = pRMapNode->pSetNode->u1RowStatus;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetRowStatus failed -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetExtCommId
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetExtCommId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetExtCommId (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          UINT4 *pu4RetValFsRMapSetExtCommId)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetExtCommId = pRMapNode->pSetNode->u2ExtCommId;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetExtCommId -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetExtCommPOI
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetExtCommPOI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetExtCommPOI (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                           UINT4 u4FsRMapSeqNum,
                           UINT4 *pu4RetValFsRMapSetExtCommPOI)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetExtCommPOI = pRMapNode->pSetNode->u2ExtCommPOI;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetExtCommPOI -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRMapSetExtCommCost
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                retValFsRMapSetExtCommCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRMapSetExtCommCost (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            UINT4 *pu4RetValFsRMapSetExtCommCost)
{
    tRMapNode          *pRMapNode;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* store output params */
    if ((pRMapNode != NULL) && (pRMapNode->pSetNode != NULL))
    {
        *pu4RetValFsRMapSetExtCommCost = pRMapNode->pSetNode->u4ExtCommCost;
        i1RetVal = SNMP_SUCCESS;

    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhGetFsRMapSetExtCommCost -RMap Entry %s not available.\n",
                       au1Name);
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRMapSetNextHopInetType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetNextHopInetType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetNextHopInetType (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                UINT4 u4FsRMapSeqNum,
                                INT4 i4SetValFsRMapSetNextHopInetType)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    /* get MAP node for given rmap name & seq num */
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetNextHopInetType == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_NEXTHOP_IPX);
                i1RetVal = SNMP_SUCCESS;
            }
            else if ((i4SetValFsRMapSetNextHopInetType != IPVX_ADDR_FMLY_IPV4)
                     && (i4SetValFsRMapSetNextHopInetType !=
                         IPVX_ADDR_FMLY_IPV6))
            {
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                pRMapNode->pSetNode->NextHopX.u1Afi =
                    i4SetValFsRMapSetNextHopInetType;
                i1RetVal = SNMP_SUCCESS;
            }

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetNextHopInetType failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetNextHopInetType failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetNextHopInetAddr
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetNextHopInetAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetNextHopInetAddr (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                UINT4 u4FsRMapSeqNum,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsRMapSetNextHopInetAddr)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1IpAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4AddrLen = 0;

    MEMSET (au1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    /* get MAP node for given rmap name & seq num */
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pSetValFsRMapSetNextHopInetAddr == NULL)
    {
        return i1RetVal;
    }
    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {

            i4AddrLen = pSetValFsRMapSetNextHopInetAddr->i4_Length;

            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */
            if (((pRMapNode->pSetNode->NextHopX.u1Afi == IPVX_ADDR_FMLY_IPV4)
                 && (MEMCMP (pSetValFsRMapSetNextHopInetAddr->pu1_OctetList,
                             au1IpAddr, IPVX_IPV4_ADDR_LEN) == 0))
                || ((pRMapNode->pSetNode->NextHopX.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    &&
                    (MEMCMP
                     (pSetValFsRMapSetNextHopInetAddr->pu1_OctetList, au1IpAddr,
                      IPVX_IPV6_ADDR_LEN) == 0)))
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_NEXTHOP_IPX);
                i1RetVal = SNMP_SUCCESS;
            }
            else if ((i4AddrLen != IPVX_IPV4_ADDR_LEN) &&
                     (i4AddrLen != IPVX_IPV6_ADDR_LEN))
            {
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                pRMapNode->pSetNode->NextHopX.u1AddrLen = i4AddrLen;
                MEMCPY (pRMapNode->pSetNode->NextHopX.au1Addr,
                        pSetValFsRMapSetNextHopInetAddr->pu1_OctetList,
                        i4AddrLen);
                i1RetVal = SNMP_SUCCESS;
            }

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetNextHopInetType failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetNextHopInetType failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetInterface
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetInterface (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetInterface)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetInterface == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_INTERFACE);
            }
            else
            {
                pRMapNode->pSetNode->u4IfIndex = i4SetValFsRMapSetInterface;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetInterface failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetInterface failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetMetric
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetMetric (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetMetric)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetMetric == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |= (1 << RMAP_SET_CMD_METRIC);
            }
            else
            {
                pRMapNode->pSetNode->i4Metric = i4SetValFsRMapSetMetric;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetMetric failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetMetric failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                    UINT4 u4FsRMapSeqNum, UINT4 u4SetValFsRMapSetTag)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (u4SetValFsRMapSetTag == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |= (1 << RMAP_SET_CMD_TAG);
            }
            else
            {
                pRMapNode->pSetNode->u4Tag = u4SetValFsRMapSetTag;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetTag failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetRouteType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetRouteType (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetRouteType)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetRouteType == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_ROUTE_TYPE);
            }
            else
            {
                pRMapNode->pSetNode->i2RouteType = i4SetValFsRMapSetRouteType;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetRouteType failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetRouteType failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetASPathTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetASPathTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetASPathTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          UINT4 u4SetValFsRMapSetASPathTag)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }

    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (u4SetValFsRMapSetASPathTag == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_ASPATH_TAG);
            }
            else
            {
                pRMapNode->pSetNode->u4NextHopAS = u4SetValFsRMapSetASPathTag;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetASPathTag failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetASPathTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetCommunity
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetCommunity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetCommunity (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsRMapSetCommunity)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1RMapCommunityList[RMAP_COMM_STR_LEN];
    UINT1               au1TempRMapCommunityList[RMAP_COMM_STR_LEN];
    UINT4               u4Asnum;
    UINT4               u4SetValFsRMapSetCommunity;
    CHR1                *pAsNoinpToken;
    CHR1                *ptoken = " ";
    UINT1                u1Count;


    MEMSET (au1TempRMapCommunityList,0,sizeof(au1TempRMapCommunityList));
    MEMSET (au1RMapCommunityList,0,sizeof(au1RMapCommunityList));
    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
		    /*populate the community values which is in OCTECT STRING 
		      into the community values or pRMapNode*/
		    RMAPCOMM_OCT_2_ASCIIZ(au1RMapCommunityList ,pRetValFsRMapSetCommunity);
                    
                   
                   /*store the octect representation in rmapSetNode*/
                  
                    if(pRMapNode->pSetNode->au1RMapCommunityList[0] == 0)
                    {
		       MEMCPY (pRMapNode->pSetNode->au1RMapCommunityList,
				    pRetValFsRMapSetCommunity->pu1_OctetList,
		    		    pRetValFsRMapSetCommunity->i4_Length);
                    }
                    else
                    {
                       MEMCPY (au1TempRMapCommunityList,
                                    pRetValFsRMapSetCommunity->pu1_OctetList,
                                    pRetValFsRMapSetCommunity->i4_Length);
                       STRNCAT(pRMapNode->pSetNode->au1RMapCommunityList," ",STRLEN(" "));
                       STRNCAT(pRMapNode->pSetNode->au1RMapCommunityList,
                                     au1TempRMapCommunityList,STRLEN(au1TempRMapCommunityList));
                    }
                    
		    pAsNoinpToken = STRTOK (au1RMapCommunityList, ptoken);
                    
                    if (pAsNoinpToken == NULL)
                    {
                        /*no set community is called*/
                        pRMapNode->pSetNode->u4RemoveMask |=
                                            (1 << RMAP_SET_CMD_COMMUNITY);
                        MEMSET(pRMapNode->pSetNode->au1RMapCommunityList,0,
                                        sizeof(pRMapNode->pSetNode->au1RMapCommunityList));
                        MEMSET(pRMapNode->pSetNode->au4Community,0,
                                         sizeof(pRMapNode->pSetNode->au4Community));
                        pRMapNode->pSetNode->u1CommunityCnt = 0;
                        MEMSET(pRMapNode->pSetNode->au1RMapCommunityList,0,
                                       sizeof(pRMapNode->pSetNode->au1RMapCommunityList));
                    }
          
		    while (pAsNoinpToken != NULL)
		    {
			    if (STRCMP(pAsNoinpToken,"internet")==0)
			    {
				    u4Asnum = RMAP_COMM_INTERNET;
			    }
			    else if (STRCMP(pAsNoinpToken,"local-as")==0)
			    {
				    u4Asnum = RMAP_COMM_LOCALAS;
			    }
			    else if (STRCMP(pAsNoinpToken,"no-advt")==0)
			    {
				    u4Asnum = RMAP_COMM_NO_ADVT;
			    }
			    else if (STRCMP(pAsNoinpToken,"no-export") ==0)
			    {
				    u4Asnum = RMAP_COMM_NO_EXPORT;
			    }
			    else if (STRCMP(pAsNoinpToken,"none")==0)
			    {
				    u4Asnum = RMAP_COMM_NONE;
			    }
			    else if (STRCHR (pAsNoinpToken,':'))
			    {
				    /*given input is A:B format */
				  RmapCliParseCommunityStringAsNum(pAsNoinpToken, &u4Asnum);
			    }
                            else if ((STRCHR (pAsNoinpToken,'x'))||(STRCHR (pAsNoinpToken,'X')))
                            {
                                    u4Asnum = CliHexStrToDecimal ((UINT1 *)pAsNoinpToken);        
                            }
                            
			    else
			    {
				    /*the given input is numeral format*/
				    u4Asnum = (UINT4) ATOI (pAsNoinpToken);
			    }
			    u4SetValFsRMapSetCommunity = u4Asnum;

            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (u4SetValFsRMapSetCommunity == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_COMMUNITY);
            }
            else
            {
				    for (u1Count = 0; u1Count < RMAP_BGP_MAX_COMM ; u1Count++)
				    {
					    if( pRMapNode->pSetNode->au4Community[u1Count] == 0)
					    {
						    pRMapNode->pSetNode->au4Community[u1Count] =
							    u4SetValFsRMapSetCommunity;
						    pRMapNode->pSetNode->u1CommunityCnt++;
                                                    break;
					    }
				    }
			    }
			    pAsNoinpToken = STRTOK (NULL, ptoken);      
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetCommunity failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetCommunity failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
    }

/****************************************************************************
 Function    :  nmhSetFsRMapSetLocalPref
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetLocalPref
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetLocalPref (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetLocalPref)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetLocalPref == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_LOCAL_PREF);
            }
            else
            {
                pRMapNode->pSetNode->u4LocalPref = i4SetValFsRMapSetLocalPref;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetLocalPref failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetLocalPref failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetOrigin
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetOrigin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetOrigin (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetOrigin)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetOrigin == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |= (1 << RMAP_SET_CMD_ORIGIN);
            }
            else
            {
                pRMapNode->pSetNode->i4Origin = i4SetValFsRMapSetOrigin;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetOrigin failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetOrigin failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetWeight
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetWeight (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, UINT4 u4SetValFsRMapSetWeight)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (u4SetValFsRMapSetWeight == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |= (1 << RMAP_SET_CMD_WEIGHT);
            }
            else
            {
                pRMapNode->pSetNode->u2Weight = u4SetValFsRMapSetWeight;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetWeight failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetWeight failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetEnableAutoTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetEnableAutoTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetEnableAutoTag (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                              UINT4 u4FsRMapSeqNum,
                              INT4 i4SetValFsRMapSetEnableAutoTag)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetEnableAutoTag == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |=
                    (1 << RMAP_SET_CMD_AUTO_TAG_ENA);
            }
            else
            {
                pRMapNode->pSetNode->u1AutoTagEna =
                    i4SetValFsRMapSetEnableAutoTag;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetEnableAutoTag failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetEnableAutoTag failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetLevel
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetLevel (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                      UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetLevel)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            /* When the user wants to delete any of the SET params, that is
             * considered as setting 0 to that SET parameter.
             * It will be taken care by the RTM after
             * processing the SET parameter change */

            if (i4SetValFsRMapSetLevel == 0)
            {
                pRMapNode->pSetNode->u4RemoveMask |= (1 << RMAP_SET_CMD_LEVEL);
            }
            else
            {
                pRMapNode->pSetNode->u1Level = i4SetValFsRMapSetLevel;
            }
            i1RetVal = SNMP_SUCCESS;

        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetLevel failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetLevel failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetRowStatus (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4SetValFsRMapSetRowStatus)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode == NULL)
    {
        /* Route map not found */
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "SetRMapSetRowStatus failed -RMap Entry %s not available.\n",
                       au1Name);
        return SNMP_FAILURE;
    }

    /* create new SET node */
    if (((i4SetValFsRMapSetRowStatus == RMAP_CREATE_AND_WAIT) ||
         (i4SetValFsRMapSetRowStatus == RMAP_CREATE_AND_GO)) &&
        (pRMapNode->pSetNode == NULL))
    {
        if ((pRMapNode->pSetNode =
             (tRMapSetNode *) MemAllocMemBlk (gRMapGlobalInfo.RMapSetPoolId))
            != NULL)
        {
            MEMSET (pRMapNode->pSetNode, 0, sizeof (tRMapSetNode));
            if (i4SetValFsRMapSetRowStatus == RMAP_CREATE_AND_WAIT)
            {
                pRMapNode->pSetNode->u1RowStatus = RMAP_NOT_READY;
            }
            else if (i4SetValFsRMapSetRowStatus == RMAP_CREATE_AND_GO)
            {
                /* Send the update message to applications RTM4, RTM6 */
                RMapSendUpdateMsg (au1Name);
                pRMapNode->pSetNode->u1RowStatus = RMAP_ACTIVE;
            }
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "SetRMapSetRowStatus failed - Malloc for SET Entry failed.\n");
        }
    }
    /* make new SET node active */
    else if ((pRMapNode->pSetNode != NULL) &&
             (i4SetValFsRMapSetRowStatus == RMAP_ACTIVE))
    {
        pRMapNode->pSetNode->u1RowStatus = (UINT1) i4SetValFsRMapSetRowStatus;

        /* Send the update message to applications RTM4, RTM6 */
        RMapSendUpdateMsg (au1Name);
        i1RetVal = SNMP_SUCCESS;
    }
    else if ((pRMapNode->pSetNode != NULL) &&
             (i4SetValFsRMapSetRowStatus == RMAP_DESTROY))
    {
        pRMapNode->pSetNode->u1RowStatus = (UINT1) i4SetValFsRMapSetRowStatus;

        /* Send the update message to applications RTM4, RTM6 */
        RMapSendUpdateMsg (au1Name);
        RMapProcessRouteMapUpdate (au1Name);
        i1RetVal = SNMP_SUCCESS;
    }
    else if ((pRMapNode->pSetNode != NULL) &&
             (i4SetValFsRMapSetRowStatus == RMAP_NOT_IN_SERVICE))
    {
        pRMapNode->pSetNode->u1RowStatus = (UINT1) i4SetValFsRMapSetRowStatus;

        /* Send the update message to applications RTM4, RTM6 */
        RMapSendUpdateMsg (au1Name);
        RMapProcessRouteMapUpdate (au1Name);
        i1RetVal = SNMP_SUCCESS;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetExtCommId
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetExtCommId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetExtCommId (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum,
                          UINT4 u4SetValFsRMapSetExtCommId)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            pRMapNode->pSetNode->u2ExtCommId = u4SetValFsRMapSetExtCommId;
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetExtCommId failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetExtCommId failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRMapSetExtCommCost
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                setValFsRMapSetExtCommCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRMapSetExtCommCost (tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                            UINT4 u4FsRMapSeqNum,
                            UINT4 u4SetValFsRMapSetExtCommCost)
{
    tRMapNode          *pRMapNode = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        return i1RetVal;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    if (pRMapNode != NULL)
    {
        if (pRMapNode->pSetNode != NULL)
        {
            pRMapNode->pSetNode->u4ExtCommCost = u4SetValFsRMapSetExtCommCost;
            if (u4SetValFsRMapSetExtCommCost != 0)
            {
                pRMapNode->pSetNode->u2ExtCommPOI = RMAP_BGP_EXT_COMM_POI;
            }
            else
            {
                pRMapNode->pSetNode->u2ExtCommPOI = 0;
            }
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "nmhSetFsRMapSetExtCommCost failed -RMap SET Entry not available.\n");
        }
    }
    else
    {
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "nmhSetFsRMapSetExtCommCost failed -RMap Entry %s not available.\n",
                       au1Name);
    }
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetNextHopInetType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetNextHopInetType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetNextHopInetType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   INT4 i4TestValFsRMapSetNextHopInetType)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetNextHopInetType != 0) &&
        (i4TestValFsRMapSetNextHopInetType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsRMapSetNextHopInetType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetNextHopInetAddr
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetNextHopInetAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetNextHopInetAddr (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                   UINT4 u4FsRMapSeqNum,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsRMapSetNextHopInetAddr)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    tIPvXAddr           XAddr;
    INT4                i4Res = SNMP_FAILURE;

    MEMSET (&XAddr, 0, sizeof (tIPvXAddr));

    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i4Res;
    }
    /* get MAP node for given rmap name & seq num */
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (pTestValFsRMapSetNextHopInetAddr != NULL)
    {
        if (pTestValFsRMapSetNextHopInetAddr->i4_Length == 0)
        {
            return SNMP_SUCCESS;
        }
        else if (pTestValFsRMapSetNextHopInetAddr->i4_Length ==
                 IPVX_IPV4_ADDR_LEN)
        {
            IPVX_ADDR_INIT (XAddr, IPVX_ADDR_FMLY_IPV4,
                            pTestValFsRMapSetNextHopInetAddr->pu1_OctetList);
            i4Res = ValidateXAddr (&XAddr);
            i4Res = (i4Res == RMAP_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE;
        }
        else if (pTestValFsRMapSetNextHopInetAddr->i4_Length ==
                 IPVX_IPV6_ADDR_LEN)
        {
            IPVX_ADDR_INIT (XAddr, IPVX_ADDR_FMLY_IPV6,
                            pTestValFsRMapSetNextHopInetAddr->pu1_OctetList);
            i4Res = ValidateXAddr (&XAddr);
            i4Res = (i4Res == RMAP_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE;
        }
    }

    if (i4Res != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetInterface
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetInterface (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             INT4 i4TestValFsRMapSetInterface)
{
    tRMapNode          *pRMapNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* get MAP node for given rmap name & seq num */
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetInterface < 0) ||
        (i4TestValFsRMapSetInterface > RMAP_SYS_DEF_MAX_INTERFACES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        CLI_SET_ERR (RMAP_CLI_ERR_INVALID_INTERFACE_ID);
        return SNMP_FAILURE;
    }

    if (i4TestValFsRMapSetInterface != 0)
    {
        CfaGetIfInfo ((UINT4) i4TestValFsRMapSetInterface, &CfaIfInfo);
        if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
              ((CfaIfInfo.u1IfType == CFA_ENET) &&
               (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                      "TestRMapSetInterface failed - Wrong Interface No.\n");
            CLI_SET_ERR (RMAP_CLI_ERR_INVALID_INTERFACE_TYPE);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetMetric
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetMetric (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4TestValFsRMapSetMetric)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (i4TestValFsRMapSetMetric < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetTag (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                       UINT4 u4FsRMapSeqNum, UINT4 u4TestValFsRMapSetTag)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (u4TestValFsRMapSetTag > RMAP_MAX_TAG_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetRouteType
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetRouteType (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             INT4 i4TestValFsRMapSetRouteType)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetRouteType != 0) &&
        (i4TestValFsRMapSetRouteType != RMAP_ROUTE_TYPE_LOCAL) &&
        (i4TestValFsRMapSetRouteType != RMAP_ROUTE_TYPE_REMOTE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetASPathTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetASPathTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetASPathTag (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             UINT4 u4TestValFsRMapSetASPathTag)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (u4TestValFsRMapSetASPathTag > RMAP_MAX_ASPATH_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetCommunity
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetCommunity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetCommunity (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             tSNMP_OCTET_STRING_TYPE * pTestValFsRMapSetCommunity)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1RMapCommunityList[RMAP_COMM_STR_LEN];
    UINT4               u4Asnum = 0;
    UINT4               u4TestValFsRMapSetCommunity = 0;
    CHR1               *pAsNoinpToken;
    CHR1                *ptoken = " ";
    UINT1               u1Count = 0;
    UINT1               u1CommunityConfigured =0;
    INT4                i4AsRmap = RMAP_SUCCESS;
    AR_UINT8            uLAsNum = 0; 
   

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR(RMAP_CLI_ENTRY_NAME_EXCEEDS_MAX_VALUE);
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* if maximum community  count reached then retrun failure */
    
    if (pRMapNode->pSetNode->u1CommunityCnt == RMAP_BGP_MAX_COMM)
    {
       RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Max %s "
                       "community count reached.\n", au1Name);
       *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
       CLI_SET_ERR (RMAP_CLI_MAX_COMMUNITIES_REACHED);
       return SNMP_FAILURE;
    }
    if (pRMapNode->pSetNode->u1CommunityCnt != 0)
     {
           if(pRMapNode->pSetNode->au4Community[CONF_COMM_STD] == RMAP_COMM_NONE)
           {
               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               CLI_SET_ERR (RMAP_CLI_COMM_NONE_CONFIGURED);
                    return SNMP_FAILURE;
           }
     }
   /* checking community values from octet-string received*/
    RMAPCOMM_OCT_2_ASCIIZ (au1RMapCommunityList ,pTestValFsRMapSetCommunity);
   
    pAsNoinpToken = STRTOK (au1RMapCommunityList, ptoken);
    while (pAsNoinpToken != NULL)
    {
            if (STRCMP(pAsNoinpToken,"internet")==0)
            {
              u4Asnum = RMAP_COMM_INTERNET;
            }
            else if (STRCMP(pAsNoinpToken,"local-as")==0)
            {
               u4Asnum = RMAP_COMM_LOCALAS;
            }
            else if (STRCMP(pAsNoinpToken,"no-advt")==0)
            {
               u4Asnum = RMAP_COMM_NO_ADVT;
            }
            else if (STRCMP(pAsNoinpToken,"no-export") ==0)
            {
               u4Asnum = RMAP_COMM_NO_EXPORT;
            }
            else if (STRCMP(pAsNoinpToken,"none")==0)
            {
               u4Asnum = RMAP_COMM_NONE;
            if(pRMapNode->pSetNode->u1CommunityCnt > 0)
                {
                   *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                   CLI_SET_ERR (RMAP_CLI_COMM_NONE_CANNOT_BE_CONFIGURED);
                    return SNMP_FAILURE;
                }
            }
            else if (STRCHR (pAsNoinpToken,'.'))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR(RMAP_CLI_COMM_RANGE_ERROR);
                return (SNMP_FAILURE);
            }
            else if (STRCHR (pAsNoinpToken,':'))
	    {
		    /*given input is A:B format */
		   i4AsRmap = RmapCliParseCommunityStringAsNum(pAsNoinpToken, &u4Asnum);
           if (i4AsRmap == RMAP_FAILURE)
           {
               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               CLI_SET_ERR(RMAP_CLI_COMM_RANGE_ERROR);
               return (SNMP_FAILURE);
           }
           else if (u4Asnum  == RMAP_COMM_NONE)
           {
               *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
               CLI_SET_ERR (RMAP_CLI_COMM_RANGE_NONE_ERROR);
               return (SNMP_FAILURE);
           }
           }
	    else if ((STRCHR (pAsNoinpToken,'x'))||(STRCHR (pAsNoinpToken,'X')))
	    {
		    u4Asnum = CliHexStrToDecimal ((UINT1 *) pAsNoinpToken);
	    }
	    else
	    {
            /*the given input is numeral format\*/
            uLAsNum = atoll (pAsNoinpToken);
            if (uLAsNum > RMAP_COMM_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RMAP_CLI_COMM_RANGE_ERROR);
                return (SNMP_FAILURE);
            }
            u4Asnum = (UINT4) ATOI (pAsNoinpToken);
            if (u4Asnum  == RMAP_COMM_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RMAP_CLI_COMM_RANGE_NONE_ERROR);
                return (SNMP_FAILURE);
            }
        }
        u4TestValFsRMapSetCommunity = u4Asnum;
        u1CommunityConfigured++;

        if ( u1CommunityConfigured > RMAP_BGP_MAX_COMM )
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
             return (SNMP_FAILURE);
        }

    if ((!((u4TestValFsRMapSetCommunity >= RMAP_MIN_COMMUNITY_NUM) &&
          (u4TestValFsRMapSetCommunity <= RMAP_MAX_COMMUNITY_NUM))) &&
        ((u4TestValFsRMapSetCommunity != RMAP_COMM_INTERNET) &&
         !(u4TestValFsRMapSetCommunity >= RMAP_COMM_NO_EXPORT &&
           u4TestValFsRMapSetCommunity <= RMAP_COMM_LOCALAS)) &&
        (u4TestValFsRMapSetCommunity != RMAP_COMM_NONE))
        {
           *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR(RMAP_CLI_COMM_RANGE_ERROR);
            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                           "TestRMapSetIface failed -RMap Test routine%s "
                           "community value not configured correctly.\n", au1Name);

        return (SNMP_FAILURE);
    }

	    for (u1Count = 0; u1Count < RMAP_BGP_MAX_COMM ; u1Count++)
	    {
	        if ( u4TestValFsRMapSetCommunity ==
				    pRMapNode->pSetNode->au4Community[u1Count])
		{

           *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR(RMAP_CLI_ENTRY_COMM_EXIST);
            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                    "TestRMapSetIface failed -RMap Test routine%s "
                    "community already configured.\n", au1Name);
            return (SNMP_FAILURE);
		}
	    }

	    pAsNoinpToken = STRTOK (NULL, ptoken);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetLocalPref
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetLocalPref
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetLocalPref (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             INT4 i4TestValFsRMapSetLocalPref)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR(RMAP_CLI_ENTRY_NOT_EXIST);
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    /* (i4TestValFsRMapSetLocalPref > RMAP_MAX_LOCAl_PREF_VAL)
     * removed the above if check to resolve coverity warning 
     * as the value of RMAP_MAX_LOCAl_PREF_VAL is 0x7fffffff 
     * and maximum value of INT4 is 0x7fffffff, the result
     * of the expression will always be false*/
    if (i4TestValFsRMapSetLocalPref < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetOrigin
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetOrigin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetOrigin (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, INT4 i4TestValFsRMapSetOrigin)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetOrigin != RMAP_ORIGIN_IGP) &&
        (i4TestValFsRMapSetOrigin != RMAP_ORIGIN_EGP) &&
        (i4TestValFsRMapSetOrigin != RMAP_ORIGIN_INCOMPLETE) &&
        (i4TestValFsRMapSetOrigin != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetWeight
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetWeight (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                          UINT4 u4FsRMapSeqNum, UINT4 u4TestValFsRMapSetWeight)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (u4TestValFsRMapSetWeight > RMAP_MAX_WEIGHT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetEnableAutoTag
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetEnableAutoTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetEnableAutoTag (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                                 UINT4 u4FsRMapSeqNum,
                                 INT4 i4TestValFsRMapSetEnableAutoTag)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetEnableAutoTag != 0) &&
        (i4TestValFsRMapSetEnableAutoTag != RMAP_AUTO_TAG_ENA) &&
        (i4TestValFsRMapSetEnableAutoTag != RMAP_AUTO_TAG_DIS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetLevel
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetLevel (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                         UINT4 u4FsRMapSeqNum, INT4 i4TestValFsRMapSetLevel)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if ((i4TestValFsRMapSetLevel != 0) &&
        (i4TestValFsRMapSetLevel != RMAP_LEVEL_1) &&
        (i4TestValFsRMapSetLevel != RMAP_LEVEL_2) &&
        (i4TestValFsRMapSetLevel != RMAP_LEVEL_1_2) &&
        (i4TestValFsRMapSetLevel != RMAP_LEVEL_STUB_AREA) &&
        (i4TestValFsRMapSetLevel != RMAP_LEVEL_BACKBONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapSetInterface failed - Wrong Interface No.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetRowStatus
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetRowStatus (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             INT4 i4TestValFsRMapSetRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRMapName);
    UNUSED_PARAM (u4FsRMapSeqNum);
    UNUSED_PARAM (i4TestValFsRMapSetRowStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetExtCommId
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetExtCommId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetExtCommId (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                             UINT4 u4FsRMapSeqNum,
                             UINT4 u4TestValFsRMapSetExtCommId)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }

    /* test value */
    if (u4TestValFsRMapSetExtCommId > RMAP_MAX_EXT_COMM_ID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "TestRMapTestExtCommId failed - Wrong Ext Community Id.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRMapSetExtCommCost
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum

                The Object
                testValFsRMapSetExtCommCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRMapSetExtCommCost (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsRMapName,
                               UINT4 u4FsRMapSeqNum,
                               UINT4 u4TestValFsRMapSetExtCommCost)
{
    tRMapNode          *pRMapNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UNUSED_PARAM (u4TestValFsRMapSetExtCommCost);

    /* get MAP node for given rmap name & seq num */
    if (pFsRMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    RMAP_OCT_2_ASCIIZ (au1Name, pFsRMapName);
    pRMapNode = RMapGetRouteMapNode (au1Name, u4FsRMapSeqNum);

    /* MAP node should exist */
    if ((pRMapNode == NULL) || (pRMapNode->pSetNode == NULL))
    {
        /* Route map not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                       "TestRMapSetIface failed -RMap Entry %s "
                       "not available.\n", au1Name);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRMapSetTable
 Input       :  The Indices
                FsRMapName
                FsRMapSeqNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRMapSetTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRmapTrapCfgEnable
 Input       :  The Indices

                The Object
                retValFsRmapTrapCfgEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRmapTrapCfgEnable (INT4 *pi4RetValFsRmapTrapCfgEnable)
{
    *pi4RetValFsRmapTrapCfgEnable = (INT4) gRMapGlobalInfo.u4RMapTrapEnabled;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRmapTrapCfgEnable
 Input       :  The Indices

                The Object
                setValFsRmapTrapCfgEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRmapTrapCfgEnable (INT4 i4SetValFsRmapTrapCfgEnable)
{
    gRMapGlobalInfo.u4RMapTrapEnabled = (UINT4) i4SetValFsRmapTrapCfgEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRmapTrapCfgEnable
 Input       :  The Indices

                The Object
                testValFsRmapTrapCfgEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRmapTrapCfgEnable (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsRmapTrapCfgEnable)
{
    /* test value */
    if ((i4TestValFsRmapTrapCfgEnable != 0) &&
        (i4TestValFsRmapTrapCfgEnable != RMAP_TRAP_ENABLED) &&
        (i4TestValFsRmapTrapCfgEnable != RMAP_TRAP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RMAP_TRC (RMAP_MOD_TRC, RMAP_MGMT_TRC, RMAP_MODULE_NAME,
                  "nmhTestv2FsRmapTrapCfgEnable failed - Wrong value\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRmapTrapCfgEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRmapTrapCfgEnable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
