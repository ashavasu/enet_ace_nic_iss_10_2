/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmaptrap.c,v 1.13 2016/04/01 12:08:32 siva Exp $
 *
 * Description: This file contain routines for route map traps to SNMP 
 *          Agent and parameter modification SNMP event routines
 *          
 *******************************************************************/
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)

#include "rmapinc.h"
#include "snmputil.h"
#include "fssnmp.h"
#include "fsrmap.h"

/* Function prototypes and variables private to this file only */
static VOID         RMapSendTrap (UINT1 u1TrapId, VOID *pTrapInfo);

static tSNMP_OID_TYPE *RMapMakeObjIdFromDotNew (INT1 *textStr);
static INT4         RMapParseSubIdNew (UINT1 **tempPtr);

/*"fsRMapTrapGroup",        "1.3.6.1.4.1.2076.155.4", -- for mib with both fsRouteMap and fsRMap */
static UINT4        RMAP_TRAPGROUP_OID[] = { 1, 3, 6, 1, 4, 1, 2076, 155, 4 };

/* must be equal to gau4snmpTrapOid */
static UINT4        SNMP_TRAP_OID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

/* enumerator of various rmap traps */
enum
{
    RMAP_TRAP_ID_MATCH = 1
};

/* for each rmap trap we define special trap-info */
typedef struct
{
    UINT1               au1MapName[RMAP_MAX_NAME_LEN];
    UINT1               u1SeqNum;
    UINT1               au1Pad[3];
} tRmapTrapInfoMatch;

/*****************************************************************************/
/*                                                                           */
/* Function     : RMapSendMatchTrap                                          */
/*                                                                           */
/* Description  : Send specific trap. Public function.                       */
/*                Send objects - map name                                    */
/*                             - sequential number                           */
/*                                                                           */
/* Input        : pu1MapName : map name                                      */
/*                u1SeqNum   : sequential number                             */
/*                                                                           */
/*****************************************************************************/
VOID
RMapSendMatchTrap (UINT1 *pu1MapName, UINT1 u1SeqNum)
{
    tRmapTrapInfoMatch  Trap;
    UINT4               u4Len;

    /* prepare trap-info */
    MEMSET (&Trap, 0, sizeof (Trap));
    u4Len = STRLEN (pu1MapName);
    MEMCPY (Trap.au1MapName, pu1MapName, u4Len);
    Trap.u1SeqNum = u1SeqNum;

    RMapSendTrap (RMAP_TRAP_ID_MATCH, &Trap);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RMapSendTrap                                               */
/*                                                                           */
/* Description  : Routine to send trap to SNMP Agent.                        */
/*                                                                           */
/* Input        : u1TrapId  : Trap identifier                                */
/*                p_trap_info : Pointer to structure having the trap info.   */
/*                                                                           */
/*****************************************************************************/
static VOID
RMapSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{

    tRmapTrapInfoMatch *pTrap;

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;

    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[256];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };

    /* allocate pEnterpriseOid */
    pEnterpriseOid = alloc_oid (sizeof (SNMP_TRAP_OID) / sizeof (UINT4));
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    /* put rmap-trapgroup-oid into pEnterpriseOid */
    MEMCPY (pEnterpriseOid->pu4_OidList, RMAP_TRAPGROUP_OID,
            sizeof (RMAP_TRAPGROUP_OID));
    pEnterpriseOid->u4_Length = sizeof (RMAP_TRAPGROUP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /* allocate pSnmpTrapOid */
    pSnmpTrapOid = alloc_oid (sizeof (SNMP_TRAP_OID) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, SNMP_TRAP_OID, sizeof (SNMP_TRAP_OID));
    pSnmpTrapOid->u4_Length = sizeof (SNMP_TRAP_OID) / sizeof (UINT4);

    /* bind pEnterpriseOid to pSnmpTrapOid, append new var as 1st */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    /* after 1st BIND remember its initial value */
    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case RMAP_TRAP_ID_MATCH:

            /*************** put 1st object in trap ***************/
            pTrap = (tRmapTrapInfoMatch *) pTrapInfo;

            /* make OID */
            SPRINTF ((CHR1 *) au1Buf, "fsRMapName");
            if ((pOid = RMapMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /* make snmp-octet-string from map-name */
            if ((pOstring =
                 SNMP_AGT_FormOctetString (pTrap->au1MapName,
                                           RMAP_MAX_NAME_LEN)) == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /* BIND snmp-octet-string to pOid, append new var as next */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                free_octetstring (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /*************** put 2nd object in trap ***************/
            pVbList = pVbList->pNextVarBind;

            /* make OID */
            SPRINTF ((CHR1 *) au1Buf, "fsRMapSeqNum");
            if ((pOid = RMapMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            /* BIND unsigned32 to pOid, append as next */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_UNSIGNED32,
                                                         (UINT4) (pTrap->
                                                                  u1SeqNum), 0,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;
        
        default :
            /*should not come here*/
            break;

    }

    /* The following API sends the V2 Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
}

/******************************************************************************
* Function : RMapMakeObjIdFromDotNew                                          *
* Input    : textStr
* Output   : NONE
* Returns  : oidPtr or NULL
*******************************************************************************/
static tSNMP_OID_TYPE *
RMapMakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *oidPtr = NULL;
    INT1               *tempPtr = NULL, *dotPtr = NULL;
    UINT2               i = 0;
    UINT2               dotCount = 0;
    UINT2               u2Len = 0;
    UINT4               u4BufferLen = 0;
    UINT4               u4tempBufferLen = 0;
    UINT4               u4Length = 0;
    UINT1              *pu1TmpPtr = NULL;
    INT1               *tempBuffer = NULL;
    INT1                ai1TmpArry[SNMP_MAX_OID_LENGTH + 1];

    tempBuffer = ai1TmpArry;

    MEMSET (tempBuffer, 0, sizeof (ai1TmpArry));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*textStr))
    {
        dotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (dotPtr == NULL)
            dotPtr = textStr + STRLEN ((INT1 *) textStr);
        tempPtr = textStr;

        for (i = 0; ((tempPtr < dotPtr) && (i < MAX_OID_LEN)); i++)
            tempBuffer[i] = *tempPtr++;
        tempBuffer[i] = '\0';

        for (i = 0; orig_mib_oid_table[i].pName != NULL; i++)
        {
            if ((STRCMP (orig_mib_oid_table[i].pName, (INT1 *) tempBuffer) ==
                 0)
                && (STRLEN ((INT1 *) tempBuffer) ==
                    STRLEN (orig_mib_oid_table[i].pName)))
            {
                u2Len =
                    (UINT2) (STRLEN (orig_mib_oid_table[i].pNumber) <
                             sizeof (ai1TmpArry) ?
                             STRLEN (orig_mib_oid_table[i].
                                     pNumber) : sizeof (ai1TmpArry) - 1);
                STRNCPY ((INT1 *) tempBuffer, orig_mib_oid_table[i].pNumber,
                         u2Len);
                tempBuffer[u2Len] = '\0';
                break;
            }
        }

        if (orig_mib_oid_table[i].pName == NULL)
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u4tempBufferLen = STRLEN (tempBuffer);
        u4BufferLen = (UINT2) (sizeof (tempBuffer) - STRLEN (tempBuffer) - 1);
        u4Length =
            (u4BufferLen < STRLEN (dotPtr) ? u4BufferLen : STRLEN (dotPtr));
        STRNCAT ((INT1 *) tempBuffer, (INT1 *) dotPtr, u4Length);
        tempBuffer[u4Length + u4tempBufferLen] = '\0';
    }
    else
    {                            /* is not alpha, so just copy into tempBuffer */
        STRCPY ((INT1 *) tempBuffer, (INT1 *) textStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    dotCount = 0;
    for (i = 0; ((i < sizeof (ai1TmpArry)) && (tempBuffer[i] != '\0')); i++)
    {
        if (tempBuffer[i] == '.')
            dotCount++;
    }
    if ((oidPtr = alloc_oid ((INT4) (dotCount + 1))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) tempBuffer;
    for (i = 0; i < dotCount + 1; i++)
    {
        if ((oidPtr->pu4_OidList[i] =
             ((UINT4) (RMapParseSubIdNew (&pu1TmpPtr)))) == (UINT4) -1)
        {
            free_oid (oidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
            pu1TmpPtr++;        /* to skip over dot */
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (oidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (oidPtr);
}

/******************************************************************************
* Function : RMapParseSubIdNew
* Input    : tempPtr
* Output   : None
* Returns  : value of tempPtr or -1
*******************************************************************************/
static INT4
RMapParseSubIdNew (UINT1 **tempPtr)
{
    INT4                value = 0;
    UINT1              *tmp = NULL;

    for (tmp = *tempPtr; (((*tmp >= '0') && (*tmp <= '9')) ||
                          ((*tmp >= 'a') && (*tmp <= 'f')) ||
                          ((*tmp >= 'A') && (*tmp <= 'F'))); tmp++)
    {
        value = (value * 10) + (*tmp & 0xf);
    }

    if (*tempPtr == tmp)
    {
        value = -1;
    }
    *tempPtr = tmp;
    return (value);
}

#endif /* defined(FUTURE_SNMP_WANTED) || defined (SNMP_3_WANTED) || defined (SNMPV3_WANTED) */
