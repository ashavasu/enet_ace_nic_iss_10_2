/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapcliu.c,v 1.11 2016/04/01 12:08:32 siva Exp $
 *
 * Description: This file has the handle routines for cli SET/GET
 *              destined for Route Map Module
 *
 *
 ********************************************************************/

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/
#include "rmapinc.h"
#include "cli.h"
#include "rmapcli.h"
#include "rmapcliu.h"
#include "fsrmapcli.h"
#include "utilcli.h"

/**************************************************************************/
/*   function name : RMapCliMatchDestIpXCreate                            */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - Destination Inet addrress type      */
/*                 : pInetAddrOct - Destination Inet addrress             */
/*                 : u4PrefixLen - Destination Inet addrress prefix length*/
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchDestIpXCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, UINT4 u4InetAddrType,
                           tSNMP_OCTET_STRING_TYPE * pInetAddrOct,
                           UINT4 u4PrefixLen)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct, u4PrefixLen, 0, 0,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
             u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
                 u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, u4InetAddrType,
                     pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
                         u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
            if (i4RetVal != CLI_SUCCESS)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
            }

        }
    }
    else
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchSourceIpXCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - Source Inet addrress type           */
/*                 : pInetAddrOct - Source Inet addrress                  */
/*                 : u4PrefixLen - Source Inet addrress prefix length     */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchSourceIpXCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4InetAddrType,
                             tSNMP_OCTET_STRING_TYPE * pInetAddrOct,
                             UINT4 u4PrefixLen)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType, pInetAddrOct,
         u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType,
             pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType, pInetAddrOct,
                 u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType,
                     pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType,
                         pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
            if (i4RetVal != CLI_SUCCESS)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
            }
        }
    }
    else
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchNextHopIpXCreate                         */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - NextHop Inet addrress type          */
/*                 : pInetAddrOct - NextHop Inet addrress                 */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchNextHopIpXCreate (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                              UINT4 u4SeqNo, UINT4 u4InetAddrType,
                              tSNMP_OCTET_STRING_TYPE * pInetAddrOct)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, u4InetAddrType, pInetAddrOct,
         0, 0, 0, 0, 0, 0, 0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0,
             u4InetAddrType, pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, u4InetAddrType,
                 pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0,
                     u4InetAddrType, pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0,
                         u4InetAddrType, pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchInterfaceCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4IfIndex - Interface                                */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchInterfaceCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4IfIndex)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, i4IfIndex, 0, 0, 0, 0,
         0, 0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
             i4IfIndex, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, i4IfIndex, 0, 0,
             0, 0, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
             i4IfIndex, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
             i4IfIndex, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*   function name : RMapCliMatchMetricCreate                             */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Metric - Metric                                    */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchMetricCreate (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                          INT4 i4Metric)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4Metric, 0, 0, 0, 0,
         0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             i4Metric, 0, 0, 0, 0, 0, 0, 0,
             RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4Metric, 0,
                 0, 0, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, i4Metric, 0, 0, 0, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         i4Metric, 0, 0, 0, 0, 0, 0, 0,
                         RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchTagCreate                                */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4Tag - Tag                                          */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchTagCreate (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                       UINT4 u4Tag)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4Tag, 0, 0, 0, 0,
         0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             u4Tag, 0, 0, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4Tag, 0,
                 0, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, u4Tag, 0, 0, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         u4Tag, 0, 0, 0, 0, 0, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchMetricTypeCreate                         */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4MetricType - Metric Type                           */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchMetricTypeCreate (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                              UINT4 u4SeqNo, INT4 i4MetricType)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4MetricType,
         0, 0, 0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             i4MetricType, 0, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 i4MetricType, 0, 0, 0, 0, 0,
                 RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, i4MetricType, 0, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         i4MetricType, 0, 0, 0, 0, 0,
                         RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchRouteTypeCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4RtType - Route Type                                */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchRouteTypeCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4RtType)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4RtType, 0,
         0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, i4RtType, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 i4RtType, 0, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, i4RtType, 0, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, i4RtType, 0, 0, 0, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchASPathTagCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4AsPath - AS path Tag                               */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchASPathTagCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4AsPath)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4AsPath,
         0, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, u4AsPath, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 u4AsPath, 0, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, u4AsPath, 0, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, 0, u4AsPath, 0, 0, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchCommunityCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4Community - Community                              */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchCommunityCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4Community)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         u4Community, 0, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, u4Community, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, u4Community, 0, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, u4Community, 0, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, 0, 0, u4Community, 0, 0,
                         RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchLocalPrefCreate                          */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Pref - Local Preference                            */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchLocalPrefCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4Pref)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i4Pref, 0, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, i4Pref, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, i4Pref, 0, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, i4Pref, 0,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, 0, 0, 0, i4Pref, 0, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchOriginCreate                             */
/*                                                                        */
/*   description   : This function creates route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Origin - Origin                                    */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchOriginCreate (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                          INT4 i4Origin)
{
    UINT4               u4ErrCode;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i4Origin, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, i4Origin, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, i4Origin, RMAP_CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsRMapMatchRowStatus
                    (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0, i4Origin,
                     RMAP_ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapMatchRowStatus
                        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                         0, 0, 0, 0, 0, i4Origin, RMAP_ACTIVE) == SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_CREATION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchDestIpXDelete                            */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - Destination Inet addrress type      */
/*                 : pInetAddrOct - Destination Inet addrress             */
/*                 : u4PrefixLen - Destination Inet addrress prefix length*/
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchDestIpXDelete (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, UINT4 u4InetAddrType,
                           tSNMP_OCTET_STRING_TYPE * pInetAddrOct,
                           UINT4 u4PrefixLen)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;
    UINT4               u4ErrCode;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsRMapMatchRowStatus
        (&u4ErrCode, pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
         u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         RMAP_DESTROY) == SNMP_SUCCESS)
    {

        if (nmhGetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct, u4PrefixLen,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
                 u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
            {
                if (nmhSetFsRMapMatchRowStatus
                    (pRMapNameOct, u4SeqNo, u4InetAddrType, pInetAddrOct,
                     u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     RMAP_DESTROY) == SNMP_SUCCESS)
                {
                    i4RetVal = CLI_SUCCESS;
                }
            }
        }
        if (i4RetVal != CLI_SUCCESS)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
        }

    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchSourceIpXDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - Source Inet addrress type           */
/*                 : pInetAddrOct - Source Inet addrress                  */
/*                 : u4PrefixLen - Source Inet addrress prefix length     */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchSourceIpXDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4InetAddrType,
                             tSNMP_OCTET_STRING_TYPE * pInetAddrOct,
                             UINT4 u4PrefixLen)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsRMapMatchRowStatus
        (&u4ErrCode, pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType,
         pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         RMAP_DESTROY) == SNMP_SUCCESS)
    {

        if (nmhGetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType, pInetAddrOct,
             u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType, pInetAddrOct,
                 u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
            {
                if (nmhSetFsRMapMatchRowStatus
                    (pRMapNameOct, u4SeqNo, 0, 0, 0, u4InetAddrType,
                     pInetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     RMAP_DESTROY) == SNMP_SUCCESS)
                {
                    i4RetVal = CLI_SUCCESS;
                }
            }
        }
        if (i4RetVal != CLI_SUCCESS)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
        }
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchNextHopIpXDelete                         */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4InetAddrType - NextHop Inet addrress type          */
/*                 : pInetAddrOct - NextHop Inet addrress                 */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchNextHopIpXDelete (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                              UINT4 u4SeqNo, UINT4 u4InetAddrType,
                              tSNMP_OCTET_STRING_TYPE * pInetAddrOct)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, u4InetAddrType, pInetAddrOct,
         0, 0, 0, 0, 0, 0, 0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, u4InetAddrType,
             pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, u4InetAddrType,
                 pInetAddrOct, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchInterfaceDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4IfIndex - Interface                                */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchInterfaceDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4IfIndex)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, i4IfIndex, 0, 0, 0, 0,
         0, 0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, i4IfIndex, 0, 0, 0,
             0, 0, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, i4IfIndex, 0, 0,
                 0, 0, 0, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchMetricDelete                             */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Metric - Metric                                    */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchMetricDelete (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                          INT4 i4Metric)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4Metric, 0, 0, 0, 0,
         0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4Metric, 0, 0,
             0, 0, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4Metric, 0,
                 0, 0, 0, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchTagDelete                                */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4Tag - Tag                                          */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchTagDelete (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                       UINT4 u4Tag)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4Tag, 0, 0, 0, 0,
         0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4Tag, 0, 0,
             0, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4Tag, 0,
                 0, 0, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchMetricTypeDelete                         */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4MetricType - Metric Type                           */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchMetricTypeDelete (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                              UINT4 u4SeqNo, INT4 i4MetricType)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4MetricType,
         0, 0, 0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             i4MetricType, 0, 0, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 i4MetricType, 0, 0, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchRouteTypeDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4RtType - Route Type                                */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchRouteTypeDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4RtType)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, i4RtType, 0,
         0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             i4RtType, 0, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 i4RtType, 0, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchASPathTagDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4AsPath - AS path Tag                               */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchASPathTagDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4AsPath)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, u4AsPath,
         0, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             u4AsPath, 0, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 u4AsPath, 0, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchCommunityDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : u4Community - Community                              */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchCommunityDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT4 u4Community)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         u4Community, 0, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             u4Community, 0, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, u4Community, 0, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchLocalPrefDelete                          */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Pref - Local Preference                            */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchLocalPrefDelete (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, INT4 i4Pref)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i4Pref, 0, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             i4Pref, 0, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, i4Pref, 0, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/**************************************************************************/
/*   function name : RMapCliMatchOriginDelete                             */
/*                                                                        */
/*   description   : This function deletes route map MATCH entry          */
/*                                                                        */
/*   input(s)      : CliHandle - cli handle                               */
/*                 : pu1RMapName - route map name                         */
/*                 : u4SeqNo - route map sequence number                  */
/*                 : i4Origin - Origin                                    */
/*                                                                        */
/*   output(s)     : none                                                 */
/*                                                                        */
/*   return values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RMapCliMatchOriginDelete (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                          INT4 i4Origin)
{
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsRMapMatchRowStatus
        (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i4Origin, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapMatchRowStatus
            (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, i4Origin, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapMatchRowStatus
                (pRMapNameOct, u4SeqNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 0, 0, 0, i4Origin, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MATCH_DELETION);
    }
    return i4RetVal;
}

/***************************************************************************/
/*   function name : RMapCliSetNextHopIpXCreate                            */
/*                                                                         */
/*   description   : Set NextHop Inet addrress type in route map SET table */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4InetAddrType - NextHop Inet addrress type           */
/*                 : pInetAddrOct - NextHop Inet addrress                  */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetNextHopIpXCreate (tCliHandle CliHandle,
                            tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                            UINT4 u4SeqNo, UINT4 u4InetAddrType,
                            tSNMP_OCTET_STRING_TYPE * pInetAddrOct)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetNextHopInetType
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4InetAddrType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetNextHopInetType
            (pRMapNameOct, u4SeqNo, u4InetAddrType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetNextHopInetAddr
            (&u4ErrCode, pRMapNameOct, u4SeqNo, pInetAddrOct) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetNextHopInetAddr
            (pRMapNameOct, u4SeqNo, pInetAddrOct) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetNextHopInetType
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4InetAddrType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetNextHopInetType
            (pRMapNameOct, u4SeqNo, u4InetAddrType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetNextHopInetAddr
            (&u4ErrCode, pRMapNameOct, u4SeqNo, pInetAddrOct) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetNextHopInetAddr
            (pRMapNameOct, u4SeqNo, pInetAddrOct) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetInterfaceCreate                             */
/*                                                                         */
/*   description   : Set Interface in route map SET table                  */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : i4IfIndex - Interface                                 */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetInterfaceCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, INT4 i4IfIndex)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetInterface
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4IfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetInterface
            (pRMapNameOct, u4SeqNo, i4IfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            if (nmhSetFsRMapSetRowStatus (pRMapNameOct,
                                          u4SeqNo,DESTROY)== SNMP_SUCCESS)
            {
                return (CLI_FAILURE);
            }
        }
        if (nmhTestv2FsRMapSetInterface
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4IfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetInterface
            (pRMapNameOct, u4SeqNo, i4IfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetMetricCreate                                */
/*                                                                         */
/*   description   : Set Metric in route map SET table                     */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : i4Metric - Metric                                     */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetMetricCreate (tCliHandle CliHandle,
                        tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                        INT4 i4Metric)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetMetric
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4Metric) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetMetric
            (pRMapNameOct, u4SeqNo, i4Metric) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetMetric
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4Metric) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetMetric
            (pRMapNameOct, u4SeqNo, i4Metric) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetTagCreate                                   */
/*                                                                         */
/*   description   : Set Tag in route map SET table                        */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4Tag - Tag                                           */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetTagCreate (tCliHandle CliHandle,
                     tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                     UINT4 u4Tag)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Tag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetTag (pRMapNameOct, u4SeqNo, u4Tag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Tag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetTag (pRMapNameOct, u4SeqNo, u4Tag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetRouteTypeCreate                             */
/*                                                                         */
/*   description   : Set RouteType in route map SET table                  */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4RouteType - RouteType                               */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetRouteTypeCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, UINT4 u4RouteType)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetRouteType
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4RouteType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRouteType
            (pRMapNameOct, u4SeqNo, u4RouteType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetRouteType
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4RouteType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRouteType
            (pRMapNameOct, u4SeqNo, u4RouteType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetASPathTagCreate                             */
/*                                                                         */
/*   description   : Set ASPath in route map SET table                     */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4AsPathTag - ASPath                                  */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetASPathTagCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, UINT4 u4AsPathTag)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetASPathTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4AsPathTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetASPathTag
            (pRMapNameOct, u4SeqNo, u4AsPathTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetASPathTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4AsPathTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetASPathTag
            (pRMapNameOct, u4SeqNo, u4AsPathTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetCommunityCreate                             */
/*                                                                         */
/*   description   : Set Community in route map SET table                  */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4Community - Community                               */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetCommunityCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, tSNMP_OCTET_STRING_TYPE * pRMapCommuityList, UINT1 u1Addflag)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetCommunity
            (&u4ErrCode, pRMapNameOct, u4SeqNo, pRMapCommuityList) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetCommunityAdditive
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u1Addflag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetCommunity
            (pRMapNameOct, u4SeqNo, pRMapCommuityList) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if ((u1Addflag!=1 )&& (nmhSetFsRMapSetCommunityAdditive
            (pRMapNameOct, u4SeqNo, u1Addflag)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetCommunity
            (&u4ErrCode, pRMapNameOct, u4SeqNo, pRMapCommuityList) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetCommunityAdditive
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u1Addflag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetCommunity
            (pRMapNameOct, u4SeqNo, pRMapCommuityList) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetCommunityAdditive
            (pRMapNameOct, u4SeqNo, u1Addflag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetLocalPrefCreate                             */
/*                                                                         */
/*   description   : Set Local Preference in route map SET table           */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : i4LocalPref - Local Preference                        */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetLocalPrefCreate (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                           UINT4 u4SeqNo, INT4 i4LocalPref)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetLocalPref
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4LocalPref) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetLocalPref
            (pRMapNameOct, u4SeqNo, i4LocalPref) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetLocalPref
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4LocalPref) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetLocalPref
            (pRMapNameOct, u4SeqNo, i4LocalPref) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetOriginCreate                                */
/*                                                                         */
/*   description   : Set Origin in route map SET table                     */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : i4Origin - Origin                                     */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetOriginCreate (tCliHandle CliHandle,
                        tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                        INT4 i4Origin)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetOrigin
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4Origin) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetOrigin
            (pRMapNameOct, u4SeqNo, i4Origin) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetOrigin
            (&u4ErrCode, pRMapNameOct, u4SeqNo, i4Origin) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetOrigin
            (pRMapNameOct, u4SeqNo, i4Origin) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetWeightCreate                                */
/*                                                                         */
/*   description   : Set Weight in route map SET table                     */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4Weight - Weight                                     */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetWeightCreate (tCliHandle CliHandle,
                        tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                        UINT4 u4Weight)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table already created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetWeight
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Weight) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetWeight
            (pRMapNameOct, u4SeqNo, u4Weight) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetWeight
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Weight) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetWeight
            (pRMapNameOct, u4SeqNo, u4Weight) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetEnableAutoTagCreate                         */
/*                                                                         */
/*   description   : Set EnableAutoTag in route map SET table              */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4EnableAutoTag - EnableAutoTag                       */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetEnableAutoTagCreate (tCliHandle CliHandle,
                               tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                               UINT4 u4SeqNo, UINT4 u4EnableAutoTag)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetEnableAutoTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo,
             u4EnableAutoTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetEnableAutoTag
            (pRMapNameOct, u4SeqNo, u4EnableAutoTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetEnableAutoTag
            (&u4ErrCode, pRMapNameOct, u4SeqNo,
             u4EnableAutoTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetEnableAutoTag
            (pRMapNameOct, u4SeqNo, u4EnableAutoTag) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetLevelCreate                                 */
/*                                                                         */
/*   description   : Set Level in route map SET table                      */
/*                   If SET table is not allocated, it's allocated now     */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                 : u4Level - Level                                       */
/*                                                                         */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetLevelCreate (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                       UINT4 u4Level)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table alresdy created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetLevel
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Level) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetLevel
            (pRMapNameOct, u4SeqNo, u4Level) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetLevel
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4Level) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetLevel
            (pRMapNameOct, u4SeqNo, u4Level) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetExtCommCostCreate                           */
/*                                                                         */
/*   description   : This function deletes entries in the SET Tablee       */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                   u1Cmd - Cmd tells which SET entry to delete           */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetExtCommCostCreate (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                             UINT4 u4SeqNo, UINT2 u2ExtCommId,
                             UINT4 u4ExtCommCost)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsRMapSetRowStatus
        (pRMapNameOct, u4SeqNo, &i4RowStatus) == SNMP_SUCCESS)
    {
        /* SET Table already created, make it NOT_IN_SERVICE and set the
         * SET entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetExtCommId
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u2ExtCommId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetExtCommId
            (pRMapNameOct, u4SeqNo, u2ExtCommId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetExtCommCost
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4ExtCommCost) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetExtCommCost
            (pRMapNameOct, u4SeqNo, u4ExtCommCost) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* SET Table not created, Create the SET table and set the Entry */
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetExtCommId
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u2ExtCommId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetExtCommId
            (pRMapNameOct, u4SeqNo, u2ExtCommId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhTestv2FsRMapSetExtCommCost
            (&u4ErrCode, pRMapNameOct, u4SeqNo, u4ExtCommCost) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetExtCommCost
            (pRMapNameOct, u4SeqNo, u4ExtCommCost) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
        if (nmhSetFsRMapSetRowStatus
            (pRMapNameOct, u4SeqNo, RMAP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_SET_CREATION);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliSetEntryDelete                                 */
/*                                                                         */
/*   description   : This function deletes entries in the SET Tablee       */
/*                                                                         */
/*   input(s)      : CliHandle - cli handle                                */
/*                 : pu1RMapName - route map name                          */
/*                 : u4SeqNo - route map sequence number                   */
/*                   u1Cmd - Cmd tells which SET entry to delete           */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliSetEntryDelete (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo,
                       UINT1 u1Cmd)
{

    tSNMP_OCTET_STRING_TYPE InetAddrOct;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE RMapCommunityList;
    UINT1               au1RMapCommunityList[RMAP_COMM_STR_LEN];

    UNUSED_PARAM (CliHandle);

    MEMSET (au1RMapCommunityList, 0, sizeof (au1RMapCommunityList));
    RMAP_INIT_OCT (InetAddrOct, au1InetAddrBuf);
    switch (u1Cmd)
    {
        case RMAP_CLI_NO_SET_NEXTHOP_IP4:
        case RMAP_CLI_NO_SET_NEXTHOP_IP6:
            if (nmhSetFsRMapSetNextHopInetType
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetNextHopInetAddr
                (pRMapNameOct, u4SeqNo, &InetAddrOct) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_INTERFACE:
            if (nmhSetFsRMapSetInterface
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_METRIC:
            if (nmhSetFsRMapSetMetric
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_TAG:
            if (nmhSetFsRMapSetTag (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_ROUTE_TYPE:
            if (nmhSetFsRMapSetRouteType
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_ASPATH_TAG:
            if (nmhSetFsRMapSetASPathTag
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_COMMUNITY:
              
            RMAP_INIT_OCT (RMapCommunityList,au1RMapCommunityList);
            if (nmhSetFsRMapSetCommunity
                (pRMapNameOct, u4SeqNo, RMapCommunityList) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_LOCAL_PREF:
            if (nmhSetFsRMapSetLocalPref
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_ORIGIN:
            if (nmhSetFsRMapSetOrigin
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_WEIGHT:
            if (nmhSetFsRMapSetWeight
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_EXTCOMM_COST:
            if (nmhSetFsRMapSetExtCommId
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            if (nmhSetFsRMapSetExtCommCost
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_AUTO_TAG_ENA:
            if (nmhSetFsRMapSetEnableAutoTag
                (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        case RMAP_CLI_NO_SET_LEVEL:
            if (nmhSetFsRMapSetLevel (pRMapNameOct, u4SeqNo, 0) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRMapSetRowStatus
                (pRMapNameOct, u4SeqNo, RMAP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_SET_DELETION);
                return (CLI_FAILURE);
            }
            break;

        default:
            return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function name : RmapCliValidateIpAddrAndPrefix                        */
/*                                                                         */
/*   Description   : This function validates the given string whether it   */
/*                   contains valid IPv4/IPV6 address and Prefix length    */
/*                                                                         */
/*   Input(s)      : pu1AddrAndMask - pointer to the string contains       */
/*                                    Ip address and prefix length         */
/*                   u1AddrType     - IPv4/IPV6 address                    */
/*   Output(s)     : pAddr - IPv4/IPV6 Address extracted from the string   */
/*                   pu4PrefixLen - Prefix length extracted                */
/*                                                                         */
/*   Return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RmapCliValidateIpAddrAndPrefix (UINT1 *pu1AddrAndMask, UINT1 u1AddrType,
                                tIpAddr * pAddr, UINT4 *pu4PrefixLen)
{
    tUtlInAddr          IpAddr;
    tUtlIn6Addr         Ip6Addr;
    CHR1               *pu1Token = NULL;

    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

    pu1Token = STRTOK (pu1AddrAndMask, "/");

    if (pu1Token == NULL)
    {
        return CLI_FAILURE;
    }

    if (u1AddrType == RMAP_IPV4_ADDR_TYPE)
    {
        if (UtlInetAton ((CONST CHR1 *) pu1Token, &IpAddr) == 0)
        {
            return CLI_FAILURE;
        }
        MEMCPY (pAddr, &IpAddr, sizeof (tUtlInAddr));
    }
    else
    {
        if (UtlInetAton6 ((CONST CHR1 *) pu1Token, &Ip6Addr) == 0)
        {
            return CLI_FAILURE;
        }
        MEMCPY (pAddr, &Ip6Addr, sizeof (tUtlIn6Addr));
    }

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token == NULL)
    {
        return CLI_FAILURE;
    }

    *pu4PrefixLen = (UINT4) ATOI (pu1Token);

    if ((u1AddrType == RMAP_IPV4_ADDR_TYPE)
        && (*pu4PrefixLen > RMAP_MAX_IPV4_PREFIX_LEN))
    {
        return CLI_FAILURE;
    }
    else if ((u1AddrType == RMAP_IPV6_ADDR_TYPE)
             && (*pu4PrefixLen > RMAP_MAX_IPV6_PREFIX_LEN))
    {
        return CLI_FAILURE;
    }

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token != NULL)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
