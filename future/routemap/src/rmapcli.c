/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapcli.c,v 1.30 2016/04/01 12:08:32 siva Exp $
 *
 * Description: This file has the handle routines for cli SET/GET
 *              destined for Route Map Module
 *
 *
 ********************************************************************/

#ifndef __RMAPCLI_C__
#define __RMAPCLI_C__

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/
#include "rmapinc.h"
#include "cli.h"
#include "rmapcli.h"
#include "rmapcliu.h"
#include "fsrmapcli.h"
#endif

extern INT4         CliGetCurModePromptStr (INT1 *);
extern tRMapGlobalInfo gRMapGlobalInfo;
/********************************************************/
/*            STATIC FUNCTIONS                          */
/********************************************************/
INT4                RMapCliCreate (tCliHandle CliHandle,
                                   tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                                   UINT4 u4SeqNum, UINT4 u4Access);

INT4                RMapCliDelete (tCliHandle CliHandle,
                                   tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                                   UINT4 u4SeqNum);

INT4                RMapCliShow (tCliHandle CliHandle, UINT1 u1DisplayFlag,
                                 tSNMP_OCTET_STRING_TYPE * pGivenRMapNameOct);

VOID                RMapCliShowMatchEntries (tCliHandle CliHandle,
                                             UINT1 *pu1Prefix,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pGivenMapNameOct,
                                             UINT4 u4GivenSeqNo,
                                             INT4 i4IsIpPrefix,
                                             UINT1 u1AddrType, UINT1 u1ShwCmd,
                                             tRMapXAddrInfo * pIpAddrInfo,
                                             UINT1 u1Flag,
                                             UINT1 *pu1DisplayName);

VOID                RMapCliShowSetEntries (tCliHandle CliHandle,
                                           UINT1 *pu1Prefix,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pGivenMapNameOct,
                                           UINT4 u4GivenSeqNo);

INT4                RMapCliShowMapNode (tCliHandle CliHandle,
                                        tSNMP_OCTET_STRING_TYPE * pRMapNameOct,
                                        UINT4 u4SeqNo);

VOID                RMapCliShowParams (tCliHandle CliHandle, UINT1 *pu1Prefix,
                                       INT4 i4DestInetType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pDestInetAddress, UINT4 u4DestInetPrefix,
                                       INT4 i4SourceInetType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSourceInetAddress,
                                       UINT4 u4SourceInetPrefix,
                                       INT4 i4NextHopInetType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextHopInetAddr, INT4 i4Interface,
                                       INT4 i4Metric, UINT4 u4Tag,
                                       INT4 i4MetricType, INT4 i4RouteType,
                                       UINT4 u4ASPathTag, UINT4 u4Community,
                                       INT4 i4LocalPref, INT4 i4Origin,
                                       UINT2 u2Weight, UINT1 u1AutoTagEna,
                                       UINT1 u1Level, UINT2 u2ExtCommId,
                                       UINT4 u4ExtCommCost, INT4 i4Additive, 
                                       tSNMP_OCTET_STRING_TYPE *pRetValFsRMapSetCommunity);

INT4                RMapCliGetRMapName (tCliHandle CliHandle, UINT1 *pRMapName);

/********************************************************/

INT4
cli_process_routemap_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tRMapNode          *pRMapNode = NULL;
    UINT4              *args[RMAP_CLI_MAX_ARGS];
    tIpPrefixInfo       IpPrefixInfo;
    tIpPrefixInfo      *pIpPrefixInfo = NULL;
    tRMapXAddrInfo      IpAddrInfo;
    UINT4               u4ErrCode = 0;
    UINT4               u4SeqNo = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT1                argno = 0;
    UINT1               u1DisplayName = 0;

    UINT4               u4PrefixLen;
    tSNMP_OCTET_STRING_TYPE InetAddrOct;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE InetAddrPrefixOct;
    UINT1               au1InetAddrPrefixBuf[IPVX_MAX_INET_ADDR_LEN];

    tSNMP_OCTET_STRING_TYPE RMapNameOct;
    UINT1               au1RMapNameBuf[RMAP_MAX_NAME_LEN + 4];

    
    CHR1 *pAsNoinp = NULL;
    tSNMP_OCTET_STRING_TYPE RMapCommunityList;
    UINT1               au1RMapCommunityList[RMAP_COMM_STR_LEN];

    MEMSET (&IpPrefixInfo, 0, sizeof (tIpPrefixInfo));
    MEMSET (&IpAddrInfo, 0, sizeof (tRMapXAddrInfo));

    /* prepare octet strings */
    RMAP_INIT_OCT (InetAddrOct, au1InetAddrBuf);
    RMAP_INIT_OCT (RMapNameOct, au1RMapNameBuf);
    RMAP_INIT_OCT (InetAddrPrefixOct, au1InetAddrPrefixBuf);
    
    CliRegisterLock (CliHandle, RMapLockWrite, RMapUnLockWrite);
    RMapLockWrite ();

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfIndex = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RMAP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    /* For Match and Set Command operations get the SeqNo and RouteMap name
     * from the Cli Prompt */

    if ((u4Command > RMAP_CLI_COMMANDS_WITH_PROMT_START) &&
        (u4Command < RMAP_CLI_COMMANDS_WITH_PROMT_END))
    {
        /* Get the SeqNo of the RouteMap */
        u4SeqNo = CLI_GET_RMAP_SEQNO ();

        /* Get the Route Map Name */
        if ((RMapCliGetRMapName (CliHandle, au1RMapNameBuf)) == CLI_FAILURE)
            return CLI_FAILURE;
        RMapNameOct.i4_Length = STRLEN (au1RMapNameBuf);
    }

    switch (u4Command)
    {
        case RMAP_CLI_CREATE_RMAP:
            /* args[0] - Route Map Name, args[1] - SeqNo, args[2] - Access */
            RMAP_ASCIIZ_2_OCT (&RMapNameOct, args[0]);

            i4RetStatus =
                RMapCliCreate (CliHandle, &RMapNameOct, CLI_PTR_TO_U4 (args[1]),
                               CLI_PTR_TO_U4 (args[2]));
            break;

        case RMAP_CLI_NO_RMAP:
            /* args[0] - Route Map Name, args[1] - SeqNo */
            RMAP_ASCIIZ_2_OCT (&RMapNameOct, args[0]);

            i4RetStatus =
                RMapCliDelete (CliHandle, &RMapNameOct,
                               CLI_PTR_TO_U4 (args[1]));
            break;

        case RMAP_CLI_IP_PREFIX_LIST:
            /* args [0] - IP Prefix list name
             * args [1] - sequence number
             * args [2] - Address type (ipv4/ipv6)
             * args [3] - IP address / NULL
             * args [4] - Prefix length
             * args [5] - Action (Permit/Deny)
             * args [6] - Min Prefix Length
             * args [7] - Max Prefix Length
             * args [8] - Row status
             */

            RMAP_ASCIIZ_2_OCT (&RMapNameOct, args[0]);

            if (args[3] != NULL)
            {
                IpPrefixInfo.AddrInfo.InetXAddr.u1Afi =
                    (UINT1) CLI_PTR_TO_U4 (args[2]);

                if (IpPrefixInfo.AddrInfo.InetXAddr.u1Afi ==
                    IPVX_ADDR_FMLY_IPV4)
                {
                    IpPrefixInfo.AddrInfo.InetXAddr.u1AddrLen =
                        IPVX_IPV4_ADDR_LEN;
                }
                else if (IpPrefixInfo.AddrInfo.InetXAddr.u1Afi ==
                         IPVX_ADDR_FMLY_IPV6)
                {
                    IpPrefixInfo.AddrInfo.InetXAddr.u1AddrLen =
                        IPVX_IPV6_ADDR_LEN;
                }

                MEMCPY (IpPrefixInfo.AddrInfo.InetXAddr.au1Addr, args[3],
                        IpPrefixInfo.AddrInfo.InetXAddr.u1AddrLen);

                IpPrefixInfo.AddrInfo.u4PrefixLen = CLI_PTR_TO_U4 (args[4]);
                IpPrefixInfo.u1MinPrefixLen = (UINT1) CLI_PTR_TO_U4 (args[6]);
                IpPrefixInfo.u1MaxPrefixLen = (UINT1) CLI_PTR_TO_U4 (args[7]);
                pIpPrefixInfo = &IpPrefixInfo;
            }
            if (CLI_PTR_TO_U4 (args[8]) == CREATE_AND_GO)
            {
                i4RetStatus =
                    RMapCliCreateIpPrefixList (CliHandle, &RMapNameOct,
                                               CLI_PTR_TO_U4 (args[1]),
                                               pIpPrefixInfo,
                                               CLI_PTR_TO_U4 (args[4]),
                                               (UINT1) CLI_PTR_TO_U4 (args[5]));
            }
            else if (CLI_PTR_TO_U4 (args[8]) == DESTROY)
            {
                i4RetStatus = RMapCliDelIpPrefixList (CliHandle, &RMapNameOct,
                                                      CLI_PTR_TO_U4 (args[1]),
                                                      CLI_PTR_TO_U4 (args[2]),
                                                      pIpPrefixInfo,
                                                      CLI_PTR_TO_U4 (args[4]),
                                                      (UINT1)
                                                      CLI_PTR_TO_U4 (args[5]));
            }

            break;
        case RMAP_CLI_SHOW_RMAP:
            /* args[0] - Type, can be   RMAP_SHOW_ONE_MAP_BY_NAME, 
             *                          RMAP_SHOW_ALL_MAPS */
            /* args[1] - route map name if Type is RMAP_SHOW_ONE_MAP_BY_NAME */
            if (args[1] != NULL)
            {
                RMAP_ASCIIZ_2_OCT (&RMapNameOct, args[1]);
            }
            else
            {
                RMAP_CLEAR_OCT (&RMapNameOct, sizeof (au1RMapNameBuf));
            }

            i4RetStatus =
                RMapCliShow (CliHandle, ((UINT1) CLI_PTR_TO_U4 (args[0])),
                             &RMapNameOct);
            break;

        case RMAP_CLI_SHOW_IP_PREFIX_LIST:
            /* args [0] - IP Prefix name or NULL
             * args [1] - Sequence number
             * args [2] - Address type IPv4/IPv6
             * args [3] - IP address
             * args [4] - Prefix length
             * args [5] - Flag to identify all matching prefixes or not
             */

            if (args[0] == NULL)
            {
                i4RetStatus =
                    RMapCliShowIpPrefixListName (CliHandle, NULL,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[2]),
                                                 RMAP_SHOW_ALL_MAPS, NULL, 0);
            }
            else
            {
                RMAP_ASCIIZ_2_OCT (&RMapNameOct, args[0]);
                if ((CLI_PTR_TO_U4 (args[1]) == 0) && (args[3] == NULL))
                {
                    i4RetStatus =
                        RMapCliShowIpPrefixListName (CliHandle, &RMapNameOct,
                                                     (UINT1)
                                                     CLI_PTR_TO_U4 (args[2]),
                                                     (UINT1)
                                                     RMAP_SHOW_ONE_MAP_BY_NAME,
                                                     NULL, 0);
                }
                else if (CLI_PTR_TO_U4 (args[1]) != 0)
                {
                    pRMapNode =
                        RMapGetRouteMapNode (RMapNameOct.pu1_OctetList,
                                             CLI_PTR_TO_U4 (args[1]));
                    if (pRMapNode != NULL)
                    {
                        if (pRMapNode->u1IsIpPrefixInfo != TRUE)
                        {
                            return CLI_FAILURE;
                        }
                        u1DisplayName = TRUE;
                        RMapCliShowMatchEntries (CliHandle, (UINT1 *) "",
                                                 &RMapNameOct,
                                                 CLI_PTR_TO_U4 (args[1]), TRUE,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[2]),
                                                 (UINT1) TRUE, NULL, 0,
                                                 &u1DisplayName);
                    }

                }
                else
                {
                    IpAddrInfo.InetXAddr.u1Afi =
                        (UINT1) CLI_PTR_TO_U4 (args[2]);

                    if (IpAddrInfo.InetXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        IpAddrInfo.InetXAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                    }
                    else if (IpAddrInfo.InetXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                    {
                        IpAddrInfo.InetXAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
                    }

                    MEMCPY (IpAddrInfo.InetXAddr.au1Addr, args[3],
                            IpAddrInfo.InetXAddr.u1AddrLen);

                    IpAddrInfo.u4PrefixLen = CLI_PTR_TO_U4 (args[4]);

                    i4RetStatus =
                        RMapCliShowIpPrefixListName (CliHandle, &RMapNameOct,
                                                     (UINT1)
                                                     CLI_PTR_TO_U4 (args[2]),
                                                     (UINT1)
                                                     RMAP_SHOW_ONE_MAP_BY_NAME,
                                                     &IpAddrInfo,
                                                     (UINT1)
                                                     CLI_PTR_TO_U4 (args[5]));
                }
            }
            break;

        case RMAP_CLI_MATCH_DST_IP4:
            /* args[0] - ip4 address, args[1]-  Mask */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            IPV4_MASK_TO_MASKLEN (u4PrefixLen, *args[1]);

            i4RetStatus = RMapCliMatchDestIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_NO_MATCH_DST_IP4:
            /* args[0] - ip4 address, args[1]-  Mask */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            IPV4_MASK_TO_MASKLEN (u4PrefixLen, *args[1]);

            i4RetStatus = RMapCliMatchDestIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_MATCH_DST_IP6:
            /* args[0] - ipv6 address as ascii, args[1]-  Prefix Lenght UINT4 */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            u4PrefixLen = *args[1];
            if ((u4PrefixLen < 1) || (u4PrefixLen > 128))
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_PREFIXLEN);
                break;
            }
            Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InetAddrPrefixOct.pu1_OctetList,
                             (tIp6Addr *) (VOID *) InetAddrOct.pu1_OctetList,
                             (INT4)u4PrefixLen);

            InetAddrPrefixOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            i4RetStatus = RMapCliMatchDestIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrPrefixOct, u4PrefixLen);
            break;

        case RMAP_CLI_NO_MATCH_DST_IP6:
            /* args[0] - ipv6 address as ascii, args[1]-  Prefix Lenght UINT4 */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            u4PrefixLen = *args[1];
            if ((u4PrefixLen < 1) || (u4PrefixLen > 128))
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_PREFIXLEN);
                break;
            }
            Ip6CopyAddrBits ((tIp6Addr *) (VOID *) InetAddrPrefixOct.pu1_OctetList,
                             (tIp6Addr *) (VOID *) InetAddrOct.pu1_OctetList,
                             (INT4)u4PrefixLen);

            InetAddrPrefixOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            i4RetStatus = RMapCliMatchDestIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrPrefixOct, u4PrefixLen);
            
            break;

        case RMAP_CLI_MATCH_SRC_IP4:
            /* args[0] - ipv4 address UINT4*, args[1]-  netmask UINT4* */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            IPV4_MASK_TO_MASKLEN (u4PrefixLen, *args[1]);

            i4RetStatus = RMapCliMatchSourceIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_NO_MATCH_SRC_IP4:
            /* args[0] - ipv4 address UINT4*, args[1]-  netmask UINT4* */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            IPV4_MASK_TO_MASKLEN (u4PrefixLen, *args[1]);

            i4RetStatus = RMapCliMatchSourceIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_MATCH_SRC_IP6:
            /* args[0] - ipv6 address as ascii, args[1]-  Prefix Lenght UINT4 */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            u4PrefixLen = *args[1];
            if ((u4PrefixLen < 1) || (u4PrefixLen > 128))
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_PREFIXLEN);
                break;
            }

            i4RetStatus = RMapCliMatchSourceIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_NO_MATCH_SRC_IP6:
            /* args[0] - ipv6 address as ascii, args[1]-  Prefix Lenght UINT4 */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            u4PrefixLen = *args[1];
            if ((u4PrefixLen < 1) || (u4PrefixLen > 128))
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_PREFIXLEN);
                break;
            }

            i4RetStatus = RMapCliMatchSourceIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrOct, u4PrefixLen);
            break;

        case RMAP_CLI_MATCH_NEXTHOP_IP4:
            /* args[0] - ip4 address */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            i4RetStatus = RMapCliMatchNextHopIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct);
            break;

        case RMAP_CLI_NO_MATCH_NEXTHOP_IP4:
            /* args[0] - ip4 address */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            i4RetStatus = RMapCliMatchNextHopIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct);
            break;

        case RMAP_CLI_MATCH_NEXTHOP_IP6:
            /* args[0] - ipv6 address as ascii */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            i4RetStatus = RMapCliMatchNextHopIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrOct);
            break;

        case RMAP_CLI_NO_MATCH_NEXTHOP_IP6:
            /* args[0] - ipv6 address as ascii */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                CLI_SET_ERR (RMAP_CLI_ERR_INVALID_IP6_ADDR);
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            i4RetStatus = RMapCliMatchNextHopIpXDelete
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrOct);
            break;

        case RMAP_CLI_MATCH_INTERFACE:
            i4RetStatus = RMapCliMatchInterfaceCreate
                (CliHandle, &RMapNameOct, u4SeqNo, i4IfIndex);
            break;

        case RMAP_CLI_NO_MATCH_INTERFACE:
            i4RetStatus = RMapCliMatchInterfaceDelete
                (CliHandle, &RMapNameOct, u4SeqNo, i4IfIndex);
            break;

        case RMAP_CLI_MATCH_METRIC:
            i4RetStatus = RMapCliMatchMetricCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_MATCH_METRIC:
            i4RetStatus = RMapCliMatchMetricDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_MATCH_TAG:
            i4RetStatus = RMapCliMatchTagCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_MATCH_TAG:
            i4RetStatus = RMapCliMatchTagDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_MATCH_METRIC_TYPE:
            i4RetStatus = RMapCliMatchMetricTypeCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_MATCH_METRIC_TYPE:
            i4RetStatus = RMapCliMatchMetricTypeDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_MATCH_ROUTE_TYPE:
            i4RetStatus = RMapCliMatchRouteTypeCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_MATCH_ROUTE_TYPE:
            i4RetStatus = RMapCliMatchRouteTypeDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_MATCH_ASPATH_TAG:
            i4RetStatus = RMapCliMatchASPathTagCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_MATCH_ASPATH_TAG:
            i4RetStatus = RMapCliMatchASPathTagDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_MATCH_COMMUNITY:
            i4RetStatus = RMapCliMatchCommunityCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_MATCH_COMMUNITY:
            i4RetStatus = RMapCliMatchCommunityDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_MATCH_LOCAL_PREF:
            i4RetStatus = RMapCliMatchLocalPrefCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_MATCH_LOCAL_PREF:
            i4RetStatus = RMapCliMatchLocalPrefDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_MATCH_ORIGIN:
            i4RetStatus = RMapCliMatchOriginCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_MATCH_ORIGIN:
            i4RetStatus = RMapCliMatchOriginDelete
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_SET_NEXTHOP_IP4:
            /* args[0] - ip4 address */

            /* prepare oct addr */
            RMAP_BUF_2_OCT (&InetAddrOct, (UINT1 *) args[0], sizeof (UINT4));

            i4RetStatus = RMapCliSetNextHopIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV4,
                 &InetAddrOct);
            break;

        case RMAP_CLI_NO_SET_NEXTHOP_IP4:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_NEXTHOP_IP4);
            break;

        case RMAP_CLI_SET_NEXTHOP_IP6:
            /* args[0] - ipv6 address as ascii */

            /* prepare oct addr */
            if (INET_ATON6 (args[0], InetAddrOct.pu1_OctetList) == 0)
            {
                break;
            }
            InetAddrOct.i4_Length = IPVX_IPV6_ADDR_LEN;

            i4RetStatus = RMapCliSetNextHopIpXCreate
                (CliHandle, &RMapNameOct, u4SeqNo, IPVX_ADDR_FMLY_IPV6,
                 &InetAddrOct);
            break;

        case RMAP_CLI_NO_SET_NEXTHOP_IP6:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_NEXTHOP_IP6);
            break;

        case RMAP_CLI_SET_INTERFACE:
            i4RetStatus = RMapCliSetInterfaceCreate
                (CliHandle, &RMapNameOct, u4SeqNo, i4IfIndex);
            break;

        case RMAP_CLI_NO_SET_INTERFACE:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_INTERFACE);
            break;

        case RMAP_CLI_SET_METRIC:
            i4RetStatus = RMapCliSetMetricCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_SET_METRIC:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_METRIC);
            break;

        case RMAP_CLI_SET_TAG:
            i4RetStatus = RMapCliSetTagCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_TAG:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_TAG);
            break;

        case RMAP_CLI_SET_ROUTE_TYPE:
            i4RetStatus = RMapCliSetRouteTypeCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_ROUTE_TYPE:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_ROUTE_TYPE);
            break;

        case RMAP_CLI_SET_ASPATH_TAG:
            i4RetStatus = RMapCliSetASPathTagCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_ASPATH_TAG:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_ASPATH_TAG);
            break;

        case RMAP_CLI_SET_COMMUNITY:
            
            RMAP_INIT_OCT (RMapCommunityList,au1RMapCommunityList);

            if(*args[0] == RMAP_COMM_COMMNUM)
            {
              pAsNoinp = (CHR1 *) args[2];
            }
            else if(*args[0] == RMAP_COMM_INTERNET)
            {
              pAsNoinp = "internet ";
            }
            else if (*args[0] == RMAP_COMM_LOCALAS)
            {
              pAsNoinp = "local-as ";
            }
            else if (*args[0] == RMAP_COMM_NO_ADVT)
            {
              pAsNoinp = "no-advt ";
            }
            else if (*args[0] == RMAP_COMM_NO_EXPORT)
            {
              pAsNoinp =  "no-export ";
            }
            else if (*args[0] == RMAP_COMM_NONE)
            {
               pAsNoinp = "none ";
            }  
            if (pAsNoinp != NULL)
            {
            if ( STRLEN(pAsNoinp) <= RMAP_COMM_STR_LEN)
            {
              STRNCPY(au1RMapCommunityList,pAsNoinp,STRLEN(pAsNoinp));
            }
            }
            /*convert ASCII to octet string for community*/
            RMAP_ASCIIZ_2_OCT (&RMapCommunityList,au1RMapCommunityList);
            
            i4RetStatus = RMapCliSetCommunityCreate
                 (CliHandle, &RMapNameOct, u4SeqNo, &RMapCommunityList, *args[1]);
            break;

        case RMAP_CLI_NO_SET_COMMUNITY:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_COMMUNITY);
            break;

        case RMAP_CLI_SET_LOCAL_PREF:
            i4RetStatus = RMapCliSetLocalPrefCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_SET_LOCAL_PREF:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_LOCAL_PREF);
            break;

        case RMAP_CLI_SET_ORIGIN:
            i4RetStatus = RMapCliSetOriginCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(INT4 *) args[0]);
            break;

        case RMAP_CLI_NO_SET_ORIGIN:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_ORIGIN);
            break;

        case RMAP_CLI_SET_WEIGHT:
            i4RetStatus = RMapCliSetWeightCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_WEIGHT:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_WEIGHT);
            break;

        case RMAP_CLI_SET_AUTO_TAG_ENA:
            i4RetStatus = RMapCliSetEnableAutoTagCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_AUTO_TAG_ENA:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo,
                 RMAP_CLI_NO_SET_AUTO_TAG_ENA);
            break;

        case RMAP_CLI_SET_LEVEL:
            i4RetStatus = RMapCliSetLevelCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *args[0]);
            break;

        case RMAP_CLI_NO_SET_LEVEL:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo, RMAP_CLI_NO_SET_LEVEL);
            break;

        case RMAP_CLI_SET_EXTCOMM_COST:
            i4RetStatus = RMapCliSetExtCommCostCreate
                (CliHandle, &RMapNameOct, u4SeqNo, *(UINT2 *) args[0],
                 *args[1]);
            break;

        case RMAP_CLI_NO_SET_EXTCOMM_COST:
            i4RetStatus = RMapCliSetEntryDelete
                (CliHandle, &RMapNameOct, u4SeqNo,
                 RMAP_CLI_NO_SET_EXTCOMM_COST);
            break;

        default:
            CliPrintf (CliHandle, "\r\nInvalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > RMAP_CLI_ERR_NONE) && (u4ErrCode < RMAP_CLI_ERR_MAX))
        {
            CliPrintf (CliHandle, "\r%%%s\r\n", RMapCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (RMAP_CLI_ERR_NONE);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);
    RMapUnLockWrite ();
    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*************************************************************************/
/*   Function Name : RMapCliCreate                                           */
/*                                                                        */
/*   Description   : This creates route map with the given route map name */
/*                   sequence number, access command                      */
/*                                                                        */
/*   Input(s)      :  1. CliHandle - CLI context ID                       */
/*                    2. pRMapName - Route Map Name                       */
/*                    3. u4SeqNo - Sequence Number                        */
/*                    4. u4Access - Permit/Deny                           */
/*   Output(s)     : None                                                 */
/*                                                                        */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                            */
/**************************************************************************/
INT4
RMapCliCreate (tCliHandle CliHandle,
               tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNum,
               UINT4 u4Access)
{
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    INT4                i4RouteMapRowStatus;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4ErrCode;
    UINT4               u4RmapOld_Access = 0;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pRMapNameOct);

    if ((pRMapNameOct->i4_Length == 0) || 
          (STRSTR(pRMapNameOct->pu1_OctetList,";") != NULL))
    {
	CLI_SET_ERR(RMAP_CLI_ERR_MAP_CREATION);
	return CLI_FAILURE;
    }
    /* ' ' found in RMapName, we quit */
    if (STRSTR(pRMapNameOct->pu1_OctetList," ") != NULL  )
    {
	CLI_SET_ERR(RMAP_CLI_ERR_SPACE_NOT_ALLOWED);
	return CLI_FAILURE;
    }
    pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                               (tRBElem *) & IpPrefixNode);
    if (pIpPrefixNode != NULL)
    {
        if (pIpPrefixNode->u1IsIpPrefixList == TRUE)
        {
            CLI_SET_ERR (RMAP_CLI_IP_PREFIX_EXIST_WITH_GIVEN_NAME);
            return CLI_FAILURE;
        }
    }

    /* create new MAP node */
    if (nmhGetFsRMapRowStatus (pRMapNameOct, u4SeqNum,
                               &i4RouteMapRowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRMapRowStatus
            (&u4ErrCode, pRMapNameOct, u4SeqNum, RMAP_CREATE_AND_WAIT)
            != SNMP_FAILURE)
        {
            if (nmhSetFsRMapRowStatus
                (pRMapNameOct, u4SeqNum, RMAP_CREATE_AND_WAIT) != SNMP_FAILURE)
            {
                if (nmhSetFsRMapAccess (pRMapNameOct, u4SeqNum,
                                        (INT4) u4Access) == SNMP_SUCCESS)
                {
                    if (nmhTestv2FsRMapRowStatus
                        (&u4ErrCode, pRMapNameOct, u4SeqNum, RMAP_ACTIVE)
                        != SNMP_FAILURE)
                    {
                        if (nmhSetFsRMapRowStatus
                            (pRMapNameOct, u4SeqNum, RMAP_ACTIVE)
                            != SNMP_FAILURE)
                        {
                            i4RetVal = CLI_SUCCESS;
                        }
                    }
                }
            }
        }
    }

    /* edit existing MAP node: change access or modify MATCH/SET */
    else
    {
        if (nmhGetFsRMapAccess
            (pRMapNameOct, u4SeqNum,
             (INT4 *) &u4RmapOld_Access) == SNMP_SUCCESS)
        {
            if (nmhSetFsRMapRowStatus
                (pRMapNameOct, u4SeqNum, RMAP_NOT_IN_SERVICE) != SNMP_FAILURE)
            {
                if (nmhSetFsRMapAccess
                    (pRMapNameOct, u4SeqNum, (INT4) u4Access) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRMapRowStatus
                        (pRMapNameOct, u4SeqNum, RMAP_ACTIVE) != SNMP_FAILURE)
                    {
                        i4RetVal = CLI_SUCCESS;
                    }
                }
            }
        }

    }

    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ERR_MAP_CREATION);
    }
    else
    {
        /* change CLI prompt */
        MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
        SPRINTF ((CHR1 *) au1Cmd, "%s%s%02u",
                 CLI_RMAP_MODE, pRMapNameOct->pu1_OctetList, u4SeqNum);

        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to enter into route map mode\r\n");
            i4RetVal = CLI_FAILURE;
        }
    }

    return i4RetVal;
}

/**************************************************************************/
/*   Function Name : RMapCliDelete                                           */
/*                                                                        */
/*   Description   : This function deltes the route map with the given    */
/*                   route map name, sequence number.                     */
/*                                                                        */
/*   Input(s)      :  1. CliHandle - CLI context ID                       */
/*                    2. pRMapName - Route Map Name                       */
/*                    3. u4SeqNo - Sequence Number                        */
/*   Output(s)     : None                                                 */
/*                                                                        */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                            */
/**************************************************************************/
INT4
RMapCliDelete (tCliHandle CliHandle,
               tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNum)
{
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    INT4                i4RouteMapRowStatus;
    INT4                i4RetVal = CLI_FAILURE;
    tRMapNode          *pRMapNode;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    tRMapMatchNode      RMapMatchNode;
    tRMapMatchNode     *pRMapNextMatchNode = NULL;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];


    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));

    UNUSED_PARAM (CliHandle);

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pRMapNameOct);
    pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                               (tRBElem *) & IpPrefixNode);
    if (pIpPrefixNode != NULL)
    {
        if (pIpPrefixNode->u1IsIpPrefixList == TRUE)
        {
            CLI_SET_ERR (RMAP_CLI_ERR_MAP_DELETION);
            return CLI_FAILURE;
        }
    }

    if (nmhGetFsRMapRowStatus
        (pRMapNameOct, u4SeqNum, &i4RouteMapRowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRMapRowStatus
            (pRMapNameOct, u4SeqNum, RMAP_NOT_IN_SERVICE) == SNMP_SUCCESS)
        {
	    RMAP_OCT_2_ASCIIZ (au1Name, pRMapNameOct);
            /* Send the update message to RTM4/RTM6 */
            RMapSendUpdateMsg (au1Name);

            pRMapNode = RMapGetRouteMapNode (au1Name, u4SeqNum);
            if (pRMapNode != NULL)
            {
               pRMapMatchNode = (tRMapMatchNode *) RBTreeGetFirst
                    (pRMapNode->RMapMatchRoot);
             while (pRMapMatchNode != NULL)
             {
               MEMSET (&RMapMatchNode, 0, sizeof (tRMapMatchNode));
               MEMCPY (&RMapMatchNode, pRMapMatchNode,
                          sizeof (tRMapMatchNode));
               if ((RMapDeleteMatchNode (pRMapNode, pRMapMatchNode)) ==
                   RMAP_FAILURE)
               {
                    return RMAP_FAILURE;
               }
                    pRMapNextMatchNode =
                        RBTreeGetNext (pRMapNode->RMapMatchRoot, &RMapMatchNode,
                                       NULL);
                    pRMapMatchNode = pRMapNextMatchNode;
             } /*end of while*/
            }

            if (nmhSetFsRMapRowStatus
                (pRMapNameOct, u4SeqNum, RMAP_DESTROY) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }

    }

    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (RMAP_CLI_ENTRY_NOT_EXIST);
    }

    return i4RetVal;
}

/**************************************************************************/
/*   Function Name : RMapCliShow                                             */
/*                                                                        */
/*   Description   : This function displays route map entry with the given*/
/*                    route map name sequence number                      */
/*                                                                        */
/*   Input(s)      :  1. CliHandle - CLI context ID                       */
/*                    2. pRMapName - Route Map Name                       */
/*                    3. u4SeqNo - Sequence Number                        */
/*                    4. u4Access - Permit/Deny                           */
/*   Output(s)     : None                                                 */
/*                                                                        */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                            */
/**************************************************************************/
INT4
RMapCliShow (tCliHandle CliHandle,
             UINT1 u1DisplayFlag, tSNMP_OCTET_STRING_TYPE * pGivenRMapNameOct)
{
    tSNMP_OCTET_STRING_TYPE RMapNameOct;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT1               au1RMapNameBuf[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4SeqNo = 0;
    INT4                i4RowStatus = 0;
    INT4                i4IsIpPrefix = 0;
    INT4                i4Result;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));
    RMAP_INIT_OCT (RMapNameOct, au1RMapNameBuf);

    if (u1DisplayFlag == RMAP_SHOW_ALL_MAPS)
    {
        /* get 1st valid name and seqnum */
        if (nmhGetFirstIndexFsRMapTable (&RMapNameOct, &u4SeqNo)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (u1DisplayFlag == RMAP_SHOW_ONE_MAP_BY_NAME)
    {
        if (pGivenRMapNameOct == NULL)
        {
            return CLI_FAILURE;
        }

        RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pGivenRMapNameOct);
        pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                   (tRBElem *) & IpPrefixNode);
        if (pIpPrefixNode != NULL)
        {
            if (pIpPrefixNode->u1IsIpPrefixList == TRUE)
            {
                return CLI_FAILURE;
            }
        }

        /* get 1st valid seqnum for this name */
        if (nmhGetNextIndexFsRMapTable
            (pGivenRMapNameOct, &RMapNameOct, 0, &u4SeqNo) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* if given name is invalid, we got seqnum of next name */
        if (RMAP_CMP_OCT (pGivenRMapNameOct, &RMapNameOct) != 0)
        {
            return CLI_FAILURE;
        }
    }

    /* loop thru MAP nodes */
    do
    {
        if (nmhGetFsRMapIsIpPrefixList (&RMapNameOct, u4SeqNo, &i4IsIpPrefix) ==
            SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsIpPrefix == TRUE)
        {
            continue;
        }
        /* select nodes for this name */
        if (u1DisplayFlag == RMAP_SHOW_ONE_MAP_BY_NAME)
        {
            if (RMAP_CMP_OCT (pGivenRMapNameOct, &RMapNameOct) != 0)
            {
                /* no more next MAP node for this map name */
                break;
            }
        }

        /* check status of this MAP node */
        i4Result = nmhGetFsRMapRowStatus (&RMapNameOct, u4SeqNo, &i4RowStatus);

        if (i4Result == SNMP_SUCCESS)
        {
            if (i4RowStatus == RMAP_ACTIVE)
            {
                /* Show map name, seqnum, access */
                i4Result =
                    RMapCliShowMapNode (CliHandle, &RMapNameOct, u4SeqNo);

                if (i4Result == CLI_SUCCESS)
                {
                    /* Show Match Entries */
                    CliPrintf (CliHandle, "\r\n Match Clauses:\r\n");
                    CliPrintf (CliHandle, " --------------\r\n");
                    RMapCliShowMatchEntries (CliHandle, (UINT1 *) "",
                                             &RMapNameOct, u4SeqNo,
                                             i4IsIpPrefix, 0, 0, NULL, 0, 0);
                    CliPrintf (CliHandle, "\r\n");

                    /* Show Set Entries */
                    CliPrintf (CliHandle, "\r\n Set Clauses:\r\n");
                    CliPrintf (CliHandle, " --------------\r\n");
                    RMapCliShowSetEntries (CliHandle, (UINT1 *) "",
                                           &RMapNameOct, u4SeqNo);
                    CliPrintf (CliHandle, "\r\n");
                }

            }
        }
    }
    while (nmhGetNextIndexFsRMapTable
           (&RMapNameOct, &RMapNameOct, u4SeqNo, &u4SeqNo) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliShowMapNode                                       */
/*                                                                         */
/*   description   : Display map name, sequential number, access           */
/*                                                                         */
/*   input(s)      :  1. clihandle - cli context id                        */
/*                    2. pRMapNameOct - Pointer on the Route Map Name      */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS - if map node exists                      */
/*                   CLI_FAILURE - if map node does not exist              */
/***************************************************************************/
INT4
RMapCliShowMapNode (tCliHandle CliHandle,
                    tSNMP_OCTET_STRING_TYPE * pRMapNameOct, UINT4 u4SeqNo)
{
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    INT4                i4Access;

    if (nmhGetFsRMapAccess (pRMapNameOct, u4SeqNo, &i4Access) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    RMAP_OCT_2_ASCIIZ (au1Name, pRMapNameOct);

    if (i4Access == RMAP_PERMIT)
    {
        CliPrintf (CliHandle, "\r\n Route-map %s, Permit, Sequence %d\r\n",
                   au1Name, u4SeqNo);
    }
    else if (i4Access == RMAP_DENY)
    {
        CliPrintf (CliHandle, "\r\n Route-map %s, Deny, Sequence %d\r\n",
                   au1Name, u4SeqNo);
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliShowMatchEntries                               */
/*                                                                         */
/*   description   : This function shows MATCH clauses of given MAP node   */
/*                                                                         */
/*   input(s)      : clihandle - cli context id                            */
/*                   pu1Prefix - 1st word in each line - "", "match"       */
/*                   pGivenMapNameOct - Pointer to the Route Map Name      */
/*                   u4GivenSeqNo - sequential number                      */
/*                   i4IsIpPrefix - Flag to identify whether it is         */
/*                                  IP prefix entry or not                 */
/*                                  (used only if i4IsIpPrefix is TRUE)    */
/*                   u1AddrType   - Address type                           */
/*                   u1ShwCmd     - Flag to identify whether it is SRC flow*/
/*                                  or Show command flow                   */
/*                                  (used only if i4IsIpPrefix is TRUE)    */
/*                   pIpAddrInfo  - NULL/pointer to the network address    */
/*                                  info whose corresponding IP Prefix     */
/*                                  needs to be displayed (used only if    */
/*                                  i4IsIpPrefix and u1ShwCmd are TRUE)    */
/*                   u1Flag       -Flag to check whether to display all the*/
/*                                 entries matching with pIpAddrInfo or not*/
/*                                 (appl if pIpAddrInfo not NULL)          */
/*                   pu1DisplayName - Flag to check whether to display the  */
/*                                   Ip Prefix list name or not            */
/***************************************************************************/
VOID
RMapCliShowMatchEntries (tCliHandle CliHandle, UINT1 *pu1Prefix,
                         tSNMP_OCTET_STRING_TYPE * pGivenMapNameOct,
                         UINT4 u4GivenSeqNo, INT4 i4IsIpPrefix,
                         UINT1 u1AddrType, UINT1 u1ShwCmd,
                         tRMapXAddrInfo * pIpAddrInfo, UINT1 u1Flag,
                         UINT1 *pu1DisplayName)
{
    tIPvXAddr           InetXAddr;
    tSNMP_OCTET_STRING_TYPE MapNameOct;
    UINT4               u4SeqNum = 0;

    INT4                i4DestInetType = 0;
    tSNMP_OCTET_STRING_TYPE DestInetAddressOct;
    UINT4               u4DestInetPrefix = 0;

    INT4                i4SourceInetType = 0;
    tSNMP_OCTET_STRING_TYPE SourceInetAddressOct;
    UINT4               u4SourceInetPrefix = 0;

    INT4                i4NextHopInetType = 0;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddrOct;

    INT4                i4Interface = 0;
    INT4                i4Metric = 0;
    UINT4               u4Tag = 0;
    INT4                i4MetricType = 0;
    INT4                i4RouteType = 0;
    UINT4               u4ASPathTag = 0;
    UINT4               u4Community = 0;
    INT4                i4LocalPref = 0;
    INT4                i4Origin = 0;
    UINT4               u4MaxPrefixLen = 0;
    UINT4               u4MinPrefixLen = 0;
    INT4                i4Access = 0;
    tSNMP_OCTET_STRING_TYPE RetValFsRMapSetCommunity;
    UINT1               au1CommunityList[RMAP_COMM_STR_LEN];

#define GET_NEXT_MATCH_NODE \
nmhGetNextIndexFsRMapMatchTable( \
&MapNameOct,            &MapNameOct,\
u4SeqNum,               &u4SeqNum,\
\
i4DestInetType,         &i4DestInetType,\
&DestInetAddressOct,    &DestInetAddressOct,\
u4DestInetPrefix,       &u4DestInetPrefix,\
\
i4SourceInetType,       &i4SourceInetType,\
&SourceInetAddressOct,  &SourceInetAddressOct,\
u4SourceInetPrefix,     &u4SourceInetPrefix,\
\
i4NextHopInetType,      &i4NextHopInetType,\
&NextHopInetAddrOct,    &NextHopInetAddrOct,\
\
i4Interface,    &i4Interface,\
i4Metric,       &i4Metric,\
u4Tag,          &u4Tag,\
i4MetricType,   &i4MetricType,\
i4RouteType,    &i4RouteType,\
u4ASPathTag,    &u4ASPathTag,\
\
u4Community,    &u4Community,\
i4LocalPref,    &i4LocalPref,\
i4Origin,       &i4Origin)

    UINT1               au1Buf1[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1Buf3[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Buf5[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Buf7[IPVX_MAX_INET_ADDR_LEN];
    INT1                i1Result;

    /* prepare octet strings */
    RMAP_INIT_OCT (MapNameOct, au1Buf1);
    RMAP_INIT_OCT (DestInetAddressOct, au1Buf3);
    RMAP_INIT_OCT (SourceInetAddressOct, au1Buf5);
    RMAP_INIT_OCT (NextHopInetAddrOct, au1Buf7);
    RMAP_INIT_OCT (RetValFsRMapSetCommunity,au1CommunityList);    

    MEMSET (&InetXAddr, 0, sizeof (tIPvXAddr));
    /* init indices */
    RMAP_COPY_OCT (&MapNameOct, pGivenMapNameOct);
    u4SeqNum = u4GivenSeqNo;

    /* get 1st MATCH node for this map - invoke get-next with zero initial params */
    while (1)
    {
        i1Result = GET_NEXT_MATCH_NODE;

        /* if no more MATCH node - exit */
        if (i1Result != SNMP_SUCCESS)
        {
            return;
        }

        /* if MAP node name and seqnum found - stop */
        if ((RMAP_CMP_OCT (pGivenMapNameOct, &MapNameOct) == 0) &&
            (u4GivenSeqNo == u4SeqNum))
        {
            break;
        }
    }

    while (1)
    {
        if (i4IsIpPrefix != TRUE)
        {
            RMapCliShowParams (CliHandle, pu1Prefix,
                               i4DestInetType,
                               &DestInetAddressOct,
                               u4DestInetPrefix,
                               i4SourceInetType,
                               &SourceInetAddressOct,
                               u4SourceInetPrefix,
                               i4NextHopInetType,
                               &NextHopInetAddrOct,
                               i4Interface,
                               i4Metric,
                               u4Tag,
                               i4MetricType,
                               i4RouteType,
                               u4ASPathTag,
                               u4Community, i4LocalPref, i4Origin, 0, 0, 0, 0,
                               0, 0, &RetValFsRMapSetCommunity);
        }
        /* If it is IP Prefix entry */
        else
        {
            /* For SHOW commands dont display the IP prefix related commands
             * It should be displayed only for SRC*/
            if (u1ShwCmd != TRUE)
            {
                if (i4DestInetType == IPVX_ADDR_FMLY_IPV4)
                {
                    CliPrintf (CliHandle, "ip prefix-list %s",
                               MapNameOct.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "ipv6 prefix-list %s",
                               MapNameOct.pu1_OctetList);
                }
            }
            /* For IP Prefix entry SHOW commands */
            else
            {
                /* Check whether the address type of the entry
                 * is same as the expected address type or not.
                 * If not dont display the IP Prefix entry.
                 */
                if (i4DestInetType != u1AddrType)
                {
                    return;
                }

                InetXAddr.u1Afi = (UINT1) i4DestInetType;

                if (InetXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    InetXAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                }
                else if (InetXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    InetXAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
                }

                MEMCPY (&InetXAddr.au1Addr, DestInetAddressOct.pu1_OctetList,
                        InetXAddr.u1AddrLen);

                /* If pIpAddrInfo is not NULL, then the entry matches with the given 
                 * IP address only should be displayed*/
                if (pIpAddrInfo != NULL)
                {
                    /* If the flag is not set, then display the entry which exactly
                     * matches with the given IP-address and prefix length*/
                    if (u1Flag == 0)
                    {
                        if (u4DestInetPrefix != pIpAddrInfo->u4PrefixLen)
                        {
                            return;
                        }

                        if (CmpIpvxAddrWithPrefix
                            (&pIpAddrInfo->InetXAddr,
                             (UINT1) pIpAddrInfo->u4PrefixLen, &InetXAddr,
                             (UINT1) u4DestInetPrefix) != 0)
                        {
                            return;
                        }
                    }
                    /* If the flag is set, then display the entries with exact matches and
                     * longer matches (Match atleast upto the prefix length)
                     */
                    else
                    {
                        if (MatchIpvxAddrWithPrefix
                            (&pIpAddrInfo->InetXAddr, &InetXAddr,
                             pIpAddrInfo->u4PrefixLen) != TRUE)
                        {
                            return;
                        }
                    }
                }
                if (*pu1DisplayName == TRUE)
                {
                    CliPrintf (CliHandle, "\r\nPrefix list name : %s\n",
                               pGivenMapNameOct->pu1_OctetList);
                    *pu1DisplayName = FALSE;
                }
            }

            CliPrintf (CliHandle, " seq %d ", u4SeqNum);

            if (nmhGetFsRMapAccess (pGivenMapNameOct, u4GivenSeqNo, &i4Access)
                == SNMP_FAILURE)
            {
                return;
            }

            if (i4Access == RMAP_PERMIT)
            {
                CliPrintf (CliHandle, "permit ");
            }
            else
            {
                CliPrintf (CliHandle, "deny ");
            }
            if (i4DestInetType == IPVX_ADDR_FMLY_IPV4)
            {
                CliPrintf (CliHandle, "%d.%d.%d.%d/%d ",
                           DestInetAddressOct.pu1_OctetList[0],
                           DestInetAddressOct.pu1_OctetList[1],
                           DestInetAddressOct.pu1_OctetList[2],
                           DestInetAddressOct.pu1_OctetList[3],
                           u4DestInetPrefix);
            }
            else
            {
                CliPrintf (CliHandle, "%s/%d ",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         DestInetAddressOct.pu1_OctetList),
                           u4DestInetPrefix);
            }

            nmhGetFsRMapMatchDestMinPrefixLen (&MapNameOct, u4SeqNum,
                                               i4DestInetType,
                                               &DestInetAddressOct,
                                               u4DestInetPrefix, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0, 0, 0,
                                               &u4MinPrefixLen);
            if (u4MinPrefixLen != 0)
            {
                CliPrintf (CliHandle, "ge %d ", u4MinPrefixLen);
            }

            nmhGetFsRMapMatchDestMaxPrefixLen (&MapNameOct, u4SeqNum,
                                               i4DestInetType,
                                               &DestInetAddressOct,
                                               u4DestInetPrefix, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0, 0, 0,
                                               &u4MaxPrefixLen);
            if (u4MaxPrefixLen != 0)
            {
                CliPrintf (CliHandle, "le %d", u4MaxPrefixLen);
            }
            CliPrintf (CliHandle, "\r\n");
        }
        i1Result = GET_NEXT_MATCH_NODE;

        /* if no more MATCH node - exit */
        if (i1Result != SNMP_SUCCESS)
        {
            return;
        }

        /* if MAP node name or seqnum changed - exit */
        if ((RMAP_CMP_OCT (pGivenMapNameOct, &MapNameOct) != 0) ||
            (u4GivenSeqNo != u4SeqNum))
        {
            return;
        }
    }
}

/***************************************************************************/
/*   function name : RMapCliShowSetEntries                                    */
/*                                                                         */
/*   description   : This function shows SET clauses of given MAP node     */
/*                                                                         */
/*   input(s)      : clihandle - cli context id                            */
/*                   pu1Prefix - 1st word in each line - "", "set"         */
/*                   pGivenMapNameOct - Pointer to the Route Map Name      */
/*                   u4GivenSeqNo - sequential number                      */
/***************************************************************************/
VOID
RMapCliShowSetEntries (tCliHandle CliHandle, UINT1 *pu1Prefix,
                       tSNMP_OCTET_STRING_TYPE * pGivenMapNameOct,
                       UINT4 u4GivenSeqNo)
{
    INT4                i4NextHopInetType = 0;
    tSNMP_OCTET_STRING_TYPE NextHopInetAddrOct;
    tSNMP_OCTET_STRING_TYPE RetValFsRMapSetCommunity;

    INT4                i4Interface = 0;
    INT4                i4Metric = 0;
    UINT4               u4Tag = 0;
    INT4                i4MetricType = 0;
    INT4                i4RouteType = 0;
    UINT4               u4ASPathTag = 0;
    UINT4               u4Community = 0;
    INT4                i4LocalPref = 0;
    INT4                i4Origin = 0;

    UINT4               u4Weight = 0;
    INT4                i4AutoTagEna = 0;
    INT4                i4Level = 0;
    INT4                i4Additive = 0;

    UINT4               u4ExtCommCost = 0;
    UINT4               u4ExtCommId = 0;

    UINT1               au1Buf1[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CommunityList[RMAP_COMM_STR_LEN];

    /* prepare octet strings */
    RMAP_INIT_OCT (NextHopInetAddrOct, au1Buf1);
    RMAP_INIT_OCT (RetValFsRMapSetCommunity,au1CommunityList);

    /* get params of SET node */
    nmhGetFsRMapSetNextHopInetType (pGivenMapNameOct, u4GivenSeqNo,
                                    &i4NextHopInetType);
    nmhGetFsRMapSetNextHopInetAddr (pGivenMapNameOct, u4GivenSeqNo,
                                    &NextHopInetAddrOct);

    nmhGetFsRMapSetInterface (pGivenMapNameOct, u4GivenSeqNo, &i4Interface);
    nmhGetFsRMapSetMetric (pGivenMapNameOct, u4GivenSeqNo, &i4Metric);
    nmhGetFsRMapSetTag (pGivenMapNameOct, u4GivenSeqNo, &u4Tag);
    nmhGetFsRMapSetRouteType (pGivenMapNameOct, u4GivenSeqNo, &i4RouteType);

    nmhGetFsRMapSetASPathTag (pGivenMapNameOct, u4GivenSeqNo, &u4ASPathTag);
    nmhGetFsRMapSetCommunity (pGivenMapNameOct, u4GivenSeqNo, &RetValFsRMapSetCommunity);
    nmhGetFsRMapSetLocalPref (pGivenMapNameOct, u4GivenSeqNo, &i4LocalPref);
    nmhGetFsRMapSetOrigin (pGivenMapNameOct, u4GivenSeqNo, &i4Origin);

    nmhGetFsRMapSetWeight (pGivenMapNameOct, u4GivenSeqNo, &u4Weight);
    nmhGetFsRMapSetEnableAutoTag (pGivenMapNameOct, u4GivenSeqNo,
                                  &i4AutoTagEna);
    nmhGetFsRMapSetLevel (pGivenMapNameOct, u4GivenSeqNo, &i4Level);
    nmhGetFsRMapSetExtCommCost (pGivenMapNameOct, u4GivenSeqNo, &u4ExtCommCost);
    nmhGetFsRMapSetExtCommId (pGivenMapNameOct, u4GivenSeqNo, &u4ExtCommId);
    nmhGetFsRMapSetCommunityAdditive (pGivenMapNameOct, u4GivenSeqNo,
                                      &i4Additive);

    RMapCliShowParams (CliHandle, pu1Prefix,
                       0,
                       0,
                       0,
                       0,
                       0,
                       0,
                       i4NextHopInetType,
                       &NextHopInetAddrOct,
                       i4Interface,
                       i4Metric,
                       u4Tag,
                       i4MetricType,
                       i4RouteType,
                       u4ASPathTag,
                       u4Community,
                       i4LocalPref,
                       i4Origin,
                       (UINT2) u4Weight, (UINT1) i4AutoTagEna, (UINT1) i4Level,
                       (UINT2) u4ExtCommId, u4ExtCommCost, i4Additive,&RetValFsRMapSetCommunity);
}

/**************************************************************************
 * Function Name : RMapCliGetRMapPrompt
 * Description   : This routine is returns the prompt to be displayed 
 *                  for route
 *                 map module.
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *************************************************************************/
INT1
RMapCliGetRMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    INT1                ai1RMapName[RMAP_MAX_NAME_LEN + 1];
    INT1                ai1RMapMode[RMAP_CLI_RMAP_MODE_LEN];
    UINT4               u4SeqNo;
    UINT4               u4Len = 0;
    UINT4               u4Size = 0;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    MEMSET (ai1RMapMode, 0, RMAP_CLI_RMAP_MODE_LEN);

    /* Copy the Route Map mode name ie config-rmap- */
    MEMCPY (ai1RMapMode, pi1ModeName, RMAP_CLI_RMAP_MODE_LEN);
    pi1ModeName = pi1ModeName + RMAP_CLI_RMAP_MODE_LEN;

    if (STRNCMP (ai1RMapMode, CLI_RMAP_MODE, RMAP_CLI_RMAP_MODE_LEN) != 0)
    {
        return FALSE;
    }

    /* Copy the Route map name amd seq no */
    u4Len = STRLEN (pi1ModeName);
    MEMSET (ai1RMapName, 0, RMAP_MAX_NAME_LEN);
    u4Size = MEM_MAX_BYTES ((u4Len - 2), (RMAP_MAX_NAME_LEN));
    MEMCPY (ai1RMapName, pi1ModeName, u4Size);
    ai1RMapName[u4Size] = '\0';

    pi1ModeName = pi1ModeName + u4Size;
    u4SeqNo = CLI_ATOI (pi1ModeName);

    CLI_SET_RMAP_SEQNO (u4SeqNo);

    STRCPY (pi1DispStr, "(config-rmap-");
    STRCAT (pi1DispStr, ai1RMapName);
    STRCAT (pi1DispStr, ")#");

    return TRUE;
}

/***************************************************************************/
/*   function name : RMapCliGetRMapName                                       */
/*                                                                         */
/*   description   : This function gets Route Map Name from the Cli Context*/
/*                                                                         */
/*   input(s)      :  1. clihandle - cli context id                        */
/*                    2. pRMapName - Pointer to get the Route Map Name     */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/

INT4
RMapCliGetRMapName (tCliHandle CliHandle, UINT1 *pRMapName)
{
    INT1                ai1Name[CLI_RMAP_MAX_NAME_LEN];
    INT4                i4Len = 0;
    INT1               *pi1Name = NULL;

    MEMSET (pRMapName, 0, RMAP_MAX_NAME_LEN + 4);
    MEMSET (ai1Name, 0, CLI_RMAP_MAX_NAME_LEN);

    if (CLI_GET_RMAP_NAME (ai1Name) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "Couldn't get the Route Map name");
        return CLI_FAILURE;
    }
    /* Route Name will be in the format of "(config-rmap-<rmapname>)#", 
     * from this extract route name alone by eliminating 
     * "(config-rmap-" and ") #" */

    i4Len = STRLEN (ai1Name) - STRLEN (CLI_RMAP_MODE) - 3;

    if (i4Len < 0)
    {
        CliPrintf (CliHandle,
                   "Couldn't extract the Route Map name from prompt");
        return CLI_FAILURE;
    }

    /* it is safe because i4Len is positive and equal
     * to ai1Name - STRLEN (CLI_RMAP_MODE) -3
     */
    pi1Name = ai1Name + STRLEN (CLI_RMAP_MODE) + 1;
    if (i4Len <= RMAP_MAX_NAME_LEN)
    {
        MEMCPY (pRMapName, pi1Name, i4Len);
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "Couldn't extract the Route Map name from prompt");
    return CLI_FAILURE;
}

/***************************************************************************/
/*   function name : RMapShowRunningConfig                                 */
/*                                                                         */
/*   description   : This function displays the Configurations for route   */
/*                   map.                                                  */
/*                                                                         */
/*   input(s)      : CliHandle - cli context id                            */
/*   output(s)     : none                                                  */
/*                                                                         */
/*   return values : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/

INT4
RMapShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RMapNameOct;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4SeqNo;
    INT4                i4Access;
    INT4                i4Result;
    INT4                i4IsIpPrefix = 0;

    RMAP_INIT_OCT (RMapNameOct, au1Name);

    /* get 1st valid name and seqnum */
    if (nmhGetFirstIndexFsRMapTable (&RMapNameOct, &u4SeqNo) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* loop thru MAP nodes */
    do
    {
        /* Show map name, seqnum, access */
        RMAP_PUT_ENDING_ZERO (RMapNameOct);

        if (nmhGetFsRMapAccess (&RMapNameOct, u4SeqNo, &i4Access) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsRMapIsIpPrefixList (&RMapNameOct, u4SeqNo, &i4IsIpPrefix) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* If it is IP Prefix entry don't display route-map commands */
        if (i4IsIpPrefix != TRUE)
        {
            if (i4Access == RMAP_PERMIT)
            {
                if (u4SeqNo == 1)
                {
                    CliPrintf (CliHandle, "route-map %s \r\n",
                               RMapNameOct.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "route-map %s %d\r\n",
                               RMapNameOct.pu1_OctetList, u4SeqNo);
                }
            }
            else if (i4Access == RMAP_DENY)
            {
                if (u4SeqNo == 1)
                {
                    CliPrintf (CliHandle, "route-map %s deny\r\n",
                               RMapNameOct.pu1_OctetList, u4SeqNo);
                }
                else
                {
                    CliPrintf (CliHandle, "route-map %s deny %d\r\n",
                               RMapNameOct.pu1_OctetList, u4SeqNo);
                }
            }
        }

        /* Show Match Entries */
        RMapCliShowMatchEntries (CliHandle, (UINT1 *) " match", &RMapNameOct,
                                 u4SeqNo, i4IsIpPrefix, 0, 0, NULL, 0, 0);

        /* For IP Prefix entry, Set node will not be used */
        if (i4IsIpPrefix != TRUE)
        {
            /* Show Set Entries */
            RMapCliShowSetEntries (CliHandle, (UINT1 *) " set", &RMapNameOct,
                                   u4SeqNo);
        }
        /* Get Next route Map Entry */
        i4Result = nmhGetNextIndexFsRMapTable
            (&RMapNameOct, &RMapNameOct, u4SeqNo, &u4SeqNo);

        CliPrintf (CliHandle, "!\r\n");

        /* no more next MAP node at all */
        if (i4Result != SNMP_SUCCESS)
        {
            break;
        }

    }
    while (1);

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   function name : RMapCliShowParams                                     */
/*                                                                         */
/*   description   : This function shows MATCH/SET parameters              */
/*                   of given MAP node                                     */
/*                                                                         */
/*   input(s)      : CliHandle - cli context id                            */
/*                   pu1Prefix - 1st word in each line - "", "match", "set"*/
/*                   ... - parameters                                      */
/***************************************************************************/
VOID
RMapCliShowParams (tCliHandle CliHandle, UINT1 *pu1Prefix,
                   INT4 i4DestInetType,
                   tSNMP_OCTET_STRING_TYPE * pDestInetAddress,
                   UINT4 u4DestInetPrefix,
                   INT4 i4SourceInetType,
                   tSNMP_OCTET_STRING_TYPE * pSourceInetAddress,
                   UINT4 u4SourceInetPrefix,
                   INT4 i4NextHopInetType,
                   tSNMP_OCTET_STRING_TYPE * pNextHopInetAddr,
                   INT4 i4Interface,
                   INT4 i4Metric,
                   UINT4 u4Tag,
                   INT4 i4MetricType,
                   INT4 i4RouteType,
                   UINT4 u4ASPathTag,
                   UINT4 u4Community,
                   INT4 i4LocalPref,
                   INT4 i4Origin,
                   UINT2 u2Weight, UINT1 u1AutoTagEna, UINT1 u1Level,
                   UINT2 u2ExtCommId, UINT4 u4ExtCommCost, INT4 i4Additive, 
                   tSNMP_OCTET_STRING_TYPE *pRetValFsRMapSetCommunity)
{
    tIPvXAddr           DstXAddr, SrcXAddr, NhXAddr;
    UINT4               u4IpAddr, u4NetMask, u4TempIpAddr;
    UINT1               au1Buf[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1RMapCommunityList[RMAP_COMM_STR_LEN];
    CHR1                *pAsNoinpToken;
    CHR1                *ptoken = " ";
    BOOL1                 bFlag = FALSE;

    IPVX_ADDR_CLEAR (&DstXAddr);
    IPVX_ADDR_CLEAR (&SrcXAddr);
    IPVX_ADDR_CLEAR (&NhXAddr);

    if (pDestInetAddress != NULL)
    {
        IPVX_ADDR_INIT (DstXAddr, i4DestInetType,
                        pDestInetAddress->pu1_OctetList);
    }
    if (pSourceInetAddress != NULL)
    {
        IPVX_ADDR_INIT (SrcXAddr, i4SourceInetType,
                        pSourceInetAddress->pu1_OctetList);
    }
    if (pNextHopInetAddr != NULL)
    {
        IPVX_ADDR_INIT (NhXAddr, i4NextHopInetType,
                        pNextHopInetAddr->pu1_OctetList);
    }

    /********************** show nonzero params *********************/
    if (TestZeroIpvxAddr (&DstXAddr) != 0)
    {
        if (DstXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TempIpAddr, DstXAddr.au1Addr, sizeof (UINT4));
            u4IpAddr = OSIX_NTOHL (u4TempIpAddr);
            IPV4_MASKLEN_TO_MASK (u4NetMask, u4DestInetPrefix);
            u4NetMask = OSIX_NTOHL (u4NetMask);
            CliPrintf (CliHandle,
                       "%s destination ip %d.%d.%d.%d  %d.%d.%d.%d\r\n",
                       pu1Prefix, ((UINT1 *) &u4IpAddr)[0],
                       ((UINT1 *) &u4IpAddr)[1], ((UINT1 *) &u4IpAddr)[2],
                       ((UINT1 *) &u4IpAddr)[3], ((UINT1 *) &u4NetMask)[0],
                       ((UINT1 *) &u4NetMask)[1], ((UINT1 *) &u4NetMask)[2],
                       ((UINT1 *) &u4NetMask)[3]);
        }
        else if (DstXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%s destination ipv6 %s  %d\r\n",
                       pu1Prefix,
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) DstXAddr.au1Addr),
                       u4DestInetPrefix);
        }
    }

    if (TestZeroIpvxAddr (&SrcXAddr) != 0)
    {
        if (SrcXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TempIpAddr, SrcXAddr.au1Addr, sizeof (UINT4));
            u4IpAddr = OSIX_NTOHL (u4TempIpAddr);
            IPV4_MASKLEN_TO_MASK (u4NetMask, u4SourceInetPrefix);
            u4NetMask = OSIX_NTOHL (u4NetMask);
            CliPrintf (CliHandle, "%s source ip %d.%d.%d.%d  %d.%d.%d.%d\r\n",
                       pu1Prefix,
                       ((UINT1 *) &u4IpAddr)[0],
                       ((UINT1 *) &u4IpAddr)[1],
                       ((UINT1 *) &u4IpAddr)[2],
                       ((UINT1 *) &u4IpAddr)[3],
                       ((UINT1 *) &u4NetMask)[0],
                       ((UINT1 *) &u4NetMask)[1],
                       ((UINT1 *) &u4NetMask)[2], ((UINT1 *) &u4NetMask)[3]);
        }
        else if (SrcXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%s source ipv6 %s  %d\r\n",
                       pu1Prefix,
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) SrcXAddr.au1Addr),
                       u4SourceInetPrefix);
        }
    }

    if (TestZeroIpvxAddr (&NhXAddr) != 0)
    {
        if (NhXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TempIpAddr, NhXAddr.au1Addr, sizeof (UINT4));
            u4IpAddr = OSIX_NTOHL (u4TempIpAddr);
            CliPrintf (CliHandle, "%s next-hop ip %d.%d.%d.%d\r\n",
                       pu1Prefix,
                       ((UINT1 *) &u4IpAddr)[0],
                       ((UINT1 *) &u4IpAddr)[1],
                       ((UINT1 *) &u4IpAddr)[2], ((UINT1 *) &u4IpAddr)[3]);
        }
        else if (NhXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "%s next-hop ipv6 %s\r\n",
                       pu1Prefix, Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                NhXAddr.au1Addr));
        }
    }
    /***********************************************************/
    if (i4Interface != 0)
    {
        CfaCliConfGetIfName ((UINT4) i4Interface, (INT1 *) &au1Buf[0]);
        CliPrintf (CliHandle, "%s interface %s\r\n", pu1Prefix, au1Buf);
    }
    if (i4Metric != 0)
    {
        CliPrintf (CliHandle, "%s metric %d\r\n", pu1Prefix, i4Metric);
    }
    if (u4Tag != 0)
    {
        CliPrintf (CliHandle, "%s tag %d\r\n", pu1Prefix, u4Tag);
    }
    if (i4MetricType != 0)
    {
        CliPrintf (CliHandle, "%s metric-type %s\r\n",
                   pu1Prefix, RMAP_CLI_SHOW_METRIC_TYPE (i4MetricType));
    }
    if (i4RouteType != 0)
    {
        CliPrintf (CliHandle, "%s route-type %s\r\n",
                   pu1Prefix, RMAP_CLI_SHOW_ROUTE_TYPE (i4RouteType));
    }
    /***********************************************************/
    if (u4ASPathTag != 0)
    {
        CliPrintf (CliHandle, "%s as-path tag %d\r\n", pu1Prefix, u4ASPathTag);
    }
    if (u4Community != 0)
    {
        if (((u4Community >= RMAP_COMM_NO_EXPORT) &&
             (u4Community <= RMAP_COMM_LOCALAS)) ||
            (u4Community == RMAP_COMM_NONE)
            || (u4Community == RMAP_COMM_INTERNET))
        {
            if (i4Additive == RMAP_BGP_COMMUNITY_ADDITIVE)
            {
                CliPrintf (CliHandle, "%s community %s additive\r\n",
                           pu1Prefix, RMAP_CLI_SHOW_COMMUNITY (u4Community));

            }
            else
            {
                CliPrintf (CliHandle, "%s community %s\r\n",
                           pu1Prefix, RMAP_CLI_SHOW_COMMUNITY (u4Community));
            }
        }
        else if (u4Community != 0)
        {
            if (i4Additive == RMAP_BGP_COMMUNITY_ADDITIVE)
            {
                CliPrintf (CliHandle, "%s community comm-num %u additive\r\n",
                           pu1Prefix, u4Community);
            }
            else
            {
                CliPrintf (CliHandle, "%s community comm-num %u\r\n",
                           pu1Prefix, u4Community);
            }
        }
    }

    /*populate the community values which is in OCTECT STRING
     *                       into the community values or pRMapNode*/
    RMAPCOMM_OCT_2_ASCIIZ (au1RMapCommunityList ,pRetValFsRMapSetCommunity);

    pAsNoinpToken = STRTOK (au1RMapCommunityList, ptoken);
    
    while (pAsNoinpToken != NULL)
    {
        if((STRCMP(pAsNoinpToken,"internet")==0) || (STRCMP(pAsNoinpToken,"local-as")==0)
           || (STRCMP(pAsNoinpToken,"no-advt")==0) || (STRCMP(pAsNoinpToken,"no-export") ==0)
           || (STRCMP(pAsNoinpToken,"none")==0))
        {
             if ( bFlag == TRUE)
             {
                 CliPrintf(CliHandle,"\" ");
                 bFlag = FALSE;
                 if (i4Additive == RMAP_BGP_COMMUNITY_ADDITIVE)
                  {
                 CliPrintf (CliHandle, " additive\r\n");
                  }
                  else {
                   CliPrintf (CliHandle, "\r\n");
                  }

             }
            CliPrintf (CliHandle, "%s community",pu1Prefix);
            CliPrintf (CliHandle, " %s\n", pAsNoinpToken);
        }
        else 
        {
        if(bFlag == FALSE)
        {
           CliPrintf (CliHandle, "%s community",pu1Prefix);
           CliPrintf(CliHandle," comm-num \"");
           CliPrintf (CliHandle, " %s",
                 pAsNoinpToken);
           bFlag = TRUE;
         }
         else
            {
                CliPrintf (CliHandle, " %s",
                pAsNoinpToken);
            }
        }

        pAsNoinpToken = STRTOK (NULL, ptoken);
    }
      
        if ( bFlag == TRUE)
        {
            CliPrintf(CliHandle,"\"");
            if (i4Additive == RMAP_BGP_COMMUNITY_ADDITIVE)
            {
                CliPrintf (CliHandle, " additive\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        else
        {
           CliPrintf (CliHandle,"\r\n");
        }

    if (i4LocalPref != 0)
    {
        CliPrintf (CliHandle, "%s local-preference %d\r\n",
                   pu1Prefix, i4LocalPref);
    }
    if (i4Origin != 0)
    {
        CliPrintf (CliHandle, "%s origin %s\r\n",
                   pu1Prefix, RMAP_CLI_SHOW_ORIGIN (i4Origin));
    }
    /***********************************************************/
    if (u2Weight != 0)
    {
        CliPrintf (CliHandle, "%s weight %d\r\n", pu1Prefix, u2Weight);
    }
    if (u1AutoTagEna == 1)
    {
        CliPrintf (CliHandle, "%s auto-tag %s\r\n",
                   pu1Prefix, RMAP_CLI_SHOW_AUTO_TAG (u1AutoTagEna));
    }
    if (u1Level != 0)
    {
        CliPrintf (CliHandle, "%s level %s\r\n",
                   pu1Prefix, RMAP_CLI_SHOW_LEVEL (u1Level));
    }
    if (u2ExtCommId != 0)
    {
        CliPrintf (CliHandle, "%s extcommunity cost %d %d \r\n",
                   pu1Prefix, u2ExtCommId, u4ExtCommCost);
    }
}

/***************************************************************************/
/*   Function name : RMapCliCreateIpPrefixList                             */
/*                                                                         */
/*   description   : This function is to create a new entry for the        */
/*                   IP Prefix list                                        */
/*                                                                         */
/*   input(s)      : CliHandle - cli context id                            */
/*                   pIpPrefixNameOct - Ip Prefix entry name               */
/*                   u4SeqNum   - Sequence number for the new entry        */
/*                   pIpPrefixInfo - Pointer containing IP Prefix          */
/*                                   informations                          */
/*                   u4PrefixLen - Prefix length                           */
/*                   u1Access    - PERMIT/DENY                             */
/*  Returns        : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliCreateIpPrefixList (tCliHandle CliHandle,
                           tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,
                           UINT4 u4SeqNum, tIpPrefixInfo * pIpPrefixInfo,
                           UINT4 u4PrefixLen, UINT1 u1Access)
{
    tSNMP_OCTET_STRING_TYPE InetAddrOct;
    tRMapIpPrefix       IpPrefixNode;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4ErrCode = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));
    RMAP_INIT_OCT (InetAddrOct, au1InetAddrBuf);

    if (pIpPrefixInfo == NULL)
    {
        return CLI_FAILURE;
    }

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pIpPrefixNameOct);
    pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                               (tRBElem *) & IpPrefixNode);
    if (pIpPrefixNode != NULL)
    {
        if (pIpPrefixNode->u1IsIpPrefixList != TRUE)
        {
            CLI_SET_ERR (RMAP_CLI_RMAP_EXIST_WITH_GIVEN_NAME);
            return CLI_FAILURE;
        }
    }

    if (RMapUtilIsIpPrefixExist (pIpPrefixNameOct, pIpPrefixInfo, u1Access) !=
        0)
    {
        CLI_SET_ERR (RMAP_CLI_IP_PREFIX_ENTRY_EXIST);
        return CLI_FAILURE;
    }

    if (u4SeqNum == 0)
    {
        /* If no previous entry is present, create the first entry with seq no 5, 
         * if sequence no is not specified*/
        if (pIpPrefixNode == NULL)
        {
            u4SeqNum = 5;
        }
        else
        {
            if ((pIpPrefixNode->u4LastUsedSeq + 5) >
                ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP)
            {
                CLI_SET_ERR (RMAP_CLI_MAX_GEN_SEQ_NUM_EXCEED);
                return CLI_FAILURE;
            }
            u4SeqNum = (pIpPrefixNode->u4LastUsedSeq + 5);
        }
    }
    else if (u4SeqNum > ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP)
    {
        CLI_SET_ERR (RMAP_CLI_MAX_SEQ_NUM_EXCEED);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsRMapRowStatus (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                  RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_SET_ERR (RMAP_CLI_IP_PREFIX_WITH_SAME_SEQ_NUM);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsRMapIsIpPrefixList (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                       TRUE) == SNMP_FAILURE)
    {
        i1RetVal =
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapIsIpPrefixList (pIpPrefixNameOct, u4SeqNum,
                                    TRUE) == SNMP_FAILURE)
    {
        i1RetVal =
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsRMapAccess (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                               (INT4) u1Access) == SNMP_FAILURE)
    {
        i1RetVal =
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapAccess (pIpPrefixNameOct, u4SeqNum,
                            (INT4) u1Access) == SNMP_FAILURE)
    {
        i1RetVal =
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    RMAP_BUF_2_OCT (&InetAddrOct, pIpPrefixInfo->AddrInfo.InetXAddr.au1Addr,
                    pIpPrefixInfo->AddrInfo.InetXAddr.u1AddrLen);

    if (nmhTestv2FsRMapMatchRowStatus (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                       pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                       &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0,
                                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                                       RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        i1RetVal =
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    RMAP_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (pIpPrefixInfo->u1MinPrefixLen != 0)
    {
        if (nmhTestv2FsRMapMatchDestMinPrefixLen
            (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
             pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi, &InetAddrOct, u4PrefixLen,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             pIpPrefixInfo->u1MinPrefixLen) == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                        pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                        &InetAddrOct, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsRMapMatchDestMinPrefixLen (pIpPrefixNameOct, u4SeqNum,
                                               pIpPrefixInfo->AddrInfo.
                                               InetXAddr.u1Afi, &InetAddrOct,
                                               u4PrefixLen, 0, 0, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0,
                                               pIpPrefixInfo->u1MinPrefixLen) ==
            SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                        pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                        &InetAddrOct, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pIpPrefixInfo->u1MaxPrefixLen != 0)
    {
        if (nmhTestv2FsRMapMatchDestMaxPrefixLen
            (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
             pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi, &InetAddrOct, u4PrefixLen,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             pIpPrefixInfo->u1MaxPrefixLen) == SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                        pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                        &InetAddrOct, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsRMapMatchDestMaxPrefixLen (pIpPrefixNameOct, u4SeqNum,
                                               pIpPrefixInfo->AddrInfo.
                                               InetXAddr.u1Afi, &InetAddrOct,
                                               u4PrefixLen, 0, 0, 0, 0, 0, 0, 0,
                                               0, 0, 0, 0, 0, 0, 0,
                                               pIpPrefixInfo->u1MaxPrefixLen) ==
            SNMP_FAILURE)
        {
            nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                        pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                        &InetAddrOct, u4PrefixLen, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        RMAP_DESTROY);
            nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRMapMatchRowStatus (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                       pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                       &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0,
                                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                                       RMAP_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, RMAP_DESTROY);
        nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0,
                                    RMAP_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, RMAP_DESTROY);
        nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsRMapRowStatus (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                  RMAP_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, RMAP_DESTROY);
        nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                    pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi,
                                    &InetAddrOct, u4PrefixLen, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, RMAP_DESTROY);
        nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, RMAP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (i1RetVal);

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function name : RMapCliDelIpPrefixList                                */
/*                                                                         */
/*   description   : This function is to delete the IP Prefix entry        */
/*                                                                         */
/*   input(s)      : CliHandle - cli context id                            */
/*                   pIpPrefixNameOct - Ip Prefix entry name               */
/*                   u4AddrType - address type                             */
/*                   u4SeqNum   - Sequence number for the new entry        */
/*                   pIpPrefixInfo - Pointer containing IP Prefix          */
/*                                   informations                          */
/*                   u4PrefixLen - Prefix length                           */
/*                   u1Access    - PERMIT/DENY                             */
/*  Returns        : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliDelIpPrefixList (tCliHandle CliHandle,
                        tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,
                        UINT4 u4SeqNum, UINT4 u4AddrType,
                        tIpPrefixInfo * pIpPrefixInfo, UINT4 u4PrefixLen,
                        UINT1 u1Access)
{
    tSNMP_OCTET_STRING_TYPE InetAddrOct;
    UINT1               au1InetAddrBuf[IPVX_MAX_INET_ADDR_LEN];
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT1               au1Name[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4ErrCode = 0;
    UINT4               u4TmpSeqNo = 0;
    UINT4               u4Len = 0;
    UINT1               u1Afi = 0;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));
    RMAP_INIT_OCT (InetAddrOct, au1InetAddrBuf);
    RMAP_OCT_2_ASCIIZ (au1Name, pIpPrefixNameOct);

    do
    {
        /* All the IP Prefix entries from the given IP Prefix List needs to be deleted
         * if pIpPrefixInfo is NULL*/
        if (pIpPrefixInfo == NULL)
        {
            pRMapNode = RMapGetNextRouteMapNode (au1Name, u4TmpSeqNo);
            if (pRMapNode == NULL)
            {
                return CLI_SUCCESS;
            }
            if (MEMCMP (pRMapNode->au1RMapName, au1Name, STRLEN (au1Name)) != 0)
            {
                return CLI_SUCCESS;
            }
            u4TmpSeqNo++;
            if (pRMapNode->u1IsIpPrefixInfo != TRUE)
            {
                continue;
            }
            pRMapMatchNode =
                pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;

            if (pRMapMatchNode == NULL)
            {
                continue;
            }
            if (pRMapMatchNode->IpPrefixAddrType != u4AddrType)
            {
                continue;
            }
            RMAP_BUF_2_OCT (&InetAddrOct, pRMapMatchNode->IpPrefixAddr,
                            pRMapMatchNode->IpPrefixAddrLen);
            u1Afi = pRMapMatchNode->IpPrefixAddrType;
            u4PrefixLen = pRMapMatchNode->IpPrefixLength;
            u4SeqNum = pRMapNode->u4SeqNo;
        }
        /* If only one IP Prefix entry from the given IP Prefix List needs to be deleted */
        else
        {
            RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pIpPrefixNameOct);
            pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                       (tRBElem *) & IpPrefixNode);
            if (pIpPrefixNode != NULL)
            {
                if (pIpPrefixNode->u1IsIpPrefixList != TRUE)
                {
                    CLI_SET_ERR (RMAP_CLI_IP_PREFIX_ENTRY_NOT_EXIST);
                    return CLI_FAILURE;
                }
            }

            if (u4SeqNum == 0)
            {
                /* Check whether the Ip prefix entry exists */
                if ((u4SeqNum =
                     RMapUtilIsIpPrefixExist (pIpPrefixNameOct, pIpPrefixInfo,
                                              u1Access)) == 0)
                {
                    CLI_SET_ERR (RMAP_CLI_IP_PREFIX_ENTRY_NOT_EXIST);
                    return CLI_FAILURE;
                }
            }

            pRMapNode = RMapGetRouteMapNode (au1Name, u4SeqNum);

            if (pRMapNode == NULL)
            {
                return CLI_FAILURE;
            }

            if (pRMapNode->u1IsIpPrefixInfo != TRUE)
            {
                return CLI_FAILURE;
            }
            if (pRMapNode->u1Access != u1Access)
            {
                return CLI_FAILURE;
            }
            RMAP_BUF_2_OCT (&InetAddrOct,
                            pIpPrefixInfo->AddrInfo.InetXAddr.au1Addr,
                            pIpPrefixInfo->AddrInfo.InetXAddr.u1AddrLen);
            u1Afi = pIpPrefixInfo->AddrInfo.InetXAddr.u1Afi;

            if (nmhGetFsRMapMatchDestMinPrefixLen
                (pIpPrefixNameOct, u4SeqNum, u1Afi, &InetAddrOct, u4PrefixLen,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 &u4Len) == SNMP_SUCCESS)
            {
                if (u4Len != pIpPrefixInfo->u1MinPrefixLen)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsRMapMatchDestMaxPrefixLen
                (pIpPrefixNameOct, u4SeqNum, u1Afi, &InetAddrOct, u4PrefixLen,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                 &u4Len) == SNMP_SUCCESS)
            {
                if (u4Len != pIpPrefixInfo->u1MaxPrefixLen)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FsRMapMatchRowStatus
            (&u4ErrCode, pIpPrefixNameOct, u4SeqNum, u1Afi, &InetAddrOct,
             u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsRMapMatchRowStatus (pIpPrefixNameOct, u4SeqNum,
                                        u1Afi, &InetAddrOct,
                                        u4PrefixLen, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsRMapRowStatus (&u4ErrCode, pIpPrefixNameOct, u4SeqNum,
                                      DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsRMapRowStatus (pIpPrefixNameOct, u4SeqNum, DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    while (1);
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function name : RMapCliShowIpPrefixListName                           */
/*                                                                         */
/*   description   : This function is to display all the IP Prefix entries */
/*                   present in the given IP Prefix list                   */
/*                                                                         */
/*   input(s)      : CliHandle - cli context id                            */
/*                   pIpPrefixNameOct - Ip Prefix list name                */
/*                   u1AddrType - address type                             */
/*                   u1Flag - Flag to decide whether to print all Prefix   */
/*                            entries or specific                          */
/*  Returns        : CLI_SUCCESS/CLI_FAILURE                               */
/***************************************************************************/
INT4
RMapCliShowIpPrefixListName (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,
                             UINT1 u1AddrType, UINT1 u1DisplayAll,
                             tRMapXAddrInfo * pIpAddrInfo, UINT1 u1Flag)
{
    tSNMP_OCTET_STRING_TYPE TempIpPrefixOct;
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;
    UINT1               au1IpPrefixNameBuf[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1TmpIpPrefix[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4SeqNo = 0;
    INT4                i4IsPrefixList = FALSE;
    UINT1               u1DisplayName = 0;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));
    MEMSET (au1TmpIpPrefix, 0, sizeof (au1TmpIpPrefix));
    RMAP_INIT_OCT (TempIpPrefixOct, au1IpPrefixNameBuf);

    if (u1DisplayAll == RMAP_SHOW_ALL_MAPS)
    {
        /* get 1st valid name and seqnum */
        if (nmhGetFirstIndexFsRMapTable (&TempIpPrefixOct, &u4SeqNo)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (pIpPrefixNameOct == NULL)
        {
            return CLI_FAILURE;
        }

        RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pIpPrefixNameOct);
        pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                                   (tRBElem *) & IpPrefixNode);
        if (pIpPrefixNode != NULL)
        {
            if (pIpPrefixNode->u1IsIpPrefixList != TRUE)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhGetNextIndexFsRMapTable
            (pIpPrefixNameOct, &TempIpPrefixOct, u4SeqNo,
             &u4SeqNo) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (RMAP_CMP_OCT (pIpPrefixNameOct, &TempIpPrefixOct) != 0)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        if (nmhGetFsRMapIsIpPrefixList
            (&TempIpPrefixOct, u4SeqNo, &i4IsPrefixList) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4IsPrefixList != TRUE)
        {
            continue;
        }
        if (u1DisplayAll != RMAP_SHOW_ALL_MAPS)
        {
            if (RMAP_CMP_OCT (pIpPrefixNameOct, &TempIpPrefixOct) != 0)
            {
                /* no more next MAP node for this map name */
                break;
            }
        }

        if (MEMCMP (au1TmpIpPrefix, TempIpPrefixOct.pu1_OctetList,
                    TempIpPrefixOct.i4_Length) != 0)
        {
            u1DisplayName = TRUE;
            MEMCPY (au1TmpIpPrefix, TempIpPrefixOct.pu1_OctetList,
                    TempIpPrefixOct.i4_Length);
        }

        RMapCliShowMatchEntries (CliHandle, (UINT1 *) "", &TempIpPrefixOct,
                                 u4SeqNo, i4IsPrefixList, u1AddrType, TRUE,
                                 pIpAddrInfo, u1Flag, &u1DisplayName);

    }
    while (nmhGetNextIndexFsRMapTable (&TempIpPrefixOct, &TempIpPrefixOct,
                                       u4SeqNo, &u4SeqNo) != SNMP_FAILURE);

    return CLI_SUCCESS;
}
