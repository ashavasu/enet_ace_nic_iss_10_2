 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmapapi.c,v 1.30 2017/12/26 13:34:30 siva Exp $
 *
 * Description:This file contains the APIs provided by the RouteMap
 * Module for other modules to interact with the RouteMap module.
 *
 *******************************************************************/

#include "rmapinc.h"
#include "cli.h"
#include "rmapcli.h"

extern UINT4        gu4RMapGlobalDbg;
extern tRMapGlobalInfo gRMapGlobalInfo;

/*****************************************************************************/
/* Function     : RMapApplyPrefixRule                                        */
/*                                                                           */
/* Description  : This function applies route map on the given               */
/*                route. If the route map MATCH not exists returns           */
/*                RMAP_ROUTE_DENY. If the route map MATCH entry exists,      */
/*                checks for the access state. If the access state is DENY   */
/*                this function returns RMAP_ROUTE_DENY. If the route map    */
/*                entry exits and the access state is PERMIT, this function  */
/*                checks for the SET rule and applies the rule to            */
/*                pOutputRtInfo                                              */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - Output Route Information                      */
/*                                                                           */
/* Returns      : RMAP_ROUTE_PERMIT -if all existing MATCH conditions are met*/
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_PERMIT -if no MATCH conditions exist            */
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_DENY   -if all existing MATCH conditions are met*/
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if no MATCH conditions exist            */
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if not all existing MATCH conditions    */
/*                                   are met (default result)                */
/*****************************************************************************/
UINT1
RMapApplyPrefixRule (tRtMapInfo * pInputRtInfo, tRtMapInfo * pOutputRtInfo,
                     UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 0;
    UINT1               u1MatchFlag = 0;
    UINT1               u1PermitFlag = RMAP_ROUTE_DENY;
    INT4                i4IsPrefixExist = 0;
    MEMSET (pOutputRtInfo, 0, sizeof (tRtMapInfo));
    if ((pInputRtInfo == NULL) || (pu1RMapName == NULL))
        return RMAP_ENTRY_NOT_MATCHED;

    MEMCPY (pOutputRtInfo, pInputRtInfo, sizeof (tRtMapInfo));
    pOutputRtInfo->u1CommunityCnt = 0;
    RMapLockRead ();

    /* Get the First instance of pu1RMapName */
    pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);
    if (pRMapNode == NULL)
    {
        /* When the route map gets deleted, i.e no sequence, the routes
         * should be permitted */
        u1PermitFlag = RMAP_ROUTE_PERMIT;
    }
    else
    {
        if (pRMapNode->u1IsIpPrefixInfo == 1)
        {
            i4IsPrefixExist = 1;
        }
    }

    /* Scan all the sequence numbers for this routemap and apply the Matchrule
     * If the MatchRule is matched, apply SetRule, Send Trap */

    for (u4SeqNo = 1; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* Search for Route map Entry */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            if (pRMapNode->u1IsIpPrefixInfo == 1)
            {
                i4IsPrefixExist = 1;
                u1MatchFlag =
                    RMapApplyMatchIpPrefixRule (pInputRtInfo, pRMapNode);
                if (u1MatchFlag == PERMIT_PREFIX)
                {
                    u1PermitFlag = RMAP_ROUTE_PERMIT;
                    RMapUnLockRead ();
                    return u1PermitFlag;
                }
                else if (u1MatchFlag == DENY_PREFIX)
                {
                    u1PermitFlag = RMAP_ROUTE_DENY;
                    RMapUnLockRead ();
                    return u1PermitFlag;
                }
                else
                {
                    continue;
                }
            }
        }
    }
    if (i4IsPrefixExist != 0)
    {
        u1PermitFlag = RMAP_ROUTE_DENY;    /* Entry not matched and Permit filter */
    }

    RMapUnLockRead ();
    return u1PermitFlag;
}

/*****************************************************************************/
/* Function     : RMapApplyRule                                              */
/*                                                                           */
/* Description  : This function applies route map on the given               */
/*                route. If the route map MATCH not exists returns           */
/*                RMAP_ROUTE_DENY. If the route map MATCH entry exists,      */
/*                checks for the access state. If the access state is DENY   */
/*                this function returns RMAP_ROUTE_DENY. If the route map    */
/*                entry exits and the access state is PERMIT, this function  */
/*                checks for the SET rule and applies the rule to            */
/*                pOutputRtInfo                                              */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - Output Route Information                      */
/*                                                                           */
/* Returns      : RMAP_ROUTE_PERMIT -if all existing MATCH conditions are met*/
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_PERMIT -if no MATCH conditions exist            */
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_DENY   -if all existing MATCH conditions are met*/
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if no MATCH conditions exist            */
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if not all existing MATCH conditions    */
/*                                   are met (default result)                */
/*****************************************************************************/
UINT1
RMapApplyRule (tRtMapInfo * pInputRtInfo, tRtMapInfo * pOutputRtInfo,
               UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 0;
    UINT1               u1MatchFlag = RMAP_ENTRY_NOT_MATCHED;
    UINT1               u1PermitFlag = RMAP_ROUTE_DENY;
    INT4                i4PermitCnt = 0;
    INT4                i4DenyCnt = 0;
    INT4                i4MatchCnt = 0;

    MEMSET (pOutputRtInfo, 0, sizeof (tRtMapInfo));
    if ((pInputRtInfo == NULL) || (pu1RMapName == NULL))
        return RMAP_ENTRY_NOT_MATCHED;

    MEMCPY (pOutputRtInfo, pInputRtInfo, sizeof (tRtMapInfo));
    pOutputRtInfo->u1CommunityCnt = 0;
    RMapLockRead ();

    /* Get the First instance of pu1RMapName */
    pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);
    if (pRMapNode == NULL)
    {
        /* When the route map gets deleted, i.e no sequence, the routes
         * should be permitted */
        u1PermitFlag = RMAP_ROUTE_PERMIT;
    }

    /* Scan all the sequence numbers for this routemap and apply the Matchrule
     * If the MatchRule is matched, apply SetRule, Send Trap */

    for (u4SeqNo = 1; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* Search for Route map Entry */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            /* Search for Route map MATCH Entry */
            u1MatchFlag = RMapApplyMatchRule (pInputRtInfo, pRMapNode);

            if (u1MatchFlag == RMAP_AFI_NOT_MATCHED)
            {
                u1MatchFlag = PERMIT_PREFIX;
            }

            if (u1MatchFlag == RMAP_ROUTE_PERMIT)
            {
                i4MatchCnt = 1;
                u1PermitFlag = RMAP_ROUTE_PERMIT;
                break;
            }
            else if ((u1MatchFlag == RMAP_ENTRY_MATCHED) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_PERMIT))
            {
                i4MatchCnt = 1;
                u1PermitFlag = RMAP_ROUTE_PERMIT;
                break;
            }
            else if ((u1MatchFlag == RMAP_ENTRY_MATCHED) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_DENY))
            {
                i4MatchCnt = 1;
                u1PermitFlag = RMAP_ROUTE_DENY;
                break;
            }
            else if ((u1MatchFlag == RMAP_ENTRY_NOT_MATCHED) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_DENY))
            {
                i4DenyCnt = 1;
                u1PermitFlag = RMAP_ROUTE_PERMIT;
            }
            else if ((u1MatchFlag == RMAP_ENTRY_NOT_MATCHED) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_PERMIT))
            {
                i4PermitCnt = 1;
                u1PermitFlag = RMAP_ROUTE_DENY;
            }
            else if ((u1MatchFlag == PERMIT_PREFIX) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_PERMIT))
            {
                i4MatchCnt = 1;
                u1PermitFlag = RMAP_ROUTE_PERMIT;
                break;
            }
            else if ((u1MatchFlag == DENY_PREFIX) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_PERMIT))
            {
                u1PermitFlag = RMAP_ROUTE_DENY;
            }
            else if ((u1MatchFlag == PERMIT_PREFIX) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_DENY))
            {
                u1PermitFlag = RMAP_ROUTE_DENY;
            }
            else if ((u1MatchFlag == DENY_PREFIX) &&
                     (pRMapNode->u1Access == RMAP_ROUTE_DENY))
            {
                u1PermitFlag = RMAP_ROUTE_DENY;
            }
        }
    }
    if (i4MatchCnt == 0)
    {
        if ((i4PermitCnt == 1) && (i4DenyCnt == 0))
        {
            u1PermitFlag = RMAP_ROUTE_DENY;    /* Entry not matched and Permit filter */
        }
        if ((i4PermitCnt == 0) && (i4DenyCnt == 1))
        {
            u1PermitFlag = RMAP_ROUTE_PERMIT;    /*Entry not matched and Deny filter */
        }
        if ((i4PermitCnt == 1) && (i4DenyCnt == 1))
        {
            u1PermitFlag = RMAP_ROUTE_PERMIT;    /*Entry not matched and Permit 
                                                   and Deny filter combination */
        }
    }
    /* MATCHED could be only for MAP node which is ACTIVE and !NULL */
    if (pRMapNode != NULL)
    {
        if (((u1MatchFlag == RMAP_ENTRY_MATCHED)
             || (u1MatchFlag == PERMIT_PREFIX))
            && (pRMapNode->u1Access == RMAP_ROUTE_PERMIT))
        {
            /* Found the Route Map MATCH entry for the route, Check for the
               SET Rule, if exists apply the rule  */
            RMapApplySetRule (pOutputRtInfo, pRMapNode);
            pOutputRtInfo->i4RMapFlag = (INT4) u4SeqNo;
        }
    }
    RMapUnLockRead ();

#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)
    if (u1MatchFlag == RMAP_ENTRY_MATCHED)
    {
        if (gRMapGlobalInfo.u4RMapTrapEnabled == RMAP_TRAP_ENABLED)
        {
            RMapSendMatchTrap (pu1RMapName, (UINT1) u4SeqNo);
        }
    }
#endif

    return u1PermitFlag;
}

/*****************************************************************************/
/* Function     : RMapApplyRule4                                             */
/*                                                                           */
/* Description  : This function applies only MATCH rules on the given        */
/*                route.                                                     */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - none                                          */
/*                                                                           */
/* Returns      : RMAP_ENTRY_MATCHED-if all existing MATCH conditions are met */
/*              : RMAP_ENTRY_MATCHED-if no MATCH conditions exist             */
/*              : RMAP_ENTRY_MISMATCHED-if not all existing MATCH conditions  */
/*                                      are met                               */
/*              : RMAP_ENTRY_NOT_MATCHED-all other cases                      */
/*                                                                            */
/* Warning      : RMAP_ENTRY_MATCHED-only this result should be used          */
/*                                   in comparison                            */
/*****************************************************************************/
UINT1
RMapApplyRule4 (tRtMapInfo * pInputRtInfo, UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 1;
    UINT1               u1MatchFlag = RMAP_ENTRY_NOT_MATCHED;

    if ((pInputRtInfo == NULL) || (pu1RMapName == NULL))
        return RMAP_ENTRY_NOT_MATCHED;

    RMapLockRead ();
    for (; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* Search for Route map Entry */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            pInputRtInfo->u1RmapAccess = pRMapNode->u1Access;
            /* Search for Route map MATCH Entry */
            u1MatchFlag = RMapApplyMatchRule (pInputRtInfo, pRMapNode);
            if (u1MatchFlag == RMAP_ENTRY_MATCHED
                || u1MatchFlag == RMAP_AFI_NOT_MATCHED
                || u1MatchFlag == PERMIT_PREFIX)
            {
                break;
            }
        }
    }
    RMapUnLockRead ();

#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)
    if (u1MatchFlag == RMAP_ENTRY_MATCHED)
    {
        if (gRMapGlobalInfo.u4RMapTrapEnabled == RMAP_TRAP_ENABLED)
        {
            RMapSendMatchTrap (pu1RMapName, (UINT1) u4SeqNo);
        }
    }

#endif
    return u1MatchFlag;
}

/*****************************************************************************/
/* Function     : RMapApplyRule2                                             */
/*                                                                           */
/* Description  : This function applies only MATCH rules on the given        */
/*                route.                                                     */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - none                                          */
/*                                                                           */
/* Returns      : RMAP_ENTRY_MATCHED-if all existing MATCH conditions are met */
/*              : RMAP_ENTRY_MATCHED-if no MATCH conditions exist             */
/*              : RMAP_ENTRY_MISMATCHED-if not all existing MATCH conditions  */
/*                                      are met                               */
/*              : RMAP_ENTRY_NOT_MATCHED-all other cases                      */
/*                                                                            */
/* Warning      : RMAP_ENTRY_MATCHED-only this result should be used          */
/*                                   in comparison                            */
/*****************************************************************************/
UINT1
RMapApplyRule2 (tRtMapInfo * pInputRtInfo, UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 1;
    UINT1               u1MatchFlag = RMAP_ENTRY_NOT_MATCHED;

    if ((pInputRtInfo == NULL) || (pu1RMapName == NULL))
        return RMAP_ENTRY_NOT_MATCHED;

    RMapLockRead ();
    for (; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* Search for Route map Entry */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if ((pRMapNode != NULL) && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
        {
            pInputRtInfo->u1RmapAccess = pRMapNode->u1Access;
            /* Search for Route map MATCH Entry */
            u1MatchFlag = RMapApplyMatchRule (pInputRtInfo, pRMapNode);
            if (u1MatchFlag == RMAP_ENTRY_MATCHED
                || u1MatchFlag == RMAP_AFI_NOT_MATCHED
                || u1MatchFlag == PERMIT_PREFIX)
            {
                break;
            }
        }
    }
    RMapUnLockRead ();

    if (u1MatchFlag == RMAP_AFI_NOT_MATCHED)
    {
        u1MatchFlag = PERMIT_PREFIX;
    }
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)
    if (u1MatchFlag == RMAP_ENTRY_MATCHED)
    {
        if (gRMapGlobalInfo.u4RMapTrapEnabled == RMAP_TRAP_ENABLED)
        {
            RMapSendMatchTrap (pu1RMapName, (UINT1) u4SeqNo);
        }
    }
#endif

    return u1MatchFlag;
}

/*****************************************************************************/
/* Function     : RMapApplyMatchRule                                         */
/*                                                                           */
/* Description  : This function applies match rules of particular            */
/*                map node on the given route                                */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pRMapNode - MAP node                                       */
/*                                                                           */
/* Returns      : RMAP_ENTRY_MATCHED - MATCH conditions are met              */
/*              : RMAP_ENTRY_NOT_MATCHED - else                              */
/*                                                                           */
/*****************************************************************************/
UINT1
RMapApplyMatchRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode)
{
/*     NOTE
 * if there are only not active match nodes or there are not match nodes at all
 *  return RMAP_ENTRY_MATCHED
 */
    tRMapMatchNode     *pRMapMatchNode = NULL;
    tAsPath            *pSrcAspath = NULL;
    UINT4               u4IfIndex = RMAP_ZERO;
    UINT1               u1MatchFlag = RMAP_ENTRY_MATCHED;
    tMatchList         *pMatchList = NULL;

    INT4                i4Index;
    INT1                i1MatchFound = -1;
    UINT1              *pu1AsNos = NULL;
    UINT4               u4AsPath = 0;
    UINT1               u1AsCnt = 0;
    UINT1               u1Count = 0;

    for (i4Index = 0; i4Index < MAX_MATCH_RECORDS; i4Index = i4Index + 1)
    {
        pMatchList = &(pRMapNode->MatchCmdArray[i4Index]);
        /*********  i1MatchFound value description: ***********
         *   -1 - no match rules for this command, so we assume this criteria as matched
         *    0 - there are match rules for this command but no matches occurs
         *    1 - there is match for this command
         */

        while (pMatchList != NULL && pMatchList->pNode != NULL)
        {
            pRMapMatchNode = pMatchList->pNode;
            if (pRMapMatchNode->u1RowStatus == RMAP_ACTIVE)    /* skip not active nodes */
            {
                i1MatchFound = 0;
                switch (pRMapMatchNode->u1Cmd)
                {
                    case RMAP_MATCH_CMD_DEST_IPX:
                        if (0 ==
                            CmpIpvxAddrWithPrefix (&(pRMapMatchNode->RMapDestX),
                                                   pRMapMatchNode->
                                                   RMapDestPrefixLen,
                                                   &(pInputRtInfo->DstXAddr),
                                                   pInputRtInfo->
                                                   u2DstPrefixLen))
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_SOURCE_IPX:
                        /* route src ip has no prefix/netmask, use one of MATCH node */
                        if (0 ==
                            CmpIpvxAddrWithPrefix (&
                                                   (pRMapMatchNode->
                                                    RMapSourceX),
                                                   pRMapMatchNode->
                                                   RMapSourcePrefixLen,
                                                   &(pInputRtInfo->SrcXAddr),
                                                   pRMapMatchNode->
                                                   RMapSourcePrefixLen))
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_NEXTHOP_IPX:
                        if (0 == CmpIpvxAddr (&(pRMapMatchNode->RMapNextHopX),
                                              &(pInputRtInfo->NextHopXAddr)))
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_INTERFACE:
                        /* IPv4 routes use port number as interface index */
                        if (pInputRtInfo->DstXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
                        {
                            if (NetIpv4GetCfaIfIndexFromPort
                                ((UINT2) pInputRtInfo->u4IfIndex,
                                 &u4IfIndex) != NETIPV4_SUCCESS)
                            {
                                break;
                            }
                        }
                        else if (pInputRtInfo->DstXAddr.u1Afi ==
                                 IPVX_ADDR_FMLY_IPV6)
                        {
                            u4IfIndex = pInputRtInfo->u4IfIndex;
                        }

                        if (u4IfIndex == pRMapMatchNode->RMapIfIndex)
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_METRIC:
                        if (pRMapMatchNode->RMapMetric ==
                            pInputRtInfo->i4Metric)
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_TAG:
                        if ((pInputRtInfo->u2RtProto == LOCAL_ID)
                            || (pInputRtInfo->u2RtProto == STATIC_ID))
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            if (pRMapMatchNode->RMapTag ==
                                pInputRtInfo->u4RouteTag)
                            {
                                i1MatchFound = 1;
                            }
                        }
                        break;

                    case RMAP_MATCH_CMD_METRIC_TYPE:
                        if (pInputRtInfo->u2RtProto != OSPF_ID)
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            if (pRMapMatchNode->RMapMetricType ==
                                pInputRtInfo->u1MetricType)
                            {
                                i1MatchFound = 1;
                            }
                        }
                        break;

                    case RMAP_MATCH_CMD_ROUTE_TYPE:
                        if (pRMapMatchNode->RMapRouteType ==
                            pInputRtInfo->i2RouteType)
                        {
                            i1MatchFound = 1;
                        }
                        break;

                    case RMAP_MATCH_CMD_COMMUNITY:
                        if (pInputRtInfo->u2RtProto != BGP_ID)
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            for (u1Count = 0; u1Count < RMAP_BGP_MAX_COMM;
                                 u1Count++)
                            {
                                if (pRMapMatchNode->RMapCommunity ==
                                    OSIX_NTOHL (pInputRtInfo->
                                                au4Community[u1Count]))
                                {
                                    i1MatchFound = 1;
                                }
                            }
                        }
                        break;

                    case RMAP_MATCH_CMD_LOCAL_PREF:
                        if (pInputRtInfo->u2RtProto != BGP_ID)
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            if (pRMapMatchNode->RMapLocalPref ==
                                pInputRtInfo->u4LocalPref)
                            {
                                i1MatchFound = 1;
                            }
                        }
                        break;

                    case RMAP_MATCH_CMD_ORIGIN:
                        if (pInputRtInfo->u2RtProto != BGP_ID)
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            if (pRMapMatchNode->RMapOrigin ==
                                pInputRtInfo->i4Origin)
                            {
                                i1MatchFound = 1;
                            }
                        }
                        break;
                    case RMAP_MATCH_CMD_ASPATH_TAG:
                        if (pInputRtInfo->u2RtProto != BGP_ID)
                        {
                            i1MatchFound = 2;
                        }
                        else
                        {
                            TMO_SLL_Scan (&(pInputRtInfo->TSASPath),
                                          pSrcAspath, tAsPath *)
                            {
                                u1AsCnt = pSrcAspath->u1Length;
                                pu1AsNos = pSrcAspath->au1ASSegs;
                                while (u1AsCnt > 0)
                                {
                                    u4AsPath =
                                        OSIX_NTOHS (*(UINT2 *) (VOID *)
                                                    pu1AsNos);
                                    if (pRMapMatchNode->RMapNextHopAS ==
                                        u4AsPath)
                                    {
                                        i1MatchFound = 1;
                                        if (pRMapNode->pSetNode != NULL)
                                        {
                                            pRMapNode->pSetNode->u4MatchAsSeg =
                                                pRMapMatchNode->RMapNextHopAS;
                                        }
                                        break;
                                    }
                                    pu1AsNos += 2;
                                    u1AsCnt--;
                                }
                            }
                        }
                        break;

                    default:
                        RMAP_TRC (RMAP_MOD_TRC, ALL_FAILURE_TRC,
                                  RMAP_MODULE_NAME,
                                  "Unknown option in Route Map Match Comparison.\n");
                        break;
                }
            }
            if (i1MatchFound > 0)
            {
                /* Match Command found. Skip to next command list */
                break;
            }
            pMatchList = pMatchList->pNext;
        }

        if (i1MatchFound == 0)    /* there are match rules for current command but no matches occured */
        {
            u1MatchFlag = RMAP_ENTRY_NOT_MATCHED;
            break;
        }
    }

    if (i1MatchFound == 1)
    {
        u1MatchFlag = RMAP_ENTRY_MATCHED;
    }
    else if (i1MatchFound == 2)
    {
        u1MatchFlag = RMAP_ROUTE_PERMIT;
    }

    return u1MatchFlag;
}

/*****************************************************************************/
/* Function     : RMapApplySetRule                                           */
/*                                                                           */
/* Description  : This function applies match rules on the given route       */
/*                                                                           */
/* Input        : pInputRtInfo - route to be modified                        */
/*                                                                           */
/*****************************************************************************/
VOID
RMapApplySetRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode)
{
    UINT4               u4IfIndex = RMAP_ZERO;

    if (pRMapNode->pSetNode != NULL && (pRMapNode->u1RowStatus == RMAP_ACTIVE))
    {
        if (TestZeroIpvxAddr (&(pRMapNode->pSetNode->NextHopX)) != 0)
        {
            IPVX_ADDR_COPY (&(pInputRtInfo->NextHopXAddr),
                            &(pRMapNode->pSetNode->NextHopX));
        }

        if (pRMapNode->pSetNode->u4IfIndex != 0)
        {
            /* IPv4 routes use port number as interface index */
            if (pInputRtInfo->DstXAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                if (NetIpv4GetPortFromIfIndex (pRMapNode->pSetNode->u4IfIndex,
                                               &u4IfIndex) == NETIPV4_SUCCESS)
                {
                    pInputRtInfo->u4IfIndex = u4IfIndex;
                }
            }
            else if (pInputRtInfo->DstXAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                pInputRtInfo->u4IfIndex = pRMapNode->pSetNode->u4IfIndex;
            }
        }
        if (pRMapNode->pSetNode->i4Metric != 0)
        {
            pInputRtInfo->i4Metric = pRMapNode->pSetNode->i4Metric;
            pInputRtInfo->i4RMapFlag = RMAP_ONE;
        }
/*  -- set of metric type is not supported --
        if (pRMapNode->pSetNode->u1MetricType != 0)
        {
            pInputRtInfo->u1MetricType = pRMapNode->pSetNode->u1MetricType;
        }
*/
        if (pRMapNode->pSetNode->i2RouteType != 0)
        {
            pInputRtInfo->i2RouteType = pRMapNode->pSetNode->i2RouteType;
        }
        if (pRMapNode->pSetNode->u4Tag != 0)
        {
            pInputRtInfo->u4RouteTag = pRMapNode->pSetNode->u4Tag;
        }

        if (pRMapNode->pSetNode->u4NextHopAS != 0)
        {
            pInputRtInfo->u4MatchAs = pRMapNode->pSetNode->u4MatchAsSeg;
            pInputRtInfo->u4NextHopAS = pRMapNode->pSetNode->u4NextHopAS;
            pInputRtInfo->u1AsCnt = RMAP_ONE;
        }
        if (pRMapNode->pSetNode->au4Community[0] != 0)
        {
            MEMCPY (pInputRtInfo->au4Community,
                    pRMapNode->pSetNode->au4Community,
                    (RMAP_BGP_MAX_COMM * sizeof (UINT4)));
        }

        if (pRMapNode->pSetNode->u1CommunityCnt != 0)
        {
            pInputRtInfo->u1CommunityCnt = pRMapNode->pSetNode->u1CommunityCnt;
        }

        if (pRMapNode->pSetNode->u1Addflag != 0)
        {
            pInputRtInfo->u1Addflag = pRMapNode->pSetNode->u1Addflag;
        }
        if (pRMapNode->pSetNode->u4LocalPref != 0)
        {
            pInputRtInfo->u4LocalPref = pRMapNode->pSetNode->u4LocalPref;
        }
        if (pRMapNode->pSetNode->i4Origin != 0)
        {
            pInputRtInfo->i4Origin = pRMapNode->pSetNode->i4Origin;
        }

        if (pRMapNode->pSetNode->u2Weight != 0)
        {
            pInputRtInfo->u2Weight = pRMapNode->pSetNode->u2Weight;
        }
        if (pRMapNode->pSetNode->u1AutoTagEna != 0)
        {
            pInputRtInfo->u1AutoTagEna = pRMapNode->pSetNode->u1AutoTagEna;
        }
        if (pRMapNode->pSetNode->u1Level != 0)
        {
            pInputRtInfo->u1Level = pRMapNode->pSetNode->u1Level;
        }
        if (pRMapNode->pSetNode->u2ExtCommId != 0)
        {
            pInputRtInfo->u2ExtCommId = pRMapNode->pSetNode->u2ExtCommId;
            pInputRtInfo->u4ExtCommCost = pRMapNode->pSetNode->u4ExtCommCost;
            pInputRtInfo->u2ExtCommPOI = pRMapNode->pSetNode->u2ExtCommPOI;
        }
    }
}

/*****************************************************************************/
/* Function     : RMapProcessRouteMapUpdate                                  */
/*                                                                           */
/* Description  : This function deletes Route Map Entry if its in            */
/*                DESTROY state, if not deletes Route Map MATCH              */
/*                entry which are in DESTROY and Route Map SET node          */
/*                if its in DESTROYE                                         */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pRMapNode - Route map node contains SET rules              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
RMapProcessRouteMapUpdate (UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode      RMapMatchNode, *pRMapMatchNode, *pRMapNextMatchNode =
        NULL;
    UINT4               u4SeqNo = 1;

    for (; u4SeqNo <= ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP; u4SeqNo++)
    {
        /* Search for Route map Entry */
        pRMapNode = RMapGetRouteMapNode (pu1RMapName, u4SeqNo);

        if (pRMapNode != NULL)
        {
            if (pRMapNode->u1RowStatus == RMAP_DESTROY)
            {
                /* Route Map Entry can be deleted */
                if ((RMapDeleteRouteMapNode (pRMapNode)) == RMAP_FAILURE)
                {
                    RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC,
                                   RMAP_MODULE_NAME,
                                   "Cannot delete the MAP node %s.\n",
                                   pRMapNode->au1RMapName);
                    return RMAP_FAILURE;
                }
            }
            else
            {
                /* Delete the Route Map MATCH and SET Entry which are in
                 * DESTROY state */
                pRMapMatchNode = (tRMapMatchNode *) RBTreeGetFirst
                    (pRMapNode->RMapMatchRoot);
                while (pRMapMatchNode != NULL)
                {
                    MEMSET (&RMapMatchNode, 0, sizeof (tRMapMatchNode));
                    MEMCPY (&RMapMatchNode, pRMapMatchNode,
                            sizeof (tRMapMatchNode));
                    if (pRMapMatchNode->u1RowStatus == RMAP_DESTROY)
                    {
                        if ((RMapDeleteMatchNode (pRMapNode, pRMapMatchNode)) ==
                            RMAP_FAILURE)
                        {
                            RMAP_TRC_ARG1 (RMAP_MOD_TRC, RMAP_MGMT_TRC,
                                           RMAP_MODULE_NAME,
                                           "Cannot delete the MAP node %s.\n",
                                           pRMapNode->au1RMapName);
                            return RMAP_FAILURE;
                        }
                    }

                    pRMapNextMatchNode =
                        RBTreeGetNext (pRMapNode->RMapMatchRoot, &RMapMatchNode,
                                       NULL);
                    pRMapMatchNode = pRMapNextMatchNode;
                }                /* while */

                /* If the SET Node is in DESTROY,
                 * make the corresponding SET Entry to 0 */
                if (pRMapNode->pSetNode != 0)
                {
                    if ((pRMapNode->pSetNode->u1RowStatus == RMAP_DESTROY)
                        || (pRMapNode->pSetNode->u1RowStatus ==
                            RMAP_NOT_IN_SERVICE))
                    {
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_NEXTHOP_IPX))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_NEXTHOP_IPX);
                            IPVX_ADDR_CLEAR (&(pRMapNode->pSetNode->NextHopX));
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_INTERFACE))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_INTERFACE);
                            pRMapNode->pSetNode->u4IfIndex = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_METRIC))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_METRIC);
                            pRMapNode->pSetNode->i4Metric = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_TAG))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_TAG);
                            pRMapNode->pSetNode->u4Tag = 0;
                        }
/*  -- set of metric type is not supported --
                        else if (pRMapNode->pSetNode->u2RemoveMask==RMAP_SET_CMD_METRIC_TYPE)
                        {
                            pRMapNode->pSetNode->u1MetricType = 0;
                        }
*/
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_ROUTE_TYPE))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_ROUTE_TYPE);
                            pRMapNode->pSetNode->i2RouteType = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_ASPATH_TAG))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_ASPATH_TAG);
                            pRMapNode->pSetNode->u4NextHopAS = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_COMMUNITY))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_COMMUNITY);
                            MEMSET ((pRMapNode->pSetNode->au4Community), 0,
                                    (sizeof (UINT4) * RMAP_BGP_MAX_COMM));
                            pRMapNode->pSetNode->u1CommunityCnt = 0;
                            pRMapNode->pSetNode->u1Addflag = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_LOCAL_PREF))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_LOCAL_PREF);
                            pRMapNode->pSetNode->u4LocalPref = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_ORIGIN))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_ORIGIN);
                            pRMapNode->pSetNode->i4Origin = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_WEIGHT))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_WEIGHT);
                            pRMapNode->pSetNode->u2Weight = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_AUTO_TAG_ENA))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_AUTO_TAG_ENA);
                            pRMapNode->pSetNode->u1AutoTagEna = 0;
                        }
                        if (pRMapNode->pSetNode->
                            u4RemoveMask & (1 << RMAP_SET_CMD_LEVEL))
                        {
                            pRMapNode->pSetNode->u4RemoveMask &=
                                ~(1 << RMAP_SET_CMD_LEVEL);
                            pRMapNode->pSetNode->u1Level = 0;
                        }

                        pRMapNode->pSetNode->u1RowStatus = RMAP_ACTIVE;

                        /* If all the SET Node Entry is 0, the SET node
                         * can be deleted */
                        /* Test all significant fields */
                        if (TestZeroArray ((UINT1 *) (pRMapNode->pSetNode),
                                           RMAP_SET_FIELDS_SIZE) == 0)
                        {
                            MemReleaseMemBlock (gRMapGlobalInfo.RMapSetPoolId,
                                                (UINT1 *) (pRMapNode->
                                                           pSetNode));
                            pRMapNode->pSetNode = NULL;
                        }

                    }            /* if(pRMapNode->pSetNode->u1RowStatus ==
                                   RMAP_DESTROY) */
                }                /* (pRMapNode->pSetNode!= 0) */
            }
        }                        /* (pRMapNode != NULL) */
    }                            /* for loop */
    return RMAP_SUCCESS;
}

/*****************************************************************************/
/* Function     : RMapRegister                                               */
/*                                                                           */
/* Description  : This function used to register the applications            */
/*                (RTM, protocol..) with RouteMap module, Route Map module   */
/*                invokes the call back function if any updation in the      */
/*                Route Map Table (map/match/set node changed)               */
/*                                                                           */
/* Input        : u1AppId - Application Id                                   */
/*                SendToApplication - Callback function                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RMapRegister (UINT1 u1AppId,
              INT4 (*SendToApplication) (UINT1 *pu1RMapName, UINT4 u4Status))
{
    RMAP_SEND_UPDATE_MSG_CALLBACK (u1AppId) = SendToApplication;
}

/*****************************************************************************/
/* Function     : RMapDeRegister                                             */
/*                                                                           */
/* Description  : This function used to deregister the applications          */
/*                (RTM, protocol..) with RouteMap module                     */
/*                                                                           */
/* Input        : u1AppId - Application Id                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RMapDeRegister (UINT1 u1AppId)
{
    RMAP_SEND_UPDATE_MSG_CALLBACK (u1AppId) = NULL;
}

VOID
RMapTRtInfo2TRtMapInfo (tRtMapInfo * pOut, tRtInfo * pIn, UINT4 u4SrcAddr)
{
    MEMSET (pOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV4 ((pOut->DstXAddr), pIn->u4DestNet);
    IPV4_MASK_TO_MASKLEN (pOut->u2DstPrefixLen, pIn->u4DestMask);

    IPVX_ADDR_INIT_FROMV4 (pOut->SrcXAddr, u4SrcAddr);

    IPVX_ADDR_INIT_FROMV4 (pOut->NextHopXAddr, pIn->u4NextHop);

    pOut->u2RtProto = pIn->u2RtProto;
    pOut->u4IfIndex = pIn->u4RtIfIndx;
    pOut->i4Metric = pIn->i4Metric1;
    pOut->u1MetricType = (INT4) pIn->i4MetricType;
    pOut->i2RouteType = (INT2) pIn->u2RtType;
    pOut->u4RouteTag = pIn->u4RouteTag;

    pOut->u4NextHopAS = pIn->u4RtNxtHopAS;
    pOut->au4Community[0] = pIn->u4Community;
    pOut->u4LocalPref = pIn->u4LocalPref;
    pOut->i4Origin = pIn->i4Origin;
}

VOID
RMapTIp6RtEntry2TRtMapInfo (tRtMapInfo * pOut,
                            tIp6RtEntry * pIn, tIp6Addr * pSrcAddr)
{
    MEMSET (pOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV6 (pOut->DstXAddr, &(pIn->dst));
    pOut->u2DstPrefixLen = pIn->u1Prefixlen;

    IPVX_ADDR_INIT_FROMV6 (pOut->SrcXAddr, pSrcAddr);

    IPVX_ADDR_INIT_FROMV6 (pOut->NextHopXAddr, &(pIn->nexthop));

    pOut->u2RtProto = (UINT2) pIn->i1Proto;
    pOut->u4IfIndex = pIn->u4Index;
    pOut->i4Metric = pIn->u4Metric;
    pOut->u1MetricType = pIn->u1MetricType;

    pOut->i2RouteType = pIn->i1Type;
    pOut->u4RouteTag = pIn->u4RouteTag;

}

VOID
RMapTRtMapInfo2TRtInfo (tRtInfo * pOut, tRtMapInfo * pIn)
{
    UINT4               u4Comm = 0;
    /* modify some route attributes, preserve others */
    MEMCPY (&pOut->u4DestNet, pIn->DstXAddr.au1Addr, sizeof (UINT4));
    IPV4_MASKLEN_TO_MASK (pOut->u4DestMask, pIn->u2DstPrefixLen);
    MEMCPY (&pOut->u4NextHop, pIn->NextHopXAddr.au1Addr, sizeof (UINT4));

    pOut->u4RtIfIndx = pIn->u4IfIndex;
    pOut->i4Metric1 = pIn->i4Metric;
    pOut->i4MetricType = pIn->u1MetricType;    /*TODO: maybe invalid */
    pOut->u2RtType = pIn->i2RouteType;
    pOut->u4RouteTag = pIn->u4RouteTag;

    pOut->u4RtNxtHopAS = pIn->u4NextHopAS;

    pOut->u1Community = pIn->u1CommunityCnt;

    if ((pOut->u1Community != 0) && (pOut->pCommunity != NULL))
    {
        if (pOut->u1Community)
        {
            MEMCPY (pOut->pCommunity, pIn->au4Community,
                    pOut->u1Community * sizeof (UINT4));
        }
        /*First Community is Standard Community */
        if (pOut->pCommunity->au4Community[0] == RMAP_COMM_NONE)
        {
            pOut->u1Community = 0;
        }
        for (u4Comm = 0; u4Comm < pOut->u1Community; u4Comm++)
        {
            if (pOut->pCommunity->au4Community[u4Comm] == RMAP_COMM_INTERNET)
            {
                pOut->pCommunity->au4Community[u4Comm] = 0;
            }
        }
    }
    pOut->u4LocalPref = pIn->u4LocalPref;
    pOut->i4Origin = pIn->i4Origin;
    pOut->i4RMapFlag = pIn->i4RMapFlag;
}

VOID
RMapTRtMapInfo2TIp6RtEntry (tIp6RtEntry * pOut, tRtMapInfo * pIn)
{
    /* modify some route attributes, preserve others */
    UINT4               u4Comm = 0;

    MEMCPY (&(pOut->dst), pIn->DstXAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
    pOut->u1Prefixlen = (UINT1) pIn->u2DstPrefixLen;
    MEMCPY (&(pOut->nexthop), pIn->NextHopXAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    pOut->u4Index = pIn->u4IfIndex;
    pOut->u4Metric = (UINT4) pIn->i4Metric;
    pOut->i1Type = (INT1) pIn->i2RouteType;

    pOut->u1MetricType = pIn->u1MetricType;
    pOut->u4RouteTag = pIn->u4RouteTag;
    pOut->i4RMapFlag = pIn->i4RMapFlag;

    pOut->u1CommCount = pIn->u1CommunityCnt;

    if ((pOut->u1CommCount != 0) && (pOut->pCommunity != NULL))
    {
        if (pOut->u1CommCount)
        {
            MEMCPY (pOut->pCommunity, pIn->au4Community,
                    pOut->u1CommCount * sizeof (UINT4));
        }
        if (pOut->pCommunity->au4Community[0] == RMAP_COMM_NONE)
        {
            pOut->u1CommCount = 0;
        }
        for (u4Comm = 0; u4Comm < pOut->u1CommCount; u4Comm++)
        {
            if (pOut->pCommunity->au4Community[u4Comm] == RMAP_COMM_INTERNET)
            {
                pOut->pCommunity->au4Community[u4Comm] = 0;
            }
        }

    }

}

/*****************************************************************************/
/* Function     : RMapGetInitialMapStatus                                    */
/*                                                                           */
/* Description  : Return statuses of MAP-nodes, one bit per node             */
/*                Should be invoked from protocol to get                     */
/*                initial status of route map                                */
/*                                                                           */
/* Input        : pRMapName - Pointer to RouteMap Name                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 0 - all MAP-nodes are not ACTIVE or not exist              */
/*                        route map should NOT be applied                    */
/*                other - at least one MAP-node exists and ACTIVE,           */
/*                        route map should be applied                        */
/*****************************************************************************/
UINT4
RMapGetInitialMapStatus (UINT1 *pu1RMapName)
{
    UINT4               u4Status = 0;

    RMapLockRead ();
    u4Status = RMapGetMapStatus (pu1RMapName);
    RMapUnLockRead ();

    return u4Status;
}

/*****************************************************************************/
/* Function     : RMapApplyRule3                                             */
/*                                                                           */
/* Description  : This function applies route map on the given               */
/*                route. If the route map MATCH not exists returns           */
/*                RMAP_ROUTE_DENY. If the route map MATCH entry exists,      */
/*                checks for the access state. If the access state is DENY   */
/*                this function returns RMAP_ROUTE_DENY. If the route map    */
/*                entry exits and the access state is PERMIT, this function  */
/*                checks for the SET rule and applies the rule to            */
/*                pOutputRtInfo                                              */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - Output Route Information                      */
/*                                                                           */
/* Returns      : RMAP_ROUTE_PERMIT/RMAP_ROUTE_DENY                          */
/*****************************************************************************/
UINT1
RMapApplyRule3 (tRtMapInfo * pInputRtInfo, tRtMapInfo * pOutputRtInfo,
                UINT1 *pu1RMapName)
{
    tRMapNode          *pRMapNode = NULL;
    UINT4               u4SeqNo = 0;
    UINT1               u1PermitFlag = RMAP_ROUTE_DENY;

    MEMSET (pOutputRtInfo, 0, sizeof (tRtMapInfo));

    if ((pInputRtInfo == NULL) || (pu1RMapName == NULL))
        return RMAP_ENTRY_NOT_MATCHED;

    MEMCPY (pOutputRtInfo, pInputRtInfo, sizeof (tRtMapInfo));

    RMapLockRead ();

    /* Get the First instance of pu1RMapName */
    pRMapNode = RMapGetNextRouteMapNode (pu1RMapName, u4SeqNo);
    do
    {
        if (pRMapNode == NULL)
        {
            /* No more RMaps to scan */
            break;
        }
        if (MEMCMP (pRMapNode->au1RMapName, pu1RMapName,
                    STRLEN (pu1RMapName)) != 0)
        {
            /* No more Rmaps with the given Rmap name pu1RMapName */
            pRMapNode = NULL;
            break;
        }
        if (pRMapNode->u1RowStatus == RMAP_ACTIVE)
        {
            break;
        }
        u4SeqNo = pRMapNode->u4SeqNo;
    }
    while ((pRMapNode =
            RMapGetNextRouteMapNode (pu1RMapName, u4SeqNo)) != NULL);

    if (pRMapNode != NULL)
    {
        /* Found the Route Map MATCH entry for the route, Check for the
         *            SET Rule, if exists apply the rule  */
        RMapApplySetRule (pOutputRtInfo, pRMapNode);
        u1PermitFlag = RMAP_ROUTE_PERMIT;
    }
    RMapUnLockRead ();

    return u1PermitFlag;
}

/*****************************************************************************/
/* Function     : RMapGetNextValidIpPrefixEntry                              */
/*                                                                           */
/* Description  : This function is to fetch the next valid IP prefix entry   */
/*                configured for the given Prefix list name                  */
/*                                                                           */
/* Input        : pu1IpPrefixName - Ip prefix list name                      */
/*                u4SeqNo - Sequence Number                                  */
/*                                                                           */
/* Output       : pOrfEntry - pointer to the orf entry                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS/RMAP_FAILURE                                  */
/*****************************************************************************/
INT4
RMapGetNextValidIpPrefixEntry (UINT1 *pu1IpPrefixName, UINT4 u4SeqNo,
                               tBgp4OrfEntry * pOrfEntry)
{
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;

    pRMapNode = RMapGetNextRouteMapNode (pu1IpPrefixName, u4SeqNo);

    while (pRMapNode != NULL)
    {
        if (STRCMP (pRMapNode->au1RMapName, pu1IpPrefixName) == 0)
        {
            if ((pRMapNode->u1RowStatus == RMAP_ACTIVE)
                && (pRMapNode->u1IsIpPrefixInfo == TRUE))
            {
                pRMapMatchNode =
                    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY -
                                             1].pNode;

                if ((pRMapMatchNode != NULL)
                    && (pRMapMatchNode->u1RowStatus == RMAP_ACTIVE))
                {
                    pOrfEntry->u4SeqNo = pRMapNode->u4SeqNo;
                    if (pRMapNode->u1Access == RMAP_PERMIT)
                    {
                        pOrfEntry->u1Match = BGP4_ORF_PERMIT;
                    }
                    else if (pRMapNode->u1Access == RMAP_DENY)
                    {
                        pOrfEntry->u1Match = BGP4_ORF_DENY;
                    }

                    pOrfEntry->AddrPrefix.u2Afi =
                        pRMapMatchNode->IpPrefixAddrType;
                    MEMCPY (pOrfEntry->AddrPrefix.au1Address,
                            pRMapMatchNode->IpPrefixAddr,
                            pRMapMatchNode->IpPrefixAddrLen);
                    pOrfEntry->AddrPrefix.u2AddressLen =
                        pRMapMatchNode->IpPrefixAddrLen;

                    pOrfEntry->u1MinPrefixLen = pRMapMatchNode->IpPrefixMinLen;
                    pOrfEntry->u1MaxPrefixLen = pRMapMatchNode->IpPrefixMaxLen;
                    pOrfEntry->u1PrefixLen =
                        (UINT1) pRMapMatchNode->IpPrefixLength;

                    return RMAP_SUCCESS;
                }
            }
            u4SeqNo = pRMapNode->u4SeqNo;
            pRMapNode = RMapGetNextRouteMapNode (pu1IpPrefixName, u4SeqNo);
        }
        else
        {
            break;
        }
    }
    return RMAP_FAILURE;
}

/*****************************************************************************/
/* Function     : RMapCheckIfIpPrefixExist                                   */
/*                                                                           */
/* Description  : This function will check if Ip Prefix with the same name   */
/*                exists                                                     */
/*                                                                           */
/* Input        : pu1RMapName - Route Map name                               */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RMAP_SUCCESS/RMAP_FAILURE                                  */
/*****************************************************************************/
INT4
RMapCheckIfIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pu1RMapName)
{
    tRMapIpPrefix      *pIpPrefixNode = NULL;
    tRMapIpPrefix       IpPrefixNode;

    MEMSET (&IpPrefixNode, 0, sizeof (tRMapIpPrefix));

    RMAP_OCT_2_ASCIIZ (IpPrefixNode.au1IpPrefixName, pu1RMapName);

    /* Check if Ip Prefix list with the same name as route-map name doesn't exist */
    pIpPrefixNode = RBTreeGet (gRMapGlobalInfo.IpPrefixRoot,
                               (tRBElem *) & IpPrefixNode);
    if (pIpPrefixNode != NULL)
    {
        if (pIpPrefixNode->u1IsIpPrefixList == TRUE)
        {
            /* Ip Prefix exists! */
            return RMAP_SUCCESS;
        }
    }
    return RMAP_FAILURE;
}

/*****************************************************************************/
/* Function     : RMapApplyMatchIpPrefixRule                                 */
/*                                                                           */
/* Description  : This function applies route map on the given               */
/*                route. If the route map MATCH not exists returns           */
/*                RMAP_ROUTE_DENY. If the route map MATCH entry exists,      */
/*                checks for the access state. If the access state is DENY   */
/*                this function returns RMAP_ROUTE_DENY. If the route map    */
/*                entry exits and the access state is PERMIT, this function  */
/*                checks for the SET rule and applies the rule to            */
/*                pOutputRtInfo                                              */
/*                                                                           */
/* Input        : pInRtInfo - Input Route Information                        */
/*                pu1RMapName - Route Map Name                               */
/*                                                                           */
/* Output       : pOutRtInfo - Output Route Information                      */
/*                                                                           */
/* Returns      : RMAP_ROUTE_PERMIT -if all existing MATCH conditions are met*/
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_PERMIT -if no MATCH conditions exist            */
/*                                   and MAP node PERMIT                     */
/*              : RMAP_ROUTE_DENY   -if all existing MATCH conditions are met*/
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if no MATCH conditions exist            */
/*                                   and MAP node DENY                       */
/*              : RMAP_ROUTE_DENY   -if not all existing MATCH conditions    */
/*                                   are met (default result)                */
/*****************************************************************************/
UINT1
RMapApplyMatchIpPrefixRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode)
{
    tRMapMatchNode     *pTempRMapMatchNode = NULL;
    tIPvXAddr           InetXAddr;
    tIPvXAddr           ZeroInetXAddr;
    tRMapNode          *pTempRMapNode = NULL;

    INT4                i4IsEntryMatched = RMAP_ZERO;
    INT4                i4IsIp4SameSubnet = RMAP_ZERO;
    INT4                i4IsIp6SameSubnet = RMAP_ZERO;
    INT1                i1MatchFound = -1;
    UINT4               u4IpAddr = 0;
    UINT4               u4IpAddr1 = 0;
    UINT4               u4Mask = 0;

    IPVX_ADDR_CLEAR (&InetXAddr);
    IPVX_ADDR_CLEAR (&ZeroInetXAddr);

    /* Search for Route map Entry */
    pTempRMapNode = pRMapNode;

    if ((pTempRMapNode != NULL) && ((pTempRMapNode->u1RowStatus == RMAP_ACTIVE)
                                    && (pTempRMapNode->u1IsIpPrefixInfo ==
                                        TRUE)))
    {
        i4IsEntryMatched = RMAP_ONE;
        pTempRMapMatchNode =
            pTempRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;

        if ((pTempRMapMatchNode != NULL)
            && (pTempRMapMatchNode->u1RowStatus == RMAP_ACTIVE))
        {
            if (pTempRMapMatchNode->IpPrefixAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                IPVX_ADDR_COPY (&InetXAddr, &(pTempRMapMatchNode->IpPrefixA));
                MEMCPY (&u4IpAddr, pTempRMapMatchNode->IpPrefixAddr,
                        sizeof (UINT4));
                u4IpAddr = OSIX_NTOHL (u4IpAddr);
                MEMCPY (InetXAddr.au1Addr, &u4IpAddr, sizeof (UINT4));
            }
            if (pTempRMapMatchNode->IpPrefixAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                IPVX_ADDR_COPY (&InetXAddr, &(pTempRMapMatchNode->IpPrefixA));
            }
            if ((pTempRMapMatchNode->IpPrefixMinLen == 0) &&
                (pTempRMapMatchNode->IpPrefixMaxLen == 0))
            {
                if (0 == (CmpIpvxAddrWithPrefix (&(InetXAddr),
                                                 pTempRMapMatchNode->
                                                 IpPrefixLength,
                                                 &(pInputRtInfo->DstXAddr),
                                                 pInputRtInfo->u2DstPrefixLen)))
                {
                    if (pTempRMapMatchNode->IpPrefixLength ==
                        pInputRtInfo->u2DstPrefixLen)
                    {
                        if (pTempRMapNode->u1Access == RMAP_PERMIT)
                        {
                            i1MatchFound = PERMIT_PREFIX;
                        }
                        else
                        {
                            i1MatchFound = DENY_PREFIX;
                        }
                    }
                }
                else
                {
                    i1MatchFound = RMAP_ENTRY_NOT_MATCHED;
                }
                if (pTempRMapMatchNode->IpPrefixAddrType ==
                    pInputRtInfo->DstXAddr.u1Afi)
                {
                    if (MEMCMP
                        (InetXAddr.au1Addr, ZeroInetXAddr.au1Addr,
                         InetXAddr.u1AddrLen) == 0)
                    {
                        PrefixListDefaultRouteHandling (InetXAddr, pInputRtInfo,
                                                        pTempRMapMatchNode,
                                                        pTempRMapNode,
                                                        &i1MatchFound);
                    }
                }

            }
            else
            {
                if (pTempRMapMatchNode->IpPrefixAddrType ==
                    pInputRtInfo->DstXAddr.u1Afi)
                {
                    i4IsIp4SameSubnet = RMAP_ZERO;

                    i4IsIp6SameSubnet = RMAP_ZERO;
                    if (pTempRMapMatchNode->IpPrefixAddrType ==
                        IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (&u4IpAddr1, pInputRtInfo->DstXAddr.au1Addr,
                                sizeof (UINT4));
                        IPV4_MASKLEN_TO_MASK (u4Mask,
                                              pTempRMapMatchNode->
                                              IpPrefixLength);

                        /* to check if the prefixes are in same subnet */
                        i4IsIp4SameSubnet =
                            RMapIpIsOnSameSubnet (&(pInputRtInfo->DstXAddr),
                                                  &InetXAddr,
                                                  (UINT4) pInputRtInfo->
                                                  u2DstPrefixLen,
                                                  pTempRMapMatchNode->
                                                  IpPrefixLength);
                    }
                    else if (pTempRMapMatchNode->IpPrefixAddrType ==
                             IPVX_ADDR_FMLY_IPV6)
                    {
                        i4IsIp6SameSubnet =
                            RMapIpIsOnSameSubnet (&(pInputRtInfo->DstXAddr),
                                                  &InetXAddr,
                                                  (UINT4) pInputRtInfo->
                                                  u2DstPrefixLen,
                                                  pTempRMapMatchNode->
                                                  IpPrefixLength);
                    }

                    if ((i4IsIp4SameSubnet == TRUE)
                        || (i4IsIp6SameSubnet == TRUE))
                    {
                        if ((pInputRtInfo->u2DstPrefixLen >=
                             pTempRMapMatchNode->IpPrefixMinLen)
                            && (pTempRMapMatchNode->IpPrefixMaxLen != 0))
                        {
                            if (pInputRtInfo->u2DstPrefixLen <=
                                pTempRMapMatchNode->IpPrefixMaxLen)
                            {
                                if (pTempRMapNode->u1Access == RMAP_PERMIT)
                                {
                                    i1MatchFound = PERMIT_PREFIX;
                                }
                                else
                                {
                                    i1MatchFound = DENY_PREFIX;
                                }
                            }
                        }
                        else if (pInputRtInfo->u2DstPrefixLen >=
                                 pTempRMapMatchNode->IpPrefixMinLen)
                        {
                            if (pTempRMapNode->u1Access == RMAP_PERMIT)
                            {
                                i1MatchFound = PERMIT_PREFIX;
                            }
                            else
                            {
                                i1MatchFound = DENY_PREFIX;
                            }
                        }
                    }
                    if (MEMCMP
                        (InetXAddr.au1Addr, ZeroInetXAddr.au1Addr,
                         InetXAddr.u1AddrLen) == 0)
                    {
                        PrefixListDefaultRouteHandling (InetXAddr, pInputRtInfo,
                                                        pTempRMapMatchNode,
                                                        pTempRMapNode,
                                                        &i1MatchFound);
                    }
                }

            }

        }
    }
    UNUSED_PARAM (u4Mask);
    UNUSED_PARAM (i4IsEntryMatched);
    return i1MatchFound;
}

/*****************************************************************************/
/* Function     : RMapIpPrefixExist                                          */
/*                                                                           */
/* Description  : This is a wrapper function to get the address-family of    */
/*                IP prefix-list                                             */
/*                                                                           */
/* Input        : pIpPrefixNameOct - Pointer to Ip Prefix list Name          */
/*                u1Afi - Address family of neighbor                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Sequence number if entry present else ZERO                 */
/*****************************************************************************/

UINT4
RMapIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct, UINT1 u1Afi)
{

    UINT4               u4Status = 0;

    RMapLockRead ();
    u4Status = RMapUtilIsSameIpPrefixExist (pIpPrefixNameOct, u1Afi);
    RMapUnLockRead ();

    return u4Status;
}
