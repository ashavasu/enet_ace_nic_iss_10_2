
/* $Id: fsrmapwr.c,v 1.8 2016/04/01 12:08:32 siva Exp $*/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrmapwr.h"
# include  "fsrmapdb.h"
#include "rmapinc.h"
INT4
GetNextIndexFsRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRouteMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRouteMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSRMAP ()
{
    SNMPRegisterMib (&fsrmapOID, &fsrmapEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsrmapOID, (const UINT1 *) "fsrmap");
}

VOID
UnRegisterFSRMAP ()
{
    SNMPUnRegisterMib (&fsrmapOID, &fsrmapEntry);
    SNMPDelSysorEntry (&fsrmapOID, (const UINT1 *) "fsrmap");
}

INT4
FsRouteMapAccessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapAccess (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapAccessSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapAccess (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsRouteMapRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRouteMapAccessTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapAccess (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRouteMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRouteMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRouteMapTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRouteMapMatchTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRouteMapMatchTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             &(pNextMultiIndex->pIndex[8].i4_SLongValue),
             &(pNextMultiIndex->pIndex[9].i4_SLongValue),
             &(pNextMultiIndex->pIndex[10].u4_ULongValue),
             &(pNextMultiIndex->pIndex[11].u4_ULongValue),
             &(pNextMultiIndex->pIndex[12].i4_SLongValue),
             &(pNextMultiIndex->pIndex[13].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRouteMapMatchTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             pFirstMultiIndex->pIndex[6].i4_SLongValue,
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             pFirstMultiIndex->pIndex[7].u4_ULongValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             pFirstMultiIndex->pIndex[8].i4_SLongValue,
             &(pNextMultiIndex->pIndex[8].i4_SLongValue),
             pFirstMultiIndex->pIndex[9].i4_SLongValue,
             &(pNextMultiIndex->pIndex[9].i4_SLongValue),
             pFirstMultiIndex->pIndex[10].u4_ULongValue,
             &(pNextMultiIndex->pIndex[10].u4_ULongValue),
             pFirstMultiIndex->pIndex[11].u4_ULongValue,
             &(pNextMultiIndex->pIndex[11].u4_ULongValue),
             pFirstMultiIndex->pIndex[12].i4_SLongValue,
             &(pNextMultiIndex->pIndex[12].i4_SLongValue),
             pFirstMultiIndex->pIndex[13].i4_SLongValue,
             &(pNextMultiIndex->pIndex[13].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRouteMapMatchRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapMatchTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].u4_ULongValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].u4_ULongValue,
         pMultiIndex->pIndex[8].i4_SLongValue,
         pMultiIndex->pIndex[9].i4_SLongValue,
         pMultiIndex->pIndex[10].u4_ULongValue,
         pMultiIndex->pIndex[11].u4_ULongValue,
         pMultiIndex->pIndex[12].i4_SLongValue,
         pMultiIndex->pIndex[13].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapMatchRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].u4_ULongValue,
             pMultiIndex->pIndex[6].i4_SLongValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].i4_SLongValue,
             pMultiIndex->pIndex[10].u4_ULongValue,
             pMultiIndex->pIndex[11].u4_ULongValue,
             pMultiIndex->pIndex[12].i4_SLongValue,
             pMultiIndex->pIndex[13].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapMatchRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapMatchRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].u4_ULongValue,
             pMultiIndex->pIndex[6].i4_SLongValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].i4_SLongValue,
             pMultiIndex->pIndex[10].u4_ULongValue,
             pMultiIndex->pIndex[11].u4_ULongValue,
             pMultiIndex->pIndex[12].i4_SLongValue,
             pMultiIndex->pIndex[13].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRouteMapMatchRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapMatchRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[5].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[6].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[7].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[8].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[9].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[10].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[11].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[12].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[13].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRouteMapMatchTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRouteMapMatchTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRouteMapSetTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRouteMapSetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRouteMapSetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRouteMapSetInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetIpNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetIpNextHop (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRouteMapSetMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsRouteMapSetMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetMetricType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetASPathTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetASPathTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRouteMapSetCommunityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetCommunity (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsRouteMapSetOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetOrigin (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetOriginASNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetOriginASNum
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRouteMapSetLocalPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetLocalPreference
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRouteMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRouteMapSetRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRouteMapSetInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetIpNextHopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetIpNextHop (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetMetricType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetASPathTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetASPathTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetCommunitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetCommunity (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsRouteMapSetOriginSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetOrigin (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetOriginASNumSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetOriginASNum
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetLocalPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetLocalPreference
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRouteMapSetRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetInterface (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetIpNextHopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetIpNextHop (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetMetric (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetTag (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetMetricType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetASPathTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetASPathTag (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetCommunityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetCommunity (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsRouteMapSetOriginTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetOrigin (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetOriginASNumTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetOriginASNum (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsRouteMapSetLocalPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetLocalPreference (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRouteMapSetRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRouteMapSetTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRouteMapSetTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRMapTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRMapAccessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapAccess (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapIsIpPrefixListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapIsIpPrefixList (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapAccessSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapAccess (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsRMapRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRMapIsIpPrefixListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapIsIpPrefixList (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRMapAccessTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapAccess (pu4Error,
                                   pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapIsIpPrefixListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapIsIpPrefixList (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRMapTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRMapMatchTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRMapMatchTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pNextMultiIndex->pIndex[6].pOctetStrValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             &(pNextMultiIndex->pIndex[8].i4_SLongValue),
             pNextMultiIndex->pIndex[9].pOctetStrValue,
             &(pNextMultiIndex->pIndex[10].i4_SLongValue),
             &(pNextMultiIndex->pIndex[11].i4_SLongValue),
             &(pNextMultiIndex->pIndex[12].u4_ULongValue),
             &(pNextMultiIndex->pIndex[13].i4_SLongValue),
             &(pNextMultiIndex->pIndex[14].i4_SLongValue),
             &(pNextMultiIndex->pIndex[15].u4_ULongValue),
             &(pNextMultiIndex->pIndex[16].u4_ULongValue),
             &(pNextMultiIndex->pIndex[17].i4_SLongValue),
             &(pNextMultiIndex->pIndex[18].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRMapMatchTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pFirstMultiIndex->pIndex[6].pOctetStrValue,
             pNextMultiIndex->pIndex[6].pOctetStrValue,
             pFirstMultiIndex->pIndex[7].u4_ULongValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             pFirstMultiIndex->pIndex[8].i4_SLongValue,
             &(pNextMultiIndex->pIndex[8].i4_SLongValue),
             pFirstMultiIndex->pIndex[9].pOctetStrValue,
             pNextMultiIndex->pIndex[9].pOctetStrValue,
             pFirstMultiIndex->pIndex[10].i4_SLongValue,
             &(pNextMultiIndex->pIndex[10].i4_SLongValue),
             pFirstMultiIndex->pIndex[11].i4_SLongValue,
             &(pNextMultiIndex->pIndex[11].i4_SLongValue),
             pFirstMultiIndex->pIndex[12].u4_ULongValue,
             &(pNextMultiIndex->pIndex[12].u4_ULongValue),
             pFirstMultiIndex->pIndex[13].i4_SLongValue,
             &(pNextMultiIndex->pIndex[13].i4_SLongValue),
             pFirstMultiIndex->pIndex[14].i4_SLongValue,
             &(pNextMultiIndex->pIndex[14].i4_SLongValue),
             pFirstMultiIndex->pIndex[15].u4_ULongValue,
             &(pNextMultiIndex->pIndex[15].u4_ULongValue),
             pFirstMultiIndex->pIndex[16].u4_ULongValue,
             &(pNextMultiIndex->pIndex[16].u4_ULongValue),
             pFirstMultiIndex->pIndex[17].i4_SLongValue,
             &(pNextMultiIndex->pIndex[17].i4_SLongValue),
             pFirstMultiIndex->pIndex[18].i4_SLongValue,
             &(pNextMultiIndex->pIndex[18].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRMapMatchRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapMatchTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue,
         pMultiIndex->pIndex[7].u4_ULongValue,
         pMultiIndex->pIndex[8].i4_SLongValue,
         pMultiIndex->pIndex[9].pOctetStrValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].i4_SLongValue,
         pMultiIndex->pIndex[12].u4_ULongValue,
         pMultiIndex->pIndex[13].i4_SLongValue,
         pMultiIndex->pIndex[14].i4_SLongValue,
         pMultiIndex->pIndex[15].u4_ULongValue,
         pMultiIndex->pIndex[16].u4_ULongValue,
         pMultiIndex->pIndex[17].i4_SLongValue,
         pMultiIndex->pIndex[18].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapMatchRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        pMultiIndex->pIndex[6].pOctetStrValue,
                                        pMultiIndex->pIndex[7].u4_ULongValue,
                                        pMultiIndex->pIndex[8].i4_SLongValue,
                                        pMultiIndex->pIndex[9].pOctetStrValue,
                                        pMultiIndex->pIndex[10].i4_SLongValue,
                                        pMultiIndex->pIndex[11].i4_SLongValue,
                                        pMultiIndex->pIndex[12].u4_ULongValue,
                                        pMultiIndex->pIndex[13].i4_SLongValue,
                                        pMultiIndex->pIndex[14].i4_SLongValue,
                                        pMultiIndex->pIndex[15].u4_ULongValue,
                                        pMultiIndex->pIndex[16].u4_ULongValue,
                                        pMultiIndex->pIndex[17].i4_SLongValue,
                                        pMultiIndex->pIndex[18].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapMatchDestMaxPrefixLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapMatchTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue,
         pMultiIndex->pIndex[7].u4_ULongValue,
         pMultiIndex->pIndex[8].i4_SLongValue,
         pMultiIndex->pIndex[9].pOctetStrValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].i4_SLongValue,
         pMultiIndex->pIndex[12].u4_ULongValue,
         pMultiIndex->pIndex[13].i4_SLongValue,
         pMultiIndex->pIndex[14].i4_SLongValue,
         pMultiIndex->pIndex[15].u4_ULongValue,
         pMultiIndex->pIndex[16].u4_ULongValue,
         pMultiIndex->pIndex[17].i4_SLongValue,
         pMultiIndex->pIndex[18].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapMatchDestMaxPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].pOctetStrValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].i4_SLongValue,
             pMultiIndex->pIndex[12].u4_ULongValue,
             pMultiIndex->pIndex[13].i4_SLongValue,
             pMultiIndex->pIndex[14].i4_SLongValue,
             pMultiIndex->pIndex[15].u4_ULongValue,
             pMultiIndex->pIndex[16].u4_ULongValue,
             pMultiIndex->pIndex[17].i4_SLongValue,
             pMultiIndex->pIndex[18].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapMatchDestMinPrefixLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapMatchTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].pOctetStrValue,
         pMultiIndex->pIndex[7].u4_ULongValue,
         pMultiIndex->pIndex[8].i4_SLongValue,
         pMultiIndex->pIndex[9].pOctetStrValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].i4_SLongValue,
         pMultiIndex->pIndex[12].u4_ULongValue,
         pMultiIndex->pIndex[13].i4_SLongValue,
         pMultiIndex->pIndex[14].i4_SLongValue,
         pMultiIndex->pIndex[15].u4_ULongValue,
         pMultiIndex->pIndex[16].u4_ULongValue,
         pMultiIndex->pIndex[17].i4_SLongValue,
         pMultiIndex->pIndex[18].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapMatchDestMinPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].pOctetStrValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].i4_SLongValue,
             pMultiIndex->pIndex[12].u4_ULongValue,
             pMultiIndex->pIndex[13].i4_SLongValue,
             pMultiIndex->pIndex[14].i4_SLongValue,
             pMultiIndex->pIndex[15].u4_ULongValue,
             pMultiIndex->pIndex[16].u4_ULongValue,
             pMultiIndex->pIndex[17].i4_SLongValue,
             pMultiIndex->pIndex[18].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapMatchRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapMatchRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].pOctetStrValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        pMultiIndex->pIndex[6].pOctetStrValue,
                                        pMultiIndex->pIndex[7].u4_ULongValue,
                                        pMultiIndex->pIndex[8].i4_SLongValue,
                                        pMultiIndex->pIndex[9].pOctetStrValue,
                                        pMultiIndex->pIndex[10].i4_SLongValue,
                                        pMultiIndex->pIndex[11].i4_SLongValue,
                                        pMultiIndex->pIndex[12].u4_ULongValue,
                                        pMultiIndex->pIndex[13].i4_SLongValue,
                                        pMultiIndex->pIndex[14].i4_SLongValue,
                                        pMultiIndex->pIndex[15].u4_ULongValue,
                                        pMultiIndex->pIndex[16].u4_ULongValue,
                                        pMultiIndex->pIndex[17].i4_SLongValue,
                                        pMultiIndex->pIndex[18].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRMapMatchDestMaxPrefixLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapMatchDestMaxPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].pOctetStrValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].i4_SLongValue,
             pMultiIndex->pIndex[12].u4_ULongValue,
             pMultiIndex->pIndex[13].i4_SLongValue,
             pMultiIndex->pIndex[14].i4_SLongValue,
             pMultiIndex->pIndex[15].u4_ULongValue,
             pMultiIndex->pIndex[16].u4_ULongValue,
             pMultiIndex->pIndex[17].i4_SLongValue,
             pMultiIndex->pIndex[18].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRMapMatchDestMinPrefixLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapMatchDestMinPrefixLen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].pOctetStrValue,
             pMultiIndex->pIndex[7].u4_ULongValue,
             pMultiIndex->pIndex[8].i4_SLongValue,
             pMultiIndex->pIndex[9].pOctetStrValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].i4_SLongValue,
             pMultiIndex->pIndex[12].u4_ULongValue,
             pMultiIndex->pIndex[13].i4_SLongValue,
             pMultiIndex->pIndex[14].i4_SLongValue,
             pMultiIndex->pIndex[15].u4_ULongValue,
             pMultiIndex->pIndex[16].u4_ULongValue,
             pMultiIndex->pIndex[17].i4_SLongValue,
             pMultiIndex->pIndex[18].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRMapMatchRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapMatchRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           pMultiIndex->pIndex[6].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[7].u4_ULongValue,
                                           pMultiIndex->pIndex[8].i4_SLongValue,
                                           pMultiIndex->pIndex[9].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[10].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[11].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[12].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[13].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[14].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[15].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[16].
                                           u4_ULongValue,
                                           pMultiIndex->pIndex[17].
                                           i4_SLongValue,
                                           pMultiIndex->pIndex[18].
                                           i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRMapMatchDestMaxPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapMatchDestMaxPrefixLen (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[3].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[4].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[5].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[6].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[7].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[8].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[9].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[10].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[11].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[12].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[13].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[14].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[15].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[16].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[17].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[18].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsRMapMatchDestMinPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapMatchDestMinPrefixLen (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[3].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[4].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[5].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[6].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[7].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[8].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[9].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[10].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[11].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[12].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[13].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[14].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[15].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[16].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[17].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[18].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsRMapMatchTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRMapMatchTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRMapSetTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRMapSetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRMapSetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRMapSetNextHopInetTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetNextHopInetType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetNextHopInetAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetNextHopInetAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsRMapSetInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetRouteType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetASPathTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetASPathTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetCommunityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetCommunity (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsRMapSetLocalPrefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetLocalPref (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetOrigin (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetWeightGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetWeight (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetEnableAutoTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetEnableAutoTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetLevel (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetExtCommIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetExtCommId (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetExtCommPOIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetExtCommPOI (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetExtCommCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetExtCommCost (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsRMapSetCommunityAdditiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRMapSetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRMapSetCommunityAdditive
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRMapSetNextHopInetTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetNextHopInetType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRMapSetNextHopInetAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetNextHopInetAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsRMapSetInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetMetric (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRMapSetTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->u4_ULongValue));

}

INT4
FsRMapSetRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetRouteType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetASPathTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetASPathTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsRMapSetCommunitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetCommunity (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsRMapSetLocalPrefSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetLocalPref (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetOriginSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetOrigin (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRMapSetWeightSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetWeight (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsRMapSetEnableAutoTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetEnableAutoTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRMapSetLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetLevel (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRMapSetRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetExtCommIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetExtCommId (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsRMapSetExtCommCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetExtCommCost (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsRMapSetCommunityAdditiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRMapSetCommunityAdditive
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRMapSetNextHopInetTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetNextHopInetType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRMapSetNextHopInetAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetNextHopInetAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsRMapSetInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetInterface (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRMapSetMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetMetric (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetTag (pu4Error,
                                   pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsRMapSetRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetRouteType (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRMapSetASPathTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetASPathTag (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsRMapSetCommunityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetCommunity (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsRMapSetLocalPrefTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetLocalPref (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRMapSetOriginTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetOrigin (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRMapSetWeightTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetWeight (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsRMapSetEnableAutoTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetEnableAutoTag (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRMapSetLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetLevel (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRMapSetRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRMapSetExtCommIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetExtCommId (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsRMapSetExtCommCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetExtCommCost (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsRMapSetCommunityAdditiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRMapSetCommunityAdditive (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsRMapSetTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRMapSetTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRmapTrapCfgEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRmapTrapCfgEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsRmapTrapCfgEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRmapTrapCfgEnable (pMultiData->i4_SLongValue));
}

INT4
FsRmapTrapCfgEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRmapTrapCfgEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRmapTrapCfgEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRmapTrapCfgEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
