/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: rmaptdfs.h,v 1.16 2016/04/01 12:08:32 siva Exp $
 * 
 * Description: Contain the data structure definitions related to
 *   the route-map.
 *************************************************************************/
#ifndef  _RMAPTDFS_H__
#define  _RMAPTDFS_H_


/* MATCH commands Macros */
enum RMap_Match_Cmds
{
    RMAP_MATCH_CMD_DEST_IPX =1,
    RMAP_MATCH_CMD_SOURCE_IPX,
    RMAP_MATCH_CMD_NEXTHOP_IPX,

    RMAP_MATCH_CMD_INTERFACE,
    RMAP_MATCH_CMD_METRIC,
    RMAP_MATCH_CMD_TAG,
    RMAP_MATCH_CMD_METRIC_TYPE,
    RMAP_MATCH_CMD_ROUTE_TYPE,

    RMAP_MATCH_CMD_ASPATH_TAG,
    RMAP_MATCH_CMD_COMMUNITY,
    RMAP_MATCH_CMD_LOCAL_PREF,
    RMAP_MATCH_CMD_ORIGIN,
    RMAP_MATCH_IP_PREFIX_ENTRY,

    RMAP_MAX_MATCH_CMD
};

/* SET commands Macros, also used as bits of remove-mask  */
enum RMap_Set_Cmds
{
    RMAP_SET_CMD_NEXTHOP_IPX=1,

    RMAP_SET_CMD_INTERFACE,
    RMAP_SET_CMD_METRIC,
    RMAP_SET_CMD_TAG,
/*  RMAP_SET_CMD_METRIC_TYPE,  -- not supported */
    RMAP_SET_CMD_ROUTE_TYPE,

    RMAP_SET_CMD_ASPATH_TAG,
    RMAP_SET_CMD_COMMUNITY,
    RMAP_SET_CMD_LOCAL_PREF,
    RMAP_SET_CMD_ORIGIN,

    RMAP_SET_CMD_WEIGHT,
    RMAP_SET_CMD_AUTO_TAG_ENA,
    RMAP_SET_CMD_LEVEL
};

typedef struct {
    INT4  (*SendToApplication)
        (UINT1 *pu1RMapName , UINT4 u4Status);
}tRMapRegnInfo;

typedef struct
{
    tRMapRegnInfo  aRMapRegnTbl[RMAP_MAX_APPLNS];
    tRBTree        RMapRoot;  /* Root of the rmap tree */
    tRBTree        IpPrefixRoot;  /* Root of the rmap tree */
    tMemPoolId     RMapPoolId;
    tMemPoolId     RMapMatchPoolId;
    tMemPoolId     RMapMatchListPoolId;
    tMemPoolId     RMapSetPoolId;
    tMemPoolId     IpPrefixPoolId;
    tOsixSemId     RMapRwSemId; /* protect rmap r/w */
    tOsixSemId     RMapIntSemId; /* protect internal data */
    UINT4          u4ReaderCount;
    UINT4          u4RMapTrapEnabled;
}tRMapGlobalInfo;

typedef struct {
 tIPvXAddr   InetXAddr; 
    UINT4       u4PrefixLen;/* u1 extended to u4 to avoid padding */
}tRMapXAddrInfo;

typedef struct 
{
    tRMapXAddrInfo  AddrInfo;
    UINT1           u1MinPrefixLen;
    UINT1           u1MaxPrefixLen;
    UINT1           au1Pad [2];
}tIpPrefixInfo;

/* Match command structure  */
typedef struct {
    tRBNodeEmbd   RBRMapMatchNode;  /* Embedded RBTree node */ 
    union {
        tRMapXAddrInfo      DstXAddrInfo;
        tRMapXAddrInfo      SrcXAddrInfo;
        tIpPrefixInfo       IpPrefixInfo;
        tIPvXAddr           NextHopX;

        UINT4               u4IfIndex;
        INT4                i4Metric;/* common for tRtInfo.i4Metric1 & tIp6RtEntry.u4Metric */
        UINT4               u4Tag;
        UINT1               u1MetricType;
        INT2                i2RouteType;/* common for tRtInfo.u2RtType & tIp6RtEntry.i1Type */

        UINT4               u4NextHopAS;
        UINT4               u4Community;
        UINT4               u4LocalPref;
        INT4                i4Origin;  
    }u;
    UINT1       u1Cmd; /* MATCH cmds can take one of the union members */
    UINT1       u1RowStatus;
    UINT1       au1Align[2];

#define RMapDestT           u.DstXAddrInfo.InetXAddr.u1Afi      /*type of addr*/
#define RMapDestL           u.DstXAddrInfo.InetXAddr.u1AddrLen  /*len of addr*/
#define RMapDestA           u.DstXAddrInfo.InetXAddr.au1Addr    /*bin addr*/
#define RMapDestX           u.DstXAddrInfo.InetXAddr            /*IPvX struct*/
#define RMapDestPrefixLen   u.DstXAddrInfo.u4PrefixLen

#define RMapSourceT         u.SrcXAddrInfo.InetXAddr.u1Afi      /*type of addr*/
#define RMapSourceL         u.SrcXAddrInfo.InetXAddr.u1AddrLen  /*len of addr*/
#define RMapSourceA         u.SrcXAddrInfo.InetXAddr.au1Addr    /*bin addr*/
#define RMapSourceX         u.SrcXAddrInfo.InetXAddr            /*IPvX struct*/
#define RMapSourcePrefixLen u.SrcXAddrInfo.u4PrefixLen

#define RMapNextHopT        u.NextHopX.u1Afi                    /*type of addr*/
#define RMapNextHopL        u.NextHopX.u1AddrLen                /*len of addr*/
#define RMapNextHopA        u.NextHopX.au1Addr                  /*bin addr*/
#define RMapNextHopX        u.NextHopX                          /*IPvX struct*/

#define RMapIfIndex         u.u4IfIndex
#define RMapMetric          u.i4Metric
#define RMapTag             u.u4Tag
#define RMapMetricType      u.u1MetricType
#define RMapRouteType       u.i2RouteType

#define RMapNextHopAS       u.u4NextHopAS
#define RMapCommunity       u.u4Community
#define RMapLocalPref       u.u4LocalPref
#define RMapOrigin          u.i4Origin

#define IpPrefixA           u.IpPrefixInfo.AddrInfo.InetXAddr
#define IpPrefixAddr        u.IpPrefixInfo.AddrInfo.InetXAddr.au1Addr
#define IpPrefixAddrType    u.IpPrefixInfo.AddrInfo.InetXAddr.u1Afi
#define IpPrefixAddrLen     u.IpPrefixInfo.AddrInfo.InetXAddr.u1AddrLen
#define IpPrefixLength      u.IpPrefixInfo.AddrInfo.u4PrefixLen
#define IpPrefixMinLen      u.IpPrefixInfo.u1MinPrefixLen
#define IpPrefixMaxLen      u.IpPrefixInfo.u1MaxPrefixLen
}tRMapMatchNode;


/* Set command structure  */
typedef struct {
    
    tIPvXAddr   NextHopX;

    UINT4       u4IfIndex;
    INT4        i4Metric;/* common for tRtInfo.i4Metric1 & tIp6RtEntry.u4Metric */
    UINT4       u4Tag;
/*  UINT1       u1MetricType; -- could not be set*/
    UINT4       u4NextHopAS;
    UINT4       au4Community[RMAP_BGP_MAX_COMM];
    UINT4       u4LocalPref;
    INT4        i4Origin;
/*  UINT4       u4OriginAsNum; -- not supported*/

    /*  BGP Extended Community Attributes */
    UINT4       u4ExtCommCost;
    UINT2       u2ExtCommId;
    UINT2       u2ExtCommPOI;

    INT2        i2RouteType;/* common for tRtInfo.u2RtType & tIp6RtEntry.i1Type */
    UINT2       u2Weight;
    UINT1       u1AutoTagEna;
    UINT1       u1Level;

    /* these fields should be the last */
    UINT1       u1RowStatus; 
    UINT1       u1Addflag;
    UINT4       u4RemoveMask; /* bitmask of fields to be removed */
    UINT1       au1ASSegs[BGP4_AS_SEG_LEN];
    UINT4       u4MatchAsSeg;
    UINT1       u1AsCnt;
    UINT1       u1CommunityCnt;
    UINT1       au1RMapCommunityList[RMAP_COMM_STR_LEN];
    UINT1       au1Pad[2];
}tRMapSetNode;

/* sizeof SET node without last fields */
#define RMAP_SET_FIELDS_SIZE    ( sizeof(tRMapSetNode)-1-4-1-BGP4_AS_SEG_LEN-4-1-3 )

struct _MatchList{
    tRMapMatchNode    *pNode;
    struct _MatchList *pNext;
};

typedef struct _MatchList tMatchList;

#define MAX_MATCH_RECORDS (RMAP_MAX_MATCH_CMD - 1) 

/*  Structure of route-map   */
typedef struct {
    tRBNodeEmbd   RBRMapNode;       
                        /*  Embedded RBTree node */ 
    tRBTree       RMapMatchRoot;        /* Root of the match list */
    tMatchList    MatchCmdArray[MAX_MATCH_RECORDS];  /* array which contain all the match records for this route map */
    tRMapSetNode *pSetNode;
    UINT1         au1RMapName[RMAP_MAX_NAME_LEN + 4];  /* Name of the  route-map  */

    UINT4         u4SeqNo;   /*Seq no in rmap and Ip-Prefix list.
                               For Ip-prefix list seq no range is (1-1-4294967294).
                               So UINT4 is used*/
    UINT1         u1Access;                            /* permit/deny         */
    UINT1         u1RowStatus;
    UINT1         u1IsIpPrefixInfo; /* To decide whether the entry corresponds to IP Prefix list or not*/
    UINT1         u1Pad;
}tRMapNode;
typedef struct 
{
    tRBNodeEmbd   IpPrefixRBNode;       
    UINT1         au1IpPrefixName[RMAP_MAX_NAME_LEN + 4];  /* Name of the  route-map  */
    UINT4         u4LastUsedSeq;
    UINT4         u4Count;
    UINT1         u1IsIpPrefixList;
    UINT1         au1Pad [3];
}tRMapIpPrefix;

#endif  /* _RMAPTDFS_H__ */
