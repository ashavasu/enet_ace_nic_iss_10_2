/***************************************************************************
* Copyright (C) Aricent communication Software Limited,2007
*
* $Id: rmapsz.h,v 1.3 2013/06/26 12:08:24 siva Exp $
* Description: Contains all the prototype of functions used in RouteMap module 
***************************************************************************/

enum {
    MAX_ROUTEMAP_MATCH_LISTS_SIZING_ID,
    MAX_ROUTEMAP_MATCH_NODES_SIZING_ID,
    MAX_ROUTEMAP_NODES_SIZING_ID,
    MAX_ROUTEMAP_SET_NODES_SIZING_ID,
    MAX_IP_PREFIX_NODES_SIZING_ID,
    RMAP_MAX_SIZING_ID
};


#ifdef  _RMAPSZ_C
tMemPoolId RMAPMemPoolIds[ RMAP_MAX_SIZING_ID];
INT4  RmapSizingMemCreateMemPools(VOID);
VOID  RmapSizingMemDeleteMemPools(VOID);
INT4  RmapSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RMAPSZ_C  */
extern tMemPoolId RMAPMemPoolIds[ ];
extern INT4  RmapSizingMemCreateMemPools(VOID);
extern VOID  RmapSizingMemDeleteMemPools(VOID);
extern INT4  RmapSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _RMAPSZ_C  */


#ifdef  _RMAPSZ_C
tFsModSizingParams FsRMAPSizingParams [] = {
{ "tMatchList", "MAX_ROUTEMAP_MATCH_LISTS", sizeof(tMatchList),MAX_ROUTEMAP_MATCH_LISTS, MAX_ROUTEMAP_MATCH_LISTS,0 },
{ "tRMapMatchNode", "MAX_ROUTEMAP_MATCH_NODES", sizeof(tRMapMatchNode),MAX_ROUTEMAP_MATCH_NODES, MAX_ROUTEMAP_MATCH_NODES,0 },
{ "tRMapNode", "MAX_ROUTEMAP_NODES", sizeof(tRMapNode),MAX_ROUTEMAP_NODES, MAX_ROUTEMAP_NODES,0 },
{ "tRMapSetNode", "MAX_ROUTEMAP_SET_NODES", sizeof(tRMapSetNode),MAX_ROUTEMAP_SET_NODES, MAX_ROUTEMAP_SET_NODES,0 },
{ "tRMapIpPrefix", "MAX_IP_PREFIX_NODES", sizeof(tRMapIpPrefix),MAX_IP_PREFIX_NODES, MAX_IP_PREFIX_NODES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RMAPSZ_C  */
extern tFsModSizingParams FsRMAPSizingParams [];
#endif /*  _RMAPSZ_C  */


