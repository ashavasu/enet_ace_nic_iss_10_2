/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmaptrace.h,v 1.4 2010/12/14 10:40:44 siva Exp $
 *
 * Description: Provides general purpose macros for tracing.
 *
 *******************************************************************/

#include "trace.h"
#ifndef _RMAP_TRACE_H_
#define _RMAP_TRACE_H_

/* Trace and debug flags */
#define  RMAP_TRC_FLAG   gu4RMapGlobalTrc 
#define  RMAP_DBG_FLAG   gu4RMapGlobalDbg 

/* Trace definitions */
#if TRACE_WANTED
#define RMAP_TRC(cmod, mask,mod,fmt) \
                { if ((RMAP_DBG_FLAG & cmod) == cmod) {\
                     UtlTrcLog(RMAP_DBG_FLAG, \
                                  mask,               \
                                  mod,         \
                                  fmt); \
                  }\
                }

#define RMAP_TRC_ARG1(cmod,  mask,  mod, fmt, arg1) \
              { if ((RMAP_DBG_FLAG & cmod) == cmod) {\
           UtlTrcLog(RMAP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1); \
                }\
              }

#define RMAP_TRC_ARG2( cmod, mask, mod, fmt,arg1,arg2) \
              { if ((RMAP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(RMAP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2);\
                }\
             }

#define RMAP_TRC_ARG3( cmod, mask, mod, fmt,arg1,arg2,arg3) \
              { if ((RMAP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(RMAP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2,                \
            arg3);\
                }\
             }
#else
#define RMAP_TRC(cmod, mask,mod,fmt) 
#define RMAP_TRC_ARG1( cmod, mask, mod, fmt,arg1)
#define RMAP_TRC_ARG2( cmod, mask, mod, fmt,arg1,arg2)
#define RMAP_TRC_ARG3( cmod, mask, mod, fmt,arg1,arg2,arg3)
#endif

#define  RMAP_MGMT_TRC          MGMT_TRC          
#define  RMAP_PROTO_TRC         ALL_FAILURE_TRC  
#define  RMAP_BUF_TRC           BUFFER_TRC 
#define  RMAP_INIT_TRC          INIT_SHUT_TRC 

 
#define  RMAP_MODULE_NAME        "RMAP" 
#define  RMAP_MOD_TRC            0x0001
#endif /* _TRACE_H_ */


