/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmapdb.h,v 1.10 2016/04/01 12:08:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRMAPDB_H
#define _FSRMAPDB_H

UINT1 FsRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsRouteMapMatchTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRouteMapSetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsRMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsRMapMatchTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsRMapSetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsrmap [] ={1,3,6,1,4,1,2076,155};
tSNMP_OID_TYPE fsrmapOID = {8, fsrmap};


UINT4 FsRouteMapName [ ] ={1,3,6,1,4,1,2076,155,1,1,1,1};
UINT4 FsRouteMapSeqNum [ ] ={1,3,6,1,4,1,2076,155,1,1,1,2};
UINT4 FsRouteMapAccess [ ] ={1,3,6,1,4,1,2076,155,1,1,1,3};
UINT4 FsRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,155,1,1,1,4};
UINT4 FsRouteMapMatchInterface [ ] ={1,3,6,1,4,1,2076,155,1,2,1,1};
UINT4 FsRouteMapMatchIpAddress [ ] ={1,3,6,1,4,1,2076,155,1,2,1,2};
UINT4 FsRouteMapMatchIpAddrMask [ ] ={1,3,6,1,4,1,2076,155,1,2,1,3};
UINT4 FsRouteMapMatchIpNextHop [ ] ={1,3,6,1,4,1,2076,155,1,2,1,4};
UINT4 FsRouteMapMatchMetric [ ] ={1,3,6,1,4,1,2076,155,1,2,1,5};
UINT4 FsRouteMapMatchTag [ ] ={1,3,6,1,4,1,2076,155,1,2,1,6};
UINT4 FsRouteMapMatchRouteType [ ] ={1,3,6,1,4,1,2076,155,1,2,1,7};
UINT4 FsRouteMapMatchMetricType [ ] ={1,3,6,1,4,1,2076,155,1,2,1,8};
UINT4 FsRouteMapMatchASPathTag [ ] ={1,3,6,1,4,1,2076,155,1,2,1,9};
UINT4 FsRouteMapMatchCommunity [ ] ={1,3,6,1,4,1,2076,155,1,2,1,10};
UINT4 FsRouteMapMatchOrigin [ ] ={1,3,6,1,4,1,2076,155,1,2,1,11};
UINT4 FsRouteMapMatchLocalPreference [ ] ={1,3,6,1,4,1,2076,155,1,2,1,12};
UINT4 FsRouteMapMatchRowStatus [ ] ={1,3,6,1,4,1,2076,155,1,2,1,13};
UINT4 FsRouteMapSetInterface [ ] ={1,3,6,1,4,1,2076,155,1,3,1,1};
UINT4 FsRouteMapSetIpNextHop [ ] ={1,3,6,1,4,1,2076,155,1,3,1,2};
UINT4 FsRouteMapSetMetric [ ] ={1,3,6,1,4,1,2076,155,1,3,1,3};
UINT4 FsRouteMapSetTag [ ] ={1,3,6,1,4,1,2076,155,1,3,1,4};
UINT4 FsRouteMapSetMetricType [ ] ={1,3,6,1,4,1,2076,155,1,3,1,5};
UINT4 FsRouteMapSetASPathTag [ ] ={1,3,6,1,4,1,2076,155,1,3,1,6};
UINT4 FsRouteMapSetCommunity [ ] ={1,3,6,1,4,1,2076,155,1,3,1,7};
UINT4 FsRouteMapSetOrigin [ ] ={1,3,6,1,4,1,2076,155,1,3,1,8};
UINT4 FsRouteMapSetOriginASNum [ ] ={1,3,6,1,4,1,2076,155,1,3,1,9};
UINT4 FsRouteMapSetLocalPreference [ ] ={1,3,6,1,4,1,2076,155,1,3,1,10};
UINT4 FsRouteMapSetRowStatus [ ] ={1,3,6,1,4,1,2076,155,1,3,1,11};
UINT4 FsRMapName [ ] ={1,3,6,1,4,1,2076,155,2,1,1,1};
UINT4 FsRMapSeqNum [ ] ={1,3,6,1,4,1,2076,155,2,1,1,2};
UINT4 FsRMapAccess [ ] ={1,3,6,1,4,1,2076,155,2,1,1,3};
UINT4 FsRMapRowStatus [ ] ={1,3,6,1,4,1,2076,155,2,1,1,4};
UINT4 FsRMapIsIpPrefixList [ ] ={1,3,6,1,4,1,2076,155,2,1,1,5};
UINT4 FsRMapMatchDestInetType [ ] ={1,3,6,1,4,1,2076,155,2,2,1,1};
UINT4 FsRMapMatchDestInetAddress [ ] ={1,3,6,1,4,1,2076,155,2,2,1,2};
UINT4 FsRMapMatchDestInetPrefix [ ] ={1,3,6,1,4,1,2076,155,2,2,1,3};
UINT4 FsRMapMatchSourceInetType [ ] ={1,3,6,1,4,1,2076,155,2,2,1,4};
UINT4 FsRMapMatchSourceInetAddress [ ] ={1,3,6,1,4,1,2076,155,2,2,1,5};
UINT4 FsRMapMatchSourceInetPrefix [ ] ={1,3,6,1,4,1,2076,155,2,2,1,6};
UINT4 FsRMapMatchNextHopInetType [ ] ={1,3,6,1,4,1,2076,155,2,2,1,7};
UINT4 FsRMapMatchNextHopInetAddr [ ] ={1,3,6,1,4,1,2076,155,2,2,1,8};
UINT4 FsRMapMatchInterface [ ] ={1,3,6,1,4,1,2076,155,2,2,1,9};
UINT4 FsRMapMatchMetric [ ] ={1,3,6,1,4,1,2076,155,2,2,1,10};
UINT4 FsRMapMatchTag [ ] ={1,3,6,1,4,1,2076,155,2,2,1,11};
UINT4 FsRMapMatchMetricType [ ] ={1,3,6,1,4,1,2076,155,2,2,1,12};
UINT4 FsRMapMatchRouteType [ ] ={1,3,6,1,4,1,2076,155,2,2,1,13};
UINT4 FsRMapMatchASPathTag [ ] ={1,3,6,1,4,1,2076,155,2,2,1,14};
UINT4 FsRMapMatchCommunity [ ] ={1,3,6,1,4,1,2076,155,2,2,1,15};
UINT4 FsRMapMatchLocalPref [ ] ={1,3,6,1,4,1,2076,155,2,2,1,16};
UINT4 FsRMapMatchOrigin [ ] ={1,3,6,1,4,1,2076,155,2,2,1,17};
UINT4 FsRMapMatchRowStatus [ ] ={1,3,6,1,4,1,2076,155,2,2,1,18};
UINT4 FsRMapMatchDestMaxPrefixLen [ ] ={1,3,6,1,4,1,2076,155,2,2,1,19};
UINT4 FsRMapMatchDestMinPrefixLen [ ] ={1,3,6,1,4,1,2076,155,2,2,1,20};
UINT4 FsRMapSetNextHopInetType [ ] ={1,3,6,1,4,1,2076,155,2,3,1,1};
UINT4 FsRMapSetNextHopInetAddr [ ] ={1,3,6,1,4,1,2076,155,2,3,1,2};
UINT4 FsRMapSetInterface [ ] ={1,3,6,1,4,1,2076,155,2,3,1,3};
UINT4 FsRMapSetMetric [ ] ={1,3,6,1,4,1,2076,155,2,3,1,4};
UINT4 FsRMapSetTag [ ] ={1,3,6,1,4,1,2076,155,2,3,1,5};
UINT4 FsRMapSetRouteType [ ] ={1,3,6,1,4,1,2076,155,2,3,1,6};
UINT4 FsRMapSetASPathTag [ ] ={1,3,6,1,4,1,2076,155,2,3,1,7};
UINT4 FsRMapSetCommunity [ ] ={1,3,6,1,4,1,2076,155,2,3,1,8};
UINT4 FsRMapSetLocalPref [ ] ={1,3,6,1,4,1,2076,155,2,3,1,9};
UINT4 FsRMapSetOrigin [ ] ={1,3,6,1,4,1,2076,155,2,3,1,10};
UINT4 FsRMapSetWeight [ ] ={1,3,6,1,4,1,2076,155,2,3,1,11};
UINT4 FsRMapSetEnableAutoTag [ ] ={1,3,6,1,4,1,2076,155,2,3,1,12};
UINT4 FsRMapSetLevel [ ] ={1,3,6,1,4,1,2076,155,2,3,1,13};
UINT4 FsRMapSetRowStatus [ ] ={1,3,6,1,4,1,2076,155,2,3,1,14};
UINT4 FsRMapSetExtCommId [ ] ={1,3,6,1,4,1,2076,155,2,3,1,15};
UINT4 FsRMapSetExtCommPOI [ ] ={1,3,6,1,4,1,2076,155,2,3,1,16};
UINT4 FsRMapSetExtCommCost [ ] ={1,3,6,1,4,1,2076,155,2,3,1,17};
UINT4 FsRMapSetCommunityAdditive [ ] ={1,3,6,1,4,1,2076,155,2,3,1,18};
UINT4 FsRmapTrapCfgEnable [ ] ={1,3,6,1,4,1,2076,155,3,1};
UINT4 FsRMapTrapName [ ] ={1,3,6,1,4,1,2076,155,4,1};
UINT4 FsRMapTrapSeqNum [ ] ={1,3,6,1,4,1,2076,155,4,2};




tMbDbEntry fsrmapMibEntry[]= {

{{12,FsRouteMapName}, GetNextIndexFsRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRouteMapTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSeqNum}, GetNextIndexFsRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRouteMapTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapAccess}, GetNextIndexFsRouteMapTable, FsRouteMapAccessGet, FsRouteMapAccessSet, FsRouteMapAccessTest, FsRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapTableINDEX, 2, 1, 0, "1"},

{{12,FsRouteMapRowStatus}, GetNextIndexFsRouteMapTable, FsRouteMapRowStatusGet, FsRouteMapRowStatusSet, FsRouteMapRowStatusTest, FsRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapTableINDEX, 2, 1, 1, NULL},

{{12,FsRouteMapMatchInterface}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchIpAddress}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchIpAddrMask}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchIpNextHop}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchMetric}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchTag}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchRouteType}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchMetricType}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchASPathTag}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchCommunity}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchOrigin}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchLocalPreference}, GetNextIndexFsRouteMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRouteMapMatchTableINDEX, 14, 1, 0, NULL},

{{12,FsRouteMapMatchRowStatus}, GetNextIndexFsRouteMapMatchTable, FsRouteMapMatchRowStatusGet, FsRouteMapMatchRowStatusSet, FsRouteMapMatchRowStatusTest, FsRouteMapMatchTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapMatchTableINDEX, 14, 1, 1, NULL},

{{12,FsRouteMapSetInterface}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetInterfaceGet, FsRouteMapSetInterfaceSet, FsRouteMapSetInterfaceTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetIpNextHop}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetIpNextHopGet, FsRouteMapSetIpNextHopSet, FsRouteMapSetIpNextHopTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetMetric}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetMetricGet, FsRouteMapSetMetricSet, FsRouteMapSetMetricTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetTag}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetTagGet, FsRouteMapSetTagSet, FsRouteMapSetTagTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetMetricType}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetMetricTypeGet, FsRouteMapSetMetricTypeSet, FsRouteMapSetMetricTypeTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetASPathTag}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetASPathTagGet, FsRouteMapSetASPathTagSet, FsRouteMapSetASPathTagTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetCommunity}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetCommunityGet, FsRouteMapSetCommunitySet, FsRouteMapSetCommunityTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetOrigin}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetOriginGet, FsRouteMapSetOriginSet, FsRouteMapSetOriginTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetOriginASNum}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetOriginASNumGet, FsRouteMapSetOriginASNumSet, FsRouteMapSetOriginASNumTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetLocalPreference}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetLocalPreferenceGet, FsRouteMapSetLocalPreferenceSet, FsRouteMapSetLocalPreferenceTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 0, NULL},

{{12,FsRouteMapSetRowStatus}, GetNextIndexFsRouteMapSetTable, FsRouteMapSetRowStatusGet, FsRouteMapSetRowStatusSet, FsRouteMapSetRowStatusTest, FsRouteMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRouteMapSetTableINDEX, 2, 1, 1, NULL},

{{12,FsRMapName}, GetNextIndexFsRMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSeqNum}, GetNextIndexFsRMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapAccess}, GetNextIndexFsRMapTable, FsRMapAccessGet, FsRMapAccessSet, FsRMapAccessTest, FsRMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapTableINDEX, 2, 0, 0, "1"},

{{12,FsRMapRowStatus}, GetNextIndexFsRMapTable, FsRMapRowStatusGet, FsRMapRowStatusSet, FsRMapRowStatusTest, FsRMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapTableINDEX, 2, 0, 1, NULL},

{{12,FsRMapIsIpPrefixList}, GetNextIndexFsRMapTable, FsRMapIsIpPrefixListGet, FsRMapIsIpPrefixListSet, FsRMapIsIpPrefixListTest, FsRMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapTableINDEX, 2, 0, 0, "0"},

{{12,FsRMapMatchDestInetType}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchDestInetAddress}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchDestInetPrefix}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchSourceInetType}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchSourceInetAddress}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchSourceInetPrefix}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchNextHopInetType}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchNextHopInetAddr}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchInterface}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchMetric}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchTag}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchMetricType}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchRouteType}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchASPathTag}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchCommunity}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchLocalPref}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchOrigin}, GetNextIndexFsRMapMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchRowStatus}, GetNextIndexFsRMapMatchTable, FsRMapMatchRowStatusGet, FsRMapMatchRowStatusSet, FsRMapMatchRowStatusTest, FsRMapMatchTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapMatchTableINDEX, 19, 0, 1, NULL},

{{12,FsRMapMatchDestMaxPrefixLen}, GetNextIndexFsRMapMatchTable, FsRMapMatchDestMaxPrefixLenGet, FsRMapMatchDestMaxPrefixLenSet, FsRMapMatchDestMaxPrefixLenTest, FsRMapMatchTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapMatchDestMinPrefixLen}, GetNextIndexFsRMapMatchTable, FsRMapMatchDestMinPrefixLenGet, FsRMapMatchDestMinPrefixLenSet, FsRMapMatchDestMinPrefixLenTest, FsRMapMatchTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapMatchTableINDEX, 19, 0, 0, NULL},

{{12,FsRMapSetNextHopInetType}, GetNextIndexFsRMapSetTable, FsRMapSetNextHopInetTypeGet, FsRMapSetNextHopInetTypeSet, FsRMapSetNextHopInetTypeTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetNextHopInetAddr}, GetNextIndexFsRMapSetTable, FsRMapSetNextHopInetAddrGet, FsRMapSetNextHopInetAddrSet, FsRMapSetNextHopInetAddrTest, FsRMapSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetInterface}, GetNextIndexFsRMapSetTable, FsRMapSetInterfaceGet, FsRMapSetInterfaceSet, FsRMapSetInterfaceTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetMetric}, GetNextIndexFsRMapSetTable, FsRMapSetMetricGet, FsRMapSetMetricSet, FsRMapSetMetricTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetTag}, GetNextIndexFsRMapSetTable, FsRMapSetTagGet, FsRMapSetTagSet, FsRMapSetTagTest, FsRMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetRouteType}, GetNextIndexFsRMapSetTable, FsRMapSetRouteTypeGet, FsRMapSetRouteTypeSet, FsRMapSetRouteTypeTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetASPathTag}, GetNextIndexFsRMapSetTable, FsRMapSetASPathTagGet, FsRMapSetASPathTagSet, FsRMapSetASPathTagTest, FsRMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetCommunity}, GetNextIndexFsRMapSetTable, FsRMapSetCommunityGet, FsRMapSetCommunitySet, FsRMapSetCommunityTest, FsRMapSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetLocalPref}, GetNextIndexFsRMapSetTable, FsRMapSetLocalPrefGet, FsRMapSetLocalPrefSet, FsRMapSetLocalPrefTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetOrigin}, GetNextIndexFsRMapSetTable, FsRMapSetOriginGet, FsRMapSetOriginSet, FsRMapSetOriginTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetWeight}, GetNextIndexFsRMapSetTable, FsRMapSetWeightGet, FsRMapSetWeightSet, FsRMapSetWeightTest, FsRMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetEnableAutoTag}, GetNextIndexFsRMapSetTable, FsRMapSetEnableAutoTagGet, FsRMapSetEnableAutoTagSet, FsRMapSetEnableAutoTagTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, "2"},

{{12,FsRMapSetLevel}, GetNextIndexFsRMapSetTable, FsRMapSetLevelGet, FsRMapSetLevelSet, FsRMapSetLevelTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetRowStatus}, GetNextIndexFsRMapSetTable, FsRMapSetRowStatusGet, FsRMapSetRowStatusSet, FsRMapSetRowStatusTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 1, NULL},

{{12,FsRMapSetExtCommId}, GetNextIndexFsRMapSetTable, FsRMapSetExtCommIdGet, FsRMapSetExtCommIdSet, FsRMapSetExtCommIdTest, FsRMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetExtCommPOI}, GetNextIndexFsRMapSetTable, FsRMapSetExtCommPOIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetExtCommCost}, GetNextIndexFsRMapSetTable, FsRMapSetExtCommCostGet, FsRMapSetExtCommCostSet, FsRMapSetExtCommCostTest, FsRMapSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, NULL},

{{12,FsRMapSetCommunityAdditive}, GetNextIndexFsRMapSetTable, FsRMapSetCommunityAdditiveGet, FsRMapSetCommunityAdditiveSet, FsRMapSetCommunityAdditiveTest, FsRMapSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRMapSetTableINDEX, 2, 0, 0, "1"},

{{10,FsRmapTrapCfgEnable}, NULL, FsRmapTrapCfgEnableGet, FsRmapTrapCfgEnableSet, FsRmapTrapCfgEnableTest, FsRmapTrapCfgEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsRMapTrapName}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRMapTrapSeqNum}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsrmapEntry = { 74, fsrmapMibEntry };

#endif /* _FSRMAPDB_H */

