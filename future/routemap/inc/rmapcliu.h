/***************************************************************************
* Copyright (C) Aricent communication Software Limited,2009
*
* Description: Contains the prototypes of CLI handlers used in RouteMap module 
*
** $Id: rmapcliu.h,v 1.8 2016/04/01 12:08:32 siva Exp $
***************************************************************************/

INT4
RMapCliMatchDestIpXCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType,
          tSNMP_OCTET_STRING_TYPE* pInetAddrOct,
          UINT4 u4PrefixLen);
INT4
RMapCliMatchSourceIpXCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType,
          tSNMP_OCTET_STRING_TYPE* pInetAddrOct,
          UINT4 u4PrefixLen);
INT4
RMapCliMatchNextHopIpXCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType, tSNMP_OCTET_STRING_TYPE* pInetAddrOct);
INT4
RMapCliMatchInterfaceCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4IfIndex);
INT4
RMapCliMatchMetricCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Metric);
INT4
RMapCliMatchTagCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Tag);
INT4
RMapCliMatchMetricTypeCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4MetricType);
INT4
RMapCliMatchRouteTypeCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4RtType);
INT4
RMapCliMatchASPathTagCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4AsPath);
INT4
RMapCliMatchCommunityCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Community);
INT4
RMapCliMatchLocalPrefCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Pref);
INT4
RMapCliMatchOriginCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Origin);
INT4
RMapCliMatchDestIpXDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType,
          tSNMP_OCTET_STRING_TYPE* pInetAddrOct,
          UINT4 u4PrefixLen);
INT4
RMapCliMatchSourceIpXDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType,
          tSNMP_OCTET_STRING_TYPE* pInetAddrOct,
          UINT4 u4PrefixLen);
INT4
RMapCliMatchNextHopIpXDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType, tSNMP_OCTET_STRING_TYPE* pInetAddrOct);
INT4
RMapCliMatchInterfaceDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4IfIndex);
INT4
RMapCliMatchMetricDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Metric);
INT4
RMapCliMatchTagDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Tag);
INT4
RMapCliMatchMetricTypeDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4MetricType);
INT4
RMapCliMatchRouteTypeDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4RtType);
INT4
RMapCliMatchASPathTagDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4AsPath);
INT4
RMapCliMatchCommunityDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Community);
INT4
RMapCliMatchLocalPrefDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Pref);
INT4
RMapCliMatchOriginDelete (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Origin);
INT4
RMapCliSetNextHopIpXCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4InetAddrType,
          tSNMP_OCTET_STRING_TYPE* pInetAddrOct);
INT4
RMapCliSetInterfaceCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4IfIndex);
INT4
RMapCliSetMetricCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Metric);
INT4
RMapCliSetTagCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Tag);
INT4
RMapCliSetRouteTypeCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4RouteType);
INT4
RMapCliSetASPathTagCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4AsPathTag);
INT4
RMapCliSetCommunityCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          tSNMP_OCTET_STRING_TYPE *, UINT1 u1Addflag);
INT4
RMapCliSetLocalPrefCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4LocalPref);
INT4
RMapCliSetOriginCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          INT4 i4Origin);
INT4
RMapCliSetWeightCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Weight);
INT4
RMapCliSetEnableAutoTagCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4EnableAutoTag);
INT4
RMapCliSetLevelCreate (tCliHandle CliHandle,
          tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
          UINT4 u4Level);
INT4
RMapCliSetEntryDelete (tCliHandle CliHandle,
                    tSNMP_OCTET_STRING_TYPE *pRMapNameOct, UINT4 u4SeqNo,
                    UINT1 u1Cmd);
INT4
RMapCliSetExtCommCostCreate(tCliHandle CliHandle,
                    tSNMP_OCTET_STRING_TYPE * pRMapNameOct,UINT4 u4SeqNo, 
                    UINT2 u2ExtCommId, UINT4 u4ExtCommCost);

INT4
RMapCliCreateIpPrefixList (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pIpPrefixNameOct,
                           UINT4 u4SeqNum, tIpPrefixInfo *pIpPrefixInfo, UINT4 u4PrefixLen,
                           UINT1 u1Access);
INT4
RMapCliDelIpPrefixList (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pIpPrefixNameOct,
                        UINT4 u4SeqNum, UINT4 u4AddrType, tIpPrefixInfo *pIpPrefixInfo, 
                        UINT4 u4PrefixLen, UINT1 u1Access);
INT4
RMapCliShowIpPrefixListName (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pIpPrefixNameOct,
                             UINT1 u1AddrType, UINT1 u1DisplayAll, tRMapXAddrInfo *pIpAddrInfo, UINT1 u1Flag);

