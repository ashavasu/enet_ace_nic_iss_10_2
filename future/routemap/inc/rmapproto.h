 /***************************************************************************
* Copyright (C) Aricent communication Software Limited,2007
*
* $Id: rmapproto.h,v 1.8 2016/12/27 12:36:00 siva Exp $
* Description: Contains all the prototype of functions used in RouteMap module 
***************************************************************************/


#ifndef  _RMAPPROTO_H__
#define  _RMAPPROTO_H__

INT4  RMapCompareMapEntry (tRBElem * e1, tRBElem * e2);
INT4  RMapCompareMatchEntry (tRBElem * e1, tRBElem * e2);
INT4  RMapIpPrefixCompareEntry (tRBElem * e1, tRBElem * e2);

UINT1
RMapApplyMatchIpPrefixRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode);

UINT1
RMapApplyMatchRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode);
VOID
RMapApplySetRule (tRtMapInfo * pInputRtInfo, tRMapNode * pRMapNode);




VOID  RMapSendUpdateMsg (UINT1 *pu1RMapName);
VOID
RMapSendStatusUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status);

tRMapNode*  RMapGetRouteMapNode(UINT1 *pu1RMapName, UINT4 u4SeqNo);
tRMapNode*  RMapGetRouteMapNodeOct (tSNMP_OCTET_STRING_TYPE *pOct, UINT4 u4SeqNo);

tRMapNode*  RMapGetNextRouteMapNode (UINT1 *pu1RMapName, UINT4 u4SeqNo);
tRMapNode*  RMapGetNextRouteMapNodeOct (tSNMP_OCTET_STRING_TYPE *pOct, UINT4 u4SeqNo);



tRMapNode* RMapAddRouteMapNode (UINT1 *pu1RMapName, UINT4 u4SeqNo);
tRMapMatchNode* RMapAddMatchNode (tRMapNode *pRMapNode,
                                  tRMapMatchNode *pRMapMatchNode);
INT4 RMapDeleteRouteMapNode (tRMapNode *pRMapNode);
INT4 RMapDeleteMatchNode (tRMapNode *pRMapNode,
                             tRMapMatchNode *pRMapMatchNode);

/************************************************************/
INT1 CmpIp6Addr(tIp6Addr *pa1,UINT1 preflen1,
                tIp6Addr *pa2,UINT1 preflen2);

INT1 CmpIpvxAddrWithPrefix( tIPvXAddr *pa1 , UINT1 preflen1,
                            tIPvXAddr *pa2 , UINT1 preflen2);

INT1 CmpIpvxAddr(tIPvXAddr *pa1,
                 tIPvXAddr *pa2);

UINT4 TestZeroIpv6Addr(tIp6Addr *pIp6Addr);

UINT4 TestZeroIpvxAddr( tIPvXAddr *pa);

UINT1 TestZeroArray( UINT1 *pu1, UINT4 u4Size);

INT4 ValidateXAddr(tIPvXAddr *pXAddr);
INT4 ValidateIpv4Addr(tIPvXAddr *pXAddr, UINT4 i4PrefixLen);
INT4 ValidateXAddrPrefix(UINT4 u4Prefix , UINT1 u1Family);
/************************************************************/

UINT4 
RMapUtilIsSameIpPrefixExist (tSNMP_OCTET_STRING_TYPE * pIpPrefixNameOct,UINT1 u1Afi );

VOID RMapParamsToMatchNode(
    tRMapMatchNode *pMatchNode,
    UINT1                   u1IsIpPrefixInfo,
    INT4                    i4DestInetType , 
    tSNMP_OCTET_STRING_TYPE *pDestInetAddress , 
    UINT4                   u4DestInetPrefix , 
    
    INT4                    i4SourceInetType , 
    tSNMP_OCTET_STRING_TYPE *pSourceInetAddress , 
    UINT4                   u4SourceInetPrefix , 

    INT4                    i4NextHopInetType , 
    tSNMP_OCTET_STRING_TYPE *pNextHopInetAddr , 

    INT4    i4Interface , 
    INT4    i4Metric , 
    UINT4   u4Tag , 
    INT4    i4MetricType , 
    INT4    i4RouteType , 
    UINT4   u4ASPathTag , 
    UINT4   u4Community , 
    INT4    i4LocalPref , 
    INT4    i4Origin);


VOID RMapMatchNodeToParams(
    INT4                    *pi4DestInetType , 
    tSNMP_OCTET_STRING_TYPE *pDestInetAddress , 
    UINT4                   *pu4DestInetPrefix , 

    INT4                    *pi4SourceInetType , 
    tSNMP_OCTET_STRING_TYPE *pSourceInetAddress , 
    UINT4                   *pu4SourceInetPrefix , 

    INT4                    *pi4NextHopInetType , 
    tSNMP_OCTET_STRING_TYPE *pNextHopInetAddr , 

    INT4    *pi4Interface , 
    INT4    *pi4Metric , 
    UINT4   *pu4Tag , 
    INT4    *pi4MetricType , 
    INT4    *pi4RouteType , 
    UINT4   *pu4ASPathTag , 
    UINT4   *pu4Community , 
    INT4    *pi4LocalPref , 
    INT4    *pi4Origin,

    tRMapMatchNode *pRMapMatchNode);

UINT4
RMapGetMapStatus (UINT1 *pu1RMapName);

UINT4
RMapUtilIsIpPrefixExist (tSNMP_OCTET_STRING_TYPE *pIpPrefixNameOct,
                         tIpPrefixInfo *pIpPrefixInfo, UINT1 u1Access);

INT4
MatchIpvxAddrWithPrefix (tIPvXAddr *pIpAddr1, tIPvXAddr *pIpAddr2, UINT4 u4PrefixLen);

BOOL1
RMapIpIsOnSameSubnet (tIPvXAddr * pDestAddr,
                        tIPvXAddr * pPeerAddr,
                        UINT4 u4IncomingRtPrefixLen,
                        UINT4 u4RMapPrefixLen
                        );
VOID
PrefixListDefaultRouteHandling(tIPvXAddr InetXAddr,
                               tRtMapInfo *pInputRtInfo,
                               tRMapMatchNode *pTempRMapMatchNode,
                               tRMapNode *pTempRMapNode,
                               INT1 *pi1MatchFound);

#if defined(FUTURE_SNMP_WANTED) || defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
VOID
RMapSendMatchTrap( UINT1 *pu1MapName , UINT1 u1SeqNum );
#endif

#endif   /* _RMAPPROTO_H__ */
