/***************************************************************************
* Copyright (C) Aricent communication Software Limited,2007
*
* Description: Contains all the constants required for RouteMap module 
*
*  $Id: rmapinc.h,v 1.8 2013/06/26 12:08:24 siva Exp $
 ***************************************************************************/


#ifndef  _RMAPINC_H__
#define  _RMAPINC_H__
#include "lr.h"
#include "ip.h"

#include "ipv6.h"
#include "utilipvx.h"

#include "cfa.h"
#include "bgp.h"
#include "rmap.h"
#include "rmapdefs.h"
#include "rmaptdfs.h"
#include "rmapproto.h"
#include "rmaptrace.h"
#include "fsrmapwr.h"
#include "fsrmaplw.h"
#include "rmapsz.h"
#include "cli.h"
#include "rmapcli.h"
#include "ip6util.h"

#endif   /* _RMAPINC_H__  */
