/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrmaplw.h,v 1.8 2016/04/01 12:08:32 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsRouteMapTable. */
INT1
nmhValidateIndexInstanceFsRouteMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRouteMapTable  */

INT1
nmhGetFirstIndexFsRouteMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRouteMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRouteMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRouteMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRouteMapAccess ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRouteMapMatchTable. */
INT1
nmhValidateIndexInstanceFsRouteMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRouteMapMatchTable  */

INT1
nmhGetFirstIndexFsRouteMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    * , 
    INT4    * , 
    UINT4    * , 
    UINT4    * , 
    UINT4    * , 
    INT4    * , 
    UINT4    * , 
    INT4    * , 
    INT4    * , 
    UINT4    * , 
    UINT4    * , 
    INT4    * , 
    INT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRouteMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    UINT4    , 
    UINT4    * , 
    UINT4    , 
    UINT4    * , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    INT4    , 
    INT4    * , 
    UINT4    , 
    UINT4    * , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    INT4    , 
    INT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRouteMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRouteMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRouteMapMatchRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRouteMapMatchTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRouteMapSetTable. */
INT1
nmhValidateIndexInstanceFsRouteMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRouteMapSetTable  */

INT1
nmhGetFirstIndexFsRouteMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRouteMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRouteMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapSetIpNextHop ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRouteMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRouteMapSetMetricType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRouteMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRouteMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapSetOriginASNum ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRouteMapSetLocalPreference ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRouteMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRouteMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapSetIpNextHop ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRouteMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRouteMapSetMetricType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRouteMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRouteMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapSetOriginASNum ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRouteMapSetLocalPreference ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRouteMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRouteMapSetInterface ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapSetIpNextHop ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRouteMapSetMetric ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapSetTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRouteMapSetMetricType ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapSetASPathTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRouteMapSetCommunity ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *  ));

INT1
nmhTestv2FsRouteMapSetOrigin ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapSetOriginASNum ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRouteMapSetLocalPreference ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRouteMapSetRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRouteMapSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRMapTable. */
INT1
nmhValidateIndexInstanceFsRMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRMapTable  */

INT1
nmhGetFirstIndexFsRMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapIsIpPrefixList ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapIsIpPrefixList ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRMapAccess ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapIsIpPrefixList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRMapMatchTable. */
INT1
nmhValidateIndexInstanceFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRMapMatchTable  */

INT1
nmhGetFirstIndexFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    * , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    * , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    * , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    * , 
    INT4    * , 
    UINT4    * , 
    INT4    * , 
    INT4    * , 
    UINT4    * , 
    UINT4    * , 
    INT4    * , 
    INT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    , 
    INT4    * , 
    INT4    , 
    INT4    * , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    INT4    , 
    INT4    * , 
    UINT4    , 
    UINT4    * , 
    UINT4    , 
    UINT4    * , 
    INT4    , 
    INT4    * , 
    INT4    , 
    INT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4    ,
    INT4    *));

INT1
nmhGetFsRMapMatchDestMaxPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsRMapMatchDestMinPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4     ,
    INT4    ));

INT1
nmhSetFsRMapMatchDestMaxPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsRMapMatchDestMinPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRMapMatchRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     , 
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     , 
    INT4     , 
    UINT4     , 
    INT4     , 
    INT4     , 
    UINT4     , 
    UINT4     , 
    INT4     , 
    INT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapMatchDestMaxPrefixLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsRMapMatchDestMinPrefixLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRMapMatchTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRMapSetTable. */
INT1
nmhValidateIndexInstanceFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRMapSetTable  */

INT1
nmhGetFirstIndexFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    UINT4    , 
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRMapSetNextHopInetType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetNextHopInetAddr ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRMapSetRouteType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsRMapSetLocalPref ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetWeight ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4    *));

INT1
nmhGetFsRMapSetEnableAutoTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetLevel ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    INT4    *));

INT1
nmhGetFsRMapSetExtCommId ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4   *));

INT1
nmhGetFsRMapSetExtCommPOI ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4   *));

INT1
nmhGetFsRMapSetExtCommCost ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4   *));


INT1
nmhGetFsRMapSetCommunityAdditive ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRMapSetNextHopInetType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetNextHopInetAddr ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRMapSetRouteType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *  ));

INT1
nmhSetFsRMapSetLocalPref ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetWeight ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhSetFsRMapSetEnableAutoTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetLevel ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhSetFsRMapSetExtCommId ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4    ,
    UINT4   ));

INT1
nmhSetFsRMapSetExtCommCost ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,  
    UINT4    ,
    UINT4   ));

INT1 
nmhSetFsRMapSetCommunityAdditive ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRMapSetNextHopInetType ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetNextHopInetAddr ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRMapSetInterface ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetMetric ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRMapSetRouteType ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetASPathTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRMapSetCommunity ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *  ));

INT1
nmhTestv2FsRMapSetLocalPref ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetOrigin ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetWeight ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4    ));

INT1
nmhTestv2FsRMapSetEnableAutoTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetLevel ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    INT4    ));

INT1
nmhTestv2FsRMapSetExtCommId ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * , 
    UINT4     ,
    UINT4   ));

INT1
nmhTestv2FsRMapSetExtCommCost ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4   ));

INT1
nmhTestv2FsRMapSetCommunityAdditive ARG_LIST((UINT4 *  ,
                tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRMapSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRmapTrapCfgEnable ARG_LIST((
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRmapTrapCfgEnable ARG_LIST((
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRmapTrapCfgEnable ARG_LIST((UINT4 *  ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRmapTrapCfgEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
