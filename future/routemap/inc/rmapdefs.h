/***********************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* 
*  
*   $Id: rmapdefs.h,v 1.8 2016/04/01 12:08:31 siva Exp $
*
* Description: Contain the data structure definitions related to
*                 the route-map.
********************************************************************/
#ifndef  _RMAPTDFS_H__
#define  _RMAPTDFS_H_

#define  ROUTEMAP_MAX_SEQNO_PER_ROUTEMAP  100

#define  RMAP_MAX_ADDR_LEN                1024
#define  RMAP_CLI_RMAP_MODE_LEN           12


#define CLI_RMAP_MODE             "config-rmap-"
#define CLI_RMAP_MAX_NAME_LEN           36
#define RMAP_CLI_PROMPT           "config-route-map"
#define RMAP_DEFAULT_SEQ_NUM      1
#define RMAP_RB_GREATER           1
#define RMAP_RB_EQUAL             0
#define RMAP_RB_LESSER            -1

#define RMAP_RW_SEM_NAME          ((UINT1 *) "RMP1" )
#define RMAP_INT_SEM_NAME         ((UINT1 *) "RMP2" )

#define RMAP_SEM_FLAGS            OSIX_DEFAULT_SEM_MODE
#define RMAPP_SEM_NODE_ID         SELF

#define RMAP_ALLOC(Size, Type)   MEM_MALLOC(Size, Type)
#define RMAP_FREE(Ptr)           MEM_FREE(Ptr)

#define CLI_GET_RMAP_SEQNO()                    CliGetModeInfo()
#define CLI_SET_RMAP_SEQNO(SeqNo)               CliSetModeInfo(SeqNo)
#define CLI_GET_RMAP_NAME(Name)                 CliGetCurModePromptStr(Name)



#define  RMAP_ACTIVE                1
#define  RMAP_NOT_IN_SERVICE        2
#define  RMAP_NOT_READY             3
#define  RMAP_CREATE_AND_GO         4
#define  RMAP_CREATE_AND_WAIT       5
#define  RMAP_DESTROY               6

#define  RMAP_TRAP_ENABLED          1
#define  RMAP_TRAP_DISABLED         2
#define  RMAP_TRAP_DEFAULT          RMAP_TRAP_ENABLED

#define   RMAP_SEND_UPDATE_MSG_CALLBACK(u1AppId)   \
    gRMapGlobalInfo.aRMapRegnTbl[u1AppId].SendToApplication  

/* clear octet string */
#define RMAP_CLEAR_OCT(pOstr,len)\
    MEMSET( (pOstr)->pu1_OctetList , 0 , len );\
    (pOstr)->i4_Length=0

/* assign octet string to buffer, clear buffer & length */
/* WARNING: buffer should be NOT POINTER */
#define RMAP_INIT_OCT(Ostr,buf)\
    (Ostr).pu1_OctetList = buf;\
    MEMSET( buf , 0 , sizeof(buf) );\
    (Ostr).i4_Length=0

/* clear octet string with ipvx address */
#define RMAP_CLEAR_OXADDR(pOstr)\
    MEMSET( (pOstr)->pu1_OctetList , 0 , IPVX_MAX_INET_ADDR_LEN );\
    (pOstr)->i4_Length=0

/* convert octet (could be no space for ending zero) string to asciiz */
#define RMAP_OCT_2_ASCIIZ(dst,src)\
    MEMSET( dst , 0 , sizeof(dst) );\
    MEMCPY( dst , (src)->pu1_OctetList , MEM_MAX_BYTES( RMAP_MAX_NAME_LEN,(UINT4) (src)->i4_Length ))

/* convert octet (could be no space for ending zero) string to asciiz */
#define RMAPCOMM_OCT_2_ASCIIZ(dst,src)\
    MEMSET( dst , 0 , sizeof(dst) );\
    MEMCPY( dst , (src)->pu1_OctetList , MEM_MAX_BYTES( RMAP_COMM_STR_LEN,(UINT4) (src)->i4_Length ))


/* put zero at the end of octet string */
#define RMAP_PUT_ENDING_ZERO(Ostr)\
    (Ostr).pu1_OctetList[ (Ostr).i4_Length ] = 0

/* convert asciiz string to octet string */
#define RMAP_ASCIIZ_2_OCT(dst,src)\
    (dst)->i4_Length = (INT4)STRLEN(src);\
    MEMCPY( (dst)->pu1_OctetList , (src) , (dst)->i4_Length )

/* convert buffer to octet string */
#define RMAP_BUF_2_OCT(dst,src,len)\
    (dst)->i4_Length = (len);\
    MEMCPY( (dst)->pu1_OctetList , (src) , (len) )

/* copy octet strings */
#define RMAP_COPY_OCT(dst,src)\
    (dst)->i4_Length = (src)->i4_Length;\
    MEMCPY( (dst)->pu1_OctetList , (src)->pu1_OctetList , (src)->i4_Length )

/* compare asciiz with octet, 0=equal */
#define RMAP_CMP_ASCIIZ_OCT(dst,src)\
    MEMCMP( (dst) , (src)->pu1_OctetList , (src)->i4_Length )

/* compare octet strings */
#define RMAP_CMP_OCT(dst,src)\
    (   (dst)->i4_Length == (src)->i4_Length ? \
        MEMCMP( (dst)->pu1_OctetList , (src)->pu1_OctetList , (src)->i4_Length ) : 1 )

#endif  /* _RMAPTDFS_H__ */
