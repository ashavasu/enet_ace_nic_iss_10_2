##############################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.3 2009/06/04 04:51:17 premap-iss Exp $
#
# Description: Specifies the options to be
#              including for building the Future Route Map module
#
##############################################################


###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

GLOBAL_OPNS = ${GENERAL_COMPILATION_SWITCHES}\
              ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################

RMAP_BASE_DIR = ${BASE_DIR}/routemap
RMAP_INCD     = ${RMAP_BASE_DIR}/inc
RMAP_SRCD     = ${RMAP_BASE_DIR}/src
RMAP_OBJD     = ${RMAP_BASE_DIR}/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

RMAP_GLOBAL_INCLUDES =
# -I${IP_UDPINCD} \
#                      -I${IP_INCD} -I${IP_IPINCD} -I${IP_RTMINCD}\
#                      -I${IP_IPIFINCD} -I${IP_ICMPINCD} \
#                      -I${IP_IRDPINCD} -I${TRIEINCD} -I${IP_PINGINCD}\
#                      -I${IP_V4OV6INCD} -I${IP6LIBD} \
#                      -I${IP_RARPINCD}

GLOBAL_INCLUDES = $(RMAP_GLOBAL_INCLUDES) \
                    $(COMMON_INCLUDE_DIRS)

#############################################################################

