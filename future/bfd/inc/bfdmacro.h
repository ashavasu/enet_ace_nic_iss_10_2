/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdmacro.h,v 1.15 2015/09/11 09:50:42 siva Exp $
*
* Description: This file contains the macros used in BFD module 
*********************************************************************/

#ifndef __BFDMACRO_H__
#define __BFDMACRO_H__

#define BFD_MICROSEC_TO_MILLISEC(x)  x/1000

#define BFD_JITTER_PERCENT(x) x/100

#define BFD_QUEMSG_POOLID  BFDMemPoolIds[MAX_BFD_QUEMSG_SIZING_ID]

#define BFD_MPLSPATHINFOSTRUCT_POOLID   BFDMemPoolIds[MAX_BFD_MPLSPATHINFOSTRUCT_SIZING_ID]

#define BFD_EXTOUTPARAMS_POOLID   BFDMemPoolIds[MAX_BFD_EXTOUTPARAMS_SIZING_ID]

#define BFD_EXTINPARAMS_POOLID   BFDMemPoolIds[MAX_BFD_EXTINPARAMS_SIZING_ID]

#define BFD_PKTBUF_POOLID   BFDMemPoolIds[MAX_BFD_PKTBUF_SIZING_ID]

#define BFD_DAD_COMP_TIME 3

#define BFD_MIN_Q_OVERFLOW_SIZE 4

#define BFD_SYSLOG_ID gBfdGlobals.i4SyslogId 

#define BFD_LOG         BfdTrapEventLogNotify
#define BFD_ISS_LOG     BfdIssTrapEventLogNotify
#define BFD_SESS_SEM gBfdSessSem
#define BFD_GET_SYS_TIME        OsixGetSysTime

#define BFD_SESS_STATE(x)       ((x)->MibObject.i4FsMIStdBfdSessState)
#define BFD_SESS_DIAG(x)        ((x)->MibObject.i4FsMIStdBfdSessDiag)
#define BFD_SESS_ROLE(x)        ((x)->MibObject.i4FsMIBfdSessRole)
#define BFD_SESS_REMOTE_DISC(x) ((x)->MibObject.u4FsMIBfdSessRemoteDiscr)
#define BFD_SESS_LOCAL_DISC(x)  ((x)->MibObject.u4FsMIStdBfdSessDiscriminator)
#define BFD_SESS_OFFLD(x)       ((x)->MibObject.i4FsMIBfdSessOffld)
#define BFD_SESS_REQ_RX(x)    ((x)->MibObject.u4FsMIStdBfdSessReqMinRxInterval)
#define BFD_SESS_MIN_TX(x)  \
    ((x)->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval)
#define BFD_SESS_NEG_TX(x) \
    ((x)->MibObject.u4FsMIStdBfdSessNegotiatedInterval)
#define BFD_SESS_CCV_MODE(x) \
    ((x)->MibObject.i4FsMIBfdSessMode)
#define BFD_SESS_TYPE(x) \
    ((x)->MibObject.i4FsMIStdBfdSessType)

#define BFD_SESS_NEG_DETECT_MULT(x) \
    ((x)->MibObject.u4FsMIStdBfdSessNegotiatedDetectMult)

#define BFD_SESS_DETECT_MULT(x) \
    ((x)->MibObject.u4FsMIStdBfdSessDetectMult)

#define BFD_SESS_CONTEXT_ID(x) \
 ((x)->MibObject.u4FsMIStdBfdContextId)

#define BFD_SESS_DEST_UDP_PORT(x) \
 ((x)->MibObject.u4FsMIStdBfdSessDestinationUdpPort)

/*Macros used to assign values to buffer */
#define BFD_PUT_1BYTE(pu1PktBuf,u1Val)\
{\
    *pu1PktBuf = u1Val;\
    pu1PktBuf += 1;\
}

#define BFD_PUT_2BYTE(pu1PktBuf, u2Val)\
{\
     *((UINT2 *) ((VOID*)pu1PktBuf)) = OSIX_HTONS(u2Val);\
     pu1PktBuf += 2;\
}

#define BFD_PUT_4BYTE(pu1PktBuf, u4Val)\
{\
    *((UINT4 *) ((VOID*)pu1PktBuf)) = OSIX_HTONL(u4Val);\
    pu1PktBuf += 4;\
}

#define BFD_PUT_NBYTE(pu1PktBuf, pu1Src, u4Len)\
{\
    MEMCPY ((UINT1 *) pu1PktBuf, (UINT1 *) pu1Src, u4Len);\
    pu1PktBuf += u4Len;\
}

/* Macros used to extract values from buffer */

#define BFD_GET_1BYTE(u1Val, pu1Buf)\
{\
    u1Val = *pu1Buf;\
    pu1Buf += 1;\
}

#define BFD_GET_2BYTE(u2Val, pu1Buf)\
{\
    MEMCPY (&u2Val, pu1Buf, 2);\
    pu1Buf += 2;\
    u2Val = (UINT2) (OSIX_HTONS(u2Val));\
}

#define BFD_GET_4BYTE(u4Val, pu1Buf)\
{\
    MEMCPY (&u4Val, pu1Buf, 4);\
    pu1Buf += 4;\
    u4Val = (UINT4) (OSIX_HTONL(u4Val));\
}

#define BFD_GET_NBYTE(pu1Dest, pu1Buf, u4Len)\
{\
    MEMCPY ((UINT1 *) pu1Dest, (UINT1 *) pu1Buf, u4Len);\
    pu1Buf += u4Len;\
}

/* CRU related Macros */
#define BFD_CRU_GET_1_BYTE(pMsg, u4Offset, u1Value)\
   CRU_BUF_Copy_FromBufChain(pMsg,((UINT1 *) &u1Value), u4Offset, 1)

#define BFD_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}
#define BFD_CRU_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define BFD_CRU_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,\
                            u4_StringLength) \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset,\
                            u4_StringLength);\
}

#define BFD_CRU_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  LinearBuf = (UINT1) u1Value;\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 1);\
}

#define BFD_CRU_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 2);\
}

#define BFD_CRU_PUT_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 4);\
}

#define BFD_CRU_PUT_STRING(pBufChain,pu1_StringMemArea,u4Offset,\
                           u4_StringLength) \
{\
   CRU_BUF_Copy_OverBufChain(pBufChain, pu1_StringMemArea, u4Offset, u4_StringLength);\
}

#define BFD_CHECK_PATH_TYPE_NOT_IP(pBfdSessEntry) \
            ((pBfdSessEntry->MibObject.i4FsMIBfdSessMapType != BFD_PATH_TYPE_IPV4) && \
             (pBfdSessEntry->MibObject.i4FsMIBfdSessMapType != BFD_PATH_TYPE_IPV6))

/* To check whether this session is associated with dynamic session */
#define BFD_CHECK_SESS_DYNAMIC_CREATION(pBfdSessEntry) \
            ((pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType == BFD_OTHER) || \
            (pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType == BFD_VOLATILE))

/* To check whether this session is associated with dynamic session alone*/
#define BFD_CHECK_SESS_IS_DYNAMIC(pBfdSessEntry) \
            (pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType == BFD_VOLATILE)
/* Lock and Unlock with Flag */
#define BFD_FLAG_LOCK(flag) if(flag) { BFD_LOCK; }
#define BFD_FLAG_UNLOCK(flag) if(flag) { BFD_UNLOCK; }

#endif  /* __BFDMACRO_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file bfdmacro.h                      */
/*-----------------------------------------------------------------------*/

