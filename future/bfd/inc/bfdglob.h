/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdglob.h,v 1.6 2015/07/16 11:26:35 siva Exp $
 *
 * Description: This file contains declaration of global variables 
 *              of bfd module.
 *******************************************************************/

#ifndef __BFDGLOBAL_H__
#define __BFDGLOBAL_H__

tBfdGlobals gBfdGlobals;

/* RED Global Info */
tBfdRedGlobalInfo  gBfdRedGblInfo;

#ifdef BFD_HA_TEST_WANTED
/*  BFD Test Related variables */
tBfdTestGlobals gBfdTestGlobals;
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
tBfdLnxVrf  gBfdLnxVrfSock[SYS_DEF_MAX_NUM_CONTEXTS];
#endif
/* RM Dynamic Info List */
tDbTblDescriptor gBfdDynInfoList;

tDbDataDescInfo  gaBfdDynDataDescList[BFD_MAX_DYN_INFO_TYPE];

tRBTree gBfdRedTable;

tDbOffsetTemplate gaBfdOffsetTbl[] =
{{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdContextId) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry,u4FsMIStdBfdSessIndex) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdSessDiscriminator) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdSessRemoteDiscr) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessState) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessRemoteHeardFlag) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessDiag) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdSessNegotiatedInterval) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdSessNegotiatedEchoInterval) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdSessNegotiatedDetectMult) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIBfdSessAdminCtrlReq) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIBfdSessAdminCtrlErrReason) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessInterface) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessType) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessStorType) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessSrcAddrType) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessDstAddrType) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessSrcAddrLen) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, i4FsMIStdBfdSessDstAddrLen) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, au1FsMIStdBfdSessSrcAddr) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 256},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, au1FsMIStdBfdSessDstAddr) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 256},
{(FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, u4FsMIBfdSessRegisteredClients) -
        FSAP_OFFSETOF(tBfdMibFsMIStdBfdSessEntry, SessDbNode) - sizeof(tDbTblNode)), 4},
{-1, 0}};





/* Global variables for queue overflow */
/* Flag to indicate BFD queue overflow */
UINT1  gu1QueueOverflow = OSIX_FALSE;
/* Id of session for which hardware audit ongoing */
UINT2  gu2OverflowSessionId = BFD_DEF_SESS_ID;

/* Control Channel Types */
/* Specifies the Continuity Check for RAW Bfd */
UINT2  gu2BfdAchChnlTypeCcBfd  = BFD_CH_TYPE_DEF_CC_BFD;
/* Specifies the Continuity Verification for RAW Bfd */
UINT2  gu2BfdAchChnlTypeCvBfd  = BFD_CH_TYPE_DEF_CV_BFD;
/* Specifies the Continuity Check for BFD with IPv4 */
UINT2  gu2BfdAchChnlTypeCcIpv4 = BFD_CH_TYPE_DEF_CC_IPV4;
/* Specifies the Continuity Verification for BFD with IPv4 */
UINT2  gu2BfdAchChnlTypeCvIpv4 = BFD_CH_TYPE_DEF_CV_IPV4;
/* Specifies the Continuity Check for BFD with IPv6 */
UINT2  gu2BfdAchChnlTypeCcIpv6 = BFD_CH_TYPE_DEF_CC_IPV6;
/* Specifies the Continuity Verification for BFD with IPv6 */
UINT2  gu2BfdAchChnlTypeCvIpv6 = BFD_CH_TYPE_DEF_CV_IPV6;
#endif  /* __BFDGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file bfdglob.h                      */
/*-----------------------------------------------------------------------*/
