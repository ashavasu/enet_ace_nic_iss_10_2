/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdtdfsg.h,v 1.7 2015/07/16 11:26:35 siva Exp $
*
* Description: This file contains data structures defined for Bfd module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsMIStdBfdGlobalConfigTable */

typedef struct
{
 BOOL1  bFsMIStdBfdContextId;
 BOOL1  bFsMIStdBfdAdminStatus;
 BOOL1  bFsMIStdBfdSessNotificationsEnable;
 BOOL1  bFsMIBfdSystemControl;
 BOOL1  bFsMIBfdTraceLevel;
 BOOL1  bFsMIBfdTrapEnable;
 BOOL1  bFsMIBfdGblSessOperMode;
 BOOL1  bFsMIBfdGblDesiredMinTxIntvl;
 BOOL1  bFsMIBfdGblReqMinRxIntvl;
 BOOL1  bFsMIBfdGblDetectMult;
 BOOL1  bFsMIBfdGblSlowTxIntvl;
 BOOL1  bFsMIBfdClrGblStats;
 BOOL1  bFsMIBfdClrAllSessStats;
} tBfdIsSetFsMIStdBfdGlobalConfigTableEntry;


/* Structure used by Bfd protocol for FsMIStdBfdGlobalConfigTable */

typedef struct
{
 tRBNodeEmbd  FsMIStdBfdGlobalConfigTableNode;
 UINT4 u4FsMIStdBfdContextId;
 INT4 i4FsMIStdBfdAdminStatus;
 INT4 i4FsMIStdBfdSessNotificationsEnable;
 INT4 i4FsMIBfdSystemControl;
 INT4 i4FsMIBfdTraceLevel;
 INT4 i4FsMIBfdTrapEnable;
 UINT4 u4FsMIBfdGblDesiredMinTxIntvl;
 UINT4 u4FsMIBfdGblReqMinRxIntvl;
 UINT4 u4FsMIBfdGblDetectMult;
 UINT4 u4FsMIBfdGblSlowTxIntvl;
 INT4 i4FsMIBfdGblSessOperMode;
 UINT4 u4FsMIBfdMemAllocFailure;
 UINT4 u4FsMIBfdInputQOverFlows;
 INT4 i4FsMIBfdClrGblStats;
 INT4 i4FsMIBfdClrAllSessStats;
} tBfdMibFsMIStdBfdGlobalConfigTableEntry;
/* Structure used by CLI to indicate which 
 all objects to be set in FsMIStdBfdSessEntry */

typedef struct
{
 BOOL1  bFsMIStdBfdSessIndex;
 BOOL1  bFsMIStdBfdSessVersionNumber;
 BOOL1  bFsMIStdBfdSessType;
 BOOL1  bFsMIStdBfdSessDestinationUdpPort;
 BOOL1  bFsMIStdBfdSessSourceUdpPort;
 BOOL1  bFsMIStdBfdSessEchoSourceUdpPort;
 BOOL1  bFsMIStdBfdSessAdminStatus;
 BOOL1  bFsMIStdBfdSessOperMode;
 BOOL1  bFsMIStdBfdSessDemandModeDesiredFlag;
 BOOL1  bFsMIStdBfdSessControlPlaneIndepFlag;
 BOOL1  bFsMIStdBfdSessMultipointFlag;
 BOOL1  bFsMIStdBfdSessInterface;
 BOOL1  bFsMIStdBfdSessSrcAddrType;
 BOOL1  bFsMIStdBfdSessSrcAddr;
 BOOL1  bFsMIStdBfdSessDstAddrType;
 BOOL1  bFsMIStdBfdSessDstAddr;
 BOOL1  bFsMIStdBfdSessGTSM;
 BOOL1  bFsMIStdBfdSessGTSMTTL;
 BOOL1  bFsMIStdBfdSessDesiredMinTxInterval;
 BOOL1  bFsMIStdBfdSessReqMinRxInterval;
 BOOL1  bFsMIStdBfdSessReqMinEchoRxInterval;
 BOOL1  bFsMIStdBfdSessDetectMult;
 BOOL1  bFsMIStdBfdSessAuthPresFlag;
 BOOL1  bFsMIStdBfdSessAuthenticationType;
 BOOL1  bFsMIStdBfdSessAuthenticationKeyID;
 BOOL1  bFsMIStdBfdSessAuthenticationKey;
 BOOL1  bFsMIStdBfdSessStorType;
 BOOL1  bFsMIStdBfdSessRowStatus;
 BOOL1  bFsMIStdBfdContextId;
 BOOL1  bFsMIBfdSessRole;
 BOOL1  bFsMIBfdSessMode;
 BOOL1  bFsMIBfdSessRemoteDiscr;
 BOOL1  bFsMIBfdSessEXPValue;
 BOOL1  bFsMIBfdSessTmrNegotiate;
 BOOL1  bFsMIBfdSessOffld;
 BOOL1  bFsMIBfdSessEncapType;
 BOOL1  bFsMIBfdSessMapType;
 BOOL1  bFsMIBfdSessMapPointer;
 BOOL1  bFsMIBfdSessCardNumber;
 BOOL1  bFsMIBfdSessSlotNumber;
 BOOL1  bFsMIBfdClearStats;
} tBfdIsSetFsMIStdBfdSessEntry;


/* Structure used by Bfd protocol for FsMIStdBfdSessEntry */

typedef struct
{
 tRBNodeEmbd  FsMIStdBfdSessTableNode;
 UINT4 u4FsMIStdBfdSessVersionNumber;
 UINT4 u4FsMIStdBfdSessDestinationUdpPort;
 UINT4 u4FsMIStdBfdSessSourceUdpPort;
 UINT4 u4FsMIStdBfdSessEchoSourceUdpPort;
 UINT4 u4FsMIStdBfdSessGTSMTTL;
 UINT4 u4FsMIStdBfdSessDesiredMinTxInterval;
 UINT4 u4FsMIStdBfdSessReqMinRxInterval;
 UINT4 u4FsMIStdBfdSessReqMinEchoRxInterval;
 UINT4 u4FsMIStdBfdSessDetectMult;
 INT4 i4FsMIStdBfdSessAdminStatus;
 INT4 i4FsMIStdBfdSessOperMode;
 INT4 i4FsMIStdBfdSessDemandModeDesiredFlag;
 INT4 i4FsMIStdBfdSessControlPlaneIndepFlag;
 INT4 i4FsMIStdBfdSessMultipointFlag;
 INT4 i4FsMIStdBfdSessGTSM;
 INT4 i4FsMIStdBfdSessAuthPresFlag;
 INT4 i4FsMIStdBfdSessAuthenticationType;
 INT4 i4FsMIStdBfdSessAuthenticationKeyID;
 INT4 i4FsMIStdBfdSessRowStatus;
 INT4 i4FsMIStdBfdSessAuthenticationKeyLen;
 UINT1 au1FsMIStdBfdSessAuthenticationKey[256];
 UINT4 u4FsMIStdBfdSessPerfCtrlPktIn;
 UINT4 u4FsMIStdBfdSessPerfCtrlPktOut;
 UINT4 u4FsMIStdBfdSessPerfCtrlPktDrop;
 UINT4 u4FsMIStdBfdSessPerfCtrlPktDropLastTime;
 UINT4 u4FsMIStdBfdSessPerfEchoPktIn;
 UINT4 u4FsMIStdBfdSessPerfEchoPktOut;
 UINT4 u4FsMIStdBfdSessPerfEchoPktDrop;
 UINT4 u4FsMIStdBfdSessPerfEchoPktDropLastTime;
 UINT4 u4FsMIStdBfdSessUpTime;
 UINT4 u4FsMIStdBfdSessPerfLastSessDownTime;
 UINT4 u4FsMIStdBfdSessPerfSessUpCount;
 UINT4 u4FsMIStdBfdSessPerfDiscTime;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfCtrlPktInHC;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfCtrlPktOutHC;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfCtrlPktDropHC;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfEchoPktInHC;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfEchoPktOutHC;
 tSNMP_COUNTER64_TYPE u8FsMIStdBfdSessPerfEchoPktDropHC;
 INT4 i4FsMIStdBfdSessPerfLastCommLostDiag;
 UINT4 u4FsMIBfdSessRemoteDiscr;
 UINT4 u4FsMIBfdSessEXPValue;
 UINT4 u4FsMIBfdSessCardNumber;
 UINT4 u4FsMIBfdSessSlotNumber;
 UINT4 au4FsMIBfdSessMapPointer[256];
 INT4 i4FsMIBfdSessRole;
 INT4 i4FsMIBfdSessMode;
 INT4 i4FsMIBfdSessTmrNegotiate;
 INT4 i4FsMIBfdSessOffld;
 INT4 i4FsMIBfdSessEncapType;
 INT4 i4FsMIBfdSessMapType;
 INT4 i4FsMIBfdSessMapPointerLen;
 UINT4 u4FsMIBfdSessPerfCCPktIn;
 UINT4 u4FsMIBfdSessPerfCCPktOut;
 UINT4 u4FsMIBfdSessPerfCVPktIn;
 UINT4 u4FsMIBfdSessPerfCVPktOut;
 UINT4 u4FsMIBfdSessMisDefCount;
 UINT4 u4FsMIBfdSessLocDefCount;
 UINT4 u4FsMIBfdSessRdiInCount;
 UINT4 u4FsMIBfdSessRdiOutCount;
 INT4 i4FsMIBfdClearStats;
/* The dynamic entries that are to be synced should be added after the
 * SessDbNode. And both the halves should be padded accordingly */

 tDbTblNode    SessDbNode;           /* Data Base node to sync BFD dynamic
                                       * information.*/
 UINT4 u4FsMIStdBfdContextId;
 UINT4 u4FsMIStdBfdSessIndex;
 UINT4 u4FsMIStdBfdSessDiscriminator;
 UINT4 u4FsMIStdBfdSessRemoteDiscr;
 INT4 i4FsMIStdBfdSessState;
 INT4 i4FsMIStdBfdSessRemoteHeardFlag;
 INT4 i4FsMIStdBfdSessDiag;
 UINT4 u4FsMIStdBfdSessNegotiatedInterval;
 UINT4 u4FsMIStdBfdSessNegotiatedEchoInterval;
 UINT4 u4FsMIStdBfdSessNegotiatedDetectMult;
 INT4 i4FsMIBfdSessAdminCtrlReq;
 INT4 i4FsMIBfdSessAdminCtrlErrReason;
 INT4 i4FsMIStdBfdSessInterface;
 INT4 i4FsMIStdBfdSessType;
 INT4 i4FsMIStdBfdSessStorType;
 INT4 i4FsMIStdBfdSessSrcAddrType;
 INT4 i4FsMIStdBfdSessDstAddrType;
 INT4 i4FsMIStdBfdSessSrcAddrLen;
 INT4 i4FsMIStdBfdSessDstAddrLen;
 UINT1 au1FsMIStdBfdSessSrcAddr[256];
 UINT1 au1FsMIStdBfdSessDstAddr[256];
 UINT4 u4FsMIBfdSessRegisteredClients;
} tBfdMibFsMIStdBfdSessEntry;


/* Structure used by Bfd protocol for FsMIStdBfdSessDiscMapEntry */

typedef struct
{
 tRBNodeEmbd  FsMIStdBfdSessDiscMapTableNode;
 UINT4 u4FsMIStdBfdSessDiscMapIndex;
 UINT4 u4FsMIStdBfdContextId;
 UINT4 u4FsMIStdBfdSessDiscriminator;
} tBfdMibFsMIStdBfdSessDiscMapEntry;


/* Structure used by Bfd protocol for FsMIStdBfdSessIpMapEntry */

typedef struct
{
 tRBNodeEmbd  FsMIStdBfdSessIpMapTableNode;
 UINT4 u4FsMIStdBfdSessIpMapIndex;
 UINT4 u4FsMIStdBfdContextId;
 INT4 i4FsMIStdBfdSessInterface;
 INT4 i4FsMIStdBfdSessSrcAddrType;
 INT4 i4FsMIStdBfdSessDstAddrType;
 INT4 i4FsMIStdBfdSessSrcAddrLen;
 INT4 i4FsMIStdBfdSessDstAddrLen;
 UINT1 au1FsMIStdBfdSessSrcAddr[256];
 UINT1 au1FsMIStdBfdSessDstAddr[256];
} tBfdMibFsMIStdBfdSessIpMapEntry;
