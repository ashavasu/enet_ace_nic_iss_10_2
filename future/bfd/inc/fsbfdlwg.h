/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbfdlwg.h,v 1.3 2013/07/13 12:43:25 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdSystemControl ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdSystemControl ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdSystemControl ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdTraceLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdTraceLevel ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdTrapEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdTrapEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdTrapEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdTrapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdGblSessOperMode ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdGblSessOperMode ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdGblSessOperMode ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdGblSessOperMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 * , UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdGblReqMinRxIntvl ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdGblReqMinRxIntvl ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdGblReqMinRxIntvl ARG_LIST((UINT4 * , UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdGblReqMinRxIntvl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdGblDetectMult ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdGblDetectMult ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdGblDetectMult ARG_LIST((UINT4 * , UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdGblDetectMult ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdGblSlowTxIntvl ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdGblSlowTxIntvl ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdGblSlowTxIntvl ARG_LIST((UINT4 * , UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdGblSlowTxIntvl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdMemAllocFailure ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdInputQOverFlows ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdClrGblStats ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdClrGblStats ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdClrGblStats ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdClrGblStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdClrAllSessStats ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdClrAllSessStats ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdClrAllSessStats ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdClrAllSessStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsBfdSessionTable. */
INT1
nmhValidateIndexInstanceFsBfdSessionTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsBfdSessionTable  */

INT1
nmhGetFirstIndexFsBfdSessionTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBfdSessionTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdSessRole ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessRemoteDiscr ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessEXPValue ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessTmrNegotiate ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessOffld ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessEncapType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessAdminCtrlReq ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessAdminCtrlErrReason ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessMapType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBfdSessMapPointer ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsBfdSessCardNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessSlotNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessRegisteredClients ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdSessRole ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessRemoteDiscr ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsBfdSessEXPValue ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsBfdSessTmrNegotiate ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessOffld ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessEncapType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessMapType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBfdSessMapPointer ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsBfdSessCardNumber ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsBfdSessSlotNumber ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdSessRole ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessMode ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessRemoteDiscr ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsBfdSessEXPValue ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsBfdSessTmrNegotiate ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessOffld ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessEncapType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessMapType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsBfdSessMapPointer ARG_LIST((UINT4 * , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsBfdSessCardNumber ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsBfdSessSlotNumber ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdSessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsBfdSessPerfTable. */
INT1
nmhValidateIndexInstanceFsBfdSessPerfTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsBfdSessPerfTable  */

INT1
nmhGetFirstIndexFsBfdSessPerfTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBfdSessPerfTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBfdSessPerfCCPktIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessPerfCCPktOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessPerfCVPktIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessPerfCVPktOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessMisDefCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessLocDefCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessRdiInCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdSessRdiOutCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBfdClearStats ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBfdClearStats ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBfdClearStats ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBfdSessPerfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
