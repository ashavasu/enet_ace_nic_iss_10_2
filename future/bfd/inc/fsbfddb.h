/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbfddb.h,v 1.4 2013/07/13 12:43:25 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSBFDDB_H
#define _FSBFDDB_H

UINT1 FsBfdSessionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsBfdSessPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsbfd [] ={1,3,6,1,4,1,29601,2,53};
tSNMP_OID_TYPE fsbfdOID = {9, fsbfd};


UINT4 FsBfdSystemControl [ ] ={1,3,6,1,4,1,29601,2,53,1,1};
UINT4 FsBfdTraceLevel [ ] ={1,3,6,1,4,1,29601,2,53,1,2};
UINT4 FsBfdTrapEnable [ ] ={1,3,6,1,4,1,29601,2,53,1,3};
UINT4 FsBfdGblSessOperMode [ ] ={1,3,6,1,4,1,29601,2,53,1,4};
UINT4 FsBfdGblDesiredMinTxIntvl [ ] ={1,3,6,1,4,1,29601,2,53,1,5};
UINT4 FsBfdGblReqMinRxIntvl [ ] ={1,3,6,1,4,1,29601,2,53,1,6};
UINT4 FsBfdGblDetectMult [ ] ={1,3,6,1,4,1,29601,2,53,1,7};
UINT4 FsBfdGblSlowTxIntvl [ ] ={1,3,6,1,4,1,29601,2,53,1,8};
UINT4 FsBfdMemAllocFailure [ ] ={1,3,6,1,4,1,29601,2,53,1,9};
UINT4 FsBfdInputQOverFlows [ ] ={1,3,6,1,4,1,29601,2,53,1,10};
UINT4 FsBfdClrGblStats [ ] ={1,3,6,1,4,1,29601,2,53,1,11};
UINT4 FsBfdClrAllSessStats [ ] ={1,3,6,1,4,1,29601,2,53,1,13};
UINT4 FsBfdSessRole [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,1};
UINT4 FsBfdSessMode [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,2};
UINT4 FsBfdSessRemoteDiscr [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,3};
UINT4 FsBfdSessEXPValue [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,4};
UINT4 FsBfdSessTmrNegotiate [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,5};
UINT4 FsBfdSessOffld [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,6};
UINT4 FsBfdSessEncapType [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,7};
UINT4 FsBfdSessAdminCtrlReq [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,8};
UINT4 FsBfdSessAdminCtrlErrReason [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,9};
UINT4 FsBfdSessMapType [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,10};
UINT4 FsBfdSessMapPointer [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,11};
UINT4 FsBfdSessCardNumber [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,12};
UINT4 FsBfdSessSlotNumber [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,13};
UINT4 FsBfdSessRegisteredClients [ ] ={1,3,6,1,4,1,29601,2,53,2,1,1,14};
UINT4 FsBfdSessPerfCCPktIn [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,1};
UINT4 FsBfdSessPerfCCPktOut [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,2};
UINT4 FsBfdSessPerfCVPktIn [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,3};
UINT4 FsBfdSessPerfCVPktOut [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,4};
UINT4 FsBfdSessMisDefCount [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,5};
UINT4 FsBfdSessLocDefCount [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,6};
UINT4 FsBfdSessRdiInCount [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,7};
UINT4 FsBfdSessRdiOutCount [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,8};
UINT4 FsBfdClearStats [ ] ={1,3,6,1,4,1,29601,2,53,2,2,1,9};




tMbDbEntry fsbfdMibEntry[]= {

{{11,FsBfdSystemControl}, NULL, FsBfdSystemControlGet, FsBfdSystemControlSet, FsBfdSystemControlTest, FsBfdSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsBfdTraceLevel}, NULL, FsBfdTraceLevelGet, FsBfdTraceLevelSet, FsBfdTraceLevelTest, FsBfdTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2048"},

{{11,FsBfdTrapEnable}, NULL, FsBfdTrapEnableGet, FsBfdTrapEnableSet, FsBfdTrapEnableTest, FsBfdTrapEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsBfdGblSessOperMode}, NULL, FsBfdGblSessOperModeGet, FsBfdGblSessOperModeSet, FsBfdGblSessOperModeTest, FsBfdGblSessOperModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsBfdGblDesiredMinTxIntvl}, NULL, FsBfdGblDesiredMinTxIntvlGet, FsBfdGblDesiredMinTxIntvlSet, FsBfdGblDesiredMinTxIntvlTest, FsBfdGblDesiredMinTxIntvlDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000000"},

{{11,FsBfdGblReqMinRxIntvl}, NULL, FsBfdGblReqMinRxIntvlGet, FsBfdGblReqMinRxIntvlSet, FsBfdGblReqMinRxIntvlTest, FsBfdGblReqMinRxIntvlDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000000"},

{{11,FsBfdGblDetectMult}, NULL, FsBfdGblDetectMultGet, FsBfdGblDetectMultSet, FsBfdGblDetectMultTest, FsBfdGblDetectMultDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,FsBfdGblSlowTxIntvl}, NULL, FsBfdGblSlowTxIntvlGet, FsBfdGblSlowTxIntvlSet, FsBfdGblSlowTxIntvlTest, FsBfdGblSlowTxIntvlDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000000"},

{{11,FsBfdMemAllocFailure}, NULL, FsBfdMemAllocFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsBfdInputQOverFlows}, NULL, FsBfdInputQOverFlowsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsBfdClrGblStats}, NULL, FsBfdClrGblStatsGet, FsBfdClrGblStatsSet, FsBfdClrGblStatsTest, FsBfdClrGblStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsBfdClrAllSessStats}, NULL, FsBfdClrAllSessStatsGet, FsBfdClrAllSessStatsSet, FsBfdClrAllSessStatsTest, FsBfdClrAllSessStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsBfdSessRole}, GetNextIndexFsBfdSessionTable, FsBfdSessRoleGet, FsBfdSessRoleSet, FsBfdSessRoleTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, "1"},

{{13,FsBfdSessMode}, GetNextIndexFsBfdSessionTable, FsBfdSessModeGet, FsBfdSessModeSet, FsBfdSessModeTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, "1"},

{{13,FsBfdSessRemoteDiscr}, GetNextIndexFsBfdSessionTable, FsBfdSessRemoteDiscrGet, FsBfdSessRemoteDiscrSet, FsBfdSessRemoteDiscrTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessEXPValue}, GetNextIndexFsBfdSessionTable, FsBfdSessEXPValueGet, FsBfdSessEXPValueSet, FsBfdSessEXPValueTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, "0"},

{{13,FsBfdSessTmrNegotiate}, GetNextIndexFsBfdSessionTable, FsBfdSessTmrNegotiateGet, FsBfdSessTmrNegotiateSet, FsBfdSessTmrNegotiateTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, "1"},

{{13,FsBfdSessOffld}, GetNextIndexFsBfdSessionTable, FsBfdSessOffldGet, FsBfdSessOffldSet, FsBfdSessOffldTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, "2"},

{{13,FsBfdSessEncapType}, GetNextIndexFsBfdSessionTable, FsBfdSessEncapTypeGet, FsBfdSessEncapTypeSet, FsBfdSessEncapTypeTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessAdminCtrlReq}, GetNextIndexFsBfdSessionTable, FsBfdSessAdminCtrlReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessAdminCtrlErrReason}, GetNextIndexFsBfdSessionTable, FsBfdSessAdminCtrlErrReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessMapType}, GetNextIndexFsBfdSessionTable, FsBfdSessMapTypeGet, FsBfdSessMapTypeSet, FsBfdSessMapTypeTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessMapPointer}, GetNextIndexFsBfdSessionTable, FsBfdSessMapPointerGet, FsBfdSessMapPointerSet, FsBfdSessMapPointerTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessCardNumber}, GetNextIndexFsBfdSessionTable, FsBfdSessCardNumberGet, FsBfdSessCardNumberSet, FsBfdSessCardNumberTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessSlotNumber}, GetNextIndexFsBfdSessionTable, FsBfdSessSlotNumberGet, FsBfdSessSlotNumberSet, FsBfdSessSlotNumberTest, FsBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessRegisteredClients}, GetNextIndexFsBfdSessionTable, FsBfdSessRegisteredClientsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsBfdSessionTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessPerfCCPktIn}, GetNextIndexFsBfdSessPerfTable, FsBfdSessPerfCCPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessPerfCCPktOut}, GetNextIndexFsBfdSessPerfTable, FsBfdSessPerfCCPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessPerfCVPktIn}, GetNextIndexFsBfdSessPerfTable, FsBfdSessPerfCVPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessPerfCVPktOut}, GetNextIndexFsBfdSessPerfTable, FsBfdSessPerfCVPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessMisDefCount}, GetNextIndexFsBfdSessPerfTable, FsBfdSessMisDefCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessLocDefCount}, GetNextIndexFsBfdSessPerfTable, FsBfdSessLocDefCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessRdiInCount}, GetNextIndexFsBfdSessPerfTable, FsBfdSessRdiInCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdSessRdiOutCount}, GetNextIndexFsBfdSessPerfTable, FsBfdSessRdiOutCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsBfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,FsBfdClearStats}, GetNextIndexFsBfdSessPerfTable, FsBfdClearStatsGet, FsBfdClearStatsSet, FsBfdClearStatsTest, FsBfdSessPerfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBfdSessPerfTableINDEX, 1, 0, 0, "2"},
};
tMibData fsbfdEntry = { 35, fsbfdMibEntry };

#endif /* _FSBFDDB_H */

