/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdtdfs.h,v 1.35 2015/09/11 09:50:42 siva Exp $
*
* Description: This file contains type definitions for Bfd module.
*********************************************************************/

#ifndef __BFDTDFS_H__
#define __BFDTDFS_H__

typedef enum
{
    BFD_INVALID_SESS_IDX,
    BFD_VALID_SESS_IDX
}tBfdSessIndex;

typedef enum
{
    BFD_TRACE_ENABLE = 1,
    BFD_TRACE_DISABLE
}tBfdTrace;

typedef enum
{
    BFD_SESS_ENABLE = 1,
    BFD_SESS_DISABLE
}tBfdAdminStatus;

typedef enum
{
    BFD_SESS_ADMIN_STOP = 1,
    BFD_SESS_ADMIN_START
}tBfdSessAdminStatus;

enum
{
    BFD_ENCAP_MPLS_IP = 1,
    BFD_ENCAP_MPLS_ACH_HDR,
    BFD_ENCAP_MPLS_ACH_IP,
};

/* request types for MPLS exit function */
typedef enum
{
    BFD_REQ_ENQUEUE_ARP_REQUEST,
    BFD_REQ_ARP_RESOLVE,
    BFD_REQ_IP_PORT_FROM_IF_INDEX,
    BFD_REQ_EGR_IF_INDEX_NH_FROM_DEST_IP,
    BFD_REQ_CFA_IFINDEX_FROM_PORT,
    BFD_REQ_GET_L3VLAN_IF_FROM_TNL_IF,
    BFD_REQ_GET_IF_MAC_ADDR,
    BFD_REQ_GET_IF_IP_FROM_IF_INDEX,
    BFD_REQ_MPLS_GET_SERVICE_POINTER_OID,
    BFD_REQ_MPLS_GET_REV_PATH_INFO_FROM_FWD_PATH_INFO,
    BFD_REQ_MPLS_GET_PATH_INFO_FROM_INLBL_INFO,
    BFD_REQ_MPLS_GET_NODE_ID,
    BFD_REQ_MPLS_GET_LSP_INFO,
    BFD_REQ_MPLS_GET_PW_INFO,
    BFD_REQ_MPLS_GET_TUNNEL_INFO,
    BFD_REQ_MPLS_OAM_GET_MEG_INFO,
    BFD_REQ_MPLS_PACKET_HANDLE_FROM_APP,
    BFD_REQ_MPLS_OAM_UPDATE_SESSION_PARAMS,
    BFD_REQ_MPLS_OAM_HANDLE_PATH_STATUS_CHANGE,
    BFD_REQ_VCM_IS_CONTEXT_VALID,
    BFD_REQ_VCM_GET_CONTEXT_NAME,
    BFD_REQ_VCM_GET_SYS_MODE,
    BFD_REQ_IF_INFO_FROM_IF_INDEX,
#ifdef MBSM_WANTED
    BFD_REQ_MBSM_PARAMS,
#endif
    BFD_REQ_FM_SEND_TRAP,
    BFD_REQ_FM_SYSLOG_MSG,
    BFD_REQ_LSPP_BOOTSTRAP_MSG,
    BFD_REQ_LSPP_BOOTSTRAP_REPLY,
    BFD_REQ_VLAN_FROM_IF,
    BFD_REQ_REG_WITH_VCM,
    BFD_REQ_VCM_GET_SYS_MODE_EXT,
    BFD_REQ_CFA_IFINDEX_FROM_IFNAME_AND_IFNUM,
    BFD_REQ_NETIP_IF_INFO,
    BFD_REQ_CFA_VALIDATE_IF_INDEX,
    BFD_REQ_CLIENT_HANDLE_IP_PATH_STATUS_CHANGE
}tExtReqType;

typedef enum
{
    BFD_OFF_GET_SESS_PARAMS,
    BFD_OFF_GET_SESS_STATS,
    BFD_OFF_CLEAR_SESS_STATS,
    BFD_OFF_CLEAR_ALL_SESS_STATS
}tBfdOffReq;



typedef struct EventLogNotify 
{
    UINT4 u4TraceLevel;
    UINT4   u4SyslogLevel;
    UINT1 au1LogMsg [BFD_MAX_MSG_LEN];
    UINT4   u4TrapType;
}tIssEventLogNotify;

typedef enum {
    BFD_PEER_ADMIN_DOWN=0,
    BFD_PEER_DOWN,
    BFD_PEER_INIT,
    BFD_PEER_UP,
    BFD_LOCAL_ADMINDOWN_EVENT,
    BFD_LOCAL_ADMINUP_EVENT,
    BFD_LOCAL_DETTMREXP_EVENT,
    BFD_PATHSTCHG,
    BFD_ADDR_COMP_TMR_EXP_EVENT,
    BFD_MAX_SEM_EVENTS
}tBfdSemEvents;

typedef enum 
{
    BFD_SEM_REQ_RX_PKT,
    BFD_SEM_REQ_PATH_DOWN,
    BFD_SEM_REQ_OFFLD_EVNT,
    BFD_SEM_REQ_TMR_EXP,
    BFD_SEM_REQ_ADMIN_DOWN,
    BFD_SEM_REQ_ADMIN_UP
}tBfdSemReqType;


typedef struct {
    UINT4 u4SyslogLevel;
    UINT1 *pSyslogMsg;
}tBfdSysLogParams;

/* This structure is the output for the Exit Funtion of BFD */
typedef union
{
    struct
    {
        INT1   ai1HwAddr[MAC_ADDR_LEN]; /* Mac addr from arp */
        UINT1  u1EncapType;         /* Encapsulation */
        UINT1  au1Pad;         /* pad*/
    }ArpOutParams;
    tBfdErrCode        eBfdErrCode;
#ifdef MBSM_WANTED 
    tBfdMbsmParams     BfdMbsmParams;
#endif
    tMplsApiOutInfo    MplsApiOutInfo;
    tLsppRespParams    BfdLsppRespParams; /* Response from LSP Ping */
    tIpConfigInfo      IpInfo;   /* To get Interface Addr from Ifindex */
    tNetIpv4RtInfo      NetIpRtInfo; /* to get ifindex from dest ip */
    tNetIpv4IfInfo     NetIpIfInfo; /* to get if-info form ifindex */
    tIp6Addr           NetIpv6Addr;   /* Ipv6 address from the if index */
    tCfaIfInfo          CfaIfInfo; /* CfaGetIfInfo */
    UINT4              u4L3VlanIf;    /* VLAN From MPLS Tunnel */
    UINT4              u4Port;    /* IPPORT from ifindex */
    UINT4              u4VcNum; 
    UINT2              u2Vlan; /* to get vlan from ifindex */
    UINT1              au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1              au1HwAddr[MAC_ADDR_LEN]; /* Mac addr from If Index */
}tBfdExtOutParams;

/* This structure is the input for the Exit funtion of BFD */
typedef struct
{
    tExtReqType eExtReqType;
    union
    {
        tLsppReqParams    BfdLsppReqParams; /* To interact with LSP Ping */
        tMplsApiInInfo    InMplsApiInfo; /* To get info from MPLS Module */
        struct
        {
            UINT2       u2TnlIfIndex;
            BOOLEAN     bLockReq;
            UINT1       u1Pad;
        }CfaReqParams;              /* L3IPVLAN for MPLS TUNNEL interface */
        struct
        {
            UINT1         au1CxtName[VCM_ALIAS_MAX_LEN];
            UINT1         au1IfName[MAX_IFNAME_SIZE];
            INT1          ai1SlotAndPort[CFA_MAX_PORT_NAME_LENGTH];
            UINT1         u1AddrType;
        }IpReqParams;
        struct
        {
            tBfdClientNbrIpPathInfo BfdClientNbrIpPathInfo;
            INT1    (*pBfdNotifyPathStatusChange [BFD_MAX_CLIENTS -1]) (UINT4, tBfdClientNbrIpPathInfo *);
        }BfdClientPathStatusInfo;

        tArpQMsg          BfdRecvdQMsg; /* For Arp Resolve */
        tRtInfoQueryMsg     RtQuery; /* For getting Eggr inf for Dest Ip */
        tFmFaultMsg       FmFaultMsg; /* For sending trap */
        tBfdSysLogParams  BfdSyslogMsg; /* For sending syslog */
        tVcmRegInfo         VcmRegInfo; /* For VCM registration */
        UINT4             u4IpAddr; /* for Mac request */
        UINT4             u4IfIndex;  /*For Mac or IpPort or vlan request */
#ifdef MBSM_WANTED
        UINT4             u4SessionId;  /*For requesting Slot and Card Num */
#endif
        UINT1             au1Alias[VCM_ALIAS_MAX_LEN];
    }unReqParams;
    UINT4 u4ContextId;
#define LsppReqParams unReqParams.BfdLsppReqParams
#define InApiInfo unReqParams.InMplsApiInfo
#define u4IfIdx unReqParams.u4IfIndex
#define u2IfTnlIdx unReqParams.CfaReqParams.u2TnlIfIndex
#define bLockRequired unReqParams.CfaReqParams.bLockReq
#define IpAddress unReqParams.u4IpAddr
#define RecvdQMsg unReqParams.BfdRecvdQMsg
#define BfdRtInfoQueryMsg unReqParams.RtQuery
#define CxtName unReqParams.IpReqParams.au1CxtName
#define IpIfName unReqParams.IpReqParams.au1IfName
#define IpSlotAndPort unReqParams.IpReqParams.ai1SlotAndPort
#define IpAddrType unReqParams.IpReqParams.u1AddrType
#define NbrPathInfo unReqParams.BfdClientPathStatusInfo.BfdClientNbrIpPathInfo
#define NotifyPathStatusChange unReqParams.BfdClientPathStatusInfo.pBfdNotifyPathStatusChange
}tBfdExtInParams;




/* This structure contains the information to parse the BFD session 
 * information from the received peer */
typedef struct
{
    tBfdRxCtrlPktParams BfdPktParams;
    UINT4               u4BfdSessDiscr; /* Local Discriminator */
    UINT4               u4BfdSessRemoteDiscr; /* Remote Discriminator */
    UINT4               u4BfdEchoRxIntrvl; /* Req Min Rx Echo Interval */
    UINT1               au1BfdPasswd[BFD_MAX_PASSWD_LEN]; /* Authentication
                                                             Password */
    UINT1               u1BfdAuthType;  /* Authentication Type */
    UINT1               u1BfdAuthLen;   /* Authentication Length */
    UINT1               u1BfdAuthKeyId; /* Authentication Key ID */
    UINT1               u1BfdPktLen;   /* BFD Packet length */
    BOOL1               bBfdAuthBit;   /* Authentication bit in the field */
    BOOL1               bBfdMBit;      /* Multipoint bit in the field */
    UINT1               au1Pad[2];
}tBfdParseRxPkt;

/*
 * This defines the structure of an UDP header.
 * The header is extracted on the arrival of each message.
 */
typedef struct
{
    UINT2     u2SrcUdpPort;
    UINT2     u2DestUdpPort;
    UINT2     u2Len;
    UINT2     u2Cksum;
} tBfdUdp;

typedef struct
{
    UINT4               u4Src;         /* Source address */
    UINT4               u4Dest;        /* Destination address */
    UINT1               u1Zero;        /* Fill ZERO for 8 bits*/
    UINT1               u1Proto;       /* Protocol */
    UINT2               u2UdpLen;
} t_UDP_IP_Pseudo_Hdr;


typedef struct BfdPktInfo
{
    tBfdPathType          PathType;
    tBfdParseRxPkt        BfdCtrlPacket;
    tMplsLblInfo          MplsHdr[MPLS_MAX_LABEL_STACK];
    tIpAddr               PeerAddr;
    tMplsAchTlv           MplsAchTlv;
    UINT1                *pau1RxPktBuf;
    UINT4                 u4PktLen;
    UINT4                 u4IngrIf;
    UINT4                 u4LableStack;
    UINT2                 u2MepTlvType;
    UINT2                 u2ChannelType;
    UINT1                 u1EncapType;
    UINT1                 u1RecvdCc;
    BOOL1                 bCvValidationFail;
    UINT1                 au1Pad[1];
}tBfdPktInfo;

typedef struct
{
    tMplsMegApiInfo MplsMegApiInfo;
    tTeTnlApiInfo MplsTeTnlApiInfo;
    tPwApiInfo MplsPwApiInfo;
    tNonTeApiInfo MplsNonTeApiInfo;
    UINT2 u2PathFilled;   /* Bitmap indicating filled paths */
    UINT1  u1PathStatus;
    BOOL1 bRevPathFetchFail;   /* Indicates reverse path fetch fail
                                * if the tunnel is tail */
#define BFD_CHECK_PATH_PW_AVAIL   0x1
#define BFD_CHECK_PATH_ME_AVAIL   0x2
#define BFD_CHECK_PATH_TE_AVAIL   0x4
#define BFD_CHECK_PATH_NONTE_AVAIL 0x8
}tBfdMplsPathInfo;

/* This structure is used to store the BFD Control packet for the session */
typedef struct
{
    UINT4 u4PacketLen;
    UINT1 au1PacketBuf[BFD_MAX_CTRL_PKT_LEN];
    UINT1 au1Pad[3];
}tBfdCntlPktBuf;

/* BFD parameters for received control packet */
typedef struct
{
    tIpAddr  BfdPeerIpAddress;       /* IP Address of the peer */
    tBfdDiagnostic      eBfdRxPktDiag;
    UINT1               u1BfdRxPktSta;            /* Sta field received */
    UINT1               au1Pad[3]; /*Pad*/
}tBfdSessPeerParams;



/*structure for all scalars in the mib*/
typedef struct
{
 tRBTree   FsMIStdBfdGlobalConfigTable;
 tRBTree   FsMIStdBfdSessTable;
 tRBTree   FsMIStdBfdSessDiscMapTable;
 tRBTree   FsMIStdBfdSessIpMapTable;
 INT4      i4FsMIStdBfdContextNameLen;
        UINT1     au1FsMIStdBfdContextName[VCM_ALIAS_MAX_LEN];
} tBfdGlbMib;



typedef struct
{
 tBfdMibFsMIStdBfdGlobalConfigTableEntry       MibObject;
    UINT1                         au1ContextName[VCM_ALIAS_MAX_LEN];
} tBfdFsMIStdBfdGlobalConfigTableEntry;


typedef struct
{
    tBfdMibFsMIStdBfdSessEntry         MibObject;
    tBfdSessPathParams                 BfdSessPathParams;
    tBfdTmr                            TxTimer;
    tBfdTmr                            DetectTimer;
 tBfdTmr          BfdAddrCompTmr;
    tBfdSessPeerParams                 LastRxCtrlPktParams;
    tBfdCntlPktBuf                     BfdCntlPktBuf;
    tBfdFsMIStdBfdGlobalConfigTableEntry*   pContextInfo;
    tBfdHwhandle                       BfdHwhandle;
    tBfdClientParams                   aBfdClientParams [BFD_MAX_CLIENTS - 1];
    UINT4                              u4NegRemoteTxTimeIntrvl;
    UINT1                              u1BfdSessionPathEnd;
    UINT1                              u1BfdSessParamAllowChg;
    UINT1                              u1EncapType;
    BOOL1                              bPollRunning;
    BOOL1                              bPollPending;
    BOOL1                              bDynamicParamsLearned;
    BOOL1                              bMisConnectDefect;
    BOOL1                              bToTrigLsppBtStrap;
    BOOL1                              bRemDiscrStaticConf;
    BOOL1                              bInformedExtModule;
    UINT1                              u1BfdClientsCount;
    UINT1                              au1Pad[1];
 #ifdef BFD_HA_TEST_WANTED 
 INT4          i4BfdTestSessPrevTmr; /*  BFD_TRANSMIT_SLOW_TMR or BFD_TRANSMIT_FAST_TMR */
 INT4          i4BfdTestSessCurrTmr;
 #endif
} tBfdFsMIStdBfdSessEntry;


typedef struct
{
 tBfdMibFsMIStdBfdSessDiscMapEntry       MibObject;
} tBfdFsMIStdBfdSessDiscMapEntry;


typedef struct
{
 tBfdMibFsMIStdBfdSessIpMapEntry       MibObject;
} tBfdFsMIStdBfdSessIpMapEntry;


typedef struct BFD_GLOBALS {
    tTimerListId        bfdTmrLst;
    tOsixTaskId         bfdTaskId;
    UINT1               au1TaskSemName[8];
    tOsixSemId          bfdTaskSemId;
    tOsixQId            bfdQueId;
    tBfdGlbMib          BfdGlbMib;
    UINT4               u4SysLogId;
    UINT4               u4BfdTrc;
    UINT4               u4BfdTrcLvl; 
    INT4                i4BfdSockId;
    INT4                i4BfdTxSockId;
    INT4                i4BfdSingleHopSockId;
    INT4                i4Bfdv6SockId;
    INT4                i4Bfdv6TxSockId;
    INT4                i4Bfdv6SingleHopSockId;
    UINT1               u1IsBfdInitialized;
    UINT1               u1Pad[3];
} tBfdGlobals;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
typedef struct {
    INT4                i4BfdSockId;
    INT4                i4BfdTxSockId;
    INT4                i4BfdSingleHopSockId;
    INT4                i4Bfdv6SockId;
    INT4                i4Bfdv6TxSockId;
    INT4                i4Bfdv6SingleHopSockId;
    
}tBfdLnxVrf;
#endif
typedef struct _BfdRedGlobalInfo {
    UINT1   u1BulkUpdStatus;  /* bulk update status
                               * BFD_HA_UPD_NOT_STARTED 0
                               * BFD_HA_UPD_COMPLETED 1
                               * BFD_HA_UPD_IN_PROGRESS 2,
                               * BFD_HA_UPD_ABORTED 3 */

    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
}tBfdRedGlobalInfo;

typedef struct _BfdRedTable {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    UINT4        u4FsMIStdBfdContextId;
    UINT4        u4FsMIStdBfdSessIndex;
    UINT4        u4FsMIStdBfdSessDiscriminator;
    UINT4        u4FsMIStdBfdSessRemoteDiscr;
    INT4         i4FsMIStdBfdSessState;
    INT4         i4FsMIStdBfdSessRemoteHeardFlag;
    INT4         i4FsMIStdBfdSessDiag;
    UINT4        u4FsMIStdBfdSessNegotiatedInterval;
    UINT4        u4FsMIStdBfdSessNegotiatedEchoInterval;
    UINT4        u4FsMIStdBfdSessNegotiatedDetectMult;
    INT4         i4FsMIBfdSessAdminCtrlReq;
    INT4         i4FsMIBfdSessAdminCtrlErrReason;
    INT4 i4FsMIStdBfdSessInterface;
    INT4 i4FsMIStdBfdSessType;
    INT4 i4FsMIStdBfdSessStorType;
    INT4 i4FsMIStdBfdSessSrcAddrType;
    INT4 i4FsMIStdBfdSessDstAddrType;
 INT4 i4FsMIStdBfdSessSrcAddrLen;
 INT4 i4FsMIStdBfdSessDstAddrLen;
    UINT1 au1FsMIStdBfdSessSrcAddr[256];
    UINT1 au1FsMIStdBfdSessDstAddr[256];
    UINT4 u4FsMIBfdSessRegisteredClients;

}tBfdRedTable;


#ifdef BFD_HA_TEST_WANTED
typedef struct _sBfdTestGlobals
{
 UINT4 u4AbortBulkUpdate;   /* To abort Dynamic bulk update */
 UINT4 u4OffLoadUpCountHW;    
 UINT4 u4OffLoadDownCountHW;
 UINT4 u4OffLoadUpCountSW;
 UINT4 u4OffLoadDownCountSW;
 UINT4 u4NonOffUpCount;
 UINT4 u4NonOffDownCount;
}tBfdTestGlobals;
#endif  /* BFD_HA_TEST_WANTED */

#endif  /* __BFDTDFS_H__ */
/*-----------------------------------------------------------------------*/


