/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbfdb.h,v 1.4 2013/07/13 12:43:25 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPBFDB_H
#define _FSMPBFDB_H

UINT1 FsMIBfdSystemConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBfdGblSessionConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBfdStatisticsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBfdSessionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBfdSessPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmpbf [] ={1,3,6,1,4,1,29601,2,55};
tSNMP_OID_TYPE fsmpbfOID = {9, fsmpbf};


UINT4 FsMIBfdSystemControl [ ] ={1,3,6,1,4,1,29601,2,55,1,1,1,1};
UINT4 FsMIBfdTraceLevel [ ] ={1,3,6,1,4,1,29601,2,55,1,1,1,2};
UINT4 FsMIBfdTrapEnable [ ] ={1,3,6,1,4,1,29601,2,55,1,1,1,3};
UINT4 FsMIBfdGblSessOperMode [ ] ={1,3,6,1,4,1,29601,2,55,1,2,1,1};
UINT4 FsMIBfdGblDesiredMinTxIntvl [ ] ={1,3,6,1,4,1,29601,2,55,1,2,1,2};
UINT4 FsMIBfdGblReqMinRxIntvl [ ] ={1,3,6,1,4,1,29601,2,55,1,2,1,3};
UINT4 FsMIBfdGblDetectMult [ ] ={1,3,6,1,4,1,29601,2,55,1,2,1,4};
UINT4 FsMIBfdGblSlowTxIntvl [ ] ={1,3,6,1,4,1,29601,2,55,1,2,1,5};
UINT4 FsMIBfdMemAllocFailure [ ] ={1,3,6,1,4,1,29601,2,55,1,3,1,1};
UINT4 FsMIBfdInputQOverFlows [ ] ={1,3,6,1,4,1,29601,2,55,1,3,1,2};
UINT4 FsMIBfdClrGblStats [ ] ={1,3,6,1,4,1,29601,2,55,1,3,1,3};
UINT4 FsMIBfdClrAllSessStats [ ] ={1,3,6,1,4,1,29601,2,55,1,3,1,4};
UINT4 FsMIBfdSessRole [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,1};
UINT4 FsMIBfdSessMode [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,2};
UINT4 FsMIBfdSessRemoteDiscr [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,3};
UINT4 FsMIBfdSessEXPValue [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,4};
UINT4 FsMIBfdSessTmrNegotiate [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,5};
UINT4 FsMIBfdSessOffld [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,6};
UINT4 FsMIBfdSessEncapType [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,7};
UINT4 FsMIBfdSessAdminCtrlReq [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,8};
UINT4 FsMIBfdSessAdminCtrlErrReason [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,9};
UINT4 FsMIBfdSessMapType [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,10};
UINT4 FsMIBfdSessMapPointer [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,11};
UINT4 FsMIBfdSessCardNumber [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,12};
UINT4 FsMIBfdSessSlotNumber [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,13};
UINT4 FsMIBfdSessRegisteredClients [ ] ={1,3,6,1,4,1,29601,2,55,2,1,1,14};
UINT4 FsMIBfdSessPerfCCPktIn [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,1};
UINT4 FsMIBfdSessPerfCCPktOut [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,2};
UINT4 FsMIBfdSessPerfCVPktIn [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,3};
UINT4 FsMIBfdSessPerfCVPktOut [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,4};
UINT4 FsMIBfdSessMisDefCount [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,5};
UINT4 FsMIBfdSessLocDefCount [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,6};
UINT4 FsMIBfdSessRdiInCount [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,7};
UINT4 FsMIBfdSessRdiOutCount [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,8};
UINT4 FsMIBfdClearStats [ ] ={1,3,6,1,4,1,29601,2,55,2,2,1,9};




tMbDbEntry fsmpbfMibEntry[]= {

{{13,FsMIBfdSystemControl}, GetNextIndexFsMIBfdSystemConfigTable, FsMIBfdSystemControlGet, FsMIBfdSystemControlSet, FsMIBfdSystemControlTest, FsMIBfdSystemConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSystemConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsMIBfdTraceLevel}, GetNextIndexFsMIBfdSystemConfigTable, FsMIBfdTraceLevelGet, FsMIBfdTraceLevelSet, FsMIBfdTraceLevelTest, FsMIBfdSystemConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBfdSystemConfigTableINDEX, 1, 0, 0, "2048"},

{{13,FsMIBfdTrapEnable}, GetNextIndexFsMIBfdSystemConfigTable, FsMIBfdTrapEnableGet, FsMIBfdTrapEnableSet, FsMIBfdTrapEnableTest, FsMIBfdSystemConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBfdSystemConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsMIBfdGblSessOperMode}, GetNextIndexFsMIBfdGblSessionConfigTable, FsMIBfdGblSessOperModeGet, FsMIBfdGblSessOperModeSet, FsMIBfdGblSessOperModeTest, FsMIBfdGblSessionConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdGblSessionConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMIBfdGblDesiredMinTxIntvl}, GetNextIndexFsMIBfdGblSessionConfigTable, FsMIBfdGblDesiredMinTxIntvlGet, FsMIBfdGblDesiredMinTxIntvlSet, FsMIBfdGblDesiredMinTxIntvlTest, FsMIBfdGblSessionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdGblSessionConfigTableINDEX, 1, 0, 0, "1000000"},

{{13,FsMIBfdGblReqMinRxIntvl}, GetNextIndexFsMIBfdGblSessionConfigTable, FsMIBfdGblReqMinRxIntvlGet, FsMIBfdGblReqMinRxIntvlSet, FsMIBfdGblReqMinRxIntvlTest, FsMIBfdGblSessionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdGblSessionConfigTableINDEX, 1, 0, 0, "1000000"},

{{13,FsMIBfdGblDetectMult}, GetNextIndexFsMIBfdGblSessionConfigTable, FsMIBfdGblDetectMultGet, FsMIBfdGblDetectMultSet, FsMIBfdGblDetectMultTest, FsMIBfdGblSessionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdGblSessionConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsMIBfdGblSlowTxIntvl}, GetNextIndexFsMIBfdGblSessionConfigTable, FsMIBfdGblSlowTxIntvlGet, FsMIBfdGblSlowTxIntvlSet, FsMIBfdGblSlowTxIntvlTest, FsMIBfdGblSessionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdGblSessionConfigTableINDEX, 1, 0, 0, "1000000"},

{{13,FsMIBfdMemAllocFailure}, GetNextIndexFsMIBfdStatisticsTable, FsMIBfdMemAllocFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIBfdInputQOverFlows}, GetNextIndexFsMIBfdStatisticsTable, FsMIBfdInputQOverFlowsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsMIBfdClrGblStats}, GetNextIndexFsMIBfdStatisticsTable, FsMIBfdClrGblStatsGet, FsMIBfdClrGblStatsSet, FsMIBfdClrGblStatsTest, FsMIBfdStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdStatisticsTableINDEX, 1, 0, 0, "2"},

{{13,FsMIBfdClrAllSessStats}, GetNextIndexFsMIBfdStatisticsTable, FsMIBfdClrAllSessStatsGet, FsMIBfdClrAllSessStatsSet, FsMIBfdClrAllSessStatsTest, FsMIBfdStatisticsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdStatisticsTableINDEX, 1, 0, 0, "2"},

{{13,FsMIBfdSessRole}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessRoleGet, FsMIBfdSessRoleSet, FsMIBfdSessRoleTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, "1"},

{{13,FsMIBfdSessMode}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessModeGet, FsMIBfdSessModeSet, FsMIBfdSessModeTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, "1"},

{{13,FsMIBfdSessRemoteDiscr}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessRemoteDiscrGet, FsMIBfdSessRemoteDiscrSet, FsMIBfdSessRemoteDiscrTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessEXPValue}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessEXPValueGet, FsMIBfdSessEXPValueSet, FsMIBfdSessEXPValueTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, "0"},

{{13,FsMIBfdSessTmrNegotiate}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessTmrNegotiateGet, FsMIBfdSessTmrNegotiateSet, FsMIBfdSessTmrNegotiateTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, "1"},

{{13,FsMIBfdSessOffld}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessOffldGet, FsMIBfdSessOffldSet, FsMIBfdSessOffldTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, "2"},

{{13,FsMIBfdSessEncapType}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessEncapTypeGet, FsMIBfdSessEncapTypeSet, FsMIBfdSessEncapTypeTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessAdminCtrlReq}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessAdminCtrlReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessAdminCtrlErrReason}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessAdminCtrlErrReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessMapType}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessMapTypeGet, FsMIBfdSessMapTypeSet, FsMIBfdSessMapTypeTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessMapPointer}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessMapPointerGet, FsMIBfdSessMapPointerSet, FsMIBfdSessMapPointerTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessCardNumber}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessCardNumberGet, FsMIBfdSessCardNumberSet, FsMIBfdSessCardNumberTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessSlotNumber}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessSlotNumberGet, FsMIBfdSessSlotNumberSet, FsMIBfdSessSlotNumberTest, FsMIBfdSessionTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessRegisteredClients}, GetNextIndexFsMIBfdSessionTable, FsMIBfdSessRegisteredClientsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIBfdSessionTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessPerfCCPktIn}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessPerfCCPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessPerfCCPktOut}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessPerfCCPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessPerfCVPktIn}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessPerfCVPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessPerfCVPktOut}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessPerfCVPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessMisDefCount}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessMisDefCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessLocDefCount}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessLocDefCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessRdiInCount}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessRdiInCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdSessRdiOutCount}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdSessRdiOutCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBfdClearStats}, GetNextIndexFsMIBfdSessPerfTable, FsMIBfdClearStatsGet, FsMIBfdClearStatsSet, FsMIBfdClearStatsTest, FsMIBfdSessPerfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBfdSessPerfTableINDEX, 2, 0, 0, "2"},
};
tMibData fsmpbfEntry = { 35, fsmpbfMibEntry };

#endif /* _FSMPBFDB_H */

