/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdmibclig.h,v 1.2 2010/10/29 22:55:39 prabuc Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMIStdBfdAdminStatus[13];

extern UINT4 FsMIStdBfdSessNotificationsEnable[13];

extern UINT4 FsMIBfdSystemControl[13];

extern UINT4 FsMIBfdTraceLevel[13];

extern UINT4 FsMIBfdTrapEnable[13];

extern UINT4 FsMIBfdGblSessOperMode[13];

extern UINT4 FsMIBfdGblDesiredMinTxIntvl[13];

extern UINT4 FsMIBfdGblReqMinRxIntvl[13];

extern UINT4 FsMIBfdGblDetectMult[13];

extern UINT4 FsMIBfdGblSlowTxIntvl[13];

extern UINT4 FsMIBfdClrGblStats[13];

extern UINT4 FsMIBfdClrAllSessStats[13];

extern UINT4 FsMIStdBfdSessVersionNumber[14];

extern UINT4 FsMIStdBfdSessType[14];

extern UINT4 FsMIStdBfdSessDestinationUdpPort[14];

extern UINT4 FsMIStdBfdSessSourceUdpPort[14];

extern UINT4 FsMIStdBfdSessEchoSourceUdpPort[14];

extern UINT4 FsMIStdBfdSessAdminStatus[14];

extern UINT4 FsMIStdBfdSessOperMode[14];

extern UINT4 FsMIStdBfdSessDemandModeDesiredFlag[14];

extern UINT4 FsMIStdBfdSessControlPlaneIndepFlag[14];

extern UINT4 FsMIStdBfdSessMultipointFlag[14];

extern UINT4 FsMIStdBfdSessInterface[14];

extern UINT4 FsMIStdBfdSessSrcAddrType[14];

extern UINT4 FsMIStdBfdSessSrcAddr[14];

extern UINT4 FsMIStdBfdSessDstAddrType[14];

extern UINT4 FsMIStdBfdSessDstAddr[14];

extern UINT4 FsMIStdBfdSessGTSM[14];

extern UINT4 FsMIStdBfdSessGTSMTTL[14];

extern UINT4 FsMIStdBfdSessDesiredMinTxInterval[14];

extern UINT4 FsMIStdBfdSessReqMinRxInterval[14];

extern UINT4 FsMIStdBfdSessReqMinEchoRxInterval[14];

extern UINT4 FsMIStdBfdSessDetectMult[14];

extern UINT4 FsMIStdBfdSessAuthPresFlag[14];

extern UINT4 FsMIStdBfdSessAuthenticationType[14];

extern UINT4 FsMIStdBfdSessAuthenticationKeyID[14];

extern UINT4 FsMIStdBfdSessAuthenticationKey[14];

extern UINT4 FsMIStdBfdSessStorType[14];

extern UINT4 FsMIStdBfdSessRowStatus[14];

extern UINT4 FsMIBfdSessRole[13];

extern UINT4 FsMIBfdSessMode[13];

extern UINT4 FsMIBfdSessRemoteDiscr[13];

extern UINT4 FsMIBfdSessEXPValue[13];

extern UINT4 FsMIBfdSessTmrNegotiate[13];

extern UINT4 FsMIBfdSessOffld[13];

extern UINT4 FsMIBfdSessEncapType[13];

extern UINT4 FsMIBfdSessMapType[13];

extern UINT4 FsMIBfdSessMapPointer[13];

extern UINT4 FsMIBfdSessCardNumber[13];

extern UINT4 FsMIBfdSessSlotNumber[13];

extern UINT4 FsMIBfdClearStats[13];


