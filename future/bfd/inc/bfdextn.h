/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdextn.h,v 1.5 2015/03/10 10:45:38 siva Exp $
 *
 * Description: This file contains extern declaration of global 
 *              variables of the bfd module.
 *******************************************************************/

#ifndef __BFDEXTN_H__
#define __BFDEXTN_H__

extern tBfdGlobals gBfdGlobals;
/*  Test Globals */
#ifdef BFD_HA_TEST_WANTED
extern tBfdTestGlobals gBfdTestGlobals; 
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
extern tBfdLnxVrf  gBfdLnxVrfSock[SYS_DEF_MAX_NUM_CONTEXTS];
#endif

/* RED Global Info */
extern tBfdRedGlobalInfo  gBfdRedGblInfo;
/* RM Dynamic Info List */
extern tDbTblDescriptor gBfdDynInfoList;

extern tDbDataDescInfo  gaBfdDynDataDescList[BFD_MAX_DYN_INFO_TYPE];


extern tDbOffsetTemplate gaBfdOffsetTbl[];

extern tRBTree gBfdRedTable;

/* queue overflow flag  */
extern UINT1  gu1QueueOverflow;
/* Id of session for which hardware audit ongoing */
extern UINT2  gu2OverflowSessionId;

/*Specifies the Continuity Check for RAW Bfd */
PUBLIC UINT2 gu2BfdAchChnlTypeCcBfd;
/*Specifies the Continuity Verification for RAW Bfd */
PUBLIC UINT2 gu2BfdAchChnlTypeCvBfd;
/*Specifies the Continuity Check for BFD with IPv4 */
PUBLIC UINT2 gu2BfdAchChnlTypeCcIpv4;
/*Specifies the Continuity Verification for BFD with IPv4 */
PUBLIC UINT2 gu2BfdAchChnlTypeCvIpv4;
/*Specifies the Continuity Check for BFD with IPv6 */
PUBLIC UINT2 gu2BfdAchChnlTypeCcIpv6;
/*Specifies the Continuity Verification for BFD with IPv6 */
PUBLIC UINT2 gu2BfdAchChnlTypeCvIpv6;
#endif/*__BFDEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file bfdextn.h                      */
/*-----------------------------------------------------------------------*/

