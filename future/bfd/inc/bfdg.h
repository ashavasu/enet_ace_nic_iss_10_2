/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdg.h,v 1.1 2010/10/14 22:06:14 prabuc Exp $
 *
 * Description: This file contains task lock definitions 
 *******************************************************************/


#define BFD_LOCK  BfdMainTaskLock ()

#define BFD_UNLOCK  BfdMainTaskUnLock ()
