/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdsem.h,v 1.8 2012/01/24 13:28:28 siva Exp $
 *
 * Description: This file contains definitions required for
 * the state machine operation in the bfd module.
 *******************************************************************/

#ifndef __BFDSEM_H__
#define __BFDSEM_H__

PUBLIC INT4 BfdSemHandleLocalSessAdminDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemHandleLocalSessAdminUp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemAdminDownHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemAdminDownHandlePathStChg (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandlePeerDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandlePeerInit (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandlePeerUp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemDownHandlePathStChg (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandlePeerDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandlePeerInit (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandlePeerUp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemInitHandlePathStChg (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandlePeerDown (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandlePeerUp (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandlePathStChg (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry *, 
        tBfdPktInfo *);
PUBLIC INT4 BfdSemUpHandlePeerInit (tBfdFsMIStdBfdSessEntry *, 
        tBfdPktInfo *);
PUBLIC INT4 BfdSemNoAction (tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *);

/* Any state when No Action to be taken */
#define   NOACTION            BfdSemNoAction

/* Admin Down State */
#define   LCLADMINDOWN        BfdSemHandleLocalSessAdminDown
#define   LCLADMINUP          BfdSemHandleLocalSessAdminUp
#define   ADDWNDETTMREXP      BfdSemAdminDownHandleDetnTmrExp
#define   ADDWNPATHSTCHG      BfdSemAdminDownHandlePathStChg
/* DOWN State */
#define   DWNPEERADMINDOWN    BfdSemDownHandlePeerAdminDown
#define   DWNPEERDOWN         BfdSemDownHandlePeerDown
#define   DWNPEERINIT         BfdSemDownHandlePeerInit
#define   DWNPEERUP           BfdSemDownHandlePeerUp
#define   DWNDETTMREXP        BfdSemDownHandleDetnTmrExp
#define   DWNPATHSTCHG        BfdSemDownHandlePathStChg
/* INIT State */
#define   INITPEERADMINDOWN   BfdSemInitHandlePeerAdminDown
#define   INITPEERDOWN        BfdSemInitHandlePeerDown
#define   INITPEERINIT        BfdSemInitHandlePeerInit
#define   INITPEERUP          BfdSemInitHandlePeerUp
#define   INITDETTMREXP       BfdSemInitHandleDetnTmrExp
#define   INITPATHSTCHG       BfdSemInitHandlePathStChg
/* UP State */
#define   UPPEERADMINDOWN     BfdSemUpHandlePeerAdminDown
#define   UPPEERDOWN          BfdSemUpHandlePeerDown
#define   UPPEERINIT          BfdSemUpHandlePeerInit
#define   UPPEERUP            BfdSemUpHandlePeerUp
#define   UPDETTMREXP         BfdSemUpHandleDetnTmrExp
#define   UPPATHSTCHG         BfdSemUpHandlePathStChg

/* BFD state machine function pointer */
typedef INT4 (*tBfdSemFuncPtr)(tBfdFsMIStdBfdSessEntry *pBfdSessEntry,
        tBfdPktInfo *pBfdRxPktInfo);

/* -------------------------------------------------------- */


/* State Event Machine */

tBfdSemFuncPtr
gBfdSessSem[BFD_SESS_MAX_STATES][BFD_MAX_SEM_EVENTS] =
{   
    /* EVENTS --> 
       PEER-ADMINDOWN       PEER-DOWN       PEER-INIT      PEER-UP
       LOCAL-ADMINDOWN   LOCAL-ADMINUP   LOCAL-DETTMREXP    PATHSTCHG    
       OFFLDEV */
    { 
        &NOACTION, 
        &NOACTION, 
        &NOACTION, 
        &NOACTION, 
        &LCLADMINDOWN,    
        &LCLADMINUP,   
        &ADDWNDETTMREXP, 
        &ADDWNPATHSTCHG,
    }, /* ADMIN DOWN */

    { 
        &DWNPEERADMINDOWN,   
        &DWNPEERDOWN,   
        &DWNPEERINIT,   
        &DWNPEERUP,   
        &LCLADMINDOWN,     
        &LCLADMINUP,   
        &DWNDETTMREXP,   
        &DWNPATHSTCHG,
    }, /* DOWN */

    { 
        &INITPEERADMINDOWN,  
        &INITPEERDOWN, 
        &INITPEERINIT, 
        &INITPEERUP, 
        &LCLADMINDOWN,    
        &LCLADMINUP,    
        &INITDETTMREXP,  
        &INITPATHSTCHG,
    }, /* INIT */

    { 
        &UPPEERADMINDOWN,  
        &UPPEERDOWN, 
        &UPPEERINIT, 
        &UPPEERUP, 
        &LCLADMINDOWN,    
        &LCLADMINUP,    
        &UPDETTMREXP,    
        &UPPATHSTCHG,
    }  /* UP */
};

INT1 gBfdSessSemState[BFD_SESS_MAX_STATES][15] = {"ADMINDOWN",
    "DOWN",
    "INIT",
    "UP"};

INT1 gBfdSessSemEvent[BFD_MAX_SEM_EVENTS][20] = {"PEER-ADMINDOWN",
    "PEER-DOWN",
    "PEER-INIT",
    "PEER-UP",
    "LOCAL-ADMINDOWN",
    "LOCAL-ADMINUP",
    "LOCAL-DETTMREXP",
    "PATHSTCHG"};




#endif  /* __BFDSEM_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file bfdsem.h                      */
/*-----------------------------------------------------------------------*/
