/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdred.h,v 1.1 2013/05/22 12:42:02 siva Exp $
 *
 * Description: This file contains all macro definitions and
 *              function prototypes for BFD module.
 *
 *******************************************************************/
#ifndef __BFD_RED_H
#define __BFD_RED_H

enum{
    BFD_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type  */
    BFD_MAX_DYN_INFO_TYPE
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
};

/* Represents the message types encoded in the update messages */
typedef enum {
    BFD_RED_DYN_CACHE_INFO  = BFD_DYN_INFO,
    BFD_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    BFD_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG
}eBfdRedRmMsgType;

typedef enum{
    BFD_HA_UPD_NOT_STARTED = 1,/* 1 */
    BFD_HA_UPD_IN_PROGRESS,    /* 2 */
    BFD_HA_UPD_COMPLETED,      /* 3 */
    BFD_HA_UPD_ABORTED,        /* 4 */
    BFD_HA_MAX_BLK_UPD_STATUS
} eBfdHaBulkUpdStatus;


#define BFD_RED_BULK_UPD_TAIL_MSG_SIZE   3 /* MsgType(1Byte) + Length (2Byte) */
#define BFD_RED_BULK_REQ_MSG_SIZE        3
#define BFD_RED_TYPE_FIELD_SIZE          1
#define BFD_RED_LEN_FIELD_SIZE           2

#ifdef L2RED_WANTED
#define BFD_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define BFD_GET_RMNODE_STATUS()  RM_ACTIVE
#endif


#define BFD_NUM_STANDBY_NODES() gBfdRedGblInfo.u1NumPeersUp

#define BFD_RM_BULK_REQ_RCVD() gBfdRedGblInfo.bBulkReqRcvd

#define BFD_RM_GET_NUM_STANDBY_NODES_UP() \
          gBfdRedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define BFD_IS_STANDBY_UP() \
          ((gBfdRedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)


#define BFD_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define BFD_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define BFD_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define BFD_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define BFD_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
            RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                            *(pu4Offset) += 1;\
}while (0)

#define BFD_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
            RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                            *(pu4Offset) += 2;\
}while (0)

#define BFD_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
            RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                            *(pu4Offset) += 4;\
}while (0)

#define BFD_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
            RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                            *(pu4Offset) += u4Size;\
}while (0)



#endif 

