/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdtrc.h,v 1.2 2013/12/07 10:52:18 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/


#ifndef __BFDTRC_H__
#define __BFDTRC_H__

#define  BFD_TRC_FLAG  gBfdGlobals.u4BfdTrc
#define  BFD_TRC_LVL  gBfdGlobals.u4BfdTrcLvl 
#define  BFD_NAME      "BFD"               

#ifdef BFD_TRACE_WANTED



#define  BFD_TRC(x)       BfdTrcPrint( __FILE__, __LINE__, BfdTrc x)
#define  BFD_TRC_FUNC(x)  BfdTrcPrint( __FILE__, __LINE__, BfdTrc x)
#define  BFD_TRC_CRIT(x)  BfdTrcPrint( __FILE__, __LINE__, BfdTrc x)
#define  BFD_TRC_PKT(x)   BfdTrcWrite( BfdTrc x)




#else /* BFD_TRACE_WANTED */

#define  BFD_TRC(x) 
#define  BFD_TRC_FUNC(x)
#define  BFD_TRC_CRIT(x)
#define  BFD_TRC_PKT(x)

#endif /* BFD_TRACE_WANTED */


#define  BFD_FN_ENTRY  (0x1 << 9)
#define  BFD_FN_EXIT   (0x1 << 10)
#define  BFD_CLI_TRC   (0x1 << 11)
#define  BFD_MAIN_TRC  (0x1 << 12)
#define  BFD_PKT_TRC   (0x1 << 13)
#define  BFD_QUE_TRC   (0x1 << 14)
#define  BFD_TASK_TRC  (0x1 << 15)
#define  BFD_TMR_TRC   (0x1 << 16)
#define  BFD_UTIL_TRC  (0x1 << 17)
#define  BFD_ALL_TRC   BFD_FN_ENTRY |\
                        BFD_FN_EXIT  |\
                        BFD_CLI_TRC  |\
                        BFD_MAIN_TRC |\
                        BFD_PKT_TRC  |\
                        BFD_QUE_TRC  |\
                        BFD_TASK_TRC |\
                        BFD_TMR_TRC  |\
                        BFD_UTIL_TRC


#endif /* _BFDTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdtrc.h                      */
/*-----------------------------------------------------------------------*/
