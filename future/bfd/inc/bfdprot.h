/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdprot.h,v 1.44 2016/07/18 11:21:11 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of bfd module.
 *******************************************************************/

#ifndef __BFDPROT_H__
#define __BFDPROT_H__ 

/* Add Prototypes here */

/**************************bfdmain.c*******************************/
PUBLIC VOID BfdMainTask              PROTO ((VOID));
PUBLIC UINT4 BfdMainTaskInit         PROTO ((VOID));
PUBLIC VOID BfdMainDeInit            PROTO ((VOID));
PUBLIC INT4 BfdMainTaskLock          PROTO ((VOID));
PUBLIC INT4 BfdMainTaskUnLock        PROTO ((VOID));
PUBLIC VOID BfdMainReleaseQMemory    PROTO ((tBfdReqParams *));
VOID BfdApiVcmCallback PROTO ((UINT4 , UINT4 , UINT1 ));

/**************************bfdque.c********************************/
PUBLIC INT4 BfdQueEnqMsg             PROTO ((tBfdReqParams *));
PUBLIC VOID BfdQueProcessMsgs        PROTO ((VOID));
PUBLIC VOID BfdQueHandleRxMsg PROTO ((tBfdReqParams * ));
INT4 BfdQueHandleOffCbMsg PROTO ((UINT4 , tBfdOffCbParams *));

/**************************bfdport.c*******************************/
PUBLIC INT4
BfdPortHandleExtInteraction PROTO ((tBfdExtInParams  *pBfdExtReqInParams,
                                    tBfdExtOutParams *pBfdExtReqOutParams ));

INT4 BfdPortGetSystemMode PROTO ((UINT2 ));
INT4 BfdPortGetSystemModeExt PROTO ((UINT2 ));

/**************************bfdtmr.c*******************************/
PUBLIC VOID  BfdTmrInitTmrDesc      PROTO(( VOID));
PUBLIC INT4  BfdTmrStartTmr         PROTO((tBfdTmr * pBfdTmr, 
                                           tBfdSubTmrId ,
                                           UINT4 ));
PUBLIC VOID  BfdTmrRestartTmr       PROTO((tBfdTmr * pBfdTmr, 
                                           tBfdTmrId eBfdTmrId, 
                                           UINT4 u4Secs));
PUBLIC INT4 BfdTmrReStart           PROTO ((tBfdFsMIStdBfdSessEntry * ,
                                            tBfdTmrId,
                                            tBfdSubTmrId eBfdSubTmrId));
PUBLIC INT4 BfdTmrStop              PROTO ((tBfdFsMIStdBfdSessEntry *,
                                            tBfdTmrId));
PUBLIC INT4 BfdTmrStart             PROTO ((tBfdFsMIStdBfdSessEntry *,
                                            tBfdTmrId , tBfdSubTmrId ));
PUBLIC VOID  BfdTmrHandleExpiry     PROTO((VOID));

PUBLIC VOID  BfdTmrStopTmr          PROTO((tBfdTmr * pBfdTmr));
PUBLIC VOID BfdTmrDeInitTmrDesc  PROTO ((VOID));
VOID
BfdTmrJitter  PROTO ((UINT4 *,
                  UINT4 ));

/**************************bfdtrc.c*******************************/
CHR1  *BfdTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   BfdTrcPrint      PROTO(( const char *fname, UINT4 u4Line,
                                const char *s));
VOID   BfdTrcWrite      PROTO(( CHR1 *s));

/**************************bfdtask.c*******************************/

PUBLIC VOID BfdTaskRegisterBfdMibs  PROTO ((VOID));
PUBLIC VOID BfdTaskUnRegisterBfdMibs  PROTO ((VOID));
PUBLIC INT4 BfdMainRegisterWithVcm  PROTO ((VOID));

/*************************bfdext.c*********************************/

VOID BfdExtFillMplsPathId PROTO ((tBfdSessPathParams *, tBfdExtInParams *));

INT4 BfdExtChkSessOperRole PROTO ((tBfdFsMIStdBfdSessEntry *, UINT4 ,BOOL1 *));

INT4 BfdExtArpResolve PROTO ((UINT4 , UINT1 *, UINT1 *));

INT4 BfdExtInformSessionState PROTO ((tBfdFsMIStdBfdSessEntry *, UINT4 ));

INT4 BfdExtGetPortFromIfIndex PROTO ((UINT4 , UINT4 *));

INT4 BfdExtEnqueueArp PROTO ((UINT4 , UINT4 ));

INT4 BfdExtGetIfVlan PROTO((UINT4 , UINT2 *));

INT4 BfdExtGetIfMacAddr PROTO ((UINT4 , UINT4 , UINT1 *));

INT4 BfdExtGetRouterId PROTO ((UINT4, UINT4 *));

INT4 BfdExtGetEggrIfNhFromDestIp PROTO ((UINT4 , UINT4 , UINT4 *, UINT4 *));

INT4 BfdExtGetPwPathIdFromIdx  PROTO ((UINT4, tPwPathId *));

PUBLIC INT4 BfdExtFmApiSysLog PROTO (( UINT4, UINT4 ,  CHR1 *));

PUBLIC INT4 BfdExtUpdateSessParams PROTO ((tBfdFsMIStdBfdSessEntry *,
                                           tBfdSessIndex));

INT4 BfdExtFmNotifyFaults PROTO ((tSNMP_OID_TYPE *, UINT4 , UINT4 ,
                          tSNMP_VAR_BIND *,
                          tSNMP_OCTET_STRING_TYPE *));
INT4
BfdExtGetSystemMode PROTO ((VOID));

INT4
BfdExtGetSystemModeExt PROTO ((VOID));

INT4 BfdExtNetIpv4GetIfInfo PROTO ((UINT4 , tNetIpv4IfInfo * ));

INT4
BfdExtGetIfIndexFromMplsTnlIf PROTO((UINT4 , UINT4 *,
                               BOOL1 ));

#ifdef MBSM_WANTED
INT4
BfdExtGetMbsmParams  PROTO ((tBfdFsMIStdBfdSessEntry *,
                        UINT4 *,UINT4 *));
#endif

/*************************bfdtrap.c********************************/

#ifdef MBSM_WANTED
PUBLIC VOID BfdMbsmHandleLCEvent  PROTO ((tMbsmProtoMsg * , UINT4));

VOID
BfdMbsmHandleCardInfoUpdate  PROTO ((UINT4 ,
                    tBfdMbsmParams *));
#endif

PUBLIC VOID 
BfdIssTrapEventLogNotify  PROTO (( UINT4 ,  UINT4 , ...));

/*************************bfdtrap.c********************************/
PUBLIC VOID BfdTrapEventLogNotify PROTO (( UINT4 ,  UINT4 , ...));
PUBLIC VOID BfdIssEventLogNotify PROTO (( UINT4 ,  UINT4 , ...));

/**************************bfdsem.c*******************************/

VOID BfdSessSm PROTO ((tBfdFsMIStdBfdSessEntry *, UINT1, UINT4));


INT4
BfdExtGetMplsPathInfo PROTO ((UINT4 u4ContextId,
                              tBfdSessPathParams *pBfdSessPathParams,
                              tBfdMplsPathInfo *pBfdMplsPathInfo,
                              BOOL1));

PUBLIC INT4 BfdSemTrigger  PROTO ((tBfdFsMIStdBfdSessEntry *, 
                                   UINT1 , tBfdPktInfo *));

/**************************bfdutil.c *******************************/
INT4
BfdUtilFsMIStdBfdGlblCfgTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *,
                                  tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));

INT4
BfdUtilFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *,
                                   tBfdIsSetFsMIStdBfdSessEntry *));

INT4
BfdUtilFsMIStdBfdSessBaseOidTest PROTO ((tBfdIsSetFsMIStdBfdSessEntry *,
                                         tBfdFsMIStdBfdSessEntry *));

INT4
BfdUtilFsMIStdBfdSessPathExist PROTO ((tBfdFsMIStdBfdSessEntry *, INT4));

INT4
BfdFsMIStdBfdSessTableClearStats PROTO((tBfdFsMIStdBfdSessEntry *));

INT4
BfdUtilTrapFsMIStdBfdGlblCfgTbl PROTO 
                                  ((tBfdFsMIStdBfdGlobalConfigTableEntry *));

INT4
BfdUtilEncapCheck PROTO ((tBfdFsMIStdBfdSessEntry *));

INT4
BfdUtilNotifFsMIStdBfdGlblCfgTbl PROTO 
                         ((tBfdFsMIStdBfdGlobalConfigTableEntry *,
                           tBfdFsMIStdBfdGlobalConfigTableEntry *));

tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdUtilGetBfdGlobalConfigTable PROTO ((UINT4));

tBfdFsMIStdBfdSessEntry *
BfdUtilGetBfdSessTable PROTO ((UINT4, UINT4));


VOID
BfdUtilUpdatePktCount PROTO ((UINT4 *, tSNMP_COUNTER64_TYPE *));

VOID
BfdUtilUpdateOffPktCount PROTO ((tSNMP_COUNTER64_TYPE *,
                      FS_UINT8 *));

INT4
BfdUtilGetBfdOffStats (tBfdFsMIStdBfdSessEntry *);


VOID
BfdUtilUpdateMemAlocFail  PROTO ((UINT4 ));

INT4
BfdUtilCheckBfdSessStatus PROTO ((tBfdFsMIStdBfdSessEntry *));

INT4 
BfdUtilCheckBfdModuleStatus PROTO ((UINT4));

INT4
BfdUtilGetIfIndexFromPort PROTO ((UINT4 u4Port, UINT4 *pu4IfIndex));

INT4
BfdUtilGetSrcAddrFromIfIndex PROTO ((UINT4 u4IfIndex, UINT1 u1AddrType,
                                     UINT1 *pu1SrcAddr, UINT1 *pu1DstAddr));

INT4
BfdUtilGetSrcv6GlobalAddr (UINT4 u4IfIndex,
                           tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr);

INT1
BfdUtilIpHandlePathStatusChange PROTO ((UINT4, tBfdClientNbrIpPathInfo *));
INT4
BfdUtilValidateIfIndex PROTO ((UINT4 u4IfIndex));

VOID
BfdUtilFillIpMapEntry PROTO ((tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo,
                       tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry));

tBfdFsMIStdBfdSessIpMapEntry *
BfdUtilCheckIpMapEntryExist PROTO ((tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo));

VOID
BfdUtilGetFreeSessIndex PROTO ((UINT4 u4ContextId, UINT4 *pu4SessIndex));

INT4 BfdUtilDynBfdSessTableTrigger PROTO ((tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *, INT4));

/**************************bfdrx.c*******************************/
INT4 BfdRxReceivePacket PROTO ((UINT4, UINT1 *, UINT4,
                                UINT4, tIpAddr *, tIpAddr *, UINT4));

INT4 BfdRxGetSessInfo PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4,
                              tIpAddr *, tIpAddr *,tBfdPktInfo *pBfdPktInfo,
                              tBfdFsMIStdBfdSessEntry **));

VOID BfdRxGetCtrlPacket PROTO ((UINT1 *pu1Buf,
                                tBfdParseRxPkt *pBfdCtrlPacket));

INT4 BfdRxProcessRxPkt PROTO ((UINT4 u4ContextId, tBfdPktInfo *pBfdPktInfo,
                               tBfdFsMIStdBfdSessEntry *pBfdSessCtrl));

VOID BfdRxFormMplsLabel PROTO ((tMplsLblInfo * pHdr, UINT4 *pu4Lable));

VOID BfdRxGetMplsHdr PROTO ((UINT1 *pBuf, tMplsHdr * pHdr));

VOID BfdRxGetAchHdr  PROTO ((UINT1 *pBuf, tMplsAchHdr * pHdr));

INT4 BfdRxValidateRxPkt PROTO ((tBfdFsMIStdBfdSessEntry *pBfdSessCtrl,
                                tBfdPktInfo *pBfdPktInfo));

INT4 BfdRxParsePacket PROTO ((UINT4, UINT1 *, UINT4,
                          UINT4, UINT4, tBfdPktInfo *));

UINT4 BfdRxGetCurrTimeinSecs PROTO ((VOID));

VOID BfdRxGetSourceMep PROTO ((UINT1 **pBuf, tBfdPktInfo *pBfdPktInfo));

INT4 BfdRxProcessCvPacket PROTO ((UINT4, tBfdPktInfo *pBfdPktInfo,
                                  tBfdMplsPathInfo *));

INT4 BfdRxValidateIpUdpHdr PROTO ((UINT4, UINT1 **, UINT4 , tBfdPktInfo *));

tBfdMibFsMIStdBfdSessEntry *
BfdGetSessionNode PROTO ((UINT4 u4ContextId, UINT4 u4SessIndex));


/**************************bfdtx.c*******************************/
PUBLIC INT4 BfdUdpInitSock PROTO ((VOID));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
PUBLIC UINT4 BfdUdpInitSockLnxVrf PROTO ((UINT4 u4ContextId));
PUBLIC UINT4 BfdUdpv6InitSockLnxVrf PROTO ((UINT4 u4ContextId));
PUBLIC INT4 BfdUdpAddOrRemoveFdLnxVrf PROTO ((BOOL1 bIsAddFd,UINT4 u4ContextId));
#endif

PUBLIC INT4 BfdUdpAddOrRemoveFd PROTO ((BOOL1 bIsAddFd));

PUBLIC INT4 BfdUdpTransmitBfdPkt PROTO ((UINT4 , UINT4, UINT4 , UINT4,
                                         INT4, UINT4, UINT1 *, UINT1, 
                                         BOOL1, UINT1 *));


PUBLIC VOID BfdUdpDeInitParams PROTO ((INT4 i4SockId));

PUBLIC INT4 BfdUdpv6InitSock PROTO ((VOID));

PUBLIC INT4 BfdUdpv6AddOrRemoveFd (BOOL1 bIsAddFd);

PUBLIC INT4 BfdUdpv6TransmitBfdPkt PROTO ((UINT4 , UINT4, 
                                           tIp6Addr * , tIp6Addr * , 
                                           INT4, UINT4, UINT1 *, UINT1, 
                                           BOOL1, UINT1 *));

INT4 BfdUdpv6ProcessPktOnSocket PROTO ((tBfdReqParams * ));

VOID Bfdv6PktRcvdOnSocket PROTO ((INT4));

/**************************bfdtx.c*******************************/
INT4 BfdTxPacket PROTO ((tBfdFsMIStdBfdSessEntry *));

VOID BfdTxFormBfdCtrlPkt PROTO ((tBfdFsMIStdBfdSessEntry *, BOOL1, BOOL1));

INT4 BfdTxFillAchHdr PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4, UINT4));

INT4 BfdTxPutSrcMepTlv PROTO ((UINT1 *, tMplsMegApiInfo *));

VOID BfdTxFillSrcMepTlv PROTO ((tBfdMplsPathInfo *, tMplsAchTlv *,
                                tMplsAchTlvHdr *));

INT4 BfdTxFillIpUdpHdr PROTO ((UINT4 , UINT4, tCRU_BUF_CHAIN_HEADER *));

VOID BfdTxFormIpUdpHdr PROTO ((UINT4 , t_IP_HEADER *,
                               tBfdUdp *, UINT4, tCRU_BUF_CHAIN_HEADER *));

INT4 BfdTxConstructPacket PROTO ((tBfdFsMIStdBfdSessEntry *,tBfdMplsPathInfo *,
                                  UINT4 *, UINT1 *, tCRU_BUF_CHAIN_HEADER *,
                                  UINT4 *));

/**************************bfdpoll.c*******************************/

INT4 BfdPollHandleOffldPollSeq PROTO ((UINT4 , tBfdOffCbParams *, 
            tBfdFsMIStdBfdSessEntry *));

PUBLIC INT4 BfdPollHandlePollSequence  PROTO ((tBfdFsMIStdBfdSessEntry *,
                                        tBfdPktInfo *));

PUBLIC INT4 BfdPollUpdateNegParams  PROTO ((tBfdFsMIStdBfdSessEntry *,
                                     tBfdPktInfo *));
PUBLIC INT4 BfdPollInitiatePollSequence  PROTO ((tBfdFsMIStdBfdSessEntry
                                          * pBfdSessEntry));

/**************************bfdgbl.c*******************************/
PUBLIC INT4
BfdGblInitFsMIBfdGblConfigTable  PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry
                                 *pBfdGlobalConfigTableEntry));

PUBLIC INT4 BfdGblCreateContext PROTO ((UINT4));

PUBLIC INT4 BfdGblDeleteContext PROTO ((UINT4,BOOL1));

PUBLIC VOID BfdGblHandleModuleStatus PROTO ((UINT4 , BOOL1));

/**************************bfdsess.c*******************************/
PUBLIC INT4 BfdSessHandleSessionEntryStatus PROTO ((tBfdFsMIStdBfdSessEntry *));

PUBLIC INT4 BfdSessHandleSessPathStatusChg PROTO ((UINT4 , UINT4 , UINT1));

PUBLIC INT4 
BfdSessHandleSessActivate(tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessInfo);

PUBLIC INT4 BfdSessScanTnlTbl PROTO ((UINT4 ,tBfdSessPathParams *, UINT4 *));

PUBLIC INT4 BfdSessReactBfdSessEntry  PROTO ((tBfdFsMIStdBfdSessEntry
                                       *pBfdNextBfdSessEntry));
PUBLIC VOID BfdSessDeactDelBfdSessTable  PROTO ((UINT4 u4ContextId,
                                          UINT1 u1OperDelete));

PUBLIC INT4 BfdSessDelSessEntryDynParams  PROTO ((tBfdFsMIStdBfdSessEntry *));

INT4 BfdSessSessionUp  PROTO ((tBfdFsMIStdBfdSessEntry * ));

INT4 BfdSessSessionDown  PROTO ((tBfdFsMIStdBfdSessEntry * ,tBfdDiagnostic,
                                 BOOL1));

INT4 BfdSessDeactBfdSessEntry  PROTO ((tBfdFsMIStdBfdSessEntry *, BOOL1));

PUBLIC UINT4 BfdSessGetDiscriminator  PROTO ((tBfdFsMIStdBfdSessEntry * ));

PUBLIC VOID BfdSessReactBfdSessTable  PROTO ((UINT4 u4ContextId));

PUBLIC INT4
BfdSessProcessAlwParamChgMsg PROTO ((UINT4 ,
        tBfdAllowParamChgMsg *));
VOID
BfdSessChkAndTransmitPkt(tBfdFsMIStdBfdSessEntry *);

VOID
BfdSessHandleClientRqst PROTO ((UINT4 u4ContextId,                                                                                                tBfdClientInfo *pBfdIpPathInfo,
                                UINT1 u1Flag));
VOID
BfdSessHandleDeRegisterAll PROTO ((UINT4 u4ContextId,
                            tBfdClientInfo * pBfdClientInfo));

INT4
BfdSessRegisterClient PROTO ((tBfdFsMIStdBfdSessEntry *,
                               tBfdClientInfo *));

INT4
BfdSessDeRegisterClient PROTO ((tBfdFsMIStdBfdSessEntry *pBfdSessEntry,
                                UINT1 u1BfdClientId));
INT4
BfdSessDisableClient  PROTO ((tBfdFsMIStdBfdSessEntry *pBfdSessEntry,
                              UINT1 u1BfdClientId));

/**************************bfdcore.c*******************************/

PUBLIC INT4 BfdCoreCreateOffldSess  PROTO ((tBfdFsMIStdBfdSessEntry *,
                                    tBfdPktInfo *));

INT4
BfdCoreUpdateExtMod  PROTO ((tBfdFsMIStdBfdSessEntry *pBfdSessEntry, BOOL1 bValidSess));

PUBLIC INT4
BfdCoreTriggerBfdSem PROTO ((tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                             tBfdSemReqType, tBfdPktInfo * pBfdRxPktInfo));
PUBLIC INT4
BfdCoreProcessRxBfdControlPkt PROTO ((tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                                      tBfdPktInfo * pBfdRxPktInfo));

PUBLIC INT4
BfdCoreCreateMapEntries  PROTO ((tBfdFsMIStdBfdSessEntry *));

PUBLIC INT4
BfdCoreDeleteMapEntries  PROTO ((tBfdFsMIStdBfdSessEntry *));

INT4
BfdCoreHandleAdminStatusChg(tBfdFsMIStdBfdSessEntry *);

INT4
BfdCoreCreateBfdSessIpMapEntry (tBfdFsMIStdBfdSessEntry *);

INT4 BfdCoreCreateDynamicSession PROTO ((UINT4, tBfdClientInfo *));

INT4
BfdCoreDeleteBfdSessIpMapEntry PROTO ((tBfdFsMIStdBfdSessEntry *
                                 pBfdFsMIStdBfdSessInfo));
/**************************bfdcore.c*******************************/

/**************************bfdsem.c*******************************/
PUBLIC VOID
BfdSemResetSemVariables PROTO ((tBfdFsMIStdBfdSessEntry *));

PUBLIC INT4
BfdSessDeleteBfdSessEntry  PROTO ((tBfdFsMIStdBfdSessEntry *));


/**************************bfdbtstr.c*******************************/
INT4 BfdBtStrTriggerOOBBtStrap PROTO ((tBfdFsMIStdBfdSessEntry *));

PUBLIC INT4 BfdBtStrProcessLsppBtStrapInfo  PROTO ((UINT4, tBfdLsppInfo *));

/**************************bfdsem.c*******************************/

INT4 BfdQueHandlePathStatus PROTO ((tBfdReqParams * ));

INT4 BfdQueHandleOffldMsg PROTO ((tBfdReqParams * ));

INT4 BfdUdpProcessPktOnSocket PROTO ((tBfdReqParams * ));

VOID BfdPktRcvdOnSocket PROTO ((INT4));

INT4 BfdExtChkSessPathEgrs PROTO ((tBfdFsMIStdBfdSessEntry *, BOOL1 *));

PUBLIC INT4 BfdExtSendPktToMpls PROTO ((UINT4, UINT4, UINT1 *,
                                        tCRU_BUF_CHAIN_HEADER *));

INT4 BfdPortNetIpv4GetPortFromIfIndex PROTO ((UINT4 , UINT4 *));

INT4 BfdPortNetIpv4GetIfInfo PROTO ((UINT4 , tNetIpv4IfInfo *));

INT4 BfdPortGetCfaIfIndexFromPort PROTO ((UINT4 , UINT4 *));


/**************************bfdoff.c*******************************/


PUBLIC INT4 BfdOffSessStop PROTO ((UINT4, tBfdFsMIStdBfdSessEntry *,
                                   tBfdSessHwStopBfdCallFlag));


INT4 BfdOffSessPollTerminate  PROTO ((UINT4 u4ContextId,
                              tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessTable,
                              tBfdPktInfo *pBfdPktInfo,
                              tBfdHwhandle *pBfdSessHwHandle));



INT4
BfdGetLabelFromOutSegInfo
                PROTO ((tBfdMibFsMIStdBfdSessEntry *,
                tMplsOutSegInfo *,
                tMplsLblInfo   *, 
                UINT1 *));

INT4
BfdGetLabelFromInSegInfo
                PROTO ((tBfdMibFsMIStdBfdSessEntry *,
                tMplsInSegInfo *,
                tMplsLblInfo   *,
                UINT1 *));

INT4
BfdGetInLabelInfoFromPw PROTO ((tBfdMplsPathInfo *,
             tBfdMibFsMIStdBfdSessEntry *,
             tMplsLblInfo    *,
             UINT1 *));
INT4
BfdGetOutLabelInfoFromPw PROTO ((tBfdMplsPathInfo *,
             tBfdMibFsMIStdBfdSessEntry *,
             tMplsLblInfo    *, 
             UINT1 *));
VOID 
BfdOffFillSessHandle  PROTO ((
        tBfdFsMIStdBfdSessEntry *,
        tBfdSessionHandle *));
INT4
BfdOffResolveArpForNh PROTO ((UINT4 ,UINT4 ,UINT1 *));

INT4
BfdOffFillSessPathInfo  PROTO ((UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdMplsPathInfo *,
        tBfdPktInfo *,
        tBfdSessionPathInfo *));

INT4
BfdOffFillIpUdpHdrInfo PROTO ((tBfdFsMIStdBfdSessEntry *,
                tBfdIpUdpHdrParams *));


INT4    
BfdOffFillSessEncapParams  PROTO ((tBfdFsMIStdBfdSessEntry *,
        tBfdMplsPathInfo *,
        tBfdPktInfo *,
        tBfdCtrlPktEncapParams *,
        tBfdCtrlPktEncapParams *,
        UINT4));

VOID
BfdOffFillSessParams  PROTO ((tBfdFsMIStdBfdSessEntry *,
        tBfdMplsPathInfo *,
        tBfdMibFsMIStdBfdGlobalConfigTableEntry *,
        tBfdPktInfo *,
        tBfdParams *,
        BOOL1 ,
        BOOL1 ,
        UINT4));

INT4
BfdOffFillTxPkt  PROTO ((tBfdFsMIStdBfdSessEntry *,
        tBfdMplsPathInfo *,
        tBfdSessionPathInfo *,
        tBfdOffPktBuf *,
        UINT4 *));

INT4
BfdOffFillSessTblFromStats 
 PROTO ((tBfdFsMIStdBfdSessEntry  *,
 tBfdSessStats               *));

INT4
BfdOffFillSessTblFromParams 
 PROTO ((tBfdMibFsMIStdBfdSessEntry  *,
 tBfdParams                  *));

INT4 
BfdOffSessConfigure  PROTO (( UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo *,
        tBfdSessHwStartBfdCallFlag  ,
        BOOL1 ,
        BOOL1 ,
        tBfdHwhandle *,
        UINT1 *));

INT4 BfdOffSessCreate  PROTO (( UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo             *,
        tBfdHwhandle            *,
        UINT1 *));

INT4 BfdOffSessEnable  PROTO (( UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdHwhandle            *));

INT4 
BfdOffSessPollInitUpdParams  PROTO (( UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdHwhandle            *));

INT4 
BfdOffSessPollResponse  PROTO (( UINT4 ,
        tBfdFsMIStdBfdSessEntry *,
        tBfdPktInfo             *,
        tBfdHwhandle            *));

INT4 
BfdOffSessRegCb  PROTO (( UINT4 ,
        tBfdEventType           ,
        FsMiBfdHwSessionCb )); 

INT4 
BfdOffSessInfo  PROTO (( UINT4 u4ContextId,
        tBfdFsMIStdBfdSessEntry *,
        tBfdFsMIStdBfdSessEntry *,
        UINT1  ));

INT4 BfdOffHandleOffCbMsg PROTO ((UINT4 ,
        tBfdOffCbParams *));

INT4
BfdOffMapHwEvents PROTO ((UINT4, tBfdSessionHandle *,
                          tBfdHwhandle *, tBfdEventType *, BOOL1,
                          tBfdParams *, tBfdOffPktBuf *));


/***************************bfdcli.c**********************************/
#ifdef BFD_HA_TEST_WANTED
INT4 
BfdTestCliShowSessTmr (tCliHandle CliHandle, UINT4 u4SessIndex);
#endif

INT4
BfdCliSetFsMIStdBfdGlobalConfigTable  PROTO ((tCliHandle ,
                                      tBfdFsMIStdBfdGlobalConfigTableEntry * ,
                                      tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));

INT4
BfdCliSetFsMIStdBfdSessTable  PROTO ((tCliHandle,
                              tBfdFsMIStdBfdSessEntry *,
                              tBfdIsSetFsMIStdBfdSessEntry *));

INT4
BfdCliPrintSessInfo PROTO ((tCliHandle CliHandle, tBfdFsMIStdBfdSessEntry
                *pBfdFsMIStdBfdSessTable, UINT1 u1Output));
VOID
BfdCliPrintNeighbors (tCliHandle CliHandle, tBfdFsMIStdBfdSessEntry *pBfdSessEntry, UINT1);

INT4
BfdCliPrintSessStatsInfo PROTO ((tCliHandle CliHandle, tBfdFsMIStdBfdSessEntry *
                    pBfdFsMIStdBfdSessTable, UINT4 u4Type));

VOID
BfdCliConvertTimetoString PROTO ((UINT4 , INT1 *));

INT4 BfdCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 

/****************************** Prototypes for redundancy ******************/
UINT4
BfdRedInitGlobalInfo PROTO ((VOID));

#ifdef L2RED_WANTED
VOID 
BfdRedHandleRmEvents PROTO ((tBfdRmMsg *pBfdRmMsg));

INT4
BfdRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));

VOID
BfdRedHandleGoStandby PROTO ((tBfdRmMsg * pMsg));
#endif
UINT4
BfdRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

VOID
BfdRedHandleGoActive PROTO ((VOID));

VOID
BfdRedHandleIdleToActive PROTO ((VOID));

VOID
BfdRedHandleStandbyToActive PROTO ((VOID));

VOID
BfdRedSendBulkUpdMsg PROTO ((VOID));

VOID
BfdRedSyncDynInfo PROTO ((VOID));

VOID
BfdRedAddAllNodeInDbTbl PROTO ((VOID));

VOID
BfdRedDbUtilAddTblNode PROTO ((tDbTblDescriptor * pBfdDataDesc,
                         tDbTblNode * pBfdSessDbNode));

UINT4
BfdRedSendMsgToRm PROTO ((tRmMsg * pMsg, UINT4 u4Length));

VOID
BfdRedSendBulkUpdTailMsg PROTO ((VOID));

VOID
BfdRedHandleActiveToStandby PROTO ((VOID));

VOID
BfdRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

VOID
BfdRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

VOID
BfdRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));

VOID
BfdRedProcessDynamicInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));

VOID
BfdRedHandleIdleToStandby PROTO ((VOID));

VOID
BfdRedSendBulkReqMsg PROTO ((VOID));

VOID
BfdRedDynDataDescInit PROTO ((VOID));

VOID
BfdRedDescrTblInit PROTO ((VOID));

VOID
BfdRedDbNodeInit PROTO ((tDbTblNode * pBfdDbTblNode));

INT4
BfdRedDeInitGlobalInfo PROTO ((VOID));

INT4
BfdRBTreeRedEntryCmp PROTO ((tRBElem * pRBElem, tRBElem * pRBElemIn));

VOID
BfdRedHwAudit PROTO ((UINT1 u1TmrStart));

INT4
BfdRedIndexCompare PROTO ((tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry,
                   tBfdNpOutput BfdNpOutput));

#ifdef RM_WANTED
INT4
BfdRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
BfdGetShowCmdOutputAndCalcChkSum (UINT2 *);

INT4
BfdCliGetShowCmdOutputToFile (UINT1 *);

INT4
BfdCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
BfdCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
#endif

#ifdef RFC6374_WANTED
INT4
BfdApiGetSessIndexFromPwInfo (UINT4 , tBfdReqParams *, tBfdRespParams *);

INT4
BfdApiGetSessIndexFromLspInfo (UINT4 , tBfdReqParams *, tBfdRespParams *);

INT4
BfdGetTxRxFromSessIndex(UINT4 , tBfdReqParams *, tBfdRespParams *);

INT4
BfdGetExpFromSessIndex (UINT4 , tBfdReqParams *, tBfdRespParams *);
#endif

VOID BfdRedHandleDynSyncAudit (VOID);

VOID BfdExecuteCmdAndCalculateChkSum  (VOID);

#endif   /* __BFDPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdprot.h                     */
/*-----------------------------------------------------------------------*/
