/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddefg.h,v 1.4 2012/02/29 09:13:28 siva Exp $
*
* Description: Proto types & DataStructure definition 
*********************************************************************/


#define BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID   BFDMemPoolIds[MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID]


#define BFD_FSMISTDBFDSESSTABLE_POOLID   BFDMemPoolIds[MAX_BFD_FSMISTDBFDSESSTABLE_SIZING_ID]


#define BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID   BFDMemPoolIds[MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE_SIZING_ID]


#define BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID   BFDMemPoolIds[MAX_BFD_FSMISTDBFDSESSIPMAPTABLE_SIZING_ID]

#define BFD_OFFLOAD_PARAMS_SIZING_ID   BFDMemPoolIds[MAX_BFD_OFFLOAD_PARAMS_SIZING_ID]


/* Macro used to fill the CLI structure for FsMIStdBfdGlobalConfigTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDGLOBALCONFIGTABLE_ARGS(pBfdFsMIStdBfdGlobalConfigTable,\
   pBfdIsSetFsMIStdBfdGlobalConfigTable,\
   pau1FsMIStdBfdContextId,\
   pau1FsMIStdBfdAdminStatus,\
   pau1FsMIStdBfdSessNotificationsEnable,\
   pau1FsMIBfdSystemControl,\
   pau1FsMIBfdTraceLevel,\
   pau1FsMIBfdTrapEnable,\
   pau1FsMIBfdGblSessOperMode,\
   pau1FsMIBfdGblDesiredMinTxIntvl,\
   pau1FsMIBfdGblReqMinRxIntvl,\
   pau1FsMIBfdGblDetectMult,\
   pau1FsMIBfdGblSlowTxIntvl,\
   pau1FsMIBfdClrGblStats,\
   pau1FsMIBfdClrAllSessStats)\
  {\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdContextId = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdAdminStatus != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIStdBfdAdminStatus = *(INT4 *) (pau1FsMIStdBfdAdminStatus);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdAdminStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdAdminStatus = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessNotificationsEnable != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIStdBfdSessNotificationsEnable = *(INT4 *) (pau1FsMIStdBfdSessNotificationsEnable);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdSessNotificationsEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIStdBfdSessNotificationsEnable = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSystemControl != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdSystemControl = *(INT4 *) (pau1FsMIBfdSystemControl);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdSystemControl = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdSystemControl = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdTraceLevel != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdTraceLevel = *(INT4 *) (pau1FsMIBfdTraceLevel);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdTraceLevel = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdTraceLevel = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdTrapEnable != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable = *(INT4 *) (pau1FsMIBfdTrapEnable);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdTrapEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdTrapEnable = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdGblSessOperMode != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdGblSessOperMode = *(INT4 *) (pau1FsMIBfdGblSessOperMode);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblSessOperMode = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblSessOperMode = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdGblDesiredMinTxIntvl != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIBfdGblDesiredMinTxIntvl = *(UINT4 *) (pau1FsMIBfdGblDesiredMinTxIntvl);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblDesiredMinTxIntvl = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblDesiredMinTxIntvl = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdGblReqMinRxIntvl != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIBfdGblReqMinRxIntvl = *(UINT4 *) (pau1FsMIBfdGblReqMinRxIntvl);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblReqMinRxIntvl = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblReqMinRxIntvl = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdGblDetectMult != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIBfdGblDetectMult = *(UINT4 *) (pau1FsMIBfdGblDetectMult);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblDetectMult = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblDetectMult = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdGblSlowTxIntvl != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIBfdGblSlowTxIntvl = *(UINT4 *) (pau1FsMIBfdGblSlowTxIntvl);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblSlowTxIntvl = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdGblSlowTxIntvl = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdClrGblStats != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdClrGblStats = *(INT4 *) (pau1FsMIBfdClrGblStats);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdClrGblStats = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdClrGblStats = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdClrAllSessStats != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdClrAllSessStats = *(INT4 *) (pau1FsMIBfdClrAllSessStats);\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdClrAllSessStats = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdGlobalConfigTable->bFsMIBfdClrAllSessStats = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMIStdBfdGlobalConfigTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDGLOBALCONFIGTABLE_INDEX(pBfdFsMIStdBfdGlobalConfigTable,\
   pau1FsMIStdBfdContextId)\
  {\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdGlobalConfigTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
  }\
  }




/* Macro used to fill the CLI structure for FsMIStdBfdSessTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDSESSTABLE_ARGS(pBfdFsMIStdBfdSessTable,\
   pBfdIsSetFsMIStdBfdSessTable,\
   pau1FsMIStdBfdContextId,\
   pau1FsMIStdBfdSessIndex,\
   pau1FsMIStdBfdSessVersionNumber,\
   pau1FsMIStdBfdSessType,\
   pau1FsMIStdBfdSessDestinationUdpPort,\
   pau1FsMIStdBfdSessSourceUdpPort,\
   pau1FsMIStdBfdSessEchoSourceUdpPort,\
   pau1FsMIStdBfdSessAdminStatus,\
   pau1FsMIStdBfdSessOperMode,\
   pau1FsMIStdBfdSessDemandModeDesiredFlag,\
   pau1FsMIStdBfdSessControlPlaneIndepFlag,\
   pau1FsMIStdBfdSessMultipointFlag,\
   pau1FsMIStdBfdSessInterface,\
   pau1FsMIStdBfdSessSrcAddrType,\
   pau1FsMIStdBfdSessSrcAddr,\
   pau1FsMIStdBfdSessSrcAddrLen,\
   pau1FsMIStdBfdSessDstAddrType,\
   pau1FsMIStdBfdSessDstAddr,\
   pau1FsMIStdBfdSessDstAddrLen,\
   pau1FsMIStdBfdSessGTSM,\
   pau1FsMIStdBfdSessGTSMTTL,\
   pau1FsMIStdBfdSessDesiredMinTxInterval,\
   pau1FsMIStdBfdSessReqMinRxInterval,\
   pau1FsMIStdBfdSessReqMinEchoRxInterval,\
   pau1FsMIStdBfdSessDetectMult,\
   pau1FsMIStdBfdSessAuthPresFlag,\
   pau1FsMIStdBfdSessAuthenticationType,\
   pau1FsMIStdBfdSessAuthenticationKeyID,\
   pau1FsMIStdBfdSessAuthenticationKey,\
   pau1FsMIStdBfdSessAuthenticationKeyLen,\
   pau1FsMIStdBfdSessStorType,\
   pau1FsMIStdBfdSessRowStatus,\
   pau1FsMIBfdSessRole,\
   pau1FsMIBfdSessMode,\
   pau1FsMIBfdSessRemoteDiscr,\
   pau1FsMIBfdSessEXPValue,\
   pau1FsMIBfdSessTmrNegotiate,\
   pau1FsMIBfdSessOffld,\
   pau1FsMIBfdSessEncapType,\
   pau1FsMIBfdSessMapType,\
   pau1FsMIBfdSessMapPointer,\
   pau1FsMIBfdSessMapPointerLen,\
   pau1FsMIBfdSessCardNumber,\
   pau1FsMIBfdSessSlotNumber,\
   pau1FsMIBfdClearStats)\
  {\
  if (pau1FsMIStdBfdSessIndex != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessIndex = *(UINT4 *) (pau1FsMIStdBfdSessIndex);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessIndex = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessVersionNumber != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessVersionNumber = *(UINT4 *) (pau1FsMIStdBfdSessVersionNumber);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessVersionNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessVersionNumber = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessType = *(INT4 *) (pau1FsMIStdBfdSessType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessType = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDestinationUdpPort != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessDestinationUdpPort = *(UINT4 *) (pau1FsMIStdBfdSessDestinationUdpPort);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDestinationUdpPort = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDestinationUdpPort = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessSourceUdpPort != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessSourceUdpPort = *(UINT4 *) (pau1FsMIStdBfdSessSourceUdpPort);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSourceUdpPort = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSourceUdpPort = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessEchoSourceUdpPort != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort = *(UINT4 *) (pau1FsMIStdBfdSessEchoSourceUdpPort);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessEchoSourceUdpPort = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessEchoSourceUdpPort = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessAdminStatus != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessAdminStatus = *(INT4 *) (pau1FsMIStdBfdSessAdminStatus);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAdminStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAdminStatus = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessOperMode != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessOperMode = *(INT4 *) (pau1FsMIStdBfdSessOperMode);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessOperMode = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessOperMode = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDemandModeDesiredFlag != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessDemandModeDesiredFlag = *(INT4 *) (pau1FsMIStdBfdSessDemandModeDesiredFlag);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDemandModeDesiredFlag = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDemandModeDesiredFlag = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessControlPlaneIndepFlag != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessControlPlaneIndepFlag = *(INT4 *) (pau1FsMIStdBfdSessControlPlaneIndepFlag);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessControlPlaneIndepFlag = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessControlPlaneIndepFlag = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessMultipointFlag != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessMultipointFlag = *(INT4 *) (pau1FsMIStdBfdSessMultipointFlag);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessMultipointFlag = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessMultipointFlag = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessInterface != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessInterface = *(INT4 *) (pau1FsMIStdBfdSessInterface);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessInterface = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessInterface = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessSrcAddrType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessSrcAddrType = *(INT4 *) (pau1FsMIStdBfdSessSrcAddrType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSrcAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSrcAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessSrcAddr != NULL)\
  {\
   MEMCPY (pBfdFsMIStdBfdSessTable->MibObject.au1FsMIStdBfdSessSrcAddr, pau1FsMIStdBfdSessSrcAddr, *(INT4 *)pau1FsMIStdBfdSessSrcAddrLen);\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessSrcAddrLen = *(INT4 *)pau1FsMIStdBfdSessSrcAddrLen;\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSrcAddr = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessSrcAddr = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDstAddrType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessDstAddrType = *(INT4 *) (pau1FsMIStdBfdSessDstAddrType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDstAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDstAddrType = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDstAddr != NULL)\
  {\
   MEMCPY (pBfdFsMIStdBfdSessTable->MibObject.au1FsMIStdBfdSessDstAddr, pau1FsMIStdBfdSessDstAddr, *(INT4 *)pau1FsMIStdBfdSessDstAddrLen);\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessDstAddrLen = *(INT4 *)pau1FsMIStdBfdSessDstAddrLen;\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDstAddr = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDstAddr = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessGTSM != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessGTSM = *(INT4 *) (pau1FsMIStdBfdSessGTSM);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessGTSM = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessGTSM = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessGTSMTTL != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessGTSMTTL = *(UINT4 *) (pau1FsMIStdBfdSessGTSMTTL);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessGTSMTTL = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessGTSMTTL = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDesiredMinTxInterval != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval = *(UINT4 *) (pau1FsMIStdBfdSessDesiredMinTxInterval);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDesiredMinTxInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDesiredMinTxInterval = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessReqMinRxInterval != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessReqMinRxInterval = *(UINT4 *) (pau1FsMIStdBfdSessReqMinRxInterval);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessReqMinRxInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessReqMinRxInterval = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessReqMinEchoRxInterval != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval = *(UINT4 *) (pau1FsMIStdBfdSessReqMinEchoRxInterval);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessReqMinEchoRxInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessReqMinEchoRxInterval = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessDetectMult != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessDetectMult = *(UINT4 *) (pau1FsMIStdBfdSessDetectMult);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDetectMult = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessDetectMult = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessAuthPresFlag != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessAuthPresFlag = *(INT4 *) (pau1FsMIStdBfdSessAuthPresFlag);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthPresFlag = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthPresFlag = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessAuthenticationType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessAuthenticationType = *(INT4 *) (pau1FsMIStdBfdSessAuthenticationType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationType = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessAuthenticationKeyID != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessAuthenticationKeyID = *(INT4 *) (pau1FsMIStdBfdSessAuthenticationKeyID);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationKeyID = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationKeyID = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessAuthenticationKey != NULL)\
  {\
   MEMCPY (pBfdFsMIStdBfdSessTable->MibObject.au1FsMIStdBfdSessAuthenticationKey, pau1FsMIStdBfdSessAuthenticationKey, *(INT4 *)pau1FsMIStdBfdSessAuthenticationKeyLen);\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen = *(INT4 *)pau1FsMIStdBfdSessAuthenticationKeyLen;\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationKey = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessAuthenticationKey = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessStorType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessStorType = *(INT4 *) (pau1FsMIStdBfdSessStorType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessStorType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessStorType = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdSessRowStatus != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessRowStatus = *(INT4 *) (pau1FsMIStdBfdSessRowStatus);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdSessRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIStdBfdContextId = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessRole != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessRole = *(INT4 *) (pau1FsMIBfdSessRole);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessRole = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessRole = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessMode != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessMode = *(INT4 *) (pau1FsMIBfdSessMode);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMode = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMode = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessRemoteDiscr != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIBfdSessRemoteDiscr = *(UINT4 *) (pau1FsMIBfdSessRemoteDiscr);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessRemoteDiscr = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessRemoteDiscr = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessEXPValue != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIBfdSessEXPValue = *(UINT4 *) (pau1FsMIBfdSessEXPValue);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessEXPValue = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessEXPValue = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessTmrNegotiate != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessTmrNegotiate = *(INT4 *) (pau1FsMIBfdSessTmrNegotiate);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessTmrNegotiate = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessTmrNegotiate = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessOffld != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessOffld = *(INT4 *) (pau1FsMIBfdSessOffld);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessOffld = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessOffld = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessEncapType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessEncapType = *(INT4 *) (pau1FsMIBfdSessEncapType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessEncapType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessEncapType = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessMapType != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessMapType = *(INT4 *) (pau1FsMIBfdSessMapType);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMapType = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMapType = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessMapPointer != NULL)\
  {\
   BfdUtlConvertStringtoOid (pBfdFsMIStdBfdSessTable,\
       (UINT4 *)pau1FsMIBfdSessMapPointer,(INT4 *)pau1FsMIBfdSessMapPointerLen);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMapPointer = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessMapPointer = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessCardNumber != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIBfdSessCardNumber = *(UINT4 *) (pau1FsMIBfdSessCardNumber);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessCardNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessCardNumber = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdSessSlotNumber != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIBfdSessSlotNumber = *(UINT4 *) (pau1FsMIBfdSessSlotNumber);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessSlotNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdSessSlotNumber = OSIX_FALSE;\
  }\
  if (pau1FsMIBfdClearStats != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdClearStats = *(INT4 *) (pau1FsMIBfdClearStats);\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdClearStats = OSIX_TRUE;\
  }\
  else\
  {\
   pBfdIsSetFsMIStdBfdSessTable->bFsMIBfdClearStats = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMIStdBfdSessTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDSESSTABLE_INDEX(pBfdFsMIStdBfdSessTable,\
   pau1FsMIStdBfdContextId,\
   pau1FsMIStdBfdSessIndex)\
  {\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
  }\
  if (pau1FsMIStdBfdSessIndex != NULL)\
  {\
   pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessIndex = *(UINT4 *) (pau1FsMIStdBfdSessIndex);\
  }\
  }




/* Macro used to fill the index values in  structure for FsMIStdBfdSessDiscMapTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDSESSDISCMAPTABLE_INDEX(pBfdFsMIStdBfdSessDiscMapTable,\
   pau1FsMIStdBfdContextId,\
   pau1FsMIStdBfdSessDiscriminator)\
  {\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdSessDiscMapTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
  }\
  if (pau1FsMIStdBfdSessDiscriminator != NULL)\
  {\
   pBfdFsMIStdBfdSessDiscMapTable->MibObject.u4FsMIStdBfdSessDiscriminator = *(UINT4 *) (pau1FsMIStdBfdSessDiscriminator);\
  }\
  }




/* Macro used to fill the index values in  structure for FsMIStdBfdSessIpMapTable 
 using the input given in def file */

#define  BFD_FILL_FSMISTDBFDSESSIPMAPTABLE_INDEX(pBfdFsMIStdBfdSessIpMapTable,\
   pau1FsMIStdBfdContextId,\
   pau1FsMIStdBfdSessInterface,\
   pau1FsMIStdBfdSessSrcAddrType,\
   pau1FsMIStdBfdSessSrcAddr,\
   pau1FsMIStdBfdSessDstAddrType,\
   pau1FsMIStdBfdSessDstAddr)\
  {\
  if (pau1FsMIStdBfdContextId != NULL)\
  {\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.u4FsMIStdBfdContextId = *(UINT4 *) (pau1FsMIStdBfdContextId);\
  }\
  if (pau1FsMIStdBfdSessInterface != NULL)\
  {\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.i4FsMIStdBfdSessInterface = *(INT4 *) (pau1FsMIStdBfdSessInterface);\
  }\
  if (pau1FsMIStdBfdSessSrcAddrType != NULL)\
  {\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.i4FsMIStdBfdSessSrcAddrType = *(INT4 *) (pau1FsMIStdBfdSessSrcAddrType);\
  }\
  if (pau1FsMIStdBfdSessSrcAddr != NULL)\
  {\
   MEMCPY (pBfdFsMIStdBfdSessIpMapTable->MibObject.au1FsMIStdBfdSessSrcAddr, pau1FsMIStdBfdSessSrcAddr, *(INT4 *)pau1FsMIStdBfdSessSrcAddrLen);\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.i4FsMIStdBfdSessSrcAddrLen = *(INT4 *)pau1FsMIStdBfdSessSrcAddrLen;\
  }\
  if (pau1FsMIStdBfdSessDstAddrType != NULL)\
  {\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.i4FsMIStdBfdSessDstAddrType = *(INT4 *) (pau1FsMIStdBfdSessDstAddrType);\
  }\
  if (pau1FsMIStdBfdSessDstAddr != NULL)\
  {\
   MEMCPY (pBfdFsMIStdBfdSessIpMapTable->MibObject.au1FsMIStdBfdSessDstAddr, pau1FsMIStdBfdSessDstAddr, *(INT4 *)pau1FsMIStdBfdSessDstAddrLen);\
   pBfdFsMIStdBfdSessIpMapTable->MibObject.i4FsMIStdBfdSessDstAddrLen = *(INT4 *)pau1FsMIStdBfdSessDstAddrLen;\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsMIStdBfdContextName*/

#define  BFD_FILL_FSMISTDBFDCONTEXTNAME(pFsMIStdBfdContextName,\
   pau1FsMIStdBfdContextName)\
  {\
  if (pau1FsMIStdBfdContextName != NULL)\
  {\
   STRCPY (pFsMIStdBfdContextName, pau1FsMIStdBfdContextName);\
  }\
  }




