/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbfdlwg.h,v 1.1 2010/10/14 22:07:05 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetBfdAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2BfdAdminStatus ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2BfdAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdSessNotificationsEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetBfdSessNotificationsEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2BfdSessNotificationsEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2BfdSessNotificationsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for BfdSessTable. */
INT1
nmhValidateIndexInstanceBfdSessTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for BfdSessTable  */

INT1
nmhGetFirstIndexBfdSessTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBfdSessTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdSessVersionNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessDiscriminator ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessRemoteDiscr ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessDestinationUdpPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessSourceUdpPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessEchoSourceUdpPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessRemoteHeardFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessDiag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessOperMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessDemandModeDesiredFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessControlPlaneIndepFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessMultipointFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessInterface ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessSrcAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessSrcAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBfdSessDstAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessDstAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBfdSessGTSM ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessGTSMTTL ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessDesiredMinTxInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessReqMinRxInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessReqMinEchoRxInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessDetectMult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessNegotiatedInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessNegotiatedEchoInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessNegotiatedDetectMult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessAuthPresFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessAuthenticationType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessAuthenticationKeyID ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessAuthenticationKey ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBfdSessStorType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetBfdSessVersionNumber ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessDestinationUdpPort ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessSourceUdpPort ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessEchoSourceUdpPort ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessOperMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessDemandModeDesiredFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessControlPlaneIndepFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessMultipointFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessInterface ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessSrcAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessSrcAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetBfdSessDstAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessDstAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetBfdSessGTSM ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessGTSMTTL ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessDesiredMinTxInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessReqMinRxInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessReqMinEchoRxInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessDetectMult ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetBfdSessAuthPresFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessAuthenticationType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessAuthenticationKeyID ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessAuthenticationKey ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetBfdSessStorType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBfdSessRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2BfdSessVersionNumber ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessDestinationUdpPort ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessSourceUdpPort ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessEchoSourceUdpPort ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessAdminStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessOperMode ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessDemandModeDesiredFlag ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessControlPlaneIndepFlag ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessMultipointFlag ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessInterface ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessSrcAddrType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessSrcAddr ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2BfdSessDstAddrType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessDstAddr ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2BfdSessGTSM ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessGTSMTTL ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessDesiredMinTxInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessReqMinRxInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessReqMinEchoRxInterval ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessDetectMult ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2BfdSessAuthPresFlag ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessAuthenticationType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessAuthenticationKeyID ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessAuthenticationKey ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2BfdSessStorType ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2BfdSessRowStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2BfdSessTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for BfdSessPerfTable. */
INT1
nmhValidateIndexInstanceBfdSessPerfTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for BfdSessPerfTable  */

INT1
nmhGetFirstIndexBfdSessPerfTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBfdSessPerfTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdSessPerfCtrlPktIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfCtrlPktOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfCtrlPktDrop ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfCtrlPktDropLastTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfEchoPktIn ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfEchoPktOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfEchoPktDrop ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfEchoPktDropLastTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessUpTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfLastSessDownTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfLastCommLostDiag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBfdSessPerfSessUpCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfDiscTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBfdSessPerfCtrlPktInHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetBfdSessPerfCtrlPktOutHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetBfdSessPerfCtrlPktDropHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetBfdSessPerfEchoPktInHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetBfdSessPerfEchoPktOutHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetBfdSessPerfEchoPktDropHC ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for BfdSessDiscMapTable. */
INT1
nmhValidateIndexInstanceBfdSessDiscMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for BfdSessDiscMapTable  */

INT1
nmhGetFirstIndexBfdSessDiscMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBfdSessDiscMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdSessDiscMapIndex ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for BfdSessIpMapTable. */
INT1
nmhValidateIndexInstanceBfdSessIpMapTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for BfdSessIpMapTable  */

INT1
nmhGetFirstIndexBfdSessIpMapTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBfdSessIpMapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBfdSessIpMapIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));
