/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdprotg.h,v 1.4 2012/01/24 13:28:28 siva Exp $
*
* This file contains prototypes for functions defined in Bfd.
*********************************************************************/




PUBLIC INT4 BfdUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 BfdFsMIStdBfdGlobalConfigTableCreate PROTO ((VOID));

PUBLIC INT4 FsMIStdBfdGlobalConfigTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 BfdFsMIStdBfdSessTableCreate PROTO ((VOID));

PUBLIC INT4 FsMIStdBfdSessTableRBCmp PROTO ((tRBElem *, tRBElem *));


INT4 BfdUtlConvertStringtoOid PROTO((tBfdFsMIStdBfdSessEntry *, UINT4 *, INT4 *));

PUBLIC INT4 BfdFsMIStdBfdSessDiscMapTableCreate PROTO ((VOID));

PUBLIC INT4 FsMIStdBfdSessDiscMapTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 BfdFsMIStdBfdSessIpMapTableCreate PROTO ((VOID));

PUBLIC INT4 FsMIStdBfdSessIpMapTableRBCmp PROTO ((tRBElem *, tRBElem *));
tBfdFsMIStdBfdGlobalConfigTableEntry * BfdFsMIStdBfdGlobalConfigTableCreateApi PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *));
tBfdFsMIStdBfdSessEntry * BfdFsMIStdBfdSessTableCreateApi PROTO ((tBfdFsMIStdBfdSessEntry *));
tBfdFsMIStdBfdSessDiscMapEntry * BfdFsMIStdBfdSessDiscMapTableCreateApi PROTO ((tBfdFsMIStdBfdSessDiscMapEntry *));
tBfdFsMIStdBfdSessIpMapEntry * BfdFsMIStdBfdSessIpMapTableCreateApi PROTO ((tBfdFsMIStdBfdSessIpMapEntry *));
tBfdFsMIStdBfdGlobalConfigTableEntry * BfdGetFirstFsMIStdBfdGlobalConfigTable PROTO ((VOID));
tBfdFsMIStdBfdGlobalConfigTableEntry * BfdGetNextFsMIStdBfdGlobalConfigTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *));
tBfdFsMIStdBfdGlobalConfigTableEntry * BfdGetFsMIStdBfdGlobalConfigTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdGetAllFsMIStdBfdGlobalConfigTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdGetAllUtlFsMIStdBfdGlobalConfigTable PROTO((tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdTestAllFsMIStdBfdGlobalConfigTable PROTO ((UINT4 *, tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdSetAllFsMIStdBfdGlobalConfigTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdSetAllFsMIStdBfdGlobalConfigTableTrigger PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *, INT4));
INT4 FsMIStdBfdGlobalConfigTableFilterInputs PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));
INT4 BfdUtilUpdateFsMIStdBfdGlobalConfigTable PROTO ((tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdFsMIStdBfdGlobalConfigTableEntry *, tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *));
tBfdFsMIStdBfdSessEntry * BfdGetFirstFsMIStdBfdSessTable PROTO ((VOID));
tBfdFsMIStdBfdSessEntry * BfdGetNextFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *));
tBfdFsMIStdBfdSessEntry * BfdGetFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *));
INT4 BfdGetAllFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *));
INT4 BfdGetAllUtlFsMIStdBfdSessTable PROTO((tBfdFsMIStdBfdSessEntry *, tBfdFsMIStdBfdSessEntry *));
INT4 BfdTestAllFsMIStdBfdSessTable PROTO ((UINT4 *, tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *, INT4 , INT4));
INT4 BfdSetAllFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *, INT4 , INT4 ));

INT4 BfdInitializeFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *));

INT4 BfdInitializeMibFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *));
INT4 BfdSetAllFsMIStdBfdSessTableTrigger PROTO ((tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *, INT4));
INT4 FsMIStdBfdSessTableFilterInputs PROTO ((tBfdFsMIStdBfdSessEntry *, tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *));
INT4 BfdUtilUpdateFsMIStdBfdSessTable PROTO ((tBfdFsMIStdBfdSessEntry *, tBfdFsMIStdBfdSessEntry *, tBfdIsSetFsMIStdBfdSessEntry *));
tBfdFsMIStdBfdSessDiscMapEntry * BfdGetFirstFsMIStdBfdSessDiscMapTable PROTO ((VOID));
tBfdFsMIStdBfdSessDiscMapEntry * BfdGetNextFsMIStdBfdSessDiscMapTable PROTO ((tBfdFsMIStdBfdSessDiscMapEntry *));
tBfdFsMIStdBfdSessDiscMapEntry * BfdGetFsMIStdBfdSessDiscMapTable PROTO ((tBfdFsMIStdBfdSessDiscMapEntry *));
INT4 BfdGetAllFsMIStdBfdSessDiscMapTable PROTO ((tBfdFsMIStdBfdSessDiscMapEntry *));
INT4 BfdGetAllUtlFsMIStdBfdSessDiscMapTable PROTO((tBfdFsMIStdBfdSessDiscMapEntry *, tBfdFsMIStdBfdSessDiscMapEntry *));
tBfdFsMIStdBfdSessIpMapEntry * BfdGetFirstFsMIStdBfdSessIpMapTable PROTO ((VOID));
tBfdFsMIStdBfdSessIpMapEntry * BfdGetNextFsMIStdBfdSessIpMapTable PROTO ((tBfdFsMIStdBfdSessIpMapEntry *));
tBfdFsMIStdBfdSessIpMapEntry * BfdGetFsMIStdBfdSessIpMapTable PROTO ((tBfdFsMIStdBfdSessIpMapEntry *));
INT4 BfdGetAllFsMIStdBfdSessIpMapTable PROTO ((tBfdFsMIStdBfdSessIpMapEntry *));
INT4 BfdGetAllUtlFsMIStdBfdSessIpMapTable PROTO((tBfdFsMIStdBfdSessIpMapEntry *, tBfdFsMIStdBfdSessIpMapEntry *));
INT4 BfdGetFsMIStdBfdContextName PROTO ((UINT1 *));

PUBLIC INT4 BfdLock (VOID);
PUBLIC INT4 BfdUnLock (VOID);

