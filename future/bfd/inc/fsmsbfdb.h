/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbfdb.h,v 1.3 2010/10/31 02:44:07 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSBFDB_H
#define _FSMSBFDB_H

UINT1 FsMIStdBfdGlobalConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdBfdSessTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdBfdSessPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdBfdSessDiscMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIStdBfdSessIpMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsmsbf [] ={1,3,6,1,4,1,29601,2,54};
tSNMP_OID_TYPE fsmsbfOID = {9, fsmsbf};


UINT4 FsMIStdBfdContextId [ ] ={1,3,6,1,4,1,29601,2,54,1,1,1,1};
UINT4 FsMIStdBfdAdminStatus [ ] ={1,3,6,1,4,1,29601,2,54,1,1,1,2};
UINT4 FsMIStdBfdSessNotificationsEnable [ ] ={1,3,6,1,4,1,29601,2,54,1,1,1,3};
UINT4 FsMIStdBfdSessIndex [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,1};
UINT4 FsMIStdBfdSessVersionNumber [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,2};
UINT4 FsMIStdBfdSessType [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,3};
UINT4 FsMIStdBfdSessDiscriminator [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,4};
UINT4 FsMIStdBfdSessRemoteDiscr [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,5};
UINT4 FsMIStdBfdSessDestinationUdpPort [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,6};
UINT4 FsMIStdBfdSessSourceUdpPort [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,7};
UINT4 FsMIStdBfdSessEchoSourceUdpPort [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,8};
UINT4 FsMIStdBfdSessAdminStatus [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,9};
UINT4 FsMIStdBfdSessState [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,10};
UINT4 FsMIStdBfdSessRemoteHeardFlag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,11};
UINT4 FsMIStdBfdSessDiag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,12};
UINT4 FsMIStdBfdSessOperMode [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,13};
UINT4 FsMIStdBfdSessDemandModeDesiredFlag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,14};
UINT4 FsMIStdBfdSessControlPlaneIndepFlag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,15};
UINT4 FsMIStdBfdSessMultipointFlag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,16};
UINT4 FsMIStdBfdSessInterface [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,17};
UINT4 FsMIStdBfdSessSrcAddrType [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,18};
UINT4 FsMIStdBfdSessSrcAddr [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,19};
UINT4 FsMIStdBfdSessDstAddrType [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,20};
UINT4 FsMIStdBfdSessDstAddr [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,21};
UINT4 FsMIStdBfdSessGTSM [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,22};
UINT4 FsMIStdBfdSessGTSMTTL [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,23};
UINT4 FsMIStdBfdSessDesiredMinTxInterval [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,24};
UINT4 FsMIStdBfdSessReqMinRxInterval [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,25};
UINT4 FsMIStdBfdSessReqMinEchoRxInterval [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,26};
UINT4 FsMIStdBfdSessDetectMult [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,27};
UINT4 FsMIStdBfdSessNegotiatedInterval [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,28};
UINT4 FsMIStdBfdSessNegotiatedEchoInterval [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,29};
UINT4 FsMIStdBfdSessNegotiatedDetectMult [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,30};
UINT4 FsMIStdBfdSessAuthPresFlag [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,31};
UINT4 FsMIStdBfdSessAuthenticationType [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,32};
UINT4 FsMIStdBfdSessAuthenticationKeyID [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,33};
UINT4 FsMIStdBfdSessAuthenticationKey [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,34};
UINT4 FsMIStdBfdSessStorType [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,35};
UINT4 FsMIStdBfdSessRowStatus [ ] ={1,3,6,1,4,1,29601,2,54,2,1,1,36};
UINT4 FsMIStdBfdSessPerfCtrlPktIn [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,1};
UINT4 FsMIStdBfdSessPerfCtrlPktOut [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,2};
UINT4 FsMIStdBfdSessPerfCtrlPktDrop [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,3};
UINT4 FsMIStdBfdSessPerfCtrlPktDropLastTime [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,4};
UINT4 FsMIStdBfdSessPerfEchoPktIn [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,5};
UINT4 FsMIStdBfdSessPerfEchoPktOut [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,6};
UINT4 FsMIStdBfdSessPerfEchoPktDrop [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,7};
UINT4 FsMIStdBfdSessPerfEchoPktDropLastTime [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,8};
UINT4 FsMIStdBfdSessUpTime [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,9};
UINT4 FsMIStdBfdSessPerfLastSessDownTime [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,10};
UINT4 FsMIStdBfdSessPerfLastCommLostDiag [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,11};
UINT4 FsMIStdBfdSessPerfSessUpCount [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,12};
UINT4 FsMIStdBfdSessPerfDiscTime [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,13};
UINT4 FsMIStdBfdSessPerfCtrlPktInHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,14};
UINT4 FsMIStdBfdSessPerfCtrlPktOutHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,15};
UINT4 FsMIStdBfdSessPerfCtrlPktDropHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,16};
UINT4 FsMIStdBfdSessPerfEchoPktInHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,17};
UINT4 FsMIStdBfdSessPerfEchoPktOutHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,18};
UINT4 FsMIStdBfdSessPerfEchoPktDropHC [ ] ={1,3,6,1,4,1,29601,2,54,2,2,1,19};
UINT4 FsMIStdBfdSessDiscMapIndex [ ] ={1,3,6,1,4,1,29601,2,54,2,3,1,1};
UINT4 FsMIStdBfdSessIpMapIndex [ ] ={1,3,6,1,4,1,29601,2,54,2,4,1,1};




tMbDbEntry fsmsbfMibEntry[]= {

{{13,FsMIStdBfdContextId}, GetNextIndexFsMIStdBfdGlobalConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdBfdGlobalConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdBfdAdminStatus}, GetNextIndexFsMIStdBfdGlobalConfigTable, FsMIStdBfdAdminStatusGet, FsMIStdBfdAdminStatusSet, FsMIStdBfdAdminStatusTest, FsMIStdBfdGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdGlobalConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsMIStdBfdSessNotificationsEnable}, GetNextIndexFsMIStdBfdGlobalConfigTable, FsMIStdBfdSessNotificationsEnableGet, FsMIStdBfdSessNotificationsEnableSet, FsMIStdBfdSessNotificationsEnableTest, FsMIStdBfdGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdGlobalConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsMIStdBfdSessIndex}, GetNextIndexFsMIStdBfdSessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessVersionNumber}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessVersionNumberGet, FsMIStdBfdSessVersionNumberSet, FsMIStdBfdSessVersionNumberTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "1"},

{{13,FsMIStdBfdSessType}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessTypeGet, FsMIStdBfdSessTypeSet, FsMIStdBfdSessTypeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDiscriminator}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDiscriminatorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessRemoteDiscr}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessRemoteDiscrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDestinationUdpPort}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDestinationUdpPortGet, FsMIStdBfdSessDestinationUdpPortSet, FsMIStdBfdSessDestinationUdpPortTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "0"},

{{13,FsMIStdBfdSessSourceUdpPort}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessSourceUdpPortGet, FsMIStdBfdSessSourceUdpPortSet, FsMIStdBfdSessSourceUdpPortTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "0"},

{{13,FsMIStdBfdSessEchoSourceUdpPort}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessEchoSourceUdpPortGet, FsMIStdBfdSessEchoSourceUdpPortSet, FsMIStdBfdSessEchoSourceUdpPortTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "0"},

{{13,FsMIStdBfdSessAdminStatus}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessAdminStatusGet, FsMIStdBfdSessAdminStatusSet, FsMIStdBfdSessAdminStatusTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessState}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessRemoteHeardFlag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessRemoteHeardFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessDiag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDiagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessOperMode}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessOperModeGet, FsMIStdBfdSessOperModeSet, FsMIStdBfdSessOperModeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDemandModeDesiredFlag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDemandModeDesiredFlagGet, FsMIStdBfdSessDemandModeDesiredFlagSet, FsMIStdBfdSessDemandModeDesiredFlagTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessControlPlaneIndepFlag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessControlPlaneIndepFlagGet, FsMIStdBfdSessControlPlaneIndepFlagSet, FsMIStdBfdSessControlPlaneIndepFlagTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessMultipointFlag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessMultipointFlagGet, FsMIStdBfdSessMultipointFlagSet, FsMIStdBfdSessMultipointFlagTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessInterface}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessInterfaceGet, FsMIStdBfdSessInterfaceSet, FsMIStdBfdSessInterfaceTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessSrcAddrType}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessSrcAddrTypeGet, FsMIStdBfdSessSrcAddrTypeSet, FsMIStdBfdSessSrcAddrTypeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessSrcAddr}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessSrcAddrGet, FsMIStdBfdSessSrcAddrSet, FsMIStdBfdSessSrcAddrTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDstAddrType}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDstAddrTypeGet, FsMIStdBfdSessDstAddrTypeSet, FsMIStdBfdSessDstAddrTypeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDstAddr}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDstAddrGet, FsMIStdBfdSessDstAddrSet, FsMIStdBfdSessDstAddrTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessGTSM}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessGTSMGet, FsMIStdBfdSessGTSMSet, FsMIStdBfdSessGTSMTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessGTSMTTL}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessGTSMTTLGet, FsMIStdBfdSessGTSMTTLSet, FsMIStdBfdSessGTSMTTLTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "0"},

{{13,FsMIStdBfdSessDesiredMinTxInterval}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDesiredMinTxIntervalGet, FsMIStdBfdSessDesiredMinTxIntervalSet, FsMIStdBfdSessDesiredMinTxIntervalTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessReqMinRxInterval}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessReqMinRxIntervalGet, FsMIStdBfdSessReqMinRxIntervalSet, FsMIStdBfdSessReqMinRxIntervalTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessReqMinEchoRxInterval}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessReqMinEchoRxIntervalGet, FsMIStdBfdSessReqMinEchoRxIntervalSet, FsMIStdBfdSessReqMinEchoRxIntervalTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDetectMult}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessDetectMultGet, FsMIStdBfdSessDetectMultSet, FsMIStdBfdSessDetectMultTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessNegotiatedInterval}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessNegotiatedIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessNegotiatedEchoInterval}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessNegotiatedEchoIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessNegotiatedDetectMult}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessNegotiatedDetectMultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessAuthPresFlag}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessAuthPresFlagGet, FsMIStdBfdSessAuthPresFlagSet, FsMIStdBfdSessAuthPresFlagTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdBfdSessAuthenticationType}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessAuthenticationTypeGet, FsMIStdBfdSessAuthenticationTypeSet, FsMIStdBfdSessAuthenticationTypeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "-1"},

{{13,FsMIStdBfdSessAuthenticationKeyID}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessAuthenticationKeyIDGet, FsMIStdBfdSessAuthenticationKeyIDSet, FsMIStdBfdSessAuthenticationKeyIDTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, "-1"},

{{13,FsMIStdBfdSessAuthenticationKey}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessAuthenticationKeyGet, FsMIStdBfdSessAuthenticationKeySet, FsMIStdBfdSessAuthenticationKeyTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessStorType}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessStorTypeGet, FsMIStdBfdSessStorTypeSet, FsMIStdBfdSessStorTypeTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessRowStatus}, GetNextIndexFsMIStdBfdSessTable, FsMIStdBfdSessRowStatusGet, FsMIStdBfdSessRowStatusSet, FsMIStdBfdSessRowStatusTest, FsMIStdBfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdBfdSessTableINDEX, 2, 0, 1, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktIn}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktOut}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktDrop}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktDropGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktDropLastTime}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktDropLastTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktIn}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktOut}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktDrop}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktDropGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktDropLastTime}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktDropLastTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessUpTime}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfLastSessDownTime}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfLastSessDownTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfLastCommLostDiag}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfLastCommLostDiagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfSessUpCount}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfSessUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfDiscTime}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfDiscTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktInHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktInHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktOutHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktOutHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfCtrlPktDropHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfCtrlPktDropHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktInHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktInHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktOutHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktOutHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessPerfEchoPktDropHC}, GetNextIndexFsMIStdBfdSessPerfTable, FsMIStdBfdSessPerfEchoPktDropHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMIStdBfdSessPerfTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessDiscMapIndex}, GetNextIndexFsMIStdBfdSessDiscMapTable, FsMIStdBfdSessDiscMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessDiscMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdBfdSessIpMapIndex}, GetNextIndexFsMIStdBfdSessIpMapTable, FsMIStdBfdSessIpMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIStdBfdSessIpMapTableINDEX, 6, 0, 0, NULL},
};
tMibData fsmsbfEntry = { 60, fsmsbfMibEntry };

#endif /* _FSMSBFDB_H */

