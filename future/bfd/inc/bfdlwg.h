/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdlwg.h,v 1.3 2013/07/13 12:43:25 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIStdBfdGlobalConfigTable. */
INT1
nmhValidateIndexInstanceFsMIStdBfdGlobalConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdBfdGlobalConfigTable  */

INT1
nmhGetFirstIndexFsMIStdBfdGlobalConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdBfdGlobalConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIBfdSystemConfigTable. */
INT1
nmhValidateIndexInstanceFsMIBfdSystemConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBfdSystemConfigTable  */

INT1
nmhGetFirstIndexFsMIBfdSystemConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBfdSystemConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIBfdGblSessionConfigTable. */
INT1
nmhValidateIndexInstanceFsMIBfdGblSessionConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBfdGblSessionConfigTable  */

INT1
nmhGetFirstIndexFsMIBfdGblSessionConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBfdGblSessionConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIBfdStatisticsTable. */
INT1
nmhValidateIndexInstanceFsMIBfdStatisticsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBfdStatisticsTable  */

INT1
nmhGetFirstIndexFsMIBfdStatisticsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBfdStatisticsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdBfdAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessNotificationsEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdTraceLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdTrapEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdGblSessOperMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdGblReqMinRxIntvl ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdGblDetectMult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdGblSlowTxIntvl ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdMemAllocFailure ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdInputQOverFlows ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdClrGblStats ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdClrAllSessStats ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdBfdAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessNotificationsEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdTraceLevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdTrapEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdGblSessOperMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdGblDesiredMinTxIntvl ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdGblReqMinRxIntvl ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdGblDetectMult ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdGblSlowTxIntvl ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdClrGblStats ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdClrAllSessStats ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdBfdAdminStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessNotificationsEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSystemControl ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdTraceLevel ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdTrapEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdGblSessOperMode ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdGblDesiredMinTxIntvl ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdGblReqMinRxIntvl ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdGblDetectMult ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdGblSlowTxIntvl ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdClrGblStats ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdClrAllSessStats ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdBfdGlobalConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBfdSystemConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBfdGblSessionConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBfdStatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdBfdSessTable. */
INT1
nmhValidateIndexInstanceFsMIStdBfdSessTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdBfdSessTable  */

INT1
nmhGetFirstIndexFsMIStdBfdSessTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdBfdSessTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIStdBfdSessPerfTable. */
INT1
nmhValidateIndexInstanceFsMIStdBfdSessPerfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdBfdSessPerfTable  */

INT1
nmhGetFirstIndexFsMIStdBfdSessPerfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdBfdSessPerfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIBfdSessionTable. */
INT1
nmhValidateIndexInstanceFsMIBfdSessionTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBfdSessionTable  */

INT1
nmhGetFirstIndexFsMIBfdSessionTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBfdSessionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto Validate Index Instance for FsMIBfdSessPerfTable. */
INT1
nmhValidateIndexInstanceFsMIBfdSessPerfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBfdSessPerfTable  */

INT1
nmhGetFirstIndexFsMIBfdSessPerfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBfdSessPerfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdBfdSessVersionNumber ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessDiscriminator ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessRemoteDiscr ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessDestinationUdpPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessSourceUdpPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessEchoSourceUdpPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessAdminStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessRemoteHeardFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessDiag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessOperMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessDemandModeDesiredFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessControlPlaneIndepFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessMultipointFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessInterface ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessSrcAddrType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessSrcAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdBfdSessDstAddrType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessDstAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdBfdSessGTSM ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessGTSMTTL ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessDesiredMinTxInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessReqMinRxInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessReqMinEchoRxInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessDetectMult ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessNegotiatedInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessNegotiatedEchoInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessNegotiatedDetectMult ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessAuthPresFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessAuthenticationType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessAuthenticationKeyID ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessAuthenticationKey ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdBfdSessStorType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktIn ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktOut ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDrop ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDropLastTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktIn ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktOut ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktDrop ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktDropLastTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessUpTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfLastSessDownTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfLastCommLostDiag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdBfdSessPerfSessUpCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfDiscTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktInHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktOutHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDropHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktInHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktOutHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIStdBfdSessPerfEchoPktDropHC ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMIBfdSessRole ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessRemoteDiscr ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessEXPValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessTmrNegotiate ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessOffld ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessEncapType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessAdminCtrlReq ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessAdminCtrlErrReason ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessMapType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBfdSessMapPointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIBfdSessCardNumber ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessSlotNumber ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessRegisteredClients ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessPerfCCPktIn ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessPerfCCPktOut ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessPerfCVPktIn ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessPerfCVPktOut ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessMisDefCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessLocDefCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessRdiInCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdSessRdiOutCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIBfdClearStats ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdBfdSessVersionNumber ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessDestinationUdpPort ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessSourceUdpPort ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessEchoSourceUdpPort ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessAdminStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessOperMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessDemandModeDesiredFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessControlPlaneIndepFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessMultipointFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessInterface ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessSrcAddrType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessSrcAddr ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdBfdSessDstAddrType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessDstAddr ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdBfdSessGTSM ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessGTSMTTL ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessDesiredMinTxInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessReqMinRxInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessReqMinEchoRxInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessDetectMult ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdBfdSessAuthPresFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessAuthenticationType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessAuthenticationKeyID ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessAuthenticationKey ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdBfdSessStorType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdBfdSessRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessRole ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessRemoteDiscr ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdSessEXPValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdSessTmrNegotiate ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessOffld ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessEncapType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessMapType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBfdSessMapPointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMIBfdSessCardNumber ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdSessSlotNumber ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIBfdClearStats ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdBfdSessVersionNumber ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessDestinationUdpPort ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessSourceUdpPort ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessEchoSourceUdpPort ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessAdminStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessOperMode ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessDemandModeDesiredFlag ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessControlPlaneIndepFlag ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessMultipointFlag ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessInterface ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessSrcAddrType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessSrcAddr ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdBfdSessDstAddrType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessDstAddr ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdBfdSessGTSM ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessGTSMTTL ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessDesiredMinTxInterval ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessReqMinRxInterval ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessReqMinEchoRxInterval ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessDetectMult ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdBfdSessAuthPresFlag ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessAuthenticationType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessAuthenticationKeyID ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessAuthenticationKey ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdBfdSessStorType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdBfdSessRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessRole ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessMode ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessRemoteDiscr ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdSessEXPValue ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdSessTmrNegotiate ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessOffld ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessEncapType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessMapType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBfdSessMapPointer ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMIBfdSessCardNumber ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdSessSlotNumber ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIBfdClearStats ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdBfdSessTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBfdSessionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBfdSessPerfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdBfdSessDiscMapTable. */
INT1
nmhValidateIndexInstanceFsMIStdBfdSessDiscMapTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdBfdSessDiscMapTable  */

INT1
nmhGetFirstIndexFsMIStdBfdSessDiscMapTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdBfdSessDiscMapTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdBfdSessDiscMapIndex ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdBfdSessIpMapTable. */
INT1
nmhValidateIndexInstanceFsMIStdBfdSessIpMapTable ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdBfdSessIpMapTable  */

INT1
nmhGetFirstIndexFsMIStdBfdSessIpMapTable ARG_LIST((UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdBfdSessIpMapTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdBfdSessIpMapIndex ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
