/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdtmr.h,v 1.4 2015/09/11 09:50:43 siva Exp $
*
* Description: contains prototypes for timer header
*********************************************************************/

#ifndef __BFDTMR_H__
#define __BFDTMR_H__



/* constants for timer types */
typedef enum {
    BFD_TRANSMIT_TMR =0,
    BFD_DETECTION_TMR,
 BFD_ADDR_COMP_TMR,
    BFD_MAX_TMRS 
}tBfdTmrId;

/* constants for sub timer types */
typedef enum {
    BFD_TRANSMIT_SLOW_TMR =0, /* Valid for Tx timer */
    BFD_TRANSMIT_FAST_TMR,    /* Valid for Tx timer */
    BFD_DETECTION_SLOW_TMR,   /* Valid for Detection timer */
    BFD_DETECTION_FAST_TMR,   /* Valid for Detection timer */
    BFD_DETECTION_OFFLOAD_TMR,  /* Offload timer, valid for detect timer */
    BFD_MAX_SUB_TMRS 
}tBfdSubTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _BFD_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tBfdTmrDesc;

typedef struct _BFD_TIMER {
       tTmrBlk         BfdTmrBlk;
       tBfdSubTmrId   eBfdSubTmrId;
} tBfdTmr;




#endif  /* __BFDTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdtmr.h                      */
/*-----------------------------------------------------------------------*/
