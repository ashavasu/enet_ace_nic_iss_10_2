/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdinc.h,v 1.12 2014/04/29 13:28:37 siva Exp $
 *
 * Description: This file contains include files of bfd module.
 *******************************************************************/

#ifndef __BFDINC_H__
#define __BFDINC_H__ 

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "iss.h"

#include "params.h"
#include "bfdg.h"
#include "tcp.h"
#include "ip.h"
#include "ipv6.h"
#include "ip6util.h"
#include "cfa.h"
#include "cfacli.h"
#include "arp.h"
#include "fssocket.h"
#include "l2iwf.h"
#include "fssyslog.h"
#include "fm.h"
#include "rtm.h"
#include "vcm.h"
#include "mplsapi.h"
#include "dbutil.h"

#include "lspp.h"
#include "bfd.h"
#include "bfdtmr.h"
#include "bfddefn.h"
#include "bfdtdfsg.h"
#include "bfdtdfs.h"
#include "bfdred.h"
#include "bfdtrc.h"
#include "bfdnp.h"
#include "npapi.h"
#ifdef __BFDMAIN_C__


#include "bfdglob.h"
#include "bfdlwg.h"
#include "bfddefg.h"
#include "bfdsz.h"
#include "bfdwrg.h"
#include "fsbfdlwg.h"
#include "fsbfdwrg.h"
#include "stdbfdwrg.h"

#else

#include "bfdextn.h"
#include "bfdmibclig.h"
#include "bfdlwg.h"
#include "bfddefg.h"
#include "bfdsz.h"
#include "bfdwrg.h"
#include "fsbfdlwg.h"
#include "fsbfdwrg.h"
#include "stdbfdwrg.h"
#include "stdbfdlwg.h"


#endif /* __BFDMAIN_C__*/


#ifdef __BFDSEM_C__
#include "bfdsem.h"
#endif


#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#include "bfdcli.h"
#include "bfdprot.h"
#include "bfdprotg.h"
#include "bfdmacro.h"

#endif   /* __BFDINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file bfdinc.h                       */
/*-----------------------------------------------------------------------*/

