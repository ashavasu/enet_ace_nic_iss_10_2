/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdsz.h,v 1.7 2012/03/21 13:03:56 siva Exp $
 *
 * Description: This file contains Sizing parameters
 *******************************************************************/

enum {
    MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID,
    MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE_SIZING_ID,
    MAX_BFD_FSMISTDBFDSESSIPMAPTABLE_SIZING_ID,
    MAX_BFD_FSMISTDBFDSESSTABLE_SIZING_ID,
    MAX_BFD_QUEMSG_SIZING_ID,
    MAX_BFD_MPLSPATHINFOSTRUCT_SIZING_ID,
    MAX_BFD_EXTOUTPARAMS_SIZING_ID,
    MAX_BFD_EXTINPARAMS_SIZING_ID,
    MAX_BFD_PKTBUF_SIZING_ID,
    MAX_BFD_OFFLOAD_PARAMS_SIZING_ID,
    BFD_MAX_SIZING_ID
};


#ifdef  _BFDSZ_C
tMemPoolId BFDMemPoolIds[ BFD_MAX_SIZING_ID];
INT4  BfdSizingMemCreateMemPools(VOID);
VOID  BfdSizingMemDeleteMemPools(VOID);
INT4   BfdSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _BFDSZ_C  */
extern tMemPoolId BFDMemPoolIds[ ];
extern INT4  BfdSizingMemCreateMemPools(VOID);
extern VOID  BfdSizingMemDeleteMemPools(VOID);
#endif /*  _BFDSZ_C  */


#ifdef  _BFDSZ_C
tFsModSizingParams FsBFDSizingParams [] = {
{ "tBfdFsMIStdBfdGlobalConfigTableEntry", "MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE", sizeof(tBfdFsMIStdBfdGlobalConfigTableEntry),MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE, MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE,0 },
{ "tBfdFsMIStdBfdSessDiscMapEntry", "MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE", sizeof(tBfdFsMIStdBfdSessDiscMapEntry),MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE, MAX_BFD_FSMISTDBFDSESSDISCMAPTABLE,0 },
{ "tBfdFsMIStdBfdSessIpMapEntry", "MAX_BFD_FSMISTDBFDSESSIPMAPTABLE", sizeof(tBfdFsMIStdBfdSessIpMapEntry),MAX_BFD_FSMISTDBFDSESSIPMAPTABLE, MAX_BFD_FSMISTDBFDSESSIPMAPTABLE,0 },
{ "tBfdFsMIStdBfdSessEntry", "MAX_BFD_FSMISTDBFDSESSTABLE", sizeof(tBfdFsMIStdBfdSessEntry),MAX_BFD_FSMISTDBFDSESSTABLE, MAX_BFD_FSMISTDBFDSESSTABLE,0 },
{ "tBfdReqParams", "MAX_BFD_QUEMSG", sizeof(tBfdReqParams),MAX_BFD_QUEMSG, MAX_BFD_QUEMSG,0 },
{ "tBfdMplsPathInfo", "MAX_BFD_MPLSPATHINFOSTRUCT", sizeof(tBfdMplsPathInfo),MAX_BFD_MPLSPATHINFOSTRUCT, MAX_BFD_MPLSPATHINFOSTRUCT,0 },
{ "tBfdExtOutParams", "MAX_BFD_EXTOUTPARAMS", sizeof(tBfdExtOutParams),MAX_BFD_EXTOUTPARAMS, MAX_BFD_EXTOUTPARAMS,0 },
{ "tBfdExtInParams", "MAX_BFD_EXTINPARAMS", sizeof(tBfdExtInParams),MAX_BFD_EXTINPARAMS, MAX_BFD_EXTINPARAMS,0 },
{ "BFD_MAX_PKT_LEN", "MAX_BFD_PKTBUF", BFD_MAX_PKT_LEN,MAX_BFD_PKTBUF, MAX_BFD_PKTBUF,0 },
{ "tBfdParams", "MAX_BFD_OFFLOAD_PARAMS", sizeof(tBfdParams),MAX_BFD_OFFLOAD_PARAMS, MAX_BFD_OFFLOAD_PARAMS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _BFDSZ_C  */
extern tFsModSizingParams FsBFDSizingParams [];
#endif /*  _BFDSZ_C  */


