/********************************************************************
 * Copyright (C) Aricent Inc . All Rights Reserved
 *
 * $Id: bfdtrap.h,v 1.31 2015/01/14 12:27:39 siva Exp $
 *
 * Description: This file contains trap data structures defined for
 *              BFD  module.
 *********************************************************************/

#ifndef _BFD_TRAP_H
#define _BFD_TRAP_H

/*Traps,Traces and Syslog*/


#define BFD_TRC_MAX_SIZE ((sizeof(gaBfdEventLogNotify)/sizeof(tIssEventLogNotify)) - 1)
#define BFD_COMMON_TRC_MAX_SIZE ((sizeof(gaIssEventLogNotify)/sizeof(tIssEventLogNotify)) - 1)
tIssEventLogNotify  gaIssEventLogNotify[ ] = 
{
    /*Array Index :0 */
    /* BFD_OBTAINING_TASK_ID_FAILED*/
    {
        OS_RESOURCE_TRC ,
        SYSLOG_ALERT_LEVEL, 
        " Obtaining task id for task name'%s' failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :1 */
    /* BFD_SEM_CREATION_FAILED */ 
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s semaphore creation failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :2 */
    /* BFD_QUEUE_CREATION_FAILED */
    { 
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Q creation failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :3 */
    /* BFD_MEMPOOL_CREATION_FAILED */
    { 
        OS_RESOURCE_TRC ,
        SYSLOG_ALERT_LEVEL,
        " %s MemPool creation failed !!!\r\n",
        BFD_NO_TRAP 
    },

    /*Array Index :4 */
    /* BFD_MEMPOOL_INITIALISATION_FAILED */
    { 
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s MemPool initialization failed !!!\r\n",
        BFD_NO_TRAP 
    },

    /*Array Index :5 */
    /* BFD_TIMER_CREATION_FAILED*/
    { 
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Timer list creation failed !!!\r\n",
        BFD_NO_TRAP 
    },

    /*Array Index :6 */
    /* BFD_TIMER_INITI_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Timer initialization failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :7 */
    /* BFD_RM_REGISTRAION_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Registration with RM failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :8 */
    /* BFD_REDU_GLOBAL_INIT_FAILED */ 
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Redundancy global Info initialization failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :9 */
    /* BFD_DEFAULT_CONTEXT_CREATION_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Default context creation failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :10 */
    /* BFD_SEM_DELETION_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s semaphore deletion failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :11 */
    /* BFD_QUEUE_DELETION_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Q deletion failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :12 */
    /* BFD_MEMPOOL_DELETION_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s MemPool deletion failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :13 */
    /* BFD_MEMPOOL_DEINIT_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s MemPool de-initialization failed!!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :14 */
    /* BFD_TIMER_LIST_DELETION_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Timer list deletion failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :15 */
    /* BFD_TIMER_DEINIT_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Timer de-initialization failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :16 */
    /* BFD_RM_DEREGISTRATION_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s De-Registration with RM failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :17 */
    /* BFD_REDU_GLOBAL_DEINIT_FAILED*/
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Redundancy global Info de-initialization failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :18 */
    /* BFD_DEFAULT_CONTEXT_DELETION_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Default context deletion failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :19 */
    /* BFD_OSIX_QUEUE_SEND_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Osix Q send failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :20 */
    /* BFD_OSIX_EVENT_SEND_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Osix Event send failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :21 */
    /* BFD_OSIX_SEM_ACQ_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Acquiring Osix semaphore (0x%x) failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :22 */
    /* BFD_OSIX_SEM_REALEASE_FAILED */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Releasing Osix semaphore (0x%x) failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :23 */
    /* Q Memory allocation failure; Argument: Module name,
     * Table or Queue Name */
     /* BFD_MEM_POOL_ALLOCATE_FAIL */
    {
        OS_RESOURCE_TRC,
        SYSLOG_ERROR_LEVEL,
        " %s Unable to allocate memory from pool for %s\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :24 */
    /* Freeing Memory failure; Argument: Module name, Table or Queue Name */ 
    /* BFD_FREE_MEMORY_FAIL */
    { 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC), 
        SYSLOG_ALERT_LEVEL,
        " %s Free memory for %s failed !!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :25 */
    /* Database Creation Failure; Argument: Function name, Data Structure,
         Table name */ 
    /* BFD_DATABASE_CREATION_FAILED */
    {
        INIT_SHUT_TRC,
        SYSLOG_ALERT_LEVEL,
        " %s Creating %s for %s failed !!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :26 */
    /* Timer Started message; Argument:Module name, Timer Type  */ 
    /* BFD_TIMER_START */
    {
        OS_RESOURCE_TRC,
        SYSLOG_INFO_LEVEL,
        " %s Timer started for type %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :27 */
    /* BFD_TIMER_STOP */
    /* Timer Stop message; Argument:Module name, Timer Type  */ 
    {
        OS_RESOURCE_TRC,
        SYSLOG_INFO_LEVEL,
        " %s Timer stopped for type %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :28 */
    /* BFD_TIMER_LIST_CREATION_FAILED */
    /* Timer List creation Failure ; Argument: Module name, Timer Type*/
    { 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Timer List creation for type %d failed!!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :29 */
    /* BFD_TIMER_LIST_DELETION_FAILED */
    /* Timer List deletion Failure ; Argument: Module name, Timer Type*/
    /*{ 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Timer List deletion for type %d failed!!! \r\n",
        BFD_NO_TRAP
    },*/

    /*Array Index :30 */
    /* BFD_TIMER_START_FAILED */
    /* Timer Start Failure ; Argument: Module name, Timer Type*/ 
    { 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Timer start for type %d failed!!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :31 */
    /* BFD_TIMER_STOP_FAILED */
    /* Timer Stop Failure ; Argument: Module name, Timer Type*/ 
    { 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Timer stop for type %d failed!!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :32 */
    /* BFD_TIMER_RESTART_FAILED */
    /* Timer Re-start Failure ; Argument: Module name, Timer Type*/ 
    { 
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Timer re-start for type %d failed!!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :33 */
    /* BFD_MODULE_NOT_STARTED */
    /* Module not started ; Argument: Module name, Context name*/ 
    { 
        ( CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL,
        " %s not started in the context %s \r\n",
        BFD_NO_TRAP 
    },

    /*Array Index :34 */
    /* BFD_RECEIVED_QUEUE_EVENT */
    /* Received Queue Event; Argument: Module name */ 
    {
        CONTROL_PLANE_TRC,
        SYSLOG_INFO_LEVEL,
        " %s Received queue event !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :35 */
     /* BFD_RECEIVED_TIMER_EVENT */
    /* Received Timer Event; Argument: Module name */ 
    {
        CONTROL_PLANE_TRC,
        SYSLOG_INFO_LEVEL,
        " %s Received Timer event !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :36 */
    /* BFD_RECEIVED_TIMER_EXPIRY_EVENT */
    /* Received Timer Expiry Event; Argument: Module name */ 
    {
        CONTROL_PLANE_TRC,
        SYSLOG_INFO_LEVEL,
        " %s Received timerexpiry event !!! \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :37 */
    /* BFD_SOCKET_OPENING_FAILED */
    /* Socket Opening Failed ; Argument : Module Name, Type of socket */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Cannot Open %s socket \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 38*/
    /* BFD_SOCKET_CLOSING_FAILED */
    /* Socket Closing Failed ; Argument : Module Name, Type of socket */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Closing %s socket failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :39 */
    /* BFD_SOCKET_BINDING_FAILED */
    /* Bind Failed ; Argument : Module Name, Type of socket */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Binding %s socket failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :40 */
    /* BFD_LISTEN_FAILED */

    /* Listen Failed ; Argument : Module Name, Type of socket */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Cannot listen on local address !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :41 */
    /* BFD_SOCKET_SETOPTION_FAILED */

    /* SetSockOption setting failure ; Argument : Module Name, SetSock Option */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s setsockopt failed for the %s !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :42 */
     /* BFD_ADD_SELECT_IN_FD_FAILED */

    /* SelAddFd failure ; Argument : Module Name, File Descriptor */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Failed to add FD %d to select FD list !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 43*/
    /* BFD_READ_ON_SOCKET_FAILED */
    /* Read on socket failure ; Argument : Module Name, File Descriptor */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Read on socket %d failed  !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 44*/
    /* BFD_WRITE_ON_SOCKET_FAILED */
    /* Write on socket failure ; Argument : Module Name, File Descriptor */
    {
        (OS_RESOURCE_TRC | BFD_CRITICAL_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " %s Write on socket %d failed  !!!\r\n",
        BFD_NO_TRAP
    }
};

tIssEventLogNotify  gaBfdEventLogNotify[ ] =
{
    /*Array Index :0 */
    /* BFD_TRC_DETECT_TMR_EXP */
    /* Detection Time Expire ; Argument : Function name */
    {
        BFD_SESS_DOWN_TRC,
        SYSLOG_ALERT_LEVEL, 
        " [BFD][SESS_DOWN]Detection Timer Expired for Session Index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :1 */
    /* BFD_TRC_SESS_CREATE_FAIL */
    /* Session Creation Failure ; Argument : Function name */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Session Creation Failed !!!\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :2 */
    /* BFD_TRC_SESS_IDX_IN_USE */
    /* Session index exists ; Argument : Function name, Session Index */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Session Index %d Already in use\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :3 */
    /* BFD_TRC_LDISCR_IN_USE */
    /* Local Discriminatore Exists ;Argument : Function name,
     * Local Discriminator*/
    { 
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Local Discriminator %d Already in use\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :4 */
    /* BFD_TRC_RDISCR_IN_USE */
    /* Remote Discriminatore Exists ;Argument :  Remote Discriminator */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Remote Discriminator %d Already in use\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :5 */
    /* BFD_TRC_DISCR_GEN_FAIL */
    /* Discriminator Generation Failed ; Argument : Session Index */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Discriminator Generation Failed for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :6 */
    /* BFD_TRC_SESS_ESTAB */
    /* Session Established message ; Argument : Session Index */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][MGMT]Session Established for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :7 */
    /* BFD_TRC_POLL_INIT */
    /* Poll sequence initiation message ; Argument : Session Index  */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_INFO_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]Poll-Sequence Initiated for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :8 */
    /* BFD_TRC_POLL_TER */
    /* Poll sequence termination message ; Argument : Session Index */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_INFO_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]Poll-Sequence Terminated for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :9 */
    /* BFD_TRC_NEW_TX */
    /* Tx interval message ; Argument : Function name, Tx Interval,
     * Session Index */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_ALERT_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]New Desired Tx Interval: %ld for Session index - %d\r\n",
        BFD_NEG_TX_CHANGE_TRAP
    },

    /*Array Index :10 */
    /* BFD_TRC_NEW_RX */
    /* Rx interval message; Argument : Function name, Rx Interval,
     * Session Index  */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_ALERT_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]New Required Rx Interval: %ld for Session index - %d\r\n",
        BFD_NEG_TX_CHANGE_TRAP
    },

    /*Array Index :11 */
    /* BFD_TRC_NEW_DMULT */
    /* Detection Multiplier message ; Argument : Detection Multiplier,
     * Session Index */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_INFO_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]New Detection Multiplier Interval: %d for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :12 */
    /* BFD_TRC_DETECT_INTRVL */
    /* Negotiated Detection Time Interval message ; Argument : Negotiated
     * Detection Multiplier , Session Index  */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_ALERT_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]Local Detection Time Interval: %ld for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :13 */
    /* BFD_TRC_SEM */
    /* BFD State messages ;Argument : state event machine id, state , event */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d], STATE: %s, EVENT: %s\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :14 */
    /* BFD_TRC_SEM_EV_IMPOS */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d]: Event Impossible\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :15 */
    /* BFD_TRC_SEM_ST_INIT */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d]: STATE = BFD_STATE_INIT\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :16 */
    /* BFD_TRC_SEM_ST_UP */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d]: STATE = BFD_STATE_UP\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :17 */
    /* BFD_TRC_SEM_ST_DOWN */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d]: STATE = BFD_STATE_DOWN\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :18 */
    /* BFD_TRC_SEM_ST_AD */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]BFD SEM[%d]: STATE = BFD_STATE_ADMIN_DOWN\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :19 */
    /* BFD_TRC_TX_MISMATCH */
    /* Tx interval mismatch ;Argument : Tx Interval, Session Index */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Mis-match in received Desired Min Tx Interval: %ld"
         "for session index %d\r\n",
        BFD_ADMIN_CTRL_ERR_TRAP
    },

    /*Array Index :20 */
    /* BFD_TRC_VER_FAIL */
    /* Version validation failed ;Argument : Version*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Version Validation Failed. Received Version: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :21 */
    /* BFD_TRC_DIAG_FAIL */
    /* Diagnostic code invalid ; Argument :Diagnostic code, session index*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Invalid Diagnostic Code for session index %d.Received Values: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :22 */
    /* BFD_TRC_DEMAND_FAIL */
    /* Demand mode bit status message */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Demand Mode Bit Set. Demand Mode not Supported\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :23 */
    /* BFD_TRC_MBIT_SET */
    /* Multipoint bit status message ; */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Multipoint Bit Set\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :24 */
    /* BFD_TRC_DMULT_FAIL */
    /* Detection multiplier invalid ; Argument : Detection Multiplier */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Invalid Detecion Multiplier. Received Value: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :25 */
    /* BFD_TRC_INVALID_LEN */
    /* Invalid length ; Argument : Length*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Invalid Length. Received Value: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :26 */
    /* BFD_TRC_DISCR_MISMATCH */
    /* My discriminator mis match ; Argument : My Discriminator*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Mis-match in received My Discriminator: %ld\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :27 */
    /* BFD_TRC_INVALID_AUTH_TYPE */
    /* Auth type invalid ; Argument : Auth type*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Invalid Auth Type. Received Value: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :28 */
    /* BFD_TRC_INVALID_AUTH_LEN */
    /* Auth Length invalid ; Argument : Auth Length*/
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Invalid Auth Length. Received Value: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :29 */
    /* BFD_TRC_NO_AUTH */
    /* Auth field not present message ; */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][CTRL_PLANE]Authentication field not present. But Auth Flag Set\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :30 */
    /* BFD_TRC_PKT_DUMP */
    /* BFD Packect dump message */
    {
        BFD_PKT_DUMP_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][PKT_DUMP]BFD Version: %d,\
         State: %d ,\
         Auth Password: %d\r\n",
        BFD_NO_TRAP
    },

   /* {
        BFD_PKT_DUMP_TRC,
        SYSLOG_INFO_LEVEL, 
        " BFD Version: %d,\
         State: %d ,\
         Poll: %d,\
         Final: %d,\
         Control Plane Independent: %d,\
         Authentication Flag: %d,\
         Demand Mode: %d,\
         Multipoint Bit: %d,\
         Detecion Multiplier: %d,\
         Length: %d,\
         My Discriminator: %d,\
         Your Discriminator: %d,\
         Desired Min Tx Interval: %ld,\
         Required Min Rx Interval: %ld,\
         Required Min Echo Rx Interval: %ld,\
         Auth Type: %d,\
         Auth Length: %d,\
         Auth KeyID: %d,\
         Auth Password: %d\r\n",
        BFD_NO_TRAP
    },*/

    /*Array Index :31 */
    /* BFD_TRC_OFFLOAD_FAIL */
    /* Session offload Failed ; Argument : session index*/
    {
        (BFD_SESS_OFFLOAD_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][SESS_OFFLOAD][ALL_FAILURE]Session Offload failed for Session index - %d\r\n",
        BFD_NO_TRAP 
    },

    /*Array Index :32 */
    /* BFD_TRC_ST_CHANGE */
    /* State Transition message ; Argument : event, state, session id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD SEM[%d], EVENT: %s , State changed from %s to %s for"
            " session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :33 */
    /* BFD_TRC_OPER_MODE_FAIL */
    /* Operational mode error message ; Argument : session index*/
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Operational mode of peer is different for session index -%d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :34 */
    /* BFD_TRC_OPER_MODE_CHANGE */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]Operational mode changed for session index - %d without"
            " disabling BFD\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :35 */
    /* BFD_TRC_NO_CV_TYPE */
    /* CV type not enabled ; Argument : session index*/
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]CV type not enabled on PW \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :36 */
    /* BFD_TRC_SESS_DOWN_CARD_DETACH */
    /* Session failure due to card detach event; Argument : session index */
    {
        (BFD_SESS_OFFLOAD_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][SESS_OFFLOAD][ALL_FAILURE]BFD session down due to card detach event for Session index - %d\r\n",
        BFD_SESS_DOWN_TRAP 
    },

    /*Array Index :37 */
    /* BFD_TRC_LSPP_FEC_FAIL */
    /* FEC validation failure ; Argument : */
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][SESS_DOWN][ALL_FAILURE]LSP Ping FEC validation failed and no BFD session established\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :38 */
    /* BFD_TRC_NO_RESP */
    /* Peer response failure for DOWN indication ; Argument : session index*/
    {
        (BFD_SESS_DOWN_TRC | ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL, 
        " [BFD][SESS_DOWN][ALL_FAILURE]No Response from Peer for DOWN indication for"
            " session index - %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :39 */
    /* BFD_TRC_SESS_ESTAB_FAIL */
    /* BFD session establishemnt failed ; Argument : session index*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][MGMT]BFD Session Established failed for Session index - %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :40 */
    /* BFD_TRC_SESS_UP */
    /* BFD session up ; Argument : session index*/
    {
        BFD_MGMT_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][MGMT]BFD Session UP for Session index - %u in ContextId- %u\r\n",
        BFD_SESS_UP_TRAP
    },

    /*Array Index :41 */
    /* BFD_TRC_SESS_DOWN */
    /* BFD session down ; Argument : session index*/
    {
        BFD_MGMT_TRC,
        SYSLOG_CRITICAL_LEVEL, 
        " [BFD][MGMT]BFD Session DOWN for Session index - %u in ContextId - %u\r\n",
        BFD_SESS_DOWN_TRAP
    },

    /*Array Index :42 */
    /* BFD_TRC_SESS_ADMIN_DOWN */
    /* BFD session down ; Argument : session index*/
    {
        BFD_MGMT_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT]BFD Session ADMIN DOWN for Session index - %u in ContextId - %u\r\n",
        BFD_SESS_DOWN_TRAP
    },

    /*Array Index :43 */
    /* BFD_TRC_NO_CNTX */
    /* Context Id creation failure ; Argument :context id*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Context Id %d not created\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :44 */
    /* BFD_TRC_INVALID_CNTX */
    /* Invalid Context Id ; Argument : context id */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Invalid Context Id %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :45 */
    /* BFD_TRC_INVAL_SYS_CTRL */
    /* System control Invalid ; */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Invalid System Control \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :46 */
    /* BFD_TRC_INVAL_MOD_STAT */
    /* Module status invalid ; */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Invalid Module Status\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :47 */
    /* BFD_TRC_INVAL_TRC_LEN */
    /* Trace Input Length Invalid ; */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Invalid Trace Input length\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 48 */
    /* BFD_TRC_SEM_INIT */
    /* State Machine initialised ; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD state machine for session index %d initialized, state: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 49 */
    /* BFD_TRC_TX_TMR_FAIL */
    /* Transmit Timer start/restart failed ; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Transmit timer start/restart failed for session index %d"
                                                         "state: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :50 */
    /* BFD_TRC_DTMR_FAIL */
    /* Detection Timer start/restart failed ; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
         SYSLOG_INFO_LEVEL,
         " [BFD][CTRL_PLANE][ALL_FAILURE]Detection timer start/restart failed for session index %d"
                                                         " state: %d! \r\n",
         BFD_NO_TRAP
    },

    /*Array Index : 51*/
    /* BFD_TRC_TX_TMR_STOP_FAIL */
    /* Transmit timer stop failed; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
         SYSLOG_INFO_LEVEL,
         " [BFD][CTRL_PLANE][ALL_FAILURE]Transmit timer stop failed for session index %d state: %d \r\n",
         BFD_NO_TRAP
    },

    /*Array Index : 52 */
    /* BFD_TRC_DTMR_STOP_FAIL */
    /* Detection timer stop failed ; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Detection timer stop failed for session index %d state: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :53 */
    /* BFD_TRC_DTMR_RUNNING_IN_ST_DOWN */
    /* Detection timer state; Argument : Session Index, state*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Detection timer running in DOWN state for session index %d "
                                                       " state: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 54 */
    /* BFD_TRC_ENTRY_CREATE_FAIL */
    /* Entry cretaion failed ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Entry creation in BFD global config table for context id %d"
                                                                "failed \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 55 */
    /* BFD_TRC_GBL_TABLE_INIT_FAIL */
    /* global config table initialization failed ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD global config table initialization failed "
                                           "for context id: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 56 */
    /* BFD_TRC_EXT_CALL_FAIL */
    /*BFD Exit function call failed ; Argument : Context id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD Exit function call failed!! Context id: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 57 */
    /* BFD_TRC_GBL_ADD_FAIL */
    /* RB tree failed ;Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Addition to BFD global config table RB tree failed!!"
                                                   "context id: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 58 */
    /* BFD_TRC_CNTX_CREATE_FAIL */
    /* Context creation failed ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD context creation failed!! Context id: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 59*/
    /* BFD_TRC_NO_CNTX_ENTRY */
    /* Conetxt entry not found ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD context entry not found !! COntext id: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 60 */
    /* BFD_TRC_GBL_TBL_DEL_FAIL */
    /* Failed to delete entry ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Entry deletion in BFD global config table "
                                        "for context id %d failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 61 */
    /* BFD_TRC_SESS_TBL_DEL_FAIL */
    /* Failed to delete session table ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session table deletion failed for context id %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 62 */
    /* BFD_TRC_DISCR_MAP_TBL_DEL_FAIL */
    /* Failed to delete Session discriminator map table
     *  Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session discriminator map table deletion failed "
                                             "for context id %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 63 */
    /* BFD_TRC_SESS_INIT_FAIL */
    /* Core init data fail ; Argument : Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD Core session init data failed for sessiond id %d,"
                                                      "context id %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :  64*/
    /* BFD_TRC_SESS_INIT_PASS */
    /* Core init data success ; Argument : Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD Core session init data successful for sessiond id %d,"
                                                     "context id %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 65 */
    /* BFD_TRC_LSPP_BSTRP_FAIL */
    /* Bootstrap failed ; Argument :Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Bootstrap using LSP Ping failed for session id %d,"
                                                        "context id %d\r\n",
        BFD_BTSTRAP_FAIL_TRAP
    },

    /*Array Index :66  */
    /* BFD_TRC_PARAM_CHANGE_FAIL */
    /* Parameter change fail ; Argument : Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Handling BFD session parameter change failed for session id %d,"
                                                    "context id %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 67 */
    /* BFD_TRC_NO_PARAM_CHANGE */
    /* Output parameter change fail ; Argument : Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Output parameters cannot be changed since the session "
           "parameters change allowed flag is set to false for session id %d"
                                                          "context id %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index :68 */
    /* BFD_TRC_DISCR_FROM_LSPP */
    /* Discriminator received from LSPP ; Argument : Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Received discriminator: %d from LSP Ping for session id: %d"
                                                     "context id: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 69 */
    /* BFD_TRC_LSPP_BSTRP_DISCR_PRESENT */
    /* Received discriminator when remote descriminator is present
     *  Argument : Received Discriminator
     * Session Id,Context Id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Received discriminator: %d from LSP Ping for session id: %d "
               "context id: %d when session is wiating for any bootstrap" 
               "response but remote discriminator is already present\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 70 */
    /* BFD_TRC_LSPP_BSTRP_NO_SESS */
    /* Received discriminator when session is not existing
     *  Argument :Received Discriminator 
     * Session Id,Context Id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Received discriminator: %d from LSP Ping for session id: %d"
            "context id: %d when session is not existing in BFD\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 71 */
    /* BFD_TRC_LSPP_BSTRP_NO_OOB */
    /* Received discriminator when session type is not OOB
     * Argument : Received Discriminator
     *  Session Id,Context Id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Received discriminator: %d from LSP Ping for session id: %d "
            "context id: %d when session dosent have session type OOB \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 72*/
    /* BFD_TRC_LSPP_BSTRP_REQ */
    /* Bootstrap LSP Ping request ; Argument :  Session Id,Context Id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Bootstrap LSP Ping request for session id: %d context id: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 73*/
    /* BFD_TRC_LSPP_BSTRP_PROCESS_FAIL */
    /* LSPP bootstrap info failed  ; Argument :  Context Id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Process LSP Ping bootstrap info failed: context id: %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 74*/
    /* BFD_TRC_NO_SESS_INDEX_IN_OAM */
    /* Invalid session index ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD session index does not exist in OAM for path type: %d"
                                                       "Context id: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 75*/
    /* BFD_TRC_SESS_INDEX_UPFATE_OAM_FAIL */
    /* Session index updation failed ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD session index updation to OAM failed for session id: %d"
                                                      "context id: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 76*/
    /* BFD_TRC_INVAL_TRACE */
    /* Trace Input String Invalid ; */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Invalid Trace Input string \r\n",
        BFD_NO_TRAP
    },
    /*Array Index :77 */
    /* BFD_TRC_OID_MEM_ALLOC_FAIL */
    /* OID Memory Allocation Failed */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]OID Memory Allocation Failed \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :78 */
    /* BFD_TRC_OID_NOT_FOUND */
    /* OID not found ; Argument: Object Name */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]OID for %s Not Found \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :79 */
    /* BFD_TRC_OCT_STR_FORM_FAIL */
    /* Octet string formation failed   */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Failed to form Octet string \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :80 */
    /* BFD_TRC_VARBIND_FORM_FAIL */
    /* Varbind formation failed  */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Failed to form VarBInd \r\n",
        BFD_NO_TRAP
    },

    /*Array Index :81 */
    /* BFD_TRC_TRIG_OOB_BSTRP */
    /* Trigger OOB boot strap  */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Trigger OOB bootstrap \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 82 */
    /* BFD_TRC_INVAL_ARG_TO_FN */
    /* Function arguments invalid ; Argument : Module name,Function name*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE]%s:%s Invalid arguments to the function \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 83 */
    /* BFD_TRC_INVAL_PATH_TYPE */
    /* Invalid Path type ; Argument : Module name,Function name*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE]%s:%s Invalid Path type\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 84 */
    /* BFD_TRC_OFFLOAD_CALL_RCVD */
    /* Offload callback received; Argument : Module name,
     * Function name,sessionid*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: Offload Callback Fn(%s) called for the Session - %d \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 85 */
    /* BFD_TRC_OFFLOAD_CALL_FAIL */
    /* Offload callback failed ; Argument : Module name,Function name,reason*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: Offload Callback Fn(%s) failed : %s \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 86 */
    /* BFD_TRC_OL_FETCH_PARAM_FAIL */
    /* Offload call to get session params failed ; 
     * Argument : Module name,Function name,Session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s Fetching Session parameters failed for "
                                             "offloaded session: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 87 */
    /* BFD_TRC_OL_FETCH_STAT_FAIL */
    /* Offload call to get session stats failed ; 
     * Argument : Module name,Function name,Session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s Fetching Session statistics failed for "
                                            "offloaded session: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 88 */
    /* BFD_TRC_OL_CREATE_STAT_FAIL */
    /* Offload call to clear session stats failed ; 
     * Argument : Module name,Function name,Session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s Clearing Session statistics failed for "
                                           "offloaded session: %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 89 */
    /* BFD_TRC_OL_CLEAR_STAT_FAIL */
    /* Offload call to clear all session stats failed ; 
     * Argument : Module name,Function name*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s Clearing All Offloaded Session statistics failed\r\n",
        BFD_NO_TRAP
    },


    /*Array Index : 90 */
    /* BFD_TRC_OL_CB_REG_FAIL */
    /* Offload call to register call back failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s Register Callback with Offload Module "
                                              "failed for session:%d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 91 */
    /* BFD_TRC_OL_SESS_DEL_FAIL */
    /* Offload call to delete/diable session failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Delete/Disable of the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 92 */
    /* BFD_TRC_OL_POLL_RESP_FAIL */
    /* Offload call for poll response  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Poll response for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 93 */
    /* BFD_TRC_OL_POLL_TERM_FAIL */
    /* Offload call for poll terminate  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Poll terminate for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 94 */
    /* BFD_TRC_SYS_CONG */
    /* Offload call for poll terminate  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Poll terminate for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 95 */
    /* BFD_TRC_SYS_CONG2 */
    /* Offload call to update params  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  params update call for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 96 */
    /* BFD_TRC_OL_SESS_ENABLE */
    /* Offload call to enable session  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Enable call for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 97 */
    /* BFD_TRC_OL_SESS_CREATE_FAIL */
    /* Offload call to create session  failed ; 
     * Argument : Module name,Function name,session id*/
    {
        BFD_MGMT_TRC|ALL_FAILURE_TRC|BFD_SESS_OFFLOAD_TRC,
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][MGMT][ALL_FAILURE][SESS_OFFLOAD]%s: %s  Create call for the offloaded session:%d Failed\r\n",
        BFD_NO_TRAP
    },

    /* Configuration Traces */
    /* Array Index :98 */
    /* BFD_TRC_SESS_TBLE_FETCH_FAIL */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Fetching Session Table enteries failed\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :99 */
    /* BFD_TRC_MOD_NOT_STARTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Module is not started\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :100 */
    /* BFD_TRC_INCONSIST_VALUE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Inconsistent value\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :101*/
    /* BFD_TRC_DEMAND_ECHO_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Demand Mode and Echo function not supported \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :102*/
    /* BFD_TRC_INVALID_INTRVL */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Configured Interval is not valid  value\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :103*/
    /* BFD_TRC_INVALID_MULT */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Configured Multiplier is not valid  value\r\n",
        BFD_NO_TRAP
    },
    /* Array Index :104*/
    /* BFD_TRC_ROW_NOT_CREATED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Row Not created\r\n",
        BFD_NO_TRAP
    },

     /* Array Index :105 */
    /* BFD_TRC_VER_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Version not supported\r\n",
        BFD_NO_TRAP
    },


    /* Array Index :106 */
    /* BFD_TRC_ECHO_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Echo function not supported\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :107*/
    /* BFD_TRC_DEMAND_MODE_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Demand mode not supported\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :108*/
    /* BFD_TRC_MULTIPOINT_FLAG_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Multipoint flag not supported\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :109*/
    /* BFD_TRC_SESS_INF_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Sess Interface not supported in the current release\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :110*/
    /* BFD_TRC_GTSM_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]GTSM not supported \r\n",
        BFD_NO_TRAP
    },


    /* Array Index :111*/
    /* BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of  authentication parameters  is not allowed "
                          "when session is active \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :112*/
    /* BFD_TRC_ENABLE_AUTH_TO_SET */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Authentication should be enabled to set authentication type \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :113*/
    /* BFD_TRC_AUTH_SIMPLE_PASSWD */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Only simple password authentication is supported \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :114*/
    /* BFD_TRC_ENABLE_AUTH_TO_SET_PARAM */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Authentication should be enabled to set "
                    "authentication parameters\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :115*/
    /* BFD_TRC_FILL_MAND_FIELD */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Mandatory fields should be filled\r\n",
        BFD_NO_TRAP
    },
    /* Array Index :116*/
    /* BFD_TRC_NO_SESS_MODE_CHANGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Session mode should not be modified when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :117*/
    /* BFD_TRC_NO_RDISCR_CAHNGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Remote Discriminator should not be modified "
                                   "when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :118*/
    /* BFD_TRC_NO_EXP_VAL_CHANGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]EXP value should not be modified when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :119*/
    /* BFD_TRC_NO_TMR_NEGOT */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Timer Negotiation should not be enabled/disabled "
                               " when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :120*/
    /* BFD_TRC_NO_OL_ENABLE_IN_SESS_ACTIVE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Offload should not be enabled/disabled when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :121*/
    /* BFD_TRC_NO_ENCAP_TYPE_CHANGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Encapsulation type is not allowed to be  modified "
                                          " when session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :122*/
    /* BFD_TRC_NO_VCCV_FOR_TNL_LSP */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Tunnel and LDP should not be vccv negotiated\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :123*/
    /* BFD_TRC_PW_VCCV */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Pseudowire should be only Vccv Negotiated\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :124*/
    /* BFD_TRC_NO_MAP_TYPE_CHANGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of Map type is not allowed when the "
                                " session is active\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :125*/
    /* BFD_TRC_LDP_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]LDP is not supported \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :126*/
    /* BFD_TRC_NO_MAPPOINTER_CHANGE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]MapPointer should not be modified when session is active \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :127*/
    /* BFD_TRC_BASE_OID_NOT_MATCH */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Base Oid does not match \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :128*/
    /* BFD_TRC_NO_PATH */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Path does not exist \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :129*/
    /* BFD_TRC_NO_MAP_TYPE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Map Type not configured \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :130*/
    /* BFD_TRC_BASE_OID_FETCH_FAIL */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Fetching base Oid from Mpls OAM failed\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :131*/
    /* BFD_TRC_NO_OID_MATCH */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Configured oid doen not match the base oid\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :132*/
    /* BFD_TRC_ALLOW_FLAG_CHECK */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of parameters is not allowed when Row is "
             "active and allow flag is false\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :133*/
    /* BFD_TRC_STAT_CLEAR */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Session Statistics cleared\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :134*/
    /* BFD_TRC_GBL_CONF_PRINT_UNABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Unable to Print Global configuration\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :135*/
    /* BFD_TRC_SESS_TBL_EMPTY */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Session table is empty\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :136*/
    /* BFD_TRC_SESS_TBL_PRINT_UNABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Unable to Print Session Details\r\n",
        BFD_NO_TRAP
    },
   
    /* Array Index :137*/
    /* BFD_TRC_NO_SESSION */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Session doesnot exist for the given context\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :138*/
    /* BFD_TRC_GBL_TBL_PRINT_UNABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Unable to print global statistics\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :139*/
    /* BFD_TRC_SESS_STAT_PRINT_UNABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Unable to print sess statistics\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :140 */
    /* BFD_TRC_NO_REFLECT_FOR_NOTIFI_ENABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Trap Enabled for session up down not "
            "reflected in notification\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :141*/
    /* BFD_TRC_DISCR_MAP_ENTRY_PRINT_UNABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Unable to print disc mapping entry\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :142*/
    /* BFD_TRC_GBL_STAT_CLEAR */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Global Statistics cleared\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :143*/
    /* BFD_TRC_SESS_STAT_NOT_CLEAR */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Session Stats not cleared\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :144*/
    /* BFD_TRC_ADDRESS_NOT_SUPPORTED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Address Configuration Not supported\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :145*/
    /* BFD_TRC_CONTEXTID_GET_UNABLE */
    /* Argument : Function name*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Unable to get the context Id\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :146*/
    /* BFD_TRC_NO_REFLECT_FOR_TRAP_ENABLE */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Notification enable not reflected in trap\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :147*/
    /* BFD_TRC_UNABLE_TO_GET_PATH_OID */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Unable to get the path oid\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :148*/
    /* BFD_TRC_PATH_OID_CONVERSION_FAIL */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Path OId conversion failed\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :149 */
    /* BFD_TRC_RETRIVE_PATH_FAIL */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        "  [BFD][MGMT]Retrieving the underlying path in ME failed\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :150 */
    /* BFD_TRC_INVALID_SUB_TMR_ID */
    /* Invalid Subtimer id ; Argument: Module name */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE]%s Invalid sub timer id !!! \r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 151*/
    /* BFD_TRC_MAP_ENTRY_DEL_FAIL */
    /* Failed to delete entry ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD session map entry deletion  "
            "for context id %d,session id %d failed\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 152 */
    /* BFD_TRC_SESS_NODE_DEL_FAIL */
    /* Failed to delete entry ; Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]BFD session node deletion  "
            "for context id %d,session id %d failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 153 */
    /* BFD_TRC_DISCR_MAP_CREATE_FAIL */
    /* Failed to create Session discriminator map table entry
     *  Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session discriminator map table creation failed "
            "for context id %d, session id %d\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 154 */
    /* BFD_TRC_IP_TBL_CREATION_FAIL */
    /* Failed to create IP map table entry
     *  Argument : Context Id , session index */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session IP map table creation failed "
            "for context id %d, session id %d\r\n",
        BFD_NO_TRAP
    },

    /* Array index :155 */
    /* BFD_TRC_DISCR_MAP_DEL_FAIL */
    /* Failed to delete discriminator mapping entry
     * Argument: context id, session id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session discriminator map entry deletion failed "
                    "for context id %d, session id %d\r\n",
        BFD_NO_TRAP
    },

    /* Array index :156 */
    /* BFD_TRC_IP_MAP_ENTRY_DEL_FAIL */
    /* Failed to delete IP mapping entry 
     * * Argument: context id, session id */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session IP map entry deletion failed "
                        "for context id %d, session id %d\r\n",
        BFD_NO_TRAP
    },

    /* Array index :157 */
    /* BFD_TRC_MEM_ALLOC_FAIL */
    /* Memory allocation failed 
     * Argument : Function name */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Memmory allocation failed in %s\r\n",
        BFD_NO_TRAP
    },

    /* Array index :158 */
    /* BFD_TRC_DISCR_MAP_ENTRY_PRESENT */
    /* Discriminator map entry present 
     * context id, map index */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Discriminator map entry present for context id %d, map index %d\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :159 */
    /* BFD_TRC_FAIL_TO_ADD_NEW_NODE */
    /* Addition of new node to database failed
     * context id, map index */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Failed to add new node to database for context id %d,"
                          "map index %d\r\n",
        BFD_NO_TRAP
    },


    /* Array Index :160*/
    /* BFD_TRC_CCV_NOT_SUPPORTED_FOR_MPLS */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]CV Mode is  not supported for MPLS\r\n",
        BFD_NO_TRAP
    },


    /* ArrayIndex :161*/
    /* BFD_TRC_MAP_TYPE_NOT_CONFIGURED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]MapType should be configured to configure Map pointer\r\n",
        BFD_NO_TRAP
    },

    /* ArrayIndex :162 */
    /* BFD_TRC_ENCAPSULATION_MISMATCH */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Encapsulation type does not match the configured path\r\n",
        BFD_NO_TRAP
    },



    /* ArrayIndex :163 */
    /* BFD_TRC_OLD_TRAP_FETCH_FAILED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Fetching old trap level failed\r\n",
         BFD_NO_TRAP
    },

    /* ArrayIndex :164 */
    /* BFD_TRC_OLD_TRACE_FETCH_FAILED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Unable to fetch the old  trace level\r\n",
         BFD_NO_TRAP
    },
    
    /* ArrayIndex :165 */
    /* BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Fetching information from the global config table failed\r\n",
         BFD_NO_TRAP
    },

    /* ArrayIndex :166 */
    /* BFD_TRC_PATH_INFO_FETCH_FAILED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Fetching Path information from MplsOAM  failed\r\n",
            BFD_NO_TRAP
    },
    /* ArrayIndex :167 */
    /* BFD_TRC_ROW_STATUS_ACTION_FAILED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Row Status Change Action failed\r\n",
            BFD_NO_TRAP
    },

    /* ArrayIndex :168 */
    /* BFD_TRC_CARD_NUM_MODIFI_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of card number is not allowed when row is active\r\n",
            BFD_NO_TRAP
    },
    /* ArrayIndex :169 */
    /* BFD_TRC_SLOT_NUM_MODIFI_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of slot number is not allowed when row is active\r\n",
            BFD_NO_TRAP
    },
    /* ArrayIndex :170 */
    /* BFD_TRC_LSPP_BSTRP_EGR_FAIL */
    /* Bootstrap failed ; Argument :Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
            SYSLOG_CRITICAL_LEVEL,
            " [BFD][CTRL_PLANE][ALL_FAILURE]Bootstrap request sent at egress had failed "
                "discriminator %d\r\n",
            BFD_BTSTRAP_FAIL_TRAP
    },
    /* ArrayIndex :171 */
    /* BFD_TRC_POST_EVENT_FAILED */
    /* Post Event failed ; Argument :Session Id,Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
            SYSLOG_CRITICAL_LEVEL,
            " [BFD][CTRL_PLANE][ALL_FAILURE]Bootstrap request sent at egress had failed "
                "discriminator %d\r\n",
            BFD_BTSTRAP_FAIL_TRAP
    },
    /* ArrayIndex :172 */
    /* BFD_TRC_DISC_MAP_ENTRY_DEL_FAILED*/
    /* Discriminator map entry deletion failed:
     * Argument :Context Id, Discriminator*/
    {
        (ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        " [BFD][ALL_FAILURE]Discriminator map entry deletion failed Context Id %d"
            "discriminator %d\r\n",
        BFD_BTSTRAP_FAIL_TRAP
    },
    /* ArrayIndex :173 */
    /* BFD_TRC_OFFLOAD_FAILED*/
    /* Discriminator map entry deletion failed:
     * Argument :Discriminator , AdminCtrlErrReason*/
    {
        (ALL_FAILURE_TRC),
         SYSLOG_CRITICAL_LEVEL,
         " [BFD][ALL_FAILURE]Session Offload failed Discriminator: %d,"
             " State: %d, AdminCtrlErrReason: %d\r\n",
         BFD_ADMIN_CTRL_ERR_TRAP
            
    },
    /* ArrayIndex :174 */
    /* BFD_TRC_TMR_MIS_CONFIG*/
    /* Timer Misconfiguration:
     * Argument :Session State , AdminCtrlErrReason*/
    {
        (ALL_FAILURE_TRC),
         SYSLOG_CRITICAL_LEVEL,
         " [BFD][ALL_FAILURE]Failed due to Timer Misconfiguration. State: %d,"
             " Discriminator: %d, AdminCtrlErrReason: %d\r\n",
         BFD_ADMIN_CTRL_ERR_TRAP
    },

   /* Array Index : 175 */
    /* BFD_TRC_NEGO_INT_CHANGE_TX */
    /* Tx interval message ; Argument : Function name, Tx Interval,
     * Session Index */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_ALERT_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]Local Negotiated interval change; "
            "Local discr - %d; Remote discr - %d; "
            "Local Negotiated Interval: %ld ms\n",
        BFD_NEG_TX_CHANGE_TRAP
    },

    /*Array Index : 176 */
    /* BFD_TRC_REMOTE_NEGO_INT_CHANGE */
    /* Remote negotiated interval change message; 
     * Argument : Function name, Rx Interval,
     * Session Index  */
    {
        (BFD_POLL_SEQ_TRC | BFD_CTRL_PLANE_TRC),
        SYSLOG_ALERT_LEVEL, 
        " [BFD][POLL_SEQ][CTRL_PLANE]Remote Negotiated interval changed; " 
            "Local discr - %d; Remote discr - %d; "
            "Remote Tx Interval: %ld ms; Local Rx Interval: %ld ms; "
            "Remote negotiated Interval: %ld ms; "
            "Local Detection Time Interval: %ld\r\n ms",
        BFD_NO_TRAP
    },

    /*Array Index : 177 */
    /* BFD_TRC_SEM_TRIGGERED */
    /* Sem  Trigerred Argument : Session Index 
     * Session Index  */
    {
        (BFD_CTRL_PLANE_TRC),
        SYSLOG_INFO_LEVEL, 
        " [BFD][CTRL_PLANE]SEM triggered for the session  Session index - %d\r\n",
        BFD_NO_TRAP
    },

     /*Array Index : 178 */
    /* BFD_TRC_LABEL_BASED_SEARCH_NA */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Lable based search is not applicable for" 
            "MPLS Tunnel and LSP Discard the packet\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 179 */
    /* BFD_TRC_SESS_INFO_NOT_PRESENT */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Session information not present\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 180 */
    /* BFD_TRC_INVALID_PACKET_LENGHT */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Invalid packet length \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 181 */
    /* BFD_TRC_AUTH_NOT_SIMPLE_PASSWORD */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Auth Type is not Simple Password\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 182 */
    /* BFD_TRC_AUTH_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Authentication Failed \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 183 */
    /* BFD_TRC_RECE_INVALID_DISC */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Received invalid discriminator \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 184  */
    /*  BFD_TRC_RECE_INVALID_ENCAP_TYPE*/
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Received invalid encap type\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 185 */
    /* BFD_TRC_VCCV_CC_VERI_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]VCCV CC Verification failed\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 186 */
    /* BFD_TRC_VCCV_CV_VERI_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]VCCV CV Verification failed \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 187 */
    /*BFD_TRC_ERROR_VERSION_FIELD  */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Error in Version field \r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 188 */
    /* BFD_TRC_DECT_MUL_ZERO */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Detect Multiplier set to zero\r\n",
        BFD_NO_TRAP
    },

    /*Array Index : 189 */
    /* BFD_TRC_INVALID_MULTIPOINT_BIT */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Invalid multipoint bit\r\n",
        BFD_NO_TRAP
    },

   /*Array Index : 190 */
    /* BFD_TRC_MY_DISC_ZERO */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]My Discriminator field is zero\r\n",
        BFD_NO_TRAP
    },

   /*Array Index : 191 */
    /* BFD_TRC_REMOTE_MEP_VALID_TUNNEL_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]IP bad tunnel Source MEP validation" 
            "against remote MEP validation failed\r\n",
        BFD_NO_TRAP
    },

   /*Array Index : 192 */
    /* BFD_TRC_REMOTE_MEP_VALID_PW_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]IP bad PW Source MEP validation" 
            "against remote MEP validation failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 193 */
    /* BFD_TRC_REMOTE_MEP_VALID_ICC_FAILED  */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]ICC Source MEP validation against remote MEP validation failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 194 */
    /* BFD_TRC_IP_VALIDATION_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Destination IP is not loopback IP\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 195 */
    /* BFD_TRC_UDP_DEST_PORT_VER_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]UDP destination port verification failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 196 */
    /* BFD_TRC_UDP_SOUR_PORT_VER_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]UDP sourcr port verification failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 197 */
    /* BFD_TRC_FILL_IP_UDP_HEADER_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Filling IP/UDP Header failed  \r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 198 */
    /* BFD_TRC_FILL_ACH_HEADER_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Filling Ach Header failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 199 */
    /* BFD_TRC_PACKET_RECEIVED*/
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Processing BFD pakcet received on socket\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 200 */
    /* BFD_TRC_CFA_IFINDEX_FETCH_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Failed to get the CFA ifindex from port \r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 201 */
    /* BFD_TRC_SEND_MESS_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]SendMsg failed\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 202 */
    /* BFD_TRC_SEND_TO_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Sendto failed\r\n",
        BFD_NO_TRAP
    },
   /*Array Index : 203 */
    /*BFD_TRC_CARD_ATTACH_DETACH_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]%s: Failed to handle Card attach/detach\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 204 */
    /* BFD_TRC_ENTERED_SESSION_IDX */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]%s: Entered for Session Idx- %d\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 205 */
    /* BFD_TRC_TASK_NOT_INITIALISED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]%s: BFD Task is not initialized\r\n",
        BFD_NO_TRAP
    },

  /*Array Index : 206 */
    /* BFD_TRC_MEM_ALLOC_MBSM_FAILED */
    {
       ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]%s: Allocation of memory for MBSM information Failed \r\n",
        BFD_NO_TRAP

    },
  /*Array Index : 207 */
    /* BFD_TRC_OFFLOAD_INVALID_SEM_STATE*/
    {
       ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Offload call back received in invalid sem state %s,Session Index %d\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 208 */
    /* BFD_TRC_INVALID_EVENT_INDEP_MONI*/
    {
       ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Invalid event if its independent monitoring and event %s Session Index:%d\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 209 */
    /* BFD_TRC_FETCH_NEXT_HOP_ADDRESS_FAIL*/
    {
       ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]Fetching next hop address failed %s, Peer IP : %d\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 210 */
    /* BFD_TRC_FETCH_ROUTER_ID_FAILED*/
    {
       ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][ALL_FAILURE]%s Fetching router ID failed\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 211 */
    /* BFD_TRC_OFFLOAD_DEBUG_TRACE*/
    {
       BFD_SESS_OFFLOAD_TRC,
        SYSLOG_INFO_LEVEL, 
        " [BFD][SESS_OFFLOAD]OFF: %s \r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 212 */
    /* BFD_TRC_PW_RECV_GAL */
    {
       ALL_FAILURE_TRC,
        SYSLOG_ALERT_LEVEL, 
        " [BFD][ALL_FAILURE]Packet validation failed due to PW receives GAL \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 213 */
    /* BFD_TRC_OAM_INFO_FAILED */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]%s Fetching OAM information failed \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 214 */
    /* BFD_TRC_MAX_LBL_STACK */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Exceeding max label stack depth. BFD supports max label "
            "stack depth : %d\r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 215 */
    /* BFD_TRC_INVALID_PKT_LEN */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Packet received with invalid packet length \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 216 */
    /* BFD_TRC_FAILED_TO_SET_RA */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Failed to set Router-Alert Option in setsockopt \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 217 */
    /* BFD_TRC_FAILED_TO_SET_DSCP */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Failed to set DSCP Option in setsockopt \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 218 */
    /* BFD_TRC_FAILED_TO_SET_CONTEXT */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Failed to set Context Option in setsockopt \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 219 */
    /*BFD_TRC_SESS_TYPE_CHANGE_NOT_ALLOWED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of sesstype is not allowed when row is active \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 220 */
    /*BFD_TRC_DESTUDP_CHANGE_NOT_ALLOWED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of destination udp port is not allowed when row is active \r\n",
        BFD_NO_TRAP
    },

    /* Array Index : 221 */
    /*BFD_TRC_SRCUDP_CHANGE_NOT_ALLOWED*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of source udp port is not allowed when row is active \r\n",
        BFD_NO_TRAP
    },
 
    /* Array Index : 222 */
    /* BFD_TRC_IP_TTL_VALIDATION_FAILED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]IP TTL is not set to 1 \r\n",
        BFD_NO_TRAP
    },

    /* Array Index : 223 */
    /* BFD_TRC_AUTH_TYPE_NOT_CONFIGURED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT] Auth type should not be no-auth, when authentication is enabled\r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 224 */
    /* BFD_TRC_FAIL_TO_REG_WITH_RM */
    {
        ALL_FAILURE_TRC | BFD_RED_TRC ,
        SYSLOG_ERROR_LEVEL,
        " [BFD] [ALL_FAILURE] [RED] BfdRedInitGlobalInfo: Failed to register with RM \r\n",
        BFD_NO_TRAP
    },

    /* Array Index : 225 */
    /* BFD_TRC_NO_SUCH_RM_EVENT */
    {
        ALL_FAILURE_TRC | BFD_RED_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD] [ALL_FAILURE][RED] BfdRedRmCallBack: This event is not associated with RM \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 226 */
    /*BFD_TRC_NO_QUEUE_FOR_RM_MSG */
    {
        ALL_FAILURE_TRC | BFD_RED_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD] [ALL_FAILURE] [RED] Queue Message associated with the event is not sent by RM \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 227 */
    /* BFD_TRC_SENT_RM_EVENT */
    {
        BFD_RED_TRC,
        SYSLOG_DEBUG_LEVEL,
        " [BFD] [RED] RM Event is notified to BFD \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 228 */
    /* BFD_TRC_RM_RECVD_GO_ACT_EVT */
    {
        BFD_RED_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD] [RED] BfdRedHandleRmEvents:Received GO_ACTIVE event \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 229 */
    /*BFD_TRC_RM_RECVD_GO_STANDBY_EVT */
    {
        BFD_RED_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD] [RED] BfdRedHandleRmEvents:Received GO_STANDBY event \r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 230 */
    /* BFD_RM_RED_TRC_EXT */
    {
        CONTROL_PLANE_TRC,
        SYSLOG_DEBUG_LEVEL,
        " [BFD][RED] [CTRL_PLANE] Exiting function : %s\r\n",
        BFD_NO_TRAP
    },
    /*Array Index : 231 */
    /* BFD_RM_RED_TRC_ENT */
    {
        CONTROL_PLANE_TRC,
        SYSLOG_DEBUG_LEVEL,
        " [BFD][RED] [CTRL_PLANE] Entering function : %s\r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 232 */
    /* BFD_RM_ACTIVE_TO_STANDBY_EVT */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_ALERT_LEVEL,
        " [BFD][RED][CTRL_PLANE] System switched from ACT to STBY \r\n",
        BFD_NEG_TX_CHANGE_TRAP

    },
    /* Array Index : 233 */
    /* BFD_TRC_RM_STANDBY_UP_EVT */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_ALERT_LEVEL,
        " [BFD][RED] BfdRedHandleRmEvents:Received RM_STANDBY_UP event \r\n",
        BFD_NEG_TX_CHANGE_TRAP

    },
    /* Array Index : 234 */
    /* BFD_TRC_RM_STANDBY_DOWN_EVT */
    {
        BFD_CTRL_PLANE_TRC,
        SYSLOG_ALERT_LEVEL,
        " [BFD][RED] BfdRedHandleRmEvents:Received RM_STANDBY_DOWN event \r\n",
        BFD_NEG_TX_CHANGE_TRAP
    },
    /*Array Index : 235 */
    /* BFD_TRC_IP_MAP_CREATE_FAIL */
    /* Failed to create Session IP map table entry
     *  Argument : Context Id*/
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]Session IP map table creation failed "
            "for context id %d, session id %d\r\n",
        BFD_NO_TRAP
    },

    /* Array index :236 */
    /* BFD_TRC_IP_MAP_ENTRY_PRESENT */
    /* Ip map entry present
     * context id, map index */
    {
        (BFD_CTRL_PLANE_TRC | ALL_FAILURE_TRC),
        SYSLOG_INFO_LEVEL,
        " [BFD][CTRL_PLANE][ALL_FAILURE]IP map entry present for context id %d, map index %d\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :237*/
    /* BFD_TRC_SESS_TYPE_NOT_SUPPORTED_IP */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Only Single hop session type is supported for IP path monitoring\r\n",
        BFD_NO_TRAP
    },

    /* Array Index :238*/
    /* BFD_TRC_SESS_INTERFACE_CHANGE_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of session interface is not allowed "
                          "when session is active \r\n",
        BFD_NO_TRAP
    },
    /* Array Index :239*/
    /* BFD_TRC_SESS_SRCADDR_CHANGE_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of session source address is not allowed "
                          "when session is active \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :240*/
    /* BFD_TRC_SESS_DSTADDR_CHANGE_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of session destination address is not allowed "
                          "when session is active \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :241*/
    /* BFD_TRC_MODIFY_DYNAMIC_SESS_NOT_ALLOWED */
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Modification of Monitoring-Path/Sess-Type/Sess-Mode/"
        "Session-AdminStatus/Storage-Type not allowed for the"
        "dynamically created entry \r\n",
        BFD_NO_TRAP
    },

    /* Array Index :242*/
    /* BFD_TRC_SRC_ADDR_DST_ADDR_MISMATCH*/
    {
        BFD_MGMT_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][MGMT]Source address type/length did match with deatination address type-length\r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 243 */
    /* BFD_TRC_FAILED_TO_SET_TTL */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Failed to set TTL Option in setsockopt \r\n",
        BFD_NO_TRAP
    },
    /* Array Index : 244 */
    /* BFD_TRC_FAILED_TO_SET_HOPLIMIT */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE]Failed to set HOPLIMIT Option in setsockopt \r\n",
        BFD_NO_TRAP
    },
     /* Array Index : 245 */
    /* BFD_TRC_SELECT_ADD_FD_FAILURE */
    {
        ALL_FAILURE_TRC,
        SYSLOG_INFO_LEVEL,
        " [BFD][ALL_FAILURE] SelAddFd failure\r\n",
        BFD_NO_TRAP
    }


};

#endif
