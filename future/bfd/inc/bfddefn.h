/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfddefn.h,v 1.41 2014/07/04 10:15:48 siva Exp $
 *
 * Description: This file contains definitions for bfd module.
 *******************************************************************/

#ifndef __BFDDEFN_H__
#define __BFDDEFN_H__


#define BFD_CTRL_PKT_LEN                24
#define BFD_AUTH_TLV_LEN                257
#define BFD_MEP_TLV_LEN                 267

#define BFD_AUTH_MAX_SIM_PAS_LEN        16
#define BFD_TRAP_NAME_MAX_LEN           30
#define BFD_MAX_MSG_LEN                 256
#define BFD_AUTH_TLID_LEN               3
#define BFD_DM_PKT_OFFST                2

#define BFD_OBJECT_NAME_LEN             256
#define BFD_MAX_CONTEXT_NAME            VCM_ALIAS_MAX_LEN

#define MAX_TRAP_ARRAY_INDEX            5

/*System Mode */
#define BFD_MI_MODE                     1
#define BFD_SI_MODE                     2

#ifdef RM_WANTED
#define BFD_AUDIT_SHOW_CMD           "snmpwalk mib name fsMIStdBfdSessEntry > "
#define BFD_CLI_EOF                  2
#define BFD_CLI_NO_EOF               1
#define BFD_CLI_RDONLY               OSIX_FILE_RO
#define BFD_CLI_WRONLY               OSIX_FILE_WO
#define BFD_CLI_MAX_GROUPS_LINE_LEN  200
#define BFD_AUDIT_FILE_ACTIVE        "/tmp/bfd_output_file_active"
#define BFD_AUDIT_FILE_STDBY         "/tmp/bfd_output_file_stdby"
#endif

/*Trace Level*/
#define BFD_INIT_SHUT_TRC               0x00000001
#define BFD_MGMT_TRC                    0x00000002
#define BFD_DATA_PLANE_TRC              0x00000004
#define BFD_CTRL_PLANE_TRC              0x00000008
#define BFD_PKT_DUMP_TRC                0x00000010
#define BFD_RESOURCE_TRC                0x00000020
#define ALL_FAILURE_TRC                 0x00000040
#define BFD_BUF_TRC                     0x00000080
#define BFD_SESS_ESTB_TRC               0x00000100
#define BFD_SESS_DOWN_TRC               0x00000200
#define BFD_POLL_SEQ_TRC                0x00000400
#define BFD_CRITICAL_TRC                0x00000800
#define BFD_RED_TRC                     0x00001000
#define BFD_SESS_OFFLOAD_TRC            0x00002000


#define BFD_JITTER_LOWER_RANGE          75
#define BFD_JITTER_HIGHER_RANGE_90      90
#define BFD_JITTER_HIGHER_RANGE_100     100

#define BFD_NP_FAILED                   1
#define BFD_OFF_FAILED                  BFD_NP_FAILED+ 1


#ifdef MBSM_WANTED
#define BFD_INVALID_CARD_SLOT_NUM 0xffffffff
#endif

#define MPLS_MAX_GACH_TLV_HEADER_LEN    4
#define MPLS_MAX_GACH_TLV_LEN           255
#define MPLS_MAX_IP_HDR_LEN             24
#define MPLS_MAX_UDP_HDR_LEN            8
#define BFD_ACH_TLV_LEN                 3

#define BFD_MAX_CTRL_PKT_LEN    (BFD_CTRL_PKT_LEN +\
                                 BFD_AUTH_TLV_LEN)

#define BFD_MAX_LBL_HDR_LEN     ((MPLS_MAX_LABEL_STACK * 4) +\
                                 MPLS_MAX_GAL_GACH_HEADER_LEN)

#define BFD_MAX_PKT_LEN         (MPLS_MAX_L2HEADER_LEN +\
                                 (MPLS_MAX_LABEL_STACK * 4)+\
                                 MPLS_MAX_GAL_GACH_HEADER_LEN +\
                                 MPLS_MAX_GACH_TLV_HEADER_LEN +\
                                 MPLS_MAX_GACH_TLV_LEN +\
                                 MPLS_MAX_IP_HDR_LEN +\
                                 MPLS_MAX_UDP_HDR_LEN +\
                                 BFD_MAX_CTRL_PKT_LEN)

#define BFD_CTRL_PKT_OFFSET     (MPLS_MAX_L2HEADER_LEN +\
                                 (MPLS_MAX_LABEL_STACK  * 4)+\
                                 MPLS_MAX_GAL_GACH_HEADER_LEN +\
                                 MPLS_MAX_GACH_TLV_HEADER_LEN +\
                                 MPLS_MAX_GACH_TLV_LEN +\
                                 MPLS_MAX_IP_HDR_LEN +\
                                 MPLS_MAX_UDP_HDR_LEN)

#define  BFD_TASK_PRIORITY             100
#define  BFD_TASK_NAME                 "BFD"
#define  BFD_QUEUE_NAME                (const UINT1 *) "BFDQ"
#define  BFD_MUT_EXCL_SEM_NAME         (const UINT1 *) "BFDM"
#define  BFD_QUEUE_DEPTH               FsBFDSizingParams[MAX_BFD_QUEMSG_SIZING_ID].u4PreAllocatedUnits
#define  BFD_SEM_CREATE_INIT_CNT       1

#define BFD_DEFAULT_MEM_BLOCK          1

#define BFD_PATH_INFO_MEM_BLOCK        3
#define BFD_OUT_PARAMS_INFO_MEM_BLOCK  1
#define BFD_IN_PARAMS_INFO_MEM_BLOCK   1
#define BFD_QMSG_INFO_MEM_BLOCK        1
#define BFD_PKT_BUF_MEM_BLOCK          1

#define  BFD_ENABLED                   1
#define  BFD_DISABLED                  2

#define  BFD_TIMER_EVENT               0x00000001 
#define  BFD_QUEUE_EVENT               0x00000002

/*Bit positions in BFD Control Packet*/
#define BFD_PROTOCOL_VERSION           0x00000001
#define BFD_POLL_BIT_LOCATION          0x00200000
#define BFD_FINAL_BIT_LOCATION         0x00100000
#define BFD_CPINDE_BIT_LOCATION        0x00080000
#define BFD_AUTH_BIT_LOCATION          0x00040000
#define BFD_DEMAND_BIT_LOCATION        0x00020000
#define BFD_MULTIPOINT_BIT_LOCATION    0x00010000

#define BFD_PACKET_MIN_TX_OFFSET       12
#define BFD_PACKET_MIN_RX_OFFSET       16
/* Encap Type based on first 4 bit of the header */
#define BFD_HEADER_IPV4                0x04
#define BFD_HEADER_IPV6                0x06
#define BFD_HEADER_ACH                 0x01

#define BFD_MEG_ID_OFFSET              3
#define BFD_ME_ID_OFFSET               2
#define BFD_MP_ID_OFFSET               1

#define BFD_TUNNEL_ID_OFFSET           4
#define BFD_TUNNEL_INST_OFFSET         3
#define BFD_TUNNEL_SRC_OFFSET          2
#define BFD_TUNNEL_DEST_OFFSET         1

/*version */
#define BFD_PACKET_ACH_HEADER_VERSION  0

/*ach flags */
#define BFD_PACKET_ACH_HEADER_FLAGS    0

#define BFD_PACKET_ACH_TLV_HDR_SIZE    4

/* Default Control Channel Types */
#define BFD_CH_TYPE_DEF_CC_BFD              0x07
#define BFD_CH_TYPE_DEF_CV_BFD              0x08
#define BFD_CH_TYPE_DEF_CC_IPV4             0x21
#define BFD_CH_TYPE_DEF_CV_IPV4             0x22
#define BFD_CH_TYPE_DEF_CC_IPV6             0x57
#define BFD_CH_TYPE_DEF_CV_IPV6             0x58

/* Control Channel Types */
#define BFD_CH_TYPE_CC_BFD              gu2BfdAchChnlTypeCcBfd
#define BFD_CH_TYPE_CV_BFD              gu2BfdAchChnlTypeCvBfd
#define BFD_CH_TYPE_CC_IPV4             gu2BfdAchChnlTypeCcIpv4
#define BFD_CH_TYPE_CV_IPV4             gu2BfdAchChnlTypeCvIpv4
#define BFD_CH_TYPE_CC_IPV6             gu2BfdAchChnlTypeCcIpv6
#define BFD_CH_TYPE_CV_IPV6             gu2BfdAchChnlTypeCvIpv6

/* BFD for VCCV CV Types */
#define BFD_CV_IP_UDP_ENCAP             L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY
#define BFD_CV_IP_UDP_ENCAP_STATUS_SIG  L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS
#define BFD_CV_ACH_ENCAP                L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY
#define BFD_CV_ACH_ENCAP_STATUS_SIG     L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS

#define BFD_UDP_DEST_PORT               3784
#define BFD_UDP_DEST_PORT_MHOP          4784
#define BFD_UDP_SRC_PORT                49155
#define BFD_MIN_UDP_SRC_PORT            49152
#define BFD_MAX_UDP_SRC_PORT            65535

#define BFD_GAL_LABLE                   13

#define BFD_VCCV_CC_ACH                 L2VPN_VCCV_CC_ACH
#define BFD_VCCV_CC_RAL                 L2VPN_VCCV_CC_RAL
#define BFD_VCCV_CC_TTL                 L2VPN_VCCV_CC_TTL_EXP

#define BFD_MAX_PASSWD_LEN              256

#define BFD_PACKET_OVER_MPLS_RTR        1
#define BFD_PACKET_OVER_IPV4            2
#define BFD_PACKET_OVER_IPV6            3

/* Session operational role */
#define BFD_SESS_OPER_ROLE_INDP_SOURCE  1
#define BFD_SESS_OPER_ROLE_INDP_SINK    2

/* Source MEP Identifiers Type */
#define BFD_MEP_TYPE_IP_TUNNEL          0x26 
#define BFD_MEP_TYPE_IP_PW              0x27 
#define BFD_MEP_TYPE_ICC                0x28 
#define BFD_127DEST_ADDR_MASK              0xff000000 

#define BFD_STATE_UP                    1
#define BFD_STATE_DOWN                  2

#define UDP_HDR_LEN                     8 

#define MPLS_OPER_TYPE_ICC              1 
#define MPLS_OPER_TYPE_IP               2 

#define BFD_SESS_ERROR_NONE             0
#define BFD_SESS_ERROR_OFFLD_FAILED     1
#define BFD_SESS_ERROR_PERIOD_MISCONFIG 2

#define BFD_TUNNEL_OID_LENGTH           4
#define BFD_TUNNEL_INGRESS_ID           2
#define BFD_TUNNEL_EGRESS_ID            3

#define BFD_SESS_ACTIVE                 1
#define BFD_SESS_PASSIVE                2

#define BFD_MIN_INTERVAL                0
#define BFD_MAX_INTERVAL                4294967295

#define BFD_MIN_DETECT_MULT             1
#define BFD_MAX_DETECT_MULT             255

#define BFD_MIN_SLOW_TX_INTVL           1000000
#define BFD_MAX_SLOW_TX_INTVL           4294967295

#define BFD_AUTH_KEY_ID_MIN_VAL         (-1)
#define BFD_AUTH_KEY_ID_MAX_VAL         255

#define BFD_REMOTE_DISC_MIN_VAL         0
#define BFD_REMOTE_DISC_MAX_VAL         4294967295

#define BFD_VER_MIN_VALUE               0
#define BFD_VER_ONE                     1
#define BFD_VER_MAX_VALUE               7

#define BFD_EXP_MIN_VAL                 0
#define BFD_EXP_MAX_VAL                 7

#define BFD_NO_TRACE                    0x00000000
#define BFD_INIT_SHUT_TRACE             0x00000001
#define BFD_MGMT_TRACE                  0x00000002
#define BFD_DATA_TRACE                  0x00000004
#define BFD_CTRL_TRACE                  0x00000008 
#define BFD_PKT_DUMP_TRACE              0x00000010
#define BFD_RESOURCE_TRACE              0x00000020
#define BFD_ALL_FAIL_TRACE              0x00000040
#define BFD_BUF_TRACE                   0x00000080

#define BFD_SESS_ESTB_TRACE             0x00000100
#define BFD_SESS_DOWN_TRACE             0x00000200
#define BFD_POLL_SEQUENCE_TRACE         0x00000400
#define BFD_CRITICAL_TRACE              0x00000800 
#define BFD_REDUNDANCY_TRACE            0x00001000
#define BFD_OFFLOAD_TRACE               0x00002000
#define BFD_ALL_TRACE                   0x00003FFF

#define BFD_STOR_MIN_VAL                1 
#define BFD_STOR_MAX_VAL                5

#define BFD_IPV4_MAX_ADDR_LEN           4
#define BFD_IPV6_MAX_ADDR_LEN           16

#define BFD_MAX_SLOT_NUMBER             65535
#define BFD_MAX_CARD_NUMBER             65535
        
/* end */

/* Default Net Mask */
#define   BFD_IP_DEF_NET_MASK           0xffffffff 

#define BFD_MAX_ADDR_LEN               

#define BFD_SNMPV2_TRAP_OID_LEN         11

#define BFD_SESS_UP_TRAP    0xFF
#define BFD_SESS_DOWN_TRAP  1

#define BFD_MAX_CONTEXT                 SYS_DEF_MAX_NUM_CONTEXTS

#define SESS_INDEX_MIN_VAL              1
#define SESS_INDEX_MAX_VAL             4294967295

#define BFD_SESS_MODE "bfdsess"


#define BFD_GLB_SESS_OPER_MODE          BFD_OPER_MODE_ASYNC_WO_ECHOFUNCTION

#define BFD_MAX_COUNTER_SIZE            0xFFFFFFFF

#define BFD_UDP_HDR_CKSUM_OFFSET       6
#define BFD_IP_HDR_CKSUM_OFFSET        10

#define BFD_MAX_CLI_TIME_STRING        9

#define BFD_MAX_SESS_INTERFACE_INDEX   2147483647

#define BFD_SESS_VERSION_NUM_OFFSET_BITS       29
#define BFD_SESS_DIAG_OFFSET_BITS              24
#define BFD_SESS_STATE_OFFSET_BITS             22
#define BFD_SESS_DETECT_MULTIPLIER_OFFSET_BITS 8
#define BFD_DSCP_TO_TOS_CONVERT_VAL            2

#define BFD_SESS_UP_DOWN_TRAP_EN               31
#define BFD_SESS_UP_TRAP_OID                   1
#define BFD_SESS_DOWN_TRAP_OID                 2
#define BFD_BTSTRAP_FAIL_TRAP_OID              1
#define BFD_NEG_TX_CHANGE_TRAP_OID             2
#define BFD_ADMIN_CTRL_ERR_TRAP_OID            3

#define BFD_SESS_UP_TRAP_MI_OID                2
#define BFD_SESS_DOWN_TRAP_MI_OID              3

# define BFD_NO_OF_SEC_IN_A_MIN                  60
# define BFD_NO_OF_MIN_IN_AN_HR                60
# define BFD_NO_OF_HR_IN_A_DAY                 24
# define BFD_MPLS_LBL_LEN                      4
# define BFD_MPLS_ACH_HDR_LEN                  4
# define BFD_HDR_TYPE_OFFSET_BITS              4
# define BFD_MPLS_BOTTOM_STACK_INDEX_WITH_GAL  2

#define BFD_LBL_OFFSET_BITS 12
#define BFD_EXP_OFFSET_BITS 9
#define BFD_SI_OFFSET_BITS 8
#define BFD_JITTER_ADJUST  3

#define BFD_MPLS_ACH_TYPE_OFFSET_BITS 28
#define BFD_MPLS_ACH_VERSION_OFFSET_BITS 24
#define BFD_MPLS_ACH_RSVD_OFFSET_BITS 16


#define BFD_CREATE_DYNAMIC_SESSION   1
#define BFD_REMOVE_DYNAMIC_SESSION   2
#define BFD_DISABLE_DYNAMIC_SESSION  3

#define BFD_CLIENT_STORAGE_SIZE      sizeof(UINT4)

#define BFD_RB_GREATER      1
#define BFD_RB_LESSER      -1
#define BFD_RB_EQUAL        0

#define BFD_THREE           3

#define   NP_UPDATED            1
#define   NP_NOT_UPDATED        0


#ifdef L2RED_WANTED
#define BFD_GET_NODE_STATUS()  gBfdRedGblInfo.u1NodeStatus
#else
#define BFD_GET_NODE_STATUS()  RM_ACTIVE
#endif

#define BFD_GREATER      1
#define BFD_LESSER      -1
#define BFD_EQUAL        0

#define BFD_DEF_SESS_ID    0xFFFF

enum
{
    BFD_OBTAINING_TASK_ID_FAILED,
    BFD_SEM_CREATION_FAILED,
    BFD_QUEUE_CREATION_FAILED,
    BFD_MEMPOOL_CREATION_FAILED,
    BFD_MEMPOOL_INITIALISATION_FAILED,
    BFD_TIMER_CREATION_FAILED,
    BFD_TIMER_INITI_FAILED,
    BFD_RM_REGISTRAION_FAILED,
    BFD_REDU_GLOBAL_INIT_FAILED,
    BFD_DEFAULT_CONTEXT_CREATION_FAILED,
    BFD_SEM_DELETION_FAILED,
    BFD_QUEUE_DELETION_FAILED,
    BFD_MEMPOOL_DELETION_FAILED,
    BFD_MEMPOOL_DEINIT_FAILED,
    BFD_TIMER_LIST_DELETION_FAILED,
    BFD_TIMER_DEINIT_FAILED,
    BFD_RM_DEREGISTRATION_FAILED,
    BFD_REDU_GLOBAL_DEINIT_FAILED,
    BFD_DEFAULT_CONTEXT_DELETION_FAILED,
    BFD_OSIX_QUEUE_SEND_FAILED,
    BFD_OSIX_EVENT_SEND_FAILED,
    BFD_OSIX_SEM_ACQ_FAILED,
    BFD_OSIX_SEM_REALEASE_FAILED,
    BFD_MEM_POOL_ALLOCATE_FAIL,
    BFD_FREE_MEMORY_FAIL,
    BFD_DATABASE_CREATION_FAILED,
    BFD_TIMER_START,
    BFD_TIMER_STOP,
    BFD_TIMER_LIST_CREATION_FAILED,
    /*BFD_TIMER_LIST_DELETION_FAILED,*/
    BFD_TIMER_START_FAILED,
    BFD_TIMER_STOP_FAILED,
    BFD_TIMER_RESTART_FAILED,
    BFD_MODULE_NOT_STARTED,
    BFD_RECEIVED_QUEUE_EVENT,
    BFD_RECEIVED_TIMER_EVENT,
    BFD_RECEIVED_TIMER_EXPIRY_EVENT,
    BFD_SOCKET_OPENING_FAILED,
    BFD_SOCKET_CLOSING_FAILED,
    BFD_SOCKET_BINDING_FAILED,
    BFD_LISTEN_FAILED,
    BFD_SOCKET_SETOPTION_FAILED,
    BFD_ADD_SELECT_IN_FD_FAILED,
    BFD_READ_ON_SOCKET_FAILED,
    BFD_WRITE_ON_SOCKET_FAILED
};

enum
{
     BFD_TRC_DETECT_TMR_EXP,      /* 0 */
     BFD_TRC_SESS_CREATE_FAIL,    /* 1 */
     BFD_TRC_SESS_IDX_IN_USE,     /* 2 */
     BFD_TRC_LDISCR_IN_USE,       /* 3 */
     BFD_TRC_RDISCR_IN_USE,       /* 4 */ 
     BFD_TRC_DISCR_GEN_FAIL,      /* 5 */ 
     BFD_TRC_SESS_ESTAB,          /* 6 */
     BFD_TRC_POLL_INIT,           /* 7 */
     BFD_TRC_POLL_TER,            /* 8 */
     BFD_TRC_NEW_TX,              /* 9 */
     BFD_TRC_NEW_RX,              /* 10 */
     BFD_TRC_NEW_DMULT,           /* 11 */
     BFD_TRC_DETECT_INTRVL,       /* 12 */
     BFD_TRC_SEM,                 /* 13 */
     BFD_TRC_SEM_EV_IMPOS,        /* 14 */  
     BFD_TRC_SEM_ST_INIT,         /* 15 */
     BFD_TRC_SEM_ST_UP,           /* 16 */
     BFD_TRC_SEM_ST_DOWN,         /* 17 */  
     BFD_TRC_SEM_ST_AD,           /* 18 */
     BFD_TRC_TX_MISMATCH,         /* 19 */
     BFD_TRC_VER_FAIL,            /* 20 */
     BFD_TRC_DIAG_FAIL,           /* 21 */
     BFD_TRC_DEMAND_FAIL,         /* 22 */ 
     BFD_TRC_MBIT_SET,            /* 23 */
     BFD_TRC_DMULT_FAIL,          /* 24 */
     BFD_TRC_INVALID_LEN,         /* 25 */
     BFD_TRC_DISCR_MISMATCH,      /* 26 */   
     BFD_TRC_INVALID_AUTH_TYPE,   /* 27 */
     BFD_TRC_INVALID_AUTH_LEN,    /* 28 */ 
     BFD_TRC_NO_AUTH,             /* 29 */
     BFD_TRC_PKT_DUMP,            /* 30 */
     BFD_TRC_OFFLOAD_FAIL,        /* 31 */
     BFD_TRC_ST_CHANGE,           /* 32 */
     BFD_TRC_OPER_MODE_FAIL,      /* 33 */
     BFD_TRC_OPER_MODE_CHANGE,    /* 34 */
     BFD_TRC_NO_CV_TYPE,          /* 35 */ 
     BFD_TRC_SESS_DOWN_CARD_DETACH, /* 36 */
     BFD_TRC_LSPP_FEC_FAIL,         /* 37 */ 
     BFD_TRC_NO_RESP,               /* 38 */
     BFD_TRC_SESS_ESTAB_FAIL,       /* 39 */
     BFD_TRC_SESS_UP,               /* 40 */
     BFD_TRC_SESS_DOWN,             /* 41 */
     BFD_TRC_SESS_ADMIN_DOWN,       /* 42 */ 
     BFD_TRC_NO_CNTX,               /* 43 */
     BFD_TRC_INVALID_CNTX,          /* 44 */
     BFD_TRC_INVAL_SYS_CTRL,        /* 45 */
     BFD_TRC_INVAL_MOD_STAT,        /* 46 */
     BFD_TRC_INVAL_TRC_LEN,         /* 47 */
     BFD_TRC_SEM_INIT,              /* 48 */
     BFD_TRC_TX_TMR_FAIL,           /* 49 */
     BFD_TRC_DTMR_FAIL,             /* 50 */
     BFD_TRC_TX_TMR_STOP_FAIL,      /* 51 */
     BFD_TRC_DTMR_STOP_FAIL,        /* 52 */
     BFD_TRC_DTMR_RUNNING_IN_ST_DOWN, /* 53 */
     BFD_TRC_ENTRY_CREATE_FAIL,       /* 54 */   
     BFD_TRC_GBL_TABLE_INIT_FAIL,     /* 55 */  
     BFD_TRC_EXT_CALL_FAIL,           /* 56 */
     BFD_TRC_GBL_ADD_FAIL,            /* 57 */
     BFD_TRC_CNTX_CREATE_FAIL,        /* 58 */
     BFD_TRC_NO_CNTX_ENTRY,           /* 59 */
     BFD_TRC_GBL_TBL_DEL_FAIL,        /* 60 */ 
     BFD_TRC_SESS_TBL_DEL_FAIL,       /* 61 */  
     BFD_TRC_DISCR_MAP_TBL_DEL_FAIL,  /* 62 */ 
     BFD_TRC_SESS_INIT_FAIL,          /* 63 */
     BFD_TRC_SESS_INIT_PASS,          /* 64 */
     BFD_TRC_LSPP_BSTRP_FAIL,         /* 65 */ 
     BFD_TRC_PARAM_CHANGE_FAIL,       /* 66 */ 
     BFD_TRC_NO_PARAM_CHANGE,         /* 67 */
     BFD_TRC_DISCR_FROM_LSPP,         /* 68 */
     BFD_TRC_LSPP_BSTRP_DISCR_PRESENT,   /* 69 */
     BFD_TRC_LSPP_BSTRP_NO_SESS,         /* 70 */ 
     BFD_TRC_LSPP_BSTRP_NO_OOB,          /* 71 */
     BFD_TRC_LSPP_BSTRP_REQ,             /* 72 */
     BFD_TRC_LSPP_BSTRP_PROCESS_FAIL,    /* 73 */ 
     BFD_TRC_NO_SESS_INDEX_IN_OAM,       /* 74 */
     BFD_TRC_SESS_INDEX_UPFATE_OAM_FAIL, /* 75 */
     BFD_TRC_INVAL_TRACE,                /* 76 */  
     BFD_TRC_OID_MEM_ALLOC_FAIL,         /* 77 */
     BFD_TRC_OID_NOT_FOUND,              /* 78 */ 
     BFD_TRC_OCT_STR_FORM_FAIL,          /* 79 */
     BFD_TRC_VARBIND_FORM_FAIL,          /* 80 */
     BFD_TRC_TRIG_OOB_BSTRP,             /* 81 */  
     BFD_TRC_INVAL_ARG_TO_FN,            /* 82 */
     BFD_TRC_INVAL_PATH_TYPE,            /* 83 */
     BFD_TRC_OFFLOAD_CALL_RCVD,          /* 84 */
     BFD_TRC_OFFLOAD_CALL_FAIL,          /* 85 */ 
     BFD_TRC_OL_FETCH_PARAM_FAIL,        /* 86 */
     BFD_TRC_OL_FETCH_STAT_FAIL,         /* 87 */
     BFD_TRC_OL_CREATE_STAT_FAIL,        /* 88 */
     BFD_TRC_OL_CLEAR_STAT_FAIL,         /* 89 */
     BFD_TRC_OL_CB_REG_FAIL,             /* 90 */ 
     BFD_TRC_OL_SESS_DEL_FAIL,           /* 91 */
     BFD_TRC_OL_POLL_RESP_FAIL,          /* 92 */
     BFD_TRC_OL_POLL_TERM_FAIL,          /* 93 */
     BFD_TRC_SYS_CONG,                   /* 94 */
     BFD_TRC_SYS_CONG2,                  /* 95 */
     BFD_TRC_OL_SESS_ENABLE,             /* 96 */
     BFD_TRC_OL_SESS_CREATE_FAIL,        /* 97 */ 
     BFD_TRC_SESS_TBLE_FETCH_FAIL,       /* 98 */
     BFD_TRC_MOD_NOT_STARTED,            /* 99 */
     BFD_TRC_INCONSIST_VALUE,            /* 100 */ 
     BFD_TRC_DEMAND_ECHO_NOT_SUPPORTED,  /* 101 */
     BFD_TRC_INVALID_INTRVL,             /* 102 */
     BFD_TRC_INVALID_MULT,               /* 103 */
     BFD_TRC_ROW_NOT_CREATED,            /* 104 */
     BFD_TRC_VER_NOT_SUPPORTED,          /* 105 */
     BFD_TRC_ECHO_NOT_SUPPORTED,         /* 106 */
     BFD_TRC_DEMAND_MODE_NOT_SUPPORTED,  /* 107 */
     BFD_TRC_MULTIPOINT_FLAG_NOT_SUPPORTED, /* 108 */
     BFD_TRC_SESS_INF_NOT_SUPPORTED,       /* 109 */   
     BFD_TRC_GTSM_NOT_SUPPORTED,           /* 110 */
     BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED,  /* 111 */
     BFD_TRC_ENABLE_AUTH_TO_SET,             /* 112 */
     BFD_TRC_AUTH_SIMPLE_PASSWD,             /* 113 */ 
     BFD_TRC_ENABLE_AUTH_TO_SET_PARAM,       /* 114 */   
     BFD_TRC_FILL_MAND_FIELD,                /* 115 */ 
     BFD_TRC_NO_SESS_MODE_CHANGE,            /* 116 */
     BFD_TRC_NO_RDISCR_CAHNGE,               /* 117 */
     BFD_TRC_NO_EXP_VAL_CHANGE,              /* 118 */  
     BFD_TRC_NO_TMR_NEGOT,                   /* 119 */
     BFD_TRC_NO_OL_ENABLE_IN_SESS_ACTIVE,    /* 120 */ 
     BFD_TRC_NO_ENCAP_TYPE_CHANGE,           /* 121 */
     BFD_TRC_NO_VCCV_FOR_TNL_LSP,            /* 122 */ 
     BFD_TRC_PW_VCCV,                        /* 123 */
     BFD_TRC_NO_MAP_TYPE_CHANGE,             /* 124 */
     BFD_TRC_LDP_NOT_SUPPORTED,              /* 125 */
     BFD_TRC_NO_MAPPOINTER_CHANGE,           /* 126 */
     BFD_TRC_BASE_OID_NOT_MATCH,             /* 127 */  
     BFD_TRC_NO_PATH,                        /* 128 */ 
     BFD_TRC_NO_MAP_TYPE,                    /* 129 */
     BFD_TRC_BASE_OID_FETCH_FAIL,            /* 130 */
     BFD_TRC_NO_OID_MATCH,                   /* 131 */  
     BFD_TRC_ALLOW_FLAG_CHECK,               /* 132 */ 
     BFD_TRC_STAT_CLEAR,                     /* 133 */
     BFD_TRC_GBL_CONF_PRINT_UNABLE,          /* 134 */
     BFD_TRC_SESS_TBL_EMPTY,                 /* 135 */
     BFD_TRC_SESS_TBL_PRINT_UNABLE,          /* 136 */ 
     BFD_TRC_NO_SESSION,                     /* 137 */ 
     BFD_TRC_GBL_TBL_PRINT_UNABLE,           /* 138 */ 
     BFD_TRC_SESS_STAT_PRINT_UNABLE,         /* 139 */
     BFD_TRC_NO_REFLECT_FOR_NOTIFI_ENABLE,   /* 140 */ 
     BFD_TRC_DISCR_MAP_ENTRY_PRINT_UNABLE,   /* 141 */
     BFD_TRC_GBL_STAT_CLEAR,                 /* 142 */
     BFD_TRC_SESS_STAT_NOT_CLEAR,            /* 143 */
     BFD_TRC_ADDRESS_NOT_SUPPORTED,          /* 144 */ 
     BFD_TRC_CONTEXTID_GET_UNABLE,           /* 145 */
     BFD_TRC_NO_REFLECT_FOR_TRAP_ENABLE,     /* 146 */ 
     BFD_TRC_UNABLE_TO_GET_PATH_OID,         /* 147 */   
     BFD_TRC_PATH_OID_CONVERSION_FAIL,       /* 148 */
     BFD_TRC_RETRIVE_PATH_FAIL,              /* 149 */
     BFD_TRC_INVALID_SUB_TMR_ID,             /* 150 */
     BFD_TRC_MAP_ENTRY_DEL_FAIL,             /* 151 */ 
     BFD_TRC_SESS_NODE_DEL_FAIL,             /* 152 */
     BFD_TRC_DISCR_MAP_CREATE_FAIL,          /* 153 */
     BFD_TRC_IP_TBL_CREATION_FAIL,           /* 154 */
     BFD_TRC_DISCR_MAP_DEL_FAIL,             /* 155 */ 
     BFD_TRC_IP_MAP_ENTRY_DEL_FAIL,          /* 156 */ 
     BFD_TRC_MEM_ALLOC_FAIL,                 /* 157 */
     BFD_TRC_DISCR_MAP_ENTRY_PRESENT,        /* 158 */
     BFD_TRC_FAIL_TO_ADD_NEW_NODE,           /* 159 */
     BFD_TRC_CCV_NOT_SUPPORTED_FOR_MPLS,     /* 160 */
     BFD_TRC_MAP_TYPE_NOT_CONFIGURED,        /* 161 */
     BFD_TRC_ENCAPSULATION_MISMATCH,         /* 162 */
     BFD_TRC_OLD_TRAP_FETCH_FAILED,          /* 163 */
     BFD_TRC_OLD_TRACE_FETCH_FAILED,         /* 164 */
     BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED,     /* 165 */
     BFD_TRC_PATH_INFO_FETCH_FAILED,         /* 166 */
     BFD_TRC_ROW_STATUS_ACTION_FAILED,       /* 167 */
     BFD_TRC_CARD_NUM_MODIFI_NOT_ALLOWED,    /* 168 */ 
     BFD_TRC_SLOT_NUM_MODIFI_NOT_ALLOWED,    /* 169 */ 
     BFD_TRC_LSPP_BSTRP_EGR_FAIL,            /* 170 */
     BFD_TRC_POST_EVENT_FAILED,              /* 171 */     
     BFD_TRC_DISC_MAP_ENTRY_DEL_FAILED,      /* 172 */
     BFD_TRC_OFFLOAD_FAILED,                 /* 173 */
     BFD_TRC_TMR_MIS_CONFIG,                 /* 174 */ 
     BFD_TRC_NEGO_INT_CHANGE_TX,             /* 175 */
     BFD_TRC_REMOTE_NEGO_INT_CHANGE,         /* 176 */
     BFD_TRC_SEM_TRIGGERED,                  /* 177 */
     BFD_TRC_LABEL_BASED_SEARCH_NA,          /* 178 */
     BFD_TRC_SESS_INFO_NOT_PRESENT,          /* 179 */ 
     BFD_TRC_INVALID_PACKET_LENGHT,          /* 180 */  
     BFD_TRC_AUTH_NOT_SIMPLE_PASSWORD,       /* 181 */
     BFD_TRC_AUTH_FAILED,                    /* 182 */
     BFD_TRC_RECE_INVALID_DISC,              /* 183 */  
     BFD_TRC_RECE_INVALID_ENCAP_TYPE,        /* 184 */  
     BFD_TRC_VCCV_CC_VERI_FAILED,            /* 185 */
     BFD_TRC_VCCV_CV_VERI_FAILED,            /* 186 */
     BFD_TRC_ERROR_VERSION_FIELD,            /* 187 */ 
     BFD_TRC_DECT_MUL_ZERO,                  /* 188 */
     BFD_TRC_INVALID_MULTIPOINT_BIT,         /* 189 */
     BFD_TRC_MY_DISC_ZERO,                   /* 190 */ 
     BFD_TRC_REMOTE_MEP_VALID_TUNNEL_FAILED, /* 191 */
     BFD_TRC_REMOTE_MEP_VALID_PW_FAILED,     /* 192 */
     BFD_TRC_REMOTE_MEP_VALID_ICC_FAILED,    /* 193 */
     BFD_TRC_IP_VALIDATION_FAILED,           /* 194 */
     BFD_TRC_UDP_DEST_PORT_VER_FAILED,       /* 195 */ 
     BFD_TRC_UDP_SOUR_PORT_VER_FAILED,       /* 196 */
     BFD_TRC_FILL_IP_UDP_HEADER_FAILED,      /* 197 */
     BFD_TRC_FILL_ACH_HEADER_FAILED,         /* 198 */
     BFD_TRC_PACKET_RECEIVED,                /* 199 */ 
     BFD_TRC_CFA_IFINDEX_FETCH_FAILED,       /* 200 */
     BFD_TRC_SEND_MESS_FAILED,               /* 201 */ 
     BFD_TRC_SEND_TO_FAILED,                 /* 202 */
     BFD_TRC_CARD_ATTACH_DETACH_FAILED,      /* 203 */
     BFD_TRC_ENTERED_SESSION_IDX,            /* 204 */
     BFD_TRC_TASK_NOT_INITIALISED,           /* 205 */
     BFD_TRC_MEM_ALLOC_MBSM_FAILED,          /* 206 */
     BFD_TRC_OFFLOAD_INVALID_SEM_STATE,      /* 207 */
     BFD_TRC_INVALID_EVENT_INDEP_MONI,       /* 208 */
     BFD_TRC_FETCH_NEXT_HOP_ADDRESS_FAIL,    /* 209 */ 
     BFD_TRC_FETCH_ROUTER_ID_FAILED,         /* 210 */
     BFD_TRC_OFFLOAD_DEBUG_TRACE,            /* 211 */
     BFD_TRC_PW_RECV_GAL,                    /* 212 */
     BFD_TRC_OAM_INFO_FAILED,                /* 213 */
     BFD_TRC_MAX_LBL_STACK,                  /* 214 */
     BFD_TRC_INVALID_PKT_LEN,                /* 215 */
     BFD_TRC_FAILED_TO_SET_RA,               /* 216 */
     BFD_TRC_FAILED_TO_SET_DSCP,             /* 217 */
     BFD_TRC_FAILED_TO_SET_CONTEXT,          /* 218 */
     BFD_TRC_SESS_TYPE_CHANGE_NOT_ALLOWED,   /* 219 */
     BFD_TRC_DESTUDP_CHANGE_NOT_ALLOWED,     /* 220 */
     BFD_TRC_SRCUDP_CHANGE_NOT_ALLOWED,      /* 221 */
     BFD_TRC_IP_TTL_VALIDATION_FAILED,       /* 222 */
     BFD_TRC_AUTH_TYPE_NOT_CONFIGURED,       /* 223 */
     BFD_TRC_FAIL_TO_REG_WITH_RM,            /* 224 */
     BFD_TRC_NO_SUCH_RM_EVENT,               /* 225 */
     BFD_TRC_NO_QUEUE_FOR_RM_MSG,            /* 226 */
     BFD_TRC_SENT_RM_EVENT,                  /* 227 */ 
     BFD_TRC_RM_RECVD_GO_ACT_EVT,            /* 228 */
     BFD_TRC_RM_RECVD_GO_STANDBY_EVT,        /* 229 */
     BFD_RM_RED_TRC_EXT,                     /* 230 */
     BFD_RM_RED_TRC_ENT,                     /* 231 */
     BFD_RM_ACTIVE_TO_STANDBY_EVT,           /* 232 */
     BFD_TRC_RM_STANDBY_UP_EVT,              /* 233 */
     BFD_TRC_RM_STANDBY_DOWN_EVT,         /* 234 */
     BFD_TRC_IP_MAP_CREATE_FAIL,             /* 235 */
     BFD_TRC_IP_MAP_ENTRY_PRESENT,           /* 236 */
     BFD_TRC_SESS_TYPE_NOT_SUPPORTED_IP,     /* 237 */
     BFD_TRC_SESS_INTERFACE_CHANGE_NOT_ALLOWED, /* 238 */
     BFD_TRC_SESS_SRCADDR_CHANGE_NOT_ALLOWED,/* 239 */
     BFD_TRC_SESS_DSTADDR_CHANGE_NOT_ALLOWED,/* 240 */
     BFD_TRC_MODIFY_DYNAMIC_SESS_NOT_ALLOWED,/* 241 */
     BFD_TRC_SRC_ADDR_DST_ADDR_MISMATCH,     /* 242 */
     BFD_TRC_FAILED_TO_SET_TTL,              /* 243 */
     BFD_TRC_FAILED_TO_SET_HOPLIMIT,         /* 244 */
     BFD_TRC_SELECT_ADD_FD_FAILURE           /* 255 */
};




#endif  /* __BFDDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file bfddefn.h                      */
/*-----------------------------------------------------------------------*/

