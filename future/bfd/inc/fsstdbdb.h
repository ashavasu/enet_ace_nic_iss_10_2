/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsstdbdb.h,v 1.1 2010/10/31 02:56:50 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSTDBDB_H
#define _FSSTDBDB_H

UINT1 BfdSessTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 BfdSessPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 BfdSessDiscMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 BfdSessIpMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsstdb [] ={1,3,6,1,4,1,29601,2,56};
tSNMP_OID_TYPE fsstdbOID = {9, fsstdb};


UINT4 BfdAdminStatus [ ] ={1,3,6,1,4,1,29601,2,56,1,1,1};
UINT4 BfdSessNotificationsEnable [ ] ={1,3,6,1,4,1,29601,2,56,1,1,2};
UINT4 BfdSessIndex [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,1};
UINT4 BfdSessVersionNumber [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,2};
UINT4 BfdSessType [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,3};
UINT4 BfdSessDiscriminator [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,4};
UINT4 BfdSessRemoteDiscr [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,5};
UINT4 BfdSessDestinationUdpPort [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,6};
UINT4 BfdSessSourceUdpPort [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,7};
UINT4 BfdSessEchoSourceUdpPort [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,8};
UINT4 BfdSessAdminStatus [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,9};
UINT4 BfdSessState [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,10};
UINT4 BfdSessRemoteHeardFlag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,11};
UINT4 BfdSessDiag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,12};
UINT4 BfdSessOperMode [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,13};
UINT4 BfdSessDemandModeDesiredFlag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,14};
UINT4 BfdSessControlPlaneIndepFlag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,15};
UINT4 BfdSessMultipointFlag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,16};
UINT4 BfdSessInterface [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,17};
UINT4 BfdSessSrcAddrType [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,18};
UINT4 BfdSessSrcAddr [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,19};
UINT4 BfdSessDstAddrType [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,20};
UINT4 BfdSessDstAddr [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,21};
UINT4 BfdSessGTSM [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,22};
UINT4 BfdSessGTSMTTL [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,23};
UINT4 BfdSessDesiredMinTxInterval [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,24};
UINT4 BfdSessReqMinRxInterval [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,25};
UINT4 BfdSessReqMinEchoRxInterval [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,26};
UINT4 BfdSessDetectMult [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,27};
UINT4 BfdSessNegotiatedInterval [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,28};
UINT4 BfdSessNegotiatedEchoInterval [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,29};
UINT4 BfdSessNegotiatedDetectMult [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,30};
UINT4 BfdSessAuthPresFlag [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,31};
UINT4 BfdSessAuthenticationType [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,32};
UINT4 BfdSessAuthenticationKeyID [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,33};
UINT4 BfdSessAuthenticationKey [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,34};
UINT4 BfdSessStorType [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,35};
UINT4 BfdSessRowStatus [ ] ={1,3,6,1,4,1,29601,2,56,1,2,1,36};
UINT4 BfdSessPerfCtrlPktIn [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,1};
UINT4 BfdSessPerfCtrlPktOut [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,2};
UINT4 BfdSessPerfCtrlPktDrop [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,3};
UINT4 BfdSessPerfCtrlPktDropLastTime [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,4};
UINT4 BfdSessPerfEchoPktIn [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,5};
UINT4 BfdSessPerfEchoPktOut [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,6};
UINT4 BfdSessPerfEchoPktDrop [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,7};
UINT4 BfdSessPerfEchoPktDropLastTime [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,8};
UINT4 BfdSessUpTime [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,9};
UINT4 BfdSessPerfLastSessDownTime [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,10};
UINT4 BfdSessPerfLastCommLostDiag [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,11};
UINT4 BfdSessPerfSessUpCount [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,12};
UINT4 BfdSessPerfDiscTime [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,13};
UINT4 BfdSessPerfCtrlPktInHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,14};
UINT4 BfdSessPerfCtrlPktOutHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,15};
UINT4 BfdSessPerfCtrlPktDropHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,16};
UINT4 BfdSessPerfEchoPktInHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,17};
UINT4 BfdSessPerfEchoPktOutHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,18};
UINT4 BfdSessPerfEchoPktDropHC [ ] ={1,3,6,1,4,1,29601,2,56,1,3,1,19};
UINT4 BfdSessDiscMapIndex [ ] ={1,3,6,1,4,1,29601,2,56,1,4,1,1};
UINT4 BfdSessIpMapIndex [ ] ={1,3,6,1,4,1,29601,2,56,1,5,1,1};




tMbDbEntry fsstdbMibEntry[]= {

{{12,BfdAdminStatus}, NULL, BfdAdminStatusGet, BfdAdminStatusSet, BfdAdminStatusTest, BfdAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,BfdSessNotificationsEnable}, NULL, BfdSessNotificationsEnableGet, BfdSessNotificationsEnableSet, BfdSessNotificationsEnableTest, BfdSessNotificationsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,BfdSessIndex}, GetNextIndexBfdSessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessVersionNumber}, GetNextIndexBfdSessTable, BfdSessVersionNumberGet, BfdSessVersionNumberSet, BfdSessVersionNumberTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "1"},

{{13,BfdSessType}, GetNextIndexBfdSessTable, BfdSessTypeGet, BfdSessTypeSet, BfdSessTypeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDiscriminator}, GetNextIndexBfdSessTable, BfdSessDiscriminatorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessRemoteDiscr}, GetNextIndexBfdSessTable, BfdSessRemoteDiscrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDestinationUdpPort}, GetNextIndexBfdSessTable, BfdSessDestinationUdpPortGet, BfdSessDestinationUdpPortSet, BfdSessDestinationUdpPortTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "0"},

{{13,BfdSessSourceUdpPort}, GetNextIndexBfdSessTable, BfdSessSourceUdpPortGet, BfdSessSourceUdpPortSet, BfdSessSourceUdpPortTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "0"},

{{13,BfdSessEchoSourceUdpPort}, GetNextIndexBfdSessTable, BfdSessEchoSourceUdpPortGet, BfdSessEchoSourceUdpPortSet, BfdSessEchoSourceUdpPortTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "0"},

{{13,BfdSessAdminStatus}, GetNextIndexBfdSessTable, BfdSessAdminStatusGet, BfdSessAdminStatusSet, BfdSessAdminStatusTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessState}, GetNextIndexBfdSessTable, BfdSessStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessRemoteHeardFlag}, GetNextIndexBfdSessTable, BfdSessRemoteHeardFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessDiag}, GetNextIndexBfdSessTable, BfdSessDiagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessOperMode}, GetNextIndexBfdSessTable, BfdSessOperModeGet, BfdSessOperModeSet, BfdSessOperModeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDemandModeDesiredFlag}, GetNextIndexBfdSessTable, BfdSessDemandModeDesiredFlagGet, BfdSessDemandModeDesiredFlagSet, BfdSessDemandModeDesiredFlagTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessControlPlaneIndepFlag}, GetNextIndexBfdSessTable, BfdSessControlPlaneIndepFlagGet, BfdSessControlPlaneIndepFlagSet, BfdSessControlPlaneIndepFlagTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessMultipointFlag}, GetNextIndexBfdSessTable, BfdSessMultipointFlagGet, BfdSessMultipointFlagSet, BfdSessMultipointFlagTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessInterface}, GetNextIndexBfdSessTable, BfdSessInterfaceGet, BfdSessInterfaceSet, BfdSessInterfaceTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessSrcAddrType}, GetNextIndexBfdSessTable, BfdSessSrcAddrTypeGet, BfdSessSrcAddrTypeSet, BfdSessSrcAddrTypeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessSrcAddr}, GetNextIndexBfdSessTable, BfdSessSrcAddrGet, BfdSessSrcAddrSet, BfdSessSrcAddrTest, BfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDstAddrType}, GetNextIndexBfdSessTable, BfdSessDstAddrTypeGet, BfdSessDstAddrTypeSet, BfdSessDstAddrTypeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDstAddr}, GetNextIndexBfdSessTable, BfdSessDstAddrGet, BfdSessDstAddrSet, BfdSessDstAddrTest, BfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessGTSM}, GetNextIndexBfdSessTable, BfdSessGTSMGet, BfdSessGTSMSet, BfdSessGTSMTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessGTSMTTL}, GetNextIndexBfdSessTable, BfdSessGTSMTTLGet, BfdSessGTSMTTLSet, BfdSessGTSMTTLTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "0"},

{{13,BfdSessDesiredMinTxInterval}, GetNextIndexBfdSessTable, BfdSessDesiredMinTxIntervalGet, BfdSessDesiredMinTxIntervalSet, BfdSessDesiredMinTxIntervalTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessReqMinRxInterval}, GetNextIndexBfdSessTable, BfdSessReqMinRxIntervalGet, BfdSessReqMinRxIntervalSet, BfdSessReqMinRxIntervalTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessReqMinEchoRxInterval}, GetNextIndexBfdSessTable, BfdSessReqMinEchoRxIntervalGet, BfdSessReqMinEchoRxIntervalSet, BfdSessReqMinEchoRxIntervalTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDetectMult}, GetNextIndexBfdSessTable, BfdSessDetectMultGet, BfdSessDetectMultSet, BfdSessDetectMultTest, BfdSessTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessNegotiatedInterval}, GetNextIndexBfdSessTable, BfdSessNegotiatedIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessNegotiatedEchoInterval}, GetNextIndexBfdSessTable, BfdSessNegotiatedEchoIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessNegotiatedDetectMult}, GetNextIndexBfdSessTable, BfdSessNegotiatedDetectMultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessAuthPresFlag}, GetNextIndexBfdSessTable, BfdSessAuthPresFlagGet, BfdSessAuthPresFlagSet, BfdSessAuthPresFlagTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "2"},

{{13,BfdSessAuthenticationType}, GetNextIndexBfdSessTable, BfdSessAuthenticationTypeGet, BfdSessAuthenticationTypeSet, BfdSessAuthenticationTypeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "-1"},

{{13,BfdSessAuthenticationKeyID}, GetNextIndexBfdSessTable, BfdSessAuthenticationKeyIDGet, BfdSessAuthenticationKeyIDSet, BfdSessAuthenticationKeyIDTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, "-1"},

{{13,BfdSessAuthenticationKey}, GetNextIndexBfdSessTable, BfdSessAuthenticationKeyGet, BfdSessAuthenticationKeySet, BfdSessAuthenticationKeyTest, BfdSessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessStorType}, GetNextIndexBfdSessTable, BfdSessStorTypeGet, BfdSessStorTypeSet, BfdSessStorTypeTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessRowStatus}, GetNextIndexBfdSessTable, BfdSessRowStatusGet, BfdSessRowStatusSet, BfdSessRowStatusTest, BfdSessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BfdSessTableINDEX, 1, 0, 1, NULL},

{{13,BfdSessPerfCtrlPktIn}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktOut}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktDrop}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktDropGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktDropLastTime}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktDropLastTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktIn}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktOut}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktDrop}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktDropGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktDropLastTime}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktDropLastTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessUpTime}, GetNextIndexBfdSessPerfTable, BfdSessUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfLastSessDownTime}, GetNextIndexBfdSessPerfTable, BfdSessPerfLastSessDownTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfLastCommLostDiag}, GetNextIndexBfdSessPerfTable, BfdSessPerfLastCommLostDiagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfSessUpCount}, GetNextIndexBfdSessPerfTable, BfdSessPerfSessUpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfDiscTime}, GetNextIndexBfdSessPerfTable, BfdSessPerfDiscTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktInHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktInHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktOutHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktOutHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfCtrlPktDropHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfCtrlPktDropHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktInHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktInHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktOutHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktOutHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessPerfEchoPktDropHC}, GetNextIndexBfdSessPerfTable, BfdSessPerfEchoPktDropHCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, BfdSessPerfTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessDiscMapIndex}, GetNextIndexBfdSessDiscMapTable, BfdSessDiscMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessDiscMapTableINDEX, 1, 0, 0, NULL},

{{13,BfdSessIpMapIndex}, GetNextIndexBfdSessIpMapTable, BfdSessIpMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, BfdSessIpMapTableINDEX, 5, 0, 0, NULL},
};
tMibData fsstdbEntry = { 59, fsstdbMibEntry };

#endif /* _FSSTDBDB_H */

