#!/bin/csh
# (C) 2010 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   $Id: make.h,v 1.4 2013/09/03 14:55:54 siva Exp $                     |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :Aricent          | 
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 22 Oct 2010                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

BFD_COMPILATION_SWITCHES = -DBFD_TRACE_WANTED $(BFD_EXP_FLAGS)

BFD_FLAGS =$(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)\
            $(BFD_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

BFD = ${BASE_DIR}/bfd
BFDSRC=$(BFD)/src/
BFDOBJ=$(BFD)/obj/
BFDINC=$(BFD)/inc/
BFDINCL= \
    -I$(BASE_DIR)/bfd\
    -I$(BASE_DIR)/bfd/inc\
    -I$(BASE_DIR)/inc/cli\
    -I$(BASE_DIR)/inc/npapi\
    -I$(BASE_DIR)/inc\
    -I$(BASE_DIR)/util/mpls\
    -I$(BASE_DIR)/bfd/src

BFD_TEST_BASE_DIR  = ${BASE_DIR}/bfd/test
BFD_TEST_SRC_DIR   = ${BFD_TEST_BASE_DIR}/src
BFD_TEST_INC_DIR   = ${BFD_TEST_BASE_DIR}/inc
BFD_TEST_OBJ_DIR   = ${BFD_TEST_BASE_DIR}/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

BFD_INCLUDES=$(COMMON_INCLUDE_DIRS) $(BFDINCL)

#############################################################################
