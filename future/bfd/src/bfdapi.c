/*****************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: bfdapi.c,v 1.19 2016/07/18 11:21:11 siva Exp $
*
* Description: This file contains the APIs exported by BFD module.
*****************************************************************************/
#ifndef _BFDAPI_C_
#define _BFDAPI_C_

#include "bfdinc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : BfdApiHandleExtRequest                                     *
 *                                                                           *
 * Description  : This function is the common entry point for all the        *
 *                external module.                                           *
 *                                                                           *
 * Input        : u4ContextId - Context ID                                   *
 *                pBfdReqParams - Pointer to the Request params.             *
 *                                                                           *
 * Output       : pBfdRespParams - Pointer to the response params            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
BfdApiHandleExtRequest (UINT4 u4ContextId,
                        tBfdReqParams * pBfdReqParams,
                        tBfdRespParams * pBfdRespParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tBfdReqParams      *pMsg = NULL;

#ifndef RFC6374_WANTED
    UNUSED_PARAM (pBfdRespParams);
#endif

    /* External Requests Handling */

    if (pBfdReqParams == NULL)
    {
        return OSIX_FAILURE;
    }

    /* External Requests Handling */
    switch (pBfdReqParams->u4ReqType)
    {
        case BFD_PROCESS_BOOTSTRAP_INFO:
            /* This API is called from the LSP Ping module.
             * This message contains the received discriminator
             * value for the BFD session bootstrap or error code
             **/
        case BFD_CREATE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the creation of context
             *
             * Input : u4ContextId-Context id
             *
             **/
        case BFD_DELETE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the deletion of context
             *
             * Input : u4ContextId-Context id
             *
             **/
        case BFD_RX_MSG:
            /* This API is called when a BFD packet is received 
             * from the external modules.
             *
             * Inputs :
             * u4ContextId-Context id
             * pBuf : Pointer to the packet buffer of type 
             * tCRU_BUF_CHAIN_HEADER
             * u4IfIndex :Index on which the packet is received
             **/
        case BFD_PATH_STATUS_CHG:
            /* This API is called by OAM, MPLS Db ,VCCV L2VPN.
             * Applications (BFD, other future applications) which 
             * are registered with OAM will be notified about the path status.
             *
             * Inputs:
             * u4ContextId-Context id
             * u4Session Id -session id
             **/
        case BFD_OFFLD_CB_MSG:
            /* This API is called by NPAPI to notify offloading 
             * event to BFD.
             *
             * Inputs:
             * u4ContextId-Context id
             * pBfdOffCbParams : Pointer to the structure pBfdOffCbParams
             * */
        case BFD_QUE_OVERFLOW_MSG:
            /* This API is called when there is a queue overflow
             * notification and it triggers complete hardware
             * audit.
             *
             * Inputs:
             * Reqtype
             */

        case BFD_SESS_PARAM_CHG_MSG:
            /* This API is called by external module to modify allow param
             * change parameter for a session.Also, this message can be used
             * to modify allow param change status for all sessions configured
             *
             * Inputs:
             * u4ContextId-Context id
             * pBfdOffCbParams : Pointer to the structure pBfdOffCbParams
             * */
        case BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING:
            /* This API is called by external module to register for neignbor
             *  IP path monitoring
             *
             * Inputs:
             * u4ContextId          : Context id
             * BfdClientNbrPathInfo : Neighbor IP path info which needs to be
             *                        monitored
             */
        case BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING:
            /* This API is called by external module to de-register from
             * neignbor IP path monitoring
             *
             * Inputs:
             * u4ContextId          : Context id
             * BfdClientNbrPathInfo : Neighbor IP path info which need not
             *                        to be monitored anymore
             */
        case BFD_CLIENT_DEREGISTER_ALL_FROM_IP_PATH_MONITORING:
            /* This API is called by external module to de-register all
			 * the bfd sessions already registered by client  from neignbor 
			 * IP path monitoring
             *
             * Inputs:
             * u4ContextId          : Context id
             * BfdClientNbrPathInfo : Client info whose sessions need not 
			 *                        to be monitored anymore.
             */
            
        case BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING:
            /* This API is called by external module to disable from
             * neignbor IP path monitoring
             *
             * Inputs:
             * u4ContextId          : Context id
             * BfdClientNbrPathInfo : Neighbor IP path info which needs to be
             *                        ddisabled
             */
#ifdef MBSM_WANTED
        case BFD_MBSM_SLOT_CARD_PARAMS:
            /* This API is called by external module to inform
             * card ID and Slot ID to the BFD.
             *
             * Inputs:
             * u4ContextId-Context id
             * BfdMbsmParams - gives session id, context id , slot num and 
             *                 card num
             * */
#endif /*MBSM_WANTED */
            if (gBfdGlobals.u1IsBfdInitialized == OSIX_FALSE)
            {
                /* BFD Task is not initialized */
                return OSIX_SUCCESS;
            }

            /* Allocate memory for Queue Message */
            if ((pMsg = (tBfdReqParams *)
                 MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
            {
                BfdUtilUpdateMemAlocFail (u4ContextId);
                return OSIX_FAILURE;
            }

            /* Form the message */
            MEMCPY (pMsg, pBfdReqParams, sizeof (tBfdReqParams));
            pMsg->u4ContextId = u4ContextId;
            i4RetVal = (BfdQueEnqMsg (pMsg));
            break;
#ifdef RFC6374_WANTED
	case BFD_GET_SESS_INDEX_FROM_PW_INFO:
	    /* This API is Called By External Module to get Session Index
 	     * from BFD Module.
    	     * 
    	     * Inputs:
    	     * u4ContextId-Context id
    	     * BfdSessParams - Gives PW Info
    	     *
    	     * Output:
    	     * Bfd Session Index - u4BfdSessionIndex*/
	    i4RetVal = BfdApiGetSessIndexFromPwInfo(u4ContextId,pBfdReqParams,pBfdRespParams);
	    break;
	case BFD_GET_SESS_INDEX_FROM_LSP_INFO:
            /* This API is Called By External Module to get Session Index
             * from BFD Module.
             *
             * Inputs:
             * u4ContextId-Context id
             * BfdSessParams - Gives LSP Info
             *
             * Output:
             * Bfd Session Index - u4BfdSessionIndex*/
	    i4RetVal = BfdApiGetSessIndexFromLspInfo(u4ContextId,pBfdReqParams,pBfdRespParams);
	    break;
	case BFD_GET_TXRX_FROM_SESS_INDEX:
	    /* This API Is called by External Module to get Session Tx and Rx
 	     * from BFD Module.
 	     *
 	     * Inputs:
 	     * u4ContextId-Context id
 	     * Bfd Session Index - u4BfdSessionIndex
 	     *
 	     * Output:
 	     * Bfd Session Info - BfdFsMIStdBfdSessEntry*/
	    i4RetVal = BfdGetTxRxFromSessIndex(u4ContextId,pBfdReqParams,pBfdRespParams);
	    break;
    case BFD_GET_EXP_FROM_SESS_INDEX:
        /* This API Is called by External Module to get Exp/TrafficClass
         * from BFD Module.
         * Inputs : 
         * u4ContextId-Context id
         * Bfd Session Index - u4BfdSessionIndex
         *
         * Output :
         *  Bfd Session Info - Exp/TrafficClass*/
         i4RetVal = BfdGetExpFromSessIndex (u4ContextId,pBfdReqParams,pBfdRespParams);
         break;
#endif /* RFC6374_WANTED */
        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* end of switch */
    return i4RetVal;
}

/*****************************************************************************
 * Function     : BfdApiVcmCallback                                          *
 *                                                                           *
 * Description  : This is the call back function registered with VCM Module. *
 *                This call back will get called when a context is created   *
 *                or deleted, notifying BFD module to delete or create the   *
 *                context.                                                   *
 *                                                                           *
 * Input        : IfIndex    - Interface index                               *
 *                u4VcmCxtId - Context Id                                    *
 *                u1Event    - event specifying creation or deletion of      *
 *                context                                                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdApiVcmCallback (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1Event)
{
    tBfdReqParams      *pBfdReqParams = NULL;
    tBfdRespParams      BfdRespParams;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4IpIfIndex);
    UNUSED_PARAM (i4RetVal);

    pBfdReqParams = (tBfdReqParams *) MemAllocMemBlk (BFD_QUEMSG_POOLID);
    if (pBfdReqParams == NULL)
    {
        return;
    }
    MEMSET (pBfdReqParams, 0, sizeof (tBfdReqParams));
    MEMSET (&BfdRespParams, 0, sizeof (tBfdRespParams));

    if ((u1Event & VCM_CONTEXT_CREATE) != 0)
    {
        pBfdReqParams->u4ReqType = BFD_CREATE_CONTEXT_MSG;
        pBfdReqParams->u4ContextId = u4VcmCxtId;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
        if(BfdUdpInitSockLnxVrf(u4VcmCxtId) == OSIX_FAILURE)
        {
            return;
        }
#ifdef LNXIP6_WANTED
        if(BfdUdpv6InitSockLnxVrf(u4VcmCxtId) == OSIX_FAILURE)
        {
            return;
        }
#endif
        BfdUdpAddOrRemoveFdLnxVrf(OSIX_TRUE,u4VcmCxtId);        
#endif
    }

    else if ((u1Event & VCM_CONTEXT_DELETE) != 0)
    {
        pBfdReqParams->u4ReqType = BFD_DELETE_CONTEXT_MSG;
        pBfdReqParams->u4ContextId = u4VcmCxtId;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
        BfdUdpAddOrRemoveFdLnxVrf(OSIX_FALSE,u4VcmCxtId);        
#endif
    }

    i4RetVal =
        BfdApiHandleExtRequest (u4VcmCxtId, pBfdReqParams, &BfdRespParams);
    MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
    return;
}

/*****************************************************************************/
/* Function     : BfdApiUpdateChannelTypes                                   */
/*                                                                           */
/* Description  : This function update the Ach Channel types used by the     */
/*                BFD Module.                                                */
/*                                                                           */
/* Input        : pMplsAchChannelTypeInfo - Pointer Holds all the channnel   */
/*                Types                                                      */
/*                bLockReq - Lock requested                                  */
/*                TRUE - Needs to take BFD_LOCK ()                           */
/*                FALSE - Need not take BFD_LOCK ()                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
BfdApiUpdateChannelTypes (tMplsAchChannelType * pMplsAchChannelTypeInfo,
                          BOOL1 bLockReq)
{
    BFD_FLAG_LOCK (bLockReq);
    gu2BfdAchChnlTypeCcBfd = pMplsAchChannelTypeInfo->u2AchChnlTypeCcBfd;
    gu2BfdAchChnlTypeCvBfd = pMplsAchChannelTypeInfo->u2AchChnlTypeCvBfd;
    gu2BfdAchChnlTypeCcIpv4 = pMplsAchChannelTypeInfo->u2AchChnlTypeCcIpv4;
    gu2BfdAchChnlTypeCvIpv4 = pMplsAchChannelTypeInfo->u2AchChnlTypeCvIpv4;
    gu2BfdAchChnlTypeCcIpv6 = pMplsAchChannelTypeInfo->u2AchChnlTypeCcIpv6;
    gu2BfdAchChnlTypeCvIpv6 = pMplsAchChannelTypeInfo->u2AchChnlTypeCvIpv6;
    BFD_FLAG_UNLOCK (bLockReq);
    return;
}

/************************************************************************
 * Function   : BfdApiCheckControlPlaneDependent                      *
 *                                                                    *
 * Description: Fucntion to check whether the bfd session assosiated  *
 *              with this neighbor is control plane dependent or not  *
 *                                                                    *
 * Input      : u4ContextId- OSPF context id                          *
 *                tBfdClientNbrIpPathInfo - Client neighbor path info *
 *                                                                    *
 * Output     : Monitored path is C bit set or unset                  *
 *                                                                    *
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE                             *
 *                                                                    *
 ************************************************************************/
INT4
BfdApiCheckControlPlaneDependent (UINT4 u4ContextId,
                                  tBfdClientNbrIpPathInfo * pBfdClientInfo)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessInfo = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdSessionEntry = NULL;
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry = NULL;

    pBfdSessInfo = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdSessInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pBfdSessInfo, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    if (pBfdClientInfo == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdSessInfo);
        return OSIX_FAILURE;
    }

    /* Fill the neighbor info */
    pBfdSessInfo->MibObject.u4FsMIStdBfdContextId = u4ContextId;
    pBfdSessInfo->MibObject.i4FsMIStdBfdSessInterface =
        (INT4) pBfdClientInfo->u4IfIndex;

    if (pBfdClientInfo->u1AddrType == BFD_INET_ADDR_IPV4)
    {
        MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessSrcAddr),
                &(pBfdClientInfo->SrcAddr), BFD_IPV4_MAX_ADDR_LEN);
        MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessDstAddr),
                &(pBfdClientInfo->NbrAddr), BFD_IPV4_MAX_ADDR_LEN);
    }
    else if (pBfdClientInfo->u1AddrType == BFD_INET_ADDR_IPV6)
    {
        MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessSrcAddr),
                &(pBfdClientInfo->SrcAddr), BFD_IPV6_MAX_ADDR_LEN);
        MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessDstAddr),
                &(pBfdClientInfo->NbrAddr), BFD_IPV6_MAX_ADDR_LEN);
    }

    pBfdSessIpMapEntry = BfdUtilCheckIpMapEntryExist (pBfdSessInfo);

    if (pBfdSessIpMapEntry != NULL)
    {
        MEMSET (pBfdSessInfo, 0, sizeof (tBfdFsMIStdBfdSessEntry));

        pBfdSessInfo->MibObject.u4FsMIStdBfdContextId = u4ContextId;
        pBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex =
            pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex;

        pBfdSessionEntry = RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                                      (tRBElem *) pBfdSessInfo);
        if ((pBfdSessionEntry) &&
            (pBfdSessionEntry->
             MibObject.i4FsMIStdBfdSessControlPlaneIndepFlag != OSIX_TRUE))
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdSessInfo);

            return OSIX_FAILURE;
        }
    }
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID, (UINT1 *) pBfdSessInfo);
    return OSIX_SUCCESS;
}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : BfdGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCESS/OSIX_FAILURE                             */
/*****************************************************************************/
INT4
BfdGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#ifdef CLI_WANTED
    UINT4               u4ContextId = 0;
    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (BfdCliGetShowCmdOutputToFile ((UINT1 *) BFD_AUDIT_FILE_ACTIVE) !=
            OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "GetShRunFile Failed\n");
            return OSIX_FAILURE;
        }
        if (BfdCliCalcSwAudCheckSum
            ((UINT1 *) BFD_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT,
                     "CalcSwAudChkSum Failed\n");
            return OSIX_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (BfdCliGetShowCmdOutputToFile ((UINT1 *) BFD_AUDIT_FILE_STDBY) !=
            OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "GetShRunFile Failed\n");
            return OSIX_FAILURE;
        }
        if (BfdCliCalcSwAudCheckSum
            ((UINT1 *) BFD_AUDIT_FILE_STDBY, pu2SwAudChkSum) != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT,
                     "CalcSwAudChkSum Failed\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT,
                 "Node is either Active or Standby\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return OSIX_SUCCESS;
}
#endif

#ifdef RFC6374_WANTED
/*****************************************************************************
 * Function     : BfdApiGetSessIndexFromPwInfo                               *
 *                                                                           *
 * Description  : This function called from mpls pm module to get Session    *
 *                Based on Particular PW Path.                               *
 *                                                                           *
 * Input        : u4ContextId - Context ID                                   *
 *                pBfdReqParams - Pointer to the Request params.             *
 *                                                                           *
 * Output       : pBfdRespParams - Pointer to the response params            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4 
BfdApiGetSessIndexFromPwInfo (UINT4 u4ContextId,
			      tBfdReqParams * pBfdReqParams,
			      tBfdRespParams * pBfdRespParams)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdNextBfdSessEntry = NULL;
    tBfdSessPwParams   *pBfdPwInfo = NULL;
    BOOL1               bEntryFound = OSIX_FALSE;

    pBfdCurrentBfdSessEntryInput = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntryInput == NULL)
    {
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdApiGetSessIndexFromPwInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdCurrentBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    do
    {
        pBfdNextBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntryInput);

        if (pBfdNextBfdSessEntry == NULL)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }
        if (pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
                u4ContextId)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

        if (pBfdNextBfdSessEntry->BfdSessPathParams.ePathType == BFD_PATH_TYPE_PW)
        {
            pBfdPwInfo =
                &(pBfdNextBfdSessEntry->BfdSessPathParams.SessPwParams);

            if ((pBfdReqParams->unReqInfo.BfdR6374Info.PathId.SessPwParams.
                        PeerAddr.u4_addr[0] == pBfdPwInfo->PeerAddr.u4_addr[0])&&
                    (pBfdReqParams->unReqInfo.BfdR6374Info.PathId.SessPwParams.
                     u4VcId == pBfdPwInfo->u4VcId) &&
                    (pBfdNextBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessState == BFD_SESS_STATE_UP))
            {
                pBfdRespParams->BfdR6374Info.u4BfdSessionIndex = 
                    pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
                bEntryFound = OSIX_TRUE;
            }
        }

        pBfdNextBfdSessEntry = NULL;

        if (OSIX_TRUE == bEntryFound)
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_SUCCESS;

        }
        else
        {
            continue;
        }
    }
    while (1);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdApiGetSessIndexFromLspInfo                              *
 *                                                                           *
 * Description  : This function is called by mpls pm mpdule to get Session   *
 *                Index based on Particular LSP Path.                        *
 *                                                                           *
 * Input        : u4ContextId - Context ID                                   *
 *                pBfdReqParams - Pointer to the Request params.             *
 *                                                                           *
 * Output       : pBfdRespParams - Pointer to the response params            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
BfdApiGetSessIndexFromLspInfo (UINT4 u4ContextId,
                              tBfdReqParams * pBfdReqParams,
                              tBfdRespParams * pBfdRespParams)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdNextBfdSessEntry = NULL;
    tBfdSessTeParams   *pBfdTnlInfo = NULL;
    BOOL1               bEntryFound = OSIX_FALSE;

    pBfdCurrentBfdSessEntryInput = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntryInput == NULL)
    {
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdApiGetSessIndexFromLspInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdCurrentBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    do
    {
        pBfdNextBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntryInput);

        if (pBfdNextBfdSessEntry == NULL)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }
        if (pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
                u4ContextId)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

        if ((pBfdNextBfdSessEntry->BfdSessPathParams.ePathType ==
                    BFD_PATH_TYPE_TE_IPV4) ||
                (pBfdNextBfdSessEntry->BfdSessPathParams.ePathType ==
                 BFD_PATH_TYPE_TE_IPV6))
        {
            pBfdTnlInfo =
                &(pBfdNextBfdSessEntry->BfdSessPathParams.SessTeParams);

            if ((pBfdReqParams->unReqInfo.BfdR6374Info.PathId.SessTeParams.
                        u4TunnelId == pBfdTnlInfo->u4TunnelId )&&
                    (pBfdNextBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessState == BFD_SESS_STATE_UP))
            {
                pBfdRespParams->BfdR6374Info.u4BfdSessionIndex  =
                    pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
                bEntryFound = OSIX_TRUE;
            }
        }

        pBfdNextBfdSessEntry = NULL;

        if (OSIX_TRUE == bEntryFound)
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_SUCCESS;

        }
        else
        {
            continue;
        }
    }
    while (1);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdGetTxRxFromSessIndex                                    *
 *                                                                           *
 * Description  : This function is used to get the Tx and Rx counter on      *
 *                Session Index of Particular LSP/PW Path                    *
 *                                                                           *
 * Input        : u4ContextId - Context ID                                   *
 *                pBfdReqParams - Pointer to the Request params.             *
 *                                                                           *
 * Output       : pBfdRespParams - Pointer to the response params            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
BfdGetTxRxFromSessIndex(UINT4 u4ContextId,
                        tBfdReqParams * pBfdReqParams,
                        tBfdRespParams * pBfdRespParams)
{
    UINT4 u4SessIndex = 0;	
    INT4    i4RetVal = 0;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    u4SessIndex = pBfdReqParams->unReqInfo.BfdR6374Info.u4BfdSessionIndex;
    pBfdFsMIStdBfdSessEntry =
        BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_SESSION);
        return OSIX_FAILURE;

    }
    i4RetVal = BfdUtilGetBfdOffStats(pBfdFsMIStdBfdSessEntry);
    if (i4RetVal == OSIX_FAILURE)
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_SESSION);
        return OSIX_FAILURE;
    }
    /*Copy the Counter to the Resp Message*/
    pBfdRespParams->BfdR6374Info.u4BfdSessPerfCtrlPktOut = 
            pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut;
    pBfdRespParams->BfdR6374Info.u4BfdSessPerfCtrlPktIn = 
            pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdGetExpFromSessIndex                                     *
 *                                                                           *
 * Description  : This function is used to get the EXP value based on        *
 *                Session Index                                              *
 *                                                                           *
 * Input        : u4ContextId - Context ID                                   *
 *                pBfdReqParams - Pointer to the Request params.             *
 *                                                                           *
 * Output       : pBfdRespParams - Pointer to the response params            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
BfdGetExpFromSessIndex (UINT4 u4ContextId,
                        tBfdReqParams * pBfdReqParams,
                        tBfdRespParams * pBfdRespParams)
{
    UINT4       u4SessIndex = 0;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    u4SessIndex = pBfdReqParams->unReqInfo.BfdR6374Info.u4BfdSessionIndex;
    pBfdFsMIStdBfdSessEntry =
        BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_SESSION);
        return OSIX_FAILURE;
    }
    pBfdRespParams->BfdR6374Info.u4BfdEXPValue = 
                    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue;
   
    return OSIX_SUCCESS;
}
#endif  /* RFC6374_WANTED */
#endif /* _BFDAPI_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdapi.c                       */
/*-----------------------------------------------------------------------*/
