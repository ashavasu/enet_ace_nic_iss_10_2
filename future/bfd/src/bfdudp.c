/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: bfdudp.c,v 1.31 2016/01/11 13:26:38 siva Exp $
 *                                                                           *
 * Description: This file contains the BFD socket related utilities          *
 *              implementation.                                              *
 *****************************************************************************/
#ifndef _BFDUDP_C_
#define _BFDUDP_C_

#include "bfdinc.h"
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
#include "lnxip.h"
PRIVATE
INT4 BfdCheckOperStatus(UINT4 u4Port);
#endif

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpInitSock                                             *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during BFD module boot up. *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpInitSock (VOID)
{
    INT4                i4Flags = 0;
    UINT1               u1OpnVal = 1;
    struct sockaddr_in  BfdLocalAddr;

    /* Open a socket with the standard port 3784 (destination port for 
     * multihop) provided in the standard for reception of BFD packet */
    gBfdGlobals.i4BfdSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4BfdSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 Socket creation failed");
        return OSIX_FAILURE;
    }

    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);

    /* Bind with the socket */
    if (bind (gBfdGlobals.i4BfdSockId, (struct sockaddr *) &BfdLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4BfdSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4BfdSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdGlobals.i4BfdSockId, IPPROTO_IP, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }

    /* Open a socket with the standard port 3784 (destination port for
     * multihop) provided in the standard for reception of BFD packet */
    gBfdGlobals.i4BfdSingleHopSockId =
        socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4BfdSingleHopSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 SingleHop Socket creation failed");
        return OSIX_FAILURE;
    }

    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_DEST_PORT);

    /* Bind with the socket */
    if (bind
        (gBfdGlobals.i4BfdSingleHopSockId, (struct sockaddr *) &BfdLocalAddr,
         sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 SingleHop Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4BfdSingleHopSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4BfdSingleHopSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdGlobals.i4BfdSingleHopSockId, IPPROTO_IP, IP_PKTINFO,
                    (UINT1 *) &u1OpnVal, sizeof (INT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }

    /* Open a socket with the standard port 49155 (A value in source port
     * range 49152 to 65535) provided in the standard for sending the 
     * BFD packet */
    gBfdGlobals.i4BfdTxSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4BfdTxSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 Transmission Socket creation failed");
        return OSIX_FAILURE;
    }

    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_SRC_PORT);

    /* Bind with the socket */
    if (bind (gBfdGlobals.i4BfdTxSockId, (struct sockaddr *) &BfdLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 Transmission Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4BfdTxSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4BfdTxSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdGlobals.i4BfdTxSockId, IPPROTO_IP, IP_PKTINFO,
                    (UINT1 *) &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }

#ifdef IP6_WANTED
    if (BfdUdpv6InitSock () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpAddOrRemoveFd                                        *
 *                                                                           *
 * Description  : This routine is called during module start or shutdown.    *
 *                This routine adds/removes the FD from the socket           *
 *                                                                           *
 * Input        : bIsAddFd - If this flag is set Add FD                      *
 *                           Otherwise Remove FD                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpAddOrRemoveFd (BOOL1 bIsAddFd)
{
    if (bIsAddFd == (UINT1) OSIX_TRUE)
    {
        if ((gBfdGlobals.i4BfdSockId != -1) &&
            (SelAddFd (gBfdGlobals.i4BfdSockId, BfdPktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
        if ((gBfdGlobals.i4BfdSingleHopSockId != -1) &&
            (SelAddFd (gBfdGlobals.i4BfdSingleHopSockId, BfdPktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (gBfdGlobals.i4BfdSockId != -1)
        {
            SelRemoveFd (gBfdGlobals.i4BfdSockId);
        }
        if (gBfdGlobals.i4BfdSingleHopSockId != -1)
        {
            SelRemoveFd (gBfdGlobals.i4BfdSingleHopSockId);
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpTransmitBfdPkt                                       *
 *                                                                           *
 * Description  : This routine transmits the given BFD packet over UDP socket*
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4DestAddr  - Destination IP Address                       *
 *                u1SessionType - session type                               *
 *                pBuf        - Pointer to the linear buffer                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUdpTransmitBfdPkt (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4SrcAddr,
                      UINT4 u4DestAddr, INT4 i4MapType, UINT4 u4PktLen,
                      UINT1 *pu1DscpValue, UINT1 u1SessionType,
                      BOOL1 bIsRouterAlert, UINT1 *pBuf)
{
    struct sockaddr_in  PeerAddr;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    struct cmsghdr     *pCmsgInfo;
    INT4                i4OpnVal = 1;
    INT4                i4SockId = -1;
    UINT4               u4Port = 0;
    INT4                i4Ttl = 255;
    UINT1               au1Cmsg[24];
    UINT4               u4NbrAddr = 0;
    UINT4               u4TmpAddr = 0;

#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    i4SockId = gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId;
#else
    i4SockId = gBfdGlobals.i4BfdTxSockId;
#endif
    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pBuf;
    Iov.iov_len = (size_t) u4PktLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
#ifndef LNXIP4_WANTED
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
#else
    PktInfo.msg_flags = 0;

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_UDP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in_pktinfo));
#endif
    pIpPktInfo = (struct in_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    if (u1SessionType != BFD_SESS_TYPE_SINGLE_HOP)
    {
        if (i4MapType == BFD_PATH_TYPE_IPV4)
        {
            u4NbrAddr = OSIX_NTOHL (u4DestAddr);
            NetIpv4GetSrcAddrToUseForDestInCxt (u4ContextId, u4NbrAddr,
                                                &u4TmpAddr);
            CfaIpIfGetIfIndexFromIpAddressInCxt (u4ContextId, u4TmpAddr,
                                                 &u4IfIndex);
        }
    }

    if ((i4MapType == BFD_PATH_TYPE_IPV4) &&
        (BfdExtGetPortFromIfIndex (u4IfIndex, &u4Port) != OSIX_SUCCESS))
    {
        return OSIX_FAILURE;
    }
    pIpPktInfo->ipi_ifindex = (INT4) u4Port;
    pIpPktInfo->ipi_spec_dst.s_addr = u4SrcAddr;

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);

    if (u1SessionType == BFD_SESS_TYPE_SINGLE_HOP)
    {
        PeerAddr.sin_port = (UINT2) OSIX_HTONS (BFD_UDP_DEST_PORT);
    }
    else
    {
        PeerAddr.sin_port = (UINT2) OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);
    }

    if (bIsRouterAlert == OSIX_TRUE)
    {
        /* Set the ROUTER_ALERT option in the socket if it is requested */
        if (setsockopt (i4SockId, IPPROTO_IP, IP_ROUTER_ALERT,
                        &i4OpnVal, sizeof (INT4)) < 0)
        {
            /* Failed to set Router-Alert Option */
            BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_RA);
            return OSIX_FAILURE;
        }
    }
#ifndef LNXIP4_WANTED
    if (pu1DscpValue != NULL)
    {
        /* Shift the Value to the TOS bits (7-2) set TOS bits (1,0) as 0  */
        i4OpnVal = (INT4) (*pu1DscpValue << BFD_DSCP_TO_TOS_CONVERT_VAL);
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TOS,
                        &i4OpnVal, sizeof (INT4)) < 0)
        {
            /* Failed to set Dscp value */
            BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_DSCP);
            return OSIX_FAILURE;
        }
    }
#else
    if (pu1DscpValue != NULL)
    {
        i4OpnVal = (INT4) (*pu1DscpValue << BFD_DSCP_TO_TOS_CONVERT_VAL);
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TOS,
                        &i4OpnVal, sizeof (INT4)) < 0)

        {
            perror ("BFD setsockopt failed");
        }
    }
#endif
#ifdef IP_WANTED
    if (setsockopt (i4SockId, IPPROTO_IP, IP_PKT_TX_CXTID,
                    &u4ContextId, sizeof (UINT4)) < 0)
    {
        /* Failed to set Context */
        BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_CONTEXT);
        return OSIX_FAILURE;
    }
#endif
    if (u1SessionType == BFD_SESS_TYPE_SINGLE_HOP)
    {
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TTL, &i4Ttl, sizeof (INT4)) <
            0)
        {
            /* Failed to set TTL value */
            BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_TTL);
            return OSIX_FAILURE;
        }
    }
    if (i4MapType == BFD_PATH_TYPE_IPV4)
    {
        /* For MULTIHOP BFD sessions, to make sure that the source address 
           is taken as in the BFD session table i.e the source address that
           has to be in the UDP packet */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
            if ( BfdCheckOperStatus(u4Port) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
#endif
            if (sendmsg (i4SockId, &PktInfo, 0) < 0)
            {
                perror ("BFD send message failed");
                return OSIX_FAILURE;
            }
    }
    /* Send the packet in the socket */
#ifndef BSDCOMP_SLI_WANTED
    if (sendto (i4SockId, pBuf, (INT4) u4PktLen, 0,
                (struct sockaddr *) &PeerAddr, sizeof (struct sockaddr_in)) < 0)
    {
        /* Failed to Transmit packet in socket */
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpProcessPktOnSocket                                   *
 *                                                                           *
 * Description  : This routine reads the data present in the given socket    *
 *                and post the packet into BFD queue for processing          *
 *                                                                           *
 * Input        : i4SockId - Socket Id on which packet is received           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpProcessPktOnSocket (tBfdReqParams * pBfdQMsg)
{
    struct sockaddr_in  PeerAddr;
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    tIpAddr             SrcAddr;
    tIpAddr             DstAddr;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsg = NULL;
    UINT1               au1Cmsg[MPLS_MAX_IP_HDR_LEN];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
#endif
    UINT1              *pu1RxBuf = NULL;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpPktSAddress = 0;
    INT4                i4SockId = 0;
    INT4                i4PktLen = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4AddrLen = 0;
#endif
    UINT4               u4OptVal = 0;
#ifdef IP_WANTED
    UINT4               u4OptLen;
#endif
    MEMSET (&SrcAddr, 0, sizeof (tIpAddr));
    MEMSET (&DstAddr, 0, sizeof (tIpAddr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return OSIX_SUCCESS;
    }

    if (gBfdGlobals.u1IsBfdInitialized == OSIX_FALSE)
    {
        /* BFD module itself is not initialized */
        return OSIX_SUCCESS;
    }

    /* Allocate memory for packet linear buffer */
    pu1RxBuf = (UINT1 *) MemAllocMemBlk (BFD_PKTBUF_POOLID);
    if (pu1RxBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pu1RxBuf, 0, BFD_MAX_CTRL_PKT_LEN);

    i4SockId = pBfdQMsg->unReqInfo.i4SockId;
#ifndef BSDCOMP_SLI_WANTED
    MEMSET (&CmsgInfo, 0, sizeof (struct cmsghdr));
    PktInfo.msg_control = (VOID *) &CmsgInfo;

    while ((i4PktLen = recvfrom (i4SockId, pu1RxBuf, BFD_MAX_CTRL_PKT_LEN, 0,
                                 (struct sockaddr *) &PeerAddr,
                                 &i4AddrLen)) > 0)
    {
        if (recvmsg (i4SockId, &PktInfo, 0) < 0)
        {
            MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
            /* Intialising for receiving from any address */
            continue;
        }
        pIpPktInfo =
            (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u4Port = (UINT4) pIpPktInfo->ipi_ifindex;
        u4IpPktSAddress = OSIX_NTOHL (pIpPktInfo->ipi_addr.s_addr);
#else
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1RxBuf;
    Iov.iov_len = BFD_MAX_CTRL_PKT_LEN;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsg = CMSG_FIRSTHDR (&PktInfo);
#ifndef LNXIP4_WANTED
    pCmsg->cmsg_level = SOL_IP;
#else
    pCmsg->cmsg_level = IPPROTO_UDP;
#endif
    pCmsg->cmsg_type = IP_PKTINFO;
    pCmsg->cmsg_len = sizeof (au1Cmsg);

    while ((i4PktLen = recvmsg (i4SockId, &PktInfo, 0)) > 0)
    {
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u4Port = (UINT4) pIpPktInfo->ipi_ifindex;
#ifdef LNXIP4_WANTED

        u4IpPktSAddress = pIpPktInfo->ipi_addr.s_addr;
#else
        u4IpPktSAddress = OSIX_NTOHL (pIpPktInfo->ipi_addr.s_addr);
#endif
#endif

#ifdef IP_WANTED
        getsockopt (i4SockId, IPPROTO_IP,
                    IP_PKT_RX_CXTID, &u4OptVal, &u4OptLen);
#endif
            MEMCPY (&SrcAddr, &(PeerAddr.sin_addr.s_addr), BFD_IPV4_MAX_ADDR_LEN);
            MEMCPY (&DstAddr, &u4IpPktSAddress, BFD_IPV4_MAX_ADDR_LEN);
            BfdUtilGetIfIndexFromPort (u4Port, &u4IfIndex);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
            if(VcmGetContextIdFromCfaIfIndex(u4IfIndex, &u4OptVal) == VCM_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if ((i4SockId == gBfdLnxVrfSock[u4OptVal].i4BfdSockId) ||
                    (i4SockId == gBfdLnxVrfSock[u4OptVal].i4BfdSingleHopSockId))
            {
                /* Process the received BFD packet */
                BfdRxReceivePacket (u4OptVal, pu1RxBuf, (UINT4) i4PktLen,
                        u4IfIndex, &DstAddr, &SrcAddr,
                        BFD_PACKET_OVER_IPV4);
            }
#else
            if ((i4SockId == gBfdGlobals.i4BfdSockId) ||
                    (i4SockId == gBfdGlobals.i4BfdSingleHopSockId))
            {
                /* Process the received BFD packet */
                BfdRxReceivePacket (u4OptVal, pu1RxBuf, (UINT4) i4PktLen,
                        u4IfIndex, &DstAddr, &SrcAddr,
                        BFD_PACKET_OVER_IPV4);
            }                            /* Packet received on the socket */
#endif
        }


        MemReleaseMemBlock (BFD_PKTBUF_POOLID, pu1RxBuf);
        return OSIX_SUCCESS;
    }
/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpDeInitParams                                         *
 *                                                                           *
 * Description  : This routine de-initializes the UDP data that are needed   *
 *                by the BFD module. This should be invoked whenever BFD     *
 *                module is shutdown. This function closes all the socktes   *
 *                and reset the options created for the sockets              *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
BfdUdpDeInitParams (INT4 i4SockId)
{
    /* Remove the FD association from SLI */
    SelRemoveFd (i4SockId);

    /* Close the socket identifier. */
    close (i4SockId);

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdPktRcvdOnSocket                                         *
 *                                                                           *
 * Description  : This routine should be invoked when packet is received on  *
 *                the socket. This message posts an event with the socket    *
 *                identifier to the BFD Task.                                *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
BfdPktRcvdOnSocket (INT4 i4SockId)
{
    tBfdReqParams      *pMsg = NULL;
    /* Allocate memory for Queue Message */
    if ((pMsg = (tBfdReqParams *) MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
    {
        /* BFD_ISS_TRC (u4ContextId, 23, "BfdPktRcvdOnSocket"); */
        return;
    }

    pMsg->u4ReqType = BFD_RECEIVE_PKT_FROM_SOCK;
    pMsg->unReqInfo.i4SockId = i4SockId;
    /* Post event to BFD Task. Event - BFD_RECEIVE_PKT_FROM_SOCK */
    BfdQueEnqMsg (pMsg);

    return;
}

#ifdef IP6_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpv6InitSock                                           *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during BFD module boot up. *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpv6InitSock (VOID)
{
    INT4                i4Flags = 0;
    struct sockaddr_in6 BfdLocalAddr;
#ifdef LNXIP6_WANTED
    INT4                i4OptVal = 1;
    MEMSET (&BfdLocalAddr, 0, sizeof (struct sockaddr_in6));
#else
    UINT1               u1OpnVal = 1;

#endif

    /* Open a socket with the standard port 3784 (destination port for
     * multihop) provided in the standard for reception of BFD packet */
    gBfdGlobals.i4Bfdv6SockId = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4Bfdv6SockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 Socket creation failed");
        return OSIX_FAILURE;
    }

#ifdef LNXIP6_WANTED
    if (setsockopt (gBfdGlobals.i4Bfdv6SockId, IPPROTO_IPV6, IPV6_V6ONLY,
                    &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("BFD - setsockopt for IPV6_V6ONLY  fails!!");
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdGlobals.i4Bfdv6SockId, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                    &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        perror ("BFD - setsockopt for IPV6_RECVPKTINFO fails!!");
        return OSIX_FAILURE;
    }

#endif

    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);

    /* Bind with the socket */
    if (bind (gBfdGlobals.i4Bfdv6SockId, (struct sockaddr *) &BfdLocalAddr,
              sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4Bfdv6SockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4Bfdv6SockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }
#ifndef LNXIP6_WANTED
    if (setsockopt (gBfdGlobals.i4Bfdv6SockId, IPPROTO_IPV6, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }
#endif

    /* Open a socket with the standard port 3784 (destination port for
     * multihop) provided in the standard for reception of BFD packet */
    gBfdGlobals.i4Bfdv6SingleHopSockId =
        socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4Bfdv6SingleHopSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 SingleHop Socket creation failed");
        return OSIX_FAILURE;
    }
#ifdef LNXIP6_WANTED
    if (setsockopt
        (gBfdGlobals.i4Bfdv6SingleHopSockId, IPPROTO_IPV6, IPV6_V6ONLY,
         &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        perror ("BFD - setsockopt for IPV6_V6ONLY fails!!");
        return OSIX_FAILURE;
    }
#endif

    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_DEST_PORT);

    /* Bind with the socket */
    if (bind
        (gBfdGlobals.i4Bfdv6SingleHopSockId, (struct sockaddr *) &BfdLocalAddr,
         sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 SingleHop Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4Bfdv6SingleHopSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4Bfdv6SingleHopSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

#ifndef LNXIP6_WANTED
    if (setsockopt
        (gBfdGlobals.i4Bfdv6SingleHopSockId, IPPROTO_IPV6, IP_PKTINFO,
         (UINT1 *) &u1OpnVal, sizeof (INT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }
#endif
    /* Open a socket with the standard port 49155 (A value in source port
     * range 49152 to 65535) provided in the standard for sending the
     * BFD packet */
    gBfdGlobals.i4Bfdv6TxSockId = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

    if (gBfdGlobals.i4Bfdv6TxSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 Transmission Socket creation failed");
        return OSIX_FAILURE;
    }
#ifdef LNXIP6_WANTED
    if (setsockopt (gBfdGlobals.i4Bfdv6TxSockId, IPPROTO_IPV6, IPV6_V6ONLY,
                    &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("BFD - setsockopt for IPV6_V6ONLY fails!!");
        return OSIX_FAILURE;
    }
#endif
    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_SRC_PORT);

    /* Bind with the socket */
    if (bind (gBfdGlobals.i4Bfdv6TxSockId, (struct sockaddr *) &BfdLocalAddr,
              sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 Transmission Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdGlobals.i4Bfdv6TxSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdGlobals.i4Bfdv6TxSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

#ifndef LNXIP6_WANTED
    if (setsockopt (gBfdGlobals.i4Bfdv6TxSockId, IPPROTO_IPV6, IP_PKTINFO,
                    (UINT1 *) &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpv6AddOrRemoveFd                                      *
 *                                                                           *
 * Description  : This routine is called during module start or shutdown.    *
 *                This routine adds/removes the FD from the socket           *
 *                                                                           *
 * Input        : bIsAddFd - If this flag is set Add FD                      *
 *                           Otherwise Remove FD                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpv6AddOrRemoveFd (BOOL1 bIsAddFd)
{
    if (bIsAddFd == (UINT1) OSIX_TRUE)
    {
        if ((gBfdGlobals.i4Bfdv6SockId != -1) &&
            (SelAddFd (gBfdGlobals.i4Bfdv6SockId, Bfdv6PktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
        if ((gBfdGlobals.i4Bfdv6SingleHopSockId != -1) &&
            (SelAddFd (gBfdGlobals.i4Bfdv6SingleHopSockId,
                       Bfdv6PktRcvdOnSocket) != OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (gBfdGlobals.i4Bfdv6SockId != -1)
        {
            SelRemoveFd (gBfdGlobals.i4Bfdv6SockId);
        }
        if (gBfdGlobals.i4Bfdv6SingleHopSockId != -1)
        {
            SelRemoveFd (gBfdGlobals.i4Bfdv6SingleHopSockId);
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpv6TransmitBfdPkt                                     *
 *                                                                           *
 * Description  : This routine transmits the given BFD packet over UDP socket*
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                  - Destination IPv6 Address                     *
 *                u1SessionType - session type                               *
 *                pBuf        - Pointer to the linear buffer                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUdpv6TransmitBfdPkt (UINT4 u4ContextId, UINT4 u4IfIndex, tIp6Addr * pSrcAddr,
                        tIp6Addr * pDestAddr, INT4 i4MapType,
                        UINT4 u4PktLen, UINT1 *pu1DscpValue,
                        UINT1 u1SessionType, BOOL1 bIsRouterAlert, UINT1 *pBuf)
{
    struct sockaddr_in6 PeerAddr;
    INT4                i4OpnVal = 1;
    INT4                i4SockId = 0;
    INT4                i4HopLimit = 255;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
#ifndef BSDCOMP_SLI_WANTED
    struct cmsghdr      CmsgInfo;
#else
    UINT4               u4Port = 0;
    UINT1               CmsgInfo[112];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    MEMSET (&Ip6MsgHdr, 0, sizeof (struct msghdr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
#ifndef LNXIP6_WANTED
    i4SockId = gBfdGlobals.i4Bfdv6TxSockId;
#else
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP6_WANTED)
    i4SockId = gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId;    
#else
    i4SockId = gBfdGlobals.i4Bfdv6SingleHopSockId;
#endif
#endif

    PeerAddr.sin6_family = AF_INET6;
    MEMCPY (PeerAddr.sin6_addr.s6_addr, pDestAddr,
            sizeof (PeerAddr.sin6_addr.s6_addr));

    if (u1SessionType == BFD_SESS_TYPE_SINGLE_HOP)
    {
        PeerAddr.sin6_port = (UINT2) OSIX_HTONS (BFD_UDP_DEST_PORT);
    }
    else
    {
        PeerAddr.sin6_port = (UINT2) OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);
    }

#ifdef BSDCOMP_SLI_WANTED
    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
#ifndef LNXIP6_WANTED
    pCmsgInfo->cmsg_level = SOL_IPV6;
#else
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
#endif
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    Iov.iov_base = pBuf;
    Iov.iov_len = (size_t) u4PktLen;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
#endif

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

#ifdef BSDCOMP_SLI_WANTED
    if (BfdExtGetPortFromIfIndex (u4IfIndex, &u4Port) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pIn6Pktinfo->ipi6_ifindex = u4Port;
#else
    pIn6Pktinfo->ipi6_ifindex = (INT4) u4IfIndex;
#endif

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 pSrcAddr);

    if (bIsRouterAlert == OSIX_TRUE)
    {
        /* Set the ROUTER_ALERT option in the socket if it is requested */
        if (setsockopt (i4SockId, IPPROTO_IPV6, IP_ROUTER_ALERT,
                        &i4OpnVal, sizeof (INT4)) < 0)
        {
            /* Failed to set Router-Alert Option */
            BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_RA);
            return OSIX_FAILURE;
        }
    }

    if (pu1DscpValue != NULL)
    {
        /* Shift the Value to the TOS bits (7-2) set TOS bits (1,0) as 0  */
        i4OpnVal = (INT4) (*pu1DscpValue << BFD_DSCP_TO_TOS_CONVERT_VAL);
        if (setsockopt (i4SockId, IPPROTO_IPV6, IP_TOS,
                        &i4OpnVal, sizeof (INT4)) < 0)
        {
            /* Failed to set Dscp value */
            BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_DSCP);
            return OSIX_FAILURE;
        }
    }
#ifdef IP_WANTED
    if (setsockopt (i4SockId, IPPROTO_IPV6, IP_PKT_TX_CXTID,
                    &u4ContextId, sizeof (UINT4)) < 0)
    {
        /* Failed to set Context */
        BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_CONTEXT);
        return OSIX_FAILURE;
    }
#endif

    if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                    &i4HopLimit, sizeof (INT4)) < 0)
    {
        /* Failed to set TTL value */
        BFD_LOG (u4ContextId, BFD_TRC_FAILED_TO_SET_HOPLIMIT);
        return OSIX_FAILURE;
    }

    if (i4MapType == BFD_PATH_TYPE_IPV6)
    {
        if (sendmsg (i4SockId, &Ip6MsgHdr, 0) < 0)
        {
            return OSIX_FAILURE;
        }
    }
#ifndef LNXIP6_WANTED
    /* Send the packet in the socket */
    if (sendto (i4SockId, pBuf, (INT4) u4PktLen, 0,
                (struct sockaddr *) &PeerAddr,
                sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failed to Transmit packet in socket */
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpv6ProcessPktOnSocket                                 *
 *                                                                           *
 * Description  : This routine reads the data present in the given socket    *
 *                and post the packet into BFD queue for processing          *
 *                                                                           *
 * Input        : i4SockId - Socket Id on which packet is received           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpv6ProcessPktOnSocket (tBfdReqParams * pBfdQMsg)
{
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6PktInfo = NULL;
    tIp6Addr            SrcAddr;
    tIp6Addr            DstAddr;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsg = NULL;
    UINT1               au1Cmsg[MPLS_MAX_IP_HDR_LEN];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
#endif
#ifdef LNXIP6_WANTED
    UINT1               CmsgInfo[112];
    INT4                i4OptVal = 1;
#endif
    UINT1              *pu1RxBuf = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4SockId = 0;
    INT4                i4PktLen = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4AddrLen = 0;
#endif
    UINT4               u4OptVal = 0;
#ifdef IP_WANTED
    UINT4               u4OptLen;
#endif
    MEMSET (&SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&DstAddr, 0, sizeof (tIp6Addr));
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&Ip6MsgHdr, 0, sizeof (struct msghdr));
#ifdef LNXIP6_WANTED
    MEMSET (&CmsgInfo, 0, sizeof (CmsgInfo));
#endif

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return OSIX_SUCCESS;
    }

    if (gBfdGlobals.u1IsBfdInitialized == OSIX_FALSE)
    {
        /* BFD module itself is not initialized */
        return OSIX_SUCCESS;
    }

    /* Allocate memory for packet linear buffer */
    pu1RxBuf = (UINT1 *) MemAllocMemBlk (BFD_PKTBUF_POOLID);
    if (pu1RxBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pu1RxBuf, 0, BFD_MAX_CTRL_PKT_LEN);

    i4SockId = pBfdQMsg->unReqInfo.i4SockId;
#ifndef BSDCOMP_SLI_WANTED
    MEMSET (&CmsgInfo, 0, sizeof (struct cmsghdr));
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    while ((i4PktLen = recvfrom (i4SockId, pu1RxBuf, BFD_MAX_CTRL_PKT_LEN, 0,
                                 (struct sockaddr *) &PeerAddr,
                                 &i4AddrLen)) > 0)
    {
        if (recvmsg (i4SockId, &Ip6MsgHdr, 0) < 0)
        {
            MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
            /* Intialising for receiving from any address */
            continue;
        }
        pIn6PktInfo =
            (tIn6Pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));
        u4IfIndex = (UINT4) pIn6PktInfo->ipi6_ifindex;
#else
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
#ifdef LNXIP6_WANTED
    if(i4SockId != 0 )
    {
        if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                    &i4OptVal, sizeof (INT4)) < 0)
        {
            /* setsockopt Failed for IP_PKTINFO */
            perror ("BFD - setsockopt for IPV6_RECVPKTINFO fails!!");
            return OSIX_FAILURE;
        }
    }
#endif

    Ip6MsgHdr.msg_name = (void *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Iov.iov_base = pu1RxBuf;
    Iov.iov_len = BFD_MAX_CTRL_PKT_LEN;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;

#ifndef LNXIP6_WANTED
    Ip6MsgHdr.msg_control = (void *) au1Cmsg;
    Ip6MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsg = CMSG_FIRSTHDR (&Ip6MsgHdr);
    pCmsg->cmsg_level = SOL_IPV6;
    pCmsg->cmsg_type = IP_PKTINFO;
    pCmsg->cmsg_len = sizeof (au1Cmsg);
#else

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
    pCmsg = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsg->cmsg_level = IPPROTO_IPV6;
    pCmsg->cmsg_type = IPV6_PKTINFO;
    pCmsg->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));
#endif

    while ((i4PktLen = recvmsg (i4SockId, &Ip6MsgHdr, 0)) > 0)
    {
        pIn6PktInfo =
            (struct in6_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));
        u4IfIndex = (UINT4) pIn6PktInfo->ipi6_ifindex;
#endif

#ifdef IP_WANTED
        getsockopt (i4SockId, IPPROTO_IPV6,
                    IP_PKT_RX_CXTID, &u4OptVal, &u4OptLen);
#endif
        MEMCPY (&SrcAddr, &(PeerAddr.sin6_addr.s6_addr), sizeof (SrcAddr));
        MEMCPY (&DstAddr, pIn6PktInfo->ipi6_addr.s6_addr, sizeof (DstAddr));
#ifdef LNXIP6_WANTED
        BfdUtilGetIfIndexFromPort (u4IfIndex, &u4IfIndex);
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP6_WANTED)
        if(VcmGetContextIdFromCfaIfIndex(u4IfIndex, &u4OptVal) == VCM_FAILURE)
        {
            return OSIX_FAILURE;
        }
        /* Process the received BFD packet */
        /* Process only when packet is received in the Rx Sock id.
         * As Tx socket is also registered with RX callback, 
         * this will be called for TX sock also */
        if ((i4SockId == gBfdLnxVrfSock[u4OptVal].i4Bfdv6SockId) ||
                (i4SockId == gBfdLnxVrfSock[u4OptVal].i4Bfdv6SingleHopSockId))
        {

            BfdRxReceivePacket (u4OptVal, pu1RxBuf, (UINT4) i4PktLen,
                    u4IfIndex, (tIpAddr *) & DstAddr,
                    (tIpAddr *) & SrcAddr, BFD_PACKET_OVER_IPV6);
        }
#else
        if ((i4SockId == gBfdGlobals.i4Bfdv6SockId) ||
            (i4SockId == gBfdGlobals.i4Bfdv6SingleHopSockId))
        {

            BfdRxReceivePacket (u4OptVal, pu1RxBuf, (UINT4) i4PktLen,
                                u4IfIndex, (tIpAddr *) & DstAddr,
                                (tIpAddr *) & SrcAddr, BFD_PACKET_OVER_IPV6);
        }
#endif    
    }                            /* Packet received on the socket */

    MemReleaseMemBlock (BFD_PKTBUF_POOLID, pu1RxBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : Bfdv6PktRcvdOnSocket                                       *
 *                                                                           *
 * Description  : This routine should be invoked when packet is received on  *
 *                the socket. This message posts an event with the socket    *
 *                identifier to the BFD Task.                                *
 *                                                                           *
 * Input        : i4SockId - Socket Identifier                               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC VOID
Bfdv6PktRcvdOnSocket (INT4 i4SockId)
{
    tBfdReqParams      *pMsg = NULL;

    /* Allocate memory for Queue Message */
    if ((pMsg = (tBfdReqParams *) MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
    {
        return;
    }

    pMsg->u4ReqType = BFD_V6_RECEIVE_PKT_FROM_SOCK;
    pMsg->unReqInfo.i4SockId = i4SockId;
    /* Post event to BFD Task. Event - BFD_V6_RECEIVE_PKT_FROM_SOCK */
    BfdQueEnqMsg (pMsg);

    return;
}
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpInitSockLnxVrf                                             *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during vrf ceration in vcm. *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       i*
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC UINT4
BfdUdpInitSockLnxVrf (UINT4 u4ContextId)
{
    INT4                i4Flags = 0;
    UINT1               u1OpnVal = 1;
    struct sockaddr_in  BfdLocalAddr;

    /* Open a socket with the standard port 3784 (destination port for
     * multihop) provided in the standard for reception of BFD packet */
    gBfdLnxVrfSock[u4ContextId].i4BfdSockId = LnxVrfGetSocketFd(u4ContextId, AF_INET, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);

    if( gBfdLnxVrfSock[u4ContextId].i4BfdSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 Socket creation failed");
        return OSIX_FAILURE;
    }
    /* open a socket for singlehop of BFD packet*/
     gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId  = LnxVrfGetSocketFd(u4ContextId, AF_INET, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);
    if( gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId  < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 SingleHop Socket creation failed");
        return OSIX_FAILURE;
    }
    /* Open a socket with the standard port 49155 (A value in source port
     * range 49152 to 65535) provided in the standard for sending the
     * BFD packet */
    gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId = LnxVrfGetSocketFd(u4ContextId, AF_INET, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);
    if( gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv4 Tx Socket creation failed");
        return OSIX_FAILURE;
    }

    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);


    /* Bind with the socket */
    if (bind ( gBfdLnxVrfSock[u4ContextId].i4BfdSockId, (struct sockaddr *) &BfdLocalAddr,
                sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl ( gBfdLnxVrfSock[u4ContextId].i4BfdSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }
    i4Flags |= O_NONBLOCK;

    if (fcntl ( gBfdLnxVrfSock[u4ContextId].i4BfdSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt ( gBfdLnxVrfSock[u4ContextId].i4BfdSockId, IPPROTO_IP, IP_PKTINFO,
                &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }


    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_DEST_PORT);

    /* Bind with the socket */
    if (bind
            (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId, (struct sockaddr *) &BfdLocalAddr,
             sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 SingleHop Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId, IPPROTO_IP, IP_PKTINFO,
                (UINT1 *) &u1OpnVal, sizeof (INT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }


    BfdLocalAddr.sin_family = AF_INET;
    BfdLocalAddr.sin_addr.s_addr = 0;
    BfdLocalAddr.sin_port = OSIX_HTONS (BFD_UDP_SRC_PORT);

    /* Bind with the socket */
    if (bind (gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId, (struct sockaddr *) &BfdLocalAddr,
                sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv4 Transmission Socket bind failed");
        return OSIX_FAILURE;
    }
    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId, IPPROTO_IP, IP_PKTINFO,
                (UINT1 *) &u1OpnVal, sizeof (UINT1)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpAddOrRemoveFdLnxVrf                                        *
 *                                                                           *
 * Description  : This routine adds/removes the FD from the socket           *
 *                                                                           *
 * Input        : bIsAddFd - If this flag is set Add FD                      *
 *                           Otherwise Remove FD                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
BfdUdpAddOrRemoveFdLnxVrf (BOOL1 bIsAddFd,UINT4 u4ContextId)
{
    if (bIsAddFd == (UINT1) OSIX_TRUE)
    {
        if ((gBfdLnxVrfSock[u4ContextId].i4BfdSockId != -1) &&
                (SelAddFd (gBfdLnxVrfSock[u4ContextId].i4BfdSockId, BfdPktRcvdOnSocket) !=
                 OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
        if ((gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId != -1) &&
                (SelAddFd (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId, BfdPktRcvdOnSocket) !=
                 OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
#ifdef LNXIP6_WANTED
        if ((gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId != -1) &&
            (SelAddFd (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, Bfdv6PktRcvdOnSocket) !=
             OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
        if ((gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId != -1) &&
            (SelAddFd (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId,
                       Bfdv6PktRcvdOnSocket) != OSIX_SUCCESS))
        {
            /* FD Add failed */
            return OSIX_FAILURE;
        }
#endif
    }
    else
    {
        if (gBfdLnxVrfSock[u4ContextId].i4BfdSockId != -1)
        {
            SelRemoveFd (gBfdLnxVrfSock[u4ContextId].i4BfdSockId);
            close(gBfdLnxVrfSock[u4ContextId].i4BfdSockId);
            gBfdLnxVrfSock[u4ContextId].i4BfdSockId = -1;
        }
        if (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId != -1)
        {
            SelRemoveFd (gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId);
            close(gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId);
            gBfdLnxVrfSock[u4ContextId].i4BfdSingleHopSockId = -1;
        }
        if(gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId != -1)
        {
            close(gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId);
            gBfdLnxVrfSock[u4ContextId].i4BfdTxSockId = -1;
        }
#ifdef LNXIP6_WANTED
        if (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId != -1)
        {
            SelRemoveFd (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId);
            close(gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId);
            gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId= -1;
        }
        if (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId != -1)
        {
            SelRemoveFd (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId);
            close(gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId);
            gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId=-1;
        }
        if( gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId != -1)
        {
            close(gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId);
             gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId = -1;
        }
#endif
    }
    return OSIX_SUCCESS;
}
/*****************************************************************************
 *                                                                           *
 * Function     : BfdUdpv6InitSockLnxVrf                                    *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during vrf creation in vcm *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC UINT4
BfdUdpv6InitSockLnxVrf (UINT4 u4ContextId)
{
    INT4                i4Flags = 0;
    struct sockaddr_in6 BfdLocalAddr;
    INT4                i4OptVal = 1;
    MEMSET (&BfdLocalAddr, 0, sizeof (struct sockaddr_in6));

    gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId= LnxVrfGetSocketFd(u4ContextId, AF_INET6, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);
    if( gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 Socket creation failed");
        return OSIX_FAILURE;
    }
    gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId= LnxVrfGetSocketFd(u4ContextId, AF_INET6, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);
    if( gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 SingleHop Socket creation failed");
        return OSIX_FAILURE;
    }
    gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId= LnxVrfGetSocketFd(u4ContextId, AF_INET6, SOCK_DGRAM,
            IPPROTO_UDP, LNX_VRF_OPEN_SOCKET);
    if( gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId < 0)
    {
        /* Failure in opening the UDP socket with port 3784 */
        perror ("BFDv6 Tx Socket creation failed");
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, IPPROTO_IPV6, IPV6_V6ONLY,
                &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("BFD - setsockopt for IPV6_V6ONLY  fails!!");
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        perror ("BFD - setsockopt for IPV6_RECVPKTINFO fails!!");
        return OSIX_FAILURE;
    }


    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_DEST_PORT_MHOP);

    /* Bind with the socket */
    if (bind (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, (struct sockaddr *) &BfdLocalAddr,
                sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt
            (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId, IPPROTO_IPV6, IPV6_V6ONLY,
             &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        perror ("BFD - setsockopt for IPV6_V6ONLY fails!!");
        return OSIX_FAILURE;
    }

    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_DEST_PORT);

    /* Bind with the socket */
    if (bind
            (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId, (struct sockaddr *) &BfdLocalAddr,
             sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 SingleHop Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6SingleHopSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    if (setsockopt (gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId, IPPROTO_IPV6, IPV6_V6ONLY,
                &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IPV6_V6ONLY */
        perror ("BFD - setsockopt for IPV6_V6ONLY fails!!");
        return OSIX_FAILURE;
    }
    BfdLocalAddr.sin6_family = AF_INET6;
    MEMSET (BfdLocalAddr.sin6_addr.s6_addr, 0,
            sizeof (BfdLocalAddr.sin6_addr.s6_addr));
    BfdLocalAddr.sin6_port = OSIX_HTONS (BFD_UDP_SRC_PORT);

    /* Bind with the socket */
    if (bind (gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId, (struct sockaddr *) &BfdLocalAddr,
                sizeof (struct sockaddr_in6)) < 0)
    {
        /* Failure in binding the UDP socket */
        perror ("BFDv6 Transmission Socket bind failed");
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId, F_GETFL, 0)) < 0)
    {
        /* Acquiring the flags for the socket failed */
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (gBfdLnxVrfSock[u4ContextId].i4Bfdv6TxSockId, F_SETFL, i4Flags) < 0)
    {
        /* Set flags for the socket failed */
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCheckOperStatus                                             *
 *                                                                           *
 * Description  : This routine checks if port status is oper up/down.        *
 *                . *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PRIVATE
INT4 BfdCheckOperStatus(UINT4 u4Port)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    if((NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_SUCCESS)
            && (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
            && (NetIpIfInfo.u4Addr != ZERO) )
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }

}


#endif
#endif /* _BFDUDP_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdudp.c                       */
/*-----------------------------------------------------------------------*/
