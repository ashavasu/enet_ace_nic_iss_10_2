/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddsg.c,v 1.3 2012/01/24 13:28:29 siva Exp $
*
* Description: This file contains the routines for DataStructure
*              access for the module BFD
*********************************************************************/

#include "bfdinc.h"

/****************************************************************************
 Function    :  BfdGetFirstFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pBfdFsMIStdBfdGlobalConfigTableEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdGetFirstFsMIStdBfdGlobalConfigTable ()
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *) RBTreeGetFirst (gBfdGlobals.
                                                                 BfdGlbMib.
                                                                 FsMIStdBfdGlobalConfigTable);

    return pBfdFsMIStdBfdGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  BfdGetNextFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                pCurrentBfdFsMIStdBfdGlobalConfigTableEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextBfdFsMIStdBfdGlobalConfigTableEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdGetNextFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                       pCurrentBfdFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry
        *pNextBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pNextBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *) RBTreeGetNext (gBfdGlobals.
                                                                BfdGlbMib.
                                                                FsMIStdBfdGlobalConfigTable,
                                                                (tRBElem *)
                                                                pCurrentBfdFsMIStdBfdGlobalConfigTableEntry,
                                                                NULL);

    return pNextBfdFsMIStdBfdGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  BfdGetFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                pBfdFsMIStdBfdGlobalConfigTableEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetBfdFsMIStdBfdGlobalConfigTableEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdGetFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                   pBfdFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry
        *pGetBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pGetBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *) RBTreeGet (gBfdGlobals.
                                                            BfdGlbMib.
                                                            FsMIStdBfdGlobalConfigTable,
                                                            (tRBElem *)
                                                            pBfdFsMIStdBfdGlobalConfigTableEntry);

    return pGetBfdFsMIStdBfdGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  BfdGetFirstFsMIStdBfdSessTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pBfdFsMIStdBfdSessEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessEntry *
BfdGetFirstFsMIStdBfdSessTable ()
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *) RBTreeGetFirst (gBfdGlobals.BfdGlbMib.
                                                    FsMIStdBfdSessTable);

    return pBfdFsMIStdBfdSessEntry;
}

/****************************************************************************
 Function    :  BfdGetNextFsMIStdBfdSessTable
 Input       :  The Indices
                pCurrentBfdFsMIStdBfdSessEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextBfdFsMIStdBfdSessEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessEntry *
BfdGetNextFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                               pCurrentBfdFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pNextBfdFsMIStdBfdSessEntry = NULL;

    pNextBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *) RBTreeGetNext (gBfdGlobals.BfdGlbMib.
                                                   FsMIStdBfdSessTable,
                                                   (tRBElem *)
                                                   pCurrentBfdFsMIStdBfdSessEntry,
                                                   NULL);

    return pNextBfdFsMIStdBfdSessEntry;
}

/****************************************************************************
 Function    :  BfdGetFsMIStdBfdSessTable
 Input       :  The Indices
                pBfdFsMIStdBfdSessEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetBfdFsMIStdBfdSessEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessEntry *
BfdGetFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pGetBfdFsMIStdBfdSessEntry = NULL;

    pGetBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *) RBTreeGet (gBfdGlobals.BfdGlbMib.
                                               FsMIStdBfdSessTable,
                                               (tRBElem *)
                                               pBfdFsMIStdBfdSessEntry);

    return pGetBfdFsMIStdBfdSessEntry;
}

/****************************************************************************
 Function    :  BfdGetFirstFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pBfdFsMIStdBfdSessDiscMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessDiscMapEntry *
BfdGetFirstFsMIStdBfdSessDiscMapTable ()
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *) RBTreeGetFirst (gBfdGlobals.
                                                           BfdGlbMib.
                                                           FsMIStdBfdSessDiscMapTable);

    return pBfdFsMIStdBfdSessDiscMapEntry;
}

/****************************************************************************
 Function    :  BfdGetNextFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                pCurrentBfdFsMIStdBfdSessDiscMapEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextBfdFsMIStdBfdSessDiscMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessDiscMapEntry *
BfdGetNextFsMIStdBfdSessDiscMapTable (tBfdFsMIStdBfdSessDiscMapEntry *
                                      pCurrentBfdFsMIStdBfdSessDiscMapEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pNextBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pNextBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *) RBTreeGetNext (gBfdGlobals.BfdGlbMib.
                                                          FsMIStdBfdSessDiscMapTable,
                                                          (tRBElem *)
                                                          pCurrentBfdFsMIStdBfdSessDiscMapEntry,
                                                          NULL);

    return pNextBfdFsMIStdBfdSessDiscMapEntry;
}

/****************************************************************************
 Function    :  BfdGetFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                pBfdFsMIStdBfdSessDiscMapEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetBfdFsMIStdBfdSessDiscMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessDiscMapEntry *
BfdGetFsMIStdBfdSessDiscMapTable (tBfdFsMIStdBfdSessDiscMapEntry *
                                  pBfdFsMIStdBfdSessDiscMapEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pGetBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pGetBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *) RBTreeGet (gBfdGlobals.BfdGlbMib.
                                                      FsMIStdBfdSessDiscMapTable,
                                                      (tRBElem *)
                                                      pBfdFsMIStdBfdSessDiscMapEntry);

    return pGetBfdFsMIStdBfdSessDiscMapEntry;
}

/****************************************************************************
 Function    :  BfdGetFirstFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pBfdFsMIStdBfdSessIpMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessIpMapEntry *
BfdGetFirstFsMIStdBfdSessIpMapTable ()
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    pBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *) RBTreeGetFirst (gBfdGlobals.BfdGlbMib.
                                                         FsMIStdBfdSessIpMapTable);

    return pBfdFsMIStdBfdSessIpMapEntry;
}

/****************************************************************************
 Function    :  BfdGetNextFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                pCurrentBfdFsMIStdBfdSessIpMapEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextBfdFsMIStdBfdSessIpMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessIpMapEntry *
BfdGetNextFsMIStdBfdSessIpMapTable (tBfdFsMIStdBfdSessIpMapEntry *
                                    pCurrentBfdFsMIStdBfdSessIpMapEntry)
{
    tBfdFsMIStdBfdSessIpMapEntry *pNextBfdFsMIStdBfdSessIpMapEntry = NULL;

    pNextBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *) RBTreeGetNext (gBfdGlobals.BfdGlbMib.
                                                        FsMIStdBfdSessIpMapTable,
                                                        (tRBElem *)
                                                        pCurrentBfdFsMIStdBfdSessIpMapEntry,
                                                        NULL);

    return pNextBfdFsMIStdBfdSessIpMapEntry;
}

/****************************************************************************
 Function    :  BfdGetFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                pBfdFsMIStdBfdSessIpMapEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetBfdFsMIStdBfdSessIpMapEntry or NULL
****************************************************************************/
tBfdFsMIStdBfdSessIpMapEntry *
BfdGetFsMIStdBfdSessIpMapTable (tBfdFsMIStdBfdSessIpMapEntry *
                                pBfdFsMIStdBfdSessIpMapEntry)
{
    tBfdFsMIStdBfdSessIpMapEntry *pGetBfdFsMIStdBfdSessIpMapEntry = NULL;

    pGetBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *) RBTreeGet (gBfdGlobals.BfdGlbMib.
                                                    FsMIStdBfdSessIpMapTable,
                                                    (tRBElem *)
                                                    pBfdFsMIStdBfdSessIpMapEntry);

    return pGetBfdFsMIStdBfdSessIpMapEntry;
}

/****************************************************************************
 Function    :  BfdGetFsMIStdBfdContextName
 Input       :  The Indices
                pFsMIStdBfdContextName
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdGetFsMIStdBfdContextName (UINT1 *pFsMIStdBfdContextName)
{
    MEMCPY (pFsMIStdBfdContextName,
            gBfdGlobals.BfdGlbMib.au1FsMIStdBfdContextName,
            gBfdGlobals.BfdGlbMib.i4FsMIStdBfdContextNameLen);

    return OSIX_SUCCESS;
}
