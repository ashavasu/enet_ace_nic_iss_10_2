/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdmain.c,v 1.21 2015/03/10 10:45:39 siva Exp $
 *
 * Description: This file contains the bfd task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#ifndef __BFDMAIN_C__
#define __BFDMAIN_C__
#include "bfdinc.h"

/* Proto types of the functions private to this file only */

PRIVATE UINT4 BfdMainMemInit PROTO ((VOID));
PRIVATE VOID BfdMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : BfdMainTask                                               *
*                                                                           *
* Description  : Main function of BFD.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
BfdMainTask ()
{
    UINT4               u4Event = 0;

    while (OSIX_TRUE == OSIX_TRUE)
    {
        if (OsixReceiveEvent (BFD_TIMER_EVENT | BFD_QUEUE_EVENT,
                              OSIX_WAIT, (UINT4) 0, (UINT4 *) &(u4Event))
            == OSIX_SUCCESS)
        {
            BfdMainTaskLock ();

            if ((u4Event & BFD_TIMER_EVENT) == BFD_TIMER_EVENT)
            {
                BfdTmrHandleExpiry ();
            }

            if ((u4Event & BFD_QUEUE_EVENT) == BFD_QUEUE_EVENT)
            {
                BfdQueProcessMsgs ();
            }

            /* Mutual exclusion flag OFF */
            BfdMainTaskUnLock ();
        }
    }
}

/****************************************************************************
*                                                                           *
* Function     : BfdMainTaskInit                                           *
*                                                                           *
* Description  : BFD initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
PUBLIC UINT4
BfdMainTaskInit (VOID)
{
    MEMSET (&gBfdGlobals, 0, sizeof (gBfdGlobals));
    MEMCPY (gBfdGlobals.au1TaskSemName, BFD_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

#ifdef BFD_HA_TEST_WANTED
    MEMSET (&gBfdTestGlobals, 0, sizeof (tBfdTestGlobals));
#endif

    if (OsixCreateSem (BFD_MUT_EXCL_SEM_NAME, BFD_SEM_CREATE_INIT_CNT, 0,
                       &gBfdGlobals.bfdTaskSemId) == OSIX_FAILURE)
    {
        /*BFD_ISS_LOG ( BFD_INVALID_CONTEXT_ID, 1,BFD_MUT_EXCL_SEM_NAME); */
        return OSIX_FAILURE;
    }
    if (BfdUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (BfdRedInitGlobalInfo () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Create buffer pools for data structures */
    if (BfdMainMemInit () == OSIX_FAILURE)
    {
        /*BFD_LOG ((BFD_MAIN_TRC, "Memory Pool Creation Failed\n")); */
        return OSIX_FAILURE;
    }

    if (BfdMainModuleStart () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    BfdTaskRegisterBfdMibs ();
#ifdef SYSLOG_WANTED
    gBfdGlobals.u4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "BFD", SYSLOG_CRITICAL_LEVEL);
#endif

    BfdRedDynDataDescInit ();

    BfdRedDescrTblInit ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : BfdMainModuleStart
 *
 *    DESCRIPTION      : This function allocates memory pools for all tables
 *                       in Bfd module. It also initalizes the global
 *                       structure.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
BfdMainModuleStart (VOID)
{
    if (OsixCreateQ (BFD_QUEUE_NAME, BFD_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gBfdGlobals.bfdQueId)) == OSIX_FAILURE)
    {
        /*BFD_ISS_LOG (BFD_INVALID_CONTEXT_ID, 2); */
        return OSIX_FAILURE;
    }

    if (TmrCreateTimerList ((CONST UINT1 *) BFD_TASK_NAME, BFD_TIMER_EVENT,
                            NULL, (tTimerListId *) & (gBfdGlobals.bfdTmrLst))
        == TMR_FAILURE)
    {
        /*BFD_ISS_LOG (BFD_INVALID_CONTEXT_ID, 5, "BFD MAIN():"); */
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }

    BfdTmrInitTmrDesc ();
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && defined (LNXIP4_WANTED)
    if (BfdUdpInitSockLnxVrf (VCM_DEFAULT_CONTEXT) != OSIX_SUCCESS)
    {
        /* BFD_LOG - Sock Init failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }
#ifdef LNXIP6_WANTED
    /* Create the UDP socket */
    if (BfdUdpv6InitSockLnxVrf (VCM_DEFAULT_CONTEXT) != OSIX_SUCCESS)
    {
        /* BFD_LOG - Sock Init failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }
#endif

    if (BfdUdpAddOrRemoveFdLnxVrf (OSIX_TRUE,VCM_DEFAULT_CONTEXT) != OSIX_SUCCESS)
    {
        /* BFD_LOG - SelAdd failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;

    }
#else
    /* Create the UDP socket */
    if (BfdUdpInitSock () != OSIX_SUCCESS)
    {
        /* BFD_LOG - Sock Init failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }

    if (BfdUdpAddOrRemoveFd (OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* BFD_LOG - SelAdd failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }

#ifdef IP6_WANTED
    if (BfdUdpv6AddOrRemoveFd (OSIX_TRUE) != OSIX_SUCCESS)
    {
        /* BFD_LOG - SelAdd failed */
        TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
        BfdMainMemClear ();
        return OSIX_FAILURE;
    }
#endif
#endif
    if (BfdUtilGetBfdGlobalConfigTable (BFD_DEFAULT_CONTEXT_ID) == NULL)
    {
        if (BfdGblCreateContext (BFD_DEFAULT_CONTEXT_ID) != OSIX_SUCCESS)
        {
            BfdUdpAddOrRemoveFd (OSIX_FALSE);
#ifdef IP6_WANTED
            BfdUdpv6AddOrRemoveFd (OSIX_FALSE);
#endif
            TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
            BfdMainMemClear ();
            return OSIX_FAILURE;
        }
    }

    gBfdGlobals.u1IsBfdInitialized = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdMainModuleShutdown                                      */
/*                                                                           */
/* Description  : This function shuts down the entire BFDmodule.             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
BfdMainModuleShutdown (VOID)
{
    tBfdReqParams      *pBfdQueMsg = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pCxt = NULL;

    BfdMainTaskLock ();
    if (gBfdGlobals.u1IsBfdInitialized != OSIX_TRUE)
    {
        BfdMainTaskUnLock ();
        return;
    }
    gBfdGlobals.u1IsBfdInitialized = OSIX_FALSE;

    if (gBfdGlobals.i4BfdSockId > 0)
    {
        BfdUdpDeInitParams (gBfdGlobals.i4BfdSockId);
        gBfdGlobals.i4BfdSockId = -1;
    }
    if (gBfdGlobals.i4BfdSingleHopSockId > 0)
    {
        BfdUdpDeInitParams (gBfdGlobals.i4BfdSingleHopSockId);
        gBfdGlobals.i4BfdSockId = -1;
    }
    if (gBfdGlobals.i4BfdTxSockId > 0)
    {
        /* Close the socket identifier. */
        BfdUdpDeInitParams (gBfdGlobals.i4BfdTxSockId);
        gBfdGlobals.i4BfdTxSockId = -1;
    }
#ifdef IP6_WANTED
    if (gBfdGlobals.i4Bfdv6SockId > 0)
    {
        BfdUdpDeInitParams (gBfdGlobals.i4Bfdv6SockId);
        gBfdGlobals.i4Bfdv6SockId = -1;
    }
    if (gBfdGlobals.i4Bfdv6SingleHopSockId > 0)
    {
        BfdUdpDeInitParams (gBfdGlobals.i4Bfdv6SingleHopSockId);
        gBfdGlobals.i4Bfdv6SockId = -1;
    }
    if (gBfdGlobals.i4Bfdv6TxSockId > 0)
    {
        /* Close the socket identifier. */
        BfdUdpDeInitParams (gBfdGlobals.i4Bfdv6TxSockId);
        gBfdGlobals.i4Bfdv6TxSockId = -1;
    }
#endif
    if (gBfdGlobals.bfdQueId != 0)
    {
        while (OsixQueRecv (gBfdGlobals.bfdQueId,
                            (UINT1 *) &pBfdQueMsg, OSIX_DEF_MSG_LEN,
                            0) == OSIX_SUCCESS)
        {
            BfdMainReleaseQMemory (pBfdQueMsg);
        }
    }
    if (gBfdGlobals.bfdQueId != 0)
    {
        OsixQueDel (gBfdGlobals.bfdQueId);
        gBfdGlobals.bfdQueId = 0;
    }

    pCxt = BfdGetFirstFsMIStdBfdGlobalConfigTable ();
    while (pCxt != NULL)
    {
        BfdGblDeleteContext (pCxt->MibObject.u4FsMIStdBfdContextId, OSIX_FALSE);
        pCxt = BfdGetNextFsMIStdBfdGlobalConfigTable (pCxt);
    }

    TmrDeleteTimerList (gBfdGlobals.bfdTmrLst);
    BfdTmrDeInitTmrDesc ();

    BfdMainTaskUnLock ();

}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdMainMemInit                                             *
 *                                                                           *
 * Description  : Memory creation and initialization required for BFD        *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PRIVATE UINT4
BfdMainMemInit (VOID)
{
    if (BfdSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdMainMemClear                                            *
 *                                                                           *
 * Description  : Deletes all the memory pools in BFD                        *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
BfdMainMemClear (VOID)
{
    BfdSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
BfdMainDeInit (VOID)
{
    BfdTaskUnRegisterBfdMibs ();
    BfdMainModuleShutdown ();

    BfdMainMemClear ();
    if (gBfdGlobals.bfdTaskSemId)
    {
        OsixSemDel (gBfdGlobals.bfdTaskSemId);
    }
    VcmDeRegisterHLProtocol (BFD_PROTOCOL_ID);
#ifdef SYSLOG_WANTED
    if (gBfdGlobals.u4SysLogId != 0)
    {
        SYS_LOG_DEREGISTER (gBfdGlobals.u4SysLogId);
    }
#endif

    BfdRedDeInitGlobalInfo ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Bfd Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
BfdMainTaskLock (VOID)
{
    BFD_TRC_FUNC ((BFD_FN_ENTRY, "FUNC:BfdMainTaskLock\n"));

    if (OsixSemTake (gBfdGlobals.bfdTaskSemId) == OSIX_FAILURE)
    {
        /*BFD_TRC ((BFD_MAIN_TRC,
           "TakeSem failure for %s \n", BFD_MUT_EXCL_SEM_NAME)); */
        return SNMP_FAILURE;
    }

    /*BFD_TRC_FUNC ((BFD_FN_EXIT, "EXIT:BfdMainTaskLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the BFD Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
BfdMainTaskUnLock (VOID)
{
    /*BFD_TRC_FUNC ((BFD_FN_ENTRY, "FUNC:BfdMainTaskUnLock\n")); */

    OsixSemGive (gBfdGlobals.bfdTaskSemId);

    /*BFD_TRC_FUNC ((BFD_FN_EXIT, "EXIT:BfdMainTaskUnLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : BfdMainReleaseQMemory                                  */
/*                                                                           */
/* DESCRIPTION      : This routine releases the memory of the message in the */
/*                    queue.                                                 */
/*                                                                           */
/* INPUT            : pMsg - Queue Memory                                    */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : VOID                                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
BfdMainReleaseQMemory (tBfdReqParams * pMsg)
{
    if (pMsg->u4ReqType == BFD_RX_MSG)
    {
        if (pMsg->unReqInfo.BfdRecvBfdPacket.pBuf != NULL)
        {
            /* Release the CRU buffer enqueued */
            CRU_BUF_Release_MsgBufChain (pMsg->unReqInfo.BfdRecvBfdPacket.pBuf,
                                         FALSE);
        }
    }
    else if (pMsg->u4ReqType == BFD_OFFLD_CB_MSG)
    {
        if (pMsg->unReqInfo.BfdOffCbParams.pBuf != NULL)
        {
            /* Release the CRU buffer enqueued */
            CRU_BUF_Release_MsgBufChain (pMsg->unReqInfo.BfdOffCbParams.pBuf,
                                         FALSE);
        }
    }
    else if (pMsg->u4ReqType == BFD_PROCESS_BOOTSTRAP_INFO)
    {
        if (pMsg->unReqInfo.BfdLsppInfo.u1ReplyPath != LSPP_NO_REPLY)
        {
            if (pMsg->unReqInfo.BfdLsppInfo.pBuf != NULL)
            {
                CRU_BUF_Release_MsgBufChain
                    (pMsg->unReqInfo.BfdLsppInfo.pBuf, 0);
            }
        }
    }
#ifdef MBSM_WANTED
    else if ((pMsg->u4ReqType == MBSM_MSG_CARD_INSERT) || (pMsg->u4ReqType) ==
             MBSM_MSG_CARD_REMOVE)
    {
        MEM_FREE (pMsg->unReqInfo.pMbsmProtoMsg);
    }
#endif
    MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pMsg);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdMainRegisterWithVcm                                     */
/*                                                                           */
/* Description  : Unregister with VCM                                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_FAILURE/OSIX_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
BfdMainRegisterWithVcm (VOID)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    tVcmRegInfo        *pVcmRegInfo = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_REG_WITH_VCM;

    /* Initialize reg info to structure in External Request Params */
    pVcmRegInfo = &pBfdExtInParams->unReqParams.VcmRegInfo;

    pVcmRegInfo->pIfMapChngAndCxtChng = BfdApiVcmCallback;
    pVcmRegInfo->u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    pVcmRegInfo->u1ProtoId = BFD_PROTOCOL_ID;

    /* Call BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdmain.c                     */
/*-----------------------------------------------------------------------*/
