/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdtrc.c,v 1.3 2014/07/05 11:28:30 siva Exp $
*
* Description: Routines for traces
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "bfdinc.h"

/****************************************************************************
*                                                                           *
* Function     : BfdTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
BfdTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("BFD: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : BfdTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
BfdTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : BfdTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
BfdTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define BFD_TRC_BUF_SIZE    2000
    static CHR1         buf[BFD_TRC_BUF_SIZE];

    MEMSET (&ap, 0, sizeof (va_list));
    if ((u4Flags & BFD_TRC_FLAG) == 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdtrc.c                      */
/*-----------------------------------------------------------------------*/
