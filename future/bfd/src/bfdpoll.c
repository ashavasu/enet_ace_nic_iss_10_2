/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: bfdpoll.c,v 1.21 2015/09/11 09:48:17 siva Exp $
 * Description: This file contains the BFD module core functionality            
 * **********************************************************************/
#ifndef _BFDPOLL_C_
#define _BFDPOLL_C_

#include "bfdinc.h"

PRIVATE INT4        BfdPollSetPAndFBit (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                                        BOOL1 bIsPSet, BOOL1 bIsFSet);

PRIVATE INT4        BfdPollUpdatePollNegParams (tBfdFsMIStdBfdSessEntry *
                                                pBfdSessEntry,
                                                tBfdPktInfo * pBfdRxPktInfo);

PRIVATE UINT4       BfdPollCalcNegTx (UINT4 u4BfdRemoteIntrvl,
                                      UINT4 u4BfdInterval);

/****************************************************************************
 * Function    :  BfdPollSetPAndFBit
 * Description :  This function is used to set the P bit and F bit in BFD
 *                control packet with the given value
 * Input       :  pBfdSessEntry - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PRIVATE INT4
BfdPollSetPAndFBit (tBfdFsMIStdBfdSessEntry * pBfdSessEntry, BOOL1 bIsPSet,
                    BOOL1 bIsFSet)
{
    UINT4               u4Val = 0;

    MEMCPY (&u4Val, pBfdSessEntry->BfdCntlPktBuf.au1PacketBuf, sizeof (UINT4));
    u4Val = OSIX_HTONL (u4Val);

    if (bIsPSet == OSIX_TRUE)
    {
        u4Val |= (BFD_POLL_BIT_LOCATION);
    }
    else
    {
        u4Val &= (~BFD_POLL_BIT_LOCATION);
    }
    if (bIsFSet == OSIX_TRUE)
    {
        u4Val |= (BFD_FINAL_BIT_LOCATION);
    }
    else
    {
        u4Val &= (~BFD_FINAL_BIT_LOCATION);
    }
    u4Val = OSIX_HTONL (u4Val);
    MEMCPY (pBfdSessEntry->BfdCntlPktBuf.au1PacketBuf, &u4Val, sizeof (UINT4));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollInitiatePollSequence
 * Description :  This function is used to initiate poll sequence based
 *                on the parameters changed
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 *                u4ParamChanged - Parameter changed
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdPollInitiatePollSequence (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    if (NULL == pBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    /* Check whether any poll sequence is running already */
    if (pBfdSessEntry->bPollRunning != OSIX_TRUE)
    {
        /* Set the packet with poll bit and new parameters */
        BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_TRUE, OSIX_FALSE);
        pBfdSessEntry->bPollRunning = OSIX_TRUE;
    }
    else
    {
        /* A poll is already running,
         * wait till it ends to update the params */
        pBfdSessEntry->bPollPending = OSIX_TRUE;
    }

    /* Check if offload is enabled and success. Then Call offload
     * for initiating a poll */
    if (BFD_SESS_OFFLD (pBfdSessEntry) == OSIX_TRUE)
    {
        if (pBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0)
        {
            if (BfdOffSessPollInitUpdParams (pBfdSessEntry->
                                             MibObject.u4FsMIStdBfdContextId,
                                             pBfdSessEntry,
                                             &pBfdSessEntry->BfdHwhandle)
                != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            return OSIX_SUCCESS;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollHandlePollSequence
 * Description :  This function is used to handle poll sequence when
 *                a packet is received
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 *                pBfdRxPktInfo - pointer to rx packet structure
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdPollHandlePollSequence (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                           tBfdPktInfo * pBfdRxPktInfo)
{

    if ((pBfdSessEntry == NULL) || (pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.bBfdRxPktPollBit == OSIX_TRUE)
    {
        /* Clear the P bit and Set F bit in Tx control packet */
        BfdPollSetPAndFBit (pBfdSessEntry, OSIX_FALSE, OSIX_TRUE);

        /*If a poll is running in our system, We can terminate it
         * and use F bit to indicate our timing parameters */
        if ((pBfdSessEntry->bPollRunning == OSIX_TRUE) ||
            (pBfdSessEntry->bPollPending == OSIX_TRUE))
        {
            BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_FALSE, OSIX_TRUE);
        }

        /* Update the changed parameters */
        BfdPollUpdatePollNegParams (pBfdSessEntry, pBfdRxPktInfo);

        /* Transmit BFD packet immidiately */
        BfdTxPacket (pBfdSessEntry);

        /* Clear the P bit and F bit in Tx control packet */
        BfdPollSetPAndFBit (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);

        pBfdSessEntry->bPollRunning = OSIX_FALSE;
        pBfdSessEntry->bPollPending = OSIX_FALSE;
    }
    else if (pBfdSessEntry->bPollRunning == OSIX_TRUE)
    {
        if (pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.bBfdRxPktFinalBit ==
            OSIX_TRUE)
        {
            /* Update the changed parameters */
            BfdPollUpdatePollNegParams (pBfdSessEntry, pBfdRxPktInfo);

            /* Clear the P bit and F bit in Tx control packet */
            BfdPollSetPAndFBit (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);
            pBfdSessEntry->bPollRunning = OSIX_FALSE;
        }
    }
    else if (pBfdSessEntry->bPollPending == OSIX_TRUE)
    {
        /* Previous poll has ended, but there is already a poll pending.
         * Since we received a packet after a poll sequence is finished,
         * we can start the new poll sequence */
        pBfdSessEntry->bPollPending = OSIX_FALSE;
        /* Set the packet with poll bit and new parameters */
        BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_TRUE, OSIX_FALSE);
        pBfdSessEntry->bPollRunning = OSIX_TRUE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollHandleOffldPollSeq
 * Description :  This function is used to handle poll sequence when
 *                a packet is received from offload module
 * Input       :  u4ContextId - Context Id of the module
 *                tBfdOffCbParams  - Offload call back parameters
 *                pBfdSessTable - pointer to the session info
 *
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdPollHandleOffldPollSeq (UINT4 u4ContextId,
                           tBfdOffCbParams * pBfdOffCbParams,
                           tBfdFsMIStdBfdSessEntry * pBfdSessTable)
{
    tBfdPktInfo         BfdPktInfo;
    UINT4               u4PktLen;
    UINT1              *pau1RxCtrlPkt = NULL;
    UINT4               u4BfdSessOldNegotiatedInterval = 0;
    UINT4               u4OldNegRemoteTxTimeIntrvl = 0;

    MEMSET (&BfdPktInfo, 0, sizeof (tBfdPktInfo));

    if ((NULL == pBfdOffCbParams) || (NULL == pBfdSessTable))
    {
        return OSIX_FAILURE;
    }
    if (pBfdOffCbParams->pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    /*allocate memory to fill the packet received in call back */
    if ((pau1RxCtrlPkt = MemAllocMemBlk (BFD_PKTBUF_POOLID)) == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_ISS_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                     "BfdPollHandleOffldPollSeq", "BFD_PKTBUF_POOLID");
        return OSIX_FAILURE;
    }
    MEMSET (pau1RxCtrlPkt, 0, BFD_MAX_PKT_LEN);
    u4PktLen = CRU_BUF_Get_ChainValidByteCount (pBfdOffCbParams->pBuf);

    CRU_BUF_Copy_FromBufChain (pBfdOffCbParams->pBuf, pau1RxCtrlPkt, 0,
                               u4PktLen);

    BfdRxParsePacket (u4ContextId, pau1RxCtrlPkt, u4PktLen, 0,
                      BFD_PACKET_OVER_MPLS_RTR, &BfdPktInfo);

    u4BfdSessOldNegotiatedInterval = pBfdSessTable->
        MibObject.u4FsMIStdBfdSessNegotiatedInterval;
    u4OldNegRemoteTxTimeIntrvl = pBfdSessTable->u4NegRemoteTxTimeIntrvl;

    switch (pBfdOffCbParams->eBfdEventType)
    {
        case BFD_OFFLD_SESS_POLL_BIT_RX:
        {
            /* If P bit is received. Sent F bit. If there is a Poll running
             * terminate the poll with the new params*/
            if ((pBfdSessTable->bPollRunning == OSIX_TRUE) ||
                (pBfdSessTable->bPollPending == OSIX_TRUE))
            {
                /* Terminate all the pending sequence with F bit set. This
                 * should inform all the pending changes to the peer */
                pBfdSessTable->bPollRunning = OSIX_FALSE;
                pBfdSessTable->bPollPending = OSIX_FALSE;
            }
            BfdPollUpdateNegParams (pBfdSessTable, &BfdPktInfo);

            BFD_SESS_NEG_DETECT_MULT (pBfdSessTable) =
                BfdPktInfo.BfdCtrlPacket.BfdPktParams.u1BfdRxPktDetectMulti;

            if (pBfdOffCbParams->bActionFlag == OSIX_TRUE)
            {                    /* Action to be taken by Control Plane */
                if (BfdOffSessPollResponse (u4ContextId, pBfdSessTable,
                                            &BfdPktInfo,
                                            &pBfdSessTable->BfdHwhandle)
                    != OSIX_SUCCESS)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_OL_POLL_RESP_FAIL, "BFD",
                             "BfdPollHandleOffldPollSeq",
                             pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);

                    MemReleaseMemBlock (BFD_PKTBUF_POOLID,
                                        (UINT1 *) pau1RxCtrlPkt);
                    return OSIX_FAILURE;
                }
            }
            break;
        }
        case BFD_OFFLD_SESS_FINAL_BIT_RX:
        {
            /* If F bit is received, calculate the new params and update */
            if (pBfdSessTable->bPollRunning == OSIX_TRUE)
            {
                pBfdSessTable->bPollRunning = OSIX_FALSE;
                /* Ignore the Packet since we didn't initiate any poll */
                BfdPollUpdatePollNegParams (pBfdSessTable, &BfdPktInfo);

                if (pBfdOffCbParams->bActionFlag == OSIX_TRUE)
                {                /* Action to be taken by Control Plane */
                    if (BfdOffSessPollTerminate (u4ContextId, pBfdSessTable,
                                                 &BfdPktInfo,
                                                 &pBfdSessTable->BfdHwhandle) !=
                        OSIX_SUCCESS)
                    {
                        BFD_LOG (u4ContextId, BFD_TRC_OL_POLL_RESP_FAIL, "BFD",
                                 "BfdPollHandleOffldPollSeq",
                                 pBfdSessTable->MibObject.
                                 u4FsMIStdBfdSessIndex);
                        MemReleaseMemBlock (BFD_PKTBUF_POOLID,
                                            (UINT1 *) pau1RxCtrlPkt);
                        return OSIX_FAILURE;
                    }
                }

            }
            break;
	}
	case BFD_OFFLD_SESS_OFFLD_FAILED:
	case BFD_OFFLD_SESS_DOWN_TO_INIT:
	case BFD_OFFLD_SESS_DOWN_TO_UP:
	case BFD_OFFLD_SESS_INIT_TO_UP:
	case BFD_OFFLD_SESS_INIT_TO_DOWN:
	case BFD_OFFLD_SESS_UP_TO_DOWN:
	case BFD_OFFLD_SESS_LOC_DEFECT:
	case BFD_OFFLD_SESS_MISCON_DEFECT:
	case BFD_OFFLD_SESS_DEFECT:
	case BFD_OFFLD_SESS_DEFECT_CLEAR:
	case BFD_OFFLD_SESS_PERIOD_MISCONF:
	case BFD_OFFLD_SESS_ADMIN_DOWN_TMR_EXP:
	case BFD_OFFLD_SESS_PERIOD_MISCONF_CLEAR:
	case BFD_OFFLD_SESS_UPD_NEG_DETECT_MULTIPLIER:
	case BFD_OFFLD_SESS_EVENT_ALL:
	default:
        {
            break;
        }
    }
    if (u4BfdSessOldNegotiatedInterval != pBfdSessTable->
        MibObject.u4FsMIStdBfdSessNegotiatedInterval)
    {
        /*Timer Value changed, raise a Trap */
        BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_NEGO_INT_CHANGE_TX,
                 pBfdSessTable->MibObject.u4FsMIStdBfdSessDiscriminator,
                 pBfdSessTable->MibObject.u4FsMIStdBfdSessRemoteDiscr,
                 BFD_MICROSEC_TO_MILLISEC (pBfdSessTable->MibObject.
                                           u4FsMIStdBfdSessNegotiatedInterval));

        BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DETECT_INTRVL,
                 BFD_MICROSEC_TO_MILLISEC ((pBfdSessTable->
                                            u4NegRemoteTxTimeIntrvl *
                                            pBfdSessTable->MibObject.
                                            u4FsMIStdBfdSessNegotiatedDetectMult)),
                 pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);

    }
    if (u4OldNegRemoteTxTimeIntrvl != pBfdSessTable->u4NegRemoteTxTimeIntrvl)
    {
        BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_REMOTE_NEGO_INT_CHANGE,
                 pBfdSessTable->MibObject.u4FsMIStdBfdSessDiscriminator,
                 pBfdSessTable->MibObject.u4FsMIStdBfdSessRemoteDiscr,
                 BFD_MICROSEC_TO_MILLISEC (BfdPktInfo.BfdCtrlPacket.
                                           BfdPktParams.u4BfdRxPktMinTxIntrvl),
                 BFD_MICROSEC_TO_MILLISEC (pBfdSessTable->MibObject.
                                           u4FsMIStdBfdSessReqMinRxInterval),
                 BFD_MICROSEC_TO_MILLISEC (pBfdSessTable->
                                           u4NegRemoteTxTimeIntrvl),
                 BFD_MICROSEC_TO_MILLISEC ((pBfdSessTable->
                                            u4NegRemoteTxTimeIntrvl *
                                            pBfdSessTable->MibObject.
                                            u4FsMIStdBfdSessNegotiatedDetectMult)));
    }
    MemReleaseMemBlock (BFD_PKTBUF_POOLID, (UINT1 *) pau1RxCtrlPkt);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollUpdatePollNegParams
 * Description :  This function is used to update the changed parameters
 *                during poll sequence when a packet is received with poll
 *                bit set
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 *                pBfdRxPktInfo - pointer to rx packet structure
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PRIVATE INT4
BfdPollUpdatePollNegParams (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                            tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4DesiredTx = 0;
    UINT4               u4ReqRx = 0;
    tBfdRxCtrlPktParams *pBfdPktParams = NULL;

    pBfdPktParams = &(pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams);

    /* Get the Tx from the packet */
    MEMCPY (&u4DesiredTx, (pBfdSessEntry->BfdCntlPktBuf.au1PacketBuf +
                           BFD_PACKET_MIN_TX_OFFSET), sizeof (UINT4));
    u4DesiredTx = OSIX_NTOHL (u4DesiredTx);

    /* Get the Rx from the packet */
    MEMCPY (&u4ReqRx, (pBfdSessEntry->BfdCntlPktBuf.au1PacketBuf +
                       BFD_PACKET_MIN_RX_OFFSET), sizeof (UINT4));
    u4ReqRx = OSIX_NTOHL (u4ReqRx);

    /* This Function is expected to be called whenever timer parameters
     * of either remote or local has changed i.e TX or RX intervals */

    pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval =
        BfdPollCalcNegTx (pBfdPktParams->u4BfdRxPktReqMinRxIntrvl, u4DesiredTx);

    /* Rx is based on Remote Tx. Calc remote Tx interval */
    pBfdSessEntry->u4NegRemoteTxTimeIntrvl =
        BfdPollCalcNegTx (u4ReqRx, pBfdPktParams->u4BfdRxPktMinTxIntrvl);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollUpdateNegParams
 * Description :  This function is used to update the changed parameters
 *                timing parameters initially
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 *                pBfdRxPktInfo - pointer to rx packet structure
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdPollUpdateNegParams (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdPktInfo * pBfdRxPktInfo)
{
    tBfdRxCtrlPktParams *pBfdPktParams = NULL;

    if ((NULL == pBfdSessEntry) || (NULL == pBfdRxPktInfo))
    {
        return OSIX_FAILURE;
    }

    pBfdPktParams = &(pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams);

    /* This Function is expected to be called whenever timer parameters
     * of either remote or local has changed i.e TX or RX intervals */
    pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval =
        BfdPollCalcNegTx (pBfdPktParams->u4BfdRxPktReqMinRxIntrvl,
                          pBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessDesiredMinTxInterval);

    /* Rx is based on Remote Tx. Calc remote Tx interval */
    pBfdSessEntry->u4NegRemoteTxTimeIntrvl =
        BfdPollCalcNegTx
        (pBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval,
         pBfdPktParams->u4BfdRxPktMinTxIntrvl);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdPollCalcNegTx
 * Description :  This function is used to calculate the Tx interval
 *                of BFD session( i.e remote or local )
 * Input       :  u4BfdRemoteIntrvl - Remote Systems Rx interval
 *                u4BfdInterval - Local systems Tx interval
 * Output      :  None
 * Returns     :  Negotiated Tx interval
 ****************************************************************************/
PRIVATE UINT4
BfdPollCalcNegTx (UINT4 u4BfdRemoteIntrvl, UINT4 u4BfdInterval)
{
    if ((u4BfdRemoteIntrvl == 0) || (u4BfdInterval == 0))
    {
        /* Negotiated Tx should be equal to the Least Rate reported. Zero
         * is the least rate. Hence use Zero in this case */
        return 0;
    }
    else
    {
        if ((u4BfdRemoteIntrvl) > (u4BfdInterval))
        {
            return u4BfdRemoteIntrvl;
        }
        else
        {
            return u4BfdInterval;
        }
    }
}

#endif /* _BFDPOLL_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdpoll.c                      */
/*-----------------------------------------------------------------------*/
