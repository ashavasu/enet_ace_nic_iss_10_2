/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: bfdrx.c,v 1.45 2016/04/23 08:24:58 siva Exp $
 *                                                                           *
 * Description: This file contains the BFD Packet RX related functions       *
 *****************************************************************************/
#ifndef _BFDRX_C_
#define _BFDRX_C_

#include "bfdinc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxReceivePacket                                         *
 *                                                                           *
 * Description  : Receives the BFD packet and process                        *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                pu1Pkt      - Ponter to the packet buffer                  *
 *                pDstAddr    - Destination address received in the packet   *
 *                pSrcAddr    - Source addres received in the packet         *
 *                u4IfIndex   - Interface index on which packet is received  *
 *                u4RcvdFrom  - Indicate whether the packet received from    *
 *                              MPLS RTR, IPv4 UDP Socket or IPv6 UDP Socket *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxReceivePacket (UINT4 u4ContextId, UINT1 *pu1Pkt, UINT4 u4PktLen,
                    UINT4 u4IfIndex, tIpAddr * pDstAddr, tIpAddr * pSrcAddr,
                    UINT4 u4RcvdFrom)
{
    tBfdPktInfo         BfdPktInfo;
    tBfdFsMIStdBfdSessEntry *pBfdSessCtrl = NULL;
    UINT1               u1PrevSemState = BFD_SESS_STATE_DOWN;

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return OSIX_SUCCESS;
    }

    MEMSET (&BfdPktInfo, 0, sizeof (tBfdPktInfo));

    /* Discard the packet if BFD is not enable or BFD module is not 
       started */
    if (OSIX_SUCCESS != BfdUtilCheckBfdModuleStatus (u4ContextId))
    {
        return OSIX_FAILURE;
    }

    /* Parse the received packet and fill the packet info struct */
    if (BfdRxParsePacket (u4ContextId, pu1Pkt, u4PktLen, u4IfIndex, u4RcvdFrom,
                          &BfdPktInfo) == OSIX_FAILURE)
    {
        /* Failed to parse the received packet */
        return OSIX_FAILURE;
    }

    /* Get the Session Index from the received packet */
    if (BfdRxGetSessInfo (u4ContextId, u4IfIndex, u4RcvdFrom, pDstAddr,
                          pSrcAddr, &BfdPktInfo, &pBfdSessCtrl) == OSIX_FAILURE)
    {
        /* Failed to get the Session information */
        return OSIX_FAILURE;
    }

    /* Validate the packet the received packet */
    if (BfdRxValidateRxPkt (pBfdSessCtrl, &BfdPktInfo) == OSIX_FAILURE)
    {
        /* BFD packet validation failed */
        /* Update pkt drop count and last drop time */
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop, NULL);
        BFD_GET_SYS_TIME (&pBfdSessCtrl->
                          MibObject.u4FsMIStdBfdSessPerfCtrlPktDropLastTime);
        return OSIX_FAILURE;
    }

    /* Process the received packet */
    if ((BfdRxProcessRxPkt (u4ContextId, &BfdPktInfo, pBfdSessCtrl) ==
         OSIX_FAILURE) && (BfdPktInfo.bCvValidationFail != OSIX_TRUE))
    {
        /* Update pkt drop count and last drop time */
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop, NULL);
        BFD_GET_SYS_TIME (&pBfdSessCtrl->
                          MibObject.u4FsMIStdBfdSessPerfCtrlPktDropLastTime);
        return OSIX_FAILURE;
    }

    if ((BfdPktInfo.PathType == BFD_PATH_TYPE_MEP)
        && (BfdPktInfo.bCvValidationFail == OSIX_TRUE))

    {
        /* Update Mis con defect count */
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIBfdSessMisDefCount, NULL);
    }

    if (pBfdSessCtrl->BfdSessPathParams.ePathType == BFD_PATH_TYPE_MEP)
    {
        if (BfdPktInfo.BfdCtrlPacket.BfdPktParams.eBfdRxPktDiag
            == BFD_DIAG_CTRL_DETECTION_TIME_EXP)
        {
            /* This is a RDI Packet */
            BfdUtilUpdatePktCount (&pBfdSessCtrl->
                                   MibObject.u4FsMIBfdSessRdiInCount, NULL);
        }
    }
    u1PrevSemState = (UINT1) BFD_SESS_STATE (pBfdSessCtrl);

    /* Call the core function for further processing */
    if (BfdCoreProcessRxBfdControlPkt (pBfdSessCtrl, &BfdPktInfo) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (u1PrevSemState != BFD_SESS_STATE (pBfdSessCtrl) ||
        (BfdPktInfo.BfdCtrlPacket.BfdPktParams.bBfdRxPktPollBit == TRUE) ||
        (pBfdSessCtrl->LastRxCtrlPktParams.eBfdRxPktDiag !=
         BfdPktInfo.BfdCtrlPacket.BfdPktParams.eBfdRxPktDiag))
    {
        BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                &(pBfdSessCtrl->MibObject.SessDbNode));
        BfdRedSyncDynInfo ();
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxParsePacket                                           *
 *                                                                           *
 * Description  : Parse the received packet and fill the packet info struct  *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                pu1Pkt      - Pointer to the received packet buffer        *
 *                u4RcvdFrom  - Indicate whether the packet received from    *
 *                              MPLS RTR, IPv4 UDP Socket or IPv6 UDP Socket *
 *                u4IfIndex   - Interface Index                              *
 *                                                                           *
 * Output       : pBfdPktInfo - Ponter to the BFD packet information         *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxParsePacket (UINT4 u4ContextId, UINT1 *pu1Pkt, UINT4 u4PktLen,
                  UINT4 u4IfIndex, UINT4 u4RcvdFrom, tBfdPktInfo * pBfdPktInfo)
{
    tMplsAchHdr         AchHdrParams;
    tMplsHdr            MplsHdr;
    UINT4               u4LblStack = 0;
    UINT4               u4OffSet = 0;
    UINT1               u1Val = 0;
    UINT1               u1HdrType = 0;
    tBfdParseRxPkt     *pBfdCtrlPacket = NULL;

    MEMSET (&AchHdrParams, 0, sizeof (tMplsAchHdr));
    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));

    pBfdCtrlPacket = &pBfdPktInfo->BfdCtrlPacket;
    pBfdPktInfo->u4IngrIf = u4IfIndex;
    pBfdPktInfo->pau1RxPktBuf = pu1Pkt;
    pBfdPktInfo->u4PktLen = u4PktLen;

    if (u4PktLen > BFD_MAX_PKT_LEN)
    {
        BFD_LOG (u4ContextId, BFD_TRC_INVALID_PKT_LEN);
        return OSIX_FAILURE;
    }

    if (u4RcvdFrom == BFD_PACKET_OVER_MPLS_RTR)
    {
        /* Get the mpls hdr info, assuming MPLS i.e either PW/GAL label */
        do
        {
            BfdRxGetMplsHdr (pu1Pkt, &MplsHdr);
            u4OffSet += BFD_MPLS_LBL_LEN;
            pu1Pkt += BFD_MPLS_LBL_LEN;
            /* Fill encapsulated CC to check against selected CC in VCCV 
             * if path type is PW */
            if ((MplsHdr.SI == 1) && (MplsHdr.Ttl == 1))
            {
                pBfdPktInfo->u1RecvdCc = BFD_VCCV_CC_TTL;
            }
            if (MplsHdr.u4Lbl == 1)
            {
                pBfdPktInfo->u1RecvdCc = BFD_VCCV_CC_RAL;
            }
            if (u4LblStack < MPLS_MAX_LABEL_STACK)
            {
                MEMCPY (&pBfdPktInfo->MplsHdr[u4LblStack], &MplsHdr,
                        sizeof (tMplsHdr));
            }
            u4LblStack++;
        }
        while ((MplsHdr.SI != 1) && (u4OffSet < u4PktLen));

        if (u4OffSet >= u4PktLen)
        {
            /* Invalid Packet encapsulation. No SI bit set in MPLS Hdr */
            return OSIX_FAILURE;
        }
        /* No. of lable stack */
        pBfdPktInfo->u4LableStack = u4LblStack;

        /* Check whether the next header is IP or ACH */
        BFD_GET_1BYTE (u1Val, pu1Pkt);

        /* Move the pointer to 1 byte to get the original position */
        pu1Pkt -= 1;
        u1HdrType = (UINT1) ((u1Val & 0xf0) >> BFD_HDR_TYPE_OFFSET_BITS);
    }
    else
    {
        /* BFD packet is received on UDP socket. Here direct BFD control 
         * packet will be received. No need to parse and validate IP heade */
        /* Need to conform - Source port validation */

        if (u4RcvdFrom == BFD_PACKET_OVER_IPV6)
        {
            pBfdPktInfo->u1EncapType = BFD_ENCAP_IPV6;
        }
        else
        {
            pBfdPktInfo->u1EncapType = BFD_ENCAP_IPV4;
        }
    }

    if ((u1HdrType == BFD_HEADER_IPV4) || (u1HdrType == BFD_HEADER_IPV6))
    {
        if (u4RcvdFrom == BFD_PACKET_OVER_MPLS_RTR)
        {
            if (u1HdrType == BFD_HEADER_IPV4)
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV4;
            }
            else
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV6;
            }
        }
        if (BfdRxValidateIpUdpHdr (u4ContextId, &pu1Pkt, u4RcvdFrom,
                                   pBfdPktInfo) == OSIX_FAILURE)
        {
            /* IP/UDP Header validation failed */
            return OSIX_FAILURE;
        }
    }
    else if (u1HdrType == BFD_HEADER_ACH)
    {
        /* Parse the ACH headers */
        BfdRxGetAchHdr (pu1Pkt, &AchHdrParams);
        pu1Pkt += BFD_MPLS_ACH_HDR_LEN;

        /* Check the ChannelType and extract the info */
        if ((AchHdrParams.u2ChannelType == BFD_CH_TYPE_CV_BFD) ||
            (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CV_IPV4) ||
            (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CV_IPV6))
        {
            /* CV type BFD message, extract the source MEP Id */
            BfdRxGetSourceMep (&pu1Pkt, pBfdPktInfo);
            if (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CV_IPV4)
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV4_ACH;
                if (BfdRxValidateIpUdpHdr (u4ContextId, &pu1Pkt, u4RcvdFrom,
                                           pBfdPktInfo) == OSIX_FAILURE)
                {
                    /* IPv4/UDP Header validation failed */
                    return OSIX_FAILURE;
                }
            }
            else if (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CV_IPV6)
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV6_ACH;
                if (BfdRxValidateIpUdpHdr (u4ContextId, &pu1Pkt, u4RcvdFrom,
                                           pBfdPktInfo) == OSIX_FAILURE)
                {
                    /* IPv6/UDP Header validation failed */
                    return OSIX_FAILURE;
                }
            }
            else
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_ACH;
            }
        }
        else if ((AchHdrParams.u2ChannelType == BFD_CH_TYPE_CC_IPV4) ||
                 (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CC_IPV6))
        {
            if (AchHdrParams.u2ChannelType == BFD_CH_TYPE_CC_IPV4)
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV4_ACH;
            }
            else
            {
                pBfdPktInfo->u1EncapType = BFD_ENCAP_MPLS_IPV6_ACH;
            }
            if (BfdRxValidateIpUdpHdr (u4ContextId, &pu1Pkt, u4RcvdFrom,
                                       pBfdPktInfo) == OSIX_FAILURE)
            {
                /* IP/UDP Header validation failed */
                return OSIX_FAILURE;
            }
        }
        /* Fill encapsulated CC to check against selected CC in VCCV 
         * if path type is PW */
        if ((pBfdPktInfo->u1RecvdCc != BFD_VCCV_CC_TTL) &&
            (pBfdPktInfo->u1RecvdCc != BFD_VCCV_CC_RAL))
        {
            pBfdPktInfo->u1RecvdCc = BFD_VCCV_CC_ACH;
        }
    }

    /* Extract the BFD control packet */
    BfdRxGetCtrlPacket (pu1Pkt, pBfdCtrlPacket);

    pBfdPktInfo->u2ChannelType = AchHdrParams.u2ChannelType;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxGetSessInfo                                           *
 *                                                                           *
 * Description  : Get the Session node bases on received packet              *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4IfIndex   - Interface index on which packet is received  *
 *                pBfdPktInfo - Ponter to the tBfdPktInfo structure which    *
 *                              contains the received packet information     *
 *                pDstAddr    - Destination address received in the packet   *
 *                              if it received from IPv4/IPv6 UDP socket     *
 *                pSrcAddr    - Source address received in the packet if it  *
 *                              received from IPv4/IPv6 UDP socket           *
 *                pBfdPktInfo - Ponter to the tBfdPktInfo structure which    *
 *                              contains the received packet information     *
 *                                                                           *
 * Output       : pBfdSessCtrl- Pointer to tBfdFsMIStdBfdSessEntry structure *
 *                              which contains BFD session information       *
 *                              If received packet is invalid NULL will be   *
 *                              returned                                     *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxGetSessInfo (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4RcvdFrom,
                  tIpAddr * pDstAddr, tIpAddr * pSrcAddr,
                  tBfdPktInfo * pBfdPktInfo,
                  tBfdFsMIStdBfdSessEntry ** pBfdSessEntry)
{
    UINT4               u4Lable = 0;
    UINT4               u4SessIdx = 0;
    tBfdFsMIStdBfdSessIpMapEntry BfdSessIpMapEntry;
    tBfdExtInParams    *pBfdExtReqInParams = NULL;
    tBfdExtOutParams   *pBfdExtReqOutParams = NULL;
    tBfdParseRxPkt     *pBfdCtrlPacket = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdSessCtrl = NULL;
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntryExist = NULL;
    INT4                i4RetVal = 0;
    INT4                i4SessExists = 0;
    UNUSED_PARAM (i4RetVal);

    pBfdCtrlPacket = &pBfdPktInfo->BfdCtrlPacket;
    pBfdSessCtrl = *pBfdSessEntry;

    MEMSET (&BfdSessIpMapEntry, 0, sizeof (tBfdFsMIStdBfdSessIpMapEntry));
    if (u4RcvdFrom == BFD_PACKET_OVER_IPV4)
    {
        if ((pDstAddr == NULL) || (pSrcAddr == NULL))
        {
            return OSIX_FAILURE;
        }
    }

    /* Allocate memory from mempool */
    pBfdExtReqInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtReqInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdRxGetSessInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtReqInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtReqOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtReqOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdExtReqInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdRxGetSessInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtReqOutParams, 0, sizeof (tBfdExtOutParams));

    pMplsApiOutInfo = &pBfdExtReqOutParams->MplsApiOutInfo;

    /* Check for initial packet. If Your Discriminator is received with 0, 
     * get sesssion index using external API */
    if (pBfdCtrlPacket->u4BfdSessRemoteDiscr == 0)
    {
        if ((u4RcvdFrom == BFD_PACKET_OVER_IPV4) ||
            (u4RcvdFrom == BFD_PACKET_OVER_IPV6))
        {
            BfdSessIpMapEntry.MibObject.u4FsMIStdBfdContextId = u4ContextId;
            BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessInterface =
                (INT4) u4IfIndex;
            if (u4RcvdFrom == BFD_PACKET_OVER_IPV4)
            {
                MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessSrcAddr),
                        pDstAddr, BFD_IPV4_MAX_ADDR_LEN);

                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrLen =
                    BFD_IPV4_MAX_ADDR_LEN;
                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrType =
                    BFD_INET_ADDR_IPV4;

                MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessDstAddr),
                        pSrcAddr, BFD_IPV4_MAX_ADDR_LEN);

                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrLen =
                    BFD_IPV4_MAX_ADDR_LEN;
                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrType =
                    BFD_INET_ADDR_IPV4;
            }
            else if (u4RcvdFrom == BFD_PACKET_OVER_IPV6)
            {
                MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessSrcAddr),
                        pDstAddr, BFD_IPV6_MAX_ADDR_LEN);

                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrLen =
                    BFD_IPV6_MAX_ADDR_LEN;
                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrType =
                    BFD_INET_ADDR_IPV6;

                MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessDstAddr),
                        pSrcAddr, BFD_IPV6_MAX_ADDR_LEN);

                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrLen =
                    BFD_IPV6_MAX_ADDR_LEN;
                BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrType =
                    BFD_INET_ADDR_IPV6;
            }
            /* Check whether the node is already present */
            pBfdSessIpMapEntryExist =
                RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
                           (tRBElem *) & BfdSessIpMapEntry);

            if (pBfdSessIpMapEntryExist == NULL)
            {
                MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                    (UINT1 *) pBfdExtReqInParams);
                MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                    (UINT1 *) pBfdExtReqOutParams);
                return OSIX_FAILURE;
            }
            else
            {
                u4SessIdx =
                    pBfdSessIpMapEntryExist->MibObject.
                    u4FsMIStdBfdSessIpMapIndex;
                i4SessExists = 1;
            }
        }
        else
        {

            /* Get session index using external API by providing tBfdPktInfo and
             * u4IfIndex  EXT_API */
            pBfdExtReqInParams->eExtReqType =
                BFD_REQ_MPLS_GET_PATH_INFO_FROM_INLBL_INFO;
            pBfdExtReqInParams->InApiInfo.InInLblInfo.u4InIf = u4IfIndex;
            /* Get the bottom most lable to get the path information 
             * if GAL Lable present ingnore and get the preivious lable */
            if (pBfdPktInfo->MplsHdr[pBfdPktInfo->u4LableStack - 1].u4Label ==
                BFD_GAL_LABLE)
            {
                BfdRxFormMplsLabel (&pBfdPktInfo->
                                    MplsHdr[pBfdPktInfo->u4LableStack -
                                            BFD_MPLS_BOTTOM_STACK_INDEX_WITH_GAL],
                                    &u4Lable);
                pBfdExtReqInParams->InApiInfo.InInLblInfo.u4Inlabel =
                    pBfdPktInfo->MplsHdr[(pBfdPktInfo->u4LableStack) -
                                         BFD_MPLS_BOTTOM_STACK_INDEX_WITH_GAL].
                    u4Label;
            }
            else
            {
                BfdRxFormMplsLabel (&pBfdPktInfo->
                                    MplsHdr[pBfdPktInfo->u4LableStack - 1],
                                    &u4Lable);
                pBfdExtReqInParams->InApiInfo.InInLblInfo.u4Inlabel =
                    pBfdPktInfo->MplsHdr[(pBfdPktInfo->u4LableStack) -
                                         1].u4Label;
            }

            /* Call BFD Exit Function */
            i4RetVal = BfdPortHandleExtInteraction (pBfdExtReqInParams,
                                                    pBfdExtReqOutParams);

            if ((pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_TUNNEL) ||
                (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_NONTE_LSP))
            {
                /* Lable based search is not applicable for MPLS Tunnel and LSP. 
                 * Discard the packet */
                BFD_LOG (u4ContextId, BFD_TRC_LABEL_BASED_SEARCH_NA);
                MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                    (UINT1 *) pBfdExtReqInParams);
                MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                    (UINT1 *) pBfdExtReqOutParams);
                return OSIX_FAILURE;
            }
            else if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_PW)
            {
                u4SessIdx = pMplsApiOutInfo->OutPwInfo.u4ProactiveSessIndex;
            }
            else if (pMplsApiOutInfo->u4PathType == MPLS_PATH_TYPE_MEG_ID)
            {
                u4SessIdx = pMplsApiOutInfo->OutMegInfo.u4ProactiveSessIndex;
            }
        }
    }
    else
    {
        u4SessIdx = pBfdCtrlPacket->u4BfdSessRemoteDiscr;
    }

    /* Get the session information based on the Session Index */
    pBfdSessCtrl =
        (tBfdFsMIStdBfdSessEntry *) BfdUtilGetBfdSessTable (u4ContextId,
                                                            u4SessIdx);
    if (pBfdSessCtrl == NULL)
    {
        /* Session information not present */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_INFO_NOT_PRESENT);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdExtReqInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtReqOutParams);
        return OSIX_FAILURE;
    }

    /* Discard the packet if
     * 1. BFD session is not ACTIVE
     * 2. BFD session is in ADMIN DOWN */
    if (OSIX_SUCCESS != BfdUtilCheckBfdSessStatus (pBfdSessCtrl))
    {
        /* Update pkt drop count and last drop time */
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop, NULL);
        BFD_GET_SYS_TIME (&pBfdSessCtrl->
                          MibObject.u4FsMIStdBfdSessPerfCtrlPktDropLastTime);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdExtReqInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtReqOutParams);
        return OSIX_FAILURE;
    }

    /* Update Remote Discriminator field. Discriminator value will be
     * the received packet My Discr
     * [OR]
     * Update the Remote discriminator field if
     * SessionIndex already exists [and]
     * Remote Discriminator value and  the incoming discriminator value or not same [and]
     * The bfd session has not been manually configured [and]
     * The route is of type IPv4 and IPv6  */

     if ((pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr == 0)
          || ((u4SessIdx != 0)
              && (pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr !=
                    pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr)
              && (pBfdSessCtrl->bRemDiscrStaticConf == OSIX_FALSE) 
	      && (!BFD_CHECK_PATH_TYPE_NOT_IP (pBfdSessCtrl))))
    
		{
        pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr =
            pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr;
        pBfdSessCtrl->MibObject.u4FsMIBfdSessRemoteDiscr =
            pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr;
        BfdTxFormBfdCtrlPkt (pBfdSessCtrl, OSIX_FALSE, OSIX_FALSE);
    }
    /*  Update the Remote Discriminator incase of change in remote discriminator value , no change in session entries.*/
    else if ((i4SessExists == 1)
              && (pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr != pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr)
              && (pBfdSessCtrl->bRemDiscrStaticConf == OSIX_FALSE))
    {
        pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr = pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr;
        pBfdSessCtrl->MibObject.u4FsMIBfdSessRemoteDiscr = pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr;
        BfdTxFormBfdCtrlPkt (pBfdSessCtrl, OSIX_FALSE, OSIX_FALSE);
    }

    pBfdPktInfo->PathType = pBfdSessCtrl->BfdSessPathParams.ePathType;
    /* Update Last recevied packet content */
    pBfdSessCtrl->LastRxCtrlPktParams.eBfdRxPktDiag
        = pBfdPktInfo->BfdCtrlPacket.BfdPktParams.eBfdRxPktDiag;
    pBfdSessCtrl->LastRxCtrlPktParams.u1BfdRxPktSta
        = pBfdPktInfo->BfdCtrlPacket.BfdPktParams.u1BfdRxPktSta;
    if (pBfdSessCtrl->LastRxCtrlPktParams.BfdPeerIpAddress.u4_addr[0] == 0)
    {
        /*If the IP address is not updated, this is the first BFD packet with
         * IP address received. So Store it in the database */
        pBfdSessCtrl->LastRxCtrlPktParams.BfdPeerIpAddress.u4_addr[0]
            = pBfdPktInfo->PeerAddr.u4_addr[0];
    }
    /* Update encap type */
    pBfdSessCtrl->u1EncapType = pBfdPktInfo->u1EncapType;

    /* Update In Pkt count */
    BfdUtilUpdatePktCount (&pBfdSessCtrl->
                           MibObject.u4FsMIStdBfdSessPerfCtrlPktIn,
                           &pBfdSessCtrl->
                           MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC);

    if ((pBfdPktInfo->u2ChannelType == BFD_CH_TYPE_CV_IPV4) ||
        (pBfdPktInfo->u2ChannelType == BFD_CH_TYPE_CV_IPV6) ||
        (pBfdPktInfo->u2ChannelType == BFD_CH_TYPE_CV_BFD))
    {

        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIBfdSessPerfCVPktIn, NULL);
    }
    else
    {
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIBfdSessPerfCCPktIn, NULL);
    }

    *pBfdSessEntry = pBfdSessCtrl;
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtReqInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtReqOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxGetCtrlPacket                                         *
 *                                                                           *
 * Description  : Get the BFD control packet detail from the given buffer    *
 *                                                                           *
 * Input        : pu1Buf         - Pointer to the buffer                     *
 *                                                                           *
 * Output       : pBfdCtrlPacket - Pointer to tBfdParseRxPkt to parse the    *
 *                                 received BFD packet                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdRxGetCtrlPacket (UINT1 *pu1Buf, tBfdParseRxPkt * pBfdCtrlPacket)
{
    UINT4               u4Val = 0;

    BFD_GET_4BYTE (u4Val, pu1Buf);
    pBfdCtrlPacket->BfdPktParams.u1BfdRxPktVersionNumber = (UINT1)
        ((u4Val & 0xe0000000) >> BFD_SESS_VERSION_NUM_OFFSET_BITS);
    pBfdCtrlPacket->BfdPktParams.eBfdRxPktDiag = ((u4Val & 0x1f000000) >>
                                                  BFD_SESS_DIAG_OFFSET_BITS);
    pBfdCtrlPacket->BfdPktParams.u1BfdRxPktSta = (UINT1)
        ((u4Val & 0x00c00000) >> BFD_SESS_STATE_OFFSET_BITS);

    if ((u4Val & BFD_POLL_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->BfdPktParams.bBfdRxPktPollBit = TRUE;
    }
    if ((u4Val & BFD_FINAL_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->BfdPktParams.bBfdRxPktFinalBit = TRUE;
    }
    if ((u4Val & BFD_CPINDE_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->BfdPktParams.bBfdRxPktCFlag = TRUE;
    }
    if ((u4Val & BFD_AUTH_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->bBfdAuthBit = TRUE;
    }
    if ((u4Val & BFD_DEMAND_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->BfdPktParams.bBfdRxPktDemandMode = TRUE;
    }
    if ((u4Val & BFD_MULTIPOINT_BIT_LOCATION) != 0)
    {
        pBfdCtrlPacket->bBfdMBit = TRUE;
    }
    pBfdCtrlPacket->BfdPktParams.u1BfdRxPktDetectMulti = (UINT1)
        ((u4Val & 0x0000ff00) >> BFD_SESS_DETECT_MULTIPLIER_OFFSET_BITS);
    pBfdCtrlPacket->u1BfdPktLen = (UINT1) (u4Val & 0x000000ff);

    BFD_GET_4BYTE (pBfdCtrlPacket->u4BfdSessDiscr, pu1Buf);
    BFD_GET_4BYTE (pBfdCtrlPacket->u4BfdSessRemoteDiscr, pu1Buf);
    BFD_GET_4BYTE (pBfdCtrlPacket->BfdPktParams.u4BfdRxPktMinTxIntrvl, pu1Buf);
    BFD_GET_4BYTE (pBfdCtrlPacket->BfdPktParams.u4BfdRxPktReqMinRxIntrvl,
                   pu1Buf);
    BFD_GET_4BYTE (pBfdCtrlPacket->u4BfdEchoRxIntrvl, pu1Buf);

    if (pBfdCtrlPacket->bBfdAuthBit == TRUE)
    {
        BFD_GET_1BYTE (pBfdCtrlPacket->u1BfdAuthType, pu1Buf);
        BFD_GET_1BYTE (pBfdCtrlPacket->u1BfdAuthLen, pu1Buf);
        BFD_GET_1BYTE (pBfdCtrlPacket->u1BfdAuthKeyId, pu1Buf);
        MEMCPY (pBfdCtrlPacket->au1BfdPasswd, pu1Buf,
                (pBfdCtrlPacket->u1BfdAuthLen - BFD_AUTH_TLID_LEN));
    }

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxProcessRxPkt                                          *
 *                                                                           *
 * Description  : Process the received BFD packet. Authentication check if   *
 *                authentication present and MEP TLV validation              *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                pBfdPktInfo - Ponter to the tBfdPktInfo structure which    *
 *                              contains the received packet information     *
 *                pBfdSessCtrl- Pointer to tBfdFsMIStdBfdSessEntry structure *
 *                              which contains BFD session information       *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxProcessRxPkt (UINT4 u4ContextId, tBfdPktInfo * pBfdPktInfo,
                   tBfdFsMIStdBfdSessEntry * pBfdSessCtrl)
{
    tBfdMplsPathInfo   *pMplsPathInfo = NULL;
    INT4                i4RetVal = 0;
    UNUSED_PARAM (i4RetVal);

    /* Allocate memory for MPLS Path info */
    pMplsPathInfo = (tBfdMplsPathInfo *)
        MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
    if (pMplsPathInfo == NULL)
    {
        BfdUtilUpdateMemAlocFail
            (pBfdSessCtrl->MibObject.u4FsMIStdBfdContextId);
        return OSIX_FAILURE;
    }
    MEMSET (pMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));
    /* Get the complete path information */
    i4RetVal = BfdExtGetMplsPathInfo (u4ContextId,
                                      &pBfdSessCtrl->BfdSessPathParams,
                                      pMplsPathInfo, OSIX_FALSE);

    /* If path type is ME, Check the received encap type and 
     * configured encap type (CC or CV) */
    if (pBfdPktInfo->PathType == BFD_PATH_TYPE_MEP)
    {
        if (pBfdSessCtrl->MibObject.i4FsMIBfdSessMode == BFD_SESS_MODE_CCV)
        {
            if ((pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CV_IPV4) &&
                (pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CV_IPV6) &&
                (pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CV_BFD))
            {
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                /* Received invalid encap type */
                pBfdPktInfo->bCvValidationFail = OSIX_TRUE;
                BFD_LOG (u4ContextId, BFD_TRC_RECE_INVALID_ENCAP_TYPE);
                return OSIX_FAILURE;
            }
        }
        else
        {
            if ((pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CC_IPV4) &&
                (pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CC_IPV6) &&
                (pBfdPktInfo->u2ChannelType != BFD_CH_TYPE_CC_BFD))
            {
                if (!(((pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
                        BFD_ENCAP_TYPE_MPLS_IP) ||
                       (pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
                        BFD_ENCAP_TYPE_MPLS_IP_ACH)) &&
                      ((pBfdPktInfo->u1EncapType == BFD_ENCAP_MPLS_IPV4) ||
                       (pBfdPktInfo->u1EncapType == BFD_ENCAP_IPV4))))
                {

                    /* Received invalid encap type. I.e when the supported
                     * encapsulation is either MPLSIP or MPLSACHIP, rx
                     * packet can be MPLS_IP encap or IP encap. If this is not
                     * the case return failure*/
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    BFD_LOG (u4ContextId, BFD_TRC_RECE_INVALID_ENCAP_TYPE);
                    return OSIX_FAILURE;
                }
            }
        }
        if ((pBfdPktInfo->u2ChannelType == BFD_CH_TYPE_CV_IPV4) ||
            (pBfdPktInfo->u2ChannelType == BFD_CH_TYPE_CV_BFD))
        {
            if (BfdRxProcessCvPacket (u4ContextId, pBfdPktInfo,
                                      pMplsPathInfo) == OSIX_FAILURE)
            {
                /* Unique MEP-ID validation failed */
                pBfdPktInfo->bCvValidationFail = OSIX_TRUE;
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
        }
        /* Check whether MEG for PW */
        if (pMplsPathInfo->MplsMegApiInfo.u1ServiceType == OAM_SERVICE_TYPE_PW)
        {
            /* Check whether received CC and selected CC on VCCV are same */
            if (pBfdPktInfo->u1RecvdCc !=
                pMplsPathInfo->MplsPwApiInfo.u1CcSelected)
            {
                /* VCCV CC Verification failed */
                BFD_LOG (u4ContextId, BFD_TRC_VCCV_CC_VERI_FAILED);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
            /* If PW receives GAL, return failure */
            if (pBfdPktInfo->MplsHdr[pBfdPktInfo->u4LableStack - 1].u4Label ==
                BFD_GAL_LABLE)
            {
                /* Packet validation failed due to PW receives GAL */
                pBfdPktInfo->bCvValidationFail = OSIX_TRUE;
                BFD_LOG (u4ContextId, BFD_TRC_PW_RECV_GAL);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
            /* Check whether BFD is enabled on VCCV */
            if ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
                 (BFD_CV_IP_UDP_ENCAP | BFD_CV_IP_UDP_ENCAP_STATUS_SIG |
                  BFD_CV_ACH_ENCAP | BFD_CV_ACH_ENCAP_STATUS_SIG)) == 0)
            {
                /* VCCV CV Verification failed */
                BFD_LOG (u4ContextId, BFD_TRC_VCCV_CV_VERI_FAILED);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
        }
    }
    else if (pBfdPktInfo->PathType == BFD_PATH_TYPE_PW)
    {
        /* Check whether received CC and selected CC on VCCV are same */
        if (pBfdPktInfo->u1RecvdCc != pMplsPathInfo->MplsPwApiInfo.u1CcSelected)
        {
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            /* VCCV CC Verification failed */
            BFD_LOG (u4ContextId, BFD_TRC_VCCV_CC_VERI_FAILED);
            return OSIX_FAILURE;
        }
        /* If PW receives GAL, return failure */
        if (pBfdPktInfo->MplsHdr[pBfdPktInfo->u4LableStack - 1].u4Label ==
            BFD_GAL_LABLE)
        {
            /* Packet validation failed due to PW receives GAL */
            pBfdPktInfo->bCvValidationFail = OSIX_TRUE;
            BFD_LOG (u4ContextId, BFD_TRC_PW_RECV_GAL);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }

        /* Check whether BFD is enabled on VCCV */
        if ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
             (BFD_CV_IP_UDP_ENCAP | BFD_CV_IP_UDP_ENCAP_STATUS_SIG |
              BFD_CV_ACH_ENCAP | BFD_CV_ACH_ENCAP_STATUS_SIG)) == 0)
        {
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            /* VCCV CV Verification failed */
            BFD_LOG (u4ContextId, BFD_TRC_VCCV_CV_VERI_FAILED);
            return OSIX_FAILURE;
        }
    }
    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID, (UINT1 *) pMplsPathInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxFormMplsLabel                                         *
 *                                                                           *
 * Description  : Function is used to put MPLS Header information into single*
 *                variable                                                   *
 *                                                                           *
 * Input        : pHdr   - pointer to tMplsHdr which contains MPLS           *
 *                         header information                                *
 *                                                                           *
 * Output       : pu4Lable - Variable in which MPLS Header information filled*
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdRxFormMplsLabel (tMplsLblInfo * pHdr, UINT4 *pu4Lable)
{
    UINT4               u4Lable = 0;

    u4Lable = ((pHdr->u4Label << BFD_LBL_OFFSET_BITS) & 0xfffff000) |
        (((UINT4) pHdr->u1Exp << BFD_EXP_OFFSET_BITS) & 0x00000e00) |
        (((UINT4) pHdr->u1SI << BFD_SI_OFFSET_BITS) & 0x00000100) |
        ((UINT4) pHdr->u1Ttl & 0x000000ff);
    *pu4Lable = u4Lable;
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxGetMplsHdr                                            *
 *                                                                           *
 * Description  : Parse the MPLS Header from the given buffer                *
 *                                                                           *
 * Input        : pBuf   - Pointer to the buffer                             *
 *                                                                           *
 * Output       : pHdr   - pointer to tMplsHdr to parse the MPLS header      *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdRxGetMplsHdr (UINT1 *pBuf, tMplsHdr * pHdr)
{
    UINT4               u4Hdr = 0;

    BFD_GET_4BYTE (u4Hdr, pBuf);
    pHdr->u4Lbl = (u4Hdr & 0xfffff000) >> BFD_LBL_OFFSET_BITS;
    pHdr->Exp = (u4Hdr & 0x00000e00) >> BFD_EXP_OFFSET_BITS;
    pHdr->SI = (u4Hdr & 0x00000100) >> BFD_SI_OFFSET_BITS;
    pHdr->Ttl = u4Hdr & 0x000000ff;

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxGetAchHdr                                             *
 *                                                                           *
 * Description  : Parse the ACH Header from the given buffer                 *
 *                                                                           *
 * Input        : pBuf   - Pointer to the buffer                             *
 *                                                                           *
 * Output       : pHdr   - pointer to tMplsAchHdr to parse the ACH header    *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdRxGetAchHdr (UINT1 *pBuf, tMplsAchHdr * pHdr)
{
    UINT4               u4Hdr = 0;

    BFD_GET_4BYTE (u4Hdr, pBuf);
    pHdr->u4AchType = (u4Hdr & 0xf0000000) >> BFD_MPLS_ACH_TYPE_OFFSET_BITS;
    pHdr->u1Version = (UINT1) ((u4Hdr & 0x0f000000) >>
                               BFD_MPLS_ACH_VERSION_OFFSET_BITS);
    pHdr->u1Rsvd = (UINT1) ((u4Hdr & 0x00ff0000) >>
                            BFD_MPLS_ACH_RSVD_OFFSET_BITS);
    pHdr->u2ChannelType = (UINT2) (u4Hdr & 0x0000ffff);
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxValidateRxPkt                                         *
 *                                                                           *
 * Description  : Validates the received BFD packet and triggers core as     *
 *                required                                                   *
 *                                                                           *
 * Input        : pBfdSessCtrl - Pointer to the BFD session information      *
 *                pBfdPktInfo  - Pointer to the received packet information  *
 *                               structure                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxValidateRxPkt (tBfdFsMIStdBfdSessEntry * pBfdSessCtrl,
                    tBfdPktInfo * pBfdPktInfo)
{
    UINT4               u4ContextId = 0;
    tBfdParseRxPkt     *pBfdCtrlPacket = NULL;

    u4ContextId = pBfdSessCtrl->MibObject.u4FsMIStdBfdContextId;
    pBfdCtrlPacket = &pBfdPktInfo->BfdCtrlPacket;

    /* First check the Authebtication in BFD control packet */
    /* If the Length field is less than the minimum correct value (24 if
     * the A bit is clear, or 26 if the A bit is set), the packet MUST be
     * discarded */
    if (pBfdCtrlPacket->bBfdAuthBit)
    {
        /* Check the length */
        if (pBfdCtrlPacket->u1BfdPktLen == BFD_CTRL_PKT_LEN)
        {
            /* Invalid packet length */
            BFD_LOG (u4ContextId, BFD_TRC_INVALID_PACKET_LENGHT);
            return OSIX_FAILURE;
        }

        /* Check the auth type. Support only Simple Password mechanism */
        if (pBfdCtrlPacket->u1BfdAuthType != BFD_AUTH_SIMPLE_PASSWORD)
        {
            /* Auth Type is not Simple Password */
            BFD_LOG (u4ContextId, BFD_TRC_AUTH_NOT_SIMPLE_PASSWORD);
            return OSIX_FAILURE;
        }

        /* Authenticate */
        if ((pBfdSessCtrl->MibObject.
             i4FsMIStdBfdSessAuthenticationKeyLen !=
             (pBfdCtrlPacket->u1BfdAuthLen - BFD_AUTH_TLID_LEN)) ||
            (MEMCMP (pBfdCtrlPacket->au1BfdPasswd,
                     pBfdSessCtrl->MibObject.au1FsMIStdBfdSessAuthenticationKey,
                     pBfdSessCtrl->MibObject.
                     i4FsMIStdBfdSessAuthenticationKeyLen) != 0) ||
            (pBfdSessCtrl->MibObject.
             i4FsMIStdBfdSessAuthenticationKeyID !=
             pBfdCtrlPacket->u1BfdAuthKeyId))
        {
            /* Authentication Failed */
            BFD_LOG (u4ContextId, BFD_TRC_AUTH_FAILED);
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Check the length */
        if (pBfdCtrlPacket->u1BfdPktLen != BFD_CTRL_PKT_LEN)
        {
            /* Invalid packet length */
            BFD_LOG (u4ContextId, BFD_TRC_INVALID_PACKET_LENGHT);
            return OSIX_FAILURE;
        }
    }

    /* Validate the Remote Discriminator value received on the packet */
    if (pBfdCtrlPacket->u4BfdSessRemoteDiscr != 0)
    {
        if ((pBfdCtrlPacket->u4BfdSessRemoteDiscr !=
             pBfdSessCtrl->MibObject.u4FsMIStdBfdSessDiscriminator) ||
            (pBfdCtrlPacket->u4BfdSessDiscr !=
             pBfdSessCtrl->MibObject.u4FsMIStdBfdSessRemoteDiscr))
        {
            /* Received invalid discriminator */
            BFD_LOG (u4ContextId, BFD_TRC_RECE_INVALID_DISC);
            return OSIX_FAILURE;
        }
    }

    /* Check version */
    if (pBfdPktInfo->BfdCtrlPacket.BfdPktParams.u1BfdRxPktVersionNumber !=
        BFD_PROTOCOL_VERSION)
    {
        /* Error in Version field */
        BFD_LOG (u4ContextId, BFD_TRC_ERROR_VERSION_FIELD);
        return OSIX_FAILURE;
    }

    /* If the Detect Mult field is zero, the packet MUST be discarded */
    if (pBfdPktInfo->BfdCtrlPacket.BfdPktParams.u1BfdRxPktDetectMulti == 0)
    {
        /* Detect Mult value is zero */
        BFD_LOG (u4ContextId, BFD_TRC_DECT_MUL_ZERO);
        return OSIX_FAILURE;
    }

    /* If the Multipoint (M) bit is nonzero, the packet MUST be
     * discarded */
    if (pBfdPktInfo->BfdCtrlPacket.bBfdMBit != 0)
    {
        BFD_LOG (u4ContextId, BFD_TRC_INVALID_MULTIPOINT_BIT);
        /* Invalid Multipoint bit value. This is one of the 
         * Defect entry criteria to down the SEM */
        pBfdPktInfo->bCvValidationFail = OSIX_TRUE;
        BfdCoreTriggerBfdSem (pBfdSessCtrl, BFD_SEM_REQ_RX_PKT, pBfdPktInfo);
        return OSIX_FAILURE;
    }

    /* If the My Discriminator field is zero, the packet MUST be
     * discarded */
    if (pBfdPktInfo->BfdCtrlPacket.u4BfdSessDiscr == 0)
    {
        /* My Discriminator field is zero */
        BFD_LOG (u4ContextId, BFD_TRC_MY_DISC_ZERO);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxGetSourceMep                                          *
 *                                                                           *
 * Description  : Parse the Source MPE TLV from the given buffer and get     *
 *                the unique MEP-ID                                          *
 *                                                                           *
 * Input        : pBuf   - Pointer to the buffer                             *
 *                                                                           *
 * Output       : pBfdPktInfo - Ponter to the tBfdPktInfo structure which    *
 *                              contains the received packet information     *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdRxGetSourceMep (UINT1 **ppPktBuf, tBfdPktInfo * pBfdPktInfo)
{
    UINT2               u2AchTlvLen = 0;
    UINT2               u2AchTlvRsvd = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    tMplsIpLspMepTlv   *pTnlMepTlv = NULL;
    tMplsIpPwMepTlv    *pPwMepTlv = NULL;
    tMplsIccMepTlv     *pIccMepTlv = NULL;
    UINT1              *pBuf = NULL;

    pBuf = *ppPktBuf;
    pTnlMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.
        unMepTlv.IpLspMepTlv;
    pPwMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.unMepTlv.IpPwMepTlv;
    pIccMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.unMepTlv.IccMepTlv;

    /* Get Ach TLV Header and check the length field */
    BFD_GET_2BYTE (u2AchTlvLen, pBuf);

    /* Remove reserved field from the buffer */
    BFD_GET_2BYTE (u2AchTlvRsvd, pBuf);

    /* Extract src mep tlv type and length */
    BFD_GET_2BYTE (u2TlvType, pBuf);
    BFD_GET_2BYTE (u2TlvLen, pBuf);

    pBfdPktInfo->MplsAchTlv.u2TlvType = u2TlvType;
    if (u2TlvType == MPLS_ACH_IP_LSP_MEP_TLV)
    {
        /* Get IP based tunnel MEP TLV */
        BFD_GET_4BYTE (pTnlMepTlv->GlobalNodeId.u4GlobalId, pBuf);
        BFD_GET_4BYTE (pTnlMepTlv->GlobalNodeId.u4NodeId, pBuf);
        BFD_GET_2BYTE (pTnlMepTlv->u2TnlNum, pBuf);
        BFD_GET_2BYTE (pTnlMepTlv->u2LspNum, pBuf);
    }
    else if (u2TlvType == MPLS_ACH_IP_PW_MEP_TLV)
    {
        /* Get IP based PW MEP TLV */
        /* Extract AGI Type */
        BFD_GET_1BYTE (pPwMepTlv->u1AgiType, pBuf);
        /* Extract AGI Len */
        BFD_GET_1BYTE (pPwMepTlv->u1AgiLen, pBuf);
        /* Extract AGI Value */
        BFD_GET_NBYTE (pPwMepTlv->au1Agi, pBuf, (UINT4) pPwMepTlv->u1AgiLen);
        /* Extract Source Global ID */
        BFD_GET_4BYTE (pPwMepTlv->u4GlobalId, pBuf);
        /* Extract Source Node ID */
        BFD_GET_4BYTE (pPwMepTlv->u4NodeId, pBuf);
        /* Extract AC ID */
        BFD_GET_4BYTE (pPwMepTlv->u4AcId, pBuf);
    }
    else if (u2TlvType == MPLS_ACH_ICC_MEP_TLV)
    {
        /* Get ICC based MEP TLV */
        /* Extract ICC */
        BFD_GET_NBYTE (pIccMepTlv->au1Icc, pBuf, MPLS_ICC_LENGTH);
        /* Extract UMC */
        BFD_GET_NBYTE (pIccMepTlv->au1Umc, pBuf, MPLS_UMC_LENGTH);
        /* Extract MEP Index */
        BFD_GET_2BYTE (pIccMepTlv->u2MepIndex, pBuf);
        /* Remove padded 1 byte */
        pBuf += 1;
    }
    *ppPktBuf = pBuf;
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxProcessCvPacket                                       *
 *                                                                           *
 * Description  : Validates the source MEP received in BFD packet            *
 *                                                                           *
 * Input        : pBfdPktInfo - Pointer to tBfdPktInfo which contains        *
 *                              received BFD packet information              *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxProcessCvPacket (UINT4 u4ContextId, tBfdPktInfo * pBfdPktInfo,
                      tBfdMplsPathInfo * pMplsPathInfo)
{
    UINT4               u4RcvdPwGblId = 0;
    UINT4               u4RcvdPwNodeId = 0;
    tMplsMegApiInfo    *pMegInfo = NULL;
    tMplsGlobalNodeId  *pSrcGblId = NULL;
    tMplsGlobalNodeId  *pRcvdTnlGblId = NULL;
    tMplsTnlLspId      *pSrcTnlMep = NULL;
    tPwPathId          *pSrcPwMep = NULL;
    tMplsIpPwMepTlv    *pPwMepTlv = NULL;
    tMplsIccMepTlv     *pIccMepTlv = NULL;
    tMplsIpLspMepTlv   *pTnlMepTlv = NULL;
    UINT1              *pu1MegInfo = NULL;
    UINT1               au1MegInfo[MPLS_MEG_ID_LENGTH] = { 0 };

    pu1MegInfo = au1MegInfo;

    pMegInfo = &pMplsPathInfo->MplsMegApiInfo;
    pSrcTnlMep = &pMplsPathInfo->MplsTeTnlApiInfo.TnlLspId;
    pSrcPwMep = &pMplsPathInfo->MplsPwApiInfo.MplsPwPathId;
    pRcvdTnlGblId = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.
        unMepTlv.IpLspMepTlv.GlobalNodeId;
    u4RcvdPwGblId = pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.
        unMepTlv.IpPwMepTlv.u4GlobalId;
    u4RcvdPwNodeId = pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.
        unMepTlv.IpPwMepTlv.u4NodeId;
    pPwMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.unMepTlv.IpPwMepTlv;
    pIccMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.unMepTlv.IccMepTlv;
    pTnlMepTlv = &pBfdPktInfo->MplsAchTlv.unAchValue.MepTlv.
        unMepTlv.IpLspMepTlv;

    if (pBfdPktInfo->MplsAchTlv.u2TlvType == MPLS_ACH_IP_LSP_MEP_TLV)
    {
        if (pMplsPathInfo->MplsTeTnlApiInfo.u1TnlRole == MPLS_TE_EGRESS)
        {
            pSrcGblId = &pMplsPathInfo->
                MplsTeTnlApiInfo.TnlLspId.SrcNodeId.MplsGlobalNodeId;
        }
        else
        {
            pSrcGblId = &pMplsPathInfo->
                MplsTeTnlApiInfo.TnlLspId.DstNodeId.MplsGlobalNodeId;
        }

        if ((pRcvdTnlGblId->u4GlobalId != pSrcGblId->u4GlobalId) ||
            (pRcvdTnlGblId->u4NodeId != pSrcGblId->u4NodeId) ||
            (pTnlMepTlv->u2TnlNum != pSrcTnlMep->u4SrcTnlNum) ||
            (pTnlMepTlv->u2LspNum != pSrcTnlMep->u4LspNum))
        {
            /* IP baded tunnel Source MEP validation against remote MEP
             * validation failed */
            BFD_LOG (u4ContextId, BFD_TRC_REMOTE_MEP_VALID_TUNNEL_FAILED);
            return OSIX_FAILURE;
        }
    }
    else if (pBfdPktInfo->MplsAchTlv.u2TlvType == MPLS_ACH_IP_PW_MEP_TLV)
    {
        if ((u4RcvdPwGblId != pSrcPwMep->DstNodeId.
             MplsGlobalNodeId.u4GlobalId) ||
            (u4RcvdPwNodeId != pSrcPwMep->DstNodeId.
             MplsGlobalNodeId.u4NodeId) ||
            (pPwMepTlv->u4AcId != pSrcPwMep->u4DstAcId) ||
            (MEMCMP (pPwMepTlv->au1Agi, pSrcPwMep->au1Agi,
                     pPwMepTlv->u1AgiLen) != 0))
        {
            /* IP baded PW Source MEP validation against remote MEP 
             * validation failed */
            BFD_LOG (u4ContextId, BFD_TRC_REMOTE_MEP_VALID_PW_FAILED);
            return OSIX_FAILURE;
        }
    }
    else
    {

        /* Extracting the ICC and UMC from the packet */
        BFD_PUT_NBYTE (pu1MegInfo, pIccMepTlv->au1Icc, MPLS_ICC_LENGTH);

        BFD_PUT_NBYTE (pu1MegInfo, pIccMepTlv->au1Umc, MPLS_UMC_LENGTH);

        pu1MegInfo = au1MegInfo;

        /* MEP Type of MPLS_ACH_ICC_MEP_TLV */
        if ((MEMCMP (pu1MegInfo, pMegInfo->au1Icc,
                     STRLEN (pMegInfo->au1Icc)) != 0) ||
            (MEMCMP ((pu1MegInfo + STRLEN (pMegInfo->au1Icc)), pMegInfo->au1Umc,
                     STRLEN (pMegInfo->au1Umc)) != 0) ||
            (pIccMepTlv->u2MepIndex != pMegInfo->u4IccSinkMepIndex))
        {
            /* ICC Source MEP validation against remote MEP 
             * validation failed */
            BFD_LOG (u4ContextId, BFD_TRC_REMOTE_MEP_VALID_ICC_FAILED);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdRxValidateIpUdpHdr                                      *
 *                                                                           *
 * Description  : Validates the IP/UDP headers received in BFD packet        *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                pu1Pkt  - Pointer to the buffer                            *
 *                u4RcvdIp - Indicates the received IP packet is IPv4 or IPv6*
 *                                                                           *
 * Output       : pBfdPktInfo - Pointer to tBfdPktInfo. In this structure,   *
 *                              peer address field will be filled            *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdRxValidateIpUdpHdr (UINT4 u4ContextId, UINT1 **pu1Pkt,
                       UINT4 u4RcvdIp, tBfdPktInfo * pBfdPktInfo)
{
    t_IP_HEADER         IpHdr;
    tBfdUdp             UdpHdr;
    t_IP_HEADER        *pIpHdr = NULL;
    tBfdUdp            *pUdpHdr = NULL;

    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    pIpHdr = &IpHdr;
    MEMSET (&UdpHdr, 0, sizeof (tBfdUdp));
    pUdpHdr = &UdpHdr;

    if ((u4RcvdIp == BFD_PACKET_OVER_MPLS_RTR) ||
        (u4RcvdIp == BFD_PACKET_OVER_IPV4))
    {
        /* Extract IP Header */
        BFD_GET_NBYTE (pIpHdr, *pu1Pkt, IP_HDR_LEN);

        pBfdPktInfo->PeerAddr.u4_addr[0] = OSIX_NTOHL (pIpHdr->u4Src);

        pIpHdr->u4Dest = OSIX_NTOHL (pIpHdr->u4Dest);
        if ((pIpHdr->u4Dest & BFD_127DEST_ADDR_MASK) != IP_LOOPBACK_ADDRESS)
        {
            /* Destination IP is not loopback IP */
            UNUSED_PARAM (u4ContextId);
            BFD_LOG (u4ContextId, BFD_TRC_IP_VALIDATION_FAILED);
            return OSIX_FAILURE;
        }
        if (pIpHdr->u1Ttl != 1)
        {
            /* IP TTL is not set to 1 */
            UNUSED_PARAM (u4ContextId);
            BFD_LOG (u4ContextId, BFD_TRC_IP_TTL_VALIDATION_FAILED);
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* IPv6 Extraction */
    }

    /* Extract UDP Header */
    BFD_GET_NBYTE (pUdpHdr, *pu1Pkt, sizeof (tBfdUdp));

    pUdpHdr->u2DestUdpPort = OSIX_NTOHS (pUdpHdr->u2DestUdpPort);
    pUdpHdr->u2SrcUdpPort = OSIX_NTOHS (pUdpHdr->u2SrcUdpPort);

    if (pUdpHdr->u2DestUdpPort != BFD_UDP_DEST_PORT)
    {
        /* UDP destination port verification failed */
        BFD_LOG (u4ContextId, BFD_TRC_UDP_DEST_PORT_VER_FAILED);
        return OSIX_FAILURE;
    }

    if (!((pUdpHdr->u2SrcUdpPort > BFD_MIN_UDP_SRC_PORT) &&
          (pUdpHdr->u2SrcUdpPort < BFD_MAX_UDP_SRC_PORT)))
    {
        /* UDP sourcr port  verification failed */
        BFD_LOG (u4ContextId, BFD_TRC_UDP_SOUR_PORT_VER_FAILED);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif /* _BFDRX_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdrx.c                        */
/*-----------------------------------------------------------------------*/
