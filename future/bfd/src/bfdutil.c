/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: bfdutil.c,v 1.25 2017/06/08 11:40:29 siva Exp $
 *
 * Description : This file contains the utility 
 *               functions of the BFD module
 *****************************************************************************/
#ifndef _BFDUTIL_C_
#define _BFDUTIL_C_

#include "bfdinc.h"

/**************************************************************************
 * Function   : BfdUtilFsMIStdBfdGlblCfgTable 
 * Description: This utility function does  initial checks while 
 *              setting an object in Global Config Table.
 * Input      : pBfdSetFsMIStdBfdGlobalConfigTableEntry 
 *              pBfdIsSetFsMIStdBfdGlobalConfigTableEntry 
 * Output     :
 * Returns    : SNMP_SUCCESS/ SNMP_FAILURE
 *
 **************************************************************************/
INT4
BfdUtilFsMIStdBfdGlblCfgTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                               pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                               tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *
                               pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTableEntry;
    UINT4               u4ContextId = 0;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* To check whether the System control is set */

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl !=
        OSIX_FALSE)
    {
        return SNMP_SUCCESS;
    }

    BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.u4FsMIStdBfdContextId =
        pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    u4ContextId = pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;
    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBLE_FETCH_FAIL);
        return SNMP_FAILURE;
    }

    if (BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.i4FsMIBfdSystemControl !=
        BFD_START)
    {
        BFD_LOG (u4ContextId, BFD_TRC_MOD_NOT_STARTED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilFsMIStdBfdSessTable
 * Description: This utility function does initial checks while 
 *              configuring session parameters
 * Input      : pBfdSetFsMIStdBfdGlobalConfigTableEntry
 *               pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
 * Output     :
 * Returns    :  SNMP_SUCCESS/SNMP_FAILURE
 *             
 *************************************************************************/
INT4
BfdUtilFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                            pBfdSetFsMIStdBfdSessEntry,
                            tBfdIsSetFsMIStdBfdSessEntry *
                            pBfdIsSetFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTableEntry;
    UINT4               u4ContextId = 0;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.u4FsMIStdBfdContextId =
        pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    u4ContextId = pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBLE_FETCH_FAIL);
        return SNMP_FAILURE;
    }
    if (BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.i4FsMIBfdSystemControl
        != BFD_START)
    {
        BFD_LOG (u4ContextId, BFD_TRC_MOD_NOT_STARTED);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilFsMIStdBfdSessBaseOidTest
 * Description: This util function checks the Base Oid
 * Input      : pBfdSetFsMIStdBfdSessEntry
 * Output     :
 * Returns    : SNMP_SUCCESS/SNMP_FAILURE
 **************************************************************************/
INT4
BfdUtilFsMIStdBfdSessBaseOidTest (tBfdIsSetFsMIStdBfdSessEntry *
                                  pBfdIsSetFsMIStdBfdSessEntry,
                                  tBfdFsMIStdBfdSessEntry *
                                  pBfdSetFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessTable = NULL;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4MapType = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessBaseOidTest");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessBaseOidTest");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdFsMIStdBfdSessTable = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessTable == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessBaseOidTest");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessTable, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId =
        pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessIndex =
        pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessTable) != OSIX_SUCCESS)
    {
        BFD_LOG (pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_TBLE_FETCH_FAIL);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessTable);
        return SNMP_FAILURE;
    }

    u4ContextId = pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType != OSIX_FALSE)
    {
        i4MapType = pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;
    }
    else
    {
        i4MapType = pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessMapType;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessTable);

    switch (i4MapType)
    {
        case BFD_PATH_TYPE_TE_IPV4:
        case BFD_PATH_TYPE_TE_IPV6:
            pBfdExtInParams->unReqParams.InMplsApiInfo.u4SubReqType =
                MPLS_GET_BASE_TNL_OID;
            break;
        case BFD_PATH_TYPE_PW:
            pBfdExtInParams->unReqParams.InMplsApiInfo.u4SubReqType =
                MPLS_GET_BASE_PW_OID;
            break;
        case BFD_PATH_TYPE_MEP:
            pBfdExtInParams->unReqParams.InMplsApiInfo.u4SubReqType =
                MPLS_GET_BASE_MEG_OID;
            break;
        default:
            BFD_LOG (u4ContextId, BFD_TRC_NO_MAP_TYPE);
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return SNMP_FAILURE;
            break;
    }

    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_SERVICE_POINTER_OID;
    pBfdExtInParams->unReqParams.InMplsApiInfo.u4ContextId = u4ContextId;
    pBfdExtInParams->unReqParams.InMplsApiInfo.u4SrcModId = BFD_MODULE;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        BFD_LOG (u4ContextId, BFD_TRC_BASE_OID_FETCH_FAIL);
        return SNMP_FAILURE;
    }

    if (MEMCMP (pBfdExtOutParams->MplsApiOutInfo.
                OutServiceOid.au4ServiceOidList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer,
                (pBfdExtOutParams->MplsApiOutInfo.unOutInfo.
                 MplsServiceOid.u2ServiceOidLen * sizeof (UINT4))) != 0)
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_OID_MATCH);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return SNMP_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilFsMIStdBfdSessPathExist
 * Description: This utility function tests whether path exists
 * Input      : pBfdSetFsMIStdBfdSessEntry
 * Output     :
 * Returns    : SNMP_SUCCESS/SNMP_FAILURE
 **************************************************************************/
INT4
BfdUtilFsMIStdBfdSessPathExist (tBfdFsMIStdBfdSessEntry *
                                pBfdSetFsMIStdBfdSessEntry, INT4 i4MapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessTable = NULL;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Index = 0;
    UINT4               u4MeId = 0;
    UINT4               u4MegId = 0;
    UINT4               u4MpId = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DstAddr = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessPathExist");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessPathExist");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdFsMIStdBfdSessTable = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessTable == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdUtilFsMIStdBfdSessPathExist");
        return OSIX_FAILURE;
    }

    MEMSET (pBfdFsMIStdBfdSessTable, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId =
        pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdSessIndex =
        pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessTable) != OSIX_SUCCESS)
    {
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_TBLE_FETCH_FAIL);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessTable);

        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessTable);

    u4ContextId = pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    switch (i4MapType)
    {
        case BFD_PATH_TYPE_TE_IPV4:
        case BFD_PATH_TYPE_TE_IPV6:
            u4SrcAddr =
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[15];
            u4DstAddr =
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[16];

            if ((NetIpv4IfIsOurAddress (u4SrcAddr) == NETIPV6_FAILURE)
                && (NetIpv4IfIsOurAddress (u4DstAddr) == NETIPV6_FAILURE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_NO_PATH);
                return SNMP_FAILURE;
            }

            u4Index = pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[pBfdSetFsMIStdBfdSessEntry->
                                         MibObject.i4FsMIBfdSessMapPointerLen -
                                         4];

            pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_TUNNEL_INFO;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                u4PathType = MPLS_PATH_TYPE_TUNNEL;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                TnlId.u4SrcTnlNum = u4Index;
            break;
        case BFD_PATH_TYPE_PW:
            /* Get PW index from PW table OID stored in database */

            /* Assume that MapPointer is integer array */
            u4Index = pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[pBfdSetFsMIStdBfdSessEntry->
                                         MibObject.i4FsMIBfdSessMapPointerLen -
                                         1];

            pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_PW_INFO;

            pBfdExtInParams->unReqParams.InMplsApiInfo.u4SubReqType =
                MPLS_GET_PW_INFO_FROM_PW_INDEX;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                u4PathType = MPLS_PATH_TYPE_PW;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                PwId.u4PwIndex = u4Index;

            break;
        case BFD_PATH_TYPE_MEP:
            /* Get Meg index from MEG table OID stored in database */

            u4MegId = pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[pBfdSetFsMIStdBfdSessEntry->MibObject.
                                         i4FsMIBfdSessMapPointerLen -
                                         BFD_MEG_ID_OFFSET];

            /* Get Mep index from MEG table OID stored in database */

            u4MeId = pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[pBfdSetFsMIStdBfdSessEntry->MibObject.
                                         i4FsMIBfdSessMapPointerLen -
                                         BFD_ME_ID_OFFSET];

            /* Get Mp index from MEG table OID stored in database */

            u4MpId = pBfdSetFsMIStdBfdSessEntry->MibObject.
                au4FsMIBfdSessMapPointer[pBfdSetFsMIStdBfdSessEntry->MibObject.
                                         i4FsMIBfdSessMapPointerLen -
                                         BFD_MP_ID_OFFSET];

            pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_OAM_GET_MEG_INFO;

            pBfdExtInParams->unReqParams.InMplsApiInfo.u4SubReqType =
                MPLS_GET_MEG_INFO_FROM_MEG_ID;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                u4PathType = MPLS_PATH_TYPE_MEG_ID;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                MegId.u4MegIndex = u4MegId;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                MegId.u4MeIndex = u4MeId;

            pBfdExtInParams->unReqParams.InMplsApiInfo.InPathId.
                MegId.u4MpIndex = u4MpId;

            break;
        default:
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return SNMP_FAILURE;
    }

    pBfdExtInParams->unReqParams.InMplsApiInfo.u4ContextId = u4ContextId;

    pBfdExtInParams->unReqParams.InMplsApiInfo.u4SrcModId = BFD_MODULE;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_PATH_INFO_FETCH_FAILED);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return SNMP_SUCCESS;
}

/**************************************************************************
 * Function   : BfdFsMIStdBfdSessTableClearStats
 * Description: To clear session statistics
 * Input      : pBfdFsMIStdBfdSessTable
 * Output     :
 * Returns    : OSIX_SUCCESS
 ****************************************************************************/
INT4
BfdFsMIStdBfdSessTableClearStats (tBfdFsMIStdBfdSessEntry * pBfdSessTable)
{
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfCtrlPktDropLastTime = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessUpTime = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfLastSessDownTime = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfSessUpCount = 0;
    pBfdSessTable->MibObject.u4FsMIStdBfdSessPerfDiscTime = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC.lsn = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC.msn = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC.lsn = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC.msn = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktDropHC.lsn = 0;
    pBfdSessTable->MibObject.u8FsMIStdBfdSessPerfCtrlPktDropHC.msn = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessPerfCCPktIn = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessPerfCCPktOut = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessPerfCVPktIn = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessPerfCVPktOut = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessMisDefCount = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessRdiInCount = 0;
    pBfdSessTable->MibObject.u4FsMIBfdSessRdiOutCount = 0;
    pBfdSessTable->MibObject.i4FsMIStdBfdSessPerfLastCommLostDiag = 0;
    pBfdSessTable->MibObject.i4FsMIBfdClearStats = BFD_SNMP_FALSE;
    BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
             BFD_TRC_STAT_CLEAR);
    return OSIX_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilNotifFsMIStdBfdGlblCfgTbl
 * Description: To set the trap corresponding to the notification input
 * Input      : pBfdFsMIStdBfdSessTable
 * Output     :
 * Returns    : OSIX_SUCCESS
 ****************************************************************************/
INT4
BfdUtilNotifFsMIStdBfdGlblCfgTbl (tBfdFsMIStdBfdGlobalConfigTableEntry
                                  * pBfdOldBfdGlobalConfigTable,
                                  tBfdFsMIStdBfdGlobalConfigTableEntry
                                  * pBfdBfdGlobalConfigTable)
{
    INT4                i4Trap = BFD_SESS_UP_DOWN_TRAP;

    if (pBfdBfdGlobalConfigTable->MibObject.
        i4FsMIStdBfdSessNotificationsEnable == BFD_SNMP_TRUE)
    {
        pBfdBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable =
            pBfdOldBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable |
            BFD_SESS_UP_DOWN_TRAP;
    }
    else
    {
        i4Trap = ~i4Trap;
        pBfdBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable =
            pBfdOldBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable & i4Trap;
    }
    return OSIX_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilTrapFsMIStdBfdGlblCfgTbl 
 * Description: To set the notification corresponding to the trap
 * Input      : pBfdFsMIStdBfdSessTable
 * Output     :
 * Returns    : OSIX_SUCCESS
 ****************************************************************************/
INT4
BfdUtilTrapFsMIStdBfdGlblCfgTbl (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                 pBfdFsMIStdBfdGlobalConfigTable)
{
    UINT4               u4Trap = 0;

    u4Trap = (UINT4)
        pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdTrapEnable;

    if ((u4Trap & BFD_SESS_UP_DOWN_TRAP) == BFD_SESS_UP_DOWN_TRAP)
    {
        pBfdFsMIStdBfdGlobalConfigTable->MibObject.
            i4FsMIStdBfdSessNotificationsEnable = BFD_SNMP_TRUE;
    }
    else
    {
        pBfdFsMIStdBfdGlobalConfigTable->MibObject.
            i4FsMIStdBfdSessNotificationsEnable = BFD_SNMP_FALSE;
    }
    return OSIX_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilEncapCheck
 * Description: To check encapsulation for the underlying path.
 * Input      : pBfdFsMIStdBfdSessTable
 * Output     : 
 * Returns    : SNMP_SUCCESS/SNMP_FAILURE
 *****************************************************************************/
INT4
BfdUtilEncapCheck (tBfdFsMIStdBfdSessEntry * pBfdSetFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessTable = NULL;
    tBfdSessPathParams *pBfdSessPathParams = NULL;
    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;
    BOOL1               bGetRevPath = OSIX_FALSE;
    UINT4               u4ContextId = 0;
    INT4                i4MapType = 0;

    /* Allocate memory for MPLS Path info */
    pBfdMplsPathInfo = (tBfdMplsPathInfo *)
        MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
    if (pBfdMplsPathInfo == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdUtilEncapCheck");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

    pBfdFsMIStdBfdSessTable =
        BfdUtilGetBfdSessTable (pBfdSetFsMIStdBfdSessEntry->
                                MibObject.u4FsMIStdBfdContextId,
                                pBfdSetFsMIStdBfdSessEntry->
                                MibObject.u4FsMIStdBfdSessIndex);
    if (pBfdFsMIStdBfdSessTable == NULL)
    {
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);

        return SNMP_FAILURE;
    }
    i4MapType = pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessMapType;

    pBfdSessPathParams = &pBfdFsMIStdBfdSessTable->BfdSessPathParams;
    /* Tunnel should not be vccv encapsulated */
    if (((i4MapType == BFD_PATH_TYPE_TE_IPV4) ||
         (i4MapType == BFD_PATH_TYPE_TE_IPV6)) &&
        ((pBfdFsMIStdBfdSessTable->MibObject.
          i4FsMIBfdSessEncapType == BFD_ENCAP_TYPE_VCCV_NEGOTIATED) ||
         (pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessEncapType ==
          BFD_ENCAP_TYPE_MPLS_ACH)
         || (pBfdFsMIStdBfdSessTable->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_MPLS_IP_ACH)))
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_VCCV_FOR_TNL_LSP);
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return SNMP_FAILURE;

    }
    /*Pseudowire should be vccv negotiated */
    if ((i4MapType == BFD_PATH_TYPE_PW) &&
        ((pBfdFsMIStdBfdSessTable->MibObject.
          i4FsMIBfdSessEncapType != BFD_ENCAP_TYPE_VCCV_NEGOTIATED)))
    {
        BFD_LOG (u4ContextId, BFD_TRC_PW_VCCV);
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return SNMP_FAILURE;
    }

    if (i4MapType == BFD_PATH_TYPE_MEP)
    {
        MEMCPY (pBfdSessPathParams,
                &pBfdFsMIStdBfdSessTable->BfdSessPathParams,
                sizeof (tBfdSessPathParams));
        u4ContextId = pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId;

        /* Get the information of the underlying path */

        if (BfdExtGetMplsPathInfo (u4ContextId,
                                   pBfdSessPathParams, pBfdMplsPathInfo,
                                   bGetRevPath) != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_TRC_RETRIVE_PATH_FAIL);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pBfdMplsPathInfo);
            return SNMP_FAILURE;

        }
        /* Check whether the underlying path is pseudowire */
        if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
            BFD_CHECK_PATH_PW_AVAIL)
        {
            if (pBfdFsMIStdBfdSessTable->MibObject.
                i4FsMIBfdSessEncapType != BFD_ENCAP_TYPE_VCCV_NEGOTIATED)
            {
                BFD_LOG (u4ContextId, BFD_TRC_PW_VCCV);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pBfdMplsPathInfo);
                return SNMP_FAILURE;
            }
        }
        /* Check whether the underlying path is TE tunnel */
        else if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) ==
                 BFD_CHECK_PATH_TE_AVAIL)
        {
            if (pBfdFsMIStdBfdSessTable->MibObject.
                i4FsMIBfdSessEncapType == BFD_ENCAP_TYPE_VCCV_NEGOTIATED)
            {
                BFD_LOG (u4ContextId, BFD_TRC_PW_VCCV);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pBfdMplsPathInfo);
                return SNMP_FAILURE;
            }
        }
    }
    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                        (UINT1 *) pBfdMplsPathInfo);
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdUtilGetBfdGlobalConfigTable                             *
 * Description  : Gets the Config table pointer based on context ID          *
 * Input        : u4ContextId - context identifier                           *
 * Output       : None                                                       *
 * Returns      : pointer to the Config table                                *
 ****************************************************************************/
PUBLIC tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdUtilGetBfdGlobalConfigTable (UINT4 u4ContextId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTable;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTable =
        NULL;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTable, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    BfdFsMIStdBfdGlobalConfigTable.MibObject.u4FsMIStdBfdContextId =
        u4ContextId;

    pBfdFsMIStdBfdGlobalConfigTable = (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        BfdGetFsMIStdBfdGlobalConfigTable (&BfdFsMIStdBfdGlobalConfigTable);
    return pBfdFsMIStdBfdGlobalConfigTable;
}

/*****************************************************************************
 * Function     : BfdUtilGetBfdSessTable                                     *
 * Description  : Gets the BFD session  table pointer based on context ID and*
 *                session index
 * Input        : u4ContextId - context identifier                           *
 *                u4BfdSessIndex - BFD session index
 * Output       : None                                                       *
 * Returns      : pointer to the BFD session entry
 ****************************************************************************/
PUBLIC tBfdFsMIStdBfdSessEntry *
BfdUtilGetBfdSessTable (UINT4 u4ContextId, UINT4 u4BfdSessIndex)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;

    pBfdSessEntryInput = MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdSessEntryInput == NULL)
    {
        return NULL;
    }
    MEMSET (pBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId = u4ContextId;
    pBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex = u4BfdSessIndex;

    pBfdSessEntry = (tBfdFsMIStdBfdSessEntry *)
        BfdGetFsMIStdBfdSessTable (pBfdSessEntryInput);

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdSessEntryInput);
    return pBfdSessEntry;
}

/*****************************************************************************
 * Function     : BfdUtilIsBfdSessTableExist                                 *
 * Description  : Function checks whether a session exist or not for the     *
 *                given session index                                        *
 * Input        : u4ContextId - context identifier                           *
 *                u4BfdSessIndex - BFD session index                         *
 * Output       : None                                                       *
 * Returns      : OSIX_TRUE/OSIX_FALSE                                       *
 ****************************************************************************/
PUBLIC INT4
BfdUtilIsBfdSessTableExist (UINT4 u4ContextId, UINT4 u4BfdSessIndex)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;

    pBfdSessEntry = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdSessEntry == NULL)
    {
        return OSIX_FALSE;
    }
    MEMSET (pBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdSessEntry->MibObject.u4FsMIStdBfdContextId = u4ContextId;
    pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex = u4BfdSessIndex;

    if ((tBfdFsMIStdBfdSessEntry *)
        BfdGetFsMIStdBfdSessTable (pBfdSessEntry) == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdSessEntry);
        return OSIX_FALSE;
    }
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdSessEntry);
    return OSIX_TRUE;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilUpdatePktCount                                      *
 *                                                                           *
 * Description  : Updates the given low capacity and high capacity counters  *
 *                                                                           *
 * Input        : pu4Counter     - Pointer to low capacity counter           *
 *                pHcCounter     - Pointer to high capacity counter          *
 *                                                                           *
 * Output       : pu4Counter     - Incremented low capacity counter          *
 *                pHcCounter     - Incremented high capacity counter         *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdUtilUpdatePktCount (UINT4 *pu4Counter, tSNMP_COUNTER64_TYPE * pHcCounter)
{
    *pu4Counter += 1;
    if (pHcCounter != NULL)
    {
        (pHcCounter)->lsn++;
    }
    if (*pu4Counter == BFD_MAX_COUNTER_SIZE)
    {
        *pu4Counter = 0;
        if (pHcCounter != NULL)
        {
            (pHcCounter)->msn++;
            (pHcCounter)->lsn = 0;
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilUpdateOffPktCount                                   *
 *                                                                           *
 * Description  : Updates the given offload high capacity counters           *
 *                                                                           *
 * Input        : pHcCounter - High capaciy control plane counter            *
 *                pHcOffCounter - High capacity offload counter              *
 *                                                                           *
 * Output       : pHcCounter - High capaciy control plane counter            *
                  pHcOffCounter - High capacity offload counter              *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdUtilUpdateOffPktCount (tSNMP_COUNTER64_TYPE * pHcCounter,
                          FS_UINT8 * pHcOffCounter)
{
    UINT4               u4MaxCount = BFD_MAX_COUNTER_SIZE;
    UINT4               u4TempDiff = 0;

    pHcCounter->msn = pHcCounter->msn + pHcOffCounter->u4Hi;

    u4TempDiff = u4MaxCount - (pHcCounter->lsn);
    if (u4TempDiff >= pHcOffCounter->u4Lo)
    {
        pHcCounter->lsn = pHcCounter->lsn + pHcOffCounter->u4Lo;
    }
    else
    {
        pHcCounter->lsn = 0;
        pHcCounter->lsn = pHcCounter->lsn + (pHcOffCounter->u4Lo - u4TempDiff);
        pHcCounter->msn = pHcCounter->msn + 1;
    }

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilUpdateMemAlocFail                                   *
 *                                                                           *
 * Description  : Updates the Mib counter whenever memory allocation fails   *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdUtilUpdateMemAlocFail (UINT4 u4ContextId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTable = NULL;

    if ((pBfdGlobalConfigTable =
         BfdUtilGetBfdGlobalConfigTable (u4ContextId)) == NULL)
    {
        return;
    }
    pBfdGlobalConfigTable->MibObject.u4FsMIBfdMemAllocFailure++;
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilCheckBfdSessStatus                                  *
 *                                                                           *
 * Description  : This function checks if                                    *
                       1. BFD session in the context                         *
                          is not ACTIVE or is in ADMIN DOWN                  *
 * Input        : pBfdFsMIStdBfdSessTable - Pointer to session entry         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : INT4                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUtilCheckBfdSessStatus (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessTable)
{
    if (NULL == pBfdFsMIStdBfdSessTable)
    {
        return OSIX_FAILURE;
    }
    if ((BFD_SESS_STATE_ADMIN_DOWN == BFD_SESS_STATE (pBfdFsMIStdBfdSessTable))
        || (pBfdFsMIStdBfdSessTable->MibObject.i4FsMIStdBfdSessRowStatus !=
            ACTIVE))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilCheckBfdModuleStatus                                *
 *                                                                           *
 * Description  : This function checks if                                    *
 *                      1. BFD module is not enabled in the context          *
 *                      2. BFD is not started in the context                 *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUtilCheckBfdModuleStatus (UINT4 u4ContextId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTable =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTable =
        BfdUtilGetBfdGlobalConfigTable (u4ContextId);

    if (NULL == pBfdFsMIStdBfdGlobalConfigTable)
    {
        return OSIX_FAILURE;
    }

    if ((pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIBfdSystemControl !=
         BFD_START) ||
        (pBfdFsMIStdBfdGlobalConfigTable->MibObject.i4FsMIStdBfdAdminStatus
         != BFD_ENABLED))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilGetBfdOffStats                                      *
 *                                                                           *
 * Description  : This function gets the stats from offload module
 * Input        : pBfdFsMIStdBfdSessTable - Pointer to session entry
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : INT4                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUtilGetBfdOffStats (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessTable)
{
    if ((BFD_SESS_OFFLD (pBfdFsMIStdBfdSessTable) == OSIX_TRUE) &&
        (pBfdFsMIStdBfdSessTable->BfdHwhandle.u4BfdSessHwHandle != 0))
    {
        if (BfdOffSessInfo
            (pBfdFsMIStdBfdSessTable->MibObject.u4FsMIStdBfdContextId,
             pBfdFsMIStdBfdSessTable,
             NULL, BFD_OFF_GET_SESS_STATS) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdUtilGetIfIndexFromPort                                  *
 *                                                                           *
 * Description  : This function is to get the Interface index from the       *
 *                Ip port number                                             *
 *                                                                           *
 * Input        : u4Port                                                     *
 *                                                                           *
 * Output       : pu4IfIndex                                                 *
 *                                                                           *
 * Returns      : INT4                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
BfdUtilGetIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    if (pu4IfIndex == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_CFA_IFINDEX_FROM_PORT;

    pBfdExtInParams->u4IfIdx = u4Port;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4IfIndex = pBfdExtOutParams->u4Port;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return OSIX_SUCCESS;

}

/**************************************************************************
 * Function   : BfdUtilGetSrcAddrFromIfIndex
 * Description: This function is get the source address to be used for the
 *              given destination address
 * Input      : u4ifIndex, u1AddrType
 * Output     : pu1SrcAddr
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * ************************************************************************/
INT4
BfdUtilGetSrcAddrFromIfIndex (UINT4 u4IfIndex, UINT1 u1AddrType,
                              UINT1 *pu1SrcAddr, UINT1 *pu1DstAddr)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT4               u4Addr = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_NETIP_IF_INFO;

    pBfdExtInParams->u4IfIdx = u4IfIndex;
    pBfdExtInParams->IpAddrType = u1AddrType;
#ifdef IP6_WANTED
    MEMCPY (&pBfdExtInParams->NbrPathInfo.NbrAddr, pu1DstAddr,
            BFD_IPV6_MAX_ADDR_LEN);
#else
    UNUSED_PARAM (pu1DstAddr);
#endif

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    if (u1AddrType == BFD_INET_ADDR_IPV4)
    {
        u4Addr = OSIX_NTOHL (pBfdExtOutParams->NetIpIfInfo.u4Addr);
        MEMCPY (pu1SrcAddr, &u4Addr, BFD_IPV4_MAX_ADDR_LEN);
    }
    else
    {
        MEMCPY (pu1SrcAddr, &pBfdExtOutParams->NetIpv6Addr,
                BFD_IPV6_MAX_ADDR_LEN);
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return OSIX_SUCCESS;
}

/**************************************************************************
 * Function   : BfdUtilValidateIfIndex 
 * Description: This function is to validate the interface index
 * Input      : u4Ifindex
 * Output     : None
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * ************************************************************************/
INT4
BfdUtilValidateIfIndex (UINT4 u4IfIndex)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_CFA_VALIDATE_IF_INDEX;

    pBfdExtInParams->u4IfIdx = u4IfIndex;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  BfdUtilFillIpMapEntry
 * Description :  This function is to fill the IP map entry from the given 
 *                session entry
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  pBfdSessIpMapEntry     - pointer to the filled IP map entry
 * Returns     :  None
 ****************************************************************************/
VOID
BfdUtilFillIpMapEntry (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo,
                       tBfdFsMIStdBfdSessIpMapEntry * pBfdSessIpMapEntry)
{
    if ((pBfdFsMIStdBfdSessInfo == NULL) || (pBfdSessIpMapEntry == NULL))
    {
        return;
    }

    /* Initialize the entry */
    pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId;
    pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex;
    pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessInterface =
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessInterface;

    MEMCPY (pBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessInfo->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessSrcAddrLen);

    pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessSrcAddrLen;
    pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessInfo->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessDstAddrLen);

    pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessDstAddrLen;
    pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessDstAddrType;

}

/****************************************************************************
 * Function    :  BfdUtilCheckIpMapEntryExist
 * Description :  This function is to check whether the IP map entry for the
 *                corresponding session entry is present or not 
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  pBfdSessIpMapEntry - pointer to the IP map entry
 * Returns     :  pBfdSessIpMapEntry on SUCCESS, NULL on FAILURE
 ****************************************************************************/
tBfdFsMIStdBfdSessIpMapEntry *
BfdUtilCheckIpMapEntryExist (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo)
{
    tBfdFsMIStdBfdSessIpMapEntry BfdSessIpMapEntry;
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntryExist = NULL;

    MEMSET (&BfdSessIpMapEntry, 0, sizeof (tBfdFsMIStdBfdSessIpMapEntry));

    if (pBfdFsMIStdBfdSessInfo == NULL)
    {
        return NULL;
    }

    BfdUtilFillIpMapEntry (pBfdFsMIStdBfdSessInfo, &BfdSessIpMapEntry);
    /* Check whether the node is already present */
    pBfdSessIpMapEntryExist =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
                   (tRBElem *) & BfdSessIpMapEntry);

    return pBfdSessIpMapEntryExist;
}

/****************************************************************************
 * Function    :  BfdUtilGetFreeSessIndex 
 * Description :  This function is to get the free session idex
 * Input       :  u4ContextId  - Context Identifier
 * Output      :  pu4SessIndex - Free Session Index
 * Returns     :  None
 ****************************************************************************/
VOID
BfdUtilGetFreeSessIndex (UINT4 u4ContextId, UINT4 *pu4SessIndex)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;
    UINT4               u4SessIndex = 1;

    do
    {
        pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);
        if (pBfdSessEntry == NULL)
        {
            *pu4SessIndex = u4SessIndex;
            return;
        }
        u4SessIndex++;
    }
    while (1);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdUtilGetSrcv6GlobalAddr                                  */
/*                                                                           */
/* Description  : The function returns the Source Ipv6 Address for the given */
/*                IfIndex and Destination Ipv6 Address.                      */
/*                                                                           */
/* Input        : u4IfIndex  - Interface Id                                  */
/*                pDstAddr   - Destination IPv6 address                      */
/*                pSrcAddr   - Source IPv6 address                           */
/*                                                                           */
/* Output       : Source IPv6 Address                                        */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
#ifdef IP6_WANTED
INT4
BfdUtilGetSrcv6GlobalAddr (UINT4 u4IfIndex,
                           tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status;

    MEMSET (&netIp6FirstAddrInfo, 0, sizeof (netIp6FirstAddrInfo));
    MEMSET (&netIp6NextAddrInfo, 0, sizeof (netIp6NextAddrInfo));

    i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);

    if (i4Status != NETIPV6_FAILURE)
    {
        do
        {
            if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                              (INT4) netIp6FirstAddrInfo.u4PrefixLength)
                == TRUE)
            {
                pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return OSIX_SUCCESS;
            }
            else
            {
                pAddr = &netIp6FirstAddrInfo.Ip6Addr;
            }
            i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                             &netIp6NextAddrInfo);
            if (i4Status != NETIPV6_FAILURE)
                MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                        sizeof (tNetIpv6AddrInfo));
        }
        while (i4Status != NETIPV6_FAILURE);

        MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}
#endif
/**************************************************************************
 * Function   : BfdUtilIsDirectlyConnected
 * Description: This function is to check whether the neighbor IP address
 *              is directly connected or not. (single-hop/multi-hop)
 * Input      : u4ConextId, pu1Address, i4AddrType
 * Output     : None
 * Returns    : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/
INT4
BfdUtilIsDirectlyConnected (UINT4 u4ContextId, UINT1 *pu1Address,
                            INT4 i4AddrType)
{
    INT4                i4RetVal = OSIX_FALSE;
    UINT4               u4Addr = 0;

    if (i4AddrType == BFD_INET_ADDR_IPV4)
    {
        MEMCPY (&u4Addr, pu1Address, CLI_BFD_IPV4_ADDR_LEN);
        if (CfaIpIfIsLocalNetInCxt (u4ContextId, OSIX_HTONL (u4Addr)) ==
            CFA_SUCCESS)
        {
            i4RetVal = OSIX_TRUE;
        }
    }
#ifdef IP6_WANTED
    else
    {
        i4RetVal = BfdUtilIpv6IsDirectlyConnected (u4ContextId, pu1Address);
    }
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : BfdUtilIpv6IsDirectlyConnected                            */
/* Description   : This function checks where the given input address is     */
/*               : belongs to any of the directly connected or not.          */
/* Input(s)      : u4ContextId,pu1Address                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : OSIX_TRUE - if input belongs to directly connected network*/
/*               : OSIX_FALSE- otherwise.                                    */
/*****************************************************************************/
#ifdef IP6_WANTED
INT4
BfdUtilIpv6IsDirectlyConnected (UINT4 u4ContextId, UINT1 *pu1Address)
{
    tIp6Addr            DestAddr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    tNetIpv6AddrInfo    NetIpv6NextAddrInfo;
    UINT4               u4PrefixLen;
    UINT4               u4Index;

    MEMSET (&DestAddr, 0, sizeof (tIp6Addr));
    MEMCPY (DestAddr.u1_addr, pu1Address, CLI_BFD_IPV6_ADDR_LEN);

    if (NetIpv6GetFwdTableRouteEntryInCxt (u4ContextId, &DestAddr,
                                           IP6_ADDR_MAX_PREFIX,
                                           &NetIpv6RtInfo) == NETIPV6_FAILURE)
    {
        return OSIX_FALSE;
    }

    /* Interface index through which this route is learnt */
    u4Index = NetIpv6RtInfo.u4Index;

    if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo) == NETIPV6_SUCCESS)
    {

        for (;;)
        {
            u4PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
            /* Matching DestAddr and LocalAddr over the If's Prefixlen */
            if (Ip6AddrMatch (&DestAddr, &NetIpv6AddrInfo.Ip6Addr,
                              (INT4) u4PrefixLen) == TRUE)
            {
                return OSIX_TRUE;
            }

            if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
                                      &NetIpv6NextAddrInfo) == NETIPV6_FAILURE)
            {
                break;
            }
            MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
                    sizeof (tNetIpv6AddrInfo));
        }
    }
    return OSIX_FALSE;
}
#endif

/*****************************************************************************/
/* Function Name : BfdUtilIpHandlePathStatusChange                           */
/* Description   : This function calls the IPv4/IPv6 PathStatusChange        */
/*               : function based on the AddrType of the NbrIpPath           */
/* Input(s)      : u4ContextId,pu1Address                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : OSIX_TRUE - if input belongs to directly connected network*/
/*               : OSIX_FALSE- otherwise.                                    */
/*****************************************************************************/
INT1
BfdUtilIpHandlePathStatusChange (UINT4 u4ContextId,
                                 tBfdClientNbrIpPathInfo * pIpNbrInfo)
{
    if (pIpNbrInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pIpNbrInfo->u1AddrType == BFD_INET_ADDR_IPV4)
    {
        IpHandlePathStatusChange (u4ContextId,
                                  (tClientNbrIpPathInfo *) pIpNbrInfo);
    }
#ifdef IP6_WANTED
    else if (pIpNbrInfo->u1AddrType == BFD_INET_ADDR_IPV6)
    {
        Ip6HandlePathStatusChange (u4ContextId,
                                   (tClientNbrIp6PathInfo *) pIpNbrInfo);
    }
#endif

    return OSIX_SUCCESS;
}

#endif /* _BFDUTIL_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdutil.c                     */
/*-----------------------------------------------------------------------*/
