/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdcli.c,v 1.75 2017/06/08 11:40:29 siva Exp $
*
* Description: This file contains the Bfd CLI related routines 
*********************************************************************/
#ifndef __BFDCLI_C__
#define __BFDCLI_C__

#include "bfdinc.h"
#include "bfdclig.h"

/*****************************************************************************
 *                                                                           *
 * Function     : cli_process_bfd_show_cmd                                   *
 *                                                                           *
 * Description  : This function is exported to CLI module to handle the      *
 *                BFD cli show commands to take the corresponding action     *
 *                                                                           *
 * Input        : Variable arguments                                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
cli_process_bfd_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_BFD_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst;
    INT4                i4SessIndex = -1;
    UINT4               u4SessIndex = 0;
    UINT4               u4Value = 0;
    UINT4               u4ErrCode = 0;
#ifdef BFD_HA_TEST_WANTED
    tBfdFsMIStdBfdSessEntry BfdTempFsMIStdBfdSessEntry;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
#endif
    CliRegisterLock (CliHandle, BfdMainTaskLock, BfdMainTaskUnLock);
    BFD_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    UNUSED_PARAM (u4IfIndex);
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_BFD_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_BFD_SHOW_GLOB_CONF:
            i4RetStatus =
                BfdCliShowGlobalInfo (CliHandle, (*(UINT4 *) args[0]));
            break;

        case CLI_BFD_SHOW_SESS_INFO:
            if (args[1] != NULL)
            {
                i4SessIndex = (*(INT4 *) args[1]);
                if (i4SessIndex == 0)
                {
                    CLI_SET_ERR (CLI_BFD_INVALID_SESS_INDEX);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u4SessIndex = (UINT4) i4SessIndex;
            }

            i4RetStatus =
                BfdCliShowSessInfo (CliHandle, (UINT1) (*(UINT4 *) (args[0])),
                                    u4SessIndex, (*(UINT4 *) args[2]));
            break;

        case CLI_BFD_SHOW_SESS_STATS:

            if (args[2] != NULL)
            {
                u4SessIndex = (*(UINT4 *) args[2]);
            }
            i4RetStatus =
                BfdCliShowStatisticsInfo (CliHandle,
                                          (*(UINT4 *) args[0]),
                                          (UINT1) (*(UINT4 *) (args[1])),
                                          u4SessIndex, (*(UINT4 *) args[3]));
            break;

        case CLI_BFD_SHOW_BFD_SESS_DISC_MAPPING:
            if (args[1] != NULL)
            {
                u4Value = (*(UINT4 *) args[1]);
            }
            i4RetStatus =
                BfdCliShowSessDiscMapping (CliHandle, (*(UINT4 *) args[0]),
                                           u4Value, (*(UINT4 *) args[2]));
            break;
#ifdef BFD_HA_TEST_WANTED
        case CLI_BFD_HA_TEST_ABORT_BLK_UPDT:
            gBfdTestGlobals.u4AbortBulkUpdate = OSIX_TRUE;
            break;
        case CLI_BFD_HA_TEST_NO_ABORT_BLK_UPDT:
            gBfdTestGlobals.u4AbortBulkUpdate = OSIX_FALSE;
            break;

        case CLI_BFD_HA_TEST_SESS_SUMMARY:

            MEMSET (&BfdTempFsMIStdBfdSessEntry, 0,
                    sizeof (tBfdFsMIStdBfdSessEntry));
            gBfdTestGlobals.u4OffLoadUpCountHW = 0;
            gBfdTestGlobals.u4OffLoadDownCountHW = 0;
            gBfdTestGlobals.u4OffLoadUpCountSW = 0;
            gBfdTestGlobals.u4OffLoadDownCountSW = 0;
            gBfdTestGlobals.u4NonOffUpCount = 0;
            gBfdTestGlobals.u4NonOffDownCount = 0;

            pBfdFsMIStdBfdSessEntry =
                RBTreeGetFirst (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable);

            while (pBfdFsMIStdBfdSessEntry != NULL)
            {
                if (BFD_SESS_OFFLD (pBfdFsMIStdBfdSessEntry) == BFD_SNMP_TRUE)
                {
                    if (OSIX_SUCCESS == BfdOffSessInfo
                        (pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId, pBfdFsMIStdBfdSessEntry,
                         &BfdTempFsMIStdBfdSessEntry, BFD_OFF_GET_SESS_PARAMS))
                    {
                        if (BfdTempFsMIStdBfdSessEntry.MibObject.
                            i4FsMIStdBfdSessState == BFD_SESS_STATE_UP)
                        {
                            gBfdTestGlobals.u4OffLoadUpCountHW++;
                        }
                        else if (BfdTempFsMIStdBfdSessEntry.MibObject.
                                 i4FsMIStdBfdSessState == BFD_SESS_STATE_DOWN)
                        {
                            gBfdTestGlobals.u4OffLoadDownCountHW++;
                        }
                    }
                    else
                    {
                        /* The offloaded session entry is present in S/W but not in the H/W !!! */
                    }

                    /*  Now Fetch state of the offloaded sessions in S/W */

                    if (BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry)
                        == BFD_SESS_STATE_UP)
                    {
                        gBfdTestGlobals.u4OffLoadUpCountSW++;
                    }
                    else if (BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry)
                             == BFD_SESS_STATE_DOWN)
                    {
                        gBfdTestGlobals.u4OffLoadDownCountSW++;
                    }
                }
                else
                {
                    if (BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry)
                        == BFD_SESS_STATE_UP)
                    {
                        gBfdTestGlobals.u4NonOffUpCount++;
                    }
                    else if (BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry)
                             == BFD_SESS_STATE_DOWN)
                    {
                        gBfdTestGlobals.u4NonOffDownCount++;
                    }
                }
                pBfdFsMIStdBfdSessEntry =
                    RBTreeGetNext (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                                   (tRBElem *) pBfdFsMIStdBfdSessEntry, NULL);
            }
            CliPrintf (CliHandle, "\rBfdTestOffLoadSessUpCountHW =: %d\n"
                       "\rBfdTestOffLoadSessDownCountHW =: %d\n"
                       "\rBfdTestOffLoadSessUpCountSW =: %d\n"
                       "\rBfdTestOffLoadSessDownCountSW =: %d\n"
                       "\rBfdTestNonOffLoadSessUpCount =: %d\n"
                       "\rBfdTestNonOffLoadSessDownCount =: %d\n",
                       gBfdTestGlobals.u4OffLoadUpCountHW,
                       gBfdTestGlobals.u4OffLoadDownCountHW,
                       gBfdTestGlobals.u4OffLoadUpCountSW,
                       gBfdTestGlobals.u4OffLoadDownCountSW,
                       gBfdTestGlobals.u4NonOffUpCount,
                       gBfdTestGlobals.u4NonOffDownCount);
            break;
        case CLI_BFD_HA_TEST_SESS_TMR:
            if (args[0] != NULL)
            {
                u4SessIndex = (*(UINT4 *) args[0]);
            }
            i4RetStatus = BfdTestCliShowSessTmr (CliHandle, u4SessIndex);
            break;
#endif
        case CLI_BFD_SHOW_BFD_NEIGHBORS:
            /* args [0] - context Id
             * args [1] - BFD client
             * args [2] - Address type (ipv4/ipv6)
             * args [3] - Ip address
             * args [4] - Output (detail/summary)
             */

            i4RetStatus =
                BfdCliShowNeighbors (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                     (UINT1) CLI_PTR_TO_U4 (args[1]),
                                     (UINT1) CLI_PTR_TO_U4 (args[2]),
                                     (UINT1 *) args[3],
                                     (UINT1) CLI_PTR_TO_U4 (args[4]));
            break;
        default:
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_BFD_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", BfdCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    BFD_UNLOCK;

    return i4RetStatus;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliShowGlobalInfo                                       *
 *                                                                           *
 * Description  : This function displays the global configuration            *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    if (BfdCliPrintGlobalInfo (CliHandle, u4ContextId) != CLI_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_GBL_CONF_PRINT_UNABLE);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliShowSessInfo                                         *
 *                                                                           *
 * Description  : This function displays the session details                 *
 *                                                                           *
 * Input        : u1Output    - Indicates output is summary or details       *
 *                u4SessIndex - Session Index                                *
 *                u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliShowSessInfo (tCliHandle CliHandle, UINT1 u1Output, UINT4 u4SessIndex,
                    UINT4 u4ContextId)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    /* If session index is not specified ,display the session 
     * details for all the session for the specified context */
    if (u4SessIndex == 0)
    {
        pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

        if (pBfdFsMIStdBfdSessEntry == NULL)
        {
            CLI_SET_ERR (CLI_BFD_SESS_TABLE_NOT_EXISTS);
            BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY);
            return CLI_FAILURE;
        }

        do
        {
            if ((u4ContextId != BFD_INVALID_CONTEXT) &&
                (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
                 u4ContextId))
            {
                continue;
            }
            else
            {
                if (BfdCliPrintSessInfo
                    (CliHandle,
                     pBfdFsMIStdBfdSessEntry, u1Output) != CLI_SUCCESS)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_PRINT_UNABLE);
                    return CLI_FAILURE;
                }
            }
        }
        while ((pBfdFsMIStdBfdSessEntry =
                BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry)) !=
               NULL);

    }
    /* If context and session index are given , display the session 
     * details for the given context and session index */
    else
    {
        pBfdFsMIStdBfdSessEntry =
            BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);

        if (pBfdFsMIStdBfdSessEntry == NULL)
        {
            CLI_SET_ERR (CLI_BFD_SESS_TABLE_NOT_EXISTS);
            return CLI_FAILURE;
        }
        else
        {
            if (BfdCliPrintSessInfo
                (CliHandle, pBfdFsMIStdBfdSessEntry, u1Output) != CLI_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintGlobalInfo                                      *
 *                                                                           *
 * Description  : This function prints the global Info                       *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliPrintGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdUtilGetBfdGlobalConfigTable (u4ContextId);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBLE_FETCH_FAIL);
        return CLI_FAILURE;
    }

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliPrintGlobalInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliPrintGlobalInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->u4ContextId = u4ContextId;
    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    if (OSIX_FAILURE == BfdPortHandleExtInteraction (pBfdExtInParams,
                                                     pBfdExtOutParams))
    {
        BFD_LOG (u4ContextId, BFD_TRC_EXT_CALL_FAIL);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);

        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nContext %s\r\n",
               pBfdExtOutParams->au1ContextName);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdSystemControl == OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\rSystem Control      :   START \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rSystem Control      :   SHUTDOWN \r\n");
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_SUCCESS;
    }

    if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus
        == BFD_ENABLED)
    {
        CliPrintf (CliHandle, "\rAdmin Status        :   Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rAdmin Status        :   Disabled \r\n");
    }
    if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rNotification        :   True \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rNotification        :   False \r\n");
    }

    if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdTrapEnable == BFD_NO_TRAP)
    {
        CliPrintf (CliHandle, "\rTrap                :   Disabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rTrap                :   Enabled \r\n");
    }
    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         i4FsMIBfdTrapEnable & BFD_SESS_UP_DOWN_TRAP) != 0)
    {
        CliPrintf (CliHandle, "\r    Sess Up Down Trap        : enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r    Sess Up Down Trap        : disabled\r\n");
    }

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         i4FsMIBfdTrapEnable & BFD_BTSTRAP_FAIL_TRAP) != 0)
    {
        CliPrintf (CliHandle, "\r    Sess Btstrap Fail Trap   : enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r    Sess Btstrap Fail Trap   : disabled\r\n");
    }

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         i4FsMIBfdTrapEnable & BFD_NEG_TX_CHANGE_TRAP) != 0)
    {
        CliPrintf (CliHandle, "\r    Sess Neg Tx Change Trap  : enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r    Sess Neg Tx Change Trap  : disabled\r\n");
    }

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         i4FsMIBfdTrapEnable & BFD_ADMIN_CTRL_ERR_TRAP) != 0)
    {
        CliPrintf (CliHandle, "\r    Sess Admin Ctrl Err Trap : enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r    Sess Admin Ctrl Err Trap : disabled\r\n");
    }

    CliPrintf (CliHandle, "\rDesired tx interval  :   ");

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         u4FsMIBfdGblDesiredMinTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                          CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%ds ",
                   (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                    u4FsMIBfdGblDesiredMinTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                                     CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblDesiredMinTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                           CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%dms",
                   ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                     u4FsMIBfdGblDesiredMinTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                                      CLI_BFD_CONVERT_SECONDS))
                    / CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblDesiredMinTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                           CLI_BFD_CONVERT_SECONDS)) == 0) &&
        (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
           u4FsMIBfdGblDesiredMinTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                            CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\rRequired tx interval :   ");

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         u4FsMIBfdGblReqMinRxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                      CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%ds ",
                   (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                    u4FsMIBfdGblReqMinRxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                                 CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblReqMinRxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                       CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%dms",
                   ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                     u4FsMIBfdGblReqMinRxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                                  CLI_BFD_CONVERT_SECONDS)) /
                    CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblReqMinRxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                       CLI_BFD_CONVERT_SECONDS)) == 0) &&
        (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
           u4FsMIBfdGblReqMinRxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                        CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\rDetection Multiplier :   %d \r\n ",
               pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
               u4FsMIBfdGblDetectMult);

    CliPrintf (CliHandle, "\rSlow tx interval     :   ");

    if ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
         u4FsMIBfdGblSlowTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                    CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%ds ",
                   (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                    u4FsMIBfdGblSlowTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                               CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblSlowTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                     CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%dms",
                   ((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                     u4FsMIBfdGblSlowTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                                CLI_BFD_CONVERT_SECONDS)) /
                    CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
          u4FsMIBfdGblSlowTxIntvl / (CLI_BFD_CONVERT_SECONDS *
                                     CLI_BFD_CONVERT_SECONDS)) == 0) &&
        (((pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
           u4FsMIBfdGblSlowTxIntvl % (CLI_BFD_CONVERT_SECONDS *
                                      CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle,
               "\rSess Oper Mode       :   Asynchronous Without "
               "Echo Function\n");

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintSessInfo                                        *
 *                                                                           *
 * Description  : This function prints the Session Information               *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4SessIndex - Session Index                                *
 *                u1Output    - Indicates output is summary or details       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliPrintSessInfo (tCliHandle CliHandle, tBfdFsMIStdBfdSessEntry
                     * pBfdFsMIStdBfdSessEntry, UINT1 u1Output)
{
    CHR1               *pu1TempStr1 = NULL;

    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT4               u4IpAddr;
    BOOL1               bIsRegisteredClient = OSIX_FALSE;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT1               u1Temp = OSIX_FALSE;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdCliPrintSessInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdCliPrintSessInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->u4ContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    if (OSIX_FAILURE == BfdPortHandleExtInteraction (pBfdExtInParams,
                                                     pBfdExtOutParams))
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nContext %s\r\n",
               pBfdExtOutParams->au1ContextName);

    CliPrintf (CliHandle, "\rSession Index:%u\r\n",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "\rSession Status : enabled, ");
    }
    else
    {
        CliPrintf (CliHandle, "\rSession Status : disabled, ");
    }
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus ==
        CLI_BFD_ADMIN_START)
    {
        CliPrintf (CliHandle, "Session Admin Status : start\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Session Admin Status : Stop\r\n");
    }

    CliPrintf (CliHandle,
               "\rVersion:%u, ",
               pBfdFsMIStdBfdSessEntry->MibObject.
               u4FsMIStdBfdSessVersionNumber);
    CliPrintf (CliHandle, " desired tx interval:");

    if ((pBfdFsMIStdBfdSessEntry->MibObject.
         u4FsMIStdBfdSessDesiredMinTxInterval / (CLI_BFD_CONVERT_SECONDS *
                                                 CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%us ",
                   (pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDesiredMinTxInterval /
                    (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessDesiredMinTxInterval % (CLI_BFD_CONVERT_SECONDS *
                                                  CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%ums",
                   ((pBfdFsMIStdBfdSessEntry->MibObject.
                     u4FsMIStdBfdSessDesiredMinTxInterval %
                     (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)) /
                    CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessDesiredMinTxInterval / (CLI_BFD_CONVERT_SECONDS *
                                                  CLI_BFD_CONVERT_SECONDS)) ==
         0)
        &&
        (((pBfdFsMIStdBfdSessEntry->MibObject.
           u4FsMIStdBfdSessDesiredMinTxInterval % (CLI_BFD_CONVERT_SECONDS *
                                                   CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }

    CliPrintf (CliHandle, ", required rx interval:");

    if ((pBfdFsMIStdBfdSessEntry->MibObject.
         u4FsMIStdBfdSessReqMinRxInterval / (CLI_BFD_CONVERT_SECONDS *
                                             CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%us ",
                   (pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinRxInterval /
                    (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessReqMinRxInterval % (CLI_BFD_CONVERT_SECONDS *
                                              CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%ums",
                   ((pBfdFsMIStdBfdSessEntry->MibObject.
                     u4FsMIStdBfdSessReqMinRxInterval %
                     (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)) /
                    CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessReqMinRxInterval / (CLI_BFD_CONVERT_SECONDS *
                                              CLI_BFD_CONVERT_SECONDS)) == 0) &&
        (((pBfdFsMIStdBfdSessEntry->MibObject.
           u4FsMIStdBfdSessReqMinRxInterval % (CLI_BFD_CONVERT_SECONDS *
                                               CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\rMultiplier:%u, diag:%d, ",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult,
               pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDiag);

    CliPrintf (CliHandle, "My discr:%u, your discr:%u, ",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator,
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr);

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessState ==
        BFD_SESS_STATE_ADMIN_DOWN)
    {
        CliPrintf (CliHandle, "state ADMINDOWN, ");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessState == BFD_SESS_STATE_UP)
    {
        CliPrintf (CliHandle, "state UP, ");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessState == BFD_SESS_STATE_INIT)
    {
        CliPrintf (CliHandle, "state INIT, ");
    }
    else
    {
        CliPrintf (CliHandle, "state DOWN, ");
    }

    CliPrintf (CliHandle, "D/C/M/A:");
    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDemandModeDesiredFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "1/");
    }
    else
    {
        CliPrintf (CliHandle, "0/");

    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessControlPlaneIndepFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "1/");
    }
    else
    {
        CliPrintf (CliHandle, "0/");

    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessMultipointFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "1/");
    }
    else
    {
        CliPrintf (CliHandle, "0/");

    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "1\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "0\r\n");
    }
    /*If output expected is summary , then return */
    if (u1Output == CLI_BFD_SHOW_SUMMARY)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_SUCCESS;
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole ==
        BFD_SESS_ROLE_ACTIVE)
    {
        CliPrintf (CliHandle, "\rRole: Active, ");
    }
    else
    {
        CliPrintf (CliHandle, "\rRole: Passive, ");
    }
    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessMode == BFD_SESS_MODE_CC)
    {
        CliPrintf (CliHandle, "Sess Mode: CC, ");
    }
    else
    {
        CliPrintf (CliHandle, "Sess Mode: CCV, ");
    }

    CliPrintf (CliHandle, "Exp: %u\r\n",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue);

    CliPrintf (CliHandle, "\rOper Mode:Asynchronous Without Echo Function\r\n");

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
        BFD_SESS_TYPE_SINGLE_HOP)
    {
        CliPrintf (CliHandle, "\rSess Type: Single hop\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
             BFD_SESS_TYPE_MULTIHOP_TOTALLY_ARBITRARYPATHS)
    {
        CliPrintf (CliHandle,
                   "\rSess Type: Multihop Totally Arbitrarypath\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
             BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING)
    {
        CliPrintf (CliHandle,
                   "\rSess Type: MultiHop Out of Band Signalling\r\n");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
             BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS)
    {
        CliPrintf (CliHandle, "\rSess Type: MultiHop UniDirectional Links\r\n");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
             BFD_SESS_TYPE_MULTIPOINT_HEAD)
    {
        CliPrintf (CliHandle, "\rSess Type: Multipoint Head\r\n");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
             BFD_SESS_TYPE_MULTIPOINT_TAIL)
    {
        CliPrintf (CliHandle, "\rSess Type:Multipoint Tail\r\n");
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessTmrNegotiate == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rTimer Negotiation:Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rTimer Negotiation:Disabled\r\n");
    }

    CliPrintf (CliHandle, "\rLocal negotiated async tx interval:");

    if ((pBfdFsMIStdBfdSessEntry->MibObject.
         u4FsMIStdBfdSessNegotiatedInterval / (CLI_BFD_CONVERT_SECONDS *
                                               CLI_BFD_CONVERT_SECONDS)) != 0)
    {
        CliPrintf (CliHandle, "%us ",
                   (pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessNegotiatedInterval /
                    (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)));

    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessNegotiatedInterval % (CLI_BFD_CONVERT_SECONDS *
                                                CLI_BFD_CONVERT_SECONDS)) /
         CLI_BFD_CONVERT_SECONDS) != 0)
    {
        CliPrintf (CliHandle, "%ums",
                   ((pBfdFsMIStdBfdSessEntry->MibObject.
                     u4FsMIStdBfdSessNegotiatedInterval %
                     (CLI_BFD_CONVERT_SECONDS * CLI_BFD_CONVERT_SECONDS)) /
                    CLI_BFD_CONVERT_SECONDS));
    }
    if (((pBfdFsMIStdBfdSessEntry->MibObject.
          u4FsMIStdBfdSessNegotiatedInterval / (CLI_BFD_CONVERT_SECONDS *
                                                CLI_BFD_CONVERT_SECONDS)) == 0)
        &&
        (((pBfdFsMIStdBfdSessEntry->MibObject.
           u4FsMIStdBfdSessNegotiatedInterval % (CLI_BFD_CONVERT_SECONDS *
                                                 CLI_BFD_CONVERT_SECONDS)) /
          CLI_BFD_CONVERT_SECONDS) == 0))
    {
        CliPrintf (CliHandle, "0s");
    }

    CliPrintf (CliHandle, ", Negotiated Detect Multiplier:%u\r\n",
               pBfdFsMIStdBfdSessEntry->MibObject.
               u4FsMIStdBfdSessNegotiatedDetectMult);
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
        BFD_SESS_TYPE_SINGLE_HOP)
    {
        CliPrintf (CliHandle, "\rDest UDP port: %u, Src UDP port: %u\r\n",
                   BFD_UDP_DEST_PORT, BFD_UDP_SRC_PORT);
    }
    else
    {
        CliPrintf (CliHandle, "\rDest UDP port: %u, Src UDP port: %u\r\n",
                   BFD_UDP_DEST_PORT_MHOP, BFD_UDP_SRC_PORT);
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag
        == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rRemote Heard Flag: True\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rRemote Heard Flag: False\r\n");
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rAuthentication : enabled, ");
    }
    else
    {
        CliPrintf (CliHandle, "\rAuthentication : disabled, ");
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Authentication Type:");

        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationType == BFD_AUTH_SIMPLE_PASSWORD)
        {
            CliPrintf (CliHandle, "Simple Password\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "None\r\n");
        }

        CliPrintf (CliHandle, "\rAuthentication Key ID:%d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   i4FsMIStdBfdSessAuthenticationKeyID);
    }
    else
    {
        CliPrintf (CliHandle, "Authentication Type:None\r\n");
        CliPrintf (CliHandle, "\rAuthentication Key ID:None\r\n");

    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld == BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rOffload:Enabled, ");
    }
    else
    {
        CliPrintf (CliHandle, "\rOffload:Disabled, ");
    }
    CliPrintf (CliHandle, "Card Number:%u, Slot Number:%u\r\n",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber,
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber);

    /* Path type */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
        BFD_PATH_TYPE_NONTE_IPV4)
    {
        CliPrintf (CliHandle, "\rMap Type:Non TE IPV4, ");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_NONTE_IPV6)
    {
        CliPrintf (CliHandle, "\rMap Type:Non TE IPV6, ");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_TE_IPV4)
    {
        CliPrintf (CliHandle, "\rMap Type:TE IPV4, ");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_TE_IPV6)
    {
        CliPrintf (CliHandle, "\rMap Type:TE IPV6, ");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_PW)
    {
        CliPrintf (CliHandle, "\rMap Type:Pseudowire, ");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_MEP)
    {
        CliPrintf (CliHandle, "\rMap Type:ME, ");
    }

    /* Encapsulation type */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType
        == BFD_ENCAP_TYPE_MPLS_IP)
    {
        CliPrintf (CliHandle, "Encap Type: Mpls Ip\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType
             == BFD_ENCAP_TYPE_MPLS_ACH)
    {
        CliPrintf (CliHandle, "Encap Type: Mpls Ach\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType
             == BFD_ENCAP_TYPE_MPLS_IP_ACH)
    {
        CliPrintf (CliHandle, "Encap Type: Mpls Ip Ach\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType
             == BFD_ENCAP_TYPE_VCCV_NEGOTIATED)
    {
        CliPrintf (CliHandle, "Encap Type: Vccv Negotiated\r\n");
    }

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen != 0)
    {
        if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
            BFD_PATH_TYPE_TE_IPV4)
        {
            CliPrintf (CliHandle, "\rTunnel Id:%d Tunnel Instance:%d\r\n",
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                       u4TunnelId,
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                       u4TunnelInst);

            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr1,
                                       pBfdFsMIStdBfdSessEntry->
                                       BfdSessPathParams.SessTeParams.SrcIpAddr.
                                       u4_addr[0]);

            CliPrintf (CliHandle, "Src Ip Addr:%s, ", pu1TempStr1);

            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr1,
                                       pBfdFsMIStdBfdSessEntry->
                                       BfdSessPathParams.SessTeParams.DstIpAddr.
                                       u4_addr[0]);

            CliPrintf (CliHandle, "Dest Ip Addr:%s\r\n", pu1TempStr1);

        }
        else if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
                 BFD_PATH_TYPE_PW)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr1,
                                       pBfdFsMIStdBfdSessEntry->
                                       BfdSessPathParams.SessPwParams.PeerAddr.
                                       u4_addr[0]);
            CliPrintf (CliHandle, "\rPeer Address:%s,Vc-Id:%d\r\n",
                       pu1TempStr1,
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessPwParams.
                       u4VcId);
        }
        else if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
                 BFD_PATH_TYPE_MEP)
        {
            CliPrintf (CliHandle, "\rMeg Id:%d, Me Id:%d, Mp Id:%d\r\n",
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.
                       u4MegId,
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.
                       u4MeId,
                       pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.
                       u4MpId);

        }
        CliPrintf (CliHandle, "Registered Protocols: MPLS\r\n");
    }
    else if ((pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
              BFD_PATH_TYPE_IPV4) ||
             ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType ==
               BFD_INET_ADDR_IPV4)
              && (pBfdFsMIStdBfdSessEntry->MibObject.
                  i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV4)))

    {

        MEMCPY (&u4IpAddr, &pBfdFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessSrcAddr, MAX_IPV4_ADDR_LEN);
        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr1, OSIX_NTOHL (u4IpAddr));
        CliPrintf (CliHandle, "Src Ip Addr:%s, ", pu1TempStr1);

        MEMCPY (&u4IpAddr, &pBfdFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessDstAddr, MAX_IPV4_ADDR_LEN);
        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr1, OSIX_NTOHL (u4IpAddr));
        CliPrintf (CliHandle, "Dest Ip Addr:%s\r\n", pu1TempStr1);

        MEMCPY (au1BfdClients, &pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_OSPF,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            u1Temp = OSIX_TRUE;
            CliPrintf (CliHandle, "Registered Protocols: OSPF");
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_LDP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", LDP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: LDP");
            }
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_BGP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", BGP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: BGP");
            }
        }

        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_ISIS,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", ISIS\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Registered Protocols: ISIS\r\n");
            }
        }
        else
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }

    }

    else if ((pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
              BFD_PATH_TYPE_IPV6) ||
             ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType ==
               BFD_INET_ADDR_IPV6)
              && (pBfdFsMIStdBfdSessEntry->MibObject.
                  i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV6)))
    {

        CliPrintf (CliHandle, "Src IPv6 Addr:%s, ",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                 pBfdFsMIStdBfdSessEntry->MibObject.
                                 au1FsMIStdBfdSessSrcAddr));

        CliPrintf (CliHandle, "Dest IPv6 Addr:%s\r\n",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                 pBfdFsMIStdBfdSessEntry->MibObject.
                                 au1FsMIStdBfdSessDstAddr));

        MEMCPY (au1BfdClients, &pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_OSPF3,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            u1Temp = OSIX_TRUE;
            CliPrintf (CliHandle, "Registered Protocols: OSPF3");
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_LDP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", LDP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: LDP");
            }
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_BGP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", BGP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: BGP");
            }
        }

        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_ISIS,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", ISIS\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Registered Protocols: ISIS\r\n");
            }
        }
        else
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }

    }

    /* Admin Ctrl Required */

    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq ==
        BFD_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\rAdmin Ctrl Required:Enabled,");
    }
    else
    {
        CliPrintf (CliHandle, "\rAdmin Ctrl Required:Disabled,");
    }
    /* Admin Ctrl Err Reason */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason ==
        BFD_SESS_ERROR_NONE)
    {
        CliPrintf (CliHandle, "Admin Ctrl Err Reason:None\r\n");
    }

    else if (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIBfdSessAdminCtrlErrReason == BFD_SESS_ERROR_OFFLD_FAILED)
    {
        CliPrintf (CliHandle, "Admin Ctrl Err Reason:Sess Offload Failure\r\n");
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIBfdSessAdminCtrlErrReason == BFD_SESS_ERROR_PERIOD_MISCONFIG)
    {
        CliPrintf (CliHandle,
                   "Admin Ctrl Err Reason:Period Misconfig Defect\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliShowStatisticsInfo                                      *
 *                                                                           *
 * Description  : This function displays the BFD session statics             *
 *                                                                           *
 * Input        : u4Display   - Indicates output is global or per session    *
 *                u1Type      - Indicates output type                        *
 *                u4SessIndex - Session Index                                *
 *                u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliShowStatisticsInfo (tCliHandle CliHandle, UINT4 u4Display,
                          UINT4 u4Type, UINT4 u4SessIndex, UINT4 u4ContextId)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    if (u4Display == CLI_BFD_GLOBAL_STATS)
    {
        if (BfdCliPrintGlobalStatsInfo (CliHandle, u4ContextId) == CLI_FAILURE)
        {
            BFD_LOG (u4ContextId, BFD_TRC_GBL_TBL_PRINT_UNABLE);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* If session index is not given display statistics for all
         * sessions  */

        if (u4SessIndex == 0)
        {
            pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();
            if (pBfdFsMIStdBfdSessEntry == NULL)
            {
                CLI_SET_ERR (CLI_BFD_SESS_TABLE_NOT_EXISTS);
                BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY);
                return CLI_FAILURE;
            }

            do
            {
                if (pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdContextId == u4ContextId)
                {
                    if (BfdUtilGetBfdOffStats (pBfdFsMIStdBfdSessEntry) !=
                        OSIX_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }

                    if (BfdCliPrintSessStatsInfo
                        (CliHandle,
                         pBfdFsMIStdBfdSessEntry, u4Type) != CLI_SUCCESS)

                    {
                        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.
                                 u4FsMIStdBfdContextId,
                                 BFD_TRC_SESS_STAT_PRINT_UNABLE);
                        return CLI_FAILURE;
                    }
                }
                pBfdFsMIStdBfdSessEntry =
                    BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);
            }
            while (pBfdFsMIStdBfdSessEntry != NULL);

        }
        else
        {
            pBfdFsMIStdBfdSessEntry =
                BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);
            if (pBfdFsMIStdBfdSessEntry == NULL)
            {
                CLI_SET_ERR (CLI_BFD_SESS_NOT_EXISTS);
                BFD_LOG (u4ContextId, BFD_TRC_NO_SESSION);
                return CLI_FAILURE;

            }
            if (BfdUtilGetBfdOffStats (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (BfdCliPrintSessStatsInfo
                (CliHandle, pBfdFsMIStdBfdSessEntry, u4Type) != CLI_SUCCESS)
            {
                CLI_SET_ERR (CLI_BFD_SESS_NOT_EXISTS);
                BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId, BFD_TRC_SESS_STAT_PRINT_UNABLE);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintGlobalStatsInfo                                 *
 *                                                                           *
 * Description  : This function prints the global statistics                 *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliPrintGlobalStatsInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTable;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTable, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliPrintGlobalStatsInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliPrintGlobalStatsInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->u4ContextId = u4ContextId;

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    if (OSIX_FAILURE ==
        BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams))
    {
        BFD_LOG (u4ContextId, BFD_TRC_EXT_CALL_FAIL);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nContext %s\r\n",
               pBfdExtOutParams->au1ContextName);

    BfdFsMIStdBfdGlobalConfigTable.MibObject.u4FsMIStdBfdContextId =
        u4ContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTable) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r    MemAllocFail InputQueOverflow \r\n");
    CliPrintf (CliHandle, "\r    ------------ ---------------- \r\n");

    CliPrintf (CliHandle, "\r    %12d %15d \r\n",
               BfdFsMIStdBfdGlobalConfigTable.MibObject.
               u4FsMIBfdMemAllocFailure,
               BfdFsMIStdBfdGlobalConfigTable.MibObject.
               u4FsMIBfdInputQOverFlows);

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintSessStatsInfo                                   *
 *                                                                           *
 * Description  : This function displays the statics of the specified session*
 *                                                                           *
 * Input        : pBfdFsMIStdBfdSessEntry - Pointer to session entry         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliPrintSessStatsInfo (tCliHandle CliHandle, tBfdFsMIStdBfdSessEntry *
                          pBfdFsMIStdBfdSessEntry, UINT4 u4Type)
{
    INT1                ai1DropTime[BFD_MAX_CLI_TIME_STRING];
    INT1                ai1UpTime[BFD_MAX_CLI_TIME_STRING];
    INT1                ai1DownTime[BFD_MAX_CLI_TIME_STRING];
    INT1                ai1DiscTime[BFD_MAX_CLI_TIME_STRING];
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdCliPrintSessStatsInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdCliPrintSessStatsInfo");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->u4ContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    if (OSIX_FAILURE ==
        BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams))
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nContext %s\r\n",
               pBfdExtOutParams->au1ContextName);

    CliPrintf (CliHandle, "\rSession Index:%d\r\n",
               pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);

    /* Displays the Defect statistics */

    if ((u4Type == CLI_BFD_SHOW_DEFECT_STATS) ||
        (u4Type == CLI_BFD_SHOW_ALL_SESS_STATS))
    {
        CliPrintf (CliHandle, "\r    Defect Statistics:\r\n");
        CliPrintf (CliHandle, "\r\tMisConDef Count       :%d\r\n ",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessMisDefCount);

        CliPrintf (CliHandle, "\r\tLoc Def Count         :%d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessLocDefCount);

        CliPrintf (CliHandle, "\r\tRdi In Count          :%d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiInCount);

        CliPrintf (CliHandle, "\r\tRdi Out Count         :%d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiOutCount);

    }

    /* Displays the packet statistics */
    if ((u4Type == CLI_BFD_SHOW_PKT_STATS) ||
        (u4Type == CLI_BFD_SHOW_ALL_SESS_STATS))
    {

        CliPrintf (CliHandle, "\r\n    Packet Statistics: \r\n");

        CliPrintf (CliHandle, "\r\tRx Count              :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessPerfCtrlPktIn);

        CliPrintf (CliHandle, "\r\tTx Count              :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessPerfCtrlPktOut);

        CliPrintf (CliHandle, "\r\tDrop Count            :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessPerfCtrlPktDrop);

        CliPrintf (CliHandle, "\r\tHC Counters\r\n ");

        CliPrintf (CliHandle, "\r\tRx Count              :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u8FsMIStdBfdSessPerfCtrlPktInHC);

        CliPrintf (CliHandle, "\r\tTx Count              :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u8FsMIStdBfdSessPerfCtrlPktOutHC);

        CliPrintf (CliHandle, "\r\tDrop Count            :%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u8FsMIStdBfdSessPerfCtrlPktDropHC);

        CliPrintf (CliHandle, "\r\tCC Control Packets\n");

        CliPrintf (CliHandle, "\r\tRx Count:%d, ",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktIn);

        CliPrintf (CliHandle, "Tx Count:%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIBfdSessPerfCCPktOut);

        CliPrintf (CliHandle, "\r\tCV Control Packets\n");

        CliPrintf (CliHandle, "\r\tRx Count:%d, ",
                   pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktIn);

        CliPrintf (CliHandle, "Tx Count:%d \r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIBfdSessPerfCVPktOut);

    }

    /* Displays the generic statistics */
    if ((u4Type == CLI_BFD_SHOW_GENERIC_STATS) ||
        (u4Type == CLI_BFD_SHOW_ALL_SESS_STATS))
    {
        CliPrintf (CliHandle, "\r\n    Generic Statistics:\r\n");
        BfdCliConvertTimetoString (pBfdFsMIStdBfdSessEntry->MibObject.
                                   u4FsMIStdBfdSessPerfCtrlPktDropLastTime,
                                   ai1DropTime);
        CliPrintf (CliHandle, "\r\tLast Packet Drop Time : %s\r\n ",
                   ai1DropTime);

        BfdCliConvertTimetoString (pBfdFsMIStdBfdSessEntry->
                                   MibObject.u4FsMIStdBfdSessUpTime, ai1UpTime);
        CliPrintf (CliHandle, "\r\tSess Up Time          : %s\r\n ", ai1UpTime);

        BfdCliConvertTimetoString (pBfdFsMIStdBfdSessEntry->MibObject.
                                   u4FsMIStdBfdSessPerfLastSessDownTime,
                                   ai1DownTime);
        CliPrintf (CliHandle, "\r\tLast Sess Down Time   : %s\r\n",
                   ai1DownTime);

        CliPrintf (CliHandle, "\r\tSess Up Count         : %d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessPerfSessUpCount);

        BfdCliConvertTimetoString (pBfdFsMIStdBfdSessEntry->MibObject.
                                   u4FsMIStdBfdSessPerfDiscTime, ai1DiscTime);
        CliPrintf (CliHandle, "\r\tDisc Time             : %s\r\n",
                   ai1DiscTime);

        CliPrintf (CliHandle, "\r\tLast Common Lost Diag : %d\r\n",
                   pBfdFsMIStdBfdSessEntry->MibObject.
                   i4FsMIStdBfdSessPerfLastCommLostDiag);

    }
    CliPrintf (CliHandle, "\r\n");
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliShowSessDiscMapping                                  *
 *                                                                           *
 * Description  : This function prints the Session Discriminator mapping     *
 *                                                                           *
 * Input        : u4Value     - Indicates output type                        *
 *                u4Index     - Session Index                                *
 *                u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliShowSessDiscMapping (tCliHandle CliHandle, UINT4 u4Index,
                           UINT4 u4Value, UINT4 u4ContextId)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pBfdFsMIStdBfdSessDiscMapEntry = BfdGetFirstFsMIStdBfdSessDiscMapTable ();

    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (u4Index == CLI_BFD_SHOW_ALL)
    {
        CliPrintf (CliHandle,
                   "\r\n   ContextId    Sess Disc   Sess Index \r\n ");
        do
        {
            if (pBfdFsMIStdBfdSessDiscMapEntry->MibObject.
                u4FsMIStdBfdContextId == u4ContextId)
            {
                if (BfdCliPrintSessDiscMapping (CliHandle,
                                                pBfdFsMIStdBfdSessDiscMapEntry->
                                                MibObject.u4FsMIStdBfdContextId,
                                                pBfdFsMIStdBfdSessDiscMapEntry->
                                                MibObject.
                                                u4FsMIStdBfdSessDiscriminator)
                    != CLI_SUCCESS)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_DISCR_MAP_ENTRY_PRINT_UNABLE);
                    return CLI_FAILURE;
                }
            }
            pBfdFsMIStdBfdSessDiscMapEntry =
                BfdGetNextFsMIStdBfdSessDiscMapTable
                (pBfdFsMIStdBfdSessDiscMapEntry);
        }
        while (pBfdFsMIStdBfdSessDiscMapEntry != NULL);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n   ContextId    Sess Disc   Sess Index \r\n ");

        if (BfdCliPrintSessDiscMapping (CliHandle, u4ContextId, u4Value)
            != CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintSessDiscMapping                                 *
 *                                                                           *
 * Description  : This function prints the Session Discriminator mappinp     *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4SessDisc  - Session discriminator                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliPrintSessDiscMapping (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4SessDisc)
{
    tBfdFsMIStdBfdSessDiscMapEntry BfdFsMIStdBfdSessDiscMapTable;

    MEMSET (&BfdFsMIStdBfdSessDiscMapTable, 0,
            sizeof (tBfdFsMIStdBfdSessDiscMapEntry));

    BfdFsMIStdBfdSessDiscMapTable.MibObject.u4FsMIStdBfdContextId = u4ContextId;

    BfdFsMIStdBfdSessDiscMapTable.MibObject.u4FsMIStdBfdSessDiscriminator =
        u4SessDisc;

    if (BfdGetAllFsMIStdBfdSessDiscMapTable
        (&BfdFsMIStdBfdSessDiscMapTable) != OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_BFD_SESS_MAPENTRY_NOT_EXISTS);
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBLE_FETCH_FAIL);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r%12u %12u %12u \r\n ",
               BfdFsMIStdBfdSessDiscMapTable.MibObject.
               u4FsMIStdBfdContextId,
               BfdFsMIStdBfdSessDiscMapTable.MibObject.
               u4FsMIStdBfdSessDiscriminator,
               BfdFsMIStdBfdSessDiscMapTable.MibObject.
               u4FsMIStdBfdSessDiscMapIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliChangeSessMode                                       *
 *                                                                           *
 * Description  : Changes the mode to BFD Session configuration mode         *
 *                                                                           *
 * Input        : CliHandle - Index of current CLI context                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliChangeSessMode (tCliHandle CliHandle, UINT4 u4SessId, UINT4 u4ContextId)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    SPRINTF ((CHR1 *) au1Cmd, "%s%u", BFD_SESS_MODE, u4SessId);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into Sess configuration mode\r\n");
        return CLI_FAILURE;
    }
    CLI_SET_CXT_ID (u4ContextId);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdGetBfdCfgPrompt                                   *
 *                                                                         *
 * DESCRIPTION      : This function returns the prompt to be displayed     *
 *                    for Protection group. It is exported to CLI module.  *
 *                    This function will be invoked when the System is     * 
 *                    running in SI mode.                                  *
 *                                                                         *
 * INPUT            : None                                                 *  
 *                                                                         *
 *                                                                         * 
 * OUTPUT           : pi1ModeName - Mode String                            *
 *                    pi1DispStr - Display string                          *
 *                                                                         *
 * RETURNS          : TRUE/FALSE                                           *
 *                                                                         *
 **************************************************************************/
INT1
BfdGetBfdCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4SessId = 0;
    UINT4               u4Len = STRLEN ("bfdsess");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "bfdsess", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4SessId = CLI_ATOI (pi1ModeName);

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_bfd_cmd.
     */
    CLI_SET_SESS_ID (u4SessId);

    STRCPY (pi1DispStr, "(config-bfdsess)#");

    return TRUE;
}

/**************************************************************************
 * Function   : BfdCliGetCxtId
 * Description: This function checks whether context exists or not
 *               and returns the context Id fro the given context name
 * Input      : pau1VrfName
 * Output     : pu4ContextId
 * Returns    : CLI_SUCCESS/CLI_FAILURE
 * 
 * ************************************************************************/
INT4
BfdCliGetCxtId (UINT1 *pau1VrfName, UINT4 *pu4ContextId)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    UINT4               u4VrfLen = STRLEN (pau1VrfName);

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BFD_LOG (BFD_INVALID_CONTEXT, BFD_TRC_CONTEXTID_GET_UNABLE);
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_IS_CONTEXT_VALID;

    MEMCPY (pBfdExtInParams->unReqParams.au1Alias, pau1VrfName, u4VrfLen);

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }
    *pu4ContextId = pBfdExtOutParams->u4VcNum;
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return CLI_SUCCESS;
}

INT4
BfdCliGetOldTrapLevel (UINT4 u4ContextId, UINT4 *pu4OldTraps)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTable;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTable, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    BfdFsMIStdBfdGlobalConfigTable.MibObject.u4FsMIStdBfdContextId
        = u4ContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTable) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_OLD_TRAP_FETCH_FAILED);
        return CLI_FAILURE;
    }

    *pu4OldTraps = BfdFsMIStdBfdGlobalConfigTable.MibObject.i4FsMIBfdTrapEnable;

    return CLI_SUCCESS;
}

INT4
BfdCliGetOldTraceLevel (UINT4 u4ContextId, INT4 *pu4OldDebugLevel)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTable;

    MEMSET (&BfdFsMIStdBfdGlobalConfigTable, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    BfdFsMIStdBfdGlobalConfigTable.MibObject.u4FsMIStdBfdContextId
        = u4ContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTable) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_OLD_TRACE_FETCH_FAILED);
        return CLI_FAILURE;
    }

    *pu4OldDebugLevel =
        BfdFsMIStdBfdGlobalConfigTable.MibObject.i4FsMIBfdTraceLevel;

    return CLI_SUCCESS;
}

/**************************************************************************
 * Function   : BfdCliGetRowPointer
 * Description: This function converts the path information into PathOid
 * Input      : BfdCLiSessPathParams, 
 * Output     : pau4PathOid
 * Returns    : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
BfdCliGetRowPointer (UINT4 u4ContextId,
                     tBfdCliSessPathParams * pBfdCliSessPathParams,
                     UINT4 *pau4PathOid, UINT4 *pu4PathOidLen)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;
    UINT4               u4PathOidLen = 0;
    UINT4               u4MegLen = 0;
    UINT4               u4MeLen = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliGetRowPointer");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdCliGetRowPointer");
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    switch (pBfdCliSessPathParams->ePathType)
    {

        case BFD_PATH_TYPE_NONTE_IPV4:
            pBfdExtInParams->InApiInfo.u4SubReqType =
                MPLS_GET_BASE_OID_FROM_FEC;
            MEMCPY (pBfdExtInParams->InApiInfo.InPathId.LspId.PeerAddr.u4_addr,
                    pBfdCliSessPathParams->SessLdpLspInfo.PeerAddr.u4_addr,
                    sizeof (tIpAddr));
            pBfdExtInParams->InApiInfo.InPathId.LspId.u4AddrType =
                (UINT4) pBfdCliSessPathParams->SessLdpLspInfo.u1AddrType;
            pBfdExtInParams->InApiInfo.InPathId.LspId.u4AddrLen =
                (UINT4) pBfdCliSessPathParams->SessLdpLspInfo.u1AddrLen;
            break;

        case BFD_PATH_TYPE_TE_IPV4:
            pBfdExtInParams->InApiInfo.u4SubReqType = MPLS_GET_BASE_TNL_OID;
            break;

        case BFD_PATH_TYPE_PW:
            pBfdExtInParams->InApiInfo.u4SubReqType =
                MPLS_GET_BASE_OID_FROM_VCID;
            pBfdExtInParams->InApiInfo.InPathId.PwId.u4VcId =
                pBfdCliSessPathParams->SessPwInfo.u4VcId;
            MEMCPY (pBfdExtInParams->InApiInfo.InPathId.PwId.SrcNodeId.
                    MplsRouterId.u4_addr,
                    pBfdCliSessPathParams->SessPwInfo.PeerAddr.u4_addr,
                    sizeof (tIpAddr));
            break;

        case BFD_PATH_TYPE_MEP:
            u4MegLen = STRLEN (pBfdCliSessPathParams->SessMegInfo.au1MegName);
            u4MeLen = STRLEN (pBfdCliSessPathParams->SessMegInfo.au1MeName);
            pBfdExtInParams->InApiInfo.u4SubReqType =
                MPLS_GET_BASE_OID_FROM_MEG_NAME;

            MEMCPY (pBfdExtInParams->InApiInfo.InPathId.MegMeName.au1MegName,
                    pBfdCliSessPathParams->SessMegInfo.au1MegName, u4MegLen);

            MEMCPY (pBfdExtInParams->InApiInfo.InPathId.MegMeName.au1MeName,
                    pBfdCliSessPathParams->SessMegInfo.au1MeName, u4MeLen);
            break;
        case BFD_PATH_TYPE_IPV4:
        case BFD_PATH_TYPE_IPV6:
        default:
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);

            return CLI_FAILURE;
            break;
    }

    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_SERVICE_POINTER_OID;
    pBfdExtInParams->InApiInfo.u4ContextId = u4ContextId;
    pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_UNABLE_TO_GET_PATH_OID);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);

        return CLI_FAILURE;
    }

    if (pBfdCliSessPathParams->ePathType == BFD_PATH_TYPE_TE_IPV4)
    {
        MEMCPY (pau4PathOid, pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
                au4ServiceOidList,
                (pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
                 u2ServiceOidLen) * sizeof (UINT4));
        u4PathOidLen = pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
            u2ServiceOidLen;

        pau4PathOid[u4PathOidLen] =
            pBfdCliSessPathParams->SessTunnelInfo.u4TunnelId;

        pau4PathOid[u4PathOidLen + 1] =
            pBfdCliSessPathParams->SessTunnelInfo.u4TunnelInst;

        MEMCPY (&u4TnlIngressId,
                &pBfdCliSessPathParams->SessTunnelInfo.SrcIpAddr,
                sizeof (UINT4));
        pau4PathOid[u4PathOidLen + BFD_TUNNEL_INGRESS_ID] = u4TnlIngressId;

        MEMCPY (&u4TnlEgressId,
                &pBfdCliSessPathParams->SessTunnelInfo.DstIpAddr,
                sizeof (UINT4));
        pau4PathOid[u4PathOidLen + BFD_TUNNEL_EGRESS_ID] = u4TnlEgressId;

        u4PathOidLen = u4PathOidLen + BFD_TUNNEL_OID_LENGTH;
        *pu4PathOidLen = u4PathOidLen;

    }
    else
    {
        MEMCPY (pau4PathOid, pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
                au4ServiceOidList,
                (pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
                 u2ServiceOidLen) * sizeof (UINT4));
        *pu4PathOidLen = pBfdExtOutParams->MplsApiOutInfo.OutServiceOid.
            u2ServiceOidLen;

    }
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *     FUNCTION NAME    : IssBfdShowDebugging                                *
 *                                                                           *
 *     DESCRIPTION      : This function prints the Bfd debug level           *
 *                                                                           *
 *     INPUT            : CliHandle                                          *
 *                                                                           *
 *     OUTPUT           : None                                               *
 *                                                                           *
 *     RETURNS          : None                                               *
 *                                                                           *
 *****************************************************************************/
VOID
IssBfdShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4TraceLevel = 0;

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdUtilGetBfdGlobalConfigTable (u4ContextId);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return;
    }
    else
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl != BFD_START)
        {
            CliPrintf (CliHandle, "\r\n  Module is shutdown\r\n");
            return;
        }
    }
    i4TraceLevel =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel;
    if (i4TraceLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rBFD :");

    if ((i4TraceLevel & CLI_BFD_DEBUG_INIT_SHUT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD Init Shut debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_MGMT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD Management debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_DATA_PATH) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD data path debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_ALL_CTRL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD control debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_ALL_PKT_DUMP) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD packet dump debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_ALL_RESOURCE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD resources debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_ALL_FAIL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD all fail debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_BUF) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  BFD buffer routing table Calculation "
                   "debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_SESS_ESTB) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  BFD Session establishment debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_SESS_DOWN) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD Session down debugging is on");
    }

    if ((i4TraceLevel & CLI_BFD_DEBUG_POLL_SEQ) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD poll sequence debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_CRITICAL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD Critical debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_REDUNDANCY) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD redundancy debugging is on");
    }
    if ((i4TraceLevel & CLI_BFD_DEBUG_OFFLOAD) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BFD offload debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/******************************************************************************
 * Function Name : BfdCliConvertTimetoString
 * Description   : This routine will display the time string in 00:00:00:00
 *                 format.
 * Input(s)      :  u4Time               - Time 
 * Output(s)     :  pi1Time              - Display Time String
 * Return(s)     : True /False
 *******************************************************************************/
VOID
BfdCliConvertTimetoString (UINT4 u4Time, INT1 *pi1Time)
{
    UINT4               u4Hrs = 0;
    UINT4               u4Mins = 0;
    UINT4               u4Secs = 0;

    MEMSET (pi1Time, 0, BFD_MAX_CLI_TIME_STRING);

    if (u4Time == 0)
    {

        SPRINTF ((CHR1 *) pi1Time, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % BFD_NO_OF_SEC_IN_A_MIN;
    u4Time = u4Time / BFD_NO_OF_SEC_IN_A_MIN;
    u4Mins = u4Time % BFD_NO_OF_MIN_IN_AN_HR;
    u4Time = u4Time / BFD_NO_OF_MIN_IN_AN_HR;
    u4Hrs = u4Time % BFD_NO_OF_HR_IN_A_DAY;

    SNPRINTF ((CHR1 *) pi1Time, BFD_MAX_CLI_TIME_STRING,
              "%02d:%02d:%02d", u4Hrs, u4Mins, u4Secs);
    return;
}

#ifdef BFD_HA_TEST_WANTED

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTestCliShowSessTmr                                        *
 *                                                                           *
 * Description  : This function displays the session Timer details           *
 *                                                                           *
 * Input        : u4SessIndex - Session Index                                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdTestCliShowSessTmr (tCliHandle CliHandle, UINT4 u4SessIndex)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    UINT4               u4ContextId = BFD_DEFAULT_CONTEXT_ID;

    /* If session index is not specified ,display the session 
     * details for all the session for the specified context */
    if (u4SessIndex == 0)
    {
        pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

        if (pBfdFsMIStdBfdSessEntry == NULL)
        {
            return CLI_FAILURE;
        }

        do
        {
            if (pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdContextId == u4ContextId)
            {
                CliPrintf (CliHandle, "\rBfdTestSessPrevTmr.%d.%d =: %d \n",
                           pBfdFsMIStdBfdSessEntry->MibObject.
                           u4FsMIStdBfdContextId,
                           pBfdFsMIStdBfdSessEntry->MibObject.
                           u4FsMIStdBfdSessIndex,
                           pBfdFsMIStdBfdSessEntry->i4BfdTestSessPrevTmr);
                CliPrintf (CliHandle, "\rBfdTestSessCurrTmr.%d.%d =: %d \n",
                           pBfdFsMIStdBfdSessEntry->MibObject.
                           u4FsMIStdBfdContextId,
                           pBfdFsMIStdBfdSessEntry->MibObject.
                           u4FsMIStdBfdSessIndex,
                           pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr);
            }
            pBfdFsMIStdBfdSessEntry =
                BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);
        }
        while (pBfdFsMIStdBfdSessEntry != NULL);

    }
    /* If session index is given , display the session 
     * details for the given session index */
    else
    {
        pBfdFsMIStdBfdSessEntry =
            BfdUtilGetBfdSessTable (u4ContextId, u4SessIndex);

        if (pBfdFsMIStdBfdSessEntry == NULL)
        {
            return CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle, "\rBfdTestSessPrevTmr.%d.%d =: %d \n",
                       pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                       pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                       pBfdFsMIStdBfdSessEntry->i4BfdTestSessPrevTmr);
            CliPrintf (CliHandle, "\rBfdTestSessCurrTmr.%d.%d =: %d \n",
                       pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                       pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                       pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr);
        }
    }
    return CLI_SUCCESS;
}
#endif /* BFD_HA_TEST_WANTED */

/**************************************************************************
 * Function   : BfdCliGetCxtName
 * Description: This function is get the context name from the contextId
 * Input      : u4ContextId
 * Output     : pu1ContextName
 * Returns    : CLI_SUCCESS/CLI_FAILURE
 *
 * ************************************************************************/
INT4
BfdCliGetCxtName (UINT4 u4ContextId, UINT1 *pu1ContextName)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    if (pu1ContextName == NULL)
    {
        return CLI_FAILURE;
    }
    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return CLI_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    pBfdExtInParams->u4ContextId = u4ContextId;

    if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                     pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return CLI_FAILURE;
    }
    MEMCPY (pu1ContextName, pBfdExtOutParams->au1ContextName,
            BFD_MAX_CONTEXT_NAME);
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdShowRunningConfig                                       *
 *                                                                           *
 * Description  : This function displays the the current running             *
 *                configuration of BFD.                                      *
 *                                                                           *
 * Input        : CliHandle - CliContext ID                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4TrcLevel = 0;
    INT4                i4SystemControl = 0;
    UINT4               u4FsMIStdBfdSessIndex = 0;
    UINT4               u4NextFsMIStdBfdSessIndex = 0;
    UINT4               u4NextContextId = 0;
    INT4                i4FsMIStdBfdSessType = 0;
    INT4                i4FsMIBfdSessRole = 0;
    INT4                i4FsMIBfdSessMode = 0;
    INT4                i4FsMIBfdSessOffld = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4GblAdminStatus = 0;
    INT4                i4MapType = 0;
    INT4                i4TrapEnable = 0;
    INT4                i4SessTmrNegotiate = 0;
    INT4                i4SessionStatus = 0;
    UINT4               u4ZERO = 0;
    UINT4               u4GblDesiredMinTxIntvl = 0;
    UINT4               u4GblReqMinRxIntvl = 0;
    UINT4               u4GblDetectMult = 0;
    UINT4               u4GblSlowTxIntvl = 0;
    UINT4               u4FsMIStdBfdSessDesiredMinTxInterval = 0;
    UINT4               u4FsMIStdBfdSessReqMinRxInterval = 0;
    UINT4               u4FsMIStdBfdSessDetectMult = 0;
    UINT4               u4RemoteDiscr = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4SessEXPValue = 0;
    UINT4               u4SrcUdpPort = 0;
    UINT4               u4DstUdpPort = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4SlotNumber = 0;
    UINT4               u4sCardNumber = 0;
    INT4                i4ControlPlaneIndepFlag = 0;
    INT4                i4MultipointFlag = 0;
    INT4                i4SessAuthPresFlag = 0;
    INT4                i4AuthenticationType = 0;
    INT4                i4AuthenticationKeyID = 0;
    INT4                i4SessEncapType = 0;
    UINT1               au1MegName[MPLS_MEG_NAME_LEN];    /* Maint Entity Group name */
    UINT1               au1MeName[MPLS_ME_NAME_LEN];    /* Maintenance Entity name */
    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;
    tBfdSessPathParams  BfdSessPathParams;
    CHR1               *pclPeerAddr = NULL;
    CHR1                SrcAddr[IP_SIXTEEN];
    CHR1                DstAddr[IP_SIXTEEN];
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tSNMP_OID_TYPE     *pSesMapPointer = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    /* Static Ipv4 data */
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];

    /* Initilation */
    pSesMapPointer = alloc_oid (MAX_OID_LEN);
    if (pSesMapPointer == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (&SrcAddr, 0, sizeof (SrcAddr));
    MEMSET (&DstAddr, 0, sizeof (DstAddr));
    MEMSET (&BfdSessPathParams, 0, sizeof (tBfdSessPathParams));
    MEMSET (&au1MegName, 0, sizeof (au1MegName));
    MEMSET (&au1MeName, 0, sizeof (au1MeName));
    MEMSET (&au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    BFD_LOCK;

    /* Global Configuration table */
    if (nmhGetFirstIndexFsMIStdBfdGlobalConfigTable
        (&u4NextContextId) == SNMP_FAILURE)
    {
        SNMP_FreeOid (pSesMapPointer);
        BFD_UNLOCK;
        return CLI_FAILURE;
    }
    do
    {
        u4ContextId = u4NextContextId;
        /* For Vrf to get the context Name  */
        VcmGetAliasName (u4ContextId, au1ContextName);
        /* debug bfd  modes */
        if (nmhGetFsMIBfdTraceLevel (u4ContextId, &i4TrcLevel) == SNMP_SUCCESS)
        {
            if (i4TrcLevel == CLI_BFD_DEBUG_ALL)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r debug bfd all\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r debug bfd all vrf %s\r\n",
                               au1ContextName);
                }
            }
            else
            {
                if ((i4TrcLevel & CLI_BFD_DEBUG_INIT_SHUT) != 0)
                {
                    if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                    {
                        CliPrintf (CliHandle, "\r debug bfd init-shut\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r debug bfd init-shut vrf %s\r\n",
                                   au1ContextName);
                    }

                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_MGMT) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd mgmt\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_DATA_PATH) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd data-path\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_ALL_CTRL) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd ctrl\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_ALL_PKT_DUMP) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd pkt-dump\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_ALL_RESOURCE) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd resource\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_ALL_FAIL) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd all-fail\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_BUF) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd buf\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_SESS_ESTB) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd sess-estb\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_SESS_DOWN) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd sess-down\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_REDUNDANCY) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd redundancy\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_OFFLOAD) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd offload\r\n");
                }
                if ((i4TrcLevel & CLI_BFD_DEBUG_POLL_SEQ) != 0)
                {
                    CliPrintf (CliHandle, "\r debug bfd poll-seq\r\n");
                }
            }

        }

        CliPrintf (CliHandle, "\r\n!");

        /* System control */

        if (nmhGetFsMIBfdSystemControl (u4ContextId, &i4SystemControl) ==
            SNMP_SUCCESS)
        {
            if (i4SystemControl != BFD_ENABLED)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r shutdown bfd \r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r shutdown bfd vrf %s\r\n",
                               au1ContextName);
                }
                /* Other global configuration should not be displayed when 
                 * BFD is shutdown */
                continue;
            }
        }

        /* Bfd Notification   */
        if (nmhGetFsMIBfdTrapEnable (u4ContextId, &i4TrapEnable) ==
            SNMP_SUCCESS)
        {
            if (i4TrapEnable != BFD_NO_TRAP)
            {
                if ((i4TrapEnable & BFD_SESS_UP_DOWN_TRAP) != 0)
                {
                    if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-up-down\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-up-down vrf %s\r\n",
                                   au1ContextName);
                    }

                }
                if ((i4TrapEnable & BFD_BTSTRAP_FAIL_TRAP) != 0)
                {
                    if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-btstrap-fail\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-btstrap-fail vrf %s\r\n",
                                   au1ContextName);
                    }
                }
                if ((i4TrapEnable & BFD_NEG_TX_CHANGE_TRAP) != 0)
                {
                    if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-neg-tx-change\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-neg-tx-change vrf %s\r\n",
                                   au1ContextName);
                    }

                }
                if ((i4TrapEnable & BFD_ADMIN_CTRL_ERR_TRAP) != 0)
                {
                    if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-admin-ctrl-error\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd notification sess-admin-ctrl-error vrf %s\r\n",
                                   au1ContextName);
                    }
                }
                if (u4ContextId != BFD_DEFAULT_CONTEXT_ID)
                {

                }
            }
        }
        /* BFD global configurations */

        CliPrintf (CliHandle, "\r\n!");

        if (nmhGetFsMIBfdGblDesiredMinTxIntvl (u4ContextId,
                                               &u4GblDesiredMinTxIntvl) ==
            SNMP_SUCCESS)
        {
            if (u4GblDesiredMinTxIntvl / (CLI_BFD_CONVERT_SECONDS) !=
                CLI_BFD_CONVERT_SECONDS)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle,
                               "\r bfd global interval %u\r\n",
                               (u4GblDesiredMinTxIntvl) /
                               (CLI_BFD_CONVERT_SECONDS));
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r bfd global interval %u vrf %s\r\n",
                               (u4GblDesiredMinTxIntvl) /
                               (CLI_BFD_CONVERT_SECONDS), au1ContextName);
                }
            }

        }
        if (nmhGetFsMIBfdGblReqMinRxIntvl (u4ContextId,
                                           &u4GblReqMinRxIntvl) == SNMP_SUCCESS)
        {
            if (u4GblReqMinRxIntvl / (CLI_BFD_CONVERT_SECONDS) !=
                CLI_BFD_CONVERT_SECONDS)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle,
                               "\r bfd global min_rx %u\r\n",
                               (u4GblReqMinRxIntvl) /
                               (CLI_BFD_CONVERT_SECONDS));
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r bfd global min_rx %u vrf %s\r\n",
                               (u4GblReqMinRxIntvl) / (CLI_BFD_CONVERT_SECONDS),
                               au1ContextName);
                }
            }
        }
        if (nmhGetFsMIBfdGblDetectMult (u4ContextId, &u4GblDetectMult) ==
            SNMP_SUCCESS)
        {
            if (u4GblDetectMult != CLI_BFD_DETECT_MULT)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle,
                               "\r bfd global multiplier %u\r\n",
                               u4GblDetectMult);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r bfd global multiplier %u vrf %s\r\n",
                               u4GblDetectMult, au1ContextName);
                }
            }
        }
        if (nmhGetFsMIBfdGblSlowTxIntvl (u4ContextId,
                                         &u4GblSlowTxIntvl) == SNMP_SUCCESS)
        {
            if (u4GblSlowTxIntvl / (CLI_BFD_CONVERT_SECONDS) !=
                CLI_BFD_CONVERT_SECONDS)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle,
                               "\r  bfd global slow-tx-interval %u\r\n",
                               (u4GblSlowTxIntvl) / (CLI_BFD_CONVERT_SECONDS));
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r  bfd global slow-tx-interval %u vrf %s\r\n",
                               (u4GblSlowTxIntvl) / (CLI_BFD_CONVERT_SECONDS),
                               au1ContextName);
                }
            }
        }
        if (nmhGetFsMIStdBfdAdminStatus (u4ContextId,
                                         &i4GblAdminStatus) == SNMP_SUCCESS)
        {
            /*Default value is Admin Start */
            if (i4GblAdminStatus != BFD_ENABLED)
            {
                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r no bfd enable \r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r no bfd enable vrf %s \r\n",
                               au1ContextName);
                }
            }
        }
        MEMSET (&au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    }
    while (nmhGetNextIndexFsMIStdBfdGlobalConfigTable (u4ContextId,
                                                       &u4NextContextId) !=
           SNMP_FAILURE);

    u4ContextId = 0;
    u4NextContextId = 0;

    /* Getting the global table entry for first context */
    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetFirstFsMIStdBfdGlobalConfigTable ();

    while (pBfdFsMIStdBfdGlobalConfigTableEntry != NULL)
    {
        CliPrintf (CliHandle, "\r\n!");

        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl == BFD_SHUTDOWN)
        {
            pBfdFsMIStdBfdGlobalConfigTableEntry =
                BfdGetNextFsMIStdBfdGlobalConfigTable
                (pBfdFsMIStdBfdGlobalConfigTableEntry);
            continue;
        }

        u4ContextId =
            pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIStdBfdContextId;
        u4FsMIStdBfdSessIndex = 0;
        while ((nmhGetNextIndexFsMIStdBfdSessTable
                (u4ContextId, &u4NextContextId, u4FsMIStdBfdSessIndex,
                 &u4NextFsMIStdBfdSessIndex) != SNMP_FAILURE)
               && (u4NextContextId == u4ContextId)
               && (u4NextFsMIStdBfdSessIndex != u4ZERO))
        {
            u4FsMIStdBfdSessIndex = u4NextFsMIStdBfdSessIndex;
            u4ContextId = u4NextContextId;
            VcmGetAliasName (u4ContextId, au1ContextName);

            pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId,
                                                    u4FsMIStdBfdSessIndex);
            if (pBfdSessEntry == NULL)
            {
                SNMP_FreeOid (pSesMapPointer);
                BFD_UNLOCK;
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\r\n");
            if (!BFD_CHECK_SESS_IS_DYNAMIC (pBfdSessEntry))
            {

                if (u4ContextId == BFD_DEFAULT_CONTEXT_ID)
                {
                    CliPrintf (CliHandle, "\r bfd session %u \r\n",
                               u4FsMIStdBfdSessIndex);
                }
                else
                {
                    CliPrintf (CliHandle, "\r bfd session %u vrf %s \r\n",
                               u4FsMIStdBfdSessIndex, au1ContextName);
                }

                MEMCPY (au1BfdClients, &pBfdSessEntry->MibObject.
                        u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

                if ((nmhGetFsMIBfdSessMapType (u4ContextId,
                                               u4FsMIStdBfdSessIndex,
                                               &i4MapType) == SNMP_SUCCESS) &&
                    ((pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
                      BFD_OTHER)
                     || (pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
                         BFD_NONVOLATILE)))
                {
                    if (i4MapType == BFD_PATH_TYPE_IPV4)
                    {
                        MEMCPY (&u4DstIpAddr, &pBfdSessEntry->MibObject.
                                au1FsMIStdBfdSessDstAddr, MAX_IPV4_ADDR_LEN);
                        CLI_CONVERT_IPADDR_TO_STR (pclPeerAddr,
                                                   OSIX_NTOHL (u4DstIpAddr));
                        STRNCPY (&DstAddr, pclPeerAddr, STRLEN (pclPeerAddr));
                        u4IfIndex =
                            (UINT4) pBfdSessEntry->MibObject.
                            i4FsMIStdBfdSessInterface;
                        /* Getting Interface name from IfIndex */
                        if (CfaCliConfGetIfName (u4IfIndex, (INT1 *) au1NameStr)
                            == CFA_SUCCESS)
                        {
                            CliPrintf (CliHandle, "\r bfd ipv4 %s %s \r\n",
                                       DstAddr, au1NameStr);
                        }

                    }
                    else if (i4MapType == BFD_PATH_TYPE_IPV6)
                    {
                        u4IfIndex =
                            (UINT4) pBfdSessEntry->MibObject.
                            i4FsMIStdBfdSessInterface;
                        /* Getting Interface name from IfIndex */
                        if (CfaCliConfGetIfName (u4IfIndex, (INT1 *) au1NameStr)
                            == CFA_SUCCESS)
                        {
                            CliPrintf (CliHandle, "\r bfd ipv6 %s %s \r\n",
                                       Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                                     pBfdSessEntry->MibObject.
                                                     au1FsMIStdBfdSessDstAddr),
                                       au1NameStr);
                        }
                    }
                }

                if (nmhGetFsMIStdBfdSessType (u4ContextId,
                                              u4FsMIStdBfdSessIndex,
                                              &i4FsMIStdBfdSessType) !=
                    SNMP_FAILURE)
                {
                    if (i4FsMIStdBfdSessType == BFD_SESS_TYPE_SINGLE_HOP)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type single-hop\r\n");
                    }
                    else if (i4FsMIStdBfdSessType ==
                             BFD_SESS_TYPE_MULTIHOP_TOTALLY_ARBITRARYPATHS)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type multi-hop-totally-arbitrary-paths\r\n");
                    }

                    else if (i4FsMIStdBfdSessType ==
                             BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type multi-hop-unidirectional-links\r\n");
                    }

                    else if (i4FsMIStdBfdSessType ==
                             BFD_SESS_TYPE_MULTIPOINT_HEAD)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type multi-point-head\r\n");
                    }

                    else if (i4FsMIStdBfdSessType ==
                             BFD_SESS_TYPE_MULTIPOINT_TAIL)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type multi-point-tail\r\n");
                    }
                    else if (i4FsMIStdBfdSessType ==
                             BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-type multi-hop-out-of-band-signalling\r\n");
                    }

                }

                if ((nmhGetFsMIBfdSessMapType (u4ContextId,
                                               u4FsMIStdBfdSessIndex,
                                               &i4MapType) == SNMP_SUCCESS) &&
                    (nmhGetFsMIBfdSessMapPointer (u4ContextId,
                                                  u4FsMIStdBfdSessIndex,
                                                  pSesMapPointer) ==
                     SNMP_SUCCESS))
                {
                    switch (i4MapType)
                    {
                        case BFD_PATH_TYPE_MEP:

                            BfdSessPathParams.ePathType = BFD_PATH_TYPE_MEP;
                            BfdSessPathParams.SessMeParams.u4MegId =
                                pSesMapPointer->pu4_OidList[pSesMapPointer->
                                                            u4_Length -
                                                            BFD_MEG_ID_OFFSET];
                            BfdSessPathParams.SessMeParams.u4MeId =
                                pSesMapPointer->pu4_OidList[pSesMapPointer->
                                                            u4_Length -
                                                            BFD_ME_ID_OFFSET];
                            BfdSessPathParams.SessMeParams.u4MpId =
                                pSesMapPointer->pu4_OidList[pSesMapPointer->
                                                            u4_Length -
                                                            BFD_MP_ID_OFFSET];

                            /* Allocate memory for MPLS Path info */
                            pBfdMplsPathInfo = (tBfdMplsPathInfo *)
                                MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
                            if (pBfdMplsPathInfo == NULL)
                            {
                                BfdUtilUpdateMemAlocFail (u4ContextId);
                                SNMP_FreeOid (pSesMapPointer);
                                BFD_UNLOCK;
                                return CLI_FAILURE;
                            }
                            MEMSET (pBfdMplsPathInfo, 0,
                                    sizeof (tBfdMplsPathInfo));

                            if (BfdExtGetMplsPathInfo (u4ContextId,
                                                       &BfdSessPathParams,
                                                       pBfdMplsPathInfo,
                                                       OSIX_TRUE) !=
                                OSIX_SUCCESS)
                            {
                                MemReleaseMemBlock
                                    (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                     (UINT1 *) pBfdMplsPathInfo);
                                SNMP_FreeOid (pSesMapPointer);
                                BFD_UNLOCK;
                                return OSIX_FAILURE;
                            }
                            STRCPY (au1MegName,
                                    pBfdMplsPathInfo->MplsMegApiInfo.
                                    MplsMegMeName.au1MegName);
                            STRCPY (au1MeName,
                                    pBfdMplsPathInfo->MplsMegApiInfo.
                                    MplsMegMeName.au1MeName);

                            CliPrintf (CliHandle,
                                       "\r bfd mpls meg-name %s me-name %s ",
                                       au1MegName, au1MeName);
                            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                                (UINT1 *) pBfdMplsPathInfo);
                            break;
                        case BFD_PATH_TYPE_PW:

                            /* Allocate memory from mempool */
                            pBfdExtInParams = (tBfdExtInParams *)
                                MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
                            if (pBfdExtInParams == NULL)
                            {
                                BfdUtilUpdateMemAlocFail (u4ContextId);
                                SNMP_FreeOid (pSesMapPointer);
                                BFD_UNLOCK;
                                return CLI_FAILURE;
                            }
                            MEMSET (pBfdExtInParams, 0,
                                    sizeof (tBfdExtInParams));

                            pBfdExtOutParams = (tBfdExtOutParams *)
                                MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
                            if (pBfdExtOutParams == NULL)
                            {
                                BfdUtilUpdateMemAlocFail (u4ContextId);
                                MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                                    (UINT1 *) pBfdExtInParams);
                                SNMP_FreeOid (pSesMapPointer);
                                BFD_UNLOCK;
                                return CLI_FAILURE;
                            }
                            MEMSET (pBfdExtOutParams, 0,
                                    sizeof (tBfdExtOutParams));
                            pBfdExtInParams->u4ContextId = u4ContextId;
                            pBfdExtInParams->eExtReqType =
                                BFD_REQ_MPLS_GET_PW_INFO;
                            pBfdExtInParams->InApiInfo.u4SubReqType =
                                MPLS_GET_PW_INFO_FROM_PW_INDEX;
                            pBfdExtInParams->InApiInfo.u4ContextId = 0;
                            pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;
                            pBfdExtInParams->InApiInfo.InPathId.PwId.u4PwIndex =
                                pSesMapPointer->pu4_OidList[pSesMapPointer->
                                                            u4_Length - 1];

                            if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                                             pBfdExtOutParams)
                                != OSIX_SUCCESS)
                            {
                                MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                                    (UINT1 *) pBfdExtInParams);
                                MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                                    (UINT1 *) pBfdExtOutParams);
                                SNMP_FreeOid (pSesMapPointer);
                                BFD_UNLOCK;
                                return CLI_FAILURE;
                            }

                            MEMCPY (&u4IpAddr,
                                    &(pBfdExtOutParams->MplsApiOutInfo.
                                      OutPwInfo.MplsPwPathId.DstNodeId.
                                      MplsRouterId), MAX_IPV4_ADDR_LEN);

                            CLI_CONVERT_IPADDR_TO_STR (pclPeerAddr,
                                                       OSIX_NTOHL (u4IpAddr));

                            BfdSessPathParams.SessPwParams.u4VcId =
                                pBfdExtOutParams->MplsApiOutInfo.OutPwInfo.
                                MplsPwPathId.u4VcId;

                            CliPrintf (CliHandle,
                                       "\r bfd mpls pseudowire %s vc-id %d ",
                                       pclPeerAddr,
                                       BfdSessPathParams.SessPwParams.u4VcId);
                            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                                (UINT1 *) pBfdExtInParams);
                            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                                (UINT1 *) pBfdExtOutParams);
                            pBfdExtInParams = NULL;
                            pBfdExtOutParams = NULL;

                            break;

                        case BFD_PATH_TYPE_TE_IPV4:
                        case BFD_PATH_TYPE_TE_IPV6:

                            MEMCPY (&u4DstIpAddr,
                                    &(pSesMapPointer->
                                      pu4_OidList[pSesMapPointer->u4_Length -
                                                  BFD_TUNNEL_DEST_OFFSET]),
                                    sizeof (UINT4));

                            CLI_CONVERT_IPADDR_TO_STR (pclPeerAddr,
                                                       OSIX_NTOHL
                                                       (u4DstIpAddr));
                            STRCPY (&DstAddr, pclPeerAddr);

                            MEMCPY (&u4SrcIpAddr,
                                    &(pSesMapPointer->
                                      pu4_OidList[pSesMapPointer->u4_Length -
                                                  BFD_TUNNEL_SRC_OFFSET]),
                                    sizeof (UINT4));

                            CLI_CONVERT_IPADDR_TO_STR (pclPeerAddr,
                                                       OSIX_NTOHL
                                                       (u4SrcIpAddr));
                            STRCPY (&SrcAddr, pclPeerAddr);

                            CliPrintf (CliHandle,
                                       "\r bfd mpls traffic-eng tunnel %u lsp %u source %s destination %s \r\n",
                                       pBfdSessEntry->BfdSessPathParams.
                                       SessTeParams.u4TunnelId,
                                       pBfdSessEntry->BfdSessPathParams.
                                       SessTeParams.u4TunnelInst, SrcAddr,
                                       DstAddr);
                            break;
                        default:
                            break;
                    }
                    if (nmhGetFsMIBfdSessEncapType (u4ContextId,
                                                    u4FsMIStdBfdSessIndex,
                                                    &i4SessEncapType) !=
                        SNMP_FAILURE)
                    {
                        if (i4SessEncapType == BFD_ENCAP_TYPE_MPLS_IP_ACH)
                        {
                            CliPrintf (CliHandle, "encap-type mpls-ach-ip\r\n");
                        }
                    }

                }
                if (nmhGetFsMIBfdSessRole (u4ContextId,
                                           u4FsMIStdBfdSessIndex,
                                           &i4FsMIBfdSessRole) != SNMP_FAILURE)
                {
                    /*Default value is Active for SessRole */
                    if (i4FsMIBfdSessRole != BFD_SESS_ROLE_ACTIVE)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params role passive \r\n");
                    }
                }
                if (nmhGetFsMIBfdSessMode (u4ContextId,
                                           u4FsMIStdBfdSessIndex,
                                           &i4FsMIBfdSessMode) != SNMP_FAILURE)
                {
                    /*Default value is CC for SESSMODE */
                    if (i4FsMIBfdSessMode != BFD_SESS_MODE_CC)
                    {
                        CliPrintf (CliHandle, "\r bfd params mode cv \r\n");
                    }
                }
                if (nmhGetFsMIStdBfdSessSourceUdpPort (u4ContextId,
                                                       u4FsMIStdBfdSessIndex,
                                                       &u4SrcUdpPort) !=
                    SNMP_FAILURE)
                {
                    if (u4SrcUdpPort != u4ZERO)
                    {
                        CliPrintf (CliHandle, "\r bfd params src-udp %u \r\n",
                                   u4SrcUdpPort);
                    }
                }
                if (nmhGetFsMIStdBfdSessDestinationUdpPort (u4ContextId,
                                                            u4FsMIStdBfdSessIndex,
                                                            &u4DstUdpPort) !=
                    SNMP_FAILURE)
                {
                    /* Default values are not displayed */
                    if ((u4DstUdpPort != u4ZERO) ||
                        (u4DstUdpPort == BFD_UDP_DEST_PORT) ||
                        (u4DstUdpPort == BFD_UDP_DEST_PORT_MHOP))
                    {
                        CliPrintf (CliHandle, "\r bfd params dest-udp %u \r\n",
                                   u4DstUdpPort);
                    }
                }
                if (nmhGetFsMIBfdSessEXPValue (u4ContextId,
                                               u4FsMIStdBfdSessIndex,
                                               &u4SessEXPValue) != SNMP_FAILURE)
                {
                    if (u4SessEXPValue != u4ZERO)
                    {
                        CliPrintf (CliHandle, "\r bfd params exp %u \r\n",
                                   u4SessEXPValue);
                    }
                }
                if (nmhGetFsMIStdBfdSessAdminStatus (u4ContextId,
                                                     u4FsMIStdBfdSessIndex,
                                                     &i4AdminStatus) !=
                    SNMP_FAILURE)
                {
                    if (i4AdminStatus != CLI_BFD_ADMIN_START)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params sess-admin-status stop\r\n");
                    }
                }
                if (nmhGetFsMIBfdSessOffld (u4ContextId,
                                            u4FsMIStdBfdSessIndex,
                                            &i4FsMIBfdSessOffld) !=
                    SNMP_FAILURE)
                {
                    if (i4FsMIBfdSessOffld == BFD_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "\r bfd set offload \r\n");
                    }
                }
                if (nmhGetFsMIBfdSessTmrNegotiate (u4ContextId,
                                                   u4FsMIStdBfdSessIndex,
                                                   &i4SessTmrNegotiate) !=
                    SNMP_FAILURE)
                {
                    if (i4SessTmrNegotiate != BFD_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "\r no bfd set timer-nego \r\n");
                    }
                }

                /* Default set Timer Nego is not shown */
                if (nmhGetFsMIStdBfdSessControlPlaneIndepFlag (u4ContextId,
                                                               u4FsMIStdBfdSessIndex,
                                                               &i4ControlPlaneIndepFlag)
                    != SNMP_FAILURE)
                {
                    if (i4ControlPlaneIndepFlag == BFD_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd set control-plane-flag \r\n");
                    }
                }
                if (nmhGetFsMIStdBfdSessMultipointFlag (u4ContextId,
                                                        u4FsMIStdBfdSessIndex,
                                                        &i4MultipointFlag) !=
                    SNMP_FAILURE)
                {
                    if (i4MultipointFlag == BFD_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd set multi-point-flag\r\n");
                    }
                }
                if (nmhGetFsMIStdBfdSessAuthPresFlag (u4ContextId,
                                                      u4FsMIStdBfdSessIndex,
                                                      &i4SessAuthPresFlag) !=
                    SNMP_FAILURE)
                {
                    if (i4SessAuthPresFlag == BFD_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "\r bfd set authentication\r\n");
                    }

                    if (nmhGetFsMIStdBfdSessAuthenticationType (u4ContextId,
                                                                u4FsMIStdBfdSessIndex,
                                                                &i4AuthenticationType)
                        != SNMP_FAILURE)
                    {
                        /* no-auth is default */
                        if (i4AuthenticationType == BFD_AUTH_SIMPLE_PASSWORD)
                        {
                            if (nmhGetFsMIStdBfdSessAuthenticationKeyID
                                (u4ContextId, u4FsMIStdBfdSessIndex,
                                 &i4AuthenticationKeyID) != SNMP_FAILURE)
                            {
                                CliPrintf (CliHandle,
                                           "\r bfd authentication type "
                                           "simple-password key-id %d key ********\r\n",
                                           i4AuthenticationKeyID);
                            }
                        }
                    }
                }

                if (pBfdSessEntry->bRemDiscrStaticConf == OSIX_TRUE)
                {
                    if (nmhGetFsMIStdBfdSessRemoteDiscr (u4ContextId,
                                                         u4FsMIStdBfdSessIndex,
                                                         &u4RemoteDiscr) !=
                        SNMP_FAILURE)
                    {
                        if (u4RemoteDiscr != u4ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "\r bfd params remote-discr %u  \r\n",
                                       u4RemoteDiscr);
                        }
                    }
                }
                if (nmhGetFsMIBfdSessCardNumber (u4ContextId,
                                                 u4FsMIStdBfdSessIndex,
                                                 &u4sCardNumber) !=
                    SNMP_FAILURE)
                {
                    if (u4sCardNumber != u4ZERO)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params card-number %u \r\n",
                                   u4sCardNumber);
                    }
                }
                if (nmhGetFsMIBfdSessSlotNumber (u4ContextId,
                                                 u4FsMIStdBfdSessIndex,
                                                 &u4SlotNumber) != SNMP_FAILURE)
                {
                    if (u4SlotNumber != u4ZERO)
                    {
                        CliPrintf (CliHandle,
                                   "\r bfd params slot-number %u \r\n",
                                   u4SlotNumber);
                    }
                }

                if ((nmhGetFsMIStdBfdSessDesiredMinTxInterval (u4ContextId,
                                                               u4FsMIStdBfdSessIndex,
                                                               &u4FsMIStdBfdSessDesiredMinTxInterval)
                     != SNMP_FAILURE)
                    &&
                    (nmhGetFsMIStdBfdSessReqMinRxInterval
                     (u4ContextId, u4FsMIStdBfdSessIndex,
                      &u4FsMIStdBfdSessReqMinRxInterval) != SNMP_FAILURE)
                    &&
                    (nmhGetFsMIStdBfdSessDetectMult
                     (u4ContextId, u4FsMIStdBfdSessIndex,
                      &u4FsMIStdBfdSessDetectMult) != SNMP_FAILURE))
                {
                    /* Default value of u4FsMIStdBfdSessDesiredMinTxInterval =1000sec
                     * ,u4FsMIStdBfdSessReqMinRxInterval 1000sec 
                     * u4FsMIStdBfdSessDetectMult is 3  as per fsmpbfd.mib*/
                    if ((u4FsMIStdBfdSessDesiredMinTxInterval /
                         (CLI_BFD_CONVERT_SECONDS) != CLI_BFD_CONVERT_SECONDS)
                        || (u4FsMIStdBfdSessReqMinRxInterval /
                            (CLI_BFD_CONVERT_SECONDS) !=
                            CLI_BFD_CONVERT_SECONDS)
                        || (u4FsMIStdBfdSessDetectMult != CLI_BFD_DETECT_MULT))
                    {

                        CliPrintf (CliHandle,
                                   "\r bfd interval %u min_rx %u multiplier %u \r\n",
                                   (u4FsMIStdBfdSessDesiredMinTxInterval /
                                    (CLI_BFD_CONVERT_SECONDS)),
                                   (u4FsMIStdBfdSessReqMinRxInterval /
                                    (CLI_BFD_CONVERT_SECONDS)),
                                   u4FsMIStdBfdSessDetectMult);

                    }
                }
                if (nmhGetFsMIStdBfdSessRowStatus (u4ContextId,
                                                   u4FsMIStdBfdSessIndex,
                                                   &i4SessionStatus) !=
                    SNMP_FAILURE)
                {                /*Default value is Admin Start */
                    if (i4SessionStatus != ACTIVE)
                    {
                        CliPrintf (CliHandle, "\r no bfd enable \r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r bfd enable \r\n");
                    }
                }
            }
            CliPrintf (CliHandle, "!\r\n");
        }
        pBfdFsMIStdBfdGlobalConfigTableEntry =
            BfdGetNextFsMIStdBfdGlobalConfigTable
            (pBfdFsMIStdBfdGlobalConfigTableEntry);
    }
    SNMP_FreeOid (pSesMapPointer);
    BFD_UNLOCK;
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliShowNeighbors                                        *
 *                                                                           *
 * Description  : This function is to identify which BFD neighbor details    *
 *                needs to be displayed                                      *
 *                                                                           *
 * Input        : CliHandle - Cli Identifier                                 *
 *                u4CxtId   - Context Id                                     *
 *                u1Client  - BFD client Id                                  *
 *                u1AddrType- Address Type                                   *
 *                pu1Addr   - Ip Address                                     *
 *                u1Output  - Indicates output is summary or details         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCliShowNeighbors (tCliHandle CliHandle, UINT4 u4CxtId, UINT1 u1Client,
                     UINT1 u1AddrType, UINT1 *pu1Addr, UINT1 u1Output)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT1               au1IpAddr[CLI_BFD_IPV6_ADDR_LEN];
    UINT4               u4Addr = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1HeaderReq = TRUE;
    BOOL1               bIsRegisteredClient = OSIX_FALSE;

    MEMSET (au1BfdClients, 0, sizeof (au1BfdClients));
    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));

    pBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

    if (pBfdSessEntry == NULL)
    {
        BFD_LOG (u4CxtId, BFD_TRC_SESS_TBL_EMPTY);
        return CLI_FAILURE;
    }

    if (u1AddrType == BFD_INET_ADDR_IPV4)
    {
        u1AddrLen = CLI_BFD_IPV4_ADDR_LEN;
        MEMCPY (&u4Addr, pu1Addr, CLI_BFD_IPV4_ADDR_LEN);
        u4Addr = OSIX_HTONL (u4Addr);
        MEMCPY (au1IpAddr, &u4Addr, CLI_BFD_IPV4_ADDR_LEN);
    }
    else if (u1AddrType == BFD_INET_ADDR_IPV6)
    {
        INET_ATON6 (pu1Addr, au1IpAddr);
        u1AddrLen = CLI_BFD_IPV6_ADDR_LEN;
    }

    do
    {
        /* If context Id is specified, then display only the neighbors
         * configured in that context*/
        if (u4CxtId != BFD_INVALID_CONTEXT)
        {
            if (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId > u4CxtId)
            {
                return CLI_SUCCESS;
            }
            if (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId != u4CxtId)
            {
                continue;
            }
        }
        /* If BFD is shutdown on a particular context, do not display the 
         * neighbors in that context */
        pBfdFsMIStdBfdGlobalConfigTableEntry =
            BfdUtilGetBfdGlobalConfigTable
            (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId);
        if ((pBfdFsMIStdBfdGlobalConfigTableEntry != NULL) &&
            (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdSystemControl != BFD_START))
        {
            continue;
        }
        /* If Client Id is specified, then display only the neighbors
         * if the given client is registered.*/
        if (u1Client != 0)
        {
            MEMCPY (au1BfdClients, &(pBfdSessEntry->MibObject.
                                     u4FsMIBfdSessRegisteredClients),
                    sizeof (au1BfdClients));
            OSIX_BITLIST_IS_BIT_SET (au1BfdClients, u1Client,
                                     sizeof (au1BfdClients),
                                     bIsRegisteredClient);
            if (bIsRegisteredClient != OSIX_TRUE)
            {
                continue;
            }

        }
        /* If the neighbor address is specified, then display only that neighbor */
        if (u1AddrType != 0)
        {
            if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType !=
                u1AddrType)
            {
                continue;
            }
            if (MEMCMP
                (au1IpAddr, pBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                 u1AddrLen) != 0)
            {
                continue;
            }
        }
        if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType == 0)
        {
            continue;
        }

        if (u1HeaderReq == TRUE)
        {
            CliPrintf (CliHandle, "OurAddr                                 "
                       "Neighbor                                "
                       "State     Interface  Session    VRF\r\n");
            CliPrintf (CliHandle, "======================================= "
                       "======================================= "
                       "========= ========== ========== ===\r\n");
        }

        if (u1Output == CLI_BFD_SHOW_SUMMARY)
        {
            u1HeaderReq = FALSE;
        }

        BfdCliPrintNeighbors (CliHandle, pBfdSessEntry, u1Output);

    }
    while ((pBfdSessEntry = BfdGetNextFsMIStdBfdSessTable (pBfdSessEntry)));

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCliPrintNeighbors                                       *
 *                                                                           *
 * Description  : This function prints the BFD neighbor informations         *
 *                                                                           *
 * Input        : CliHandle - Cli Identifier                                 *
 *                pBfdSessEntry - BFD neighbor session to be displayed       *
 *                u1Output  - Indicates output is summary or details         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
BfdCliPrintNeighbors (tCliHandle CliHandle,
                      tBfdFsMIStdBfdSessEntry * pBfdSessEntry, UINT1 u1Output)
{
    CHR1               *pu1TempStr = NULL;
    BOOL1               bIsRegisteredClient = OSIX_FALSE;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT1               au1SessionState[BFD_MAX_CLI_TIME_STRING + 1];
    UINT1               au1Array[BFD_MAX_CLI_TIME_STRING];
    UINT4               u4IpAddr = 0;
    UINT4               u4SysTime = 0;
    UINT1               u1Temp = OSIX_FALSE;

    MEMSET (au1Array, 0, sizeof (au1Array));
    MEMSET (au1SessionState, 0, sizeof (au1SessionState));

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType ==
        BFD_INET_ADDR_IPV4)
    {
        MEMCPY (&u4IpAddr, &pBfdSessEntry->MibObject.
                au1FsMIStdBfdSessSrcAddr, MAX_IPV4_ADDR_LEN);
        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr, OSIX_NTOHL (u4IpAddr));
        CliPrintf (CliHandle, "%-40s", pu1TempStr);
    }

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType ==
        BFD_INET_ADDR_IPV4)
    {
        MEMCPY (&u4IpAddr, &pBfdSessEntry->MibObject.
                au1FsMIStdBfdSessDstAddr, MAX_IPV4_ADDR_LEN);
        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr, OSIX_NTOHL (u4IpAddr));
        CliPrintf (CliHandle, "%-40s", pu1TempStr);
    }
    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType ==
        BFD_INET_ADDR_IPV6)
    {
        CliPrintf (CliHandle, "%-40s",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) pBfdSessEntry->MibObject.
                                 au1FsMIStdBfdSessSrcAddr));
    }

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType ==
        BFD_INET_ADDR_IPV6)
    {
        CliPrintf (CliHandle, "%-40s",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) pBfdSessEntry->MibObject.
                                 au1FsMIStdBfdSessDstAddr));
    }

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessState ==
        BFD_SESS_STATE_ADMIN_DOWN)
    {
        STRCPY (au1SessionState, "ADMINDOWN");
    }
    else if (pBfdSessEntry->MibObject.
             i4FsMIStdBfdSessState == BFD_SESS_STATE_UP)
    {
        STRCPY (au1SessionState, "UP");
    }
    else if (pBfdSessEntry->MibObject.
             i4FsMIStdBfdSessState == BFD_SESS_STATE_INIT)
    {
        STRCPY (au1SessionState, "INIT");
    }
    else
    {
        STRCPY (au1SessionState, "DOWN");
    }
    CliPrintf (CliHandle, "%-10s", au1SessionState);

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
        BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS)
    {
        CliPrintf (CliHandle, " %-10s", "--------");
    }
    else
    {
        /* Interface name */
        MEMSET (au1Array, 0, sizeof (au1Array));
        CfaCliGetIfName ((UINT4) pBfdSessEntry->MibObject.
                         i4FsMIStdBfdSessInterface, (INT1 *) au1Array);
        CliPrintf (CliHandle, "%-11s", au1Array);
    }

    CliPrintf (CliHandle, "%-11lu",
               pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);

    CliPrintf (CliHandle, "%-3d\r\n",
               pBfdSessEntry->MibObject.u4FsMIStdBfdContextId);

    if (u1Output == CLI_BFD_SHOW_DETAILS)
    {
        CliPrintf (CliHandle,
                   "Session state is %s and not using echo function.\r\n",
                   au1SessionState);
        CliPrintf (CliHandle, "MinTxInt: %d, MinRxInt: %d, Multiplier: %d\r\n",
                   pBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessDesiredMinTxInterval,
                   pBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval,
                   pBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult);

        CliPrintf (CliHandle,
                   "Received MinRxInt: %d, Received Multiplier: %d\r\n",
                   pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval,
                   pBfdSessEntry->MibObject.
                   u4FsMIStdBfdSessNegotiatedDetectMult);

        CliPrintf (CliHandle, "Tx Count: %d, Rx Count: %d\r\n",
                   pBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut,
                   pBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn);

        MEMCPY (au1BfdClients, &pBfdSessEntry->MibObject.
                u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_OSPF,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            u1Temp = OSIX_TRUE;
            CliPrintf (CliHandle, "Registered Protocols: OSPF");
        }

        bIsRegisteredClient = OSIX_FALSE;

        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_OSPF3,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            u1Temp = OSIX_TRUE;
            CliPrintf (CliHandle, "Registered Protocols: OSPF3");
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_LDP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", LDP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: LDP");
            }
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_BGP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", BGP");
            }
            else
            {
                u1Temp = OSIX_TRUE;
                CliPrintf (CliHandle, "Registered Protocols: BGP");
            }
        }
        bIsRegisteredClient = OSIX_FALSE;
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_ISIS,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ", ISIS\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Registered Protocols: ISIS\r\n");
            }
        }
        else
        {
            if (u1Temp == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }

        /* Session UP time */
        if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessState == BFD_SESS_STATE_UP)
        {
            BFD_GET_SYS_TIME (&u4SysTime);
            u4SysTime =
                (u4SysTime - pBfdSessEntry->MibObject.u4FsMIStdBfdSessUpTime);
            MEMSET (au1Array, 0, sizeof (au1Array));
            BfdCliConvertTimetoString (u4SysTime, (INT1 *) au1Array);
            CliPrintf (CliHandle, "Uptime: %s\r\n", au1Array);
        }

    }
}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : BfdCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              */
/*****************************************************************************/
INT4
BfdCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return OSIX_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) BFD_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BfdCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              */
/*****************************************************************************/
INT4
BfdCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[BFD_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, BFD_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return OSIX_FAILURE;
    }
    MEMSET (ai1Buf, 0, BFD_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (BfdCliReadLineFromFile (i4Fd, ai1Buf, BFD_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != BFD_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', BFD_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BfdCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              */
/*****************************************************************************/
INT1
BfdCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (BFD_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (BFD_CLI_EOF);
}
#endif
#endif /* _BFDCLI_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdcli.c                       */
/*-----------------------------------------------------------------------*/
