/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdutl.c,v 1.40 2016/11/02 09:36:10 siva Exp $
*
* Description: This file contains utility functions used by protocol Bfd
*********************************************************************/

#include "bfdinc.h"
#include "bfdclig.h"
INT4
BfdGetAllUtlFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                         pBfdGetFsMIStdBfdGlobalConfigTableEntry,
                                         tBfdFsMIStdBfdGlobalConfigTableEntry *
                                         pBfddsFsMIStdBfdGlobalConfigTableEntry)
{
    UNUSED_PARAM (pBfdGetFsMIStdBfdGlobalConfigTableEntry);
    UNUSED_PARAM (pBfddsFsMIStdBfdGlobalConfigTableEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdUtilUpdateFsMIStdBfdGlobalConfigTable
 * Input       :  The Indices
                pBfdFsMIStdBfdGlobalConfigTableEntry
                pBfdSetFsMIStdBfdGlobalConfigTable
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdUtilUpdateFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                          pBfdOldFsMIStdBfdGlobalConfigTableEntry,
                                          tBfdFsMIStdBfdGlobalConfigTableEntry *
                                          pBfdFsMIStdBfdGlobalConfigTableEntry,
                                          tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
                                          *
                                          pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (pBfdOldFsMIStdBfdGlobalConfigTableEntry);
    pBfdCurrentBfdSessEntryInput = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdCurrentBfdSessEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pBfdCurrentBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    u4ContextId = pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    /* Clear Global statistics */
    /* Check if Clear Global Statistics Object is set */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrGblStats == BFD_SNMP_TRUE)
        {
            /* Clear the global stats */
            pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                u4FsMIBfdMemAllocFailure = 0;
            pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                u4FsMIBfdInputQOverFlows = 0;

            pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                i4FsMIBfdClrGblStats = BFD_SNMP_FALSE;

        }
        BFD_LOG (u4ContextId, BFD_TRC_GBL_STAT_CLEAR);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdCurrentBfdSessEntryInput);
        return OSIX_SUCCESS;
    }

    /* Clear All Session Statistics */
    /* Check if all clear session stats is set */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrAllSessStats == BFD_SNMP_TRUE)
        {
            pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
                u4ContextId;

            /* Also, clear all the stats from the Offload module */
            if (BfdOffSessInfo
                (u4ContextId, NULL, NULL,
                 BFD_OFF_CLEAR_ALL_SESS_STATS) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdCurrentBfdSessEntryInput);
                return OSIX_FAILURE;
            }

            do
            {
                pBfdFsMIStdBfdSessEntry =
                    BfdGetNextFsMIStdBfdSessTable
                    (pBfdCurrentBfdSessEntryInput);

                if (pBfdFsMIStdBfdSessEntry == NULL)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY);
                    break;
                }

                if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
                    u4ContextId)
                {
                    break;
                }
                else
                {
                    BfdFsMIStdBfdSessTableClearStats (pBfdFsMIStdBfdSessEntry);
                }

                pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
                    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
                pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex =
                    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

                pBfdFsMIStdBfdSessEntry = NULL;
            }
            while (1);

            pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                i4FsMIBfdClrAllSessStats = BFD_SNMP_FALSE;
        }
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdCurrentBfdSessEntryInput);

    /* If notification is enabled reflect it in trap */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable != OSIX_FALSE)
    {
        if (BfdUtilNotifFsMIStdBfdGlblCfgTbl
            (pBfdOldFsMIStdBfdGlobalConfigTableEntry,
             pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_TRC_NO_REFLECT_FOR_TRAP_ENABLE);
            return OSIX_FAILURE;
        }
    }

    /* If trap is enabled for session up down reflect 
     * in notification */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable !=
        OSIX_FALSE)
    {
        if (BfdUtilTrapFsMIStdBfdGlblCfgTbl
            (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_TRC_NO_REFLECT_FOR_NOTIFI_ENABLE);
            return OSIX_FAILURE;
        }
    }

    /* If the System Control is set to shutdown */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl !=
            pBfdOldFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl)
        {
            if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                i4FsMIBfdSystemControl == BFD_SHUTDOWN)
            {
                /* Call the util function to delete all enteries */
                if (BfdGblDeleteContext (u4ContextId, OSIX_FALSE) !=
                    OSIX_SUCCESS)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_GBL_TBL_DEL_FAIL);
                    return OSIX_FAILURE;
                }
                pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                    i4FsMIBfdSystemControl = BFD_SHUTDOWN;
            }
            else
            {
                pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                    i4FsMIStdBfdAdminStatus = BFD_SESS_ENABLE;
                BfdGblHandleModuleStatus (u4ContextId, OSIX_TRUE);
            }
        }
    }

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus !=
            pBfdOldFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus)
        {
            if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                i4FsMIStdBfdAdminStatus == BFD_SESS_ENABLE)
            {
                BfdGblHandleModuleStatus (u4ContextId, OSIX_TRUE);
            }
            else
            {
                BfdGblHandleModuleStatus (u4ContextId, OSIX_FALSE);
            }
        }
    }
    return OSIX_SUCCESS;
}

INT4
BfdUtlConvertStringtoOid (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                          UINT4 *pau1FsMIBfdSessMapPointer,
                          INT4 *pau1FsMIBfdSessMapPointerLen)
{
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
            au4FsMIBfdSessMapPointer, pau1FsMIBfdSessMapPointer,
            (*pau1FsMIBfdSessMapPointerLen * sizeof (UINT4)));
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen
        = *pau1FsMIBfdSessMapPointerLen;
    return OSIX_SUCCESS;
}

INT4
BfdGetAllUtlFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                                 pBfdGetFsMIStdBfdSessTable,
                                 tBfdFsMIStdBfdSessEntry *
                                 pBfddsFsMIStdBfdSessTable)
{
    pBfdGetFsMIStdBfdSessTable->BfdHwhandle.u4BfdSessHwHandle =
        pBfddsFsMIStdBfdSessTable->BfdHwhandle.u4BfdSessHwHandle;

    if(BfdUtilGetBfdOffStats (pBfdGetFsMIStdBfdSessTable) 
                   ==  OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdUtilUpdateFsMIStdBfdSessTable
 * Input       :  The Indices
 pBfdFsMIStdBfdSessEntry
 pBfdSetFsMIStdBfdSessTable
 * Output      :  This Routine checks set value 
 with that of the value in database
 and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
BfdUtilUpdateFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                                  pBfdOldFsMIStdBfdSessEntry,
                                  tBfdFsMIStdBfdSessEntry *
                                  pBfdFsMIStdBfdSessEntry,
                                  tBfdIsSetFsMIStdBfdSessEntry *
                                  pBfdIsSetFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdFsMIStdBfdGlobalConfigTableEntry;
    tBfdSessPathParams  BfdSessPathParams;

    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry = NULL;

    UINT4               u4ContextId = 0;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT1               au1EventMask[1] = { 0 };
    UINT1               u1PollInitiated = OSIX_FALSE;

    MEMSET (&BfdSessPathParams, 0, sizeof (tBfdSessPathParams));
    MEMSET (au1BfdClients, 0, sizeof (au1BfdClients));
    MEMSET (&BfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
        (pBfdFsMIStdBfdSessEntry->MibObject.
         i4FsMIStdBfdSessRowStatus == NOT_READY))
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
            CREATE_AND_WAIT;
    }

    u4ContextId = pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    /* To clear  the session statistics */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats == BFD_SNMP_TRUE)
    {
        if (BfdFsMIStdBfdSessTableClearStats (pBfdFsMIStdBfdSessEntry)
            != OSIX_SUCCESS)
        {
            BFD_LOG (u4ContextId, BFD_TRC_SESS_STAT_NOT_CLEAR);
            if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
                (pBfdFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT))
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
                    NOT_READY;
            }
            return OSIX_FAILURE;
        }

        if ((BFD_SESS_OFFLD (pBfdFsMIStdBfdSessEntry) == OSIX_TRUE) &&
            (pBfdFsMIStdBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0))
        {
            if (BfdOffSessInfo
                (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdFsMIStdBfdSessEntry,
                 NULL, BFD_OFF_CLEAR_SESS_STATS) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
    }

    /* If interval and detect multiplier is not configured 
     * inherit from the global table */

    BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.u4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    /* Fetch the information from the global table */

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED);
        if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
            (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT))
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
                NOT_READY;
        }
        return OSIX_FAILURE;
    }

    /* Set the default values from the global table
     * when the row is created, else set the old value if the row 
     * has already been configured*/

    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT)
        {
            pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdSessDiscriminator =
                BfdSessGetDiscriminator (pBfdFsMIStdBfdSessEntry);

            pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessType =
                BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING;

            pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessOperMode =
                BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.
                i4FsMIBfdGblSessOperMode;

            pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdSessDesiredMinTxInterval =
                BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.
                u4FsMIBfdGblDesiredMinTxIntvl;

            pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdSessReqMinRxInterval =
                BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.
                u4FsMIBfdGblReqMinRxIntvl;

            pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdSessDetectMult =
                BfdFsMIStdBfdGlobalConfigTableEntry.MibObject.
                u4FsMIBfdGblDetectMult;

            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
                BFD_SNMP_FALSE;
        }
    }

    /* Set default values for the session objects */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus
        == CREATE_AND_WAIT)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
            BFD_NONVOLATILE;
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
            BFD_SNMP_FALSE;
        pBfdFsMIStdBfdSessEntry->bRemDiscrStaticConf = OSIX_FALSE;
    }

    /* Fill the path info in the database */

    pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType =
        (UINT1) pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;

    /*If Row pointer is set and Encapsulation is not set
     * the encapsulation type is set to the corresponding 
     * to the underlying path*/

    if ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer != OSIX_FALSE)
        && (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType == 0))
    {
        switch (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType)
        {
            case BFD_PATH_TYPE_TE_IPV4:
            case BFD_PATH_TYPE_TE_IPV6:
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
                    BFD_ENCAP_TYPE_MPLS_IP;
                break;
            case BFD_PATH_TYPE_PW:
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
                    BFD_ENCAP_TYPE_VCCV_NEGOTIATED;
                break;
            case BFD_PATH_TYPE_MEP:
                /* Check if underlying path is PW. If it is PW set the
                 * encap type to VCCV negotiated. Else MPLS_ACH */

                /* Fill the path details */
                BfdSessPathParams.ePathType =
                    pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType;
                BfdSessPathParams.SessMeParams.u4MegId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_MEG_ID_OFFSET];
                BfdSessPathParams.SessMeParams.u4MeId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_ME_ID_OFFSET];
                BfdSessPathParams.SessMeParams.u4MpId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_MP_ID_OFFSET];
                /* Allocate memory for MPLS Path info */
                pBfdMplsPathInfo = (tBfdMplsPathInfo *)
                    MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
                if (pBfdMplsPathInfo == NULL)
                {
                    BfdUtilUpdateMemAlocFail (u4ContextId);
                    BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                             "BfdUtilUpdateFsMIStdBfdSessTable");
                    return OSIX_FAILURE;
                }
                MEMSET (pBfdMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

                if (BfdExtGetMplsPathInfo
                    (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     &BfdSessPathParams, pBfdMplsPathInfo, OSIX_FALSE) !=
                    OSIX_SUCCESS)
                {
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pBfdMplsPathInfo);
                    BFD_LOG (u4ContextId, BFD_TRC_RETRIVE_PATH_FAIL);
                    return OSIX_FAILURE;
                }

                if ((pBfdMplsPathInfo->
                     u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
                    BFD_CHECK_PATH_PW_AVAIL)
                {
                    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
                        BFD_ENCAP_TYPE_VCCV_NEGOTIATED;
                }
                else
                {
                    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
                        BFD_ENCAP_TYPE_MPLS_ACH;
                }
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pBfdMplsPathInfo);

                break;
            default:
                return OSIX_FAILURE;
        }
    }

    /* Encapsulation check */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessRowStatus == ACTIVE)
        {
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType != 0)
            {
                if (BfdUtilEncapCheck (pBfdFsMIStdBfdSessEntry) == SNMP_FAILURE)
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_ENCAP_TYPE_MISMATCH);
#endif
                    return OSIX_FAILURE;

                }
            }

        }
    }

    /* Set the Sess Discriminator static flag to true if remote 
     * discriminator is configured statically */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr != OSIX_FALSE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr != 0)
        {
            /* Copy the value of remote discriminator to reflect into the
             * read-only remote discriminator value */

            pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr;
            pBfdFsMIStdBfdSessEntry->bRemDiscrStaticConf = OSIX_TRUE;
        }
    }

    /* Reset the authkey, authkey id and authtype if the auth present flag is being set to
     * FALSE */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag != OSIX_FALSE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag ==
            BFD_SNMP_FALSE)
        {
            pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationType = BFD_AUTH_NONE;

            if (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyID != BFD_AUTH_KEY_ID_MIN_VAL)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyID =
                    BFD_AUTH_KEY_ID_MIN_VAL;
            }

            MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.
                    au1FsMIStdBfdSessAuthenticationKey, '\0',
                    sizeof (pBfdFsMIStdBfdSessEntry->MibObject.
                            au1FsMIStdBfdSessAuthenticationKey));
        }
    }

    /* Clear the Administrative Ctrl and error flags */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate == OSIX_TRUE)
    {
        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate ==
             BFD_SNMP_FALSE) &&
            ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq ==
              BFD_SNMP_TRUE)
             && (pBfdFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessAdminCtrlErrReason ==
                 BFD_SESS_ERROR_PERIOD_MISCONFIG)))
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
                BFD_SNMP_FALSE;
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                BFD_SESS_ERROR_NONE;
        }
    }

    if ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval ==
         OSIX_TRUE) ||
        (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval ==
         OSIX_TRUE) ||
        (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult == OSIX_TRUE))
    {
        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq ==
             BFD_SNMP_TRUE) &&
            (pBfdFsMIStdBfdSessEntry->MibObject.
             i4FsMIBfdSessAdminCtrlErrReason ==
             BFD_SESS_ERROR_PERIOD_MISCONFIG))
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
                BFD_SNMP_FALSE;
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                BFD_SESS_ERROR_NONE;
        }
    }

    /* Fill the path info in the database */
    if (pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer[0] != 0)
    {
        switch (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType)
        {
            case BFD_PATH_TYPE_TE_IPV4:
            case BFD_PATH_TYPE_TE_IPV6:
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType =
                    BFD_PATH_TYPE_TE_IPV4;
                pBfdFsMIStdBfdSessEntry->
                    BfdSessPathParams.SessTeParams.u4AddrType =
                    MPLS_ADDR_TYPE_IPV4;
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                    u4TunnelId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_TUNNEL_ID_OFFSET];
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                    u4TunnelInst =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_TUNNEL_INST_OFFSET];
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                    SrcIpAddr.u4_addr[0] =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_TUNNEL_SRC_OFFSET];
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessTeParams.
                    DstIpAddr.u4_addr[0] =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_TUNNEL_DEST_OFFSET];
                break;

            case BFD_PATH_TYPE_PW:
                /* Allocate memory from mempool */
                pBfdExtInParams = (tBfdExtInParams *)
                    MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
                if (pBfdExtInParams == NULL)
                {
                    BfdUtilUpdateMemAlocFail (u4ContextId);
                    BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                             "BfdUtilUpdateFsMIStdBfdSessTable");
                    return OSIX_FAILURE;
                }
                MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

                pBfdExtOutParams = (tBfdExtOutParams *)
                    MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
                if (pBfdExtOutParams == NULL)
                {
                    BfdUtilUpdateMemAlocFail (u4ContextId);
                    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                        (UINT1 *) pBfdExtInParams);
                    BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                             "BfdUtilUpdateFsMIStdBfdSessTable");
                    return OSIX_FAILURE;
                }
                MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

                pBfdExtInParams->u4ContextId = u4ContextId;
                pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_PW_INFO;
                pBfdExtInParams->InApiInfo.u4SubReqType =
                    MPLS_GET_PW_INFO_FROM_PW_INDEX;
                pBfdExtInParams->InApiInfo.u4ContextId = 0;
                pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;
                pBfdExtInParams->InApiInfo.InPathId.PwId.u4PwIndex =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen - 1];

                if (BfdPortHandleExtInteraction (pBfdExtInParams,
                                                 pBfdExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
                        (pBfdFsMIStdBfdSessEntry->MibObject.
                         i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT))
                    {
                        pBfdFsMIStdBfdSessEntry->MibObject.
                            i4FsMIStdBfdSessRowStatus = NOT_READY;
                    }
                    BFD_LOG (u4ContextId, BFD_TRC_PATH_INFO_FETCH_FAILED);
                    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                        (UINT1 *) pBfdExtInParams);
                    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                        (UINT1 *) pBfdExtOutParams);
                    return OSIX_FAILURE;
                }
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType =
                    BFD_PATH_TYPE_PW;
                MEMCPY (&
                        (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.
                         SessPwParams.PeerAddr),
                        &(pBfdExtOutParams->MplsApiOutInfo.OutPwInfo.
                          MplsPwPathId.DstNodeId.MplsRouterId),
                        sizeof (tIpAddr));

                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessPwParams.u4VcId =
                    pBfdExtOutParams->MplsApiOutInfo.OutPwInfo.MplsPwPathId.
                    u4VcId;
                MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                    (UINT1 *) pBfdExtInParams);
                MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                    (UINT1 *) pBfdExtOutParams);
                pBfdExtInParams = NULL;
                pBfdExtOutParams = NULL;

                break;

            case BFD_PATH_TYPE_MEP:
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType =
                    BFD_PATH_TYPE_MEP;
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.
                    u4MegId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_MEG_ID_OFFSET];
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.u4MeId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_ME_ID_OFFSET];
                pBfdFsMIStdBfdSessEntry->BfdSessPathParams.SessMeParams.u4MpId =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    au4FsMIBfdSessMapPointer[pBfdFsMIStdBfdSessEntry->MibObject.
                                             i4FsMIBfdSessMapPointerLen -
                                             BFD_MP_ID_OFFSET];
                break;

            default:
                break;
        }
    }
    /* When making the row status as ACTIVE, if source address,type,len value is not configured,
     * then it can be filled by using the configured interface index and destination address type
     */
    if ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
        && (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
            ACTIVE) && (!BFD_CHECK_PATH_TYPE_NOT_IP (pBfdFsMIStdBfdSessEntry)))
    {

        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
             BFD_OTHER) ||
            (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
             BFD_NONVOLATILE))
        {
            /* Session DOWN state alone will be notified to the IP module while
             * monitoring IPv4/v6 path
             */
            OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);

            MEMCPY (au1BfdClients, &pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

            /* For IPv4/IPv6 path monitoring set the corresponding bit (IP client Id) in the
             * registered clients info
             */
            if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
                 BFD_PATH_TYPE_IPV4) &&
                (pBfdFsMIStdBfdSessEntry->
                 aBfdClientParams[BFD_CLIENT_ID_IP -
                                  1].pBfdNotifyPathStatusChange == NULL))
            {
                OSIX_BITLIST_SET_BIT (au1BfdClients, BFD_CLIENT_ID_IP,
                                      sizeof (au1BfdClients));
#ifdef BSDCOMP_SLI_WANTED
                OSIX_BITLIST_RESET_BIT (au1BfdClients, BFD_CLIENT_ID_IP6,
                                        sizeof (au1BfdClients));
#endif
                MEMCPY (&
                        (pBfdFsMIStdBfdSessEntry->
                         aBfdClientParams[BFD_CLIENT_ID_IP - 1].u1EventMask),
                        au1EventMask, 1);
                pBfdFsMIStdBfdSessEntry->aBfdClientParams[BFD_CLIENT_ID_IP - 1].
                    pBfdNotifyPathStatusChange =
                    BfdUtilIpHandlePathStatusChange;
                pBfdFsMIStdBfdSessEntry->u1BfdClientsCount++;
            }
#ifdef IP6_WANTED
            if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType
                 == BFD_PATH_TYPE_IPV6) &&
                (pBfdFsMIStdBfdSessEntry->
                 aBfdClientParams[BFD_CLIENT_ID_IP6 -
                                  1].pBfdNotifyPathStatusChange == NULL))
            {
                OSIX_BITLIST_SET_BIT (au1BfdClients, BFD_CLIENT_ID_IP6,
                                      sizeof (au1BfdClients));
#ifdef BSDCOMP_SLI_WANTED
                OSIX_BITLIST_RESET_BIT (au1BfdClients, BFD_CLIENT_ID_IP,
                                        sizeof (au1BfdClients));
#endif
                MEMCPY (&
                        (pBfdFsMIStdBfdSessEntry->
                         aBfdClientParams[BFD_CLIENT_ID_IP6 - 1].u1EventMask),
                        au1EventMask, 1);

                pBfdFsMIStdBfdSessEntry->aBfdClientParams[BFD_CLIENT_ID_IP6 -
                                                          1].
                    pBfdNotifyPathStatusChange =
                    BfdUtilIpHandlePathStatusChange;

                pBfdFsMIStdBfdSessEntry->u1BfdClientsCount++;
            }
#endif
            MEMCPY (&
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     u4FsMIBfdSessRegisteredClients), au1BfdClients,
                    sizeof (au1BfdClients));
        }

        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen == 0)
        {
            if (BfdUtilGetSrcAddrFromIfIndex
                ((UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessInterface,
                 (UINT1) pBfdFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDstAddrType,
                 pBfdFsMIStdBfdSessEntry->MibObject.
                 au1FsMIStdBfdSessSrcAddr,
                 pBfdFsMIStdBfdSessEntry->MibObject.
                 au1FsMIStdBfdSessDstAddr) == OSIX_SUCCESS)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDstAddrLen;

                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
                    pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDstAddrType;
            }
        }
        /* Check If any Ip map entry is already present */
        pBfdSessIpMapEntry =
            BfdUtilCheckIpMapEntryExist (pBfdFsMIStdBfdSessEntry);

        if (pBfdSessIpMapEntry != NULL)
        {
            /* If IP map entry is already existing for different session,
             * return FAILURE. Since only one BFD session should be created for
             * monitoring a single IP path
             */

            /* This flow is hit only when BFD is monitoring a particular path
             * dynamically and same path is now configured for static 
             * monitoring also. 
             * For an existing dynamic session, another dynamic client addition 
             * will be handled separately and not here */

            if (pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex !=
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex)
            {
                BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId, BFD_TRC_IP_MAP_ENTRY_PRESENT,
                         pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId,
                         pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdSessIndex);
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SAME_PATH_MONITORING_EXIST);
#endif
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (BfdCoreCreateBfdSessIpMapEntry (pBfdFsMIStdBfdSessEntry) !=
                OSIX_SUCCESS)
            {
                BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId, BFD_TRC_IP_MAP_CREATE_FAIL,
                         pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdContextId,
                         pBfdFsMIStdBfdSessEntry->MibObject.
                         u4FsMIStdBfdSessIndex);
                return OSIX_FAILURE;
            }
        }
    }

    if ((pBfdOldFsMIStdBfdSessEntry != NULL) &&
        ((pBfdFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessDesiredMinTxInterval !=
          pBfdOldFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessDesiredMinTxInterval) ||
         (pBfdFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessReqMinRxInterval !=
          pBfdOldFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessReqMinRxInterval)))
    {

        if ((pBfdFsMIStdBfdSessEntry->
             MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE) &&
            (pBfdFsMIStdBfdSessEntry->
             MibObject.i4FsMIStdBfdSessAdminStatus == BFD_SESS_ADMIN_START))
        {
            if (BfdPollInitiatePollSequence
                (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
            {
                BFD_LOG (pBfdFsMIStdBfdSessEntry->
                         MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_PARAM_CHANGE_FAIL,
                         pBfdFsMIStdBfdSessEntry->
                         MibObject.u4FsMIStdBfdSessIndex,
                         pBfdFsMIStdBfdSessEntry->
                         MibObject.u4FsMIStdBfdContextId);
                return OSIX_FAILURE;
            }
            u1PollInitiated = OSIX_TRUE;
        }
    }

    /* If u1PollInitiated is set to TRUE, it means that the new params are
     * updated to NP already. If there is a change in Detect multiplier
     * it would have been synced to NP during Poll Sequence Initiation.
     * Hence there is no need for checking Detect Multiplier change. */
    if ((u1PollInitiated == OSIX_FALSE) &&
        (pBfdOldFsMIStdBfdSessEntry != NULL) &&
        ((pBfdFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessDetectMult !=
          pBfdOldFsMIStdBfdSessEntry->
          MibObject.u4FsMIStdBfdSessDetectMult)))
    {
        if ((pBfdFsMIStdBfdSessEntry->
              MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE) &&
            (pBfdFsMIStdBfdSessEntry->
              MibObject.i4FsMIStdBfdSessAdminStatus == BFD_SESS_ADMIN_START))
        {
            /* Check if offload is enabled and success. Then Call offload
             * for updating the Detect Multiplier change in the next Tx packet*/
            if (BFD_SESS_OFFLD (pBfdFsMIStdBfdSessEntry) == OSIX_TRUE)
            {
                if (pBfdFsMIStdBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0)
                {
                    if (BfdOffSessPollInitUpdParams (pBfdFsMIStdBfdSessEntry->
                                MibObject.u4FsMIStdBfdContextId,
                                pBfdFsMIStdBfdSessEntry,
                                &pBfdFsMIStdBfdSessEntry->BfdHwhandle)
                            != OSIX_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    return OSIX_SUCCESS;
                }
            }
        }
    }

    if ((pBfdOldFsMIStdBfdSessEntry != NULL))
    {

        if ((pBfdFsMIStdBfdSessEntry->
             MibObject.u4FsMIStdBfdSessDetectMult !=
             pBfdOldFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult))
        {
            /* update the detect multiplier in the packet */
            pBfdFsMIStdBfdSessEntry->BfdCntlPktBuf.
                au1PacketBuf[BFD_DM_PKT_OFFST] =
                (UINT1) pBfdFsMIStdBfdSessEntry->MibObject.
                u4FsMIStdBfdSessDetectMult;
        }
    }

    /* If Admin Status is set */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus != OSIX_FALSE)
    {
        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus
             == ACTIVE) && (pBfdOldFsMIStdBfdSessEntry != NULL))
        {
            if (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAdminStatus != pBfdOldFsMIStdBfdSessEntry->
                MibObject.i4FsMIStdBfdSessAdminStatus)
            {
                BfdCoreHandleAdminStatusChg (pBfdFsMIStdBfdSessEntry);
            }
        }
    }

    /* If Row Status is set */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
    {
        if ((pBfdOldFsMIStdBfdSessEntry == NULL) ||
            (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus !=
             pBfdOldFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus))
        {
            if (OSIX_SUCCESS !=
                BfdSessHandleSessionEntryStatus (pBfdFsMIStdBfdSessEntry))
            {
                if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT))
                {
                    pBfdFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessRowStatus = NOT_READY;
                }
				else if (pBfdOldFsMIStdBfdSessEntry != NULL)
				{
					pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus=
						pBfdOldFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus;
				}
                return OSIX_FAILURE;
            }
        }
    }

    if ((pBfdOldFsMIStdBfdSessEntry == NULL) &&
        (pBfdFsMIStdBfdSessEntry->MibObject.
         i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT))
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
            NOT_READY;
    }

    return OSIX_SUCCESS;
}

INT4
BfdGetAllUtlFsMIStdBfdSessDiscMapTable (tBfdFsMIStdBfdSessDiscMapEntry *
                                        pBfdGetFsMIStdBfdSessDiscMapEntry,
                                        tBfdFsMIStdBfdSessDiscMapEntry *
                                        pBfddsFsMIStdBfdSessDiscMapEntry)
{
    UNUSED_PARAM (pBfdGetFsMIStdBfdSessDiscMapEntry);
    UNUSED_PARAM (pBfddsFsMIStdBfdSessDiscMapEntry);
    return OSIX_SUCCESS;
}

INT4
BfdGetAllUtlFsMIStdBfdSessIpMapTable (tBfdFsMIStdBfdSessIpMapEntry *
                                      pBfdGetFsMIStdBfdSessIpMapEntry,
                                      tBfdFsMIStdBfdSessIpMapEntry *
                                      pBfddsFsMIStdBfdSessIpMapEntry)
{
    UNUSED_PARAM (pBfdGetFsMIStdBfdSessIpMapEntry);
    UNUSED_PARAM (pBfddsFsMIStdBfdSessIpMapEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdUtilDynBfdSessTableTrigger
 Input       :  The Indices
                pBfdSetFsMIStdBfdSessEntry
                pBfdIsSetFsMIStdBfdSessEntry
 Description :  This Routine is used to send to MSR only
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
BfdUtilDynBfdSessTableTrigger (tBfdFsMIStdBfdSessEntry *
                               pBfdSetFsMIStdBfdSessEntry,
                               tBfdIsSetFsMIStdBfdSessEntry *
                               pBfdIsSetFsMIStdBfdSessEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessSrcAddrVal;
    UINT1               au1FsMIStdBfdSessSrcAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessDstAddrVal;
    UINT1               au1FsMIStdBfdSessDstAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessAuthenticationKeyVal;
    UINT1               au1FsMIStdBfdSessAuthenticationKeyVal[256];
    tSNMP_OID_TYPE     *pFsMIBfdSessMapPointerVal = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;

    pFsMIBfdSessMapPointerVal = alloc_oid (MAX_OID_LEN);

    MEMSET (au1FsMIStdBfdSessSrcAddrVal, 0,
            sizeof (au1FsMIStdBfdSessSrcAddrVal));
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    FsMIStdBfdSessSrcAddrVal.pu1_OctetList = au1FsMIStdBfdSessSrcAddrVal;
    FsMIStdBfdSessSrcAddrVal.i4_Length = 0;

    MEMSET (au1FsMIStdBfdSessDstAddrVal, 0,
            sizeof (au1FsMIStdBfdSessDstAddrVal));
    FsMIStdBfdSessDstAddrVal.pu1_OctetList = au1FsMIStdBfdSessDstAddrVal;
    FsMIStdBfdSessDstAddrVal.i4_Length = 0;

    MEMSET (au1FsMIStdBfdSessAuthenticationKeyVal, 0,
            sizeof (au1FsMIStdBfdSessAuthenticationKeyVal));
    FsMIStdBfdSessAuthenticationKeyVal.pu1_OctetList =
        au1FsMIStdBfdSessAuthenticationKeyVal;
    FsMIStdBfdSessAuthenticationKeyVal.i4_Length = 0;

    if (pFsMIBfdSessMapPointerVal == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Seq number should be zero, because this sync-up is not required for standby */
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = BfdMainTaskLock;
    SnmpNotifyInfo.pUnLockPointer = BfdMainTaskUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = (INT1) i4SetOption;
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber == OSIX_TRUE)
    {

        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessVersionNumber;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessVersionNumber) / sizeof (UINT4);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessVersionNumber));
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessType;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdBfdSessType) / sizeof (UINT4);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessType));
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDestinationUdpPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDestinationUdpPort) / sizeof (UINT4);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessDestinationUdpPort));
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessSourceUdpPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessSourceUdpPort) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessSourceUdpPort));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessEchoSourceUdpPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessEchoSourceUdpPort) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessEchoSourceUdpPort));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessAdminStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessAdminStatus) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessAdminStatus));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessOperMode;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessOperMode) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessOperMode));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDemandModeDesiredFlag;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDemandModeDesiredFlag) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessDemandModeDesiredFlag));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessControlPlaneIndepFlag;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessControlPlaneIndepFlag) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessControlPlaneIndepFlag));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessMultipointFlag;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessMultipointFlag) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessMultipointFlag));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessInterface;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessInterface) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessInterface));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessSrcAddrType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessSrcAddrType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessSrcAddrType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr == OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessSrcAddrVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessSrcAddrLen);
        FsMIStdBfdSessSrcAddrVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessSrcAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessSrcAddr) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %s",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex, &FsMIStdBfdSessSrcAddrVal));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDstAddrType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDstAddrType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessDstAddrType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr == OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessDstAddrVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrLen);
        FsMIStdBfdSessDstAddrVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDstAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDstAddr) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %s",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex, &FsMIStdBfdSessDstAddrVal));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessGTSM;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdBfdSessGTSM) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessGTSM));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessGTSMTTL;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessGTSMTTL) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessGTSMTTL));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDesiredMinTxInterval;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDesiredMinTxInterval) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessDesiredMinTxInterval));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessReqMinRxInterval;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessReqMinRxInterval) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessReqMinRxInterval));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessReqMinEchoRxInterval;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessReqMinEchoRxInterval) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessReqMinEchoRxInterval));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessDetectMult;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessDetectMult) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessDetectMult));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessAuthPresFlag;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessAuthPresFlag) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessAuthPresFlag));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessAuthenticationType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessAuthenticationType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessAuthenticationType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID ==
        OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessAuthenticationKeyID;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessAuthenticationKeyID) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessAuthenticationKeyID));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey ==
        OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessAuthenticationKeyVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessAuthenticationKey,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyLen);
        FsMIStdBfdSessAuthenticationKeyVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen;

        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessAuthenticationKey;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessAuthenticationKey) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %s",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          &FsMIStdBfdSessAuthenticationKeyVal));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessStorType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessStorType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessStorType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessRole;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdSessRole) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessRole));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessMode;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdSessMode) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessMode));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessRemoteDiscr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessRemoteDiscr) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIBfdSessRemoteDiscr));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessEXPValue;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdSessEXPValue) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIBfdSessEXPValue));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessTmrNegotiate;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessTmrNegotiate) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessTmrNegotiate));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessOffld;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdSessOffld) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessOffld));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessEncapType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessEncapType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessEncapType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessMapType;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdSessMapType) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdSessMapType));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer == OSIX_TRUE)
    {
        MEMCPY (pFsMIBfdSessMapPointerVal->pu4_OidList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapPointerLen * (INT4) (sizeof (UINT4))));
        pFsMIBfdSessMapPointerVal->u4_Length =
            (UINT4) pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIBfdSessMapPointerLen;

        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessMapPointer;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessMapPointer) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %o",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex, pFsMIBfdSessMapPointerVal));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessCardNumber;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessCardNumber) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIBfdSessCardNumber));
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdSessSlotNumber;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIBfdSessSlotNumber) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIBfdSessSlotNumber));

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIBfdClearStats;
        SnmpNotifyInfo.u4OidLen = sizeof (FsMIBfdClearStats) / sizeof (UINT4);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIBfdClearStats));

    }
    /* Setting Session Row Status of fsMIStdBfdSessTable is dependent on
     * the members of fsMIBfdSessionTable. Hence setting the rowstatus after setting
     * all the members of fsMIBfdSessionTable is set */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus == OSIX_TRUE)
    {
        SnmpNotifyInfo.pu4ObjectId = FsMIStdBfdSessRowStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsMIStdBfdSessRowStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u1RowStatus = TRUE;

        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdContextId,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          u4FsMIStdBfdSessIndex,
                          pBfdSetFsMIStdBfdSessEntry->MibObject.
                          i4FsMIStdBfdSessRowStatus));

    }

    SNMP_FreeOid (pFsMIBfdSessMapPointerVal);
    return OSIX_SUCCESS;
}
