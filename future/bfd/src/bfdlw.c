/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: bfdlw.c,v 1.6 2013/12/09 10:57:29 siva Exp $
 *                                                                           *
 * Description:                                                              *
 *****************************************************************************/

#include "bfdinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdBfdGlobalConfigTable (UINT4
                                                     u4FsMIStdBfdContextId)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIBfdSystemConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIBfdSystemConfigTable (UINT4 u4FsMIStdBfdContextId)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIBfdGblSessionConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIBfdGblSessionConfigTable (UINT4
                                                      u4FsMIStdBfdContextId)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIBfdStatisticsTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIBfdStatisticsTable (UINT4 u4FsMIStdBfdContextId)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdBfdSessTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdBfdSessTable (UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (u4FsMIStdBfdSessIndex < SESS_INDEX_MIN_VAL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdBfdSessPerfTable (UINT4 u4FsMIStdBfdContextId,
                                                 UINT4 u4FsMIStdBfdSessIndex)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (u4FsMIStdBfdSessIndex < SESS_INDEX_MIN_VAL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIBfdSessionTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIBfdSessionTable (UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (u4FsMIStdBfdSessIndex < SESS_INDEX_MIN_VAL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIBfdSessPerfTable (UINT4 u4FsMIStdBfdContextId,
                                              UINT4 u4FsMIStdBfdSessIndex)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (u4FsMIStdBfdSessIndex < SESS_INDEX_MIN_VAL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessDiscriminator
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdBfdSessDiscMapTable (UINT4 u4FsMIStdBfdContextId,
                                                    UINT4
                                                    u4FsMIStdBfdSessDiscriminator)
{
    if (u4FsMIStdBfdSessDiscriminator < SESS_INDEX_MIN_VAL)
    {
        return SNMP_FAILURE;
    }
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessInterface
                FsMIStdBfdSessSrcAddrType
                FsMIStdBfdSessSrcAddr
                FsMIStdBfdSessDstAddrType
                FsMIStdBfdSessDstAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdBfdSessIpMapTable (UINT4 u4FsMIStdBfdContextId,
                                                  INT4
                                                  i4FsMIStdBfdSessInterface,
                                                  INT4
                                                  i4FsMIStdBfdSessSrcAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIStdBfdSessSrcAddr,
                                                  INT4
                                                  i4FsMIStdBfdSessDstAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIStdBfdSessDstAddr)
{
    if ((u4FsMIStdBfdContextId > BFD_MAX_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMIStdBfdSessSrcAddrType != BFD_INET_ADDR_IPV4)
        && (i4FsMIStdBfdSessSrcAddrType != BFD_INET_ADDR_IPV6))
    {
        return SNMP_FAILURE;
    }
    if ((i4FsMIStdBfdSessDstAddrType != BFD_INET_ADDR_IPV4)
        && (i4FsMIStdBfdSessDstAddrType != BFD_INET_ADDR_IPV6))
    {
        return SNMP_FAILURE;
    }
    if ((pFsMIStdBfdSessSrcAddr->i4_Length != BFD_IPV4_MAX_ADDR_LEN)
        && (pFsMIStdBfdSessSrcAddr->i4_Length != BFD_IPV6_MAX_ADDR_LEN))
    {
        return SNMP_FAILURE;
    }
    if ((pFsMIStdBfdSessDstAddr->i4_Length != BFD_IPV4_MAX_ADDR_LEN)
        && (pFsMIStdBfdSessDstAddr->i4_Length != BFD_IPV6_MAX_ADDR_LEN))
    {
        return SNMP_FAILURE;
    }
    if (CfaValidateIfMainTableIndex (i4FsMIStdBfdSessInterface) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
