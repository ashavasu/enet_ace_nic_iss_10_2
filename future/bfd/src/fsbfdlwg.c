/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbfdlwg.c,v 1.3 2013/07/13 12:43:28 siva Exp $
*
* Description: Low Level GET Routine for All Objects
*********************************************************************/
#include "bfdinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdSystemControl
 Input       :  The Indices

                The Object 
                retValFsBfdSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSystemControl (INT4 *pi4RetValFsBfdSystemControl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdSystemControl (0, pi4RetValFsBfdSystemControl);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdSystemControl
 Input       :  The Indices

                The Object 
                setValFsBfdSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSystemControl (INT4 i4SetValFsBfdSystemControl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdSystemControl (0, i4SetValFsBfdSystemControl);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdSystemControl
 Input       :  The Indices

                The Object 
                testValFsBfdSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsBfdSystemControl)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsMIBfdSystemControl (pu4ErrorCode, 0,
                                       i4TestValFsBfdSystemControl);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdTraceLevel
 Input       :  The Indices

                The Object 
                retValFsBfdTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdTraceLevel (INT4 *pi4RetValFsBfdTraceLevel)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdTraceLevel (0, pi4RetValFsBfdTraceLevel);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdTraceLevel
 Input       :  The Indices

                The Object 
                setValFsBfdTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdTraceLevel (INT4 i4SetValFsBfdTraceLevel)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdTraceLevel (0, i4SetValFsBfdTraceLevel);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdTraceLevel
 Input       :  The Indices

                The Object 
                testValFsBfdTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsBfdTraceLevel)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdTraceLevel (pu4ErrorCode, 0, i4TestValFsBfdTraceLevel);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdTraceLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdTrapEnable
 Input       :  The Indices

                The Object 
                retValFsBfdTrapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdTrapEnable (INT4 *pi4RetValFsBfdTrapEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdTrapEnable (0, pi4RetValFsBfdTrapEnable);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdTrapEnable
 Input       :  The Indices

                The Object 
                setValFsBfdTrapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdTrapEnable (INT4 i4SetValFsBfdTrapEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdTrapEnable (0, i4SetValFsBfdTrapEnable);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdTrapEnable
 Input       :  The Indices

                The Object 
                testValFsBfdTrapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdTrapEnable (UINT4 *pu4ErrorCode, INT4 i4TestValFsBfdTrapEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdTrapEnable (pu4ErrorCode, 0, i4TestValFsBfdTrapEnable);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdTrapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdTrapEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdGblSessOperMode
 Input       :  The Indices

                The Object 
                retValFsBfdGblSessOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdGblSessOperMode (INT4 *pi4RetValFsBfdGblSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdGblSessOperMode (0, pi4RetValFsBfdGblSessOperMode);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdGblSessOperMode
 Input       :  The Indices

                The Object 
                setValFsBfdGblSessOperMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdGblSessOperMode (INT4 i4SetValFsBfdGblSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdGblSessOperMode (0, i4SetValFsBfdGblSessOperMode);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdGblSessOperMode
 Input       :  The Indices

                The Object 
                testValFsBfdGblSessOperMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdGblSessOperMode (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsBfdGblSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdGblSessOperMode (pu4ErrorCode, 0,
                                         i4TestValFsBfdGblSessOperMode);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdGblSessOperMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdGblSessOperMode (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdGblDesiredMinTxIntvl
 Input       :  The Indices

                The Object 
                retValFsBfdGblDesiredMinTxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdGblDesiredMinTxIntvl (UINT4 *pu4RetValFsBfdGblDesiredMinTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdGblDesiredMinTxIntvl (0,
                                           pu4RetValFsBfdGblDesiredMinTxIntvl);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdGblDesiredMinTxIntvl
 Input       :  The Indices

                The Object 
                setValFsBfdGblDesiredMinTxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdGblDesiredMinTxIntvl (UINT4 u4SetValFsBfdGblDesiredMinTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdGblDesiredMinTxIntvl (0,
                                           u4SetValFsBfdGblDesiredMinTxIntvl);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdGblDesiredMinTxIntvl
 Input       :  The Indices

                The Object 
                testValFsBfdGblDesiredMinTxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdGblDesiredMinTxIntvl (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsBfdGblDesiredMinTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdGblDesiredMinTxIntvl (pu4ErrorCode, 0,
                                              u4TestValFsBfdGblDesiredMinTxIntvl);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdGblDesiredMinTxIntvl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdGblDesiredMinTxIntvl (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdGblReqMinRxIntvl
 Input       :  The Indices

                The Object 
                retValFsBfdGblReqMinRxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdGblReqMinRxIntvl (UINT4 *pu4RetValFsBfdGblReqMinRxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdGblReqMinRxIntvl (0, pu4RetValFsBfdGblReqMinRxIntvl);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdGblReqMinRxIntvl
 Input       :  The Indices

                The Object 
                setValFsBfdGblReqMinRxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdGblReqMinRxIntvl (UINT4 u4SetValFsBfdGblReqMinRxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdGblReqMinRxIntvl (0, u4SetValFsBfdGblReqMinRxIntvl);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdGblReqMinRxIntvl
 Input       :  The Indices

                The Object 
                testValFsBfdGblReqMinRxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdGblReqMinRxIntvl (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsBfdGblReqMinRxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdGblReqMinRxIntvl (pu4ErrorCode, 0,
                                          u4TestValFsBfdGblReqMinRxIntvl);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdGblReqMinRxIntvl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdGblReqMinRxIntvl (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdGblDetectMult
 Input       :  The Indices

                The Object 
                retValFsBfdGblDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdGblDetectMult (UINT4 *pu4RetValFsBfdGblDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdGblDetectMult (0, pu4RetValFsBfdGblDetectMult);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdGblDetectMult
 Input       :  The Indices

                The Object 
                setValFsBfdGblDetectMult
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdGblDetectMult (UINT4 u4SetValFsBfdGblDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdGblDetectMult (0, u4SetValFsBfdGblDetectMult);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdGblDetectMult
 Input       :  The Indices

                The Object 
                testValFsBfdGblDetectMult
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdGblDetectMult (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsBfdGblDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdGblDetectMult (pu4ErrorCode, 0,
                                       u4TestValFsBfdGblDetectMult);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdGblDetectMult
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdGblDetectMult (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdGblSlowTxIntvl
 Input       :  The Indices

                The Object 
                retValFsBfdGblSlowTxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdGblSlowTxIntvl (UINT4 *pu4RetValFsBfdGblSlowTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdGblSlowTxIntvl (0, pu4RetValFsBfdGblSlowTxIntvl);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdGblSlowTxIntvl
 Input       :  The Indices

                The Object 
                setValFsBfdGblSlowTxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdGblSlowTxIntvl (UINT4 u4SetValFsBfdGblSlowTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdGblSlowTxIntvl (0, u4SetValFsBfdGblSlowTxIntvl);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdGblSlowTxIntvl
 Input       :  The Indices

                The Object 
                testValFsBfdGblSlowTxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdGblSlowTxIntvl (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsBfdGblSlowTxIntvl)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdGblSlowTxIntvl (pu4ErrorCode, 0,
                                        u4TestValFsBfdGblSlowTxIntvl);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdGblSlowTxIntvl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdGblSlowTxIntvl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdMemAllocFailure
 Input       :  The Indices

                The Object 
                retValFsBfdMemAllocFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdMemAllocFailure (UINT4 *pu4RetValFsBfdMemAllocFailure)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdMemAllocFailure (0, pu4RetValFsBfdMemAllocFailure);
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdInputQOverFlows
 Input       :  The Indices

                The Object 
                retValFsBfdInputQOverFlows
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdInputQOverFlows (UINT4 *pu4RetValFsBfdInputQOverFlows)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdInputQOverFlows (0, pu4RetValFsBfdInputQOverFlows);
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdClrGblStats
 Input       :  The Indices

                The Object 
                retValFsBfdClrGblStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdClrGblStats (INT4 *pi4RetValFsBfdClrGblStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdClrGblStats (0, pi4RetValFsBfdClrGblStats);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdClrGblStats
 Input       :  The Indices

                The Object 
                setValFsBfdClrGblStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdClrGblStats (INT4 i4SetValFsBfdClrGblStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdClrGblStats (0, i4SetValFsBfdClrGblStats);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdClrGblStats
 Input       :  The Indices

                The Object 
                testValFsBfdClrGblStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdClrGblStats (UINT4 *pu4ErrorCode, INT4 i4TestValFsBfdClrGblStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdClrGblStats (pu4ErrorCode, 0,
                                     i4TestValFsBfdClrGblStats);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdClrGblStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdClrGblStats (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdClrAllSessStats
 Input       :  The Indices

                The Object 
                retValFsBfdClrAllSessStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdClrAllSessStats (INT4 *pi4RetValFsBfdClrAllSessStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIBfdClrAllSessStats (0, pi4RetValFsBfdClrAllSessStats);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdClrAllSessStats
 Input       :  The Indices

                The Object 
                setValFsBfdClrAllSessStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdClrAllSessStats (INT4 i4SetValFsBfdClrAllSessStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdClrAllSessStats (0, i4SetValFsBfdClrAllSessStats);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdClrAllSessStats
 Input       :  The Indices

                The Object 
                testValFsBfdClrAllSessStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdClrAllSessStats (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsBfdClrAllSessStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdClrAllSessStats (pu4ErrorCode, 0,
                                         i4TestValFsBfdClrAllSessStats);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdClrAllSessStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdClrAllSessStats (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsBfdSessionTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsBfdSessionTable (UINT4 u4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhValidateIndexInstanceFsMIBfdSessionTable (0, u4BfdSessIndex);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsBfdSessionTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsBfdSessionTable (UINT4 *pu4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIBfdSessionTable (&u4FirstContextId,
                                             pu4BfdSessIndex);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsBfdSessionTable
 Input       :  The Indices
                BfdSessIndex
                nextBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsBfdSessionTable (UINT4 u4BfdSessIndex,
                                  UINT4 *pu4NextBfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIBfdSessionTable (0, &u4NextContextId, u4BfdSessIndex,
                                            pu4NextBfdSessIndex);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdSessRole
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessRole (UINT4 u4BfdSessIndex, INT4 *pi4RetValFsBfdSessRole)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessRole (0, u4BfdSessIndex, pi4RetValFsBfdSessRole);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessMode (UINT4 u4BfdSessIndex, INT4 *pi4RetValFsBfdSessMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessMode (0, u4BfdSessIndex, pi4RetValFsBfdSessMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessRemoteDiscr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessRemoteDiscr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessRemoteDiscr (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessRemoteDiscr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessRemoteDiscr (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessRemoteDiscr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessEXPValue
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessEXPValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessEXPValue (UINT4 u4BfdSessIndex,
                         UINT4 *pu4RetValFsBfdSessEXPValue)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessEXPValue (0, u4BfdSessIndex,
                                   pu4RetValFsBfdSessEXPValue);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessTmrNegotiate
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessTmrNegotiate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessTmrNegotiate (UINT4 u4BfdSessIndex,
                             INT4 *pi4RetValFsBfdSessTmrNegotiate)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessTmrNegotiate (0, u4BfdSessIndex,
                                       pi4RetValFsBfdSessTmrNegotiate);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessOffld
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessOffld
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessOffld (UINT4 u4BfdSessIndex, INT4 *pi4RetValFsBfdSessOffld)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessOffld (0, u4BfdSessIndex, pi4RetValFsBfdSessOffld);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessEncapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessEncapType (UINT4 u4BfdSessIndex,
                          INT4 *pi4RetValFsBfdSessEncapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessEncapType (0, u4BfdSessIndex,
                                    pi4RetValFsBfdSessEncapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessAdminCtrlReq
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessAdminCtrlReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessAdminCtrlReq (UINT4 u4BfdSessIndex,
                             INT4 *pi4RetValFsBfdSessAdminCtrlReq)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessAdminCtrlReq (0, u4BfdSessIndex,
                                       pi4RetValFsBfdSessAdminCtrlReq);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessAdminCtrlErrReason
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessAdminCtrlErrReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessAdminCtrlErrReason (UINT4 u4BfdSessIndex,
                                   INT4 *pi4RetValFsBfdSessAdminCtrlErrReason)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessAdminCtrlErrReason (0, u4BfdSessIndex,
                                             pi4RetValFsBfdSessAdminCtrlErrReason);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessMapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessMapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessMapType (UINT4 u4BfdSessIndex, INT4 *pi4RetValFsBfdSessMapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessMapType (0, u4BfdSessIndex, pi4RetValFsBfdSessMapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessMapPointer
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessMapPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessMapPointer (UINT4 u4BfdSessIndex,
                           tSNMP_OID_TYPE * pRetValFsBfdSessMapPointer)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessMapPointer (0, u4BfdSessIndex,
                                     pRetValFsBfdSessMapPointer);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessCardNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessCardNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessCardNumber (UINT4 u4BfdSessIndex,
                           UINT4 *pu4RetValFsBfdSessCardNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsMIBfdSessCardNumber (0, u4BfdSessIndex,
                                     pu4RetValFsBfdSessCardNumber);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessSlotNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessSlotNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessSlotNumber (UINT4 u4BfdSessIndex,
                           UINT4 *pu4RetValFsBfdSessSlotNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsMIBfdSessSlotNumber (0, u4BfdSessIndex,
                                     pu4RetValFsBfdSessSlotNumber);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessRegisteredClients
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessRegisteredClients
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessRegisteredClients (UINT4 u4BfdSessIndex,
                                  UINT4 *pu4RetValFsBfdSessRegisteredClients)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsMIBfdSessRegisteredClients (0, u4BfdSessIndex,
                                            pu4RetValFsBfdSessRegisteredClients);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdSessRole
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessRole (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdSessRole)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdSessRole (0, u4BfdSessIndex, i4SetValFsBfdSessRole);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessMode (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdSessMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIBfdSessMode (0, u4BfdSessIndex, i4SetValFsBfdSessMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessRemoteDiscr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessRemoteDiscr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessRemoteDiscr (UINT4 u4BfdSessIndex,
                            UINT4 u4SetValFsBfdSessRemoteDiscr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessRemoteDiscr (0, u4BfdSessIndex,
                                      u4SetValFsBfdSessRemoteDiscr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessEXPValue
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessEXPValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessEXPValue (UINT4 u4BfdSessIndex, UINT4 u4SetValFsBfdSessEXPValue)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessEXPValue (0, u4BfdSessIndex,
                                   u4SetValFsBfdSessEXPValue);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessTmrNegotiate
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessTmrNegotiate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessTmrNegotiate (UINT4 u4BfdSessIndex,
                             INT4 i4SetValFsBfdSessTmrNegotiate)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessTmrNegotiate (0, u4BfdSessIndex,
                                       i4SetValFsBfdSessTmrNegotiate);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessOffld
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessOffld
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessOffld (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdSessOffld)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessOffld (0, u4BfdSessIndex, i4SetValFsBfdSessOffld);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessEncapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessEncapType (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdSessEncapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessEncapType (0, u4BfdSessIndex,
                                    i4SetValFsBfdSessEncapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessMapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessMapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessMapType (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdSessMapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessMapType (0, u4BfdSessIndex, i4SetValFsBfdSessMapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessMapPointer
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessMapPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessMapPointer (UINT4 u4BfdSessIndex,
                           tSNMP_OID_TYPE * pSetValFsBfdSessMapPointer)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessMapPointer (0, u4BfdSessIndex,
                                     pSetValFsBfdSessMapPointer);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessCardNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessCardNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessCardNumber (UINT4 u4BfdSessIndex,
                           UINT4 u4SetValFsBfdSessCardNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessCardNumber (0, u4BfdSessIndex,
                                     u4SetValFsBfdSessCardNumber);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetFsBfdSessSlotNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdSessSlotNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdSessSlotNumber (UINT4 u4BfdSessIndex,
                           UINT4 u4SetValFsBfdSessSlotNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdSessSlotNumber (0, u4BfdSessIndex,
                                     u4SetValFsBfdSessSlotNumber);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessRole
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessRole (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                        INT4 i4TestValFsBfdSessRole)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessRole (pu4ErrorCode, 0, u4BfdSessIndex,
                                  i4TestValFsBfdSessRole);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessMode (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                        INT4 i4TestValFsBfdSessMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessMode (pu4ErrorCode, 0, u4BfdSessIndex,
                                  i4TestValFsBfdSessMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessRemoteDiscr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessRemoteDiscr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessRemoteDiscr (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                               UINT4 u4TestValFsBfdSessRemoteDiscr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessRemoteDiscr (pu4ErrorCode, 0, u4BfdSessIndex,
                                         u4TestValFsBfdSessRemoteDiscr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessEXPValue
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessEXPValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessEXPValue (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                            UINT4 u4TestValFsBfdSessEXPValue)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessEXPValue (pu4ErrorCode, 0, u4BfdSessIndex,
                                      u4TestValFsBfdSessEXPValue);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessTmrNegotiate
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessTmrNegotiate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessTmrNegotiate (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                INT4 i4TestValFsBfdSessTmrNegotiate)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessTmrNegotiate (pu4ErrorCode, 0, u4BfdSessIndex,
                                          i4TestValFsBfdSessTmrNegotiate);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessOffld
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessOffld
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessOffld (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                         INT4 i4TestValFsBfdSessOffld)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessOffld (pu4ErrorCode, 0, u4BfdSessIndex,
                                   i4TestValFsBfdSessOffld);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessEncapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessEncapType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                             INT4 i4TestValFsBfdSessEncapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessEncapType (pu4ErrorCode, 0, u4BfdSessIndex,
                                       i4TestValFsBfdSessEncapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessMapType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessMapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessMapType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                           INT4 i4TestValFsBfdSessMapType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessMapType (pu4ErrorCode, 0, u4BfdSessIndex,
                                     i4TestValFsBfdSessMapType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessMapPointer
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessMapPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessMapPointer (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                              tSNMP_OID_TYPE * pTestValFsBfdSessMapPointer)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessMapPointer (pu4ErrorCode, 0, u4BfdSessIndex,
                                        pTestValFsBfdSessMapPointer);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessCardNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessCardNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessCardNumber (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                              UINT4 u4TestValFsBfdSessCardNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessCardNumber (pu4ErrorCode, 0, u4BfdSessIndex,
                                        u4TestValFsBfdSessCardNumber);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsBfdSessSlotNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdSessSlotNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdSessSlotNumber (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                              UINT4 u4TestValFsBfdSessSlotNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdSessSlotNumber (pu4ErrorCode, 0, u4BfdSessIndex,
                                        u4TestValFsBfdSessSlotNumber);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdSessionTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdSessionTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsBfdSessPerfTable (UINT4 u4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhValidateIndexInstanceFsMIBfdSessPerfTable (0, u4BfdSessIndex);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsBfdSessPerfTable (UINT4 *pu4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIBfdSessPerfTable (&u4FirstContextId,
                                              pu4BfdSessIndex);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
                nextBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsBfdSessPerfTable (UINT4 u4BfdSessIndex,
                                   UINT4 *pu4NextBfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIBfdSessPerfTable (0, &u4NextContextId,
                                             u4BfdSessIndex,
                                             pu4NextBfdSessIndex);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBfdSessPerfCCPktIn
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessPerfCCPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessPerfCCPktIn (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessPerfCCPktIn)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessPerfCCPktIn (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessPerfCCPktIn);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessPerfCCPktOut
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessPerfCCPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessPerfCCPktOut (UINT4 u4BfdSessIndex,
                             UINT4 *pu4RetValFsBfdSessPerfCCPktOut)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessPerfCCPktOut (0, u4BfdSessIndex,
                                       pu4RetValFsBfdSessPerfCCPktOut);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessPerfCVPktIn
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessPerfCVPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessPerfCVPktIn (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessPerfCVPktIn)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessPerfCVPktIn (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessPerfCVPktIn);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessPerfCVPktOut
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessPerfCVPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessPerfCVPktOut (UINT4 u4BfdSessIndex,
                             UINT4 *pu4RetValFsBfdSessPerfCVPktOut)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessPerfCVPktOut (0, u4BfdSessIndex,
                                       pu4RetValFsBfdSessPerfCVPktOut);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessMisDefCount
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessMisDefCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessMisDefCount (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessMisDefCount)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessMisDefCount (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessMisDefCount);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessLocDefCount
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessLocDefCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessLocDefCount (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessLocDefCount)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessLocDefCount (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessLocDefCount);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessRdiInCount
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessRdiInCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessRdiInCount (UINT4 u4BfdSessIndex,
                           UINT4 *pu4RetValFsBfdSessRdiInCount)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessRdiInCount (0, u4BfdSessIndex,
                                     pu4RetValFsBfdSessRdiInCount);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdSessRdiOutCount
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdSessRdiOutCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdSessRdiOutCount (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValFsBfdSessRdiOutCount)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdSessRdiOutCount (0, u4BfdSessIndex,
                                      pu4RetValFsBfdSessRdiOutCount);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFsBfdClearStats
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValFsBfdClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBfdClearStats (UINT4 u4BfdSessIndex, INT4 *pi4RetValFsBfdClearStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIBfdClearStats (0, u4BfdSessIndex, pi4RetValFsBfdClearStats);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBfdClearStats
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValFsBfdClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBfdClearStats (UINT4 u4BfdSessIndex, INT4 i4SetValFsBfdClearStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIBfdClearStats (0, u4BfdSessIndex, i4SetValFsBfdClearStats);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBfdClearStats
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValFsBfdClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBfdClearStats (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                          INT4 i4TestValFsBfdClearStats)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIBfdClearStats (pu4ErrorCode, 0, u4BfdSessIndex,
                                    i4TestValFsBfdClearStats);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBfdSessPerfTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
