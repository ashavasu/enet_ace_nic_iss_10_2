/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: bfdtx.c,v 1.49 2017/06/08 11:40:29 siva Exp $
 *                                                                           *
 * Description: This file contains the BFD Packet TX related functions       *
 *****************************************************************************/
#ifndef _BFDTX_C_
#define _BFDTX_C_

#include "bfdinc.h"

PUBLIC VOID MplsUtlFillMplsAchTlvHdr PROTO ((tMplsAchTlvHdr *,
                                             tCRU_BUF_CHAIN_HEADER *));
PUBLIC VOID MplsUtlFillMplsAchTlv PROTO ((tMplsAchTlv *,
                                          tCRU_BUF_CHAIN_HEADER *));
PUBLIC VOID MplsUtlFillMplsHdr PROTO ((tMplsHdr *, tCRU_BUF_CHAIN_HEADER *));
PUBLIC VOID MplsUtlFillMplsAchHdr PROTO ((tMplsAchHdr *,
                                          tCRU_BUF_CHAIN_HEADER *));

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxPacket                                                *
 *                                                                           *
 * Description  : Transmits the BFD packet                                   *
 *                                                                           *
 * Input        : pBfdSessCtrl- Pointer to tBfdFsMIStdBfdSessEntry structure *
 *                              which contains BFD session information       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdTxPacket (tBfdFsMIStdBfdSessEntry * pBfdSessCtrl)
{
    tBfdMplsPathInfo   *pMplsPathInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DstAddr = 0;
    UINT4               u4TmpAddr = 0;
    UINT4               u4EncapType = 0;
    UINT1               au1DestMac[MAC_ADDR_LEN];
#ifdef IP6_WANTED
    tIp6Addr            Ip6SrcAddr;
    tIp6Addr            Ip6DstAddr;

    MEMSET (&Ip6SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6DstAddr, 0, sizeof (tIp6Addr));
#endif
    MEMSET (au1DestMac, 0, MAC_ADDR_LEN);

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return OSIX_SUCCESS;
    }

    u4ContextId = pBfdSessCtrl->MibObject.u4FsMIStdBfdContextId;

    /* Allocate memory for MPLS Path info */
    pMplsPathInfo = (tBfdMplsPathInfo *)
        MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
    if (pMplsPathInfo == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_TRC_MEM_ALLOC_FAIL, "BfdTxPacket");
        return OSIX_FAILURE;
    }
    MEMSET (pMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

    if (BFD_CHECK_PATH_TYPE_NOT_IP (pBfdSessCtrl))
    {
        /*if (BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING !=
           pBfdSessCtrl->MibObject.i4FsMIStdBfdSessType)
           { */
        pBfdSessCtrl->BfdSessPathParams.unSessParams.BfdSessTeParams.
            u4TunnelInst = ZERO;
        /*} */

        if (BfdExtGetMplsPathInfo (u4ContextId,
                                   &pBfdSessCtrl->BfdSessPathParams,
                                   pMplsPathInfo, OSIX_TRUE) == OSIX_FAILURE)
        {

            if (((pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
                  BFD_ENCAP_TYPE_MPLS_IP) ||
                 (pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
                  BFD_ENCAP_TYPE_MPLS_IP_ACH)) &&
                (pMplsPathInfo->bRevPathFetchFail) == OSIX_TRUE)
            {

                /* No reverse path available for the tunnel 
                 * Do IP routing. Send packet on udp socket */
                if (pBfdSessCtrl->LastRxCtrlPktParams.BfdPeerIpAddress.
                    u4_addr[0] == 0)
                {
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    return OSIX_FAILURE;
                }
                MEMCPY (&u4TmpAddr,
                        pBfdSessCtrl->MibObject.au1FsMIStdBfdSessSrcAddr,
                        BFD_IPV4_MAX_ADDR_LEN);
                u4SrcAddr = OSIX_NTOHL (u4TmpAddr);

                if ((pBfdSessCtrl->BfdSessPathParams.ePathType ==
                     BFD_PATH_TYPE_TE_IPV4) &&
                    (NetIpv4IfIsOurAddress
                     (pBfdSessCtrl->BfdSessPathParams.SessTeParams.DstIpAddr.
                      u4_addr[0]) == NETIPV4_SUCCESS))
                {
                    pBfdSessCtrl->LastRxCtrlPktParams.
                        BfdPeerIpAddress.u4_addr[0] = pBfdSessCtrl->
                        BfdSessPathParams.SessTeParams.SrcIpAddr.u4_addr[0];
                }
                if (BfdUdpTransmitBfdPkt (u4ContextId,
                                          (UINT4) pBfdSessCtrl->MibObject.
                                          i4FsMIStdBfdSessInterface,
                                          u4SrcAddr,
                                          pBfdSessCtrl->
                                          LastRxCtrlPktParams.
                                          BfdPeerIpAddress.u4_addr[0],
                                          pBfdSessCtrl->MibObject.
                                          i4FsMIBfdSessMapType,
                                          pBfdSessCtrl->BfdCntlPktBuf.
                                          u4PacketLen, NULL,
                                          (UINT1) pBfdSessCtrl->MibObject.
                                          i4FsMIStdBfdSessType, OSIX_FALSE,
                                          pBfdSessCtrl->BfdCntlPktBuf.
                                          au1PacketBuf) != OSIX_SUCCESS)
                {
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    return OSIX_FAILURE;
                }
                /* Update Out Pkt count */
                BfdUtilUpdatePktCount (&pBfdSessCtrl->
                                       MibObject.u4FsMIStdBfdSessPerfCtrlPktOut,
                                       &pBfdSessCtrl->
                                       MibObject.
                                       u8FsMIStdBfdSessPerfCtrlPktOutHC);
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);

                return OSIX_SUCCESS;
            }
            else
            {
                /* Failed to get MPLS Path information by giving path params */
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
        }

        /* Util the Session is Up, the Tunnel Instance is not properly updated        *
         * So, once the session is Up, updating the tunnel instance with proper value */
        if ((pBfdSessCtrl->BfdSessPathParams.ePathType == BFD_PATH_TYPE_TE_IPV4)
            && (pMplsPathInfo->u1PathStatus == BFD_MPLS_PATH_UP))
        {
            pBfdSessCtrl->BfdSessPathParams.unSessParams.BfdSessTeParams.
                u4TunnelInst =
                pMplsPathInfo->MplsTeTnlApiInfo.TnlLspId.u4LspNum;
        }

        if (pMplsPathInfo->u1PathStatus == BFD_MPLS_PATH_DOWN)
        {
            /* Path Status down */
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }
        /* Allocate memory */
        pBuf = CRU_BUF_Allocate_MsgBufChain (BFD_MAX_PKT_LEN, 0);
        if (pBuf == NULL)
        {
            /* CRU BUF Alloaction Failed */
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }
        if (BfdTxConstructPacket (pBfdSessCtrl, pMplsPathInfo, &u4IfIndex,
                                  au1DestMac, pBuf,
                                  &u4EncapType) == OSIX_FAILURE)
        {
            /* Failed to construct BFD control packet */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }

        /* Send the packet to MPLS-RTR to forward */
        if (BfdExtSendPktToMpls (u4ContextId, u4IfIndex, au1DestMac, pBuf)
            == OSIX_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (pBfdSessCtrl->MibObject.i4FsMIStdBfdSessSrcAddrLen == 0)
        {
            if (BfdUtilGetSrcAddrFromIfIndex
                ((UINT4) pBfdSessCtrl->MibObject.
                 i4FsMIStdBfdSessInterface,
                 (UINT1) pBfdSessCtrl->MibObject.
                 i4FsMIStdBfdSessDstAddrType,
                 pBfdSessCtrl->MibObject.
                 au1FsMIStdBfdSessSrcAddr,
                 pBfdSessCtrl->MibObject.
                 au1FsMIStdBfdSessDstAddr) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
            pBfdSessCtrl->MibObject.i4FsMIStdBfdSessSrcAddrLen =
                pBfdSessCtrl->MibObject.i4FsMIStdBfdSessDstAddrLen;

            pBfdSessCtrl->MibObject.i4FsMIStdBfdSessSrcAddrType =
                pBfdSessCtrl->MibObject.i4FsMIStdBfdSessDstAddrType;
        }

        if (pBfdSessCtrl->MibObject.i4FsMIBfdSessMapType == BFD_PATH_TYPE_IPV4)
        {
            MEMCPY (&u4TmpAddr,
                    pBfdSessCtrl->MibObject.au1FsMIStdBfdSessDstAddr,
                    BFD_IPV4_MAX_ADDR_LEN);
            u4DstAddr = OSIX_NTOHL (u4TmpAddr);
            MEMCPY (&u4TmpAddr,
                    pBfdSessCtrl->MibObject.au1FsMIStdBfdSessSrcAddr,
                    BFD_IPV4_MAX_ADDR_LEN);
            u4SrcAddr = OSIX_NTOHL (u4TmpAddr);

            if (BfdUdpTransmitBfdPkt (u4ContextId,
                                      (UINT4) pBfdSessCtrl->MibObject.
                                      i4FsMIStdBfdSessInterface,
                                      u4SrcAddr, u4DstAddr,
                                      pBfdSessCtrl->MibObject.
                                      i4FsMIBfdSessMapType,
                                      pBfdSessCtrl->BfdCntlPktBuf.u4PacketLen,
                                      NULL, (UINT1) pBfdSessCtrl->MibObject.
                                      i4FsMIStdBfdSessType, OSIX_FALSE,
                                      pBfdSessCtrl->BfdCntlPktBuf.
                                      au1PacketBuf) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
        }
#ifdef IP6_WANTED
        else if (pBfdSessCtrl->MibObject.i4FsMIBfdSessMapType
                 == BFD_PATH_TYPE_IPV6)
        {
            MEMCPY (&Ip6SrcAddr,
                    pBfdSessCtrl->MibObject.au1FsMIStdBfdSessSrcAddr,
                    sizeof (Ip6SrcAddr));
            MEMCPY (&Ip6DstAddr,
                    pBfdSessCtrl->MibObject.au1FsMIStdBfdSessDstAddr,
                    sizeof (Ip6DstAddr));
            if (BfdUdpv6TransmitBfdPkt (u4ContextId,
                                        (UINT4) pBfdSessCtrl->MibObject.
                                        i4FsMIStdBfdSessInterface,
                                        &Ip6SrcAddr, &Ip6DstAddr,
                                        pBfdSessCtrl->MibObject.
                                        i4FsMIBfdSessMapType,
                                        pBfdSessCtrl->BfdCntlPktBuf.u4PacketLen,
                                        NULL, (UINT1) pBfdSessCtrl->MibObject.
                                        i4FsMIStdBfdSessType, OSIX_FALSE,
                                        pBfdSessCtrl->BfdCntlPktBuf.
                                        au1PacketBuf) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
            }
        }
#endif
    }
    /* Update Out Pkt count */

    BfdUtilUpdatePktCount (&pBfdSessCtrl->
                           MibObject.u4FsMIStdBfdSessPerfCtrlPktOut,
                           &pBfdSessCtrl->
                           MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC);

    if (pBfdSessCtrl->MibObject.i4FsMIBfdSessMode == BFD_SESS_MODE_CCV)
    {
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIBfdSessPerfCVPktOut, NULL);
    }
    else
    {
        BfdUtilUpdatePktCount (&pBfdSessCtrl->
                               MibObject.u4FsMIBfdSessPerfCCPktOut, NULL);
    }
    if (pBfdSessCtrl->BfdSessPathParams.ePathType == BFD_PATH_TYPE_MEP)
    {
        if (BFD_SESS_DIAG (pBfdSessCtrl) == BFD_DIAG_CTRL_DETECTION_TIME_EXP ||
            BFD_SESS_DIAG (pBfdSessCtrl) ==
            BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN)
        {
            /* This is a RDI Packet */
            BfdUtilUpdatePktCount (&pBfdSessCtrl->
                                   MibObject.u4FsMIBfdSessRdiOutCount, NULL);
        }
    }

    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID, (UINT1 *) pMplsPathInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxConstructPacket                                       *
 *                                                                           *
 * Description  : Forms the BFD Control packet                               *
 *                                                                           *
 * Input        : pBfdSessCtrl- Pointer to the BFD session table             *
 *                pPathInfo   - Pointer to the entire MPLS path info         *
 *                                                                           *
 * Output       : pu4IfIndex  - Pointer to the interface index               *
 *                pBuf        - Pointer to CRU Buf which contains constructed*
 *                              BFD packet                                   *
 *                pu4EncapType - Returns the encap type filled so that       *
 *                               offload module can use the same encap       *
 *                                                                           *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdTxConstructPacket (tBfdFsMIStdBfdSessEntry * pBfdSessCtrl,
                      tBfdMplsPathInfo * pMplsPathInfo, UINT4 *pu4IfIndex,
                      UINT1 *pu1DestMac, tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT4 *pu4EncapType)
{
    tMplsHdr            MplsHdr;
    tMplsAchTlvHdr      AchTlvHdr;
    tMplsLblInfo        MplsLblInfo[MPLS_MAX_LABEL_STACK];
    tMplsAchTlv         MplsAchTlv;
    tBfdMibFsMIStdBfdSessEntry *pBfdSessTbl = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1LblCnt = 0;
    UINT1               u1Count = 0;
    BOOL1               bIsGalLbl = OSIX_FALSE;
    BOOL1               bIsPwLbl = OSIX_FALSE;

    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));
    MEMSET (&MplsAchTlv, 0, sizeof (tMplsAchTlv));
    MEMSET (&AchTlvHdr, 0, sizeof (tMplsAchTlvHdr));

    pBfdSessTbl = &pBfdSessCtrl->MibObject;
    u4ContextId = pBfdSessTbl->u4FsMIStdBfdContextId;

    if (CRU_BUF_Copy_OverBufChain (pBuf,
                                   pBfdSessCtrl->BfdCntlPktBuf.au1PacketBuf,
                                   BFD_CTRL_PKT_OFFSET,
                                   pBfdSessCtrl->BfdCntlPktBuf.u4PacketLen)
        == CRU_FAILURE)
    {
        /* Failed to Copy BFD Control Packet to CRU Buf */
        return OSIX_FAILURE;
    }

    CRU_BUF_Move_ValidOffset (pBuf, BFD_CTRL_PKT_OFFSET);

    if (pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
        BFD_ENCAP_TYPE_VCCV_NEGOTIATED)
    {
        /* If EncapType is configured as vccvNegotiated, the monitoring path 
         * should be PW. This check should be done while configuring
         * EncapType */
        /* Get the encap type based on configured CV value in VCCV */
        if (((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
              L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY)
             == L2VPN_VCCV_CV_BFD_IP_FAULT_ONLY) ||
            ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
              L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS) ==
             L2VPN_VCCV_CV_BFD_IP_FAULT_STATUS))
        {
            if (((pMplsPathInfo->MplsPwApiInfo.u1IpVersion ==
                  BFD_HEADER_IPV4) &&
                 (pMplsPathInfo->MplsPwApiInfo.u1CcSelected ==
                  L2VPN_VCCV_CC_ACH)) ||
                (pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_ME_AVAIL)
                == BFD_CHECK_PATH_ME_AVAIL)
            {
                *pu4EncapType = BFD_ENCAP_MPLS_IPV4_ACH;
            }
            /* IPv6 - Add Encap type check for IPv6 
             * BFD_ENCAP_MPLS_IPV6_ACH */

            else if (pMplsPathInfo->MplsPwApiInfo.u1IpVersion ==
                     BFD_HEADER_IPV4)
            {
                *pu4EncapType = BFD_ENCAP_MPLS_IPV4;
            }
            /* IPv6 - Add Encap type check for IPv6 
             * BFD_ENCAP_MPLS_IPV6 */
        }
        else
        {
            *pu4EncapType = BFD_ENCAP_MPLS_ACH;
        }
    }
    else if (pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_MPLS_IP)
    {
        *pu4EncapType = BFD_ENCAP_MPLS_IPV4;
    }
    else if (pBfdSessCtrl->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_MPLS_IP_ACH)
    {
        *pu4EncapType = BFD_ENCAP_MPLS_IPV4_ACH;
    }
    else
    {
        *pu4EncapType = BFD_ENCAP_MPLS_ACH;
    }

    if ((*pu4EncapType == BFD_ENCAP_MPLS_IPV4) ||
        (*pu4EncapType == BFD_ENCAP_MPLS_IPV4_ACH))
    {
        /* Form and Fill IP/UDP Header */
        if (BfdTxFillIpUdpHdr (u4ContextId, BFD_HEADER_IPV4,
                               pBuf) == OSIX_FAILURE)
        {
            /* Filling IP/UDP Header failed */
            BFD_LOG (u4ContextId, BFD_TRC_FILL_IP_UDP_HEADER_FAILED);
            return OSIX_FAILURE;
        }
    }
    else if ((*pu4EncapType == BFD_ENCAP_MPLS_IPV6) ||
             (*pu4EncapType == BFD_ENCAP_MPLS_IPV6_ACH))
    {
        /* Form and Fill IP/UDP Header */
        if (BfdTxFillIpUdpHdr (u4ContextId, BFD_HEADER_IPV6,
                               pBuf) == OSIX_FAILURE)
        {
            /* Filling IP/UDP Header failed */
            BFD_LOG (u4ContextId, BFD_TRC_FILL_IP_UDP_HEADER_FAILED);
            return OSIX_FAILURE;
        }
    }

    if (((pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_ME_AVAIL) ==
         BFD_CHECK_PATH_ME_AVAIL) && (*pu4EncapType != BFD_ENCAP_MPLS_IPV4))
    {
        if (pBfdSessTbl->i4FsMIBfdSessMode == BFD_SESS_MODE_CCV)
        {
            /* Fill Source MEP TLV */
            BfdTxFillSrcMepTlv (pMplsPathInfo, &MplsAchTlv, &AchTlvHdr);
            MplsUtlFillMplsAchTlv (&MplsAchTlv, pBuf);
            MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, pBuf);
        }
        if ((pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) !=
            BFD_CHECK_PATH_PW_AVAIL)
        {
            /* Fill Ach Header */
            if (BfdTxFillAchHdr (pBuf, *pu4EncapType, (UINT4) pBfdSessTbl->
                                 i4FsMIBfdSessMode) == OSIX_FAILURE)
            {
                /* Filling Ach Header failed */
                BFD_LOG (u4ContextId, BFD_TRC_FILL_ACH_HEADER_FAILED);
                return OSIX_FAILURE;
            }
            /* Fill GAL, if path is not PW */
            bIsGalLbl = OSIX_TRUE;
            MplsHdr.u4Lbl = BFD_GAL_LABLE;
            MplsHdr.Ttl = 1;
            MplsHdr.Exp = 0;
            MplsHdr.SI = 1;
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        }
        else if ((pMplsPathInfo->MplsPwApiInfo.u1CcSelected !=
                  L2VPN_VCCV_CC_ACH) &&
                 ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
                   L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) !=
                  L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) &&
                 ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
                   L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS) !=
                  L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS))
        {
            /* Fill ACH Header */
            if (BfdTxFillAchHdr (pBuf, *pu4EncapType, (UINT4) pBfdSessTbl->
                                 i4FsMIBfdSessMode) == OSIX_FAILURE)
            {
                /* Filling Ach Header failed */
                BFD_LOG (u4ContextId, BFD_TRC_FILL_ACH_HEADER_FAILED);
                return OSIX_FAILURE;
            }
        }
    }
    if ((pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
        BFD_CHECK_PATH_PW_AVAIL)
    {
        /* Check whether BFD is enabled on VCCV */
        if ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
             (BFD_CV_IP_UDP_ENCAP | BFD_CV_IP_UDP_ENCAP_STATUS_SIG |
              BFD_CV_ACH_ENCAP | BFD_CV_ACH_ENCAP_STATUS_SIG)) == 0)
        {
            /* VCCV CV Verification failed */
            BFD_LOG (u4ContextId, BFD_TRC_VCCV_CV_VERI_FAILED);
            return OSIX_FAILURE;
        }

        *pu4IfIndex = pMplsPathInfo->MplsPwApiInfo.PwOnlyIfIndex;
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsPwApiInfo.au1NextHopMac,
                MAC_ADDR_LEN);
        if ((pMplsPathInfo->MplsPwApiInfo.u1CcSelected ==
             L2VPN_VCCV_CC_ACH) ||
            ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
              L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) ==
             L2VPN_VCCV_CV_BFD_ACH_FAULT_ONLY) ||
            ((pMplsPathInfo->MplsPwApiInfo.u1CvSelected &
              L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS) ==
             L2VPN_VCCV_CV_BFD_ACH_FAULT_STATUS))
        {
            /* Selected CC is Ach */
            /* u4EncapType should be BFD_ENCAP_MPLS_IPV4_ACH, 
             * BFD_ENCAP_MPLS_IPV4_ACH or BFD_ENCAP_MPLS_ACH */
            if (BfdTxFillAchHdr (pBuf, *pu4EncapType, (UINT4) pBfdSessTbl->
                                 i4FsMIBfdSessMode) == OSIX_FAILURE)
            {
                /* Filling Ach Header failed */
                BFD_LOG (u4ContextId, BFD_TRC_FILL_ACH_HEADER_FAILED);
                return OSIX_FAILURE;
            }
        }
        /* Fill PW lable */
        MplsHdr.u4Lbl = pMplsPathInfo->MplsPwApiInfo.u4OutVcLabel;
        if (pMplsPathInfo->MplsPwApiInfo.u1CcSelected == L2VPN_VCCV_CC_TTL_EXP)
        {
            MplsHdr.Ttl = 1;
        }
        else
        {
            MplsHdr.Ttl = pMplsPathInfo->MplsPwApiInfo.u1Ttl;
        }
        MplsHdr.Exp = pBfdSessTbl->u4FsMIBfdSessEXPValue;
        MplsHdr.SI = 1;
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        bIsPwLbl = OSIX_TRUE;
        /* Check whether selected CC on VCCV is RA. If CC is RA,
         * put RA lable */
        if (pMplsPathInfo->MplsPwApiInfo.u1CcSelected == L2VPN_VCCV_CC_RAL)
        {
            MplsHdr.u4Lbl = 1;
            MplsHdr.Ttl = 1;
            MplsHdr.Exp = 0;
            MplsHdr.SI = 0;
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        }
    }
    if ((pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) ==
        BFD_CHECK_PATH_TE_AVAIL)
    {
        *pu4IfIndex =
            pMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.u4OutIf;
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsTeTnlApiInfo.au1NextHopMac,
                MAC_ADDR_LEN);
        /* Fill Tunnel Label */
        u1LblCnt = pMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
            FwdOutSegInfo.u1LblStkCnt;
        if (u1LblCnt <= MPLS_MAX_LABEL_STACK)
        {
            MEMCPY (MplsLblInfo,
                    pMplsPathInfo->
                    MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.LblInfo,
                    (sizeof (tMplsLblInfo) * u1LblCnt));
        }
        else
        {
            /* Exceeding max label stack depth. BFD supports max label 
             * stack depth : MPLS_MAX_LABEL_STACK */
            BFD_LOG (u4ContextId, BFD_TRC_MAX_LBL_STACK, MPLS_MAX_LABEL_STACK);
            return OSIX_FAILURE;
        }
        while (u1Count < u1LblCnt)
        {
            MplsHdr.u4Lbl = MplsLblInfo[u1Count].u4Label;
            MplsHdr.Ttl = MplsLblInfo[u1Count].u1Ttl;
            if ((u1Count + 1) == u1LblCnt)
            {
                MplsHdr.Exp = pBfdSessTbl->u4FsMIBfdSessEXPValue;
            }
            else
            {
                MplsHdr.Exp = MplsLblInfo[u1Count].u1Exp;
            }
            if ((bIsPwLbl == OSIX_TRUE) || (bIsGalLbl == OSIX_TRUE))
            {
                MplsHdr.SI = 0;
            }
            else
            {
                MplsHdr.SI = MplsLblInfo[u1Count].u1SI;
            }
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
            u1Count++;
        };
    }
    else if ((pMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_NONTE_AVAIL) ==
             BFD_CHECK_PATH_NONTE_AVAIL)
    {
        *pu4IfIndex = pMplsPathInfo->
            MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.u4OutIf;
        MEMCPY (pu1DestMac, pMplsPathInfo->MplsNonTeApiInfo.
                XcApiInfo.FwdOutSegInfo.au1NextHopMac, MAC_ADDR_LEN);
        /* Fill Non-te path Label */
        u1LblCnt = pMplsPathInfo->
            MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.u1LblStkCnt;
        if (u1LblCnt <= MPLS_MAX_LABEL_STACK)
        {
            MEMCPY (MplsLblInfo,
                    pMplsPathInfo->
                    MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.LblInfo,
                    (sizeof (tMplsLblInfo) * u1LblCnt));
        }
        else
        {
            /* Exceeding max label stack depth. BFD supports max label 
             * stack depth : MPLS_MAX_LABEL_STACK */
            BFD_LOG (u4ContextId, BFD_TRC_MAX_LBL_STACK, MPLS_MAX_LABEL_STACK);
            return OSIX_FAILURE;
        }
        while (u1Count < u1LblCnt)
        {
            MplsHdr.u4Lbl = MplsLblInfo[u1Count].u4Label;
            MplsHdr.Ttl = MplsLblInfo[u1Count].u1Ttl;
            if ((u1Count + 1) == u1LblCnt)
            {
                MplsHdr.Exp = pBfdSessTbl->u4FsMIBfdSessEXPValue;
            }
            else
            {
                MplsHdr.Exp = MplsLblInfo[u1Count].u1Exp;
            }
            if ((bIsPwLbl == OSIX_TRUE) || (bIsGalLbl == OSIX_TRUE))
            {
                MplsHdr.SI = 0;
            }
            else
            {
                MplsHdr.SI = MplsLblInfo[u1Count].u1SI;
            }
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
            u1Count++;
        };
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxFormBfdCtrlPkt                                        *
 *                                                                           *
 * Description  : Constructs the BFD control packet                          *
 *                                                                           *
 * Input        : pBfdSessCtrl - Pointer to the tBfdFsMIStdBfdSessEntry which*
 *                               contains BFD information to form BFD packet *
 *                bIsPoll - Indicates if poll bit is to be set               *
 *                bIsFinal - Indicates if final bit is to be set             *
 *                                                                           *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdTxFormBfdCtrlPkt (tBfdFsMIStdBfdSessEntry * pBfdSessCtrl, BOOL1 bIsPoll,
                     BOOL1 bIsFinal)
{
    UINT4               u4Val = 0;
    UINT4               u4Length = 0;
    UINT1              *pu1Buf = NULL;
    tBfdMibFsMIStdBfdSessEntry *pBfdSessTbl = NULL;

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }

    pu1Buf = pBfdSessCtrl->BfdCntlPktBuf.au1PacketBuf;
    pBfdSessTbl = &pBfdSessCtrl->MibObject;

    u4Val = ((pBfdSessTbl->u4FsMIStdBfdSessVersionNumber <<
              BFD_SESS_VERSION_NUM_OFFSET_BITS) & 0xe0000000) |
        ((pBfdSessTbl->i4FsMIStdBfdSessDiag <<
          BFD_SESS_DIAG_OFFSET_BITS) & 0x1f000000) |
        ((pBfdSessTbl->i4FsMIStdBfdSessState <<
          BFD_SESS_STATE_OFFSET_BITS) & 0x00c00000);
    if (bIsPoll == OSIX_TRUE)
    {
        u4Val |= BFD_POLL_BIT_LOCATION;
    }
    if (bIsFinal == OSIX_TRUE)
    {
        u4Val |= BFD_FINAL_BIT_LOCATION;
    }
    if (pBfdSessTbl->i4FsMIStdBfdSessControlPlaneIndepFlag == BFD_SNMP_TRUE)
    {
        u4Val |= BFD_CPINDE_BIT_LOCATION;
    }
    if (pBfdSessTbl->i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        u4Val |= BFD_AUTH_BIT_LOCATION;
    }
    if (pBfdSessTbl->i4FsMIStdBfdSessDemandModeDesiredFlag == BFD_SNMP_TRUE)
    {
        u4Val |= BFD_DEMAND_BIT_LOCATION;
    }
    if (pBfdSessTbl->i4FsMIStdBfdSessMultipointFlag == BFD_SNMP_TRUE)
    {
        u4Val |= BFD_MULTIPOINT_BIT_LOCATION;
    }
    u4Val |= ((pBfdSessTbl->u4FsMIStdBfdSessDetectMult <<
               BFD_SESS_DETECT_MULTIPLIER_OFFSET_BITS) & 0x0000ff00);
    u4Length = BFD_CTRL_PKT_LEN;
    if (pBfdSessTbl->i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        /* Length : AuthType (1), AuthLen(1) and Len of Key */
        u4Length += pBfdSessTbl->i4FsMIStdBfdSessAuthenticationKeyLen +
            BFD_AUTH_TLID_LEN;
    }
    u4Val |= (u4Length & 0x000000ff);

    BFD_PUT_4BYTE (pu1Buf, u4Val);

    BFD_PUT_4BYTE (pu1Buf, pBfdSessTbl->u4FsMIStdBfdSessDiscriminator);
    BFD_PUT_4BYTE (pu1Buf, pBfdSessTbl->u4FsMIBfdSessRemoteDiscr);
    BFD_PUT_4BYTE (pu1Buf, pBfdSessTbl->u4FsMIStdBfdSessDesiredMinTxInterval);
    BFD_PUT_4BYTE (pu1Buf, pBfdSessTbl->u4FsMIStdBfdSessReqMinRxInterval);
    BFD_PUT_4BYTE (pu1Buf, pBfdSessTbl->u4FsMIStdBfdSessReqMinEchoRxInterval);

    if (pBfdSessTbl->i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE)
    {
        BFD_PUT_1BYTE (pu1Buf, (UINT1) pBfdSessTbl->
                       i4FsMIStdBfdSessAuthenticationType);
        BFD_PUT_1BYTE (pu1Buf, (UINT1) pBfdSessTbl->
                       i4FsMIStdBfdSessAuthenticationKeyLen +
                       BFD_AUTH_TLID_LEN);
        BFD_PUT_1BYTE (pu1Buf, (UINT1) pBfdSessTbl->
                       i4FsMIStdBfdSessAuthenticationKeyID);
        BFD_PUT_NBYTE (pu1Buf, pBfdSessTbl->
                       au1FsMIStdBfdSessAuthenticationKey,
                       (UINT4) pBfdSessTbl->
                       i4FsMIStdBfdSessAuthenticationKeyLen);
    }
    pBfdSessCtrl->BfdCntlPktBuf.u4PacketLen = u4Length;
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxFillAchHdr                                            *
 *                                                                           *
 * Description  : Constructs the ACH Header and sets the Channel Type in     *
 *                header based on encap type and configured operation mode   *
 *                                                                           *
 * Input        : u4EncapType - Encapsulation type used to TX the BFD packet *
 *                u4SessMode - Configured operation mode (CC/CV)             *
 *                                                                           *
 * Output       : pBuf - Pointer to the CRU buffer in which headers to be    *
 *                       prepended                                           *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdTxFillAchHdr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4EncapType,
                 UINT4 u4SessMode)
{
    tMplsAchHdr         MplsAchHdr;

    MEMSET (&MplsAchHdr, 0, sizeof (tMplsAchHdr));

    MplsAchHdr.u4AchType = 1;
    MplsAchHdr.u1Version = 0;
    MplsAchHdr.u1Rsvd = 0;
    /* Set the Channel Type based on encap type and 
     * configured operation mode */
    if ((u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH) ||
        (u4EncapType == BFD_ENCAP_MPLS_IPV6_ACH))
    {
        if (u4SessMode == BFD_SESS_MODE_CCV)
        {
            MplsAchHdr.u2ChannelType = BFD_CH_TYPE_CV_IPV4;
        }
        else
        {
            MplsAchHdr.u2ChannelType = BFD_CH_TYPE_CC_IPV4;
        }
    }
    else if (u4EncapType == BFD_ENCAP_MPLS_ACH)
    {
        if (u4SessMode == BFD_SESS_MODE_CCV)
        {
            MplsAchHdr.u2ChannelType = BFD_CH_TYPE_CV_BFD;
        }
        else
        {
            MplsAchHdr.u2ChannelType = BFD_CH_TYPE_CC_BFD;
        }
    }
    else
    {
        /* Invalid Encap type */
        return OSIX_FAILURE;
    }
    MplsUtlFillMplsAchHdr (&MplsAchHdr, pBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxFillSrcMepTlv                                         *
 *                                                                           *
 * Description  : Fill the tMplsMepTlv structure based on the operator type  *
 *                                                                           *
 * Input        : pMplsPathInfo - Pointer to entire path information.        *
 *                                tMplsMegApiInfo which contains MEG info.   *
 *                                It gives source MEP tlv details to fill    *
 *                                the tlv.                                   *
 *                                                                           *
 * Output       : pMplsAchTlv - Pointer to the tMplsMepTlv structure which   *
 *                              contains MEP tlv info based on the operator  *
 *                              type                                         *
 *                pAchTlvHdr  - Pointer to the tMplsAchTlvHdr structure which*
 *                              contains ACH TLV Header length               *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdTxFillSrcMepTlv (tBfdMplsPathInfo * pMplsPathInfo,
                    tMplsAchTlv * pMplsAchTlv, tMplsAchTlvHdr * pAchTlvHdr)
{
    tMplsMegApiInfo    *pMplsMegInfo = NULL;
    tMplsIccMepTlv     *pIccMepTlv = NULL;
    tMplsIpLspMepTlv   *pIpLspMepTlv = NULL;
    tMplsIpPwMepTlv    *pIpPwMepTlv = NULL;
    tPwApiInfo         *pPwApiInfo = NULL;
    tTeTnlApiInfo      *pMplsTnlInfo = NULL;
    tMplsMepTlv        *pMplsMepTlv = NULL;

    pMplsMepTlv = &pMplsAchTlv->unAchValue.MepTlv;
    pMplsMegInfo = &pMplsPathInfo->MplsMegApiInfo;
    pMplsTnlInfo = &pMplsPathInfo->MplsTeTnlApiInfo;
    if (pMplsMegInfo->u4OperatorType == OAM_OPERATOR_TYPE_ICC)
    {
        pIccMepTlv = &pMplsMepTlv->unMepTlv.IccMepTlv;
        pMplsAchTlv->u2TlvType = MPLS_ACH_ICC_MEP_TLV;
        pMplsAchTlv->u2TlvLen = (UINT2) (sizeof (tMplsIccMepTlv));
        /* Copy the MEG Index (ICC and UMC) */
        MEMCPY (pIccMepTlv->au1Icc, pMplsMegInfo->au1Icc,
                STRLEN (pMplsMegInfo->au1Icc));
        MEMCPY (pIccMepTlv->au1Umc, pMplsMegInfo->au1Umc,
                STRLEN (pMplsMegInfo->au1Umc));

        /* Copy the MEP Index */
        pIccMepTlv->u2MepIndex = (UINT2) pMplsMegInfo->u4IccSrcMepIndex;
    }
    else
    {
        if (pMplsMegInfo->u1ServiceType == OAM_SERVICE_TYPE_LSP)
        {
            pMplsAchTlv->u2TlvType = MPLS_ACH_IP_LSP_MEP_TLV;
            pMplsAchTlv->u2TlvLen = (UINT2) sizeof (tMplsIpLspMepTlv);

            /* Copy Global Id and Node Id */
            pIpLspMepTlv = &pMplsMepTlv->unMepTlv.IpLspMepTlv;
            if ((pMplsTnlInfo->u1TnlRole == MPLS_TE_EGRESS)
                && (pMplsTnlInfo->u1TnlMode ==
                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
            {

                pIpLspMepTlv->GlobalNodeId.u4GlobalId =
                    pMplsTnlInfo->TnlLspId.DstNodeId.MplsGlobalNodeId.
                    u4GlobalId;
                pIpLspMepTlv->GlobalNodeId.u4NodeId =
                    pMplsTnlInfo->TnlLspId.DstNodeId.MplsGlobalNodeId.u4NodeId;

            }
            else
            {
                pIpLspMepTlv->GlobalNodeId.u4GlobalId =
                    pMplsTnlInfo->TnlLspId.SrcNodeId.MplsGlobalNodeId.
                    u4GlobalId;
                pIpLspMepTlv->GlobalNodeId.u4NodeId =
                    pMplsTnlInfo->TnlLspId.SrcNodeId.MplsGlobalNodeId.u4NodeId;
            }
            /* Copy Tunnel Number and LSP Number */
            pIpLspMepTlv->u2TnlNum = (UINT2) pMplsMegInfo->
                MegMplsTnlId.u4SrcTnlNum;
            pIpLspMepTlv->u2LspNum = (UINT2) pMplsMegInfo->
                MegMplsTnlId.u4LspNum;
        }
        else
        {
            pMplsAchTlv->u2TlvType = MPLS_ACH_IP_PW_MEP_TLV;
            /* Copy Global Id and Node Id */
            pIpPwMepTlv = &pMplsMepTlv->unMepTlv.IpPwMepTlv;
            pPwApiInfo = &pMplsPathInfo->MplsPwApiInfo;
            pIpPwMepTlv->u4GlobalId =
                pPwApiInfo->MplsPwPathId.SrcNodeId.MplsGlobalNodeId.u4GlobalId;
            pIpPwMepTlv->u4NodeId =
                pPwApiInfo->MplsPwPathId.SrcNodeId.MplsGlobalNodeId.u4NodeId;
            /* Copy AGI and AC Id */
            pIpPwMepTlv->u1AgiLen = pPwApiInfo->MplsPwPathId.u1AgiLen;
            pIpPwMepTlv->u1AgiType = pPwApiInfo->MplsPwPathId.u1AgiType;
            MEMCPY (pIpPwMepTlv->au1Agi, pPwApiInfo->MplsPwPathId.au1Agi,
                    pIpPwMepTlv->u1AgiLen);
            pIpPwMepTlv->u4AcId = pPwApiInfo->MplsPwPathId.u4SrcAcId;
            /* Header Length : AC ID(4), Global ID(4), Node ID(4),
             * AGI Type(1), Len(1) and Value(defined in Len) */
            pMplsAchTlv->u2TlvLen = (UINT2) (BFD_ACH_TLV_LEN * sizeof (UINT4) +
                                             sizeof (UINT2) +
                                             pIpPwMepTlv->u1AgiLen);
        }
    }
    pAchTlvHdr->u2TlvHdrLen = (UINT2) (pMplsAchTlv->u2TlvLen + sizeof (UINT4));
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxFillIpUdpHdr                                          *
 *                                                                           *
 * Description  : Form the IP/UDP header and fill it in the given buffer     *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4IpVer     - IP Version                                   *
 *                                                                           *
 * Output       : pBuf - Pointer to the CRU buffer in which headers to be    *
 *                       prepended                                           *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdTxFillIpUdpHdr (UINT4 u4ContextId, UINT4 u4IpVer,
                   tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tBfdUdp             UdpHdr;
    t_IP_HEADER         IpHdr;
    t_UDP_IP_Pseudo_Hdr TxUdpPseudoHdr;
    UINT4               u4BufLen = 0;
    UINT2               u2Cksum = 0;

    MEMSET (&UdpHdr, 0, sizeof (tBfdUdp));
    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (&TxUdpPseudoHdr, 0, sizeof (t_UDP_IP_Pseudo_Hdr));

    /* Form IP/UDP Header */
    BfdTxFormIpUdpHdr (u4ContextId, &IpHdr, &UdpHdr, u4IpVer, pBuf);

    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);
    /* Fill UDP IP Pseudo Header Info */
    TxUdpPseudoHdr.u4Src = IpHdr.u4Src;
    TxUdpPseudoHdr.u4Dest = IpHdr.u4Dest;
    TxUdpPseudoHdr.u1Zero = 0;
    TxUdpPseudoHdr.u1Proto = IPPROTO_UDP;
    TxUdpPseudoHdr.u2UdpLen = OSIX_HTONS ((UINT2)
                                          (u4BufLen + MPLS_MAX_UDP_HDR_LEN));

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &UdpHdr, sizeof (tBfdUdp)) != CRU_SUCCESS)
    {
        /* CRU Buf Prepend Failed */
        return OSIX_FAILURE;
    }

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &TxUdpPseudoHdr,
         sizeof (t_UDP_IP_Pseudo_Hdr)) != CRU_SUCCESS)
    {
        /* CRU Buf Copy Failed */
        return OSIX_FAILURE;
    }
    /* Get the CRU Buf Length with UDP Header */
    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Calculate checksum for UDP */
    u2Cksum = UtlIpCSumCruBuf (pBuf, u4BufLen, 0);
    u2Cksum = OSIX_HTONS (u2Cksum);
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Cksum,
         (sizeof (t_UDP_IP_Pseudo_Hdr) + BFD_UDP_HDR_CKSUM_OFFSET),
         sizeof (UINT2)) != CRU_SUCCESS)
    {
        /* CRU Buf Copy Failed */
        return OSIX_FAILURE;
    }

    /* Since we want to prepend the IP header before UDP header, move the valid
     * offset to the beginning of UDP IP Pseudo header */
    CRU_BUF_Move_ValidOffset (pBuf, sizeof (t_UDP_IP_Pseudo_Hdr));

    if (u4IpVer == BFD_HEADER_IPV4)
    {
        /* Fill IP Header in the buffer */
        if (CRU_BUF_Prepend_BufChain
            (pBuf, (UINT1 *) &IpHdr, IP_HDR_LEN) != CRU_SUCCESS)
        {
            /* CRU Buf Prepend Failed */
            return OSIX_FAILURE;
        }
        /* Calculate checksum for IP */
        u2Cksum = UtlIpCSumCruBuf (pBuf, IP_HDR_LEN, 0);
        u2Cksum = OSIX_HTONS (u2Cksum);
        if (CRU_BUF_Copy_OverBufChain
            (pBuf, (UINT1 *) &u2Cksum, BFD_IP_HDR_CKSUM_OFFSET,
             sizeof (UINT2)) != CRU_SUCCESS)
        {
            /* CRU Buf Copy Failed */
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* IPv6 */
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdTxFormIpUdpHdr                                          *
 *                                                                           *
 * Description  : Forms the IP/UDP header                                    *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4IpVer     - IP Version                                   *
 *                                                                           *
 * Output       : pIpHdr  - Pointer to the t_IP_HEADER which contains        *
 *                          IP Header information                            *
 *                pUdpHdr - Pointer to the tBfdUdp which contains UDP        *
 *                          Header information                               *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdTxFormIpUdpHdr (UINT4 u4ContextId, t_IP_HEADER * pIpHdr,
                   tBfdUdp * pUdpHdr, UINT4 u4IpVer,
                   tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4BufLen = 0;
    UINT4               u4RouterIp = 0;

    if (u4IpVer == BFD_HEADER_IPV4)
    {
        u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

        /* Get the Src IP */
        BfdExtGetRouterId (u4ContextId, &u4RouterIp);

        /* Fill IP Header Info */
        pIpHdr->u1Ver_hdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, ZERO);
        pIpHdr->u1Tos = 0;
        pIpHdr->u2Totlen = OSIX_HTONS ((UINT2) (u4BufLen + IP_HDR_LEN +
                                                MPLS_MAX_UDP_HDR_LEN));
        pIpHdr->u2Id = 0;
        pIpHdr->u2Fl_offs = 0;
        pIpHdr->u1Ttl = 1;
        pIpHdr->u1Proto = IPPROTO_UDP;
        pIpHdr->u2Cksum = 0;
        pIpHdr->u4Src = OSIX_HTONL (u4RouterIp);
        pIpHdr->u4Dest = OSIX_HTONL (IP_LOOPBACK_ADDRESS);
    }
    else
    {
        /* IPv6 */
    }

    /* Fill UDP Header Info */
    pUdpHdr->u2DestUdpPort = OSIX_HTONS (BFD_UDP_DEST_PORT);
    pUdpHdr->u2SrcUdpPort = OSIX_HTONS (BFD_UDP_SRC_PORT);
    pUdpHdr->u2Len = OSIX_HTONS ((UINT2) (u4BufLen + MPLS_MAX_UDP_HDR_LEN));

    return;
}

#endif /* _BFDTX_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdtx.c                        */
/*-----------------------------------------------------------------------*/
