/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddbg.c,v 1.13 2014/08/11 12:01:23 siva Exp $
*
* Description: This file contains the routines for the protocol
*              Database Access for the module BFD
*********************************************************************/

#include "bfdinc.h"

/****************************************************************************
 Function    :  BfdGetAllFsMIStdBfdGlobalConfigTable
 Input       :  pBfdGetFsMIStdBfdGlobalConfigTableEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdGetAllFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                      pBfdGetFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    /* Check whether the node is already present */
    pBfdFsMIStdBfdGlobalConfigTableEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable,
                   (tRBElem *) pBfdGetFsMIStdBfdGlobalConfigTableEntry);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdGetAllFsMIStdBfdGlobalConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdGblSessOperMode =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdGblSessOperMode;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblDesiredMinTxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblDesiredMinTxIntvl;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblReqMinRxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblReqMinRxIntvl;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdMemAllocFailure =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdMemAllocFailure;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdInputQOverFlows =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdInputQOverFlows;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats;

    pBfdGetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdClrAllSessStats =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdClrAllSessStats;

    if (BfdGetAllUtlFsMIStdBfdGlobalConfigTable
        (pBfdGetFsMIStdBfdGlobalConfigTableEntry,
         pBfdFsMIStdBfdGlobalConfigTableEntry) == OSIX_FAILURE)

    {
        BFD_TRC ((BFD_UTIL_TRC, "BfdGetAllFsMIStdBfdGlobalConfigTable:"
                  "BfdGetAllUtlFsMIStdBfdGlobalConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdGetAllFsMIStdBfdSessTable
 Input       :  pBfdGetFsMIStdBfdSessEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdGetAllFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                              pBfdGetFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    /* Check whether the node is already present */
    pBfdFsMIStdBfdSessEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                   (tRBElem *) pBfdGetFsMIStdBfdSessEntry);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdGetAllFsMIStdBfdSessTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessState =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessState;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDiag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDiag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDemandModeDesiredFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDemandModeDesiredFlag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessControlPlaneIndepFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessControlPlaneIndepFlag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pBfdGetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen);

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType;

    MEMCPY (pBfdGetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen);

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessNegotiatedEchoInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessNegotiatedEchoInterval;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedDetectMult =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedDetectMult;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessAuthenticationType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationType;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID;

    MEMCPY (pBfdGetFsMIStdBfdSessEntry->MibObject.
            au1FsMIStdBfdSessAuthenticationKey,
            pBfdFsMIStdBfdSessEntry->MibObject.
            au1FsMIStdBfdSessAuthenticationKey,
            pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen);

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfCtrlPktDropLastTime =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfCtrlPktDropLastTime;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktIn;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktOut;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktDrop =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktDrop;

    pBfdGetFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfEchoPktDropLastTime =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfEchoPktDropLastTime;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessUpTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessUpTime;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfLastSessDownTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfLastSessDownTime;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessPerfLastCommLostDiag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessPerfLastCommLostDiag;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfSessUpCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfSessUpCount;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfDiscTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfDiscTime;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktDropHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktDropHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktInHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktInHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktOutHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktOutHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktDropHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktDropHC;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;

    MEMCPY (pBfdGetFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
            pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
            (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen *
             sizeof (UINT4)));
    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktIn;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktOut;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktIn;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktOut;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessMisDefCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessMisDefCount;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessLocDefCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessLocDefCount;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiInCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiInCount;

    pBfdGetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiOutCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiOutCount;

    pBfdGetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats;

    if (BfdGetAllUtlFsMIStdBfdSessTable
        (pBfdGetFsMIStdBfdSessEntry, pBfdFsMIStdBfdSessEntry) == OSIX_FAILURE)

    {
        BFD_TRC ((BFD_UTIL_TRC, "BfdGetAllFsMIStdBfdSessTable:"
                  "BfdGetAllUtlFsMIStdBfdSessTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdGetAllFsMIStdBfdSessDiscMapTable
 Input       :  pBfdGetFsMIStdBfdSessDiscMapEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdGetAllFsMIStdBfdSessDiscMapTable (tBfdFsMIStdBfdSessDiscMapEntry *
                                     pBfdGetFsMIStdBfdSessDiscMapEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    /* Check whether the node is already present */
    pBfdFsMIStdBfdSessDiscMapEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
                   (tRBElem *) pBfdGetFsMIStdBfdSessDiscMapEntry);

    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdGetAllFsMIStdBfdSessDiscMapTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pBfdGetFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex =
        pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex;

    if (BfdGetAllUtlFsMIStdBfdSessDiscMapTable
        (pBfdGetFsMIStdBfdSessDiscMapEntry,
         pBfdFsMIStdBfdSessDiscMapEntry) == OSIX_FAILURE)

    {
        BFD_TRC ((BFD_UTIL_TRC, "BfdGetAllFsMIStdBfdSessDiscMapTable:"
                  "BfdGetAllUtlFsMIStdBfdSessDiscMapTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdGetAllFsMIStdBfdSessIpMapTable
 Input       :  pBfdGetFsMIStdBfdSessIpMapEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdGetAllFsMIStdBfdSessIpMapTable (tBfdFsMIStdBfdSessIpMapEntry *
                                   pBfdGetFsMIStdBfdSessIpMapEntry)
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    /* Check whether the node is already present */
    pBfdFsMIStdBfdSessIpMapEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
                   (tRBElem *) pBfdGetFsMIStdBfdSessIpMapEntry);

    if (pBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdGetAllFsMIStdBfdSessIpMapTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pBfdGetFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex;

    if (BfdGetAllUtlFsMIStdBfdSessIpMapTable
        (pBfdGetFsMIStdBfdSessIpMapEntry,
         pBfdFsMIStdBfdSessIpMapEntry) == OSIX_FAILURE)

    {
        BFD_TRC ((BFD_UTIL_TRC, "BfdGetAllFsMIStdBfdSessIpMapTable:"
                  "BfdGetAllUtlFsMIStdBfdSessIpMapTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdSetAllFsMIStdBfdGlobalConfigTable
 Input       :  pBfdSetFsMIStdBfdGlobalConfigTableEntry
                pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdSetAllFsMIStdBfdGlobalConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                      pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                                      tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
                                      *
                                      pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdOldFsMIStdBfdGlobalConfigTableEntry;

    MEMSET (&BfdOldFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Check whether the node is already present */
    pBfdFsMIStdBfdGlobalConfigTableEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable,
                   (tRBElem *) pBfdSetFsMIStdBfdGlobalConfigTableEntry);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdSetAllFsMIStdBfdGlobalConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMIStdBfdGlobalConfigTableFilterInputs
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdSetFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&BfdOldFsMIStdBfdGlobalConfigTableEntry,
            pBfdFsMIStdBfdGlobalConfigTableEntry,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign values for the existing node */
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable != OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdSessNotificationsEnable =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdSessNotificationsEnable;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTraceLevel;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTrapEnable;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdGblSessOperMode =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdGblSessOperMode;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIBfdGblDesiredMinTxIntvl != OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDesiredMinTxIntvl =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDesiredMinTxIntvl;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblReqMinRxIntvl !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblReqMinRxIntvl =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblReqMinRxIntvl;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDetectMult;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblSlowTxIntvl =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblSlowTxIntvl;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrGblStats;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrAllSessStats =
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrAllSessStats;
    }

    if (BfdUtilUpdateFsMIStdBfdGlobalConfigTable
        (&BfdOldFsMIStdBfdGlobalConfigTableEntry,
         pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        if (BfdSetAllFsMIStdBfdGlobalConfigTableTrigger
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry,
             pBfdIsSetFsMIStdBfdGlobalConfigTableEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pBfdFsMIStdBfdGlobalConfigTableEntry,
                &BfdOldFsMIStdBfdGlobalConfigTableEntry,
                sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
        return OSIX_FAILURE;
    }

    if (BfdSetAllFsMIStdBfdGlobalConfigTableTrigger
        (pBfdSetFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdSetAllFsMIStdBfdSessTable
 Input       :  pBfdSetFsMIStdBfdSessEntry
                pBfdIsSetFsMIStdBfdSessEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdSetAllFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                              pBfdSetFsMIStdBfdSessEntry,
                              tBfdIsSetFsMIStdBfdSessEntry *
                              pBfdIsSetFsMIStdBfdSessEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdOldFsMIStdBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdTrgFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdTrgIsSetFsMIStdBfdSessEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pBfdOldFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdOldFsMIStdBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pBfdTrgFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdTrgFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }
    pBfdTrgIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdTrgIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdOldFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdTrgFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdTrgIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Check whether the node is already present */
    pBfdFsMIStdBfdSessEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                   (tRBElem *) pBfdSetFsMIStdBfdSessEntry);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
             CREATE_AND_WAIT)
            || (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessRowStatus == CREATE_AND_GO)
            ||
            ((pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pBfdFsMIStdBfdSessEntry =
                (tBfdFsMIStdBfdSessEntry *)
                MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
            if (pBfdFsMIStdBfdSessEntry == NULL)
            {

                BFD_TRC ((BFD_UTIL_TRC,
                          "BfdSetAllFsMIStdBfdSessTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pBfdFsMIStdBfdSessEntry, 0,
                    sizeof (tBfdFsMIStdBfdSessEntry));
            if ((BfdInitializeFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry)) ==
                OSIX_FAILURE)
            {
                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdSetFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);

                    return OSIX_FAILURE;
                }
                BFD_TRC ((BFD_UTIL_TRC,
                          "BfdSetAllFsMIStdBfdSessTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessVersionNumber =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessVersionNumber;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessDestinationUdpPort != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDestinationUdpPort =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDestinationUdpPort;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessSourceUdpPort =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessSourceUdpPort;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessEchoSourceUdpPort != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessEchoSourceUdpPort =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessEchoSourceUdpPort;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAdminStatus;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessOperMode;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessDemandModeDesiredFlag != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDemandModeDesiredFlag =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDemandModeDesiredFlag;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessControlPlaneIndepFlag != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessControlPlaneIndepFlag =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessControlPlaneIndepFlag;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessMultipointFlag =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessMultipointFlag;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessInterface;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessSrcAddrType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr !=
                OSIX_FALSE)
            {
                MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessSrcAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessSrcAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessSrcAddrLen);

                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessSrcAddrLen;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDstAddrType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr !=
                OSIX_FALSE)
            {
                MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessDstAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessDstAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessDstAddrLen);

                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessDstAddrLen;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessGTSMTTL;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessDesiredMinTxInterval != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDesiredMinTxInterval =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDesiredMinTxInterval;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinRxInterval =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinRxInterval;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessReqMinEchoRxInterval != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinEchoRxInterval =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinEchoRxInterval;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDetectMult;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthPresFlag =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthPresFlag;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessAuthenticationType != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessAuthenticationKeyID != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyID =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyID;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->
                bFsMIStdBfdSessAuthenticationKey != OSIX_FALSE)
            {
                MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessAuthenticationKey, 0,
                        sizeof (pBfdFsMIStdBfdSessEntry->MibObject.
                                au1FsMIStdBfdSessAuthenticationKey));
                MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessAuthenticationKey,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessAuthenticationKey,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessAuthenticationKeyLen);

                pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyLen =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyLen;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessStorType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIBfdSessRemoteDiscr;
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIBfdSessRemoteDiscr;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIBfdSessTmrNegotiate;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIBfdSessEncapType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer !=
                OSIX_FALSE)
            {
                MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
                        au4FsMIBfdSessMapPointer,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au4FsMIBfdSessMapPointer,
                        (pBfdSetFsMIStdBfdSessEntry->MibObject.
                         i4FsMIBfdSessMapPointerLen * sizeof (UINT4)));

                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIBfdSessMapPointerLen;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIBfdSessCardNumber;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber !=
                OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIBfdSessSlotNumber;
            }

            if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats != OSIX_FALSE)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats;
            }

            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pBfdSetFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessRowStatus == ACTIVE)))
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
                    ACTIVE;

                pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
                pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

                pBfdTrgFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus = CREATE_AND_WAIT;
                pBfdTrgIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus =
                    OSIX_TRUE;

                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdTrgFsMIStdBfdSessEntry,
                     pBfdTrgIsSetFsMIStdBfdSessEntry,
                     SNMP_SUCCESS) != OSIX_SUCCESS)

                {
                    BFD_TRC ((BFD_UTIL_TRC,
                              "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger "
                              " function returns failure.\r\n"));

                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);
                    return OSIX_FAILURE;
                }
            }

            else if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessRowStatus == CREATE_AND_WAIT)
            {
                pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
                    NOT_READY;

                /* For MSR and RM Trigger */
                pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
                pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
                pBfdTrgFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus = CREATE_AND_WAIT;
                pBfdTrgIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus =
                    OSIX_TRUE;

                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdTrgFsMIStdBfdSessEntry,
                     pBfdTrgIsSetFsMIStdBfdSessEntry,
                     SNMP_SUCCESS) != OSIX_SUCCESS)
                {
                    BFD_TRC ((BFD_UTIL_TRC,
                              "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger"
                              " function returns failure.\r\n"));

                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);
                    return OSIX_FAILURE;
                }

            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                 (tRBElem *) pBfdFsMIStdBfdSessEntry) != RB_SUCCESS)
            {
                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdTrgFsMIStdBfdSessEntry,
                     pBfdTrgIsSetFsMIStdBfdSessEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    BFD_TRC ((BFD_UTIL_TRC,
                              "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);
                    return OSIX_FAILURE;
                }

                BFD_TRC ((BFD_UTIL_TRC,
                          "BfdSetAllFsMIStdBfdSessTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
                return OSIX_FAILURE;
            }
            if (BfdUtilUpdateFsMIStdBfdSessTable
                (NULL, pBfdFsMIStdBfdSessEntry,
                 pBfdIsSetFsMIStdBfdSessEntry) != OSIX_SUCCESS)
            {
                BFD_TRC ((BFD_UTIL_TRC,
                          "BfdSetAllFsMIStdBfdSessTable: BfdUtilUpdateFsMIStdBfdSessTable function returns failure.\r\n"));

                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdSetFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);

                    return OSIX_FAILURE;

                }
                RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                           pBfdFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
                return OSIX_FAILURE;
            }

            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessRowStatus != CREATE_AND_WAIT)
            {

                if (BfdSetAllFsMIStdBfdSessTableTrigger
                    (pBfdSetFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry,
                     SNMP_SUCCESS) != OSIX_SUCCESS)
                {

                    BFD_TRC ((BFD_UTIL_TRC,
                              "BfdSetAllFsMIStdBfdSessTable:  BfdSetAllFsMIStdBfdSessTableTrigger"
                              "function returns failure.\r\n"));
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                        (UINT1 *)
                                        pBfdTrgIsSetFsMIStdBfdSessEntry);

                    return OSIX_FAILURE;
                }
            }

            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

            return OSIX_SUCCESS;
        }

        else
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

            return OSIX_FAILURE;
        }
    }
    else if ((pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
              CREATE_AND_WAIT)
             || (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessRowStatus == CREATE_AND_GO))
    {
        if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdSetFsMIStdBfdSessEntry,
                                                 pBfdIsSetFsMIStdBfdSessEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

            return OSIX_FAILURE;
        }
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdSetAllFsMIStdBfdSessTable: The row is already present.\r\n"));
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pBfdOldFsMIStdBfdSessEntry, pBfdFsMIStdBfdSessEntry,
            sizeof (tBfdFsMIStdBfdSessEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
        DESTROY)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = DESTROY;

        if (BfdUtilUpdateFsMIStdBfdSessTable (pBfdOldFsMIStdBfdSessEntry,
                                              pBfdFsMIStdBfdSessEntry,
                                              pBfdIsSetFsMIStdBfdSessEntry) !=
            OSIX_SUCCESS)
        {

            if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdSetFsMIStdBfdSessEntry,
                                                     pBfdIsSetFsMIStdBfdSessEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

                return OSIX_FAILURE;
            }
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable: BfdUtilUpdateFsMIStdBfdSessTable function returns failure.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

            return OSIX_FAILURE;
        }
        RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                   pBfdFsMIStdBfdSessEntry);
        if (BfdSetAllFsMIStdBfdSessTableTrigger
            (pBfdSetFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMIStdBfdSessTableFilterInputs
        (pBfdFsMIStdBfdSessEntry, pBfdSetFsMIStdBfdSessEntry,
         pBfdIsSetFsMIStdBfdSessEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE)
        && (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus !=
            NOT_IN_SERVICE))
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
        pBfdTrgFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
        pBfdTrgFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
            NOT_IN_SERVICE;
        pBfdTrgIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus = OSIX_TRUE;

        if (BfdUtilUpdateFsMIStdBfdSessTable (pBfdOldFsMIStdBfdSessEntry,
                                              pBfdFsMIStdBfdSessEntry,
                                              pBfdIsSetFsMIStdBfdSessEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pBfdFsMIStdBfdSessEntry, pBfdOldFsMIStdBfdSessEntry,
                    sizeof (tBfdFsMIStdBfdSessEntry));
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable:                 BfdUtilUpdateFsMIStdBfdSessTable Function returns failure.\r\n"));

            if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdSetFsMIStdBfdSessEntry,
                                                     pBfdIsSetFsMIStdBfdSessEntry,
                                                     SNMP_FAILURE) !=
                OSIX_SUCCESS)

            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

                return OSIX_FAILURE;
            }
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
            return OSIX_FAILURE;
        }

        if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdTrgFsMIStdBfdSessEntry,
                                                 pBfdTrgIsSetFsMIStdBfdSessEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);

            return OSIX_FAILURE;
        }
    }

    if (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDestinationUdpPort;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessEchoSourceUdpPort;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessDemandModeDesiredFlag =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessDemandModeDesiredFlag;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessControlPlaneIndepFlag =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessControlPlaneIndepFlag;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessMultipointFlag;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr != OSIX_FALSE)
    {
        MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessSrcAddrLen);

        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType != OSIX_FALSE)
    {
#ifdef BSDCOMP_SLI_WANTED
        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType !=
             0)
            && (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrType !=
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrType))
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType = 0;
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen = 0;
            MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                    0,
                    sizeof (pBfdFsMIStdBfdSessEntry->MibObject.
                            au1FsMIStdBfdSessSrcAddr));

        }
#endif

        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType;

    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr != OSIX_FALSE)
    {
        MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrLen);

        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinRxInterval;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinEchoRxInterval =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinEchoRxInterval;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationType;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID !=
        OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyID;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey !=
        OSIX_FALSE)
    {
        MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessAuthenticationKey, 0,
                sizeof (pBfdFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessAuthenticationKey));

        MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessAuthenticationKey,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessAuthenticationKey,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyLen);

        pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType != OSIX_FALSE)
    {
        /* For dynamic sessions storage type will be VOLATILE.
         * For dynamic and static it should be BFD_OTHER.
         * So if the session is created dynamically (volatile),
         * and if the same path needs to be monitored statically (non-volatile).
         * then the storage type should be changed to BFD_OTHER
         */
        if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
             BFD_VOLATILE)
            && (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessStorType == BFD_NONVOLATILE))
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
                BFD_OTHER;
        }
        else
        {
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
                pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType;
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr;
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer != OSIX_FALSE)
    {
        MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapPointerLen * sizeof (UINT4)));

        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats != OSIX_FALSE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = ACTIVE;
    }

    if (BfdUtilUpdateFsMIStdBfdSessTable (pBfdOldFsMIStdBfdSessEntry,
                                          pBfdFsMIStdBfdSessEntry,
                                          pBfdIsSetFsMIStdBfdSessEntry) !=
        OSIX_SUCCESS)
    {

        if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdSetFsMIStdBfdSessEntry,
                                                 pBfdIsSetFsMIStdBfdSessEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));

        }
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdSetAllFsMIStdBfdSessTable: BfdUtilUpdateFsMIStdBfdSessTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pBfdFsMIStdBfdSessEntry, pBfdOldFsMIStdBfdSessEntry,
                sizeof (tBfdFsMIStdBfdSessEntry));
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
        return OSIX_FAILURE;

    }
    if (BfdSetAllFsMIStdBfdSessTableTrigger (pBfdSetFsMIStdBfdSessEntry,
                                             pBfdIsSetFsMIStdBfdSessEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdSetAllFsMIStdBfdSessTable: BfdSetAllFsMIStdBfdSessTableTrigger function returns failure.\r\n"));
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdOldFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdTrgFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdTrgIsSetFsMIStdBfdSessEntry);
    return OSIX_SUCCESS;

}
