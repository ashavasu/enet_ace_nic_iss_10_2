/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _BFDSZ_C
#include "bfdinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
BfdSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BFD_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsBFDSizingParams[i4SizingId].u4StructSize,
                                     FsBFDSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(BFDMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            BfdSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
BfdSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBFDSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BFDMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
BfdSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BFD_MAX_SIZING_ID; i4SizingId++)
    {
        if (BFDMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BFDMemPoolIds[i4SizingId]);
            BFDMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
