/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdoff.c,v 1.47 2015/09/04 07:19:46 siva Exp $
 *
 * Description: This file contains the routines for offloading a session 
 *********************************************************************/

#include "bfdinc.h"

PRIVATE VOID        BfdOffDebug (UINT4, CHR1 *, ...);

PRIVATE VOID
 
 
   BfdOffFillRxSrcMepTlv (tBfdMplsPathInfo *, tMplsAchTlv *, tMplsAchTlvHdr *);
#ifndef NPAPI_WANTED
PRIVATE VOID        BfdOffStubFsMiBfdHwStartBfd (UINT4,
                                                 tBfdSessionHandle *,
                                                 tBfdSessionPathInfo *,
                                                 tBfdParams *,
                                                 tBfdOffPktBuf *,
                                                 tBfdOffPktBuf *,
                                                 tBfdSessHwStartBfdCallFlag,
                                                 tBfdHwhandle *);
#endif

/***************************************************************************
 * FUNCTION NAME    : BfdGetLabelFromOutSegInfo
 *
 * DESCRIPTION      : Function is used to get the complete Label stack from
 *                    outsegment details of LSP or Tunnel
 *
 * INPUT            : tMplsOutSegInfo - Outsegment information for path
 *
 *                    pBfdMibFsMIStdBfdSessEntry - Session MIB objects
 *
 * OUTPUT           : pOutLabelList - out label list. memory of supporting
 *                                    (MAX_LABEL_STACK_DEPTH-1) should be 
 *                                    allocated to this.
 *                    pu1OutLblStkCnt - number of lables in the list
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdGetLabelFromOutSegInfo (tBfdMibFsMIStdBfdSessEntry
                           * pBfdMibFsMIStdBfdSessEntry,
                           tMplsOutSegInfo * pMplsOutSegInfo,
                           tMplsLblInfo * pOutLabelList, UINT1 *pu1OutLblStkCnt)
{
    UINT1               u1LblStkCnt = 0;
    /* Get the label stack count */
    u1LblStkCnt = pMplsOutSegInfo->u1LblStkCnt;

    /* check the label stack is not greater than the memory available
     * In case of PW, one label is already occupied*/
    if (u1LblStkCnt > (MPLS_MAX_LABELS_PER_ENTRY - 1))
    {

        return OSIX_FAILURE;
    }

    /* Memcopy the labels */
    MEMCPY (pOutLabelList,
            pMplsOutSegInfo->LblInfo, (sizeof (tMplsLblInfo)) * (u1LblStkCnt));

    /* Modify the exp values to the PHB configured in BFD */
    for (; u1LblStkCnt > 0; u1LblStkCnt--)
    {
        pOutLabelList[u1LblStkCnt].u1Exp =
            (UINT1) (pBfdMibFsMIStdBfdSessEntry->u4FsMIBfdSessEXPValue);
    }

    *pu1OutLblStkCnt = pMplsOutSegInfo->u1LblStkCnt;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdGetLabelFromInSegInfo
 *
 * DESCRIPTION      : Function is used to get the complete Label stack from
 *                    insegment details of LSP or Tunnel
 *
 * INPUT            : tMplsInSegInfo - InSegment information for path
 *
 *                    pBfdMibFsMIStdBfdSessEntry - Session MIB objects
 *
 * OUTPUT           : pInLabelList - in label list. memory of supporting
 *                                    (MAX_LABEL_STACK_DEPTH-1) should be 
 *                                    allocated to this
 *                    pu1InLblStkCnt - number of labels in the list
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdGetLabelFromInSegInfo (tBfdMibFsMIStdBfdSessEntry
                          * pBfdMibFsMIStdBfdSessEntry,
                          tMplsInSegInfo * pMplsInSegInfo,
                          tMplsLblInfo * pInLabelList, UINT1 *pu1InLblStkCnt)
{
    UINT1               u1LblStkCnt = 0;
    /* Get the label stack count */
    u1LblStkCnt = pMplsInSegInfo->u1LblStkCnt;

    /* check the label stack is not greater than the memory available
     * In case of PW, one label is already occupied*/
    if (u1LblStkCnt > (MPLS_MAX_LABELS_PER_ENTRY - 1))
    {

        return OSIX_FAILURE;
    }

    /* Memcopy the labels */
    MEMCPY (pInLabelList,
            pMplsInSegInfo->LblInfo, (sizeof (tMplsLblInfo)) * (u1LblStkCnt));

    /* Modify the exp values to the PHB configured in BFD */
    for (; u1LblStkCnt > 0; u1LblStkCnt--)
    {
        pInLabelList[u1LblStkCnt].u1Exp =
            (UINT1) (pBfdMibFsMIStdBfdSessEntry->u4FsMIBfdSessEXPValue);
    }

    *pu1InLblStkCnt = pMplsInSegInfo->u1LblStkCnt;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdGetOutLabelInfoFromPw
 *
 * DESCRIPTION      : Function is used to get the complete Outgoing Label 
 *                    stack for the PW.Calling function will check if path 
 *                    is PW
 *
 * INPUT            : pBfdMplsPathInfo - pointer to the MPLS Path structure.
 *                                       based on the path, this structure
 *                                       is filled with path info
 *
 *                    pBfdMibFsMIStdBfdSessEntry - Session MIB objects
 *
 * OUTPUT           : pOutLabelList - out label list. memory of supporting
 *                                    MAX_LABEL_STACK_DEPTH should be 
 *                                    allocated to this
 *                    pu1OutLblStkCnt - number of lables in the list
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdGetOutLabelInfoFromPw (tBfdMplsPathInfo * pBfdMplsPathInfo,
                          tBfdMibFsMIStdBfdSessEntry *
                          pBfdMibFsMIStdBfdSessEntry,
                          tMplsLblInfo * pOutLabelList, UINT1 *pu1OutLblStkCnt)
{
    tMplsOutSegInfo    *pMplsOutSegInfo = NULL;
    UINT4               u4MaxLabelStack = 0;

    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_RAL)
        == L2VPN_VCCV_CC_RAL)
    {
        u4MaxLabelStack = MPLS_MAX_LABELS_PER_ENTRY - 2;
    }
    else
    {
        u4MaxLabelStack = MPLS_MAX_LABELS_PER_ENTRY - 1;
    }

    /* Get the labels when underlying path is Tunnel */
    if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_TE)
    {
        pMplsOutSegInfo =
            &(pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo);
    }
    else if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType ==
             L2VPN_MPLS_TYPE_NONTE)
    {
        pMplsOutSegInfo =
            &(pBfdMplsPathInfo->MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo);
    }
    else if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType !=
             L2VPN_MPLS_TYPE_VCONLY)
    {
        /* MPLS TYPE is unknown, return failure */
        return OSIX_FAILURE;
    }

    if (pMplsOutSegInfo != NULL)
    {
        /* Non VCONLY case , get labels from outsegment info */
        if (BfdGetLabelFromOutSegInfo (pBfdMibFsMIStdBfdSessEntry,
                                       pMplsOutSegInfo,
                                       pOutLabelList,
                                       pu1OutLblStkCnt) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (pMplsOutSegInfo->u1LblStkCnt > u4MaxLabelStack)
        {
            /* Cant fill VC Label */
            return OSIX_FAILURE;
        }

        if (pMplsOutSegInfo->u1LblStkCnt != 0)
        {
            pOutLabelList[pMplsOutSegInfo->u1LblStkCnt - 1].u1SI = 0;
        }
        pOutLabelList += pMplsOutSegInfo->u1LblStkCnt;
    }
    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_RAL)
        == L2VPN_VCCV_CC_RAL)
    {
        /* If CC type is RAL, insert RAL label */
        pOutLabelList->u4Label = MPLS_ROUTER_ALERT_LABEL;
        pOutLabelList->u1Ttl = MPLS_DEF_LSP_TTL;
        pOutLabelList++;
        (*pu1OutLblStkCnt)++;
    }

    /* Get the PW label information first */
    pOutLabelList->u4Label = pBfdMplsPathInfo->MplsPwApiInfo.u4OutVcLabel;

    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_TTL_EXP)
        == L2VPN_VCCV_CC_TTL_EXP)
    {
        /* If CC selected is TTL Expiry, make TTL = 1 */
        pOutLabelList->u1Ttl = 1;
    }
    else
    {
        pOutLabelList->u1Ttl = pBfdMplsPathInfo->MplsPwApiInfo.u1Ttl;
    }
    pOutLabelList->u1SI = 1;
    (*pu1OutLblStkCnt)++;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdGetInLabelInfoFromPw
 *
 * DESCRIPTION      : Function is used to get the complete incoming Label 
 *                    stack for the PW.Calling function will check if path 
 *                    is PW
 *
 * INPUT            : pBfdMplsPathInfo - pointer to the MPLS Path structure.
 *                                       based on the path, this structure
 *                                       is filled with path info
 *
 *                    pBfdMibFsMIStdBfdSessEntry - Session MIB objects
 *
 * OUTPUT           : pInLabelList - in label list. memory of supporting
 *                                   MAX_LABEL_STACK_DEPTH should be 
 *                                   allocated to this 
 *                    pu1InLblStkCnt - number of labels in the list
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdGetInLabelInfoFromPw (tBfdMplsPathInfo * pBfdMplsPathInfo,
                         tBfdMibFsMIStdBfdSessEntry *
                         pBfdMibFsMIStdBfdSessEntry,
                         tMplsLblInfo * pInLabelList, UINT1 *pu1InLblStkCnt)
{
    tMplsInSegInfo     *pMplsInSegInfo = NULL;
    UINT4               u4MaxLabelStack = 0;

    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_RAL)
        == L2VPN_VCCV_CC_RAL)
    {
        u4MaxLabelStack = MPLS_MAX_LABELS_PER_ENTRY - 2;
    }
    else
    {
        u4MaxLabelStack = MPLS_MAX_LABELS_PER_ENTRY - 1;
    }

    /* Get the labels when underlying path is Tunnel */
    if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_TE)
    {
        pMplsInSegInfo =
            &(pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.RevInSegInfo);
    }
    else if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType ==
             L2VPN_MPLS_TYPE_NONTE)
    {
        pMplsInSegInfo =
            &(pBfdMplsPathInfo->MplsNonTeApiInfo.XcApiInfo.RevInSegInfo);
    }
    else if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType !=
             L2VPN_MPLS_TYPE_VCONLY)
    {
        /* MPLS TYPE is unknown, return failure */
        return OSIX_FAILURE;
    }

    if (pMplsInSegInfo != NULL)
    {
        /* Non VCONLY case , get labels from insegment info */
        if (BfdGetLabelFromInSegInfo (pBfdMibFsMIStdBfdSessEntry,
                                      pMplsInSegInfo,
                                      pInLabelList,
                                      pu1InLblStkCnt) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (pMplsInSegInfo->u1LblStkCnt > u4MaxLabelStack)
        {
            /* Cant fill VC Label */
            return OSIX_FAILURE;
        }

        if (pMplsInSegInfo->u1LblStkCnt != 0)
        {
            pInLabelList[pMplsInSegInfo->u1LblStkCnt - 1].u1SI = 0;
        }
        pInLabelList += pMplsInSegInfo->u1LblStkCnt;
    }
    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_RAL)
        == L2VPN_VCCV_CC_RAL)
    {
        /* If CC type is RAL, insert RAL label */
        pInLabelList->u4Label = MPLS_ROUTER_ALERT_LABEL;
        pInLabelList->u1Ttl = MPLS_DEF_LSP_TTL;
        pInLabelList++;
        (*pu1InLblStkCnt)++;
    }

    /* Get the PW label information first */
    pInLabelList->u4Label = pBfdMplsPathInfo->MplsPwApiInfo.u4InVcLabel;

    if ((pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected & L2VPN_VCCV_CC_TTL_EXP)
        == L2VPN_VCCV_CC_TTL_EXP)
    {
        /* If CC selected is TTL Expiry, make TTL = 1 */
        pInLabelList->u1Ttl = 1;
    }
    else
    {
        pInLabelList->u1Ttl = pBfdMplsPathInfo->MplsPwApiInfo.u1Ttl;
    }

    pInLabelList->u1SI = 1;
    (*pu1InLblStkCnt)++;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessHandle
 *
 * DESCRIPTION      : Function is used to construct bfd session handle 
 *                    structure from BFD session table structure
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *
 * OUTPUT           : pBfdSessHandle - pointer to the session handle structure 
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
VOID
BfdOffFillSessHandle (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                      tBfdSessionHandle * pBfdSessHandle)
{

    tBfdMibFsMIStdBfdSessEntry *pBfdMibFsMIStdBfdSessEntry = NULL;

    pBfdMibFsMIStdBfdSessEntry = &pBfdFsMIStdBfdSessEntry->MibObject;

    /* Get the session index */
    pBfdSessHandle->u4BfdSessId =
        pBfdMibFsMIStdBfdSessEntry->u4FsMIStdBfdSessIndex;

    /* Get the Local discriminator */
    pBfdSessHandle->u4BfdSessLocalDiscr =
        pBfdMibFsMIStdBfdSessEntry->u4FsMIStdBfdSessDiscriminator;

    /* Get the Slot number */
    pBfdSessHandle->u4BfdSlotId =
        pBfdMibFsMIStdBfdSessEntry->u4FsMIBfdSessSlotNumber;

    /* Get the Card Id */
    pBfdSessHandle->u4BfdCardNum =
        pBfdMibFsMIStdBfdSessEntry->u4FsMIBfdSessCardNumber;

    return;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffResolveArpForNh
 *
 * DESCRIPTION      : This function is used to get Mac Addr for NH or enqueue request
 *                              to resolve in case its unavailable
 *
 * INPUT            : u4Nhip - Next Hop ip address
 *                        u4IfIndex - If index
 *
 * OUTPUT           : pau1MacAddr - Mac address for the next hop
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffResolveArpForNh (UINT4 u4Nhip, UINT4 u4IfIndex, UINT1 *pau1MacAddr)
{

    UINT1               u1EncapType = 0;
    UINT4               u4IpPort = 0;

    /* ARP Resolve */
    if (BfdExtArpResolve (u4Nhip,
                          (UINT1 *) pau1MacAddr, &u1EncapType) != OSIX_SUCCESS)
    {

        if (BfdExtGetPortFromIfIndex (u4IfIndex, &u4IpPort) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        BfdExtEnqueueArp (u4Nhip, u4IpPort);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessPathInfo
 *
 * DESCRIPTION      : Function is used to construct bfd session path 
 *                    structure from BFD session table structure
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *
 * OUTPUT           : pBfdSessPathInfo - pointer to the session path 
 *                                       structure 
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillSessPathInfo (UINT4 u4ContextId,
                        tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                        tBfdMplsPathInfo * pBfdMplsPathInfo,
                        tBfdPktInfo * pBfdPktInfo,
                        tBfdSessionPathInfo * pBfdOffSessPathInfo)
{
    UINT4               u4PeerIp = 0;
    UINT4               u4NhIp = 0;
    UINT4               u4VlanIf = 0;
    tMplsLblInfo       *pOutLabelList = NULL;
    tMplsLblInfo       *pInLabelList = NULL;
    tBfdMibFsMIStdBfdSessEntry *pBfdMibFsMIStdBfdSessEntry = NULL;
    UINT1               u1ServiceType = 0;

    pBfdMibFsMIStdBfdSessEntry = &pBfdFsMIStdBfdSessEntry->MibObject;
    MEMCPY (&(pBfdOffSessPathInfo->BfdTxPathInfo),
            &(pBfdFsMIStdBfdSessEntry->BfdSessPathParams),
            sizeof (tBfdSessPathParams));

    pOutLabelList = pBfdOffSessPathInfo->LabelList.OutLabelList;
    pInLabelList = pBfdOffSessPathInfo->LabelList.InLabelList;

    MEMSET ((pBfdOffSessPathInfo->au1SrcMacAddr), 0, CFA_ENET_ADDR_LEN);
    /* Get Eggr If and Destination Mac address */
    if (((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) ==
         BFD_CHECK_PATH_TE_AVAIL) &&
        (pBfdMplsPathInfo->bRevPathFetchFail != OSIX_TRUE))
    {
        pBfdOffSessPathInfo->u4EggrItf =
            pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.u4OutIf;

        if (MEMCMP
            (pBfdOffSessPathInfo->au1DstMacAddr,
             pBfdMplsPathInfo->MplsTeTnlApiInfo.au1NextHopMac,
             CFA_ENET_ADDR_LEN) == 0)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:Tunnel NH Mac unavailable ");
            return OSIX_FAILURE;
        }
        else
        {

            MEMCPY (pBfdOffSessPathInfo->au1DstMacAddr,
                    pBfdMplsPathInfo->MplsTeTnlApiInfo.au1NextHopMac,
                    CFA_ENET_ADDR_LEN);
        }
        if (BfdExtGetIfIndexFromMplsTnlIf (pBfdOffSessPathInfo->u4EggrItf,
                                           &u4VlanIf, OSIX_TRUE) !=
            OSIX_SUCCESS)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:Fetching Ifindex "
                         "from TnlIf failed ");
            return OSIX_FAILURE;
        }

    }
    else if (((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_NONTE_AVAIL) ==
              BFD_CHECK_PATH_NONTE_AVAIL) &&
             (pBfdMplsPathInfo->bRevPathFetchFail != OSIX_TRUE))
    {
        pBfdOffSessPathInfo->u4EggrItf =
            pBfdMplsPathInfo->MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.u4OutIf;

        if (MEMCMP (pBfdOffSessPathInfo->au1DstMacAddr,
                    pBfdMplsPathInfo->MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.
                    au1NextHopMac, CFA_ENET_ADDR_LEN) == 0)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:LSP NH Mac unavailable ");
            return OSIX_FAILURE;
        }
        else
        {

            MEMCPY (pBfdOffSessPathInfo->au1DstMacAddr,
                    pBfdMplsPathInfo->MplsNonTeApiInfo.XcApiInfo.FwdOutSegInfo.
                    au1NextHopMac, CFA_ENET_ADDR_LEN);
        }
        if (BfdExtGetIfIndexFromMplsTnlIf (pBfdOffSessPathInfo->u4EggrItf,
                                           &u4VlanIf, OSIX_TRUE) !=
            OSIX_SUCCESS)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:Vlan If fetch failed ");
            return OSIX_FAILURE;
        }
    }
    else if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
             BFD_CHECK_PATH_PW_AVAIL)
    {
        pBfdOffSessPathInfo->u4EggrItf =
            pBfdMplsPathInfo->MplsPwApiInfo.unOutTnlInfo.u4OutIfIndex;

        if (MEMCMP (pBfdOffSessPathInfo->au1DstMacAddr,
                    pBfdMplsPathInfo->MplsPwApiInfo.au1NextHopMac,
                    CFA_ENET_ADDR_LEN) == 0)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:PW NH Mac unavailable ");
            return OSIX_FAILURE;
        }
        else
        {
            MEMCPY (pBfdOffSessPathInfo->au1DstMacAddr,
                    pBfdMplsPathInfo->MplsPwApiInfo.au1NextHopMac,
                    CFA_ENET_ADDR_LEN);
        }
        u4VlanIf = pBfdOffSessPathInfo->u4EggrItf;
    }
    else
    {
        u4PeerIp =
            pBfdFsMIStdBfdSessEntry->LastRxCtrlPktParams.BfdPeerIpAddress.
            ip6_addr_u.u4WordAddr[0];
        if (u4PeerIp == 0)
        {
            /* Return Path is supposed to be IP return Path.
             * Since IP address is not learned, Fail as the peer
             * can't be reached */
            return OSIX_FAILURE;
        }

        /* Since there is no found forward path, fill based on IP */
        if (BfdExtGetEggrIfNhFromDestIp (u4ContextId, u4PeerIp,
                                         &(pBfdOffSessPathInfo->u4EggrItf),
                                         &u4NhIp) != OSIX_SUCCESS)
        {
            BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_FETCH_NEXT_HOP_ADDRESS_FAIL,
                     "BfdOffFillSessEncapParams", u4PeerIp);
            return OSIX_FAILURE;
        }

        if (BfdOffResolveArpForNh
            (u4NhIp, pBfdOffSessPathInfo->u4EggrItf,
             pBfdOffSessPathInfo->au1DstMacAddr) != OSIX_SUCCESS)
        {
            return BFD_OFF_FAILED;
        }
        u4VlanIf = pBfdOffSessPathInfo->u4EggrItf;

    }

    /* Get source mac */
    if (BfdExtGetIfMacAddr (u4ContextId,
                            u4VlanIf, pBfdOffSessPathInfo->au1SrcMacAddr) !=
        OSIX_SUCCESS)
    {
        /*source mac fetch failed */
        BfdOffDebug (u4ContextId,
                     "BfdOffFillSessPathInfo:Source Mac fetch fail ");
        return OSIX_FAILURE;
    }

    /* Get Vlan Information */
    if (BfdExtGetIfVlan (u4VlanIf, &pBfdOffSessPathInfo->u2Vlan) !=
        OSIX_SUCCESS)
    {
        /*vlan fetch failed */
        BfdOffDebug (u4ContextId, "BfdOffFillSessPathInfo:Vlan fetch fail ");
        return OSIX_FAILURE;
    }

    if (pBfdPktInfo != NULL)
    {
        /* Get the InLabel information from the In packet information */
        MEMCPY (pInLabelList, pBfdPktInfo->MplsHdr,
                sizeof (pBfdPktInfo->MplsHdr));
    }

    /* Get the label information from path information */
    if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
        BFD_PATH_TYPE_MEP && (pBfdMplsPathInfo->bRevPathFetchFail != OSIX_TRUE))
    {
        /* Path type is ME, get the underlying path information and
         * fill the labels */
        u1ServiceType = pBfdMplsPathInfo->MplsMegApiInfo.u1ServiceType;

        pBfdOffSessPathInfo->u1ServiceType = u1ServiceType;
        if (u1ServiceType == OAM_SERVICE_TYPE_PW)
        {
            if (BfdGetOutLabelInfoFromPw (pBfdMplsPathInfo,
                                          pBfdMibFsMIStdBfdSessEntry,
                                          pOutLabelList,
                                          &(pBfdOffSessPathInfo->LabelList.
                                            u1OutLblStkCnt)) != OSIX_SUCCESS)
            {
                BfdOffDebug (u4ContextId, "BfdOffFillSessPathInfo:PW Out Label "
                             "fetch failed ");
                return OSIX_FAILURE;
            }

            if (pBfdPktInfo == NULL)
            {
                if (BfdGetInLabelInfoFromPw (pBfdMplsPathInfo,
                                             pBfdMibFsMIStdBfdSessEntry,
                                             pInLabelList,
                                             &(pBfdOffSessPathInfo->LabelList.
                                               u1InLblStkCnt)) != OSIX_SUCCESS)
                {
                    BfdOffDebug (u4ContextId, "BfdOffFillSessPathInfo:PW In "
                                 "Label fetch failed ");
                    return OSIX_FAILURE;
                }
            }

            pBfdOffSessPathInfo->u4PhyPort =
                pBfdMplsPathInfo->MplsPwApiInfo.u4PhyPort;
            pBfdOffSessPathInfo->u4PwIfIndex =
                pBfdMplsPathInfo->MplsPwApiInfo.u4PwIfIndex;
            pBfdOffSessPathInfo->u4PwVcIndex =
                pBfdMplsPathInfo->MplsPwApiInfo.u4PwVcIndex;
            pBfdOffSessPathInfo->u2VplsFdbId =
                pBfdMplsPathInfo->MplsPwApiInfo.u2VplsFdbId;
            pBfdOffSessPathInfo->u4VpnId =
                pBfdMplsPathInfo->MplsPwApiInfo.u4VpnId;
            pBfdOffSessPathInfo->u1CcSelected =
                pBfdMplsPathInfo->MplsPwApiInfo.u1CcSelected;
        }
        else if (u1ServiceType == OAM_SERVICE_TYPE_LSP)
        {
            if (BfdGetLabelFromOutSegInfo (pBfdMibFsMIStdBfdSessEntry,
                                           &pBfdMplsPathInfo->MplsTeTnlApiInfo.
                                           XcApiInfo.FwdOutSegInfo,
                                           pOutLabelList,
                                           &(pBfdOffSessPathInfo->LabelList.
                                             u1OutLblStkCnt)) != OSIX_SUCCESS)
            {
                BfdOffDebug (u4ContextId, "BfdOffFillSessPathInfo: LSP Out "
                             "Label fetch failed ");
                return OSIX_FAILURE;
            }

            if (pBfdPktInfo == NULL)
            {
                if (BfdGetLabelFromInSegInfo (pBfdMibFsMIStdBfdSessEntry,
                                              &pBfdMplsPathInfo->
                                              MplsTeTnlApiInfo.XcApiInfo.
                                              RevInSegInfo, pInLabelList,
                                              &(pBfdOffSessPathInfo->LabelList.
                                                u1InLblStkCnt)) != OSIX_SUCCESS)
                {
                    BfdOffDebug (u4ContextId, "BfdOffFillSessPathInfo: LSP In "
                                 "Label fetch failed ");
                    return OSIX_FAILURE;
                }
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType !=
                BFD_ENCAP_TYPE_MPLS_IP)
            {
                pOutLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.
                              XcApiInfo.FwdOutSegInfo.u1LblStkCnt - 1].u1SI = 0;

                /* MPLS-TP Path, fill the GAL LABEL after the tunnel label */
                pOutLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.
                              XcApiInfo.FwdOutSegInfo.u1LblStkCnt].u4Label =
                    MPLS_GAL_LABEL;
                pOutLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
                              FwdOutSegInfo.u1LblStkCnt].u1SI = 1;
                pOutLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
                              FwdOutSegInfo.u1LblStkCnt].u1Ttl = 1;

                (pBfdOffSessPathInfo->LabelList.u1OutLblStkCnt)++;

                if (pBfdPktInfo == NULL)
                {
                    pInLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.
                                 XcApiInfo.RevInSegInfo.u1LblStkCnt - 1].u1SI =
                        0;

                    /* MPLS-TP Path, fill the GAL LABEL after the tunnel label */
                    pInLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.
                                 XcApiInfo.RevInSegInfo.u1LblStkCnt].u4Label =
                        MPLS_GAL_LABEL;
                    pInLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
                                 RevInSegInfo.u1LblStkCnt].u1SI = 1;
                    pInLabelList[pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.
                                 RevInSegInfo.u1LblStkCnt].u1Ttl = 1;

                    (pBfdOffSessPathInfo->LabelList.u1InLblStkCnt)++;
                }
            }

            pBfdOffSessPathInfo->u4PhyPort =
                pBfdMplsPathInfo->MplsTeTnlApiInfo.XcApiInfo.FwdOutSegInfo.
                u4PhyPort;
        }
        else
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:Unknown service type for MEP");
            return OSIX_FAILURE;
        }
    }

    /* Get the label information in case path is PW */
    if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
        BFD_PATH_TYPE_PW)
    {
        if (BfdGetOutLabelInfoFromPw (pBfdMplsPathInfo,
                                      pBfdMibFsMIStdBfdSessEntry,
                                      pOutLabelList,
                                      &(pBfdOffSessPathInfo->LabelList.
                                        u1OutLblStkCnt)) != OSIX_SUCCESS)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffFillSessPathInfo:PW Label fetch failed ");
            return OSIX_FAILURE;
        }
    }

    /* Fill the PHB configured */
    pOutLabelList[0].u1Exp =
        (UINT1) (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue);

    /* Get the label information in case path is TE */
    if ((pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType ==
         BFD_PATH_TYPE_TE_IPV4) &&
        (pBfdMplsPathInfo->bRevPathFetchFail != OSIX_TRUE))
    {
        if (BfdGetLabelFromOutSegInfo (pBfdMibFsMIStdBfdSessEntry,
                                       &pBfdMplsPathInfo->MplsTeTnlApiInfo.
                                       XcApiInfo.FwdOutSegInfo,
                                       pOutLabelList,
                                       &(pBfdOffSessPathInfo->LabelList.
                                         u1OutLblStkCnt)) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillIpUdpHdrInfo
 *
 * DESCRIPTION      : Function is used to construct IP UDP Hdr information 
 *                    structure from BFD session table structure
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *
 * OUTPUT           : pBfdIpUdpHdrParams - pointer to the Udp hdr information
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillIpUdpHdrInfo (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                        tBfdIpUdpHdrParams * pBfdIpUdpHdrParams)
{
    INT4                i4Result = OSIX_SUCCESS;

    /* Filling IP UDP parameters */
    pBfdIpUdpHdrParams->UdpHdrInfo.u2SrcUdpPort = BFD_UDP_SRC_PORT;

    pBfdIpUdpHdrParams->UdpHdrInfo.u2DstUdpPort = BFD_UDP_DEST_PORT;

    /* Filling IP parameters */
    pBfdIpUdpHdrParams->unHdrParams.IpHdrInfo.u4DestIpAddr =
        pBfdFsMIStdBfdSessEntry->LastRxCtrlPktParams.BfdPeerIpAddress.
        ip6_addr_u.u4WordAddr[0];

    /* For source IP, Fetch the router ID */
    i4Result =
        BfdExtGetRouterId (pBfdFsMIStdBfdSessEntry->MibObject.
                           u4FsMIStdBfdContextId,
                           &(pBfdIpUdpHdrParams->unHdrParams.IpHdrInfo.
                             u4SrcIpAddr));

    if (i4Result != OSIX_SUCCESS)
    {
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_FETCH_ROUTER_ID_FAILED, "BfdOffFillIpUdpHdrInfo");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdOffFillRxSrcMepTlv                                      *
 *                                                                           *
 * Description  : Fills the expected Source MEP tlv                          *
 *                                                                           *
 * Input        : pMplsPathInfo - Pointer to entire path information.        *
 *                                tMplsMegApiInfo which contains MEG info.   *
 *                                It gives source MEP tlv details to fill    *
 *                                the tlv.                                   *
 *                                                                           *
 * Output       : pMplsAchTlv - Pointer to the tMplsMepTlv structure which   *
 *                              contains MEP tlv info based on the operator  *
 *                              type                                         *
 *                pAchTlvHdr  - Pointer to the tMplsAchTlvHdr structure which*
 *                              contains ACH TLV Header length               *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
BfdOffFillRxSrcMepTlv (tBfdMplsPathInfo * pMplsPathInfo,
                       tMplsAchTlv * pMplsAchTlv, tMplsAchTlvHdr * pAchTlvHdr)
{
    tMplsMegApiInfo    *pMplsMegInfo = NULL;
    tMplsIccMepTlv     *pIccMepTlv = NULL;
    tMplsIpLspMepTlv   *pIpLspMepTlv = NULL;
    tMplsIpPwMepTlv    *pIpPwMepTlv = NULL;
    tPwApiInfo         *pPwApiInfo = NULL;
    tTeTnlApiInfo      *pMplsTnlInfo = NULL;
    tMplsMepTlv        *pMplsMepTlv = NULL;

    pMplsMepTlv = &pMplsAchTlv->unAchValue.MepTlv;
    pMplsMegInfo = &pMplsPathInfo->MplsMegApiInfo;
    pMplsTnlInfo = &pMplsPathInfo->MplsTeTnlApiInfo;
    if (pMplsMegInfo->u4OperatorType == OAM_OPERATOR_TYPE_ICC)
    {
        pIccMepTlv = &pMplsMepTlv->unMepTlv.IccMepTlv;
        pMplsAchTlv->u2TlvType = MPLS_ACH_ICC_MEP_TLV;
        pMplsAchTlv->u2TlvLen = (UINT2) (sizeof (tMplsIccMepTlv));
        /* Copy the MEG Index (ICC and UMC) */
        MEMCPY (pIccMepTlv->au1Icc, pMplsMegInfo->au1Icc,
                STRLEN (pMplsMegInfo->au1Icc));
        MEMCPY (pIccMepTlv->au1Umc, pMplsMegInfo->au1Umc,
                STRLEN (pMplsMegInfo->au1Umc));

        /* Copy the MEP Index */
        pIccMepTlv->u2MepIndex = (UINT2) pMplsMegInfo->u4IccSinkMepIndex;
    }
    else
    {
        if (pMplsMegInfo->u1ServiceType == OAM_SERVICE_TYPE_LSP)
        {
            pMplsAchTlv->u2TlvType = MPLS_ACH_IP_LSP_MEP_TLV;
            pMplsAchTlv->u2TlvLen = (UINT2) sizeof (tMplsIpLspMepTlv);

            /* Copy Global Id and Node Id */
            pIpLspMepTlv = &pMplsMepTlv->unMepTlv.IpLspMepTlv;
            if (pMplsTnlInfo->u1TnlRole == MPLS_TE_EGRESS)
            {

                pIpLspMepTlv->GlobalNodeId.u4GlobalId =
                    pMplsTnlInfo->TnlLspId.SrcNodeId.MplsGlobalNodeId.
                    u4GlobalId;
                pIpLspMepTlv->GlobalNodeId.u4NodeId =
                    pMplsTnlInfo->TnlLspId.SrcNodeId.MplsGlobalNodeId.u4NodeId;

            }
            else
            {
                pIpLspMepTlv->GlobalNodeId.u4GlobalId =
                    pMplsTnlInfo->TnlLspId.DstNodeId.MplsGlobalNodeId.
                    u4GlobalId;
                pIpLspMepTlv->GlobalNodeId.u4NodeId =
                    pMplsTnlInfo->TnlLspId.DstNodeId.MplsGlobalNodeId.u4NodeId;
            }
            /* Copy Tunnel Number and LSP Number */
            pIpLspMepTlv->u2TnlNum = (UINT2) pMplsMegInfo->
                MegMplsTnlId.u4SrcTnlNum;
            pIpLspMepTlv->u2LspNum = (UINT2) pMplsMegInfo->
                MegMplsTnlId.u4LspNum;
        }
        else
        {
            pMplsAchTlv->u2TlvType = MPLS_ACH_IP_PW_MEP_TLV;
            /* Copy Global Id and Node Id */
            pIpPwMepTlv = &pMplsMepTlv->unMepTlv.IpPwMepTlv;
            pPwApiInfo = &pMplsPathInfo->MplsPwApiInfo;
            pIpPwMepTlv->u4GlobalId =
                pPwApiInfo->MplsPwPathId.DstNodeId.MplsGlobalNodeId.u4GlobalId;
            pIpPwMepTlv->u4NodeId =
                pPwApiInfo->MplsPwPathId.DstNodeId.MplsGlobalNodeId.u4NodeId;
            /* Copy AGI and AC Id */
            pIpPwMepTlv->u1AgiLen = pPwApiInfo->MplsPwPathId.u1AgiLen;
            pIpPwMepTlv->u1AgiType = pPwApiInfo->MplsPwPathId.u1AgiType;
            MEMCPY (pIpPwMepTlv->au1Agi, pPwApiInfo->MplsPwPathId.au1Agi,
                    pIpPwMepTlv->u1AgiLen);
            pIpPwMepTlv->u4AcId = pPwApiInfo->MplsPwPathId.u4DstAcId;
            /* Header Length : AC ID(4), Global ID(4), Node ID(4),
             * AGI Type(1), Len(1) and Value(defined in Len) */
            pMplsAchTlv->u2TlvLen = (UINT2) (BFD_ACH_TLV_LEN * sizeof (UINT4) +
                                             sizeof (UINT2) +
                                             pIpPwMepTlv->u1AgiLen);
        }
    }
    pAchTlvHdr->u2TlvHdrLen = (UINT2) (pMplsAchTlv->u2TlvLen + sizeof (UINT4));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessEncapParams
 *
 * DESCRIPTION      : Function is used to fill encapsulation parameters for 
 *                    both Tx and RX. Incase  pBfdPktInfo is NULL, Rx related 
 *                    info is not filled
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *                    pBfdMplsPathInfo - structure containing path information
 *                    pBfdPktInfo - Rx packet info. This can be NULL in case 
 *                    RX is not needed
 *                    u4EncapType - Encapsulation to be used
 *                        
 *
 * OUTPUT           : pBfdTxCtrlPktEncapInfo - Tx encap params
 *                          tBfdCtrlPktEncapParams - Rx encap params
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillSessEncapParams (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                           tBfdMplsPathInfo * pBfdMplsPathInfo,
                           tBfdPktInfo * pBfdPktInfo,
                           tBfdCtrlPktEncapParams * pBfdTxCtrlPktEncapInfo,
                           tBfdCtrlPktEncapParams * pBfdRxCtrlPktEncapInfo,
                           UINT4 u4EncapType)
{
    tBfdAchParams      *pBfdAchParams = NULL;
    tBfdIpUdpHdrParams *pBfdIpUdpHdrParams = NULL;

    if (pBfdPktInfo != NULL)
    {
        /* Filling Rx encap parameters */
        pBfdRxCtrlPktEncapInfo->eBfdEncapType = pBfdPktInfo->u1EncapType;

        if ((pBfdRxCtrlPktEncapInfo->eBfdEncapType == BFD_ENCAP_IPV4) ||
            (pBfdRxCtrlPktEncapInfo->eBfdEncapType == BFD_ENCAP_MPLS_IPV4))
        {
            pBfdIpUdpHdrParams =
                &(pBfdRxCtrlPktEncapInfo->unEncapParams.BfdIpUdpHdrInfo);
            pBfdIpUdpHdrParams->unHdrParams.IpHdrInfo.u4SrcIpAddr =
                pBfdPktInfo->PeerAddr.u4_addr[0];
        }
        else if ((pBfdRxCtrlPktEncapInfo->eBfdEncapType ==
                  BFD_ENCAP_MPLS_ACH) ||
                 (pBfdRxCtrlPktEncapInfo->eBfdEncapType ==
                  BFD_ENCAP_MPLS_IPV4_ACH))
        {
            /* MPLS ACH case */
            if (pBfdRxCtrlPktEncapInfo->eBfdEncapType == BFD_ENCAP_MPLS_ACH)
            {
                pBfdAchParams =
                    &(pBfdRxCtrlPktEncapInfo->unEncapParams.BfdMplsTpAchHdrInfo.
                      BfdAchInfo);
            }
            else
            {
                pBfdAchParams =
                    &(pBfdRxCtrlPktEncapInfo->unEncapParams.
                      BfdIpMplsTpAchHdrInfo.BfdAchInfo);
            }

            MEMCPY (&(pBfdAchParams->MplsAchTlv),
                    &(pBfdPktInfo->MplsAchTlv), sizeof (tMplsAchTlv));

            /*ACH Header Info */
            pBfdAchParams->BfdAchHdrInfo.u2ChannelType =
                pBfdPktInfo->u2ChannelType;
            pBfdAchParams->BfdAchHdrInfo.u1AchNibble = BFD_HEADER_ACH;
            pBfdAchParams->BfdAchHdrInfo.u1AchVersion =
                BFD_PACKET_ACH_HEADER_VERSION;
            pBfdAchParams->BfdAchHdrInfo.u1AchFlags =
                BFD_PACKET_ACH_HEADER_FLAGS;

            /*ACH TLV header info */
            if (pBfdAchParams->MplsAchTlv.u2TlvLen != 0)
            {
                pBfdAchParams->AchTlvHdrInfo.u2TlvHdrLen =
                    (UINT2) (pBfdAchParams->MplsAchTlv.u2TlvLen +
                             BFD_PACKET_ACH_TLV_HDR_SIZE);
            }

            if (pBfdRxCtrlPktEncapInfo->eBfdEncapType ==
                BFD_ENCAP_MPLS_IPV4_ACH)
            {
                pBfdIpUdpHdrParams =
                    &(pBfdRxCtrlPktEncapInfo->unEncapParams.
                      BfdIpMplsTpAchHdrInfo.BfdPktIpUdpHdrInfo);

                pBfdIpUdpHdrParams->unHdrParams.IpHdrInfo.u4SrcIpAddr =
                    pBfdPktInfo->PeerAddr.u4_addr[0];
            }
        }
        else
        {
            /* unknown Rx encap parameters */
            return OSIX_FAILURE;
        }
    }

    /*Fill the expected Source mep tlv in Rx */
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode ==
        BFD_SESS_MODE_CCV)
    {
        pBfdAchParams =
            &(pBfdRxCtrlPktEncapInfo->unEncapParams.BfdMplsTpAchHdrInfo.
              BfdAchInfo);

        /* Fill Source MEP TLV */
        BfdOffFillRxSrcMepTlv (pBfdMplsPathInfo,
                               &(pBfdAchParams->MplsAchTlv),
                               &(pBfdAchParams->AchTlvHdrInfo));
    }

    pBfdTxCtrlPktEncapInfo->eBfdEncapType = u4EncapType;

    /*Filling the Encapsulations based on the Configured Value */
    if ((u4EncapType == BFD_ENCAP_IPV4) ||
        (u4EncapType == BFD_ENCAP_MPLS_IPV4) ||
        (u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH))
    {
        if ((u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH))
        {
            pBfdIpUdpHdrParams =
                &(pBfdTxCtrlPktEncapInfo->unEncapParams.BfdIpMplsTpAchHdrInfo.
                  BfdPktIpUdpHdrInfo);
        }
        else
        {
            pBfdIpUdpHdrParams =
                &(pBfdTxCtrlPktEncapInfo->unEncapParams.BfdIpUdpHdrInfo);
        }

        if (BfdOffFillIpUdpHdrInfo (pBfdFsMIStdBfdSessEntry,
                                    pBfdIpUdpHdrParams) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    if ((u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH) ||
        (u4EncapType == BFD_ENCAP_MPLS_ACH))
    {
        pBfdAchParams =
            &(pBfdTxCtrlPktEncapInfo->unEncapParams.BfdMplsTpAchHdrInfo.
              BfdAchInfo);

        /* Construct the source MEP TLV in case its CV */
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode ==
            BFD_SESS_MODE_CCV)
        {
            /* Fill Source MEP TLV */
            BfdTxFillSrcMepTlv (pBfdMplsPathInfo,
                                &(pBfdAchParams->MplsAchTlv),
                                &(pBfdAchParams->AchTlvHdrInfo));

            if (u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH)
            {
                /*ACH Header Info */
                pBfdAchParams->BfdAchHdrInfo.u2ChannelType =
                    BFD_CH_TYPE_CV_IPV4;
            }
            else
            {
                /*ACH Header Info */
                pBfdAchParams->BfdAchHdrInfo.u2ChannelType = BFD_CH_TYPE_CV_BFD;
            }
        }
        else
        {
            if (u4EncapType == BFD_ENCAP_MPLS_IPV4_ACH)
            {
                /*ACH Header Info for CC type */
                pBfdAchParams->BfdAchHdrInfo.u2ChannelType =
                    BFD_CH_TYPE_CC_IPV4;
            }
            else
            {
                /*ACH Header Info for CC type */
                pBfdAchParams->BfdAchHdrInfo.u2ChannelType = BFD_CH_TYPE_CC_BFD;
            }
        }

        pBfdAchParams->BfdAchHdrInfo.u1AchNibble = BFD_HEADER_ACH;
        pBfdAchParams->BfdAchHdrInfo.u1AchVersion =
            BFD_PACKET_ACH_HEADER_VERSION;
        pBfdAchParams->BfdAchHdrInfo.u1AchFlags = BFD_PACKET_ACH_HEADER_FLAGS;

    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessParams
 *
 * DESCRIPTION      : Function is used to construct bfd session params 
 *                    structure from BFD session table structure
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *                        pBfdMplsPathInfo - Mpls Path info
 *                        pBfdGlobalConfigTableEntry - global config table
 *                        pBfdPktInfo - Rx packet info
 *                        bBfdTxPktPollBit - if true poll bit will be set
 *                        bBfdTxPktFinalBit - if true final bit will be set
 *                        u4EncapType - Provides the Encaptype to be filled
 *
 * OUTPUT           : pBfdParams - pointer to the session params structure 
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
VOID
BfdOffFillSessParams (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                      tBfdMplsPathInfo * pBfdMplsPathInfo,
                      tBfdMibFsMIStdBfdGlobalConfigTableEntry *
                      pBfdGlobalConfigTableEntry, tBfdPktInfo * pBfdPktInfo,
                      tBfdParams * pBfdParams, BOOL1 bBfdTxPktPollBit,
                      BOOL1 bBfdTxPktFinalBit, UINT4 u4EncapType)
{

    tBfdGblParams      *pBfdGblInfo = NULL;
    tBfdSessParams     *pBfdSessInfo = NULL;
    tBfdCtrlPktParams  *pBfdCtrlPktInfo = NULL;
    tBfdCtrlPktEncapParams *pBfdTxCtrlPktEncapInfo = NULL;
    tBfdCtrlPktEncapParams *pBfdRxCtrlPktEncapInfo = NULL;
    tBfdMibFsMIStdBfdSessEntry *pBfdMibSessTable = NULL;
    BOOL1               bCheckEgr = OSIX_FALSE;

    pBfdGblInfo = &(pBfdParams->BfdGblInfo);
    pBfdSessInfo = &(pBfdParams->BfdSessInfo);
    pBfdCtrlPktInfo = &(pBfdParams->BfdCtrlPktInfo);
    pBfdTxCtrlPktEncapInfo = &(pBfdParams->BfdTxCtrlPktEncapInfo);
    pBfdRxCtrlPktEncapInfo = &(pBfdParams->BfdRxCtrlPktEncapInfo);
    pBfdMibSessTable = &pBfdFsMIStdBfdSessEntry->MibObject;

    /* Filling bfd global info */
    pBfdGblInfo->u4BfdGblSlowTxIntvl =
        pBfdGlobalConfigTableEntry->u4FsMIBfdGblSlowTxIntvl;

    /* Filling Session information */
    pBfdSessInfo->eBfdSessType = pBfdMibSessTable->i4FsMIStdBfdSessType;
    pBfdSessInfo->eBfdSessOperMode = pBfdMibSessTable->i4FsMIStdBfdSessOperMode;
    pBfdSessInfo->eBfdSessCcCvMode = pBfdMibSessTable->i4FsMIBfdSessMode;
    pBfdSessInfo->eBfdSessSrcAddrType =
        pBfdMibSessTable->i4FsMIStdBfdSessSrcAddrType;
    pBfdSessInfo->eBfdSessDstAddrType =
        pBfdMibSessTable->i4FsMIStdBfdSessDstAddrType;
    pBfdSessInfo->eBfdSessRole = pBfdMibSessTable->i4FsMIBfdSessRole;
    pBfdSessInfo->u4NegoTxInterval =
        pBfdMibSessTable->u4FsMIStdBfdSessNegotiatedInterval;
    pBfdSessInfo->u4DetectionTimeIntvl =
        pBfdFsMIStdBfdSessEntry->u4NegRemoteTxTimeIntrvl *
        BFD_SESS_NEG_DETECT_MULT (pBfdFsMIStdBfdSessEntry);
    pBfdSessInfo->u4BfdSessInterface =
        (UINT4) pBfdMibSessTable->i4FsMIStdBfdSessInterface;
    pBfdSessInfo->u1BfdSessNegoDetectMult =
        (UINT1) pBfdMibSessTable->u4FsMIStdBfdSessNegotiatedDetectMult;

    pBfdSessInfo->bBfdSessTmrNegotiate =
        (BOOL1) pBfdMibSessTable->i4FsMIBfdSessTmrNegotiate;

    if (pBfdPktInfo != NULL)
    {
        /* Filling BFD control packet information */
        MEMCPY (&pBfdCtrlPktInfo->BfdRxCtrlPktParams,
                &pBfdPktInfo->BfdCtrlPacket.BfdPktParams,
                sizeof (tBfdRxCtrlPktParams));
    }

    pBfdCtrlPktInfo->eBfdSessTxPktDiag = pBfdMibSessTable->i4FsMIStdBfdSessDiag;
    pBfdCtrlPktInfo->eBfdSessState = pBfdMibSessTable->i4FsMIStdBfdSessState;
    pBfdCtrlPktInfo->eBfdSessAuthType =
        pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationType;
    pBfdCtrlPktInfo->u4BfdSessRemoteDiscr =
        pBfdMibSessTable->u4FsMIStdBfdSessRemoteDiscr;
    pBfdCtrlPktInfo->u4BfdSessTxPktDesiredMinTxInterval =
        pBfdMibSessTable->u4FsMIStdBfdSessDesiredMinTxInterval;
    pBfdCtrlPktInfo->u4BfdSessTxPktReqMinRxInterval =
        pBfdMibSessTable->u4FsMIStdBfdSessReqMinRxInterval;
    MEMCPY (pBfdCtrlPktInfo->au1BfdSessAuthKey,
            pBfdMibSessTable->au1FsMIStdBfdSessAuthenticationKey,
            BFD_MAX_PASSWD_LEN);
    pBfdCtrlPktInfo->u1BfdSessTxPktVersionNumber =
        (UINT1) pBfdMibSessTable->u4FsMIStdBfdSessVersionNumber;
    pBfdCtrlPktInfo->u1BfdSessTxPktDetectMult =
        (UINT1) pBfdMibSessTable->u4FsMIStdBfdSessDetectMult;
    pBfdCtrlPktInfo->u1BfdCtrlPktLen = BFD_CTRL_PKT_LEN;
    pBfdCtrlPktInfo->u1BfdSessAuthLength =
        (UINT1) pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationKeyLen;
    pBfdCtrlPktInfo->u1BfdSessAuthKeyLen =
        (UINT1) pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationKeyLen;
    pBfdCtrlPktInfo->i1BfdSessAuthKeyID =
        (INT1) pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationKeyID;
    pBfdCtrlPktInfo->bBfdTxPktPollBit = bBfdTxPktPollBit;
    pBfdCtrlPktInfo->bBfdTxPktFinalBit = bBfdTxPktFinalBit;
    pBfdCtrlPktInfo->bBfdSessTxPktCtrlPlaneIndepFlag =
        (BOOL1) pBfdMibSessTable->i4FsMIStdBfdSessControlPlaneIndepFlag;
    pBfdCtrlPktInfo->bAuthPresFlag =
        (BOOL1) pBfdMibSessTable->i4FsMIStdBfdSessAuthPresFlag;
    pBfdCtrlPktInfo->bTxPktDemandMode =
        (BOOL1) pBfdMibSessTable->i4FsMIStdBfdSessDemandModeDesiredFlag;
    pBfdCtrlPktInfo->bBfdSessMultipointFlag =
        (BOOL1) pBfdMibSessTable->i4FsMIStdBfdSessMultipointFlag;

    BfdExtChkSessPathEgrs (pBfdFsMIStdBfdSessEntry, &bCheckEgr);

    /*Fill type of monitoring */
    if (pBfdFsMIStdBfdSessEntry->BfdSessPathParams.ePathType
        == BFD_PATH_TYPE_MEP)
    {
        /*Check if the ME underlying path is PW. If so, this is not 
         * independent monitoring */
        if (pBfdMplsPathInfo->bRevPathFetchFail == OSIX_TRUE)
        {
            pBfdSessInfo->eBfdSessMonMode = BFD_MON_INDEPENDENT_EGRESS;
        }
        else
        {
            if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
                BFD_CHECK_PATH_PW_AVAIL)
            {
                pBfdSessInfo->eBfdSessMonMode = BFD_MON_BIDIRECTIONAL;
            }
            else if ((pBfdMplsPathInfo->
                      u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) !=
                     BFD_CHECK_PATH_TE_AVAIL)
            {
                pBfdSessInfo->eBfdSessMonMode = BFD_MON_BIDIRECTIONAL;
            }
            else if (pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlMode ==
                     TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
            {
                pBfdSessInfo->eBfdSessMonMode = BFD_MON_BIDIRECTIONAL;
            }
            else if (bCheckEgr == OSIX_FALSE)
            {
                pBfdSessInfo->eBfdSessMonMode = BFD_MON_INDEPENDENT_INGRESS;
            }
            else if (bCheckEgr == OSIX_TRUE)
            {
                pBfdSessInfo->eBfdSessMonMode = BFD_MON_INDEPENDENT_EGRESS;
            }
        }
    }
    else
    {
        pBfdSessInfo->eBfdSessMonMode = BFD_MON_BIDIRECTIONAL;
    }

    /* Fill the encapsulation parameters */
    BfdOffFillSessEncapParams (pBfdFsMIStdBfdSessEntry,
                               pBfdMplsPathInfo,
                               pBfdPktInfo,
                               pBfdTxCtrlPktEncapInfo,
                               pBfdRxCtrlPktEncapInfo, u4EncapType);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillTxPkt
 *
 * DESCRIPTION      : Function is used to construct bfd session tx  
 *                    packet from BFD session table structure
 *
 * INPUT            : pBfdFsMIStdBfdSessEntry - pointer to session table 
 *                                              structure
 *                        pBfdMplsPathInfo - Mpls Path info
 *                        pBfdOffSessPathInfo - Session path information
 *
 * OUTPUT           : pu1BfdTxCtrlPkt - pointer to the tx packet 
 *                    pu4EncapType - Encap type to be used
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillTxPkt (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                 tBfdMplsPathInfo * pBfdMplsPathInfo,
                 tBfdSessionPathInfo * pBfdOffSessPathInfo,
                 tBfdOffPktBuf * pBfdTxCtrlPkt, UINT4 *pu4EncapType)
{
    tEnetV2Header       EnetV2Hdr;
    UINT4               u4IfIndex = 0;
    UINT2               u2Tag = 0;
    UINT2               u2Protocol = 0;
    UINT1               au1VlanTag[VLAN_TAG_PID_LEN];
    UINT1               au1DestMac[MAC_ADDR_LEN];
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;

    MEMSET (au1VlanTag, 0, sizeof (au1VlanTag));
    MEMSET (au1DestMac, 0, sizeof (au1DestMac));
    MEMSET (&EnetV2Hdr, 0, sizeof (tEnetV2Header));

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (BFD_MAX_PKT_LEN, 0)) == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Get destination mac and source mac */
    MEMCPY (EnetV2Hdr.au1SrcAddr,
            pBfdOffSessPathInfo->au1SrcMacAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (EnetV2Hdr.au1DstAddr,
            pBfdOffSessPathInfo->au1DstMacAddr, CFA_ENET_ADDR_LEN);

    /*Vlan Tag */
    u2Protocol = OSIX_HTONS (VLAN_PROTOCOL_ID);
    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);
    u2Tag = (UINT2) (u2Tag | (pBfdOffSessPathInfo->u2Vlan & VLAN_ID_MASK));
    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));
    MEMCPY (au1VlanTag, (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);
    MEMCPY (&au1VlanTag[VLAN_TYPE_OR_LEN_SIZE], (UINT1 *) &u2Tag,
            VLAN_TAG_SIZE);

    if (pBfdMplsPathInfo->bRevPathFetchFail == OSIX_TRUE)
    {
        /* Currently when reverse path fetch fails, only IP return path is 
         * used. To check the validity of using IP return path is done else
         * where*/
        *pu4EncapType = BFD_ENCAP_IPV4;

        if (CRU_BUF_Copy_OverBufChain (pBuf,
                                       pBfdFsMIStdBfdSessEntry->BfdCntlPktBuf.
                                       au1PacketBuf, BFD_CTRL_PKT_OFFSET,
                                       pBfdFsMIStdBfdSessEntry->BfdCntlPktBuf.
                                       u4PacketLen) == CRU_FAILURE)
        {
            /* Failed to Copy BFD Control Packet to CRU Buf */
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return OSIX_FAILURE;
        }

        CRU_BUF_Move_ValidOffset (pBuf, BFD_CTRL_PKT_OFFSET);

        /* Form and Fill IP/UDP Header */
        if (BfdTxFillIpUdpHdr
            (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
             BFD_HEADER_IPV4, pBuf) == OSIX_FAILURE)
        {
            /* Filling IP/UDP Header failed */
            BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_FILL_IP_UDP_HEADER_FAILED);
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return OSIX_FAILURE;
        }
        EnetV2Hdr.u2LenOrType = OSIX_HTONS (CFA_ENET_IPV4);
    }
    else
    {
        if (BfdTxConstructPacket
            (pBfdFsMIStdBfdSessEntry,
             pBfdMplsPathInfo, &u4IfIndex, au1DestMac, pBuf, pu4EncapType) !=
            OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return OSIX_FAILURE;
        }
        EnetV2Hdr.u2LenOrType = OSIX_HTONS (CFA_ENET_MPLS);
    }

    /* Put Data type */
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &(EnetV2Hdr.u2LenOrType),
                              sizeof (EnetV2Hdr.u2LenOrType));

    /* Put VLan Tag */
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1VlanTag, VLAN_TAG_PID_LEN);

    /* Put Dest and Source Mac */
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) (EnetV2Hdr.au1SrcAddr),
                              sizeof (EnetV2Hdr.au1SrcAddr));

    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) (EnetV2Hdr.au1DstAddr),
                              sizeof (EnetV2Hdr.au1DstAddr));

    pBfdTxCtrlPkt->u4Len = CRU_BUF_Copy_FromBufChain (pBuf,
                                                      pBfdTxCtrlPkt->pau1PktBuf,
                                                      0, BFD_MAX_PKT_LEN);

    CRU_BUF_Release_MsgBufChain (pBuf, 0);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessTblFromStats
 *
 * DESCRIPTION      : Function is used to construct bfd session 
 *                    structure from BFD session stats structure
 *
 * INPUT            : pBfdSessStats - pointer to session statistics 
 *                                              structure
 *
 * OUTPUT           : pBfdFsMIStdBfdSessEntry - pointer to the session  
 *                                              structure 
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillSessTblFromStats (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                            tBfdSessStats * pBfdSessStats)
{

    tBfdMibFsMIStdBfdSessEntry *pBfdFsGetMIStdBfdSessTable = NULL;
    pBfdFsGetMIStdBfdSessTable = &pBfdFsMIStdBfdSessEntry->MibObject;

    if (pBfdSessStats == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_TBL_EMPTY, "BfdOffFillSessTblFromStats",
                 "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    /* Filling pBfdFsGetMIStdBfdSessTable from pBfdSessStats */
    pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdSessPerfCtrlPktIn =
        pBfdSessStats->u4BfdSessPerfCtrlPktIn;
    pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdSessPerfCtrlPktOut =
        pBfdSessStats->u4BfdSessPerfCtrlPktOut;
    pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdSessPerfCtrlPktDrop =
        pBfdSessStats->u4BfdSessPerfCtrlPktDrop;
    pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdSessPerfCtrlPktDropLastTime =
        pBfdSessStats->u4BfdSessPerfCtrlPktDropLastTime;
    pBfdFsGetMIStdBfdSessTable->u4FsMIStdBfdSessPerfDiscTime =
        pBfdSessStats->u4BfdSessPerfDiscTime;

    BfdUtilUpdateOffPktCount
        (&pBfdFsGetMIStdBfdSessTable->u8FsMIStdBfdSessPerfCtrlPktInHC,
         &pBfdSessStats->u8BfdSessPerfCtrlPktInHC);

    BfdUtilUpdateOffPktCount
        (&pBfdFsGetMIStdBfdSessTable->u8FsMIStdBfdSessPerfCtrlPktOutHC,
         &pBfdSessStats->u8BfdSessPerfCtrlPktOutHC);

    BfdUtilUpdateOffPktCount
        (&pBfdFsGetMIStdBfdSessTable->u8FsMIStdBfdSessPerfCtrlPktDropHC,
         &pBfdSessStats->u8BfdSessPerfCtrlPktDropHC);

    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessPerfCCPktIn =
        pBfdSessStats->u4BfdSessPerfCCPktIn;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessPerfCCPktOut =
        pBfdSessStats->u4BfdSessPerfCCPktOut;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessPerfCVPktIn =
        pBfdSessStats->u4BfdSessPerfCVPktIn;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessPerfCVPktOut =
        pBfdSessStats->u4BfdSessPerfCVPktOut;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessMisDefCount =
        pBfdSessStats->u4BfdSessMisDefCount;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessLocDefCount =
        pBfdSessStats->u4BfdSessLocDefCount;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessRdiInCount =
        pBfdSessStats->u4BfdSessRdiInCount;
    pBfdFsGetMIStdBfdSessTable->u4FsMIBfdSessRdiOutCount =
        pBfdSessStats->u4BfdSessRdiOutCount;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffFillSessTblFromParams
 *
 * DESCRIPTION      : Function is used to construct bfd session 
 *                    structure from BFD session params structure
 *
 * INPUT            : tBfdParams - pointer to session params 
 *                                              structure
 *
 * OUTPUT           : pBfdFsMIStdBfdSessEntry - pointer to the session  
 *                                              structure 
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffFillSessTblFromParams (tBfdMibFsMIStdBfdSessEntry * pBfdMibSessTable,
                             tBfdParams * pBfdParams)
{
    tBfdSessParams     *pBfdSessInfo = NULL;
    tBfdCtrlPktParams  *pBfdCtrlPktInfo = NULL;
    pBfdSessInfo = &(pBfdParams->BfdSessInfo);
    pBfdCtrlPktInfo = &(pBfdParams->BfdCtrlPktInfo);

    /* Filling Session information */
    pBfdMibSessTable->i4FsMIStdBfdSessType = pBfdSessInfo->eBfdSessType;
    pBfdMibSessTable->i4FsMIStdBfdSessOperMode = pBfdSessInfo->eBfdSessOperMode;
    pBfdMibSessTable->i4FsMIBfdSessMode = pBfdSessInfo->eBfdSessCcCvMode;
    pBfdMibSessTable->i4FsMIStdBfdSessSrcAddrType =
        pBfdSessInfo->eBfdSessSrcAddrType;
    pBfdMibSessTable->i4FsMIStdBfdSessDstAddrType =
        pBfdSessInfo->eBfdSessDstAddrType;
    pBfdMibSessTable->i4FsMIBfdSessRole = pBfdSessInfo->eBfdSessRole;
    pBfdMibSessTable->u4FsMIStdBfdSessNegotiatedInterval =
        pBfdSessInfo->u4NegoTxInterval;
    pBfdMibSessTable->i4FsMIStdBfdSessInterface =
        pBfdSessInfo->u4BfdSessInterface;
    pBfdMibSessTable->u4FsMIStdBfdSessNegotiatedDetectMult =
        pBfdSessInfo->u1BfdSessNegoDetectMult;

    pBfdMibSessTable->i4FsMIBfdSessTmrNegotiate =
        pBfdSessInfo->bBfdSessTmrNegotiate;

    pBfdMibSessTable->i4FsMIStdBfdSessDiag = pBfdCtrlPktInfo->eBfdSessTxPktDiag;
    pBfdMibSessTable->i4FsMIStdBfdSessState = pBfdCtrlPktInfo->eBfdSessState;
    pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationType =
        pBfdCtrlPktInfo->eBfdSessAuthType;
    pBfdMibSessTable->u4FsMIStdBfdSessRemoteDiscr =
        pBfdCtrlPktInfo->u4BfdSessRemoteDiscr;
    pBfdMibSessTable->u4FsMIStdBfdSessDesiredMinTxInterval =
        pBfdCtrlPktInfo->u4BfdSessTxPktDesiredMinTxInterval;
    pBfdMibSessTable->u4FsMIStdBfdSessReqMinRxInterval =
        pBfdCtrlPktInfo->u4BfdSessTxPktReqMinRxInterval;
    pBfdMibSessTable->u4FsMIStdBfdSessVersionNumber =
        pBfdCtrlPktInfo->u1BfdSessTxPktVersionNumber;
    pBfdMibSessTable->u4FsMIStdBfdSessDetectMult =
        pBfdCtrlPktInfo->u1BfdSessTxPktDetectMult;

    pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationKeyLen =
        pBfdCtrlPktInfo->u1BfdSessAuthKeyLen;
    pBfdMibSessTable->i4FsMIStdBfdSessAuthenticationKeyID =
        pBfdCtrlPktInfo->i1BfdSessAuthKeyID;
    pBfdMibSessTable->i4FsMIStdBfdSessControlPlaneIndepFlag =
        pBfdCtrlPktInfo->bBfdSessTxPktCtrlPlaneIndepFlag;
    pBfdMibSessTable->i4FsMIStdBfdSessAuthPresFlag =
        pBfdCtrlPktInfo->bAuthPresFlag;
    MEMCPY (pBfdMibSessTable->au1FsMIStdBfdSessAuthenticationKey,
            pBfdCtrlPktInfo->au1BfdSessAuthKey,
            pBfdCtrlPktInfo->u1BfdSessAuthKeyLen);
    pBfdMibSessTable->i4FsMIStdBfdSessDemandModeDesiredFlag =
        pBfdCtrlPktInfo->bTxPktDemandMode;
    pBfdMibSessTable->i4FsMIStdBfdSessMultipointFlag =
        pBfdCtrlPktInfo->bBfdSessMultipointFlag;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessConfigure
 *
 * DESCRIPTION      : Function is used to create a BFD session in the 
 *                    BFD Offload module in NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *                    pBfdPktInfo - Rx packet information. This can be NULL
 *
 *                    eBfdSessHwStartBfdCallFlag - Flag indicating the 
 *                                                configuration type
 *                        bBfdTxPktPollBit - if true poll bit will be set
 *                        bBfdTxPktFinalBit - if true final bit will be set
 *
 * OUTPUT           : pBfdSessHwHandle - HW handle for the session. Returned 
 *                                         whenever the configuration type is 
 *                                         create
 *                    pu1Error   - Specifies the error code in case of offload
 *                                 failure
 * 
 * RETURNS          : OSIX_SUCCESS/BFD_NP_FAILED/BFD_OFF_FAILED
 * 
 **************************************************************************/
INT4
BfdOffSessConfigure (UINT4 u4ContextId,
                     tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                     tBfdPktInfo * pBfdPktInfo,
                     tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag,
                     BOOL1 bBfdTxPktPollBit,
                     BOOL1 bBfdTxPktFinalBit, tBfdHwhandle * pBfdSessHwHandle,
                     UINT1 *pu1Error)
{
    tBfdSessionHandle   BfdSessHandle;
    tBfdSessionPathInfo BfdSessPathInfo;
    tBfdMibFsMIStdBfdSessEntry *pBfdMibFsMIStdBfdSessEntry = NULL;
    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTableEntry = NULL;
    tBfdCtrlPktEncapParams *pEncapInfo = NULL;
    tBfdParams         *pBfdParams = NULL;
    INT4                i4Return = OSIX_FAILURE;
    UINT4               u4EncapType = 0;
    tBfdOffPktBuf       BfdTxPkt;
    tBfdOffPktBuf       BfdRxPkt;

    if ((pBfdParams =
         (tBfdParams *) MemAllocMemBlk (BFD_OFFLOAD_PARAMS_SIZING_ID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu1Error = BFD_OFF_FAILED;
    pEncapInfo = &(pBfdParams->BfdTxCtrlPktEncapInfo);
    UNUSED_PARAM (pEncapInfo);
    pBfdGlobalConfigTableEntry = BfdUtilGetBfdGlobalConfigTable (u4ContextId);

    if (pBfdGlobalConfigTableEntry == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_INVAL_ARG_TO_FN,
                 "BfdOffSessConfigure", "pBfdGlobalConfigTableEntry");
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }

    if (pBfdSessHwHandle == NULL)
    {
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }

    if ((pBfdMplsPathInfo = (tBfdMplsPathInfo *)
         MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID)) == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_ISS_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                     "BfdOffSessConfigure", "BfdMplsPathInfoPoolId");
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }

    /* Initialize the structures */
    MEMSET (&BfdSessHandle, 0, sizeof (tBfdSessionHandle));
    MEMSET (&BfdSessPathInfo, 0, sizeof (tBfdSessionPathInfo));
    MEMSET (pBfdParams, 0, sizeof (tBfdParams));
    MEMSET (&BfdTxPkt, 0, sizeof (tBfdOffPktBuf));
    MEMSET (&BfdRxPkt, 0, sizeof (tBfdOffPktBuf));
    MEMSET (pBfdMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

    /*allocate memory to fill the packet received in call back */
    if ((BfdTxPkt.pau1PktBuf = MemAllocMemBlk (BFD_PKTBUF_POOLID)) == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }
    MEMSET (BfdTxPkt.pau1PktBuf, 0, BFD_MAX_PKT_LEN);

    pBfdMibFsMIStdBfdSessEntry = &pBfdFsMIStdBfdSessEntry->MibObject;
    UNUSED_PARAM (pBfdMibFsMIStdBfdSessEntry);
    do
    {
        /* Get the path information */
        if (BfdExtGetMplsPathInfo (u4ContextId,
                                   &(pBfdFsMIStdBfdSessEntry->
                                     BfdSessPathParams),
                                   pBfdMplsPathInfo, OSIX_TRUE) != OSIX_SUCCESS)
        {
            if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType !=
                 BFD_ENCAP_TYPE_MPLS_IP) &&
                (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType !=
                 BFD_ENCAP_TYPE_MPLS_IP_ACH) &&
                (pBfdMplsPathInfo->bRevPathFetchFail == OSIX_TRUE))
            {
                /* Path fetch failed and even IP return Path can't be used */
                BfdOffDebug (u4ContextId,
                             "BfdOffSessConfigure:Path Info Fetch Failed ");
                break;
            }
        }

        /* Filling BFD Session handle */
        BfdOffFillSessHandle (pBfdFsMIStdBfdSessEntry, &BfdSessHandle);

        /* Filling BFD Session Path information for offload */
        if (BfdOffFillSessPathInfo (u4ContextId, pBfdFsMIStdBfdSessEntry,
                                    pBfdMplsPathInfo, pBfdPktInfo,
                                    &BfdSessPathInfo) != OSIX_SUCCESS)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffSessConfigure: Filling Path Info Failed ");
            break;
        }

        /* Filling BFD Tx packet for offload. Also fetch the encap
         * to be used from the Tx function */
        if (BfdOffFillTxPkt (pBfdFsMIStdBfdSessEntry, pBfdMplsPathInfo,
                             &BfdSessPathInfo, &BfdTxPkt, &u4EncapType)
            != OSIX_SUCCESS)
        {
            BfdOffDebug (u4ContextId,
                         "BfdOffSessConfigure:Tx packet construct failed ");
            break;
        }

        /* Filling BFD Session Path information for offload */
        BfdOffFillSessParams (pBfdFsMIStdBfdSessEntry, pBfdMplsPathInfo,
                              &(pBfdGlobalConfigTableEntry->MibObject),
                              pBfdPktInfo, pBfdParams,
                              bBfdTxPktPollBit, bBfdTxPktFinalBit, u4EncapType);

        if (pBfdPktInfo != NULL)
        {
            /*allocate memory to fill the packet received in call back */
            BfdRxPkt.pau1PktBuf = pBfdPktInfo->pau1RxPktBuf;
            BfdRxPkt.u4Len = pBfdPktInfo->u4PktLen;
        }

#ifdef NPAPI_WANTED
        /* Call the NPAPI */

        if (FsMiBfdHwStartBfd (u4ContextId, &BfdSessHandle, &BfdSessPathInfo,
                               pBfdParams, &BfdTxPkt,
                               &BfdRxPkt, eBfdSessHwStartBfdCallFlag,
                               pBfdSessHwHandle) != FNP_SUCCESS)
        {
            *pu1Error = BFD_NP_FAILED;
            break;
        }

#else
        BfdOffStubFsMiBfdHwStartBfd (u4ContextId, &BfdSessHandle,
                                     &BfdSessPathInfo, pBfdParams, &BfdTxPkt,
                                     &BfdRxPkt, eBfdSessHwStartBfdCallFlag,
                                     pBfdSessHwHandle);

#endif
        i4Return = OSIX_SUCCESS;
    }
    while (0);

    /* Release the memory */
    if (MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo) != MEM_SUCCESS)
    {
        BFD_ISS_LOG (u4ContextId, BFD_FREE_MEMORY_FAIL,
                     "BfdOffSessConfigure", "BFD_MPLSPATHINFOSTRUCT_POOLID");
    }

    /* Release the memory */
    if (MemReleaseMemBlock (BFD_PKTBUF_POOLID,
                            (UINT1 *) BfdTxPkt.pau1PktBuf) != MEM_SUCCESS)
    {
        BFD_ISS_LOG (u4ContextId, BFD_FREE_MEMORY_FAIL, "BfdOffSessConfigure",
                     "BFD_PKTBUF_POOLID");
    }

    MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);

    return i4Return;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffDebug
 *
 * DESCRIPTION      : Function is used to print debug trace in offload  
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    pau1Strn - Format string
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None 
 * 
 **************************************************************************/
PRIVATE VOID
BfdOffDebug (UINT4 u4ContextId, CHR1 * pau1Strn, ...)
{
    va_list             vTrcArgs;
    CHR1                au1Trc[BFD_MAX_MSG_LEN];
    MEMSET (&vTrcArgs, 0, sizeof (va_list));
    MEMSET (au1Trc, 0, BFD_MAX_MSG_LEN);
    va_start (vTrcArgs, pau1Strn);
    vsprintf (au1Trc, (CHR1 *) pau1Strn, vTrcArgs);
    va_end (vTrcArgs);
    BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_DEBUG_TRACE, au1Trc);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessCreate
 *
 * DESCRIPTION      : Function is used to create a BFD session in the 
 *                    BFD Offload module in NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *                    pu1BfdRxCtrlPkt - pointer to the Rx packet received
 *
 * OUTPUT           : pu4BfdSessHwHandle - HW handle for the session. Returned
 *                                         whenever the configuration type is
 *                                         create
 *
 * 
 * RETURNS          : OSIX_SUCCESS/BFD_NP_FAILED/BFD_OFF_FAILED
 * 
 **************************************************************************/
INT4
BfdOffSessCreate (UINT4 u4ContextId,
                  tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                  tBfdPktInfo * pBfdPktInfo, tBfdHwhandle * pBfdSessHwHandle,
                  UINT1 *pu1Error)
{
    tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "BfdOffSessCreate", "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    /* Session create flag */
    eBfdSessHwStartBfdCallFlag = BFD_OFFLD_CREATE_SESSION;

    BfdOffDebug (u4ContextId, "Offload Create session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Call Bfd Session configure to create a BFD Session */
    return
        BfdOffSessConfigure (u4ContextId, pBfdFsMIStdBfdSessEntry,
                             pBfdPktInfo, eBfdSessHwStartBfdCallFlag,
                             OSIX_FALSE, OSIX_FALSE, pBfdSessHwHandle,
                             pu1Error);

}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessEnable
 *
 * DESCRIPTION      : Function is used to enable a BFD session in the 
 *                    BFD Offload module in NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessEnable (UINT4 u4ContextId,
                  tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                  tBfdHwhandle * pBfdSessHwHandle)
{
    tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag;
    UINT1               u1Error;
    INT4                i4RetVal = 0;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "BfdOffSessEnable", "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    BfdOffDebug (u4ContextId, "Offload Enable session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Session create flag */
    eBfdSessHwStartBfdCallFlag = BFD_OFFLD_ENABLE_SESSION;

    /* Call Bfd Session configure to create a BFD Session */
    i4RetVal =
        BfdOffSessConfigure (u4ContextId, pBfdFsMIStdBfdSessEntry,
                             NULL, eBfdSessHwStartBfdCallFlag, OSIX_FALSE,
                             OSIX_FALSE, pBfdSessHwHandle, &u1Error);
    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessPollInitUpdParams
 *
 * DESCRIPTION      : Function is used to Modify a BFD session in the 
 *                    BFD Offload module in NP and Start poll sequence
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *
 *                    pBfdModifiedParams - pointer to structure identifying 
 *                                         modified parameters. Used only 
 *                                         when session parameters are modified
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessPollInitUpdParams (UINT4 u4ContextId,
                             tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                             tBfdHwhandle * pBfdSessHwHandle)
{
    tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag;
    UINT1               u1Error;
    INT4                i4RetVal = 0;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "BfdOffSessPollInitUpdParams", "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    BfdOffDebug (u4ContextId, "Offload Poll Init session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Session create flag */
    eBfdSessHwStartBfdCallFlag = BFD_OFFLD_POLL_INITIATE_UPD_PARAMS;

    /* Call Bfd Session configure to create a BFD Session */
    i4RetVal =
        BfdOffSessConfigure (u4ContextId, pBfdFsMIStdBfdSessEntry,
                             NULL, eBfdSessHwStartBfdCallFlag, OSIX_TRUE,
                             OSIX_FALSE, pBfdSessHwHandle, &u1Error);
    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessPollTerminate
 *
 * DESCRIPTION      : Function is used to terminate a poll sequence for 
 *                    the offloaded BFD session in the NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *
 *                    pBfdModifiedParams - pointer to structure identifying 
 *                                         modified parameters. Used only 
 *                                         when session parameters are modified
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessPollTerminate (UINT4 u4ContextId,
                         tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                         tBfdPktInfo * pBfdPktInfo,
                         tBfdHwhandle * pBfdSessHwHandle)
{
    tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag;
    UINT1               u1Error;
    INT4                i4RetVal = 0;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "BfdOffSessPollTerminate", "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    BfdOffDebug (u4ContextId, "Offload Poll Terminate session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Session create flag */
    eBfdSessHwStartBfdCallFlag = BFD_OFFLD_POLL_TERMINATE;

    /* Call Bfd Session configure to create a BFD Session */
    i4RetVal = BfdOffSessConfigure (u4ContextId, pBfdFsMIStdBfdSessEntry,
                                    pBfdPktInfo, eBfdSessHwStartBfdCallFlag,
                                    OSIX_FALSE, OSIX_FALSE, pBfdSessHwHandle,
                                    &u1Error);

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessPollResponse
 *
 * DESCRIPTION      : Function is used to terminate a poll sequence for 
 *                    the offloaded BFD session in the NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *
 *                    pBfdPktInfo - pointer to the Rx info received
 *
 *                
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessPollResponse (UINT4 u4ContextId,
                        tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                        tBfdPktInfo * pBfdPktInfo,
                        tBfdHwhandle * pBfdSessHwHandle)
{
    tBfdSessHwStartBfdCallFlag eBfdSessHwStartBfdCallFlag;
    UINT1               u1Error;
    INT4                i4RetVal = 0;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "BfdOffSessPollResponse", "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    BfdOffDebug (u4ContextId, "Offload Poll Response session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Session create flag */
    eBfdSessHwStartBfdCallFlag = BFD_OFFLD_POLL_RESPONSE;

    /* Call Bfd Session configure to create a BFD Session */
    i4RetVal =
        BfdOffSessConfigure (u4ContextId, pBfdFsMIStdBfdSessEntry,
                             pBfdPktInfo, eBfdSessHwStartBfdCallFlag,
                             OSIX_FALSE, OSIX_TRUE, pBfdSessHwHandle, &u1Error);

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessStop
 *
 * DESCRIPTION      : Function is used to delete the offloaded BFD session 
 *                    in the NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *
 *                    pBfdFsMIStdBfdSessEntry - pointer to the session table 
 *                                              structure
 *
 *                    eBfdSessHwStopBfdCallFlag - Flag specifying to delete 
 *                                                or disable a session in 
 *                                                Offload module 
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessStop (UINT4 u4ContextId,
                tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                tBfdSessHwStopBfdCallFlag eBfdSessHwStopBfdCallFlag)
{
    tBfdMibFsMIStdBfdSessEntry *pBfdMibFsMIStdBfdSessEntry = NULL;
    tBfdSessionHandle   BfdSessHandle;

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY, "BfdOffSessStop",
                 "pBfdFsMIStdBfdSessEntry");
        return OSIX_FAILURE;
    }

    pBfdMibFsMIStdBfdSessEntry = &pBfdFsMIStdBfdSessEntry->MibObject;
    UNUSED_PARAM (pBfdMibFsMIStdBfdSessEntry);
    BfdOffDebug (u4ContextId, "Offload Stop session[%d] ",
                 pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
    /* Filling BFD Session handle */
    BfdOffFillSessHandle (pBfdFsMIStdBfdSessEntry, &BfdSessHandle);

#ifdef NPAPI_WANTED
    /* Call Bfd Session configure to create a BFD Session */
    if (FsMiBfdHwStopBfd (u4ContextId, &BfdSessHandle,
                          &pBfdFsMIStdBfdSessEntry->BfdHwhandle,
                          eBfdSessHwStopBfdCallFlag) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (eBfdSessHwStopBfdCallFlag);
#endif
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessRegCb
 *
 * DESCRIPTION      : Function is used to register a callback function 
 *                    with the offload module for the specified events
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    
 *                    eBfdEventType - events to give callback
 *
 *                    BfdOffCallBack - Pointer to call back function
 *
 * OUTPUT           : None
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessRegCb (UINT4 u4ContextId, tBfdEventType eBfdEventType,
                 FsMiBfdHwSessionCb BfdOffCallBack)
{
#ifdef NPAPI_WANTED
    /* Call Bfd Session configure to create a BFD Session */
    if (FsMiBfdHwRegisterSessionCb (u4ContextId, eBfdEventType,
                                    BfdOffCallBack) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (eBfdEventType);
    UNUSED_PARAM (BfdOffCallBack);
#endif
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : BfdOffSessInfo
 *
 * DESCRIPTION      : Function is used to fetch session params or statistics
 *                    from offload module in NP
 *
 * INPUT            : u4ContextId - Context id of the BFD
 *                    pBfdFsMIStdBfdSessEntry - pointer to the BFD Session 
 *                                              table
 *                    u1Flags - indicates which info to get
 *                              BFD_OFF_GET_SESS_PARAMS for parameters
 *                              BFD_OFF_GET_SESS_STATS for statistics
 *                              BFD_OFF_CLEAR_SESS_STATS for  clearing 
 *                              statistics of a individual session
 *                              BFD_OFF_CLEAR_ALL_SESS_STATS for  clearing 
 *                              statistics of all sessions
 *
 * OUTPUT           : pBfdFsGetMIStdBfdSessTable - pointer to the BFD Session, 
 *                                                 with BFD Params fetched 
 *                                                 from the NP
 *
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
BfdOffSessInfo (UINT4 u4ContextId,
                tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessEntry,
                tBfdFsMIStdBfdSessEntry * pBfdFsGetMIStdBfdSessTable,
                UINT1 u1Flags)
{
    tBfdSessionHandle   BfdSessHandle;
    tBfdSessStats       BfdSessStats;
    tBfdParams         *pBfdParams = NULL;
    tBfdMibFsMIStdBfdSessEntry *pBfdMibFsMIStdBfdSessEntry = NULL;

    if ((pBfdParams =
         (tBfdParams *) MemAllocMemBlk (BFD_OFFLOAD_PARAMS_SIZING_ID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pBfdFsMIStdBfdSessEntry == NULL)
        && (u1Flags != BFD_OFF_CLEAR_ALL_SESS_STATS))
    {
        /* Session table unavailable */
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY, "BfdOffSessInfo",
                 "pBfdFsMIStdBfdSessEntry");
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }

    MEMSET (&BfdSessStats, 0, sizeof (tBfdSessStats));
    MEMSET (pBfdParams, 0, sizeof (tBfdParams));
    MEMSET (&BfdSessHandle, 0, sizeof (tBfdSessionHandle));

    if (pBfdFsMIStdBfdSessEntry != NULL)
    {
        pBfdMibFsMIStdBfdSessEntry = &pBfdFsMIStdBfdSessEntry->MibObject;

        /* Filling BFD Session handle */
        BfdOffFillSessHandle (pBfdFsMIStdBfdSessEntry, &BfdSessHandle);
    }
    UNUSED_PARAM (pBfdMibFsMIStdBfdSessEntry);

    if (u1Flags == BFD_OFF_GET_SESS_PARAMS)
    {
        /* Call Bfd NPAPI to get the parameters */
        BfdOffDebug (u4ContextId, "Offload Get session Params[%d] ",
                     pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        /* Call Bfd Session configure to create a BFD Session */
#ifdef NPAPI_WANTED
        if (FsMiBfdGetHwBfdParameters (u4ContextId,
                                       &BfdSessHandle,
                                       &pBfdFsMIStdBfdSessEntry->BfdHwhandle,
                                       pBfdParams) != FNP_SUCCESS)
        {
            MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID,
                                (UINT1 *) pBfdParams);
            return OSIX_FAILURE;
        }
#endif
        /* Filling BFD Session table with BFD Params */
        BfdOffFillSessTblFromParams (&pBfdFsGetMIStdBfdSessTable->MibObject,
                                     pBfdParams);
    }
    else if (u1Flags == BFD_OFF_GET_SESS_STATS)
    {
        BfdOffDebug (u4ContextId, "Offload Get session Stats[%d] ",
                     pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        /* Call Bfd NPAPI to get the statistics */
#ifdef NPAPI_WANTED
        if (FsMiBfdGetHwBfdSessionStats (u4ContextId,
                                         &BfdSessHandle,
                                         &pBfdFsMIStdBfdSessEntry->BfdHwhandle,
                                         &BfdSessStats) != FNP_SUCCESS)
        {
            MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID,
                                (UINT1 *) pBfdParams);
            return OSIX_FAILURE;
        }
#endif
        /* Filling BFD Session table with BFD Params */
        BfdOffFillSessTblFromStats (pBfdFsMIStdBfdSessEntry, &BfdSessStats);
    }
    else if (u1Flags == BFD_OFF_CLEAR_SESS_STATS)
    {
        BfdOffDebug (u4ContextId, "Offload Clear session Stats[%d] ",
                     pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        /* Call Bfd NPAPI to clear the statistics in NP */
#ifdef NPAPI_WANTED
        if (FsMiBfdHwClearBfdStats (u4ContextId,
                                    &BfdSessHandle,
                                    &pBfdFsMIStdBfdSessEntry->BfdHwhandle,
                                    FALSE) != FNP_SUCCESS)
        {
            MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID,
                                (UINT1 *) pBfdParams);
            return OSIX_FAILURE;
        }
#endif
    }
    else if (u1Flags == BFD_OFF_CLEAR_ALL_SESS_STATS)
    {
#ifdef NPAPI_WANTED
        /* Call Bfd NPAPI to clear the statistics in NP */
        if (FsMiBfdHwClearBfdStats (u4ContextId, NULL, NULL, TRUE) !=
            FNP_SUCCESS)
        {
            MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID,
                                (UINT1 *) pBfdParams);
            return OSIX_FAILURE;
        }
#endif
    }
    else
    {
        MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_OFFLOAD_PARAMS_SIZING_ID, (UINT1 *) pBfdParams);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 * Function Name      : FsMiBfdHwSessionCallBack
 *                                                                           
 * Description        : The routine is the callback for the NPAPI in case 
 *                      any of the events for which Control plane registered 
 *                      for has occurred. 
 *                                                                           
 * Input(s)           : u4ContextId - Context Identifier                   
 *                      
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier 
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type 
 *                                   tMplsHdrParams.
 *                      
 *                      pBfdSessHwHandle - pointer to the handle returned 
 *                          by the hardware for the BFD session 
 *
 *                      eBfdEventType - Event type the call back is given 
 *           
 *                      bActionFlag - If this is set, control plane action 
 *                                    is needed 
 *                      
 *                      pBfdParams - Pointer to the structure tBfdParams. 
 *                          This structure contains all the parameters to be 
 *                          used by the hardware to construct and transmit BFD 
 *                          control packets at negotiated interval and use the 
 *                          calculated detection time interval to detect faults
 *                          .This structure additionally contains values 
 *                          expected to be present in the received BFD control 
 *                          packet which can be used to perform validation
 *
 *                          
 *                      pBfdPkt - Pointer to the buffer containing the 
 *                          entire BFD packet (including the encapsulation 
 *                          and labels) received on the interface by the 
 *                          BFD Offload module.
 *
 *                      If the variable
 *                      pBfdParams->BfdCtrlPktInfo.u1BfdOffHwActionFlag is set
 *                      to TRUE, this function maps the current state passed
 *                      by the offload module with the previous state available
 *                      in control plane and notifies the BFD module about the
 *                      correct state change.
 *
 *                      If the variable
 *                      pBfdParams->BfdCtrlPktInfo.u1BfdOffHwActionFlag is set
 *                      to FALSE, this function directly notifies state change
 *                      to BFD module.
 *                      
 * Output(s)          : None 
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS - On Success                             
 *                      OSIX_FAILURE - On failure                             
 *****************************************************************************/
VOID
FsMiBfdHwSessionCallBack (UINT4 u4ContextId,
                          tBfdSessionHandle * pBfdSessHandle,
                          tBfdHwhandle * pBfdHwSessHandle,
                          tBfdEventType eBfdEventType,
                          BOOL1 bActionFlag,
                          tBfdParams * pBfdParams, tBfdOffPktBuf * pBfdPkt)
{
    tBfdReqParams      *pBfdReqParams = NULL;
    tBfdOffCbParams    *pBfdOffCbParams = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4FreeMemCount = 0;

    if ((pBfdSessHandle == NULL) || (pBfdHwSessHandle == NULL) ||
        (pBfdParams == NULL))
    {
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY,
                 "FsMiBfdHwSessionCallBack",
                 "pBfdSessHandle,pu4BfdSessHwHandle,pBfdParams");
        return;
    }

/*
- Check if "gu1QueueOverflow" is set as TRUE. If so, don't post the
  message.
- If "QueueOverflow" is FALSE, compare the incoming session-id
  with the value of "gu2OverflowSessionId". If the new session-id
  is greater than "gu2OverflowSessionId", discard the event; else, go on.
- Fetch the free memory-unit count of the queue.
  If count is two: - allocate memory and post a queue overflow message.
                   - set global flag "QueueOverflow" to TRUE.
*/

    if (gu1QueueOverflow == OSIX_TRUE)
    {
        BfdOffDebug (u4ContextId,
                     "FsMiBfdHwSessionCallBack: queue overflow message already posted");
        return;
    }

    if (pBfdSessHandle->u4BfdSessId > gu2OverflowSessionId)
    {
        BfdOffDebug (u4ContextId,
                     "FsMiBfdHwSessionCallBack: Hw audit expected for this session");
        return;
    }

    if ((u4FreeMemCount =
         MemGetFreeUnits (BFD_QUEMSG_POOLID)) <= BFD_MIN_Q_OVERFLOW_SIZE)
    {
        gu1QueueOverflow = OSIX_TRUE;
    }

    if ((pBfdReqParams = (tBfdReqParams *)
         MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
    {
        BFD_ISS_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                     "FsMiBfdHwSessionCallBack", "BFD_QUEMSG_POOLID");
        BfdUtilUpdateMemAlocFail (u4ContextId);
        gu1QueueOverflow = OSIX_FALSE;
        return;
    }

    MEMSET (pBfdReqParams, 0, sizeof (tBfdReqParams));

    pBfdReqParams->u4ContextId = u4ContextId;

    if (gu1QueueOverflow == OSIX_FALSE)
    {
        /* Initialise to off cb params in Bfd Req Params */
        pBfdOffCbParams = &pBfdReqParams->unReqInfo.BfdOffCbParams;

        /*Filling the offload params structure */
        MEMCPY (&pBfdOffCbParams->BfdSessHandle, pBfdSessHandle,
                sizeof (tBfdSessionHandle));

        pBfdOffCbParams->BfdSessHwHandle.u4BfdSessHwHandle =
            pBfdHwSessHandle->u4BfdSessHwHandle;
        pBfdOffCbParams->BfdSessHwHandle.u4BfdOffErrNum =
            pBfdHwSessHandle->u4BfdOffErrNum;
        pBfdOffCbParams->bActionFlag = bActionFlag;

        MEMCPY (&pBfdOffCbParams->BfdParams, pBfdParams, sizeof (tBfdParams));

        if (pBfdParams->BfdCtrlPktInfo.u1BfdOffHwActionFlag == TRUE)
        {
            i4RetVal = BfdOffMapHwEvents (u4ContextId, pBfdSessHandle,
                                          pBfdHwSessHandle, &eBfdEventType,
                                          bActionFlag, pBfdParams, pBfdPkt);
            if (i4RetVal != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
                BfdOffDebug (u4ContextId,
                             "FsMiBfdHwSessionCallBack: eBfdEventType Invalid");
                return;
            }
        }
        else
        {
            if (pBfdPkt == NULL)
            {
                MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
                return;
            }

            if ((pBfdPkt->u4Len > 0) && (pBfdPkt->pau1PktBuf == NULL))
            {
                MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
                return;
            }

            /*allocate memory to fill the packet received in call back */
            if ((pBuf =
                 CRU_BUF_Allocate_MsgBufChain (pBfdPkt->u4Len, 0)) == NULL)
            {
                MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
                return;
            }
            /*copy the Pkt to CRU BUFF at offset 0 */
            if (CRU_BUF_Copy_OverBufChain
                (pBuf, pBfdPkt->pau1PktBuf, 0, pBfdPkt->u4Len) == CRU_FAILURE)
            {
                MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
                CRU_BUF_Release_MsgBufChain (pBuf, 0);
                return;
            }
            pBfdOffCbParams->pBuf = pBuf;
        }
        /* Call the BFD entry function to notify offload event */
        pBfdReqParams->u4ContextId = u4ContextId;
        pBfdReqParams->u4ReqType = BFD_OFFLD_CB_MSG;
        pBfdOffCbParams->eBfdEventType = eBfdEventType;
    }
    else
    {
        pBfdReqParams->u4ReqType = BFD_QUE_OVERFLOW_MSG;
    }

    /* Call the BFD entry function to notify offload event */
    if (BfdApiHandleExtRequest (u4ContextId,
                                pBfdReqParams, NULL) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
        if (pBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
        }
        return;
    }
    MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdReqParams);
    return;
}

/****************************************************************************
 *                                                                           *
 * Function     : BfdOffHandleOffCbMsg                                       *
 *                                                                           *
 * Description  : Process the message from offload module                    *
 *                                                                           *
 * Input        : u4ContextId - Context ID received                          *
 *                pBfdOffCbParams - Offload callback params                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdOffHandleOffCbMsg (UINT4 u4ContextId, tBfdOffCbParams * pBfdOffCbParams)
{

    tBfdFsMIStdBfdSessEntry *pBfdSessTable = NULL;
    tBfdParams         *pBfdParams = NULL;

    if (pBfdOffCbParams == NULL)
    {
        return OSIX_FAILURE;
    }
    pBfdParams = &(pBfdOffCbParams->BfdParams);

    /* Based on the parameters do a get all  of the session info */
    pBfdSessTable =
        BfdUtilGetBfdSessTable (u4ContextId,
                                pBfdOffCbParams->BfdSessHandle.u4BfdSessId);
    if (pBfdSessTable == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY, "BFD",
                 "BfdQueHandleOffCbMsg", "Invalid Session ID");
        return OSIX_FAILURE;
    }

    switch (pBfdOffCbParams->eBfdEventType)
    {
        case BFD_OFFLD_SESS_OFFLD_FAILED:
        {
            /* For asynchronous notification from offload module in case of 
             * failure to create a session */
            pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlReq = BFD_SNMP_TRUE;
            pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                BFD_SESS_ERROR_OFFLD_FAILED;

            /* Raise a trap indicating session offload failed */
            BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_OFFLOAD_FAILED,
                     pBfdSessTable->MibObject.
                     u4FsMIStdBfdSessDiscriminator,
                     pBfdSessTable->MibObject.i4FsMIStdBfdSessState,
                     pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlErrReason);

            break;
        }
        case BFD_OFFLD_SESS_DOWN_TO_INIT:
        {
            /* Event indicating session moved from DOWN TO INIT */
            pBfdSessTable->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_TRUE;

            if (BFD_SESS_STATE (pBfdSessTable) != BFD_SESS_STATE_DOWN)
            {
                BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_INVALID_SEM_STATE,
                         "BfdQueHandleOffCbMsg",
                         pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);
            }
            else
            {
                /* update the session information */
                BFD_SESS_STATE (pBfdSessTable) = BFD_SESS_STATE_INIT;
            }
            break;
        }
        case BFD_OFFLD_SESS_DOWN_TO_UP:
        {
            /* Event indicating session moved from Down to Up */
            pBfdSessTable->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_TRUE;
            if (BFD_SESS_STATE (pBfdSessTable) != BFD_SESS_STATE_DOWN)
            {
                BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_INVALID_SEM_STATE,
                         "BfdQueHandleOffCbMsg",
                         pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);
            }
            else
            {
                BfdSessSessionUp (pBfdSessTable);
                BfdOffFillSessTblFromParams (&(pBfdSessTable->MibObject),
                                             pBfdParams);
            }

            break;
        }
        case BFD_OFFLD_SESS_INIT_TO_UP:
        {
            /* Event indicating session moved from Init to Up */
            if (BFD_SESS_STATE (pBfdSessTable) != BFD_SESS_STATE_INIT)
            {
                BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_INVALID_SEM_STATE,
                         "BfdQueHandleOffCbMsg",
                         pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);
            }
            else
            {
                /* update the session information */
                BfdSessSessionUp (pBfdSessTable);
                BfdOffFillSessTblFromParams (&(pBfdSessTable->MibObject),
                                             pBfdParams);
            }

            break;
        }
        case BFD_OFFLD_SESS_INIT_TO_DOWN:
        {
            /* Event indicating session moved from init to down */
            if (BFD_SESS_STATE (pBfdSessTable) != BFD_SESS_STATE_INIT)
            {
                BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_INVALID_SEM_STATE,
                         "BfdQueHandleOffCbMsg",
                         pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);
            }
            else
            {
                BfdSessSessionDown (pBfdSessTable,
                                    pBfdOffCbParams->BfdParams.
                                    BfdCtrlPktInfo.eBfdSessTxPktDiag,
                                    OSIX_TRUE);
            }
            break;
        }
        case BFD_OFFLD_SESS_UP_TO_DOWN:
        {
            /* Event indicating session moved from Up to Down */
            if (BFD_SESS_STATE (pBfdSessTable) != BFD_SESS_STATE_UP)
            {
                BFD_LOG (u4ContextId, BFD_TRC_OFFLOAD_INVALID_SEM_STATE,
                         "BfdQueHandleOffCbMsg",
                         pBfdSessTable->MibObject.u4FsMIStdBfdSessIndex);
            }
            else
            {
                /* update the session information */
                BfdSessSessionDown (pBfdSessTable,
                                    pBfdOffCbParams->BfdParams.
                                    BfdCtrlPktInfo.eBfdSessTxPktDiag,
                                    OSIX_TRUE);
            }
            break;
        }

        case BFD_OFFLD_SESS_LOC_DEFECT:
        {
            /* Indicates Loss of continuity defect */
            /* Stopped hearing from remote due to LOC */
            pBfdSessTable->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_FALSE;

            if (BFD_SESS_STATE (pBfdSessTable) == BFD_SESS_STATE_UP)
            {
                BfdSessSessionDown (pBfdSessTable,
                                    pBfdOffCbParams->BfdParams.
                                    BfdCtrlPktInfo.eBfdSessTxPktDiag,
                                    OSIX_TRUE);
            }
            /* update the session information */
            BFD_SESS_STATE (pBfdSessTable) = BFD_SESS_STATE_DOWN;
            break;
        }

        case BFD_OFFLD_SESS_MISCON_DEFECT:
        {
            break;
        }

        case BFD_OFFLD_SESS_POLL_BIT_RX:
        case BFD_OFFLD_SESS_FINAL_BIT_RX:
        {
            BfdPollHandleOffldPollSeq (u4ContextId,
                                       pBfdOffCbParams, pBfdSessTable);

            break;
        }
        case BFD_OFFLD_SESS_DEFECT:
        case BFD_OFFLD_SESS_DEFECT_CLEAR:
        {
            break;
        }
        case BFD_OFFLD_SESS_PERIOD_MISCONF:
        {
            if (pBfdSessTable->MibObject.i4FsMIBfdSessTmrNegotiate != OSIX_TRUE)
            {

                pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlReq =
                    BFD_SNMP_TRUE;
                pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                    BFD_SESS_ERROR_PERIOD_MISCONFIG;
                /* Raise Timer misconfiguration defect trap */
                BFD_LOG (pBfdSessTable->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_TMR_MIS_CONFIG,
                         pBfdSessTable->MibObject.i4FsMIStdBfdSessState,
                         pBfdSessTable->MibObject.
                         u4FsMIStdBfdSessDiscriminator,
                         pBfdSessTable->MibObject.
                         i4FsMIBfdSessAdminCtrlErrReason);
            }
            break;
        }
        case BFD_OFFLD_SESS_ADMIN_DOWN_TMR_EXP:
        {
            /* NP is expected to send BFD control packets for one detection
             * time with ADMIN DOWN state and at the end trigger a call
             * back to control plane. Control plane will clear the dynamic
             * parameters. This behaviour is coherent with Control plane 
             * behaviour. In case timing is such that ADMIN UP is done 
             * and this call back is received after, it is neglected and the 
             * NP can relearn the BFD params from control plane call.*/
            if (BFD_SESS_STATE (pBfdSessTable) == BFD_SESS_STATE_ADMIN_DOWN)
            {
                /* clear the dynamic params learned */
                BfdSessDelSessEntryDynParams (pBfdSessTable);
            }

            /* The offload session need not to be cleared on ADMIN DOWN 
               and hence the hardware handle is not requried to be reset.
               Applicable only when Remote Discriminator is configured
               statically
             */

            if (pBfdSessTable->bRemDiscrStaticConf != OSIX_TRUE)
            {
                pBfdSessTable->BfdHwhandle.u4BfdSessHwHandle = 0;
            }

            pBfdSessTable->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_FALSE;

            break;
        }
        case BFD_OFFLD_SESS_PERIOD_MISCONF_CLEAR:
        {
            pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlReq = BFD_SNMP_FALSE;
            pBfdSessTable->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                BFD_SESS_ERROR_NONE;
            break;
        }
        case BFD_OFFLD_SESS_UPD_NEG_DETECT_MULTIPLIER:
        {
            BfdOffFillSessTblFromParams (&(pBfdSessTable->MibObject),
                    pBfdParams);
            break;
        }
        default:
        {
            break;
        }
    }
    BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                            &(pBfdSessTable->MibObject.SessDbNode));
    BfdRedSyncDynInfo ();
    return OSIX_SUCCESS;
}

#ifndef NPAPI_WANTED
VOID
BfdOffStubFsMiBfdHwStartBfd (UINT4 u4ContextId,
                             tBfdSessionHandle * pBfdSessHandle,
                             tBfdSessionPathInfo * pBfdSessPathInfo,
                             tBfdParams * pBfdParams,
                             tBfdOffPktBuf * pBfdTxPkt,
                             tBfdOffPktBuf * pBfdRxPkt,
                             tBfdSessHwStartBfdCallFlag
                             eBfdSessHwStartBfdCallFlag,
                             tBfdHwhandle * pBfdSessHwHandle)
{

    tBfdCtrlPktEncapParams *pEncapInfo = NULL;
    pEncapInfo = &(pBfdParams->BfdTxCtrlPktEncapInfo);

    BfdOffDebug (u4ContextId, "BfdSessHandle : ");
    BfdOffDebug (u4ContextId, "u4BfdSessId %d  u4BfdSessLocalDiscr %d \n\
                 u4BfdSlotId %d u4BfdCardNum %d", pBfdSessHandle->u4BfdSessId, pBfdSessHandle->u4BfdSessLocalDiscr, pBfdSessHandle->u4BfdSlotId, pBfdSessHandle->u4BfdCardNum);

    BfdOffDebug (u4ContextId, "\nBfdSessPathInfo : \n");
    BfdOffDebug (u4ContextId, "tBfdMplsHdrParams:  \n\
             In Label List 1 %d:%d:%d 2 %d:%d:%d 3 %d:%d:%d 4 %d:%d:%d", pBfdSessPathInfo->LabelList.InLabelList[0].u4Label, pBfdSessPathInfo->LabelList.InLabelList[0].u1Exp, pBfdSessPathInfo->LabelList.InLabelList[0].u1SI, pBfdSessPathInfo->LabelList.InLabelList[1].u4Label, pBfdSessPathInfo->LabelList.InLabelList[1].u1Exp, pBfdSessPathInfo->LabelList.InLabelList[1].u1SI, pBfdSessPathInfo->LabelList.InLabelList[2].u4Label, pBfdSessPathInfo->LabelList.InLabelList[2].u1Exp, pBfdSessPathInfo->LabelList.InLabelList[2].u1SI, pBfdSessPathInfo->LabelList.InLabelList[3].u4Label, pBfdSessPathInfo->LabelList.InLabelList[3].u1Exp, pBfdSessPathInfo->LabelList.InLabelList[3].u1SI);

    BfdOffDebug (u4ContextId, "tBfdMplsHdrParams: \n\
             OUT Label List 1 %d:%d:%d 2 %d:%d:%d 3 %d:%d:%d 4 %d:%d:%d", pBfdSessPathInfo->LabelList.OutLabelList[0].u4Label, pBfdSessPathInfo->LabelList.OutLabelList[0].u1Exp, pBfdSessPathInfo->LabelList.OutLabelList[0].u1SI, pBfdSessPathInfo->LabelList.OutLabelList[1].u4Label, pBfdSessPathInfo->LabelList.OutLabelList[1].u1Exp, pBfdSessPathInfo->LabelList.OutLabelList[1].u1SI, pBfdSessPathInfo->LabelList.OutLabelList[2].u4Label, pBfdSessPathInfo->LabelList.OutLabelList[2].u1Exp, pBfdSessPathInfo->LabelList.OutLabelList[2].u1SI, pBfdSessPathInfo->LabelList.OutLabelList[3].u4Label, pBfdSessPathInfo->LabelList.OutLabelList[3].u1Exp, pBfdSessPathInfo->LabelList.OutLabelList[3].u1SI);

    BfdOffDebug (u4ContextId, "au1SrcMacAddr %x:%x:%x:%x:%x:%x ",
                 pBfdSessPathInfo->au1SrcMacAddr[0],
                 pBfdSessPathInfo->au1SrcMacAddr[1],
                 pBfdSessPathInfo->au1SrcMacAddr[2],
                 pBfdSessPathInfo->au1SrcMacAddr[3],
                 pBfdSessPathInfo->au1SrcMacAddr[4],
                 pBfdSessPathInfo->au1SrcMacAddr[5]);

    BfdOffDebug (u4ContextId, "au1DstMacAddr %x:%x:%x:%x:%x:%x ",
                 pBfdSessPathInfo->au1DstMacAddr[0],
                 pBfdSessPathInfo->au1DstMacAddr[1],
                 pBfdSessPathInfo->au1DstMacAddr[2],
                 pBfdSessPathInfo->au1DstMacAddr[3],
                 pBfdSessPathInfo->au1DstMacAddr[4],
                 pBfdSessPathInfo->au1DstMacAddr[5]);

    BfdOffDebug (u4ContextId, "u4IngrItf %d u4EggrItf %d u2Vlan %d ",
                 pBfdSessPathInfo->u4IngrItf, pBfdSessPathInfo->u4EggrItf,
                 pBfdSessPathInfo->u2Vlan);

    BfdOffDebug (u4ContextId, "Encapsulation type [%d]",
                 pEncapInfo->eBfdEncapType);

    BfdOffDebug (u4ContextId, "Offload of BFD Session %d SUCCESS ",
                 pBfdSessHandle->u4BfdSessId);
    /* Added Only to prevent retrying of offload create */
    pBfdSessHwHandle->u4BfdSessHwHandle = 1;
    UNUSED_PARAM (eBfdSessHwStartBfdCallFlag);
    UNUSED_PARAM (pBfdSessHwHandle);
    UNUSED_PARAM (pBfdTxPkt);
    UNUSED_PARAM (pBfdRxPkt);
    return;
}
#endif

/****************************************************************************
 *
 * Function Name     : BfdOffMapHwEvents
 *
 * Description       : This function is used to map the hardware events
 *                     received from the offload module with the events in the
 *                     control plane.
 *
 * Input(s)           : u4ContextId - Context Identifier
 *
 *                      pBfdSessHandle - Pointer to structure tBfdSessionHandle
 *                          This structure contains the following:
 *                             u4BfdSessId - Contains the session identifier
 *                                   that uniquely identifies the BFD session.
 *                             u4BfdSessLocalDiscr -Local discriminator value
 *                                   assigned for this BFD session.
 *                             LabelList - Structure variable of type
 *                                   tMplsHdrParams.
 *
 *                      pBfdSessHwHandle - pointer to the handle returned
 *                          by the hardware for the BFD session
 *
 *                      eBfdEventType - to be update by the present and 
 *                           previous event,Compatible to
 *                           FsMiBfdHwSessionCallBack function.
 *
 *                      bActionFlag - If this is set, control plane action
 *                                    is needed. 
 *
 *                      pBfdParams - Pointer to the structure tBfdParams.
 *                          This structure contains all the parameters to be
 *                          used by the hardware to construct and transmit BFD
 *                          control packets at negotiated interval and use the
 *                          calculated detection time interval to detect faults
 *                          .This structure additionally contains values
 *                          expected to be present in the received BFD control
 *                          packet which can be used to perform validation
 *
 *                      pBfdPkt - Pointer to the buffer containing the
 *                          entire BFD packet (including the encapsulation
 *                          and labels) received on the interface by the
 *                          BFD Offload module.
 *
 * Output(s)          : None 
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *                      
 *****************************************************************************/
INT4
BfdOffMapHwEvents (UINT4 u4ContextId,
                   tBfdSessionHandle * pBfdSessHandle,
                   tBfdHwhandle * pBfdHwSessHandle,
                   tBfdEventType * peBfdEventType,
                   BOOL1 bActionFlag,
                   tBfdParams * pBfdParams, tBfdOffPktBuf * pBfdPkt)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessTable = NULL;
    INT4                i4BfdSessState = 0;

    UNUSED_PARAM (bActionFlag);
    UNUSED_PARAM (pBfdHwSessHandle);
    UNUSED_PARAM (pBfdPkt);

    if (*peBfdEventType == BFD_OFFLD_SESS_ADMIN_DOWN_TMR_EXP)
    {
        return OSIX_SUCCESS;
    }

    BfdMainTaskLock ();
    pBfdSessTable = BfdUtilGetBfdSessTable (u4ContextId,
                                            pBfdSessHandle->u4BfdSessId);

    if (pBfdSessTable == NULL)
    {
        BfdMainTaskUnLock ();
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY, "BFD",
                 "BfdOffMapHwEvents", "Invalid Session ID");
        return OSIX_FAILURE;
    }

    i4BfdSessState = BFD_SESS_STATE (pBfdSessTable);
    BfdMainTaskUnLock ();

    if (pBfdParams->BfdCtrlPktInfo.eBfdSessTxPktDiag
        == BFD_DIAG_CTRL_DETECTION_TIME_EXP)
    {
        *peBfdEventType = BFD_OFFLD_SESS_LOC_DEFECT;
        return OSIX_SUCCESS;
    }

    switch (pBfdParams->BfdCtrlPktInfo.eBfdSessState)
    {
        case BFD_SESS_STATE_INIT:
        {
            *peBfdEventType = BFD_OFFLD_SESS_DOWN_TO_INIT;
            break;
        }
        case BFD_SESS_STATE_DOWN:
        {
            if (i4BfdSessState == BFD_SESS_STATE_UP)
            {
                *peBfdEventType = BFD_OFFLD_SESS_UP_TO_DOWN;
            }
            else if (i4BfdSessState == BFD_SESS_STATE_INIT)
            {
                *peBfdEventType = BFD_OFFLD_SESS_INIT_TO_DOWN;
            }
            break;
        }
        case BFD_SESS_STATE_UP:
        {
            if (i4BfdSessState == BFD_SESS_STATE_DOWN)
            {
                *peBfdEventType = BFD_OFFLD_SESS_DOWN_TO_UP;
            }
            else if (i4BfdSessState == BFD_SESS_STATE_INIT)
            {
                *peBfdEventType = BFD_OFFLD_SESS_INIT_TO_UP;
            }
            break;
        }
        default:
        {
            BFD_LOG (u4ContextId, BFD_TRC_SESS_TBL_EMPTY, "BFD",
                     "BfdOffMapHwEvents", "Invalid Session ID");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}
