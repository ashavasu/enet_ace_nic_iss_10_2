/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdlwg.c,v 1.11 2014/09/15 10:42:00 siva Exp $
*
* Description: contains low level routines for snmp
*********************************************************************/
#include "bfdinc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdBfdGlobalConfigTable (UINT4 *pu4FsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetFirstFsMIStdBfdGlobalConfigTable ();

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIBfdSystemConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIBfdSystemConfigTable (UINT4 *pu4FsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetFirstFsMIStdBfdGlobalConfigTable ();

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIBfdGblSessionConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIBfdGblSessionConfigTable (UINT4 *pu4FsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetFirstFsMIStdBfdGlobalConfigTable ();

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIBfdStatisticsTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIBfdStatisticsTable (UINT4 *pu4FsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetFirstFsMIStdBfdGlobalConfigTable ();

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdBfdSessTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdBfdSessTable (UINT4 *pu4FsMIStdBfdContextId,
                                     UINT4 *pu4FsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4FsMIStdBfdSessIndex =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdBfdSessPerfTable (UINT4 *pu4FsMIStdBfdContextId,
                                         UINT4 *pu4FsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4FsMIStdBfdSessIndex =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIBfdSessionTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIBfdSessionTable (UINT4 *pu4FsMIStdBfdContextId,
                                     UINT4 *pu4FsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4FsMIStdBfdSessIndex =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIBfdSessPerfTable (UINT4 *pu4FsMIStdBfdContextId,
                                      UINT4 *pu4FsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4FsMIStdBfdSessIndex =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessDiscriminator
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdBfdSessDiscMapTable (UINT4 *pu4FsMIStdBfdContextId,
                                            UINT4
                                            *pu4FsMIStdBfdSessDiscriminator)
{

    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pBfdFsMIStdBfdSessDiscMapEntry = BfdGetFirstFsMIStdBfdSessDiscMapTable ();

    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4FsMIStdBfdSessDiscriminator =
        pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscriminator;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessInterface
                FsMIStdBfdSessSrcAddrType
                FsMIStdBfdSessSrcAddr
                FsMIStdBfdSessDstAddrType
                FsMIStdBfdSessDstAddr
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdBfdSessIpMapTable (UINT4 *pu4FsMIStdBfdContextId,
                                          INT4 *pi4FsMIStdBfdSessInterface,
                                          INT4 *pi4FsMIStdBfdSessSrcAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdBfdSessSrcAddr,
                                          INT4 *pi4FsMIStdBfdSessDstAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdBfdSessDstAddr)
{

    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    pBfdFsMIStdBfdSessIpMapEntry = BfdGetFirstFsMIStdBfdSessIpMapTable ();

    if (pBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId;

    *pi4FsMIStdBfdSessInterface =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessInterface;

    *pi4FsMIStdBfdSessSrcAddrType =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen);
    pFsMIStdBfdSessSrcAddr->i4_Length =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

    *pi4FsMIStdBfdSessDstAddrType =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType;

    MEMCPY (pFsMIStdBfdSessDstAddr->pu1_OctetList,
            pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen);
    pFsMIStdBfdSessDstAddr->i4_Length =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIStdBfdGlobalConfigTable (UINT4 u4FsMIStdBfdContextId,
                                            UINT4 *pu4NextFsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    tBfdFsMIStdBfdGlobalConfigTableEntry
        * pNextBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pNextBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetNextFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry);

    if (pNextBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIBfdSystemConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIBfdSystemConfigTable (UINT4 u4FsMIStdBfdContextId,
                                         UINT4 *pu4NextFsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    tBfdFsMIStdBfdGlobalConfigTableEntry
        * pNextBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pNextBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetNextFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry);

    if (pNextBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIBfdGblSessionConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIBfdGblSessionConfigTable (UINT4 u4FsMIStdBfdContextId,
                                             UINT4 *pu4NextFsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    tBfdFsMIStdBfdGlobalConfigTableEntry
        * pNextBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pNextBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetNextFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry);

    if (pNextBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIBfdStatisticsTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIBfdStatisticsTable (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 *pu4NextFsMIStdBfdContextId)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    tBfdFsMIStdBfdGlobalConfigTableEntry
        * pNextBfdFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pNextBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdGetNextFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry);

    if (pNextBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdBfdSessTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessIndex
                nextFsMIStdBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIStdBfdSessTable (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 *pu4NextFsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    UINT4 *pu4NextFsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    tBfdFsMIStdBfdSessEntry *pNextBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    pNextBfdFsMIStdBfdSessEntry =
        BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);

    if (pNextBfdFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4NextFsMIStdBfdSessIndex =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessIndex
                nextFsMIStdBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIStdBfdSessPerfTable (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 *pu4NextFsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        UINT4 *pu4NextFsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    tBfdFsMIStdBfdSessEntry *pNextBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    pNextBfdFsMIStdBfdSessEntry =
        BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);

    if (pNextBfdFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4NextFsMIStdBfdSessIndex =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIBfdSessionTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessIndex
                nextFsMIStdBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIBfdSessionTable (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 *pu4NextFsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    UINT4 *pu4NextFsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    tBfdFsMIStdBfdSessEntry *pNextBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    pNextBfdFsMIStdBfdSessEntry =
        BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);

    if (pNextBfdFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4NextFsMIStdBfdSessIndex =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessIndex
                nextFsMIStdBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIBfdSessPerfTable (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 *pu4NextFsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     UINT4 *pu4NextFsMIStdBfdSessIndex)
{

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    tBfdFsMIStdBfdSessEntry *pNextBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    pNextBfdFsMIStdBfdSessEntry =
        BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);

    if (pNextBfdFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4NextFsMIStdBfdSessIndex =
        pNextBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdBfdSessDiscMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessDiscriminator
                nextFsMIStdBfdSessDiscriminator
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIStdBfdSessDiscMapTable (UINT4 u4FsMIStdBfdContextId,
                                           UINT4 *pu4NextFsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessDiscriminator,
                                           UINT4
                                           *pu4NextFsMIStdBfdSessDiscriminator)
{

    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    tBfdFsMIStdBfdSessDiscMapEntry *pNextBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID);

    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessDiscMapEntry, 0,
            sizeof (tBfdFsMIStdBfdSessDiscMapEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscriminator =
        u4FsMIStdBfdSessDiscriminator;

    pNextBfdFsMIStdBfdSessDiscMapEntry =
        BfdGetNextFsMIStdBfdSessDiscMapTable (pBfdFsMIStdBfdSessDiscMapEntry);

    if (pNextBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessDiscMapEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId;

    *pu4NextFsMIStdBfdSessDiscriminator =
        pNextBfdFsMIStdBfdSessDiscMapEntry->MibObject.
        u4FsMIStdBfdSessDiscriminator;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessDiscMapEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdBfdSessIpMapTable
 Input       :  The Indices
                FsMIStdBfdContextId
                nextFsMIStdBfdContextId
                FsMIStdBfdSessInterface
                nextFsMIStdBfdSessInterface
                FsMIStdBfdSessSrcAddrType
                nextFsMIStdBfdSessSrcAddrType
                FsMIStdBfdSessSrcAddr
                nextFsMIStdBfdSessSrcAddr
                FsMIStdBfdSessDstAddrType
                nextFsMIStdBfdSessDstAddrType
                FsMIStdBfdSessDstAddr
                nextFsMIStdBfdSessDstAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsMIStdBfdSessIpMapTable (UINT4 u4FsMIStdBfdContextId,
                                         UINT4 *pu4NextFsMIStdBfdContextId,
                                         INT4 i4FsMIStdBfdSessInterface,
                                         INT4 *pi4NextFsMIStdBfdSessInterface,
                                         INT4 i4FsMIStdBfdSessSrcAddrType,
                                         INT4 *pi4NextFsMIStdBfdSessSrcAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdBfdSessSrcAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMIStdBfdSessSrcAddr,
                                         INT4 i4FsMIStdBfdSessDstAddrType,
                                         INT4 *pi4NextFsMIStdBfdSessDstAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIStdBfdSessDstAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsMIStdBfdSessDstAddr)
{

    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    tBfdFsMIStdBfdSessIpMapEntry *pNextBfdFsMIStdBfdSessIpMapEntry = NULL;

    pBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID);

    if (pBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessIpMapEntry, 0,
            sizeof (tBfdFsMIStdBfdSessIpMapEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessInterface =
        i4FsMIStdBfdSessInterface;

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pFsMIStdBfdSessSrcAddr->i4_Length);

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pFsMIStdBfdSessSrcAddr->i4_Length;
    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        i4FsMIStdBfdSessDstAddrType;

    MEMCPY (pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pFsMIStdBfdSessDstAddr->pu1_OctetList,
            pFsMIStdBfdSessDstAddr->i4_Length);

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pFsMIStdBfdSessDstAddr->i4_Length;

    pNextBfdFsMIStdBfdSessIpMapEntry =
        BfdGetNextFsMIStdBfdSessIpMapTable (pBfdFsMIStdBfdSessIpMapEntry);

    if (pNextBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessIpMapEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMIStdBfdContextId =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId;

    *pi4NextFsMIStdBfdSessInterface =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessInterface;

    *pi4NextFsMIStdBfdSessSrcAddrType =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pNextFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.
            au1FsMIStdBfdSessSrcAddr,
            pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.
            i4FsMIStdBfdSessSrcAddrLen);
    pNextFsMIStdBfdSessSrcAddr->i4_Length =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

    *pi4NextFsMIStdBfdSessDstAddrType =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType;

    MEMCPY (pNextFsMIStdBfdSessDstAddr->pu1_OctetList,
            pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.
            au1FsMIStdBfdSessDstAddr,
            pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.
            i4FsMIStdBfdSessDstAddrLen);
    pNextFsMIStdBfdSessDstAddr->i4_Length =
        pNextBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessIpMapEntry);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIStdBfdAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdAdminStatus (UINT4 u4FsMIStdBfdContextId,
                             INT4 *pi4RetValFsMIStdBfdAdminStatus)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdAdminStatus =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessNotificationsEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessNotificationsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessNotificationsEnable (UINT4 u4FsMIStdBfdContextId,
                                         INT4
                                         *pi4RetValFsMIStdBfdSessNotificationsEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessNotificationsEnable =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSystemControl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSystemControl (UINT4 u4FsMIStdBfdContextId,
                            INT4 *pi4RetValFsMIBfdSystemControl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSystemControl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdTraceLevel
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdTraceLevel (UINT4 u4FsMIStdBfdContextId,
                         INT4 *pi4RetValFsMIBfdTraceLevel)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdTraceLevel =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdTrapEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdTrapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdTrapEnable (UINT4 u4FsMIStdBfdContextId,
                         INT4 *pi4RetValFsMIBfdTrapEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdTrapEnable =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdGblSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdGblSessOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdGblSessOperMode (UINT4 u4FsMIStdBfdContextId,
                              INT4 *pi4RetValFsMIBfdGblSessOperMode)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdGblSessOperMode =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdGblSessOperMode;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdGblDesiredMinTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdGblDesiredMinTxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdGblDesiredMinTxIntvl (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 *pu4RetValFsMIBfdGblDesiredMinTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdGblDesiredMinTxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblDesiredMinTxIntvl;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdGblReqMinRxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdGblReqMinRxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdGblReqMinRxIntvl (UINT4 u4FsMIStdBfdContextId,
                               UINT4 *pu4RetValFsMIBfdGblReqMinRxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdGblReqMinRxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblReqMinRxIntvl;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdGblDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdGblDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdGblDetectMult (UINT4 u4FsMIStdBfdContextId,
                            UINT4 *pu4RetValFsMIBfdGblDetectMult)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdGblDetectMult =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdGblSlowTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdGblSlowTxIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdGblSlowTxIntvl (UINT4 u4FsMIStdBfdContextId,
                             UINT4 *pu4RetValFsMIBfdGblSlowTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdGblSlowTxIntvl =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdMemAllocFailure
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdMemAllocFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdMemAllocFailure (UINT4 u4FsMIStdBfdContextId,
                              UINT4 *pu4RetValFsMIBfdMemAllocFailure)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdMemAllocFailure =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdMemAllocFailure;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdInputQOverFlows
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                UINT4 *pu4RetValFsMIBfdInputQOverFlows
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdInputQOverFlows (UINT4 u4FsMIStdBfdContextId,
                              UINT4 *pu4RetValFsMIBfdInputQOverFlows)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdInputQOverFlows =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdInputQOverFlows;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdClrGblStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdClrGblStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdClrGblStats (UINT4 u4FsMIStdBfdContextId,
                          INT4 *pi4RetValFsMIBfdClrGblStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdClrGblStats =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdClrAllSessStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                INT4 *pi4RetValFsMIBfdClrAllSessStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdClrAllSessStats (UINT4 u4FsMIStdBfdContextId,
                              INT4 *pi4RetValFsMIBfdClrAllSessStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdClrAllSessStats =
        pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIBfdClrAllSessStats;

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessVersionNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessVersionNumber (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 *pu4RetValFsMIStdBfdSessVersionNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessVersionNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessType (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 *pi4RetValFsMIStdBfdSessType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDiscriminator
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessDiscriminator
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDiscriminator (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 *pu4RetValFsMIStdBfdSessDiscriminator)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessDiscriminator =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessRemoteDiscr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessRemoteDiscr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessRemoteDiscr (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 UINT4 *pu4RetValFsMIStdBfdSessRemoteDiscr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessRemoteDiscr =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDestinationUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessDestinationUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDestinationUdpPort (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        UINT4
                                        *pu4RetValFsMIStdBfdSessDestinationUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessDestinationUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessSourceUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessSourceUdpPort (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 *pu4RetValFsMIStdBfdSessSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessSourceUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessEchoSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessEchoSourceUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessEchoSourceUdpPort (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       UINT4
                                       *pu4RetValFsMIStdBfdSessEchoSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessEchoSourceUdpPort =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessAdminStatus (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 *pi4RetValFsMIStdBfdSessAdminStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessAdminStatus =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessState
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessState (UINT4 u4FsMIStdBfdContextId,
                           UINT4 u4FsMIStdBfdSessIndex,
                           INT4 *pi4RetValFsMIStdBfdSessState)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessState =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessState + 1;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessRemoteHeardFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessRemoteHeardFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessRemoteHeardFlag (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     INT4
                                     *pi4RetValFsMIStdBfdSessRemoteHeardFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessRemoteHeardFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDiag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessDiag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDiag (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 *pi4RetValFsMIStdBfdSessDiag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessDiag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDiag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessOperMode (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              INT4 *pi4RetValFsMIStdBfdSessOperMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessOperMode =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDemandModeDesiredFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessDemandModeDesiredFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDemandModeDesiredFlag (UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           INT4
                                           *pi4RetValFsMIStdBfdSessDemandModeDesiredFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessDemandModeDesiredFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDemandModeDesiredFlag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessControlPlaneIndepFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessControlPlaneIndepFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessControlPlaneIndepFlag (UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           INT4
                                           *pi4RetValFsMIStdBfdSessControlPlaneIndepFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessControlPlaneIndepFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessControlPlaneIndepFlag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessMultipointFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessMultipointFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessMultipointFlag (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    INT4 *pi4RetValFsMIStdBfdSessMultipointFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessMultipointFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessInterface
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessInterface (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 *pi4RetValFsMIStdBfdSessInterface)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessInterface =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessSrcAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessSrcAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessSrcAddrType (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 *pi4RetValFsMIStdBfdSessSrcAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessSrcAddrType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessSrcAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMIStdBfdSessSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessSrcAddr (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIStdBfdSessSrcAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen);
    pRetValFsMIStdBfdSessSrcAddr->i4_Length =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDstAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessDstAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDstAddrType (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 *pi4RetValFsMIStdBfdSessDstAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessDstAddrType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDstAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMIStdBfdSessDstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDstAddr (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMIStdBfdSessDstAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMIStdBfdSessDstAddr->pu1_OctetList,
            pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen);
    pRetValFsMIStdBfdSessDstAddr->i4_Length =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessGTSM
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessGTSM
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessGTSM (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 *pi4RetValFsMIStdBfdSessGTSM)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessGTSM =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessGTSMTTL
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessGTSMTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessGTSMTTL (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 *pu4RetValFsMIStdBfdSessGTSMTTL)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessGTSMTTL =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDesiredMinTxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessDesiredMinTxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDesiredMinTxInterval (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          *pu4RetValFsMIStdBfdSessDesiredMinTxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessDesiredMinTxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessReqMinRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessReqMinRxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessReqMinRxInterval (UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      UINT4
                                      *pu4RetValFsMIStdBfdSessReqMinRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessReqMinRxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessReqMinEchoRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessReqMinEchoRxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessReqMinEchoRxInterval (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          *pu4RetValFsMIStdBfdSessReqMinEchoRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessReqMinEchoRxInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDetectMult (UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                UINT4 *pu4RetValFsMIStdBfdSessDetectMult)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessDetectMult =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessNegotiatedInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessNegotiatedInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessNegotiatedInterval (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        UINT4
                                        *pu4RetValFsMIStdBfdSessNegotiatedInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessNegotiatedInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessNegotiatedEchoInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessNegotiatedEchoInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessNegotiatedEchoInterval (UINT4 u4FsMIStdBfdContextId,
                                            UINT4 u4FsMIStdBfdSessIndex,
                                            UINT4
                                            *pu4RetValFsMIStdBfdSessNegotiatedEchoInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessNegotiatedEchoInterval =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessNegotiatedEchoInterval;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessNegotiatedDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessNegotiatedDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessNegotiatedDetectMult (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          *pu4RetValFsMIStdBfdSessNegotiatedDetectMult)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessNegotiatedDetectMult =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedDetectMult;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessAuthPresFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessAuthPresFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessAuthPresFlag (UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  INT4 *pi4RetValFsMIStdBfdSessAuthPresFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessAuthPresFlag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessAuthenticationType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMIStdBfdSessAuthenticationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessAuthenticationType (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        INT4
                                        *pi4RetValFsMIStdBfdSessAuthenticationType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */

    *pi4RetValFsMIStdBfdSessAuthenticationType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessAuthenticationKeyID
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessAuthenticationKeyID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessAuthenticationKeyID (UINT4 u4FsMIStdBfdContextId,
                                         UINT4 u4FsMIStdBfdSessIndex,
                                         INT4
                                         *pi4RetValFsMIStdBfdSessAuthenticationKeyID)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessAuthenticationKeyID =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessAuthenticationKey
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsMIStdBfdSessAuthenticationKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessAuthenticationKey (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsMIStdBfdSessAuthenticationKey)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMIStdBfdSessAuthenticationKey->pu1_OctetList,
            pBfdFsMIStdBfdSessEntry->MibObject.
            au1FsMIStdBfdSessAuthenticationKey,
            pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen);
    pRetValFsMIStdBfdSessAuthenticationKey->i4_Length =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessStorType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessStorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessStorType (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              INT4 *pi4RetValFsMIStdBfdSessStorType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessStorType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessRowStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessRowStatus (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 *pi4RetValFsMIStdBfdSessRowStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessRowStatus =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktIn
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfCtrlPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktIn (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 *pu4RetValFsMIStdBfdSessPerfCtrlPktIn)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfCtrlPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktIn;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktOut
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfCtrlPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktOut (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    UINT4
                                    *pu4RetValFsMIStdBfdSessPerfCtrlPktOut)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfCtrlPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktOut;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktDrop
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfCtrlPktDrop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDrop (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     UINT4
                                     *pu4RetValFsMIStdBfdSessPerfCtrlPktDrop)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfCtrlPktDrop =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfCtrlPktDrop;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktDropLastTime
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfCtrlPktDropLastTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDropLastTime (UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex,
                                             UINT4
                                             *pu4RetValFsMIStdBfdSessPerfCtrlPktDropLastTime)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfCtrlPktDropLastTime =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfCtrlPktDropLastTime;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktIn
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfEchoPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktIn (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 *pu4RetValFsMIStdBfdSessPerfEchoPktIn)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfEchoPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktIn;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktOut
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfEchoPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktOut (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    UINT4
                                    *pu4RetValFsMIStdBfdSessPerfEchoPktOut)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfEchoPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktOut;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktDrop
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfEchoPktDrop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktDrop (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     UINT4
                                     *pu4RetValFsMIStdBfdSessPerfEchoPktDrop)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfEchoPktDrop =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfEchoPktDrop;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktDropLastTime
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfEchoPktDropLastTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktDropLastTime (UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex,
                                             UINT4
                                             *pu4RetValFsMIStdBfdSessPerfEchoPktDropLastTime)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfEchoPktDropLastTime =
        pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessPerfEchoPktDropLastTime;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessUpTime
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessUpTime (UINT4 u4FsMIStdBfdContextId,
                            UINT4 u4FsMIStdBfdSessIndex,
                            UINT4 *pu4RetValFsMIStdBfdSessUpTime)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessUpTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessUpTime;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfLastSessDownTime
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfLastSessDownTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfLastSessDownTime (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          *pu4RetValFsMIStdBfdSessPerfLastSessDownTime)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfLastSessDownTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfLastSessDownTime;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfLastCommLostDiag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIStdBfdSessPerfLastCommLostDiag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfLastCommLostDiag (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          INT4
                                          *pi4RetValFsMIStdBfdSessPerfLastCommLostDiag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIStdBfdSessPerfLastCommLostDiag =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessPerfLastCommLostDiag;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfSessUpCount
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfSessUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfSessUpCount (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     UINT4
                                     *pu4RetValFsMIStdBfdSessPerfSessUpCount)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfSessUpCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfSessUpCount;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfDiscTime
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessPerfDiscTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfDiscTime (UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  UINT4 *pu4RetValFsMIStdBfdSessPerfDiscTime)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessPerfDiscTime =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessPerfDiscTime;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktInHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfCtrlPktInHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktInHC (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMIStdBfdSessPerfCtrlPktInHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfCtrlPktInHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktInHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktOutHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfCtrlPktOutHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktOutHC (UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdBfdSessPerfCtrlPktOutHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfCtrlPktOutHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktOutHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfCtrlPktDropHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfCtrlPktDropHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfCtrlPktDropHC (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFsMIStdBfdSessPerfCtrlPktDropHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfCtrlPktDropHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfCtrlPktDropHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktInHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfEchoPktInHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktInHC (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMIStdBfdSessPerfEchoPktInHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfEchoPktInHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktInHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktOutHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfEchoPktOutHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktOutHC (UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMIStdBfdSessPerfEchoPktOutHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfEchoPktOutHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktOutHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessPerfEchoPktDropHC
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsMIStdBfdSessPerfEchoPktDropHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessPerfEchoPktDropHC (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFsMIStdBfdSessPerfEchoPktDropHC)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsMIStdBfdSessPerfEchoPktDropHC =
        pBfdFsMIStdBfdSessEntry->MibObject.u8FsMIStdBfdSessPerfEchoPktDropHC;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessRole
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessRole (UINT4 u4FsMIStdBfdContextId, UINT4 u4FsMIStdBfdSessIndex,
                       INT4 *pi4RetValFsMIBfdSessRole)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessRole =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessMode (UINT4 u4FsMIStdBfdContextId, UINT4 u4FsMIStdBfdSessIndex,
                       INT4 *pi4RetValFsMIBfdSessMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessMode =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessRemoteDiscr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessRemoteDiscr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessRemoteDiscr (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessRemoteDiscr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessRemoteDiscr =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessEXPValue
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessEXPValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessEXPValue (UINT4 u4FsMIStdBfdContextId,
                           UINT4 u4FsMIStdBfdSessIndex,
                           UINT4 *pu4RetValFsMIBfdSessEXPValue)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessEXPValue =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessTmrNegotiate
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessTmrNegotiate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessTmrNegotiate (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 *pi4RetValFsMIBfdSessTmrNegotiate)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessTmrNegotiate =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessOffld
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessOffld
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessOffld (UINT4 u4FsMIStdBfdContextId,
                        UINT4 u4FsMIStdBfdSessIndex,
                        INT4 *pi4RetValFsMIBfdSessOffld)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessOffld =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessEncapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessEncapType (UINT4 u4FsMIStdBfdContextId,
                            UINT4 u4FsMIStdBfdSessIndex,
                            INT4 *pi4RetValFsMIBfdSessEncapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessEncapType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessAdminCtrlReq
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessAdminCtrlReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessAdminCtrlReq (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 *pi4RetValFsMIBfdSessAdminCtrlReq)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessAdminCtrlReq =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessAdminCtrlErrReason
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessAdminCtrlErrReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessAdminCtrlErrReason (UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     INT4
                                     *pi4RetValFsMIBfdSessAdminCtrlErrReason)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessAdminCtrlErrReason =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessMapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdSessMapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessMapType (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 *pi4RetValFsMIBfdSessMapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdSessMapType =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessMapPointer
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                tSNMP_OID_TYPE * pRetValFsMIBfdSessMapPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessMapPointer (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OID_TYPE * pRetValFsMIBfdSessMapPointer)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsMIBfdSessMapPointer->pu4_OidList,
            pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
            pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen *
            sizeof (UINT4));
    pRetValFsMIBfdSessMapPointer->u4_Length =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessCardNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessCardNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessCardNumber (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 *pu4RetValFsMIBfdSessCardNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessCardNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessSlotNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessSlotNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessSlotNumber (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 *pu4RetValFsMIBfdSessSlotNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessSlotNumber =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessRegisteredClients
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 
                The Object
                UINT4 *pu4RetValFsMIBfdSessRegisteredClients
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessRegisteredClients (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    UINT4
                                    *pu4RetValFsMIBfdSessRegisteredClients)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessRegisteredClients =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessPerfCCPktIn
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessPerfCCPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessPerfCCPktIn (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessPerfCCPktIn)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessPerfCCPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktIn;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessPerfCCPktOut
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessPerfCCPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessPerfCCPktOut (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               UINT4 *pu4RetValFsMIBfdSessPerfCCPktOut)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessPerfCCPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCCPktOut;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessPerfCVPktIn
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessPerfCVPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessPerfCVPktIn (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessPerfCVPktIn)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessPerfCVPktIn =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktIn;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessPerfCVPktOut
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessPerfCVPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessPerfCVPktOut (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               UINT4 *pu4RetValFsMIBfdSessPerfCVPktOut)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessPerfCVPktOut =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessPerfCVPktOut;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessMisDefCount
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessMisDefCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessMisDefCount (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessMisDefCount)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessMisDefCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessMisDefCount;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessLocDefCount
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessLocDefCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessLocDefCount (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessLocDefCount)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessLocDefCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessLocDefCount;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessRdiInCount
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessRdiInCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessRdiInCount (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 *pu4RetValFsMIBfdSessRdiInCount)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessRdiInCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiInCount;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdSessRdiOutCount
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                UINT4 *pu4RetValFsMIBfdSessRdiOutCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdSessRdiOutCount (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 *pu4RetValFsMIBfdSessRdiOutCount)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIBfdSessRdiOutCount =
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRdiOutCount;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIBfdClearStats
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                INT4 *pi4RetValFsMIBfdClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIBfdClearStats (UINT4 u4FsMIStdBfdContextId,
                         UINT4 u4FsMIStdBfdSessIndex,
                         INT4 *pi4RetValFsMIBfdClearStats)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;

    if (BfdGetAllFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMIBfdClearStats =
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessDiscMapIndex
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessDiscriminator

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessDiscMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessDiscMapIndex (UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessDiscriminator,
                                  UINT4 *pu4RetValFsMIStdBfdSessDiscMapIndex)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    pBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID);

    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessDiscMapEntry, 0,
            sizeof (tBfdFsMIStdBfdSessDiscMapEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscriminator =
        u4FsMIStdBfdSessDiscriminator;

    if (BfdGetAllFsMIStdBfdSessDiscMapTable (pBfdFsMIStdBfdSessDiscMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessDiscMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessDiscMapIndex =
        pBfdFsMIStdBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessDiscMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdBfdSessIpMapIndex
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessInterface
                FsMIStdBfdSessSrcAddrType
                FsMIStdBfdSessSrcAddr
                FsMIStdBfdSessDstAddrType
                FsMIStdBfdSessDstAddr

                The Object 
                UINT4 *pu4RetValFsMIStdBfdSessIpMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdBfdSessIpMapIndex (UINT4 u4FsMIStdBfdContextId,
                                INT4 i4FsMIStdBfdSessInterface,
                                INT4 i4FsMIStdBfdSessSrcAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdBfdSessSrcAddr,
                                INT4 i4FsMIStdBfdSessDstAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIStdBfdSessDstAddr,
                                UINT4 *pu4RetValFsMIStdBfdSessIpMapIndex)
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    pBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID);

    if (pBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessIpMapEntry, 0,
            sizeof (tBfdFsMIStdBfdSessIpMapEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessInterface =
        i4FsMIStdBfdSessInterface;

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        i4FsMIStdBfdSessSrcAddrType;

    MEMCPY (pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pFsMIStdBfdSessSrcAddr->i4_Length);

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pFsMIStdBfdSessSrcAddr->i4_Length;
    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        i4FsMIStdBfdSessDstAddrType;

    MEMCPY (pBfdFsMIStdBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pFsMIStdBfdSessDstAddr->pu1_OctetList,
            pFsMIStdBfdSessDstAddr->i4_Length);

    pBfdFsMIStdBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pFsMIStdBfdSessDstAddr->i4_Length;

    if (BfdGetAllFsMIStdBfdSessIpMapTable (pBfdFsMIStdBfdSessIpMapEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessIpMapEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMIStdBfdSessIpMapIndex =
        pBfdFsMIStdBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex;

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessIpMapEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIStdBfdAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdAdminStatus (UINT4 u4FsMIStdBfdContextId,
                             INT4 i4SetValFsMIStdBfdAdminStatus)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus =
        i4SetValFsMIStdBfdAdminStatus;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessNotificationsEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessNotificationsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessNotificationsEnable (UINT4 u4FsMIStdBfdContextId,
                                         INT4
                                         i4SetValFsMIStdBfdSessNotificationsEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable =
        i4SetValFsMIStdBfdSessNotificationsEnable;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSystemControl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSystemControl (UINT4 u4FsMIStdBfdContextId,
                            INT4 i4SetValFsMIBfdSystemControl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl =
        i4SetValFsMIBfdSystemControl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdTraceLevel
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdTraceLevel (UINT4 u4FsMIStdBfdContextId,
                         INT4 i4SetValFsMIBfdTraceLevel)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel =
        i4SetValFsMIBfdTraceLevel;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdTrapEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdTrapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdTrapEnable (UINT4 u4FsMIStdBfdContextId,
                         INT4 i4SetValFsMIBfdTrapEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable =
        i4SetValFsMIBfdTrapEnable;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdGblSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdGblSessOperMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdGblSessOperMode (UINT4 u4FsMIStdBfdContextId,
                              INT4 i4SetValFsMIBfdGblSessOperMode)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdGblSessOperMode =
        i4SetValFsMIBfdGblSessOperMode;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdGblDesiredMinTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  UINT4 u4SetValFsMIBfdGblDesiredMinTxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdGblDesiredMinTxIntvl (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4SetValFsMIBfdGblDesiredMinTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblDesiredMinTxIntvl = u4SetValFsMIBfdGblDesiredMinTxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDesiredMinTxIntvl =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdGblReqMinRxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  UINT4 u4SetValFsMIBfdGblReqMinRxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdGblReqMinRxIntvl (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4SetValFsMIBfdGblReqMinRxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblReqMinRxIntvl =
        u4SetValFsMIBfdGblReqMinRxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblReqMinRxIntvl =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdGblDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  UINT4 u4SetValFsMIBfdGblDetectMult
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdGblDetectMult (UINT4 u4FsMIStdBfdContextId,
                            UINT4 u4SetValFsMIBfdGblDetectMult)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult =
        u4SetValFsMIBfdGblDetectMult;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdGblSlowTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  UINT4 u4SetValFsMIBfdGblSlowTxIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdGblSlowTxIntvl (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4SetValFsMIBfdGblSlowTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl =
        u4SetValFsMIBfdGblSlowTxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdClrGblStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdClrGblStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdClrGblStats (UINT4 u4FsMIStdBfdContextId,
                          INT4 i4SetValFsMIBfdClrGblStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats =
        i4SetValFsMIBfdClrGblStats;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdClrAllSessStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
             :  INT4 i4SetValFsMIBfdClrAllSessStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdClrAllSessStats (UINT4 u4FsMIStdBfdContextId,
                              INT4 i4SetValFsMIBfdClrAllSessStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrAllSessStats =
        i4SetValFsMIBfdClrAllSessStats;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessVersionNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessVersionNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessVersionNumber (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 u4SetValFsMIStdBfdSessVersionNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber =
        u4SetValFsMIStdBfdSessVersionNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessType (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 i4SetValFsMIStdBfdSessType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType =
        i4SetValFsMIStdBfdSessType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDestinationUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessDestinationUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDestinationUdpPort (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        UINT4
                                        u4SetValFsMIStdBfdSessDestinationUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort =
        u4SetValFsMIStdBfdSessDestinationUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessSourceUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessSourceUdpPort (UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 u4SetValFsMIStdBfdSessSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort =
        u4SetValFsMIStdBfdSessSourceUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessEchoSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessEchoSourceUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessEchoSourceUdpPort (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       UINT4
                                       u4SetValFsMIStdBfdSessEchoSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort =
        u4SetValFsMIStdBfdSessEchoSourceUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessAdminStatus (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 i4SetValFsMIStdBfdSessAdminStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus =
        i4SetValFsMIStdBfdSessAdminStatus;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessOperMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessOperMode (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              INT4 i4SetValFsMIStdBfdSessOperMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode =
        i4SetValFsMIStdBfdSessOperMode;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDemandModeDesiredFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessDemandModeDesiredFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDemandModeDesiredFlag (UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           INT4
                                           i4SetValFsMIStdBfdSessDemandModeDesiredFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDemandModeDesiredFlag =
        i4SetValFsMIStdBfdSessDemandModeDesiredFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessControlPlaneIndepFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessControlPlaneIndepFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessControlPlaneIndepFlag (UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           INT4
                                           i4SetValFsMIStdBfdSessControlPlaneIndepFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessControlPlaneIndepFlag =
        i4SetValFsMIStdBfdSessControlPlaneIndepFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessMultipointFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessMultipointFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessMultipointFlag (UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    INT4 i4SetValFsMIStdBfdSessMultipointFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag =
        i4SetValFsMIStdBfdSessMultipointFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessInterface
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessInterface (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 i4SetValFsMIStdBfdSessInterface)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
        i4SetValFsMIStdBfdSessInterface;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessSrcAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessSrcAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessSrcAddrType (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 i4SetValFsMIStdBfdSessSrcAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        i4SetValFsMIStdBfdSessSrcAddrType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessSrcAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMIStdBfdSessSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessSrcAddr (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMIStdBfdSessSrcAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pSetValFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pSetValFsMIStdBfdSessSrcAddr->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pSetValFsMIStdBfdSessSrcAddr->i4_Length;

    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDstAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessDstAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDstAddrType (UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 i4SetValFsMIStdBfdSessDstAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        i4SetValFsMIStdBfdSessDstAddrType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDstAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMIStdBfdSessDstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDstAddr (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMIStdBfdSessDstAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pSetValFsMIStdBfdSessDstAddr->pu1_OctetList,
            pSetValFsMIStdBfdSessDstAddr->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pSetValFsMIStdBfdSessDstAddr->i4_Length;

    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessGTSM
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessGTSM
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessGTSM (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 i4SetValFsMIStdBfdSessGTSM)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM =
        i4SetValFsMIStdBfdSessGTSM;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessGTSMTTL
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessGTSMTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessGTSMTTL (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 u4SetValFsMIStdBfdSessGTSMTTL)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL =
        u4SetValFsMIStdBfdSessGTSMTTL;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDesiredMinTxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessDesiredMinTxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDesiredMinTxInterval (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          u4SetValFsMIStdBfdSessDesiredMinTxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval =
        u4SetValFsMIStdBfdSessDesiredMinTxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessReqMinRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessReqMinRxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessReqMinRxInterval (UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      UINT4
                                      u4SetValFsMIStdBfdSessReqMinRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
        u4SetValFsMIStdBfdSessReqMinRxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessReqMinEchoRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessReqMinEchoRxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessReqMinEchoRxInterval (UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          u4SetValFsMIStdBfdSessReqMinEchoRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval =
        u4SetValFsMIStdBfdSessReqMinEchoRxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIStdBfdSessDetectMult
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessDetectMult (UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                UINT4 u4SetValFsMIStdBfdSessDetectMult)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
        u4SetValFsMIStdBfdSessDetectMult;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessAuthPresFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessAuthPresFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessAuthPresFlag (UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  INT4 i4SetValFsMIStdBfdSessAuthPresFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag =
        i4SetValFsMIStdBfdSessAuthPresFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessAuthenticationType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMIStdBfdSessAuthenticationType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessAuthenticationType (UINT4 u4FsMIStdBfdContextId,
                                        UINT4 u4FsMIStdBfdSessIndex,
                                        INT4
                                        i4SetValFsMIStdBfdSessAuthenticationType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationType =
        i4SetValFsMIStdBfdSessAuthenticationType;

    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessAuthenticationKeyID
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessAuthenticationKeyID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessAuthenticationKeyID (UINT4 u4FsMIStdBfdContextId,
                                         UINT4 u4FsMIStdBfdSessIndex,
                                         INT4
                                         i4SetValFsMIStdBfdSessAuthenticationKeyID)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID =
        i4SetValFsMIStdBfdSessAuthenticationKeyID;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID =
        OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessAuthenticationKey
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsMIStdBfdSessAuthenticationKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessAuthenticationKey (UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsMIStdBfdSessAuthenticationKey)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
            au1FsMIStdBfdSessAuthenticationKey,
            pSetValFsMIStdBfdSessAuthenticationKey->pu1_OctetList,
            pSetValFsMIStdBfdSessAuthenticationKey->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen =
        pSetValFsMIStdBfdSessAuthenticationKey->i4_Length;

    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessStorType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessStorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessStorType (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              INT4 i4SetValFsMIStdBfdSessStorType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
        i4SetValFsMIStdBfdSessStorType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdBfdSessRowStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIStdBfdSessRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdBfdSessRowStatus (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 i4SetValFsMIStdBfdSessRowStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
        i4SetValFsMIStdBfdSessRowStatus;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessRole
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessRole (UINT4 u4FsMIStdBfdContextId, UINT4 u4FsMIStdBfdSessIndex,
                       INT4 i4SetValFsMIBfdSessRole)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole =
        i4SetValFsMIBfdSessRole;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessMode (UINT4 u4FsMIStdBfdContextId, UINT4 u4FsMIStdBfdSessIndex,
                       INT4 i4SetValFsMIBfdSessMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode =
        i4SetValFsMIBfdSessMode;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessRemoteDiscr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIBfdSessRemoteDiscr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessRemoteDiscr (UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 u4SetValFsMIBfdSessRemoteDiscr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
        u4SetValFsMIBfdSessRemoteDiscr;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessEXPValue
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIBfdSessEXPValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessEXPValue (UINT4 u4FsMIStdBfdContextId,
                           UINT4 u4FsMIStdBfdSessIndex,
                           UINT4 u4SetValFsMIBfdSessEXPValue)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue =
        u4SetValFsMIBfdSessEXPValue;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessTmrNegotiate
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessTmrNegotiate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessTmrNegotiate (UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 i4SetValFsMIBfdSessTmrNegotiate)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate =
        i4SetValFsMIBfdSessTmrNegotiate;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessOffld
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessOffld
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessOffld (UINT4 u4FsMIStdBfdContextId,
                        UINT4 u4FsMIStdBfdSessIndex,
                        INT4 i4SetValFsMIBfdSessOffld)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld =
        i4SetValFsMIBfdSessOffld;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessEncapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessEncapType (UINT4 u4FsMIStdBfdContextId,
                            UINT4 u4FsMIStdBfdSessIndex,
                            INT4 i4SetValFsMIBfdSessEncapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
        i4SetValFsMIBfdSessEncapType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessMapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdSessMapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessMapType (UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 i4SetValFsMIBfdSessMapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =
        i4SetValFsMIBfdSessMapType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessMapPointer
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  tSNMP_OID_TYPE *pSetValFsMIBfdSessMapPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessMapPointer (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             tSNMP_OID_TYPE * pSetValFsMIBfdSessMapPointer)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
            pSetValFsMIBfdSessMapPointer->pu4_OidList,
            pSetValFsMIBfdSessMapPointer->u4_Length * sizeof (UINT4));
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen =
        pSetValFsMIBfdSessMapPointer->u4_Length;

    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessCardNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIBfdSessCardNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessCardNumber (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 u4SetValFsMIBfdSessCardNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber =
        u4SetValFsMIBfdSessCardNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdSessSlotNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  UINT4 u4SetValFsMIBfdSessSlotNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdSessSlotNumber (UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             UINT4 u4SetValFsMIBfdSessSlotNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
        u4SetValFsMIBfdSessSlotNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIBfdClearStats
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
             :  INT4 i4SetValFsMIBfdClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIBfdClearStats (UINT4 u4FsMIStdBfdContextId,
                         UINT4 u4FsMIStdBfdSessIndex,
                         INT4 i4SetValFsMIBfdClearStats)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats =
        i4SetValFsMIBfdClearStats;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats = OSIX_TRUE;

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIStdBfdAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdAdminStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                INT4 i4TestValFsMIStdBfdAdminStatus)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus =
        i4TestValFsMIStdBfdAdminStatus;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessNotificationsEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIStdBfdSessNotificationsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessNotificationsEnable (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMIStdBfdContextId,
                                            INT4
                                            i4TestValFsMIStdBfdSessNotificationsEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        i4FsMIStdBfdSessNotificationsEnable =
        i4TestValFsMIStdBfdSessNotificationsEnable;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSystemControl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSystemControl (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                               INT4 i4TestValFsMIBfdSystemControl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl =
        i4TestValFsMIBfdSystemControl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdTraceLevel
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdTraceLevel (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                            INT4 i4TestValFsMIBfdTraceLevel)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel =
        i4TestValFsMIBfdTraceLevel;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdTrapEnable
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdTrapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdTrapEnable (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                            INT4 i4TestValFsMIBfdTrapEnable)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable =
        i4TestValFsMIBfdTrapEnable;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdGblSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdGblSessOperMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdGblSessOperMode (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIStdBfdContextId,
                                 INT4 i4TestValFsMIBfdGblSessOperMode)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdGblSessOperMode =
        i4TestValFsMIBfdGblSessOperMode;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdGblDesiredMinTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdGblDesiredMinTxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdGblDesiredMinTxIntvl (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMIStdBfdContextId,
                                      UINT4
                                      u4TestValFsMIBfdGblDesiredMinTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIBfdGblDesiredMinTxIntvl = u4TestValFsMIBfdGblDesiredMinTxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDesiredMinTxIntvl =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdGblReqMinRxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdGblReqMinRxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdGblReqMinRxIntvl (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4TestValFsMIBfdGblReqMinRxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblReqMinRxIntvl =
        u4TestValFsMIBfdGblReqMinRxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblReqMinRxIntvl =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdGblDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdGblDetectMult
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdGblDetectMult (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4TestValFsMIBfdGblDetectMult)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult =
        u4TestValFsMIBfdGblDetectMult;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdGblSlowTxIntvl
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdGblSlowTxIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdGblSlowTxIntvl (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4TestValFsMIBfdGblSlowTxIntvl)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl =
        u4TestValFsMIBfdGblSlowTxIntvl;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdClrGblStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdClrGblStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdClrGblStats (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                             INT4 i4TestValFsMIBfdClrGblStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats =
        i4TestValFsMIBfdClrGblStats;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdClrAllSessStats
 Input       :  The Indices
                FsMIStdBfdContextId

                The Object 
                testValFsMIBfdClrAllSessStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdClrAllSessStats (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIStdBfdContextId,
                                 INT4 i4TestValFsMIBfdClrAllSessStats)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
        (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

    /* Assign the index */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrAllSessStats =
        i4TestValFsMIBfdClrAllSessStats;
    pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (pu4ErrorCode, pBfdFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *)
                            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessVersionNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessVersionNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessVersionNumber (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      UINT4
                                      u4TestValFsMIStdBfdSessVersionNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber =
        u4TestValFsMIStdBfdSessVersionNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessType (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             INT4 i4TestValFsMIStdBfdSessType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType =
        i4TestValFsMIStdBfdSessType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDestinationUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDestinationUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDestinationUdpPort (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           UINT4
                                           u4TestValFsMIStdBfdSessDestinationUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort =
        u4TestValFsMIStdBfdSessDestinationUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessSourceUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessSourceUdpPort (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMIStdBfdContextId,
                                      UINT4 u4FsMIStdBfdSessIndex,
                                      UINT4
                                      u4TestValFsMIStdBfdSessSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort =
        u4TestValFsMIStdBfdSessSourceUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessEchoSourceUdpPort
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessEchoSourceUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessEchoSourceUdpPort (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          UINT4
                                          u4TestValFsMIStdBfdSessEchoSourceUdpPort)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort =
        u4TestValFsMIStdBfdSessEchoSourceUdpPort;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessAdminStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessAdminStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    INT4 i4TestValFsMIStdBfdSessAdminStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus =
        i4TestValFsMIStdBfdSessAdminStatus;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessOperMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessOperMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessOperMode (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 i4TestValFsMIStdBfdSessOperMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode =
        i4TestValFsMIStdBfdSessOperMode;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDemandModeDesiredFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDemandModeDesiredFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDemandModeDesiredFlag (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsMIStdBfdContextId,
                                              UINT4 u4FsMIStdBfdSessIndex,
                                              INT4
                                              i4TestValFsMIStdBfdSessDemandModeDesiredFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDemandModeDesiredFlag =
        i4TestValFsMIStdBfdSessDemandModeDesiredFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessControlPlaneIndepFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessControlPlaneIndepFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessControlPlaneIndepFlag (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsMIStdBfdContextId,
                                              UINT4 u4FsMIStdBfdSessIndex,
                                              INT4
                                              i4TestValFsMIStdBfdSessControlPlaneIndepFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessControlPlaneIndepFlag =
        i4TestValFsMIStdBfdSessControlPlaneIndepFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessMultipointFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessMultipointFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessMultipointFlag (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMIStdBfdContextId,
                                       UINT4 u4FsMIStdBfdSessIndex,
                                       INT4
                                       i4TestValFsMIStdBfdSessMultipointFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag =
        i4TestValFsMIStdBfdSessMultipointFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessInterface
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessInterface (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  INT4 i4TestValFsMIStdBfdSessInterface)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
        i4TestValFsMIStdBfdSessInterface;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessSrcAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessSrcAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessSrcAddrType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    INT4 i4TestValFsMIStdBfdSessSrcAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        i4TestValFsMIStdBfdSessSrcAddrType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessSrcAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessSrcAddr (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsMIStdBfdSessSrcAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            pTestValFsMIStdBfdSessSrcAddr->pu1_OctetList,
            pTestValFsMIStdBfdSessSrcAddr->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
        pTestValFsMIStdBfdSessSrcAddr->i4_Length;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDstAddrType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDstAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDstAddrType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIStdBfdContextId,
                                    UINT4 u4FsMIStdBfdSessIndex,
                                    INT4 i4TestValFsMIStdBfdSessDstAddrType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        i4TestValFsMIStdBfdSessDstAddrType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDstAddr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDstAddr (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsMIStdBfdSessDstAddr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            pTestValFsMIStdBfdSessDstAddr->pu1_OctetList,
            pTestValFsMIStdBfdSessDstAddr->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen =
        pTestValFsMIStdBfdSessDstAddr->i4_Length;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessGTSM
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessGTSM
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessGTSM (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             INT4 i4TestValFsMIStdBfdSessGTSM)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM =
        i4TestValFsMIStdBfdSessGTSM;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessGTSMTTL
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessGTSMTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessGTSMTTL (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                UINT4 u4TestValFsMIStdBfdSessGTSMTTL)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL =
        u4TestValFsMIStdBfdSessGTSMTTL;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDesiredMinTxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDesiredMinTxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDesiredMinTxInterval (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex,
                                             UINT4
                                             u4TestValFsMIStdBfdSessDesiredMinTxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval =
        u4TestValFsMIStdBfdSessDesiredMinTxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessReqMinRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessReqMinRxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessReqMinRxInterval (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMIStdBfdContextId,
                                         UINT4 u4FsMIStdBfdSessIndex,
                                         UINT4
                                         u4TestValFsMIStdBfdSessReqMinRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
        u4TestValFsMIStdBfdSessReqMinRxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessReqMinEchoRxInterval
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessReqMinEchoRxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessReqMinEchoRxInterval (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMIStdBfdContextId,
                                             UINT4 u4FsMIStdBfdSessIndex,
                                             UINT4
                                             u4TestValFsMIStdBfdSessReqMinEchoRxInterval)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval =
        u4TestValFsMIStdBfdSessReqMinEchoRxInterval;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessDetectMult
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessDetectMult
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessDetectMult (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMIStdBfdContextId,
                                   UINT4 u4FsMIStdBfdSessIndex,
                                   UINT4 u4TestValFsMIStdBfdSessDetectMult)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
        u4TestValFsMIStdBfdSessDetectMult;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessAuthPresFlag
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessAuthPresFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessAuthPresFlag (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIStdBfdContextId,
                                     UINT4 u4FsMIStdBfdSessIndex,
                                     INT4 i4TestValFsMIStdBfdSessAuthPresFlag)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag =
        i4TestValFsMIStdBfdSessAuthPresFlag;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessAuthenticationType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessAuthenticationType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessAuthenticationType (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMIStdBfdContextId,
                                           UINT4 u4FsMIStdBfdSessIndex,
                                           INT4
                                           i4TestValFsMIStdBfdSessAuthenticationType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationType =
        i4TestValFsMIStdBfdSessAuthenticationType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessAuthenticationKeyID
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessAuthenticationKeyID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessAuthenticationKeyID (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMIStdBfdContextId,
                                            UINT4 u4FsMIStdBfdSessIndex,
                                            INT4
                                            i4TestValFsMIStdBfdSessAuthenticationKeyID)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID =
        i4TestValFsMIStdBfdSessAuthenticationKeyID;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID =
        OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessAuthenticationKey
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessAuthenticationKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessAuthenticationKey (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMIStdBfdContextId,
                                          UINT4 u4FsMIStdBfdSessIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsMIStdBfdSessAuthenticationKey)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.
            au1FsMIStdBfdSessAuthenticationKey,
            pTestValFsMIStdBfdSessAuthenticationKey->pu1_OctetList,
            pTestValFsMIStdBfdSessAuthenticationKey->i4_Length);
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyLen =
        pTestValFsMIStdBfdSessAuthenticationKey->i4_Length;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessStorType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessStorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessStorType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 INT4 i4TestValFsMIStdBfdSessStorType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
        i4TestValFsMIStdBfdSessStorType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdBfdSessRowStatus
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIStdBfdSessRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdBfdSessRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  INT4 i4TestValFsMIStdBfdSessRowStatus)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
        i4TestValFsMIStdBfdSessRowStatus;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessRole
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessRole (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 i4TestValFsMIBfdSessRole)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole =
        i4TestValFsMIBfdSessRole;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessMode
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessMode (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                          UINT4 u4FsMIStdBfdSessIndex,
                          INT4 i4TestValFsMIBfdSessMode)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode =
        i4TestValFsMIBfdSessMode;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessRemoteDiscr
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessRemoteDiscr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessRemoteDiscr (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIStdBfdContextId,
                                 UINT4 u4FsMIStdBfdSessIndex,
                                 UINT4 u4TestValFsMIBfdSessRemoteDiscr)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
        u4TestValFsMIBfdSessRemoteDiscr;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessEXPValue
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessEXPValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessEXPValue (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                              UINT4 u4FsMIStdBfdSessIndex,
                              UINT4 u4TestValFsMIBfdSessEXPValue)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue =
        u4TestValFsMIBfdSessEXPValue;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessTmrNegotiate
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessTmrNegotiate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessTmrNegotiate (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIStdBfdContextId,
                                  UINT4 u4FsMIStdBfdSessIndex,
                                  INT4 i4TestValFsMIBfdSessTmrNegotiate)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate =
        i4TestValFsMIBfdSessTmrNegotiate;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessOffld
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessOffld
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessOffld (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                           UINT4 u4FsMIStdBfdSessIndex,
                           INT4 i4TestValFsMIBfdSessOffld)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld =
        i4TestValFsMIBfdSessOffld;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessEncapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessEncapType (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                               UINT4 u4FsMIStdBfdSessIndex,
                               INT4 i4TestValFsMIBfdSessEncapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType =
        i4TestValFsMIBfdSessEncapType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessMapType
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessMapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessMapType (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                             UINT4 u4FsMIStdBfdSessIndex,
                             INT4 i4TestValFsMIBfdSessMapType)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =
        i4TestValFsMIBfdSessMapType;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessMapPointer
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessMapPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessMapPointer (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                tSNMP_OID_TYPE * pTestValFsMIBfdSessMapPointer)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
            pTestValFsMIBfdSessMapPointer->pu4_OidList,
            pTestValFsMIBfdSessMapPointer->u4_Length * sizeof (UINT4));

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessCardNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessCardNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessCardNumber (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                UINT4 u4TestValFsMIBfdSessCardNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber =
        u4TestValFsMIBfdSessCardNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdSessSlotNumber
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdSessSlotNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdSessSlotNumber (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMIStdBfdContextId,
                                UINT4 u4FsMIStdBfdSessIndex,
                                UINT4 u4TestValFsMIBfdSessSlotNumber)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
        u4TestValFsMIBfdSessSlotNumber;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIBfdClearStats
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex

                The Object 
                testValFsMIBfdClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIBfdClearStats (UINT4 *pu4ErrorCode, UINT4 u4FsMIStdBfdContextId,
                            UINT4 u4FsMIStdBfdSessIndex,
                            INT4 i4TestValFsMIBfdClearStats)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pBfdIsSetFsMIStdBfdSessEntry =
        (tBfdIsSetFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    /* Assign the index */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
        u4FsMIStdBfdContextId;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_TRUE;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
        u4FsMIStdBfdSessIndex;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_TRUE;

    /* Assign the value */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats =
        i4TestValFsMIBfdClearStats;
    pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats = OSIX_TRUE;

    if (BfdTestAllFsMIStdBfdSessTable (pu4ErrorCode, pBfdFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_FALSE,
                                       OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdFsMIStdBfdSessEntry);
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdBfdGlobalConfigTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIStdBfdSessTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdBfdSessTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIBfdSystemConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIBfdSystemConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIBfdGblSessionConfigTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIBfdGblSessionConfigTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIBfdStatisticsTable
 Input       :  The Indices
                FsMIStdBfdContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIBfdStatisticsTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIBfdSessionTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIBfdSessionTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIBfdSessPerfTable
 Input       :  The Indices
                FsMIStdBfdContextId
                FsMIStdBfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIBfdSessPerfTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
