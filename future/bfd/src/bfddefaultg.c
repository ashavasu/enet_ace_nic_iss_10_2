/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddefaultg.c,v 1.5 2013/07/03 12:45:22 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Bfd 
*********************************************************************/

#include "bfdinc.h"

/****************************************************************************
* Function    : BfdInitializeMibFsMIStdBfdSessTable
* Input       : pBfdFsMIStdBfdSessEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
BfdInitializeMibFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                                     pBfdFsMIStdBfdSessEntry)
{

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber = 1;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDestinationUdpPort = 0;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort = 0;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessEchoSourceUdpPort = 0;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus = 2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDemandModeDesiredFlag =
        2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessControlPlaneIndepFlag =
        2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag = 2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM = 2;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL = 0;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag = 2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationType = -1;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthenticationKeyID = -1;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole = 1;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode = 1;

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue = 0;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate = 1;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld = 2;

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats = 2;

    BfdRedDbNodeInit (&pBfdFsMIStdBfdSessEntry->MibObject.SessDbNode);

#ifdef BFD_HA_TEST_WANTED
    pBfdFsMIStdBfdSessEntry->i4BfdTestSessPrevTmr = -1;
    pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr = -1;
#endif

    return OSIX_SUCCESS;
}
