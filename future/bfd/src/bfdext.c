/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdext.c,v 1.41 2017/06/08 11:40:29 siva Exp $
 *
 * Description: This file contains the routines for the protocol
 *              Database Access for the module BFD
 *********************************************************************/
#ifndef _BFDEXT_C_
#define _BFDEXT_C_

#include "bfdinc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtFillMplsPathId                                       *
 *                                                                           *
 * Description  : This function is used to fill the path id based on the     *
 *                path type                                                  *
 *                                                                           *
 * Input        : pPathParms  - Pointer to the path identifier               *
 *                                                                           *
 * Output       : pBfdInParams - Pointer to the path identifier filled       *
 *                               tBfdExtInParams structure                   *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
BfdExtFillMplsPathId (tBfdSessPathParams * pPathParms,
                      tBfdExtInParams * pBfdInParams)
{
    UINT4               u4PathType = 0;

    u4PathType = pPathParms->ePathType;
    pBfdInParams->InApiInfo.InPathId.u4PathType = u4PathType;
    if (u4PathType == BFD_PATH_TYPE_NONTE_IPV4)
    {
        pBfdInParams->InApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
        MEMCPY (&pBfdInParams->InApiInfo.InPathId.LspId.PeerAddr,
                &pPathParms->SessNonTeParams.PeerAddr, sizeof (tIpAddr));
        pBfdInParams->InApiInfo.InPathId.LspId.u4AddrType =
            (UINT4) pPathParms->SessNonTeParams.u1AddrType;
        pBfdInParams->InApiInfo.InPathId.LspId.u4AddrLen =
            (UINT4) pPathParms->SessNonTeParams.u1AddrLen;
    }
    else if (u4PathType == BFD_PATH_TYPE_TE_IPV4)
    {
        pBfdInParams->InApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
        pBfdInParams->InApiInfo.InPathId.TnlId.u4SrcTnlNum =
            pPathParms->SessTeParams.u4TunnelId;

        MEMCPY (&(pBfdInParams->
                  InApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId),
                &(pPathParms->SessTeParams.SrcIpAddr), sizeof (tIpAddr));

        pBfdInParams->InApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
            pPathParms->SessTeParams.u4AddrType;

        MEMCPY (&(pBfdInParams->
                  InApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId),
                &(pPathParms->SessTeParams.DstIpAddr), sizeof (tIpAddr));

        pBfdInParams->InApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
            pPathParms->SessTeParams.u4AddrType;

        pBfdInParams->InApiInfo.InPathId.TnlId.u4LspNum =
            pPathParms->SessTeParams.u4TunnelInst;
    }
    else if (u4PathType == BFD_PATH_TYPE_PW)
    {
        pBfdInParams->InApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
        pBfdInParams->InApiInfo.InPathId.PwId.u4VcId =
            pPathParms->SessPwParams.u4VcId;

        MEMCPY (&(pBfdInParams->
                  InApiInfo.InPathId.PwId.SrcNodeId.MplsRouterId),
                &(pPathParms->SessPwParams.PeerAddr), sizeof (tIpAddr));
    }
    else if (u4PathType == BFD_PATH_TYPE_MEP)
    {
        pBfdInParams->InApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
        pBfdInParams->InApiInfo.InPathId.MegId.u4MegIndex =
            pPathParms->SessMeParams.u4MegId;
        pBfdInParams->InApiInfo.InPathId.MegId.u4MeIndex =
            pPathParms->SessMeParams.u4MeId;
        pBfdInParams->InApiInfo.InPathId.MegId.u4MpIndex =
            pPathParms->SessMeParams.u4MpId;
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtUpdateSessParams                                     *
 *                                                                           *
 * Description  : This function is used to call external OAM function to     *
 *                update session index based on the path id                  *
 *                                                                           *
 * Input        : pBfdSessEntry - Session Entry structure                    *
 *                eBfdSessIndex - Invalid or valid session index             *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtUpdateSessParams (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdSessIndex eBfdSessIndex)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    if (pBfdSessEntry == NULL)
    {
        return OSIX_SUCCESS;
    }
    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdExtUpdateSessParams");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdExtUpdateSessParams");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* Fill output parameter for LSPP - discriminator */
    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_OAM_UPDATE_SESSION_PARAMS;
    pBfdExtInParams->InApiInfo.u4ContextId =
        pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
    if (eBfdSessIndex == BFD_INVALID_SESS_IDX)
    {
        pBfdExtInParams->InApiInfo.InOamSessionParams.u4ProactiveSessIndex =
            BFD_INVALID_SESS_IDX;
    }
    else
    {
        pBfdExtInParams->InApiInfo.InOamSessionParams.u4ProactiveSessIndex =
            pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
    }
    BfdExtFillMplsPathId (&pBfdSessEntry->BfdSessPathParams, pBfdExtInParams);
    /* Call BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtSendPktToMpls                                        *
 *                                                                           *
 * Description  : This function is used to call external OAM function to     *
 *                give the packet to MPLS-RTR to forward                     *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4IfIndex   - Interface Index                              *
 *                pBuf        - Pointer to the packet CRU Buffer             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtSendPktToMpls (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 *pu1DestMac,
                     tCRU_BUF_CHAIN_HEADER * pBuf)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UNUSED_PARAM (i4RetVal);

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtSendPktToMpls");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtSendPktToMpls");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* Fill output parameter for LSPP - discriminator */
    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_PACKET_HANDLE_FROM_APP;
    pBfdExtInParams->InApiInfo.u4ContextId = u4ContextId;
    pBfdExtInParams->InApiInfo.InPktInfo.u4OutIfIndex = u4IfIndex;
    pBfdExtInParams->InApiInfo.InPktInfo.bIsUnNumberedIf = FALSE;
    pBfdExtInParams->InApiInfo.InPktInfo.bIsMplsLabelledPacket = TRUE;
    MEMCPY (pBfdExtInParams->InApiInfo.InPktInfo.au1DstMac,
            pu1DestMac, MAC_ADDR_LEN);
    pBfdExtInParams->InApiInfo.InPktInfo.pBuf = pBuf;
    /* Call BFD exit function */
    i4RetVal = BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams);
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtChkSessPathEgrs                                      *
 *                                                                           *
 * Description  : This function is used to check whether path is egress      *
 *                for the Session                                            *
 *                                                                           *
 * Input        : pBfdSessEntry - Pointer to BFD session information         *
 *                                                                           *
 * Output       : pbCheck - If egress OSIX_TRUE otherwise OSIX_FALSE         *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtChkSessPathEgrs (tBfdFsMIStdBfdSessEntry * pBfdSessEntry, BOOL1 * pbCheck)
{
    tBfdSessPathParams *pBfdSessPathParams = NULL;
    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;

    *pbCheck = OSIX_FALSE;

    pBfdSessPathParams = &pBfdSessEntry->BfdSessPathParams;
    if (pBfdSessPathParams->ePathType == BFD_PATH_TYPE_PW)
    {
        return OSIX_SUCCESS;
    }

    /* Allocate memory for MPLS Path info */
    pBfdMplsPathInfo = (tBfdMplsPathInfo *)
        MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
    if (pBfdMplsPathInfo == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdExtChkSessPathEgrs");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

    if (BfdExtGetMplsPathInfo (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                               pBfdSessPathParams, pBfdMplsPathInfo,
                               OSIX_FALSE) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_FAILURE;
    }

    if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
        BFD_CHECK_PATH_PW_AVAIL)
    {
        *pbCheck = OSIX_FALSE;
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_NONTE_AVAIL) ==
        BFD_CHECK_PATH_NONTE_AVAIL)
    {
        *pbCheck = OSIX_FALSE;
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) ==
        BFD_CHECK_PATH_TE_AVAIL)
    {
        if ((pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlRole == MPLS_TE_EGRESS)
            && (pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlMode !=
                TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
        {
            *pbCheck = OSIX_TRUE;
        }
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                        (UINT1 *) pBfdMplsPathInfo);
    return OSIX_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtChkSessOperRole                                      *
 *                                                                           *
 * Description  : This function is used to check whether the node is a SINK  *
 *                or SOURCE for indepedent monitoring ME                     *
 *                                                                           *
 * Input        : pBfdSessEntry - Pointer to BFD session information         *
 *                u4BfdChkSessRole - BFD session role                        *
 *                                                                           *
 * Output       : pbCheck - If Tunnel role is Head and session role is Sink  *
 *                          return OSIX_TRUE                                 * 
 *                          otherwise OSIX_FALSE                             *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtChkSessOperRole (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                       UINT4 u4BfdChkSessRole, BOOL1 * pbCheck)
{
    tBfdSessPathParams *pBfdSessPathParams = NULL;
    tBfdMplsPathInfo   *pBfdMplsPathInfo = NULL;

    /* Allocate memory for MPLS Path info */
    pBfdMplsPathInfo = (tBfdMplsPathInfo *)
        MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
    if (pBfdMplsPathInfo == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

    pBfdSessPathParams = &pBfdSessEntry->BfdSessPathParams;
    if (pBfdSessPathParams->ePathType != BFD_PATH_TYPE_MEP)
    {
        *pbCheck = OSIX_FALSE;
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    if (BfdExtGetMplsPathInfo (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                               pBfdSessPathParams, pBfdMplsPathInfo,
                               OSIX_FALSE) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_FAILURE;
    }

    /*Check if the ME underlying path is PW. If so, this is not 
     * independent monitoring */
    if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_PW_AVAIL) ==
        BFD_CHECK_PATH_PW_AVAIL)
    {
        *pbCheck = OSIX_FALSE;
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    if ((pBfdMplsPathInfo->u2PathFilled & BFD_CHECK_PATH_TE_AVAIL) !=
        BFD_CHECK_PATH_TE_AVAIL)
    {
        *pbCheck = OSIX_FALSE;
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pBfdMplsPathInfo);
        return OSIX_SUCCESS;
    }

    if (pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlMode ==
        TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        *pbCheck = OSIX_FALSE;
    }
    else if ((pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlRole ==
              MPLS_TE_INGRESS) &&
             (u4BfdChkSessRole == BFD_SESS_OPER_ROLE_INDP_SOURCE))
    {
        *pbCheck = OSIX_TRUE;
    }
    else if ((pBfdMplsPathInfo->MplsTeTnlApiInfo.u1TnlRole ==
              MPLS_TE_EGRESS)
             && (u4BfdChkSessRole == BFD_SESS_OPER_ROLE_INDP_SINK))
    {
        *pbCheck = OSIX_TRUE;
    }
    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                        (UINT1 *) pBfdMplsPathInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtInformSessionState                                   *
 *                                                                           *
 * Description  : This function is used to inform OAM about BFD              *
 *                session status                                             *
 *                                                                           *
 * Input        : pBfdSessEntry - Pointer to BFD session information         *
 *                u4BfdSessState - Session state                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtInformSessionState (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          UINT4 u4BfdSessState)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT1               au1Event[1];
    tBfdClientId        u1BfdClientId = BFD_CLIENT_ID_IP;
    BOOL1               bIsRegistered = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (au1BfdClients, 0, sizeof (au1BfdClients));
    MEMSET (au1Event, 0, sizeof (au1Event));

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdExtInformSessionState");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdExtInformSessionState");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    if (BFD_CHECK_PATH_TYPE_NOT_IP (pBfdSessEntry))
    {

        pBfdExtInParams->eExtReqType =
            BFD_REQ_MPLS_OAM_HANDLE_PATH_STATUS_CHANGE;
        pBfdExtInParams->InApiInfo.u4ContextId =
            pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
        pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;
        if (u4BfdSessState == BFD_SESS_STATE_UP)
        {
            pBfdExtInParams->InApiInfo.InOamPathStatus.u1PathStatus =
                MPLS_PATH_STATUS_UP;
        }
        else
        {
            pBfdExtInParams->InApiInfo.InOamPathStatus.u1PathStatus =
                MPLS_PATH_STATUS_DOWN;
        }

        BfdExtFillMplsPathId (&pBfdSessEntry->BfdSessPathParams,
                              pBfdExtInParams);
    }
    /* For IP path status change notification */
    else
    {
        pBfdExtInParams->eExtReqType =
            BFD_REQ_CLIENT_HANDLE_IP_PATH_STATUS_CHANGE;
        pBfdExtInParams->u4ContextId =
            pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        /* Fill the informations needed by the BFD client */
        MEMCPY (&pBfdExtInParams->NbrPathInfo.SrcAddr,
                pBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen);
        MEMCPY (&pBfdExtInParams->NbrPathInfo.NbrAddr,
                pBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen);

        pBfdExtInParams->NbrPathInfo.u4IfIndex =
            (UINT4) pBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;
        pBfdExtInParams->NbrPathInfo.u4PathStatus = u4BfdSessState;

        if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen ==
            BFD_IPV4_MAX_ADDR_LEN)
        {
            pBfdExtInParams->NbrPathInfo.u1AddrType = BFD_INET_ADDR_IPV4;
        }
#ifdef IP6_WANTED
        else if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen ==
                 BFD_IPV6_MAX_ADDR_LEN)
        {
            pBfdExtInParams->NbrPathInfo.u1AddrType = BFD_INET_ADDR_IPV6;
        }
#endif

        if (pBfdSessEntry->MibObject.
            i4FsMIStdBfdSessControlPlaneIndepFlag == BFD_SNMP_TRUE)
        {
            pBfdExtInParams->NbrPathInfo.bIsSessCtrlPlaneInDep = OSIX_TRUE;
        }
        else
        {
            pBfdExtInParams->NbrPathInfo.bIsSessCtrlPlaneInDep = OSIX_FALSE;
        }

        /* Identify the clients registered for the particular IP path monitoring
         * and inform the Path status
         */
        MEMCPY (au1BfdClients,
                &pBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients,
                sizeof (au1BfdClients));

        do
        {
            /* Check whether the client is registered */
            OSIX_BITLIST_IS_BIT_SET (au1BfdClients, u1BfdClientId,
                                     sizeof (au1BfdClients), bIsRegistered);

            if (bIsRegistered == OSIX_TRUE)
            {
                /* If any client is registered for the session, then check whether the client
                 * wants to get notified for the received path status change
                 */
                MEMCPY (au1Event, &pBfdSessEntry->
                        aBfdClientParams[u1BfdClientId - 1].u1EventMask, 1);
                /*Checking whether the client wants to receive this path status
                 * change notification.
                 *
                 * The OSIX_BITLIST utilities, shoule not be called with second argument "0".
                 * Since session state starts from the value "0", while registering and
                 * checking the the session state will be incremented by "1" to set/check
                 * the corrsponding bit.
                 */
                OSIX_BITLIST_IS_BIT_SET (au1Event, (u4BfdSessState + 1), 1,
                                         bResult);

                if (bResult == OSIX_TRUE)
                {
                    /* If the registered client wants to receive this path status change
                     * notification, then copy the corresponding function pointer
                     */
                    if (u4BfdSessState == BFD_SESS_STATE_DOWN)
                    {
                        /* If the client is registered only for PATH DOWN notification
                         * then, path down due to detection timer expiry alone should be notified
                         * Path down because of remote admin down, neighbor signalled session down
                         * (it may be because of remote admin down) should not be notified
                         */
                        if (BFD_SESS_DIAG (pBfdSessEntry) ==
                            BFD_DIAG_CTRL_DETECTION_TIME_EXP)
                        {
                            pBfdExtInParams->
                                NotifyPathStatusChange[u1BfdClientId - 1] =
                                pBfdSessEntry->
                                aBfdClientParams[u1BfdClientId - 1].
                                pBfdNotifyPathStatusChange;
                            if ((u1BfdClientId == BFD_CLIENT_ID_OSPF) ||
                                (u1BfdClientId == BFD_CLIENT_ID_BGP) ||
                                (u1BfdClientId == BFD_CLIENT_ID_OSPF3) ||
                                (u1BfdClientId == BFD_CLIENT_ID_ISIS))
                            {
                                BfdUtilIpHandlePathStatusChange
                                    (pBfdExtInParams->u4ContextId,
                                     &pBfdExtInParams->NbrPathInfo);

                            }

                        }
                    }
                    else
                    {
                        pBfdExtInParams->
                            NotifyPathStatusChange[u1BfdClientId - 1] =
                            pBfdSessEntry->aBfdClientParams[u1BfdClientId - 1].
                            pBfdNotifyPathStatusChange;
                    }
                }
            }
            u1BfdClientId++;
        }
        while (u1BfdClientId < BFD_MAX_CLIENTS);
    }

    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtArpResolve                                           *
 *                                                                           *
 * Description  : This function is used to call the BFD exit function to     *
 *                get Mac Addr                                               *
 *                                                                           *
 * Input        : u4NhIp - Nex-hop Address                                   *
 *                                                                           *
 * Output       : pu1EncapType - Pointer to the encapsulation type           *
 *                pau1MacAddr - Pointer to MAC Address                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtArpResolve (UINT4 u4NhIp, UINT1 *pau1MacAddr, UINT1 *pu1EncapType)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request HW address for destination IP */
    pBfdExtInParams->eExtReqType = BFD_REQ_ARP_RESOLVE;

    /* Fill in parameters */
    pBfdExtInParams->IpAddress = u4NhIp;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pau1MacAddr, pBfdExtOutParams->ArpOutParams.ai1HwAddr,
            sizeof (pBfdExtOutParams->ArpOutParams.ai1HwAddr));

    *pu1EncapType = pBfdExtOutParams->ArpOutParams.u1EncapType;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetPortFromIfIndex                                   *
 *                                                                           *
 * Description  : This function is used to get port from interface index     *
 *                                                                           *
 * Input        : u4IfIndex - interface index                                *
 *                                                                           *
 * Output       : pu4Port - Pointer to the port number                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request L3VLAN IF */
    pBfdExtInParams->eExtReqType = BFD_REQ_IP_PORT_FROM_IF_INDEX;
    pBfdExtInParams->u4IfIdx = u4IfIndex;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4Port = pBfdExtOutParams->u4Port;
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtNetIpv4GetIfInfo                                     *
 *                                                                           *
 * Description  : This function is used to get interface related information *
 *                from interface index                                       *
 *                                                                           *
 * Input        : u4IpPort - Port number                                     *
 *                                                                           *
 * Output       : pNetIpIfInfo - Interface Info for the given u4IpPort       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtNetIpv4GetIfInfo (UINT4 u4IpPort, tNetIpv4IfInfo * pNetIpIfInfo)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request L3VLAN IF */
    pBfdExtInParams->eExtReqType = BFD_REQ_IF_INFO_FROM_IF_INDEX;
    pBfdExtInParams->u4IfIdx = u4IpPort;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }
    MEMCPY (pNetIpIfInfo, &pBfdExtOutParams->NetIpIfInfo,
            sizeof (tNetIpv4IfInfo));
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtEnqueueArp                                           *
 *                                                                           *
 * Description  : This function is used to give ARP request to ARP module    *
 *                                                                           *
 * Input        : u4NhIp - Next-hop Address                                  *
 *                u4Port - Port number                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtEnqueueArp (UINT4 u4NhIp, UINT4 u4Port)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request L3VLAN IF */
    pBfdExtInParams->eExtReqType = BFD_REQ_ENQUEUE_ARP_REQUEST;
    pBfdExtInParams->RecvdQMsg.u4MsgType = ARP_APP_IF_MSG;
    pBfdExtInParams->RecvdQMsg.u2Port = (UINT2) u4Port;
    pBfdExtInParams->RecvdQMsg.u4IpAddr = u4NhIp;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetIfVlan                                            *
 *                                                                           *
 * Description  : This function is used to get vlan interface from interface *
 *                index                                                      *
 *                                                                           *
 * Input        : u4IfIndex - Interface index                                *
 *                                                                           *
 * Output       : pu2Vlan   - Pointer to vlan interface                      *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetIfVlan (UINT4 u4IfIndex, UINT2 *pu2Vlan)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request L3VLAN IF */
    pBfdExtInParams->eExtReqType = BFD_REQ_VLAN_FROM_IF;
    pBfdExtInParams->u4IfIdx = u4IfIndex;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu2Vlan = pBfdExtOutParams->u2Vlan;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtFmNotifyFaults                                       *
 *                                                                           *
 * Description  : This function is used to raise trap on BFD protocol fault  *
 *                                                                           *
 * Input        : pEnterpriseOid - Pointer to enterprise OID                 *
 *                u4GenTrapType  - General Trap type                         *
 *                u4SpecTrapType - Specific Trap type                        *
 *                pStartVb       - Pointer to var bind                       *
 *                pCxtName       - Pointer to context name                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid,
                      UINT4 u4GenTrapType, UINT4 u4SpecTrapType,
                      tSNMP_VAR_BIND * pStartVb,
                      tSNMP_OCTET_STRING_TYPE * pCxtName)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_FM_SEND_TRAP;
    pBfdExtInParams->unReqParams.FmFaultMsg.pTrapMsg = pStartVb;
    pBfdExtInParams->unReqParams.FmFaultMsg.pSyslogMsg = NULL;
    pBfdExtInParams->unReqParams.FmFaultMsg.pEnterpriseOid = pEnterpriseOid;

    pBfdExtInParams->unReqParams.FmFaultMsg.pCxtName = pCxtName;
    pBfdExtInParams->unReqParams.FmFaultMsg.u4GenTrapType = u4GenTrapType;
    pBfdExtInParams->unReqParams.FmFaultMsg.u4SpecTrapType = u4SpecTrapType;
    pBfdExtInParams->unReqParams.FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_BFD;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetIfMacAddr                                         *
 *                                                                           *
 * Description  : This function is used to get MAC address of an given       *
 *                interface index                                            *
 *                                                                           *
 * Input        : u4EggrItf - Interface index                                *
 *                                                                           *
 * Output       : pau1SrcMacAddr   - Pointer to MAC address                  *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetIfMacAddr (UINT4 u4ContextId, UINT4 u4EggrItf, UINT1 *pau1SrcMacAddr)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetIfMacAddr");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetIfMacAddr");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request HW address of the interface */
    pBfdExtInParams->eExtReqType = BFD_REQ_GET_IF_MAC_ADDR;

    /* Fill in parameters */
    pBfdExtInParams->u4IfIdx = u4EggrItf;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pau1SrcMacAddr, pBfdExtOutParams->au1HwAddr,
            sizeof (pBfdExtOutParams->au1HwAddr));

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetMplsPathInfo                                      *
 *                                                                           *
 * Description  : Function is used to get the complete path information      *
 *                from the MPLS Module based on path type                    *
 *                                                                           *
 * Input        : pBfdSessPathParams - pointer to path identifier structure  *
 *                bGetRevPath - Indicates, only FWD path is needed.          *
 *                              i.e whenever the Node is the egress          *
 *                              of a tunnel or LSP, it will try to get the   *
 *                              reverse path for this path                   *
 *                                                                           *
 * Output       : pBfdMplsPathInfo - pointer to the MPLS Path structure.     *
 *                                   based on the path, this structure       *
 *                                   is filled with path info                *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetMplsPathInfo (UINT4 u4ContextId,
                       tBfdSessPathParams * pBfdSessPathParams,
                       tBfdMplsPathInfo * pBfdMplsPathInfo, BOOL1 bGetRevPath)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    BOOL1               bGetMplsTnlInfoFromMe = OSIX_FALSE;
    BOOL1               bGetMplsTnlInfoFromPw = OSIX_FALSE;
    BOOL1               bGetMplsLspInfoFromPw = OSIX_FALSE;
    BOOL1               bGetMplsPwInfoFromMe = OSIX_FALSE;
    UINT4               u4TmpTnlIns = 0;

    if ((pBfdSessPathParams == NULL) || (pBfdMplsPathInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetMplsPathInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetMplsPathInfo");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* Point to Mpls in info */
    pMplsApiInInfo = &pBfdExtInParams->unReqParams.InMplsApiInfo;

    /* Point to Mpls out info */
    pMplsApiOutInfo = &pBfdExtOutParams->MplsApiOutInfo;

    /* Check the path type is ME */
    if (pBfdSessPathParams->ePathType == BFD_PATH_TYPE_MEP)
    {
        /* get the ME info from MPLS Module */
        pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_OAM_GET_MEG_INFO;

        /* Fill the MPLS In info */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = BFD_MODULE;
        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
        pMplsApiInInfo->InPathId.MegId.u4MegIndex =
            pBfdSessPathParams->SessMeParams.u4MegId;
        pMplsApiInInfo->InPathId.MegId.u4MeIndex =
            pBfdSessPathParams->SessMeParams.u4MeId;
        pMplsApiInInfo->InPathId.MegId.u4MpIndex =
            pBfdSessPathParams->SessMeParams.u4MpId;

        /* call the BFD exit function to fetch ME Info */
        if (BfdPortHandleExtInteraction
            (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return OSIX_FAILURE;
        }
        /* Check the Meg State */
        if ((pMplsApiOutInfo->OutMegInfo.u4MeOperStatus &
             OAM_ME_OPER_STATUS_MEG_DOWN) ||
            (pMplsApiOutInfo->OutMegInfo.u4MeOperStatus &
             OAM_ME_OPER_STATUS_ME_DOWN) ||
            (pMplsApiOutInfo->OutMegInfo.u4MeOperStatus &
             OAM_ME_OPER_STATUS_PATH_DOWN))
        {
            pBfdMplsPathInfo->u1PathStatus = BFD_MPLS_PATH_DOWN;
        }
        /* check the service type to get the further path info */
        if (pMplsApiOutInfo->OutMegInfo.u1ServiceType == OAM_SERVICE_TYPE_LSP)
        {
            bGetMplsTnlInfoFromMe = OSIX_TRUE;
        }
        if (pMplsApiOutInfo->OutMegInfo.u1ServiceType == OAM_SERVICE_TYPE_PW)
        {
            bGetMplsPwInfoFromMe = OSIX_TRUE;
        }

        /* copy the meg mep information */
        MEMCPY (&(pBfdMplsPathInfo->MplsMegApiInfo),
                &(pMplsApiOutInfo->OutMegInfo), sizeof (tMplsMegApiInfo));

        pBfdMplsPathInfo->u2PathFilled |= BFD_CHECK_PATH_ME_AVAIL;

        /* clear structures to use again */
        MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));
        MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));
    }

    /* Check the path type is PW. Get the PW details if the session
     * path is PW or fetch the PW details if the ME is for a PW*/
    if ((pBfdSessPathParams->ePathType == BFD_PATH_TYPE_PW) ||
        (bGetMplsPwInfoFromMe == OSIX_TRUE))
    {
        /* get the ME info from MPLS Module */
        pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_PW_INFO;

        /* Fill the MPLS In info */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = BFD_MODULE;

        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_PW;

        /*Fill the path id with PW ID */
        if (pBfdSessPathParams->ePathType == BFD_PATH_TYPE_PW)
        {
            pMplsApiInInfo->InPathId.PwId.u4VcId =
                pBfdSessPathParams->SessPwParams.u4VcId;

            MEMCPY (&(pMplsApiInInfo->InPathId.PwId.SrcNodeId.MplsRouterId),
                    &(pBfdSessPathParams->SessPwParams.PeerAddr),
                    sizeof (tIpAddr));
        }

        /* Need to fill path id from the ME Info */
        if (bGetMplsPwInfoFromMe == OSIX_TRUE)
        {
            BfdExtGetPwPathIdFromIdx (pBfdMplsPathInfo->MplsMegApiInfo.MegPwId,
                                      &(pMplsApiInInfo->InPathId.PwId));
        }

        /* call the BFD exit function to fetch PW information */
        if (BfdPortHandleExtInteraction
            (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return OSIX_FAILURE;
        }

        /* Check the Pw State 
         * If Pw is associated with MEG, MEG status is enough*/
        if (bGetMplsPwInfoFromMe != OSIX_TRUE)
        {
            if (pMplsApiOutInfo->OutPwInfo.i1OperStatus != MPLS_OPER_UP)
            {
                pBfdMplsPathInfo->u1PathStatus = BFD_MPLS_PATH_DOWN;
            }
        }
        /* copy the pw information */
        MEMCPY (&(pBfdMplsPathInfo->MplsPwApiInfo),
                &(pMplsApiOutInfo->OutPwInfo), sizeof (tPwApiInfo));

        pBfdMplsPathInfo->u2PathFilled |= BFD_CHECK_PATH_PW_AVAIL;

        if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_TE)
        {
            bGetMplsTnlInfoFromPw = OSIX_TRUE;
        }
        if (pBfdMplsPathInfo->MplsPwApiInfo.u1MplsType == L2VPN_MPLS_TYPE_NONTE)
        {
            bGetMplsLspInfoFromPw = OSIX_TRUE;
        }

        /* clear structures to use again */
        MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));
        MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));
    }

    /* Check the path type is Tunnel. Get the Tunnel details if the session
     * path is Tunnel or fetch the tunnel details if the ME is for a tunnel 
     * or fetch tunnel id details from PW if tunnel is underlying path for 
     * PW*/
    if ((pBfdSessPathParams->ePathType == BFD_PATH_TYPE_TE_IPV4) ||
        (bGetMplsTnlInfoFromMe == OSIX_TRUE) ||
        (bGetMplsTnlInfoFromPw == OSIX_TRUE))
    {

        /* get the Tunnel info from MPLS Module */
        pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_TUNNEL_INFO;

        /* Fill the MPLS In info */
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = BFD_MODULE;

        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

        /*Fill the path id from tunnel id */
        if (pBfdSessPathParams->ePathType == BFD_PATH_TYPE_TE_IPV4)
        {

            pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                pBfdSessPathParams->SessTeParams.u4TunnelId;

            MEMCPY (&(pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId),
                    &(pBfdSessPathParams->SessTeParams.SrcIpAddr),
                    sizeof (tIpAddr));

            pMplsApiInInfo->InPathId.TnlId.SrcNodeId.u4NodeType =
                pBfdSessPathParams->SessTeParams.u4AddrType;

            MEMCPY (&(pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId),
                    &(pBfdSessPathParams->SessTeParams.DstIpAddr),
                    sizeof (tIpAddr));

            pMplsApiInInfo->InPathId.TnlId.DstNodeId.u4NodeType =
                pBfdSessPathParams->SessTeParams.u4AddrType;

            pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                pBfdSessPathParams->SessTeParams.u4TunnelInst;
        }

        /*Fill the path id with info from ME */
        if (bGetMplsTnlInfoFromMe == OSIX_TRUE)
        {
            MEMCPY (&(pMplsApiInInfo->InPathId.TnlId),
                    &(pBfdMplsPathInfo->MplsMegApiInfo.MegMplsTnlId),
                    sizeof (tMplsTnlLspId));
            pMplsApiInInfo->InPathId.TnlId.
                SrcNodeId.MplsRouterId.u4_addr[0] =
                pBfdMplsPathInfo->MplsMegApiInfo.MegMplsTnlId.u4SrcLocalMapNum;

            pMplsApiInInfo->InPathId.TnlId.
                DstNodeId.MplsRouterId.u4_addr[0] =
                pBfdMplsPathInfo->MplsMegApiInfo.MegMplsTnlId.u4DstLocalMapNum;

        }

        /*Fill the path id with info from PW */
        if (bGetMplsTnlInfoFromPw == OSIX_TRUE)
        {
            MEMCPY (&(pMplsApiInInfo->InPathId.TnlId),
                    &(pBfdMplsPathInfo->MplsPwApiInfo.TeTnlId),
                    sizeof (tMplsTnlLspId));

        }

        /* call the BFD exit function to fetch Tunnel information */
        if (BfdPortHandleExtInteraction
            (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return OSIX_FAILURE;
        }

        /* Check the Tunnel State */
        if ((bGetMplsTnlInfoFromMe != OSIX_TRUE) &&
            (bGetMplsTnlInfoFromPw != OSIX_TRUE))
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1OperStatus != MPLS_OPER_UP)
            {
                pBfdMplsPathInfo->u1PathStatus = BFD_MPLS_PATH_DOWN;
            }
        }
        /* Check from the path info if it is egress, if so fetch the reverse
         * path*/
        if ((bGetRevPath == OSIX_TRUE) &&
            (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS))
        {
            /* In Code, Only Fwd info is used. hence in co-routed case
             * Copying from rev to fwd*/
            if ((pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                 TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
            {
                MEMCPY (&pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo,
                        &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo,
                        sizeof (tMplsInSegInfo));

                MEMCPY (&pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo,
                        &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo,
                        sizeof (tMplsOutSegInfo));
            }
            else
            {
                /* Clear the path status in case it was set for the 
                 * forward path */
                pBfdMplsPathInfo->u1PathStatus = BFD_MPLS_PATH_UP;

                /* Fill the MPLS In info */
                pMplsApiInInfo->u4ContextId = u4ContextId;
                pMplsApiInInfo->u4SrcModId = BFD_MODULE;
                /* clear structures to use again */
                MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));
                if (pBfdSessPathParams->ePathType == BFD_PATH_TYPE_MEP)
                {
                    /* Non - Co-routed MPLS TP path */

                    /* get the Tunnel info from MPLS Module */
                    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_TUNNEL_INFO;

                    /* Path params are already filled above */
                    pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
                    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                        pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstTnlNum;
                    pMplsApiInInfo->InPathId.TnlId.u4LspNum
                        = pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4DstLspNum;
                    MEMCPY (&pMplsApiInInfo->InPathId.TnlId.SrcNodeId,
                            &pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.DstNodeId,
                            sizeof (pMplsApiInInfo->InPathId.TnlId.SrcNodeId));

                }
                else
                {
                    pBfdExtInParams->InApiInfo.InNodeId.u4NodeType =
                        MPLS_ADDR_TYPE_IPV4;
                    MEMCPY (&(pBfdExtInParams->
                              InApiInfo.InNodeId.MplsRouterId),
                            &(pBfdSessPathParams->SessTeParams.SrcIpAddr),
                            sizeof (tIpAddr));
                    pBfdExtInParams->eExtReqType =
                        BFD_REQ_MPLS_GET_REV_PATH_INFO_FROM_FWD_PATH_INFO;
                }
                u4TmpTnlIns = pMplsApiOutInfo->OutTeTnlInfo.TnlLspId.u4LspNum;
                MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));
                /* call the BFD exit function to fetch reverse path
                 * information */
                if (BfdPortHandleExtInteraction
                    (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
                {
                    if ((pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                         TE_TNL_MODE_UNIDIRECTIONAL))
                    {
                        pBfdSessPathParams->SessTeParams.u4TunnelInst
                            = u4TmpTnlIns;
                    }
                    /* Set reverse path fetch failed, indicating actual path
                     * is present but the fetch for reverse path has failed */
                    pBfdMplsPathInfo->bRevPathFetchFail = OSIX_TRUE;

                    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                        (UINT1 *) pBfdExtInParams);
                    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                        (UINT1 *) pBfdExtOutParams);
                    return OSIX_FAILURE;
                }

                if (pBfdExtOutParams->MplsApiOutInfo.u4PathType ==
                    MPLS_PATH_TYPE_NONTE_LSP)
                {
                    /* Return Path fetch should give us a valid path with Oper
                     * status. If the Path is not UP, return Reverse path fetch
                     * fail.
                     * If a IP path can be used, it will be used by the calling
                     * function */
                    if (pMplsApiOutInfo->
                        OutNonTeTnlInfo.XcApiInfo.u1OperStatus != MPLS_OPER_UP)
                    {
                        pBfdMplsPathInfo->bRevPathFetchFail = OSIX_TRUE;
                        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                            (UINT1 *) pBfdExtInParams);
                        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                            (UINT1 *) pBfdExtOutParams);
                        return OSIX_FAILURE;
                    }

                    /* copy the LSP information */
                    MEMCPY (&(pBfdMplsPathInfo->MplsNonTeApiInfo),
                            &(pMplsApiOutInfo->OutNonTeTnlInfo),
                            sizeof (tNonTeApiInfo));

                    pBfdMplsPathInfo->u2PathFilled |=
                        BFD_CHECK_PATH_NONTE_AVAIL;
                    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                        (UINT1 *) pBfdExtInParams);
                    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                        (UINT1 *) pBfdExtOutParams);
                    return OSIX_SUCCESS;
                }
                else
                {
                    /* Return Path fetch should give us a valid path with Oper
                     * status. If the Path is not UP, return Reverse path fetch
                     * fail.
                     * If a IP path can be used, it will be used by the calling
                     * function */
                    if (pMplsApiOutInfo->OutTeTnlInfo.u1OperStatus !=
                        MPLS_OPER_UP)
                    {
                        pBfdMplsPathInfo->bRevPathFetchFail = OSIX_TRUE;

                        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                            (UINT1 *) pBfdExtInParams);
                        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                            (UINT1 *) pBfdExtOutParams);
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        /* copy the tunnel information */
        MEMCPY (&(pBfdMplsPathInfo->MplsTeTnlApiInfo),
                &(pMplsApiOutInfo->OutTeTnlInfo), sizeof (tTeTnlApiInfo));

        pBfdMplsPathInfo->u2PathFilled |= BFD_CHECK_PATH_TE_AVAIL;
    }

    /* Check the underlying path for PW is LSP */
    if (bGetMplsLspInfoFromPw == OSIX_TRUE)
    {
        /* get the ME info from MPLS Module */
        pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_LSP_INFO;

        /* Fill the MPLS In info */
        pMplsApiInInfo->u4SubReqType = MPLS_GET_LSP_INFO_FROM_XC_INDEX;
        pMplsApiInInfo->u4ContextId = u4ContextId;
        pMplsApiInInfo->u4SrcModId = BFD_MODULE;

        pMplsApiInInfo->InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;

        pMplsApiInInfo->InPathId.LspId.u4XCIndex =
            pBfdMplsPathInfo->MplsPwApiInfo.NonTeTnlId;

        /* call the BFD exit function to fetch PW information */
        if (BfdPortHandleExtInteraction
            (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdExtOutParams);
            return OSIX_FAILURE;
        }

        /* copy the LSP information */
        MEMCPY (&(pBfdMplsPathInfo->MplsNonTeApiInfo),
                &(pMplsApiOutInfo->OutNonTeTnlInfo), sizeof (tNonTeApiInfo));

        pBfdMplsPathInfo->u2PathFilled |= BFD_CHECK_PATH_NONTE_AVAIL;
    }
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetPwPathIdFromIdx                                   *
 *                                                                           *
 * Description  : This function is used to get Pseudowire path id from       *
 *                Pseudowire index                                           *
 *                                                                           *
 * Input        : u4PwIndex - Pseudowire index                               *
 *                                                                           *
 * Output       : pu4RouterId - Pointer to Pseudowire path id                *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetPwPathIdFromIdx (UINT4 u4PwIndex, tPwPathId * pPwPathId)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_PW_INFO;
    pBfdExtInParams->InApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;
    pBfdExtInParams->InApiInfo.InPathId.PwId.u4PwIndex = u4PwIndex;

    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pPwPathId, &pBfdExtOutParams->MplsApiOutInfo.OutPwInfo.MplsPwPathId,
            sizeof (tPwPathId));

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetRouterId                                          *
 *                                                                           *
 * Description  : This function is used to get router ID from context info   *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : pu4RouterId - Pointer to router ID                         *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetRouterId (UINT4 u4ContextId, UINT4 *pu4RouterId)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetRouterId");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetRouterId");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_MPLS_GET_NODE_ID;
    pBfdExtInParams->InApiInfo.u4SubReqType = MPLS_GET_ROUTER_ID;
    pBfdExtInParams->InApiInfo.u4SrcModId = BFD_MODULE;

    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4RouterId = pBfdExtOutParams->MplsApiOutInfo.OutNodeId.u4NodeId;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetEggrIfNhFromDestIp                                *
 *                                                                           *
 * Description  : Gets the next-hop address from destination ip              *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4PeerIp    - Peer IP address                              *
 *                                                                           *
 * Output       : pu4EggrItf  - Pointer to egress interface                  *
 *                pu4NhAddr   - Pointer to next-hop address                  *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetEggrIfNhFromDestIp (UINT4 u4ContextId, UINT4 u4PeerIp,
                             UINT4 *pu4EggrItf, UINT4 *pu4NhAddr)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetEggrIfNhFromDestIp");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (u4ContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                 "BFD", "BfdExtGetEggrIfNhFromDestIp");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->BfdRtInfoQueryMsg.u4DestinationIpAddress = u4PeerIp;
    pBfdExtInParams->BfdRtInfoQueryMsg.u4DestinationSubnetMask
        = BFD_IP_DEF_NET_MASK;
    pBfdExtInParams->BfdRtInfoQueryMsg.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;
    pBfdExtInParams->BfdRtInfoQueryMsg.u4ContextId = u4ContextId;

    /* request egress interface from the source ip of peer */
    pBfdExtInParams->eExtReqType = BFD_REQ_EGR_IF_INDEX_NH_FROM_DEST_IP;

    /* call the BFD exit function to Eggr interface */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    /*Next Hop */
    *pu4NhAddr = pBfdExtOutParams->NetIpRtInfo.u4NextHop;

    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    /* request CFA if index from the port */
    pBfdExtInParams->eExtReqType = BFD_REQ_CFA_IFINDEX_FROM_PORT;

    pBfdExtInParams->u4IfIdx = pBfdExtOutParams->NetIpRtInfo.u4RtIfIndx;

    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* call the BFD exit function to get CFA IF Index */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4EggrItf = pBfdExtOutParams->u4Port;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtFmApiSysLog                                          *
 *                                                                           *
 * Description  : To call external syslog api                                *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                u4SyslogLevel - Syslog Level                               *
 *                pBuf          - Syslog message                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtFmApiSysLog (UINT4 u4ContextId, UINT4 u4SyslogLevel, CHR1 * pBuf)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;

    UNUSED_PARAM (u4ContextId);
    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_FM_SYSLOG_MSG;
    pBfdExtInParams->unReqParams.BfdSyslogMsg.pSyslogMsg = (UINT1 *) pBuf;
    pBfdExtInParams->unReqParams.BfdSyslogMsg.u4SyslogLevel = u4SyslogLevel;

    /* call the BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdExtInParams, NULL) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetSystemMode                                        *
 *                                                                           *
 * Description  : This function calls the VCM Module to get the              *
 *                      mode of the system (SI / MI).                        *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : BFD_SI_MODE or BFD_MI_MODE                                 *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetSystemMode ()
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    INT4                i4Return = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_SYS_MODE;

    /* call the BFD exit function to get Mac Addr */
    i4Return = BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams);

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return i4Return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetSystemModeExt                                     *
 *                                                                           *
 * Description  : This function calls the VCM Module to get the              *
 *                      mode of the system (SI / MI).                        *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : BFD_SI_MODE or BFD_MI_MODE                                 *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetSystemModeExt ()
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;
    INT4                i4Return = 0;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_SYS_MODE_EXT;

    /* call the BFD exit function to get Mac Addr */
    i4Return = BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams);

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return i4Return;
}

/*****************************************************************************
 * Function     : BfdExtGetIfIndexFromMplsTnlIf                              *
 *                                                                           *
 * Description  : This function is used to get interface index from MPLS     *
 *                tunnel interface                                           *
 *                                                                           *
 * Input        : u4TnlIfIndex - Tunnel interface index                      *
 *                bLockReq     - Lock required or not                        *
 *                                                                           *
 * Output       : pu4L3VlanIf - Pointer to the interface index               *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetIfIndexFromMplsTnlIf (UINT4 u4TnlIfIndex, UINT4 *pu4L3VlanIf,
                               BOOL1 bLockReq)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    /* Allocate memory from mempool */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* request L3VLAN IF */
    pBfdExtInParams->eExtReqType = BFD_REQ_GET_L3VLAN_IF_FROM_TNL_IF;
    pBfdExtInParams->u2IfTnlIdx = (UINT2) u4TnlIfIndex;
    pBfdExtInParams->bLockRequired = bLockReq;

    /* call the BFD exit function to get Mac Addr */
    if (BfdPortHandleExtInteraction
        (pBfdExtInParams, pBfdExtOutParams) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4L3VlanIf = pBfdExtOutParams->u4L3VlanIf;
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}

#ifdef MBSM_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : BfdExtGetMbsmParams                                        *
 *                                                                           *
 * Description  : This function is used to get slot id and card num for a    *
 *                session                                                    *
 *                                                                           *
 * Input        : pBfdSessEntry - Session Entry structure                    *
 *                                                                           *
 * Output       : pi4CardNum  - Card Number of the session                   *
 *                pi4SlotId   - slot id of the session                       *
 * Returns      : OSIX_SUCCES/OSIX_FAILURE                                   *
 *                                                                           *
 *****************************************************************************/
INT4
BfdExtGetMbsmParams (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                     UINT4 *pu4CardNum, UINT4 *pu4SlotId)
{
    tBfdExtInParams    *pBfdExtInParams = NULL;
    tBfdExtOutParams   *pBfdExtOutParams = NULL;

    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdSessEntry->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        return OSIX_FAILURE;
    }
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdExtInParams->eExtReqType = BFD_REQ_MBSM_PARAMS;
    pBfdExtInParams->unReqParams.u4SessionId =
        pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
    pBfdExtInParams->u4ContextId =
        pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

    /* Call BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdExtInParams, pBfdExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }
    *pu4CardNum = pBfdExtOutParams->BfdMbsmParams.u4CardNum;
    *pu4SlotId = pBfdExtOutParams->BfdMbsmParams.u4SlotId;

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);
    return OSIX_SUCCESS;
}
#endif /*MBSM_WANTED */

#endif /* _BFDEXT_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdext.c                       */
/*-----------------------------------------------------------------------*/
