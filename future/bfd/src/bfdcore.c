/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdcore.c,v 1.41 2016/11/02 09:36:10 siva Exp $
 *
 * Description: This file contains the BFD module core functionality  
 *****************************************************************************/

#ifndef _BFDCORE_C_
#define _BFDCORE_C_

#include "bfdinc.h"

PRIVATE INT4
 
 
 
 BfdCoreDeleteBfdSessDiscMapNode (tBfdFsMIStdBfdSessDiscMapEntry *
                                  pBfdSessDiscMapEntry);

PRIVATE INT4        BfdCoreCreateBfdSessDiscMapEntry (tBfdFsMIStdBfdSessEntry
                                                      * pBfdFsMIStdBfdSessInfo);

PRIVATE INT4        BfdCoreDeleteBfdSessDiscMapEntry (tBfdFsMIStdBfdSessEntry
                                                      * pBfdSessEntry);

/*******************************************************************************
 * * Function     : BfdCoreDeleteBfdSessDiscMapNode                            *
 * * Description  : Deletes entries from BFD session discriminator map table   *
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PRIVATE INT4
BfdCoreDeleteBfdSessDiscMapNode (tBfdFsMIStdBfdSessDiscMapEntry
                                 * pBfdSessDiscMapEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdSessDiscMapEntryExist = NULL;

    if (NULL == pBfdSessDiscMapEntry)
    {
        return OSIX_FAILURE;
    }

    pBfdSessDiscMapEntryExist =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
                   (tRBElem *) pBfdSessDiscMapEntry);

    if (pBfdSessDiscMapEntryExist != NULL)
    {

        RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
                   (tRBElem *) pBfdSessDiscMapEntry);

        if (OSIX_FAILURE ==
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                                (UINT1 *) pBfdSessDiscMapEntryExist))
        {
            BFD_ISS_LOG (pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_FREE_MEMORY_FAIL, "BFD",
                         "BfdFsMIStdBfdSessDiscMapTable");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreCreateMapEntries
 * Description :  This function is used to create BFD session map entries
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdCoreCreateMapEntries (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo)
{
    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    if (OSIX_SUCCESS != BfdCoreCreateBfdSessDiscMapEntry
        (pBfdFsMIStdBfdSessInfo))
    {
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DISCR_MAP_CREATE_FAIL,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreDeleteMapEntries
 * Description :  This function is used to delete BFD session map entries
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdCoreDeleteMapEntries (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo)
{
    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    /* Delete the BFD session discriminator map entry */
    if (OSIX_FAILURE ==
        BfdCoreDeleteBfdSessDiscMapEntry (pBfdFsMIStdBfdSessInfo))
    {
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DISCR_MAP_DEL_FAIL,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex);
        return OSIX_FAILURE;
    }
    if (BfdCoreDeleteBfdSessIpMapEntry (pBfdFsMIStdBfdSessInfo) != OSIX_SUCCESS)
    {
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_IP_MAP_ENTRY_DEL_FAIL,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreCreateBfdSessDiscMapEntry
 * Description :  This function is used to create session discriminator map
 *                entry
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PRIVATE INT4
BfdCoreCreateBfdSessDiscMapEntry (tBfdFsMIStdBfdSessEntry *
                                  pBfdFsMIStdBfdSessInfo)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdSessDiscMapEntry = NULL;
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdSessDiscMapEntryExist = NULL;

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    /* Allocate memory for BFD Session Discriminator Map
     * Entry */
    pBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID);

    if (pBfdSessDiscMapEntry == NULL)
    {
        BfdUtilUpdateMemAlocFail
            (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);
        BFD_ISS_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                     BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                     "BfdFsMIStdBfdSessDiscMapTable");
        return OSIX_FAILURE;
    }

    /* Initialize the entry */
    pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId;
    pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscriminator =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessDiscriminator;
    pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex;

    /* Check whether the node is already present */
    pBfdSessDiscMapEntryExist =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
                   (tRBElem *) pBfdSessDiscMapEntry);

    if (pBfdSessDiscMapEntryExist)
    {
        BFD_LOG (pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DISCR_MAP_ENTRY_PRESENT,
                 pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex);
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                            (UINT1 *) pBfdSessDiscMapEntry);
        return OSIX_FAILURE;
    }

    /* Add the new node to the database */
    if (OSIX_FAILURE == RBTreeAdd
        (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
         (tRBElem *) pBfdSessDiscMapEntry))
    {
        BFD_LOG (pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_FAIL_TO_ADD_NEW_NODE,
                 pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdSessDiscMapIndex);

        if (OSIX_FAILURE ==
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                                (UINT1 *) pBfdSessDiscMapEntry))
        {
            BFD_ISS_LOG (pBfdSessDiscMapEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_MEMPOOL_DEINIT_FAILED, "BFD:BfdSessDiscMapEntry");
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdCoreDeleteBfdSessDiscMapEntry                           *
 * * Description  : Deletes entry from BFD session discriminator map table     *
 * * Input        : pBfdSessEntry - pointer to BFD session entry               *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PRIVATE INT4
BfdCoreDeleteBfdSessDiscMapEntry (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry BfdSessDiscMapEntry;

    if (NULL == pBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&BfdSessDiscMapEntry, 0, sizeof (tBfdFsMIStdBfdSessDiscMapEntry));

    BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdContextId =
        pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
    BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdSessDiscriminator =
        pBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator;
    BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdSessDiscMapIndex =
        pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

    /* Delete the BFD session discriminator map node from the database */
    if (OSIX_FAILURE == BfdCoreDeleteBfdSessDiscMapNode (&BfdSessDiscMapEntry))
    {
        BFD_LOG (BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DISC_MAP_ENTRY_DEL_FAILED,
                 BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdContextId,
                 BfdSessDiscMapEntry.MibObject.u4FsMIStdBfdSessDiscriminator);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreCreateOffldSess
 * Description :  This function is used to create a offload session.
 *                Incase of failure start a timer to trigger offload again.
 *                In case offload fails due to NP API failure, trigger a
 *                trap for user intervention.
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session
 *                pBfdPktInfo - Rx packet info. This can be null in cases
 *                where discriminator is statically configured.
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdCoreCreateOffldSess (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo,
                        tBfdPktInfo * pBfdPktInfo)
{
    INT4                i4Result;
    UINT4               u4RemainingTime = 0;
    UINT1               u1Error = 0;

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    if (pBfdFsMIStdBfdSessInfo->BfdHwhandle.u4BfdSessHwHandle != 0)
    {
        /*Should never happen */
        return OSIX_SUCCESS;
    }

    i4Result =
        BfdOffSessCreate (pBfdFsMIStdBfdSessInfo->MibObject.
                          u4FsMIStdBfdContextId, pBfdFsMIStdBfdSessInfo,
                          pBfdPktInfo, &pBfdFsMIStdBfdSessInfo->BfdHwhandle,
                          &u1Error);
    if (i4Result == OSIX_SUCCESS)
    {
        /* Session create is successful. Register Offload callback */
        BfdOffSessRegCb (pBfdFsMIStdBfdSessInfo->MibObject.
                         u4FsMIStdBfdContextId, BFD_OFFLD_SESS_EVENT_ALL,
                         &FsMiBfdHwSessionCallBack);

        if ((pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessAdminCtrlReq ==
             BFD_SNMP_TRUE)
            && (pBfdFsMIStdBfdSessInfo->MibObject.
                i4FsMIBfdSessAdminCtrlErrReason == BFD_SESS_ERROR_OFFLD_FAILED))
        {
            pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessAdminCtrlReq =
                BFD_SNMP_FALSE;
            pBfdFsMIStdBfdSessInfo->
                MibObject.i4FsMIBfdSessAdminCtrlErrReason = BFD_SESS_ERROR_NONE;
        }

        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdFsMIStdBfdSessInfo->
                             TxTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        if (u4RemainingTime > 0)
        {
            /* Timer is still running, dynamic parameters are still
             * available */
            BfdTmrStop (pBfdFsMIStdBfdSessInfo, BFD_TRANSMIT_TMR);
            BfdTmrStop (pBfdFsMIStdBfdSessInfo, BFD_DETECTION_TMR);
        }

    }
    else if (u1Error == BFD_OFF_FAILED)
    {
        /* Start a timer to start offload again after some time */
        BfdTmrStart (pBfdFsMIStdBfdSessInfo, BFD_DETECTION_TMR,
                     BFD_DETECTION_OFFLOAD_TMR);
        i4Result = OSIX_SUCCESS;

    }
    else if (u1Error == BFD_NP_FAILED)
    {
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessAdminCtrlReq =
            BFD_SNMP_TRUE;
        pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
            BFD_SESS_ERROR_OFFLD_FAILED;

        /* Raise a trap indicating session offload failed */
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_OFFLOAD_FAILED,
                 pBfdFsMIStdBfdSessInfo->MibObject.
                 u4FsMIStdBfdSessDiscriminator,
                 pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessState,
                 pBfdFsMIStdBfdSessInfo->MibObject.
                 i4FsMIBfdSessAdminCtrlErrReason);
        i4Result = OSIX_FAILURE;

    }

    if (pBfdFsMIStdBfdSessInfo->bInformedExtModule != OSIX_TRUE)
    {
        BfdCoreUpdateExtMod (pBfdFsMIStdBfdSessInfo, BFD_VALID_SESS_IDX);
    }
    return i4Result;

}

/*****************************************************************************
 *                                                                           *
 * Function     : BfdCoreUpdateExtMod                                        *
 *                                                                           *
 * Description  : This function is used to update the session index to       *
 *                external modules and to get the slot id and the card       *
 *                num of the session                                         *
 *                                                                           *
 * Input        : pBfdSessEntry - Session Entry structure                    *
 *                bValidSess - Invalid or valid session index                *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_FAILURE/OSIX_SUCCESS                                                      *
 *                                                                           *
 *****************************************************************************/
INT4
BfdCoreUpdateExtMod (tBfdFsMIStdBfdSessEntry * pBfdSessEntry, BOOL1 bValidSess)
{
#ifdef MBSM_WANTED
    UINT4               u4CardNum = BFD_INVALID_CARD_SLOT_NUM;
    UINT4               u4SlotId = BFD_INVALID_CARD_SLOT_NUM;
#endif

    if (pBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Update the Session ID to OAM or external Module */
    if (BfdExtUpdateSessParams (pBfdSessEntry, bValidSess) == OSIX_SUCCESS)
    {
        pBfdSessEntry->bInformedExtModule = OSIX_TRUE;
    }

#ifdef MBSM_WANTED
    /* Get the Slot ID and Card Num for the session */
    if (BfdExtGetMbsmParams (pBfdSessEntry,
                             &u4CardNum, &u4SlotId) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4CardNum != BFD_INVALID_CARD_SLOT_NUM)
    {
        pBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber = u4CardNum;
    }

    if (u4SlotId != BFD_INVALID_CARD_SLOT_NUM)
    {
        pBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber = u4SlotId;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreProcessRxBfdControlPkt
 * Description :  This function is used perfrom the necesssary actions on
 *                receiving the BFD control packet from the peer 
 * Input       :  pBfdSessEntry - pointer to the session info
 *                pBfdRxPktInfo - pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdCoreProcessRxBfdControlPkt (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                               tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4BfdSessOldNegotiatedInterval = 0;
    UINT4               u4OldNegRemoteTxTimeIntrvl = 0;
    UINT4               u4RemainingTime = 0;

    if ((NULL == pBfdSessEntry) || (NULL == pBfdRxPktInfo))
    {
        return OSIX_FAILURE;
    }

    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus != ACTIVE)
    {
        return OSIX_FAILURE;
    }

    if (pBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate != OSIX_TRUE)
    {
        /* Timer negotiation is not allowed, check for timer 
         * misconfiguration defect */
        if ((BFD_SESS_REQ_RX (pBfdSessEntry) !=
             pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.
             u4BfdRxPktMinTxIntrvl) ||
            (BFD_SESS_MIN_TX (pBfdSessEntry) !=
             pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.
             u4BfdRxPktReqMinRxIntrvl) ||
            (pBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult !=
             pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.u1BfdRxPktDetectMulti))
        {
            /* Raise Timer misconfiguration defect trap */
            if (pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq !=
                BFD_SNMP_TRUE)
            {

                pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
                    BFD_SNMP_TRUE;
                pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                    BFD_SESS_ERROR_PERIOD_MISCONFIG;
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_TMR_MIS_CONFIG,
                         pBfdSessEntry->MibObject.i4FsMIStdBfdSessState,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator,
                         pBfdSessEntry->MibObject.
                         i4FsMIBfdSessAdminCtrlErrReason);

            }
            return OSIX_SUCCESS;
        }
        else
        {
            if (pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason
                == BFD_SESS_ERROR_PERIOD_MISCONFIG)
            {
                pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq =
                    BFD_SNMP_FALSE;
                pBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
                    BFD_SESS_ERROR_NONE;
            }
        }
    }
    /* Received valid BFD control packet from the peer */
    /* Negotiated Detect multi */
    BFD_SESS_NEG_DETECT_MULT (pBfdSessEntry) =
        pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.u1BfdRxPktDetectMulti;

    /*Check whether dynamic timing parameters are learned */
    if (pBfdSessEntry->bDynamicParamsLearned == OSIX_FALSE)
    {
        /* first packet, take down the remote timing parameters
         * and check for misconfiguration defects. After learning
         * if the timer negotiation is not allowed or params change
         * is not allowed, none of the timing parameters are
         * relearned */
        BfdPollUpdateNegParams (pBfdSessEntry, pBfdRxPktInfo);

        /* Set the params learned flag */
        pBfdSessEntry->bDynamicParamsLearned = OSIX_TRUE;
    }

    /*Handle Poll sequence irrespective of state */
    if (pBfdRxPktInfo->bCvValidationFail != OSIX_TRUE)
    {
        u4BfdSessOldNegotiatedInterval =
            pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval;
        u4OldNegRemoteTxTimeIntrvl = pBfdSessEntry->u4NegRemoteTxTimeIntrvl;

        if (BfdPollHandlePollSequence (pBfdSessEntry, pBfdRxPktInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (u4BfdSessOldNegotiatedInterval != pBfdSessEntry->
            MibObject.u4FsMIStdBfdSessNegotiatedInterval)
        {

            /* Detect timer will be updated in SEM based on the state.
             * Tx Timer needs to be updated if there is any change 
             * due to poll sequence. Otherwise, we risk waiting for
             * Tx timer expiry to update */

            TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                                 &pBfdSessEntry->
                                 TxTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

            if (u4RemainingTime > 0)
            {
                /* restart the timer with new value */
                BfdTmrReStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                               pBfdSessEntry->TxTimer.eBfdSubTmrId);
            }

            /* Local Negotiated Timer Value changed, raise a Trap */

            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_NEGO_INT_CHANGE_TX,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr,
                     (BFD_MICROSEC_TO_MILLISEC (pBfdSessEntry->MibObject.
                                                u4FsMIStdBfdSessNegotiatedInterval)));

            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_DETECT_INTRVL,
                     ((BFD_MICROSEC_TO_MILLISEC
                       (pBfdSessEntry->u4NegRemoteTxTimeIntrvl *
                        pBfdSessEntry->MibObject.
                        u4FsMIStdBfdSessNegotiatedDetectMult))),
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        }

        if (u4OldNegRemoteTxTimeIntrvl !=
            pBfdSessEntry->u4NegRemoteTxTimeIntrvl)
        {

            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_REMOTE_NEGO_INT_CHANGE,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessDiscriminator,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr,
                     BFD_MICROSEC_TO_MILLISEC
                     (pBfdRxPktInfo->BfdCtrlPacket.BfdPktParams.
                      u4BfdRxPktMinTxIntrvl),
                     BFD_MICROSEC_TO_MILLISEC (pBfdSessEntry->MibObject.
                                               u4FsMIStdBfdSessReqMinRxInterval),
                     BFD_MICROSEC_TO_MILLISEC
                     (pBfdSessEntry->u4NegRemoteTxTimeIntrvl),
                     BFD_MICROSEC_TO_MILLISEC
                     ((pBfdSessEntry->u4NegRemoteTxTimeIntrvl *
                       pBfdSessEntry->MibObject.
                       u4FsMIStdBfdSessNegotiatedDetectMult)));
        }
    }

    /* Trigger state machine with Rx packet state */
    if (BFD_SESS_OFFLD (pBfdSessEntry) != OSIX_TRUE)
    {
        BfdCoreTriggerBfdSem (pBfdSessEntry, BFD_SEM_REQ_RX_PKT, pBfdRxPktInfo);
    }
    else
    {
        /* If not already offloaded, offload.Else ignore the 
         * packet */

        if (pBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle == 0)
        {
            BfdCoreCreateOffldSess (pBfdSessEntry, pBfdRxPktInfo);
        }

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreHandleAdminStatusChg
 * Description :  This function is used to Handle Admin status changes
 *                when BFD session row status is ACTIVE
 *                required event
 * Input       :  pBfdSessEntry - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
INT4
BfdCoreHandleAdminStatusChg (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* This function is called only when admin status change
     * occurs */
    if ((BFD_SESS_OFFLD (pBfdSessEntry) == OSIX_TRUE) &&
        (pBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0) &&
        (pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr != 0))
    {
        if (pBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAdminStatus == BFD_SESS_ADMIN_START)
        {
            i4RetVal = BfdOffSessEnable (pBfdSessEntry->
                                         MibObject.u4FsMIStdBfdContextId,
                                         pBfdSessEntry,
                                         &pBfdSessEntry->BfdHwhandle);
            if (i4RetVal == OSIX_SUCCESS)
            {
                BFD_SESS_STATE (pBfdSessEntry) = BFD_SESS_STATE_DOWN;
                BFD_SESS_DIAG (pBfdSessEntry) = BFD_DIAG_NO_DIAG;
            }
            return i4RetVal;
        }
        else
        {
            i4RetVal = BfdUtilGetBfdOffStats (pBfdSessEntry);
            if(i4RetVal == OSIX_FAILURE)
            {
                return i4RetVal;
            }
            BFD_SESS_STATE (pBfdSessEntry) = BFD_SESS_STATE_ADMIN_DOWN;
            BFD_SESS_DIAG (pBfdSessEntry) = BFD_DIAG_ADMIN_DOWN;
            return BfdOffSessStop (pBfdSessEntry->
                                   MibObject.u4FsMIStdBfdContextId,
                                   pBfdSessEntry, BFD_OFFLD_DISABLE_SESSION);
        }
    }
    else
    {
        if (pBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAdminStatus == BFD_SESS_ADMIN_START)
        {
            return BfdCoreTriggerBfdSem (pBfdSessEntry,
                                         BFD_SEM_REQ_ADMIN_UP, NULL);
        }
        else
        {
            return BfdCoreTriggerBfdSem (pBfdSessEntry,
                                         BFD_SEM_REQ_ADMIN_DOWN, NULL);
        }
    }
}

/****************************************************************************
 * Function    :  BfdCoreTriggerBfdSem
 * Description :  This function is used to trigger the BFD sem with the 
 *                required event
 * Input       :  pBfdSessEntry - pointer to the session info
 *                BfdSemReqType - request type for the sem
 *                pBfdRxPktInfo - Rx pkt information. Can be NULL
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdCoreTriggerBfdSem (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                      tBfdSemReqType BfdSemReqType, tBfdPktInfo * pBfdRxPktInfo)
{

    if (NULL == pBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    switch (BfdSemReqType)
    {
        case BFD_SEM_REQ_RX_PKT:
        {
            /* Packet is received from peer. Update peer header flag */
            pBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_TRUE;
            BfdSemTrigger (pBfdSessEntry,
                           (pBfdSessEntry->LastRxCtrlPktParams.u1BfdRxPktSta),
                           pBfdRxPktInfo);
            break;
        }
        case BFD_SEM_REQ_ADMIN_UP:
        {
            BfdSemTrigger (pBfdSessEntry, BFD_LOCAL_ADMINUP_EVENT, NULL);
            break;
        }
        case BFD_SEM_REQ_ADMIN_DOWN:
        {
            BfdSemTrigger (pBfdSessEntry, BFD_LOCAL_ADMINDOWN_EVENT, NULL);
            /* Bringing session ADMIN DOWN. Set remote heard flag */
            pBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_FALSE;
            break;
        }

        case BFD_SEM_REQ_TMR_EXP:
        {
            BfdSemTrigger (pBfdSessEntry, BFD_LOCAL_DETTMREXP_EVENT, NULL);
            /* Detection timer expired. Not heard "recently" from peer */
            pBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_FALSE;
            break;
        }
        case BFD_SEM_REQ_PATH_DOWN:
        {
            BfdSemTrigger (pBfdSessEntry, BFD_PATHSTCHG, NULL);
            break;
        }
        default:
        {
            return OSIX_FAILURE;
        }

    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  BfdCoreCreateBfdSessIpMapEntry
 * Description :  This function is used to create session IP map entry
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdCoreCreateBfdSessIpMapEntry (tBfdFsMIStdBfdSessEntry *
                                pBfdFsMIStdBfdSessInfo)
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry = NULL;
	UINT1 au1FsMIStdBfdSessSrcAddr[BFD_IPV6_MAX_ADDR_LEN];

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }
	MEMSET(au1FsMIStdBfdSessSrcAddr, 0, BFD_IPV6_MAX_ADDR_LEN);

    /* Allocate memory for BFD Session IP Map Entry */
    pBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID);

    if (pBfdSessIpMapEntry == NULL)
    {
        BfdUtilUpdateMemAlocFail
            (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);
        BFD_ISS_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                     BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                     "BfdFsMIStdBfdSessIpMapTable");
        return OSIX_FAILURE;
    }

    BfdUtilFillIpMapEntry (pBfdFsMIStdBfdSessInfo, pBfdSessIpMapEntry);
	if (pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV6) 
	{
		if ((pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen == 0) ||
				(pBfdSessIpMapEntry->MibObject.i4FsMIStdBfdSessSrcAddrType == 0))
		{
			if (MEMCMP(pBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr, 
						au1FsMIStdBfdSessSrcAddr, BFD_IPV6_MAX_ADDR_LEN) == 0)
			{
				if (TmrStart (gBfdGlobals.bfdTmrLst, &(pBfdFsMIStdBfdSessInfo->BfdAddrCompTmr.BfdTmrBlk), 
							BFD_ADDR_COMP_TMR, BFD_DAD_COMP_TIME ,0) != TMR_SUCCESS)
				{
					MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
							(UINT1 *) pBfdSessIpMapEntry);
					return OSIX_FAILURE;
				}
			}

		}
	}


    /* Add the new node to the database */
    if (OSIX_FAILURE == RBTreeAdd
        (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
         (tRBElem *) pBfdSessIpMapEntry))
    {
        BFD_LOG (pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_FAIL_TO_ADD_NEW_NODE,
                 pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex);

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                            (UINT1 *) pBfdSessIpMapEntry);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdCoreDeleteBfdSessIpMapEntry
 * Description :  This function is to delete the session IP map entry
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdCoreDeleteBfdSessIpMapEntry (tBfdFsMIStdBfdSessEntry *
                                pBfdFsMIStdBfdSessInfo)
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry = NULL;

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    pBfdSessIpMapEntry = BfdUtilCheckIpMapEntryExist (pBfdFsMIStdBfdSessInfo);

    if (pBfdSessIpMapEntry != NULL)
    {
        RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
                   (tRBElem *) pBfdSessIpMapEntry);

        MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                            (UINT1 *) pBfdSessIpMapEntry);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : BfdCoreCreateDynamicSession
 * Description : This function is used to create a BFD session dynamically for
 *               monitoring the neighbor IP path requested by the BFD client
 * Input       : u4ContextId    - Context Identifier
 *               pBfdClientInfo - pointer to the neigbhor IP path info
 * Output      : None
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdCoreCreateDynamicSession (UINT4 u4ContextId, tBfdClientInfo * pBfdClientInfo)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdGlobalConfigTableEntry;
    tBfdIsSetFsMIStdBfdSessEntry BfdIsSetFsMIStdBfdSessEntry;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    UINT1               au1BfdClient[BFD_CLIENT_STORAGE_SIZE];
    UINT4               u4SessIndex = 0;
    UINT4               u4ReqInterval = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1MapType = 0;
    INT4                i4RetCode = OSIX_SUCCESS;

    if (pBfdClientInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (au1BfdClient, 0, sizeof (au1BfdClient));
    MEMSET (&BfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
    MEMSET (&BfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    BfdGlobalConfigTableEntry.MibObject.u4FsMIStdBfdContextId = u4ContextId;
    /* Get the Global config table to fill the desired transmit interval,
     * detect multiplier and rx interval, if it is not provided*/
    if (BfdGetAllFsMIStdBfdGlobalConfigTable
        (&BfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Allocate memory for the new node */
    pBfdFsMIStdBfdSessEntry = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdCoreCreateDynamicSession : Fail to Allocate Memory\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pBfdFsMIStdBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    /* Intialize the entry with default values */
    if ((BfdInitializeFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry)) ==
        OSIX_FAILURE)
    {
        BFD_TRC ((BFD_UTIL_TRC, "BfdCoreCreateDynamicSession :\
                         Fail to Initialize the Objects.\r\n"));
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }

    /* Get the free session index to be used for the new session */
    BfdUtilGetFreeSessIndex (u4ContextId, &u4SessIndex);

    /* Fill the other parameters for the dynamic session from the inputs
     * provided by the client */
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId = u4ContextId;
    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex = u4SessIndex;
    pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIStdBfdSessDiscriminator = u4SessIndex;

    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessType = pBfdClientInfo->u1SessionType;
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessOperMode = BFD_OPER_MODE_ASYNC_WO_ECHOFUNCTION;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface =
        (INT4) pBfdClientInfo->BfdClientNbrIpPathInfo.u4IfIndex;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
        pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType =
        pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType;

    if (pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType == BFD_INET_ADDR_IPV4)
    {
        u1AddrLen = BFD_IPV4_MAX_ADDR_LEN;
        u1MapType = BFD_PATH_TYPE_IPV4;
    }
#ifdef IP6_WANTED
    else if (pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType ==
             BFD_INET_ADDR_IPV6)
    {
        u1AddrLen = BFD_IPV6_MAX_ADDR_LEN;
        u1MapType = BFD_PATH_TYPE_IPV6;
    }
#endif
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen = u1AddrLen;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen = u1AddrLen;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType = u1MapType;

    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
            &pBfdClientInfo->BfdClientNbrIpPathInfo.SrcAddr, u1AddrLen);
    MEMCPY (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
            &pBfdClientInfo->BfdClientNbrIpPathInfo.NbrAddr, u1AddrLen);

    /* If Desired detection time is not provided by the client, use the
     * values from the global config table*/
    if (pBfdClientInfo->BfdClientParams.u4DesiredDetectionTime != 0)
    {
        u4ReqInterval = pBfdClientInfo->BfdClientParams.u4DesiredDetectionTime / 3;

        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinRxInterval = u4ReqInterval;
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval = u4ReqInterval;
	pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult = 3;
    }

    else
    {
        pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
            BfdGlobalConfigTableEntry.MibObject.u4FsMIBfdGblReqMinRxIntvl;
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval =
            BfdGlobalConfigTableEntry.MibObject.u4FsMIBfdGblDesiredMinTxIntvl;
    

    pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult =
        BfdGlobalConfigTableEntry.MibObject.u4FsMIBfdGblDetectMult;
    }

    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessAdminCtrlReq = BFD_SNMP_FALSE;
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessRemoteHeardFlag = BFD_SNMP_FALSE;
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType = BFD_VOLATILE;

    /* Set the bit corresponding to the client ID */
    OSIX_BITLIST_SET_BIT (au1BfdClient, pBfdClientInfo->u1BfdClientId,
                          sizeof (au1BfdClient));
    MEMCPY (&pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients,
            au1BfdClient, sizeof (au1BfdClient));

    pBfdFsMIStdBfdSessEntry->u1BfdClientsCount = 1;

    MEMCPY (&(pBfdFsMIStdBfdSessEntry->
              aBfdClientParams[pBfdClientInfo->u1BfdClientId - 1]),
            &pBfdClientInfo->BfdClientParams, sizeof (tBfdClientParams));

    /* Add the new node to the database */
    if (RBTreeAdd (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                   (tRBElem *) pBfdFsMIStdBfdSessEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdFsMIStdBfdSessEntry);
        return OSIX_FAILURE;
    }

    /* Incremental save for the row status object with CREATE_AND_WAIT for Dynamic sessions */
    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus =
        CREATE_AND_WAIT;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessRowStatus = OSIX_TRUE;
    BfdUtilDynBfdSessTableTrigger (pBfdFsMIStdBfdSessEntry,
                                   &BfdIsSetFsMIStdBfdSessEntry, SNMP_SUCCESS);

    i4RetCode = BfdCoreCreateMapEntries (pBfdFsMIStdBfdSessEntry);
    UNUSED_PARAM(i4RetCode);

    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = ACTIVE;

    if (BfdUtilUpdateFsMIStdBfdSessTable (NULL, pBfdFsMIStdBfdSessEntry,
                                          &BfdIsSetFsMIStdBfdSessEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Incremental save for all other objects updated during Dynamic session creation */
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessType = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessOperMode = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessInterface = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessSrcAddrType = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessSrcAddr = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessDstAddrType = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessDstAddr = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIBfdSessMapType = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessDesiredMinTxInterval = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessReqMinRxInterval = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessDetectMult = OSIX_TRUE;
    BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessStorType = OSIX_TRUE;

    BfdUtilDynBfdSessTableTrigger (pBfdFsMIStdBfdSessEntry,
                                   &BfdIsSetFsMIStdBfdSessEntry, SNMP_SUCCESS);

    BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                            &(pBfdFsMIStdBfdSessEntry->MibObject.SessDbNode));
    BfdRedSyncDynInfo ();


    return OSIX_SUCCESS;
}

#endif /* _BFDCORE_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdcore.c                      */
/*-----------------------------------------------------------------------*/
