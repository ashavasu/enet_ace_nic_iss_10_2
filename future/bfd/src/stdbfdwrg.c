/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbfdwrg.c,v 1.5 2010/10/31 03:43:32 prabuc Exp $
*
* Description:  Low Level GET Routine for All Objects
*********************************************************************/
#include "bfdinc.h"
#include "fsstdbdb.h"

VOID
RegisterSTDBFD ()
{
    SNMPRegisterMibWithLock (&fsstdbOID, &fsstdbEntry, BfdMainTaskLock,
                             BfdMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fsstdbOID, (const UINT1 *) "fsstdbfd");
}

VOID
UnRegisterSTDBFD ()
{
    SNMPUnRegisterMib (&fsstdbOID, &fsstdbEntry);
    SNMPDelSysorEntry (&fsstdbOID, (const UINT1 *) "fsstdbfd");
}

INT4
BfdAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetBfdAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
BfdAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2BfdAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
BfdAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetBfdAdminStatus (pMultiData->i4_SLongValue));
}

INT4
BfdAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2BfdAdminStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
BfdSessNotificationsEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetBfdSessNotificationsEnable (&(pMultiData->i4_SLongValue)));
}

INT4
BfdSessNotificationsEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2BfdSessNotificationsEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
BfdSessNotificationsEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetBfdSessNotificationsEnable (pMultiData->i4_SLongValue));
}

INT4
BfdSessNotificationsEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2BfdSessNotificationsEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexBfdSessTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexBfdSessTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexBfdSessTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
BfdSessVersionNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessVersionNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessType (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessDiscriminatorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDiscriminator (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessRemoteDiscrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessRemoteDiscr (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessDestinationUdpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDestinationUdpPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessSourceUdpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessSourceUdpPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessEchoSourceUdpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessEchoSourceUdpPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessAdminStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessState (pMultiIndex->pIndex[0].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessRemoteHeardFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessRemoteHeardFlag (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessDiagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDiag (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessOperModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessOperMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessDemandModeDesiredFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDemandModeDesiredFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessControlPlaneIndepFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessControlPlaneIndepFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessMultipointFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessMultipointFlag (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessInterface (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessSrcAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessSrcAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessSrcAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessSrcAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
BfdSessDstAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDstAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessDstAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDstAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
BfdSessGTSMGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessGTSM (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessGTSMTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessGTSMTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessDesiredMinTxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDesiredMinTxInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessReqMinRxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessReqMinRxInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessReqMinEchoRxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessReqMinEchoRxInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessDetectMultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDetectMult (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessNegotiatedIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessNegotiatedInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessNegotiatedEchoIntervalGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessNegotiatedEchoInterval
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessNegotiatedDetectMultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessNegotiatedDetectMult
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessAuthPresFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessAuthPresFlag (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessAuthenticationTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessAuthenticationType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessAuthenticationKeyIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessAuthenticationKeyID
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessAuthenticationKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessAuthenticationKey
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
BfdSessStorTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessStorType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessVersionNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessVersionNumber (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
BfdSessTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessType (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
BfdSessDestinationUdpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDestinationUdpPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
BfdSessSourceUdpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessSourceUdpPort (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
BfdSessEchoSourceUdpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessEchoSourceUdpPort (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
BfdSessAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessAdminStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
BfdSessOperModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessOperMode (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
BfdSessDemandModeDesiredFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDemandModeDesiredFlag (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
BfdSessControlPlaneIndepFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessControlPlaneIndepFlag (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
BfdSessMultipointFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessMultipointFlag (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
BfdSessInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessInterface (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
BfdSessSrcAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessSrcAddrType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
BfdSessSrcAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessSrcAddr (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
BfdSessDstAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDstAddrType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
BfdSessDstAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDstAddr (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
BfdSessGTSMTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessGTSM (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
BfdSessGTSMTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessGTSMTTL (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
BfdSessDesiredMinTxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDesiredMinTxInterval (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
BfdSessReqMinRxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessReqMinRxInterval (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
BfdSessReqMinEchoRxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessReqMinEchoRxInterval (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
BfdSessDetectMultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessDetectMult (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
BfdSessAuthPresFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessAuthPresFlag (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessAuthenticationType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationKeyIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessAuthenticationKeyID (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessAuthenticationKey (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
BfdSessStorTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessStorType (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
BfdSessRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2BfdSessRowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
BfdSessVersionNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessVersionNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
BfdSessTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessType (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->i4_SLongValue));

}

INT4
BfdSessDestinationUdpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDestinationUdpPort
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
BfdSessSourceUdpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessSourceUdpPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
BfdSessEchoSourceUdpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessEchoSourceUdpPort
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
BfdSessAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessAdminStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
BfdSessOperModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessOperMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
BfdSessDemandModeDesiredFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDemandModeDesiredFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
BfdSessControlPlaneIndepFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessControlPlaneIndepFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
BfdSessMultipointFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessMultipointFlag (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
BfdSessInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessInterface (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
BfdSessSrcAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessSrcAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
BfdSessSrcAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessSrcAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
BfdSessDstAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDstAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
BfdSessDstAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDstAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
BfdSessGTSMSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessGTSM (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->i4_SLongValue));

}

INT4
BfdSessGTSMTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessGTSMTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
BfdSessDesiredMinTxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDesiredMinTxInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
BfdSessReqMinRxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessReqMinRxInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
BfdSessReqMinEchoRxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessReqMinEchoRxInterval
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
BfdSessDetectMultSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessDetectMult (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
BfdSessAuthPresFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessAuthPresFlag (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessAuthenticationType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationKeyIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessAuthenticationKeyID
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
BfdSessAuthenticationKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessAuthenticationKey
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
BfdSessStorTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessStorType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
BfdSessRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetBfdSessRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
BfdSessTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2BfdSessTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexBfdSessPerfTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexBfdSessPerfTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexBfdSessPerfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
BfdSessPerfCtrlPktInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfCtrlPktOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfCtrlPktDropGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktDrop (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfCtrlPktDropLastTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktDropLastTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfEchoPktInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktIn (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfEchoPktOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktOut (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfEchoPktDropGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktDrop (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfEchoPktDropLastTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktDropLastTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessUpTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfLastSessDownTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfLastSessDownTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfLastCommLostDiagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfLastCommLostDiag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
BfdSessPerfSessUpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfSessUpCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfDiscTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfDiscTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
BfdSessPerfCtrlPktInHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktInHC (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
BfdSessPerfCtrlPktOutHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktOutHC (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
BfdSessPerfCtrlPktDropHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfCtrlPktDropHC
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
BfdSessPerfEchoPktInHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktInHC (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
BfdSessPerfEchoPktOutHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktOutHC (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
BfdSessPerfEchoPktDropHCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessPerfEchoPktDropHC
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
GetNextIndexBfdSessDiscMapTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexBfdSessDiscMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexBfdSessDiscMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
BfdSessDiscMapIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessDiscMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessDiscMapIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexBfdSessIpMapTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexBfdSessIpMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexBfdSessIpMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
BfdSessIpMapIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceBfdSessIpMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetBfdSessIpMapIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}
