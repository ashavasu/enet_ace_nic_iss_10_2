/*****************************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  $Id: bfdbtstr.c,v 1.25 2017/06/08 11:40:29 siva Exp $
 *  Description: This file contains the session bootstrap related functionality
 *               of the BFD module
 ******************************************************************************/

#ifndef _BFDBTSTR_C_
#define _BFDBTSTR_C_

#include "bfdinc.h"

PRIVATE INT4        BfdBtStrTriggerOOBBtStrapReply (tBfdFsMIStdBfdSessEntry *,
                                                    tBfdLsppInfo *);

/****************************************************************************
 * Function    :  BfdBtStrProcessLsppBtStrapInfo
 * Description :  This function is used to take appropriate actions when
 *                Lsp ping module indicates the receipt of a bootstrap
 *                Lsp Ping request or reply
 * Input       :  pBfdLsppInInfo - pointer to the BFD bootstrap info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdBtStrProcessLsppBtStrapInfo (UINT4 u4ContextId, tBfdLsppInfo * pBfdLsppInfo)
{
    tBfdMplsPathInfo   *pMplsPathInfo = NULL;
    tBfdSessPathParams  BfdSessPathParams;
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;
    UINT4               u4BfdSessIdx = 0;
    BOOL1               bUpdateSessIdxToOAM = OSIX_FALSE;
    UINT4               u4Return = OSIX_SUCCESS;

    MEMSET (&BfdSessPathParams, 0, sizeof (tBfdSessPathParams));

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return OSIX_SUCCESS;
    }

    if (NULL == pBfdLsppInfo)
    {
        return OSIX_FAILURE;
    }

    BfdSessPathParams.ePathType = pBfdLsppInfo->PathId.ePathType;

    /* Check if the call is made for a received LSPP response
     * or for a received LSP Ping bootstrap request */
    if (OSIX_TRUE == pBfdLsppInfo->bIsEgress)
    {
        /* Allocate memory for MPLS Path info */
        pMplsPathInfo = (tBfdMplsPathInfo *)
            MemAllocMemBlk (BFD_MPLSPATHINFOSTRUCT_POOLID);
        if (pMplsPathInfo == NULL)
        {
            BfdUtilUpdateMemAlocFail (u4ContextId);
            BFD_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                     "BfdBtStrProcessLsppBtStrapInfo");
            return OSIX_FAILURE;
        }
        MEMSET (pMplsPathInfo, 0, sizeof (tBfdMplsPathInfo));

        /* Received LSP Ping bootstrap request */
        /* Get the session index from OAM using the Path Type */

        if (OSIX_SUCCESS != BfdExtGetMplsPathInfo
            (u4ContextId, &pBfdLsppInfo->PathId, pMplsPathInfo, OSIX_FALSE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_PROCESS_FAIL, u4ContextId);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }

        switch (pBfdLsppInfo->PathId.ePathType)
        {
            case BFD_PATH_TYPE_MEP:
                if (pMplsPathInfo->MplsMegApiInfo.u4ProactiveSessIndex == 0)
                {
                    /* Session index does not exist and path type is MEG */
                    BFD_LOG (u4ContextId, BFD_TRC_NO_SESS_INDEX_IN_OAM,
                             BfdSessPathParams.ePathType, u4ContextId);
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    return OSIX_FAILURE;
                }
                else
                {
                    /* Update session index */
                    u4BfdSessIdx =
                        pMplsPathInfo->MplsMegApiInfo.u4ProactiveSessIndex;
                }
                break;

            case BFD_PATH_TYPE_PW:
                if (pMplsPathInfo->MplsPwApiInfo.u4ProactiveSessIndex == 0)
                {
                    /* Session index does not exist and path type is PW */
                    BFD_LOG (u4ContextId, BFD_TRC_NO_SESS_INDEX_IN_OAM,
                             BfdSessPathParams.ePathType, u4ContextId);
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    return OSIX_FAILURE;
                }
                else
                {
                    /* Update session index */
                    u4BfdSessIdx =
                        pMplsPathInfo->MplsPwApiInfo.u4ProactiveSessIndex;
                }
                break;

            case BFD_PATH_TYPE_NONTE_IPV4:
            case BFD_PATH_TYPE_NONTE_IPV6:

                if (pMplsPathInfo->MplsNonTeApiInfo.u4ProactiveSessIndex == 0)
                {
                    /* Session index does not exist and path type is Non-TE
                     *                      * LSP */
                    BFD_LOG (u4ContextId, BFD_TRC_NO_SESS_INDEX_IN_OAM,
                             BfdSessPathParams.ePathType, u4ContextId);
                    MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                        (UINT1 *) pMplsPathInfo);
                    return OSIX_FAILURE;

                }
                else
                {
                    /* Update session index */
                    u4BfdSessIdx =
                        pMplsPathInfo->MplsNonTeApiInfo.u4ProactiveSessIndex;
                }
                break;

            case BFD_PATH_TYPE_TE_IPV4:
            case BFD_PATH_TYPE_TE_IPV6:
                if (pMplsPathInfo->MplsTeTnlApiInfo.u4ProactiveSessIndex == 0)
                {
                    /* Session index does not exist and path type is TE 
                     * Tunnel */
                    /* This is a rare case (and only for the first of bootstrap
                     * messages) if operator configures BFD session
                     * before the MPLS tunnel is established */
                    /* Four tuple scan on tunnel table to find session index is
                     * needed */

                    MEMCPY (&BfdSessPathParams.SessTeParams.SrcIpAddr,
                            &pBfdLsppInfo->PathId.SessTeParams.SrcIpAddr,
                            sizeof (tIpAddr));
                    MEMCPY (&BfdSessPathParams.SessTeParams.DstIpAddr,
                            &pBfdLsppInfo->PathId.SessTeParams.DstIpAddr,
                            sizeof (tIpAddr));
                    BfdSessPathParams.SessTeParams.u4TunnelId =
                        pBfdLsppInfo->PathId.SessTeParams.u4TunnelId;
                    BfdSessPathParams.SessTeParams.u4TunnelInst =
                        pBfdLsppInfo->PathId.SessTeParams.u4TunnelInst;
                    BfdSessPathParams.SessTeParams.u4AddrType =
                        pBfdLsppInfo->PathId.SessTeParams.u4AddrType;

                    if (OSIX_SUCCESS != BfdSessScanTnlTbl
                        (u4ContextId, &BfdSessPathParams, &u4BfdSessIdx))
                    {
                        /* Session Index not found in tunnel table */
                        BFD_LOG (u4ContextId, BFD_TRC_SESS_INFO_NOT_PRESENT);
                        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                            (UINT1 *) pMplsPathInfo);
                        return OSIX_FAILURE;
                    }
                    else
                    {
                        /* Update session index to OAM */
                        bUpdateSessIdxToOAM = OSIX_TRUE;
                    }
                }
                else
                {
                    BfdSessPathParams.SessTeParams.u4TunnelInst =
                        pBfdLsppInfo->PathId.SessTeParams.u4TunnelInst;
                    /* Update session index */
                    u4BfdSessIdx =
                        pMplsPathInfo->MplsTeTnlApiInfo.u4ProactiveSessIndex;
                }
                break;

            case BFD_PATH_TYPE_IPV4:
            case BFD_PATH_TYPE_IPV6:
            default:
                MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                    (UINT1 *) pMplsPathInfo);
                return OSIX_FAILURE;
        }
        /* Get the session entry using the GetApi */
        pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId, u4BfdSessIdx);

        if (NULL == pBfdSessEntry)
        {
            /* Entry is not existing */
            /* We assume here operator might have deleted the entry */
            BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_NO_SESS,
                     pBfdLsppInfo->u4BfdDiscriminator,
                     pBfdLsppInfo->u4SessionIndex, u4ContextId);
            MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                                (UINT1 *) pMplsPathInfo);
            return OSIX_FAILURE;
        }

        if (ACTIVE == pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus)
        {

            pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
                pBfdLsppInfo->u4BfdDiscriminator;
            pBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
                pBfdLsppInfo->u4BfdDiscriminator;

            if (OSIX_TRUE == bUpdateSessIdxToOAM)
            {
                BfdCoreUpdateExtMod (pBfdSessEntry, BFD_VALID_SESS_IDX);

            }

            /* Store source IP of LSP Ping */
            MEMCPY (&pBfdSessEntry->LastRxCtrlPktParams.BfdPeerIpAddress,
                    &pBfdLsppInfo->DestIpAddr, sizeof (tIpAddr));

            if (pBfdLsppInfo->u1ReplyPath != LSPP_NO_REPLY)
            {
                /* Check if session type is OOB or not */
                if (BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING !=
                    pBfdSessEntry->MibObject.i4FsMIStdBfdSessType)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_REQ,
                             pBfdLsppInfo->u4BfdDiscriminator,
                             u4BfdSessIdx, u4ContextId);
                }
                else
                {
                    /* Trigger LSP Ping reply */
                    pBfdSessEntry->BfdSessPathParams.unSessParams.
                        BfdSessTeParams.u4TunnelInst =
                        BfdSessPathParams.SessTeParams.u4TunnelInst;

                    if (OSIX_SUCCESS != BfdBtStrTriggerOOBBtStrapReply
                        (pBfdSessEntry, pBfdLsppInfo))
                    {
                        u4Return = OSIX_FAILURE;
                        BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_PROCESS_FAIL,
                                 u4ContextId);
                    }
                }
            }

            /* Trigger BFD control packet as response */
            BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);
            BfdTxPacket (pBfdSessEntry);

            /* If Passive start the timer */
            if (BFD_SESS_ROLE_PASSIVE == BFD_SESS_ROLE (pBfdSessEntry))
            {
                /* Start the slow timer */
                BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                             BFD_TRANSMIT_SLOW_TMR);
            }
            else
            {
                /* Restart the slow timer */
                u4Return =
                    (UINT4) BfdTmrReStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                                           BFD_TRANSMIT_SLOW_TMR);
            }
            BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                    &(pBfdSessEntry->MibObject.SessDbNode));
            BfdRedSyncDynInfo ();
        }
        else
        {
            u4Return = OSIX_FAILURE;
        }
        MemReleaseMemBlock (BFD_MPLSPATHINFOSTRUCT_POOLID,
                            (UINT1 *) pMplsPathInfo);
    }
    else
    {
        /* Received response for an already sent bootstrap
         * request */

        /* Checks */

        /* Get the session entry using the GetApi */
        pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId,
                                                pBfdLsppInfo->u4SessionIndex);
        if (NULL == pBfdSessEntry)
        {
            /* Entry is not existing */
            /* We assume here operator might have deleted the entry */
            BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_PROCESS_FAIL,
                     pBfdLsppInfo->u4BfdDiscriminator,
                     pBfdLsppInfo->u4SessionIndex, u4ContextId);
            return OSIX_FAILURE;
        }

        if (ACTIVE == pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus)
        {

            /* Check if session type is OOB or not
             * The response for a bootstrap should have been only 
             * received if this node would have triggered a bootstrap
             * Even if the operator meanwhile had changed the sessType 
             * by making NIS (which ideally should not have been done, 
             * the LSP Ping response is ignored */
            if (BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING !=
                pBfdSessEntry->MibObject.i4FsMIStdBfdSessType)
            {
                BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_NO_OOB,
                         pBfdLsppInfo->u4BfdDiscriminator,
                         pBfdLsppInfo->u4SessionIndex, u4ContextId);
                return OSIX_SUCCESS;
            }

            /* Check if we really are waiting for a response */
            if (OSIX_TRUE != pBfdSessEntry->bToTrigLsppBtStrap)
            {
                /* We already received a response */
                return OSIX_SUCCESS;
            }

            /* Check first if the bootstrap succeded or not */
            if (OSIX_FALSE == pBfdLsppInfo->u4ErrorCode)
            {
                /* Check and store the received discriminator */
                if (pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr == 0)
                {
                    pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr =
                        pBfdLsppInfo->u4BfdDiscriminator;
                    pBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr =
                        pBfdLsppInfo->u4BfdDiscriminator;

                    BFD_LOG (u4ContextId, BFD_TRC_DISCR_FROM_LSPP,
                             pBfdLsppInfo->u4BfdDiscriminator,
                             pBfdLsppInfo->u4SessionIndex, u4ContextId);
                    BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                            &(pBfdSessEntry->MibObject.
                                              SessDbNode));
                    BfdRedSyncDynInfo ();
                }
                else
                {
                    BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_DISCR_PRESENT,
                             pBfdLsppInfo->u4BfdDiscriminator,
                             pBfdLsppInfo->u4SessionIndex, u4ContextId);
                    return OSIX_SUCCESS;
                }
                /* Set the flag back to OSIX_FALSE as we are not waiting for
                 * a response anymore */
                /* We just dont set this flag in the else case below wherein we
                 * got an invalid response. The same flag will trigger the
                 * bootstrap request again */
                pBfdSessEntry->bToTrigLsppBtStrap = OSIX_FALSE;
            }
            else
            {
                /* Bootstrap request sent had a failure at egress */
                /* Raise trap */
                BFD_LOG (u4ContextId, BFD_TRC_LSPP_BSTRP_EGR_FAIL,
                         pBfdLsppInfo->u4BfdDiscriminator);
            }
        }
        else
        {
            u4Return = OSIX_FAILURE;
        }
    }

    return u4Return;
}

/****************************************************************************
 * Function    :  BfdBtStrTriggerOOBBtStrapReply
 * Description :  This function is used to reply for a bootstrap LSP Ping
 *                request
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 *                pLsppBfdInfo - pointer to LSPP info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
BfdBtStrTriggerOOBBtStrapReply (tBfdFsMIStdBfdSessEntry *
                                pBfdFsMIStdBfdSessInfo,
                                tBfdLsppInfo * pBfdLsppInfo)
{
    INT4                i4RetCode = OSIX_SUCCESS;
    tBfdExtInParams    *pBfdLsppExtInParams = NULL;
    tBfdExtOutParams   *pBfdLsppExtOutParams = NULL;
    tLsppPathId        *pLsppPathId = NULL;

    if ((NULL == pBfdFsMIStdBfdSessInfo) || (NULL == pBfdLsppInfo))
    {
        return OSIX_FAILURE;
    }

    /* Allocate memory from mempool */
    pBfdLsppExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdLsppExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessInfo->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdBtStrTriggerOOBBtStrapReply");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdLsppExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdLsppExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdLsppExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessInfo->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdLsppExtInParams);
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdBtStrTriggerOOBBtStrapReply");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdLsppExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdLsppExtInParams->u4ContextId =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId;

    pBfdLsppExtInParams->eExtReqType = BFD_REQ_LSPP_BOOTSTRAP_REPLY;

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.pBuf =
        pBfdLsppInfo->pBuf;

    /*To fill LSPP path info */
    pLsppPathId =
        &pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.PathId;

    pLsppPathId->u1PathType = (UINT1) pBfdLsppInfo->PathId.ePathType;

    switch (pBfdLsppInfo->PathId.ePathType)
    {
        case BFD_PATH_TYPE_NONTE_IPV4:
        case BFD_PATH_TYPE_NONTE_IPV6:
        {
            MEMCPY (&pLsppPathId->unPathId.LdpInfo.LdpPrefix,
                    &pBfdLsppInfo->PathId.SessNonTeParams.PeerAddr,
                    sizeof (pBfdLsppInfo->PathId.SessNonTeParams.PeerAddr));

            pLsppPathId->unPathId.LdpInfo.u4AddrType =
                pBfdLsppInfo->PathId.SessNonTeParams.u1AddrType;

            (pLsppPathId->unPathId.LdpInfo.u4PrefixLength) =
                pBfdLsppInfo->PathId.SessNonTeParams.u1AddrLen;
        }
            break;
        case BFD_PATH_TYPE_TE_IPV4:
        case BFD_PATH_TYPE_TE_IPV6:
        {
            MEMCPY (&pLsppPathId->unPathId.TnlInfo.SrcNodeId.MplsRouterId,
                    &pBfdLsppInfo->PathId.SessTeParams.SrcIpAddr,
                    sizeof (pBfdLsppInfo->PathId.SessTeParams.SrcIpAddr));

            MEMCPY (&pLsppPathId->unPathId.TnlInfo.DstNodeId.MplsRouterId,
                    &pBfdLsppInfo->PathId.SessTeParams.DstIpAddr,
                    sizeof (pBfdLsppInfo->PathId.SessTeParams.DstIpAddr));

            pLsppPathId->unPathId.TnlInfo.u4SrcTnlId =
                pBfdLsppInfo->PathId.SessTeParams.u4TunnelId;
            pLsppPathId->unPathId.TnlInfo.u4LspId =
                pBfdLsppInfo->PathId.SessTeParams.u4TunnelInst;
            pLsppPathId->unPathId.TnlInfo.SrcNodeId.u4NodeType =
                pBfdLsppInfo->PathId.SessTeParams.u4AddrType;
        }
            break;
        case BFD_PATH_TYPE_PW:
        {
            MEMCPY (&pLsppPathId->unPathId.PwInfo.PeerAddr,
                    &pBfdLsppInfo->PathId.SessPwParams.PeerAddr,
                    sizeof (pBfdLsppInfo->PathId.SessPwParams.PeerAddr));

            pLsppPathId->unPathId.PwInfo.u4PwVcId =
                pBfdLsppInfo->PathId.SessPwParams.u4VcId;

            pLsppPathId->u1PathType = (UINT1) BFD_LSPP_PATH_TYPE_MEP;
        }
            break;
        case BFD_PATH_TYPE_MEP:
        {
            pLsppPathId->unPathId.MepIndices.u4MegIndex =
                pBfdLsppInfo->PathId.SessMeParams.u4MegId;

            pLsppPathId->unPathId.MepIndices.u4MeIndex =
                pBfdLsppInfo->PathId.SessMeParams.u4MeId;

            pLsppPathId->unPathId.MepIndices.u4MpIndex =
                pBfdLsppInfo->PathId.SessMeParams.u4MpId;
        }
            break;
        case BFD_PATH_TYPE_IPV4:
        case BFD_PATH_TYPE_IPV6:
        default:
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdLsppExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdLsppExtOutParams);
            return OSIX_FAILURE;
    }

    MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.PathId,
            pLsppPathId, sizeof (tLsppPathId));

    MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.
            DestIpAddr, &pBfdLsppInfo->DestIpAddr,
            sizeof (pBfdLsppInfo->DestIpAddr));

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.u4OffSet =
        pBfdLsppInfo->u4OffSet;
    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.u4SrcUdpPort =
        pBfdLsppInfo->u4SrcUdpPort;
    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.u4OutIfIndex =
        pBfdLsppInfo->u4OutIfIndex;
    pBfdLsppExtInParams->LsppReqParams.unMsgParam.
        LsppBfdInfo.u4BfdDiscriminator = BfdSessGetDiscriminator
        (pBfdFsMIStdBfdSessInfo);
    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.u4SessionIndex =
        pBfdLsppInfo->u4SessionIndex;

    MEMCPY (pBfdLsppExtInParams->LsppReqParams.
            unMsgParam.LsppBfdInfo.au1NextHopMac,
            pBfdLsppInfo->au1NextHopMac, sizeof (pBfdLsppInfo->au1NextHopMac));

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.u1ReplyPath =
        pBfdLsppInfo->u1ReplyPath;

    MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.
            LsppBfdInfo.NextHopIpAddrType, &pBfdLsppInfo->NextHopIpAddrType,
            sizeof (pBfdLsppInfo->NextHopIpAddrType));

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppBfdInfo.bIsEgress =
        pBfdLsppInfo->bIsEgress;

    /* Call BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdLsppExtInParams, pBfdLsppExtOutParams)
        != OSIX_SUCCESS)
    {

        BFD_LOG (pBfdLsppExtInParams->u4ContextId, BFD_TRC_POST_EVENT_FAILED,
                 pBfdLsppExtInParams->u4ContextId,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex);
        i4RetCode = OSIX_FAILURE;
    }
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdLsppExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                        (UINT1 *) pBfdLsppExtOutParams);
    return i4RetCode;
}

/***************************************************************************
 * Function    :  BfdBtStrTriggerOOBBtStrap
 * Description :  This function is used to bootstrap BFD session using LSP Ping
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdBtStrTriggerOOBBtStrap (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo)
{
    UINT1               u1RetCode = OSIX_SUCCESS;
    tBfdExtInParams    *pBfdLsppExtInParams = NULL;
    tBfdExtOutParams   *pBfdLsppExtOutParams = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTableEntry = NULL;

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    /* Allocate memory from mempool */
    pBfdLsppExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdLsppExtInParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessInfo->MibObject.
                                  u4FsMIStdBfdContextId);
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdBtStrTriggerOOBBtStrap");
        return OSIX_FAILURE;
    }
    MEMSET (pBfdLsppExtInParams, 0, sizeof (tBfdExtInParams));

    pBfdLsppExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdLsppExtOutParams == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdFsMIStdBfdSessInfo->MibObject.
                                  u4FsMIStdBfdContextId);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdLsppExtInParams);
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD",
                 "BfdBtStrTriggerOOBBtStrap");
        return OSIX_FAILURE;
    }

    pBfdGlobalConfigTableEntry = BfdUtilGetBfdGlobalConfigTable
        (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);

    if (pBfdGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                            (UINT1 *) pBfdLsppExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdLsppExtOutParams);
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED);
        return OSIX_FAILURE;
    }

    /*Fill the WFR interval for LSPP */
    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
        BfdBtStrapInfo.u4WFRInterval =
        pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl;

    MEMSET (pBfdLsppExtOutParams, 0, sizeof (tBfdExtOutParams));

    pBfdLsppExtInParams->u4ContextId =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId;

    pBfdLsppExtInParams->eExtReqType = BFD_REQ_LSPP_BOOTSTRAP_MSG;

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.LsppPathId.
        u1PathType =
        (UINT1) pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessMapType;

    switch (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessMapType)
    {
        case BFD_PATH_TYPE_NONTE_IPV4:
        case BFD_PATH_TYPE_NONTE_IPV6:
        {
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.LdpInfo.u4AddrType =
                (UINT4) pBfdFsMIStdBfdSessInfo->BfdSessPathParams.
                SessNonTeParams.u1AddrType;

            MEMCPY (&(pBfdLsppExtInParams->LsppReqParams.unMsgParam.
                      LsppExtTrigInfo.LsppPathId.
                      unPathId.LdpInfo.LdpPrefix),
                    &pBfdFsMIStdBfdSessInfo->BfdSessPathParams.
                    SessNonTeParams.PeerAddr, sizeof (tIpAddr));

            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.LdpInfo.u4PrefixLength =
                (UINT4) pBfdFsMIStdBfdSessInfo->BfdSessPathParams.
                SessNonTeParams.u1AddrLen;
        }
            break;
        case BFD_PATH_TYPE_TE_IPV4:
        case BFD_PATH_TYPE_TE_IPV6:
        {
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.TnlInfo.SrcNodeId.u4NodeType =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                u4AddrType;
            MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.
                    LsppExtTrigInfo.LsppPathId.unPathId.TnlInfo.SrcNodeId.
                    MplsRouterId,
                    &pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                    SrcIpAddr, sizeof (tIpAddr));

            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.TnlInfo.DstNodeId.u4NodeType =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                u4AddrType;

            MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.
                    LsppExtTrigInfo.LsppPathId.
                    unPathId.TnlInfo.DstNodeId.MplsRouterId,
                    &pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                    DstIpAddr, sizeof (tIpAddr));

            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.TnlInfo.u4SrcTnlId =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                u4TunnelId;
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.TnlInfo.u4LspId =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessTeParams.
                u4TunnelInst;
        }
            break;
        case BFD_PATH_TYPE_PW:
        {
            MEMCPY (&pBfdLsppExtInParams->LsppReqParams.unMsgParam.
                    LsppExtTrigInfo.LsppPathId.
                    unPathId.PwInfo.PeerAddr,
                    &pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessPwParams.
                    PeerAddr, sizeof (tIpAddr));

            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.PwInfo.u4PwVcId =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessPwParams.u4VcId;
        }
            break;
        case BFD_PATH_TYPE_MEP:
        {
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.MepIndices.u4MegIndex =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessMeParams.u4MegId;
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.MepIndices.u4MeIndex =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessMeParams.u4MeId;
            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.unPathId.MepIndices.u4MpIndex =
                pBfdFsMIStdBfdSessInfo->BfdSessPathParams.SessMeParams.u4MpId;

            pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
                LsppPathId.u1PathType = (UINT1) BFD_LSPP_PATH_TYPE_MEP;

        }
            break;
        default:
        {
            MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID,
                                (UINT1 *) pBfdLsppExtInParams);
            MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                                (UINT1 *) pBfdLsppExtOutParams);
            return OSIX_FAILURE;
        }
    }

    /* Fill the encap to be used */
    if (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessEncapType ==
        BFD_ENCAP_TYPE_MPLS_IP)
    {
        pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
            BfdBtStrapInfo.u4LsppEncapType = LSPP_ENCAP_TYPE_IP;
    }
    else if (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_MPLS_ACH)
    {
        pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
            BfdBtStrapInfo.u4LsppEncapType = LSPP_ENCAP_TYPE_ACH;
    }
    else if (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_MPLS_IP_ACH)
    {
        pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
            BfdBtStrapInfo.u4LsppEncapType = LSPP_ENCAP_TYPE_ACH_IP;
    }
    else if (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIBfdSessEncapType ==
             BFD_ENCAP_TYPE_VCCV_NEGOTIATED)
    {
        pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
            BfdBtStrapInfo.u4LsppEncapType = LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;
    }

    /* Fetch discriminator : We use BFD session index as "My Discriminator" for
     * that session. Function is provided so as to implement discriminator
     * generation logic, if needed, in the future */

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
        BfdBtStrapInfo.u4BfdDiscriminator =
        BfdSessGetDiscriminator (pBfdFsMIStdBfdSessInfo);

    pBfdLsppExtInParams->LsppReqParams.unMsgParam.LsppExtTrigInfo.
        BfdBtStrapInfo.u4BfdSessionIndex =
        pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex;

    /* Call BFD exit function */
    if (BfdPortHandleExtInteraction (pBfdLsppExtInParams, pBfdLsppExtOutParams)
        != OSIX_SUCCESS)
    {

        BFD_LOG (pBfdLsppExtInParams->u4ContextId, BFD_TRC_POST_EVENT_FAILED,
                 pBfdLsppExtInParams->u4ContextId,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex);
        u1RetCode = OSIX_FAILURE;
    }

    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdLsppExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                        (UINT1 *) pBfdLsppExtOutParams);
    return u1RetCode;
}

#endif /* _BFDBTSTR_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdbtstr.c                     */
/*-----------------------------------------------------------------------*/
