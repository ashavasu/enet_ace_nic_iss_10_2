/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbfdlwg.c,v 1.2 2010/10/22 10:27:37 prabuc Exp $
*
* Description:  Low Level GET Routine for All Objects
*********************************************************************/
#include "bfdinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdAdminStatus
 Input       :  The Indices

                The Object 
                retValBfdAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdAdminStatus (INT4 *pi4RetValBfdAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhGetFsMIStdBfdAdminStatus (0, pi4RetValBfdAdminStatus);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetBfdAdminStatus
 Input       :  The Indices

                The Object 
                setValBfdAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdAdminStatus (INT4 i4SetValBfdAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhSetFsMIStdBfdAdminStatus (0, i4SetValBfdAdminStatus);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2BfdAdminStatus
 Input       :  The Indices

                The Object 
                testValBfdAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdAdminStatus (UINT4 *pu4ErrorCode, INT4 i4TestValBfdAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdAdminStatus (pu4ErrorCode, 0,
                                        i4TestValBfdAdminStatus);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2BfdAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2BfdAdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdSessNotificationsEnable
 Input       :  The Indices

                The Object 
                retValBfdSessNotificationsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessNotificationsEnable (INT4 *pi4RetValBfdSessNotificationsEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessNotificationsEnable (0,
                                                 pi4RetValBfdSessNotificationsEnable);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetBfdSessNotificationsEnable
 Input       :  The Indices

                The Object 
                setValBfdSessNotificationsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessNotificationsEnable (INT4 i4SetValBfdSessNotificationsEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessNotificationsEnable (0,
                                                 i4SetValBfdSessNotificationsEnable);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2BfdSessNotificationsEnable
 Input       :  The Indices

                The Object 
                testValBfdSessNotificationsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessNotificationsEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValBfdSessNotificationsEnable)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessNotificationsEnable (pu4ErrorCode, 0,
                                                    i4TestValBfdSessNotificationsEnable);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2BfdSessNotificationsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2BfdSessNotificationsEnable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBfdSessTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceBfdSessTable (UINT4 u4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return = nmhValidateIndexInstanceFsMIStdBfdSessTable (0, u4BfdSessIndex);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBfdSessTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBfdSessTable (UINT4 *pu4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIStdBfdSessTable (&u4FirstContextId,
                                             pu4BfdSessIndex);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBfdSessTable
 Input       :  The Indices
                BfdSessIndex
                nextBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBfdSessTable (UINT4 u4BfdSessIndex, UINT4 *pu4NextBfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIStdBfdSessTable (0, &u4NextContextId, u4BfdSessIndex,
                                            pu4NextBfdSessIndex);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdSessVersionNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessVersionNumber (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValBfdSessVersionNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessVersionNumber (0, u4BfdSessIndex,
                                           pu4RetValBfdSessVersionNumber);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessType (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessType (0, u4BfdSessIndex, pi4RetValBfdSessType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDiscriminator
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDiscriminator
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDiscriminator (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValBfdSessDiscriminator)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDiscriminator (0, u4BfdSessIndex,
                                           pu4RetValBfdSessDiscriminator);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessRemoteDiscr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessRemoteDiscr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessRemoteDiscr (UINT4 u4BfdSessIndex,
                          UINT4 *pu4RetValBfdSessRemoteDiscr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessRemoteDiscr (0, u4BfdSessIndex,
                                         pu4RetValBfdSessRemoteDiscr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDestinationUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDestinationUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDestinationUdpPort (UINT4 u4BfdSessIndex,
                                 UINT4 *pu4RetValBfdSessDestinationUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDestinationUdpPort (0, u4BfdSessIndex,
                                                pu4RetValBfdSessDestinationUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessSourceUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessSourceUdpPort (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValBfdSessSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessSourceUdpPort (0, u4BfdSessIndex,
                                           pu4RetValBfdSessSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessEchoSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessEchoSourceUdpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessEchoSourceUdpPort (UINT4 u4BfdSessIndex,
                                UINT4 *pu4RetValBfdSessEchoSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessEchoSourceUdpPort (0, u4BfdSessIndex,
                                               pu4RetValBfdSessEchoSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessAdminStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessAdminStatus (UINT4 u4BfdSessIndex,
                          INT4 *pi4RetValBfdSessAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessAdminStatus (0, u4BfdSessIndex,
                                         pi4RetValBfdSessAdminStatus);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessState
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessState (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessState)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessState (0, u4BfdSessIndex, pi4RetValBfdSessState);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessRemoteHeardFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessRemoteHeardFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessRemoteHeardFlag (UINT4 u4BfdSessIndex,
                              INT4 *pi4RetValBfdSessRemoteHeardFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessRemoteHeardFlag (0, u4BfdSessIndex,
                                             pi4RetValBfdSessRemoteHeardFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDiag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDiag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDiag (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessDiag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDiag (0, u4BfdSessIndex, pi4RetValBfdSessDiag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessOperMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessOperMode (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessOperMode (0, u4BfdSessIndex,
                                      pi4RetValBfdSessOperMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDemandModeDesiredFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDemandModeDesiredFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDemandModeDesiredFlag (UINT4 u4BfdSessIndex,
                                    INT4 *pi4RetValBfdSessDemandModeDesiredFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDemandModeDesiredFlag (0, u4BfdSessIndex,
                                                   pi4RetValBfdSessDemandModeDesiredFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessControlPlaneIndepFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessControlPlaneIndepFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessControlPlaneIndepFlag (UINT4 u4BfdSessIndex,
                                    INT4 *pi4RetValBfdSessControlPlaneIndepFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessControlPlaneIndepFlag (0, u4BfdSessIndex,
                                                   pi4RetValBfdSessControlPlaneIndepFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessMultipointFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessMultipointFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessMultipointFlag (UINT4 u4BfdSessIndex,
                             INT4 *pi4RetValBfdSessMultipointFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessMultipointFlag (0, u4BfdSessIndex,
                                            pi4RetValBfdSessMultipointFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessInterface
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessInterface (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessInterface)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessInterface (0, u4BfdSessIndex,
                                       pi4RetValBfdSessInterface);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessSrcAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessSrcAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessSrcAddrType (UINT4 u4BfdSessIndex,
                          INT4 *pi4RetValBfdSessSrcAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessSrcAddrType (0, u4BfdSessIndex,
                                         pi4RetValBfdSessSrcAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessSrcAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessSrcAddr (UINT4 u4BfdSessIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValBfdSessSrcAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessSrcAddr (0, u4BfdSessIndex, pRetValBfdSessSrcAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDstAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDstAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDstAddrType (UINT4 u4BfdSessIndex,
                          INT4 *pi4RetValBfdSessDstAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDstAddrType (0, u4BfdSessIndex,
                                         pi4RetValBfdSessDstAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDstAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDstAddr (UINT4 u4BfdSessIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDstAddr (0, u4BfdSessIndex, pRetValBfdSessDstAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessGTSM
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessGTSM
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessGTSM (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessGTSM)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessGTSM (0, u4BfdSessIndex, pi4RetValBfdSessGTSM);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessGTSMTTL
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessGTSMTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessGTSMTTL (UINT4 u4BfdSessIndex, UINT4 *pu4RetValBfdSessGTSMTTL)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessGTSMTTL (0, u4BfdSessIndex,
                                     pu4RetValBfdSessGTSMTTL);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDesiredMinTxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDesiredMinTxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDesiredMinTxInterval (UINT4 u4BfdSessIndex,
                                   UINT4 *pu4RetValBfdSessDesiredMinTxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDesiredMinTxInterval (0, u4BfdSessIndex,
                                                  pu4RetValBfdSessDesiredMinTxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessReqMinRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessReqMinRxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessReqMinRxInterval (UINT4 u4BfdSessIndex,
                               UINT4 *pu4RetValBfdSessReqMinRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessReqMinRxInterval (0, u4BfdSessIndex,
                                              pu4RetValBfdSessReqMinRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessReqMinEchoRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessReqMinEchoRxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessReqMinEchoRxInterval (UINT4 u4BfdSessIndex,
                                   UINT4 *pu4RetValBfdSessReqMinEchoRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessReqMinEchoRxInterval (0, u4BfdSessIndex,
                                                  pu4RetValBfdSessReqMinEchoRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessDetectMult
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDetectMult (UINT4 u4BfdSessIndex,
                         UINT4 *pu4RetValBfdSessDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDetectMult (0, u4BfdSessIndex,
                                        pu4RetValBfdSessDetectMult);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessNegotiatedInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessNegotiatedInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessNegotiatedInterval (UINT4 u4BfdSessIndex,
                                 UINT4 *pu4RetValBfdSessNegotiatedInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessNegotiatedInterval (0, u4BfdSessIndex,
                                                pu4RetValBfdSessNegotiatedInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessNegotiatedEchoInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessNegotiatedEchoInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessNegotiatedEchoInterval (UINT4 u4BfdSessIndex,
                                     UINT4
                                     *pu4RetValBfdSessNegotiatedEchoInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessNegotiatedEchoInterval (0, u4BfdSessIndex,
                                                    pu4RetValBfdSessNegotiatedEchoInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessNegotiatedDetectMult
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessNegotiatedDetectMult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessNegotiatedDetectMult (UINT4 u4BfdSessIndex,
                                   UINT4 *pu4RetValBfdSessNegotiatedDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessNegotiatedDetectMult (0, u4BfdSessIndex,
                                                  pu4RetValBfdSessNegotiatedDetectMult);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessAuthPresFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessAuthPresFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessAuthPresFlag (UINT4 u4BfdSessIndex,
                           INT4 *pi4RetValBfdSessAuthPresFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessAuthPresFlag (0, u4BfdSessIndex,
                                          pi4RetValBfdSessAuthPresFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessAuthenticationType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessAuthenticationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessAuthenticationType (UINT4 u4BfdSessIndex,
                                 INT4 *pi4RetValBfdSessAuthenticationType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessAuthenticationType (0, u4BfdSessIndex,
                                                pi4RetValBfdSessAuthenticationType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessAuthenticationKeyID
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessAuthenticationKeyID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessAuthenticationKeyID (UINT4 u4BfdSessIndex,
                                  INT4 *pi4RetValBfdSessAuthenticationKeyID)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessAuthenticationKeyID (0, u4BfdSessIndex,
                                                 pi4RetValBfdSessAuthenticationKeyID);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessAuthenticationKey
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessAuthenticationKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessAuthenticationKey (UINT4 u4BfdSessIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValBfdSessAuthenticationKey)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessAuthenticationKey (0, u4BfdSessIndex,
                                               pRetValBfdSessAuthenticationKey);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessStorType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessStorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessStorType (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessStorType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessStorType (0, u4BfdSessIndex,
                                      pi4RetValBfdSessStorType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessRowStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessRowStatus (UINT4 u4BfdSessIndex, INT4 *pi4RetValBfdSessRowStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessRowStatus (0, u4BfdSessIndex,
                                       pi4RetValBfdSessRowStatus);
    return i4Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetBfdSessVersionNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessVersionNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessVersionNumber (UINT4 u4BfdSessIndex,
                            UINT4 u4SetValBfdSessVersionNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessVersionNumber (0, u4BfdSessIndex,
                                           u4SetValBfdSessVersionNumber);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessType (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessType (0, u4BfdSessIndex, i4SetValBfdSessType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDestinationUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDestinationUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDestinationUdpPort (UINT4 u4BfdSessIndex,
                                 UINT4 u4SetValBfdSessDestinationUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDestinationUdpPort (0, u4BfdSessIndex,
                                                u4SetValBfdSessDestinationUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessSourceUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessSourceUdpPort (UINT4 u4BfdSessIndex,
                            UINT4 u4SetValBfdSessSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessSourceUdpPort (0, u4BfdSessIndex,
                                           u4SetValBfdSessSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessEchoSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessEchoSourceUdpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessEchoSourceUdpPort (UINT4 u4BfdSessIndex,
                                UINT4 u4SetValBfdSessEchoSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessEchoSourceUdpPort (0, u4BfdSessIndex,
                                               u4SetValBfdSessEchoSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessAdminStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessAdminStatus (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessAdminStatus (0, u4BfdSessIndex,
                                         i4SetValBfdSessAdminStatus);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessOperMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessOperMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessOperMode (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessOperMode (0, u4BfdSessIndex,
                                      i4SetValBfdSessOperMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDemandModeDesiredFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDemandModeDesiredFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDemandModeDesiredFlag (UINT4 u4BfdSessIndex,
                                    INT4 i4SetValBfdSessDemandModeDesiredFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDemandModeDesiredFlag (0, u4BfdSessIndex,
                                                   i4SetValBfdSessDemandModeDesiredFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessControlPlaneIndepFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessControlPlaneIndepFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessControlPlaneIndepFlag (UINT4 u4BfdSessIndex,
                                    INT4 i4SetValBfdSessControlPlaneIndepFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessControlPlaneIndepFlag (0, u4BfdSessIndex,
                                                   i4SetValBfdSessControlPlaneIndepFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessMultipointFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessMultipointFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessMultipointFlag (UINT4 u4BfdSessIndex,
                             INT4 i4SetValBfdSessMultipointFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessMultipointFlag (0, u4BfdSessIndex,
                                            i4SetValBfdSessMultipointFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessInterface
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessInterface (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessInterface)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessInterface (0, u4BfdSessIndex,
                                       i4SetValBfdSessInterface);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessSrcAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessSrcAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessSrcAddrType (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessSrcAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessSrcAddrType (0, u4BfdSessIndex,
                                         i4SetValBfdSessSrcAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessSrcAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessSrcAddr (UINT4 u4BfdSessIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValBfdSessSrcAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessSrcAddr (0, u4BfdSessIndex, pSetValBfdSessSrcAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDstAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDstAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDstAddrType (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessDstAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDstAddrType (0, u4BfdSessIndex,
                                         i4SetValBfdSessDstAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDstAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDstAddr (UINT4 u4BfdSessIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDstAddr (0, u4BfdSessIndex, pSetValBfdSessDstAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessGTSM
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessGTSM
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessGTSM (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessGTSM)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessGTSM (0, u4BfdSessIndex, i4SetValBfdSessGTSM);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessGTSMTTL
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessGTSMTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessGTSMTTL (UINT4 u4BfdSessIndex, UINT4 u4SetValBfdSessGTSMTTL)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessGTSMTTL (0, u4BfdSessIndex, u4SetValBfdSessGTSMTTL);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDesiredMinTxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDesiredMinTxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDesiredMinTxInterval (UINT4 u4BfdSessIndex,
                                   UINT4 u4SetValBfdSessDesiredMinTxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDesiredMinTxInterval (0, u4BfdSessIndex,
                                                  u4SetValBfdSessDesiredMinTxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessReqMinRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessReqMinRxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessReqMinRxInterval (UINT4 u4BfdSessIndex,
                               UINT4 u4SetValBfdSessReqMinRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessReqMinRxInterval (0, u4BfdSessIndex,
                                              u4SetValBfdSessReqMinRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessReqMinEchoRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessReqMinEchoRxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessReqMinEchoRxInterval (UINT4 u4BfdSessIndex,
                                   UINT4 u4SetValBfdSessReqMinEchoRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessReqMinEchoRxInterval (0, u4BfdSessIndex,
                                                  u4SetValBfdSessReqMinEchoRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessDetectMult
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessDetectMult
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessDetectMult (UINT4 u4BfdSessIndex, UINT4 u4SetValBfdSessDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessDetectMult (0, u4BfdSessIndex,
                                        u4SetValBfdSessDetectMult);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessAuthPresFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessAuthPresFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessAuthPresFlag (UINT4 u4BfdSessIndex,
                           INT4 i4SetValBfdSessAuthPresFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessAuthPresFlag (0, u4BfdSessIndex,
                                          i4SetValBfdSessAuthPresFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessAuthenticationType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessAuthenticationType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessAuthenticationType (UINT4 u4BfdSessIndex,
                                 INT4 i4SetValBfdSessAuthenticationType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessAuthenticationType (0, u4BfdSessIndex,
                                                i4SetValBfdSessAuthenticationType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessAuthenticationKeyID
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessAuthenticationKeyID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessAuthenticationKeyID (UINT4 u4BfdSessIndex,
                                  INT4 i4SetValBfdSessAuthenticationKeyID)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessAuthenticationKeyID (0, u4BfdSessIndex,
                                                 i4SetValBfdSessAuthenticationKeyID);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessAuthenticationKey
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessAuthenticationKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessAuthenticationKey (UINT4 u4BfdSessIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValBfdSessAuthenticationKey)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessAuthenticationKey (0, u4BfdSessIndex,
                                               pSetValBfdSessAuthenticationKey);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessStorType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessStorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessStorType (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessStorType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessStorType (0, u4BfdSessIndex,
                                      i4SetValBfdSessStorType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhSetBfdSessRowStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                setValBfdSessRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBfdSessRowStatus (UINT4 u4BfdSessIndex, INT4 i4SetValBfdSessRowStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhSetFsMIStdBfdSessRowStatus (0, u4BfdSessIndex,
                                       i4SetValBfdSessRowStatus);
    return i4Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2BfdSessVersionNumber
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessVersionNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessVersionNumber (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                               UINT4 u4TestValBfdSessVersionNumber)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessVersionNumber (pu4ErrorCode, 0, u4BfdSessIndex,
                                              u4TestValBfdSessVersionNumber);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                      INT4 i4TestValBfdSessType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessType (pu4ErrorCode, 0, u4BfdSessIndex,
                                     i4TestValBfdSessType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDestinationUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDestinationUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDestinationUdpPort (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                    UINT4 u4TestValBfdSessDestinationUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDestinationUdpPort (pu4ErrorCode, 0,
                                                   u4BfdSessIndex,
                                                   u4TestValBfdSessDestinationUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessSourceUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessSourceUdpPort (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                               UINT4 u4TestValBfdSessSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessSourceUdpPort (pu4ErrorCode, 0, u4BfdSessIndex,
                                              u4TestValBfdSessSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessEchoSourceUdpPort
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessEchoSourceUdpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessEchoSourceUdpPort (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                   UINT4 u4TestValBfdSessEchoSourceUdpPort)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessEchoSourceUdpPort (pu4ErrorCode, 0,
                                                  u4BfdSessIndex,
                                                  u4TestValBfdSessEchoSourceUdpPort);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessAdminStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessAdminStatus (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                             INT4 i4TestValBfdSessAdminStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessAdminStatus (pu4ErrorCode, 0, u4BfdSessIndex,
                                            i4TestValBfdSessAdminStatus);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessOperMode
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessOperMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessOperMode (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                          INT4 i4TestValBfdSessOperMode)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessOperMode (pu4ErrorCode, 0, u4BfdSessIndex,
                                         i4TestValBfdSessOperMode);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDemandModeDesiredFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDemandModeDesiredFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDemandModeDesiredFlag (UINT4 *pu4ErrorCode,
                                       UINT4 u4BfdSessIndex,
                                       INT4
                                       i4TestValBfdSessDemandModeDesiredFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDemandModeDesiredFlag (pu4ErrorCode, 0,
                                                      u4BfdSessIndex,
                                                      i4TestValBfdSessDemandModeDesiredFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessControlPlaneIndepFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessControlPlaneIndepFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessControlPlaneIndepFlag (UINT4 *pu4ErrorCode,
                                       UINT4 u4BfdSessIndex,
                                       INT4
                                       i4TestValBfdSessControlPlaneIndepFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessControlPlaneIndepFlag (pu4ErrorCode, 0,
                                                      u4BfdSessIndex,
                                                      i4TestValBfdSessControlPlaneIndepFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessMultipointFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessMultipointFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessMultipointFlag (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                INT4 i4TestValBfdSessMultipointFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessMultipointFlag (pu4ErrorCode, 0, u4BfdSessIndex,
                                               i4TestValBfdSessMultipointFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessInterface
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessInterface (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                           INT4 i4TestValBfdSessInterface)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessInterface (pu4ErrorCode, 0, u4BfdSessIndex,
                                          i4TestValBfdSessInterface);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessSrcAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessSrcAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessSrcAddrType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                             INT4 i4TestValBfdSessSrcAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessSrcAddrType (pu4ErrorCode, 0, u4BfdSessIndex,
                                            i4TestValBfdSessSrcAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessSrcAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessSrcAddr (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValBfdSessSrcAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessSrcAddr (pu4ErrorCode, 0, u4BfdSessIndex,
                                        pTestValBfdSessSrcAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDstAddrType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDstAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDstAddrType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                             INT4 i4TestValBfdSessDstAddrType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDstAddrType (pu4ErrorCode, 0, u4BfdSessIndex,
                                            i4TestValBfdSessDstAddrType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDstAddr
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDstAddr (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                         tSNMP_OCTET_STRING_TYPE * pTestValBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDstAddr (pu4ErrorCode, 0, u4BfdSessIndex,
                                        pTestValBfdSessDstAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessGTSM
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessGTSM
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessGTSM (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                      INT4 i4TestValBfdSessGTSM)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessGTSM (pu4ErrorCode, 0, u4BfdSessIndex,
                                     i4TestValBfdSessGTSM);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessGTSMTTL
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessGTSMTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessGTSMTTL (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                         UINT4 u4TestValBfdSessGTSMTTL)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessGTSMTTL (pu4ErrorCode, 0, u4BfdSessIndex,
                                        u4TestValBfdSessGTSMTTL);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDesiredMinTxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDesiredMinTxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDesiredMinTxInterval (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                      UINT4
                                      u4TestValBfdSessDesiredMinTxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDesiredMinTxInterval (pu4ErrorCode, 0,
                                                     u4BfdSessIndex,
                                                     u4TestValBfdSessDesiredMinTxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessReqMinRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessReqMinRxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessReqMinRxInterval (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                  UINT4 u4TestValBfdSessReqMinRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessReqMinRxInterval (pu4ErrorCode, 0,
                                                 u4BfdSessIndex,
                                                 u4TestValBfdSessReqMinRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessReqMinEchoRxInterval
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessReqMinEchoRxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessReqMinEchoRxInterval (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                      UINT4
                                      u4TestValBfdSessReqMinEchoRxInterval)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessReqMinEchoRxInterval (pu4ErrorCode, 0,
                                                     u4BfdSessIndex,
                                                     u4TestValBfdSessReqMinEchoRxInterval);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessDetectMult
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessDetectMult
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessDetectMult (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                            UINT4 u4TestValBfdSessDetectMult)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessDetectMult (pu4ErrorCode, 0, u4BfdSessIndex,
                                           u4TestValBfdSessDetectMult);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessAuthPresFlag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessAuthPresFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessAuthPresFlag (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                              INT4 i4TestValBfdSessAuthPresFlag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessAuthPresFlag (pu4ErrorCode, 0, u4BfdSessIndex,
                                             i4TestValBfdSessAuthPresFlag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessAuthenticationType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessAuthenticationType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessAuthenticationType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                    INT4 i4TestValBfdSessAuthenticationType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessAuthenticationType (pu4ErrorCode, 0,
                                                   u4BfdSessIndex,
                                                   i4TestValBfdSessAuthenticationType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessAuthenticationKeyID
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessAuthenticationKeyID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessAuthenticationKeyID (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                     INT4 i4TestValBfdSessAuthenticationKeyID)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessAuthenticationKeyID (pu4ErrorCode, 0,
                                                    u4BfdSessIndex,
                                                    i4TestValBfdSessAuthenticationKeyID);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessAuthenticationKey
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessAuthenticationKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessAuthenticationKey (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValBfdSessAuthenticationKey)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessAuthenticationKey (pu4ErrorCode, 0,
                                                  u4BfdSessIndex,
                                                  pTestValBfdSessAuthenticationKey);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessStorType
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessStorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessStorType (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                          INT4 i4TestValBfdSessStorType)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessStorType (pu4ErrorCode, 0, u4BfdSessIndex,
                                         i4TestValBfdSessStorType);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhTestv2BfdSessRowStatus
 Input       :  The Indices
                BfdSessIndex

                The Object 
                testValBfdSessRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BfdSessRowStatus (UINT4 *pu4ErrorCode, UINT4 u4BfdSessIndex,
                           INT4 i4TestValBfdSessRowStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhTestv2FsMIStdBfdSessRowStatus (pu4ErrorCode, 0, u4BfdSessIndex,
                                          i4TestValBfdSessRowStatus);
    return i4Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2BfdSessTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2BfdSessTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceBfdSessPerfTable (UINT4 u4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhValidateIndexInstanceFsMIStdBfdSessPerfTable (0, u4BfdSessIndex);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBfdSessPerfTable (UINT4 *pu4BfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIStdBfdSessPerfTable (&u4FirstContextId,
                                                 pu4BfdSessIndex);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBfdSessPerfTable
 Input       :  The Indices
                BfdSessIndex
                nextBfdSessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBfdSessPerfTable (UINT4 u4BfdSessIndex,
                                 UINT4 *pu4NextBfdSessIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIStdBfdSessPerfTable (0, &u4NextContextId,
                                                u4BfdSessIndex,
                                                pu4NextBfdSessIndex);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktIn
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktIn (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValBfdSessPerfCtrlPktIn)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktIn (0, u4BfdSessIndex,
                                           pu4RetValBfdSessPerfCtrlPktIn);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktOut
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktOut (UINT4 u4BfdSessIndex,
                             UINT4 *pu4RetValBfdSessPerfCtrlPktOut)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktOut (0, u4BfdSessIndex,
                                            pu4RetValBfdSessPerfCtrlPktOut);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktDrop
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktDrop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktDrop (UINT4 u4BfdSessIndex,
                              UINT4 *pu4RetValBfdSessPerfCtrlPktDrop)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktDrop (0, u4BfdSessIndex,
                                             pu4RetValBfdSessPerfCtrlPktDrop);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktDropLastTime
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktDropLastTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktDropLastTime (UINT4 u4BfdSessIndex,
                                      UINT4
                                      *pu4RetValBfdSessPerfCtrlPktDropLastTime)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktDropLastTime (0, u4BfdSessIndex,
                                                     pu4RetValBfdSessPerfCtrlPktDropLastTime);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktIn
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktIn (UINT4 u4BfdSessIndex,
                            UINT4 *pu4RetValBfdSessPerfEchoPktIn)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktIn (0, u4BfdSessIndex,
                                           pu4RetValBfdSessPerfEchoPktIn);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktOut
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktOut (UINT4 u4BfdSessIndex,
                             UINT4 *pu4RetValBfdSessPerfEchoPktOut)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktOut (0, u4BfdSessIndex,
                                            pu4RetValBfdSessPerfEchoPktOut);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktDrop
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktDrop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktDrop (UINT4 u4BfdSessIndex,
                              UINT4 *pu4RetValBfdSessPerfEchoPktDrop)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktDrop (0, u4BfdSessIndex,
                                             pu4RetValBfdSessPerfEchoPktDrop);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktDropLastTime
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktDropLastTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktDropLastTime (UINT4 u4BfdSessIndex,
                                      UINT4
                                      *pu4RetValBfdSessPerfEchoPktDropLastTime)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktDropLastTime (0, u4BfdSessIndex,
                                                     pu4RetValBfdSessPerfEchoPktDropLastTime);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessUpTime
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessUpTime (UINT4 u4BfdSessIndex, UINT4 *pu4RetValBfdSessUpTime)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessUpTime (0, u4BfdSessIndex, pu4RetValBfdSessUpTime);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfLastSessDownTime
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfLastSessDownTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfLastSessDownTime (UINT4 u4BfdSessIndex,
                                   UINT4 *pu4RetValBfdSessPerfLastSessDownTime)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfLastSessDownTime (0, u4BfdSessIndex,
                                                  pu4RetValBfdSessPerfLastSessDownTime);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfLastCommLostDiag
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfLastCommLostDiag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfLastCommLostDiag (UINT4 u4BfdSessIndex,
                                   INT4 *pi4RetValBfdSessPerfLastCommLostDiag)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfLastCommLostDiag (0, u4BfdSessIndex,
                                                  pi4RetValBfdSessPerfLastCommLostDiag);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfSessUpCount
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfSessUpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfSessUpCount (UINT4 u4BfdSessIndex,
                              UINT4 *pu4RetValBfdSessPerfSessUpCount)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfSessUpCount (0, u4BfdSessIndex,
                                             pu4RetValBfdSessPerfSessUpCount);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfDiscTime
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfDiscTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfDiscTime (UINT4 u4BfdSessIndex,
                           UINT4 *pu4RetValBfdSessPerfDiscTime)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfDiscTime (0, u4BfdSessIndex,
                                          pu4RetValBfdSessPerfDiscTime);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktInHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktInHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktInHC (UINT4 u4BfdSessIndex,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValBfdSessPerfCtrlPktInHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktInHC (0, u4BfdSessIndex,
                                             pu8RetValBfdSessPerfCtrlPktInHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktOutHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktOutHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktOutHC (UINT4 u4BfdSessIndex,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValBfdSessPerfCtrlPktOutHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktOutHC (0, u4BfdSessIndex,
                                              pu8RetValBfdSessPerfCtrlPktOutHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfCtrlPktDropHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfCtrlPktDropHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfCtrlPktDropHC (UINT4 u4BfdSessIndex,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValBfdSessPerfCtrlPktDropHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfCtrlPktDropHC (0, u4BfdSessIndex,
                                               pu8RetValBfdSessPerfCtrlPktDropHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktInHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktInHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktInHC (UINT4 u4BfdSessIndex,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValBfdSessPerfEchoPktInHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktInHC (0, u4BfdSessIndex,
                                             pu8RetValBfdSessPerfEchoPktInHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktOutHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktOutHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktOutHC (UINT4 u4BfdSessIndex,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValBfdSessPerfEchoPktOutHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktOutHC (0, u4BfdSessIndex,
                                              pu8RetValBfdSessPerfEchoPktOutHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetBfdSessPerfEchoPktDropHC
 Input       :  The Indices
                BfdSessIndex

                The Object 
                retValBfdSessPerfEchoPktDropHC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessPerfEchoPktDropHC (UINT4 u4BfdSessIndex,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValBfdSessPerfEchoPktDropHC)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessPerfEchoPktDropHC (0, u4BfdSessIndex,
                                               pu8RetValBfdSessPerfEchoPktDropHC);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBfdSessDiscMapTable
 Input       :  The Indices
                BfdSessDiscriminator
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceBfdSessDiscMapTable (UINT4 u4BfdSessDiscriminator)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhValidateIndexInstanceFsMIStdBfdSessDiscMapTable (0,
                                                            u4BfdSessDiscriminator);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBfdSessDiscMapTable
 Input       :  The Indices
                BfdSessDiscriminator
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBfdSessDiscMapTable (UINT4 *pu4BfdSessDiscriminator)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIStdBfdSessDiscMapTable (&u4FirstContextId,
                                                    pu4BfdSessDiscriminator);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBfdSessDiscMapTable
 Input       :  The Indices
                BfdSessDiscriminator
                nextBfdSessDiscriminator
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBfdSessDiscMapTable (UINT4 u4BfdSessDiscriminator,
                                    UINT4 *pu4NextBfdSessDiscriminator)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIStdBfdSessDiscMapTable (0, &u4NextContextId,
                                                   u4BfdSessDiscriminator,
                                                   pu4NextBfdSessDiscriminator);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdSessDiscMapIndex
 Input       :  The Indices
                BfdSessDiscriminator

                The Object 
                retValBfdSessDiscMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessDiscMapIndex (UINT4 u4BfdSessDiscriminator,
                           UINT4 *pu4RetValBfdSessDiscMapIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessDiscMapIndex (0, u4BfdSessDiscriminator,
                                          pu4RetValBfdSessDiscMapIndex);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceBfdSessIpMapTable
 Input       :  The Indices
                BfdSessInterface
                BfdSessSrcAddrType
                BfdSessSrcAddr
                BfdSessDstAddrType
                BfdSessDstAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceBfdSessIpMapTable (INT4 i4BfdSessInterface,
                                           INT4 i4BfdSessSrcAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pBfdSessSrcAddr,
                                           INT4 i4BfdSessDstAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhValidateIndexInstanceFsMIStdBfdSessIpMapTable (0, i4BfdSessInterface,
                                                          i4BfdSessSrcAddrType,
                                                          pBfdSessSrcAddr,
                                                          i4BfdSessDstAddrType,
                                                          pBfdSessDstAddr);
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBfdSessIpMapTable
 Input       :  The Indices
                BfdSessInterface
                BfdSessSrcAddrType
                BfdSessSrcAddr
                BfdSessDstAddrType
                BfdSessDstAddr
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBfdSessIpMapTable (INT4 *pi4BfdSessInterface,
                                   INT4 *pi4BfdSessSrcAddrType,
                                   tSNMP_OCTET_STRING_TYPE * pBfdSessSrcAddr,
                                   INT4 *pi4BfdSessDstAddrType,
                                   tSNMP_OCTET_STRING_TYPE * pBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4FirstContextId = 0;

    i4Return =
        nmhGetFirstIndexFsMIStdBfdSessIpMapTable (&u4FirstContextId,
                                                  pi4BfdSessInterface,
                                                  pi4BfdSessSrcAddrType,
                                                  pBfdSessSrcAddr,
                                                  pi4BfdSessDstAddrType,
                                                  pBfdSessDstAddr);
    if (u4FirstContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBfdSessIpMapTable
 Input       :  The Indices
                BfdSessInterface
                nextBfdSessInterface
                BfdSessSrcAddrType
                nextBfdSessSrcAddrType
                BfdSessSrcAddr
                nextBfdSessSrcAddr
                BfdSessDstAddrType
                nextBfdSessDstAddrType
                BfdSessDstAddr
                nextBfdSessDstAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBfdSessIpMapTable (INT4 i4BfdSessInterface,
                                  INT4 *pi4NextBfdSessInterface,
                                  INT4 i4BfdSessSrcAddrType,
                                  INT4 *pi4NextBfdSessSrcAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pBfdSessSrcAddr,
                                  tSNMP_OCTET_STRING_TYPE * pNextBfdSessSrcAddr,
                                  INT4 i4BfdSessDstAddrType,
                                  INT4 *pi4NextBfdSessDstAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pBfdSessDstAddr,
                                  tSNMP_OCTET_STRING_TYPE * pNextBfdSessDstAddr)
{
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4NextContextId = 0;

    i4Return =
        nmhGetNextIndexFsMIStdBfdSessIpMapTable (0, &u4NextContextId,
                                                 i4BfdSessInterface,
                                                 pi4NextBfdSessInterface,
                                                 i4BfdSessSrcAddrType,
                                                 pi4NextBfdSessSrcAddrType,
                                                 pBfdSessSrcAddr,
                                                 pNextBfdSessSrcAddr,
                                                 i4BfdSessDstAddrType,
                                                 pi4NextBfdSessDstAddrType,
                                                 pBfdSessDstAddr,
                                                 pNextBfdSessDstAddr);
    if (u4NextContextId != 0)
    {
        i4Return = SNMP_FAILURE;
    }
    return i4Return;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBfdSessIpMapIndex
 Input       :  The Indices
                BfdSessInterface
                BfdSessSrcAddrType
                BfdSessSrcAddr
                BfdSessDstAddrType
                BfdSessDstAddr

                The Object 
                retValBfdSessIpMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBfdSessIpMapIndex (INT4 i4BfdSessInterface, INT4 i4BfdSessSrcAddrType,
                         tSNMP_OCTET_STRING_TYPE * pBfdSessSrcAddr,
                         INT4 i4BfdSessDstAddrType,
                         tSNMP_OCTET_STRING_TYPE * pBfdSessDstAddr,
                         UINT4 *pu4RetValBfdSessIpMapIndex)
{
    INT4                i4Return = SNMP_FAILURE;

    i4Return =
        nmhGetFsMIStdBfdSessIpMapIndex (0, i4BfdSessInterface,
                                        i4BfdSessSrcAddrType, pBfdSessSrcAddr,
                                        i4BfdSessDstAddrType, pBfdSessDstAddr,
                                        pu4RetValBfdSessIpMapIndex);
    return i4Return;
}
