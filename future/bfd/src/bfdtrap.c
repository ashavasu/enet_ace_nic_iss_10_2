/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdtrap.c,v 1.26 2016/04/08 09:58:45 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/

#include "bfdinc.h"
#include "bfdtrap.h"
#include "fsmibfd.h"
#include "fsbfd.h"

PRIVATE tSNMP_OID_TYPE *BfdTrapMakeObjIdFrmString PROTO ((INT1 *, UINT1 *));

PRIVATE INT4 BfdTrapParseSubIdNew PROTO ((UINT1 **, UINT4 *));
PRIVATE INT4 BfdFormVarBindList PROTO ((UINT4, UINT4, UINT4, UINT1 *, UINT4,
                                        tSNMP_VAR_BIND **, tSNMP_VAR_BIND **,
                                        tSNMP_OID_TYPE *,
                                        tSNMP_COUNTER64_TYPE));

PRIVATE VOID BfdTrapSendTrapNotifications PROTO ((va_list, UINT4,
                                                  UINT1 *, UINT4));

/****************************************************************************
 *
 * Function     : BfdEventLogNotify
 *
 * Description  : converts variable argument in to Trace  depending on flag
 *
 * Input        : u4ContextId - Context Identifier
 *                u4ArrayIndex -Array Index
 *
 * Output       : None
 *
 * Returns      : VOID
 *
 *****************************************************************************/
PUBLIC VOID
BfdTrapEventLogNotify (UINT4 u4ContextId, UINT4 u4ArrayIndex, ...)
{
    va_list             vTrcArgs;
    va_list             vTrapArgs;
    INT4                i4TraceLevel = 0;
    UINT4               u4SyslogLevel = 0;
    UINT4               u4TrapType = 0;
    UINT4               u4OffSet = 0;
    UINT1              *pLogMsg = NULL;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    CHR1                ac1Buf[BFD_MAX_MSG_LEN];
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGblConfigTblEntry = NULL;

    MEMSET (&vTrcArgs, 0, sizeof (va_list));
    MEMSET (&vTrapArgs, 0, sizeof (va_list));
    MEMSET (ac1Buf, 0, BFD_MAX_MSG_LEN);
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    /*Identify the message and related information from the ArrayIndex */
    if (BFD_TRC_MAX_SIZE < u4ArrayIndex)
    {
        return;
    }
    i4TraceLevel = (INT4) gaBfdEventLogNotify[u4ArrayIndex].u4TraceLevel;
    u4SyslogLevel = gaBfdEventLogNotify[u4ArrayIndex].u4SyslogLevel;
    u4TrapType = gaBfdEventLogNotify[u4ArrayIndex].u4TrapType;
    pLogMsg = gaBfdEventLogNotify[u4ArrayIndex].au1LogMsg;

    if (u4ContextId == BFD_INVALID_CONTEXT)
    {
        if (gBfdGlobals.u4BfdTrc == OSIX_TRUE)
        {
            SNPRINTF (ac1Buf, BFD_MAX_MSG_LEN, "BFD: ");
        }
        else
        {
            return;
        }
    }
    else
    {
        /* Get Global Context info */
        pBfdGblConfigTblEntry = BfdUtilGetBfdGlobalConfigTable (u4ContextId);
        if (pBfdGblConfigTblEntry == NULL)
        {
            return;
        }

        STRCPY (au1ContextName, pBfdGblConfigTblEntry->au1ContextName);

        if (i4TraceLevel & pBfdGblConfigTblEntry->MibObject.i4FsMIBfdTraceLevel)
        {
            SNPRINTF (ac1Buf, BFD_MAX_MSG_LEN, (CONST CHR1 *) au1ContextName);
        }
        /* Even when traces level is not set, continue to see if trap
         * needs to be raised */
    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (vTrcArgs, u4ArrayIndex);

    vsprintf (&ac1Buf[u4OffSet], (CHR1 *) pLogMsg, vTrcArgs);

    /*Invoke API to log in syslog application. */
    BfdExtFmApiSysLog (u4ContextId, u4SyslogLevel, ac1Buf);

    va_end (vTrcArgs);

    if ((u4ContextId != BFD_INVALID_CONTEXT) && (i4TraceLevel &
                                                 pBfdGblConfigTblEntry->
                                                 MibObject.i4FsMIBfdTraceLevel))
    {
        UtlTrcPrint (ac1Buf);
    }
    else if (u4ContextId == BFD_INVALID_CONTEXT)
    {
        UtlTrcPrint (ac1Buf);
    }

    va_start (vTrapArgs, u4ArrayIndex);

    if (pBfdGblConfigTblEntry != NULL)
    {
        if (u4TrapType == BFD_SESS_UP_TRAP || u4TrapType == BFD_SESS_DOWN_TRAP)
        {
            if (pBfdGblConfigTblEntry->MibObject.i4FsMIBfdTrapEnable <<
                BFD_SESS_UP_DOWN_TRAP_EN)
            {
                BfdTrapSendTrapNotifications (vTrapArgs, u4ContextId,
                                              au1ContextName, u4TrapType);
            }
        }
        else if (u4TrapType & ((UINT4) pBfdGblConfigTblEntry->MibObject.
                               i4FsMIBfdTrapEnable))
        {
            BfdTrapSendTrapNotifications (vTrapArgs, u4ContextId,
                                          au1ContextName, u4TrapType);
        }
    }
    va_end (vTrapArgs);
    return;

}

/*****************************************************************************/
/* Function Name      : BfdTrapSendTrapNotifications                         */
/*                                                                           */
/* Description        : This function will send an SNMP trap to the          */
/*                      administrator for various Bfd conditions.            */
/*                                                                           */
/* Input(s)           : pNotifyInfo - Information to be sent in the Trap     */
/*                                   message.                                */
/*                      u1TrapType - Specific Type for Trap Message          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
PUBLIC VOID
BfdTrapSendTrapNotifications (va_list vTrapArgs, UINT4 u4ContextId,
                              UINT1 *pu1ContextName, UINT4 u4SpecTrapType)
{
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOidCopy = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               au4FsMpTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 55, 3, 0 };
    UINT4               au4FsMsTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 54, 0 };
    UINT4               au4FsTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 53, 3, 0 };
    UINT4               au4FsStdTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 56, 0 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4TrapObjVal = 0;
    UINT4               u4SysMode = 0;
    UINT1               au1Buf[BFD_OBJECT_NAME_LEN];
    UINT4               u4SesIdx = BFD_INVALID_SESS_IDX;
    INT4                i4Flag = TRUE;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Filling the Enterprise OID */
    pEnterpriseOid = alloc_oid (BFD_SNMPV2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    /* Allocating SNMP Trap OID */
    pSnmpTrapOid = alloc_oid (BFD_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    u4SysMode = (UINT4) BfdPortGetSystemModeExt (BFD_PROTOCOL_ID);

    if (u4SysMode == VCM_SI_MODE)
    {
        switch (u4SpecTrapType)
        {
                /* Assigning the correct Trap Oid */
            case BFD_SESS_UP_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsStdTrapOid,
                        sizeof (au4FsStdTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsStdTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_SESS_UP_TRAP_OID;

                break;
            case BFD_SESS_DOWN_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsStdTrapOid,
                        sizeof (au4FsStdTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsStdTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_SESS_DOWN_TRAP_OID;

                break;
            case BFD_BTSTRAP_FAIL_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsTrapOid,
                        sizeof (au4FsTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_BTSTRAP_FAIL_TRAP_OID;

                break;
            case BFD_NEG_TX_CHANGE_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsTrapOid,
                        sizeof (au4FsTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_NEG_TX_CHANGE_TRAP_OID;

                break;
            case BFD_ADMIN_CTRL_ERR_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsTrapOid,
                        sizeof (au4FsTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsTrapOid) / sizeof (UINT4);
                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_ADMIN_CTRL_ERR_TRAP_OID;

                break;
            default:
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                i4Flag = FALSE;
                break;
        }

        if (i4Flag == TRUE)
        {
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
                    BFD_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

            /*Assigning the Snmp TrapOID value */
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID,
                        0, 0, NULL, pEnterpriseOid, u8CounterVal);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;
        }

        MEMSET (au1Buf, 0, sizeof (au1Buf));
        switch (u4SpecTrapType)
        {
            case BFD_SESS_UP_TRAP:
            case BFD_SESS_DOWN_TRAP:
                u4SesIdx = va_arg (vTrapArgs, UINT4);
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "bfdSessDiag");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "bfdSessDiag");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                break;

            case BFD_BTSTRAP_FAIL_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "bfdSessDiscriminator");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                break;
            case BFD_NEG_TX_CHANGE_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "bfdSessDiscriminator");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "bfdSessRemoteDiscr");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                SPRINTF ((CHR1 *) au1Buf, "bfdSessNegotiatedInterval");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                break;

            case BFD_ADMIN_CTRL_ERR_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "bfdSessState");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "bfdSessDiscriminator");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "fsBfdSessAdminCtrlErrReason");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                break;

            default:
                SNMP_AGT_FreeVarBindList (pStartVb);
                pStartVb = NULL;
                return;
        }
    }
    else
    {
        switch (u4SpecTrapType)
        {
                /* Assigning the correct Trap Oid */
            case BFD_SESS_UP_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsMsTrapOid,
                        sizeof (au4FsMsTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsMsTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_SESS_UP_TRAP_MI_OID;
                break;
            case BFD_SESS_DOWN_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsMsTrapOid,
                        sizeof (au4FsMsTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsMsTrapOid) / sizeof (UINT4);

                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_SESS_DOWN_TRAP_MI_OID;
                break;
            case BFD_BTSTRAP_FAIL_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsMpTrapOid,
                        sizeof (au4FsMpTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsMpTrapOid) / sizeof (UINT4);
                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_BTSTRAP_FAIL_TRAP_OID;
                break;
            case BFD_NEG_TX_CHANGE_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsMpTrapOid,
                        sizeof (au4FsMpTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsMpTrapOid) / sizeof (UINT4);
                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_NEG_TX_CHANGE_TRAP_OID;
                break;
            case BFD_ADMIN_CTRL_ERR_TRAP:
                MEMCPY (pEnterpriseOid->pu4_OidList, au4FsMpTrapOid,
                        sizeof (au4FsMpTrapOid));
                pEnterpriseOid->u4_Length =
                    sizeof (au4FsMpTrapOid) / sizeof (UINT4);
                pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                    BFD_ADMIN_CTRL_ERR_TRAP_OID;
                break;
            default:
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                i4Flag = FALSE;
                break;
        }

        if (i4Flag == TRUE)
        {

            MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
                    BFD_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

            /*Assigning the Snmp TrapOID value */
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID,
                        0, 0, NULL, pEnterpriseOid, u8CounterVal);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            /* Filling the Context-Name */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdContextName");

            pOid = BfdTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                    (UINT1 *) orig_mib_mi_oid_table);

            /* For SI mode context name is not required for raising the trap */

            if (pOid == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                pStartVb = NULL;
                return;
            }

            MEMSET (au1Buf, 0, sizeof (au1Buf));
            STRNCPY ((CHR1 *) au1Buf, (CHR1 *) pu1ContextName,
                    MEM_MAX_BYTES (STRLEN (pu1ContextName),
                        BFD_OBJECT_NAME_LEN - 1));
            pOstring =
                SNMP_AGT_FormOctetString (pu1ContextName, (BFD_MAX_CONTEXT_NAME));

            if (pOstring == NULL)
            {
                SNMP_AGT_FreeVarBindList (pStartVb);
                pStartVb = NULL;
                SNMP_FreeOid (pOid);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                        pOstring, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pOstring);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                pStartVb = NULL;
                return;
            }

            /* Move VbList to last node */
            pVbList = pVbList->pNextVarBind;
        }

        MEMSET (au1Buf, 0, sizeof (au1Buf));
        switch (u4SpecTrapType)
        {
            case BFD_SESS_UP_TRAP:
            case BFD_SESS_DOWN_TRAP:
                u4SesIdx = va_arg (vTrapArgs, UINT4);
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessDiag");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessDiag");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                break;

            case BFD_BTSTRAP_FAIL_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessDiscriminator");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                break;
            case BFD_NEG_TX_CHANGE_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessDiscriminator");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessRemoteDiscr");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }
                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessNegotiatedInterval");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                break;

            case BFD_ADMIN_CTRL_ERR_TRAP:
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessDiscriminator");
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "fsMIStdBfdSessState");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                SPRINTF ((CHR1 *) au1Buf, "fsMIBfdSessAdminCtrlErrReason");
                u4TrapObjVal = va_arg (vTrapArgs, UINT4);
                if (BfdFormVarBindList (u4ContextId, u4SesIdx, u4SysMode,
                                        au1Buf, u4TrapObjVal, &pVbList,
                                        &pStartVb, pEnterpriseOid,
                                        SnmpCnt64Type) != OSIX_SUCCESS)
                {
                    SNMP_AGT_FreeVarBindList (pStartVb);
                    pStartVb = NULL;
                    return;
                }

                break;

            default:
                SNMP_AGT_FreeVarBindList (pStartVb);
                pStartVb = NULL;
                return;
        }
    }
    if (u4SysMode == VCM_MI_MODE)
    {
        pContextName = SNMP_AGT_FormOctetString (pu1ContextName,
                                                 (BFD_MAX_CONTEXT_NAME));
        if (pContextName == NULL)
        {
            SNMP_AGT_FreeVarBindList (pStartVb);
            pStartVb = NULL;
            return;
        }

        /* Allocating the Enterprise OID Copy */
        pEnterpriseOidCopy = alloc_oid (BFD_SNMPV2_TRAP_OID_LEN);
        if (pEnterpriseOidCopy == NULL)
        {
            SNMP_AGT_FreeOctetString (pContextName);
            SNMP_AGT_FreeVarBindList (pStartVb);
            pStartVb = NULL;
            return;
        }

        /*In case of MI, a copy of enterprise OID is needed as 
         * pEnterpriseOid is already assigned in VbList. This copy 
         * is done to prevent double free of enterprise OID*/
        pEnterpriseOidCopy->u4_Length = pEnterpriseOid->u4_Length;
        memcpy (pEnterpriseOidCopy->pu4_OidList, pEnterpriseOid->pu4_OidList,
                pEnterpriseOidCopy->u4_Length * sizeof (UINT4));
    }
    else
    {
        /*SI mode doesn't need a copy of enterprise OID and Doesn't free it 
         * internally */
        pEnterpriseOidCopy = pEnterpriseOid;
    }

    BfdExtFmNotifyFaults (pEnterpriseOidCopy, u4GenTrapType, u4SpecTrapType,
                          pStartVb, pContextName);

    if (pContextName != NULL)
    {
        SNMP_AGT_FreeOctetString (pContextName);
    }
    return;
}

/******************************************************************************
 * * Function :   BfdTrapMakeObjIdFrmString
 * *
 * * Description: This Function retuns the OID  of the given string for the
 * *              proprietary MIB.
 * *
 * * Input    :   pi1TextStr - pointer to the string.
 * *              pTableName - TableName has to be fetched.
 * *
 * * Output   :   None.
 * *
 * * Returns  :   pOidPtr or NULL
 * ****************************************************************************/
PRIVATE tSNMP_OID_TYPE *
BfdTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[BFD_MAX_MSG_LEN];
    UINT1              *pu1TempPtr = NULL;
    UINT2               u2Length = 0;

    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < (BFD_MAX_MSG_LEN - 1))); u2Index++)
        {
            ai1TempBuffer[u2Index] = (INT1) *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                MEMSET (ai1TempBuffer, 0, BFD_MAX_MSG_LEN);
                u2Length =
                    (UINT2) MEM_MAX_BYTES (STRLEN (pTableName[u2Index].pNumber),
                                           BFD_MAX_MSG_LEN - 1);
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         u2Length);
                ai1TempBuffer[sizeof (ai1TempBuffer) - 1] = '\0';
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u2Length =
            (UINT2) MEM_MAX_BYTES (STRLEN (pi1DotPtr),
                                   BFD_MAX_MSG_LEN - 1 -
                                   STRLEN (ai1TempBuffer));
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr, u2Length);
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 MEM_MAX_BYTES (STRLEN (pi1TextStr), BFD_MAX_MSG_LEN - 1));
    }

    /* Now we've got something with numbers instead of an alpha header */
    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index <= (BFD_MAX_MSG_LEN - 1)) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (BfdTrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/*****************************************************************************
 * Function :   BfdTrapParseSubIdNew
 *
 * Description : Parse the string format in number.number..format.
 *
 * Input       : ppu1TempPtr - pointer to the string.
 *               pu4Value    - Pointer the OID List value.
 *
 * Output      : value of ppu1TempPtr
 *
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
BfdTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : BfdFormVarBindList                                   */
/*                                                                           */
/* Description        : This function will form the varbind list for the     */
/*                      trap objects.                                        */
/*                                                                           */
/* Input(s)           : pu1Buf- Pointer to the Trap obj name                 */
/*                      u4TrapObjVal- Trap object value                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
PRIVATE INT4
BfdFormVarBindList (UINT4 u4ContextId, UINT4 u4SesIdx, UINT4 u4SysMode,
                    UINT1 *pu1Buf, UINT4 u4TrapObjVal,
                    tSNMP_VAR_BIND ** ppVbList, tSNMP_VAR_BIND ** ppStartVb,
                    tSNMP_OID_TYPE * pEnterpriseOid,
                    tSNMP_COUNTER64_TYPE SnmpCnt64Type)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    if (u4SysMode == VCM_MI_MODE)
    {
        pOid = BfdTrapMakeObjIdFrmString ((INT1 *) pu1Buf,
                                          (UINT1 *) orig_mib_mi_oid_table);
        if (pOid == NULL)
        {
            return OSIX_FAILURE;
        }
        /* The Object is tabular. Hence append the Index value. */
        pOid->pu4_OidList[pOid->u4_Length] = u4ContextId;
        pOid->u4_Length++;
    }
    else if (u4SysMode == VCM_SI_MODE)
    {
        pOid = BfdTrapMakeObjIdFrmString ((INT1 *) pu1Buf,
                                          (UINT1 *) orig_mib_oid_table);
        if (pOid == NULL)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        return OSIX_FAILURE;
    }

    /* The Object is tabular. Hence append the Index value. */
    pOid->pu4_OidList[pOid->u4_Length] = u4SesIdx;
    pOid->u4_Length++;

    if (NULL == *ppVbList)
    {
        *ppVbList = SNMP_AGT_FormVarBind
            (pOid, SNMP_DATA_TYPE_UNSIGNED32, u4TrapObjVal, 0,
             NULL, NULL, SnmpCnt64Type);
        if ((*ppVbList) == NULL)
        {
            SNMP_FreeOid (pOid);
            return OSIX_FAILURE;
        }
        *ppStartVb = *ppVbList;
    }
    else
    {
        (*ppVbList)->pNextVarBind = SNMP_AGT_FormVarBind
            (pOid, SNMP_DATA_TYPE_UNSIGNED32, u4TrapObjVal, 0,
             NULL, NULL, SnmpCnt64Type);

        if ((*ppVbList)->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pOid);
            return OSIX_FAILURE;
        }
        *ppVbList = (*ppVbList)->pNextVarBind;
    }
    UNUSED_PARAM (pEnterpriseOid);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 * Function     : BfdIssTrapEventLogNotify
 *
 * Description  : converts variable argument in to Trace  depending on flag
 *
 * Input        : u4ContextId - Context Identifier
 *                u4ArrayIndex -Array Index
 *
 * Output       : None
 *
 * Returns      : VOID
 *
 *****************************************************************************/
PUBLIC VOID
BfdIssTrapEventLogNotify (UINT4 u4ContextId, UINT4 u4ArrayIndex, ...)
{
    va_list             vTrcArgs;
    INT4                i4TraceLevel = 0;
    UINT4               u4SyslogLevel = 0;
    UINT4               u4OffSet = 0;
    UINT1              *pLogMsg = NULL;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    CHR1                ac1Buf[BFD_MAX_MSG_LEN];
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGblConfigTblEntry = NULL;

    MEMSET (&vTrcArgs, 0, sizeof (va_list));
    MEMSET (ac1Buf, 0, BFD_MAX_MSG_LEN);
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    /*Identify the message and related information from the ArrayIndex */
    if (BFD_COMMON_TRC_MAX_SIZE < u4ArrayIndex)
    {
        return;
    }
    i4TraceLevel = (INT4) gaIssEventLogNotify[u4ArrayIndex].u4TraceLevel;
    u4SyslogLevel = gaIssEventLogNotify[u4ArrayIndex].u4SyslogLevel;
    pLogMsg = gaIssEventLogNotify[u4ArrayIndex].au1LogMsg;

    if (u4ContextId == BFD_INVALID_CONTEXT)
    {
        if (gBfdGlobals.u4BfdTrc == OSIX_TRUE)
        {
            SNPRINTF (ac1Buf, BFD_MAX_MSG_LEN, "BFD: ");
        }
        else
        {
            return;
        }
    }
    else
    {
        /* Get Global Context info */
        pBfdGblConfigTblEntry = BfdUtilGetBfdGlobalConfigTable (u4ContextId);
        if (pBfdGblConfigTblEntry == NULL)
        {
            return;
        }

        STRCPY (au1ContextName, pBfdGblConfigTblEntry->au1ContextName);

        if (!
            (i4TraceLevel & pBfdGblConfigTblEntry->MibObject.
             i4FsMIBfdTraceLevel))
        {
            SNPRINTF (ac1Buf, BFD_MAX_MSG_LEN, (CONST CHR1 *) au1ContextName);
        }
        else
        {
            return;
        }
    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (vTrcArgs, u4ArrayIndex);

    vsprintf (&ac1Buf[u4OffSet], (CHR1 *) pLogMsg, vTrcArgs);

    /*Invoke API to log in syslog application. */
    BfdExtFmApiSysLog (u4ContextId, u4SyslogLevel, ac1Buf);

    va_end (vTrcArgs);

    UtlTrcPrint (ac1Buf);

}
