/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdutlg.c,v 1.15 2016/04/23 08:24:58 siva Exp $
*
* Description: This file contains utility functions used by protocol Bfd
*********************************************************************/

#include "bfdinc.h"
#ifdef ISS_WANTED
#include "msr.h"
#endif

/****************************************************************************
 Function    :  BfdUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
BfdUtlCreateRBTree ()
{

    if (BfdFsMIStdBfdGlobalConfigTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (BfdFsMIStdBfdSessTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (BfdFsMIStdBfdSessDiscMapTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (BfdFsMIStdBfdSessIpMapTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdGlobalConfigTableCreate
 Input       :  None
 Description :  This function creates the FsMIStdBfdGlobalConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
BfdFsMIStdBfdGlobalConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tBfdFsMIStdBfdGlobalConfigTableEntry,
                       MibObject.FsMIStdBfdGlobalConfigTableNode);

    if ((gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMIStdBfdGlobalConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessTableCreate
 Input       :  None
 Description :  This function creates the FsMIStdBfdSessTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
BfdFsMIStdBfdSessTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tBfdFsMIStdBfdSessEntry,
                       MibObject.FsMIStdBfdSessTableNode);

    if ((gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMIStdBfdSessTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessDiscMapTableCreate
 Input       :  None
 Description :  This function creates the FsMIStdBfdSessDiscMapTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
BfdFsMIStdBfdSessDiscMapTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tBfdFsMIStdBfdSessDiscMapEntry,
                       MibObject.FsMIStdBfdSessDiscMapTableNode);

    if ((gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMIStdBfdSessDiscMapTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessIpMapTableCreate
 Input       :  None
 Description :  This function creates the FsMIStdBfdSessIpMapTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
BfdFsMIStdBfdSessIpMapTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tBfdFsMIStdBfdSessIpMapEntry,
                       MibObject.FsMIStdBfdSessIpMapTableNode);

    if ((gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMIStdBfdSessIpMapTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMIStdBfdGlobalConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMIStdBfdGlobalConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMIStdBfdGlobalConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tBfdFsMIStdBfdGlobalConfigTableEntry *pFsMIStdBfdGlobalConfigTableEntry1 =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *) pRBElem1;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pFsMIStdBfdGlobalConfigTableEntry2 =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMIStdBfdGlobalConfigTableEntry1->MibObject.u4FsMIStdBfdContextId >
        pFsMIStdBfdGlobalConfigTableEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return 1;
    }
    else if (pFsMIStdBfdGlobalConfigTableEntry1->MibObject.
             u4FsMIStdBfdContextId <
             pFsMIStdBfdGlobalConfigTableEntry2->MibObject.
             u4FsMIStdBfdContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMIStdBfdSessTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMIStdBfdSessTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMIStdBfdSessTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tBfdFsMIStdBfdSessEntry *pFsMIStdBfdSessEntry1 =
        (tBfdFsMIStdBfdSessEntry *) pRBElem1;
    tBfdFsMIStdBfdSessEntry *pFsMIStdBfdSessEntry2 =
        (tBfdFsMIStdBfdSessEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMIStdBfdSessEntry1->MibObject.u4FsMIStdBfdContextId >
        pFsMIStdBfdSessEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessEntry1->MibObject.u4FsMIStdBfdContextId <
             pFsMIStdBfdSessEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return -1;
    }

    if (pFsMIStdBfdSessEntry1->MibObject.u4FsMIStdBfdSessIndex >
        pFsMIStdBfdSessEntry2->MibObject.u4FsMIStdBfdSessIndex)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessEntry1->MibObject.u4FsMIStdBfdSessIndex <
             pFsMIStdBfdSessEntry2->MibObject.u4FsMIStdBfdSessIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMIStdBfdSessDiscMapTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMIStdBfdSessDiscMapTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMIStdBfdSessDiscMapTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tBfdFsMIStdBfdSessDiscMapEntry *pFsMIStdBfdSessDiscMapEntry1 =
        (tBfdFsMIStdBfdSessDiscMapEntry *) pRBElem1;
    tBfdFsMIStdBfdSessDiscMapEntry *pFsMIStdBfdSessDiscMapEntry2 =
        (tBfdFsMIStdBfdSessDiscMapEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMIStdBfdSessDiscMapEntry1->MibObject.u4FsMIStdBfdContextId >
        pFsMIStdBfdSessDiscMapEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessDiscMapEntry1->MibObject.u4FsMIStdBfdContextId <
             pFsMIStdBfdSessDiscMapEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return -1;
    }

    if (pFsMIStdBfdSessDiscMapEntry1->MibObject.u4FsMIStdBfdSessDiscriminator >
        pFsMIStdBfdSessDiscMapEntry2->MibObject.u4FsMIStdBfdSessDiscriminator)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessDiscMapEntry1->MibObject.
             u4FsMIStdBfdSessDiscriminator <
             pFsMIStdBfdSessDiscMapEntry2->MibObject.
             u4FsMIStdBfdSessDiscriminator)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsMIStdBfdSessIpMapTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsMIStdBfdSessIpMapTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsMIStdBfdSessIpMapTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tBfdFsMIStdBfdSessIpMapEntry *pFsMIStdBfdSessIpMapEntry1 =
        (tBfdFsMIStdBfdSessIpMapEntry *) pRBElem1;
    tBfdFsMIStdBfdSessIpMapEntry *pFsMIStdBfdSessIpMapEntry2 =
        (tBfdFsMIStdBfdSessIpMapEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.u4FsMIStdBfdContextId >
        pFsMIStdBfdSessIpMapEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.u4FsMIStdBfdContextId <
             pFsMIStdBfdSessIpMapEntry2->MibObject.u4FsMIStdBfdContextId)
    {
        return -1;
    }

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessInterface >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessInterface)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessInterface <
             pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessInterface)
    {
        return -1;
    }

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrType >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrType)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrType <
             pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrLen >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrLen)
    {
        i4MaxLength =
            pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrLen;
    }
    else
    {
        i4MaxLength =
            pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrLen;
    }

    if (MEMCMP
        (pFsMIStdBfdSessIpMapEntry1->MibObject.au1FsMIStdBfdSessSrcAddr,
         pFsMIStdBfdSessIpMapEntry2->MibObject.au1FsMIStdBfdSessSrcAddr,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMIStdBfdSessIpMapEntry1->MibObject.au1FsMIStdBfdSessSrcAddr,
              pFsMIStdBfdSessIpMapEntry2->MibObject.au1FsMIStdBfdSessSrcAddr,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrLen >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrLen)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessSrcAddrLen <
             pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessSrcAddrLen)
    {
        return -1;
    }

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrType >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrType)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrType <
             pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrLen >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrLen)
    {
        i4MaxLength =
            pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrLen;
    }
    else
    {
        i4MaxLength =
            pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrLen;
    }

    if (MEMCMP
        (pFsMIStdBfdSessIpMapEntry1->MibObject.au1FsMIStdBfdSessDstAddr,
         pFsMIStdBfdSessIpMapEntry2->MibObject.au1FsMIStdBfdSessDstAddr,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsMIStdBfdSessIpMapEntry1->MibObject.au1FsMIStdBfdSessDstAddr,
              pFsMIStdBfdSessIpMapEntry2->MibObject.au1FsMIStdBfdSessDstAddr,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrLen >
        pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrLen)
    {
        return 1;
    }
    else if (pFsMIStdBfdSessIpMapEntry1->MibObject.i4FsMIStdBfdSessDstAddrLen <
             pFsMIStdBfdSessIpMapEntry2->MibObject.i4FsMIStdBfdSessDstAddrLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsMIStdBfdGlobalConfigTableFilterInputs
 Input       :  The Indices
                pBfdFsMIStdBfdGlobalConfigTableEntry
                pBfdSetFsMIStdBfdGlobalConfigTableEntry
                pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMIStdBfdGlobalConfigTableFilterInputs (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                         pBfdFsMIStdBfdGlobalConfigTableEntry,
                                         tBfdFsMIStdBfdGlobalConfigTableEntry *
                                         pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                                         tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
                                         *
                                         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIStdBfdContextId ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIStdBfdContextId)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdAdminStatus)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdSessNotificationsEnable ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIStdBfdSessNotificationsEnable)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
                bFsMIStdBfdSessNotificationsEnable = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTraceLevel ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTraceLevel)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTrapEnable ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdTrapEnable)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdGblSessOperMode ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdGblSessOperMode)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIBfdGblDesiredMinTxIntvl == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDesiredMinTxIntvl ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDesiredMinTxIntvl)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
                bFsMIBfdGblDesiredMinTxIntvl = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblReqMinRxIntvl ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblReqMinRxIntvl ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblReqMinRxIntvl)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
                bFsMIBfdGblReqMinRxIntvl = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDetectMult ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblDetectMult)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblSlowTxIntvl ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            u4FsMIBfdGblSlowTxIntvl)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrGblStats ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrGblStats)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrAllSessStats ==
            pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdClrAllSessStats)
            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats =
                OSIX_FALSE;
    }
    if ((pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdContextId ==
         OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
            bFsMIStdBfdSessNotificationsEnable == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
            bFsMIBfdGblSessOperMode == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
            bFsMIBfdGblDesiredMinTxIntvl == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
            bFsMIBfdGblReqMinRxIntvl == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
            bFsMIBfdClrAllSessStats == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsMIStdBfdSessTableFilterInputs
 Input       :  The Indices
                pBfdFsMIStdBfdSessEntry
                pBfdSetFsMIStdBfdSessEntry
                pBfdIsSetFsMIStdBfdSessEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMIStdBfdSessTableFilterInputs (tBfdFsMIStdBfdSessEntry *
                                 pBfdFsMIStdBfdSessEntry,
                                 tBfdFsMIStdBfdSessEntry *
                                 pBfdSetFsMIStdBfdSessEntry,
                                 tBfdIsSetFsMIStdBfdSessEntry *
                                 pBfdIsSetFsMIStdBfdSessEntry)
{
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    BOOL1               bIsRegisteredClient = OSIX_FALSE;
    BOOL1               bIsSameMapType = OSIX_FALSE;
    BOOL1               bIsSameSessType = OSIX_FALSE;
    BOOL1               bIsSameSessIf = OSIX_FALSE;
    BOOL1               bIsSameDstType = OSIX_FALSE;
    BOOL1               bIsSameDstAddr = OSIX_FALSE;

    MEMSET (au1BfdClients, 0, sizeof (au1BfdClients));

    MEMCPY (au1BfdClients, &(pBfdFsMIStdBfdSessEntry->MibObject.
                             u4FsMIBfdSessRegisteredClients),
            sizeof (au1BfdClients));
    if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
        BFD_PATH_TYPE_IPV4)
    {
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_IP,
                                 sizeof (au1BfdClients), bIsRegisteredClient);
    }
    else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
             BFD_PATH_TYPE_IPV6)
    {
        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, BFD_CLIENT_ID_IP6,
                                 sizeof (au1BfdClients), bIsRegisteredClient);
    }

    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessVersionNumber)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType)
        {
            if (bIsRegisteredClient == OSIX_FALSE)
            {
                bIsSameSessType = OSIX_TRUE;
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType = OSIX_TRUE;
            }
            else
            {
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType = OSIX_FALSE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDestinationUdpPort ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDestinationUdpPort)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessSourceUdpPort)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessEchoSourceUdpPort ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessEchoSourceUdpPort)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessOperMode)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessDemandModeDesiredFlag ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessDemandModeDesiredFlag)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessControlPlaneIndepFlag ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessControlPlaneIndepFlag)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessMultipointFlag ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessMultipointFlag)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface)
        {
            if (bIsRegisteredClient == OSIX_FALSE)
            {
                bIsSameSessIf = OSIX_TRUE;
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface =
                    OSIX_TRUE;
            }
            else
            {
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface =
                    OSIX_FALSE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr == OSIX_TRUE)
    {
        if ((MEMCMP
             (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
              pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
              pBfdSetFsMIStdBfdSessEntry->MibObject.
              i4FsMIStdBfdSessSrcAddrLen) == 0)
            && (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen ==
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessSrcAddrLen))
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType)
        {
            if (bIsRegisteredClient == OSIX_FALSE)
            {
                bIsSameDstType = OSIX_TRUE;
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType =
                    OSIX_TRUE;
            }
            else
            {
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType =
                    OSIX_FALSE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr == OSIX_TRUE)
    {
        if ((MEMCMP
             (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
              pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
              pBfdSetFsMIStdBfdSessEntry->MibObject.
              i4FsMIStdBfdSessDstAddrLen) == 0)
            && (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen ==
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrLen))
        {
            if (bIsRegisteredClient == OSIX_FALSE)
            {
                bIsSameDstAddr = OSIX_TRUE;
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr =
                    OSIX_TRUE;
            }
            else
            {
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr =
                    OSIX_FALSE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessDesiredMinTxInterval)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinRxInterval ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinRxInterval)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinEchoRxInterval ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessReqMinEchoRxInterval)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessDetectMult)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationType)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID ==
        OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyID ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyID)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pBfdFsMIStdBfdSessEntry->MibObject.
              au1FsMIStdBfdSessAuthenticationKey,
              pBfdSetFsMIStdBfdSessEntry->MibObject.
              au1FsMIStdBfdSessAuthenticationKey,
              pBfdSetFsMIStdBfdSessEntry->MibObject.
              i4FsMIStdBfdSessAuthenticationKeyLen) == 0)
            && (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyLen ==
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyLen))
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey =
                OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessEXPValue)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessTmrNegotiate)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessEncapType)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType)
        {
            if (bIsRegisteredClient == OSIX_FALSE)
            {
                bIsSameMapType = OSIX_TRUE;
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType = OSIX_TRUE;
            }
            else
            {
                pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType = OSIX_FALSE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer == OSIX_TRUE)
    {
        if ((MEMCMP
             (pBfdFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
              pBfdSetFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
              pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen *
              sizeof (UINT4)) == 0)
            && (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapPointerLen ==
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIBfdSessMapPointerLen))
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber = OSIX_FALSE;
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats == OSIX_TRUE)
    {
        if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats ==
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdClearStats)
            pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats = OSIX_FALSE;
    }
    if ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessIndex == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->
            bFsMIStdBfdSessDemandModeDesiredFlag == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->
            bFsMIStdBfdSessControlPlaneIndepFlag == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdContextId == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate ==
            OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber == OSIX_FALSE)
        && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
#ifdef ISS_WANTED
        if (MsrGetRestorationStatus () == ISS_TRUE)
        {
            return OSIX_TRUE;
        }
#endif
        if ((bIsSameSessType == OSIX_TRUE) &&
            (bIsSameSessIf == OSIX_TRUE) &&
            (bIsSameDstType == OSIX_TRUE) &&
            (bIsSameDstAddr == OSIX_TRUE) && (bIsSameMapType == OSIX_TRUE))
        {
            return OSIX_TRUE;
        }

        /* If a session is created dynamically for monitoring a particular neighbor,
         * the same session cannot be used to monitor other neighbors either
         * statically or dynamically */
        if ((BFD_CHECK_SESS_DYNAMIC_CREATION (pBfdFsMIStdBfdSessEntry)) &&
            (((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType == OSIX_TRUE)
              && (bIsSameSessType == OSIX_FALSE))
             ||
             ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface ==
               OSIX_TRUE) && (bIsSameSessIf == OSIX_FALSE))
             ||
             ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType ==
               OSIX_TRUE) && (bIsSameDstType == OSIX_FALSE))
             ||
             ((pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr ==
               OSIX_TRUE) && (bIsSameDstAddr == OSIX_FALSE))))
        {
            return -1;
        }

	/* Block BFD session parameter configuration if there is 
         * no static BFD session. This is avoid configuration lose after 
         * MSR if the session is pure dynamic session. Hence only the 
         * row status object can be changed for a dynamic BFD session 
         * to enable creation of a static BFD session for the same 
         * destination */

        if (BFD_CHECK_SESS_IS_DYNAMIC (pBfdFsMIStdBfdSessEntry) &&
            (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus ==
            OSIX_FALSE))
        {
            return -1;
        }
    }

    return OSIX_TRUE;
}

/****************************************************************************
 Function    :  BfdSetAllFsMIStdBfdGlobalConfigTableTrigger
 Input       :  The Indices
                pBfdSetFsMIStdBfdGlobalConfigTableEntry
                pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    BfdSetAllFsMIStdBfdGlobalConfigTableTrigger
    (tBfdFsMIStdBfdGlobalConfigTableEntry *
     pBfdSetFsMIStdBfdGlobalConfigTableEntry,
     tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *
     pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, INT4 i4SetOption)
{

    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdAdminStatus, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIStdBfdAdminStatus);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessNotificationsEnable, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIStdBfdSessNotificationsEnable);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSystemControl, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdSystemControl);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdTraceLevel, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdTraceLevel);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdTrapEnable, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdTrapEnable);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdGblSessOperMode, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdGblSessOperMode);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIBfdGblDesiredMinTxIntvl == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdGblDesiredMinTxIntvl, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIBfdGblDesiredMinTxIntvl);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblReqMinRxIntvl ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdGblReqMinRxIntvl, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIBfdGblReqMinRxIntvl);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdGblDetectMult, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIBfdGblDetectMult);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdGblSlowTxIntvl, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIBfdGblSlowTxIntvl);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdClrGblStats, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdClrGblStats);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdClrAllSessStats, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
                      i4FsMIBfdClrAllSessStats);
#else
        UNUSED_PARAM (pBfdSetFsMIStdBfdGlobalConfigTableEntry);
        UNUSED_PARAM (i4SetOption);
#endif
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdSetAllFsMIStdBfdSessTableTrigger
 Input       :  The Indices
                pBfdSetFsMIStdBfdSessEntry
                pBfdIsSetFsMIStdBfdSessEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
BfdSetAllFsMIStdBfdSessTableTrigger (tBfdFsMIStdBfdSessEntry *
                                     pBfdSetFsMIStdBfdSessEntry,
                                     tBfdIsSetFsMIStdBfdSessEntry *
                                     pBfdIsSetFsMIStdBfdSessEntry,
                                     INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessSrcAddrVal;
    UINT1               au1FsMIStdBfdSessSrcAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessDstAddrVal;
    UINT1               au1FsMIStdBfdSessDstAddrVal[256];
    tSNMP_OCTET_STRING_TYPE FsMIStdBfdSessAuthenticationKeyVal;
    UINT1               au1FsMIStdBfdSessAuthenticationKeyVal[256];
    tSNMP_OID_TYPE     *pFsMIBfdSessMapPointerVal = NULL;
    UINT4               u4TempDstAddrVa;
#ifdef IP6_WANTED
    UINT4               u4Index = 0;
    tIp6Addr            TempAddr;
#endif

    pFsMIBfdSessMapPointerVal = alloc_oid (MAX_OID_LEN);

    MEMSET (au1FsMIStdBfdSessSrcAddrVal, 0,
            sizeof (au1FsMIStdBfdSessSrcAddrVal));
    FsMIStdBfdSessSrcAddrVal.pu1_OctetList = au1FsMIStdBfdSessSrcAddrVal;
    FsMIStdBfdSessSrcAddrVal.i4_Length = 0;

    MEMSET (au1FsMIStdBfdSessDstAddrVal, 0,
            sizeof (au1FsMIStdBfdSessDstAddrVal));
    FsMIStdBfdSessDstAddrVal.pu1_OctetList = au1FsMIStdBfdSessDstAddrVal;
    FsMIStdBfdSessDstAddrVal.i4_Length = 0;

    MEMSET (au1FsMIStdBfdSessAuthenticationKeyVal, 0,
            sizeof (au1FsMIStdBfdSessAuthenticationKeyVal));
    FsMIStdBfdSessAuthenticationKeyVal.pu1_OctetList =
        au1FsMIStdBfdSessAuthenticationKeyVal;
    FsMIStdBfdSessAuthenticationKeyVal.i4_Length = 0;

    if (pFsMIBfdSessMapPointerVal == NULL)
    {
        return OSIX_FAILURE;
    }
    if (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType ==
        BFD_INET_ADDR_IPV4)
    {
        /* Check for self Ip address */
        MEMCPY (&u4TempDstAddrVa,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrLen);
        if (NetIpv4IfIsOurAddress (OSIX_NTOHL (u4TempDstAddrVa)) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    else if (pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV6)
    {
#ifdef IP6_WANTED
        MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
        MEMCPY (&TempAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                IPVX_IPV6_ADDR_LEN);
        /* Check for self Ip address */
        if (NetIpv6IsOurAddress (&TempAddr, &u4Index) == NETIPV6_SUCCESS)
        {
            return OSIX_FAILURE;

        }
#endif
    }

    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessVersionNumber, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessVersionNumber);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDestinationUdpPort, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessDestinationUdpPort);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessSourceUdpPort, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessSourceUdpPort);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessEchoSourceUdpPort, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessEchoSourceUdpPort);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessAdminStatus, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessAdminStatus);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessOperMode, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessOperMode);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDemandModeDesiredFlag ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDemandModeDesiredFlag, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessDemandModeDesiredFlag);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessControlPlaneIndepFlag ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessControlPlaneIndepFlag, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessControlPlaneIndepFlag);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessMultipointFlag, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessMultipointFlag);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessInterface, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessInterface);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessSrcAddrType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessSrcAddrType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr == OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessSrcAddrVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessSrcAddrLen);
        FsMIStdBfdSessSrcAddrVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessSrcAddr, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %s",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex, &FsMIStdBfdSessSrcAddrVal);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDstAddrType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessDstAddrType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr == OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessDstAddrVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrLen);
        FsMIStdBfdSessDstAddrVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDstAddr, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %s",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex, &FsMIStdBfdSessDstAddrVal);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessGTSM, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessGTSM);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessGTSMTTL, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessGTSMTTL);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDesiredMinTxInterval ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDesiredMinTxInterval, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessDesiredMinTxInterval);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinRxInterval ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessReqMinRxInterval, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessReqMinRxInterval);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessReqMinEchoRxInterval, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessReqMinEchoRxInterval);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessDetectMult, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessDetectMult);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessAuthPresFlag, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessAuthPresFlag);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessAuthenticationType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessAuthenticationType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID ==
        OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessAuthenticationKeyID, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessAuthenticationKeyID);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey ==
        OSIX_TRUE)
    {
        MEMCPY (FsMIStdBfdSessAuthenticationKeyVal.pu1_OctetList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                au1FsMIStdBfdSessAuthenticationKey,
                pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationKeyLen);
        FsMIStdBfdSessAuthenticationKeyVal.i4_Length =
            pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessAuthenticationKeyLen;

#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessAuthenticationKey, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %s",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      &FsMIStdBfdSessAuthenticationKeyVal);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessStorType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessStorType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessRole, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessRole);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessMode, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMode);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessRemoteDiscr, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIBfdSessRemoteDiscr);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessEXPValue, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIBfdSessEXPValue);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessTmrNegotiate, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIBfdSessTmrNegotiate);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessOffld, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessOffld);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessEncapType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIBfdSessEncapType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessMapType, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIBfdSessMapType);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer == OSIX_TRUE)
    {
        MEMCPY (pFsMIBfdSessMapPointerVal->pu4_OidList,
                pBfdSetFsMIStdBfdSessEntry->MibObject.au4FsMIBfdSessMapPointer,
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapPointerLen * sizeof (UINT4)));
        pFsMIBfdSessMapPointerVal->u4_Length =
            (UINT4) pBfdSetFsMIStdBfdSessEntry->MibObject.
            i4FsMIBfdSessMapPointerLen;

#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessMapPointer, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %o",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex, pFsMIBfdSessMapPointerVal);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessCardNumber, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIBfdSessCardNumber);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdSessSlotNumber, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIBfdSessSlotNumber);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIBfdClearStats, 13, BfdMainTaskLock, BfdMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIBfdClearStats);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }
    /* Setting Session Row Status of fsMIStdBfdSessTable is dependent on 
     * the members of fsMIBfdSessionTable. Hence setting the rowstatus after setting
     * all the members of fsMIBfdSessionTable is set */
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus == OSIX_TRUE)
    {
#ifdef SNMP_3_WANTED
        nmhSetCmnNew (FsMIStdBfdSessRowStatus, 13, BfdMainTaskLock,
                      BfdMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %u %i",
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdContextId,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      u4FsMIStdBfdSessIndex,
                      pBfdSetFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessRowStatus);
#else
        UNUSED_PARAM (i4SetOption);
#endif
    }

    SNMP_FreeOid (pFsMIBfdSessMapPointerVal);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdGlobalConfigTableCreateApi
 Input       :  pBfdFsMIStdBfdGlobalConfigTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tBfdFsMIStdBfdGlobalConfigTableEntry *
BfdFsMIStdBfdGlobalConfigTableCreateApi (tBfdFsMIStdBfdGlobalConfigTableEntry *
                                         pSetBfdFsMIStdBfdGlobalConfigTableEntry)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    if (pSetBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdGlobalConfigTableCreatApi: pSetBfdFsMIStdBfdGlobalConfigTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pBfdFsMIStdBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdGlobalConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pBfdFsMIStdBfdGlobalConfigTableEntry,
                pSetBfdFsMIStdBfdGlobalConfigTableEntry,
                sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));
        if (RBTreeAdd
            (gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable,
             (tRBElem *) pBfdFsMIStdBfdGlobalConfigTableEntry) != RB_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdFsMIStdBfdGlobalConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pBfdFsMIStdBfdGlobalConfigTableEntry);
            return NULL;
        }
        return pBfdFsMIStdBfdGlobalConfigTableEntry;
    }
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessTableCreateApi
 Input       :  pBfdFsMIStdBfdSessEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tBfdFsMIStdBfdSessEntry *
BfdFsMIStdBfdSessTableCreateApi (tBfdFsMIStdBfdSessEntry *
                                 pSetBfdFsMIStdBfdSessEntry)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    if (pSetBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessTableCreatApi: pSetBfdFsMIStdBfdSessEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pBfdFsMIStdBfdSessEntry, pSetBfdFsMIStdBfdSessEntry,
                sizeof (tBfdFsMIStdBfdSessEntry));
        if (RBTreeAdd
            (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
             (tRBElem *) pBfdFsMIStdBfdSessEntry) != RB_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdFsMIStdBfdSessTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdFsMIStdBfdSessEntry);
            return NULL;
        }
        return pBfdFsMIStdBfdSessEntry;
    }
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessDiscMapTableCreateApi
 Input       :  pBfdFsMIStdBfdSessDiscMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tBfdFsMIStdBfdSessDiscMapEntry *
BfdFsMIStdBfdSessDiscMapTableCreateApi (tBfdFsMIStdBfdSessDiscMapEntry *
                                        pSetBfdFsMIStdBfdSessDiscMapEntry)
{
    tBfdFsMIStdBfdSessDiscMapEntry *pBfdFsMIStdBfdSessDiscMapEntry = NULL;

    if (pSetBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessDiscMapTableCreatApi: pSetBfdFsMIStdBfdSessDiscMapEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pBfdFsMIStdBfdSessDiscMapEntry =
        (tBfdFsMIStdBfdSessDiscMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID);
    if (pBfdFsMIStdBfdSessDiscMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessDiscMapTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pBfdFsMIStdBfdSessDiscMapEntry,
                pSetBfdFsMIStdBfdSessDiscMapEntry,
                sizeof (tBfdFsMIStdBfdSessDiscMapEntry));
        if (RBTreeAdd
            (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessDiscMapTable,
             (tRBElem *) pBfdFsMIStdBfdSessDiscMapEntry) != RB_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdFsMIStdBfdSessDiscMapTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSDISCMAPTABLE_POOLID,
                                (UINT1 *) pBfdFsMIStdBfdSessDiscMapEntry);
            return NULL;
        }
        return pBfdFsMIStdBfdSessDiscMapEntry;
    }
}

/****************************************************************************
 Function    :  BfdFsMIStdBfdSessIpMapTableCreateApi
 Input       :  pBfdFsMIStdBfdSessIpMapEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tBfdFsMIStdBfdSessIpMapEntry *
BfdFsMIStdBfdSessIpMapTableCreateApi (tBfdFsMIStdBfdSessIpMapEntry *
                                      pSetBfdFsMIStdBfdSessIpMapEntry)
{
    tBfdFsMIStdBfdSessIpMapEntry *pBfdFsMIStdBfdSessIpMapEntry = NULL;

    if (pSetBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessIpMapTableCreatApi: pSetBfdFsMIStdBfdSessIpMapEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pBfdFsMIStdBfdSessIpMapEntry =
        (tBfdFsMIStdBfdSessIpMapEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID);
    if (pBfdFsMIStdBfdSessIpMapEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdFsMIStdBfdSessIpMapTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pBfdFsMIStdBfdSessIpMapEntry, pSetBfdFsMIStdBfdSessIpMapEntry,
                sizeof (tBfdFsMIStdBfdSessIpMapEntry));
        if (RBTreeAdd
            (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
             (tRBElem *) pBfdFsMIStdBfdSessIpMapEntry) != RB_SUCCESS)
        {
            BFD_TRC ((BFD_UTIL_TRC,
                      "BfdFsMIStdBfdSessIpMapTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSIPMAPTABLE_POOLID,
                                (UINT1 *) pBfdFsMIStdBfdSessIpMapEntry);
            return NULL;
        }
        return pBfdFsMIStdBfdSessIpMapEntry;
    }
}
