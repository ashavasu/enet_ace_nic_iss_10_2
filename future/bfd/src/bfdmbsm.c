/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdmbsm.c,v 1.9 2014/04/29 13:28:37 siva Exp $
 *
 * Description: This file contains functions for handling card insertion 
 *              and removal in a chassis system.
 *****************************************************************************/
#ifndef _BFDMBSM_C_
#define _BFDMBSM_C_

#include "bfdinc.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 BfdMbsmUpdateCardInsertOrRemove PROTO ((tMbsmSlotInfo *,
                                                     tMbsmPortInfo *, UINT4));
PRIVATE INT4 BfdMbsmHandleCardInsertion PROTO ((tBfdFsMIStdBfdSessEntry *));

PRIVATE INT4 BfdMbsmHandleCardRemoval PROTO ((tBfdFsMIStdBfdSessEntry *));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : BfdMbsmPostMessage 
 *                                                                          
 *    DESCRIPTION      : Allocates a CRU buffer and enqueues the Line card
 *                       change status to the BFD Task
 *
 *    INPUT            : pProtoMsg - Contains the Slot and Port Information 
 *                       i4Event  - Line card Up/Down status          
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
BfdMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tBfdReqParams      *pMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    MEMSET (&(MbsmProtoAckMsg), 0, sizeof (tMbsmProtoAckMsg));

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }

    if (gBfdGlobals.u1IsBfdInitialized == OSIX_FALSE)
    {
        BFD_LOG (BFD_INVALID_CONTEXT, BFD_TRC_TASK_NOT_INITIALISED,
                 "BfdMbsmPostMessage");
        /* If the module is not initialized no need to proceed further, 
         * send the ack to MBSM so that other modules get the LcStatus 
         * Notification. The ack is sent only for hardware updation to 
         * be in synchronization across the dependant modules. 
         */
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_FAILURE;
    }

    if ((pMsg = (tBfdReqParams *) MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
    {
        /* BFD_ISS_LOG (u4ContextId, 23, "BfdMbsmPostMessage"); */
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tBfdReqParams));
    pMsg->u4ReqType = (UINT4) i4Event;

    if ((pMsg->unReqInfo.pMbsmProtoMsg =
         MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)) == NULL)
    {
        BFD_LOG (BFD_INVALID_CONTEXT, BFD_TRC_MEM_ALLOC_MBSM_FAILED,
                 "BfdMbsmPostMessage");
        MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pMsg);
        return MBSM_FAILURE;
    }

    /* Copy the MBSM message */
    MEMCPY (pMsg->unReqInfo.pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));

    if (BfdQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        /* BFD_LOG (200, "BfdMbsmPostMessage"); */
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : BfdMbsmHandleLCEvent
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the BFD  
 *                       software configuration. 
 *                       This function will be called from the BFD module
 *                       when an indication for the Card Insertion/Removal   
 *                       is received from the MBSM. 
 *
 *    INPUT            : pBuf  - Buffer containing the protocol message 
 *                               information 
 *                       u4Cmd - Line card - Insert/Remove
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
BfdMbsmHandleLCEvent (tMbsmProtoMsg * pProtoMsg, UINT4 u4Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmProtoAckMsg    ProtoAckMsg;

    if ((u4Cmd == MBSM_MSG_CARD_INSERT) || (u4Cmd == MBSM_MSG_CARD_REMOVE))
    {
        if (ISS_GET_STACKING_MODEL () != ISS_CTRL_PLANE_STACKING_MODEL)
        {
            i4RetStatus = BfdMbsmUpdateCardInsertOrRemove
                (&(pProtoMsg->MbsmSlotInfo), &(pProtoMsg->MbsmPortInfo), u4Cmd);
        }
    }

    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */
    ProtoAckMsg.i4RetStatus = i4RetStatus;
    ProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
    ProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmSendAckFromProto (&ProtoAckMsg);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : BfdMbsmUpdateCardInsertOrRemove
 *                                                                          
 *    DESCRIPTION      : This function used to perform the necessary actions 
 *                       in HW and SW  when an indication for the 
 *                       Card Insertion/Card Removal is received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Inserted port list     
 *                       pSlotInfo - Information about inserted slot
 *                       u4Event   - MBSM_MSG_CARD_INSERT / 
 *                                   MBSM_MSG_CARD_REMOVE
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
BfdMbsmUpdateCardInsertOrRemove (tMbsmSlotInfo * pSlotInfo,
                                 tMbsmPortInfo * pPortInfo, UINT4 u4Event)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pNextGblConfig = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pGblConfig = NULL;
    tBfdFsMIStdBfdSessEntry *pNextSess = NULL;
    tBfdFsMIStdBfdSessEntry *pSess = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = 0;
    UNUSED_PARAM (pPortInfo);

    if (OSIX_FALSE != MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        return OSIX_SUCCESS;
    }

    /* Get First node in global database */
    pNextGblConfig = BfdGetFirstFsMIStdBfdGlobalConfigTable ();
    if (pNextGblConfig == NULL)
    {
        /* No entries in the database */
        return OSIX_FAILURE;
    }
    do
    {
        pGblConfig = pNextGblConfig;
        /* Get First node in context database */
        pNextSess = BfdGetFirstFsMIStdBfdSessTable ();
        if (pNextSess == NULL)
        {
            /* No entries in the database */
            continue;
        }
        do
        {
            pSess = pNextSess;
            if (pSess->MibObject.i4FsMIStdBfdSessRowStatus != ACTIVE)
            {
                continue;
            }
            if (pSess->MibObject.u4FsMIBfdSessCardNumber !=
                (UINT4) pSlotInfo->i4CardType ||
                pSess->MibObject.u4FsMIBfdSessSlotNumber !=
                (UINT4) pSlotInfo->i4SlotId)
            {
                continue;
            }
            if (u4Event == MBSM_MSG_CARD_INSERT)
            {
                i4RetVal = BfdMbsmHandleCardInsertion (pSess);
            }
            else
            {
                i4RetVal = BfdMbsmHandleCardRemoval (pSess);
            }
            u4ContextId = pSess->MibObject.u4FsMIStdBfdContextId;
            if (i4RetVal == OSIX_FAILURE)
            {
                BFD_LOG (u4ContextId, BFD_TRC_CARD_ATTACH_DETACH_FAILED,
                         "BfdMbsmUpdateCardInsertOrRemove");
                return MBSM_FAILURE;
            }
        }
        while ((pNextSess = BfdGetNextFsMIStdBfdSessTable (pSess)) != NULL);
    }
    while ((pNextGblConfig =
            BfdGetNextFsMIStdBfdGlobalConfigTable (pGblConfig)) != NULL);
    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : BfdMbsmHandleCardInsertion
 *                                                                          
 *    DESCRIPTION      : This function handles the Card Insertion event.
 *
 *    INPUT            : pSess     - Pointer to the Session information.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
BfdMbsmHandleCardInsertion (tBfdFsMIStdBfdSessEntry * pSess)
{
    BFD_LOG (pSess->MibObject.u4FsMIStdBfdContextId,
             BFD_TRC_ENTERED_SESSION_IDX, "BfdMbsmHandleCardInsertion",
             pSess->MibObject.u4FsMIStdBfdSessIndex);

    if (BfdSessReactBfdSessEntry (pSess) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : BfdMbsmHandleCardRemoval
 *                                                                          
 *    DESCRIPTION      : This function handles the Card Removal event.
 *
 *    INPUT            : pSess     - Pointer to the Session information.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
BfdMbsmHandleCardRemoval (tBfdFsMIStdBfdSessEntry * pSess)
{
    BFD_LOG (pSess->MibObject.u4FsMIStdBfdContextId,
             BFD_TRC_ENTERED_SESSION_IDX, "BfdMbsmHandleCardRemoval",
             pSess->MibObject.u4FsMIStdBfdSessIndex);

    if (BfdSessDeactBfdSessEntry (pSess, OSIX_TRUE) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *FUNCTION NAME    : BfdMbsmHandleCardInfoUpdate
 *                                                                      
 *DESCRIPTION      : This function handles the Card information update
 *
 *INPUT            : u4ContextId - Contextid of the session.
 *                   pBfdMbsmParams - pointer to MBSM parameters
 *                                                                      
 *OUTPUT           : None
 *                                                                      
 *RETURNS          : None
 ****************************************************************************/
VOID
BfdMbsmHandleCardInfoUpdate (UINT4 u4ContextId, tBfdMbsmParams * pBfdMbsmParams)
{
    tBfdFsMIStdBfdSessEntry *pSessEntry = NULL;

    if (pBfdMbsmParams == NULL)
    {
        return;
    }

    pSessEntry = BfdUtilGetBfdSessTable (u4ContextId,
                                         pBfdMbsmParams->u4SessionId);

    if (pSessEntry == NULL)
    {
        return;
    }

    if (pBfdMbsmParams->u4CardNum != BFD_INVALID_CARD_SLOT_NUM)
    {
        pSessEntry->MibObject.u4FsMIBfdSessCardNumber =
            pBfdMbsmParams->u4CardNum;
    }

    if (pBfdMbsmParams->u4SlotId != BFD_INVALID_CARD_SLOT_NUM)
    {
        pSessEntry->MibObject.u4FsMIBfdSessSlotNumber =
            pBfdMbsmParams->u4SlotId;
    }
    return;
}

#endif /* _BFDMBSM_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdmbsm.c                      */
/*-----------------------------------------------------------------------*/
