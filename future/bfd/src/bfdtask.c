/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdtask.c,v 1.9 2012/04/16 14:11:55 siva Exp $
 *
 * Description:This file contains procedures related to
 *             BFD - Task Initialization
 *******************************************************************/

#include "bfdinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdTaskRegisterBfdMibs()                                  */
/*                                                                           */
/* Description  : This procedure is provided to register BFD mibs            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
BfdTaskRegisterBfdMibs ()
{
    RegisterFSMSBF ();
    RegisterFSMPBF ();

    if (BfdExtGetSystemModeExt () == BFD_SI_MODE)
    {
        RegisterSTDBFD ();
        RegisterFSBFD ();
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdTaskUnRegisterBfdMibs()                                 */
/*                                                                           */
/* Description  : This procedure is provided to unregister BFD mibs          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
BfdTaskUnRegisterBfdMibs ()
{
    UnRegisterFSMSBF ();
    UnRegisterFSMPBF ();

    if (BfdExtGetSystemModeExt () == BFD_SI_MODE)
    {
        UnRegisterSTDBFD ();
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdTaskSpawnBfdTask()                                      */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Bfd Task               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if BFD Task is successfully spawned          */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
BfdTaskSpawnBfdTask (INT1 *pi1Arg)
{
    UNUSED_PARAM (pi1Arg);

    BFD_TRC_FUNC ((BFD_FN_ENTRY, "FUNC:BfdTaskSpawnBfdTask\n"));

    /* task initializations */
    if (BfdMainTaskInit () == OSIX_FAILURE)
    {
        BFD_TRC ((BFD_TASK_TRC, " !!!!! BFD TASK INIT FAILURE  !!!!! \n"));
        BfdMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (BfdMainRegisterWithVcm () != OSIX_SUCCESS)
    {
        BFD_TRC ((BFD_TASK_TRC, " !!!!! BFD TASK INIT FAILURE  !!!!! \n"));
        BfdMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (OsixGetTaskId (SELF, (UINT1 *) BFD_TASK_NAME, &(gBfdGlobals.bfdTaskId))
        == OSIX_FAILURE)
    {
        BFD_TRC ((BFD_TASK_TRC, " !!!!! BFD TASK INIT FAILURE  !!!!! \n"));
        BfdMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    lrInitComplete (OSIX_SUCCESS);
    BfdMainTask ();
    BFD_TRC_FUNC ((BFD_FN_EXIT, "EXIT:BfdTaskSpawnBfdTask\n"));
    return;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  bfdtask.c                     */
/*------------------------------------------------------------------------*/
