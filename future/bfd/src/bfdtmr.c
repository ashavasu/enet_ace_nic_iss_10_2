/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdtmr.c,v 1.24 2015/09/11 09:50:44 siva Exp $
 *
 * Description : This file contains procedures containing Bfd Timer
 *               related operations
 *****************************************************************************/
#ifndef __BFDTMR_C__
#define __BFDTMR_C__

#include "bfdinc.h"

PRIVATE tBfdTmrDesc gaBfdTmrDesc[BFD_MAX_TMRS];

/* Prototypes of the functions private to this file only */
PRIVATE VOID BfdTmrTxTmrExp PROTO ((VOID *));
PRIVATE VOID BfdTmrDetectTmrExp PROTO ((VOID *));
PRIVATE VOID BfdTmrAddrCompTmrExp PROTO ((VOID *));


/****************************************************************************
*                                                                           *
* Function     : BfdTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Bfd Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

VOID
BfdTmrInitTmrDesc ()
{
    /* Tx Timer */
    gaBfdTmrDesc[BFD_TRANSMIT_TMR].pTmrExpFunc = (VOID *) BfdTmrTxTmrExp;
    gaBfdTmrDesc[BFD_TRANSMIT_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tBfdFsMIStdBfdSessEntry, TxTimer);

    /* Detection Timer */
    gaBfdTmrDesc[BFD_DETECTION_TMR].pTmrExpFunc = (VOID *) BfdTmrDetectTmrExp;
    gaBfdTmrDesc[BFD_DETECTION_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tBfdFsMIStdBfdSessEntry, DetectTimer);

    gaBfdTmrDesc[BFD_ADDR_COMP_TMR].pTmrExpFunc = (VOID *) BfdTmrAddrCompTmrExp;
    gaBfdTmrDesc[BFD_ADDR_COMP_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tBfdFsMIStdBfdSessEntry, BfdAddrCompTmr);
    

}

/****************************************************************************
*                                                                           *
* Function     : BfdTmrDeInitTmrDesc                                         *
*                                                                           *
* Description  : DeInitialize Bfd Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
VOID
BfdTmrDeInitTmrDesc ()
{
    MEMSET (gaBfdTmrDesc, 0, sizeof (gaBfdTmrDesc));
}

/****************************************************************************
*                                                                           *
* Function     : BfdTmrStartTmr                                             *
*                                                                           *
* Description  : Starts Bfd Timer                                           *
*                                                                           *
* Input        : pBfdTmr - pointer to BfdTmr structure                      *
*                eBfdSubTmrId - BFD Sub timer ID                            *
*                u4MicroSecs     - ticks in microseconds                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
BfdTmrStartTmr (tBfdTmr * pBfdTmr, tBfdSubTmrId eBfdSubTmrId, UINT4 u4Intrvl)
{
    UINT4               u4Tmr = 0;
    pBfdTmr->eBfdSubTmrId = eBfdSubTmrId;

    u4Tmr = BFD_MICROSEC_TO_MILLISEC (u4Intrvl);

    if (TmrStart (gBfdGlobals.bfdTmrLst, &(pBfdTmr->BfdTmrBlk),
                  (UINT1) pBfdTmr->BfdTmrBlk.u1TimerId, 0,
                  u4Tmr) == TMR_FAILURE)
    {
        /*BFD_ISS_LOG (BFD_INVALID_CONTEXT_ID, 30, "BFD TIMER",
           pBfdTmr->eBfdSubTmrId); */
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : BfdTmrRestart                                           *
*                                                                           *
* Description  :  ReStarts Bfd Timer                                        *
*                                                                           *
* Input        : pBfdTmr - pointer to BfdTmr structure                      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
BfdTmrReStart (tBfdFsMIStdBfdSessEntry * pBfdSessEntry, tBfdTmrId eBfdTmrId,
               tBfdSubTmrId eBfdSubTmrId)
{
    if (eBfdTmrId == BFD_TRANSMIT_TMR)
    {
        TmrStopTimer (gBfdGlobals.bfdTmrLst,
                      &(pBfdSessEntry->TxTimer.BfdTmrBlk.TimerNode));
    }
    else
    {
        TmrStopTimer (gBfdGlobals.bfdTmrLst,
                      &(pBfdSessEntry->DetectTimer.BfdTmrBlk.TimerNode));
    }
    return BfdTmrStart (pBfdSessEntry, eBfdTmrId, eBfdSubTmrId);
}

/****************************************************************************
*                                                                           *
* Function     : BfdTmrStop                                                 *
*                                                                           *
* Description  : Stops Bfd Timer                                            *
*                                                                           *
* Input        : pBfdSessEntry - pointer to session entry table             *
*                u1BfdTmrType -> Timer type                                 *  
*                (BFD_DETECTION_TMR, BFD_TRANSMIT_TMR)                      *
*                                                                           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
BfdTmrStop (tBfdFsMIStdBfdSessEntry * pBfdSessEntry, tBfdTmrId eBfdTmrId)
{
    tBfdTmr            *pBfdTmr = NULL;
    if (eBfdTmrId == BFD_TRANSMIT_TMR)
    {
        pBfdTmr = &pBfdSessEntry->TxTimer;
    }
    else if (eBfdTmrId == BFD_DETECTION_TMR)
    {
        pBfdTmr = &pBfdSessEntry->DetectTimer;
    }
    else
    {
        return OSIX_FAILURE;
    }

    if (TmrStopTimer (gBfdGlobals.bfdTmrLst, &(pBfdTmr->BfdTmrBlk.TimerNode)) ==
        TMR_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdTmrJitter
 * Description :  This function will apply jitter to the Timing parameters
 * Input       :  *pu4TimeIntvl-> time interval to be jittered
 *                u4DetectMult ->based on detect multiplier, jitter is appied
 *               
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
VOID
BfdTmrJitter (UINT4 *pu4TimeIntvl, UINT4 u4DetectMult)
{
    PRIVATE UINT1       u1Range75To100 = BFD_JITTER_LOWER_RANGE;
    PRIVATE UINT1       u1Range75To90 = BFD_JITTER_LOWER_RANGE;
    PRIVATE UINT1       u1Range75To90Adjust = 0;
    PRIVATE UINT1       u1Range75To100Adjust = 0;

    if (u4DetectMult == 1)
    {
        if (u1Range75To90 > BFD_JITTER_HIGHER_RANGE_90)
        {
            u1Range75To90 = BFD_JITTER_LOWER_RANGE;
        }
        if (u1Range75To90Adjust > BFD_JITTER_ADJUST)
        {
            u1Range75To90Adjust = 0;
        }
        *pu4TimeIntvl = (BFD_JITTER_PERCENT (*pu4TimeIntvl)) * u1Range75To90;
        u1Range75To90 = (UINT1) u1Range75To90 + u1Range75To90Adjust;
        u1Range75To90Adjust++;
    }
    else
    {
        if (u1Range75To100 > BFD_JITTER_HIGHER_RANGE_100)
        {
            u1Range75To100 = BFD_JITTER_LOWER_RANGE;
        }
        if (u1Range75To100Adjust > BFD_JITTER_ADJUST)
        {
            u1Range75To100Adjust = 0;
        }
        *pu4TimeIntvl = (BFD_JITTER_PERCENT (*pu4TimeIntvl)) * u1Range75To100;
        u1Range75To100 = (UINT1) u1Range75To100 + u1Range75To100Adjust;
        u1Range75To100Adjust++;
    }
}

/****************************************************************************
 * Function    :  BfdTmrStart
 * Description :  This function is a wrapper for the request to timer module
 *                to start/restart a timer
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                u1BfdTmrType -> Timer type 
 *                (BFD_DETECTION_TMR, BFD_TRANSMIT_TMR)
 *                u1BfdSubTmrType -> Signifies the sub timer type to be run for
 *                this timer.
 *                (BFD_TRANSMIT_SLOW_TMR,BFD_TRANSMIT_FAST_TMR,
 *                BFD_DETECTION_SLOW_TMR,BFD_DETECTION_FAST_TMR,
 *                BFD_DEFECT_EXIT_TMR)
 *
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdTmrStart (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
             tBfdTmrId eBfdTmrId, tBfdSubTmrId eBfdSubTmrId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTableEntry = NULL;
    UINT4               u4TimeIntvl = 0;
    UINT4               u4ContextId = 0;
    tBfdTmr            *pBfdTmr = NULL;

    u4ContextId = pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
    if (BFD_TRANSMIT_TMR == eBfdTmrId)
    {
        pBfdTmr = &pBfdSessEntry->TxTimer;
        pBfdTmr->BfdTmrBlk.u1TimerId = BFD_TRANSMIT_TMR;

        if (BFD_TRANSMIT_SLOW_TMR == eBfdSubTmrId)
        {

            /* To be provided by tool */
            pBfdGlobalConfigTableEntry =
                BfdUtilGetBfdGlobalConfigTable (u4ContextId);

            if (pBfdGlobalConfigTableEntry == NULL)
            {
                BFD_LOG (u4ContextId, BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED);
                return OSIX_FAILURE;
            }

            pBfdTmr->eBfdSubTmrId = BFD_TRANSMIT_SLOW_TMR;
            /*Start the slow timer by taking down the timer from globals */
            u4TimeIntvl =
                pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl;
#ifdef BFD_HA_TEST_WANTED
            pBfdSessEntry->i4BfdTestSessCurrTmr = BFD_TRANSMIT_SLOW_TMR;
#endif

        }
        else if (BFD_TRANSMIT_FAST_TMR == eBfdSubTmrId)
        {
            pBfdTmr->eBfdSubTmrId = BFD_TRANSMIT_FAST_TMR;
            u4TimeIntvl =
                pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval;
#ifdef BFD_HA_TEST_WANTED
            if (BFD_TRANSMIT_FAST_TMR != pBfdSessEntry->i4BfdTestSessCurrTmr)
            {
                pBfdSessEntry->i4BfdTestSessPrevTmr =
                    pBfdSessEntry->i4BfdTestSessCurrTmr;
                pBfdSessEntry->i4BfdTestSessCurrTmr = BFD_TRANSMIT_FAST_TMR;
            }
#endif
        }
        else
        {
            return OSIX_FAILURE;
        }
        BfdTmrJitter (&u4TimeIntvl, BFD_SESS_DETECT_MULT (pBfdSessEntry));
    }
    else if (BFD_DETECTION_TMR == eBfdTmrId)    /* Detection timer */
    {
        pBfdTmr = &pBfdSessEntry->DetectTimer;
        pBfdTmr->BfdTmrBlk.u1TimerId = BFD_DETECTION_TMR;
        if ((eBfdSubTmrId == BFD_DETECTION_SLOW_TMR) ||
            (eBfdSubTmrId == BFD_DETECTION_OFFLOAD_TMR))
        {
            /* To be provided by tool */
            pBfdGlobalConfigTableEntry =
                BfdUtilGetBfdGlobalConfigTable (u4ContextId);

            if (pBfdGlobalConfigTableEntry == NULL)
            {
                BFD_LOG (u4ContextId, BFD_TRC_GLOBAL_CONFIG_FETCH_FAILED);
                return OSIX_FAILURE;
            }

            pBfdTmr->eBfdSubTmrId = eBfdSubTmrId;
            /*Start the slow timer by taking down the timer from globals */
            u4TimeIntvl =
                pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl;

            if (eBfdSubTmrId != BFD_DETECTION_OFFLOAD_TMR)
            {
                /* Multiply with detect multiplier to get the interval */
                u4TimeIntvl *= BFD_SESS_NEG_DETECT_MULT (pBfdSessEntry);
            }
        }
        else if (eBfdSubTmrId == BFD_DETECTION_FAST_TMR)
        {
            pBfdTmr->eBfdSubTmrId = BFD_DETECTION_FAST_TMR;
            u4TimeIntvl = pBfdSessEntry->u4NegRemoteTxTimeIntrvl;

            /* Multiply with detect multiplier to get the interval */
            u4TimeIntvl *= BFD_SESS_NEG_DETECT_MULT (pBfdSessEntry);
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Invalid Timer */
        return OSIX_FAILURE;
    }

    /* If the Time interval is Zero, Dont start the timer */
    if (u4TimeIntvl != 0)
    {
        return (BfdTmrStartTmr (pBfdTmr, eBfdSubTmrId, u4TimeIntvl));
    }
    else
    {
        return OSIX_SUCCESS;
    }
}

/****************************************************************************
*                                                                           *
* Function     : BfdTmrHandleExpiry                                         *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
BfdTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    tBfdTmrId           eBfdTmrId = BFD_TRANSMIT_TMR;
    INT2                i2Offset = 0;

    while ((pExpiredTimers = TmrGetNextExpiredTimer (gBfdGlobals.bfdTmrLst))
           != NULL)
    {
        eBfdTmrId = ((tBfdTmr *) pExpiredTimers)->BfdTmrBlk.u1TimerId;

        i2Offset = gaBfdTmrDesc[eBfdTmrId].i2Offset;

        if (i2Offset < 0)
        {
            /* The timer function does not take any parameter */
            (*(gaBfdTmrDesc[eBfdTmrId].pTmrExpFunc)) (NULL);
        }
        else
        {
            /* The timer function requires a parameter */
            (*(gaBfdTmrDesc[eBfdTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    return;
}

/****************************************************************************
 *                                                                           *
 * Function     : BfdTmrTxTmrExp                                             *
 *                                                                           *
 * Description  : This function will be invoked in the case of Tx timer      *
 *                expiry. It will performs the operations needed on the      *
 *                expiry of the Tx  timer                                    *
 *                                                                           *
 * Input        : pointer to the SessionEntry  table                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/

PRIVATE VOID
BfdTmrTxTmrExp (VOID *pArg)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessionEntry =
        (tBfdFsMIStdBfdSessEntry *) pArg;

    if (pBfdSessionEntry == NULL)
    {
        return;
    }
    BfdSessChkAndTransmitPkt (pBfdSessionEntry);
    return;
}

 /****************************************************************************
 *                                                                           *
 * Function     : BfdTmrDetectTmrExp                                         *
 *                                                                           *
 * Description  : This function will be invoked in the case of Detect timer  *
 *                expiry. It will performs the operations needed on the      *
 *                expiry of the detect timer                                 *
 *                                                                           *
 * Input        : pointer to the SessionEntry  table                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/

PRIVATE VOID
BfdTmrDetectTmrExp (VOID *pArg)
{
    UINT4               u4ContextId = 0;
    UINT1               u1PrevSemState = BFD_SESS_STATE_DOWN;
    tBfdFsMIStdBfdSessEntry *pBfdSessionEntry =
        (tBfdFsMIStdBfdSessEntry *) pArg;
    if (pBfdSessionEntry == NULL)
    {
        return;
    }
    u4ContextId = pBfdSessionEntry->MibObject.u4FsMIStdBfdContextId;
    UNUSED_PARAM (u4ContextId);
    u1PrevSemState = BFD_SESS_STATE (pBfdSessionEntry);

    if (pBfdSessionEntry->DetectTimer.eBfdSubTmrId != BFD_DETECTION_OFFLOAD_TMR)
    {
        BfdCoreTriggerBfdSem (pBfdSessionEntry, BFD_SEM_REQ_TMR_EXP, NULL);

        if (u1PrevSemState != BFD_SESS_STATE (pBfdSessionEntry))
        {
            BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                    &(pBfdSessionEntry->MibObject.SessDbNode));
            BfdRedSyncDynInfo ();
        }

        return;
    }

    /*This is a offload timer, call the create offload session again */
    BfdCoreCreateOffldSess (pBfdSessionEntry, NULL);
    return;
}

PRIVATE VOID
BfdTmrAddrCompTmrExp(VOID *pArg)
{
	tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntryExist = NULL;
	tBfdFsMIStdBfdSessIpMapEntry BfdSessIpMapEntry;
	tBfdFsMIStdBfdSessEntry *pBfdSessEntry = (tBfdFsMIStdBfdSessEntry *) pArg;
	UINT1 au1FsMIStdBfdSessSrcAddr[BFD_IPV6_MAX_ADDR_LEN];


	MEMSET(au1FsMIStdBfdSessSrcAddr, 0, BFD_IPV6_MAX_ADDR_LEN);
   MEMSET(&BfdSessIpMapEntry,0, sizeof(tBfdFsMIStdBfdSessIpMapEntry));


		if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType ==  BFD_INET_ADDR_IPV6)
		{
			if ((pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen == 0) ||
					(MEMCMP(pBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
							au1FsMIStdBfdSessSrcAddr, BFD_IPV6_MAX_ADDR_LEN) == 0))
			{
				if (BfdUtilGetSrcAddrFromIfIndex
						((UINT4) pBfdSessEntry->MibObject.
						 i4FsMIStdBfdSessInterface,
						 (UINT1) pBfdSessEntry->MibObject.
						 i4FsMIStdBfdSessDstAddrType,
						 pBfdSessEntry->MibObject.
						 au1FsMIStdBfdSessSrcAddr,
						 pBfdSessEntry->MibObject.
						 au1FsMIStdBfdSessDstAddr) == OSIX_SUCCESS)
				{

					pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen =
						pBfdSessEntry->MibObject.
						i4FsMIStdBfdSessDstAddrLen;

					pBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType =
						pBfdSessEntry->MibObject.
						i4FsMIStdBfdSessDstAddrType;

					BfdSessIpMapEntry.MibObject.u4FsMIStdBfdContextId = pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

					BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessInterface =
						pBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;



					MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessSrcAddr),
							au1FsMIStdBfdSessSrcAddr, BFD_IPV6_MAX_ADDR_LEN);

					BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrLen =
						0;
					BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrType =
						0;

					MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessDstAddr),
							pBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr, BFD_IPV6_MAX_ADDR_LEN);

					BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrLen =
						BFD_IPV6_MAX_ADDR_LEN;
					BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrType =
						BFD_INET_ADDR_IPV6;
					/* Check whether the node is already present */
					pBfdSessIpMapEntryExist =
						RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
								(tRBElem *) & BfdSessIpMapEntry);

					if (pBfdSessIpMapEntryExist != NULL)
					{
						MEMCPY(pBfdSessIpMapEntryExist->MibObject.au1FsMIStdBfdSessSrcAddr,
								pBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr,
								BFD_IPV6_MAX_ADDR_LEN);
						pBfdSessIpMapEntryExist->MibObject.i4FsMIStdBfdSessSrcAddrLen  = BFD_IPV6_MAX_ADDR_LEN;
						pBfdSessIpMapEntryExist->MibObject.i4FsMIStdBfdSessSrcAddrType  = BFD_INET_ADDR_IPV6;


					}




				}

			}
		}


}





#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdtmr.c                      */
/*-----------------------------------------------------------------------*/
