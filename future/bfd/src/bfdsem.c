/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdsem.c,v 1.34 2015/08/06 05:37:50 siva Exp $
 *
 * Description: This file contains the BFD SEM functionality
 *******************************************************************/

#ifndef __BFDSEM_C__
#define __BFDSEM_C__

#include "bfdinc.h"
#include "bfdsem.h"

/****************************************************************************
 * Function    :  BfdSemTrigger
 * Description :  This function is used to trigger the SEM for a BFD session 
 * Input       :  pBfdSessEntry - pointer to BFD session entry
 *                pBfdRxPktInfo - pointer to received packet information
 *                u1Event - Event due to which the SEM is to be triggered
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemTrigger (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
               UINT1 u1Event, tBfdPktInfo * pBfdRxPktInfo)
{
    if (pBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u1Event >= BFD_MAX_SEM_EVENTS)
    {
        return OSIX_FAILURE;
    }
    /* Trigger state machine */
    (*BFD_SESS_SEM[(BFD_SESS_STATE (pBfdSessEntry))][u1Event])
        (pBfdSessEntry, pBfdRxPktInfo);

    BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
             BFD_TRC_SEM_TRIGGERED,
             pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemResetSemVariables
 * Description :  This function is used to handle SEM init/reinit 
 * Input       :  pBfdSessEntry - pointer to BFD session entry
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC VOID
BfdSemResetSemVariables (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    /* Initialize/Reset SEM state to DOWN */
    BFD_SESS_STATE (pBfdSessEntry) = BFD_SESS_STATE_DOWN;
    BFD_SESS_DIAG (pBfdSessEntry) = BFD_DIAG_NO_DIAG;
    BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);

    BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId, BFD_TRC_SEM_INIT,
             pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
             (BFD_SESS_STATE (pBfdSessEntry)));

}

/****************************************************************************
 * Function    :  BfdSemNoAction
 * Description :  This function is used when no action needs to be taken
 *                in a state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemNoAction (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdSessEntry);
    UNUSED_PARAM (pBfdRxPktInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemAdminDownHandleDetnTmrExp
 * Description :  This function is used to handle Detection Timer expiry when 
 *                BFD session is in ADMIN DOWN state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemAdminDownHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                                 tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);
    /* Detection Timer expiring in Admin Down state should only happen once 
     * after Admin Down was set last */
    if (OSIX_SUCCESS == BfdTmrStop (pBfdSessEntry, BFD_TRANSMIT_TMR))
    {
        /* clear the dynamic params learned */
        BfdSessDelSessEntryDynParams (pBfdSessEntry);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemAdminDownHandlePathStChg
 * Description :  This function is used to handle status change of the path 
 being monitored by this BFD session when the session is 
 *                in Admin Down state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD control packet 
 *                information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemAdminDownHandlePathStChg (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                                tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);
    /* Stop all the timers */
    BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
    BfdTmrStop (pBfdSessEntry, BFD_TRANSMIT_TMR);

    /* delete the dynamic learned entries */
    BfdSessDelSessEntryDynParams (pBfdSessEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemHandleLocalSessAdminDown
 * Description :  This function is used to handle session Admin Down request
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemHandleLocalSessAdminDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                                tBfdPktInfo * pBfdRxPktInfo)
{
    INT4                i4BfdPrevState = 0;

    UNUSED_PARAM (pBfdRxPktInfo);

    i4BfdPrevState = BFD_SESS_STATE (pBfdSessEntry);

    if (BFD_SESS_STATE_ADMIN_DOWN != i4BfdPrevState)
    {

        /* Go to Down state to inform Ext modules and then move 
         * to admin down */
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_ADMIN_DOWN, OSIX_FALSE);

        /* Start/Restart (in case the timer was already running)  
         * transmit interval for one detection time interval */

        if (OSIX_SUCCESS ==
            BfdTmrReStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                           BFD_TRANSMIT_SLOW_TMR))
        {
            if (OSIX_SUCCESS !=
                BfdTmrReStart (pBfdSessEntry, BFD_DETECTION_TMR,
                               BFD_DETECTION_SLOW_TMR))
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_DTMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }
        }
        else
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_TX_TMR_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemHandleLocalSessAdminUp
 * Description :  This function is used to handle session Admin Up request
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemHandleLocalSessAdminUp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                              tBfdPktInfo * pBfdRxPktInfo)
{
    INT4                i4BfdPrevState = 0;
    INT4                i4RetCode = OSIX_SUCCESS;
    UINT4               u4RemainingTime = 0;
    UNUSED_PARAM (pBfdRxPktInfo);

    i4BfdPrevState = BFD_SESS_STATE (pBfdSessEntry);

    if (BFD_SESS_STATE_ADMIN_DOWN == i4BfdPrevState)
    {
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             DetectTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        if (u4RemainingTime > 0)
        {
            /* Timer is still running, dynamic parameters are still
             * available */
            i4RetCode = BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
        }
        BFD_SESS_STATE (pBfdSessEntry) = BFD_SESS_STATE_DOWN;
        BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);

        /* Start/ Restart (if the timer is already running) the transmit
         * timer to SLOW_INTVL */

        /* Incase of offload, transmit timer will take care of checking for
         * discriminator availability and initiating a bootstrap. When
         * a valid full packet is received, offload is done again in 
         * Rx flow */

        i4RetCode = BfdTmrReStart
            (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR);

        if (OSIX_SUCCESS != i4RetCode)
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_TX_TMR_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
        }
    }
    return i4RetCode;
}

/****************************************************************************
 * Function    :  BfdSemDownHandleDetnTmrExp
 * Description :  This function is used to handle Detectiom Timer expiry when
 *                BFD session is in DOWN state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                            tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);

    /* Detection timer should only be running if the session is in 
     * misconnectivity defect. This timer will start when a valid packet 
     * is received to satisfy defect exit criteria */
    /*
       Detection timer can run in the DOWN state for checking DefectExit
       Criteria if the session had entered into defect condition. 
       DefectExit Criteria handling:
       - The session enters into misconnectivity defect condition on receving a
       BFD packet with misconnetivity defect
       - When session receives a first  valid BFD packet without misconnectivity
       defect, starts defect exit criteria with the DetectionTimer for Slow-duration. 
       - This is to check if within this period of time a valid BFD packets are
       received or not
       - If no more misconnectivity defected BFD packets are received, then
       sesssion comes out of defect condition.
       - If any misconnectivity defected BFD packets are received within the
       DetectionTimer (slowtime) duration, then DefectExit criteria is
       not met. Hence timer is stopped and be in the misconnectivity
       defect state.
     */

    if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
    {
        /* Mis connectivity defect has cleared, session is free to 
         * transition states on next packet onwards */
        pBfdSessEntry->bMisConnectDefect = OSIX_FALSE;

        /*BFD_TRC (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,92,
           pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex); */
    }
    else
    {
        /* Detection Timer Expiry should not be running in DOWN state */
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DTMR_RUNNING_IN_ST_DOWN,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemDownHandlePathStChg
 * Description :  This function is used to handle status change of the path
 being monitored by this BFD session when the session is 
 *                in Down state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD control packet
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandlePathStChg (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                           tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);

    /* Stop the Detection timers */
    BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

    /* delete the dynamic learned entries */
    BfdSessDelSessEntryDynParams (pBfdSessEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemDownHandlePeerAdminDown
 * Description :  This function is used to handle peer Admin Down when the 
 *                session is in Down state 
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                               tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4RemainingTime = 0;

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        /* Ignore this packet in down condition .If detect timer is 
         * running, stop it*/

        if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
        {
            TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                                 &pBfdSessEntry->
                                 DetectTimer.BfdTmrBlk.TimerNode,
                                 &u4RemainingTime);

            if (u4RemainingTime > 0)
            {
                /* Defect Exit aborted, Defect packet received before 
                 * Timer expiry */
                BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
            }

        }
        return OSIX_SUCCESS;
    }
    else if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
    {
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             DetectTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        /* There is a misconnectivity defect in the session. Check
         * for Detection timer. If it is not running, this is the
         * first packet without misconnectivity defect. Initiate 
         * defect exit criteria */

        if (u4RemainingTime == 0)
        {
            if (OSIX_SUCCESS !=
                BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR,
                             BFD_DETECTION_SLOW_TMR))
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_DTMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }

        }
        return OSIX_SUCCESS;
    }

    /* Remain in DOWN, no change in Diag */
    /* Invalid Event */

    /* Delete remote paramaters of the sesison */
    BfdSessDelSessEntryDynParams (pBfdSessEntry);

    BfdSemResetSemVariables (pBfdSessEntry);

    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  BfdSemDownHandlePeerDown
 * Description :  This function is used to handle peer DOWN when the session
 *                is in Down state. 
 *                1. If the received packet is with Cvvalidation failure and if
 *                misconnectivity defect exit criteria is already running, we
 *                stop the timer and abort the exit criteria.
 *                2. If there is no Cvvalidation failure but misconnectivity
 *                defect is existing, ie. after entering into the 
 *                misconnectivity defect state, we have now got a good packet,
 *                so we start the timer for defect exit criteria.
 *                Under other case of no cvvalidation or misconnectivity defect:
 *                3. If its a passive BFD session and received discriminator is
 *                zero, we start the Tx timer.
 *                4. SEM is moved to INIT state and Detection timer is started
 *                based on slow timer.
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandlePeerDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4RemainingTime = 0;

    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        /* Ignore this packet in down condition .If detect timer is 
         * running, stop it*/

        if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
        {
            TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                                 &pBfdSessEntry->
                                 DetectTimer.BfdTmrBlk.TimerNode,
                                 &u4RemainingTime);

            if (u4RemainingTime > 0)
            {
                /* Defect Exit aborted, Defect packet received before 
                 * Timer expiry */
                BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
            }

        }
        return OSIX_SUCCESS;
    }
    else if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
    {
        /* There is a misconnectivity defect in the session. Check
         * for Detection timer. If it is not running, this is the
         * first packet without misconnectivity defect. Initiate 
         * defect exit criteria */
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             DetectTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        if (u4RemainingTime == 0)
        {
            if (OSIX_SUCCESS !=
                BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR,
                             BFD_DETECTION_SLOW_TMR))
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_DTMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }

        }
        return OSIX_SUCCESS;
    }

    u4RemainingTime = 0;
    /* If passive and received with a your disc as Zero, start the
     * Tx timer */
    if (BFD_SESS_ROLE (pBfdSessEntry) == BFD_SESS_ROLE_PASSIVE)
    {
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             TxTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        if (u4RemainingTime == 0)
        {
            /* Passive session received a packet, start the timer */
            if (BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                             BFD_TRANSMIT_SLOW_TMR) != OSIX_SUCCESS)
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_TX_TMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }
        }
    }

    /* Session establishment case - Getting a DOWN after session was
     * initialized or initialized and came back to DOWN from INIT state */
    if (BFD_DIAG_NO_DIAG != (BFD_SESS_DIAG (pBfdSessEntry)))
    {
        /* Set Diagnostic code to (0) - No diagnostic */
        BFD_SESS_DIAG (pBfdSessEntry) = BFD_DIAG_NO_DIAG;
    }

    /* Move to INIT state */
    BFD_SESS_STATE (pBfdSessEntry) = BFD_SESS_STATE_INIT;

    BfdTxFormBfdCtrlPkt (pBfdSessEntry, OSIX_FALSE, OSIX_FALSE);

    /* Start Detection Timer calculated based on slow timer */

    if (OSIX_SUCCESS != BfdTmrStart
        (pBfdSessEntry, BFD_DETECTION_TMR, BFD_DETECTION_SLOW_TMR))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DTMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemDownHandlePeerInit
 * Description :  This function is used to handle peer Init when BFD session 
 *                is Down
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandlePeerInit (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4RemainingTime = 0;

    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        /* Ignore this packet in down condition .If detect timer is 
         * running, stop it*/

        if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
        {
            TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                                 &pBfdSessEntry->
                                 DetectTimer.BfdTmrBlk.TimerNode,
                                 &u4RemainingTime);

            if (u4RemainingTime > 0)
            {
                /* Defect Exit aborted, Defect packet received before 
                 * Timer expiry */
                BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
            }

        }
        return OSIX_SUCCESS;
    }
    else if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
    {
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             DetectTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        /* There is a misconnectivity defect in the session. Check
         * for Detection timer. If it is not running, this is the
         * first packet without misconnectivity defect. Initiate 
         * defect exit criteria */

        if (u4RemainingTime == 0)
        {
            if (OSIX_SUCCESS !=
                BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR,
                             BFD_DETECTION_SLOW_TMR))
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_DTMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }

        }
        return OSIX_SUCCESS;
    }

    /* Valid packet */
    if (BFD_DIAG_NO_DIAG != (BFD_SESS_DIAG (pBfdSessEntry)))
    {
        /* Set Diagnostic code to (0) - No diagnostic */
        BFD_SESS_DIAG (pBfdSessEntry) = BFD_DIAG_NO_DIAG;
    }

    BfdSessSessionUp (pBfdSessEntry);

    /* Restart the transmit/detection timer to fast time interval */
    BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_FAST_TMR);
    BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR);

    return OSIX_FAILURE;
}

/****************************************************************************
 * Function    :  BfdSemDownHandlePeerUp
 * Description :  This function is used to handle peer UP event when the 
 *                session is in Down state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemDownHandlePeerUp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdPktInfo * pBfdRxPktInfo)
{
    UINT4               u4RemainingTime = 0;

    BOOL1               bPathEgrCheck = OSIX_FALSE;
    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        /* Ignore this packet in down condition .If detect timer is 
         * running, stop it*/

        if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
        {
            TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                                 &pBfdSessEntry->
                                 DetectTimer.BfdTmrBlk.TimerNode,
                                 &u4RemainingTime);

            if (u4RemainingTime > 0)
            {
                /* Defect Exit aborted, Defect packet received before 
                 * Timer expiry */
                BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);
            }

        }
        return OSIX_SUCCESS;
    }
    else if (pBfdSessEntry->bMisConnectDefect == OSIX_TRUE)
    {
        TmrGetRemainingTime (gBfdGlobals.bfdTmrLst,
                             &pBfdSessEntry->
                             DetectTimer.BfdTmrBlk.TimerNode, &u4RemainingTime);

        /* There is a misconnectivity defect in the session. Check
         * for Detection timer. If it is not running, this is the
         * first packet without misconnectivity defect. Initiate 
         * defect exit criteria */

        if (u4RemainingTime == 0)
        {
            if (OSIX_SUCCESS !=
                BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR,
                             BFD_DETECTION_SLOW_TMR))
            {
                BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                         BFD_TRC_DTMR_FAIL,
                         pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                         (BFD_SESS_STATE (pBfdSessEntry)));
                return OSIX_FAILURE;
            }

        }
        return OSIX_SUCCESS;
    }

    if (BfdExtChkSessOperRole (pBfdSessEntry, BFD_SESS_OPER_ROLE_INDP_SINK,
                               &bPathEgrCheck) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Check if this is egress and MPLS-TP */
    if ((bPathEgrCheck == OSIX_TRUE))
    {
        /* Move to UP */
        BfdSessSessionUp (pBfdSessEntry);

        BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_FAST_TMR);
        BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR);

    }

    /* No action */

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandleDetnTmrExp
 * Description :  This function is used to handle Detection Timer expiry when
 *                BFD session is in INIT state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                            tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);

    /* Move to Down state */
    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandlePathStChg
 * Description :  This function is used to handle status change of the path
 being monitored by this BFD session when the session is in
 Init state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD control packet
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandlePathStChg (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                           tBfdPktInfo * pBfdRxPktInfo)
{
    UNUSED_PARAM (pBfdRxPktInfo);

    /* Stop the Detection timers */
    BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

    /* delete the dynamic learned entries */
    BfdSessDelSessEntryDynParams (pBfdSessEntry);

    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_PATH_DOWN, OSIX_TRUE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandlePeerAdminDown
 * Description :  This function is used to handle peer Admin Down when the 
 *                session is in Init state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                               tBfdPktInfo * pBfdRxPktInfo)
{
    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        /*Set misconnectivity defect */
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
    }

    /* Move to Down state */
    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN,
                        OSIX_FALSE);

    if (OSIX_SUCCESS != BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DTMR_STOP_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandlePeerDown
 * Description :  This function is used to handle peer DOWN event when session
 *                is in INIT state 
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandlePeerDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          tBfdPktInfo * pBfdRxPktInfo)
{
    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);
        return OSIX_SUCCESS;
    }

    /* Restart Detection Timer */
    if (OSIX_FAILURE ==
        BfdTmrReStart (pBfdSessEntry,
                       BFD_DETECTION_TMR, BFD_DETECTION_SLOW_TMR))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DTMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandlePeerInit
 * Description :  This function is used to handle peer Init event when
 *                BFD session is in Init state 
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandlePeerInit (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          tBfdPktInfo * pBfdRxPktInfo)
{

    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    /* if CV, CV validation should be successful */
    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);
        return OSIX_SUCCESS;
    }

    /* Move to UP state */
    BfdSessSessionUp (pBfdSessEntry);

    /* Restart transmit and detection timer to faster interval */
    if (OSIX_SUCCESS ==
        BfdTmrReStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_FAST_TMR))
    {
        if (OSIX_SUCCESS !=
            BfdTmrReStart (pBfdSessEntry,
                           BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR))
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_DTMR_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
            return OSIX_FAILURE;
        }
    }
    else
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_TX_TMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemInitHandlePeerUp
 * Description :  This function is used to handle peer UP event when the 
 *                session is in INIT state 
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemInitHandlePeerUp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdPktInfo * pBfdRxPktInfo)
{
    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    /* Check for Cv validation */
    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);
        return OSIX_SUCCESS;
    }

    /* Timers started, Move to UP state */
    BfdSessSessionUp (pBfdSessEntry);

    /* Restart transmit and detection timer to faster interval */
    if (OSIX_SUCCESS ==
        BfdTmrReStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_FAST_TMR))
    {
        if (OSIX_SUCCESS !=
            BfdTmrReStart (pBfdSessEntry,
                           BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR))
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_DTMR_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
            return OSIX_FAILURE;
        }
    }
    else
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_TX_TMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemUpHandleDetnTmrExp
 * Description :  This function is used to handle Detection Timer expiry when 
 *                session is in UP state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
INT4
BfdSemUpHandleDetnTmrExp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                          tBfdPktInfo * pBfdRxPktInfo)
{
    INT4                i4RetCode = OSIX_SUCCESS;

    UNUSED_PARAM (pBfdRxPktInfo);

    /* Loss of Connectivity defect */
    BfdUtilUpdatePktCount (&pBfdSessEntry->
                           MibObject.u4FsMIBfdSessLocDefCount, NULL);
    /* Session is UP and Detection Timer expired - Indicates FAULT */
    /* Move to Down state */
    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_CTRL_DETECTION_TIME_EXP,
                        OSIX_TRUE);

    /* Restart the transmit timer back to slow interval */
    i4RetCode = BfdTmrStart
        (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR);

    if (OSIX_FAILURE == i4RetCode)
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_TX_TMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
    }

    return i4RetCode;
}

/****************************************************************************
 * Function    :  BfdSemUpHandlePathStChg
 * Description :  This function is used to handle status change of the path
 *                being monitored by this BFD session when the session is in
 *                Up state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD control packet
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemUpHandlePathStChg (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                         tBfdPktInfo * pBfdRxPktInfo)
{

    UNUSED_PARAM (pBfdRxPktInfo);
    /* Stop the Detection timers */
    BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

    /* delete the dynamic learned entries */
    BfdSessDelSessEntryDynParams (pBfdSessEntry);

    /* Move to Down state */
    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_PATH_DOWN, OSIX_TRUE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemUpHandlePeerAdminDown
 * Description :  This function is used to handle peer Admin Down when
 *                the session is UP
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemUpHandlePeerAdminDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                             tBfdPktInfo * pBfdRxPktInfo)
{
    BOOL1               bPathIngrCheck = OSIX_FALSE;

    UNUSED_PARAM (pBfdRxPktInfo);
    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        if (BfdExtChkSessOperRole
            (pBfdSessEntry, BFD_SESS_OPER_ROLE_INDP_SOURCE,
             &bPathIngrCheck) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if ((OSIX_TRUE == bPathIngrCheck))
        {
            /*Ignore the packet .Ideally this case should never occur.
             * Rx is supposed to be Zero. So no packets should be recieved */
            return OSIX_SUCCESS;
        }

        /* Since Cv validation failed, ignore the peer state and 
         * enter CV defect state*/
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;

        /* Move to Down state */
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);

        BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR);
        BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

        return OSIX_SUCCESS;
    }

    /* Move to Down state */
    BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN,
                        OSIX_FALSE);

    if (OSIX_SUCCESS == BfdTmrStart
        (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR))
    {
        if (OSIX_SUCCESS != BfdTmrStart (pBfdSessEntry, BFD_DETECTION_TMR,
                                         BFD_DETECTION_SLOW_TMR))
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_DTMR_STOP_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
            return OSIX_FAILURE;
        }
    }
    else
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_TX_TMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemUpHandlePeerDown
 * Description :  This function is used to handle peer Down event when the 
 *                session is in Up state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received BFD packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemUpHandlePeerDown (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdPktInfo * pBfdRxPktInfo)
{
    BOOL1               bPathIngrCheck = OSIX_FALSE;

    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (BfdExtChkSessOperRole (pBfdSessEntry, BFD_SESS_OPER_ROLE_INDP_SOURCE,
                               &bPathIngrCheck) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Check if this is ingress and MPLS-TP. Independant monitoring
     *  is identified by Rx = 0*/
    if (bPathIngrCheck == OSIX_TRUE)
    {
        /* Source MEP, with independent monitoring . Remain in UP */
        return OSIX_SUCCESS;

    }
    else
    {
        if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
        {
            /* Since Cv validation failed, ignore the peer state and 
             * enter CV defect state.This is assumed to not occur for source
             * mep of independent session*/
            pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
        }
        else
        {
            /* Peer has detected FAULT and sent a DOWN or (RDI) message */
            BFD_SESS_DIAG (pBfdSessEntry) =
                BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN;
        }

        BfdSessDelSessEntryDynParams (pBfdSessEntry);

        /* Move to Down state */
        BfdSessSessionDown (pBfdSessEntry,
                            BFD_DIAG_NEIGHBOR_SIGNALED_SESS_DOWN, OSIX_TRUE);

        if (BfdTmrReStart
            (pBfdSessEntry, BFD_TRANSMIT_TMR,
             BFD_TRANSMIT_SLOW_TMR) == OSIX_FAILURE)
        {
            BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_DTMR_FAIL,
                     pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessEntry)));
        }
        BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSemUpHandlePeerInit
 * Description :  This function is used to handle peer Init event when
 *                the session is UP
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemUpHandlePeerInit (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                        tBfdPktInfo * pBfdRxPktInfo)
{
    BOOL1               bPathIngrCheck = OSIX_FALSE;
    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {
        if (BfdExtChkSessOperRole
            (pBfdSessEntry, BFD_SESS_OPER_ROLE_INDP_SOURCE,
             &bPathIngrCheck) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if ((OSIX_TRUE == bPathIngrCheck))
        {
            /*Ignore the packet .Ideally this case should never occur.
             * Rx is supposed to be Zero. So no packets should be recieved */
            return OSIX_SUCCESS;
        }

        /* Since Cv validation failed, ignore the peer state and 
         * enter CV defect state*/
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;

        /* Move to Down state */
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);

        BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR);
        BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR);

        return OSIX_SUCCESS;
    }

    /* Restart detection timer */
    return (BfdTmrReStart
            (pBfdSessEntry, BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR));

}

/****************************************************************************
 * Function    :  BfdSemUpHandlePeerUp
 * Description :  This function is used to handle peer Up event when the 
 *                session is in UP state
 * Input       :  pBfdSessEntry -> pointer to BFD session entry
 *                pBfdRxPktInfo -> pointer to received packet information
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 * ****************************************************************************/
PUBLIC INT4
BfdSemUpHandlePeerUp (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                      tBfdPktInfo * pBfdRxPktInfo)
{
    BOOL1               bPathIngrCheck = OSIX_FALSE;

    if ((pBfdRxPktInfo == NULL))
    {
        return OSIX_FAILURE;
    }

    /* Check for CV validation */
    if (pBfdRxPktInfo->bCvValidationFail == OSIX_TRUE)
    {

        if (BfdExtChkSessOperRole
            (pBfdSessEntry, BFD_SESS_OPER_ROLE_INDP_SOURCE,
             &bPathIngrCheck) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if ((OSIX_TRUE == bPathIngrCheck))
        {
            /*Ignore the packet */
            return OSIX_SUCCESS;
        }

        /* Move to Down state */
        BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);

        /* Defect entry, move to down state. In Independent monitoring,
         * packet is not received in Source Mep. Hence no need to check*/
        /* stop detection timer */
        if (BfdTmrStop (pBfdSessEntry, BFD_DETECTION_TMR) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        /* restart tx timer in slow timer */
        if (BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR, BFD_TRANSMIT_SLOW_TMR)
            != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        /* Since Cv validation failed, ignore the peer state and 
         * enter CV defect state*/
        pBfdSessEntry->bMisConnectDefect = OSIX_TRUE;
        return OSIX_SUCCESS;
    }

    /* Restart detection timer */
    if (OSIX_SUCCESS !=
        BfdTmrReStart (pBfdSessEntry, BFD_DETECTION_TMR,
                       BFD_DETECTION_FAST_TMR))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_DTMR_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex,
                 (BFD_SESS_STATE (pBfdSessEntry)));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif /* __BFDSEM_C__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file bfdsem.c                        */
/*-----------------------------------------------------------------------*/
