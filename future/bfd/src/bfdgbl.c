/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdgbl.c,v 1.15 2014/02/18 10:35:37 siva Exp $
 *
 * Description: This file contains the BFD global config table related 
 * functionality  
 *****************************************************************************/

#ifndef _BFDGBL_C_
#define _BFDGBL_C_

#include "bfdinc.h"

PRIVATE INT4        BfdGblCreateGlobalConfig (UINT4 u4ContextId);
PRIVATE INT4        BfdGblDeleteGlobalConfig (UINT4 u4ContextId);

/*****************************************************************************
 * Function     : BfdGblInitFsMIBfdGlobalConfigTable                         *
 * Description  : This intializes the BFD Global Config Table entry          *
 * Input        : pBfdGlobalConfigTableEntry - Pointer to BFD session table  *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*****************************************************************************/
PUBLIC INT4
BfdGblInitFsMIBfdGblConfigTable (tBfdFsMIStdBfdGlobalConfigTableEntry
                                 * pBfdGlobalConfigTableEntry)
{
    if (NULL == pBfdGlobalConfigTableEntry)
    {
        return OSIX_FAILURE;
    }

    /* Initialize all the parameters to default values */
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdAdminStatus =
        BFD_SESS_ENABLE;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIStdBfdSessNotificationsEnable =
        BFD_SNMP_FALSE;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTrapEnable = BFD_NO_TRAP;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdSystemControl = BFD_START;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdTraceLevel =
        BFD_CRITICAL_TRACE;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDesiredMinTxIntvl =
        BFD_GLB_DES_MIN_TX_INTVL;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblReqMinRxIntvl =
        BFD_GLB_REQ_MIN_RX_INTVL;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblDetectMult =
        BFD_GLB_DETECT_MULT;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdGblSlowTxIntvl =
        BFD_GLB_SLOW_TX_INTVL;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdGblSessOperMode =
        BFD_GLB_SESS_OPER_MODE;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdMemAllocFailure = 0;
    pBfdGlobalConfigTableEntry->MibObject.u4FsMIBfdInputQOverFlows = 0;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrGblStats = BFD_SNMP_FALSE;
    pBfdGlobalConfigTableEntry->MibObject.i4FsMIBfdClrAllSessStats =
        BFD_SNMP_FALSE;

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdGblCreateGlobalConfig                                   *
 * * Description  : Creates the entry in BFD Global config table for           *
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PRIVATE INT4
BfdGblCreateGlobalConfig (UINT4 u4ContextId)
{
    tBfdExtInParams    *pBfdExtInParams;
    tBfdExtOutParams   *pBfdExtOutParams;

    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTableEntry = NULL;

    /* Allocate memory for BFD Global Config Entry for the new context */
    pBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        MemAllocMemBlk (BFDMemPoolIds
                        [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID]);
    if (pBfdGlobalConfigTableEntry == NULL)
    {
        /*BFD_ISS_LOG (u4ContextId, 3, "BFD:BfdGblCreateGlobalConfig"); */
        return OSIX_FAILURE;
    }

    pBfdGlobalConfigTableEntry->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    /* Initialize the BFD Global Config Entry */
    if (OSIX_FAILURE ==
        BfdGblInitFsMIBfdGblConfigTable (pBfdGlobalConfigTableEntry))
    {
        BFD_LOG (u4ContextId, BFD_TRC_GBL_TABLE_INIT_FAIL);
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                            (UINT1 *) pBfdGlobalConfigTableEntry);
        return OSIX_FAILURE;
    }

    /* Allocate memory for input and output params */
    pBfdExtInParams = (tBfdExtInParams *)
        MemAllocMemBlk (BFD_EXTINPARAMS_POOLID);
    if (pBfdExtInParams == NULL)
    {
        BFD_ISS_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                     "BfdGblCreateGlobalConfig", "BFD_EXTINPARAMS_POOLID");
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                            (UINT1 *) pBfdGlobalConfigTableEntry);
        return OSIX_FAILURE;
    }
    pBfdExtOutParams = (tBfdExtOutParams *)
        MemAllocMemBlk (BFD_EXTOUTPARAMS_POOLID);
    if (pBfdExtOutParams == NULL)
    {
        BFD_ISS_LOG (u4ContextId, BFD_MEM_POOL_ALLOCATE_FAIL,
                     "BfdGblCreateGlobalConfig", "BFD_EXTOUTPARAMS_POOLID");
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                            (UINT1 *) pBfdGlobalConfigTableEntry);
        return OSIX_FAILURE;
    }

    MEMSET (pBfdExtInParams, 0, sizeof (tBfdExtInParams));
    MEMSET (pBfdExtOutParams, 0, sizeof (tBfdExtOutParams));

    /* Get the context name from the VCM module */
    pBfdExtInParams->u4ContextId = u4ContextId;
    pBfdExtInParams->eExtReqType = BFD_REQ_VCM_GET_CONTEXT_NAME;

    if (OSIX_FAILURE == BfdPortHandleExtInteraction (pBfdExtInParams,
                                                     pBfdExtOutParams))
    {
        BFD_LOG (u4ContextId, BFD_TRC_EXT_CALL_FAIL);
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                            (UINT1 *) pBfdGlobalConfigTableEntry);
        MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
        MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID,
                            (UINT1 *) pBfdExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pBfdGlobalConfigTableEntry->au1ContextName,
            pBfdExtOutParams->au1ContextName,
            sizeof (pBfdGlobalConfigTableEntry->au1ContextName));

    /* Allocate memory for input and output params */
    MemReleaseMemBlock (BFD_EXTINPARAMS_POOLID, (UINT1 *) pBfdExtInParams);
    MemReleaseMemBlock (BFD_EXTOUTPARAMS_POOLID, (UINT1 *) pBfdExtOutParams);

    /* Add the new node to the database */
    if (OSIX_FAILURE == RBTreeAdd
        (gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable,
         (tRBElem *) pBfdGlobalConfigTableEntry))
    {
        BFD_LOG (u4ContextId, BFD_TRC_GBL_ADD_FAIL);
        if (OSIX_FAILURE ==
            MemReleaseMemBlock (BFDMemPoolIds
                                [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                                (UINT1 *) pBfdGlobalConfigTableEntry))
        {
            BFD_ISS_LOG (u4ContextId, BFD_MEMPOOL_DEINIT_FAILED,
                         "BFD:BfdGlobalConfigTableEntry");
        }
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdGblCreateContext                                        *
 * * Description  : Creates the context in the BFD module                      *
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC INT4
BfdGblCreateContext (UINT4 u4ContextId)
{
    if (BFD_INVALID_CONTEXT == u4ContextId)
    {
        return OSIX_FAILURE;
    }
    /* Create an entry in the BFD Global Config table */
    if (OSIX_FAILURE == BfdGblCreateGlobalConfig (u4ContextId))
    {
        BFD_LOG (u4ContextId, BFD_TRC_ENTRY_CREATE_FAIL, u4ContextId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdGblDeleteGlobalConfig                                   *
 * * Description  : Deletes the entry from the BFD Global config Table         *
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PRIVATE INT4
BfdGblDeleteGlobalConfig (UINT4 u4ContextId)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry BfdGlobalConfigTableEntry;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdGlobalConfigTableEntry = NULL;

    MEMSET (&BfdGlobalConfigTableEntry, 0,
            sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

    BfdGlobalConfigTableEntry.MibObject.u4FsMIStdBfdContextId = u4ContextId;
    pBfdGlobalConfigTableEntry =
        (tBfdFsMIStdBfdGlobalConfigTableEntry *)
        BfdGetFsMIStdBfdGlobalConfigTable (&BfdGlobalConfigTableEntry);

    if (pBfdGlobalConfigTableEntry == NULL)
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_CNTX_ENTRY, u4ContextId);
        return OSIX_FAILURE;
    }

    /* Remove the entry from the BFD Global Config table. */
    RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdGlobalConfigTable,
               (tRBElem *) pBfdGlobalConfigTableEntry);

    if (OSIX_FAILURE ==
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDGLOBALCONFIGTABLE_SIZING_ID],
                            (UINT1 *) pBfdGlobalConfigTableEntry))
    {
        BFD_ISS_LOG (u4ContextId, BFD_MEMPOOL_DEINIT_FAILED,
                     "BFD:BfdGblDeleteGlobalConfig");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdGblDeleteContext                                       *
 * * Description  : Deletes the context in the BFD module                      *
 * * Input        : u4ContextId - context identifier   
 * *                bVcmDelete - Deletion called from VCM
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC INT4
BfdGblDeleteContext (UINT4 u4ContextId, BOOL1 bVcmDelete)
{
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigEntry =
        NULL;
    /* Delete BFD Session table */
    BfdSessDeactDelBfdSessTable (u4ContextId, OSIX_FALSE);

    if (OSIX_TRUE == bVcmDelete)
    {
        /* Delete BFD Global Config table */
        if (OSIX_FAILURE == BfdGblDeleteGlobalConfig (u4ContextId))
        {
            BFD_LOG (u4ContextId, BFD_TRC_GBL_TBL_DEL_FAIL);
            return OSIX_FAILURE;
        }
    }
    else
    {
        pBfdFsMIStdBfdGlobalConfigEntry = BfdUtilGetBfdGlobalConfigTable
            (u4ContextId);

        if (NULL != pBfdFsMIStdBfdGlobalConfigEntry)
        {
            /* Re-Initialize the BFD Global Config Entry */
            if (OSIX_FAILURE ==
                BfdGblInitFsMIBfdGblConfigTable
                (pBfdFsMIStdBfdGlobalConfigEntry))
            {
                BFD_LOG (u4ContextId, BFD_TRC_GBL_TABLE_INIT_FAIL);
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdGblHandleModuleStatus                                  *
 * * Description  : Handles enable/disable BFD module 
 * *                This function should be called from the SetAll Util 
 * *                function and its the callers responsiblilty to check and 
 *                  old and new parameters and call only if different. 
 *                  Also its the callers responsibility to check that the
 *                  function need not be called during the first time set.
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC VOID
BfdGblHandleModuleStatus (UINT4 u4ContextId, BOOL1 bModuleStatus)
{
    if (OSIX_TRUE == bModuleStatus)
    {
        /* Enables module status
         * This function is not called during first time set and thus since
         * previously during disable the remote parameters were removed,
         * bootstrapping has to be done again, i.e activate state machine
         * again */
        BfdSessReactBfdSessTable (u4ContextId);

    }
    else
    {
        /* Disable module status */

        /* Deactivate BFD session entries for this context */

        BfdSessDeactDelBfdSessTable (u4ContextId, OSIX_FALSE);
    }
}

#endif /* _BFDGBL_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdgbl.c                       */
/*-----------------------------------------------------------------------*/
