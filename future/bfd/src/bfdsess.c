/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdsess.c,v 1.51 2017/06/08 11:40:29 siva Exp $
 *
 * Description: This file contains the BFD module core functionality
 *****************************************************************************/

#ifndef _BFDSESS_C_
#define _BFDSESS_C_

#include "bfdinc.h"
#ifdef ISS_WANTED
#include "msr.h"
#endif

PRIVATE INT4        BfdSessDeleteBfdSessNode (tBfdFsMIStdBfdSessEntry *
                                              pBfdSessEntry);

/****************************************************************************
 * Function    :  BfdSessHandleSessionEntryStatus
 * Description :  This function is used to initialize session SEM data and
 *                trigger bootstrap
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdSessHandleSessionEntryStatus (tBfdFsMIStdBfdSessEntry
                                 * pBfdFsMIStdBfdSessInfo)
{
    INT4                i4RetCode = OSIX_SUCCESS;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry
        = NULL;

    if (NULL == pBfdFsMIStdBfdSessInfo)
    {
        return OSIX_FAILURE;
    }

    /* Perform requisite operations based on the configuration action */

    switch (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessRowStatus)
    {
        case CREATE_AND_WAIT:
        {
            pBfdFsMIStdBfdSessInfo->u1BfdSessParamAllowChg = OSIX_TRUE;

            /* Create Map entries */
            i4RetCode = BfdCoreCreateMapEntries (pBfdFsMIStdBfdSessInfo);

            if (OSIX_SUCCESS == i4RetCode)
            {
                /*Initialize the sem */
                BfdSemResetSemVariables (pBfdFsMIStdBfdSessInfo);
            }
        }
            break;

        case ACTIVE:
        {
#ifdef ISS_WANTED
            if ((MsrGetRestorationStatus () == ISS_TRUE) &&
                (BFD_CHECK_SESS_IS_DYNAMIC (pBfdFsMIStdBfdSessInfo)))
            {
                pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessRowStatus =
                    NOT_READY;
            }
            else
            {
#endif
                /* If BFD is shutdown on a particular context, do not display the
                 * neighbors in that context */
                pBfdFsMIStdBfdGlobalConfigTableEntry =
                    BfdUtilGetBfdGlobalConfigTable
                    (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);
                if ((pBfdFsMIStdBfdGlobalConfigTableEntry != NULL) &&
                    (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
                     i4FsMIBfdSystemControl == OSIX_TRUE))
                {
                    i4RetCode =
                        BfdSessHandleSessActivate (pBfdFsMIStdBfdSessInfo);
                }

#ifdef ISS_WANTED
            }
#endif
        }
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
        {
            /* If the session is dynamically created for a BFD client, during MSR restoration
             * IpMapEntry should be created when setting the row status to NOT_IN_SERVICE/NOT_READY,
             * to avoid creating duplicate Session entry for the monitoring the same IP path*/
#ifdef ISS_WANTED
            if ((MsrGetRestorationStatus () == ISS_TRUE) &&
                (BFD_CHECK_SESS_IS_DYNAMIC (pBfdFsMIStdBfdSessInfo)))
            {
                BfdCoreCreateBfdSessIpMapEntry (pBfdFsMIStdBfdSessInfo);
            }
#endif
            /* Deactivate the BFD session */
            i4RetCode =
                BfdSessDeactBfdSessEntry (pBfdFsMIStdBfdSessInfo, OSIX_FALSE);

            if (OSIX_SUCCESS == i4RetCode)
            {
                /* Call Hw if session was offlaoded */
                if ((BFD_SNMP_TRUE == BFD_SESS_OFFLD (pBfdFsMIStdBfdSessInfo))
                    && (pBfdFsMIStdBfdSessInfo->BfdHwhandle.u4BfdSessHwHandle !=
                        0))
                {
                    i4RetCode = BfdOffSessStop
                        (pBfdFsMIStdBfdSessInfo->MibObject.
                         u4FsMIStdBfdContextId, pBfdFsMIStdBfdSessInfo,
                         BFD_OFFLD_DELETE_SESSION);
                    if (i4RetCode == OSIX_SUCCESS)
                    {
                        pBfdFsMIStdBfdSessInfo->BfdHwhandle.u4BfdSessHwHandle =
                            0;
                    }
                }
            }

            /* Bringing session to NOT_IN_SERVICE. Set remote heard flag */
            pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessRemoteHeardFlag =
                BFD_SNMP_FALSE;

        }
            break;

        case DESTROY:
        {
            /* Deactivate session entries, stop timers */
            i4RetCode =
                BfdSessDeactBfdSessEntry (pBfdFsMIStdBfdSessInfo, OSIX_FALSE);

            if (OSIX_SUCCESS == i4RetCode)
            {
                /* Delete session map entries */
                i4RetCode = BfdCoreDeleteMapEntries (pBfdFsMIStdBfdSessInfo);

                if (OSIX_SUCCESS == i4RetCode)
                {
                    /* If offload delete */
                    /* Call Hw if session was offlaoded */
                    if ((BFD_SNMP_TRUE ==
                         BFD_SESS_OFFLD (pBfdFsMIStdBfdSessInfo))
                        && (pBfdFsMIStdBfdSessInfo->BfdHwhandle.
                            u4BfdSessHwHandle != 0))
                    {
                        i4RetCode = BfdOffSessStop
                            (pBfdFsMIStdBfdSessInfo->MibObject.
                             u4FsMIStdBfdContextId, pBfdFsMIStdBfdSessInfo,
                             BFD_OFFLD_DELETE_SESSION);
                        if (i4RetCode == OSIX_SUCCESS)
                        {
                            pBfdFsMIStdBfdSessInfo->BfdHwhandle.
                                u4BfdSessHwHandle = 0;
                        }
                    }
                }
            }
            break;
        }

        default:
            break;
    }
    if (OSIX_SUCCESS != i4RetCode)
    {
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_INIT_FAIL,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);
    }
    else
    {
        BFD_LOG (pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_INIT_PASS,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex,
                 pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIStdBfdContextId);
    }
    return i4RetCode;
}

/****************************************************************************
 * Function    :  BfdSessHandleSessActivate
 * Description :  This function is used to take necessary actions
 *                when session needs to be activate/reactivated
 * Input       :  u4ContextId - Context Id
 *                pPathInfo   - pointer to the path params received from LSPP
 * Output      :  u4BfdSessIndex
 * Returns     :  Pointer to session or NULL
 * ****************************************************************************/
PUBLIC INT4
BfdSessHandleSessActivate (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSessInfo)
{
    INT4                i4RetCode = OSIX_SUCCESS;

    if (pBfdFsMIStdBfdSessInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Process the activate request only if the ROW STATUS is ACTIVE
     * and the ADMIN STATUS of the session is start */

    if ((pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE)
        && (pBfdFsMIStdBfdSessInfo->MibObject.i4FsMIStdBfdSessAdminStatus ==
            BFD_SESS_ADMIN_START))
    {
        /* Session is configured and SEM is already initialized
         * to DOWN state*/
        BfdSemResetSemVariables (pBfdFsMIStdBfdSessInfo);

        /* Offload if offload flag is set and Discriminator
         * (as of now only "Your Discriminator" is configurable) is
         * configured statically */
        if ((BFD_SNMP_TRUE == (BFD_SESS_OFFLD (pBfdFsMIStdBfdSessInfo)))
            && (0 !=
                pBfdFsMIStdBfdSessInfo->MibObject.u4FsMIBfdSessRemoteDiscr))
        {
            /* Call to offload */
            i4RetCode = BfdCoreCreateOffldSess (pBfdFsMIStdBfdSessInfo, NULL);
        }
        else
        {
            if (BFD_CHECK_PATH_TYPE_NOT_IP (pBfdFsMIStdBfdSessInfo))
            {
                BfdCoreUpdateExtMod (pBfdFsMIStdBfdSessInfo,
                                     BFD_VALID_SESS_IDX);
            }
            BfdTxFormBfdCtrlPkt (pBfdFsMIStdBfdSessInfo, OSIX_FALSE,
                                 OSIX_FALSE);

            if (BFD_SESS_ROLE (pBfdFsMIStdBfdSessInfo) == BFD_SESS_ROLE_ACTIVE)
            {
                /* Only in active case try to transmit a packet.
                 * Else in passive case wait to receive a packet */
                BfdSessChkAndTransmitPkt (pBfdFsMIStdBfdSessInfo);
            }
            i4RetCode = OSIX_SUCCESS;
        }
    }
    return i4RetCode;
}

/****************************************************************************
 * Function    :  BfdSessScanTnlTbl
 * Description :  This function is used to scan BFD session table and
 *                if the path type is te-tunnel and matches with received
 *                path params, return pointer to BFD session.
 * Input       :  u4ContextId - Context Id
 *                pPathInfo   - pointer to the path params received from LSPP
 * Output      :  u4BfdSessIndex
 * Returns     :  Pointer to session or NULL
 * ****************************************************************************/
PUBLIC INT4
BfdSessScanTnlTbl (UINT4 u4ContextId, tBfdSessPathParams * pBfdSessPathParams,
                   UINT4 *u4BfdSessIndex)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdNextBfdSessEntry = NULL;
    tBfdSessTeParams   *pBfdTnlInfo = NULL;
    BOOL1               bEntryFound = OSIX_FALSE;

    pBfdCurrentBfdSessEntryInput = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdCurrentBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    if (NULL == pBfdSessPathParams)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdCurrentBfdSessEntryInput);
        return OSIX_FAILURE;
    }

    pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    do
    {
        pBfdNextBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntryInput);

        if (pBfdNextBfdSessEntry == NULL)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }

        if (pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
            u4ContextId)
        {
            /* No more entries in the database */
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_FAILURE;
        }

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

        if ((pBfdNextBfdSessEntry->BfdSessPathParams.ePathType ==
             BFD_PATH_TYPE_TE_IPV4) ||
            (pBfdNextBfdSessEntry->BfdSessPathParams.ePathType ==
             BFD_PATH_TYPE_TE_IPV6))
        {
            pBfdTnlInfo =
                &(pBfdNextBfdSessEntry->BfdSessPathParams.SessTeParams);

            if ((pBfdSessPathParams->SessTeParams.SrcIpAddr.u4_addr[0] ==
                 pBfdTnlInfo->SrcIpAddr.u4_addr[0]) &&
                (pBfdSessPathParams->SessTeParams.DstIpAddr.u4_addr[0] ==
                 pBfdTnlInfo->DstIpAddr.u4_addr[0]) &&
                (pBfdSessPathParams->SessTeParams.u4TunnelId ==
                 pBfdTnlInfo->u4TunnelId) &&
                (pBfdSessPathParams->SessTeParams.u4TunnelInst ==
                 pBfdTnlInfo->u4TunnelInst))
            {
                *u4BfdSessIndex =
                    pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
                bEntryFound = OSIX_TRUE;
            }
        }

        pBfdNextBfdSessEntry = NULL;

        if (OSIX_TRUE == bEntryFound)
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdCurrentBfdSessEntryInput);
            return OSIX_SUCCESS;

        }
        else
        {
            continue;
        }
    }
    while (1);
}

/*******************************************************************************
 * * Function     : BfdSessReactBfdSessEntry                                   *
 * * Description  : Activates a BFD session entry                              *
 *                  Note: This function does not set rowStatus to Active       *
 * *                Caller does the same.                                      *
 * * Input        : pBfdSessEntry - pointer to BFD session entry               *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC INT4
BfdSessReactBfdSessEntry (tBfdFsMIStdBfdSessEntry * pBfdNextBfdSessEntry)
{
    if (NULL == pBfdNextBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    if (OSIX_FAILURE == BfdSessHandleSessActivate (pBfdNextBfdSessEntry))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdSessDeactDelBfdSessTable                                *
 * * Description  : Deactivate and or deletes entries from BFD session table   *
 *                  for the context.
 *                  Note: This function does not set rowStatus to Not In
 *                  Service
 * * Input        : u4ContextId - context identifier                           *
 *                  u1OperDelete - indicates if the the entries need to be     *
 *                  deleted. If false entries are only deactivated.            *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC VOID
BfdSessDeactDelBfdSessTable (UINT4 u4ContextId, UINT1 u1OperDelete)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdNextBfdSessEntry = NULL;

    pBfdCurrentBfdSessEntry = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntry == NULL)
    {
        return;
    }
    MEMSET (pBfdCurrentBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    do
    {
        pBfdNextBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntry);
        if (pBfdNextBfdSessEntry == NULL)
        {
            /* No more entries in the database */
            break;
        }

        if (pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
            u4ContextId)
        {
            /* No more entries in the database */
            break;
        }

        pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

        if (BfdSessDeactBfdSessEntry (pBfdNextBfdSessEntry, OSIX_FALSE) ==
            OSIX_FAILURE)
        {
            continue;
        }

        /* Call Hw if session was offlaoded */
        if ((BFD_SNMP_TRUE == BFD_SESS_OFFLD (pBfdNextBfdSessEntry))
            && (pBfdNextBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0))
        {
            if (BfdOffSessStop
                (pBfdNextBfdSessEntry->MibObject.
                 u4FsMIStdBfdContextId, pBfdNextBfdSessEntry,
                 BFD_OFFLD_DELETE_SESSION) == OSIX_SUCCESS)
            {
                pBfdNextBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle = 0;
            }
        }

        if (u1OperDelete == OSIX_TRUE)
        {
            BfdSessDeleteBfdSessEntry (pBfdNextBfdSessEntry);
        }

        pBfdNextBfdSessEntry = NULL;

    }
    while (1);

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdCurrentBfdSessEntry);
    return;
}

/*******************************************************************************
 * * Function     : BfdSessReactDelBfdSessTable                                *
 * * Description  : Activates entries from BFD session table for the context   *
 *                  Note: This function does not set rowStatus to Active
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC VOID
BfdSessReactBfdSessTable (UINT4 u4ContextId)
{
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntryInput = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdNextBfdSessEntry = NULL;

    pBfdCurrentBfdSessEntryInput = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntryInput == NULL)
    {
        return;
    }
    MEMSET (pBfdCurrentBfdSessEntryInput, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    do
    {
        pBfdNextBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntryInput);
        if (pBfdNextBfdSessEntry == NULL)
        {
            /* No more entries in the database */
            break;
        }

        if (pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId !=
            u4ContextId)
        {
            /* No more entries in the database */
            break;
        }

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdContextId =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdContextId;

        pBfdCurrentBfdSessEntryInput->MibObject.u4FsMIStdBfdSessIndex =
            pBfdNextBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;

        BfdSessReactBfdSessEntry (pBfdNextBfdSessEntry);

        pBfdNextBfdSessEntry = NULL;

    }
    while (1);

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdCurrentBfdSessEntryInput);

    return;
}

/****************************************************************************
 * Function    :  BfdSessDelSessEntryDynParams
 * Description :  This function is used to remove learnt dynamic params
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdSessDelSessEntryDynParams (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    /* Set the dynamic params learned as false i.e need to learn again */
    pBfdSessEntry->bDynamicParamsLearned = OSIX_FALSE;
    pBfdSessEntry->bPollRunning = OSIX_FALSE;
    pBfdSessEntry->bPollPending = OSIX_FALSE;
    pBfdSessEntry->bToTrigLsppBtStrap = OSIX_FALSE;
    pBfdSessEntry->bMisConnectDefect = OSIX_FALSE;
    pBfdSessEntry->u4NegRemoteTxTimeIntrvl = 0;
    MEMSET (&pBfdSessEntry->LastRxCtrlPktParams, 0,
            sizeof (tBfdSessPeerParams));
    if (OSIX_TRUE != pBfdSessEntry->bRemDiscrStaticConf)
    {
        pBfdSessEntry->MibObject.u4FsMIStdBfdSessRemoteDiscr = 0;
        pBfdSessEntry->MibObject.u4FsMIBfdSessRemoteDiscr = 0;
    }
    pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedInterval = 0;
    pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedEchoInterval = 0;
    pBfdSessEntry->MibObject.u4FsMIStdBfdSessNegotiatedDetectMult = 0;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSessSessionUp
 * Description :  This function is used perform required action on session UP
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdSessSessionUp (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSesInfo)
{
    UINT4               u4ContextId = 0;

    if (pBfdFsMIStdBfdSesInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    u4ContextId = pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId;

    BFD_SESS_STATE (pBfdFsMIStdBfdSesInfo) = BFD_SESS_STATE_UP;
    BfdTxFormBfdCtrlPkt (pBfdFsMIStdBfdSesInfo, OSIX_FALSE, OSIX_FALSE);

    /* Transmit the packet to inform session state to peer 
       if offload is not enabled or session is not yet offloaded. */
    if ((BFD_SNMP_TRUE != BFD_SESS_OFFLD (pBfdFsMIStdBfdSesInfo)) ||
        ((BFD_SNMP_TRUE == BFD_SESS_OFFLD (pBfdFsMIStdBfdSesInfo))
         && (pBfdFsMIStdBfdSesInfo->BfdHwhandle.u4BfdSessHwHandle == 0)))
    {
        BfdTxPacket (pBfdFsMIStdBfdSesInfo);
    }

    if (BfdExtInformSessionState (pBfdFsMIStdBfdSesInfo, BFD_SESS_STATE_UP)
        != OSIX_SUCCESS)
    {
        BFD_LOG (u4ContextId, BFD_TRC_OAM_INFO_FAILED, "BfdSessSessionUp");
        return OSIX_FAILURE;
    }

    /* Note down the time of Session UP */
    BFD_GET_SYS_TIME (&pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessUpTime);

    /* Increase the Session up count */
    pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessPerfSessUpCount++;

    /* Raise the session UP trap */
    BFD_LOG (pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
             BFD_TRC_SESS_UP,
             pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessIndex,
             pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
             (BFD_SESS_DIAG (pBfdFsMIStdBfdSesInfo)));

    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  BfdSessSessionDown
 * Description :  This function is used perform required action on session 
 *                DOWN
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
BfdSessSessionDown (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSesInfo,
                    tBfdDiagnostic eBfdDiag, BOOL1 bInformClient)
{
    UINT4               u4ContextId = 0;

    if (pBfdFsMIStdBfdSesInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    u4ContextId = pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId;

    /* Set the state to ADMIN DOWN in case the request is received when
       session is made Administratively Down. In all other cases state is
       set to Down. */
    if (eBfdDiag == BFD_DIAG_ADMIN_DOWN)
    {
        BFD_SESS_STATE (pBfdFsMIStdBfdSesInfo) = BFD_SESS_STATE_ADMIN_DOWN;
    }
    else
    {
        BFD_SESS_STATE (pBfdFsMIStdBfdSesInfo) = BFD_SESS_STATE_DOWN;
    }

    /* BFD sess Diag */
    BFD_SESS_DIAG (pBfdFsMIStdBfdSesInfo) = eBfdDiag;
    BfdTxFormBfdCtrlPkt (pBfdFsMIStdBfdSesInfo, OSIX_FALSE, OSIX_FALSE);

    /* Transmit the packet to inform session state to peer
       if offload is not enabled or session is not yet offloaded. */
    if ((BFD_SNMP_TRUE != BFD_SESS_OFFLD (pBfdFsMIStdBfdSesInfo)) ||
        ((BFD_SNMP_TRUE == BFD_SESS_OFFLD (pBfdFsMIStdBfdSesInfo))
         && (pBfdFsMIStdBfdSesInfo->BfdHwhandle.u4BfdSessHwHandle == 0)))
    {
        BfdTxPacket (pBfdFsMIStdBfdSesInfo);
    }

    if (OSIX_TRUE == bInformClient)
    {
        if (BfdExtInformSessionState (pBfdFsMIStdBfdSesInfo,
                                      (UINT4) BFD_SESS_STATE
                                      (pBfdFsMIStdBfdSesInfo)) != OSIX_SUCCESS)
        {

            BFD_LOG (u4ContextId, BFD_TRC_OAM_INFO_FAILED,
                     "BfdSessSessionDown");
            return OSIX_FAILURE;
        }
    }

    /* Note down the time of Session down */
    BFD_GET_SYS_TIME (&pBfdFsMIStdBfdSesInfo->
                      MibObject.u4FsMIStdBfdSessPerfLastSessDownTime);

    /* Increase the Session up count */
    pBfdFsMIStdBfdSesInfo->MibObject.i4FsMIStdBfdSessPerfLastCommLostDiag =
        eBfdDiag;

    if (eBfdDiag == BFD_DIAG_ADMIN_DOWN)
    {
        /* Raise Session ADMIN_DOWN trap */
        BFD_LOG (pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_ADMIN_DOWN,
                 pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessIndex,
                 pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
                 (BFD_SESS_DIAG (pBfdFsMIStdBfdSesInfo)));
    }
    else
    {
        /* Raise Session DOWN trap */
        BFD_LOG (pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_DOWN,
                 pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessIndex,
                 pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdContextId,
                 (BFD_SESS_DIAG (pBfdFsMIStdBfdSesInfo)));
    }

    return OSIX_SUCCESS;

}

/*******************************************************************************
 * * Function     : BfdSessDeactBfdSessEntry                                   *
 * * Description  : Deactivates a BFD session entry                            *
 *                  Note: This function does not set rowStatus to Not In       *
 *                  Service. Caller does the same.                             *
 * * Input        : pBfdSessEntry - pointer to BFD session entry               *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
INT4
BfdSessDeactBfdSessEntry (tBfdFsMIStdBfdSessEntry * pBfdNextBfdSessEntry,
                          BOOL1 bFromMbsm)
{
    if (NULL == pBfdNextBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    /* Move to Down state */
    if (BFD_SESS_STATE (pBfdNextBfdSessEntry) != BFD_SESS_STATE_DOWN)
    {
        if (OSIX_FALSE == bFromMbsm)
        {
            BfdSessSessionDown (pBfdNextBfdSessEntry, BFD_DIAG_NO_DIAG,
                                OSIX_TRUE);
        }
        else
        {
            BfdSessSessionDown (pBfdNextBfdSessEntry, BFD_DIAG_FWD_PLANE_RESET,
                                OSIX_FALSE);
        }
    }

    /* Delete remote paramaters of the sesison */
    BfdSessDelSessEntryDynParams (pBfdNextBfdSessEntry);
    if (pBfdNextBfdSessEntry->bInformedExtModule == OSIX_TRUE)
    {
        BfdCoreUpdateExtMod (pBfdNextBfdSessEntry, BFD_INVALID_SESS_IDX);
        pBfdNextBfdSessEntry->bInformedExtModule = OSIX_FALSE;
    }

    /*Session is desabled so clear the Admin cntrl error if present */
    pBfdNextBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq = BFD_SNMP_FALSE;
    pBfdNextBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason =
        BFD_SESS_ERROR_NONE;

    /* Stop the timers running for this session */
    BfdTmrStop (pBfdNextBfdSessEntry, BFD_TRANSMIT_TMR);
    BfdTmrStop (pBfdNextBfdSessEntry, BFD_DETECTION_TMR);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSessGetDiscriminator
 * Description :  This function is used to provide a unique discriminator value
 *                for the session
 * Input       :  pBfdFsMIStdBfdSessInfo - pointer to the session info
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC UINT4
BfdSessGetDiscriminator (tBfdFsMIStdBfdSessEntry * pBfdFsMIStdBfdSesInfo)
{
    return pBfdFsMIStdBfdSesInfo->MibObject.u4FsMIStdBfdSessIndex;
}

/*******************************************************************************
 * * Function     : BfdSessDeleteBfdSessNode                                   *
 * * Description  : Deletes entries from BFD session table                     *
 * * Input        : u4ContextId - context identifier                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PRIVATE INT4
BfdSessDeleteBfdSessNode (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    if (NULL == pBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    RBTreeRemove (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                  (tRBElem *) pBfdSessEntry);

    if (OSIX_FAILURE ==
        MemReleaseMemBlock (BFDMemPoolIds
                            [MAX_BFD_FSMISTDBFDSESSTABLE_SIZING_ID],
                            (UINT1 *) pBfdSessEntry))
    {
        BFD_ISS_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_MEMPOOL_DEINIT_FAILED, "BFD:BfdSessDeleteBfdSessNode");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * * Function     : BfdSessDeleteBfdSessEntry                                  *
 * * Description  : Deletes a BFD session entry                                *
 * * Input        : pBfdSessEntry - pointer to BFD session entry               *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * ****************************************************************************/
PUBLIC INT4
BfdSessDeleteBfdSessEntry (tBfdFsMIStdBfdSessEntry * pBfdSessEntry)
{
    if (NULL == pBfdSessEntry)
    {
        return OSIX_FAILURE;
    }

    /* Delete the BFD session map entries */
    if (OSIX_FAILURE == BfdCoreDeleteMapEntries (pBfdSessEntry))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_MAP_ENTRY_DEL_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        return OSIX_FAILURE;
    }

    /* Delete the BFD session entry from the database */
    if (OSIX_FAILURE == BfdSessDeleteBfdSessNode (pBfdSessEntry))
    {
        BFD_LOG (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_SESS_NODE_DEL_FAIL,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSessHandleSessPathStatusChg
 * Description :  This function is used to handle path status change
 *                notification from OAM
 * Input       :  u4BfdSessIndex - Bfd session index
 *                u1PathStatus - Path Status UP/DOWN
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdSessHandleSessPathStatusChg (UINT4 u4ContextId, UINT4 u4BfdSessIndex,
                                UINT1 u1PathStatus)
{
    INT4                i4RetCode;
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;

    /* Call GetApi to get the BFD session entry pointer */
    pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId, u4BfdSessIndex);
    if (pBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Trigger state machine and let it handle path status change event
     * based on state. We are ignoring PATH UP, because our timers
     * are runnning anyway which check the status of the paths*/
    if (u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {

        /* Call Hw if session was offlaoded */
        if ((BFD_SNMP_TRUE == BFD_SESS_OFFLD (pBfdSessEntry))
            && (pBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle != 0))
        {
            if (pBfdSessEntry->bRemDiscrStaticConf != OSIX_TRUE)
            {
                i4RetCode = BfdOffSessStop
                    (pBfdSessEntry->MibObject.
                     u4FsMIStdBfdContextId, pBfdSessEntry,
                     BFD_OFFLD_DELETE_SESSION);
                if (i4RetCode == OSIX_SUCCESS)
                {
                    pBfdSessEntry->BfdHwhandle.u4BfdSessHwHandle = 0;
                }

                BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);
                /* delete the dynamic learned entries */
                BfdSessDelSessEntryDynParams (pBfdSessEntry);

                if (BFD_SESS_ROLE (pBfdSessEntry) == BFD_SESS_ROLE_ACTIVE)
                {
                    BfdTmrStart (pBfdSessEntry, BFD_TRANSMIT_TMR,
                                 BFD_TRANSMIT_SLOW_TMR);
                }
            }
            else
            {
                BfdSessSessionDown (pBfdSessEntry, BFD_DIAG_NO_DIAG, OSIX_TRUE);
            }
        }
        else
        {
            BfdCoreTriggerBfdSem (pBfdSessEntry, BFD_SEM_REQ_PATH_DOWN, NULL);
        }
        BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                &(pBfdSessEntry->MibObject.SessDbNode));

        BfdRedSyncDynInfo ();
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  BfdSessProcessAlwParamChgMsg
 * Description :  This function is used to handle allow param change
 *                message from the external modules
 * Input       :  u4ContextId - Context Id
 *                pBfdAllowParamChgMsg - information about allow change
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
BfdSessProcessAlwParamChgMsg (UINT4 u4ContextId,
                              tBfdAllowParamChgMsg * pBfdAllowParamChgMsg)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdCurrentBfdSessEntry = NULL;

    pBfdCurrentBfdSessEntry = MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pBfdCurrentBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Check if the message is for all sessions or for one session */
    if (pBfdAllowParamChgMsg->bForAllSession != OSIX_TRUE)
    {
        /* Call GetApi to get the BFD session entry pointer */
        pBfdSessEntry = BfdUtilGetBfdSessTable (u4ContextId,
                                                pBfdAllowParamChgMsg->
                                                u4SessionId);

        if (pBfdSessEntry == NULL)
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdCurrentBfdSessEntry);
            return OSIX_FAILURE;
        }

        /* Assign the received parameter */
        pBfdSessEntry->u1BfdSessParamAllowChg =
            pBfdAllowParamChgMsg->bAllowParamChg;
    }
    else
    {
        /* This call is for all the session configured till now */

        MEMSET (pBfdCurrentBfdSessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
        pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdContextId = u4ContextId;
        do
        {
            pBfdSessEntry =
                BfdGetNextFsMIStdBfdSessTable (pBfdCurrentBfdSessEntry);
            if (pBfdSessEntry == NULL)
            {
                /* No more entries in the database */
                break;
            }

            if (pBfdSessEntry->MibObject.u4FsMIStdBfdContextId != u4ContextId)
            {
                /* No more entries in the database */
                break;
            }

            /* Assign the received parameter */
            pBfdSessEntry->u1BfdSessParamAllowChg =
                pBfdAllowParamChgMsg->bAllowParamChg;

            /* Proceed to get the next session */
            pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdContextId =
                pBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
            pBfdCurrentBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex =
                pBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex;
            pBfdSessEntry = NULL;
        }
        while (1);
    }

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pBfdCurrentBfdSessEntry);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdSessChkAndTransmitPkt                                   *
 * Description  : This function will be invoked whenever a packet is to be   *
 *                transmitted. This is called whenever decision has to be    *
 *                whether packet has to be transmitted or not. This will     *
 *                also restart/start the timer                               *
 * Input        : pointer to the SessionEntry  table                         *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
BfdSessChkAndTransmitPkt (tBfdFsMIStdBfdSessEntry * pBfdSessionEntry)
{
    BOOL1               bCheckEgr = OSIX_FALSE;

    if (pBfdSessionEntry == NULL)
    {
        return;
    }

    if (BFD_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }

    /* Check for the discriminator availability. If discriminator is 
     * unavailable, trigger the Bootstrap if OOB is enable. In case,
     * session is on egress node and active act like a Passive node until 
     * discriminator is available.*/
    if ((BFD_SESS_REMOTE_DISC (pBfdSessionEntry) == 0))
    {
        /* In passive this condition should never be true */
        if ((BFD_SESS_STATE (pBfdSessionEntry) == BFD_SESS_STATE_DOWN) &&
            (BFD_SESS_ROLE (pBfdSessionEntry) == BFD_SESS_ROLE_PASSIVE))
        {
            return;
        }
        /* For IP path monitoring trasmit the packet even if remote discriminator
         * is not known*/
        if (BFD_CHECK_PATH_TYPE_NOT_IP (pBfdSessionEntry))
        {
            if (BfdExtChkSessPathEgrs (pBfdSessionEntry, &bCheckEgr)
                == OSIX_FAILURE)
            {

                if (OSIX_SUCCESS !=
                    BfdTmrReStart (pBfdSessionEntry, BFD_TRANSMIT_TMR,
                                   BFD_TRANSMIT_SLOW_TMR))
                {
                    BFD_LOG (pBfdSessionEntry->MibObject.u4FsMIStdBfdContextId,
                             BFD_TRC_TX_TMR_FAIL,
                             pBfdSessionEntry->MibObject.u4FsMIStdBfdSessIndex,
                             (BFD_SESS_STATE (pBfdSessionEntry)));
                }

                return;
            }

            if ((BFD_SESS_TYPE (pBfdSessionEntry) ==
                 BFD_SESS_TYPE_MULTIHOP_OUTOFBAND_SIGNALING) &&
                (bCheckEgr != OSIX_TRUE))
            {

                /* This flag should be reset on getting a LSP Ping response
                 * or a BFD control packet */
                pBfdSessionEntry->bToTrigLsppBtStrap = OSIX_TRUE;

                /* SessType: OOB Trigger LSPP for bootstrap .This is 
                 * a Active session which is not on egress of a independent
                 * monitored path*/
                BfdBtStrTriggerOOBBtStrap (pBfdSessionEntry);
            }
            else if (bCheckEgr != OSIX_TRUE)
            {
                /* Non Egress, Non Passive , Non OOB,No discriminator case */
                BfdTxPacket (pBfdSessionEntry);

            }
        }
        else if (BFD_SESS_DEST_UDP_PORT (pBfdSessionEntry) == BFD_UDP_DEST_PORT
                 || BFD_SESS_DEST_UDP_PORT (pBfdSessionEntry) == 0)
        {
            BfdTxPacket (pBfdSessionEntry);
        }
        if (OSIX_SUCCESS !=
            BfdTmrReStart (pBfdSessionEntry, BFD_TRANSMIT_TMR,
                           BFD_TRANSMIT_SLOW_TMR))
        {
            BFD_LOG (pBfdSessionEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_TX_TMR_FAIL,
                     pBfdSessionEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessionEntry)));
            return;
        }

        return;
    }

    /* Trigger Tx module to send a packet.
     * If the path is unavailable, Packet send should fail*/
    BfdTxPacket (pBfdSessionEntry);

    if (BFD_SESS_STATE (pBfdSessionEntry) == BFD_SESS_STATE_UP)
    {
        /*start fast timer */
        if (OSIX_SUCCESS !=
            BfdTmrStart (pBfdSessionEntry, BFD_TRANSMIT_TMR,
                         BFD_TRANSMIT_FAST_TMR))
        {
            BFD_LOG (pBfdSessionEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_TX_TMR_FAIL,
                     pBfdSessionEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessionEntry)));
            return;
        }
    }
    else
    {
        /*start slow timer */
        if (OSIX_SUCCESS !=
            BfdTmrStart (pBfdSessionEntry, BFD_TRANSMIT_TMR,
                         BFD_TRANSMIT_SLOW_TMR))
        {
            BFD_LOG (pBfdSessionEntry->MibObject.u4FsMIStdBfdContextId,
                     BFD_TRC_TX_TMR_FAIL,
                     pBfdSessionEntry->MibObject.u4FsMIStdBfdSessIndex,
                     (BFD_SESS_STATE (pBfdSessionEntry)));
            return;
        }
    }
    return;
}

/*****************************************************************************
 * Function     : BfdSessHandleClientRqst                                    *
 * Description  : This function will be invoked whenever a BFD client        *
 *                registers/de-registers for neighbor IP path monitoring     *
 * Input        : uContextId     - Context Id                                *
 *                pBfdClientInfo - Neighbor IP path info                     *
 *                u1Flag         - Flag to differntiate register/de-register *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
BfdSessHandleClientRqst (UINT4 u4ContextId,
                         tBfdClientInfo * pBfdClientInfo, UINT1 u1Flag)
{
    tBfdFsMIStdBfdSessEntry *pBfdSessInfo = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdSessionEntry = NULL;
    tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntry = NULL;
    tBfdFsMIStdBfdSessIpMapEntry BfdSessIpMapEntry;
    UINT1               u1AddrLen = 0;
#ifdef IP6_WANTED
    tIp6Addr            Ip6SrcAddr;
    tIp6Addr            Ip6DstAddr;

    MEMSET (&Ip6SrcAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ip6DstAddr, 0, sizeof (tIp6Addr));
#endif
    MEMSET (&BfdSessIpMapEntry, 0, sizeof (tBfdFsMIStdBfdSessIpMapEntry));
    pBfdSessInfo = (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

    if (pBfdSessInfo == NULL)
    {
        return;
    }

    if (pBfdClientInfo == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pBfdSessInfo);
        return;
    }

    MEMSET (pBfdSessInfo, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    /* Fill the neighbor info */
    pBfdSessInfo->MibObject.u4FsMIStdBfdContextId = u4ContextId;

    if (pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType == BFD_INET_ADDR_IPV4)
    {
        u1AddrLen = BFD_IPV4_MAX_ADDR_LEN;
        /* In case of dynamic sessions, the interface index has to be
           taken depending on the source address to be used for a particular
           destination. */
        pBfdSessInfo->MibObject.i4FsMIStdBfdSessInterface =
            (INT4) pBfdClientInfo->BfdClientNbrIpPathInfo.u4IfIndex;
    }
#ifdef IP6_WANTED
    else if (pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType ==
             BFD_INET_ADDR_IPV6)
    {
        u1AddrLen = BFD_IPV6_MAX_ADDR_LEN;
        pBfdSessInfo->MibObject.i4FsMIStdBfdSessInterface =
            (INT4) pBfdClientInfo->BfdClientNbrIpPathInfo.u4IfIndex;

    }
#endif
    MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessSrcAddr),
            &(pBfdClientInfo->BfdClientNbrIpPathInfo.SrcAddr), u1AddrLen);

    pBfdSessInfo->MibObject.i4FsMIStdBfdSessSrcAddrLen = u1AddrLen;
    pBfdSessInfo->MibObject.i4FsMIStdBfdSessSrcAddrType =
        pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType;

    MEMCPY (&(pBfdSessInfo->MibObject.au1FsMIStdBfdSessDstAddr),
            &(pBfdClientInfo->BfdClientNbrIpPathInfo.NbrAddr), u1AddrLen);

    pBfdSessInfo->MibObject.i4FsMIStdBfdSessDstAddrLen = u1AddrLen;
    pBfdSessInfo->MibObject.i4FsMIStdBfdSessDstAddrType =
        pBfdClientInfo->BfdClientNbrIpPathInfo.u1AddrType;

    /* Check whether BFD session is already established for the
     * neighbor IP path requested by the client
     */

    pBfdSessIpMapEntry = BfdUtilCheckIpMapEntryExist (pBfdSessInfo);

    if (pBfdSessIpMapEntry != NULL)
    {
        MEMSET (pBfdSessInfo, 0, sizeof (tBfdFsMIStdBfdSessEntry));

        pBfdSessInfo->MibObject.u4FsMIStdBfdContextId = u4ContextId;
        pBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex =
            pBfdSessIpMapEntry->MibObject.u4FsMIStdBfdSessIpMapIndex;

        pBfdSessionEntry = RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                                      (tRBElem *) pBfdSessInfo);

        if (pBfdSessionEntry == NULL)
        {
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdSessInfo);
            return;
        }
        if (u1Flag == BFD_CREATE_DYNAMIC_SESSION)
        {
            /* Register the client informations if already BFD session is
             * establsihed for monitoring the requested IP path
             */
            if (BfdSessRegisterClient (pBfdSessionEntry, pBfdClientInfo) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSessInfo);
                return;
            }
        }
        else if (u1Flag == BFD_DISABLE_DYNAMIC_SESSION)
        {
            /* Disable the client from the IP path monitoring dynamically */
            if (BfdSessDisableClient (pBfdSessionEntry,
                                      pBfdClientInfo->u1BfdClientId) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSessInfo);
                return;

            }
        }
        else
        {
            /* De-register the client from the IP path monitoring */
            if (BfdSessDeRegisterClient (pBfdSessionEntry,
                                         pBfdClientInfo->u1BfdClientId) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSessInfo);
                return;
            }
        }
    }
    else
    {
        /* Checking whether already a BFD multi-hop session is 
         * created for monitoring the same source and destination 
         * with different interface index */
        if (pBfdClientInfo->u1SessionType
            == BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS)
        {
            BfdSessIpMapEntry.MibObject.u4FsMIStdBfdContextId = u4ContextId;
            pBfdSessIpMapEntry =
                BfdGetNextFsMIStdBfdSessIpMapTable (&BfdSessIpMapEntry);
            while (pBfdSessIpMapEntry != NULL)
            {
                if (pBfdSessIpMapEntry->MibObject.
                    u4FsMIStdBfdContextId != u4ContextId)
                {
                    break;
                }

                if ((MEMCMP
                     (&(pBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessSrcAddr),
                      &(pBfdClientInfo->BfdClientNbrIpPathInfo.SrcAddr),
                      u1AddrLen) == 0)
                    &&
                    (MEMCMP
                     (&(pBfdSessIpMapEntry->MibObject.au1FsMIStdBfdSessDstAddr),
                      &(pBfdClientInfo->BfdClientNbrIpPathInfo.NbrAddr),
                      u1AddrLen) == 0))
                {
                    pBfdSessInfo->MibObject.u4FsMIStdBfdContextId = u4ContextId;
                    pBfdSessInfo->MibObject.u4FsMIStdBfdSessIndex =
                        pBfdSessIpMapEntry->MibObject.
                        u4FsMIStdBfdSessIpMapIndex;

                    pBfdSessionEntry =
                        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                                   (tRBElem *) pBfdSessInfo);

                    if (pBfdSessionEntry == NULL)
                    {
                        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                            (UINT1 *) pBfdSessInfo);
                        return;
                    }

                    if (BfdSessDeRegisterClient (pBfdSessionEntry,
                                                 pBfdClientInfo->
                                                 u1BfdClientId) != OSIX_SUCCESS)
                    {
                        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                            (UINT1 *) pBfdSessInfo);
                        return;
                    }
                    break;
                }
                pBfdSessIpMapEntry =
                    BfdGetNextFsMIStdBfdSessIpMapTable (pBfdSessIpMapEntry);
            }
        }

        /* No BFD session established for the requested IP path */

        if ((u1Flag == BFD_CREATE_DYNAMIC_SESSION) &&
            (pBfdClientInfo->BfdClientNbrIpPathInfo.u4IfIndex != 0))
        {

            /* Create a new BFD session for monitoring the requested IP path */
            if (BfdCoreCreateDynamicSession (u4ContextId, pBfdClientInfo) !=
                OSIX_SUCCESS)
            {
                PRINTF ("BfdCoreCreateDynamicSession failed: cxt = %d, ip=%x\n",
                        u4ContextId,
                        pBfdClientInfo->BfdClientNbrIpPathInfo.SrcAddr.
                        u4_addr[0]);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSessInfo);
                return;
            }
        }
    }
    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID, (UINT1 *) pBfdSessInfo);
    return;
}

/*****************************************************************************
 * Function     : BfdSessHandleDeRegisterAll
 * Description  : This function will be invoked whenever a BFD client        *
 *                registers/de-registers for neighbor IP path monitoring     *
 * Input        : uContextId     - Context Id                                *
 *                pBfdClientInfo - Neighbor IP path info                     *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
BfdSessHandleDeRegisterAll (UINT4 u4ContextId, tBfdClientInfo * pBfdClientInfo)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdTempFsMIStdBfdSessEntry = NULL;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    BOOL1               bIsRegisteredClient = OSIX_FALSE;

    UNUSED_PARAM (u4ContextId);
    pBfdFsMIStdBfdSessEntry =
        RBTreeGetFirst (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable);

    while (pBfdFsMIStdBfdSessEntry != NULL)
    {
        MEMCPY (au1BfdClients, &(pBfdFsMIStdBfdSessEntry->MibObject.
                                 u4FsMIBfdSessRegisteredClients),
                sizeof (au1BfdClients));

        OSIX_BITLIST_IS_BIT_SET (au1BfdClients, pBfdClientInfo->u1BfdClientId,
                                 sizeof (au1BfdClients), bIsRegisteredClient);

        if (bIsRegisteredClient == OSIX_TRUE)
        {
            /* De-register the client from the IP path monitoring */
            if (BfdSessDeRegisterClient (pBfdFsMIStdBfdSessEntry,
                                         pBfdClientInfo->u1BfdClientId) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdFsMIStdBfdSessEntry);
            }
        }
        /* Get the next BFD session entry */
        pBfdTempFsMIStdBfdSessEntry = pBfdFsMIStdBfdSessEntry;
        pBfdFsMIStdBfdSessEntry =
            RBTreeGetNext (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                           (tRBElem *) pBfdTempFsMIStdBfdSessEntry, NULL);
    }

    return;

}

/*****************************************************************************
 * Function     : BfdSessRegisterClient                                      *
 * Description  : This function is to update the client parameters info      *
 *                in the corresponding BFD session entry                     *
 * Input        : pBfdSessEntry  - BFD session entry                         *
 *                pBfdClientInfo - Neighbor IP path info                     *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *****************************************************************************/
INT4
BfdSessRegisterClient (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                       tBfdClientInfo * pBfdClientInfo)
{
    tBfdIsSetFsMIStdBfdSessEntry BfdIsSetFsMIStdBfdSessEntry;
    INT4                i4Ret = OSIX_FAILURE;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    UINT4               u4CurrentDetectionTime = 0;
    UINT4               u4ReqInterval = 0;
    UINT1               au1Event[1];
    BOOL1               bIsRegisteredClient = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    MEMSET (au1BfdClients, 0, sizeof (au1BfdClients));
    MEMSET (&BfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    MEMCPY (au1BfdClients, &(pBfdSessEntry->MibObject.
                             u4FsMIBfdSessRegisteredClients),
            sizeof (au1BfdClients));
    OSIX_BITLIST_IS_BIT_SET (au1BfdClients, pBfdClientInfo->u1BfdClientId,
                             sizeof (au1BfdClients), bIsRegisteredClient);
    if (bIsRegisteredClient != OSIX_TRUE)
    {
        /* Increment the Client count if not yet registered */
        pBfdSessEntry->u1BfdClientsCount++;
    }

    /* Set the appropriate bit in the registered client info */
    MEMCPY (au1BfdClients, &(pBfdSessEntry->MibObject.
                             u4FsMIBfdSessRegisteredClients),
            sizeof (au1BfdClients));

    OSIX_BITLIST_SET_BIT (au1BfdClients, pBfdClientInfo->u1BfdClientId,
                          sizeof (au1BfdClients));

    MEMCPY (&(pBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients),
            au1BfdClients, sizeof (au1BfdClients));

    /* Copy the client parameters in the sesion entry */
    MEMCPY (&
            (pBfdSessEntry->
             aBfdClientParams[pBfdClientInfo->u1BfdClientId - 1]),
            &pBfdClientInfo->BfdClientParams, sizeof (tBfdClientParams));

    /* Set the storage type as BFD_OTHER, if this session is associated with
     * dynamic and static path monitoring
     * If it is BFD_NONVOLATILE, then it is associated with static, so
     * change it as BFD_OTHER, since it is now associated with dynamic also
     */
    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType == BFD_NONVOLATILE)
    {
        pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType = BFD_OTHER;
        BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessStorType = OSIX_TRUE;
    }

    if (!((pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE) ||
          (pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
           NOT_IN_SERVICE)))
    {
        pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = ACTIVE;
        if (BfdSessHandleSessionEntryStatus (pBfdSessEntry) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessRowStatus = OSIX_TRUE;
    }

    /* calculate the current detection time */
    u4CurrentDetectionTime = pBfdSessEntry->u4NegRemoteTxTimeIntrvl *
        BFD_SESS_NEG_DETECT_MULT (pBfdSessEntry);

    /* If the detection time desired by the client is less than the current detection time,
     * calculate the desired minimum tx interval from the desired detction time and
     * the configured detct multiplier and inititae the poll sequence
     */
    if ((pBfdClientInfo->BfdClientParams.u4DesiredDetectionTime != 0) &&
        (u4CurrentDetectionTime > pBfdClientInfo->BfdClientParams.
         u4DesiredDetectionTime))
    {
        u4ReqInterval =
            (pBfdClientInfo->BfdClientParams.u4DesiredDetectionTime /
             BFD_SESS_NEG_DETECT_MULT (pBfdSessEntry));

        pBfdSessEntry->MibObject.u4FsMIStdBfdSessDesiredMinTxInterval =
            u4ReqInterval;
        pBfdSessEntry->MibObject.u4FsMIStdBfdSessReqMinRxInterval =
            u4ReqInterval;

        BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessDesiredMinTxInterval =
            OSIX_TRUE;
        BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessReqMinRxInterval = OSIX_TRUE;

        if ((pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE) &&
            (pBfdSessEntry->MibObject.i4FsMIStdBfdSessAdminStatus ==
             BFD_SESS_ADMIN_START))
        {
            if (BfdPollInitiatePollSequence (pBfdSessEntry) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
    }

    BfdUtilDynBfdSessTableTrigger (pBfdSessEntry,
                                   &BfdIsSetFsMIStdBfdSessEntry, SNMP_SUCCESS);

    /* If the client is registering for session up notification, then
     * check if there is already a BFD session present for this index and the
     * session state is UP. If so, notify the client about the BFD session UP */
    if (pBfdSessEntry->MibObject.i4FsMIStdBfdSessState == BFD_SESS_STATE_UP)
    {
        MEMCPY (au1Event, &pBfdSessEntry->
                aBfdClientParams[pBfdClientInfo->u1BfdClientId - 1].
                u1EventMask, 1);
        OSIX_BITLIST_IS_BIT_SET (au1Event, (BFD_SESS_STATE_UP + 1), 1, bResult);
        if (bResult == OSIX_TRUE)
        {
            i4Ret = BfdExtInformSessionState (pBfdSessEntry, BFD_SESS_STATE_UP);
            if (i4Ret == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : BfdSessDeRegisterClient                                    *
 * Description  : This function is to remove the client parameters info      *
 *                in the corresponding BFD session entry                     *
 * Input        : pBfdSessEntry  - BFD session entry                         *
 *                u1BfdClientId  - BFD client ID                             *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *****************************************************************************/

INT4
BfdSessDeRegisterClient (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                         UINT1 u1BfdClientId)
{
    tBfdIsSetFsMIStdBfdSessEntry BfdIsSetFsMIStdBfdSessEntry;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    tBfdClientId        u1TempClientId = BFD_CLIENT_ID_OSPF;
    UINT1               u1Result = OSIX_FALSE;
    BOOL1               bIsRegisteredClient = OSIX_FALSE;

    MEMSET (&BfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    MEMCPY (au1BfdClients, &pBfdSessEntry->MibObject.
            u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

    OSIX_BITLIST_IS_BIT_SET (au1BfdClients, u1BfdClientId,
                             sizeof (au1BfdClients), bIsRegisteredClient);

    if ((bIsRegisteredClient == OSIX_TRUE)
        || (pBfdSessEntry->u1BfdClientsCount == 0))
    {
        if (pBfdSessEntry->u1BfdClientsCount <= 1)
        {
            /* If this is the only client, who registered for the monitoring of
             * this IP path, then delete the session entry */
            pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = DESTROY;
            BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessRowStatus = OSIX_TRUE;

            if (BfdSessHandleSessionEntryStatus (pBfdSessEntry) == OSIX_SUCCESS)
            {
                RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                           pBfdSessEntry);
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSessEntry);
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
        else if (pBfdSessEntry->u1BfdClientsCount > 1)
        {
            /* If some other client is also monitoring the same IP path,
             * then remove only the client info who de-registers from this session
             */
            OSIX_BITLIST_RESET_BIT (au1BfdClients, u1BfdClientId,
                                    sizeof (au1BfdClients));

            MEMCPY (&pBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients,
                    au1BfdClients, sizeof (au1BfdClients));

            MEMSET (&(pBfdSessEntry->aBfdClientParams[u1BfdClientId - 1]), 0,
                    sizeof (tBfdClientParams));

            pBfdSessEntry->u1BfdClientsCount--;

            /* Except IP, all other BFD clients will request for IP path monitoring
             * dynamically. The storage type of the dynamically created entries will be
             * stored as BFD_OTHER.

             * Check if this session is associated with any of the dynamic requests
             * from the BFD clients. If NO, change the storage type to volatile
             */

            do
            {
                OSIX_BITLIST_IS_BIT_SET (au1BfdClients, u1TempClientId,
                                         sizeof (au1BfdClients), u1Result);
                if (u1Result == OSIX_TRUE)
                {
                    /*Session entry associated with dynamic request */
                    break;
                }
                u1TempClientId++;
            }
            while (u1TempClientId < BFD_MAX_CLIENTS);

            if (u1Result == OSIX_FALSE)
            {
                /* Session entry not associated with any of the dynamic requests
                 * from the clients
                 */
                pBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType =
                    BFD_NONVOLATILE;
                BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessStorType = OSIX_TRUE;
            }
        }

        BfdUtilDynBfdSessTableTrigger (pBfdSessEntry,
                                       &BfdIsSetFsMIStdBfdSessEntry,
                                       SNMP_SUCCESS);
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function     : BfdSessDisableClient                                       *
 * Description  : This function is to disable the client                     *
 *                in the corresponding BFD session entry                     *
 * Input        : pBfdSessEntry  - BFD session entry                         *
 *                u1BfdClientId  - BFD client ID                             *
 * Output       : None                                                       *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *****************************************************************************/
INT4
BfdSessDisableClient (tBfdFsMIStdBfdSessEntry * pBfdSessEntry,
                      UINT1 u1BfdClientId)
{
    tBfdIsSetFsMIStdBfdSessEntry BfdIsSetFsMIStdBfdSessEntry;
    UINT1               au1BfdClients[BFD_CLIENT_STORAGE_SIZE];
    BOOL1               bIsRegisteredClient = OSIX_FALSE;

    MEMSET (&BfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));

    MEMCPY (au1BfdClients, &pBfdSessEntry->MibObject.
            u4FsMIBfdSessRegisteredClients, sizeof (au1BfdClients));

    OSIX_BITLIST_IS_BIT_SET (au1BfdClients, u1BfdClientId,
                             sizeof (au1BfdClients), bIsRegisteredClient);

    if (bIsRegisteredClient == OSIX_TRUE)
    {
        if (pBfdSessEntry->u1BfdClientsCount == 1)
        {
            /* If this is the only client, who registered for the monitoring of
             * this IP path, then disable the session */
            pBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus = NOT_READY;

            if (BfdSessHandleSessionEntryStatus (pBfdSessEntry) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            BfdIsSetFsMIStdBfdSessEntry.bFsMIStdBfdSessRowStatus = OSIX_TRUE;
            BfdUtilDynBfdSessTableTrigger (pBfdSessEntry,
                                           &BfdIsSetFsMIStdBfdSessEntry,
                                           SNMP_SUCCESS);
        }

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

#endif /* _BFDSESS_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdsess.c                      */
/*-----------------------------------------------------------------------*/
