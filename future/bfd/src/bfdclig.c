/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfdclig.c,v 1.12 2017/06/08 11:40:29 siva Exp $
*
* Description: This file contains the Bfd CLI related routines 
*********************************************************************/
#ifndef __BFDCLIG_C__
#define __BFDCLIG_C__
#include "bfdinc.h"
#include "bfdclig.h"

/****************************************************************************
 * Function    :  cli_process_Bfd_cmd
 * Description :  This function is exported to CLI module to handle the
                BFD cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Bfd_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[BFD_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4Args;

    tBfdFsMIStdBfdGlobalConfigTableEntry
        * pBfdSetFsMIStdBfdGlobalConfigTableEntry = NULL;
    tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
        * pBfdIsSetFsMIStdBfdGlobalConfigTableEntry = NULL;

    tBfdFsMIStdBfdSessEntry *pBfdSetFsMIStdBfdSessEntry = NULL;
    tBfdIsSetFsMIStdBfdSessEntry *pBfdIsSetFsMIStdBfdSessEntry = NULL;

    CliRegisterLock (CliHandle, BfdMainTaskLock, BfdMainTaskUnLock);
    UNUSED_PARAM (u4CmdType);
    BFD_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    UNUSED_PARAM (u4IfIndex);
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == BFD_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_BFD_FSMISTDBFDGLOBALCONFIGTABLE:
            if (args[13] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[13]);
                BfdCliSetDebugLevel (CliHandle, i4Args);
            }

            pBfdSetFsMIStdBfdGlobalConfigTableEntry =
                (tBfdFsMIStdBfdGlobalConfigTableEntry *)
                MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

            if (pBfdSetFsMIStdBfdGlobalConfigTableEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pBfdSetFsMIStdBfdGlobalConfigTableEntry, 0,
                    sizeof (tBfdFsMIStdBfdGlobalConfigTableEntry));

            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry =
                (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry *)
                MemAllocMemBlk (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID);

            if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry == NULL)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                                    (UINT1 *)
                                    pBfdSetFsMIStdBfdGlobalConfigTableEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry, 0,
                    sizeof (tBfdIsSetFsMIStdBfdGlobalConfigTableEntry));

            BFD_FILL_FSMISTDBFDGLOBALCONFIGTABLE_ARGS ((pBfdSetFsMIStdBfdGlobalConfigTableEntry), (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12]);

            i4RetStatus =
                BfdCliSetFsMIStdBfdGlobalConfigTable (CliHandle,
                                                      (pBfdSetFsMIStdBfdGlobalConfigTableEntry),
                                                      (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry));
            MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *)
                                pBfdSetFsMIStdBfdGlobalConfigTableEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *)
                                pBfdIsSetFsMIStdBfdGlobalConfigTableEntry);

            break;

        case CLI_BFD_FSMISTDBFDSESSTABLE:

            pBfdSetFsMIStdBfdSessEntry =
                (tBfdFsMIStdBfdSessEntry *)
                MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

            if (pBfdSetFsMIStdBfdSessEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pBfdSetFsMIStdBfdSessEntry, 0,
                    sizeof (tBfdFsMIStdBfdSessEntry));

            pBfdIsSetFsMIStdBfdSessEntry =
                (tBfdIsSetFsMIStdBfdSessEntry *)
                MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);

            if (pBfdIsSetFsMIStdBfdSessEntry == NULL)
            {
                MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                    (UINT1 *) pBfdSetFsMIStdBfdSessEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pBfdIsSetFsMIStdBfdSessEntry, 0,
                    sizeof (tBfdIsSetFsMIStdBfdSessEntry));

            BFD_FILL_FSMISTDBFDSESSTABLE_ARGS ((pBfdSetFsMIStdBfdSessEntry),
                                               (pBfdIsSetFsMIStdBfdSessEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5],
                                               args[6], args[7], args[8],
                                               args[9], args[10], args[11],
                                               args[12], args[13], args[14],
                                               args[15], args[16], args[17],
                                               args[18], args[19], args[20],
                                               args[21], args[22], args[23],
                                               args[24], args[25], args[26],
                                               args[27], args[28], args[29],
                                               args[30], args[31], args[32],
                                               args[33], args[34], args[35],
                                               args[36], args[37], args[38],
                                               args[39], args[40], args[41],
                                               args[42], args[43], args[44]);

            i4RetStatus =
                BfdCliSetFsMIStdBfdSessTable (CliHandle,
                                              (pBfdSetFsMIStdBfdSessEntry),
                                              (pBfdIsSetFsMIStdBfdSessEntry));
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdSetFsMIStdBfdSessEntry);
            MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                                (UINT1 *) pBfdIsSetFsMIStdBfdSessEntry);

            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_BFD_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", BfdCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    BFD_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  BfdCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
BfdCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{

    UNUSED_PARAM (CliHandle);
    BFD_TRC_LVL = 0;

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        BFD_TRC_LVL = BFD_FN_ENTRY |
            BFD_FN_EXIT |
            BFD_CLI_TRC |
            BFD_MAIN_TRC |
            BFD_PKT_TRC |
            BFD_QUE_TRC | BFD_TASK_TRC | BFD_TMR_TRC | BFD_UTIL_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        BFD_TRC_LVL = BFD_FN_ENTRY | BFD_FN_EXIT;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        BFD_TRC_LVL = BFD_FN_ENTRY |
            BFD_FN_EXIT |
            BFD_CLI_TRC |
            BFD_MAIN_TRC | BFD_PKT_TRC | BFD_QUE_TRC | BFD_TASK_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        BFD_TRC_LVL = BFD_FN_ENTRY |
            BFD_FN_EXIT |
            BFD_CLI_TRC |
            BFD_MAIN_TRC | BFD_PKT_TRC | BFD_QUE_TRC | BFD_TASK_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        BFD_TRC_LVL = BFD_FN_ENTRY |
            BFD_FN_EXIT |
            BFD_CLI_TRC |
            BFD_MAIN_TRC |
            BFD_PKT_TRC |
            BFD_QUE_TRC | BFD_TASK_TRC | BFD_TMR_TRC | BFD_UTIL_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
        {
            return CLI_SUCCESS;
        }
        BFD_TRC_LVL = BFD_PKT_TRC | BFD_QUE_TRC | BFD_TASK_TRC | BFD_TMR_TRC;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  BfdCliSetFsMIStdBfdGlobalConfigTable
* Description :
* Input       :  CliHandle 
*            pBfdSetFsMIStdBfdGlobalConfigTableEntry
*            pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
BfdCliSetFsMIStdBfdGlobalConfigTable (tCliHandle CliHandle,
                                      tBfdFsMIStdBfdGlobalConfigTableEntry *
                                      pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                                      tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
                                      *
                                      pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    UINT4               u4ErrorCode;

    if (BfdTestAllFsMIStdBfdGlobalConfigTable
        (&u4ErrorCode, pBfdSetFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (BfdSetAllFsMIStdBfdGlobalConfigTable
        (pBfdSetFsMIStdBfdGlobalConfigTableEntry,
         pBfdIsSetFsMIStdBfdGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to configure BFD "
                   "global configuration table\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  BfdCliSetFsMIStdBfdSessTable
* Description :
* Input       :  CliHandle 
*            pBfdSetFsMIStdBfdSessEntry
*            pBfdIsSetFsMIStdBfdSessEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
BfdCliSetFsMIStdBfdSessTable (tCliHandle CliHandle,
                              tBfdFsMIStdBfdSessEntry *
                              pBfdSetFsMIStdBfdSessEntry,
                              tBfdIsSetFsMIStdBfdSessEntry *
                              pBfdIsSetFsMIStdBfdSessEntry)
{
    UINT4               u4ErrorCode;

    if (BfdTestAllFsMIStdBfdSessTable (&u4ErrorCode, pBfdSetFsMIStdBfdSessEntry,
                                       pBfdIsSetFsMIStdBfdSessEntry, OSIX_TRUE,
                                       OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4ErrorCode == SNMP_ERR_RESOURCE_UNAVAILABLE)
    {
        CliPrintf (CliHandle, "Warning: Path is down!\n"
                   "BFD will monitor the path once it is UP!\n");
    }

    if (BfdSetAllFsMIStdBfdSessTable
        (pBfdSetFsMIStdBfdSessEntry, pBfdIsSetFsMIStdBfdSessEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to configure BFD session table\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#endif
