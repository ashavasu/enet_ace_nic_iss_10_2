/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdred.c,v 1.15 2015/09/04 07:20:12 siva Exp $
 *
 * Description: This file contains BFD High Availability related 
 *              Core functionalities
 *******************************************************************/
#ifndef __BFDRED_C
#define __BFDRED_C

#include "bfdinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDynDataDescInit                            */
/*                                                                           */
/*    Description         : This function will initialize session dynamic    */
/*                          info data descriptor.                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pBfdDynDataDesc = NULL;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedDynDataDescInit");
    pBfdDynDataDesc = &(gaBfdDynDataDescList[BFD_DYN_INFO]);

    pBfdDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pBfdDynDataDesc->u4DbDataSize =
        sizeof (tBfdMibFsMIStdBfdSessEntry) -
        FSAP_OFFSETOF (tBfdMibFsMIStdBfdSessEntry, u4FsMIStdBfdContextId);


    pBfdDynDataDesc->pDbDataOffsetTbl = gaBfdOffsetTbl;
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedDynDataDescInit");

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDescrTblInit                               */
/*                                                                           */
/*    Description         : This function will initialize Bfd descriptor     */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDescrTblInit (VOID)
{
    tDbDescrParams      BfdDescrParams;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedDescrTblInit");
    MEMSET (&BfdDescrParams, 0, sizeof (tDbDescrParams));

    BfdDescrParams.u4ModuleId = RM_BFD_APP_ID;

    BfdDescrParams.pDbDataDescList = gaBfdDynDataDescList;

    DbUtilTblInit (&gBfdDynInfoList, &BfdDescrParams);

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedDescrTblInit");

    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : BfdRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register BFD with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
BfdRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedInitGlobalInfo");
    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_BFD_APP_ID;

    RmRegParams.pFnRcvPkt = BfdRedRmCallBack;

    /* Register BFD with RM */
    if (RmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        BFD_LOG (u4ContextId, BFD_TRC_FAIL_TO_REG_WITH_RM);
        return OSIX_FAILURE;
    }

    MEMSET (&gBfdRedGblInfo, 0, sizeof (tBfdRedGlobalInfo));

    BFD_GET_NODE_STATUS () = RM_INIT;
    BFD_NUM_STANDBY_NODES () = 0;
    BFD_RM_BULK_REQ_RCVD () = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedInitGlobalInfo");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDbNodeInit                                 */
/*                                                                           */
/*    Description         : This function will initialize Bfd Oper db node.  */
/*                                                                           */
/*    Input(s)            : pBfdDbTblNode - pointer to Bfd Db Entry.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDbNodeInit (tDbTblNode * pBfdDbTblNode)
{
    DbUtilNodeInit (pBfdDbTblNode, BFD_DYN_INFO);

    return;
}

/************************************************************************
 * Function Name      : BfdRedDeInitGlobalInfo
 *
 * Description        : This function is invoked by the BFD module
 *                      during module shutdown and this function
 *                      deinitializes the redundancy global variables
 *                      and de-register BFD with RM.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

INT4
BfdRedDeInitGlobalInfo (VOID)
{
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedDeInitGlobalInfo");

    if (RmDeRegisterProtocols (RM_BFD_APP_ID) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedDeInitGlobalInfo");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : BfdRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to BFD        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
BfdRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tBfdReqParams      *pMsg = NULL;
    UINT4               u4ContextId = 0;

    /* Callback function for RM events. The event and the message is sent as
     * parameters to this function */
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedRmCallBack");

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        BFD_LOG (u4ContextId, BFD_TRC_NO_SUCH_RM_EVENT);
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        BFD_LOG (u4ContextId, BFD_TRC_NO_QUEUE_FOR_RM_MSG);
        return OSIX_FAILURE;
    }

    /* Allocate memory for Queue Message */
    if ((pMsg = (tBfdReqParams *) MemAllocMemBlk (BFD_QUEMSG_POOLID)) == NULL)
    {
        BFD_LOG (u4ContextId,
                 BFD_MEM_POOL_ALLOCATE_FAIL, "BFD", "BfdRedRmCallBack");

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            BfdRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tBfdReqParams));

    pMsg->unReqInfo.BfdRmMsg.RmCtrlMsg.pData = pData;
    pMsg->unReqInfo.BfdRmMsg.RmCtrlMsg.u1Event = u1Event;
    pMsg->unReqInfo.BfdRmMsg.RmCtrlMsg.u2DataLen = u2DataLen;

    pMsg->u4ReqType = BFD_RM_MSG_RECVD;
    /* Post event to BFD Task. Event - BFD_RM_MSG_RECVD */
    BfdQueEnqMsg (pMsg);
    BFD_LOG (u4ContextId, BFD_TRC_SENT_RM_EVENT);

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedRmCallBack");

    return OSIX_SUCCESS;

}

/************************************************************************/
/* Function Name      : BfdRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the Bfd module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
BfdRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UINT4               u4ContextId = 0;
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedRmReleaseMemoryForMsg");

    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedRmReleaseMemoryForMsg");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : BfdRedHandleRmEvents
 *
 * Description        : This function is invoked by the BFD module to
 *                      process all the events and messages posted by
 *                      the RM module
 *
 * Input(s)           : pMsg -- Pointer to the BFD Q Msg
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/
PUBLIC VOID
BfdRedHandleRmEvents (tBfdRmMsg * pBfdRmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleRmEvents");

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    switch (pBfdRmMsg->RmCtrlMsg.u1Event)
    {
        case GO_ACTIVE:

            BFD_LOG (u4ContextId, BFD_TRC_RM_RECVD_GO_ACT_EVT);
            BfdRedHandleGoActive ();
            break;
        case GO_STANDBY:
            BFD_LOG (u4ContextId, BFD_TRC_RM_RECVD_GO_STANDBY_EVT);
            BfdRedHandleGoStandby (pBfdRmMsg);
            /* pMsg is passed as a argument to GoStandby function and
             * it is released there. If the transformation is from active
             * to standby then there is no need for releasing the mempool
             * as we are calling DeInit function. This function clears
             * all the mempools, so it is returned here instead of break.*/
            break;
        case RM_STANDBY_UP:
            BFD_LOG (u4ContextId, BFD_TRC_RM_STANDBY_UP_EVT);

            /* Standby up event, number of standby node is updated.
             * BulkReqRcvd flag is checked, if it is true, then Bulk update
             * message is sent to the standby node and the flag is reset */

            pData = (tRmNodeInfo *) pBfdRmMsg->RmCtrlMsg.pData;
            BFD_NUM_STANDBY_NODES () = pData->u1NumStandby;
            BfdRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (BFD_RM_BULK_REQ_RCVD () == OSIX_TRUE)
            {
                BFD_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_NOT_STARTED;
                BfdRedSendBulkUpdMsg ();
            }
            break;
        case RM_STANDBY_DOWN:
            BFD_LOG (u4ContextId, BFD_TRC_RM_STANDBY_DOWN_EVT);
            /* Standby down event, number of standby nodes is updated */
            pData = (tRmNodeInfo *) pBfdRmMsg->RmCtrlMsg.pData;
            BFD_NUM_STANDBY_NODES () = pData->u1NumStandby;
            BfdRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;
        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pBfdRmMsg->RmCtrlMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pBfdRmMsg->RmCtrlMsg.pData,
                                 pBfdRmMsg->RmCtrlMsg.u2DataLen);

            ProtoAck.u4AppId = RM_BFD_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gBfdRedGblInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* Process the message at active */
                BfdRedProcessPeerMsgAtActive (pBfdRmMsg->RmCtrlMsg.pData,
                                              pBfdRmMsg->RmCtrlMsg.u2DataLen);
            }
            else if (gBfdRedGblInfo.u1NodeStatus == RM_STANDBY)
            {

                /* Process the message at standby */
                BfdRedProcessPeerMsgAtStandby (pBfdRmMsg->RmCtrlMsg.pData,
                                               pBfdRmMsg->RmCtrlMsg.u2DataLen);
            }

            RM_FREE (pBfdRmMsg->RmCtrlMsg.pData);
            RmApiSendProtoAckToRM (&ProtoAck);
            break;
        case RM_CONFIG_RESTORE_COMPLETE:
            /* Config restore complete event is sent by the RM on completing
             * the static configurations. If the node status is init,
             * and the rm state is standby, then the VRRP status is changed
             * from idle to standby */
            if (gBfdRedGblInfo.u1NodeStatus == RM_INIT)
            {
                if (BFD_GET_RMNODE_STATUS () == RM_STANDBY)
                {
                    BfdRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    RmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            /* L2 Initiate bulk update is sent by RM to the standby node
             * to send a bulk update request to the active node */
            BfdRedSendBulkReqMsg ();
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            BfdRedHandleDynSyncAudit ();
            break;

        default:
            break;
    }
    /*MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pBfdRmMsg); */
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleRmEvents");
    return;
}

/************************************************************************
 * Function Name      : BfdRedHandleGoActive
 *
 * Description        : This function is invoked by the BFD upon
 *                      receiving the GO_ACTIVE indication from RM
 *                      module. And this function responds to RM with an
 *                      acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
BfdRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleGoActive");
    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_NOT_STARTED;

    if (BFD_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        return;
    }
    if (BFD_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        BfdRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (BFD_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node,
         * Do hardware audit, and start the timers for all the arp entries
         * and change the state to active */
        BfdRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (BFD_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        BFD_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_NOT_STARTED;
        BfdRedSendBulkUpdMsg ();
    }

    RmApiHandleProtocolEvent (&ProtoEvt);
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleGoActive");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleIdleToActive (VOID)
{
    UINT4               u4ContextId = 0;
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleIdleToActive");
    /* Node status is set to active and the number of standby nodes
     * are updated */
    BFD_GET_NODE_STATUS () = RM_ACTIVE;
    BFD_RM_GET_NUM_STANDBY_NODES_UP ();

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleIdleToActive");

    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleStandbyToActive (VOID)
{
    UINT4               u4ContextId = 0;
    UINT1               u1TmrStart = OSIX_TRUE;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleStandbyToActive");
    /* Hardware audit is done and the hardware and software entries are
     * checked for synchronization */

    BFD_GET_NODE_STATUS () = RM_ACTIVE;
    BfdRedHwAudit (u1TmrStart);
    gu2OverflowSessionId = BFD_DEF_SESS_ID;
    BFD_RM_GET_NUM_STANDBY_NODES_UP ();

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleStandbyToActive");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedSendBulkUpdMsg (VOID)
{
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedSendBulkUpdMsg");
    if (BFD_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }
    if (gBfdRedGblInfo.u1BulkUpdStatus == BFD_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_IN_PROGRESS;
        BfdRedAddAllNodeInDbTbl ();

        BfdRedSyncDynInfo ();

        gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_COMPLETED;
        BfdRedSendBulkUpdTailMsg ();
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedSendBulkUpdMsg");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  BfdRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedAddAllNodeInDbTbl (VOID)
{
    UINT4               u4ContextId = 0;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedAddAllNodeInDbTbl");

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();
    while (pBfdFsMIStdBfdSessEntry != NULL)
    {
        BfdRedDbUtilAddTblNode (&gBfdDynInfoList,
                                &(pBfdFsMIStdBfdSessEntry->MibObject.
                                  SessDbNode));
        pBfdFsMIStdBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedAddAllNodeInDbTbl");
    return;
}

/*****************************************************************************/
/*    Function Name       : BfdRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate BFD dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedSyncDynInfo (VOID)
{
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedSyncDynInfo");

    if ((BFD_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gBfdRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

#ifdef BFD_HA_TEST_WANTED
    if (OSIX_TRUE == gBfdTestGlobals.u4AbortBulkUpdate)
    {
        printf ("\n\nAborted BFD bulk update!!!");
        assert (0);
        return;
    }
#endif

    DbUtilSyncModuleNodes (&gBfdDynInfoList);
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedSyncDynInfo");

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pBfdDataDesc - This is BFD sepcific data desri-  */
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pBfdSessDbNode -This is db node defined in the BFD*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDbUtilAddTblNode (tDbTblDescriptor * pBfdDataDesc,
                        tDbTblNode * pBfdSessDbNode)
{
    UINT4               u4ContextId = 0;
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedDbUtilAddTblNode");

    if ((BFD_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gBfdRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pBfdDataDesc, pBfdSessDbNode);
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedDbUtilAddTblNode");

    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends                             */
/*                                                                      */
/* Input(s)           : pBfdBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
BfdRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedSendBulkUpdTailMsg");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * BFD_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (BFD_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u4OffSet = 0;

    BFD_RM_PUT_1_BYTE (pMsg, &u4OffSet, BFD_RED_BULK_UPD_TAIL_MESSAGE);
    BFD_RM_PUT_2_BYTE (pMsg, &u4OffSet, BFD_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (BfdRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedSendBulkUpdTailMsg");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
BfdRedSendMsgToRm (tRmMsg * pMsg, UINT4 u4Length)
{
    UINT4               u4RetVal = 0;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedSendMsgToRm");

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, (UINT2) u4Length,
                                     RM_BFD_APP_ID, RM_BFD_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedSendMsgToRm");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : BfdRedHandleGoStandby
 *
 * Description        : This function is invoked by the BFD upon
 *                      receiving the GO_STANBY indication from RM
 *                      module. And this function responds to RM module
 *                      with an acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
BfdRedHandleGoStandby (tBfdRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleGoStandby");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
/*    MemReleaseMemBlock (BFD_QUEMSG_POOLID, (UINT1 *) pMsg); */
    UNUSED_PARAM (pMsg);
    gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_NOT_STARTED;

    if (BFD_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleGoStandby");
        return;
    }
    if (BFD_GET_NODE_STATUS () == RM_INIT)
    {
        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleGoStandby");
        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        BfdRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
        {
            BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleGoStandby");
            return;
        }
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleGoStandby");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleActiveToStandby (VOID)
{
    UINT4               u4ContextId = 0;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleActiveToStandby");

    pBfdFsMIStdBfdSessEntry = BfdGetFirstFsMIStdBfdSessTable ();
    /* Stop the running timers in the active node. Check whether any timers are
     * running, if running, stop those timers. If there are any messages in the
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */
    while (pBfdFsMIStdBfdSessEntry != NULL)
    {
        BfdTmrStop (pBfdFsMIStdBfdSessEntry, BFD_TRANSMIT_TMR);
        BfdTmrStop (pBfdFsMIStdBfdSessEntry, BFD_DETECTION_TMR);


        pBfdFsMIStdBfdSessEntry->MibObject.
            i4FsMIStdBfdSessState = BFD_SESS_STATE_DOWN;
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRemoteHeardFlag = 0;
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessNegotiatedInterval = 0;
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessNegotiatedEchoInterval = 0;
        pBfdFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessNegotiatedDetectMult = 0;
        pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlReq = 0;
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessAdminCtrlErrReason = 0;
		if (BFD_CHECK_SESS_IS_DYNAMIC(pBfdFsMIStdBfdSessEntry))
		{
			pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface = 0;
			pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessType = 0;
			pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessStorType = 0;
			pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType = 0;
			pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType = 0;
			pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessRegisteredClients = 0;
			MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr, 0, 256);
			MEMSET (pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr, 0, 256);
		}

        pBfdFsMIStdBfdSessEntry =
            BfdGetNextFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry);
    }

    /*update the statistics */
    BFD_GET_NODE_STATUS () = RM_STANDBY;
    BFD_RM_GET_NUM_STANDBY_NODES_UP ();

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleActiveToStandby");

    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                     */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;
    UINT4               u4ContextId = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedProcessPeerMsgAtActive");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;

    BFD_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    BFD_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != BFD_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == BFD_RED_BULK_REQ_MESSAGE)
    {
        gBfdRedGblInfo.u1BulkUpdStatus = BFD_HA_UPD_NOT_STARTED;
        if (!BFD_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gBfdRedGblInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        BfdRedSendBulkUpdMsg ();
    }
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedProcessPeerMsgAtActive");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedProcessPeerMsgAtStandby");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    u2MinLen = BFD_RED_TYPE_FIELD_SIZE + BFD_RED_LEN_FIELD_SIZE;

    while (u4OffSet <= u2DataLen)
    {
        u2ExtractMsgLen = (UINT2) u4OffSet;
        BFD_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        BFD_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }

        switch (u1MsgType)
        {
            case BFD_RED_BULK_UPD_TAIL_MESSAGE:
                BfdRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case BFD_RED_DYN_CACHE_INFO:
                BfdRedProcessDynamicInfo (pMsg, &u4OffSet);
                break;
            default:
                break;
        }
    }

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedProcessPeerMsgAtStandby");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedProcessBulkTailMsg");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedProcessBulkTailMsg");

    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
BfdRedProcessDynamicInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
	tBfdFsMIStdBfdSessEntry *pTemBfdFsMIStdBfdSessEntry = NULL;
	tBfdFsMIStdBfdSessIpMapEntry *pBfdSessIpMapEntryExist = NULL;
	tBfdFsMIStdBfdSessIpMapEntry BfdSessIpMapEntry;  
    UINT4               u4ContextId = 0;
    UINT4               u4BfdLocalTempVar = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedProcessDynamicInfo");
MEMSET (&BfdSessIpMapEntry, 0, sizeof (tBfdFsMIStdBfdSessIpMapEntry));

    /* Allocate memory for the new node */
    pTemBfdFsMIStdBfdSessEntry =
        (tBfdFsMIStdBfdSessEntry *)
        MemAllocMemBlk (BFD_FSMISTDBFDSESSTABLE_POOLID);
    if (pTemBfdFsMIStdBfdSessEntry == NULL)
    {
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdRedProcessDynamicInfo: Fail to Allocate Memory.\r\n"));
        return;
    }

    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       pTemBfdFsMIStdBfdSessEntry->MibObject.
                       u4FsMIStdBfdContextId);
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       pTemBfdFsMIStdBfdSessEntry->MibObject.
                       u4FsMIStdBfdSessIndex);

    /* Check whether the node is already present */
    pBfdFsMIStdBfdSessEntry =
        RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                   (tRBElem *) pTemBfdFsMIStdBfdSessEntry);

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                            (UINT1 *) pTemBfdFsMIStdBfdSessEntry);
        BFD_TRC ((BFD_UTIL_TRC,
                  "BfdGetAllFsMIStdBfdSessTable: Entry doesn't exist\r\n"));
        return;
    }

    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       BFD_SESS_LOCAL_DISC (pBfdFsMIStdBfdSessEntry));
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       pBfdFsMIStdBfdSessEntry->MibObject.
                       u4FsMIStdBfdSessRemoteDiscr);

    /* To Fix FC17 Warnings for BFD_SESS_STATE this assginment is done */
    u4BfdLocalTempVar = (UINT4) BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry);

    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry) = (INT4) u4BfdLocalTempVar;
    /* END - To Fix FC17 Warnings for BFD_SESS_STATE this assginment is done */

    /* To Fix FC17 Warnings for i4FsMIStdBfdSessRemoteHeardFlag this assginment is done */
    u4BfdLocalTempVar = (UINT4) (pBfdFsMIStdBfdSessEntry->MibObject.
                                 i4FsMIStdBfdSessRemoteHeardFlag);
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessRemoteHeardFlag = (INT4) u4BfdLocalTempVar;
    /*END Fix FC17 Warnings for i4FsMIStdBfdSessRemoteHeardFlag this assginment is done */

    /*To Fix FC17 Warnings for BFD_SESS_DIAG this assginment is done */
    u4BfdLocalTempVar = (UINT4) BFD_SESS_DIAG (pBfdFsMIStdBfdSessEntry);

    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    BFD_SESS_DIAG (pBfdFsMIStdBfdSessEntry) = (INT4) u4BfdLocalTempVar;
    /*END To Fix FC17 Warnings for BFD_SESS_DIAG this assginment is done */

    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       BFD_SESS_NEG_TX (pBfdFsMIStdBfdSessEntry));
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       pBfdFsMIStdBfdSessEntry->MibObject.
                       u4FsMIStdBfdSessNegotiatedEchoInterval);
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet,
                       BFD_SESS_NEG_DETECT_MULT (pBfdFsMIStdBfdSessEntry));

    /*To Fix FC17 Warnings for i4FsMIBfdSessAdminCtrlReq this assginment is done */
    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessAdminCtrlReq;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessAdminCtrlReq = (INT4) u4BfdLocalTempVar;
    /*END To Fix FC17 Warnings for i4FsMIBfdSessAdminCtrlReq this assginment is done */

    /*To Fix FC17 Warnings for i4FsMIBfdSessAdminCtrlErrReason this assginment is done */
    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessAdminCtrlErrReason;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIBfdSessAdminCtrlErrReason = (INT4) u4BfdLocalTempVar;
    /*EndTo Fix FC17 Warnings for i4FsMIBfdSessAdminCtrlErrReason this assginment is done */

    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessInterface;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessInterface = (INT4) u4BfdLocalTempVar;

    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessType;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessType = (INT4) u4BfdLocalTempVar;

    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessStorType;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessStorType = (INT4) u4BfdLocalTempVar;


    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessSrcAddrType;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessSrcAddrType = (INT4) u4BfdLocalTempVar;

    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDstAddrType;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDstAddrType = (INT4) u4BfdLocalTempVar;


	if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType == BFD_INET_ADDR_IPV4)
	{
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType =  BFD_PATH_TYPE_IPV4;
	}
	else if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType == BFD_INET_ADDR_IPV6)
	{
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType = BFD_PATH_TYPE_IPV6;
	}

    u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessSrcAddrLen;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessSrcAddrLen = (INT4) u4BfdLocalTempVar;
    
	u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDstAddrLen;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        i4FsMIStdBfdSessDstAddrLen = (INT4) u4BfdLocalTempVar;

    BFD_RM_GET_N_BYTE (pMsg, pu4OffSet, pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr, 256);
    BFD_RM_GET_N_BYTE (pMsg, pu4OffSet, pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr, 256);
    
	u4BfdLocalTempVar = (UINT4) pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIBfdSessRegisteredClients;
    BFD_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BfdLocalTempVar);
    pBfdFsMIStdBfdSessEntry->MibObject.
        u4FsMIBfdSessRegisteredClients = (INT4) u4BfdLocalTempVar;

	BfdSessIpMapEntry.MibObject.u4FsMIStdBfdContextId = 
		pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
	BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessInterface =
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessInterface;


	BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrLen =
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrLen;

	MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessSrcAddr),
			pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessSrcAddr, 
			BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrLen);

	BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessSrcAddrType =
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessSrcAddrType;


	BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrLen =
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrLen;

	MEMCPY (&(BfdSessIpMapEntry.MibObject.au1FsMIStdBfdSessDstAddr),
			pBfdFsMIStdBfdSessEntry->MibObject.au1FsMIStdBfdSessDstAddr, 
			BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrLen );

	BfdSessIpMapEntry.MibObject.i4FsMIStdBfdSessDstAddrType =
		pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessDstAddrType;
	pBfdSessIpMapEntryExist =
		RBTreeGet (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessIpMapTable,
				(tRBElem *) & BfdSessIpMapEntry);

	if (pBfdSessIpMapEntryExist == NULL)
	{
		BfdCoreCreateBfdSessIpMapEntry (pBfdFsMIStdBfdSessEntry);

	}

    MemReleaseMemBlock (BFD_FSMISTDBFDSESSTABLE_POOLID,
                        (UINT1 *) pTemBfdFsMIStdBfdSessEntry);
    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedProcessDynamicInfo");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleIdleToStandby (VOID)
{
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHandleIdleToStandby");
    /* the node status is set to standby and no of standby nodes is set to 0 */
    BFD_GET_NODE_STATUS () = RM_STANDBY;
    BFD_NUM_STANDBY_NODES () = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHandleIdleToStandby");
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4ContextId = 0;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedSendBulkReqMsg");

    ProtoEvt.u4AppId = RM_BFD_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    BFD Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */
    if ((pMsg = RM_ALLOC_TX_BUF (BFD_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    BFD_RM_PUT_1_BYTE (pMsg, &u4OffSet, BFD_RED_BULK_REQ_MESSAGE);
    BFD_RM_PUT_2_BYTE (pMsg, &u4OffSet, BFD_RED_BULK_REQ_MSG_SIZE);

    if (BfdRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedSendBulkReqMsg");

    return;
}

/*****************************************************************************/
/* Function Name      : BfdRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
BfdRedHandleDynSyncAudit ()
{
    /*On receiving this event, Bfd should execute show cmd and calculate checksum */
    BfdExecuteCmdAndCalculateChkSum ();
}

/************************************************************************
 * Function Name      : BfdRedHwAudit
 *
 * Description        : This function does the hardware audit in the
 *                      following approach.
 *
 *                      When there is a transaction between standby and
 *                      active node, the BfdRedTable is walked, if
 *                      there  are any entries in the table, they are
 *                      verified with the hardware, if the entry is
 *                      present in the hardware, then the entry is
 *                      added to the sofware. If not, the entry is
 *                      deleted.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/
VOID
BfdRedHwAudit (UINT1 u1TmrStart)
{
    UINT4               u4ContextId = 0;
    tBfdIsSetFsMIStdBfdSessEntry BfdIsSetFsMIStdBfdSessEntry;
    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdFsMIStdBfdSessEntry *pBfdTempFsMIStdBfdSessEntry = NULL;

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT, "BfdRedHwAudit");

    MEMSET (&BfdIsSetFsMIStdBfdSessEntry, 0,
            sizeof (tBfdIsSetFsMIStdBfdSessEntry));
    pBfdFsMIStdBfdSessEntry =
        RBTreeGetFirst (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable);

    while (pBfdFsMIStdBfdSessEntry != NULL)
    {
        if (BFD_SESS_OFFLD (pBfdFsMIStdBfdSessEntry) == BFD_SNMP_TRUE)
        {
            if (OSIX_SUCCESS == BfdOffSessInfo
                (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 pBfdFsMIStdBfdSessEntry,
                 pBfdFsMIStdBfdSessEntry, BFD_OFF_GET_SESS_PARAMS))
            {
                /* The session entry is present both in the H/W and the S/W. Get the 
                   next entry */
                gu2OverflowSessionId =
                    (UINT2) (pBfdFsMIStdBfdSessEntry->MibObject.
                             u4FsMIStdBfdSessIndex);
            }
        }
        else if (u1TmrStart == OSIX_TRUE)
        {
            BfdCoreUpdateExtMod (pBfdFsMIStdBfdSessEntry, BFD_VALID_SESS_IDX);
            BfdTxFormBfdCtrlPkt (pBfdFsMIStdBfdSessEntry, OSIX_FALSE,
                                 OSIX_FALSE);
            BfdSessChkAndTransmitPkt (pBfdFsMIStdBfdSessEntry);
            if (BFD_SESS_STATE (pBfdFsMIStdBfdSessEntry) == BFD_SESS_STATE_UP)
            {

                /*start slow detection timer as the
                 * u4NegRemoteTxTimeIntrvl is noted synced dynamically.
                 * Once after the first packet is received the interval
                 * will be calculated and then moved accordingly*/
                if (pBfdFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessReqMinRxInterval != 0)
                {
                    BfdTmrStart (pBfdFsMIStdBfdSessEntry,
                                 BFD_DETECTION_TMR, BFD_DETECTION_SLOW_TMR);
                }
                else
                {
                    BfdTmrStart (pBfdFsMIStdBfdSessEntry,
                                 BFD_DETECTION_TMR, BFD_DETECTION_FAST_TMR);
                }
#ifdef BFD_HA_TEST_WANTED
                if (BFD_TRANSMIT_FAST_TMR !=
                    pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr)
                {
                    pBfdFsMIStdBfdSessEntry->i4BfdTestSessPrevTmr =
                        pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr;
                    pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr =
                        BFD_TRANSMIT_FAST_TMR;
                }
#endif
            }
            else
            {

                /*start slow detection timer */
                BfdTmrStart (pBfdFsMIStdBfdSessEntry,
                             BFD_DETECTION_TMR, BFD_DETECTION_SLOW_TMR);
#ifdef BFD_HA_TEST_WANTED
                pBfdFsMIStdBfdSessEntry->i4BfdTestSessPrevTmr =
                    pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr;
                pBfdFsMIStdBfdSessEntry->i4BfdTestSessCurrTmr =
                    BFD_TRANSMIT_SLOW_TMR;
#endif

            }
        }
        /* Get the next BFD session entry */
        pBfdTempFsMIStdBfdSessEntry = pBfdFsMIStdBfdSessEntry;
        pBfdFsMIStdBfdSessEntry =
            RBTreeGetNext (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable,
                           (tRBElem *) pBfdTempFsMIStdBfdSessEntry, NULL);
    }

    BFD_LOG (u4ContextId, BFD_RM_RED_TRC_EXT, "BfdRedHwAudit");
    return;
}

/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
BfdExecuteCmdAndCalculateChkSum (VOID)
{
#ifdef RM_WANTED
    /*Execute CLI commands and calculate checksum */
    UINT4               u4ContextId = 0;
    UINT2               u2AppId = RM_BFD_APP_ID;
    UINT2               u2ChkSum = 0;

    BFD_UNLOCK;
    if (BfdGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == OSIX_FAILURE)
    {
        BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT,
                 "Checksum of calculation failed for BFD\n");
        BFD_LOCK;
        return;
    }

    if (BfdRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == OSIX_FAILURE)
    {
        BFD_LOG (u4ContextId, BFD_RM_RED_TRC_ENT,
                 "Sending checkum to RM failed\n");
        BFD_LOCK;
        return;
    }
    BFD_LOCK;
#endif
    return;
}

#endif
