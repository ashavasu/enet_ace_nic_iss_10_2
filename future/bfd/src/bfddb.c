/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddb.c,v 1.37 2017/06/08 11:40:29 siva Exp $
*
* Description: This file contains the routines for the protocol
*              Database Access for the module Bfd 
*********************************************************************/
#ifndef _BFDDB_C_
#define _BFDDB_C_

#include "bfdinc.h"
#include "bfdclig.h"
/****************************************************************************
 Function    :  BfdTestAllFsMIStdBfdGlobalConfigTable
 Input       :  The Indices
                pu4ErrorCode
                pBfdFsMIStdBfdGlobalConfigTableEntry
                pBfdIsSetFsMIStdBfdGlobalConfigTableEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdTestAllFsMIStdBfdGlobalConfigTable (UINT4 *pu4ErrorCode,
                                       tBfdFsMIStdBfdGlobalConfigTableEntry *
                                       pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                                       tBfdIsSetFsMIStdBfdGlobalConfigTableEntry
                                       *
                                       pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
{
    UINT4               u4ContextId = 0;

    u4ContextId = pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
        u4FsMIStdBfdContextId;

    /* To check whether the Module is started */
    if (BfdUtilFsMIStdBfdGlblCfgTable (pBfdSetFsMIStdBfdGlobalConfigTableEntry,
                                       pBfdIsSetFsMIStdBfdGlobalConfigTableEntry)
        != SNMP_SUCCESS)
    {

#ifdef CLI_WANTED
        CLI_SET_ERR (CLI_BFD_MODULE_NOT_STARTED);
#endif
        BFD_LOG (u4ContextId, BFD_TRC_SESS_TBLE_FETCH_FAIL);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return OSIX_FAILURE;

    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIStdBfdAdminStatus !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->
             MibObject.i4FsMIStdBfdAdminStatus != BFD_ENABLED) &&
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->
             MibObject.i4FsMIStdBfdAdminStatus != BFD_DISABLED))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->
        bFsMIStdBfdSessNotificationsEnable != OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIStdBfdSessNotificationsEnable != BFD_SNMP_TRUE) &&
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIStdBfdSessNotificationsEnable != BFD_SNMP_FALSE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdSystemControl !=
        OSIX_FALSE)
    {

        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdSystemControl != BFD_START) &&
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdSystemControl != BFD_SHUTDOWN))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTraceLevel !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdTraceLevel < BFD_NO_TRACE) ||
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdTraceLevel > BFD_ALL_TRACE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdTrapEnable !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdTrapEnable < BFD_NO_TRAP) ||
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdTrapEnable > BFD_ALL_TRAP))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSessOperMode !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdGblSessOperMode < BFD_OPER_MODE_ASYNC_W_ECHOFUNCTION) ||
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdGblSessOperMode > BFD_OPER_MODE_DEMAND_WO_ECHOFUNCTION))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* Demand mode and Echo function are not supported */

        if (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdGblSessOperMode != BFD_OPER_MODE_ASYNC_WO_ECHOFUNCTION)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_DEMAND_ECHO_NOT_SUPPORTED);
#endif
            BFD_LOG (u4ContextId, BFD_TRC_DEMAND_ECHO_NOT_SUPPORTED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblDetectMult !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             u4FsMIBfdGblDetectMult < BFD_MIN_DETECT_MULT) ||
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             u4FsMIBfdGblDetectMult > BFD_MAX_DETECT_MULT))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INVALID_MULT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdGblSlowTxIntvl !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             u4FsMIBfdGblSlowTxIntvl < BFD_MIN_SLOW_TX_INTVL))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INVALID_MULT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrGblStats !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdClrGblStats != BFD_SNMP_TRUE) &&
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdClrGblStats != BFD_SNMP_FALSE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pBfdIsSetFsMIStdBfdGlobalConfigTableEntry->bFsMIBfdClrAllSessStats !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdClrAllSessStats != BFD_SNMP_TRUE) &&
            (pBfdSetFsMIStdBfdGlobalConfigTableEntry->MibObject.
             i4FsMIBfdClrAllSessStats != BFD_SNMP_FALSE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  BfdTestAllFsMIStdBfdSessTable
 Input       :  The Indices
                pu4ErrorCode
                pBfdFsMIStdBfdSessEntry
                pBfdIsSetFsMIStdBfdSessEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
BfdTestAllFsMIStdBfdSessTable (UINT4 *pu4ErrorCode,
                               tBfdFsMIStdBfdSessEntry *
                               pBfdSetFsMIStdBfdSessEntry,
                               tBfdIsSetFsMIStdBfdSessEntry *
                               pBfdIsSetFsMIStdBfdSessEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    UINT4               u4ContextId = 0;
    INT4                i4MapType = 0;
    INT4                i4RetVal = 0;
#ifdef BSDCOMP_SLI_WANTED
    UINT4               u4TempDstAddrVa;
#ifdef IP6_WANTED
    UINT4               u4Index = 0;
    tIp6Addr            TempAddr;
#endif
#endif

    tBfdFsMIStdBfdSessEntry *pBfdFsMIStdBfdSessEntry = NULL;
    tBfdFsMIStdBfdGlobalConfigTableEntry *pBfdFsMIStdBfdGlobalConfigTableEntry =
        NULL;

    pBfdFsMIStdBfdGlobalConfigTableEntry =
        BfdUtilGetBfdGlobalConfigTable (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                        u4FsMIStdBfdContextId);
    if (pBfdFsMIStdBfdGlobalConfigTableEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
    else
    {
        if (pBfdFsMIStdBfdGlobalConfigTableEntry->MibObject.
            i4FsMIBfdSystemControl != BFD_START)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_MODULE_NOT_STARTED);
#endif
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }

    pBfdFsMIStdBfdSessEntry =
        BfdUtilGetBfdSessTable (pBfdSetFsMIStdBfdSessEntry->MibObject.
                                u4FsMIStdBfdContextId,
                                pBfdSetFsMIStdBfdSessEntry->MibObject.
                                u4FsMIStdBfdSessIndex);

    /* Only row Status creates a row 
     * entry in the table and read-create does not 
     * create a row entry in the table.*/

    if (pBfdFsMIStdBfdSessEntry == NULL)
    {

        if (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
            DESTROY)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_SESS_NOT_CREATED);
#endif
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_TRUE)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_SESS_NOT_CREATED);
#endif
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        else
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessIndex
                < SESS_INDEX_MIN_VAL)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_INVALID_SESS_INDEX);
#endif
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    else if ((pBfdFsMIStdBfdSessEntry->
              u1BfdSessParamAllowChg == BFD_SNMP_FALSE) &&
             (pBfdFsMIStdBfdSessEntry->
              MibObject.i4FsMIStdBfdSessRowStatus == ACTIVE))
    {
        BFD_LOG (pBfdFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId,
                 BFD_TRC_ALLOW_FLAG_CHECK);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    u4ContextId = pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdContextId;
    /* To test whether System is started */
    if (BfdUtilFsMIStdBfdSessTable (pBfdSetFsMIStdBfdSessEntry,
                                    pBfdIsSetFsMIStdBfdSessEntry)
        != SNMP_SUCCESS)
    {
#ifdef CLI_WANTED
        CLI_SET_ERR (CLI_BFD_MODULE_NOT_STARTED);
#endif
        BFD_LOG (u4ContextId, BFD_TRC_MOD_NOT_STARTED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessVersionNumber !=
        OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
             u4FsMIStdBfdSessVersionNumber > BFD_VER_MAX_VALUE))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (pBfdSetFsMIStdBfdSessEntry->MibObject.
            u4FsMIStdBfdSessVersionNumber != BFD_VER_ONE)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_VER_NOT_SUPPORTED);
#endif
            BFD_LOG (u4ContextId, BFD_TRC_VER_NOT_SUPPORTED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pBfdFsMIStdBfdSessEntry != NULL)
    {
        /*Function to check whether the given input is same as there in database */
        if ((i4RetVal = FsMIStdBfdSessTableFilterInputs
             (pBfdFsMIStdBfdSessEntry, pBfdSetFsMIStdBfdSessEntry,
              pBfdIsSetFsMIStdBfdSessEntry)) != OSIX_TRUE)
        {
            if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                 ACTIVE)
                && (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus != ACTIVE))
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_MODIFY_NOT_ALLOWED_WHEN_ACTIVE);
#endif
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (i4RetVal == -1)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_MODIFY_DYNAMIC_SESS_NOT_ALLOWED);
#endif
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                BFD_LOG (u4ContextId, BFD_TRC_MODIFY_DYNAMIC_SESS_NOT_ALLOWED);
                return OSIX_FAILURE;
            }

            return OSIX_SUCCESS;
        }

        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessType != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessType < BFD_SESS_TYPE_SINGLE_HOP) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessType > BFD_SESS_TYPE_MULTIPOINT_TAIL))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SESS_TYPE_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_SESS_TYPE_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDestinationUdpPort !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 u4FsMIStdBfdSessDestinationUdpPort != BFD_UDP_DEST_PORT) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 u4FsMIStdBfdSessDestinationUdpPort != BFD_UDP_DEST_PORT_MHOP)
                && (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    u4FsMIStdBfdSessDestinationUdpPort != 0))
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_INVALID_DEST_UDP_PORT);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_DESTUDP_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_DESTUDP_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSourceUdpPort !=
            OSIX_FALSE)
        {
            if (((pBfdSetFsMIStdBfdSessEntry->MibObject.
                  u4FsMIStdBfdSessSourceUdpPort < BFD_MIN_UDP_SRC_PORT) ||
                 (pBfdSetFsMIStdBfdSessEntry->MibObject.
                  u4FsMIStdBfdSessSourceUdpPort > BFD_MAX_UDP_SRC_PORT)) &&
                ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                  u4FsMIStdBfdSessSourceUdpPort != 0)))
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_INVALID_SRC_UDP_PORT);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SRCUDP_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_SRCUDP_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessEchoSourceUdpPort !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 u4FsMIStdBfdSessEchoSourceUdpPort != 0))
            {
                BFD_LOG (u4ContextId, BFD_TRC_ECHO_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAdminStatus !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAdminStatus != BFD_SESS_ADMIN_STOP) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAdminStatus != BFD_SESS_ADMIN_START))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessOperMode != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessOperMode < BFD_OPER_MODE_ASYNC_W_ECHOFUNCTION)
                || (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessOperMode >
                    BFD_OPER_MODE_DEMAND_WO_ECHOFUNCTION))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessOperMode != BFD_OPER_MODE_ASYNC_WO_ECHOFUNCTION)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_DEMAND_ECHO_NOT_SUPPORTED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_DEMAND_ECHO_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->
            bFsMIStdBfdSessDemandModeDesiredFlag != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDemandModeDesiredFlag != BFD_SNMP_FALSE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDemandModeDesiredFlag != BFD_SNMP_TRUE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;

            }
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDemandModeDesiredFlag != BFD_SNMP_FALSE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_DEMAND_MODE_NOT_SUPPORTED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_DEMAND_MODE_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->
            bFsMIStdBfdSessControlPlaneIndepFlag != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessControlPlaneIndepFlag != BFD_SNMP_TRUE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessControlPlaneIndepFlag != BFD_SNMP_FALSE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessMultipointFlag !=
            OSIX_FALSE)
        {

            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessMultipointFlag != BFD_SNMP_TRUE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessMultipointFlag != BFD_SNMP_FALSE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessMultipointFlag != BFD_SNMP_FALSE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_MULTIPOINT_FLAG_NOT_SUPPORTED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_MULTIPOINT_FLAG_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessInterface !=
            OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessInterface < 0)
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (BfdUtilValidateIfIndex ((UINT4) pBfdSetFsMIStdBfdSessEntry->
                                        MibObject.i4FsMIStdBfdSessInterface) !=
                OSIX_SUCCESS)
            {
                BFD_LOG (u4ContextId, BFD_TRC_SESS_INF_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
                CLI_SET_ERR (CLI_BFD_SESS_INTERFACE_CHANGE_NOT_ALLOWED);
                BFD_LOG (u4ContextId,
                         BFD_TRC_SESS_INTERFACE_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddrType !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessSrcAddrType != BFD_INET_ADDR_IPV4)
                &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessSrcAddrType != BFD_INET_ADDR_IPV6))
            {
                BFD_LOG (u4ContextId, BFD_TRC_ADDRESS_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            if (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessRowStatus == ACTIVE)
            {
                CLI_SET_ERR (CLI_BFD_SESS_SRCADDR_CHANGE_NOT_ALLOWED);
                BFD_LOG (u4ContextId, BFD_TRC_SESS_SRCADDR_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessSrcAddr != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessSrcAddrLen != BFD_IPV4_MAX_ADDR_LEN)
                &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessSrcAddrLen != BFD_IPV6_MAX_ADDR_LEN))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
                CLI_SET_ERR (CLI_BFD_SESS_SRCADDR_CHANGE_NOT_ALLOWED);
                BFD_LOG (u4ContextId, BFD_TRC_SESS_SRCADDR_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddrType !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDstAddrType != BFD_INET_ADDR_IPV4)
                &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDstAddrType != BFD_INET_ADDR_IPV6))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
                CLI_SET_ERR (CLI_BFD_SESS_DSTADDR_CHANGE_NOT_ALLOWED);
                BFD_LOG (u4ContextId, BFD_TRC_SESS_DSTADDR_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDstAddr != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDstAddrLen != BFD_IPV4_MAX_ADDR_LEN)
                &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessDstAddrLen != BFD_IPV6_MAX_ADDR_LEN))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
                CLI_SET_ERR (CLI_BFD_SESS_DSTADDR_CHANGE_NOT_ALLOWED);
                BFD_LOG (u4ContextId, BFD_TRC_SESS_DSTADDR_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
#ifdef BSDCOMP_SLI_WANTED
            if (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV4)
            {
                /* Check for self Ip address */
                MEMCPY (&u4TempDstAddrVa,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessDstAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        i4FsMIStdBfdSessDstAddrLen);
                if (NetIpv4IfIsOurAddress (OSIX_NTOHL (u4TempDstAddrVa)) == 0)
                {
                    return OSIX_FAILURE;
                }
            }
            else if (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessDstAddrType == BFD_INET_ADDR_IPV6)
            {
#ifdef IP6_WANTED
                MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
                MEMCPY (&TempAddr,
                        pBfdSetFsMIStdBfdSessEntry->MibObject.
                        au1FsMIStdBfdSessDstAddr, IPVX_IPV6_ADDR_LEN);
                /* Check for self Ip address */
                if (NetIpv6IsOurAddress (&TempAddr, &u4Index) ==
                    NETIPV6_SUCCESS)
                {
                    return OSIX_FAILURE;

                }
#endif
            }
#endif
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSM != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM !=
                 BFD_SNMP_TRUE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM !=
                 BFD_SNMP_FALSE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;

            }
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessGTSM !=
                BFD_SNMP_FALSE)
            {
                BFD_LOG (u4ContextId, BFD_TRC_GTSM_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessGTSMTTL != OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIStdBfdSessGTSMTTL !=
                0)
            {
                BFD_LOG (u4ContextId, BFD_TRC_GTSM_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessReqMinEchoRxInterval !=
            OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->
                MibObject.u4FsMIStdBfdSessReqMinEchoRxInterval != 0)
            {
                BFD_LOG (u4ContextId, BFD_TRC_ECHO_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessDetectMult !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 u4FsMIStdBfdSessDetectMult < BFD_MIN_DETECT_MULT) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 u4FsMIStdBfdSessDetectMult > BFD_MAX_DETECT_MULT))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INVALID_MULT);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthPresFlag !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthPresFlag != BFD_SNMP_TRUE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthPresFlag != BFD_SNMP_FALSE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /* Enabling/Disabling of authentication is not allowed when row is *
             * active*/

            if (pBfdFsMIStdBfdSessEntry != NULL)
            {
                if (pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus == ACTIVE)
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_AUTH_CHANGE_NOT_ALLOWED);
#endif
                    BFD_LOG (u4ContextId,
                             BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationType !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthenticationType < BFD_AUTH_NONE) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthenticationType >
                 BFD_AUTH_METICULOUS_KEYED_SHA1))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (!((pBfdSetFsMIStdBfdSessEntry->MibObject.
                   i4FsMIStdBfdSessAuthenticationType == BFD_AUTH_NONE) ||
                  (pBfdSetFsMIStdBfdSessEntry->MibObject.
                   i4FsMIStdBfdSessAuthenticationType ==
                   BFD_AUTH_SIMPLE_PASSWORD)))
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_AUTH_SIMPLE_PASSWD);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_AUTH_SIMPLE_PASSWD);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            /* Modification of authentication type is not allowed when row *
             * is active */
            if (pBfdFsMIStdBfdSessEntry != NULL)
            {
                if (pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessRowStatus == ACTIVE)
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_AUTH_PARAM_CHANGE_NOT_ALLOWED);
#endif
                    BFD_LOG (u4ContextId,
                             BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }

                if (pBfdFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthPresFlag != BFD_SNMP_TRUE)
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_AUTH_NOT_ENABLED);
#endif
                    BFD_LOG (u4ContextId, BFD_TRC_ENABLE_AUTH_TO_SET_PARAM);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKeyID !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthenticationKeyID < BFD_AUTH_KEY_ID_MIN_VAL)
                || (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyID >
                    BFD_AUTH_KEY_ID_MAX_VAL))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /*Authentication Key Id cannot be changed when row status
             *is active*/
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_AUTH_PARAM_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag
                != BFD_SNMP_TRUE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_AUTH_NOT_ENABLED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_ENABLE_AUTH_TO_SET_PARAM);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            /* Authentication key id should be set to != -1 only when
             * authentication present is False */
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthenticationKeyID !=
                 BFD_AUTH_KEY_ID_MIN_VAL) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE))
            {
#ifdef CLI_WANTED
                /* Log CLI and SNMP */
                CLI_SET_ERR (CLI_BFD_AUTH_PARAM_CHANGE_NOT_ALLOWED);
#endif
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;

            }
        }

        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessAuthenticationKey !=
            OSIX_FALSE)
        {
            /* Authentication key cannot be modified when the row is active */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_AUTH_PARAM_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_AUTH_PARAM_CHANGE_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            /* Authentication key can be set only if Authentication 
             * is enabled */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessAuthPresFlag
                != BFD_SNMP_TRUE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_AUTH_NOT_ENABLED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_ENABLE_AUTH_TO_SET_PARAM);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (pBfdFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessAuthenticationType == BFD_AUTH_SIMPLE_PASSWORD)
            {
                if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessAuthenticationKeyLen >
                    BFD_AUTH_MAX_SIM_PAS_LEN)
                {
                    BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                    return OSIX_FAILURE;
                }

            }
            else
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_INVALID_AUTH_KEY_LEN);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessStorType != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIStdBfdSessStorType < BFD_STOR_MIN_VAL)
                || (pBfdSetFsMIStdBfdSessEntry->MibObject.
                    i4FsMIStdBfdSessStorType > BFD_STOR_MAX_VAL))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRole != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessRole < BFD_SESS_ACTIVE) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessRole > BFD_SESS_PASSIVE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /* Session Role should not be modified when the row is active */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SESS_ROLE_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_SESS_MODE_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMode != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMode < BFD_SESS_MODE_CC) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMode > BFD_SESS_MODE_CCV))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /* Session Mode should not  be modified when the row is active */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SESS_MODE_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_SESS_MODE_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessRemoteDiscr != OSIX_FALSE)
        {
            /* Remote discriminator should not be modified when 
             * the row is active*/
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_NO_RDISCR_CHANGE);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_RDISCR_CAHNGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEXPValue != OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                u4FsMIBfdSessEXPValue > BFD_EXP_MAX_VAL)
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /*Modification of Exp value is not allowed*
             *when the row is active*/
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_EXP_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_EXP_VAL_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessTmrNegotiate !=
            OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessTmrNegotiate != BFD_SNMP_FALSE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessTmrNegotiate != BFD_SNMP_TRUE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /*Modification of Timer Negotiation is not allowed*
             *when the row is active*/
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_TMR_NEGOT_MODI_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_TMR_NEGOT);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessOffld != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessOffld != BFD_SNMP_FALSE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessOffld != BFD_SNMP_TRUE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /* Enabling/disabling offload  is not allowed *
             * when the row is active*/
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_OFFLOAD_ENA_DIS_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_OL_ENABLE_IN_SESS_ACTIVE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessEncapType != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessEncapType < BFD_ENCAP_TYPE_MPLS_IP) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessEncapType > BFD_ENCAP_TYPE_VCCV_NEGOTIATED))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /*Modification of Encapsulation type is not allowed
             * when the row is active*/

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_ENCAP_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_ENCAP_TYPE_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapType < BFD_PATH_TYPE_NONTE_IPV4) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapType >= BFD_PATH_TYPE_MAX_ENTRY))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /*Modification of Map type is not allowed *
             * when the row is active*/

            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_MAPTYPE_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_MAP_TYPE_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapType == BFD_PATH_TYPE_NONTE_IPV4) ||
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdSessMapType == BFD_PATH_TYPE_NONTE_IPV6))
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_LDP_NOT_SUPPORTED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_LDP_NOT_SUPPORTED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapPointer != OSIX_FALSE)
        {
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_MAPPOINTER_CHANGE_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_NO_MAPPOINTER_CHANGE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            /* If existing Session Map Type is 0 and the incoming request also 
             * does not have map-type set, then return failure 
             */
            if ((pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType == 0)
                && (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessMapType ==
                    OSIX_FALSE))
            {
                return OSIX_FAILURE;
            }

            /* To check whether Base OID exists */

            if (BfdUtilFsMIStdBfdSessBaseOidTest (pBfdIsSetFsMIStdBfdSessEntry,
                                                  pBfdSetFsMIStdBfdSessEntry)
                != SNMP_SUCCESS)
            {

                BFD_LOG (u4ContextId, BFD_TRC_BASE_OID_NOT_MATCH);
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_BASE_OID_DOES_NOT_MATCH);
#endif
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((i4RowStatusLogic == OSIX_FALSE) &&
                (i4RowCreateOption == OSIX_FALSE))
            {
                i4MapType =
                    pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;
            }
            else
            {
                i4MapType =
                    pBfdSetFsMIStdBfdSessEntry->MibObject.i4FsMIBfdSessMapType;
            }
            /* To check whether Path exits */
            if (BfdUtilFsMIStdBfdSessPathExist
                (pBfdSetFsMIStdBfdSessEntry, i4MapType) != SNMP_SUCCESS)
            {
                BFD_LOG (u4ContextId, BFD_TRC_NO_PATH);
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_PATH_NOT_EXIST);
#endif
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                /*return OSIX_FAILURE; */
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessCardNumber != OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessCardNumber
                > BFD_MAX_CARD_NUMBER)
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;

            }

            /* Modification of card number is not allowed when Row is Active */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_CARD_MODI_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_CARD_NUM_MODIFI_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdSessSlotNumber != OSIX_FALSE)
        {
            if (pBfdSetFsMIStdBfdSessEntry->MibObject.u4FsMIBfdSessSlotNumber
                > BFD_MAX_SLOT_NUMBER)
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;

            }

            /* Modification of slot number is not allowed when Row is Active */
            if (pBfdFsMIStdBfdSessEntry->MibObject.i4FsMIStdBfdSessRowStatus ==
                ACTIVE)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_SLOT_MODI_NOT_ALLOWED);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_SLOT_NUM_MODIFI_NOT_ALLOWED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIBfdClearStats != OSIX_FALSE)
        {
            if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdClearStats != BFD_SNMP_TRUE) &&
                (pBfdSetFsMIStdBfdSessEntry->MibObject.
                 i4FsMIBfdClearStats != BFD_SNMP_FALSE))
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            if (BfdUtilIsBfdSessTableExist (u4ContextId,
                                            pBfdSetFsMIStdBfdSessEntry->
                                            MibObject.u4FsMIStdBfdSessIndex) !=
                OSIX_TRUE)
            {
                BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pBfdIsSetFsMIStdBfdSessEntry->bFsMIStdBfdSessRowStatus != OSIX_FALSE)
    {
        if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus < ACTIVE) ||
            (pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus > DESTROY))
        {
            BFD_LOG (u4ContextId, BFD_TRC_INCONSIST_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if ((pBfdFsMIStdBfdSessEntry != NULL) &&
            (BFD_CHECK_SESS_DYNAMIC_CREATION (pBfdFsMIStdBfdSessEntry)) &&
            (pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus) == DESTROY)
        {
#ifdef CLI_WANTED
            CLI_SET_ERR (CLI_BFD_MODIFY_DYNAMIC_SESS_NOT_ALLOWED);
#endif
            BFD_LOG (u4ContextId, BFD_TRC_MODIFY_DYNAMIC_SESS_NOT_ALLOWED);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        if ((pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus == CREATE_AND_GO) ||
            (pBfdSetFsMIStdBfdSessEntry->MibObject.
             i4FsMIStdBfdSessRowStatus == ACTIVE))
        {
            if (pBfdFsMIStdBfdSessEntry == NULL)
            {
#ifdef CLI_WANTED
                CLI_SET_ERR (CLI_BFD_FILL_MAND_FIELD);
#endif
                BFD_LOG (u4ContextId, BFD_TRC_FILL_MAND_FIELD);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            /* Map pointer (row pointer) should be filled for MPLS LSPs */
            if (BFD_CHECK_PATH_TYPE_NOT_IP (pBfdFsMIStdBfdSessEntry))
            {
                if ((pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIBfdSessMapType == 0) ||
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIBfdSessMapPointerLen == 0))
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_FILL_MAND_FIELD);
#endif
                    BFD_LOG (u4ContextId, BFD_TRC_FILL_MAND_FIELD);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            /* Destination address, address type, length, interface should be filled for
             * IP path monitoring*/
            else
            {
                if ((pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessDstAddrType == 0) ||
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessDstAddrLen == 0) ||
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessInterface == 0) ||
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIBfdSessMapType == 0))

                {
                    CLI_SET_ERR (CLI_BFD_FILL_MAND_FIELD);
                    BFD_LOG (u4ContextId, BFD_TRC_FILL_MAND_FIELD);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if (((pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessSrcAddrType != 0) &&
                     (pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessSrcAddrType !=
                      pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessDstAddrType)) ||
                    ((pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessSrcAddrLen != 0) &&
                     (pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessSrcAddrLen !=
                      pBfdFsMIStdBfdSessEntry->MibObject.
                      i4FsMIStdBfdSessDstAddrLen)))
                {
                    CLI_SET_ERR (CLI_BFD_SRC_ADDR_DST_ADDR_MISMATCH);
                    BFD_LOG (u4ContextId, BFD_TRC_SRC_ADDR_DST_ADDR_MISMATCH);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }

            if (pBfdSetFsMIStdBfdSessEntry->MibObject.
                i4FsMIStdBfdSessRowStatus == ACTIVE)
            {
                if ((pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIBfdSessMode == BFD_SESS_MODE_CCV) &&
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIBfdSessMapType != BFD_PATH_TYPE_MEP))
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_CCV_NOT_SUPPORTED_FOR_MPLS);
#endif
                    BFD_LOG (u4ContextId, BFD_TRC_CCV_NOT_SUPPORTED_FOR_MPLS);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if ((pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessAuthPresFlag == BFD_SNMP_TRUE) &&
                    (pBfdFsMIStdBfdSessEntry->MibObject.
                     i4FsMIStdBfdSessAuthenticationType == BFD_AUTH_NONE))
                {
#ifdef CLI_WANTED
                    CLI_SET_ERR (CLI_BFD_AUTH_ENABLED);
#endif
                    BFD_LOG (u4ContextId, BFD_TRC_AUTH_TYPE_NOT_CONFIGURED);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

#endif /* _BFDDB_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  bfddb.c                        */
/*-----------------------------------------------------------------------*/
