/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdque.c,v 1.16 2014/11/03 12:18:43 siva Exp $
 *
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "bfdinc.h"

/****************************************************************************/
/* FUNCTION NAME    : BfdQueEnqMsg                                          */
/*                                                                          */
/* DESCRIPTION      : Function is used to post queue message to BFD task.   */
/*                                                                          */
/* INPUT            : pMsg - Pointer to the message to be posted.           */
/*                                                                          */
/* OUTPUT           : NONE                                                  */
/*                                                                          */
/* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                           */
/*                                                                          */
/****************************************************************************/
PUBLIC INT4
BfdQueEnqMsg (tBfdReqParams * pBfdQueMsg)
{
    /*Queue the message */
    if (OsixQueSend (gBfdGlobals.bfdQueId, (UINT1 *) &pBfdQueMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        /* Release the queue memory */
        BfdMainReleaseQMemory (pBfdQueMsg);
        return OSIX_FAILURE;
    }
    /*post event to BFD Task */
    if (OsixEvtSend (gBfdGlobals.bfdTaskId, BFD_QUEUE_EVENT) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : BfdQueProcessMsgs                                          *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
BfdQueProcessMsgs ()
{
    tBfdReqParams      *pBfdQueMsg = NULL;
    UINT1               u1TmrStart = OSIX_FALSE;

    while (OsixQueRecv (gBfdGlobals.bfdQueId,
                        (UINT1 *) &pBfdQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        switch (pBfdQueMsg->u4ReqType)
        {
            case BFD_PROCESS_BOOTSTRAP_INFO:
                if (BfdBtStrProcessLsppBtStrapInfo
                    (pBfdQueMsg->u4ContextId,
                     &pBfdQueMsg->unReqInfo.BfdLsppInfo) != OSIX_SUCCESS)
                {
                    if (pBfdQueMsg->unReqInfo.BfdLsppInfo.pBuf != NULL)
                    {
                        CRU_BUF_Release_MsgBufChain
                            (pBfdQueMsg->unReqInfo.BfdLsppInfo.pBuf, 0);
                    }
                }
                break;

            case BFD_CREATE_CONTEXT_MSG:
                if (BfdGblCreateContext (pBfdQueMsg->u4ContextId) ==
                    OSIX_FAILURE)
                {
                    BFD_ISS_LOG (pBfdQueMsg->u4ContextId,
                                 BFD_DEFAULT_CONTEXT_CREATION_FAILED);
                }

                break;

            case BFD_DELETE_CONTEXT_MSG:
                if (BfdGblDeleteContext (pBfdQueMsg->u4ContextId, OSIX_TRUE)
                    == OSIX_FAILURE)
                {
                    BFD_ISS_LOG (pBfdQueMsg->u4ContextId,
                                 BFD_DEFAULT_CONTEXT_DELETION_FAILED);
                }

                break;

            case BFD_RX_MSG:
                BfdQueHandleRxMsg (pBfdQueMsg);
                if (pBfdQueMsg->unReqInfo.BfdRecvBfdPacket.pBuf != NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pBfdQueMsg->unReqInfo.
                                                 BfdRecvBfdPacket.pBuf, FALSE);
                }
                break;

            case BFD_PATH_STATUS_CHG:
                BfdSessHandleSessPathStatusChg (pBfdQueMsg->u4ContextId,
                                                pBfdQueMsg->unReqInfo.
                                                BfdOamInInfo.u4SessIndex,
                                                (UINT1) pBfdQueMsg->unReqInfo.
                                                BfdOamInInfo.u4PathStatus);
                break;

            case BFD_OFFLD_CB_MSG:
                BfdOffHandleOffCbMsg (pBfdQueMsg->u4ContextId,
                                      &pBfdQueMsg->unReqInfo.BfdOffCbParams);
                if (pBfdQueMsg->unReqInfo.BfdOffCbParams.pBuf != NULL)
                {
                    /* Release the CRU buffer enqueued */
                    CRU_BUF_Release_MsgBufChain (pBfdQueMsg->
                                                 unReqInfo.BfdOffCbParams.
                                                 pBuf, FALSE);
                }
                break;

                /*
                   In case of queue overflow:
                   - re-set the global flag "QueueOverflow" as FALSE.
                   - start complete hardware audit.
                   - update "OverflowSessionId" with each session-id processed.
                   - on completion of hardware audit, re-set "OverflowSessionId"
                   as 0xFFFF.
                 */
            case BFD_QUE_OVERFLOW_MSG:
                gu1QueueOverflow = OSIX_FALSE;
                BfdRedHwAudit (u1TmrStart);
                gu2OverflowSessionId = BFD_DEF_SESS_ID;
                break;

            case BFD_RECEIVE_PKT_FROM_SOCK:
                BfdUdpProcessPktOnSocket (pBfdQueMsg);
                if (SelAddFd (pBfdQueMsg->unReqInfo.i4SockId,
                              BfdPktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    /* SelAddFd returned failure */
                    BFD_ISS_LOG (pBfdQueMsg->u4ContextId,
                                 BFD_TRC_SELECT_ADD_FD_FAILURE);
                }
                break;

            case BFD_V6_RECEIVE_PKT_FROM_SOCK:
#ifdef IP6_WANTED
                BfdUdpv6ProcessPktOnSocket (pBfdQueMsg);
                if (SelAddFd (pBfdQueMsg->unReqInfo.i4SockId,
                              Bfdv6PktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    /* SelAddFd returned failure */
                    BFD_ISS_LOG (pBfdQueMsg->u4ContextId,
                                 BFD_TRC_SELECT_ADD_FD_FAILURE);
                }
#endif
                break;

            case BFD_SESS_PARAM_CHG_MSG:
                BfdSessProcessAlwParamChgMsg (pBfdQueMsg->u4ContextId,
                                              &pBfdQueMsg->unReqInfo.
                                              BfdAllowParamChgMsg);
                break;

#ifdef L2RED_WANTED
            case BFD_RM_MSG_RECVD:

                BfdRedHandleRmEvents (&pBfdQueMsg->unReqInfo.BfdRmMsg);

                break;
#endif

            case BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING:
                BfdSessHandleClientRqst (pBfdQueMsg->u4ContextId,
                                         &pBfdQueMsg->BfdClntInfo,
                                         BFD_CREATE_DYNAMIC_SESSION);
                break;

            case BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING:
                BfdSessHandleClientRqst (pBfdQueMsg->u4ContextId,
                                         &pBfdQueMsg->BfdClntInfo,
                                         BFD_REMOVE_DYNAMIC_SESSION);
                break;

            case BFD_CLIENT_DEREGISTER_ALL_FROM_IP_PATH_MONITORING:
                BfdSessHandleDeRegisterAll (pBfdQueMsg->u4ContextId,
                                            &pBfdQueMsg->BfdClntInfo);
                break;
           
		   case BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING:
                BfdSessHandleClientRqst (pBfdQueMsg->u4ContextId,
                                         &pBfdQueMsg->BfdClntInfo,
                                         BFD_DISABLE_DYNAMIC_SESSION);
                break;
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                BfdMbsmHandleLCEvent (pBfdQueMsg->unReqInfo.pMbsmProtoMsg,
                                      pBfdQueMsg->u4ReqType);
                MEM_FREE (pBfdQueMsg->unReqInfo.pMbsmProtoMsg);
                break;
            case BFD_MBSM_SLOT_CARD_PARAMS:
                /* This API is called by external module to inform
                 * card ID and Slot ID to the BFD.
                 *
                 * Inputs:
                 * u4ContextId-Context id
                 * BfdMbsmParams - gives session id, context id , slot num and 
                 *                 card num
                 * */
                BfdMbsmHandleCardInfoUpdate (pBfdQueMsg->u4ContextId,
                                             &pBfdQueMsg->unReqInfo.
                                             BfdMbsmParams);
                break;
#endif /* MBSM_WANTED */

            default:
                break;
        }
        /* Release the buffer to pool */
        if ((MemReleaseMemBlock (BFD_QUEMSG_POOLID,
                                 (UINT1 *) pBfdQueMsg)) == MEM_FAILURE)
        {
            /*BFD_ISS_LOG (tBfdReqParams->u4ContextId, BFD_FREE_MEMORY_FAIL,
               "FUNC:BfdQueProcessMsgs"); */
            return;
        }
    }
}

/****************************************************************************
*                                                                           *
* Function     : BfdQueHandleRxMsg                                          *
*                                                                           *
* Description  : Process the receive packet msg and give it to the rx       *
*                module for packet processing                               *
*                                                                           *
* Input        : pBfdQueMsg - Pointer to the received packet buffer and     *
*                             interface index                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
BfdQueHandleRxMsg (tBfdReqParams * pBfdQueMsg)
{
    UINT1              *pu1Buf = NULL;
    UINT4               u4PktLen = 0;

    if (pBfdQueMsg->unReqInfo.BfdRecvBfdPacket.pBuf == NULL)
    {
        return;
    }
    /* Allocate memory for packet linear buffer */
    pu1Buf = (UINT1 *) MemAllocMemBlk (BFD_PKTBUF_POOLID);
    if (pu1Buf == NULL)
    {
        BfdUtilUpdateMemAlocFail (pBfdQueMsg->u4ContextId);
        /* BFD_LOG (u4ContextId,); */
        return;
    }

    CRU_BUF_Copy_FromBufChain (pBfdQueMsg->unReqInfo.BfdRecvBfdPacket.pBuf,
                               pu1Buf, 0, BFD_MAX_PKT_LEN);

    u4PktLen =
        CRU_BUF_Get_ChainValidByteCount (pBfdQueMsg->unReqInfo.BfdRecvBfdPacket.
                                         pBuf);

    BfdRxReceivePacket (pBfdQueMsg->u4ContextId,
                        pu1Buf, u4PktLen,
                        pBfdQueMsg->unReqInfo.BfdRecvBfdPacket.u4IfIndex,
                        NULL, NULL, BFD_PACKET_OVER_MPLS_RTR);

    MemReleaseMemBlock (BFD_PKTBUF_POOLID, (UINT1 *) pu1Buf);
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  bfdque.c                      */
/*-----------------------------------------------------------------------*/
