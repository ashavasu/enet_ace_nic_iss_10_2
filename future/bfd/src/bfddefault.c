/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bfddefault.c,v 1.3 2012/01/24 13:28:29 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Bfd 
*********************************************************************/

#include "bfdinc.h"

/****************************************************************************
* Function    : BfdInitializeFsMIStdBfdSessTable
* Input       : pBfdFsMIStdBfdSessTable
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
BfdInitializeFsMIStdBfdSessTable (tBfdFsMIStdBfdSessEntry *
                                  pBfdFsMIStdBfdSessEntry)
{
    if (pBfdFsMIStdBfdSessEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((BfdInitializeMibFsMIStdBfdSessTable (pBfdFsMIStdBfdSessEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
