/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: bfdstub.c,v 1.3 2015/04/08 06:06:53 siva Exp $
 *
 * Description: This file contains BFD High Availability related 
 *              Core functionalities
 *******************************************************************/
#ifndef __BFDSTUB_C
#define __BFDSTUB_C

#include "bfdinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize ring dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDescrTblInit                               */
/*                                                                           */
/*    Description         : This function will initialize Bfd descriptor     */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/* FUNCTION NAME    : BfdRedInitGlobalInfo                                  */
/*                                                                           */
/* DESCRIPTION      : Initializes redundancy global variables. This function */
/*                    will register BFD with RM.                            */
/*                                                                           */
/* INPUT            : None                                                   */
/*                                                                           */
/* OUTPUT           : None                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
BfdRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}
#ifdef L2RED_WANTED
/************************************************************************/
/* Function Name      : BfdRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to BFD        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
BfdRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
}
#endif
/************************************************************************/
/* Function Name      : BfdRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the Bfd module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
BfdRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}
#ifdef L2RED_WANTED
/************************************************************************
 * Function Name      : BfdRedHandleRmEvents
 *
 * Description        : This function is invoked by the BFD module to
 *                      process all the events and messages posted by
 *                      the RM module
 *
 * Input(s)           : pMsg -- Pointer to the BFD Q Msg
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/
PUBLIC VOID
BfdRedHandleRmEvents (tBfdRmMsg * pBfdRmMsg)
{
    UNUSED_PARAM (pBfdRmMsg);

}
#endif
/************************************************************************
 * Function Name      : BfdRedHandleGoActive
 *
 * Description        : This function is invoked by the BFD upon
 *                      receiving the GO_ACTIVE indication from RM
 *                      module. And this function responds to RM with an
 *                      acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
BfdRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedSendBulkUpdMsg (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  BfdRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedAddAllNodeInDbTbl (VOID)
{
    return;
}

/*****************************************************************************/
/*    Function Name       : BfdRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate BFD dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedSyncDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pBfdDataDesc - This is BFD sepcific data desri-  */
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pBfdSessDbNode -This is db node defined in the BFD*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDbUtilAddTblNode (tDbTblDescriptor * pBfdDataDesc,
                        tDbTblNode * pBfdSessDbNode)
{
    UNUSED_PARAM (pBfdDataDesc);
    UNUSED_PARAM (pBfdSessDbNode);
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends                             */
/*                                                                      */
/* Input(s)           : pBfdBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
BfdRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC UINT4
BfdRedSendMsgToRm (tRmMsg * pMsg, UINT4 u4Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u4Length);

    return OSIX_SUCCESS;
}
#ifdef L2RED_WANTED
/************************************************************************
 * Function Name      : BfdRedHandleGoStandby
 *
 * Description        : This function is invoked by the BFD upon
 *                      receiving the GO_STANBY indication from RM
 *                      module. And this function responds to RM module
 *                      with an acknowledgement.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : None
 ************************************************************************/

PUBLIC VOID
BfdRedHandleGoStandby (tBfdRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}
#endif
/************************************************************************/
/* Function Name      : BfdRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);

    return;
}

/************************************************************************/
/* Function Name      : BfdRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
BfdRedProcessDynamicInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : BfdRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : BfdRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
BfdRedSendBulkReqMsg (VOID)
{
    return;
}

VOID
BfdRedHwAudit (UINT1 u1TmrStart)
{
    UNUSED_PARAM (u1TmrStart);
    return;
}

/************************************************************************
 * Function Name      : BfdRedDeInitGlobalInfo
 *
 * Description        : This function is invoked by the BFD module
 *                      during module shutdown and this function
 *                      deinitializes the redundancy global variables
 *                      and de-register BFD with RM.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

INT4
BfdRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : BfdRedDbNodeInit                                 */
/*                                                                           */
/*    Description         : This function will initialize Bfd Oper db node.  */
/*                                                                           */
/*    Input(s)            : pBfdDbTblNode - pointer to Bfd Db Entry.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
BfdRedDbNodeInit (tDbTblNode * pBfdDbTblNode)
{
    UNUSED_PARAM (pBfdDbTblNode);
    return;
}

#endif
