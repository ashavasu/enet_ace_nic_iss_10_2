/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: bfdport.c,v 1.24 2015/04/17 02:35:13 siva Exp $
 *
 * Description : This file contains the utility 
 *               functions of the BFD module
 *****************************************************************************/
#ifndef _BFDPORT_C_
#define _BFDPORT_C_

#include "bfdinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : BfdPortHandleExtInteraction                                */
/*                                                                           */
/* Description  : This function is the common exit  point for interacting    */
/*                with all external module.                                  */
/*                                                                           */
/* Input        :                                                            */
/*                pBfdExtReqInParams - Pointer to the Request params.        */
/*                                                                           */
/* Output       : pBfdExtReqOutParams - Pointer to the response params       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
BfdPortHandleExtInteraction (tBfdExtInParams * pBfdExtReqInParams,
                             tBfdExtOutParams * pBfdExtReqOutParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4Port = 0;
    UINT1               u1BfdClient = 0;

#if defined (IP6_WANTED) && (!defined (LNXIP6_WANTED))
    tIp6Addr           *pIp6SrcAddr = NULL;
#endif


    /* External requests handling */
    switch (pBfdExtReqInParams->eExtReqType)
    {
        case BFD_REQ_LSPP_BOOTSTRAP_REPLY:
            /* This function is called by the BFD module to send 
             * LSP Ping reply.
             * Input :
             *      u4ContextId -Virtual Context of the LSP Ping.
             *      u1MsgType   - Action that LSP Ping module should perform
             *      LsppInfo - structure variable to tLsppBfdInfo
             * Output:
             *      pErrCode - Pointer to an enum indicating failure
             **/
            pBfdExtReqInParams->LsppReqParams.u4ContextId =
                pBfdExtReqInParams->u4ContextId;
            pBfdExtReqInParams->LsppReqParams.u1MsgType =
                LSPP_BFD_BOOTSTRAP_ECHO_REPLY;

#ifdef LSPP_WANTED
            i4RetVal = LsppApiHandleExtRequest (&pBfdExtReqInParams->
                                                LsppReqParams,
                                                &pBfdExtReqOutParams->
                                                BfdLsppRespParams);
#else
            i4RetVal = OSIX_FAILURE;
#endif

            break;

        case BFD_REQ_LSPP_BOOTSTRAP_MSG:
            /* This function is called by the BFD module to trigger 
             * LSP Ping for BFD Bootstarp.
             * Input:
             *      u4ContextId - Virtual Context of the LSP Ping.
             *      u1MsgType   - Action that LSP Ping module should perform
             *      LsppExtTrigInfo - structure variable to tLsppExtTrigInfo
             * Output:
             *      pErrCode - Pointer to an enum indicating failure
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE 
             * */
            pBfdExtReqInParams->LsppReqParams.u4ContextId =
                pBfdExtReqInParams->u4ContextId;
            pBfdExtReqInParams->LsppReqParams.u1MsgType =
                LSPP_BFD_BOOTSTRAP_ECHO_REQ;
#ifdef LSPP_WANTED
            i4RetVal = LsppApiHandleExtRequest (&pBfdExtReqInParams->
                                                LsppReqParams,
                                                &pBfdExtReqOutParams->
                                                BfdLsppRespParams);
#else
            i4RetVal = OSIX_FAILURE;
#endif
            break;

        case BFD_REQ_VCM_GET_CONTEXT_NAME:
            /* This function is called by BFD module to fetch the context
             * name for the given context id.
             * Input:
             *      u4ContextId- Context Id
             * Output:
             *      pu1Alias - Alias Name
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = VcmGetAliasName (pBfdExtReqInParams->u4ContextId,
                                        pBfdExtReqOutParams->au1ContextName);
            if (i4RetVal == VCM_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                i4RetVal = OSIX_SUCCESS;
            }
            break;

        case BFD_REQ_VCM_IS_CONTEXT_VALID:
            /* This function is called by BFD module to check whether the
             * given Context name is present or not.BFD calls this function
             * to validate the string input from CLI for switch alias name.
             * Input:
             *      pu1Alias - Alias name
             * Output:
             *      pu4VcNum - Context Identifier.
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = VcmIsVrfExist (pBfdExtReqInParams->
                                      unReqParams.au1Alias,
                                      &pBfdExtReqOutParams->u4VcNum);
            if (i4RetVal == VCM_TRUE)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case BFD_REQ_MPLS_OAM_HANDLE_PATH_STATUS_CHANGE:
            /* This function is called by the BFD module to update the 
             * OAM module about the BFD session status change.
             * Input:
             *       eExtReqType - Request Type.
             *       pInMplsApiInfo - Pointer to the input structure
             *                        tInMplsApiInfo
             * Returns:
             *       OSIX_SUCCESS or OSIX_FAILURE.
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_OAM_UPDATE_SESSION_PARAMS:
            /* This function is called by the BFD module to update OAM
             * module with session index.This is invoked when the session
             * is active and in the BFD packet transmission path.
             * Input:
             *        eExtReqType - Request Type.
             *        pInMplsApiInfo - Pointer to the input structure
             * Returns:
             *       OSIX_SUCCESS or OSIX_FAILURE
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_PACKET_HANDLE_FROM_APP:
            /* This function is called by the BFD module to send BFD 
             * control packet to MPLS-RTR module for forwarding.
             * Input:
             *        eExtReqType - Request Type.
             *        pInMplsApiInfo - Pointer to the input structure
             *  Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
            if (OSIX_FAILURE == i4RetVal)
            {
                CRU_BUF_Release_MsgBufChain (pBfdExtReqInParams->
                                             InApiInfo.InPktInfo.pBuf, FALSE);
            }
#else
            CRU_BUF_Release_MsgBufChain (pBfdExtReqInParams->
                                         InApiInfo.InPktInfo.pBuf, FALSE);
#endif
            break;

        case BFD_REQ_MPLS_OAM_GET_MEG_INFO:
            /* This function is called by the BFD module to get the 
             * MEG, MEP information
             * Input:
             *       eExtReqType - Request Type.
             *       pInMplsApiInfo - Pointer to the input structure
             * Output:
             *       pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_OAM_GET_MEG_INFO,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_TUNNEL_INFO:
            /* This function is called by the BFD module to get the
             * tunnel information based on path identifiers.
             * Input:
             *       eExtReqType - Request Type.
             *       pInMplsApiInfo - Pointer to the input structure
             * Output:
             *        pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_TUNNEL_INFO,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_PW_INFO:
            /*This function is called by the BFD module to get the
             * PW information based on the PW path identifiers.
             * Input:
             *       eExtReqType - Request Type
             *       pInMplsApiInfo - Pointer to the input structure
             * Output:
             *        pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             * */
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_PW_INFO,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_LSP_INFO:
            /* This function is called by the BFD module to get the
             * LSP information based on the LSP path identifiers.
             * Input:
             *       eExtReqType - Request Type
             *       pInMplsApiInfo - Pointer to the input structure
             * Output:
             *        pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_LSP_INFO,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_NODE_ID:
            /* This function is used by the BFD module to get the Router
             * Id.
             * Input:
             *        eExtReqType - Request Type
             *        pInMplsApiInfo - Pointer to the input structure
             * Output:
             *         pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_NODE_ID,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_PATH_INFO_FROM_INLBL_INFO:
            /*This function is used by the BFD module to get path 
             * information and session index associated with the path 
             * based on the path identifiers.
             * Input:
             *         eExtReqType - Request Type
             *         pInMplsApiInfo - Pointer to the input structure
             * Output:
             *         pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_REV_PATH_INFO_FROM_FWD_PATH_INFO:
            /*This function is used by the BFD module to get the reverse
             * path information.
             * Input:
             *          eExtReqType - Request Type
             *          pInMplsApiInfo - Pointer to the input structure
             * Output:
             *          pOutMplsApiInfo - Pointer to the output API info.
             * Returns:
             *          OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef MPLS_WANTED

            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_MPLS_GET_SERVICE_POINTER_OID:
            /*This function is used by the BFD module to get the
             * first serviceable OID from the path information.
             * Input:
             *          eExtReqType - Request Type
             *          pInMplsApiInfo - Pointer to the input structure
             * Output:
             *          pOutMplsApiInfo - Pointer to the output API info.
             *  Returns:
             *          OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef MPLS_WANTED
            i4RetVal =
                MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                              &pBfdExtReqInParams->InApiInfo,
                                              &pBfdExtReqOutParams->
                                              MplsApiOutInfo);
#endif
            break;

        case BFD_REQ_GET_IF_MAC_ADDR:
            /* This function is called by the BFD module to get the 
             * MAC address of the interface.
             * Input:
             *         u4IfIndex - Interface Index
             * Output:
             *         pu1HwAddr - Pointer to the Mac address.
             **/
            i4RetVal = CfaGetIfHwAddr (pBfdExtReqInParams->
                                       unReqParams.u4IfIndex,
                                       pBfdExtReqOutParams->au1HwAddr);
            if (i4RetVal == CFA_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                i4RetVal = OSIX_SUCCESS;
            }
            break;

        case BFD_REQ_IP_PORT_FROM_IF_INDEX:
            /* This function is called by the BFD module to get the IP 
             * port number corresponidng to IfIndex
             * Input:
             *        u4IfIndex - Interafce index.
             * Output:
             *        pu4Port - Ip port number
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = NetIpv4GetPortFromIfIndex (pBfdExtReqInParams->
                                                  unReqParams.u4IfIndex,
                                                  &pBfdExtReqOutParams->u4Port);
            if (i4RetVal == NETIPV4_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case BFD_REQ_ARP_RESOLVE:
            /* This function is called by the BFD module to check 
             * whether the entry is available in the Arp cache.
             * Input:
             *        u4IpAddr- Ip address to be resolved.
             * Output:
             *        p1HwAddr - Hardware address
             *        pu1EncapType - Encap type for that entry.
             * Return:
             *        OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = ArpResolveInCxt (pBfdExtReqInParams->
                                   unReqParams.u4IpAddr,
                                   (INT1 *) pBfdExtReqOutParams->au1HwAddr,
                                   &pBfdExtReqOutParams->
                                   ArpOutParams.u1EncapType,
                                   pBfdExtReqInParams->u4ContextId);

            if (i4RetVal == ARP_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case BFD_REQ_ENQUEUE_ARP_REQUEST:
            /* This function is called by the BFD module to enqueue the 
             * packets to ARP for queuing until address is resolved.
             * Input:
             *          pRecvdQMsg - Pointer to the tArpQMsg structure
             * Returns: OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = ArpEnqueuePkt (&pBfdExtReqInParams->RecvdQMsg);
            if (i4RetVal == ARP_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case BFD_REQ_FM_SEND_TRAP:
            /* This function is called by the BFD module
             * to notify about the protocol faults.
             * The fault message is Logged through a TRAP 
             * message is sent to the Network Manager.
             *
             * Input:
             *         FmFaultMsg - Pointer to the Fault message structure
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = FmApiNotifyFaults (&pBfdExtReqInParams->
                                          unReqParams.FmFaultMsg);
            break;

        case BFD_REQ_FM_SYSLOG_MSG:
            /* This function is called by the BFD module
             * to log about the protocol faults.
             * The fault message is Logged through SYSLOG.
             *
             * Input:
             *         FmFaultMsg - Pointer to the Fault message structure
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG ((pBfdExtReqInParams->unReqParams.BfdSyslogMsg.
                          u4SyslogLevel,
                          gBfdGlobals.u4SysLogId, " %s ",
                          pBfdExtReqInParams->unReqParams.
                          BfdSyslogMsg.pSyslogMsg));

            if (pBfdExtReqInParams->unReqParams.BfdSyslogMsg.u4SyslogLevel 
                           == SYSLOG_CRITICAL_LEVEL)
            {
                UtlTrcLog (1, 1, "BFD", (CHR1 *) pBfdExtReqInParams->unReqParams.
                           BfdSyslogMsg.pSyslogMsg);
            }
#endif
            i4RetVal = OSIX_SUCCESS;
            break;

        case BFD_REQ_VLAN_FROM_IF:
            /* This function is called by the BFD module
             * to get vlan from the interface
             *
             * Input:
             *         u4IfIndex - Interafce index.
             * output 
             *         u2Vlan - VlanId of the interface
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
            if (CfaGetVlanId (pBfdExtReqInParams->u4IfIdx,
                              &(pBfdExtReqOutParams->u2Vlan)) != CFA_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }
            break;
        case BFD_REQ_EGR_IF_INDEX_NH_FROM_DEST_IP:
            /* This function is called by the BFD module
             * to get vlan from the interface
             *
             * Input:
             *         RtQuery - RT query structure.
             * output 
             *         NetIpRtInfo - Net Ip info structure
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = NetIpv4GetRoute (&pBfdExtReqInParams->BfdRtInfoQueryMsg,
                                        &(pBfdExtReqOutParams->NetIpRtInfo));
            if (i4RetVal == NETIPV4_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }
            break;
        case BFD_REQ_CFA_IFINDEX_FROM_PORT:
            /* This function is called by the BFD module
             * to get vlan from the interface
             * 
             * Input:
             *         u4IfIdx - Port IfIndex .
             * output
             *         u4Port - Cfa IfIndex
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/

            i4RetVal =
                NetIpv4GetCfaIfIndexFromPort (pBfdExtReqInParams->u4IfIdx,
                                              &(pBfdExtReqOutParams->u4Port));
            if (i4RetVal == NETIPV4_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }
            break;

        case BFD_REQ_VCM_GET_SYS_MODE:
            /* This function is called by the BFD module
             * to get the System mode from VCM
             *
             * Input:
             *         u2ProtocolId - BFD protocol ID.
             * output 
             *         None
             * Returns:
             *        BFD_SI_MODE or BFD_MI_MODE  
             **/
            if (VcmGetSystemMode (BFD_PROTOCOL_ID) == VCM_SI_MODE)
            {
                return BFD_SI_MODE;
            }
            else
            {
                return BFD_MI_MODE;
            }
            break;

        case BFD_REQ_VCM_GET_SYS_MODE_EXT:
            /* This function is called by the BFD module
             * to get the System mode from VCM
             *
             * Input:
             *         u2ProtocolId - BFD protocol ID.
             * output 
             *         None
             * Returns:
             *        BFD_SI_MODE or BFD_MI_MODE  
             **/
            if (VcmGetSystemModeExt (BFD_PROTOCOL_ID) == VCM_SI_MODE)
            {
                return BFD_SI_MODE;
            }
            else
            {
                return BFD_MI_MODE;
            }
            break;

        case BFD_REQ_GET_L3VLAN_IF_FROM_TNL_IF:
            /* This function is called by the BFD module to get the 
             * L3IPVLAN on which MPLS tunnel interface is stacked over
             * Input:
             *        u2TnlIfIndex - MPLS Tunnel Interface
             *        bLockReq - Lock Requested .
             * Output:
             *        pu4L3VlanIf - L3IPVLAN interface
             * Returns:
             *        CFA_SUCCESS or CFA_FAILURE
             **/
#ifdef MPLS_WANTED
            i4RetVal =
                CfaUtilGetIfIndexFromMplsTnlIf (pBfdExtReqInParams->
                                                unReqParams.CfaReqParams.
                                                u2TnlIfIndex,
                                                &pBfdExtReqOutParams->
                                                u4L3VlanIf,
                                                pBfdExtReqInParams->
                                                unReqParams.CfaReqParams.
                                                bLockReq);
#else
            i4RetVal = CFA_SUCCESS;
#endif
            if (i4RetVal == CFA_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
            }
            else
            {
                i4RetVal = OSIX_SUCCESS;
            }
            break;
        case BFD_REQ_REG_WITH_VCM:
            /* This function is called by the BFD module to register
             * with VCM module for context create/delete
             * Input:
             *        VcmRegInfo - VCM reg information
             * Output:
             *        NONE
             * Returns:
             *        VCM_SUCCESS or VCM_FAILURE
             **/
            i4RetVal =
                VcmRegisterHLProtocol (&pBfdExtReqInParams->unReqParams.
                                       VcmRegInfo);
            if (i4RetVal == VCM_FAILURE)
            {
                i4RetVal = VCM_FAILURE;
            }
            else
            {
                i4RetVal = OSIX_SUCCESS;
            }
            break;

#ifdef MBSM_WANTED
        case BFD_REQ_MBSM_PARAMS:
            /* This function is called by BFD Module to get slot 
             *  and card information from external module. This 
             *  request has to be ported in case Card and slot 
             *  number is provided by a external module instead 
             *  of configuration. Value of BFD_INVALID_CARD_SLOT_NUM
             *  should be returned
             *  to be considered as invalid slot and card num and
             *  the values are not used to update the Session 
             *  information.
             *
             * Input:
             *         u4ContextId
             *         u4SessionId
             * output 
             *         i4CardNum
             *         i4SlotId
             * Returns:
             *         OSIX_SUCCESS or OSIX_FAILURE
             **/
            /* Returing invalid Slot ID and Card Num. Should be 
             * ported with appropriate function if needed*/
            pBfdExtReqOutParams->BfdMbsmParams.u4CardNum =
                BFD_INVALID_CARD_SLOT_NUM;
            pBfdExtReqOutParams->BfdMbsmParams.u4SlotId =
                BFD_INVALID_CARD_SLOT_NUM;
            i4RetVal = OSIX_SUCCESS;
            break;
#endif

#ifdef CFA_WANTED
        case BFD_REQ_CFA_IFINDEX_FROM_IFNAME_AND_IFNUM:
            /* This function is called by the BFD module to get the
             * Interface Index from Interface name and SlotAndPort Number
             * Input:
             *        IfName      - Interafce name.
             *        SlotAndPort - Slot Number and Port Number
             * Output:
             *        pu4IfIndex  - Interface Index
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = CfaCliGetIfIndexInCxt (pBfdExtReqInParams->CxtName,
                                              (INT1 *) pBfdExtReqInParams->
                                              IpIfName,
                                              pBfdExtReqInParams->IpSlotAndPort,
                                              &pBfdExtReqOutParams->u4Port);
            if (i4RetVal == CLI_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;
#endif
        case BFD_REQ_NETIP_IF_INFO:
            /* This function is called by the BFD module to get the
             * Interface infor from the interface index
             * Input:
             *        u4IfIndex    - Interface Index
             * Output:
             *        Ifinfo       - Interface info
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE.
             * */
            if (pBfdExtReqInParams->IpAddrType == BFD_INET_ADDR_IPV4)
            {
                if (NetIpv4GetPortFromIfIndex
                    (pBfdExtReqInParams->u4IfIdx, &u4Port) != NETIPV4_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                i4RetVal =
                    NetIpv4GetIfInfo (u4Port,
                                      &pBfdExtReqOutParams->NetIpIfInfo);

                if (i4RetVal == NETIPV4_SUCCESS)
                {
                    return OSIX_SUCCESS;
                }
                else
                {
                    return OSIX_FAILURE;
                }
            }
#ifdef IP6_WANTED
            else if (pBfdExtReqInParams->IpAddrType == BFD_INET_ADDR_IPV6)
            {
                 if (!(IS_ADDR_LLOCAL (pBfdExtReqInParams->NbrPathInfo.NbrAddr)))
                 {

                i4RetVal = BfdUtilGetSrcv6GlobalAddr
                    (pBfdExtReqInParams->u4IfIdx,
                     (tIp6Addr *) & pBfdExtReqInParams->
                     NbrPathInfo.NbrAddr,
                     (tIp6Addr *) & pBfdExtReqOutParams->NetIpv6Addr);
                 }
#ifndef LNXIP6_WANTED
                 else
                 {
                      pIp6SrcAddr = Ip6GetLlocalAddr(pBfdExtReqInParams->u4IfIdx);
                      if(pIp6SrcAddr == NULL)
                      {
                          i4RetVal = OSIX_FAILURE;
                      }
                      else
                      {
                          Ip6AddrCopy (&(pBfdExtReqOutParams->NetIpv6Addr),pIp6SrcAddr);
                          i4RetVal = OSIX_SUCCESS;
                      }
                 }
#endif
                 return i4RetVal;
            }
#endif

            break;

#ifdef CFA_WANTED
        case BFD_REQ_CFA_VALIDATE_IF_INDEX:
            /* This function is called by the BFD module to get the
             * Interface Index from Interface name and SlotAndPort Number
             * Input:
             *        u4IfIndexName      - Interface index
             * Output:
             *        None
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = CfaValidateIfIndex (pBfdExtReqInParams->u4IfIdx);

            if (i4RetVal == CFA_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;
#endif
        case BFD_REQ_CLIENT_HANDLE_IP_PATH_STATUS_CHANGE:
            /* This function is called by the BFD module to inform the
             * BFD clients about the path status change
             * Input:
             *        BfdClientNbrIpPathInfo - Neighbor Ip path info
             * Output:
             *        None
             * Returns:
             *        OSIX_SUCCESS or OSIX_FAILURE.
             * */
            for (u1BfdClient = BFD_CLIENT_ID_IP; u1BfdClient < BFD_MAX_CLIENTS;
                 u1BfdClient++)
            {
                if (pBfdExtReqInParams->
                    NotifyPathStatusChange[u1BfdClient - 1] != NULL)
                {
                    i4RetVal =
                        (*pBfdExtReqInParams->
                         NotifyPathStatusChange[u1BfdClient -
                                                1]) (pBfdExtReqInParams->
                                                     u4ContextId,
                                                     &pBfdExtReqInParams->
                                                     NbrPathInfo);
                }
            }
            break;

        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* End of switch */
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : BfdPortGetSystemMode                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
BfdPortGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : BfdPortGetSystemModeExt                              */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
BfdPortGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : BfdRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
BfdRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
#endif
