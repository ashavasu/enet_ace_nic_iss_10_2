
/*********************************************************************
-----------------------------------------------------------------------

 $Id: file.c,v 1.6 2012/02/09 11:11:23 siva Exp $

    FILE NAME                :file.c

    PRINCIPAL AUTHOR         :

    SUBSYSTEM NAME           : mmi

    MODULE NAME              : user_table_scan

    LANGUAGE                 : yacc 

    TARGET ENVIRONMENT       :

    DATE OF FIRST RELEASE    :

    DESCRIPTION              : This module to create header
                               from user_table_definition file

    CHANGE RECORD            :-

----------------------------------------------------------------------
    VERSION             AUTHOR/              DESCRIPTION OF
                        DATE                   CHANGE
----------------------------------------------------------------------

-----------------------------------------------------------------------
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

/*
 * This routine will open a file in filename in "mode" mode
 * for fptr
 *
 * if success will return NULL
 *
 */
char               *
open_file (f_ptr, filename, mode)
     FILE              **f_ptr;
     char               *filename;
     char               *mode;

{
    if ((*f_ptr = fopen (filename, mode)) == NULL)

    {
        printf ("%s cant be opened in \"%s\" mode\n", filename, mode);
        return NULL;
    }
    return filename;
}

/*
 * This routine will compare str1, str2 without considering
 * the lower, upper case
 * if equal will return 0
 * else nonzero
 */
int
strcmpi (str1, str2)
     const char         *str1, *str2;

{
    int                 i;
    if (strlen (str1) != strlen (str2))
        return 1;
    for (i = 0; i < strlen (str1); i++)
    {
        if (tolower (str1[i]) != tolower (str2[i]))
            return 1;
    }
    return 0;
}

/*
 * This routine will compare the str1, str2 for the length
 * specified without considering the lower, upper case
 * if equal will return 0
 * else nonzero
 */
int
mmi_strncmpi (str1, str2, n)
     char               *str1, *str2;
     int                 n;

{
    int                 i;
    if ((strlen (str1) < n) || (strlen (str2) < n))
    {
        return 1;
    }
    for (i = 0; i < n; i++)
    {
        if (tolower (str1[i]) != tolower (str2[i]))
        {
            return 1;
        }
    }
    return 0;
}

/* This routine will find the occurence tar(target) string
   in sou(source) string and will return the address
   if not found return NULL */
char               *
mmistrstr (sou, tar)
     const char         *sou, *tar;

{
    int                 i, j, k;
    int                 source_len, tar_len;
    source_len = strlen (sou);
    tar_len = strlen (tar);
    if (source_len < tar_len)
        return NULL;
    for (i = 0; i <= source_len - tar_len; i++)

    {
        k = i;
        for (j = 0; j < tar_len; j++)

        {
            if (sou[k++] != tar[j])
                break;
        }
        if (j == tar_len)
            return ((char *) &sou[i]);
    }
    return NULL;
}
