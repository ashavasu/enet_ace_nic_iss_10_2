/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mmist.h,v 1.27 2015/02/05 11:24:35 siva Exp $
 *
 *******************************************************************/

#include <sys/types.h>


#define MALLOC(a)  malloc(a)
#define CALLOC(a)  calloc(a, 1)
#define FREE(a)    free(a)

typedef     unsigned char  UINT1;
typedef     unsigned short UINT2;
typedef     unsigned int   UINT4;
typedef     float          FLT4;
typedef     double         DBL8;
typedef     signed char    INT1;
typedef     short          INT2;
typedef     int            INT4;
typedef     void           VOID;

typedef enum {
    FALSE,
    TRUE
}tBOOLEAN; 


#define HEADER  "\n\
/* PREFIX FILE HEADER :\n\
 *\n\
 *  ---------------------------------------------------------------------------\n\
 * |  FILE NAME             : %s   \n\
 * |                                                                           |\n\
 * |  PRINCIPAL AUTHOR      : TOOL : %s   \n\
 * |                                                                           |\n\
 * |  SUBSYSTEM NAME        : MMI                                              |\n\
 * |                                                                           |\n\
 * |  MODULE NAME           : %s         \n\
 * |                                                                           |\n\
 * |  LANGUAGE              : C                                                |\n\
 * |                                                                           |\n\
 * |  TARGET ENVIRONMENT    :                                                  |\n\
 * |                                                                           |\n\
 * |  DATE OF FIRST RELEASE : %s            \n\
 * |                                                                           |\n\
 * |  DESCRIPTION           : %s           \n\
 * |                                                                           |\n\
 *  ---------------------------------------------------------------------------\n\
 *\n\
 * CHANGE RECORD   :\n\
 *  ---------------------------------------------------------------------------\n\
 * |        |                 |                                                |\n\
 * |VERSION |   AUTHOR/       |                DESCRIPTION OF                  |\n\
 * |        |     DATE        |                  CHANGE                        |\n\
 *  ---------------------------------------------------------------------------\n\
 * |   1    | STK 16, DEC 93  |    original                                    |\n\
 *  ---------------------------------------------------------------------------\n\
 */\n\n\n"

#define MAX_MODE_LEN    50
#define MAX_PROMPT_LEN  50
#define MAX_PAR_LEN     50
#define PROMPT_STR       0
#define PROMPT_FN        1
#define FAILURE          0
#define SUCCESS          1
#define MAX_FILE_NAME   50
#define MAX_GRP_NAME    600
#define MAX_LGT_GRP     20
#define MAX_LINE_LEN   1000
#define CXT_LINE_LEN   20000
#define MAX_DYN_HELP_TOKENS 100

#define CMD_MODE           "MODE"
#define CMD_PARENT         "PARENT"
#define CMD_PROMPT         "PROMPT"
#define CMD_SYNTAX         "SYNTAX" 
#define CMD_HELP           "HELP"
#define CMD_CXT_HELP       "CXT_HELP"
#define CMD_CXT_DYN_HELP   "DYN_HELP"
#define CMD_ACTION         "ACTION"
#define CMD_COMMAND        "COMMAND" 
#define CMD_ADD_GROUP      "ADD_GROUP"
#define CMD_DEFINE         "DEFINE"
#define CMD_GROUP          "GROUP"
#define CMD_END            "END"
#define CMD_CALL           "CALL"

#define CMD_GRPID          "GRPID"
#define CMD_PRIVILEGE      "PRVID"


/* Warnings */

#define WARN_NOT_DIGIT      2
#define INDENT_SPACE       11

#define CMD_OPEN_OR        '{'
#define CMD_OPEN_OPT       '['
#define CMD_OPEN_REC       '('
#define CMD_OPEN_STR       's'
#define CMD_OPEN_CMD       'g'
#define CMD_PAR_REFER      '$'

#define MODECRE_FILE       "mmi.mode.c.c"
#define ACTION_FILE        "mmi.action.c.c"
#define SYNTAX_FILE        "mmi.syntax.c.c"
#define GRPID_FILE         "mmi.grpid.c.c"
#define PRIVILEGE_FILE     "mmi.privilege.c.c"
#define HELP_FILE          "mmi.help.c.c"
#define CXT_HELP_FILE      "mmi.cxthelp.c.c"
#define EXTERN_FILE        "mmi.extern.c.c"
#define INDMODE_FILE       "mmi.indmodes.c.c"
#define CMDCRE_FILE        "tmp.h"
#define DYN_HELP_FILE      "clicxdyn.h"

/********* Is it essential to declare MACROS ? **********/

#define ADD_MODE "\n\
  mmi_addmode(\"%s\", %s, %s, %s, %s, %s);\n"

#define ADD_HELP "\n\
  mmi_addhelp(\"%s\", &MMIMODE_%s_syntax[0], &MMIMODE_%s_semantics[0], &MMIMODE_%s_cxthelp[0], &MMIMODE_%s_privilege[0], -1);\n"


#define ADD_PROC "\n\
VOID mmi_mode_init (VOID) \n\
{\n"

#define ADD_GR_HELP_TO_MODE "\n\
  mmi_addhelp (\"%s\", &MMIGROUP_%s_syntax[0], &MMIGROUP_%s_semantics[0], &MMIGROUP_%s_cxthelp[0], &MMIGROUP_%s_privilege[0], MMI_%s_ACTNUM_START);\n\
"

#define ADD_GR_CMDS_TO_MODE "\n\
  mmi_add_grp_to_mode (\"%s\", &MMIGROUP_%s_cmds);\n\
"

#define ADD_MODE_CMD "\n\
  mmi_add_mode_cmd (\"%s\", MMICUR_PTR, %d); \n\
"

#define ADD_GR_CMD "\n\
  mmi_add_grp_cmd (&MMIGROUP_%s_cmds, MMICUR_PTR); \n\
" 

#define ACTION_PROC "\n\
INT1  mmi_action_procedure (CliHandle, vpMmi_values, u4Action_number) \n\
tCliHandle CliHandle; \n\
VOID *vpMmi_values[]; \n\
UINT4 u4Action_number; \n\
{ \n\
   switch (u4Action_number) \n\
   {\n"

#define ACTION_CASE "\
       case %d:\n"           /* 11 space before action */


#define ACTION_END "\
       default:\n\
     return FAILURE;\n\
   }\n\
   UNUSED_PARAM (CliHandle);\n\
   return SUCCESS;\n\
}\n"

#define MODE_SYNTAX  "\
CONST CHR1 *MMIMODE_%s_syntax[] = \n\
{\n"

#define MODE_GRPID  "\
CONST CHR1 *MMIMODE_%s_grpid[] = \n\
{\n"

#define MODE_PRIVILEGE  "\
CONST INT1 MMIMODE_%s_privilege[] = \n\
{\n"

#define MODE_HELP "\
CONST CHR1 *MMIMODE_%s_semantics[] = \n\
{\n"

#define MODE_CXT_HELP "\
CONST CHR1 *MMIMODE_%s_cxthelp[] = \n\
{\n"

#define GROUP_SYNTAX "\
CONST CHR1 *MMIGROUP_%s_syntax[] = \n\
{\n"

#define GROUP_GRPID "\
CONST CHR1 *MMIGROUP_%s_grpid[] = \n\
{\n"

#define GROUP_PRIVILEGE "\
CONST INT1 MMIGROUP_%s_privilege[] = \n\
{\n"

#define GROUP_HELP "\
CONST CHR1 *MMIGROUP_%s_semantics[] = \n\
{\n"

#define GROUP_CXT_HELP "\
CONST CHR1 *MMIGROUP_%s_cxthelp[] = \n\
{\n"


#define GROUP_CMDS "\
t_MMI_COMMAND_LIST MMIGROUP_%s_cmds = \n\
{\n\
   NULL,\n\
   NULL\n\
};\n"

#define  TOKEN_TYPE_STRUCT "\n\
t_MMI_TOKEN_TYPE   mmi_user_cmd_token[] = {\n"

#define  END_TOKEN_TYPE "\
   {NULL, NULL, NULL}\n\
};\n"

#define  TO_BE_EXTERN "\n\
#include \"clicmds.h\"\n\
#include \"climib.h\"\n\
#include \"cligntype.h\"\n\
#define  MMICUR_PTR gCliSessions.pMmiCurcmd_ptr\n\
#define  MMICMD_RETGET create_return_node\n\
extern   tCliSessions gCliSessions;\n"

typedef struct STACK  {
   UINT1 c; 
   INT2  count; 
} t_MMI_STACK_CMD; 


typedef struct DynHelpRegister {
    INT1 ai1Tkn[75];
    INT1 ai1Fn[75];
}tDynHelpRegister;



INT1 read_line_until();

VOID cs_init_grp_write ();
VOID cs_close_grp_write ();
VOID cs_command_write_start ();
VOID cs_do_close_command_part (); 
VOID cs_remove_dollar_inact_print (); 
VOID cs_copy_new_parent (); 
VOID cs_copy_new_prompt (); 
VOID cs_copy_new_call (); 
VOID cs_write_add_mode (); 
VOID cs_init_new_mode_parameters (); 
VOID cs_read_action_write (); 
VOID cs_read_syntax_write (); 
VOID cs_read_grpid_write (); 
VOID cs_read_privilege_write (INT1); 
VOID cs_read_help_write (); 
VOID cs_read_cxt_help_write ();
VOID cs_read_no_cxt_help_write ();
VOID cs_read_declare_write (); 
VOID cs_creat_outfiles (); 
VOID cs_close_outfiles (); 
INT4 cs_put_itin_grp_list (); 
INT4 cs_search_grp_name ();
VOID cs_add_dyn_help (INT1 *pi1Token, INT1 *pi1Function);
VOID cs_dyn_help_file_create (VOID);
