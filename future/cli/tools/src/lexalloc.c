/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lexalloc.c,v 1.5 2017/06/15 13:34:11 siva Exp $
 *
 *
 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>

#define MAX_PARSE_TOKEN 12
#define MAX_TOKEN_SIZE 100
#define MAX_MEM  MAX_PARSE_TOKEN * MAX_TOKEN_SIZE
char                mem_source[MAX_MEM];
extern int          yylineno;
void               *
lex_alloc (size)
     int                 size;

{
    static int          mem_addr = 0;
    int                 to_send;
    if (size > MAX_MEM)
    {
        printf
            ("\"lexalloc.c\" : error in lex_alloc size exceeds in line %d \n",
             yylineno);
        exit (0);
    }
    to_send = mem_addr;
    mem_addr += size;
    if (mem_addr > MAX_MEM)
    {
        mem_addr = size;
        to_send = 0;
    }
    if (size > MAX_TOKEN_SIZE)
    {
        printf
            ("line : %d warning : some where token size exceeds verify tokens\n",
             yylineno);
    }
    return (void *) &mem_source[to_send];
}
