INT1 *warn_msg[] = {
     "The recur block '(' in Or block '{' shold be the last block", /*0*/
     "Out of memory in action Part",             /** 1 ***/
     "Mode name Too long",                       /** 2 ***/
     "Non Digit Used with $"                     /** 3 ***/
};

char *err_msg[] = {
   "No mode defined", /*  0 */
   "Invalid prompt", /*  1 */   
   "already parent defined", /*  2 */  
   "parent mode long", /*  3 */ 
   "already prompt defined", /*  4 */
   "already call defined", /*  5 */
   "prompt long", /*  6 */  
   "for previous mode prompt not defined", /*  7 */
   "dollar symbol not having index", /*  8 */
   "action part expected", /*  9 */
   "syntax expected", /* 10 */
   "help expected ", /* 11 */
   "\" should not be there use escape \\", /* 12 */
   "for a mode after addgroup COMMAND should not come", /* 13 */
   " Illegal character in command ", /* 14 */
   " First a string token expected", /* 15 */
   " (inside (shold not come ", /* 16 */
   " Group name redefined ", /* 17 */
   " Group name not defined ", /* 18 */ 
   " Too many group names ", /* 19 */ 
   " Grp_name length too long ", /* 20 */
   " With out mode group going to be added ", /*21 */
   " Inside (  the token [ [ not valid ",     /*22 */
   " Inside ( the token ] ] not valid ",        /* 23 */
   " Token definitions misplaced" ,              /* 24 */
   " Redefinition of Mode name"  ,     /* 25 **/
   " The token type undefined" ,         /** 26 **/
   "Illegal No of tokens in Block",      /*** 27 ***/
   " Can not understand" ,               /** 28 **/
   " Infile Outfile can not be the same ",  /*** 29 **/
   " Usage : cmdscan  Infile  Outfile",     /*** 30 **/
   " Too many errors ",                    /** 31 **/
   "Illegal no of tokens in command"      /** 32 ***/
}; 
