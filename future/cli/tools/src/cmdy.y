/*
    * Copyright (C) 2006 Aricent Inc . All Rights Reserved
    *
    * $Id: cmdy.y,v 1.30 2016/10/03 10:34:41 siva Exp $
    *
-----------------------------------------------------------------------

    FILE NAME                : commandmode.y

    PRINCIPAL AUTHOR         :

    SUBSYSTEM NAME           : mmi

    MODULE NAME              : user_table_scan

    LANGUAGE                 : yacc 

    TARGET ENVIRONMENT       :

    DATE OF FIRST RELEASE    :

    DESCRIPTION              : This module to create header
                               from user_table_definition file


    CHANGE RECORD            :-

----------------------------------------------------------------------
    VERSION             AUTHOR/              DESCRIPTION OF
                        DATE                   CHANGE
----------------------------------------------------------------------


-----------------------------------------------------------------------
*/

%{
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "mmist.h"
#include "mmicmderr.h"

#define CLI_DISCARD_SPACE(pi1Str)  while(isspace(*(++pi1Str)))  /* Discards                                                                      whitespaces */

#define MAX_VAL_LEN(type)        (((sizeof(type) * 8) / 3) + 1)
#define MAX_VAL_SIZE(type)       ((sizeof(type) * 8))

INT1  buffer[CXT_LINE_LEN]; 
INT4  mmi_mode_num = 0;

FILE *p_cs_command;      /* for header_file to write mode creations */
FILE *p_cs_action;       /* to create action procedure */
FILE *p_cs_syntax;       /* to create command syntax file */
FILE *p_cs_grpid;        /* to create command grpid file */
FILE *p_cs_privilege;    /* to create command privilege file */
FILE *p_cs_help;         /* to create command help file */
FILE *p_cs_cxt_help;     /* to create command's context sensitive help file */
FILE *p_cs_dyn_help;     /* to create a file for dynamic cxt help register */
FILE *p_cs_extrn;        /* to extern some procedures */
FILE *p_cs_mode;         /* to seperate each mode */
FILE *p_cs_cmdcre;       /* to create the commands  */

UINT1 cs_u1outfile[MAX_FILE_NAME]; 
UINT1 cs_u1infile[MAX_FILE_NAME]; /* to have argv[1] ie. input file*/

INT4 i4ts_errno=0;             /*to count no of errors */
INT4 i4ts_parsendflag = FALSE; /* to indicate the end of parsing */ 
INT4 cs_i4case_count = 0;      /* to count the no of actions in p_cs_action file */
INT4 cs_command_token_count = 0;
INT4 gi4DynRegIndex = 0;

tDynHelpRegister DynHelpReg[MAX_DYN_HELP_TOKENS]; /* Dynamic help register */

extern yylineno; 
extern yyleng; 
extern FILE *yyin; 
extern INT1 yytext[];


UINT1 u1cm_mode_flag   = FALSE; /* indicates the presence of mode */
UINT1 u1cm_parent_flag = FALSE; /* indicates the presence of parent */
UINT1 u1cm_prompt_flag = FALSE; /* indicates the presence of prompt */
UINT1 u1cm_call_flag   = FALSE; /* indicates the presence of call */
UINT1 u1cm_declare_flag = FALSE;  /*user declaration into %% */
UINT1 u1cm_add_group_flag = FALSE;  /*after add_group no command come*/
UINT1 u1cm_all_defined_flag = FALSE;/*whether all defined or not*/ 
UINT1 u1cs_token_def_flag = FALSE;

UINT1 u1cm_mode[MAX_MODE_LEN];     /* to extract the mode */
UINT1 u1cm_parent[MAX_MODE_LEN];   /* to extract the parent */
UINT1 u1cm_prompt[MAX_PROMPT_LEN]; /* to extract the prompt */
UINT1 u1cm_call[MAX_PROMPT_LEN];   /* to extract the call function */
UINT1 u1cm_cmd_group[MAX_MODE_LEN]; /* group name presently defined */

UINT1 u1cm_prompt_type = 0; /* indicates the type of prompt */
UINT1 u1cm_cmd_valid = 0;   /* to check the first token in command to be string */    
UINT1 u1cm_cmd_recur_valid = 0; /*to check the command ( valid */

t_MMI_STACK_CMD   stack_char[40];   /* to maintain the command to be closed */
INT2  stack_top = 0; 

/* to maintain the grp_name with out duplication */
INT1 cs_i1grp_name_list[MAX_GRP_NAME][MAX_LGT_GRP+1]; 
INT2 i2grp_count = 0; 

static print_warning (INT2);
VOID cs_read_privilege_write (INT1); 
extern char * open_file (FILE **, char *, char *);
VOID print_valid_error (INT4);
VOID cs_write_token_struct (INT1 *,INT1 *,INT1 *);
VOID cs_parse_integer (INT1 *);
VOID cs_parse_sinteger (INT1 *);
VOID cs_parse_short (INT1 *);
VOID cs_parse_float (INT1 *);
VOID cs_parse_string (INT1 *);
UINT4 cs_atol (INT1  *, INT2);
INT4 cs_satol (INT1  *, INT2);
UINT2 cs_atoi (INT1  *, INT2);
DBL8 cs_atof (INT1 *);
char *mmistrstr (char *, char *);

%} 

%start begin
%union {
   UINT1 *s; 
   INT4 intval; 
}

%token OPEN_BR CLOSE_BR OPEN_OPT CLOSE_OPT OPEN_REC CLOSE_REC
%token OPT_OR
%token OPEN_DEC  
%token MODE PARENT PROMPT CALL COLON UNKNOWN TOKEN_DEF
%token COMMAND ACTION GRPID PRIVILEGE SYNTAX HELP CXT_HELP CXT_DYN_HELP 
%token DEFINE GROUP END ADD_GROUP
%token <s> STRING QUOTED_STRING VALTOK 

%%

begin :
cs_search
| begin cs_search
; 

/*
 * This will parse the command definition file as defined
 * for each mode
 *
 * MODE :
 * PARENT:
 * PROMPT : CALL:
 * COMMAND :
 * ACTION:
 * SYNTAX :
 * HELP :
 *  .
 *  .
 * or
 * DEFINE GROUP :
 *    COMMAND:
 *    ACTION :
 *    SYNTAX :
 *    HELP :
 * END GROUP
 */

cs_search:
TOKEN_DEF STRING STRING STRING 
{
   enter_valtok_defined ($2);
   if (u1cs_token_def_flag == FALSE) {
      cs_write_token_struct ((INT1 *)$2,(INT1 *) $3,(INT1 *) $4);
   }
   else {
      print_valid_error(24);
   }
}
|
TOKEN_DEF STRING STRING 
{
   enter_valtok_defined ($2);
   if (u1cs_token_def_flag == FALSE) {
      cs_write_token_struct ((INT1 *)$2,(INT1 *)$3, "NULL");
   }
   else {
      print_valid_error(24);
   }
}
|
OPEN_DEC
{
   /* if already occured set u1cm_declare_flag to false */ 
   /* otherwise start now                               */
   if (!u1cm_declare_flag) {
      cs_read_declare_write (); 
      u1cm_declare_flag = TRUE; 
   }
   else {
      u1cm_declare_flag = FALSE; 
   }
} 
| MODE COLON STRING 
{
   /* check whether GROUP ALL declared                 */
   /* if ok init new mode                              */

   if (u1cs_token_def_flag == FALSE) {
       u1cs_token_def_flag = TRUE;
       fprintf (p_cs_command, END_TOKEN_TYPE);
       fprintf (p_cs_command, ADD_PROC); 
   }
  /* if (u1cm_all_defined_flag == FALSE) {
      printf ("Group ALL should be defined first\n"); 
      cs_close_outfiles (); 
      exit (1); 
    } */
   check_mode_for_redefine ($3);
   cs_init_new_mode_parameters ($3); 
	fprintf(p_cs_mode, "\nUNUSED_PARAM(i);\n} \n");
    fprintf(p_cs_mode, "\nVOID mmi_%s_cmd_creat(VOID);\n",$3);
    fprintf(p_cs_mode, "\nVOID mmi_%s_cmd_creat(VOID) \n{ \nINT2 i = 0;\n",$3);
    fprintf(p_cs_cmdcre, "   mmi_%s_cmd_creat(); \n",$3);
} 
| PARENT COLON STRING {
   cs_copy_new_parent ($3); 
} 
| PROMPT COLON {
} cs_prompt_ext 
| CALL COLON STRING {
   cs_copy_new_call ($3); 
}
| COMMAND COLON {
   cs_command_write_start ();
} cs_command_ext
| DEFINE GROUP COLON STRING{
   /* if group already defined show error                    */
   /* otherwise put it in list                               */
   /* if any mode is in pending write it and init group write*/
   if (cs_search_grp_name ($4) == SUCCESS) {
      print_valid_error (17); 
   }  
   else {
      cs_put_itin_grp_list ($4); 
   }
   if (u1cm_mode_flag) {
       cs_write_add_mode (); 
   } 
   strcpy ((INT1 *)u1cm_cmd_group,(INT1 *) $4); 
   cs_init_grp_write ();
} cs_group_cmd_ext
| ADD_GROUP COLON STRING{
   /* check this group already defined or not */
   /* defined add that one with present mode  */
   if (cs_search_grp_name ($3) == FAILURE) {
      print_valid_error (18); 
   }  
   if (u1cm_mode_flag) {  
      u1cm_add_group_flag = TRUE; 
      fprintf (p_cs_mode, ADD_GR_HELP_TO_MODE, u1cm_mode, $3, $3, $3, $3, $3); 
      fprintf (p_cs_mode, ADD_GR_CMDS_TO_MODE, u1cm_mode, $3); 
   } 
   else {
      print_valid_error(21);
   }
}
|
{
   print_valid_error (11); 
}
; 

/*
 * This will collect the commands under the group defined
 */

cs_group_cmd_ext:
COMMAND COLON {
   cs_command_write_start ();  
} cs_command_ext cs_group_cmd_ext
| END GROUP{
   cs_close_grp_write ();  
}
; 


/*
 * This will collect the commands creation and actions for a command
 */
cs_command_ext:
STRING {
   /* increment number of parameter ie.count */
   u1cm_cmd_valid = 1;  /* set string has been strated */
   stack_char[stack_top].count++; 
   fprintf (p_cs_mode, "  TOK (\"%s\", %d), ", $1, cs_command_token_count); 
   cs_command_token_count++;
} cs_command_ext
|
VALTOK {

   INT4 i4Token_type = -1;

   INT1 *pi1TempStr, i1Temp;

   pi1TempStr =(INT1 *)$1;

   /* Checks for VALTOK defined r not */

   while (*pi1TempStr)
   {
       if ( (!isalnum (*pi1TempStr)) && (*pi1TempStr != '_') )
          
           break;
       pi1TempStr++;
   }

   i1Temp = *pi1TempStr;
   *pi1TempStr = '\0';

   i4Token_type = check_valtok_defined ($1);
   if (!u1cm_cmd_valid) {
      print_valid_error (15);
   }

   u1cm_cmd_valid = 2;
   stack_char[stack_top].count++;
   fprintf (p_cs_mode, "  VALTOK (\"%s\", %d,", $1, cs_command_token_count);
   cs_command_token_count++;

   *pi1TempStr = i1Temp;

   pi1TempStr--;
   CLI_DISCARD_SPACE (pi1TempStr);

/*
 * This will check for optional Range values given in the .def file
*/

   if (*pi1TempStr != '\0')
   {
       switch(i4Token_type)
       {
           case 0:
               cs_parse_integer (pi1TempStr);
           break;
           case 1:
               cs_parse_string (pi1TempStr);
           break;
           case 9:
               cs_parse_float (pi1TempStr);
           break;
           case 15:
               cs_parse_short (pi1TempStr);
           break;
           case 29:
               cs_parse_sinteger (pi1TempStr);
           break;
           default:
               yyerror ();
           break;
       }
   }
   else
   {
       fprintf (p_cs_mode, " NULL ), ");
   }

} cs_command_ext
|
OPEN_BR {
   if (!u1cm_cmd_valid) {
      print_valid_error (15); 
   }
   u1cm_cmd_valid = 3;
   /* inc par of pre command and put it it in stack */
   stack_char[stack_top].count++; 
   stack_top++; 
   stack_char[stack_top].c = CMD_OPEN_OR; 
   stack_char[stack_top].count = 0; /* take care diff*/
   fprintf (p_cs_mode, "  GRP_OR (NULL, "); 

   /* put first or option */
   stack_char[stack_top].count++; 
   stack_top++; 
   stack_char[stack_top].c = CMD_OPEN_STR; 
   stack_char[stack_top].count = 0; 
   fprintf (p_cs_mode, "  GRP_CMD1 (NULL, "); 
} cs_command_ext
|
CLOSE_BR {
   INT1 c; 
   /* close for last or option if there */
   u1cm_cmd_valid = 4;
   c = stack_char[stack_top].c; 
   if (c== CMD_OPEN_STR) {
      cs_do_close_command_part (c); 
      stack_top--; 
   }  
   else {
      print_valid_error (14); 
   }

   /* close the GRP_OR  */
   c = stack_char[stack_top].c; 
   if (c== CMD_OPEN_OR) {
      cs_do_close_command_part (c); 
      stack_top--; 
   }  
   else {
      print_valid_error (14); 
   }
} cs_command_ext
|
OPEN_OPT {
   if (!u1cm_cmd_valid) {
      print_valid_error (15); 
   }
   if (u1cm_cmd_recur_valid && u1cm_cmd_valid == 5) {
      print_valid_error(22);
   }
   u1cm_cmd_valid = 5; 
   stack_char[stack_top].count++; 
   stack_top++; 
   stack_char[stack_top].c = CMD_OPEN_OPT; 
   stack_char[stack_top].count = 0; 
   fprintf (p_cs_mode, "  GRP_OPT (%d, i,", cs_i4case_count); 

   /* put first or option */
   stack_char[stack_top].count++; 
   stack_top++; 
   stack_char[stack_top].c = CMD_OPEN_STR; 
   stack_char[stack_top].count = 0; 
   fprintf (p_cs_mode, "  GRP_CMD1 (NULL, "); 
} cs_command_ext
|
CLOSE_OPT {
   INT1 c; 
   if (u1cm_cmd_recur_valid && u1cm_cmd_valid == 6) {
      print_valid_error(23);
   }
   /* close the GRP_CMD1 */
   u1cm_cmd_valid = 6;
   c = stack_char[stack_top].c; 
   if (c == CMD_OPEN_STR) {
      cs_do_close_command_part (c); 
      stack_top--; 
   }
   else {
      print_valid_error (14); 
   }

   /* close the GRP_OPT */
   c = stack_char[stack_top].c; 
   if (c== CMD_OPEN_OPT) {
      cs_do_close_command_part (c); 
      stack_top--; 
   }  
   else {
      print_valid_error (14); 
   }
} cs_command_ext
|
OPEN_REC {
   INT1 c; 
   /* check error REC should not come in REC */
   /*             REC should not come in OR  */ 
   if (!u1cm_cmd_valid) {
      print_valid_error (15); 
   }
   if (!u1cm_cmd_recur_valid) {
      u1cm_cmd_recur_valid = 1; 
   }
   else{
      print_valid_error (16);  
   }
   u1cm_cmd_valid = 7;

   c = stack_char[stack_top].c; 
   if ((c==CMD_OPEN_STR || c==CMD_OPEN_OR) && stack_char[stack_top].count ==0) {
      print_valid_warning (0);
   }
   stack_char[stack_top].count++; 
   stack_top++; 
   stack_char[stack_top].c = CMD_OPEN_REC; 
   stack_char[stack_top].count = 0; 
   fprintf (p_cs_mode, "  GRP_REC (NULL, "); 
} cs_command_ext
|
CLOSE_REC {
   INT1 c; 
   u1cm_cmd_recur_valid = 0; 
   u1cm_cmd_valid = 8;
   c = stack_char[stack_top].c; 
   if (c == CMD_OPEN_REC) {
      cs_do_close_command_part (c); 
      stack_top--; 
   }
   else {
      print_valid_error (14); 
   }
} cs_command_ext
| 
OPT_OR {
   UINT1 c; 
   /* close the previous or command */
   u1cm_cmd_valid = 9;
   c = stack_char[stack_top].c; 
   if (c == CMD_OPEN_STR) {
      cs_do_close_command_part (c); 
      stack_top--; 
   } 
   else {
      print_valid_error (14); 
   }

   /* start neew or command */
   c = stack_char[stack_top].c; 
   if (c == CMD_OPEN_OR) {
      stack_char[stack_top].count++; 
      stack_top++; 
      stack_char[stack_top].c = CMD_OPEN_STR; 
      stack_char[stack_top].count = 0; 
      fprintf (p_cs_mode, "  GRP_CMD1 (NULL, "); 
   }
   else {
      print_valid_error (14); 
   }
} cs_command_ext
|
ACTION COLON {
   UINT1 c; 
   c = stack_char[stack_top].c; 
   if (c == CMD_OPEN_CMD) {
      cs_do_close_command_part (c); 
      stack_top = 0; 
      cs_command_token_count =0;
   }
   else {
      print_valid_error (14); 
   }
   u1cm_cmd_valid = 0; /* indicating the command finished */ 
   cs_read_action_write (); 
} cs_action_ext
|
QUOTED_STRING {
   /* increment number of parameter ie.count */
   u1cm_cmd_valid = 1;  /* set string has been strated */
   stack_char[stack_top].count++; 
   fprintf (p_cs_mode, "  TOK (\"%s\", %d), ", $1, cs_command_token_count); 
   cs_command_token_count++;
} cs_command_ext
|
{
   printf ("\r\n ERROR : %s",yytext);
   print_valid_error (9); 
}
; 

/*
 * This  will extract the action for a command as
 *  1.action name 
 *  2.parameters
 * until SYNTAX coming
 */

cs_action_ext:
SYNTAX COLON {
   cs_read_syntax_write (); 
} cs_privilege_ext
|
{
   print_valid_error (10); 
}
;

cs_privilege_ext:
PRIVILEGE COLON {
   cs_read_privilege_write (-1);
} cs_help_ext
|  cs_help_ext {cs_read_privilege_write (1);}
|
{
   print_valid_error (11); 
}
; 

/*
 * This will extract the help as semantics for command
 */

cs_help_ext:
cs_help_only {   fprintf (p_cs_cxt_help, "NULL, \n"); }
| cs_help_only cs_cxt_help_ext 
;


cs_help_only:
HELP COLON  {
   cs_read_help_write (); 
} 
|
{
   print_valid_error (11); 
}
; 

cs_cxt_help_ext : cs_cxt_help
                | cs_cxt_help cs_cxt_dyn_help
                ;


cs_cxt_help: 
CXT_HELP COLON {
#if defined (CXT_HELP_WANTED)
    cs_read_cxt_help_write();
#else
    cs_read_no_cxt_help_write ();
#endif
} 
|
{
    print_valid_error (32);
}
;


cs_cxt_dyn_help: CXT_DYN_HELP COLON cs_dyn_help_ext;

cs_dyn_help_ext: cs_dyn_help_params
               | cs_dyn_help_params OPT_OR cs_dyn_help_ext
               ;


cs_dyn_help_params: STRING STRING { cs_add_dyn_help ($1,$2); }
|
{
        print_valid_error (32);
}
;

                  
/*
 * This will extract the prompt for a mode
 * the prompt may be as
 *    1.string (if function call) 
 *    2.quoted_string (if directly specified) 
 */   

cs_prompt_ext:
STRING  {
   /* if fuction write it as extern */
   u1cm_prompt_type = PROMPT_FN; /* for function call */
   /*fprintf (p_cs_extrn, "extern %s; \n", $1); */
   cs_copy_new_prompt ($1); 
}
|
QUOTED_STRING {
   u1cm_prompt_type = PROMPT_STR; 
   cs_copy_new_prompt ($1); 
} 
|
{
   print_valid_error (1); 
}

; 

%%

/*
 * This will start to writ grp creation
 */
VOID cs_init_grp_write()
{
   fprintf (p_cs_syntax, GROUP_CMDS, u1cm_cmd_group); /****/
   fprintf (p_cs_syntax, GROUP_SYNTAX, u1cm_cmd_group); 
   fprintf (p_cs_grpid, GROUP_GRPID, u1cm_cmd_group); 
   fprintf (p_cs_privilege, GROUP_PRIVILEGE, u1cm_cmd_group); 
   fprintf (p_cs_help, GROUP_HELP, u1cm_cmd_group);
   fprintf (p_cs_cxt_help, GROUP_CXT_HELP, u1cm_cmd_group);
   return;
}

VOID cs_close_grp_write()
{
   fprintf (p_cs_syntax, "   NULL\n}; \n"); 
   fprintf (p_cs_grpid, "   NULL\n}; \n"); 
   fprintf (p_cs_privilege, "   -1\n}; \n"); 
   fprintf (p_cs_help, "   NULL\n}; \n");
   fprintf (p_cs_cxt_help, "   NULL\n}; \n");
   return;
}

/*
 * This will init to start to write a command 
 */
VOID cs_command_write_start ()
{
   fprintf (p_cs_mode, "\n  i++;");
   fprintf (p_cs_mode, "\n  MMICUR_PTR = GRP_CMD (%d, i,", cs_i4case_count);
   stack_top = 0; 
   stack_char[0].c = CMD_OPEN_CMD; 
   stack_char[0].count = 0; 
   return;
}


/*
 * This will close the command function which was started by filling
 * number of parameter by NULL according to the command function
 */
VOID cs_do_close_command_part (u1top_char) 
UINT1 u1top_char; 
{
   INT2 i, i2no_par; 

   if ( (u1top_char == CMD_OPEN_OR) || (u1top_char == CMD_OPEN_OPT)
        || (u1top_char == CMD_OPEN_STR) || (u1top_char == CMD_OPEN_REC) 
        || (u1top_char == CMD_OPEN_CMD) ) 
   {
   }
   else 
   {
      print_valid_error (14); 
   }

   /* i indicates the number of par already filled */
   i = stack_char[stack_top].count; 
   /* 'g' indicates to close total command */

   if (u1top_char == CMD_OPEN_CMD) {
      fprintf (p_cs_mode, "NULL); \n"); 
      if (u1cm_mode_flag) {
         fprintf (p_cs_mode, ADD_MODE_CMD, u1cm_mode, cs_i4case_count); 
      }
      else {
         fprintf (p_cs_mode, ADD_GR_CMD, u1cm_cmd_group); 
      }
   }
   else {
      fprintf (p_cs_mode, "NULL), \n"); 
      for (i=0; i<stack_top; i++) {
         fprintf (p_cs_mode, "  "); 
      }
   }
}


/*
 * This will write the actios for a command into p_cs_action
 * by giving proper index for each command
 *
 * And if any CMD_PAR_REFER has been referenced that will be removed as
 * 'vpMmi_values'
 */

VOID cs_remove_dollar_inact_print (p_i1act_part) 
INT1 *p_i1act_part; /* (K) action part line by line */
{
   INT1 i1temp_buf[CXT_LINE_LEN]; 
   INT1 *temp_ptr1; 
   INT1 *temp_ptr2; 
   INT2 i2len=0, 
        i2dollar_flag; 
   
   if (!strlen (p_i1act_part) )  {
      fprintf (p_cs_action, "\n");
      return; 
   }
   temp_ptr1 = p_i1act_part; 
   
   strcpy (i1temp_buf, ""); 
   while ((temp_ptr2 = strchr (temp_ptr1, (int) CMD_PAR_REFER) ) != NULL) {
      *temp_ptr2 = '\0'; 
      if ( (i2len + strlen(temp_ptr1) +20)  >= CXT_LINE_LEN) {
         print_valid_warning (1);
      }
      i2dollar_flag = FALSE; 
      temp_ptr2++; 
      strcat (i1temp_buf, temp_ptr1); 
      strcat (i1temp_buf, "vpMmi_values["); 
      i2len = strlen (i1temp_buf); 
      while (isalnum (*temp_ptr2) || (*temp_ptr2 == '_')){
         if (!isdigit (*temp_ptr2)) {
               i2dollar_flag = WARN_NOT_DIGIT;
         }
         if (i2dollar_flag == FALSE)
            i2dollar_flag = TRUE; 
         i1temp_buf[i2len] = *temp_ptr2; 
         temp_ptr2++; 
         i2len++; 
      }
      if (i2dollar_flag == FALSE) {
         print_valid_error (8); 
      }
      else 
      {
         if (i2dollar_flag  == WARN_NOT_DIGIT)
             print_warning (WARN_NOT_DIGIT);
      }

      i1temp_buf[i2len++] = ']'; 
      i1temp_buf[i2len] = '\0'; 
      temp_ptr1 = temp_ptr2; 
   }     
   if ( (i2len + strlen(temp_ptr1))  >= CXT_LINE_LEN) {
      print_valid_warning (1);
   }
   strcat (i1temp_buf, temp_ptr1); 


   fprintf (p_cs_action, "%s\n",i1temp_buf); 

}


/*
 *  This will copy the parent name for the current mode
 *     ( parent will be in quoted )
 *  If the parent name already defined this will show the error 
 */
VOID cs_copy_new_parent (u1parent) 
UINT1 *u1parent; /* (K) parent name */
{
   UINT1 u1temp[MAX_FILE_NAME]; 

   if (!u1cm_parent_flag) {
      u1cm_parent_flag = TRUE; 
      if (strlen (u1parent) > MAX_MODE_LEN) {
         print_valid_error (3); 
      }
      sprintf (u1cm_parent, "\"%s\"", u1parent); 
   }
   else {
      print_valid_error (2); 
   }
}


/* This will copy the prompt for the current mode and if already
 * present this will show the error
 */

VOID cs_copy_new_prompt (u1prompt) 
UINT1 *u1prompt; /* (K) */
{
   if (!u1cm_prompt_flag) {
      u1cm_prompt_flag = TRUE; 
      if (strlen (u1prompt) > MAX_PROMPT_LEN) {
         print_valid_error (6); 
      }
      strcpy (u1cm_prompt, u1prompt); 
   }
   else {
      print_valid_error (4); 
   }
}


/*
 * This routine will copy the call procedure for the validation of 
 * prompt for the current mode and if already present will whow the 
 * error
 */

VOID cs_copy_new_call (u1call) 
UINT1 *u1call; /* (K) */
{
  /* fprintf (p_cs_extrn, "extern %s; \n", u1call);  */
   if (!u1cm_call_flag) {
      u1cm_call_flag = TRUE; 
      if (strlen (u1call) > MAX_PROMPT_LEN) {
         print_valid_error (6); 
      }
      strcpy (u1cm_call, u1call); 
   }
   else {
      print_valid_error (5); 
   }
}

/*
 * This will write the addmode () call for a mode defined
 * each flag will show the parameters defined if the flag values
 * are 1 then the parameter will be assumed for this mode
 *
 * for u1cm_prompt_flag the string will be copied as "PROMPT_STR"
 * for value 0, "PROMPT_FN" for value 1. otherwise '-1'
 *
 * After writing this will reinitialize all flags
 */

VOID cs_write_add_mode () 
{
   UINT1 u1type[MAX_PROMPT_LEN]; 
   UINT1 u1tmpprompt[MAX_PROMPT_LEN]; 
   
   if (u1cm_prompt_flag) {
      if (u1cm_prompt_type) {
         strcpy (u1type, "PROMPT_FN"); 
      }
      else {
         strcpy (u1type, "PROMPT_STR"); 
         sprintf (u1tmpprompt, "\"%s\"", u1cm_prompt); 
         strcpy (u1cm_prompt, u1tmpprompt); 
      }
   }
   else {
      strcpy (u1type, "-1"); 
      strcpy (u1cm_prompt, "NULL"); 
      /* print_valid_error (7); */
   }
   
   if (!u1cm_parent_flag) {
      strcpy (u1cm_parent, "NULL"); 
   }
   if (!u1cm_call_flag) {
      strcpy (u1cm_call, "NULL"); 
   }
   if (u1cm_mode_flag) {
      if ((strcmp (u1type, "PROMPT_STR")) == 0)
      {
          fprintf (p_cs_command, ADD_MODE, u1cm_mode, u1cm_parent, u1type, u1cm_prompt, "NULL", u1cm_call); 
      }
      else if((strcmp (u1type, "PROMPT_FN")) == 0)
      {
          fprintf (p_cs_command, ADD_MODE, u1cm_mode, u1cm_parent, u1type, "NULL", u1cm_prompt, u1cm_call); 
      }
      fprintf (p_cs_mode, ADD_HELP, u1cm_mode, u1cm_mode, u1cm_mode, u1cm_mode, u1cm_mode); 
      fprintf (p_cs_mode, ADD_GR_HELP_TO_MODE, u1cm_mode, "ALL", "ALL", "ALL", "ALL","ALL"); 
      fprintf (p_cs_mode, ADD_GR_CMDS_TO_MODE, u1cm_mode, "ALL"); 
      fprintf (p_cs_syntax, "   NULL\n}; \n"); 
      fprintf (p_cs_grpid, "   NULL\n}; \n"); 
      fprintf (p_cs_privilege, "   -1\n}; \n"); 
      fprintf (p_cs_help, "   NULL\n}; \n");
      fprintf (p_cs_cxt_help, "   NULL\n}; \n");
   }
   else {
      print_valid_error (0); 
   }
   u1cm_mode_flag = u1cm_parent_flag = u1cm_prompt_flag = u1cm_call_flag = FALSE; 
   u1cm_prompt_type = 0; 
   u1cm_add_group_flag = FALSE; 
   
}


/*
 * This routine will be called when new Mode definition coming
 *
 * At that time it will check the u1cm_mode_flag for any has been 
 * defined previously. If defined this will call cs_write_add_mode () 
 *
 * at the end this will set u1cm_mode_flag
 */ 

VOID cs_init_new_mode_parameters (u1mode) 
UINT1 *u1mode; /* (K) mode extracted */
{
   if (strlen (u1mode) >= MAX_MODE_LEN) {
      print_valid_warning (2);
   }
   if (!u1cm_mode_flag) {
      u1cm_mode_flag = TRUE; 
      sprintf (u1cm_mode, "%s", u1mode); 
   }
   else {
      cs_write_add_mode (); 
      u1cm_mode_flag = TRUE; 
      sprintf (u1cm_mode, "%s", u1mode); 
   }
   fprintf (p_cs_syntax, MODE_SYNTAX, u1cm_mode); 
   fprintf (p_cs_grpid, MODE_GRPID, u1cm_mode); 
   fprintf (p_cs_privilege, MODE_PRIVILEGE, u1cm_mode); 
   fprintf (p_cs_help, MODE_HELP, u1cm_mode);
   fprintf (p_cs_cxt_help, MODE_CXT_HELP, u1cm_mode);
}


/*
 * To print the error from err_msg
 */
VOID print_valid_error (i4err_ind) 
INT4 i4err_ind; 
{
   i4ts_errno++; 
   printf ("line : %d %s  token : %s\n", yylineno, err_msg[i4err_ind], yytext); 
   
   if (i4ts_errno > 20) {
      printf ("%s\n", err_msg[31]); 
      cs_close_outfiles (); 
      exit (1); 
   } 
} 

print_valid_warning (i4err_ind) 
INT4 i4err_ind; 
{
   printf ("line : %d Warning %s  token : %s\n", yylineno, warn_msg[i4err_ind], yytext); 
}

VOID print_usage () 
{
   printf ("%s\n", err_msg[30]); 
   exit (1); 
} 


/*
 * This will print the error whenver improper token is coming
 */
yyerror () 
{
   i4ts_errno++; 
   yytext[yyleng]='\0'; 
   
   printf ("line : %d \" %s \"  %s\n", yylineno, yytext, err_msg[28]); 

   cs_close_outfiles (); 
   exit (1); 
} 


/* This is used when EOF is coming
 */
yywrap () 
{
   return (1); 
} 



/*
 * This routine will read whatever in the input file until
 * any one of the strings in *p_cs_i1action_until[]
 * and will write into ACTION_FILE ie. p_cs_action
 */

INT1 *p_cs_i1action_until[] =
{
   CMD_SYNTAX, 
   CMD_PRIVILEGE, 
   NULL
}; 

VOID cs_read_action_write () 
{
   INT1 c; 
   
   fprintf (p_cs_action, ACTION_CASE, cs_i4case_count); 
   fprintf (p_cs_action, "# line %d \"%s\"\n", yylineno, cs_u1infile); 
   /* skip_white_input (); */
   do{
      c = read_line_until (buffer, p_cs_i1action_until, 0); 
      cs_remove_dollar_inact_print (buffer); 
   }while (c != 0); 
   fprintf (p_cs_action, "%-12sbreak; \n\n", ""); 
   cs_i4case_count++; 
} 


/*
 * This routine will read whatever in the file until 
 *  any one of strings in *p_cs_i1syntax_until[]
 * and will write into SYNTAX_FILE  ie. p_cs_syntax
 */

INT1 *p_cs_i1syntax_until[] =
{
   "/*",
   CMD_HELP, 
   CMD_PRIVILEGE, 
   "%%", 
   NULL
}; 

VOID cs_read_syntax_write () 
{
   INT1 c;
   char *p; 
   fprintf (p_cs_syntax, "   \""); 
   skip_white_input ();
   do{
      c = read_line_until (buffer, p_cs_i1syntax_until, 1); 
      if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
         print_valid_error (12); 
      }
      fprintf (p_cs_syntax, "%s", buffer); 
   }while (c != 0); 
   fprintf (p_cs_syntax, "\", \n"); 
} 

/*
 * This routine will read whatever in the file until 
 *  any one of strings in *p_cs_i1grpid_until[]
 * and will write into GRPID_FILE  ie. p_cs_grpid
 */

INT1 *p_cs_i1grpid_until[] =
{
   "\n",
   "/*",
   CMD_HELP, 
   CMD_COMMAND, 
   "%%", 
   NULL
}; 


VOID cs_read_grpid_write (INT1 *pi1GrpId) 
{
   INT1 c; 
   fprintf (p_cs_grpid, "   \""); 
   if (pi1GrpId == NULL)
   {
       skip_white_input ();
       do{
             c = read_line_until (buffer, p_cs_i1grpid_until, 1); 
             if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
                 print_valid_error (12); 
             }
             fprintf (p_cs_grpid, "%s", buffer); 
             if ( c!= 0) {  
                 fprintf (p_cs_grpid, "\\n"); 
             }  
         }while (c != 0); 
   }
   else
   {
        fprintf (p_cs_grpid, "%s", pi1GrpId); 
   }
   fprintf (p_cs_grpid, "\", \n"); 
} 

/*
 * This routine will read whatever in the file until 
 *  any one of strings in *p_cs_i1privilege_until[]
 * and will write into PRIVILEGE_FILE  ie. p_cs_privilege
 */

INT1 *p_cs_i1privilege_until[] =
{
   "\n",
   "/*",
   CMD_HELP, 
   CMD_COMMAND, 
   "%%", 
   NULL
}; 


VOID cs_read_privilege_write (INT1 i1Privilege) 
{
   INT1 c; 
   if (i1Privilege == -1)
   {
       skip_white_input ();
       do{
             c = read_line_until (buffer, p_cs_i1privilege_until, 1); 
             if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
                 print_valid_error (12); 
             }
             fprintf (p_cs_privilege, "    %d", atoi (buffer)); 
             if ( c!= 0) {  
                 fprintf (p_cs_privilege, "\\n"); 
             }  
         }while (c != 0); 
   }
   else
   {
        fprintf (p_cs_privilege, "    %d", i1Privilege); 
   }
   fprintf (p_cs_privilege, ", \n"); 
} 

/*
 * This routine will read whatever in the input file until
 * any one of the strings in p_cs_i1help_until
 * will write into HELP_FILE ie. p_cs_help
 */

INT1 *p_cs_i1help_until[] =
{
   "/*",
   CMD_ADD_GROUP, 
   CMD_COMMAND, 
   CMD_END, 
   CMD_DEFINE, 
   CMD_MODE,
   CMD_CXT_HELP,
   CMD_PRIVILEGE, 
   "%%", 
   NULL
}; 

VOID cs_read_help_write () 
{
   INT1 c; 
   INT4 i4len = 0; 
   
   fprintf (p_cs_help, "   \""); 
   skip_white_input ();
   do{
      c = read_line_until (buffer, p_cs_i1help_until, 1); 
      if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
         print_valid_error (12); 
      }
      i4len += strlen(buffer);
      if (i4len < CXT_LINE_LEN)    {
         fprintf (p_cs_help, "%s", buffer); 
      }
      if (( c != 0) && (c != EOF)) {
         fprintf (p_cs_help, "\\r\\n"); 
      }  
   }while (c != 0); 
   fprintf (p_cs_help, "\", \n"); 
} 

/* This will use the same logic as help and read until the expected few tokens
 * that applies for both help and context sensitive help*/

INT1 *p_cs_i1cxthelp_until[] =
{
    "/*",
    CMD_ADD_GROUP,
    CMD_COMMAND,
    CMD_END,
    CMD_DEFINE,
    CMD_MODE,
    CMD_CXT_DYN_HELP,
    CMD_PRIVILEGE,
    "%%",
    NULL
};


VOID cs_read_cxt_help_write ()
{
    INT1 c;
    INT4 i4len = 0;

    fprintf (p_cs_cxt_help, "   \"");
    skip_white_input ();
    do{
        c = read_line_until (buffer, p_cs_i1cxthelp_until, 1);
        if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
            print_valid_error (12);
        }
        i4len += strlen(buffer);
        if (i4len < CXT_LINE_LEN)    {
            fprintf (p_cs_cxt_help, "%s", buffer);
        }
        if (( c != 0) && (c != EOF)) {
            fprintf (p_cs_cxt_help, "\\r\\n");
        }
    }while (c != 0);
    fprintf (p_cs_cxt_help, "\", \n");
}


/* This routine will store the token for dynamic help and related function
   name that has to be registered */
VOID cs_add_dyn_help (INT1 *pi1Token, INT1 *pi1Function)
{
    INT4 i4Count = 0;
    for (i4Count = 0; i4Count < gi4DynRegIndex ; i4Count++)
    {
        if (strcmp (DynHelpReg[i4Count].ai1Tkn, pi1Token) == 0)
        {
            return;
        }
    }
    strcpy (DynHelpReg[i4Count].ai1Tkn, pi1Token);
    strcpy (DynHelpReg[i4Count].ai1Fn, pi1Function);
    gi4DynRegIndex++;
    return;
}

/* This Routine updates the dynamic help register to the file clicxdyn.h */

VOID cs_dyn_help_file_create (VOID)
{

    INT4 i4Count = 0;

    /* Include header files */
    fprintf (p_cs_dyn_help, "#include \"clicmds.h\"\n\n");

    /* Write ifndef */
    fprintf (p_cs_dyn_help, "#ifndef __CLICXDYN_H__\n");
    fprintf (p_cs_dyn_help, "#define __CLICXDYN_H__\n\n\n");

    /* Declare global variable with max entries */
    fprintf (p_cs_dyn_help,"\n/* Variable to store number of records */\n");
    fprintf (p_cs_dyn_help,"\nINT4  gi4MaxRegEntries = %d;",gi4DynRegIndex);
    /* Create Register */
    fprintf (p_cs_dyn_help,"\n\n/* Registration table */\n");
    fprintf (p_cs_dyn_help,"tDynHelpRegInfo gDynHelpRegTable[] =\n");
    fprintf (p_cs_dyn_help,"                                 { ");

    for (i4Count = 0; i4Count < gi4DynRegIndex ; i4Count++)
    {
        if (i4Count != 0)
        {
            fprintf (p_cs_dyn_help, "                                   ");
        }
        fprintf (p_cs_dyn_help, " { \"%s\", %s }",
                 DynHelpReg[i4Count].ai1Tkn, DynHelpReg[i4Count].ai1Fn);
        if (i4Count != (gi4DynRegIndex - 1))
        {
            fprintf (p_cs_dyn_help, ",\n");
        }
       
    }

    fprintf (p_cs_dyn_help," };\n");

    /* wirte #endif */
    fprintf (p_cs_dyn_help, "#endif /* __CLICXDYN_H__ */\r\n");
    return;

}


/*
 * This routine will read the declaration part of input file
 * and write into the extern file
 */

INT1 *p_cs_i1dec_until[]=
{
   "%%", 
   NULL
}; 

VOID cs_read_declare_write () 
{
   INT1 c; 
   
   fprintf (p_cs_extrn, "# line %d \"%s\"\n", yylineno, cs_u1infile); 
   do{
      c = read_line_until (buffer, p_cs_i1dec_until,0); 
      fprintf (p_cs_extrn, "%s\n", buffer); 
   }while (c != 0); 
}


/*
 * This routine will create all output files
 * and will write the headers
 */

VOID cs_creat_outfiles () 
{
   FILE *pp; 
   UINT1 u1date[20]; 
   
   pp = popen ("date | cut -c5-20", "r"); 
   fscanf (pp, "%[^\n]", u1date); 
   pclose (pp); 
   
   if (open_file (&yyin, cs_u1infile, "r") ==NULL) exit (1); 
   if (open_file (&p_cs_command, MODECRE_FILE, "w") ==NULL) exit (1); 

	/***** #define INDMODE_FILE mmi_indmodes.c.c in mmist.h *****/

   if (open_file (&p_cs_mode, INDMODE_FILE, "w") ==NULL) exit (1); 
   if (open_file (&p_cs_cmdcre, CMDCRE_FILE, "w") ==NULL) exit (1); 
   if (open_file (&p_cs_action, ACTION_FILE, "w") ==NULL) exit (1); 
   if (open_file (&p_cs_syntax, SYNTAX_FILE, "w") == NULL) exit (1); 
   if (open_file (&p_cs_grpid, GRPID_FILE, "w") == NULL) exit (1); 
   if (open_file (&p_cs_privilege, PRIVILEGE_FILE, "w") == NULL) exit (1); 
   if (open_file (&p_cs_help, HELP_FILE, "w") == NULL) exit (1); 
   if (open_file (&p_cs_cxt_help, CXT_HELP_FILE, "w") == NULL) exit (1);
   if (open_file (&p_cs_extrn, EXTERN_FILE, "w") == NULL) exit (1);
   if (open_file (&p_cs_dyn_help, DYN_HELP_FILE, "w+") == NULL) exit (1);
 
   fprintf (p_cs_extrn, HEADER, cs_u1outfile, "cmdscan", "MMI-cmdscan", u1date, "");
   fprintf (p_cs_extrn, TO_BE_EXTERN);
   fprintf (p_cs_command, "%s", "#define PROMPT_STR 0\n#define PROMPT_FN 1\n"); 
   fprintf (p_cs_cmdcre, "VOID mmi_cmd_creat (VOID) \n{\n"); 
   fprintf (p_cs_command, TOKEN_TYPE_STRUCT);
   fprintf (p_cs_action, ACTION_PROC); 
}


/*
 * This routine will close all out files
 *
 * if error this will unlink all outfiles
 */
VOID cs_close_outfiles () 
{
   UINT1 u1tem[500]; 
  
   if (u1cm_all_defined_flag == FALSE) {
      strcpy ((INT1 *)u1cm_cmd_group, "ALL");
      cs_init_grp_write ();
      cs_close_grp_write ();
   }

   if (u1cm_mode_flag) {
      cs_write_add_mode (); 
   }
   fprintf (p_cs_command, "\n} \n"); 
   fprintf (p_cs_cmdcre, "\n} \n"); 
	fprintf(p_cs_mode, "\nUNUSED_PARAM(i);\n} \n");
   fprintf (p_cs_action, ACTION_END); 
   i4ts_parsendflag = TRUE; 

   fclose (p_cs_command); 
   fclose (p_cs_cmdcre); 
   fclose (p_cs_mode); 
   fclose (p_cs_action); 
   fclose (p_cs_syntax); 
   fclose (p_cs_grpid); 
   fclose (p_cs_privilege); 
   fclose (p_cs_help);
   fclose (p_cs_cxt_help);
   fclose (yyin); 
   fclose (p_cs_extrn);
   fclose (p_cs_dyn_help);
   if (!i4ts_errno) {
      sprintf ((INT1 *)u1tem, "cat %s %s %s %s %s %s %s %s %s %s > %s", EXTERN_FILE, SYNTAX_FILE, GRPID_FILE, PRIVILEGE_FILE, HELP_FILE, CXT_HELP_FILE,  MODECRE_FILE, INDMODE_FILE, CMDCRE_FILE, ACTION_FILE, cs_u1outfile); 
      system ((INT1 *)u1tem); 
   }
   unlink (MODECRE_FILE); 
   unlink (INDMODE_FILE); 
   unlink (CMDCRE_FILE); 
   unlink (ACTION_FILE); 
   unlink (HELP_FILE);
   unlink (CXT_HELP_FILE);
   unlink (SYNTAX_FILE); 
   unlink (GRPID_FILE); 
   unlink (PRIVILEGE_FILE); 
   unlink (EXTERN_FILE); 
   if (i4ts_errno) {
      exit (1);
   }
}


INT4 cs_put_itin_grp_list (p_i1grp_name) 
INT1 *p_i1grp_name; 
{
   if (i2grp_count >= MAX_GRP_NAME) {
      print_valid_error (19); 
      cs_close_outfiles ();
      exit (1); 
   }   
   if (strlen (p_i1grp_name) >= MAX_LGT_GRP) {
      print_valid_error (20); 
   }
   if (strcmp (p_i1grp_name, "ALL") == 0) {
      u1cm_all_defined_flag = TRUE; 
   }
   strcpy (cs_i1grp_name_list[i2grp_count], p_i1grp_name); 
   i2grp_count++; 
	if(mmi_mode_num > 0) 
	    fprintf(p_cs_mode, "\nUNUSED_PARAM(i);\n} \n");
        fprintf(p_cs_mode, "\nVOID mmi_%s_cmd_creat(VOID);\n",p_i1grp_name);
	fprintf(p_cs_mode,"\nVOID mmi_%s_cmd_creat(VOID)\n{\nINT2 i = 0;\n#define MMI_%s_ACTNUM_START %d\n",p_i1grp_name, p_i1grp_name, cs_i4case_count);
	fprintf(p_cs_cmdcre,"   mmi_%s_cmd_creat();\n",p_i1grp_name);
	mmi_mode_num++;
}


INT4 cs_search_grp_name (p_i1grp_name) 
INT1 *p_i1grp_name; 
{
   INT2 i; 
   for (i=0; i<i2grp_count; i++) {
      if (strcmp (cs_i1grp_name_list[i], p_i1grp_name) == 0) {
         return SUCCESS; 
      }
   }
   return FAILURE; 
}


main (argc, argv) 
INT4 argc; 
UINT1 *argv[]; 
{
   if (argc>=3) {
      if (!strcmp (argv[1], argv[2]) ) {
         printf ("%s \n", err_msg[29]); 
         exit (1); 
      } 
      strcpy (cs_u1infile, argv[1]); 
      strcpy (cs_u1outfile, argv[2]); 
      cs_creat_outfiles (argv[2]); 
   } 
   else
   print_usage (); 
   stack_top = 0; 
   while (!i4ts_parsendflag && !feof (yyin) ) {
      yyparse (); 
      if (!i4ts_parsendflag && !feof (yyin) )
      if (read_newline_eof () ==EOF) 
      break; 
   }
   cs_dyn_help_file_create();
   cs_close_outfiles (); 
} 



VOID cs_write_token_struct (i1type, i1func, i1value_func)
INT1 *i1type;
INT1 *i1func;
INT1 *i1value_func;
{
   fprintf (p_cs_command,"   {\"%s\", %s, %s},\n",i1type, i1func, i1value_func); 
}

static print_warning (i2Warn_no)
INT2 i2Warn_no;
{
   print_valid_warning (3);
}


check_mode_for_redefine (i1mode)
INT1 *i1mode;
{
  static INT1  i1mmimodes[30][MAX_MODE_LEN];
  static mode_count = 0;
  INT4 i;

  for (i=0; i<mode_count; i++) {
      if (strcmp(i1mmimodes[i],i1mode) == 0) {
         print_valid_error (25);
         return;
      }
  }
  if (mode_count < 30) {
     if (strlen (i1mode) < MAX_MODE_LEN)
        strcpy (i1mmimodes[mode_count], i1mode);
     mode_count++;
  }
}

static INT1 mmi_token_defs[36][20] = {
    "integer",
    "string", 
    "mac_addr", 
    "num_str",
    "quote_str", 
    "hex_str", 
    "mode_str",
    "random_str",
    "ip_addr",
    "float",
    "ucast_addr",
    "mcast_addr",
    "ip6_addr",
    "ucast_mac",
    "mcast_mac",
    "short",
    "ip_mask",
    "systime",
    "iftype",
    "ifnum",
    "iface_list",
    "port_list",
    "lucast_addr",
    "tftp_url",
    "flash_url",
    "ifXtype",
    "oui",
    "integer64",
    "sftp_url",
    "negative",
    "cust_url",
    "ip_cisco_mask",
    "vlan_vfi_id",
    "ipiftype",
    "dns_host_name",
    "ifxnum"
};
static INT4 max_token_def=36;

enter_valtok_defined (i1valtok)
INT1 *i1valtok;
{
   INT4 i;
   if (strlen (i1valtok) >=  20) 
      return;

   for (i=0; i<max_token_def; i++) {
       if (strcmp (mmi_token_defs[i], i1valtok) == 0) {
          return;
       }
   }
   strcpy (mmi_token_defs[max_token_def], i1valtok);
   max_token_def++;
}

INT4 check_valtok_defined (i1valtok)
INT1 *i1valtok;
{

   INT4 i;
    for (i=0; i< max_token_def; i++) {
       if (strcmp (mmi_token_defs[i], i1valtok) == 0) {
          return i;
       }
    }
    print_valid_error (26);
}

/*
* Converts pi1Str from string to short integer Value
*/

UINT2 cs_atoi (pi1Str, i2Base)
INT1  *pi1Str;
INT2  i2Base;
{
    UINT2               u2PrevVal = 0;
    UINT2               u2CurrVal = 0;
    INT1                i1Char;

    for (; *pi1Str ; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
           i1Char = *pi1Str - '0';
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
           i1Char = *pi1Str - 'a' + 10;
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
           i1Char = *pi1Str - 'A' + 10;
        else
            return u2CurrVal;

        u2CurrVal = (u2CurrVal * i2Base) + i1Char;

        if (((u2CurrVal - i1Char ) /i2Base ) != u2PrevVal)
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }
        if ((i1Char != 0) && (u2CurrVal <= u2PrevVal))
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }

        u2PrevVal = u2CurrVal;
    }
    return u2CurrVal;
}

/*
* Converts pi1Str from string to integer Value
*/

UINT4 cs_atol (pi1Str, i2Base)
INT1  *pi1Str;
INT2  i2Base;
{
    UINT4               u4PrevVal = 0;
    UINT4               u4CurrVal = 0;
    INT1                i1Char;

    for (; *pi1Str ; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
           i1Char = *pi1Str - '0';
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
           i1Char = *pi1Str - 'a' + 10;
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
           i1Char = *pi1Str - 'A' + 10;
        else
            return u4CurrVal;

        u4CurrVal = (u4CurrVal * i2Base) + i1Char;

        if (((u4CurrVal - i1Char ) /i2Base ) != u4PrevVal)
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }
        if ((i1Char != 0) && (u4CurrVal <= u4PrevVal))
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }

        u4PrevVal = u4CurrVal;
    }
    return u4CurrVal;
}

/*
* Converts pi1Str from string to signed integer Value
*/

INT4 cs_satol (pi1Str, i2Base)
INT1  *pi1Str;
INT2  i2Base;
{
    INT4                i4PrevVal = 0;
    INT4                i4CurrVal = 0;
    INT1                i1Char;

    for (; *pi1Str ; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
           i1Char = *pi1Str - '0';
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
           i1Char = *pi1Str - 'a' + 10;
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
           i1Char = *pi1Str - 'A' + 10;
        else
            return i4CurrVal;

        i4CurrVal = (i4CurrVal * i2Base) + i1Char;

        if (((i4CurrVal - i1Char ) /i2Base ) != i4PrevVal)
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }
        if ((i1Char != 0) && (i4CurrVal <= i4PrevVal))
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }

        i4PrevVal = i4CurrVal;
    }
    return i4CurrVal;
}

/*
* Converts pi1Str from string to float Value
*/

DBL8
cs_atof (INT1 *pi1Str)
{
    DBL8               d8Power   = 1.0;    /* float */
    DBL8               d8CurrVal = 0.0;    /* float */
    DBL8               d8PrevVal = 0.0;    /* float */

    /* Converts ascii to float */

    for (d8CurrVal = 0.0; isdigit(*pi1Str); pi1Str++)
    {
        d8CurrVal = (d8CurrVal * 10.0) + ((*pi1Str) - '0');

        if (((d8CurrVal - ((*pi1Str) - '0')) / 10) != d8PrevVal)
        {
            printf ("\n Wrong Value Specified");
            yyerror ();
        }

        d8PrevVal = d8CurrVal;
    }

    if (*pi1Str == '.')
        pi1Str++;

    for (d8Power = 1.0; isdigit(*pi1Str); pi1Str++)
    {
        d8CurrVal = (d8CurrVal * 10.0) + ((*pi1Str) - '0');
        d8Power = d8Power * 10.0;

        if (d8Power >= 1000000)    /* restricts precision value to 6 digits */
            break;
    }

    return (d8CurrVal / d8Power);
}

/*
* Parses thru Range values and writes in src/clicmds.c file
*/

VOID cs_parse_integer (pi1Str)
INT1 *pi1Str;
{
   UINT4      u4MinRange   = -1 ;
   UINT4      u4MaxRange   = -1 ;
   INT2       i2Base       = 10;
   INT1       *pi1TempStr  = NULL;
   INT1       *pi1Temp     = NULL;

   pi1Temp = pi1Str;
   pi1TempStr = pi1Str;

   while (*pi1TempStr++)
   {
       /* Discards whitespaces between the Range Values */

       if (isspace (*pi1TempStr) )
           CLI_DISCARD_SPACE (pi1TempStr);

       if (*pi1TempStr == ')') /* Indicating the end of maximum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           u4MaxRange = cs_atol (pi1Str, i2Base);
           break;
       }

       if (*pi1TempStr == '-') /* Indicating the end of minimum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           u4MinRange = cs_atol (pi1Str, i2Base);
           i2Base = 10;
           pi1Temp = pi1Str;
           continue;
       }

       if ((!isdigit (*pi1TempStr)) && (isxdigit (*pi1TempStr))
            && (i2Base != 16))
           yyerror ();

       if (!(isxdigit (*pi1TempStr)) && 
            !((*pi1TempStr == 'x') || (*pi1TempStr == 'X')))
           yyerror ();

       if (*pi1TempStr == 'x' || *pi1TempStr == 'X')
       {
           pi1Temp--;
           i2Base = 16;
           continue;
       }

       *pi1Temp++ = *pi1TempStr;
   }

   if (u4MinRange > u4MaxRange)
       yyerror();

   fprintf (p_cs_mode, "\"3\", %u, %u, \"<%u-%u>\" ), ", u4MinRange,
            u4MaxRange, u4MinRange, u4MaxRange);

   return ;
}

/*
* Parses thru Range values and writes in src/clicmds.c file
*/

VOID cs_parse_sinteger (pi1Str)
INT1 *pi1Str;
{
   INT4       i4MinRange   = -1 ;
   INT4       i4MaxRange   = -1 ;
   INT2       i2Base       = 10;
   INT1       *pi1TempStr  = NULL;
   INT1       *pi1Temp     = NULL;
   INT1       i1MinFlag    = 0;
   INT1       i1SetFlag    = 0;
   INT1       i1MaxFlag    = 0;

   pi1Temp = pi1Str;
   pi1TempStr = pi1Str;

   while (*pi1TempStr++)
   {
       /* Discards whitespaces between the Range Values */

       if (isspace (*pi1TempStr) )
           CLI_DISCARD_SPACE (pi1TempStr);

       if (*pi1TempStr == ')') /* Indicating the end of maximum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           i4MaxRange = cs_satol (pi1Str, i2Base);
           if (i1MaxFlag == 1)
           {
               i4MaxRange = (i4MaxRange * -1);
           }
           break;
       }

       /* Indicating the minmum value is negative value */
       if (*pi1TempStr == '-' && i1MinFlag == 0)
       {
           i1MinFlag = 1;
           continue;
       }
       /* Indicating the end of minimum value for range, 
           while parsing the input string.*/
       if (*pi1TempStr == '-' && i1MinFlag == 1 && i1MaxFlag == 0
           && i1SetFlag == 0) 
       {
           *pi1Temp = '\0';
           i4MinRange = cs_satol (pi1Str, i2Base);
           i4MinRange = (i4MinRange * -1);
           i2Base = 10;
           pi1Temp = pi1Str;
           i1SetFlag = 1;
           continue;
       }
       /*Check if max value is a negative value*/
       if (*pi1TempStr == '-' && i1MaxFlag == 0 && i1SetFlag == 1)
       {
           i1MaxFlag = 1;
           continue;
       }

       if ((!isdigit (*pi1TempStr)) && (isxdigit (*pi1TempStr))
            && (i2Base != 16))
           yyerror ();

       if (!(isxdigit (*pi1TempStr)) && 
            !((*pi1TempStr == 'x') || (*pi1TempStr == 'X')))
           yyerror ();

       if (*pi1TempStr == 'x' || *pi1TempStr == 'X')
       {
           pi1Temp--;
           i2Base = 16;
           continue;
       }

       *pi1Temp++ = *pi1TempStr;
   }

   if (i4MinRange > i4MaxRange)
       yyerror();

   fprintf (p_cs_mode, "\"3\", %d, %d, \"<%d-%d>\" ), ", i4MinRange,
            i4MaxRange, i4MinRange, i4MaxRange);
   return ;
}


/*
* Parses thru short int range values and writes in src/clicmds.c file
*/

VOID cs_parse_short (pi1Str)
INT1 *pi1Str;
{
   UINT2      u2MinRange   = -1 ;
   UINT2      u2MaxRange   = -1 ;
   INT2       i2Base       = 10;
   INT1       *pi1TempStr  = NULL;
   INT1       *pi1Temp     = NULL;

   pi1Temp = pi1Str;
   pi1TempStr = pi1Str;

   while (*pi1TempStr++)
   {
       /* Discards whitespaces between the Range Values */

       if (isspace (*pi1TempStr) )
           CLI_DISCARD_SPACE (pi1TempStr);

       if (*pi1TempStr == ')') /* Indicating the end of maximum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           u2MaxRange = cs_atoi (pi1Str, i2Base);
           break;
       }

       if (*pi1TempStr == '-') /* Indicating the end of minimum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           u2MinRange = cs_atoi (pi1Str, i2Base);
           i2Base = 10;
           pi1Temp = pi1Str;
           continue;
       }

       if ((!isdigit (*pi1TempStr)) && (isxdigit (*pi1TempStr))
            && (i2Base != 16))
           yyerror ();

       if (!(isxdigit (*pi1TempStr)) && 
            !((*pi1TempStr == 'x') || (*pi1TempStr == 'X')))
           yyerror ();

       if (*pi1TempStr == 'x' || *pi1TempStr == 'X')
       {
           pi1Temp--;
           i2Base = 16;
           continue;
       }

       *pi1Temp++ = *pi1TempStr;
   }

   if (u2MinRange > u2MaxRange)
       yyerror();

   fprintf (p_cs_mode, "\"3\", %u, %u, \"<%u-%u>\" ), ", u2MinRange,
            u2MaxRange, u2MinRange, u2MaxRange);

   return ;
}

/*
* Parses thru Range values of token type FLOAT and writes in src/clicmds.c file
*/

VOID cs_parse_float (pi1Str)
INT1 *pi1Str;
{
   DBL8         d8MinRange    = -1.0;
   DBL8         d8MaxRange    = -1.0;
   INT1         *pi1TempStr   = NULL;
   INT1         *pi1Temp      = NULL;

   pi1Temp = pi1Str;
   pi1TempStr = pi1Str;

   while (*pi1TempStr++)
   {
       /* Discards whitespaces between the Range Values */

       if (isspace (*pi1TempStr) )
           CLI_DISCARD_SPACE (pi1TempStr);

       if (*pi1TempStr == ')') /* Indicating the end of maximum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           d8MaxRange = cs_atof (pi1Str);
           break;
       }

       if (*pi1TempStr == '-') /* Indicating the end of minimum value                                            for range, while parsing the input string.*/
       {
           *pi1Temp = '\0';
           d8MinRange = cs_atof (pi1Str);
           pi1Temp = pi1Str;
           continue;
       }

       if (!(isdigit (*pi1TempStr)) && *pi1TempStr != '.')
           yyerror ();

       *pi1Temp++ = *pi1TempStr;
   }

   if ((d8MinRange > d8MaxRange))
       yyerror();

   if (d8MinRange != -1) 
   {
       fprintf (p_cs_mode, "\"3\", %f, %f, \"<%f-%f>\" ), ", d8MinRange,
                d8MaxRange, d8MinRange, d8MaxRange);
   }

   return ;
}

/*
* Parses thru the given string token length and writes in src/clicmds.c file
*/

VOID cs_parse_string (pi1Str)
INT1 *pi1Str;
{
   UINT4        u4StrLen      = 0;
   INT1         *pi1TempStr   = NULL;
   INT1         *pi1Temp      = NULL;

   pi1Temp = pi1Str;
   pi1TempStr = pi1Str;

   while (*pi1TempStr++)
   {
       /* Discards whitespaces between the braces */

       if (isspace (*pi1TempStr) )
           CLI_DISCARD_SPACE (pi1TempStr);

       if (*pi1TempStr == ')') /* Indicating the end the
                                * string length value
                                */
       {
           *pi1Temp = '\0';
           u4StrLen = cs_atol (pi1Str, 10);
           break;
       }

       if (!isdigit (*pi1TempStr))
           yyerror ();

       *pi1Temp++ = *pi1TempStr;
   }

   if (u4StrLen <= 0)
       yyerror();

   fprintf (p_cs_mode, "\"2\", %u, \"<%u>\" ), ", u4StrLen, u4StrLen);

   return ;
}

VOID cs_read_no_cxt_help_write ()
{
    INT1 c;

    skip_white_input ();
    do{
        c = read_line_until (buffer, p_cs_i1cxthelp_until, 1);
        if ((char *) (void *) mmistrstr (buffer, "\"") != NULL) {
            print_valid_error (12);
        }
    }while (c != 0);
}

