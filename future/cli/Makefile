
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : Makefile                                      |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 05/07/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : The makefile for builing the cli              |
# |                                                                          |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | 05/07/2001 | Creation of makefile                              |
# +--------------------------------------------------------------------------+


# The make.h for CLI which specifies the options for 
# building the Router.
include ../LR/make.h
include ../LR/make.rule
include ./make.h

#object module list

CLI_OBJS = \
           $(CLI_OBJD)/clicmds.o \
           $(CLI_OBJD)/cliutils.o \
           ${CLI_OBJD}/climain.o \
           ${CLI_OBJD}/clieval.o \
           ${CLI_OBJD}/clipswd.o \
           ${CLI_OBJD}/cliadcmd.o \
           ${CLI_OBJD}/climode.o \
           ${CLI_OBJD}/clichmod.o \
           ${CLI_OBJD}/clihelp.o \
           ${CLI_OBJD}/clicmdcr.o \
           ${CLI_OBJD}/clilex.o \
           ${CLI_OBJD}/clicxhlp.o \
           ${CLI_OBJD}/clicxutl.o \
		   ${CLI_OBJD}/clicxdyn.o \
           ${CLI_OBJD}/cliparse.o\
           ${CLI_OBJD}/clihist.o \
           ${CLI_OBJD}/clilxsup.o \
           ${CLI_OBJD}/clisktif.o \
           ${CLI_OBJD}/cligrps.o \
           $(CLI_OBJD)/clitrggr.o \
           $(CLI_OBJD)/clisz.o


ifeq (${TARGET_OS}, OS_VXWORKS)
CLI_OBJS += \
           ${CLI_OBJD}/clivxworks.o
else
CLI_OBJS += \
           ${CLI_OBJD}/cliport.o
endif

ifeq (${SSH}, YES)
CLI_OBJS += \
           ${CLI_OBJD}/clisshif.o
endif

ifeq (${RMGR}, YES)
CLI_OBJS += ${CLI_OBJD}/clirm.o
endif

ifeq (${PAM_AUTH}, YES)
ifeq ("",$(findstring DPAM_AUTH_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
CLI_OBJS += ${CLI_OBJD}/clipamldap.o
endif
endif

###########################################
#             Final Object              ### 
###########################################
GENERATE=NO
ifneq (${TARGET_OS}, OS_VXWORKS)
ifneq (${TARGET_OS}, OS_QNX)

GENERATE=YES


endif
endif
# FOR NATIVE COMPILATION ON QNX 
ifeq (${QNXHOST}, NATIVE)
GENERATE=YES
endif
ifeq (${GENERATE},YES)
CLIDEFS_DIR := ${shell if test -d ../clidefs; then echo 1; else echo 0; fi }

#all : MAKEALL ${CLI_EXED}/FutureCLI.o

#MAKEALL : CMDSCAN CLICMDS
all : 
	$(MAKE) CMDSCAN 
	$(MAKE) CLIDEFS 
	$(MAKE) CLIPLTFM
	$(MAKE) CLICMDS
	$(MAKE) ${CLI_EXED}/FutureCLI.o
endif


${CLI_EXED}/FutureCLI.o : obj ${CLI_OBJS}
	${LD} ${LD_FLAGS} -o ${CLI_EXED}/FutureCLI.o ${CLI_OBJS}

obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) obj
endif

CMDSCAN:
	$(MAKE) -w -C $(CLI_BASE_DIR)/tools

CLICMDS: $(CLI_SRCD)/clicmds.c

CLIDEFS:
	@if  [ "${CLIDEFS_DIR}" -ne "1" ]; then \
	echo "cli requires clidefs. Pls. checkout future/clidefs before building CLI";\
	echo "Exitting...";\
	exit 1;\
	fi
ifeq (${CLI}, YES)
	   $(MAKE) -w -C $(BASE_DIR)/clidefs
endif

CLIPLTFM:
ifeq (${CLI}, YES)
	$(MAKE) -w -C $(CLI_NPTD)
endif

ifeq (${GENERATE},YES)
$(CLI_SRCD)/clicmds.c:../clidefs/clicmds.def tools/exe/cmdscan 
	 tools/exe/cmdscan ../clidefs/clicmds.def ./src/clicmds.c
	 mv clicxdyn.h ./inc/
endif
	

###########################################
#        object module dependancy       ### 
###########################################

${CLI_OBJD}/climain.o : \
                          ${CLI_SRCD}/climain.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/climain.c

${CLI_OBJD}/cliport.o : \
                          ${CLI_SRCD}/cliport.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/cliport.c

${CLI_OBJD}/clivxworks.o : \
                          ${CLI_SRCD}/clivxworks.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clivxworks.c

${CLI_OBJD}/clieval.o : \
                          ${CLI_SRCD}/clieval.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clieval.c

${CLI_OBJD}/clipswd.o : \
                          ${CLI_SRCD}/clipswd.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clipswd.c

${CLI_OBJD}/climode.o : \
                          ${CLI_SRCD}/climode.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/climode.c

${CLI_OBJD}/clichmod.o : \
                          ${CLI_SRCD}/clichmod.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clichmod.c

${CLI_OBJD}/clihelp.o : \
                          ${CLI_SRCD}/clihelp.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/clihelp.c

${CLI_OBJD}/clicxhlp.o : \
                          ${CLI_SRCD}/clicxhlp.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/clicxhlp.c

${CLI_OBJD}/clicxutl.o : \
                          ${CLI_SRCD}/clicxutl.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/clicxutl.c

${CLI_OBJD}/clicxdyn.o : \
                          ${CLI_SRCD}/clicxdyn.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/clicxdyn.c
${CLI_OBJD}/cliadcmd.o : \
                          ${CLI_SRCD}/cliadcmd.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/cliadcmd.c

${CLI_OBJD}/clicmdcr.o : \
                          ${CLI_SRCD}/clicmdcr.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clicmdcr.c

${CLI_OBJD}/clilex.o : \
                          ${CLI_SRCD}/clilex.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clilex.c

${CLI_OBJD}/clilxsup.o : \
                          ${CLI_SRCD}/clilxsup.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clilxsup.c

${CLI_OBJD}/cliparse.o : \
                          ${CLI_SRCD}/cliparse.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/cliparse.c

${CLI_OBJD}/clihist.o : \
                          ${CLI_SRCD}/clihist.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clihist.c

${CLI_OBJD}/clisktif.o : \
                          ${CLI_SRCD}/clisktif.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clisktif.c

${CLI_OBJD}/clisshif.o : \
                          ${CLI_SRCD}/clisshif.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clisshif.c
 
${CLI_OBJD}/clirm.o : \
                          ${CLI_SRCD}/clirm.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clirm.c
 
${CLI_OBJD}/cligrps.o : \
                          ${CLI_SRCD}/cligrps.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/cligrps.c
   
$(CLI_OBJD)/clicmds.o : \
                        $(CLI_SRCD)/clicmds.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS}  -o $@ ${CLI_SRCD}/clicmds.c

$(CLI_OBJD)/cliutils.o : \
                        $(CLI_SRCD)/cliutils.c \
                        $(CLI_INCD)/cliutils.h \
                        ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/cliutils.c

$(CLI_OBJD)/clitrggr.o : \
		$(CLI_SRCD)/clitrggr.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clitrggr.c

$(CLI_OBJD)/clisz.o : \
		$(CLI_SRCD)/clisz.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clisz.c

ifeq (${PAM_AUTH}, YES)
ifeq ("",$(findstring DPAM_AUTH_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
${CLI_OBJD}/clipamldap.o : \
	${CLI_SRCD}/clipamldap.c ${PROJECT_DEPENDENCIES}
	${CC} ${C_FLAGS} -o $@ ${CLI_SRCD}/clipamldap.c
	ar rcs ${CLI_OBJD}/libldap.a $(CLI_OBJD)/clipamldap.o
	$(CC) -fPIC -c ${CLI_SRCD}/clipamldap.c -o $(CLI_OBJD)/clipamldap.o $(C_FLAGS)
	$(CC) -shared -o ${CLI_OBJD}/pamldapnew.so $(CLI_OBJD)/clipamldap.o -lldap -llber -lcrypt
endif
endif

clean   :
	@echo Cleaning files from ${CLI_OBJD}
ifeq (${GENERATE}, YES)
	$(RM) $(RM_FLAGS) $(CLI_BASE_DIR)/tools/obj/*.o
ifeq (${CLI}, YES)
	$(MAKE) -w -C $(BASE_DIR)/clidefs clean
	$(MAKE) -w -C $(CLI_NPTD) clean
endif
endif
	$(RM) $(RM_FLAGS) $(CLI_OBJD)/*.o
ifeq (${PAM_AUTH}, YES)
	$(RM) $(RM_FLAGS) $(CLI_OBJD)/*.a
	$(RM) $(RM_FLAGS) $(CLI_OBJD)/*.so
endif

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/cli.tgz  -T ${BASE_DIR}/cli/FILES.NEW;\
        cd ${CUR_PWD};

