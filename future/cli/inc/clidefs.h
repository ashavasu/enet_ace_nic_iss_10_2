/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clidefs.h,v 1.19 2017/12/20 11:06:49 siva Exp $
 *
 * Description: contains macro definitions
 *
 *******************************************************************/
#ifndef __CLIDEFS_H__
#define __CLIDEFS_H__

#define CLI_EOF                  2
#define CLI_RDONLY               OSIX_FILE_RO
#define CLI_WRONLY               OSIX_FILE_WO
/* For password encryption */
#define MMI_SPEC_CHAR                  '>'

/* For Password Configuration */
#define CLI_NUM_CHAR_START 48
#define CLI_NUM_CHAR_END   57
#define CLI_UPPER_CHAR_START  65
#define CLI_UPPER_CHAR_END    90
#define CLI_LOWER_CHAR_START  97
#define CLI_LOWER_CHAR_END    122
#define CLI_SPL_CHAR_1     33    /* ! */
#define CLI_SPL_CHAR_2     64    /* @ */
#define CLI_SPL_CHAR_3     35    /* # */
#define CLI_SPL_CHAR_4     36    /* $ */
#define CLI_SPL_CHAR_5     37    /* % */
#define CLI_SPL_CHAR_6     94    /* ^ */
#define CLI_SPL_CHAR_7     38    /* & */
#define CLI_SPL_CHAR_8     42    /* * */
#define CLI_SPL_CHAR_9     40    /* ( */
#define CLI_SPL_CHAR_10    41    /* ) */

#define CLI_GET_IFACE_TYPE       1
#define CLI_GET_IFACE_POS        2
#define CLI_GET_IFACE_STATUS     3

#define MAX_BUF_LEN                    64
#define MAX_IFC_LEN                4

/* Mode names */
#define CLI_MODE_ROOT     "ROOT"
#define CLI_MODE_ENET     "ENET"  
#define CLI_MODE_AAL5     "AAL5"  
#define CLI_MODE_ATMVC    "ATMVC" 
#define CLI_MODE_PPP     "PPP"  
#define CLI_MODE_IPOA    "IPOA"
#define CLI_MODE_L2TP    "l2tp"
#define CLI_MODE_L3IPVLAN   "L3IPVLAN"

/* cli prompt prefix */
#define CLI_IFC_ROOT            "/"
#define CLI_IFC_ENET      "eth"
#define CLI_IFC_AAL5      "aal5"  
#define CLI_IFC_ATMVC     "atmvc"
#define CLI_IFC_PPP       "ppp"
#define CLI_IFC_IPOA   "ipoa"
#define CLI_IFC_VLAN            "vlan"
#define CLI_IFC_ZNB             "znb"


/*cli command names*/

#define CLI_CONF_TERM_CMD (CONST CHR1 *) "configure terminal"


/* 
 * The following definitions denotes starting position of corresponding 
 * interface type for cli prompt suffix. For ethernet interface, user will 
 * create ethN where N is interface number. After displaying the interface type
 * (in this case "eth"),the interface number is extracted from the corresponding
 * position (in this case 4th position). So the cli prompt will be "eth0". 
 *
 * See the table below for examples.
 * Interface   User Input   CLI Prefix   Suffix Position     CLI Prompt
 * ---------   ----------   ------------  ---------------     ----------
 * Ethernet    eth0         CLI_IFC_ENET        4             eth0 
 * ATM     aal50        CLI_IFC_AAL5        5             aal50
 * ATMVC       atmvc0       CLI_IFC_ATMVC       6             atmvc0 
 * PPP         ppp0         CLI_IFC_PPP         4             ppp0
 * IPOA        ipoa0        CLI_IFC_IPOA        5             ipoa0
 *
 */

/* position = (Suffix pos - 1) because C Array is 0-based index */
#define CLI_IFC_ENET_POS     3 
#define CLI_IFC_AAL5_POS     4
#define CLI_IFC_ATMVC_POS    5
#define CLI_IFC_PPP_POS      3
#define CLI_IFC_IPOA_POS      4
#define CLI_IFC_L3IPVLAN_POS            4


/* interface type constants */ 
#define CLI_CFA_ENET      6
#define CLI_CFA_AAL5      49 
#define CLI_CFA_ATMVC     134
#define CLI_CFA_PPP       23
#define CLI_CFA_IPOA      114
#define CLI_CFA_L3IPVLAN         136

/* ethernet type, mtu, encapsulation */ 
#define CLI_ENET_TYPE   "6"
/* aal5 type and mtu */ 
#define CLI_AAL5_TYPE   "49"
/* atmvc type, mtu and encapsulation */
#define CLI_ATMVC_TYPE   "134"
/* PPP link type */
#define CLI_PPP_TYPE      "23"
/* IPoA type */
#define CLI_IPOA_TYPE   "114"
#define CLI_L3IPVLAN_TYPE  "136"

#define CLI_UP            "1"
#define CLI_DOWN         "2"
#define CLI_TEST                "3"

#define CLI_START   "1"
#define CLI_SHUTDOWN  "2"

#define CLI_BEEP()             mmi_printf ("\7")

/* Macros related to 'help' option flag checks */
#define CLI_ISSET_HELP_DESC(x)          (x & CLI_HELP_DESC) ? TRUE : FALSE
#define CLI_ISSET_HELP_GRPNAME(x)       (x & CLI_HELP_GRPNAME) ? TRUE : FALSE

#define CLI_CALLBACK_FN  gaCliCallBack

#define CLI_MAX_SEND_RETRY 3


#define CLI_MAX_HELP_LINE_LEN 1000
#define CLI_MAX_HELP_TOKEN_NUM 100

#ifdef PAM_AUTH_WANTED
#define MAX_LDAP_PWD_PRIVIL  15
#define MIN_LDAP_PWD_PRIVIL   0
#define MAX_LDAP_NSLCD_LEN   50
#define MAX_LDAP_URI_LEN      4
#define MAX_LDAP_BASE_LEN     5
#define MAX_LDAP_BIND_LEN     7
#define LDAP_FILE_NAME       "/etc/nslcd.conf"

#define PAM_PROMPT_ECHO_OFF   1
#define PAM_PROMPT_ECHO_ON    2
#define PAM_ERROR_MSG         3
#define PAM_TEXT_INFO         4
#define PAM_CONV              5
#define PAM_MAX_NUM_MSG       32
#define PAM_SUCCESS           0

#endif
#endif /* __CLIDEFS_H__ */
