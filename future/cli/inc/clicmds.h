/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clicmds.h,v 1.141 2017/04/28 11:46:51 siva Exp $
 *
 * Description: contains the header file inclusion
 *
 *******************************************************************/
#ifndef  __CLICMDS_H__
#define  __CLICMDS_H__

#include "lr.h"

#include "cfa.h"
#include "vcm.h"
#include "fpam.h"

#include <stdarg.h>
#include "fssyslog.h"
#include "fssnmp.h"

#include "iss.h"
#include "issu.h"
#include "msr.h"
#include "utilipvx.h"
#ifdef EVB_WANTED
#include "vlnevcon.h"
#endif
#include "rmgr.h"
#include "fsutil.h"
#ifdef ARP_WANTED
#include "arpcli.h"
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED) 
#include "ipcli.h"
#endif
#ifdef NPAPI_TEST_WANTED
#include "npapicli.h"
#endif
#ifdef IP6_WANTED
#include "ipv6.h"
#include "ip6util.h"
#include "ip6cli.h"
#include "rtm6cli.h"
#endif

#ifdef IPSECv6_WANTED
#include "seccli.h"
#include "secv6cli.h"
#endif

#ifdef BGP_WANTED
#include "bgp4cli.h"
#include "bgp.h"
#endif

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#include "snpcli.h"
#include "snp.h"
#endif

#ifdef RMON_WANTED
#include "rmoncli.h"
#endif


#ifdef DSMON_WANTED
#include "dsmncli.h"
#endif

#ifdef RMON2_WANTED
#include "rmn2cli.h"
#endif

#ifdef DHCPC_WANTED
#include "dhcp.h"
#include "dhcpccli.h"
#endif

#ifdef DHCP6_CLNT_WANTED
#include "d6clcli.h"
#endif
#ifdef DHCP_SRV_WANTED
#include "dhcpcli.h"
#endif

#ifdef DHCP_RLY_WANTED
#include "dhrlcli.h"
#endif

#ifdef DHCP6_SRV_WANTED
#include "d6srcli.h"
#endif
#ifdef DHCP6_RLY_WANTED
#include "d6rlcli.h"
#endif

#ifdef LLDP_WANTED
#include "lldp.h"
#include "lldcli.h"
#endif

#ifdef EOAM_WANTED
#include "emcli.h"
#include "eoam.h"
#include "emfmcli.h"
#include "eoamfm.h"
#ifdef EOAM_TEST_WANTED
#include "emtstpt.h"
#endif
#endif

#ifdef ECFM_WANTED
#include "ecfmcli.h"
#include "ecfm.h"
#endif

#ifdef BEEP_SERVER_WANTED
#include "bpsrvcli.h"
#endif

#ifdef VCM_WANTED
#include "vcmcli.h"
#include "sispcli.h"
#endif

#ifdef SSL_WANTED
#include "httpssl.h"
#include "sslcli.h"
#endif

#ifdef SSH_WANTED
#include "sshcli.h"
#include "sshfs.h"
#endif

#ifdef RADIUS_WANTED
#include "radcli.h"
#include "radius.h"
#endif

#ifdef HOTSPOT2_WANTED
#include "hscli.h"
#endif

#ifdef TACACS_WANTED
#include "tpcli.h"
#include "tacacs.h"
#endif

#ifdef BRIDGE_WANTED
#include "bridge.h"
#endif

#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "pbbte.h"
#ifdef VLAN_WANTED
#include "vlancli.h"
#ifdef PB_WANTED
#include "vlnpbcli.h"
#endif
#ifdef EVB_WANTED
#include "vlnevcli.h"
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
#include "pbbcli.h"
#endif
#endif
#include "vlantrc.h"
#include "vlancons.h"
#include "l2iwf.h"
#include "garp.h"
#ifdef GARP_WANTED
#include "garpcli.h"
#endif
#endif
#ifdef MRP_WANTED
#include "mrp.h"
#include "mrpcli.h"
#endif
#ifdef PBBTE_WANTED
#include "pbtcli.h"
#endif
#ifdef ELPS_WANTED
#ifndef ECFM_WANTED 
/* When ECFM is present, this file will be include by ECFM module itself. */
#include "ecfm.h"
#endif
#include "elps.h"
#include "elpscli.h"
#endif
#ifdef PNAC_WANTED
#include "pnaccli.h"
#include "pnac.h"
#endif
#ifdef WPS_WANTED
#include "wpscli.h"
#include "wps.h"
#endif
#ifdef RSNA_WANTED
#include "rsnacli.h"
#include "rsna.h"
#endif
#ifdef ERPS_WANTED
#include "erpscli.h"
#include "erps.h"
#endif
#ifdef ESAT_WANTED
#include "esatcli.h"
#include "esat.h"
#endif
#ifdef Y1564_WANTED
#include "y1564cli.h"
#include "y1564.h"
#endif
#ifdef RFC2544_WANTED
#include "r2544cli.h"
#include "r2544.h"
#endif

#ifdef ISSMEM_WANTED
#include "issszcli.h"
#endif

#include "isscli.h"
#ifdef ISS_WANTED
#include "l4scli.h"
#ifdef SWC
#include "aclcxecli.h"
#else
#ifdef MRVLLS
#include "aclmrvlcli.h"
#else
#ifdef ISS_METRO_WANTED
#include "aclmcli.h"
#endif
#include "aclcli.h"
#endif
#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
#include "dscxecli.h"
#else
#ifdef MRVLLS
#include "qosmrvlscli.h"
#else
#include "dscli.h"
#endif
#endif
#endif
#endif

#ifdef QOSX_WANTED
#include "qosxcli.h"
#endif
#ifdef DCBX_WANTED
#include "dcbxcli.h"
#endif/*DCBX_WANTED*/
#ifdef CN_WANTED
#include "cn.h"
#include "cncli.h"
#endif /* CN_WANTED */
#ifdef RBRG_WANTED
#include "rbrgcli.h"
#endif
#ifdef SYSLOG_WANTED
#include "syslgcli.h"
#endif 

#ifdef OSPF_WANTED
#include "osdefn.h"
#include "ospfcli.h"
#endif

#ifdef TLM_WANTED
#include "tlmcli.h"
#include "tlm.h"
#endif

#ifdef OSPFTE_WANTED
#include "ospfte.h"
#include "ostecli.h"
#endif
#ifdef OSPF3_WANTED
#include "ospf3.h"
#include "o3defn.h"
#include "ospf3cli.h"
#endif

#ifdef VRRP_WANTED
#include "vrrpcli.h"
#endif


#ifdef RRD_WANTED
#include "rrdcli.h"
#include "rtm.h"
#endif
#ifdef IP_RTMTEST_WANTED
#include "rtmcli.h"
#endif
#ifdef TCP_WANTED
#include "fstcpcli.h"
#endif

#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
#include "pingcli.h"
#include "rmapcli.h"
#include "tcpcli.h"
#include "udpcli.h"
#include "tracecli.h"
#endif

#ifdef RIP6_WANTED
#include "rip6cli.h"
#endif

 #include "ripcli.h" 

#ifdef RM_WANTED
#include "rmcli.h"
#endif

#ifdef HB_WANTED
#include "hbcli.h"
#endif

#ifdef ISSU_WANTED
#include "issucli.h"
#endif
#ifdef ICCH_WANTED
#include "icchcli.h"
#endif

#include "fsbuddy.h"

#include "cli.h"             /* stored in future/inc */
#include "cligndfs.h"      
#include "cliport.h"
#include "cliconst.h"        
#include "climdcmd.h"       
#include "cliextn.h"
#include "cliproto.h"
#include "cliutils.h"        
#include "clidefs.h"         

#ifdef CFA_WANTED
#ifdef PPP_WANTED
#include "authsnmp.h"
#endif
#include "stdmaulw.h"
#include "stdethlw.h"
#include "cfacli.h"
#include "cfagen.h"
#include "ipdb.h"
#include "ipdbcli.h"
#include "l2ds.h"
#include "l2dscli.h"
#endif

#ifdef MPLS_WANTED
#include "mplscli.h"
#include "stdpwcli.h"
#include "l3vpncli.h"
#ifdef LSPP_WANTED
#include "lspp.h"
#include "lsppcli.h"
#include "lsppclig.h"
#endif
#endif
#ifdef RFC6374_WANTED
#include "r6374.h"
#include "r6374cli.h"
#endif

#ifdef BFD_WANTED
#include "bfd.h"
#include "bfdcli.h"
/*#include "bfdclig.h"*/
#endif

#ifdef LA_WANTED
#include "lacli.h"
#include "la.h"
#include "laconst.h"
#endif
#ifdef RSTP_WANTED
#include "stpcli.h"
#include "astconst.h"
#include "astdbg.h"
#include "rstp.h"
#ifdef MSTP_WANTED
#include "astmcons.h"
#endif
#endif  

#ifdef DVMRP_WANTED
#include "dvmrp.h"
#include "dpdef.h"
#include "dvmrpcli.h"
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
#include "pimcli.h"
#endif

#ifdef WEBNM_WANTED
#include "httpcli.h"
#endif

#ifdef DNS_WANTED
#include "dnscli.h"
#include "dns.h"
#endif

#ifdef IGMP_WANTED
#include "igmpcli.h"
#include "igmp.h"
#endif

#ifdef IGMPPRXY_WANTED
#include "igpcli.h"
#endif

#ifdef MLD_WANTED
#include "mldcli.h"
#include "mld.h"
#endif

#ifdef MRI_WANTED
#include "mricli.h"
#endif

#ifdef SNMP_3_WANTED
#include "snmp3cli.h"
#include "snmpcmn.h"
#endif

#ifdef MFWD_WANTED
#include "mfwdcli.h"
#endif

#ifdef MBSM_WANTED
#include "mbsmcli.h"
#endif

#ifdef NAT_WANTED
#include "nat.h"
#include "natcli.h"
#endif

#ifdef FIREWALL_WANTED
#include "firewall.h"
#include "fwlcli.h"
#endif

#ifdef SMOD_WANTED
#include "secidscli.h"
#endif

#ifdef POE_WANTED
#include "poecli.h"
#endif

#ifndef SWC
#ifdef KERNEL_WANTED
#include "kerncli.h"
#endif
#endif

#ifdef CUST_CLI_WANTED
#include "custcli.h"
#endif

#include "cust.h"

#ifdef EVCPRO_WANTED
#include "evcproglobcli.h"
#include "evcpro.h"
#include "uni.h"
#include "uniglobcli.h"
#endif

#ifdef LCM_WANTED
#include "lcmglobcli.h"
#include "lcm.h"
#endif

#ifdef ELMI_WANTED
#include "elmcli.h"
#include "elm.h"
#endif

#ifdef TTCP_WANTED
#include "ttcpcli.h"
#endif

#ifdef TAC_WANTED
#include "tac.h"
#include "taccli.h"
#endif

/* 25/11/2008 */
#ifdef SNTP_WANTED
#include "sntpcli.h"
#endif

#ifdef VPN_WANTED
#include "vpn.h"
#include "vpncli.h"
#endif

#ifdef IPSECv4_WANTED
#include "secv4cli.h"
#include "secv4.h"
#endif
#ifdef ISIS_WANTED
#include "iscli.h"
#endif
#ifdef PTP_WANTED
#include "ptp.h"
#include "ptpcli.h"
#endif
#ifdef SYNCE_WANTED
#include "synce.h"
#include "syncecli.h"
#endif
#ifdef CLKIWF_WANTED
#include "fsclk.h"
#include "clkiwcli.h"
#endif

#ifdef MSDP_WANTED
#include "msdpcli.h"
#endif

#include "tftpc.h"
#include "sftpc.h"

#include "tacacs.h"
#include "clisz.h"

#ifdef PPP_WANTED
#include "l2tp.h"
#include "ppp.h"
#include "pppcli.h"
#endif

#ifdef FIPS_WANTED
#include "fipscli.h"
#endif

#ifdef NPAPI_WANTED

#ifndef LINUXSIM_WANTED
#include "npdbgcli.h"
#endif

#endif
#ifdef MEF_WANTED
#include "fsmefcli.h"
#endif
#if (defined WLC_WANTED) || (defined WTP_WANTED)
#include "wsscfgcli.h"
#include "capwapcli.h"
#ifdef WSSUSER_WANTED
#include "usercli.h"
#endif
#endif

#ifdef RFMGMT_WANTED
#include "rfmcli.h"
#endif

#ifdef OPENFLOW_WANTED
#include "ofccli.h"
#endif
#ifdef OFC_TEST_SERVER_WANTED
#include "ofctscli.h"
#endif

#ifdef VXLAN_WANTED
#include "vxcli.h"
#endif

#ifdef FSB_WANTED
#include "fsbcli.h"
#endif
#endif   /* __CLICMDS_H__ */

