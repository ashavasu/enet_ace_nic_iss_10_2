/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliutils.h,v 1.21 2015/01/29 11:16:10 siva Exp $
 *
 * Description: This will have prototypes and Macros for Cli utilities 
 *
 *******************************************************************/
#ifndef __CLIUTILS_H__
#define __CLIUTILS_H__

#ifndef  __CLIMAIN_C__
extern tCliSpecialPriv               gCliSpecialPriv;
#else
tCliSpecialPriv               gCliSpecialPriv;
#endif

#define CLI_IP6_ADDRESS 4

INT4 CliGetCurModePromptStr (INT1 *);

extern UINT1 gau1PortBitMask[CLI_PORTS_PER_BYTE];

typedef struct t_SSH_SESSION
{
    UINT4 u4ClientAddr;
    UINT4 au4ClientIpAddr[CLI_IP6_ADDRESS];  /*Ipv6 client address*/
    UINT4 u4FailCount;
    tTmrAppTimer timerNode;
    UINT1 u1UseFlag;
    UINT1 u1Blocked;
    UINT1 u1TimerId;
    UINT1 u1Rsvd;
}tSshSession;

#ifdef L2RED_WANTED
/* list of strings that are allowed during bulk synch-up */
typedef struct {
 CHR1 *pCmdString;
}tCliFilterStringList;
#endif

PUBLIC tTimerListId gSshTimerLst;

/* give three times chance to enter. each time he will get 3 chances.
 *  so if 9 attempts failed, block him for an hour */
#define CLI_MAX_NUM_ATTACK 9
#define CLI_SSH_BLOCK_TIME_IN_SECONDS 3600

extern VOID CliSshTimerExpiryCallBkFun (VOID);
extern INT4 CliGetSessionAuth (UINT4 u4ClientAddr);
extern INT4 CliGetSessionAuthIpv6 (UINT4 *pu4ClientIpAddr);
extern INT2 CliGetSessionBlock (UINT4 u4ClientAddr);
extern VOID CliSshResetSessionAuth (UINT4 u4ClientAddr);

#define CLI_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              if (u2Port)\
              {\
               u2PortBytePos = (UINT2)(u2Port / CLI_PORTS_PER_BYTE);\
               u2PortBitPos  = (UINT2)(u2Port % CLI_PORTS_PER_BYTE);\
               if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
               au1PortArray[u2PortBytePos] =\
                             (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1PortBitMask[u2PortBitPos]);\
              }\
           }    

#define CLI_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / CLI_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % CLI_PORTS_PER_BYTE);\
      if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gau1PortBitMask[u2PortBitPos]) != 0) {\
           \
              u1Result = PORT_SUCCESS;\
           }\
           else {\
           \
              u1Result = PORT_FAILURE; \
           } \
        }


#endif  /* __CLIUTILS_H__ */
