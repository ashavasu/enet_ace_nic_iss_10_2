/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliconst.h,v 1.95 2016/10/03 10:34:41 siva Exp $
 *
 * Description:This will have the mode level typedefs 
 *             For constants                          
 *******************************************************************/
#ifndef __CLICONST_H__
#define __CLICONST_H__

/* utilities to handle dynamic memory allocation, free using Buddy memory */
#define CLI_BUDDY_ALLOC(size,type)           (type *) CliBuddyMalloc(size)
#define CLI_BUDDY_FREE(ptr)                  CliBuddyFree((UINT1 *) ptr)
#define CLI_BUDDY_CALLOC(n, s, t)            (t *) CliBuddyCalloc(n, s)

#define CLI_ALLOC_OUTPUT_MSG_MEM_BLOCK(pu1Block, type) \
    pu1Block = (type *) (MemAllocMemBlk ((tMemPoolId) gCliMaxOutputPoolId))

#define CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK(ppu1Block) \
   MemReleaseMemBlock((tMemPoolId)(gCliMaxOutputPoolId), (UINT1 *) ppu1Block)

#define CLI_ALLOC_AMB_MEM_BLOCK(pu1Block) \
    pu1Block = (tCliAmbVar *) (MemAllocMemBlk ((tMemPoolId) gCliAmbMemPoolId))

#define CLI_RELEASE_AMB_MEM_BLOCK(ppu1Block) \
    MemReleaseMemBlock((tMemPoolId)(gCliAmbMemPoolId), (UINT1 *) ppu1Block)

#define CLI_ALLOC_MAXLINE_MEM_BLOCK(pu1Block, type) \
    pu1Block = (type *) (MemAllocMemBlk ((tMemPoolId) gCliMaxLineMemPoolId))

#define CLI_ALLOC_HELPSTRING_MEM_BLOCK(pu1Block, type) \
   pu1Block = (type *) (MemAllocMemBlk ((tMemPoolId) gCliMaxHelpStrMemPoolId))

#define CLI_RELEASE_MAXLINE_MEM_BLOCK(ppu1Block) \
 MemReleaseMemBlock((tMemPoolId)(gCliMaxLineMemPoolId), (UINT1 *) ppu1Block) 

#define CLI_RELEASE_HELPSTRING_MEM_BLOCK(ppu1Block) \
    MemReleaseMemBlock((tMemPoolId)(gCliMaxHelpStrMemPoolId), (UINT1 *) ppu1Block)

#define CLI_ALLOC_USERGROUP_MEM_BLOCK(pu1Block, type) \
    pu1Block = (type *) (MemAllocMemBlk ((tMemPoolId) gCliUserGroupMemPoolId))

#define CLI_RELEASE_USERGROUP_MEM_BLOCK(ppu1Block) \
   MemReleaseMemBlock((tMemPoolId)(gCliUserGroupMemPoolId), (UINT1 *) ppu1Block)

#define CLI_MAX_AMB_MEM_BLOCKS                2  /* One for Application context
                                                    and other for the cli 
                                                    context sessions */
#define CLI_MAX_MEM_BLOCKS           CLI_MAX_SESSION_MEMORY / (MAX_LINE_LEN + 1)
#define CLI_MAX_USERGROUP_MEM_BLOCKS CLI_MAX_SESSION_MEMORY / (CLI_MAX_USERS_LINE_LEN + 1)
#define CLI_MAX_IF_TYPE_LEN                   20
#define CLI_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define CLI_TELNET_TASK_NAME    "CTS"
#define   CLI_TASK_NAME         "C"
#define   CLI_TASK_PRIORITY     200

/*Macro for command removal*/
#define CLI_REM_CMD       (CONST CHR1 *)(0xffffffff)

/* Macro for File length */
#define CLI_FILE_LEN    32
/* Macro for the redirection symbol postition*/
#define CLI_REDIR_TOK   2

/* Macros for dynamic cxt hlp */
#ifdef CFA_UNIQUE_INTF_NAME
#define MAX_DYN_IFTYPES 4
#else
#define MAX_DYN_IFTYPES 6
#endif
#define MAX_DYN_IFTYPE_HLP 100
#define MAX_DYN_IFTYPE_SIZE 20
#define MAX_IFNUM_SIZE 60
#define MAX_PWID_SIZE 50
#define MAX_LBLRANGE_SIZE 50
#define MAX_VRF_HLP 50
#define MAX_SWITCH_HLP 100
#define MAX_DYN_PARAMS 255 
/* Macros for cxt hlp */
#define CLI_OUTPUT_SPACE        35
#define CLI_OPTNLTKNS          210
#define CLI_ACTINDEX_VAL       999
#define CLI_CXT_HELP_CHR_LEN    30
#define CLI_INVALID_PRIVILEGE_ID 100
#define MAX_NO_OF_TOKENS_IN_MMI 202

/* To add a new CLI token, the following two macros should be increased */
#define MAX_TYPE_TOKEN          36

/* The following macros are used when setting bit position
 * in FS_UINT8 data structure */
#define CLI_LOW_BYTE_MAX_VAL    32
#define CLI_HIGH_BYTE_MAX_VAL   64

/* The following macros are used in validate function MmiVlanVfiIdValidate */
#define CLI_MIN_VLAN_VFI_ID     1
#define CLI_MAX_VLAN_VFI_ID     VLAN_DEV_MAX_VLAN_ID 


#define MAX_CHAR_IN_TOKEN       MAX_LINE_LEN
#define CLI_MAX_TOKEN_VAL_MEM   50
#define MAX_CMD_REC_PAR         20
#define MAX_INNER_RECUR         20 
#define MAX_CXT_TKN_OPTNS       50
/* This macro is tuned, as metro package modules has more show commands. */
#define MAX_AMBIG_CMD           700

#define MAX_MODE_LEVEL          16 
#define MAX_MODE_NAME           50 
#define MAX_COMMAND_LEN         50

#define PROMPT_STR               0
#define PROMPT_FUN               1
#define MMI_AND                  0   /* according to the tabscan **/
#define MMI_OR                   1
#define MMI_COND_END            -1
#define MMI_CF                  -2


#define MMI_EQ                   0
#define MMI_NEQ                  1
#define MMI_LESS                 2
#define MMI_GREAT                3
#define MMI_LE                   4
#define MMI_GE                   5

#define MAX_LINE_IN_PAGE         20 

#define MAX_KEY_BUF              300   /* DSL_ADD old value 200 */
#define MAX_HIST_COM             15
#define MAX_HIST_NUM             99
#define MAX_VARS_IN_TABLE        40
#define MMI_MAX_TABLE_SET_VARS   11

#define MAX_SCRIPT_REC_COUNT     2

#define LIST_TYPE                 1
#define TABLE_TYPE                2
#define TABLE_SET_TYPE            3
#define PURGE_TYPE                4
#define TABLE_SPACING             2

#define        MAX_PASSWORD_TRIES          gCliSessions.u1LoginAttempts
#define        CLI_DEFAULT_LOCK_OUT_TIME   30
#define        CLI_DEFAULT_LOGIN_ATTEMPT   3
#define        CLI_TMR_EXP_EVENT           (0x00000001)
#define        CLI_INPUT_ARRIVAL_EVENT     (0x00000002)
#define        CLI_TOUT_DURATION            10*SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#ifdef FALSE
#undef FALSE
#endif
#ifdef TRUE
#undef TRUE
#endif

#define CLI_INACTIVE    0
#define CLI_ACTIVE                1

/* Needed for TAB command status */
#define CMD_TAB_INACTIVE          0
#define CMD_TAB_INCOMPLETE        1
#define CMD_TAB_COMPLETE          2

/* Needed for Control characters */
#define CTRL_CHAR_START           27
#define CTRL_CHAR_PREPEND         79
#define CLI_SPACE                 32
#define CLI_ENTER                 10
#define CLI_UPARROW               65
#define CLI_DOWNARROW             66
#define CLI_RIGHTARROW            67
#define CLI_LEFTARROW             68
#define CLI_HOME                  49  /* Ascii Value */
#define CLI_HOME1                 50  /* Ascii Value */
#define CLI_END                   52  /* Ascii Value */
#define CLI_END1                  53  /* Ascii Value */
#define BACKSPACE                127  /* Ascii Value */
#define BACKSPACE1               '\b' 
#define CLI_CTRL_U                21  /*Ascii value of CTRL+U*/
#define TAB                      '\t'                    
#define PRE_PROCESS_CHAR          33  /* !->Ascii Value */
#define CLI_HELP                 '?'
#define CLI_COMMENT_CHAR         '#'
#define CLI_SCROLL_UP            "\33[KA\10A\33[1H"
#define CLI_SCROLL_UP1           "\33[H\33M"
#define CLI_SCROLL_DOWN          "\33[25;1H\33[K"
#define CLI_MOVE_CURSOR_TO_END   "\33[100B"

#define CLI_MAX_MODE              12

#define          TS_WILL          251    /* Indicates the desire to begin
                                         performing, or confirmation that
                                         you are now performing, the
                                         indicated option.                  */
#define          TS_WONT          252    /* Indicates the refusal to perform,
                                         or continue performing, the
                                         indicated option.                  */
#define          TS_DO            253    /* Indicates the request that the
                                         other party perform, or
                                         confirmation that you are expecting
                                         the other party to perform, the
                                         indicated option.                  */
#define          TS_DONT          254    /* Indicates the demand that the
                                         other party stop performing,
                                         or confirmation that you are no
                                         longer expecting the other party
                                         to perform, the indicated option.  */
#define          TS_IAC           255    /* Interpret As Command Escape     */
#define          TS_ECHO          1     /* Terminal - Echo option           */  
#define          TS_CHAR          3     /* Terminal - Char Mode option      */   
#define          TS_NAWS          31

#define     TS_ECHO_WONT_LEN          3
#define     TS_ECHO_WILL_LEN          3
#define     TS_CHAR_WONT_LEN          3
#define     TS_CHAR_WILL_LEN          3

#define     TS_NAWS_DO_LEN            3
#define     CLI_SUBNEG                250 /* Telnet Sub Negotiation (SB) Code */
#define     CLI_SBNEND                240 /* Sub Negotiation End (SE) Code */
#define     CLI_NAWS                  31  /* Telnet NAWS Code */
#define     CLI_SBNCOUNT              7

#define     CLI_WIN_SIZE              10  /* Size of Buf to contain Win Size */
#define     CLI_INDENT                4   /* Indent used for 'more' */

#define    CLI_UNENCRYPTED_PSWD        0
#define    CLI_ENCRYPTED_PSWD          7

#define    CLI_INVALID_CONTEXT_ID     (UINT4) (~0UL)

#define    CLI_MAX_PRIVILEGES         16
#define    CLI_DEFAULT_PRIVILEGE       1
#define    CLI_MAX_USERS_LINE_LEN     200
#define    CLI_MAX_GROUPS_LINE_LEN    200
#define    CLI_MAX_PRIVILEGES_LINE_LEN  100
#define    CLI_USER_FILE_NAME         FLASH_USERS ISS_USERS_FILE_NAME
#define    CLI_GROUP_FILE_NAME        FLASH_GROUPS ISS_GROUPS_FILE_NAME
#define    CLI_PRIVILEGES_FILE_NAME   FLASH_PRIVILEGES ISS_PRIVILEGES_FILE_NAME
#define    CLI_ROOT_USER              FpamGetDefaultRootUser()

#define    CLI_GUEST_USER             ISS_CUST_SYS_DEF_GUEST_USER
#define    CLI_GUEST_GRP              "guest"
#define    CLI_ROOT_UID               "0"
#define    CLI_GUEST_UID              "1"
#define    CLI_ROOT_MODE              "/"
#define    CLI_ROOT_PASSWD            FpamGetDefaultRootUsrDefaultPwd()
#define    CLI_GUEST_PASSWD           ISS_CUST_SYS_DEF_GUEST_PASSWD
#define    CLI_ROOT_GROUP             "root:0:root"
#define    CLI_GUEST_GROUP            "guest:1:guest"
#define    CLI_ROOT_PRIVILEGE         "15:admin123"
#define    CLI_ROOT_PRIVILEGE_ID      "15"
#define    CLI_ROOT_PRIVILEGE_PSWD    "admin123"
#define    CLI_DEF_MODE               "/"
#define    CLI_DEF_PASSWD             "Password123#"
#define    CLI_NO_EOF                 1
#define    CLI_MAX_GRPID_LEN          1000
#define    CLI_MAX_GRPNAME_LEN        12
#define    CLI_MAX_USERNAME_LEN       MAX_PASSWD_LEN 
#define    CLI_MAX_USERID_LEN         3
#define    CLI_MAX_USERMODE_LEN       MAX_PROMPT_LEN
#define    CLI_MAX_USERPASSWD_LEN     MAX_PASSWD_LEN
#define    CLI_MAX_ALIAS              20
#define    CLI_USER_STATUS_ENABLE     1
#define    CLI_USER_STATUS_DISABLE    0
#define    CLI_USER_STATUS_NA         -1 /* Status not available*/

#define    CLI_MIN(x,y)              ((x>y)?y:x)

/* Default Passwords in strict password rules enabled mode */
#define    CLI_STRICT_ROOT_PASSWD       "Admin123#"
#define    CLI_STRICT_GUEST_PASSWD      "Guest123#"
#define    CLI_STRICT_MBSM_CHASSIS_PASSWD "Chassis123#"


#define    CLI_EOC_DELIMITS           ";\n\t"
#define    CLI_EOT_DELIMITS           "; \n\t|"
#define    CLI_QUOTE_DELIMITS         "\"\n"
#define    CLI_SPACE_CHAR             ' '

#define    CLI_RANGE_PROMPT           "if-range"
#define    CLI_END_CMD                "end\n"
#define    CLI_INTERFACE_RANGE_CMD    "interface range"
#define    CLI_CONF_TERMINAL_CMD      "configure terminal"
#define    CLI_EXIT_CMD               "exit\n"

#define    CLI_IF_RANGE_MODE_ENABLE  1
#define    CLI_IF_RANGE_MODE_DISABLE 0

/* Length of max value of thegiven type */
#define    CLI_MAX_VAL_LEN(type)      (((sizeof(type)*8) / 3) + 1)

/* Buffer size of the CLI Pipe */
#define    CLI_PIPE_CHAR              '|'
#define    CLI_QUIT_CHAR              'q'
#define    CLI_MORE_OUT_NUMPAGE       16
#define    CLI_MORE_END               "\r\33[7m(END)\33[27m"
/*  Spaces needed to erase the previously displayed text */
#define    CLI_DEFAULT_BG             "\33[27m\r                       \r"

#define    CLI_MORE                   "more"
#define    CLI_GREP                   "grep"
/* Bitmap for Grep command options */
#define    CLI_GREP_INVERSE           0x01

/* Maximum no of allocations used to track */
#define    CLI_MAX_NO_OF_ALLOC        50

#define    CLI_DEFAULT_STACK_SIZE     15000 /* Cli Task Default stack size */

/* Bit Maps related to 'help' feature */
#define    CLI_HELP_DESC              0x01
#define    CLI_HELP_GRPNAME           0x02

#define    CLI_ZERO                   0

#define    CLI_REGULAR_MODE           1
#define    CLI_LINK_MODE              2

#define    CLI_MAX_GREP              10 /* Maximum number of recursive greps */

/* Values for get/set of default more flag option */
#define    CLI_MORE_DISABLE           OSIX_FALSE
#define    CLI_MORE_ENABLE            OSIX_TRUE

/* Used to display the current value of a parameter */
#define    CLI_DISPLAY                -1

#define    CLI_LOCAL_LOGIN             0x01

#define    CLI_CMDPRV_SET            0x01
#define    CLI_CMDPRV_RESET          0x02

#define CLI_ACCEPT_EVENT   1
#define CLI_REJECT_EVENT   2
#define CLI_RADIUS_TIMEOUT_EVENT 4
#define CLI_TACACS_TIMEOUT_EVENT 5

#define CLI_AMBIG_INTERFACE_TOKEN_FA   "fa "
#define CLI_AMBIG_MATCH_TOKEN_ONE      "interface"
#define CLI_AMBIG_MATCH_TOKEN_TWO      "iface"

#define CLI_AMBIG_INTERFACE_TOKEN_EX   "ex "
#define CLI_AMBIG_INTERFACE_TOKEN_EXT  "ext "
#define CLI_AMBIG_MATCH_TOKEN_THREE    "context"

#define CLI_MBSM_CHASSIS_USER     "chassisuser"
#define CLI_MBSM_CHASSIS_PASSWD   "chassis123"
#define CLI_MBSM_CHASSIS_USER_PRV  0

#define CLI_MATCH_EXACT_TOKEN       0
#define CLI_MATCH_SUBSET_TOKEN      1
#define CLI_MATCH_CXT_TOKEN         2
#define CLI_CXT_TKN_LIST_SIZE      ((MAX_NO_OF_TOKENS_IN_MMI / 8) + 1)
/* Timer macro to send a timer expiry event to stop the timer
 * in SLI module.
 */
#ifdef SLI_WANTED
#define CLI_SELECT_TIMER_EVENT      SELECT_TIMER_EVENT
#endif

#define CLI_CONSOLE_MASKED() (gu4CliConsoleBitMap & CLI_MASK_CONSOLE_SESSION)
#define CLI_TELNET_MASKED() (gu4CliConsoleBitMap & CLI_MASK_TELNET_SESSION)

#define CLI_IDLE_TIME_EXPIRY_MSG 0
#define CLI_FORCED_LOGOUT_MSG    1
#define CLI_USER_LOGIN_FAILED_MSG 2

typedef enum
{
      FALSE,
      TRUE
}
t_MMI_BOOLEAN;

typedef struct t_MMI_INDEX_DESC
{
     INT4    value;
     INT1   *val_desc;
}  t_MMI_INDEX_DESC;

PUBLIC VOID *FsCustMalloc (size_t);
PUBLIC VOID *FsCustCalloc (size_t , size_t);
PUBLIC VOID FsCustFree (void*);
PUBLIC VOID *FsCustRealloc (void *, size_t );
#define malloc(a) FsCustMalloc (a)
#define calloc(a,b) FsCustCalloc (a,b)
#define free(a) FsCustFree (a)


#endif   /* __CLICONST_H__ */
