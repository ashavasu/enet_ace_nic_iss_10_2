/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliport.h,v 1.49 2014/06/27 10:37:59 siva Exp $
 *
 * Description: contains portable definitions
 *
 *******************************************************************/
#ifndef __CLIPORT_H__
#define __CLIPORT_H__ 1

#define CLI_INPUT_STREAM            stdin
#define CLI_OUTPUT_STREAM           stdout
#define NP_CLI_TASK_NAME            "tShell"

#define   CLI_DEF_TASK_NAME         "CLIC"
#define   CLI_SSH_TASK_ID            gSshMainTaskId 
/* Events to be received from the call back function 
 * registered with select library
 */
/*Macros for CLI events*/
#define CLI_AFINET_CONN_EVENT            0x00000001
#define CLI_AFINET6_CONN_EVENT           0x00000002

#define CLI_SSH_AFINET_CONN_EVENT        0x00000004
#define CLI_SSH_AFINET6_CONN_EVENT       0x00000008
#define CLI_SSH_DISABLE_EVENT            0x00000080

#define CLI_RESUME_EVENT                 0x00000010
#define CLI_EVENT_WAIT_FLAGS             OSIX_WAIT

#define CLI_TELCONN_EVENT_WAIT_FLAGS     OSIX_WAIT
#define CLI_TELCONN_EVENT_WAIT_TIMEOUT   0
 
#define CLI_SSHCONN_EVENT_WAIT_FLAGS     OSIX_WAIT
#define CLI_SSHCONN_EVENT_WAIT_TIMEOUT   0
 
#define CLI_TELNET_ENABLE_EVENT          0x00000020
#define CLI_TELNET_DISABLE_EVENT         0x00000040
#define CLI_SSH_TIMER_EVENT              0x00000100

#define CLI_DEF_MAX_ROWS  25
#define CLI_DEF_MAX_COLS  80

#define CLI_AUDIT_MAX_FILE_NAME_LEN 64
#define CLI_AUDIT_MAX_COMMAND_LEN   128

/* Macros defining command length */
#define CLI_MAX_CMD_LENGTH 100

/* Buddy memory size should be calculated based on the following params
 * 1. 4K for Output message buffer.
 * 2. Number of buddy memory allocations per command execution
 * 3. Max buffer size required per buddy memory allocation
 * 4. Max recursive run script execution
 */

/* 4 is the number of buddy memory allocation per command execution */

#define CLI_MEM_SIZE_PER_COMMAN_EXEC ((16 * (MAX_LINE_LEN +1)) + \
                                      (MAX_NO_OF_TOKENS_IN_MMI * \
                                       CLI_MAX_TOKEN_VAL_MEM))

/* Max. Memory required for command execution between script recursions
 * will be in multiples of no. of recursion allowed in run script + 
 * size of buddy memory required per command execution
 */
/* Total buddy memory required */
#define CLI_MAX_BUDDY_MEMORY  \
   (MAX_CLI_OUTPUT_BUF_SIZE + \
    (CLI_MEM_SIZE_PER_COMMAN_EXEC * (MAX_SCRIPT_REC_COUNT + 1)) + \
    MAX_LINE_LEN)
    
/* The Max buddy memory per CLI session, must be given as multiples of 4 
 * as it is required by MemBuddyCreate 
 */
#define CLI_MAX_SESSION_MEMORY \
        ((CLI_MAX_BUDDY_MEMORY) + (4 - (CLI_MAX_BUDDY_MEMORY % 4)))

/* Multiplication factor of 20 is introduced to handle 64-bit processor 
 * memory requirements */
#define CLI_MAX_BUDDY_BLOCK_SIZE  CLI_MAX_SESSION_MEMORY * 20
#define CLI_MAX_HELP_BLOCK_SIZE  CLI_MAX_BUDDY_BLOCK_SIZE

/* 1 Block of CLI_MAX_SESSION_MEMORY mem for each CLI session*/
#define CLI_SES_NUM_BLOCKS      1  

#define CLI_MIN_SESSION_MEMORY  4     /* Min. Memory required 4 Byte */

/* CLI_POLL - which when enabled will scan the input at the console on 
 * polling mode. When the switch is disabled the console will be set
 * to blocking mode for efficiency. Blocking mode will not work for 
 * Single threaded OS (tmo). By default the switch is enabled 
 * (Polling Mode).
 */
#ifdef OS_TMO
#define CLI_POLL
#endif
#ifdef CLI_POLL
#define  CLI_POLL_SET_NONBLOCK(x,y,z)  CliSerialIoctl(x,y,z)
#define  CLI_POLL_DELAY(u4Delay)  OsixTskDelay(u4Delay)
#else
#define  CLI_POLL_SET_NONBLOCK(x,y,z)
#define  CLI_POLL_DELAY(u4Delay)
#endif

/* Ioctl Directives */

#define CLI_SET_NONBLOCK    1
#define CLI_ECHO_ENABLE     2
#define CLI_ECHO_DISABLE    3
#define CLI_SWAP_INPUT      4
#define CLI_SWAP_OUTPUT     5
#define CLI_CLEAR_SCREEN    6
#define CLI_FLUSH_OUTPUT    7
#define CLI_GET_ENV         8
#define CLI_SET_ENV         9

#define CLI_DEF_FORMAT     3
#define CLI_REL_TIME_FORMAT 4

#define CLI_SET_NONCANON    20

#define CLI_GET_WINSZ       21
#define CLI_SET_DEFAULT     22

#define CLI_SERIAL_MAX_MTU  200 

#define CLI_OS_FAILURE      (-1)
#define CLI_OS_SUCCESS      (0)

#define CLI_MSG_BUF          100

#define CLI_MAX_MODES        32
#define CLI_MODE_LEN         10

#define MAX_PASSWD_LEN       CLI_MAX_LEN_PASSWD 
#define MAX_USER_NAME_LEN    MAX_PASSWD_LEN 
#define MAX_USER_SUBSTITUTES 10 /* Number of recursive "su" possible */

#define MAX_NO_USER          12

#define MAX_RMT_CONN_REQ_PEND 5
#define CLI_LOGIN_PROMPT  "\rLogin: "
#define CLI_LOGIN_PROM_LEN  30                 

/* Size of Temporary Buffer to Store Control dat from Telnet */
#define MAX_TEL_CTRL_DATA   20

#define MIN_PRINTABLE_CHAR  32    /* ascii value of SPACE */
#define MAX_PRINTABLE_CHAR 126    /* ascii value of ~ */

#define IS_NON_PRINTABLE_CHAR(u1Input) \
((((u1Input < MIN_PRINTABLE_CHAR) || (u1Input > MAX_PRINTABLE_CHAR)) && \
 ((u1Input != '\n') && (u1Input != '\t') && (u1Input != '\r'))) ? 1 : 0)

#define IS_BACKSPACE_CHAR(u1Input) \
( ((u1Input == BACKSPACE) || (u1Input == BACKSPACE1)) ? 1 : 0)

#define IS_UNDO_KEY(u1Input) \
( ((u1Input == CLI_CTRL_U )) ? 1 : 0)

#define IS_CONTROL_CHAR(u1Input) \
( (u1Input == CTRL_CHAR_START) ? 1 : 0)

#define IS_TAB_CHAR(u1Input) ( (u1Input == TAB) ? 1 : 0)

#define IS_HELP_CHAR(u1Input) ( (u1Input == CLI_HELP) ? 1 : 0)

#define IS_PRE_PROCESS_CHAR(u1Input) ( (u1Input == PRE_PROCESS_CHAR) ? 1 : 0)

#define CLI_DISCARD_SPACE(x) while( CliIsDelimit(CliGetInput(x), CLI_SPACE_CHAR) ) 

/* Macros defining date parameters */
#define CLI_MAX_DATE_LENGTH 40
/* This structure is used by MMI and should not be changed when ported to
 * target platforms. It is used to fill the user details from NVRAM at 
 * system startup.
 */ 

typedef struct t_MMI_PASSWD
{
    INT1                i1user_name[MAX_USER_NAME_LEN];
    INT1                i1user_passwd[MAX_PASSWD_LEN];
    INT1                i1user_mode[MAX_PROMPT_LEN];
    INT1                i1user_write;
    INT1                i1user_purge;
    UINT2               u2reserved;
}
t_MMI_PASSWD;

#define CLI_MAX_CMDS_IN_GROUP   200

#endif /* __CLIPORT_H__ */
