/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliproto.h,v 1.93 2017/12/20 11:06:49 siva Exp $
 *
 * Description:Prototypes for CLI Module functions 
 *
 *******************************************************************/
#ifndef __CLI_PROTO_H__
#define __CLI_PROTO_H__ 
#ifdef SSH_WANTED
INT4                CliSshInit(VOID);
#endif
VOID                CliGetTaskName (tCliContext *);
tCliContext *       CliGetFreeContext (VOID);
tCliContext *       CliGetContext (VOID);
INT2                CliSessionsInit (VOID);
VOID                CliContextInit (tCliContext *pCliContext);
VOID                CliSerialInit (VOID);
VOID                CliTelnetInit (tCliContext *);
INT4                CliTelnetIoInit (tCliContext *);
VOID                CliContextSerialInit (tCliContext *);
VOID                CliMainProcess (tCliContext *);
VOID                CliContextTelnetInit (tCliContext *);
VOID                CliProcessCommand (tCliContext *);
VOID                CliSendEvntIncomingConnection PROTO ((INT4 i4SockFd));
VOID                CliSshSendEvntIncomingConnection PROTO ((INT4 i4SockFd));
UINT1               CliGetInput (tCliContext *);
VOID                TelnetTaskInit (VOID);
t_MMI_BOOLEAN       CliIsEndOfCommand (UINT1 );
UINT1               CliIsEndOfToken (UINT1 );
VOID                CliProcessOutOfBandData (tCliContext *, UINT1);
VOID                CliHandleControlChar (tCliContext *, UINT1 *, UINT2 *);
VOID                CliMultLineDisplay (tCliContext * pCliContext, 
      UINT2 u2PrevCmdLen, UINT2 *pu2CharIndex, 
      CONST UINT1 *pu1UsrCmd, 
      INT1 *pi1Prompt);
INT1 *              CliGetHistCommand (tCliContext *, UINT4 );
INT2                CliPreProcessCommand (tCliContext *, INT1 *);
VOID                CliHandleBackspace (INT1 *pi1Prompt, 
      tCliContext * pCliContext, UINT1 *pu1command, 
      UINT2 u2CmdLen);
INT1                CliGetNextToken (INT1 *, CONST CHR1 *, INT1**, INT1**);
VOID                CliInitMmiVal (tCliContext *);
INT1                CliGetLine (INT4, INT1 *);
INT4                mmi_first_tok_parse (tCliContext *, INT1 *, INT2 *);
VOID                mmi_print_cmdargs_afterparse (tCliContext *);
INT2                mmi_parse (tCliContext *, INT1 *, INT4 *, INT2 *, INT1 **, 
      INT1);
VOID                mmi_exit (VOID);
VOID                mmi_reenter (tCliContext *);
INT4                find_pos_in_int (FS_UINT8);
INT2                CliExecuteCommand (tCliContext *, CONST CHR1 *);
INT2                CliRestoreCxtFileInfo (tCliContext * pCliContext);
INT2                CliExecuteIfRangeCommand (tCliContext *, CONST CHR1 *);
VOID                mmi_lx_set_bit_pos (INT4, FS_UINT8 *);
INT1                mmi_gettoken (tCliContext *, INT1 *, INT1 **);
VOID                mmi_rearrange_user_token_types (tCliContext *);
INT4                integer_value_function (INT1 *);
INT4                MmiIpAddrValueFunction (INT1 *);
INT4                MmiIfTypeValueFunc (INT1 *);
INT4                MmiIfXTypeValueFunc (INT1 *);
INT4                short_value_function (INT1 *);
INT4                float_value_function (INT1 *);
INT4                negative_value_function (INT1 *);
INT4                mmi_lex_integer_check (INT1 *);
INT4                mmi_lex_short_check (INT1 *);
INT4                mmi_lex_float_check (INT1 *);
INT4                mmi_lex_negative_check (INT1 *);
INT4                mmi_macaddress_check (INT1 *);
INT4                mmi_string_check (INT1 *);
INT4                mmi_mode_string_check (INT1 *);
INT4                mmi_numstring_check (INT1 *);
INT4                mmi_tftp_url_check (INT1 *);
INT4                mmi_sftp_url_check (INT1 *);
INT4                mmi_flash_url_check (INT1 *);
INT4                mmi_cust_url_check (INT1 *);
INT4                mmi_quoted_string_check (INT1 *);
INT4                mmi_random_string_check (INT1 *);
INT4                MmiOuiCheck (INT1 *);
INT4                MmiInteger64Check (INT1 *);
INT4                MmiIpMaskCheck (INT1 *);
INT4    MmiIpCiscoMaskCheck (INT1 *);
INT4                MmiIpAddrCheck (INT1 *);
INT4                MmiUCastMACCheck (INT1 *);
INT4                MmiMCastMACCheck (INT1 *);
INT4                MmiTimeCheck (INT1 *);
INT4                MmiIfNumCheck (INT1 *);
INT4                MmiIfTypeCheck (INT1 *);
INT4                MmiIfXTypeCheck (INT1 *);
INT4                MmiPortListCheck (INT1 *);
INT4                MmiIfaceListCheck (INT1 *);
INT4                MmiUCastAddrCheck (INT1 *);
INT4                MmiLUCastAddrCheck (INT1 *);
INT4                MmiMCastAddrCheck (INT1 *);
INT4                MmiIp6AddrCheck (INT1 *);
INT4                MmiVlanVfiIdValidate (INT1 *);
INT4                MmiIpIfTypeCheck (INT1 *);
INT4                MmiHostNameCheck (INT1 *);
INT4                MmiIfxNumCheck (INT1 *);
INT4                mmi_hexstring_check (INT1 *);
VOID                mmi_get_type_token ( INT1 *, FS_UINT8 *);
VOID                mmi_echo (INT1 *);
VOID                mmi_set_echo (INT1 );
VOID                mmi_pause (INT1 *);
t_MMI_MODE_TREE    *mmicm_search_parent (CONST CHR1 *);
INT4                mmi_addmode (CONST CHR1 *, 
                                 CONST CHR1 *, 
                                 INT1,
     CONST CHR1 *,
     INT1 (*) (INT1 *, INT1 *),
                                 INT4 (*)(UINT1 *)
                                 );
VOID                mmi_display_mode_tree (CONST CHR1 *);
VOID                mmi_print_prompt (tCliContext *, INT1 *, UINT4);
VOID                mmicm_init_mode_to_root (tCliContext *);
VOID                mmi_goback_to_root (t_MMI_MODE_TREE *);
VOID                mmi_come_from_root (t_MMI_MODE_TREE *,
   INT1 *,INT1,INT1 *);
VOID                mmicm_copy_curconfig_to_temp (tCliContext *);
VOID                mmicm_copy_temp_to_curconfig (VOID);
VOID                mmicm_copy_curconfig_to_inter (tCliContext *);
VOID                mmicm_copy_inter_to_curconfig (tCliContext *);
VOID                mmicm_copy_root_to_temp (tCliContext *);
VOID                mmicm_copy_root_to_curconfig (tCliContext *);
VOID                mmi_copy_curconfig_to_save (tCliContext *, INT2);
INT4                mmicm_remote_exe (tCliContext *, INT1 *);
INT4                mmi_check_the_parent_of_child (t_MMI_MODE_TREE *, 
                                                   t_MMI_MODE_TREE *);
INT4                mmi_login (tCliContext *, INT1 *pi1User);
INT4                mmi_getpasswd (CHR1 *, INT1);
INT4                mmi_change_user_passwd (CHR1 *);
VOID                mmi_print_user_prompt (INT1 *, tCliContext *);
CHR1               *mmi_encript_passwd (CHR1 *);
VOID                cli_lock_console (VOID);
VOID                mmi_list_users(VOID);
VOID                mmi_current_user (VOID);
VOID                mmicmdlog_user_logout(tCliContext *, UINT1);
INT4                mmi_add_mode_cmd (CONST CHR1 *, t_MMI_CMD *, INT4);
INT4                mmi_add_grp_cmd (t_MMI_COMMAND_LIST *, t_MMI_CMD *);
INT4                mmi_add_grp_to_mode (CONST CHR1 *, t_MMI_COMMAND_LIST *);
t_MMI_CMD          *TOK (CONST CHR1 *, INT2);
t_MMI_CMD          *VALTOK (CONST CHR1 *, INT2, ...);
t_MMI_CMD          *create_return_node (INT4, INT2);

t_MMI_CMD          *GRP_OR (t_MMI_CMD *, ...); 
t_MMI_CMD          *GRP_OPT (INT2, INT2, ...); 
t_MMI_CMD          *GRP_REC (t_MMI_CMD *, ...); 
t_MMI_CMD          *GRP_CMD1 (t_MMI_CMD *, ...); 
t_MMI_CMD          *GRP_CMD (INT2, INT2, ...); 
VOID                mmi_print_history_buffer (VOID);
VOID                mmi_execute_history_command (tCliContext *, INT4);
VOID                mmi_AddUserCommandToHistory (tCliContext *, CONST CHR1 *);
VOID                mmi_help (INT1 *, UINT2);
INT4                mmi_addhelp (CONST CHR1 *, CONST CHR1 **, 
   CONST CHR1 **,CONST CHR1 **,CONST INT1 *, INT4);
INT4                findnewlines (CONST CHR1 *);

             /*--- address validation protos ---*/
INT4                mmi_convert (INT4, INT1 *);

             /*-- String manipulation protos --*/
INT4                mmi_new_strcpy (INT1 *, CONST INT1 *);
INT4                mmi_new_strcmp (CONST INT1 *, CONST INT1 *);
INT1                mmi_strcmp (INT1 *, INT1 *);
INT1                mmi_strncmp (INT1 *, INT1 *, INT4);
INT1                mmi_strcmpi (CONST INT1 *, CONST INT1 *);
INT1                mmi_strncmpi (CONST INT1 *, CONST INT1 *, UINT4);
INT1                mmi_strcmp_without_case (INT1 *, INT1 *);
CONST CHR1         *mmi_strstr (CONST CHR1 *, CONST CHR1 *);
INT4                mmi_substri (CONST CHR1 *, INT1 *);
INT4                mmi_atoi (UINT1 *);
INT4                mmi_atol (INT1 *, UINT4 *);
INT4                mmi_atos (INT1 *, UINT4 *);
INT4                mmi_atof (INT1 *, DBL8 *);
INT4                mmi_satoi (INT1 *, INT4 *);
INT4                mmi_low_atoi (INT1 *);
INT4                mmi_power1 (INT4, INT4);

             /*--- range validation protos ---*/
INT1                *mmi_check_integer_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_sinteger_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_float_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_IpAddress_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_IP6Address_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *MmiIfTypeCompleteFunc (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *MmiIfXTypeCompleteFunc (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *MmiIfNumTokenCheck (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *MmiIfaceListTokenCheck (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_MacAddress_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_UcastAddress_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_LUcastAddress_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_McastAddress_range (t_MMI_CMD *, INT1 *, INT2 *);
INT1                *mmi_check_String_length (t_MMI_CMD *, INT1 *, INT2 *);


VOID                mmi_cmd_creat (VOID);
VOID                mmi_mode_init (VOID);
INT1                mmi_action_procedure (tCliHandle, VOID **, UINT4);

INT4 cli_change_path (CONST CHR1 *pu1Path);

INT4 CliSerialIoInit (tCliContext *);
UINT1 CliSerialRead (tCliContext *);
INT4 CliSerialWrite(tCliContext *, CONST CHR1 *pOutMsg, UINT4 u4Length);
INT4 CliSerialIoctl(tCliContext *, INT4 Code, VOID *ptr);
INT4 IsEndofFile(tCliContext *);
VOID CliDisplayLogo (VOID);
VOID CliGetLoginPrompt(INT1 *);
VOID CliLogEvent(INT4 i4Code);
INT4 CliSetUsersInNvRam(t_MMI_PASSWD *);
INT4 CliGetUsersFromNvRam(t_MMI_PASSWD *);
VOID CliRemoveUserFromNvRam(INT1 *);

VOID CliGetSecondsSinceBase (tUtlTm UtlTm, UINT4 *pu4SysTimeSecs );
INT4 CliUtilUnlockUser (CHR1 *);
INT1 CliFpamSetMinPasswordLen(INT4);
INT1 CliFpamShowMinPasswordLen(VOID);
INT1 CliFpamShowPasswdMaxLifeTime PROTO ((VOID));
INT1 CliFpamSetPasswdMaxLifeTime PROTO ((UINT4 u4LifeTime));
INT1 CliFpamPasswdValidate PROTO ((UINT4 u4Flag, UINT4 u4Param));
UINT1 CliTelnetRead (tCliContext *);
INT4 CliTelnetWrite (tCliContext *, CONST CHR1 *, UINT4);
INT4 CliFileWrite (tCliContext *, CONST CHR1 *, UINT4);

INT4 CliTsDisableEchoMode (tCliContext *);
INT4 CliTsEnableEchoMode (tCliContext *);
INT4 CliTsEnableCharMode (tCliContext *);

INT4 CliTelnetIoctl (tCliContext *, INT4, VOID *);
VOID CliAcceptIncomingConnection PROTO ((INT4 i4SockDesc));
INT4 CliConnectPassiveSocket (VOID);
INT4 CliConnectPassiveV6Socket PROTO ((VOID));
INT2 cli_run_script ( CONST CHR1 *, CONST CHR1 * );
INT4 CliOpenFile (CONST CHR1 *pi1Filename, INT4 i4Flag);
INT2 CliCloseFile (INT4 i4FileFd);
UINT4 CliSockSend(INT4 i4OutputFd, CONST CHR1 *pMessage, UINT4 u4Len);
INT4 CliSockRecv(INT4 i4InputFd, UINT1 *u1Char, INT4 i4NumToRead);
INT1 *CliGetBangCmdFromHist (tCliContext *, INT1 *, INT1 *);
VOID CliHandleOutOfBandData (tCliContext * pCliContext);
INT4 CliTsEnableWinSzMode (tCliContext * pCliContext);
VOID CliClear (VOID);
INT2 CliSerialWinResize (tCliContext *, UINT1 *, INT2 , INT2 );
INT2 CliUserDel( CHR1 *);
VOID CliSplitGroupTokens( INT1 *, INT1 **, INT1 **, INT1 **);
VOID CliSplitUserTokens( INT1 *, INT1 **, INT1 **, INT1 **,  INT1 **, INT1 **);
INT2 cli_file_operation ( CONST CHR1 *, CONST CHR1 * );
INT1 CliGetGroupIndex( INT1 *, INT2 *);
INT1 CliAddUserToGroups( CHR1 *, INT1 *);
INT1 CliUserAdd( INT1 *, INT1 *, INT1 *, INT1 *, INT1 *);
INT1 CliGroupAdd( INT4 *, INT1 * );
INT1 CliGetUserIndex( CONST CHR1 *, INT2 *);
INT1 CliReadLineFromFile( INT4 , INT1 *, INT2 , INT2 *);
INT1 CliCheckFormat (INT1 *, INT4);
INT1 CliInitUsersGroups(VOID);
INT1 CliReadUsers(VOID);
INT1 CliReadPrivileges(VOID);
INT1 CliWriteUsers(VOID);
INT1 CliWritePrivileges(VOID);
INT1 CliReadGroups(VOID);
INT1 CliWriteGroups(VOID);
INT1 CliDeleteUserFromGroup (CONST CHR1 *, INT1 *);
INT2 CliChangePasswd (CONST CHR1 *, CHR1 *);
INT2 CliGetUserMode (CONST CHR1 *, INT1 *);
INT2 CliUserMod( INT1 *, INT1 *, INT1 *, INT1 *, CHR1 *);
INT2 CliHandleAlias (INT1 *, INT1 *);
INT2 CliAliasAdd (INT1 *, INT1 *);
INT2 CliAliasDel (INT1 *);
INT2 CliGroupDel(INT1 *);
INT2 CliHandleTab (tCliContext *, UINT1 *, UINT2 *);
INT2 CliCommandComplete (tCliContext *, UINT1 *, INT2, INT2 *, UINT4);
INT1 *CliStrSpn (CONST CHR1 *, INT1 *, UINT2 *);
INT2 CliInitUserMode (tCliContext *);
INT4 CliChmod PROTO ((tCliContext *pCliContext, CONST CHR1 *pPath));
INT2 CliIdleTimeout PROTO ((INT4 *pi4Timeout));
INT2 CliNoIdleTimeout PROTO ((VOID));
INT2 CliDisplayId PROTO ((VOID));
INT2 CliDisplayHistCmd PROTO ((tCliContext *pCliContext, UINT2 *pu2CharIndex, 
     UINT1 **ppu1UsrCmd, INT1 *pi1HistCmd, INT1 *pi1Prompt));
VOID CliAddInputToCmd PROTO ((tCliContext *pCliContext, UINT1 u1Input, 
     UINT1 *pu1UserCmd, UINT2 *pu2CharIndex));
VOID CliMultiLineErase PROTO ((tCliContext * pCliContext, UINT1 *pu1UsrCmd, 
        UINT2 u2CmdLen, INT1 *pi1Prompt));
t_MMI_BOOLEAN CliIsSameFile PROTO ((CONST CHR1 *pu1FileName1, 
   CONST CHR1 *pu1FileName2));
t_MMI_BOOLEAN CliIsRegularFile PROTO ((CONST CHR1 *pu1FileName));
INT4 CliHelp PROTO ((tCliContext *pCliContext, UINT1 *pu1UserCmd));
INT4 CliModeHelp PROTO ((tCliContext *pCliContext));
INT4 CliCommandHelp PROTO ((tCliContext *pCliContext));
INT4 CliTabComplete PROTO ((tCliContext *pCliContext, UINT1 *pu1UserCmd, 
  UINT1 *pu1LongestMatch, INT1 i1HelpFlag, UINT4 u4AmbiTknCnt));
INT4 CliHandleHelp PROTO ((tCliContext * pCliContext, UINT1 *pu1UserCmd));
INT4 CliModeComplete PROTO ((tCliContext * pCliContext, UINT1 *pu1UserCmd, 
  UINT1 *pu1LongestMatch, INT2 *pi2AmbigCnt));
INT4 CliGetModeLongestMatch PROTO ((tCliContext *pCliContext, 
   UINT1 *pu1UserCmd, UINT1 *pu1LongestMatch));
INT4 CliGetCommandStartIndex PROTO ((tCliContext *pCliContext, 
              UINT1 *pu1UserCmd, INT2 *pi2CmdStartIndex));
INT4 CliSplitPathAndCommand PROTO ((UINT1 *pu1UserCommand, INT2 *pu2CmdPos));
INT2 CliGetGroups PROTO ((CHR1 *pu1UserName, CHR1 *pu1Grps, UINT4 u4Len));
INT4 CliGetNextCommand ( INT1 *pi1UserCommand, INT1 **ppi1NextCommand);
VOID CliNpInit PROTO ((VOID));
INT4 CliCreatePipe PROTO ((tCliContext *pCliContext));
INT4 CliDeletePipe PROTO ((tCliContext *pCliContext));
INT4 CliClosePipe PROTO ((tCliContext *pCliContext));
INT4 CliPipeWrite PROTO ((tCliContext * pCliContext, CONST CHR1 * pOutMsg, 
   UINT4 u4Length));
INT4 CliProcessGrep PROTO ((tCliContext * pCliContext, UINT1 *pu1Buf));
INT4 CliGrep PROTO ((tCliContext * pCliContext, INT2 i2GrepIndex, 
     UINT1 *pu1LineBuf));
INT4 CliExtractStringFromQuotes PROTO ((UINT1 *pu1QuoteStr));
INT4 CliMorePrintf PROTO ((tCliContext *pCliContext, INT4 i4LineIndex));
INT2 CliGetPrevLineOffSet PROTO ((tCliContext *pCliContext, INT2 i2Index));
INT2 CliGetNextLineOffSet PROTO ((tCliContext *pCliContext, INT2 i2TopIndex));
VOID CliContextCleanup PROTO ((tCliContext *pCliContext));
VOID CliHandleWinSizeChange PROTO ((tCliContext * pCliContext));
INT2 CliFlushPipe PROTO ((tCliContext * pCliContext));
INT4 CliCheckPipeSyntax PROTO ((tCliContext *pCliContext, UINT1 *pu1Command, 
   INT2 *pi2ErrCode));
INT4 CliFlushOutputBuff PROTO ((tCliContext *pCliContext));
INT4 CliMoreHandleUpArrow PROTO ((tCliContext *pCliContext, INT4 i4LineIndex, 
  INT4 *pi4PageTopOffset));
INT4 CliMoreHandleDownArrow PROTO ((tCliContext *pCliContext, 
   INT4 *pi4LineIndex, INT1 *pi1RetChar));
INT1 CliMorePrompt PROTO ((VOID));

VOID CliMemTrcInit PROTO ((tCliContext *));
VOID CliGetMemTrcMemAlloc PROTO ((VOID));
VOID CliSetDisplayPrompt PROTO  ((tCliContext *));

UINT1 CliTelnetIsBreak PROTO ((tCliContext *));
INT4  CliChangeCmdGrp PROTO ((INT4 *pi4ActionNum, INT1 *pi1Command, 
   INT1 *pi1Group));
INT4 CliChangeUser PROTO ((INT1 *pi1User));
INT4 CliTimeoutExpiry PROTO ((tCliContext *pCliContext));

INT1 CliGetLastModeName PROTO ((INT1 *, INT1 **));
t_MMI_MODE_TREE *CliGetMode PROTO ((t_MMI_MODE_TREE *, 
                                    t_MMI_MODE_TREE **, CONST INT1 *));
INT4 CliCommandMatch PROTO ((CONST UINT1 *pCommand, UINT1 *pString));
INT4 CliCommandCompare PROTO ((CONST UINT1 *pCommand, UINT1 *pString, 
  INT1 i1SubsetFlag, UINT1 *pu1Tokencount));

INT4 CliCxtTokenMatch PROTO ((CONST UINT1 *pCommand, 
                              UINT1 *pString, UINT1 *pu1TokenCount));
VOID *CliBuddyMalloc PROTO ((UINT4 u4Size));

VOID *CliBuddyCalloc PROTO ((UINT4 u4NumBlocks, UINT4 u4Size));

VOID CliBuddyFree PROTO ((UINT1 *pu1Ptr));

INT4 CliContextMemInit PROTO ((tCliContext *));

VOID CliChangePipeBuf PROTO ((tCliContext *pCliContext));

VOID CliInitialisePipeBuf PROTO ((tCliContext *pCliContext));

VOID CliDestroySession PROTO ((tCliContext *pCliContext));

INT4 CliSetDefaultMoreFlag PROTO ((tCliHandle, INT1));
VOID CliDisplayPWD PROTO ((tCliHandle));

VOID CliEnable PROTO ((UINT1, VOID *));
VOID CliShowPrivilege PROTO ((VOID));
VOID CliConfigure PROTO ((VOID));
VOID CliShowLineInfo PROTO ((INT4 *));
VOID CliExecTimeoutShowRunningConfig PROTO ((tCliHandle CliHandle));

VOID CliListActiveUsers PROTO ((VOID));

VOID CliConfigPassword PROTO ((INT4 i4PrivilegeLevel, UINT1 u1EncryptionType, INT1 *pi1PswdStr));

VOID CliRemovePrivilege PROTO ((INT4 i4PrivilegeLevel));

VOID CliDisplayHelp PROTO ((INT1 *i1pStr, UINT2 u2OptFlag, BOOL1 bLockStatus));

/* Cli Context senstive help related Prototypes */
UINT4 CliCxHlpDisplayCxtHelp PROTO ((INT1 *i1pStr,INT1 i1SpaceFlag, INT1 l1HelpCountFlag));

PUBLIC VOID CliCxUtlGetCxtHlpFrmSyntax PROTO ((tCxtHelpParams * pCxtHelpParams,
                                               INT2 *pi2SyntaxFlag,
                                               INT1 i1SpaceFlag));

PUBLIC
VOID CliCxHlpParseCxtHlpString PROTO ((tCxtHelpParams *pCxtHelpParams,
                                        INT4 *pi4OptionalTokens,
                                        INT2 i2ActIndArrCnt));
PUBLIC
INT4 CliCxHlpParseEnteredCmd PROTO ((t_MMI_CMD *pParseCmd, 
                                     tCxtHelpParams *pCxtHlpParams,
                                     t_MMI_CMD **paRecurNodes,
                                     t_MMI_CMD **pRetAddr,
                                     t_MMI_CMD **pRecurToken,INT4 *pi4RecurFlag,
                                     INT4 *pi4RecurLoopCount,
                                     INT4 *pi4InnerRecurCmpltFlag,
                                     INT1 i1SpaceFlag));

PUBLIC INT4
CliCxHlpHandleRecurLoop PROTO ((tCxtHelpParams * pCxtHlpParams,t_MMI_CMD **paRecurNodes,
                        t_MMI_CMD **ppRecurTkn,t_MMI_CMD **ppRetToken,
                        t_MMI_CMD **ppSameLevel,t_MMI_CMD **ppToken,
                        INT4 *pi4RecurFlag,INT4 *pi4TknCnt,
                        INT4 *pi4RecurLoopCount,INT4 *pi4InnerRecurCmpltFlag,
                        INT1 i1SpaceFlag));

PUBLIC INT4 CliCxHlpParseOptionalTkns PROTO ((t_MMI_CMD * pCurrToken,
                                              UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM],
                                              t_MMI_CMD ** ppLastTkn,INT4 i4Cnt));
 
PUBLIC INT4 CliCxHlpParseNxtCmdTkns PROTO ((t_MMI_CMD * pCurrToken,
                                            UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM],
                                            t_MMI_CMD ** ppLastTkn,INT4 i4Cnt));

PUBLIC  INT2 
CliCxHlpFindNxtTknAftrParsing PROTO ((t_MMI_CMD *pRetToken,
                                      tCxtHelpParams *pCxtHelpParams,
                                      t_MMI_CMD **pTempNext1,
                                      t_MMI_CMD **pTempNext2,
                                      t_MMI_CMD **ppRecurToken,
                                      INT4 *pi4OptionalTokens,
                                      INT4 i4RecurFlag,
                                      INT4 i4InnerRecurCmpltFlag,
                                      INT2 i2ArrCnt,
                                      INT1 i1SpaceFlag));
PUBLIC t_MMI_COMMAND_LIST* 
CliCxHlpGetGrpFrmCmdStruct PROTO ((t_MMI_COMMAND_LIST *pTmpCmd,
                                   INT2 i2CurGrpCnt,INT2 i2TotGrp));

PUBLIC
VOID CliCxHlpFindNxtCmdTkns PROTO ((t_MMI_CMD *pNextCmd,
                                    t_MMI_CMD *pRecurToken,
                                    t_MMI_CMD **paRecurNodes,
                                    INT4 *pi4TempArray1,
                                    INT2 *pi2ArrCnt,
                                    INT4 i4RecurLoopCount,
                                    INT4 i4RecurFlag,
                                    INT1 i1SpaceFlag));
PUBLIC
VOID CliCxHlpFindOptionalTkns PROTO ((t_MMI_CMD *pOptToken,
                                      t_MMI_CMD *pRecurToken,
                                      t_MMI_CMD **paRecurNodes,
                                      INT4 *pi4TempArray2,
                                      INT2 *pi2ArrCnt,
                                      INT4 i4RecurLoopCount,
                                      INT4 i4RecurFlag,
                                      INT1 i1SpaceFlag));
PUBLIC VOID
CliCxUtlMakeLowerCaseToCmp PROTO ((UINT1 *pu1Token, CONST CHR1 *pc1KeyWord,
                                   UINT1 *pu1TempKeyWord));

INT2 CliCxUtlGetBitMaskPosition PROTO ((FS_UINT8 u8Check));

INT4 CliCxUtlSplitTokens PROTO ((INT1 *i1pTempStr,
                                 UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM]));

INT2 CliCxUtlGetGrpCnt PROTO (( t_MMI_COMMAND_LIST * pTmpCmd));

VOID CliCxUtlGetFirstTokenInCmd PROTO ((t_MMI_COMMAND_LIST *pCmdTkn,
                                        t_MMI_CMD **pRetFirstTkn));

PUBLIC VOID
CliCxUtlFindMatchInAllGrps PROTO ((t_MMI_HELP * p_thelp_ptr,
                                   tCxtHelpParams *pCxtHelpParams,
                                   INT2 * pi2SyntaxCheckFlag,
                                   INT1 i1SpaceFlag ));
PUBLIC VOID CliCxHlpFindMatchingOptionalTkns PROTO ((t_MMI_CMD * pCmd,
                                                     UINT1 *pu1Str,
                                                     INT4 *pi4Array,
                                                     INT2 *pi2ArrayCnt,
           INT4 i4RecurFlag,INT2 i2Loop));

PUBLIC VOID CliCxHlpFindMatchingNxtCmdTkns PROTO ((t_MMI_CMD * pCmd,
                                                     UINT1 *pu1Str,
                                                     INT4 *pi4Array,
                                                     INT2 *pi2ArrayCnt,
           INT4 i4RecurFlag,INT2 i2Loop));

PUBLIC VOID CliCxUtlPrintTkn PROTO ((UINT1 *pu1String));

PUBLIC VOID CliCxUtlPrintTknWithHlpString PROTO ((UINT1 *pu1String));

PUBLIC VOID 
CliCxUtlSortCxtHelpStrings PROTO ((UINT1 **ppu1HelpStr,INT2 i2HelpDescriptor));

PUBLIC VOID 
CliCxUtlGetFirstWord PROTO ((UINT1 *pu1String, UINT1 *pu1RetString));

PUBLIC VOID
CliCxUtlGetFirstTknHelp PROTO (( t_MMI_HELP * p_thelp_ptr ,
                                 tCxtHelpParams *pCxtHelpParams,
                                 INT4 *pi4CheckFlag));
PUBLIC
VOID CliCxDynGetDynamicHelp PROTO ((tCxtHelpParams *pCxtHelpParams,
                                  UINT1 *pu1TempHelpStr1));

/* For Dynamic Context sensitive help */
VOID CliCxDynVrfName (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynSwitchName (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynInterfaceType (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynIfNum (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynIfXNum (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynPwIdRange (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynL2vpLblRange (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynLdpLblRange (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynRsvpTeLblRange (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynStaticLblRange (tCxtHelpParams *pCxtHelpParams);
VOID CliCxDynPathOptNum (tCxtHelpParams *pCxtHelpParams);


/* --- Context sensitive help prototype ends ------- */


VOID CliLineConfigure PROTO ((UINT1 u1ConsoleFlag));
INT1 CliGetPromptStr PROTO ((t_MMI_MODE_TREE *pCurMode, INT1 *pi1PromptStr));

INT1 CliIsUserInPrivilege (INT1 *pi1UserName, INT1 i1CmdPrivilege);

INT2 CliCheckPrivilege (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegePswd);

VOID CliSplitPrivilegesTokens (INT1 *pi1Buf, INT1 **ppi1Privilege, INT1 **ppi1Pswd);

INT1 CliGetPrivilegeIndex (INT1 *pi1PrivilegePswd, INT1 *pi1PrivilegeIndex);

INT1 CliCheckPrivilegeIndex (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegeIndex);

INT2 CliCheckPrivilegePswd (INT1 i1PrivilegeIndex, INT1 *pi1PrivilegePswd);

INT2 CliPrivilegeAdd (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegePswd);

INT2 CliPrivilegeDel (INT1 i1PrivilegeIndex);

INT1 CliExecUserName (tCliUserInfo *CliUserInfo);


INT2 CliSetUserPrivilege (INT2 i2UserIndex, INT1 *pi1PrivilegeLevel);

INT1 CliSetSysTime (INT1 *, UINT4 *, UINT4, UINT4 *);
VOID CliShowClock (VOID);
INT1 CliExitConfigMode (VOID);

VOID 
CliClearLine  PROTO ((tCliHandle CliHandle, UINT1 u1SessionId));

VOID
CliDestroyLine (tCliContext *pCliContext);

VOID
CliClearAllLines PROTO ((tCliHandle CliHandle));

INT4 CliCmdPrivilege PROTO ((UINT1 u1CmdType, INT1 i1PrivilegeLevel, INT1 *pi1Mode, INT1 *pi1Cmd));

PUBLIC VOID CliRestoreConfig PROTO((UINT1 *pu1RunScriptfile));
/* API for getting the modes prompt string */
INT1 CliGetUserExecPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CliGetExecPrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CliGetConfigurePrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));
INT1 CliGetLinePrompt PROTO ((INT1 *pi1ModeName, INT1 *pi1DispStr));

INT1 CliHandlePasswordExpiry PROTO ((tCliContext *pCliContext, INT1 *pi1username));

INT1 CliHandleChangePassword PROTO ((tCliContext *pCliContext, INT1 *pi1username));
INT1 CliValidateUserPasswd PROTO ((INT1 *pi1UserName, INT1 *pi1NewPassword));

VOID CliHandleAuthFailure (tCliContext * , INT1 * );
VOID CliSetUserPriv ( tOsixTaskId , INT1);

#ifdef RADIUS_WANTED
VOID CliFreeRadiusMem (tRadInterface *);
INT1 CliRadiusAuthenticate (INT1 * , INT1 * , tOsixTaskId , INT1 *);
VOID CliHandleRadiusResponse (VOID *);
VOID CliSetRadiusUserPriv ( tRadInterface *);
#endif

#ifdef TACACS_WANTED
INT1 CliTacacsAuthenticate (INT1 * , INT1 * , tOsixTaskId, INT1 *);
INT4 CliHandleTacacsResponse (VOID *);
VOID CliFreeTacacsMem (tTacMsgOutput *);
#endif

#ifdef MBSM_WANTED
INT1 CliGetBootConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr);
INT1 CliMbsmCheckUserPasswd PROTO ((INT1 *pi1Name, INT1 *pi1Passwd));
#endif /* MBSM_WANTED */

VOID CliWaitForever PROTO ((tCliContext *pCliContext));

#ifdef SLI_WANTED
VOID ClosePassiveConnections (VOID);
#endif

VOID CliSendAuditLoginInfo PROTO ((tCliContext *pCliContext, INT4 i4LogFlag));

/* Functions for range Command */ 

INT4
CliGetSlotAndRange PROTO ((UINT1 *pu1PortRange,
                           UINT2 *pu2StartIndex, UINT2 *pu2StopIndex, 
                           UINT2 *pu2SlotNo));

INT4 CliGetRangeList PROTO((tPortList *portList));
    
VOID *CliMemAllocMemBlk PROTO((tMemPoolId PoolId, UINT4 u4Size));
INT4 CliMemReleaseMemBlock PROTO((tMemPoolId PoolId, UINT1 **ppu1Block));

/* Functions for Range commands End */

/* Functions for Customization in CLI */
INT4 CliUtilCallBack (UINT4  u4Event, tCliCallBackArgs *);

INT1        CliFindCksum (INT4, INT2, UINT4);
INT1
CliVerifyCheckSum (INT1 *, INT2, INT2,
                    UINT4, UINT2);

tCliContext        *CliGetApplicationContext (VOID);

/* Functions for command removal */
t_MMI_MODE_TREE    *CliGetModeFromName (INT1 *);

INT1                CliGetCommand (tCliContext *,t_MMI_CMD **, INT4 *,INT4);

INT4 CliUpdateSyntax (CHR1  *,INT1 *);

INT4 CliValidatePositionName (t_MMI_CMD *,INT1 *);

INT4 CliSearchOption(t_MMI_CMD **, t_MMI_CMD **,INT4,INT1 *);

VOID CliObtainCommandHelp(t_MMI_HELP **,INT4 ); 

VOID CliFreeOption(t_MMI_CMD **, INT4);

VOID CliModifyCmd(t_MMI_CMD **, t_MMI_CMD **, t_MMI_CMD **, INT4);

VOID CliModifyCmdPrvOptional(t_MMI_CMD **, t_MMI_CMD **, 
                             t_MMI_CMD **, INT4);
VOID CliRemoveAlternateOpts(t_MMI_CMD **, t_MMI_CMD **,t_MMI_CMD **);

VOID CliModifyPrevCmd(t_MMI_CMD **, t_MMI_CMD **, t_MMI_CMD **);

INT4 CliContextLock (VOID);

INT4 CliContextUnlock (VOID);

/* Function to check for password config */
INT4 CliChkStrictPasswdConfRules (INT1 *);

UINT1
*CliValidateSyslogMsg PROTO ((tCliContext *pCliContext, UINT1 *));

UINT1
CliCxUtlMatchIncompleteToken (UINT1 *, CONST UINT1 *);

INT2
CliGetCurrentContextIndex (VOID);

INT4 CliSshLock (VOID);

INT4 CliSshUnLock (VOID);
INT4 ISSGetUserPrivilegeForPAMUser(INT1 *i1username,UINT1 *u1Privil);
INT4 ISSGetUserPrivilegeForPAMUserUNIX(INT1 *i1username, UINT1 *u1Privil);
VOID
CliSplitPamPrivilegesTokens (INT1 *pi1Buf, INT1 **ppi1User, INT1 **ppi1Privilege);
INT4 ISSGetUserPrivilegeForPAMUserLDAP(INT1 *i1username,UINT1 *u1Privil);

#endif /* __CLI_PROTO_H__ */
