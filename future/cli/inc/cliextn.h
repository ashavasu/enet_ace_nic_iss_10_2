/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliextn.h,v 1.15 2017/12/20 11:06:49 siva Exp $
 *
 * Description: contains extern declarations
 *
 *******************************************************************/
#ifndef __CLI_EXTN_H__
#define __CLI_EXTN_H__ 

extern UINT1             gau1Modes[CLI_MAX_MODES][CLI_MODE_LEN];
extern UINT1             gau1Command[CLI_AUDIT_MAX_FILE_NAME_LEN + CLI_AUDIT_MAX_COMMAND_LEN + 1];
extern tMemPoolId          gCliCtxPendBuff;
extern tMemPoolId          gCliCtxOutputBuff;
extern tMemPoolId          gCliCtxOutputBuffArr;
#ifdef ISS_WANTED
extern UINT1        gu1IssLoginAuthMode;
extern UINT1        gu1IssPamDefaultPrivilege;
#endif
extern INT4                  gi4IssCliTlntSrvrPrimaryBindPort;
extern INT4                  gi4IssCliTlntSrvrAlternateBindPort;
extern UINT4                 gu4IssCliTlntSrvrBindAddr;
#ifndef SNMP_3_WANTED
extern INT1                  nmhGetFirstIndexIfTable PROTO ((INT4 *));
extern INT1                  nmhGetNextIndexIfTable PROTO ((INT4, INT4 *));
#endif
#endif /* __CLI_EXTN_H__ */
