/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cligndfs.h,v 1.14 2015/05/13 12:53:55 siva Exp $
 *
 * Description:This file has the constant definition for message buffer 
 *             index which defined in mmigenmsg.h
 *******************************************************************/
#ifndef  __CLIGNDFS_H__
#define  __CLIGNDFS_H__ 

#define  MMI_GEN_WELCOME                            0
#define  MMI_GEN_ERR_SNMP_NOT_RESPONDED             1
#define  MMI_GEN_ERR_TOO_BIG                        2
#define  MMI_GEN_ERR_END_OF_MIB                     3 
#define  MMI_GEN_ERR_NO_SUCH_VAR                    4
#define  MMI_GEN_ERR_BAD_VALUE                      5
#define  MMI_GEN_ERR_READ_ONLY                      6
#define  MMI_GEN_ERR_UNKNOWN_CODE                   7 
#define  MMI_GEN_ERR_CANT_TRANSLATE                 8
#define  MMI_GEN_ERR_NON_TRAP_RCVD                  9
#define  MMI_GEN_COLD_START_TRAP                   10
#define  MMI_GEN_WARM_START_TRAP                   11
#define  MMI_GEN_LINK_DOWN_TRAP                    12
#define  MMI_GEN_LINK_UP_TRAP                      13
#define  MMI_GEN_AUTH_FAIL_TRAP                    14 
#define  MMI_GEN_NEIGHBOUR_LOSS_TRAP               15
#define  MMI_GEN_ENT_SPECIFIC_TRAP                 16
#define  MMI_GEN_UNKNOWN_TRAP                      17
#define  MMI_GEN_ERR_NO_TRAPS_IN_Q                 18
#define  MMI_GEN_ERR_NOT_ALLOW_PURGE               19
#define  MMI_GEN_ERR_NOT_ALLOW_SET                 20 
#define  MMI_GEN_ERR_MALLOC                        21 
#define  MMI_GEN_OPERATION_SUCCESSFUL              22
#define  MMI_GEN_ERR_ERR_IN_DOTSTRING              23
#define  MMI_GEN_ERR_NO_ENTRIES_TO_DELETE          24
#define  MMI_GEN_WARN_IN_GET_VARIABLES             25
#define  MMI_GEN_ERR_VARBIND_ILLEGAL               26
#define  MMI_GEN_ERR_NO_ENTRIES_IN_TABLE           27
#define  MMI_GEN_ERR_PDU_NOT_SENT                  28
#define  MMI_GEN_ERR_PDU_NOT_RCVD                  29
#define  MMI_GEN_ERR_HIST_ERR                      30
#define  MMI_GEN_ERR_HIST_MODIFY                   31 
#define  MMI_GEN_ERR_HIST_WINDOW                   32
#define  MMI_GEN_ERR_TOKEN_LONG                    33
#define  MMI_GEN_ERR_IDLE_TIMER_EXPIRED            34
#define  MMI_GEN_ERR_HIST_COMMAND                  35
#define  MMI_GEN_ERR_EXTRA_TOKEN                   36
#define  MMI_GEN_ERR_RECUR_REPEAT                  37
#define  MMI_GEN_ERR_COMMAND_AMBIGUITY             38
#define  MMI_GEN_ERR_ILLEGAL_NO_TOKEN              39
#define  MMI_GEN_ERR_INCOMPLETE_COMMAND            40
#define  MMI_GEN_ERR_PASSWD_NOT_MATCH              41
#define  MMI_GEN_ERR_NO_SUCH_USER                  42
#define  MMI_GEN_MMI_LOGIN                         43
#define  MMI_GEN_REENTER_PASSWD                    45
#define  MMI_GEN_ERR_PASSWD_REENTER                46
#define  MMI_GEN_ERR_ADDUSER_PARAMETER             47
#define  MMI_GEN_ERR_USER_ALREADY_PRESENT          48 
#define  MMI_GEN_OLD_PASSWD                        49
#define  MMI_GEN_ERR_PASSWD_TEMP_FILE              50
#define  MMI_GEN_ERR_OLD_PASSWD_NOT_MATCH          51
#define  MMI_GEN_ERR_IOCTL                         52
#define  MMI_GEN_ERR_ALREADY_IN_SU                 53
#define  MMI_GEN_NEW_PASSWD                        54
#define  MMI_GEN_ERR_CHANGE_PASSWD_FAIL            55
#define  MMI_ROOT_PROMPT                           56
#define  MMI_USER_PROMPT                           57
#define  MMI_GEN_MORE                              58
#define  MMI_GEN_ERR_ADDRESS_VALIDATION_FAIL_NOPAR 59 
#define  MMI_GEN_ERR_INCOMPLETE_IFACE_NAME         60
#define  MMI_GEN_ERR_INVALID_IFACE_NUM             61 
#define  MMI_GEN_ERR_NO_SUCH_SER_IFACE             62
#define  MMI_GEN_ERR_NO_SUCH_IFACE                 63
#define  MMI_GEN_ERR_SER_IFACE_ONLY                64
#define  MMI_GEN_ERR_NO_SUCH_ETH_IFACE             65
#define  MMI_GEN_ERR_ADDRESS_VALIDATION_FAIL       66
#define  MMI_GEN_ERR_RECV_GEN_ERROR                67
#define  MMI_GEN_ERR_PERMISSION_DENIED             68
#define  MMI_GEN_ERR_NO_SUCH_MODE                  69 
#define  MMI_GEN_ERR_TOO_MANY_LEX_TYPES            70
#define  MMI_GEN_ERR_TYPE_NOT_SUPPORTED            71
#define  MMI_GEN_ERR_ILLEGAL_CONDITION_CHECK       72
#define  MMI_GEN_ERR_IN_FORMOCTET                  73
#define  MMI_GEN_ERR_IP_ADDR_NOT_SET               74
#define  MMI_GEN_ERR_GET_IN_ONE_VALUE              75
#define  MMI_GEN_ERR_COMMUNITY                     76 
#define  MMI_GEN_DEF_ROOT                          77 
#define  MMI_GEN_DEF_PASSWD                        78 
#define  MMI_GEN_TRAP_RECVD                        79
#define  MMI_GEN_ERR_IN_RECV_TRAP_PDU              80
#define  MMI_GEN_ERR_MORE_TABLE_SET                81
#define  MMI_GEN_TRAP_OVERFLOW                     82
#define  MMI_GEN_ERR_ILLEGAL_VAR_NAME              83
#define  MMI_GEN_ERR_MEMORY_ALLOCATION_FAILURE     84
#define  MMI_GEN_ERR_GETTING_INSTANCE              85
#define  MMI_GEN_ERR_SOCKET_OPEN_ERR               86
#define  MMI_GEN_ERR_SOCKET_BIND_ERR               87
#define MMI_RELEASE_CONSOLE                        88
#define  MMI_GEN_ERR_INVALID_COMMAND               89
#define  MMI_GEN_ERR_INVALID_RANGE                 90 
#define  MMI_GEN_ERR_INVALID_MAC_ADDR_FORMAT       91
#define  MMI_GEN_ERR_INVALID_STRING_LEN            92
#define  MMI_GEN_ERR_UNKNOWN_HOST                  93
#define  MMI_GEN_ERR_INVALID_UCAST_ADDR            94
#define  MMI_GEN_ERR_INVALID_MCAST_ADDR            95
#define  MMI_GEN_ERR_MAX_USER_SUBS                 96
#define  MMI_GEN_ERR_GREP_OVERFLOW                 97
#define  MMI_GEN_ERR_INVALID_LUCAST_ADDR           98
#define  MMI_GEN_ERR_INVALID_IFTYPE                99
#define  MMI_GEN_ERR_WRONG_IFTYPE_FOR_PORT        100
#define  MMI_GEN_ERR_INSUFFICIENT_USER_PRIV       101
#define  MMI_GEN_ERR_USER_PASSWD_EXCEEDED         102
#define  MMI_GEN_ERR_TOKEN_AMBIGUITY              103
#define  MMI_GEN_ERR_INVALID_USER_INPUT           104

#define MMI_LOGIN_SUCCESS  1
#define MMI_LOGIN_FAILURE  2


#endif   /* __CLIGNDFS_H__ */
