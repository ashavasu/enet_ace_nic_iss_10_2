/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: climib.h,v 1.3 2007/02/01 14:45:27 iss Exp $
 *
 * Description: Contains type defintions used by CLI module
 *
 *******************************************************************/
typedef struct t_MMI_VAR_DESC
{     
  INT1 *var_name; 
  INT4 type;       
  INT4 index;      
} t_MMI_VAR_DESC;     

typedef INT1 *mmi_local_string;
t_MMI_INDEX_DESC  index_list[1];
t_MMI_VAR_DESC   var_list[1];
