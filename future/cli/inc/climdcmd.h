/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: climdcmd.h,v 1.76 2017/12/20 11:06:49 siva Exp $
 *
 * Description:This file has the Mode Command tree data
 *             structure typedefs                      
 *******************************************************************/
#include <unistd.h>

#define MMI_MAX_ALIAS_LEN    64
#define MMI_HELP_ALSO      0x80
#define MMI_NOT_ALL_GRP    0x40
#define MMI_GRP_NOTE       0x20

/* Macros for CXHLP */
#define CLI_MAX_HELP_STR_LEN     250 
#define MAX_SSH_MEM_TRACE_NODE   200
typedef   tMemPoolId                   tCliAmbMemPoolId;
/*
 * Help node for a mode. This will have the syntax and semantics
 * given in command definition file for each command
 * This data structure will be used in cmdscan generated file also
 */

typedef struct t_MMI_HELP 
{
    CONST CHR1 **pSyntax;
    CONST CHR1 **pSemantics;
    CONST CHR1 **pCxtHelp;
    CHR1       **pGrpId;
    INT1        *pPrivilegeId;
    INT1        *pOrigPrivilegeId;
    INT4         i4ActNumStart;
    INT4         i4ActNumEnd;
    struct t_MMI_HELP *pNext;
}t_MMI_HELP;

/*
 * This is to have the token level definition fo a command
 */

typedef struct t_MMI_CMD 
{
    struct t_MMI_CMD *pNext_command;   /* other command at same level */
    struct t_MMI_CMD *pOptional;       /* if this node is optional
                                          then next node to look for  */
    struct t_MMI_CMD *pNext_token;     /* next token in this command  */
    CONST CHR1 *i1pToken;              /* if keyword then ptr ;
                                         else bitmap telling type     */
    FS_UINT8 u8Typeof_token;           /* type of the token           */
    INT4 i4Action_number;
    INT2 i2ModeIndex;
    INT2 i2Padding;

    /* function pointer to Range and validity check functions       */

    INT1 *(*pRangeChkFunc) (struct t_MMI_CMD *, INT1 *, INT2 *);

    union t_MMI_RANGE *pRange;      /* to hold Range values for the
                                       token type                   */

} t_MMI_CMD;


typedef struct t_RANGE_INTEGER
{                                   /* struct to hold the Range values */
    UINT4 u4MinRange;
    UINT4 u4MaxRange;
    INT1 *pi1RangeStr;

}t_RANGE_INTEGER;

typedef struct t_RANGE_SINTEGER
{                                   /* struct to hold the Range values                                              for Signed Integer type tokens  */
    INT4 i4MinRange;
    INT4 i4MaxRange;
    INT1 *pi1RangeStr;

}t_RANGE_SINTEGER;

typedef struct t_RANGE_FLOAT
{                                   /* struct to hold the Range values                                              for Float type tokens           */
    DBL8  d8MinRange;
    DBL8  d8MaxRange;
    INT1  *pi1RangeStr;

}t_RANGE_FLOAT;

typedef struct t_RANGE_STRING
{                                   /* struct to hold the Range values                                                 for Integer type tokens      */
    UINT4 u4StrLen;
    INT1 *pi1RangeStr;
}t_RANGE_STRING;

/* union of all struct to hold the Range values of token types 
 * for which Range or Validity checking is supported
 */

typedef union t_MMI_RANGE
{

    struct t_RANGE_INTEGER pRangeInt;
    struct t_RANGE_FLOAT   pRangeFloat;
    struct t_RANGE_STRING  pStringLen;
    struct t_RANGE_SINTEGER pRangeSInt;

}t_MMI_RANGE;

/*
 * This  is for grouping the commands
 */

typedef struct COMMAND_LIST
{
    t_MMI_CMD *pCmd;
    struct COMMAND_LIST *pNext;
}t_MMI_COMMAND_LIST;

/*
 * For storing the Prompt String/Prompt function ptr
 */
typedef struct {
 UINT1 u1PromptType; /* to indicate PROMPT_STR/PROMPT_FUN */
 UINT1 au1Padding[3];
 union {
  CHR1 ac1PromptStr[MAX_PROMPT_LEN]; /* prompt str */
                INT1 (*pPromptFunc)(INT1 *, INT1 *); /* prompt func */
 }uPrompt;
 
#define PromptStr  uPrompt.ac1PromptStr
#define PromptFunc uPrompt.pPromptFunc
}tPromptInfo;

/*
 * This is for mode level tree in mmi ROOT will be the top mode
 */
typedef struct MODE_TREE
{
    struct MODE_TREE     *pChild;
    struct MODE_TREE     *pNeighbour;
    struct MODE_TREE     *pParent;
    tPromptInfo          PromptInfo;
    INT4 (*pValidate_function)(UINT1 *); /* Call this with mode is entered */
    CONST INT1           *pMode_name;           
    t_MMI_COMMAND_LIST   *pCommand_tree;/* Command trees for this mode */  
    t_MMI_HELP           *pHelp;
    INT4                 i4ActionNumStartIdx;
    UINT4                u4ModeType;
    UINT1                u1Type;            /* Iface level, Circuit Level etc */
    UINT1                u1Padding;
    UINT2                u2Padding;
}t_MMI_MODE_TREE; 
  
/*
 * This data structure for defining the token type in mmi
 * This will be used in mmi_lex.c
 */
typedef struct t_MMI_TOKEN_TYPE {
   CONST CHR1 *token_type;
   INT4 (*type_check)(INT1 *);
   INT4 (*value_function) (INT1 *);
} t_MMI_TOKEN_TYPE;

typedef struct  TS_LOC_TERM_CHAR_SET {
    UINT2   u2Reserved;
    UINT1   u1FlushChar;
    UINT1   u1IntrChar;
    UINT1   u1QuitChar;
    UINT1   u1EraseChar;
    UINT1   u1KillChar;
    UINT1   u1Padding;
} tTsLocTermCharSet;

    /*  
     *  The following structure is common for all connections and they
     *  convey the same meaning independent of connections.
     */

typedef struct  TS_LOC_COM_CHAR_SET {
    UINT2   u2Reserved;
    UINT1   u1LFChar;
    UINT1   u1CRChar;
    UINT1   u1BelChar;
    UINT1   u1HTChar;
    UINT1   u1VTChar;
    UINT1   u1SpaceChar;
} tTsLocComCharSet;

/*
 * This is to have the Alias tokens and thier corresponding replacements
 */
typedef struct tCliAlias 
{
    INT1  *pi1Token;
    INT1  *pi1RepToken;
} tCliAlias;

/*
 * This is to have the Grep options and the tokens to be matched
 */
typedef struct tCliGrep 
{
    INT1   i1OptFlag;
    INT1   ai1Pad[3];
    INT1  *pi1GrepStr;
} tCliGrep;

#ifdef CLI_MEM_TRC
/*
 * This is to keep track of Memory allocated and freed dynamically.
 */
typedef struct tMemPoolTrc
{
    UINT4 u4BufSize;
    UINT1 *pu1MemPtr[CLI_MAX_NO_OF_ALLOC];
    UINT4  u4MemSize[CLI_MAX_NO_OF_ALLOC];
} tMemPoolTrc;

#endif

typedef struct CliAmbVar{
    VOID           *amb_vpMmi_values[MAX_AMBIG_CMD][MAX_NO_OF_TOKENS_IN_MMI];
    t_MMI_CMD      *amb_ptr_start[MAX_AMBIG_CMD];
    t_MMI_CMD      *amb_recur_node[MAX_AMBIG_CMD][MAX_INNER_RECUR];
    t_MMI_CMD      *amb_recur_repeat_node[MAX_AMBIG_CMD][MAX_INNER_RECUR];
    INT2   amb_ptr_start_flag[MAX_AMBIG_CMD];
    INT2   amb_recur_flag[MAX_AMBIG_CMD];
    INT2            amb_set_recur_node_flag[MAX_AMBIG_CMD][MAX_INNER_RECUR]
                                        [MAX_CMD_REC_PAR];
    INT2   amb_recur_repeat_flag[MAX_AMBIG_CMD][MAX_INNER_RECUR];
}tCliAmbVar;     
                 
#define CLI_MAX_LINE_BUFF_LEN     512
/* This structure contains all the global variables used previously */
typedef struct CliContext {
    tCliAlias           gCliAlias[CLI_MAX_ALIAS];
    t_MMI_TOKEN_TYPE    mmi_user_cmd_token[MAX_NO_OF_TOKENS_IN_MMI];
    t_MMI_BOOLEAN mmi_file_flag;
    t_MMI_BOOLEAN passwdMode;
    t_MMI_BOOLEAN SessionActive;
    FILE               *ReadFp;
    FILE               *WriteFp;
    t_MMI_MODE_TREE    *pMmiCur_mode;
    t_MMI_MODE_TREE    *pmmi_user_mode;
    VOID               *vpMmi_values[MAX_NO_OF_TOKENS_IN_MMI];
    t_MMI_CMD          ***amb_env_ptr_start;
    t_MMI_CMD          *recur_repeat_node[MAX_INNER_RECUR];
    t_MMI_CMD          *env_ptr_start;
    t_MMI_CMD          *p_mp_recur_node[MAX_INNER_RECUR]; 
    t_MMI_CMD          *MMI_CMD_RET; 
    
    t_MMI_MODE_TREE    *pinter_cur_mode;
    t_MMI_MODE_TREE    *pMmitemp_cur_mode;
    tCliAmbVar         *pCliAmbVar;
    tMemPoolId         SSHMemRbPoolId;
    tRBTree            pSSHMemRoot;
#ifdef CLI_MEM_TRC
    tMemPoolTrc         MemPoolTrc;
#endif
    tCliGrep            aCliGrep[CLI_MAX_GREP];

    INT4               (*fpCliIOInit)(struct CliContext *);
    UINT1              (*fpCliInput)(struct CliContext *);
    INT4               (*fpCliOutput)(struct CliContext *, CONST CHR1 *, 
                          UINT4);
    INT4               (*fpCliTempOutput)(struct CliContext *, CONST CHR1 *, 
                          UINT4);
    INT4               (*fpCliInit)(struct CliContext *);
    INT4               (*fpCliIoctl)(struct CliContext *, INT4, VOID *);
    INT4               (*fpCliFileWrite)(struct CliContext *, CONST CHR1 *, 
                              UINT4);
    INT4               (*fpLock)(VOID);   /* 
                                           * fpLock, fpUnLock - Protocol 
                                           * Lock/UnLock fns which
                                           * will be invoked before calling
                                           * any blocking calls in 
                                           * CliPrintf ()
                                           */
    INT4               (*fpUnLock)(VOID);

    UINT1              *pau1ModeName[CLI_MAX_MODES];
    UINT1              *pu1OutputMessage;
    UINT1              *pu1UserCommand;
    UINT1              **ppu1OutBuf; /* Buffer for Pipe */
    /* Id of the Buddy memory area of this CLI session */
    INT4           i4BuddyId;
    /* Id of the Task this Structure is Valid for */
    tOsixTaskId        TaskId;
    INT4        i4InputFd;
    INT4        i4OutputFd;
    INT4        present_ambig_count; 
    INT4        mmi_keychar;
    INT4        mmi_lineno;
    INT4        mmi_trap_count;
    INT4        mmi_out_put_file;
    INT4        i4ClientSockfd;
    INT4        i4ConnIdx; /* Applicable to Telnet Sessions */
    INT4               i4SessionTimer;
    INT4               i4OutBufLineIndex; /* Index of next line in buffer */
    INT4               i4PromptInfo;
    INT4               i4LineBuffLen;
    INT4               i4LineBuffPos;
    UINT4        gu4CliMode;
    UINT4        gu4CliSysLogId;
    UINT4              u4LineCount; /* User for Pagewise display */

   /* Bits in the Flag will indicate the present status of the CLI session
    * bit-CLI_CTRL_BRK_MASK if set, indicates Ctrl-C is pressed at prompt.
    * Bit -CLI_CTRL_QUIT_MASK if set,indicates CTRL-\ and CTRL-4 is pressed at prompt*/
    UINT4               u4Flags;
    UINT4               au4ClientIpAddr[4];
    UINT4               u4ErrorCode;
    UINT4               u4ContextId;
    INT2  mp_i2set_recur_node_flag
                 [MAX_INNER_RECUR][MAX_CMD_REC_PAR];
 /* To indicate recur_repeat_node can come only time 
  * when parsing */
    INT2  recur_repeat_flag[MAX_INNER_RECUR];
    INT2  mmi_i1user_index;
    INT2  prompt_flag;
    INT2  mp_i2recur_flag;
    INT2  mp_i2set_flag;
    INT2  mmi_cmd_reprint_flag;
    INT2                i2HistCurrIndex;
    INT2                i2TokenCount;
    INT2                i2ScriptRecCount;
    INT2                i2MaxCol;
    INT2                i2MaxRow;
    INT2                i2PrevMaxCol;
    INT2                i2PrevMaxRow;
    INT2                i2CursorPos;
    INT2                i2CmdLen;
    INT2                i2CmdIndex; /* Index of the command in case of multiple
           commands separated by ';' */
    INT2                i2UserIndex;
    INT2                i2PgDnLineCount; /* For page down scrolling */
    INT2                i2Preindexvalue0;    
                        /* for function mmi_convert_value_totype_check */
    INT2                i2Preindexvalue1;    
                        /* for function mmi_convert_value_totype */
    /* To hold prompt array */
    INT1  Mmi_i1Cur_prompt[MAX_PROMPT_LEN];
    /* Begining of current prompt */
    INT1  Mmi_i1Mode_prompt_beg[MAX_MODE_LEVEL];
    INT1  mmi_i1disp_prompt[MAX_PROMPT_LEN];
    INT1  MmiCmdToken[MAX_NO_OF_TOKENS_IN_MMI][MAX_CHAR_IN_TOKEN];
    INT1  ai1CmdString[MAX_LINE_LEN]; 
    INT1  ai1CmdBuf[MAX_LINE_LEN + 4]; 
    INT1        ai1FirstToken[MAX_LINE_LEN]; /*To store the first token of the 
                                               command in expanded form */
    INT1  mmi_i1history_str[MAX_HIST_COM][MAX_LINE_LEN];
    INT1  inter_cur_prompt[MAX_PROMPT_LEN];
    INT1  temp_cur_prompt[MAX_PROMPT_LEN];
    INT1  i1inter_mode_prompt_beg[MAX_MODE_LEVEL];
    INT1  i1temp_mode_prompt_beg[MAX_MODE_LEVEL];
    INT1                ai1UserName[MAX_USER_NAME_LEN];
    INT1                ai1UserStack[MAX_USER_SUBSTITUTES][MAX_USER_NAME_LEN];
    INT1                ai1Tokentype_flag[MAX_NO_OF_TOKENS_IN_MMI];
    INT1                ai1ModePath[MAX_PROMPT_LEN];
    INT1                ai1TempCurModePath[MAX_PROMPT_LEN];
    INT1                ai1InterCurModePath[MAX_PROMPT_LEN];
    UINT1  au1TskName[OSIX_NAME_LEN + 4];
    UINT1  au1PromptStr[MAX_PROMPT_LEN];
    UINT1  au1TempPromptStr[MAX_PROMPT_LEN];
    UINT1  au1InterPromptStr[MAX_PROMPT_LEN];
    UINT1  au1LineBuff[CLI_MAX_LINE_BUFF_LEN];


    UINT1              *pu1PendBuff;
    UINT4               u4PendBuffLen;

    /* Flag indicating structure Active or Inactive */
    INT1  i1Status;
    INT1  i1passwdMode;
    /* Current level in the mode tree */
    INT1  Mmi_i1Cur_modelevel;
    INT1  mmi_echo_flag;
    INT1  mmi_exit_flag;
    INT1  i1inter_cur_level;
    INT1  i1temp_cur_level;
    INT1                i1WinResizeFlag;
    INT1                i1UserAuth;
    /* Indicates whether pipe present in command */
    INT1                i1PipeFlag; 
    INT1                i1MoreQuitFlag; /* Flag to check if 'more' quit'ed */
    INT1                i1MoreFlag;
    INT1                i1GrepFlag;

    UINT1  u1OptionCmd;
    UINT1  u1LastChar;
    UINT1  u1AuthFlag;
    UINT1  u1NoOfAttempts;
    UINT1       u1TelnetCharDiscard;
    UINT1       u1Retries;
    /* User to store the max. number of pages that can be scrolled up/down  */
    INT1                i1PageCount;
    INT1                i1DefaultMoreFlag;
    INT1                i1PipeDisable;
    INT1                i1PrivilegeLevel;
    BOOL1               bIsUserPrivileged;

    /* This global variable is used as a flag to decide what to be displayed
     * during connection termination in CLI. */
    UINT1               u1CliLogOutMessage;
    UINT1               u1InitWinSize;
    INT1                i1ClearFlagStatus;
    INT1                ai1Pad[1];
    UINT4          u4CmdStatus;
    INT1                ai1OrgCmdToken[MAX_LINE_LEN];
    /* Interface Range Command requires the following information*/
    INT4        i4Mode;
    INT4        i4CmdErrCode;
    UINT4       u4StartIndex;
    UINT4       u4EndIndex;
    UINT1       au1RangeList[MAX_CHAR_IN_TOKEN];
    UINT1       au1RangeType[CLI_MAX_IF_TYPE_LEN];
     /* Redirecting the ouptut of any command to a file requires the following information */
    INT4        i4FileFlag;
    UINT1       au1FileName[CLI_FILE_LEN];
    INT4                i4ContextWriteFd; /*= -1;*/
    INT4                (*pTempOutputFp) (struct CliContext *, CONST CHR1 *,
                                          UINT4);
    UINT4       u4IpAddrType;
    UINT4       u4PoolId;
    UINT4       u4NatPoolId;;
    INT1        ai1ModeName[MAX_PROMPT_LEN];
    UINT4       u4ReqUpdated; /* OSIX_TRUE if request is updated */
    UINT4       mmi_hist_top;
    UINT1       u1SkipDoubleBang;
    INT1        i1HelpFlag;
    INT1        i1SpaceFlag;
    UINT1       u1InputChar;
    UINT1       u1GrepErrorFlag;
    UINT1       u1UserNameCreation;
    UINT1       u1PrintOngoing;
    UINT1       u1IsTimedWaitLockFail; /* CLI_TRUE if lock could not be taken on timer expiry */
    UINT4       u4ConcatFlag;
    INT4        i4AuthStatus;
    UINT4      u4LineWriteCount; /*actual number of lines displayed in console */
    UINT1      *pu1HelpStr;
    UINT1      *pu1TempHelpStr1;
    UINT1      *pu1TempHelpStr2;
    UINT1      *pu1OptionalTokens;
    UINT1      *pu1OutputBuf;
    UINT1      *pu1ResizeCommand;
    UINT1      *pu1TempString;
    UINT1      *pu1Temp;
    UINT1      *pu1Cmd;
    UINT1      *pu1TknList;
    UINT1      u1BangStatus; /*! status*/
    UINT1      au1Padding[3]; 
} tCliContext;

/* This structure contains information common to all Sessions */
typedef struct CliSessions {
    /* Command list for each of the Modes in Cli e.g ROOT, ETH ...*/
    t_MMI_COMMAND_LIST  *mmi_cmd_list[CLI_MAX_MODE];
    /* This ptr will be the root of all mode tree in mmi */
    t_MMI_MODE_TREE     *p_mmi_root_mode;
    t_MMI_CMD           *pMmiCurcmd_ptr; 
    t_MMI_MODE_TREE     *p_mmi_just_added;
    t_MMI_PASSWD  mmi_user[MAX_NO_USER];
    t_MMI_TOKEN_TYPE mmi_cmd_token[MAX_TYPE_TOKEN];
    /* Context Specific information for each session */
    tCliContext  gaCliContext[CLI_MAX_SESSIONS];
    tOsixSemId          CliAmbVarSemId;
    tOsixSemId          CliContextSemId;
    tOsixSemId          CliSshSemId;
    tOsixSemId          CliUsersFileSemId;
    tOsixSemId          CliGroupsFileSemId;
    tOsixSemId          CliPrivilegesFileSemId;
    tOsixSemId          CliPamPrivilegesFileSemId;
    INT1                *pi1CliUsers[CLI_MAX_USERS + 1];
    INT1                *pi1CliGroups[CLI_MAX_GROUPS + 1];
    INT1                *pi1CliPrivileges[CLI_MAX_PRIVILEGES + 1];
    UINT1               au1CliAmbVarSemName [OSIX_NAME_LEN + 1];
    UINT1               au1CliContextSemName [OSIX_NAME_LEN + 1];
    UINT1               au1CliUsersFileSemName [OSIX_NAME_LEN + 1];
    UINT1               au1CliGroupsFileSemName [OSIX_NAME_LEN + 1];
    UINT1               au1CliPrivilegesFileSemName [OSIX_NAME_LEN + 1];
    UINT1               au1CliSshSemName  [OSIX_NAME_LEN + 1];
    UINT1               au1CliPamPrivilegesFileSemName [OSIX_NAME_LEN + 1];
    UINT1  u1LoginAttempts; 
    BOOL1               bIsClCliEnabled;
    INT1                i1LoginMethod;
    UINT1               iPadbytes[2];
    INT4  i4SessCount;
    UINT4  u4LoginLockOutTime;
    INT2                i2NofGroups;
    INT2                i2NofUsers;
    INT2                i2NofPrivileges;
    UINT2  cmdListIndex;
} tCliSessions;


/*
 * Context help params that would be passed to 
 * find the expected token & fetch the corresponding
 * help string for a particular syntax.
 * 
 */

typedef struct t_CxtHelpParams
{
    UINT1               **ppu1HelpStr;  /* Array of help strings to be displayed */
    UINT1               **ppu1TknList;  /* Array of token strings */
    INT1                *pu1Str;        /* String entered by the user */
    struct t_MMI_HELP   *p_thelp_ptr;   /* Current Help Pointer */
    t_MMI_COMMAND_LIST  *pCurrGrpCmdList; /* Current command pointer */
    INT4                i4EnteredTok;   /* Number of tokens entered by user*/
    INT2                i2Index;        /* Command Index in a group */
    INT2                i2HelpStrIndex; /* Index of the help string to be stored */
    INT2                i2TotGrpCnt;    /* Total number of groups in a  command tree*/
    INT2                i2GrpCnt;       /* Current group index that is under process */
    INT1                i1TknMatchFlag[CLI_CXT_TKN_LIST_SIZE];
    INT1                i1IsGrpCntChanged; /* Flag that indicates whether we have moved to next group */
    INT1                i1SpaceFlag;    /* flag to indicate if ? is given after space */
}tCxtHelpParams;

/* Structure for storing token and the corresponding function to be invoked */

typedef struct DynHelpRegInfo
{
    CONST CHR1 * pcToken;
    VOID (*CliCxDynAddDynHelpStr) (tCxtHelpParams * pCxtHelpParams);
}tDynHelpRegInfo;


typedef struct _tCliUserBlock
{
    UINT1     au1CliUserBlock[CLI_MAX_USERS_LINE_LEN + 1];
    UINT1     au1Reserved[3];
}tCliUserBlock;

typedef struct _tCliHelpBlock
{
    UINT1     au1CliHelpBlock[CLI_MAX_HELP_BLOCK_SIZE];
}tCliHelpBlock;

typedef struct _tCliGroupBlock
{
    UINT1    au1CliGroupBlock[CLI_MAX_GROUPS_LINE_LEN + 1];
    UINT1    au1Reserved[3];
}tCliGroupBlock;

typedef struct _tCliPrivilegesBlock
{
    UINT1    au1CliPrivilegesBlock[CLI_MAX_PRIVILEGES_LINE_LEN];
}tCliPrivilegesBlock;

typedef struct _tCliTokenBlock
{
    UINT1   au1CliTokenBlock[MAX_CHAR_IN_TOKEN];
}tCliTokenBlock;

typedef struct _tCliCtxBuffArrBlock
{
    UINT1   *pu1CliCtxBuffArrBlock[CLI_DEF_MAX_ROWS][CLI_MORE_OUT_NUMPAGE];
}tCliCtxBuffArrBlock;

typedef struct _tCliCtxOutputBuffBlock
{
    UINT1  au1CliCtxOutputBuffBlock[CLI_DEF_MAX_ROWS - 1]
                          [CLI_DEF_MAX_COLS + 3][CLI_MORE_OUT_NUMPAGE];
}tCliCtxOutputBuffBlock;

typedef struct _tCliCtxPendBufBlock
{
    UINT1 au1CliCtxPendBufBlock[MAX_CLI_OUTPUT_PEND_BUF_SIZE];
}tCliCtxPendBufBlock;

typedef struct _tCliAmbPtrBlock
{
    t_MMI_CMD  *pCliAmbPtrBlock[MAX_AMBIG_CMD];
}tCliAmbPtrBlock;

typedef struct _tCliAmbArrBlock
{
    t_MMI_CMD  *pCliAmbArrBlock[MAX_NO_OF_TOKENS_IN_MMI];
}tCliAmbArrBlock;

typedef struct _tCliMaxLineBlock
{
    UINT1     au1CliMaxLineBlock[MAX_LINE_LEN + 1];
    UINT1     au1Reserved[3];
}tCliMaxLineBlock;

typedef struct _tCliCmdHelpPrivBlock
{
    UINT1     au1CliCmdHelpPrivBlock[CLI_MAX_CMDS_IN_GROUP];
}tCliCmdHelpPrivBlock;

typedef struct _tCliCtxHelpBlock
{
    UINT1     au1CliCtxHelpBlock[MAX_NO_OF_TOKENS_IN_MMI][CLI_MAX_TOKEN_VAL_MEM];
}tCliCtxHelpBlock;

/* To handle message/events given by redundancy manager. */
typedef struct CliRmMsg
{
    tRmMsg             *pFrame;    /* Message given by RM module. */
    UINT2               u2Length;    /* Length of message given by RM module. */
    UINT2               u2Rsvd;
} tCliRmMsg;

typedef struct CliRedQMsg
{
    UINT4               u4MsgType;
    tCliRmMsg           RmData;
} tCliRedQMsg;

/* To handle CallBack for Customization in CLI */
typedef union CliCallBackEntry
{
        INT4 (*pCliCustCheckrootLoginAllowed) (INT2);
        INT4 (*pCliCustCheckStrictPasswd) (VOID);
}unCliCallBackEntry;

typedef struct CliCallBackArgs
{
          INT2  i2PrivilegeId;
          UINT1 au1Padding[2];
}tCliCallBackArgs;

typedef struct _tCliMaxOutputBlock
{
    UINT1     au1CliMaxOutputBlock[MAX_CLI_OUTPUT_BUF_SIZE];
}tCliMaxOutputBlock;
typedef struct _SshMemTraceInfo
{
      VOID  *pAddr;
} tSshMemTraceInfo;

