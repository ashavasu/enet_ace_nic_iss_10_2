
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clihist.c,v 1.14 2014/09/02 11:59:13 siva Exp $
 *
 * Description: This will have the read routines and 
 *              history routines                     
 *******************************************************************/

#include "clicmds.h"

INT1                mmi_i1history_str[MAX_HIST_COM][MAX_LINE_LEN];
UINT1               mmi_hist_top = 0;
INT4                mmi_history_char_index = 0;
INT2                mmi_cmd_reprint_flag = 0;
UINT4               mmi_cur_hist_num_base = 0;

/* Array of Context Structures */
extern tCliSessions gCliSessions;

/***********************************************************************
    Routine to display list of 'MAX_HIST_COM'  recent commands.
    MAX_HIST_NUM indicates the maximum command number that will be
    printed before starting command number at 1.
    mmi_cur_hist_num_base is used as starting point to current 15 history
    commands.
    i.e. if mmi_cur_hist_num_base == 10
     command numbers will be from 11 to 26.
    The mmi_cur_hist_num_base will be 0 initially and will start getting
    incremented  with each command when total no. of typed commands
    exceed MAX_HIST_COM.
    When user wants to execute a command the command chosen is calculated
    based on mmi_cur_hist_num_base and mmi_hist_top.
***********************************************************************/
VOID
mmi_print_history_buffer ()
{
    UINT4               i, j = 1;
    tCliContext        *pcurrContext;

    if ((pcurrContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    if (pcurrContext->mmi_hist_top <= MAX_HIST_COM)
    {
        i = 0;
        j = 1;
    }
    else
    {
        i = (pcurrContext->mmi_hist_top - 1) % MAX_HIST_COM + 1;
        j = pcurrContext->mmi_hist_top - MAX_HIST_COM + 1;
    }
    for (; j != pcurrContext->mmi_hist_top + 1;)
    {
        if (i == MAX_HIST_COM)
            i = 0;
        mmi_printf ("\r%2d  %s \r\n", j, pcurrContext->mmi_i1history_str[i]);
        i++;
        j++;
    }
}
VOID
mmi_AddUserCommandToHistory (pCliContext, pu1UsrInp)
     tCliContext        *pCliContext;
     CONST CHR1         *pu1UsrInp;
{
    UINT1              *pu1TmpInp;
    INT2                i2CmdLen;
    UINT1               u1Cnt = 0;

    i2CmdLen = (INT2) STRLEN (pu1UsrInp);
    if (((pCliContext->mmi_hist_top % MAX_HIST_COM) == 0) ||
        (STRNCMP (pu1UsrInp, pCliContext->mmi_i1history_str
                  [(pCliContext->mmi_hist_top % MAX_HIST_COM) - 1],
                  i2CmdLen - 1) != 0) || (STRLEN (pu1UsrInp) - 1) !=
        STRLEN (pCliContext->
                mmi_i1history_str[(pCliContext->mmi_hist_top % MAX_HIST_COM) -
                                  1]))
    {
        STRCPY (pCliContext->
                mmi_i1history_str[pCliContext->mmi_hist_top % MAX_HIST_COM],
                pu1UsrInp);

        pu1TmpInp =
            (UINT1 *) pCliContext->mmi_i1history_str[pCliContext->mmi_hist_top %
                                                     MAX_HIST_COM];
        if (pu1TmpInp[i2CmdLen - 1] == '\n')
        {
            pu1TmpInp[i2CmdLen - 1] = '\0';
        }
        pCliContext->mmi_hist_top++;

        if (pCliContext->mmi_hist_top == 0)
        {
            for (u1Cnt = 0; u1Cnt < MAX_HIST_COM - 1; u1Cnt++)
            {
                STRNCPY (pCliContext->mmi_i1history_str[u1Cnt],
                         pCliContext->mmi_i1history_str[u1Cnt + 1],
                         STRLEN (pCliContext->mmi_i1history_str[u1Cnt + 1]));
            }

            pCliContext->mmi_hist_top = (UINT4) (-2);

            STRNCPY (pCliContext->mmi_i1history_str[pCliContext->mmi_hist_top %
                                                    MAX_HIST_COM], pu1UsrInp,
                     STRLEN (pu1UsrInp));

            pu1TmpInp =
                (UINT1 *) pCliContext->mmi_i1history_str[pCliContext->
                                                         mmi_hist_top %
                                                         MAX_HIST_COM];

            i2CmdLen = (INT2) STRLEN (pu1TmpInp);

            if (pu1TmpInp[i2CmdLen - 1] == '\n')
            {
                pu1TmpInp[i2CmdLen - 1] = '\0';
            }
            pCliContext->mmi_hist_top = MAX_HIST_COM;

        }
    }
}
