/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clipamldap.c,v 1.1 2017/12/20 11:08:43 siva Exp $
 *
 * Description: clipamldap.c
 *
 * *******************************************************************/

#include <sys/param.h>

#include <pwd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <lber.h>
#include <ldap.h>
#include <sys/time.h>
#include <security/pam_modules.h>
#include <security/pam_appl.h>
#include "clicmds.h"
#include "fips.h"

#ifndef PAM_EXTERN
#define PAM_EXTERN
#endif

#ifdef malloc
#undef malloc
#endif
#ifdef free
#undef free
#endif

extern INT4 CliLdapGetInputFromFile (CHR1 *pu1LdapBase, CHR1 *pu1LdapHost, CHR1 *pu1LdapRootDn);
INT4
converse( pam_handle_t *pamh,INT4 nargs,struct pam_message **message,struct pam_response **response);

CHR1 au1InpLine[MAX_LDAP_NSLCD_LEN];
CHR1 au1InpWord[MAX_LDAP_NSLCD_LEN];

PAM_EXTERN INT4
pam_sm_authenticate(pam_handle_t *pamh, INT4 flags,
		INT4 argc, CONST CHR1 *argv[])
{
    INT4    i4LdapVersion = LDAP_VERSION3;
    INT4    i4Result = -1;
    INT4    i4Index = 0;
    INT4    i4IsUserPrivil = 0;
    INT4    i4ValidUser = 0;
    CHR1    *pu1Password = NULL;
    CHR1    *pu1IssPrivilege = NULL;
    CHR1    *pu1LdapAttr = NULL;
    CHR1    *pu1LdapAttrDummy = NULL;
    CHR1    *pu1PrivilStr = NULL;
    CHR1    *pu1LdapFilter="(objectClass=*)";
    CHR1    *pu1LdapBase = NULL;
    CHR1    *pu1LdapHost = NULL;
    CHR1    *pu1LdapSearchAttr[8]={"uid","userPassword","issPrivilege"};
    CHR1    *pu1LdapRootDn = NULL;
    CHR1    *pu1LdapRootPwd = "secret";
    CHR1     u1UserIdentified = 0;
    CONST CHR1 *pu1User = NULL;
    struct  berval bervalCredentials;
    struct  berval **structBervals;
    struct  berval **structBervalsDummy;
    struct  timeval structTimeval;
    struct  pam_message msg[1],*pmsg[1];
    struct  pam_response *response;
    LDAP    *ldap = NULL;
    LDAPMessage* LdapMsg = NULL;
    LDAPMessage* LdapEntry = NULL;
    LDAPMessage* LdapEntryDummy = NULL;
    BerElement* berElement;

    structTimeval.tv_sec=5;
    structTimeval.tv_usec=0;
    pmsg[0] = &msg[0];
    msg[0].msg_style = PAM_PROMPT_ECHO_OFF;
    msg[0].msg = "Password: ";
    response = NULL;

    UNUSED_PARAM(flags);
    UNUSED_PARAM(argc);
    UNUSED_PARAM(argv);
    pu1LdapBase = malloc(MAX_LDAP_NSLCD_LEN);
    pu1LdapHost = malloc(MAX_LDAP_NSLCD_LEN);
    pu1LdapRootDn = malloc(MAX_LDAP_NSLCD_LEN);
    i4Result = CliLdapGetInputFromFile (pu1LdapBase, pu1LdapHost, pu1LdapRootDn);
    pu1LdapBase = pu1LdapBase + MAX_LDAP_BASE_LEN;
    pu1LdapHost = pu1LdapHost + MAX_LDAP_URI_LEN;
    pu1LdapRootDn = pu1LdapRootDn + MAX_LDAP_BIND_LEN;

    pu1Password = malloc(MAX_LDAP_URI_LEN);
    pu1IssPrivilege = malloc(MAX_LDAP_URI_LEN);
    pu1PrivilStr = malloc(MAX_LDAP_URI_LEN);

    /* identify user */
    if (pam_get_user(pamh, &pu1User, NULL) != PAM_SUCCESS)   
        return PAM_AUTH_ERR;
    if(( i4Result = converse( pamh, 1 , pmsg, &response)) != PAM_SUCCESS )
        return PAM_CONV_ERR;
    memcpy(&pu1Password,response,sizeof(MAX_LDAP_NSLCD_LEN));
    strcpy(pu1IssPrivilege,"issPrivilege=1");
    ldap_initialize(&ldap,pu1LdapHost);
    if (ldap == NULL )
    {
        return PAM_AUTH_ERR;
    }
    /* set the LDAP version to be 3 */
    if (ldap_set_option(ldap, LDAP_OPT_PROTOCOL_VERSION, &i4LdapVersion) != LDAP_OPT_SUCCESS)
    {
        return PAM_AUTH_ERR;
    }
    bervalCredentials.bv_val= pu1LdapRootPwd;
    bervalCredentials.bv_len=strlen(pu1LdapRootPwd);
    if (ldap_sasl_bind_s(ldap, pu1LdapRootDn, NULL, &bervalCredentials,NULL,NULL,NULL) != LDAP_SUCCESS )
    {
        return PAM_AUTH_ERR;
    }
    if(ldap_search_ext_s(ldap,pu1LdapBase,LDAP_SCOPE_SUBTREE,pu1LdapFilter,pu1LdapSearchAttr,0,NULL,NULL,
                         &structTimeval,0,&LdapMsg)!=LDAP_SUCCESS)
    {
        return PAM_AUTH_ERR;
    }
    /* Iterate through the returned entries */
    for(LdapEntry = ldap_first_entry(ldap, LdapMsg);
        LdapEntry != NULL;
        LdapEntry = ldap_next_entry(ldap, LdapEntry))
    {
        u1UserIdentified = 0;
        for( pu1LdapAttr= ldap_first_attribute(ldap, LdapEntry, &berElement);
             pu1LdapAttr != NULL;
             pu1LdapAttr = ldap_next_attribute(ldap, LdapEntry, berElement))
        {
            if ((structBervals = ldap_get_values_len(ldap, LdapEntry, pu1LdapAttr)) != NULL)
            {
                for(i4Index = 0; structBervals[i4Index] != '\0'; i4Index++)
                {
                    if((strcmp(pu1LdapAttr,"uid")==0)
                       && (strcmp(structBervals[i4Index]->bv_val,pu1User)==0))
                    {
                        u1UserIdentified = 1;
                    }

                    if((u1UserIdentified == 1) 
                       && ((strcmp(pu1LdapAttr,"userPassword")) == 0))
                    {
                        if ((strcmp(structBervals[i4Index]->bv_val,pu1Password)) == 0)
                        {
                            i4ValidUser = 1;
                            LdapEntryDummy = LdapEntry;
                        }
                    }
                }
                ldap_value_free_len(structBervals);
            }
        }
        ldap_memfree(pu1LdapAttr);
        if(berElement!=NULL)
            ber_free(berElement,0);
    }
    if (i4ValidUser == 1)
    {
        for( pu1LdapAttrDummy = ldap_first_attribute(ldap, LdapEntryDummy, &berElement);
             pu1LdapAttrDummy != NULL;
             pu1LdapAttrDummy = ldap_next_attribute(ldap, LdapEntryDummy, berElement))
        {
            if ((structBervalsDummy = ldap_get_values_len(ldap, LdapEntryDummy, pu1LdapAttrDummy)) != NULL)
            {
                for(i4Index = 0; structBervalsDummy[i4Index] != '\0'; i4Index++)
                {
                    if(strcmp(pu1LdapAttrDummy,"issPrivilege")==0)
                    {
                        memset (pu1PrivilStr, '\0', sizeof(pu1PrivilStr));
                        strcpy(pu1PrivilStr, "issPrivilege=");
                        if(( atoi(structBervalsDummy[i4Index]->bv_val) <= MAX_LDAP_PWD_PRIVIL) &&
                           atoi(structBervalsDummy[i4Index]->bv_val) > MIN_LDAP_PWD_PRIVIL)
                        {
                            strcat(pu1PrivilStr, structBervalsDummy[i4Index]->bv_val);
                            i4IsUserPrivil = 1;
                        }
                        break;
                    }
                }
                ldap_value_free_len(structBervalsDummy);
            }
        }
        ldap_memfree(pu1LdapAttrDummy);
        if(berElement!=NULL)
            ber_free(berElement,0);
        if (i4IsUserPrivil == 1)
        {
            i4Result = pam_putenv(pamh,pu1PrivilStr);
        }
        else
        {
            i4Result = pam_putenv(pamh,pu1IssPrivilege);
        }
        return PAM_SUCCESS;
    }
    else
    {
        return PAM_AUTH_ERR;
    }
    /* clean up */
    ldap_msgfree(LdapMsg);
    i4Result = ldap_unbind_ext_s(ldap,NULL,NULL);;
    if (i4Result != 0)
    {
        return PAM_AUTH_ERR;
    }

    return PAM_AUTH_ERR;
}
int
CliLdapGetInputFromFile (char *pu1LdapBase, char *pu1LdapHost, char *pu1LdapRootDn)
{
    FILE               *pFile = NULL;

    if ((pFile = fopen(LDAP_FILE_NAME, "r")) != NULL)
    {
        if (fgets (au1InpLine, sizeof (au1InpLine), pFile) == NULL)
        {
            fclose (pFile);
            return 0;
        }
        memset (au1InpLine, 0, MAX_LDAP_NSLCD_LEN);
        while (fgets (au1InpLine, sizeof (au1InpLine), pFile) != NULL)
        {
            sscanf (au1InpLine, "%s", au1InpWord);
            if (strncmp (au1InpWord, "base ", strlen(au1InpWord)) == 0)
            {
                memcpy (pu1LdapBase, au1InpLine, MAX_LDAP_NSLCD_LEN);
                memset (au1InpLine, 0, MAX_LDAP_NSLCD_LEN);
                fgets (au1InpLine, sizeof (au1InpLine), pFile);
            }
            else if (strncmp (au1InpWord, "uri ldap", strlen(au1InpWord)) == 0)
            {
                memcpy (pu1LdapHost,au1InpLine,MAX_LDAP_NSLCD_LEN);
                memset (au1InpLine, 0, MAX_LDAP_NSLCD_LEN);
                fgets (au1InpLine, sizeof (au1InpLine), pFile);
            }
            else if (strncmp (au1InpWord, "binddn ", strlen(au1InpWord)) == 0)
            {
                memcpy (pu1LdapRootDn , au1InpLine, MAX_LDAP_NSLCD_LEN);
                memset (au1InpLine, 0, MAX_LDAP_NSLCD_LEN);
                fgets (au1InpLine, sizeof (au1InpLine), pFile);
            }
            memset (au1InpLine, 0, MAX_LDAP_NSLCD_LEN);
        }
        fclose (pFile);
    }
    else
    {
         return 0;
    }
    return 1;
}

INT4 converse( pam_handle_t *pamh,INT4 nargs,struct pam_message **message,struct pam_response **response )
{
    INT4 i4Retval;
    struct pam_conv *conv;

    i4Retval = pam_get_item(pamh, PAM_CONV,  (const void **) &conv ) ;
    if( i4Retval == PAM_SUCCESS ) {
        i4Retval = conv->conv( nargs,
                             ( const struct pam_message ** ) message,
                             response,
                             conv->appdata_ptr );
    }
    return i4Retval;
}
