/* $Id: clitrggr.c,v 1.14 2010/12/23 12:39:50 siva Exp $ */
#include "fssnmp.h"
#include "msr.h"
#include "rmgr.h"

/******************************************************************************
*      function Name        : nmhSetCmn                                       *
*      Role of the function : This fn call all the nmhset functions and sends *
*                             an update trigger in case object status is not  *
*                             deprecated                                       *    
*      Formal Parameters    : Objectid, Wrapperfn,  isDeprecated, isRowStatus *
*                             no_of_indices, fmt string, variable no args     *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
nmhSetCmn (UINT4 au4ObjectID[], INT4 i4OidLen,
           INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
           INT4 (*LockPtr) (VOID),
           INT4 (*UnlockPtr) (VOID),
           UINT4 u4Deprecated,
           UINT4 u4RowStatus, INT4 i4Indices, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);
    i1RetVal = MsrSetWithNotify (au4ObjectID, i4OidLen, Wrapperfn,
                                 LockPtr, UnlockPtr, u4Deprecated,
                                 u4RowStatus, i4Indices, fmt, ap);
    va_end (ap);
    return i1RetVal;
}

/******************************************************************************
*      function Name        : nmhSetCmnNew                                    *
*      Role of the function : This fn call sends an update trigger            *
*                             in case object status is not deprecated.        *
*                             This fn is used for the generated code were the *
*                             Set operation will happen and then the          *
*                             trigger called                                  *
*      Formal Parameters    : Objectid, isDeprecated, isRowStatus             *
*                             no_of_indices, ret_val status fmt string,       * 
*                             variable no args                                *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
nmhSetCmnNew (UINT4 au4ObjectID[], INT4 i4OidLen,
              INT4 (*LockPtr) (VOID),
              INT4 (*UnlockPtr) (VOID),
              UINT4 u4Deprecated,
              UINT4 u4RowStatus, INT4 i4Indices, INT4 i4RetVal, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);
    i1RetVal = MsrSetWithNotifyNew (au4ObjectID, i4OidLen, LockPtr, UnlockPtr,
                                    u4Deprecated, u4RowStatus, i4Indices,
                                    i4RetVal, fmt, ap);
    va_end (ap);
    return i1RetVal;
}

/******************************************************************************
*      function Name        : nmhSetCmnWithLock                               *
*      Role of the function : This fn call is used to release the protocol    *
*                             lock before calling nmhSetCmn and retake the    *
*                             lock after the nmhSet function. This function   *
*                             is primarily used for set routines for which    *
*                             the protocol lock is taken within the wrapper   *
*                             routines.                                       *
*      Formal Parameters    : Objectid, Wrapperfn,  isDeprecated, isRowStatus *
*                             no_of_indices, fmt string, variable no args     *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
nmhSetCmnWithLock (UINT4 au4ObjectID[], INT4 i4OidLen,
                   INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
                   INT4 (*LockPtr) (VOID),
                   INT4 (*UnlockPtr) (VOID),
                   UINT4 u4Deprecated, UINT4 u4RowStatus,
                   INT4 i4Indices, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);

    if (UnlockPtr != NULL)
    {
        UnlockPtr ();
    }

    i1RetVal = MsrSetWithNotify (au4ObjectID, i4OidLen, Wrapperfn, NULL, NULL,
                                 u4Deprecated, u4RowStatus, i4Indices, fmt, ap);

    if (LockPtr != NULL)
    {
        LockPtr ();
    }
    va_end (ap);

    return i1RetVal;
}
