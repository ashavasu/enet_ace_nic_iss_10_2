
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clilex.c,v 1.48 2016/10/06 12:19:06 siva Exp $
 *
 * Description: This will have the basic read routines from  
 *              keyboard                                     
 *******************************************************************/
#include "clicmds.h"

extern tCliSessions gCliSessions;
extern tMemPoolId   gCliMaxLineMemPoolId;

t_MMI_TOKEN_TYPE    mmi_cmd_token[MAX_TYPE_TOKEN] = {
    /* The 0 th position is reserved for integer .
     * this position will be used to convert to the
     * integer type so user directly can refer *$#
     */

    {"ucast_mac", MmiUCastMACCheck, NULL}
    ,                            /* 1 */
    {"mcast_mac", MmiMCastMACCheck, NULL}
    ,                            /* 2 */
    {"mac_addr", mmi_macaddress_check, NULL}
    ,                            /* 4 */
    {"ucast_addr", MmiUCastAddrCheck, MmiIpAddrValueFunction}
    ,                            /* 8 */
    {"lucast_addr", MmiLUCastAddrCheck, MmiIpAddrValueFunction}
    ,                            /* 16 */
    {"mcast_addr", MmiMCastAddrCheck, MmiIpAddrValueFunction}
    ,                            /* 32 */
    {"ip_mask", MmiIpMaskCheck, MmiIpAddrValueFunction}
    ,                            /* 64 */
    {"ip_addr", MmiIpAddrCheck, MmiIpAddrValueFunction}
    ,                            /* 128 */
    {"ip6_addr", MmiIp6AddrCheck, NULL}
    ,                            /* 256 */
    {"float", mmi_lex_float_check, float_value_function}
    ,                            /* 512 */
    {"short", mmi_lex_short_check, short_value_function}
    ,                            /* 1024 */
    {"integer", mmi_lex_integer_check, integer_value_function}
    ,                            /* 2048 */
    {"hex_str", mmi_hexstring_check, NULL}
    ,                            /* 4096 */
    {"systime", MmiTimeCheck, NULL}
    ,                            /* 8192 */
    {"ifnum", MmiIfNumCheck, NULL}
    ,                            /* 16384 */
    {"iface_list", MmiIfaceListCheck, NULL}
    ,                            /* 32768 */
    {"num_str", mmi_numstring_check, NULL}
    ,                            /* 65536 */
    {"port_list", MmiPortListCheck, NULL}
    ,                            /* 131072 */
    {"tftp_url", mmi_tftp_url_check, NULL}
    ,                            /* 262144 */
    {"flash_url", mmi_flash_url_check, NULL}
    ,                            /* 524288 */
    {"iftype", MmiIfTypeCheck, MmiIfTypeValueFunc}
    ,                            /* 1048576 */
    {"ifXtype", MmiIfXTypeCheck, MmiIfXTypeValueFunc}
    ,                            /* 2097152 */
    {"string", mmi_string_check, NULL}
    ,                            /* 4194304 */
    {"quote_str", mmi_quoted_string_check, NULL}
    ,                            /* 8388608 */
    {"mode_str", mmi_mode_string_check, NULL}
    ,                            /* 16777216 */
    {"random_str", mmi_random_string_check, NULL}    /* 33554432 */
    ,
    {"oui", MmiOuiCheck, NULL}    /*67108864 */
    ,
    {"integer64", MmiInteger64Check, NULL}    /*134217728 */
    ,
    {"sftp_url", mmi_sftp_url_check, NULL}    /* 268435456 */
    ,
    {"negative", mmi_lex_negative_check, negative_value_function}    /*536870912 */
    ,
    {"cust_url", mmi_cust_url_check, NULL}    /*1073741824 */
    ,
    {"ip_cisco_mask", MmiIpCiscoMaskCheck, MmiIpAddrValueFunction}    /*2147483648 */
    ,
    {"vlan_vfi_id", MmiVlanVfiIdValidate, integer_value_function}
    ,
    {"ipiftype", MmiIpIfTypeCheck, NULL}
    ,
    {"dns_host_name", MmiHostNameCheck, NULL}
    ,
    {"ifxnum", MmiIfxNumCheck, NULL}
};

/* This file will have mmi_gettoken () and mmi_parse () 
 * 
 * The variable Mmi_i4lex_token_type will be used in other
 * module also
 *
 * Aim :
 *        1. The lex will have token type and grammer
 *        2. To specify the grammer it will be written
 *           as a function
 *           example 
 *             for "string"   mmi_string_check () 
 *           The function written for grammer should return INT4
 *           FALSE if grammer ok
 *           TRUE  if grammer fail
 *        3. Then the token type entered in
 *           t_MMI_TOKEN_TYPE mmi_cmd_token[] as
 *           { "string", mmi_string_check}, 
 *           should be at last  but before {NULL, NULL}
 *           { because the first one integer is reserved for 
 *             0 th bit position} 
 *        4. The token position in the mmi_cmd_token[] will be
 *           used in cmd_create () and setting the bit map
 *        5. Using this the developer can specify the token
 *           "string" in command as <string>
 *        6. Beacuse of Mmi_i4lex_token_type is INT4, maximum
 *           31 grammer can be defined
 */

/*
 * This routine will set the pos-th bit in Mmi_i4lex_token_type
 */

VOID
mmi_lx_set_bit_pos (i4pos, pu8TokenType)
     INT4                i4pos;
     FS_UINT8           *pu8TokenType;
{
    FS_UINT8            u8num_to_or;
    INT4                i = 0;

    if (i4pos > (MAX_TYPE_TOKEN - 1))
    {
        mmi_printf ("Illegal setting in Mmi_i4lex_token_type\r\n");
        return;
    }

    FSAP_U8_CLR (&u8num_to_or);

    FSAP_U8_FETCH_LO (&u8num_to_or) = 1;
    FSAP_U8_FETCH_HI (&u8num_to_or) = 1;

    if (i4pos < CLI_LOW_BYTE_MAX_VAL)
    {
        for (i = 0; i < i4pos; i++)
        {
            FSAP_U8_FETCH_LO (&u8num_to_or) *= 2;
        }
        FSAP_U8_FETCH_LO (pu8TokenType) |= FSAP_U8_FETCH_LO (&u8num_to_or);
    }
    else
    {
        for (i = CLI_LOW_BYTE_MAX_VAL; i < i4pos; i++)
        {
            FSAP_U8_FETCH_HI (&u8num_to_or) *= 2;
        }
        FSAP_U8_FETCH_HI (pu8TokenType) |= FSAP_U8_FETCH_HI (&u8num_to_or);
    }
    return;
}

/*
 * This is for "short" type of token 
 */

INT4
mmi_lex_short_check (INT1 *pi1Token)    /* [0-9]+  but limited to 12 digit */
{
    INT2                i2Base = 10;

    if (*pi1Token == '0')
    {
        pi1Token++;

        if (*pi1Token == 'x' || *pi1Token == 'X')
        {
            pi1Token++;
            i2Base = 16;
        }
        else
            pi1Token--;
    }

    if (*pi1Token == '\0')
        return FALSE;

    while (*pi1Token)
    {
        if (!isxdigit (*pi1Token))
        {
            return FALSE;
        }
        if (!(isdigit (*pi1Token)) && (i2Base != 16))
        {
            return FALSE;
        }

        pi1Token++;
    }
    return TRUE;
}

/*
 * This is for "integer" type of token 
 */

INT4
mmi_lex_integer_check (INT1 *pi1Token)    /* [0-9]+  but limited to 12 digit */
{
    INT2                i2Base = 10;

    if (*pi1Token == '0')
    {
        pi1Token++;

        if (*pi1Token == 'x' || *pi1Token == 'X')
        {
            pi1Token++;
            i2Base = 16;
        }
        else
            pi1Token--;
    }

    if (*pi1Token == '\0')
        return FALSE;

    while (*pi1Token)
    {
        if (!isxdigit (*pi1Token))
        {
            return FALSE;
        }
        if (!(isdigit (*pi1Token)) && (i2Base != 16))
        {
            return FALSE;
        }

        pi1Token++;
    }
    return TRUE;
}

/*
 * This is for "signed integer" type of token 
 */

INT4
mmi_lex_negative_check (INT1 *pi1Token)    /* [0-9]+  but limited to 12 digit */
{
    INT2                i2Base = 10;

    if (*pi1Token == '0')
    {
        pi1Token++;

        if (*pi1Token == 'x' || *pi1Token == 'X')
        {
            pi1Token++;
            i2Base = 16;
        }
        else
            pi1Token--;
    }

    if (*pi1Token == '-')
    {
        pi1Token++;
    }
    if (*pi1Token == '\0')
        return FALSE;

    while (*pi1Token)
    {
        if (!isxdigit (*pi1Token))
        {
            return FALSE;
        }
        if (!(isdigit (*pi1Token)) && (i2Base != 16))
        {
            return FALSE;
        }

        pi1Token++;
    }
    return TRUE;
}

INT4
short_value_function (i1str)
     INT1               *i1str;
{
    UINT4              *ptr;
#ifdef CLI_DEBUG
    mmi_printf ("INT %s\r\n", i1str);
#endif
    ptr = (UINT4 *) (VOID *) i1str;
    if (mmi_atos (i1str, ptr) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef CLI_DEBUG
    mmi_printf ("INT %d\r\n", *i1str);
#endif
    return OSIX_SUCCESS;
}

INT4
integer_value_function (i1str)
     INT1               *i1str;
{
    UINT4              *ptr;
#ifdef CLI_DEBUG
    mmi_printf ("INT %s\r\n", i1str);
#endif
    ptr = (UINT4 *) (VOID *) i1str;
    if (mmi_atol (i1str, ptr) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef CLI_DEBUG
    mmi_printf ("INT %d\r\n", *i1str);
#endif
    return OSIX_SUCCESS;
}

INT4
negative_value_function (i1str)
     INT1               *i1str;
{
    INT4               *ptr;
#ifdef CLI_DEBUG
    mmi_printf ("INT %s\r\n", i1str);
#endif
    ptr = (INT4 *) (VOID *) i1str;
    if (mmi_satoi (i1str, ptr) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef CLI_DEBUG
    mmi_printf ("INT %d\r\n", *i1str);
#endif
    return OSIX_SUCCESS;
}

/* This function is to check the given interface type prefix and 
 * will do tab completion for tokens of type <iftype>.
 * It is assumed that pi1IfTypeStr has sufficient memory allocated 
 * to copy the longest match prefix in it 
 * (i.e sufficient memory is allocated in mmi_convert_value_totype function.
 */
INT4
MmiIfTypeValueFunc (pi1IfTypeStr)
     INT1               *pi1IfTypeStr;
{

#ifdef CLI_DEBUG
    mmi_printf ("iftype %s\r\n", pi1IfTypeStr);
#endif

#ifdef CFA_WANTED
    if (CfaCliValidateInterfaceName (pi1IfTypeStr, pi1IfTypeStr, NULL) ==
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CLI_STRCAT (pi1IfTypeStr, " ");
#else
    UNUSED_PARAM (pi1IfTypeStr);
#endif

#ifdef CLI_DEBUG
    mmi_printf ("iftype %s\r\n", pi1IfTypeStr);
#endif
    return OSIX_SUCCESS;
}

/* This function is to check the given interface type prefix and 
 * will do tab completion for tokens of type <ifXtype>.
 * It is assumed that pi1IfXTypeStr has sufficient memory allocated 
 * to copy the longest match prefix in it 
 * (i.e sufficient memory is allocated in mmi_convert_value_totype function.
 */
INT4
MmiIfXTypeValueFunc (pi1IfXTypeStr)
     INT1               *pi1IfXTypeStr;
{

#ifdef CLI_DEBUG
    mmi_printf ("ifXtype %s\r\n", pi1IfXTypeStr);
#endif

#ifdef CFA_WANTED
    if (CfaCliValidateXInterfaceName (pi1IfXTypeStr, pi1IfXTypeStr, NULL) ==
        CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfXTypeStr, pi1IfXTypeStr, NULL,
                                           NULL) == CLI_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    CLI_STRCAT (pi1IfXTypeStr, " ");
#else
    UNUSED_PARAM (pi1IfXTypeStr);
#endif

#ifdef CLI_DEBUG
    mmi_printf ("ifXtype %s\r\n", pi1IfXTypeStr);
#endif
    return OSIX_SUCCESS;
}

/* This function is to convert the given ip address (ucast,mcast,genereal Ip addr, Ip mask),
 * token inputs in to UINT4 value.
 */
INT4
MmiIpAddrValueFunction (pi1IpAddr)
     INT1               *pi1IpAddr;
{
    UINT4              *pu1Ptr;
    tUtlInAddr          UtlInAddr;

#ifdef CLI_DEBUG
    mmi_printf ("ip-address %s\r\n", pi1IpAddr);
#endif
    pu1Ptr = (UINT4 *) (VOID *) pi1IpAddr;
    if (UtlInetAton ((CONST CHR1 *) pi1IpAddr, &UtlInAddr) == 0)
    {
        return OSIX_FAILURE;
    }
    *pu1Ptr = OSIX_NTOHL (UtlInAddr.u4Addr);
#ifdef CLI_DEBUG
    mmi_printf ("ip-address %0x\r\n", *pu1Ptr);
#endif
    return OSIX_SUCCESS;
}

/*
 * This is for "ucast_mac" type of token
 */

INT4
MmiUCastMACCheck (pi1Token)        /* [a-f0-9]+ but restricted to 12 */
     INT1               *pi1Token;
{
    UINT4               u4StrLen = 0;
    UINT1               au1Mac[CLI_MAC_ADDR_SIZE];
    UINT1               au1ZeroMac[CLI_MAC_ADDR_SIZE];
    INT2                i2dotCount = 0;
    INT2                i2charCount = 0;
    INT2                i2MaxcharCount = 0;
    INT2                i2valid_flag = 0;
    INT1               *pi1TempPtr;

    if (pi1Token == NULL)
    {
        return FALSE;
    }
    u4StrLen = (UINT4) CLI_STRLEN (pi1Token);

    /*  
     * Mac address format may be : 1.2.3.4.5.6 or aa.bb.cc.dd.ee.ff or 
     * aaaa.aaaa.aaaa or a.a.a
     * So min len is 5 and max len is 17. 
     */

    if ((u4StrLen < 5) || (u4StrLen > 17))
    {
        return FALSE;
    }

    /* checks for correct format of Mac Address with dotted notation */
    pi1TempPtr = pi1Token;

    if (pi1TempPtr)
    {
        while (*pi1TempPtr)
        {
            if (isxdigit (*pi1TempPtr))
            {
                i2valid_flag = 1;
            }
            else if (*pi1TempPtr == ':')
            {
                i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
                    (i2MaxcharCount) : (i2charCount);
                if (i2charCount == 3)
                {
                    i2valid_flag = 0;
                }

                i2charCount = -1;
                i2dotCount++;

                if (i2valid_flag)
                {
                    i2valid_flag = 0;
                }
                else
                {
                    break;
                }
            }
            else
            {
                i2valid_flag = 0;
                break;
            }

            i2charCount++;
            pi1TempPtr++;
        }
        if (!i2valid_flag)
        {
            return FALSE;
        }
    }

    i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
        (i2MaxcharCount) : (i2charCount);

    /* Checks whether the given format is in correct format
     * with 6 octests or 3 octests
     */

    if ((i2MaxcharCount > 4) || (i2dotCount == 5 && i2MaxcharCount > 2) ||
        ((i2dotCount != 2) && (i2dotCount != 5)) ||
        i2MaxcharCount == 3 || i2charCount == 3)
    {
        return FALSE;
    }

    StrToMac ((UINT1 *) pi1Token, au1Mac);
    CLI_MEMSET (au1ZeroMac, 0, CLI_MAC_ADDR_SIZE);

    if ((CLI_IS_MCASTMAC (au1Mac) == CLI_SUCCESS) ||
        (CLI_IS_NULLMAC (au1Mac, au1ZeroMac) == CLI_SUCCESS))
        return FALSE;

    return TRUE;
}

/*
 * This is for "mcast_mac" type of token
 */

INT4
MmiMCastMACCheck (pi1Token)        /* [a-f0-9]+ but restricted to 12 */
     INT1               *pi1Token;
{
    UINT4               u4StrLen = 0;
    UINT1               au1Mac[CLI_MAC_ADDR_SIZE];
    INT2                i2dotCount = 0;
    INT2                i2charCount = 0;
    INT2                i2MaxcharCount = 0;
    INT2                i2valid_flag = 0;
    INT1               *pi1TempPtr;

    if (pi1Token == NULL)
    {
        return FALSE;
    }

    u4StrLen = CLI_STRLEN (pi1Token);
    /*  
     * Mac address format may be : 1.2.3.4.5.6 or aa.bb.cc.dd.ee.ff or 
     * aaaa.aaaa.aaaa or a.a.a
     * So min len is 5 and max len is 17. 
     */

    if ((u4StrLen < 5) || (u4StrLen > 17))
    {
        return FALSE;
    }

    /* checks for correct format of Mac Address with dotted notation */
    pi1TempPtr = pi1Token;

    if (pi1TempPtr)
    {
        while (*pi1TempPtr)
        {
            if (isxdigit (*pi1TempPtr))
            {
                i2valid_flag = 1;
            }
            else if (*pi1TempPtr == ':')
            {
                i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
                    (i2MaxcharCount) : (i2charCount);
                i2charCount = -1;
                i2dotCount++;

                if (i2valid_flag)
                {
                    i2valid_flag = 0;
                }
                else
                {
                    break;
                }
            }
            else
            {
                i2valid_flag = 0;
                break;
            }

            i2charCount++;
            pi1TempPtr++;
        }
        if (!i2valid_flag)
        {
            return FALSE;
        }
    }

    i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
        (i2MaxcharCount) : (i2charCount);

    /* Checks whether the given format is in correct format
     * with 6 octests or 3 octests
     */

    if ((i2MaxcharCount > 4) || (i2dotCount == 5 && i2MaxcharCount > 2) ||
        ((i2dotCount != 2) && (i2dotCount != 5)))
    {
        return FALSE;
    }

    StrToMac ((UINT1 *) pi1Token, au1Mac);

    if ((CLI_IS_MCASTMAC (au1Mac) != CLI_SUCCESS))
        return FALSE;

    return TRUE;
}

/*
 * This is for "mac_addr" type of token
 */

INT4
mmi_macaddress_check (p_i1token)    /* [a-f0-9]+ but restricted to 12 */
     INT1               *p_i1token;
{
    UINT4               i = 0;
    UINT4               j = 0;
    INT2                i2valid_flag = 0;

    j = CLI_STRLEN (p_i1token);

    /*  
     * Mac address format may be : 1.2.3.4.5.6 or aa.bb.cc.dd.ee.ff or 
     * aaaa.aaaa.aaaa or a.a.a
     * So min len is 5 and max len is 17. 
     */

    if ((j < 5) || (j > 17))
    {
        return FALSE;
    }

    for (i = 0; i < j; i++)
    {
        if (isxdigit (p_i1token[i]))
        {
            i2valid_flag = 1;
        }
        else if (p_i1token[i] == ':')
        {
            if (i2valid_flag)
            {
                i2valid_flag = 0;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }
    if (!i2valid_flag)
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * This is "iftype" type of token
 */

INT4
MmiIfTypeCheck (pi1IfTypeStr)
     INT1               *pi1IfTypeStr;
{
#ifdef CFA_WANTED
    if (CfaCliValidateInterfaceName (pi1IfTypeStr, NULL, NULL) == CLI_FAILURE)
    {
        return FALSE;
    }
    return TRUE;
#else
    UNUSED_PARAM (pi1IfTypeStr);
    return FALSE;
#endif
}

/*
 * This is "ifXtype" type of token
 */

INT4
MmiIfXTypeCheck (pi1IfXTypeStr)
     INT1               *pi1IfXTypeStr;
{
#ifdef CFA_WANTED
    if (CfaCliValidateXInterfaceName (pi1IfXTypeStr, NULL, NULL) == CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfXTypeStr, NULL, NULL, NULL) ==
            CLI_FAILURE)
        {
            return FALSE;
        }
    }
    return TRUE;
#else
    UNUSED_PARAM (pi1IfXTypeStr);
    return FALSE;
#endif
}

/*
 * This is "string" type of token
 */

INT4
mmi_string_check (p_i1token)    /* ([a-zA-Z\.]+[a-zA-Z0-9\._]*) */
     INT1               *p_i1token;
{
    UINT4               j = 0;
    j = CLI_STRLEN (p_i1token);

    /* Restrict the length to MAX_LINE_LEN characters */
    if (j > MAX_LINE_LEN - 1)
        return FALSE;

    if (*p_i1token == '"')
        return (CliExtractStringFromQuotes ((UINT1 *) p_i1token));

    return TRUE;
}

INT4
mmi_mode_string_check (p_i1token)    /* ([a-zA-Z\./~]+[a-zA-Z0-9\._/~]*) */
     INT1               *p_i1token;
{
    UINT4               i = 0;
    UINT4               j = 0;
    j = CLI_STRLEN (p_i1token);

    /* Restrict the mode string length to MAX_PROMPT_LEN-1 */

    if (j > (MAX_PROMPT_LEN - 1))
    {
        return FALSE;
    }
    if (!isalnum (p_i1token[0]) && p_i1token[0] != '.'
        && p_i1token[0] != '/' && p_i1token[0] != '~')
    {
        return FALSE;
    }
    for (i = 1; i < j; i++)
    {

        if (!isalnum (p_i1token[i]) && p_i1token[i] != '.'
            && p_i1token[i] != '_' && p_i1token[i] != '/'
            && p_i1token[0] != '~' && p_i1token[i] != '-'
            && p_i1token[i] != '\\')
        {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 * This is for "num_str" type of token
 */

INT4
mmi_numstring_check (p_i1token)    /*  [0-9]+ ([\.][0-9]+) +  */
     INT1               *p_i1token;
{
    UINT4               i = 0;
    UINT4               j = 0;
    INT2                i2valid_flag = 0;

    j = CLI_STRLEN (p_i1token);

    /* Restrict the input length to 24 characters 
     * This is an arbitrary value */

    if (j > 64)
    {
        return FALSE;
    }

    for (i = 0; i < j; i++)
    {
        if (isdigit (p_i1token[i]))
        {
            i2valid_flag = 1;
        }
        else if (p_i1token[i] == '.')
        {
            if (i2valid_flag)
            {
                i2valid_flag = 0;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }
    if (!i2valid_flag)
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * This is for "flash_url" type of token
 */

INT4
mmi_flash_url_check (pi1Token)
     INT1               *pi1Token;
{
    if (CliGetFlashFileName (pi1Token, NULL) == CLI_FAILURE)
        return FALSE;
    return TRUE;
}

/*
 * This is for "cust_url" type of token
 */

INT4
mmi_cust_url_check (pi1Token)
     INT1               *pi1Token;
{
    if (CliGetCustFileName (pi1Token, NULL, NULL) == CLI_FAILURE)
        return FALSE;
    return TRUE;
}

/*
 * This is for "tftp_url" type of token
 */

INT4
mmi_tftp_url_check (pi1Token)
     INT1               *pi1Token;
{
    if (CliGetTftpParams (pi1Token, NULL, NULL,NULL) == CLI_FAILURE)
        return FALSE;
    return TRUE;
}

INT4
mmi_sftp_url_check (pi1Token)
     INT1               *pi1Token;
{
    if (CliGetSftpParams (pi1Token, NULL, NULL, NULL, NULL, NULL) == CLI_FAILURE)
        return FALSE;
    return TRUE;
}

/*
 * This is for "quote_str" type of token
 */

INT4
mmi_quoted_string_check (INT1 *pi1Token)
/* [\"]+[a-zA-Z0-9\.\-; : _!\?@#~\{\}\ (\) \\\/ ]+[\"]  */
{
    if (*pi1Token != '"')
        return FALSE;

    pi1Token++;
    while (*pi1Token)
        pi1Token++;

    if (*(pi1Token - 1) != '"')
        return FALSE;

    return TRUE;
}

INT4
mmi_random_string_check (p_i1token)
/* [\"]+[a-zA-Z0-9\.\-; : _!\?@#~\{\}\ (\) \\\/ ]+[\"]  */
     INT1               *p_i1token;
{
    UINT4               j = 0;

    j = CLI_STRLEN (p_i1token);

    /* Restrict the length to MAX_LINE_LEN characters */
    if (j > MAX_LINE_LEN - 1)
    {
        return FALSE;
    }
    return TRUE;
}

/*
 * This is for "oui" type of token
 */

INT4
MmiOuiCheck (p_i1token)            /* [a-f0-9]+ but restricted to 12 */
     INT1               *p_i1token;
{
    UINT4               i = 0;
    UINT4               j = 0;
    INT2                i2valid_flag = 0;

    j = CLI_STRLEN (p_i1token);

    /*  
     * OUI format is aa:bb:cc
     * So len is 8. 
     */

    if (j != 8)
    {
        return FALSE;
    }

    for (i = 0; i < j; i++)
    {
        if (isxdigit (p_i1token[i]))
        {
            if (++i2valid_flag > 2)
            {
                return FALSE;
            }
        }
        else if (p_i1token[i] == ':')
        {
            if (i2valid_flag)
            {
                i2valid_flag = 0;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }
    if (!i2valid_flag)
    {
        return FALSE;
    }

    return TRUE;
}

/*
 *  This is for "hex_str" type of token
 */

INT4
mmi_hexstring_check (p_i1token)    /*  [a-f0-9]+   */
     INT1               *p_i1token;
{
    UINT4               i = 0;
    UINT4               j = 0;

    j = CLI_STRLEN (p_i1token);

    /* Restrict the length to 32 characters */
    if (j > 32)
    {
        return FALSE;
    }

    for (i = 0; i < j; i++)
    {
        if (!(isxdigit (p_i1token[i])))
        {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 *  This function validates "integer64" type token
 */

INT4
MmiInteger64Check (INT1 *pi1Str)
{
    FS_UINT8            u8Val;

    FSAP_U8_CLR (&u8Val);

    if (FSAP_STR2_U8 ((CHR1 *) pi1Str, &u8Val) == 0)
    {
        return FALSE;
    }

    return TRUE;
}

/*
 * This routine will be used to get the token from command
 * after getting the token it will set i1Lex_return_type
 * by calling mmi_get_type_token () .
 *
 * This will be called from mmi_parse () 
 *
 * 1.First will skip white spaces until '\n' or other chracter
 * 2.then it will read the cahracters until one white space or '\n'
 * 3.the characters read will be copied to the token (ie. sideeffect) 
 * 4.Then the token will be passed to mmi_get_type_token () 
 *
 * will return first white space after token if success
 * else -1
 */
INT1
mmi_gettoken (tCliContext * pCliContext, INT1 *pi1UserCmd, INT1 **ppi1RemCmd)
{
    INT2                i2TokenIndex = 0;
    INT1               *pi1Token = NULL;
    INT1                i1Delimit;
    INT1                i1TempDelim = -1;

    for (; pi1UserCmd && *pi1UserCmd;)
    {
        i1Delimit = CliGetNextToken (pi1UserCmd, CLI_EOT_DELIMITS, ppi1RemCmd,
                                     &pi1Token);
        if (i1Delimit == CLI_PIPE_CHAR)
        {
            return (i1Delimit);
        }
        if (pi1Token != NULL && *pi1Token)
        {
            STRCPY (pCliContext->MmiCmdToken[i2TokenIndex++], pi1Token);
            pCliContext->i2TokenCount = i2TokenIndex;
        }
        else if (i1TempDelim == ' ' && i1Delimit == 0)
        {
            i2TokenIndex++;
            pCliContext->i2TokenCount = i2TokenIndex;
        }

        i1TempDelim = i1Delimit;
        if ((i1Delimit = CliIsDelimit (i1Delimit, CLI_EOC_DELIMITS)) != 0)
        {
            if (i1Delimit != TAB)
            {
                pCliContext->i2TokenCount = i2TokenIndex;
                return (i1Delimit);
            }
        }

        pi1UserCmd = *ppi1RemCmd;
    }

    if (i1TempDelim == ' ')
    {
        i2TokenIndex++;
        pCliContext->i2TokenCount = i2TokenIndex;
    }
    return (0);
}

/*
 * This will be called from mmi_gettoken () 
 * to find the type of token
 *
 * By passing the token to all functionptr specified in the
 * mmi_cmd_token[] array it will set the bit in Mmi_i4lex_token_type
 * by set_bit_pos () 
 *
 * it will return Mmi_i4lex_token_type
 */

VOID
mmi_get_type_token (p_i1token, pu8TokenType)
     INT1               *p_i1token;
     FS_UINT8           *pu8TokenType;
{
    INT1                i;

    FSAP_U8_CLR (pu8TokenType);

    for (i = 0; i < MAX_TYPE_TOKEN; i++)
    {
        if (mmi_cmd_token[i].type_check == NULL)
            break;
        if (mmi_cmd_token[i].type_check (p_i1token) == TRUE)
        {
            mmi_lx_set_bit_pos (i, pu8TokenType);
        }
    }
    return;
}

/*
 * This to add the token types in mmi_cmd_token defined by
 * the user.
 * If the user defined type matching with already defined type
 * User type will over write the already defined type.
 * Or it will add at the end of mmi_cmd_token.
 *  It will be called from INIT module
 */

VOID
mmi_rearrange_user_token_types (tCliContext * pCliContext)
{
    INT2                i2_user_type = 0;
    INT2                i2search = 0;

    while (pCliContext->mmi_user_cmd_token[i2_user_type].token_type != NULL)
    {
        i2search = 0;
        while (mmi_cmd_token[i2search].token_type != NULL)
        {
            if (mmi_new_strcmp ((CONST INT1 *) mmi_cmd_token[i2search].
                                token_type,
                                (CONST INT1 *) pCliContext->
                                mmi_user_cmd_token[i2_user_type].token_type) ==
                0)
            {
                mmi_cmd_token[i2search].type_check =
                    pCliContext->mmi_user_cmd_token[i2_user_type].type_check;
                mmi_cmd_token[i2search].value_function =
                    pCliContext->mmi_user_cmd_token[i2_user_type].
                    value_function;
                break;
            }
            i2search++;
        }
        if (mmi_cmd_token[i2search].token_type == NULL)
        {
            mmi_cmd_token[i2search].token_type =
                pCliContext->mmi_user_cmd_token[i2_user_type].token_type;
            mmi_cmd_token[i2search].type_check =
                pCliContext->mmi_user_cmd_token[i2_user_type].type_check;
            mmi_cmd_token[i2search].value_function =
                pCliContext->mmi_user_cmd_token[i2_user_type].value_function;
            mmi_cmd_token[i2search + 1].token_type = NULL;
            mmi_cmd_token[i2search + 1].type_check = NULL;
            mmi_cmd_token[i2search + 1].value_function = NULL;
        }
        if (i2search >= MAX_TYPE_TOKEN - 1)
        {
            mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOO_MANY_LEX_TYPES]);
            exit (0);
        }
        i2_user_type++;
    }
    return;
}

INT4
MmiIpMaskCheck (pi1Token)
     INT1               *pi1Token;
{
#define   CLI_CFA_MAX_CIDR                   32
    UINT1               u1Counter;
    tUtlInAddr          InAddr;

    /* table for mapping CIDR numbers to IP subnet masks - from RFC 1878 */
    UINT4               u4CidrSubnetMask[CLI_CFA_MAX_CIDR + 1] = {
        0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
        0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
        0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
        0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
        0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
        0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
    };

    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */
    if (UtlInetAton ((CONST CHR1 *) pi1Token, &InAddr) == 0)
    {
        return FALSE;
    }

    /* Check whether OSIX_NTOHL of InAddr.u4Addr is needed or not */
    InAddr.u4Addr = OSIX_NTOHL (InAddr.u4Addr);

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CLI_CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == InAddr.u4Addr)
        {
            break;
        }
    }

    if (u1Counter > CLI_CFA_MAX_CIDR)
    {
        return FALSE;
    }

    return TRUE;
}

INT4
MmiIpCiscoMaskCheck (pi1Token)
     INT1               *pi1Token;
{
#define   CLI_CFA_MAX_CIDR                   32
    UINT1               u1Counter;
    tUtlInAddr          InAddr;

    /* table for mapping CIDR numbers to IP subnet masks - from RFC 1878 */
    UINT4               u4CidrSubnetMask[CLI_CFA_MAX_CIDR + 1] = {
        0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
        0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
        0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
        0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
        0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
        0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
    };

    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */
    if (UtlInetAton ((CONST CHR1 *) pi1Token, &InAddr) == 0)
    {
        return FALSE;
    }

    InAddr.u4Addr = OSIX_NTOHL (InAddr.u4Addr);

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= CLI_CFA_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == InAddr.u4Addr)
        {
            break;
        }
    }

    if (u1Counter > CLI_CFA_MAX_CIDR)
    {
        InAddr.u4Addr = (UINT4) (((InAddr.u4Addr & 0xFF000000) >> 24) |
                                 ((InAddr.u4Addr & 0x00FF0000) >> 8) |
                                 ((InAddr.u4Addr & 0x0000FF00) << 8) |
                                 ((InAddr.u4Addr & 0x000000FF) << 24));
        for (u1Counter = 0; u1Counter <= CLI_CFA_MAX_CIDR; ++u1Counter)
        {
            if (u4CidrSubnetMask[u1Counter] == InAddr.u4Addr)
            {
                break;
            }
        }
    }

    if (u1Counter > CLI_CFA_MAX_CIDR)
    {
        return FALSE;
    }

    return TRUE;
}

INT4
MmiIpAddrCheck (pi1Token)
     INT1               *pi1Token;
{
    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */
    if (UtlInetAton ((CONST CHR1 *) pi1Token, NULL))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

INT1
CliGetNextToken (pi1Command, pi1Delim, ppi1RemCommand, ppi1Token)
     INT1               *pi1Command;
     CONST CHR1         *pi1Delim;
     INT1              **ppi1RemCommand;
     INT1              **ppi1Token;
{

    INT1                i1Delimit = 0;
    INT1               *pi1TempDelimit = NULL;

    *ppi1Token = NULL;

    if (!pi1Command)
        return (i1Delimit);

    /* Discarding the initial spaces */
    while ((CliIsDelimit (*pi1Command, " ")))
        pi1Command++;

    *ppi1Token = pi1Command;

    if (!(CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1TempDelimit, INT1)))
    {
        return (0);
    }
    STRCPY (pi1TempDelimit, pi1Delim);

    while ((i1Delimit = CliIsDelimit (*pi1Command,
                                      (CHR1 *) pi1TempDelimit)) == 0
           && *pi1Command)
    {
        if (*pi1Command == '"')
            STRCPY (pi1TempDelimit, CLI_QUOTE_DELIMITS);
        pi1Command++;
    }

    while ((i1Delimit = CliIsDelimit (*pi1Command, pi1Delim)) == 0 &&
           *pi1Command)
    {
        pi1Command++;
    }

    if (i1Delimit == CLI_PIPE_CHAR)
    {
        /* '|' is not a token */
        *ppi1RemCommand = pi1Command + 1;

        if (!**ppi1RemCommand)
            *ppi1RemCommand = NULL;
    }
    else if (!*pi1Command)
    {
        *ppi1RemCommand = NULL;
    }
    else
    {
        /* Include the ending '"' */
        if (*pi1Command == '"')
        {
            pi1Command++;
        }
        *pi1Command = '\0';
        *ppi1RemCommand = pi1Command + 1;
        if (!**ppi1RemCommand)
            *ppi1RemCommand = NULL;
    }

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TempDelimit);
    return (i1Delimit);
}

INT4
CliExtractStringFromQuotes (UINT1 *pu1QuoteStr)
{
    UINT1              *pu1TempStr;
    INT2                i2StrLen;
    tCliContext        *pCliContext = NULL;

    pCliContext = CliGetContext ();

    if (pCliContext == NULL)
    {
        return CLI_FAILURE;
    }

    i2StrLen = (INT2) CLI_STRLEN (pu1QuoteStr);
    if (!i2StrLen)
    {
        pu1QuoteStr = NULL;
        return (TRUE);
    }

    if ((i2StrLen == 1) || (pu1QuoteStr[i2StrLen - 1] != '"'))
    {
        if ((pCliContext->i1HelpFlag == 1) && (pCliContext->i1SpaceFlag != 1))
        {
            return (TRUE);
        }
        return (FALSE);
    }

    pu1TempStr = CLI_BUDDY_ALLOC (i2StrLen + 1, UINT1);

    if (pu1TempStr == NULL)
    {
        return (FALSE);
    }
    SNPRINTF ((CHR1 *) pu1TempStr, i2StrLen - 1, "%s", &pu1QuoteStr[1]);
    STRCPY (pu1QuoteStr, pu1TempStr);

    CLI_BUDDY_FREE (pu1TempStr);
    return (TRUE);
}

/*
 * This is for "float" type of token
 */

INT4
mmi_lex_float_check (INT1 *pi1Token)    /* [0-9]+(.[0-9]+)? */
{
    INT2                i2dotCount = 0;
    INT2                i2charCount = 0;

    while (*pi1Token)
    {
        if (*pi1Token == '.')
            i2dotCount++;

        if ((!isdigit (*pi1Token) && (*pi1Token != '.')) || (i2dotCount > 1))
        {
            return FALSE;
        }

        pi1Token++;
        i2charCount++;
    }

    if ((i2charCount - i2dotCount) <= 0)
    {
        return FALSE;
    }

    return TRUE;
}

INT4
float_value_function (pi1str)
     INT1               *pi1str;
{
    DBL8               *ptr;
#ifdef CLI_DEBUG
    mmi_printf ("DBL %s\r\n", pi1str);
#endif
    ptr = (DBL8 *) (VOID *) pi1str;
    if (mmi_atof (pi1str, ptr) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef CLI_DEBUG
    mmi_printf ("DBL %g\r\n", *pi1str);
#endif
    return OSIX_SUCCESS;
}

INT4
MmiUCastAddrCheck (pi1Token)
     INT1               *pi1Token;
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */
    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_E (u4IpAddr))))
        {
            return FALSE;
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

INT4
MmiLUCastAddrCheck (pi1Token)
     INT1               *pi1Token;
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */
    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_E (u4IpAddr)) ||
              (CLI_IS_ADDR_LOOPBACK (u4IpAddr))))
        {
            return FALSE;
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

INT4
MmiIfaceListCheck (pi1Token)
     INT1               *pi1Token;
{
    tCliContext        *pCliContext = NULL;
    INT4                i4RetStatus = FALSE;
    UINT1               au1Ptr[MAX_CHAR_IN_TOKEN];
    UINT4               u4Len = 0;

    MEMSET (au1Ptr, 0, sizeof (au1Ptr));
    /* Return TRUE, If the list matches if any of 
     * physical interface list or Port channel list
     */
    if (MmiPortListCheck (pi1Token) == TRUE)
        return TRUE;

    if (!(pi1Token) || !(*pi1Token))
        return FALSE;

    u4Len =
        ((STRLEN (pi1Token) <
          sizeof (au1Ptr)) ? STRLEN (pi1Token) : sizeof (au1Ptr) - 1);
    CLI_STRNCPY (au1Ptr, pi1Token, u4Len);
    au1Ptr[u4Len] = '\0';

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return CLI_FAILURE;
    }

    ++pi1Token;
    if ((*pi1Token == '/') && (*(++pi1Token) == '\0'))
    {
        if ((pCliContext->i1HelpFlag == 1) && (pCliContext->i1SpaceFlag != 1))
            return TRUE;
    }

    if (CliStrToIfaceList (au1Ptr, NULL, 0, 0) != OSIX_FAILURE)
    {
        i4RetStatus = TRUE;
    }

    return i4RetStatus;
}

INT4
MmiPortListCheck (pi1Token)
     INT1               *pi1Token;
{
    INT4                i4RetStatus = FALSE;
    UINT1               au1Ptr[MAX_CHAR_IN_TOKEN];
    UINT4               u4Len = 0;

    MEMSET (au1Ptr, 0, sizeof (au1Ptr));

    if (!(pi1Token) || !(*pi1Token))
        return FALSE;

    u4Len =
        ((STRLEN (pi1Token) <
          sizeof (au1Ptr)) ? STRLEN (pi1Token) : sizeof (au1Ptr) - 1);
    CLI_STRNCPY (au1Ptr, pi1Token, u4Len);
    au1Ptr[u4Len] = '\0';

    if (CliStrToPortList (au1Ptr, NULL, 0, 0) != OSIX_FAILURE)
    {
        i4RetStatus = TRUE;
    }

    return i4RetStatus;
}

INT4
MmiIfNumCheck (pi1Token)
     INT1               *pi1Token;
{
    tCliContext        *pCliContext = NULL;
    UINT2               u2StrLen = 0;
    UINT2               u2Index = 0;
    UINT4               u4SlotNum = 0;
    UINT4               u4PortNum = 0;
    BOOL1               bIsDigitFound = FALSE;
    INT1               *pi1Ptr;

    if (!(pi1Token) || !(*pi1Token))
        return FALSE;
    if (*pi1Token == '-')
    {
        pi1Token++;
    }
    u2StrLen = (UINT2) CLI_STRLEN (pi1Token);

    pi1Ptr = &pi1Token[u2Index];
    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1Token[u2Index])))
        {
            if ((pi1Token[u2Index] != '/') || (bIsDigitFound == FALSE))
                return FALSE;
            else
                break;
        }
        bIsDigitFound = TRUE;
    }

    /* If it is only an index then return TRUE */
    if (!(pi1Token[u2Index]))
    {
        if (mmi_atol (pi1Ptr, &u4PortNum) == CLI_FAILURE)
            return FALSE;
        return TRUE;
    }

    if ((pi1Token[u2Index] != '/'))
        return FALSE;

    pi1Token[u2Index] = '\0';

    if (mmi_atol (pi1Ptr, &u4SlotNum) == CLI_FAILURE)
    {
        pi1Token[u2Index] = '/';
        return FALSE;
    }
    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return CLI_FAILURE;
    }

    pi1Token[u2Index] = '/';

    u2Index++;
    pi1Ptr = &pi1Token[u2Index];
    if (*pi1Ptr == '\0')
    {
        if ((pCliContext->i1HelpFlag == 1) && (pCliContext->i1SpaceFlag != 1))
        {
            return TRUE;
        }
    }
    bIsDigitFound = FALSE;

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1Token[u2Index])))
        {
            return FALSE;
        }
        bIsDigitFound = TRUE;
    }
    if (bIsDigitFound == FALSE)
        return FALSE;
    if (mmi_atol (pi1Ptr, &u4PortNum) == CLI_FAILURE)
    {
        return FALSE;
    }
    return TRUE;
}

INT4
MmiTimeCheck (pi1Token)
     INT1               *pi1Token;
{
    UINT1               u1Index = 0;
    UINT1               u1Counter = 0;
    UINT1               u1DotCount = 0;
    UINT1               u1StrLen = 0;
    UINT1               u1Hrs = 0;
    UINT1               u1Mins = 0;
    UINT1               u1Secs = 0;

    u1StrLen = (UINT1) CLI_STRLEN (pi1Token);

    /* Accepted formats hh:mm:ss */

    if ((u1StrLen < 5) || (u1StrLen > 8))
        return FALSE;

    for (u1Index = 0; u1Index < u1StrLen; u1Index++)
    {
        if (isdigit (pi1Token[u1Index]))
        {
            u1Counter++;
        }
        else if ((pi1Token[u1Index] == ':') && (u1Counter) && (u1Counter <= 2))
        {
            u1Counter = 0;
            u1DotCount++;
        }
        else
        {
            return FALSE;
        }
    }
    if ((u1DotCount != 2) || !(u1Counter))
    {
        return FALSE;
    }

    CliSplitTimeToken (pi1Token, &u1Hrs, &u1Mins, &u1Secs);

    if ((u1Hrs >= 24) || (u1Mins > 59) || (u1Secs > 59))
    {
        return FALSE;
    }

    return TRUE;
}

INT4
MmiMCastAddrCheck (pi1Token)
     INT1               *pi1Token;
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    /* UtlInetAton validates the given ip address
     * and returns 1 if it is a valid ip address
     * and returns 0 if not */

    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        u4IpAddr = ((u4IpAddr & 0xff000000) >> 24);
        if ((u4IpAddr < 224) || (u4IpAddr >= 240))
        {
            return FALSE;
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/***************************************************************************  
 * FUNCTION NAME : MmiIp6AddrCheck
 * DESCRIPTION   : This function validates the given IPv6 address type tokens
 *                 
 * INPUT         : pi1Token  - pointer to the IPv6 address token string.
 * 
 * RETURN VALUE  : TRUE if, the token is a valid IPv6 address, else FALSE.
 ***************************************************************************/

INT4
MmiIp6AddrCheck (INT1 *pi1Token)    /* ([0-9A-Fa-f:]+[0-9.]*) */
{
    INT4                i4RetVal = FALSE;
    INT1                i1ch;

    /* INET_ATON6 validates the given ipv6 address
     * and returns 1 if it is a valid ipv6 address
     * and returns 0 if not */

    if ((INET_ATON6 ((CONST CHR1 *) pi1Token, NULL)) > 0)
    {
        i4RetVal = TRUE;
        while ((i1ch = *pi1Token++))
        {
            if ((isspace (i1ch)))
            {
                while (isspace (*pi1Token))
                    pi1Token++;
                if ((*pi1Token != '\0'))
                {
                    i4RetVal = FALSE;
                    break;
                }
                else
                    break;        /* end of token reached */
            }
            if ((i1ch != ':') && (i1ch != '.') && (!isxdigit (i1ch)))
            {
                i4RetVal = FALSE;
                break;
            }
        }
    }

    return (i4RetVal);

}

/***************************************************************************
 * FUNCTION NAME : MmiVlanVfiIdValidate
 * DESCRIPTION   : This function validates the given VLAN identifier or
 *                 Virtual Forward Instance
 *
 * INPUT         : pi1Token  - pointer to the VFI token string.
 *
 * RETURN VALUE  : TRUE if, the token is a valid VFI Id, else FALSE.
 ***************************************************************************/

INT4
MmiVlanVfiIdValidate (INT1 *pi1Token)
{
    INT1               *pi1Ptr = pi1Token;
    UINT4               u4VfiId = 0;
    UINT2               u2StrLen = 0;
    UINT2               u2Index = 0;

    if (!(pi1Token) || !(*pi1Token))
    {
        return FALSE;
    }

    u2StrLen = (UINT2) CLI_STRLEN (pi1Token);

    for (u2Index = 0; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (*pi1Ptr)))
        {
            return FALSE;
        }
        pi1Ptr++;
    }

    if (mmi_atol (pi1Token, &u4VfiId) == CLI_FAILURE)
    {
        return FALSE;
    }
    if ((u4VfiId < CLI_MIN_VLAN_VFI_ID) || (u4VfiId > CLI_MAX_VLAN_VFI_ID))
    {
        return FALSE;
    }

    /* VLAN 4095, VLAN 4096, VLAN 4097 and VLAN 65535 are not configurable */
    if (IS_STANDARD_VLAN_ID (u4VfiId))
    {
        return FALSE;
    }

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME : MmiIpIfTypeCheck
 * DESCRIPTION   : This function validates the IP interface type
 *
 * INPUT         : pi1Token  - pointer to the VFI token string.
 *
 * RETURN VALUE  : TRUE if, the token is a valid IP interface, else FALSE.
 ***************************************************************************/
INT4
MmiIpIfTypeCheck (pi1IfTypeStr)
     INT1               *pi1IfTypeStr;
{
#ifdef CFA_WANTED
    if (CfaCliValidateIpInterfaceName (pi1IfTypeStr, NULL, NULL) == CLI_FAILURE)
    {
        return FALSE;
    }
    return TRUE;
#else
    UNUSED_PARAM (pi1IfTypeStr);
    return FALSE;
#endif
}
/***************************************************************************
 * FUNCTION NAME : MmiHostNameCheck
 * DESCRIPTION   : This function validates the Host name(without any special 
 *                 characters except dot.)
 *
 * INPUT         : pi1Token  - pointer to the received host name.
 *
 * RETURN VALUE  : TRUE if, the token is a valid host name, else FALSE.
 ***************************************************************************/
INT4
MmiHostNameCheck (INT1 *pi1Hostname)
{
    UINT1               u1AlphaPresentFlag = 0;
    INT1                i1ch = 0;
    UINT4               u4Ret = 0;
    UINT1               u1SpecialFlag = 0;
    /*to restrict user entering string more than 255 characters */
    u4Ret = CLI_STRLEN (pi1Hostname);

    if (u4Ret > DNS_MAX_QUERY_LEN)
    {
        return FALSE;
    }

    i1ch = *pi1Hostname;

    u4Ret = ISALPHA (i1ch);
    /*first character must be a alphabet
     * as per RFC 1035 section 2.3.1*/
    if (u4Ret == 0)
    {
        return FALSE;
    }

    while ((i1ch = *pi1Hostname))
    {
        u4Ret = ISALPHA (i1ch);
        
        if (u4Ret == 0)
        {
            /*character is not a alphabet*/
            u4Ret = 0;
            u4Ret = ISDIGIT (i1ch);

            if (u4Ret == 0)
            {
                /*to check whether previous character is 
                 * a special character and  next character also special character*/
                if (u1SpecialFlag == 1)
                {
                    return FALSE;
                }
                /*its not a digit*/
                if ((i1ch != '.') && (i1ch != '-'))
                {
                    /*if the entered string is not an alphabet or digit or . 
                     * then return failure*/
                    return FALSE;
                }
                else
                {
                    u1SpecialFlag = 1;
                }

            }
            else
            {
                /*resetting the and u1SpecialFlag, since 
                 * the character is digit*/
                u1SpecialFlag = 0;
            }
        }
        else
        {
            /*resetting the u1SpecialFlag, since 
             * the character is alphabet*/
            u1SpecialFlag = 0;
            u1AlphaPresentFlag = 1;
        }
        pi1Hostname++;
    }

    /* should have atleast one alphabet somewhere */
    if (u1AlphaPresentFlag != 1)
    {
        return FALSE;
    }

    return TRUE;

}
/***************************************************************************
 * FUNCTION NAME : MmiIfxNumCheck
 * DESCRIPTION   : This function validates the interface number
 *
 * INPUT         : pi1Token  - pointer to the VFI token string.
 *
 * RETURN VALUE  : TRUE if, the token is a valid interface number, else FALSE.
 ***************************************************************************/
INT4
MmiIfxNumCheck (pi1Token)
     INT1               *pi1Token;
{
    tCliContext        *pCliContext = NULL;
    UINT2               u2StrLen = 0;
    UINT2               u2Index = 0;
    UINT2               u2Idx = 0;
    UINT2               u2SubLen = 0;
    UINT4               u4SlotNum = 0;
    UINT4               u4PortNum = 0;
    BOOL1               bIsDigitFound = FALSE;
    BOOL1               bIsSubFlag = FALSE;
    INT1               *pi1Ptr = NULL;
    INT1               *pi1SubStr = NULL;
    INT1                ai1Token[MAX_CHAR_IN_TOKEN];

    MEMSET (ai1Token, '\0', MAX_CHAR_IN_TOKEN);
    if (!(pi1Token) || !(*pi1Token))
        return FALSE;
    if (*pi1Token == '-')
    {
        pi1Token++;
    }
    u2StrLen = (UINT2) CLI_STRLEN (pi1Token);

    pi1Ptr = &pi1Token[u2Index];
    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1Token[u2Index])))
        {
            if ((pi1Token[u2Index] != '/') || (bIsDigitFound == FALSE))
                return FALSE;
            else
                break;
        }
        bIsDigitFound = TRUE;
    }

    /* If it is only an index then return TRUE */
    if (!(pi1Token[u2Index]))
    {
        if (mmi_atol (pi1Ptr, &u4PortNum) == CLI_FAILURE)
            return FALSE;
        return TRUE;
    }

    if ((pi1Token[u2Index] != '/'))
        return FALSE;

    pi1Token[u2Index] = '\0';

    /* u4SlotNum is SlotNumber */
    if (mmi_atol (pi1Ptr, &u4SlotNum) == CLI_FAILURE)
    {
        pi1Token[u2Index] = '/';
        return FALSE;
    }
    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        pi1Token[u2Index] = '/';
        return CLI_FAILURE;
    }

    pi1Token[u2Index] = '/';

    u2Index++;
    pi1Ptr = &pi1Token[u2Index];
    /* If the char "." exists in the interface index then it is sub-channel interface*/
    if (CLI_STRSTR (pi1Ptr, ".") != NULL)
    {
        bIsSubFlag = TRUE;
        u2SubLen = (UINT2) CLI_STRLEN (CLI_STRSTR (pi1Ptr, "."));
    }
    if (*pi1Ptr == '\0')
    {
        if ((pCliContext->i1HelpFlag == 1) && (pCliContext->i1SpaceFlag != 1))
        {
            return TRUE;
        }
    }
    /*Separating the parent and child channel indices*/ 
    if (bIsSubFlag ==  TRUE)
    {
        for (u2Idx = 0 ; u2Idx < u2StrLen; u2Idx++)
        {
            if (pi1Ptr[u2Idx] == '.')
            {
                u2StrLen = (UINT2)(u2StrLen - u2SubLen);
                MEMCPY(ai1Token, pi1Ptr, u2StrLen); 
                ai1Token[u2Idx] = '\0';
                u2Idx++;
                pi1SubStr = &pi1Ptr[u2Idx];
                break;
            }

        }

        bIsDigitFound = FALSE;
        if ((pi1SubStr) == NULL)
        {
            return FALSE;
        }
        for (u2Idx = 0; pi1SubStr[u2Idx] != '\0' ; u2Idx++)
        {
            if (!(isdigit (pi1SubStr[u2Idx])))
            {
                /* If Logical index is not an integer return false*/
                return FALSE;
            }
            bIsDigitFound = TRUE;
        }
        /* if no digit is found after "." return False*/
        pCliContext = CliGetContext ();
        if (pCliContext == NULL)
        {
            return CLI_FAILURE;
        }
    
        if (*pi1SubStr == '\0')
        {
            if ((pCliContext->i1HelpFlag == 1) && (pCliContext->i1SpaceFlag != 1))
                return TRUE;
        }
        if (bIsDigitFound == FALSE)
            return FALSE;
    
        /* u4PortNum is Logical PortNum */
        if (mmi_atol (pi1SubStr, &u4PortNum) == CLI_FAILURE)
        {
            return FALSE;
        }
        /* Maximum supported l3subinterfaces */
        if ((u4PortNum > SYS_DEF_MAX_L3SUB_IFACES) || (u4PortNum == 0))
        {
            return TRUE;
        }

    } 
    bIsDigitFound = FALSE;

    for (; u2Index < u2StrLen; u2Index++)
    {
        if (!(isdigit (pi1Token[u2Index])))
        {
            return FALSE;
        }
        bIsDigitFound = TRUE;
    }
    if (bIsDigitFound == FALSE)
        return FALSE;
        
    /* u4PortNum is Physical Port number*/
    if (mmi_atol (ai1Token, &u4PortNum) == CLI_FAILURE)
    {
        return FALSE;
    }
    return TRUE;
}
