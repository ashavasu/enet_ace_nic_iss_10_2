/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clichmod.c,v 1.33 2015/02/06 10:44:55 siva Exp $
 *
 * Description: This file will have the routines to change mode 
 *              and mode configuration routines                 
 *******************************************************************/

#include "clicmds.h"

extern tCliSessions gCliSessions;
extern INT4         gCliRmRole;
extern tMemPoolId   gCliMaxLineMemPoolId;

/** protos **/
static INT4         mmicm_change_mode_continue (tCliContext *, CONST CHR1 *);

/*
 * This routine will change the mode according to the path specified
 * when changing the mode the configuration parameters will be stored in
 * temporary buffer. If it is successful these should be copied into 
 * permanent configuration buffer after calling this routine 
 *
 * cases:
 *     1.chmod from root by giving path
 *     2.chmod from current mode
 *     3.chmod from root without giving path as (~ starts) 
 *
 * 
 */

INT4
CliChmod (tCliContext * pCliContext, CONST CHR1 * pPath)
{
    if (gCliSessions.p_mmi_root_mode == NULL)
    {
        /* checking the root */
        return CLI_FAILURE;
    }

    if (!STRCMP (pPath, "/"))
    {
        mmicm_copy_root_to_curconfig (pCliContext);
        return CLI_SUCCESS;
    }

    if (mmicm_change_mode_continue (pCliContext, pPath) == (INT4) CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliSetDisplayPrompt (pCliContext);
    return CLI_SUCCESS;
}

/*
 * This routine will change the temporary mode
 *
 * cases
 *            1. if .. to the root of temp_current_mode
 *            2. . no change
 *            3. if some name search the child of current temp mode
 *               which has the prompt pName
 *
 */

INT4
mmicm_change_mode_continue (pCliContext, pName)
     tCliContext        *pCliContext;
     CONST CHR1         *pName;
{
    t_MMI_MODE_TREE    *p_t_temp_mode = NULL;
    t_MMI_MODE_TREE    *pCurMode = NULL;
    t_MMI_MODE_TREE    *pTempLinkMode = NULL;
    INT1               *pi1Pos = NULL;
    INT1               *pi1TmpPos = NULL;
    INT1                ai1PromptStr[MAX_PROMPT_LEN];
    INT1                ai1DispStr[MAX_PROMPT_LEN];
    INT1                ai1TempPathStr[MAX_PROMPT_LEN];
    INT1                ai1Temp[MAX_PROMPT_LEN];
    UINT1               u1RetVal;
    UINT1               u1Size = 0;
    UINT1               u1Len = 0;
    UINT4               u4Len = 0;

    MEMSET (ai1TempPathStr, 0, MAX_PROMPT_LEN);
    MEMSET (ai1Temp, 0, MAX_PROMPT_LEN);
    MEMSET (ai1PromptStr, 0, MAX_PROMPT_LEN);
    MEMSET (ai1DispStr, 0, MAX_PROMPT_LEN);

    u4Len =
        ((STRLEN (pName) <
          sizeof (ai1TempPathStr)) ? STRLEN (pName) : sizeof (ai1TempPathStr) -
         1);
    STRNCPY (ai1TempPathStr, pName, u4Len);
    ai1TempPathStr[u4Len] = '\0';

    if (!mmi_new_strcmp ((CONST INT1 *) pName, (CONST INT1 *) ".."))
    {
        pCliContext->ai1ModePath[sizeof (pCliContext->ai1ModePath) - 1] = '\0';
        if ((CLI_STRLEN (pCliContext->ai1ModePath) > 1) &&
            (CLI_STRLEN (pCliContext->ai1ModePath) < MAX_PROMPT_LEN))
        {
            if ((pi1Pos = (INT1 *) STRRCHR (pCliContext->ai1ModePath,
                                            '/')) == NULL)
            {
                return CLI_FAILURE;
            }

            *pi1Pos = '\0';

            pCurMode = CliGetMode (pCliContext->pMmiCur_mode, NULL,
                                   pCliContext->ai1ModePath);
            if (pCurMode != NULL)
                pCliContext->pMmiCur_mode = pCurMode;
            else
            {
                if (pCliContext->pMmiCur_mode != NULL)
                {
                    pCliContext->pMmiCur_mode =
                        pCliContext->pMmiCur_mode->pParent;
                }
                else
                {
                    return CLI_FAILURE;
                }
            }
        }
        else
        {
            if (pCliContext->pMmiCur_mode != NULL)
            {
                pCliContext->pMmiCur_mode = pCliContext->pMmiCur_mode->pParent;
            }
            else
            {
                return CLI_FAILURE;
            }
        }

        if (pCliContext->pMmiCur_mode == gCliSessions.p_mmi_root_mode)
        {
            mmicm_copy_root_to_curconfig (pCliContext);
            return CLI_SUCCESS;
        }
        pCliContext->Mmi_i1Cur_prompt[sizeof (pCliContext->Mmi_i1Cur_prompt) -
                                      1] = '\0';
        if ((CLI_STRLEN (pCliContext->Mmi_i1Cur_prompt) > 1)
            && (CLI_STRLEN (pCliContext->Mmi_i1Cur_prompt) < MAX_PROMPT_LEN))
        {
            if ((pi1Pos = (INT1 *) STRRCHR (pCliContext->Mmi_i1Cur_prompt,
                                            '/')) == NULL)
            {
                return CLI_FAILURE;
            }
            *pi1Pos = '\0';
        }
        else
        {
            CliGetPromptStr (gCliSessions.p_mmi_root_mode,
                             pCliContext->Mmi_i1Cur_prompt);

        }
        if (pCliContext->Mmi_i1Cur_modelevel != 0)
        {
            pCliContext->Mmi_i1Cur_modelevel--;
        }
        CliSetDisplayPrompt (pCliContext);
    }
    else if ((mmi_new_strcmp ((CONST INT1 *) pName, (CONST INT1 *) ".")) != 0)
    {
        if ((pCliContext->pMmiCur_mode) != NULL)
        {
            p_t_temp_mode = pCliContext->pMmiCur_mode->pChild;
        }

        while (p_t_temp_mode)
        {
            if (p_t_temp_mode->u4ModeType == CLI_LINK_MODE)
            {
                pTempLinkMode = p_t_temp_mode;
                p_t_temp_mode = p_t_temp_mode->pChild;
            }

            if (p_t_temp_mode->PromptInfo.u1PromptType == PROMPT_FUN)
            {

                if (gCliRmRole == GO_ACTIVE)
                {
                    if ((p_t_temp_mode->PromptInfo.PromptFunc ((INT1 *)
                                                               ai1TempPathStr,
                                                               ai1DispStr)) ==
                        TRUE)
                    {
                        break;
                    }
                }
                else
                {
                    if ((CLI_STRCMP (p_t_temp_mode->pMode_name, "EXEC") != 0))
                    {
                        return CLI_SUCCESS;
                    }
                    if (!mmi_new_strcmp
                        (p_t_temp_mode->pMode_name,
                         (CONST INT1 *) ai1TempPathStr))
                    {
                        pCliContext->pMmiCur_mode = p_t_temp_mode;
                        return CLI_SUCCESS;
                    }
                    if ((p_t_temp_mode->PromptInfo.PromptFunc ((INT1 *)
                                                               ai1TempPathStr,
                                                               ai1DispStr)) ==
                        TRUE)
                    {
                        pCliContext->pMmiCur_mode = p_t_temp_mode;
                        return CLI_SUCCESS;
                    }
                }

            }
            else if (!mmi_strcmpi ((CONST INT1 *) pName,
                                   (INT1 *) p_t_temp_mode->PromptInfo.
                                   PromptStr))
            {
                break;
            }
            if (pTempLinkMode)
            {
                p_t_temp_mode = pTempLinkMode;
                pTempLinkMode = NULL;
            }
            p_t_temp_mode = p_t_temp_mode->pNeighbour;
        }

        if (p_t_temp_mode == NULL)
        {
            return CLI_FAILURE;
        }

        MEMSET (ai1PromptStr, 0, sizeof (ai1PromptStr));
        if ((pCliContext->pMmiCur_mode != gCliSessions.p_mmi_root_mode) ||
            (gCliSessions.bIsClCliEnabled == OSIX_TRUE))
        {
            STRCPY (ai1PromptStr, "/");
        }
        pCliContext->pMmiCur_mode = p_t_temp_mode;
        if (pCliContext->pMmiCur_mode->PromptInfo.u1PromptType == PROMPT_FUN)
        {

            u1RetVal = pCliContext->pMmiCur_mode->PromptInfo.PromptFunc
                ((INT1 *) ai1TempPathStr, ai1DispStr);
            if (u1RetVal == FALSE)
            {
                return CLI_FAILURE;
            }
            SNPRINTF ((CHR1 *) ai1Temp, (sizeof (ai1Temp) - 1), "%s",
                      ai1DispStr);
            u1Len = STRLEN (ai1PromptStr);
            u1Size = (sizeof (ai1PromptStr) - 1) - u1Len;
            pi1TmpPos = &(ai1PromptStr[u1Len]);
            STRNCPY (pi1TmpPos, ai1Temp, u1Size);
            ai1PromptStr[sizeof (ai1PromptStr) - 1] = '\0';
        }
        else
        {
            SNPRINTF ((CHR1 *) ai1Temp, (sizeof (ai1Temp) - 1), "%s",
                      pCliContext->pMmiCur_mode->PromptInfo.PromptStr);
            u1Len = STRLEN (ai1PromptStr);
            u1Size = (sizeof (ai1PromptStr) - 1) - u1Len;
            pi1TmpPos = &(ai1PromptStr[u1Len]);
            STRNCPY (pi1TmpPos, ai1Temp, u1Size);
            ai1PromptStr[sizeof (ai1PromptStr) - 1] = '\0';
        }
        if (!pTempLinkMode)
        {
            pTempLinkMode = pCliContext->pMmiCur_mode;
        }
        STRCPY (ai1TempPathStr, "/");
        SNPRINTF ((CHR1 *) ai1Temp, (sizeof (ai1Temp) - 1), "%s%s",
                  pTempLinkMode->pMode_name, "/");
        u1Len = STRLEN (ai1TempPathStr);
        u1Size = (sizeof (ai1TempPathStr) - 1) - u1Len;
        pi1TmpPos = &(ai1TempPathStr[u1Len]);
        STRNCPY (pi1TmpPos, ai1Temp, u1Size);
        ai1TempPathStr[sizeof (ai1TempPathStr) - 1] = '\0';

        if ((STRSTR (pCliContext->ai1ModePath, ai1TempPathStr)) != NULL)
        {
            return CLI_FAILURE;
        }

        if ((STRLEN (pCliContext->Mmi_i1Cur_prompt) + STRLEN (ai1PromptStr))
            >= MAX_PROMPT_LEN)
        {
            mmi_printf
                ("%% The length of the Cli prompt is greater than Maximum prompt Length\n");
            return CLI_FAILURE;
        }

        STRCAT (pCliContext->Mmi_i1Cur_prompt, ai1PromptStr);

	STRCAT (pCliContext->ai1ModePath, "/");
	u4Len =
		(UINT4) ((sizeof(pCliContext->ai1ModePath)-STRLEN(pCliContext->ai1ModePath)-1) <
				STRLEN (pTempLinkMode->pMode_name) ?
				(sizeof(pCliContext->ai1ModePath)-STRLEN(pCliContext->ai1ModePath)-1) :
				STRLEN (pTempLinkMode->pMode_name));

	STRNCAT (pCliContext->ai1ModePath, pTempLinkMode->pMode_name,
			u4Len);

    }
    return CLI_SUCCESS;
}

/* This routine used to go back to the root by calling validate function
 */
VOID
mmi_goback_to_root (from_mode)
     t_MMI_MODE_TREE    *from_mode;
{
    while (from_mode != gCliSessions.p_mmi_root_mode)
    {
        if (from_mode->pValidate_function != NULL)
        {
            from_mode->pValidate_function (NULL);
        }
        from_mode = from_mode->pParent;
    }
}

VOID
mmi_come_from_root (t_MMI_MODE_TREE * to_mode,
                    INT1 *use_prompt, INT1 mode_level, INT1 *prompt_begin)
{
    INT2                i;
    t_MMI_MODE_TREE    *temp_mode, *path_mode;
    INT1                temp_prompt[MAX_PROMPT_LEN];

    temp_mode = to_mode;
    mmi_new_strcpy (temp_prompt, use_prompt);

    for (i = 1; i <= mode_level; i++)
    {
        temp_prompt[prompt_begin[i] - 1] = '\0';
    }
    i = 1;
    path_mode = gCliSessions.p_mmi_root_mode;

    while (i <= mode_level)
    {
        temp_mode = to_mode;
        while (temp_mode->pParent != path_mode)
        {
            temp_mode = temp_mode->pParent;
        }
        if (temp_mode->pValidate_function != NULL)
        {
            temp_mode->
                pValidate_function ((UINT1 *) &temp_prompt[prompt_begin[i]]);
        }
        path_mode = temp_mode;
        i++;
    }
}

/*
 * This routine will copy the current configuration to 
 * temporary configuration variables
 */

VOID
mmicm_copy_curconfig_to_temp (pCliContext)
     tCliContext        *pCliContext;
{
    INT1                i;

    pCliContext->pMmitemp_cur_mode = pCliContext->pMmiCur_mode;
    mmi_new_strcpy (pCliContext->temp_cur_prompt,
                    pCliContext->Mmi_i1Cur_prompt);
    mmi_new_strcpy (pCliContext->ai1TempCurModePath, pCliContext->ai1ModePath);
    CLI_STRCPY (pCliContext->au1TempPromptStr, pCliContext->au1PromptStr);

    pCliContext->i1temp_cur_level = pCliContext->Mmi_i1Cur_modelevel;
    for (i = 0; i <= pCliContext->Mmi_i1Cur_modelevel; i++)
    {
        pCliContext->i1temp_mode_prompt_beg[i] =
            pCliContext->Mmi_i1Mode_prompt_beg[i];
    }
}

/*
 * This routine checks the pModepar whether it is one of the elder mode of
 * pModechild. 
 * If pModepar is p_mmi_root_mode It should be a elder of all modes.
 * Otherwise go through all parent of pModechild by checking with pModepar
 */
INT4
mmi_check_the_parent_of_child (pModepar, pModechild)
     t_MMI_MODE_TREE    *pModepar, *pModechild;
{
    t_MMI_MODE_TREE    *pTmpMode;

    if (pModepar == gCliSessions.p_mmi_root_mode)
    {
        return TRUE;
    }
    pTmpMode = pModechild;
    while (pTmpMode != gCliSessions.p_mmi_root_mode)
    {
        if (pTmpMode == pModepar)
        {
            return TRUE;
        }
        pTmpMode = pTmpMode->pParent;
    }
    return FALSE;
}

/*
 * This routine will copy the temporary configuration to curent
 * configuration variables
 */

VOID
mmicm_copy_temp_to_curconfig (VOID)
{
    INT1                i;
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    mmi_goback_to_root (pCliContext->pMmiCur_mode);
    mmi_come_from_root (pCliContext->pMmitemp_cur_mode,
                        pCliContext->temp_cur_prompt,
                        pCliContext->i1temp_cur_level,
                        pCliContext->i1temp_mode_prompt_beg);
    pCliContext->pMmiCur_mode = pCliContext->pMmitemp_cur_mode;
    mmi_new_strcpy (pCliContext->Mmi_i1Cur_prompt,
                    pCliContext->temp_cur_prompt);

    CLI_STRCPY (pCliContext->au1PromptStr, pCliContext->au1TempPromptStr);
    mmi_new_strcpy (pCliContext->ai1ModePath, pCliContext->ai1TempCurModePath);
    CliSetDisplayPrompt (pCliContext);

    pCliContext->Mmi_i1Cur_modelevel = pCliContext->i1temp_cur_level;
    for (i = 0; i <= pCliContext->i1temp_cur_level; i++)
    {
        pCliContext->Mmi_i1Mode_prompt_beg[i] =
            pCliContext->i1temp_mode_prompt_beg[i];
    }
}

VOID
mmicm_copy_curconfig_to_inter (pCliContext)
     tCliContext        *pCliContext;
{
    INT1                i;
    pCliContext->pinter_cur_mode = pCliContext->pMmiCur_mode;
    mmi_new_strcpy (pCliContext->inter_cur_prompt,
                    pCliContext->Mmi_i1Cur_prompt);
    mmi_new_strcpy (pCliContext->ai1InterCurModePath, pCliContext->ai1ModePath);
    CLI_STRCPY (pCliContext->au1InterPromptStr, pCliContext->au1PromptStr);
    pCliContext->i1inter_cur_level = pCliContext->Mmi_i1Cur_modelevel;
    for (i = 0; i <= pCliContext->Mmi_i1Cur_modelevel; i++)
    {
        pCliContext->i1inter_mode_prompt_beg[i] =
            pCliContext->Mmi_i1Mode_prompt_beg[i];
    }
}

VOID
mmicm_copy_inter_to_curconfig (pCliContext)
     tCliContext        *pCliContext;
{
    INT1                i;

    mmi_goback_to_root (pCliContext->pMmiCur_mode);
    mmi_come_from_root (pCliContext->pinter_cur_mode,
                        pCliContext->inter_cur_prompt,
                        pCliContext->i1inter_cur_level,
                        pCliContext->i1inter_mode_prompt_beg);
    pCliContext->pMmiCur_mode = pCliContext->pinter_cur_mode;
    mmi_new_strcpy (pCliContext->Mmi_i1Cur_prompt,
                    pCliContext->inter_cur_prompt);

    CLI_STRCPY (pCliContext->au1PromptStr, pCliContext->au1InterPromptStr);
    mmi_new_strcpy (pCliContext->ai1ModePath, pCliContext->ai1InterCurModePath);
    CliSetDisplayPrompt (pCliContext);

    pCliContext->Mmi_i1Cur_modelevel = pCliContext->i1inter_cur_level;
    for (i = 0; i <= pCliContext->i1inter_cur_level; i++)
    {
        pCliContext->Mmi_i1Mode_prompt_beg[i] =
            pCliContext->i1inter_mode_prompt_beg[i];
    }
}

/*
 * This routine will copy the root mode configuration to the 
 * temporary configurations
 */

VOID
mmicm_copy_root_to_temp (pCliContext)
     tCliContext        *pCliContext;
{
    pCliContext->pMmitemp_cur_mode = gCliSessions.p_mmi_root_mode;
    mmi_new_strcpy (pCliContext->temp_cur_prompt, (CONST INT1 *) "/");
    mmi_new_strcpy (pCliContext->ai1TempCurModePath, (CONST INT1 *) "\0");
    CLI_STRCPY (pCliContext->au1TempPromptStr, (CONST INT1 *) "/");
    pCliContext->i1temp_cur_level = 0;
    pCliContext->i1temp_mode_prompt_beg[0] = 0;
}

/*
 * This routine will copy the root mode configuration to the 
 * Current configurations
 */

VOID
mmicm_copy_root_to_curconfig (pCliContext)
     tCliContext        *pCliContext;
{
    pCliContext->pMmiCur_mode = gCliSessions.p_mmi_root_mode;
    CLI_STRCPY (pCliContext->Mmi_i1Cur_prompt, "/");
    CLI_STRCPY (pCliContext->au1PromptStr, "\0");
    if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
    {
        CliGetPromptStr (gCliSessions.p_mmi_root_mode,
                         (pCliContext->Mmi_i1Cur_prompt + 1));
    }
    CliSetDisplayPrompt (pCliContext);

    pCliContext->Mmi_i1Cur_modelevel = 0;
    pCliContext->Mmi_i1Mode_prompt_beg[0] = 0;
    *pCliContext->ai1ModePath = '\0';
}

/*
 *This routine to init to rootmode at starting
 */

VOID
mmicm_init_mode_to_root (tCliContext * pCliContext)
{
    pCliContext->pMmiCur_mode = gCliSessions.p_mmi_root_mode;
    CLI_STRCPY (pCliContext->Mmi_i1Cur_prompt, "/");
    if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
    {
        CliGetPromptStr (gCliSessions.p_mmi_root_mode,
                         (pCliContext->Mmi_i1Cur_prompt + 1));
    }
    CLI_MEMSET (pCliContext->au1PromptStr, 0, MAX_PROMPT_LEN);
    CliSetDisplayPrompt (pCliContext);

    pCliContext->Mmi_i1Cur_modelevel = 0;
    pCliContext->Mmi_i1Mode_prompt_beg[0] = 0;
    *pCliContext->ai1ModePath = '\0';

}

/*
 * This Routine will save the current mode configuration
 * if the option is 0 
 * Otherwise it will recopy the mode configurations
 *
 * It will be called when the secondlevel superuser creations
 */

VOID
mmi_copy_curconfig_to_save (tCliContext * pCliContext, INT2 option)
{
    static t_MMI_MODE_TREE *psaveMmiCur_mode;
    static INT1         save_i1Cur_prompt[MAX_PROMPT_LEN];
    static INT1         save_i1Mode_prompt_beg[MAX_MODE_LEVEL];
    static INT1         save_i1Cur_modelevel;
    INT1                i;

    if (option == 0)
    {
        mmi_goback_to_root (pCliContext->pMmiCur_mode);
        psaveMmiCur_mode = pCliContext->pMmiCur_mode;
        mmi_new_strcpy (save_i1Cur_prompt, pCliContext->Mmi_i1Cur_prompt);
        save_i1Cur_modelevel = pCliContext->Mmi_i1Cur_modelevel;
        for (i = 0; i <= pCliContext->Mmi_i1Cur_modelevel; i++)
        {
            save_i1Mode_prompt_beg[i] = pCliContext->Mmi_i1Mode_prompt_beg[i];
        }
    }
    else
    {
        mmi_goback_to_root (pCliContext->pMmiCur_mode);
        mmi_come_from_root (psaveMmiCur_mode, save_i1Cur_prompt,
                            save_i1Cur_modelevel, save_i1Mode_prompt_beg);
        pCliContext->pMmiCur_mode = psaveMmiCur_mode;
        mmi_new_strcpy (pCliContext->Mmi_i1Cur_prompt, save_i1Cur_prompt);
        pCliContext->Mmi_i1Cur_modelevel = save_i1Cur_modelevel;
        for (i = 0; i <= pCliContext->Mmi_i1Cur_modelevel; i++)
        {
            pCliContext->Mmi_i1Mode_prompt_beg[i] = save_i1Mode_prompt_beg[i];
        }

    }
}

/*
 * This routine to execute the command from remote mode
 * the path of remote mode and command to be executed
 * will be in pCommand.
 *
 * the path and command to be executed will be separated first
 * the path will be changed to temporary mode
 * and
 *   execute_command_using_temp_mode (); 
 * will be called.
 *
 * 
 */

INT4
mmicm_remote_exe (pCliContext, p_i1command_to_remote)
     tCliContext        *pCliContext;
     INT1               *p_i1command_to_remote;    /* (K) command with path have to be executed */
{
    UINT1               u1com_exe[MAX_COMMAND_LEN];
    INT1                pCommand[MAX_COMMAND_LEN];    /* temporary storage */
    INT1                i;
    INT2                i2ErrCode = CLI_SUCCESS;

    if (CLI_STRLEN (p_i1command_to_remote) > MAX_COMMAND_LEN)
    {
        return OSIX_FAILURE;
    }

    mmi_new_strcpy (pCommand, p_i1command_to_remote);
    i = (INT1) CLI_STRLEN (pCommand);
    while (i >= 0 && (pCommand[i] != '/'))
    {
        i--;
    }
    if (i >= 0 && i < (MAX_COMMAND_LEN - 1))
    {
        mmi_new_strcpy ((INT1 *) u1com_exe, &pCommand[i + 1]);
        pCommand[i + 1] = '\0';
        if (CliChmod (pCliContext, (CHR1 *) pCommand) == CLI_SUCCESS)
        {
            if (CLI_STRLEN (u1com_exe))
            {
                mmicm_copy_curconfig_to_inter (pCliContext);
                mmicm_copy_temp_to_curconfig ();
                if (mmi_first_tok_parse (pCliContext, (INT1 *) u1com_exe,
                                         &i2ErrCode) > 0)
                {
                    return OSIX_SUCCESS;
                }
                else
                {
                    mmicm_copy_inter_to_curconfig (pCliContext);
                    return OSIX_FAILURE;
                }
            }
            return OSIX_FAILURE;
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        return OSIX_FAILURE;
    }
}

VOID
mmi_print_prompt (tCliContext * pCliContext, INT1 *Buffer, UINT4 u4BufLen)
{
    INT1               *pi1Pos = NULL;
    UINT1               u1CliPrompt[MAX_PROMPT_LEN];
    UINT4               u4Len = 0;

    pCliContext->Mmi_i1Cur_prompt[sizeof (pCliContext->Mmi_i1Cur_prompt) - 1] =
        '\0';
    if ((CLI_STRLEN (pCliContext->Mmi_i1Cur_prompt) >= 1)
        && (CLI_STRLEN (pCliContext->Mmi_i1Cur_prompt) < MAX_PROMPT_LEN))
    {
        if ((pi1Pos = (INT1 *) STRRCHR (pCliContext->Mmi_i1Cur_prompt,
                                        '/')) != NULL)
        {
            if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
            {
                u4Len =
                    ((STRLEN (CLI_GET_ISS_CLI_PROMPT ()) <
                      sizeof (u1CliPrompt)) ? STRLEN (CLI_GET_ISS_CLI_PROMPT ())
                     : sizeof (u1CliPrompt) - 1);

                CLI_STRNCPY (u1CliPrompt, CLI_GET_ISS_CLI_PROMPT (), u4Len);
                u1CliPrompt[u4Len] = '\0';

                if (STRLEN (u1CliPrompt) < u4BufLen)
                {
                    CLI_STRCPY (Buffer, u1CliPrompt);
                }
                else
                {
                    MEMCPY (Buffer, u1CliPrompt, u4BufLen - 1);
                    Buffer[u4BufLen - 1] = '\0';
                }

                CLI_STRCAT (Buffer, (pi1Pos + 1));
            }
            else
            {
                CLI_STRCPY (Buffer, pi1Pos);
                if (CLI_STRCMP (pCliContext->ai1UserName, CLI_ROOT_USER) == 0)
                {
                    CLI_STRCAT (Buffer, mmi_gen_messages[MMI_ROOT_PROMPT]);
                }
                else
                {
                    CLI_STRCAT (Buffer, mmi_gen_messages[MMI_USER_PROMPT]);
                }
            }
        }
    }
}

/*
 * Sets the mmi_i1disp_prompt string from Mmi_i1Cur_prompt 
 */

VOID
CliSetDisplayPrompt (tCliContext * pCliContext)
{
    INT1               *pi1Pos = NULL;
    UINT4               u4Len = 0;

    if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
    {
        u4Len =
            (STRLEN (CLI_GET_ISS_CLI_PROMPT ()) <
             sizeof (pCliContext->
                     mmi_i1disp_prompt) ? STRLEN (CLI_GET_ISS_CLI_PROMPT ()) :
             sizeof (pCliContext->mmi_i1disp_prompt) - 1);
        CLI_STRNCPY (pCliContext->mmi_i1disp_prompt, CLI_GET_ISS_CLI_PROMPT (),
                     u4Len);
        pCliContext->mmi_i1disp_prompt[u4Len] = '\0';
    }

    if ((pi1Pos = (INT1 *) STRRCHR (pCliContext->Mmi_i1Cur_prompt,
                                    '/')) == NULL)
    {
        if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
        {
            if (STRLEN (pCliContext->mmi_i1disp_prompt) + STRLEN (">") <
                sizeof (pCliContext->mmi_i1disp_prompt))
            {
                STRCAT (pCliContext->mmi_i1disp_prompt, ">");
                pCliContext->
                    mmi_i1disp_prompt[sizeof (pCliContext->mmi_i1disp_prompt) -
                                      1] = '\0';
            }
        }
        else
        {
            STRCPY (pCliContext->mmi_i1disp_prompt, "/");

        }
    }
    else
    {
        if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
        {
            if (STRLEN (pCliContext->mmi_i1disp_prompt) + STRLEN (pi1Pos + 1) <
                sizeof (pCliContext->mmi_i1disp_prompt))
            {
                STRCAT (pCliContext->mmi_i1disp_prompt, (pi1Pos + 1));
                pCliContext->
                    mmi_i1disp_prompt[sizeof (pCliContext->mmi_i1disp_prompt) -
                                      1] = '\0';
            }
        }
        else
        {
            if (STRLEN (pCliContext->mmi_i1disp_prompt) + STRLEN (pi1Pos) <
                sizeof (pCliContext->mmi_i1disp_prompt))
            {
                STRCAT (pCliContext->mmi_i1disp_prompt, pi1Pos);
                pCliContext->
                    mmi_i1disp_prompt[sizeof (pCliContext->mmi_i1disp_prompt) -
                                      1] = '\0';
            }
        }
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliGetLastModeName
 * DESCRIPTION   : This function returns the last mode name from a 
 *                 mode path string
 *                 
 * INPUT         : pi1ModeStr - Mode path String. 
 *                 ppi1Pos    - Return position of the last mode name in path
 * 
 * RETURN VALUE  : i1Delimit - The character '/' if, it is present in mode path
 *                             else,  '0'.
 ***************************************************************************/

INT1
CliGetLastModeName (INT1 *pi1ModeStr, INT1 **ppi1Pos)
{
    INT1                i1TempChar = 0;
    INT1                i1Delimit = 0;

    /* Get last mode parent path from pi1ModeStr and get its mode pointer */
    while ((pi1ModeStr) && (*pi1ModeStr))
    {
        i1Delimit = CliGetNextToken (pi1ModeStr, "/", &pi1ModeStr, ppi1Pos);

        if ((i1Delimit) == 0 || (!pi1ModeStr))
            break;

        if (pi1ModeStr)
            *(pi1ModeStr - 1) = i1Delimit;

        i1TempChar = i1Delimit;
    }
    return i1TempChar;
}

/***************************************************************************  
 * FUNCTION NAME : CliGetMode
 * DESCRIPTION   : Locates mode pi1ModeName from current mode or
 *                 from Root mode if pi1ModeName starts with '/'.
 *                 Optionally it is used to return the previous neighbour
 *                 mode for the mode to be found.
 *
 * INPUT         : pCurMode    - Current Mode pointer.
 *                 ppPrevMode  - Previous mode pointer to be returned.
 *                 pi1ModeName - Mode path/name.
 * 
 * RETURN VALUE  : mode pointer of mode pi1ModeName,if pi1ModeName is valid 
 *                 NULL if pi1ModeName is not valid.
 ***************************************************************************/

t_MMI_MODE_TREE    *
CliGetMode (t_MMI_MODE_TREE * pCurMode,
            t_MMI_MODE_TREE ** ppPrevMode, CONST INT1 *pi1ModeName)
{
    t_MMI_MODE_TREE    *pMode = NULL;
    t_MMI_MODE_TREE    *pTempMode = NULL;
    INT1               *pi1Token = NULL;
    INT1               *pi1ModeStr = NULL;
    INT1               *pi1BasePtr = NULL;
    INT1                i1Delimit = 0;
    BOOL1               bIsModeFoundFlag = FALSE;

    if (!(pi1ModeName) ||
        ((CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1ModeStr, INT1)) == NULL))
    {
        return NULL;
    }
    pi1BasePtr = pi1ModeStr;
    if ((STRCMP (pi1ModeName, "/")) && (*pi1ModeName))
    {
        CLI_STRCPY (pi1ModeStr, pi1ModeName);

        if (*pi1ModeStr == '/')
        {
            pMode = gCliSessions.p_mmi_root_mode->pChild;

            while (CliIsDelimit (*pi1ModeStr, "/"))
                pi1ModeStr++;
        }
        else
        {
            if (pCurMode != NULL)
            {
                pMode = pCurMode->pChild;
            }
            else
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BasePtr);
                return NULL;
            }
        }
        if (ppPrevMode == NULL)
        {
            ppPrevMode = &pTempMode;
        }
        i1Delimit = CliGetNextToken (pi1ModeStr, "/",
                                     &pi1ModeStr, (INT1 **) &pi1Token);

        while (pMode != NULL && pi1Token != NULL)
        {
            if (!(STRCASECMP (pMode->pMode_name, pi1Token)))
            {
                bIsModeFoundFlag = TRUE;
            }
            if (bIsModeFoundFlag == TRUE)
            {
                i1Delimit = CliGetNextToken
                    (pi1ModeStr, "/", &pi1ModeStr, &pi1Token);
                if (pMode->u4ModeType == CLI_LINK_MODE)
                {
                    pMode = pMode->pChild;
                }
                if (!(i1Delimit) && (pi1Token == NULL))
                {
                    break;
                }
                bIsModeFoundFlag = FALSE;
                pMode = pMode->pChild;
                *ppPrevMode = NULL;
            }
            else
            {
                *ppPrevMode = pMode;
                pMode = pMode->pNeighbour;
            }
        }
    }
    else
    {
        pMode = gCliSessions.p_mmi_root_mode;
    }

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BasePtr);

    return pMode;
}

/***************************************************************************
 * FUNCTION NAME : CliGetPromptFromMode
 * DESCRIPTION   : Returns the prompt string for the given mode name.
 *
 * INPUT         : pi1ModeName    - Mode name.
 *                 pi1PromptStr   - Pointer to the Prompt String.
 * 
 * RETURN VALUE  : CLI_SUCCESS - if mode is valid, else CLI_FAILURE.
 ***************************************************************************/

INT1
CliGetPromptFromMode (CONST CHR1 * pi1ModeName, INT1 *pi1PromptStr)
{
    t_MMI_MODE_TREE    *p_tstack[MAX_MODE_LEVEL];
    t_MMI_MODE_TREE    *p_tsearch;
    INT1                i1top = 0;

    if (!(pi1ModeName))
    {
        return (CLI_FAILURE);
    }

    p_tstack[i1top] = gCliSessions.p_mmi_root_mode->pChild;
    while (i1top >= 0)
    {
        /* checking the stack empty */
        p_tsearch = p_tstack[i1top];    /* pop stack */
        i1top--;
        while (p_tsearch != NULL)
        {
            if (!(STRCASECMP (p_tsearch->pMode_name, pi1ModeName)))
            {
                return (CliGetPromptStr (p_tsearch, pi1PromptStr));
            }

            if (p_tsearch->pChild != NULL)
            {
                i1top++;
                p_tstack[i1top] = p_tsearch->pNeighbour;
                p_tsearch = p_tsearch->pChild;
            }
            else
            {
                if (i1top < 0)
                {
                    p_tsearch = NULL;
                }
                else
                {
                    p_tsearch = p_tsearch->pNeighbour;

                }
            }
        }
    }

    return (CLI_FAILURE);
}

/***************************************************************************
 * FUNCTION NAME : CliGetPromptStr
 * DESCRIPTION   : Returns the prompt string by calling the prompt function
 *                 or the prompt string of the given mode.
 *
 * INPUT         : pCurMode       - Mode structure of the mode.
 *                 pi1PromptStr   - Pointer to the Prompt String.
 * 
 * RETURN VALUE  : CLI_SUCCESS - if mode is valid, else CLI_FAILURE.
 ***************************************************************************/

INT1
CliGetPromptStr (t_MMI_MODE_TREE * pCurMode, INT1 *pi1PromptStr)
{
    INT1                ai1ModeName[MAX_PROMPT_LEN];
    UINT4               u4Len = 0;

    if (pCurMode->PromptInfo.u1PromptType == PROMPT_FUN)
    {
        u4Len = ((STRLEN (pCurMode->pMode_name) < sizeof (ai1ModeName)) ?
                 STRLEN (pCurMode->pMode_name) : sizeof (ai1ModeName) - 1);
        CLI_STRNCPY (ai1ModeName, pCurMode->pMode_name, u4Len);
        ai1ModeName[u4Len] = '\0';
        if (!(pCurMode->PromptInfo.PromptFunc (ai1ModeName, pi1PromptStr)))
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        STRCPY (pi1PromptStr, pCurMode->PromptInfo.PromptStr);
    }
    return CLI_SUCCESS;
}

INT1
CliExitConfigMode ()
{
    INT1                ai1TempStr[MAX_PROMPT_LEN];
    tCliContext        *pCliContext = NULL;

    CliChangePath ("/");
    ai1TempStr[0] = '\0';
    CliSetContextIdInfo (CLI_INVALID_CONTEXT_ID);
    CliSetModeInfo (-1);

    CliGetPromptFromMode ("USEREXEC", ai1TempStr);

    if (!(pCliContext = CliGetContext ()))
    {
        return CLI_FAILURE;
    }

    if (pCliContext->i1PrivilegeLevel > CLI_DEFAULT_PRIVILEGE)
    {
        CliChangePath ((CONST CHR1 *) ai1TempStr);
        CliGetPromptFromMode ("EXEC", ai1TempStr);
    }

    CliChangePath ((CHR1 *) ai1TempStr);
    return CLI_SUCCESS;
}

#ifdef MBSM_WANTED
INT1
CliGetBootConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, "-boot>");
    return TRUE;
}
#endif

INT1
CliGetUserExecPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, ">");
    return TRUE;
}

INT1
CliGetExecPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, "#");
    return TRUE;
}

INT1
CliGetConfigurePrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }
    if ((CLI_STRCMP (pi1ModeName, "CONFIGURE") != 0) &&
        (CLI_STRCMP (pi1ModeName, "(config)#") != 0))

    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, "(config)#");
    return TRUE;
}

INT1
CliGetLinePrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }

    if ((CLI_STRCMP (pi1ModeName, "LINE") != 0) &&
        (CLI_STRCMP (pi1ModeName, "(config-line)#") != 0))

    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, "(config-line)#");
    return TRUE;
}
