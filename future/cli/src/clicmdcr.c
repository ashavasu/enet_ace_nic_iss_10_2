/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clicmdcr.c,v 1.24 2014/02/07 13:24:35 siva Exp $
 *
 * Description: This will have the lowlevel routines to   
 *               create the command_data structure         
 *******************************************************************/

#include "clicmds.h"

extern tCliSessions gCliSessions;
extern t_MMI_TOKEN_TYPE mmi_cmd_token[];
extern INT4         Mmi_i4lex_token_type;

#define   MMI_CUR_CMD_PTR gCliSessions.pMmiCurcmd_ptr
#define   MMICMD_RETGET create_return_node

/***** protos ****/
static VOID         mmi_link_last_nc (t_MMI_CMD *, t_MMI_CMD *);
static VOID         mmi_link_last_nt (t_MMI_CMD *, t_MMI_CMD *);
static VOID         mmi_link_last_nc_nt (t_MMI_CMD *, t_MMI_CMD *);
static VOID         mmi_link_last_alt (t_MMI_CMD *, t_MMI_CMD *);
static VOID         mmi_link_alt_for_altnodes_innc (t_MMI_CMD *, t_MMI_CMD *);
static VOID         mmicm_get_type_token_from_str (CONST CHR1 *, FS_UINT8 *);

/*
 * This will search the token type in mmi_cmd_token array 
 * after searching according to the index it will set the the bit
 * in Mmi_i4lex_token_type
 * This will be called from VALTOK()
 */

extern tMemPoolId   gCliCmdTokenPoolId;
extern tMemPoolId   gCliTokenPoolId;
extern tMemPoolId   gCliTokenRangeId;

VOID
mmicm_get_type_token_from_str (i1str, pu8TokenType)
     CONST CHR1         *i1str;
     FS_UINT8           *pu8TokenType;
{
    FS_UINT8            u8SetTokenType;
    INT2                i;

    FSAP_U8_CLR (&u8SetTokenType);
    FSAP_U8_CLR (pu8TokenType);

    for (i = 0; mmi_cmd_token[i].token_type != NULL; i++)
    {
        if (mmi_strcmpi ((CONST INT1 *) mmi_cmd_token[i].token_type,
                         (CONST INT1 *) i1str) == 0)
        {
            FSAP_U8_CLR (&u8SetTokenType);
            mmi_lx_set_bit_pos (i, &u8SetTokenType);
            FSAP_U8_ASSIGN (pu8TokenType, &u8SetTokenType);
            return;
        }
    }
    return;
}

/*
 * This will allocate a command node and will copy the string token
 */

t_MMI_CMD          *
TOK (CONST CHR1 * i1str, INT2 token_num)
{
    t_MMI_CMD          *t_tmp_ptr;
    INT4               *i4ptr;

    if ((t_tmp_ptr =
         CliMemAllocMemBlk (gCliCmdTokenPoolId, sizeof (t_MMI_CMD))) == NULL)
    {
        return NULL;
    }

    t_tmp_ptr->i1pToken = i1str;
    FSAP_U8_CLR (&(t_tmp_ptr->u8Typeof_token));
    i4ptr = (INT4 *) (VOID *) &(t_tmp_ptr->i4Action_number);
    *i4ptr = token_num;

    MMI_CUR_CMD_PTR = t_tmp_ptr;
    return t_tmp_ptr;
}

/*
 * This will allocate a command node and will set the token type
 */
t_MMI_CMD          *
VALTOK (CONST CHR1 * i1str, INT2 token_num, ...)
{

    t_MMI_CMD          *t_tmp_ptr;
    INT4               *i4ptr;
    INT4                i4Argc = 0;
    UINT4               u4RangeVal = 0;
    INT4                i4RangeVal = 0;
    UINT1              *pu1Temp = NULL;
    INT1               *pi1Args = NULL;
    DBL8                d8RangeVal = 0.0;

    va_list             ap;

    if ((t_tmp_ptr =
         CliMemAllocMemBlk (gCliCmdTokenPoolId, sizeof (t_MMI_CMD))) == NULL)
    {
        return NULL;
    }

    mmicm_get_type_token_from_str (i1str, &(t_tmp_ptr->u8Typeof_token));
    i4ptr = (INT4 *) (VOID *) &(t_tmp_ptr->i4Action_number);

    va_start (ap, token_num);

    *i4ptr = token_num;
    pi1Args = va_arg (ap, INT1 *);

    /* Gets values from the Variable argument list */

    if (pi1Args != NULL)
    {
        if ((pu1Temp =
             CliMemAllocMemBlk (gCliTokenPoolId, MAX_CHAR_IN_TOKEN)) == NULL)
        {
            MemReleaseMemBlock (gCliCmdTokenPoolId, (UINT1 *) t_tmp_ptr);
            va_end (ap);
            return NULL;
        }

        CLI_STRCPY (pu1Temp, pi1Args);
        i4Argc = mmi_atoi (pu1Temp);

        MemReleaseMemBlock (gCliTokenPoolId, (UINT1 *) pu1Temp);
    }

    /* Checks for optional range values specified for each type of tokens */

    /* The following switch validates for the tokens present in the 
     * range between (1-32) in t_MMI_TOKEN_TYPE.
     */

    switch (FSAP_U8_FETCH_LO (&(t_tmp_ptr->u8Typeof_token)))
    {
        case 4:
            /* to check the validity of tokens of Mac Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_MacAddress_range;

            break;
        case 8:
            /* to check the validity of tokens of Unicast Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_UcastAddress_range;

            break;
        case 16:
            /* to check the validity of tokens of Unicast/Loopback Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_LUcastAddress_range;

            break;
        case 32:
            /* to check the validity of tokens of Multicast Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_McastAddress_range;

            break;
        case 64:
            /* to check the validity of tokens of Ip Mask type */

            break;
        case 128:
            /* to check the validity of tokens of Ip Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_IpAddress_range;

            break;
        case 256:
            /* to check the validity of tokens of IPv6 Address type */

            t_tmp_ptr->pRangeChkFunc = mmi_check_IP6Address_range;

            break;
        case 512:                /* to get Range values of                                                          Float Type tokens      */
            if (i4Argc != 0)
            {
                t_tmp_ptr->pRangeChkFunc = mmi_check_float_range;

                if ((t_tmp_ptr->pRange =
                     CliMemAllocMemBlk (gCliTokenRangeId,
                                        sizeof (t_MMI_RANGE))) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                d8RangeVal = va_arg (ap, DBL8);
                t_tmp_ptr->pRange->pRangeFloat.d8MinRange = d8RangeVal;

                d8RangeVal = va_arg (ap, DBL8);
                t_tmp_ptr->pRange->pRangeFloat.d8MaxRange = d8RangeVal;

                pi1Args = va_arg (ap, INT1 *);

                if ((t_tmp_ptr->pRange->pRangeFloat.pi1RangeStr =
                     CliMemAllocMemBlk (gCliTokenPoolId,
                                        MAX_CHAR_IN_TOKEN)) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                /* Copies range string in to command structure */

                CLI_STRCPY (t_tmp_ptr->pRange->pRangeFloat.pi1RangeStr,
                            pi1Args);
            }

            break;
        case 1024:                /* to get Range values of
                                 * Short Int Type tokens */
            if (i4Argc != 0)
            {
                t_tmp_ptr->pRangeChkFunc = mmi_check_integer_range;

                if ((t_tmp_ptr->pRange =
                     CliMemAllocMemBlk (gCliTokenRangeId,
                                        sizeof (t_MMI_RANGE))) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                u4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pRangeInt.u4MinRange = u4RangeVal;

                u4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pRangeInt.u4MaxRange = u4RangeVal;

                pi1Args = va_arg (ap, INT1 *);

                if ((t_tmp_ptr->pRange->pRangeInt.pi1RangeStr =
                     CliMemAllocMemBlk (gCliTokenPoolId,
                                        MAX_CHAR_IN_TOKEN)) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                /* Copies range string in to command structure */

                CLI_STRCPY (t_tmp_ptr->pRange->pRangeInt.pi1RangeStr, pi1Args);
            }

            break;
        case 2048:                /* to get Range values of
                                   Integer Tye tokens */
            if (i4Argc != 0)
            {
                t_tmp_ptr->pRangeChkFunc = mmi_check_integer_range;

                if ((t_tmp_ptr->pRange =
                     CliMemAllocMemBlk (gCliTokenRangeId,
                                        sizeof (t_MMI_RANGE))) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                u4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pRangeInt.u4MinRange = u4RangeVal;

                u4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pRangeInt.u4MaxRange = u4RangeVal;

                pi1Args = va_arg (ap, INT1 *);

                if ((t_tmp_ptr->pRange->pRangeInt.pi1RangeStr =
                     CliMemAllocMemBlk (gCliTokenPoolId,
                                        MAX_CHAR_IN_TOKEN)) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                /* Copies range string in to command structure */

                CLI_STRCPY (t_tmp_ptr->pRange->pRangeInt.pi1RangeStr, pi1Args);
            }

            break;
        case 16384:
            /* to check the validity of ifnum token based 
             * on the type of the interface given as previous token 
             */

            t_tmp_ptr->pRangeChkFunc = MmiIfNumTokenCheck;

            break;
        case 32768:
            /* to check the validity of iface_list token based 
             * on the type of the interface given as previous token 
             */

            t_tmp_ptr->pRangeChkFunc = MmiIfaceListTokenCheck;

            break;

        case 1048576:
            /* to check the validity of tokens of iftype and 
             * do word complete of iftype
             * if it matches with any avaliable iftypes */

            t_tmp_ptr->pRangeChkFunc = MmiIfTypeCompleteFunc;

            break;
        case 2097152:
            /* to check the validity of tokens of ifXtype and 
             * do word complete of ifXtype
             * if it matches with any avaliable ifXtypes */

            t_tmp_ptr->pRangeChkFunc = MmiIfXTypeCompleteFunc;

            break;
        case 4194304:
            /* to check the Length of string type tokens */

            if (i4Argc != 0)
            {
                t_tmp_ptr->pRangeChkFunc = mmi_check_String_length;

                if ((t_tmp_ptr->pRange =
                     CliMemAllocMemBlk (gCliTokenRangeId,
                                        sizeof (t_MMI_RANGE))) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                u4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pStringLen.u4StrLen = u4RangeVal;

                pi1Args = va_arg (ap, INT1 *);

                if ((t_tmp_ptr->pRange->pStringLen.pi1RangeStr =
                     CliMemAllocMemBlk (gCliTokenPoolId,
                                        MAX_CHAR_IN_TOKEN)) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                /* Copies string length in to command structure */

                CLI_STRCPY (t_tmp_ptr->pRange->pStringLen.pi1RangeStr, pi1Args);
            }

            break;
        case 536870912:        /* to get Range values of signed
                                   Integer Type tokens */
            if (i4Argc != 0)
            {
                t_tmp_ptr->pRangeChkFunc = mmi_check_sinteger_range;

                if ((t_tmp_ptr->pRange =
                     CliMemAllocMemBlk (gCliTokenRangeId,
                                        sizeof (t_MMI_RANGE))) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                i4RangeVal = va_arg (ap, INT4);
                t_tmp_ptr->pRange->pRangeSInt.i4MinRange = i4RangeVal;

                i4RangeVal = va_arg (ap, UINT4);
                t_tmp_ptr->pRange->pRangeSInt.i4MaxRange = i4RangeVal;

                pi1Args = va_arg (ap, INT1 *);

                if ((t_tmp_ptr->pRange->pRangeInt.pi1RangeStr =
                     CliMemAllocMemBlk (gCliTokenPoolId,
                                        MAX_CHAR_IN_TOKEN)) == NULL)
                {
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) t_tmp_ptr);
                    va_end (ap);
                    return NULL;
                }

                /* Copies range string in to command structure */

                CLI_STRCPY (t_tmp_ptr->pRange->pRangeSInt.pi1RangeStr, pi1Args);
            }

            break;
        default:
            break;
    }

    /* The following switch validates for the tokens present in the 
     * range between (33-64) in t_MMI_TOKEN_TYPE.
     */

    switch (FSAP_U8_FETCH_HI (&(t_tmp_ptr->u8Typeof_token)))
    {
        default:
            break;
    }

    va_end (ap);

    MMI_CUR_CMD_PTR = t_tmp_ptr;

    return t_tmp_ptr;
}

/*
 * This will allocate a command node and copy "ret"
 */

t_MMI_CMD          *
create_return_node (INT4 i4action_num, INT2 i2ModeIndex)
{
    t_MMI_CMD          *t_tmp_ptr;

    if ((t_tmp_ptr =
         CliMemAllocMemBlk (gCliCmdTokenPoolId, sizeof (t_MMI_CMD))) == NULL)
    {
        return NULL;
    }
    t_tmp_ptr->i1pToken = "ret";
    FSAP_U8_CLR (&(t_tmp_ptr->u8Typeof_token));
    t_tmp_ptr->i2ModeIndex = i2ModeIndex;
    t_tmp_ptr->i4Action_number = i4action_num;
    return t_tmp_ptr;
}

/*
 * This will link the t_ptr2 to t_ptr1 at the end of p_Next_command list
 */
VOID
mmi_link_last_nc (t_ptr1, t_ptr2)
     t_MMI_CMD          *t_ptr1, *t_ptr2;
{
    t_MMI_CMD          *t_tmp_ptr;

    if (t_ptr1 == NULL)
    {
        return;
    }
    t_tmp_ptr = t_ptr1;
    while (t_tmp_ptr->pNext_command != NULL)
    {
        if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            mmi_printf
                (" warning : a recursive node should not be in same list\n");
            mmi_printf (" error : %s  going to be removed \n",
                        t_tmp_ptr->pNext_command->i1pToken);
            exit (0);
        }
        t_tmp_ptr = t_tmp_ptr->pNext_command;
        if (t_tmp_ptr == t_ptr2)
        {
            return;
        }
    }
    t_tmp_ptr->pNext_command = t_ptr2;
    return;
}

/*
 * This will link t_ptr2 to t_ptr1  as a last pNext->token
 */
VOID
mmi_link_last_nt (t_ptr1, t_ptr2)
     t_MMI_CMD          *t_ptr1, *t_ptr2;
{
    t_MMI_CMD          *t_tmp_ptr;

    if (t_ptr1 == NULL)
        return;
    t_tmp_ptr = t_ptr1;
    while (t_tmp_ptr->pNext_token != NULL)
    {
/*
 Added for Linux 1.2.1 
 becoz strcmp does not check for NULL address
*/
        if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur")
            && t_tmp_ptr->pNext_command != NULL)
        {
            mmi_link_last_nt (t_tmp_ptr->pNext_command, t_ptr2);
        }
        t_tmp_ptr = t_tmp_ptr->pNext_token;
        if (t_tmp_ptr == t_ptr2)
        {
            return;
        }
    }
    if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->i1pToken,
                        (CONST INT1 *) "recur")
        && t_tmp_ptr->pNext_command != NULL)
    {
        mmi_link_last_nt (t_tmp_ptr->pNext_command, t_ptr2);
    }
    t_tmp_ptr->pNext_token = t_ptr2;
    return;
}

/*
 * This will link t_ptr2 to t_ptr1 as last pNext_token
 * AND by calling recursively will link t_ptr2 to all nodes those are
 * in pNext_command of t_ptr1
 */
VOID
mmi_link_last_nc_nt (t_ptr1, t_ptr2)
     t_MMI_CMD          *t_ptr1, *t_ptr2;
{
    t_MMI_CMD          *t_tmp_ptr;

    if (t_ptr1 == NULL)
    {
        return;
    }

    t_tmp_ptr = t_ptr1;
    while (t_tmp_ptr != NULL)
    {
        mmi_link_last_nt (t_tmp_ptr, t_ptr2);
        if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            break;
        }
        t_tmp_ptr = t_tmp_ptr->pNext_command;
    }
    return;
}

/*
 * This will link t_ptr2 to t_ptr1 as last pOptional
 */
VOID
mmi_link_last_alt (t_ptr1, t_ptr2)
     t_MMI_CMD          *t_ptr1, *t_ptr2;
{
    t_MMI_CMD          *t_tmp_ptr = NULL;
    t_MMI_CMD          *t_tmp_ptr1;

    if (t_ptr1 == NULL)
    {
        return;
    }

    t_tmp_ptr1 = t_ptr1;
    while (t_tmp_ptr1 != NULL)
    {
        t_tmp_ptr = t_tmp_ptr1;
        while (t_tmp_ptr->pOptional != NULL)
        {
            if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->pOptional->i1pToken,
                                (CONST INT1 *) "ret") == 0)
            {
                t_tmp_ptr->pOptional = t_ptr2;
                break;
            }
            t_tmp_ptr = t_tmp_ptr->pOptional;
            if (t_tmp_ptr == t_ptr2)
            {
                return;
            }
        }
        if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr1->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            break;
        }
        t_tmp_ptr1 = t_tmp_ptr1->pNext_command;
    }
    t_tmp_ptr->pOptional = t_ptr2;
    return;
}

/*
 * This routine for { one | ...} type command part
 * This will link all ptrs those are in parameter list in a 
 * pNext_command list and will make last next token of all ptrs 
 * as next_token
 */
t_MMI_CMD          *
GRP_OR (t_MMI_CMD * pDummy, ...)
{
    t_MMI_CMD          *temp_ptr1 = NULL;
    t_MMI_CMD          *temp_ptr2 = NULL;
    t_MMI_CMD          *first_arg = NULL;
    va_list             ap;
    UINT2               u2Idx = 0;

    va_start (ap, pDummy);
    UNUSED_PARAM (pDummy);

    temp_ptr1 = va_arg (ap, t_MMI_CMD *);
    first_arg = temp_ptr1;

    while (temp_ptr1)
    {
        temp_ptr2 = va_arg (ap, t_MMI_CMD *);
        mmi_link_last_nc (temp_ptr1, temp_ptr2);
        if (!temp_ptr2)
        {
            u2Idx++;
            break;
        }
        temp_ptr1 = temp_ptr2;
        u2Idx++;
    }
    va_end (ap);

    if (u2Idx > 0)
    {
        MMI_CUR_CMD_PTR = first_arg;
        return first_arg;
    }
    else
    {
        return NULL;
    }
}

/*
 * This routine for  [ on  ....] part of command
 * This will link all ptrs those are in parameter list
 * and will make the alternative for first ptr as next_token
 */
t_MMI_CMD          *
GRP_OPT (INT2 action_num, INT2 i2ModeIndex, ...)
{
    t_MMI_CMD          *temp_ptr1 = NULL;
    t_MMI_CMD          *temp_ptr2 = NULL;
    t_MMI_CMD          *first_arg = NULL;
    t_MMI_CMD          *next_token = NULL;
    va_list             ap;
    UINT2               u2Idx = 0;

    va_start (ap, i2ModeIndex);

    temp_ptr1 = va_arg (ap, t_MMI_CMD *);
    first_arg = temp_ptr1;

    u2Idx = 1;
    if (temp_ptr1)
    {
        next_token = MMICMD_RETGET (action_num, i2ModeIndex);
        while (temp_ptr1)
        {
            u2Idx++;
            if (temp_ptr1)
            {
                temp_ptr2 = va_arg (ap, t_MMI_CMD *);
            }
            mmi_link_last_nt (temp_ptr1, temp_ptr2);

            /* Sets the pOptional pointer for all pNext_command's
             * MMI_CMD_RET for optional tokens  */

            while (temp_ptr1)
            {
                if (!temp_ptr1->pOptional)
                {
                    temp_ptr1->pOptional = next_token;
                }
                temp_ptr1 = temp_ptr1->pNext_command;
            }

            temp_ptr1 = temp_ptr2;
        }
        mmi_link_last_alt (first_arg, next_token);
    }
    else
    {
        va_end (ap);
        return NULL;
    }

    va_end (ap);
    return first_arg;
}

/*
 * This will link t_ptr2 as p_Optioanal to all the nodes those are in 
 * the pNext_command of t_ptr2
 */
VOID
mmi_link_alt_for_altnodes_innc (t_ptr1, t_ptr2)
     t_MMI_CMD          *t_ptr1, *t_ptr2;
{
    t_MMI_CMD          *t_tmp_ptr;

    t_tmp_ptr = t_ptr1;
    while (t_tmp_ptr != NULL)
    {
        if (t_ptr2 != NULL)
            t_tmp_ptr->pOptional = t_ptr2;
        if (mmi_new_strcmp ((CONST INT1 *) t_tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            mmi_printf
                (" warning : Check the recursive definition should not have any in same level \n");
            return;
        }
        t_tmp_ptr = t_tmp_ptr->pNext_command;
    }
    return;
}

/*
 *This routine for ( one ... ) part of the command
 * This will link all ptrs those are in the parameter list
 * in a recursive node
 */
t_MMI_CMD          *
GRP_REC (t_MMI_CMD * pDummy, ...)
{
    t_MMI_CMD          *new_recur_node;
    t_MMI_CMD          *temp_ptr1;
    t_MMI_CMD          *temp_ptr2;
    t_MMI_CMD          *first_arg;
    va_list             ap;
    UINT2               u2Idx = 0;

    va_start (ap, pDummy);
    UNUSED_PARAM (pDummy);

    temp_ptr1 = va_arg (ap, t_MMI_CMD *);
    first_arg = temp_ptr1;

    while (temp_ptr1)
    {
        temp_ptr2 = va_arg (ap, t_MMI_CMD *);
        mmi_link_alt_for_altnodes_innc (temp_ptr1, temp_ptr2);
        mmi_link_last_nc_nt (temp_ptr1, temp_ptr2);
        if (!temp_ptr2)
        {
            u2Idx++;
            break;
        }
        temp_ptr1 = temp_ptr2;
        u2Idx++;
    }
    va_end (ap);

    if (u2Idx > 0)
    {
        mmi_link_last_nc_nt (temp_ptr1, first_arg);
        mmi_link_alt_for_altnodes_innc (temp_ptr1, first_arg);
    }

    new_recur_node = TOK ("recur", -1);
    if (new_recur_node != NULL)
    {
        new_recur_node->pNext_command = first_arg;
        new_recur_node->pNext_token = NULL;
    }
    /* ? new_recur_node->pNext_token = next_token; */
    MMI_CUR_CMD_PTR = new_recur_node;

    return new_recur_node;
}

/*
 * This routine will create the full command as using the parameters
 * This will link all parameters in list as pNext_token and will  link
 * MMI_CMD_RET node which will have the action number action_num at last
 *
 * If action_num < 0 this will not link MMI_CMD_RET
 */
t_MMI_CMD          *
GRP_CMD (INT2 action_num, INT2 i2ModeIndex, ...)
{
    t_MMI_CMD          *next_token;
    t_MMI_CMD          *temp_ptr1;
    t_MMI_CMD          *temp_ptr2;
    t_MMI_CMD          *first_arg;
    va_list             ap;
    UINT2               u2Idx = 0;

    va_start (ap, i2ModeIndex);

    if (action_num < 0)
    {
        next_token = NULL;        /* for use in GRP_CMD1 call */
    }
    else
    {
        next_token = MMICMD_RETGET (action_num, i2ModeIndex);
    }

    temp_ptr1 = va_arg (ap, t_MMI_CMD *);
    first_arg = temp_ptr1;

    while (temp_ptr1)
    {
        temp_ptr2 = va_arg (ap, t_MMI_CMD *);
        mmi_link_last_nt (temp_ptr1, temp_ptr2);
        if (!temp_ptr2)
        {
            u2Idx++;
            break;
        }
        if (temp_ptr1->pOptional)
        {
            mmi_link_last_alt (temp_ptr1, temp_ptr2);
        }

        temp_ptr1 = temp_ptr2;
        u2Idx++;
    }

    va_end (ap);
    if (u2Idx > 0)
    {
        mmi_link_last_nc_nt (temp_ptr1, next_token);
    }
    else
    {
        if (next_token != NULL)
        {
            MemReleaseMemBlock (gCliCmdTokenPoolId, (UINT1 *) next_token);
        }
    }
    if (action_num > 0)
    {
        MMI_CUR_CMD_PTR = first_arg;
    }
    return first_arg;
}

/*
 * This is for or token in { part of the command
 * this will call GRP_CMD using action_num < 0
 */
t_MMI_CMD          *
GRP_CMD1 (t_MMI_CMD * pDummy, ...)
/* next_token not used */
{
    t_MMI_CMD          *temp_ptr;
    t_MMI_CMD          *aCmdPtr[MAX_NO_OF_TOKENS_IN_MMI];
    va_list             ap;
    UINT2               u2Idx = 0;

    va_start (ap, pDummy);
    UNUSED_PARAM (pDummy);

    while (((temp_ptr = va_arg (ap, t_MMI_CMD *)) != NULL) &&
           (u2Idx < MAX_NO_OF_TOKENS_IN_MMI))
    {
        aCmdPtr[u2Idx++] = temp_ptr;
    }
    if (u2Idx < MAX_NO_OF_TOKENS_IN_MMI)
        aCmdPtr[u2Idx] = NULL;
    temp_ptr = GRP_CMD (-1, 0, aCmdPtr[0], aCmdPtr[1], aCmdPtr[2], aCmdPtr[3],
                        aCmdPtr[4], aCmdPtr[5], aCmdPtr[6], aCmdPtr[7],
                        aCmdPtr[8], aCmdPtr[9], aCmdPtr[10], aCmdPtr[11],
                        aCmdPtr[12], aCmdPtr[13], aCmdPtr[14], aCmdPtr[15],
                        aCmdPtr[16], aCmdPtr[17], aCmdPtr[18], aCmdPtr[19],
                        aCmdPtr[20], aCmdPtr[21], aCmdPtr[22], aCmdPtr[23],
                        aCmdPtr[24], aCmdPtr[25], aCmdPtr[26]);
    va_end (ap);

    return temp_ptr;
}
