/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cligrps.c,v 1.82 2017/12/20 11:06:49 siva Exp $
 *
 * Description: cligrps.c
 *
 * *******************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "clicmds.h"
#include "fips.h"

#include "fssocket.h"
#ifdef PAM_AUTH_WANTED
#include <errno.h>
#include <signal.h>
#include <libgen.h>
#include <termio.h>
#include <security/pam_appl.h>
static INT4         i4SCount;    /*Static variable to keep track of the interruption */
extern INT4         LdapPamTtyConv (INT4 num_msg, struct pam_message **msg,
                                    struct pam_response **response,
                                    VOID *appdata_ptr);
static INT4         pam_passwd_conv (INT4 i4Input,
                                     const struct pam_message **msg,
                                     struct pam_response **resp, VOID *data);
CONST INT1         *pam_password = NULL;
#define PAM_MSG_MEMBER(msg, n, member) ((*(msg))[(n)].member)
#endif

tSshSession         gSshSessionAuth[CLI_NUM_SSH_CLIENT_WATCH];
tTimerListId        gSshTimerLst;

extern tCliSessions gCliSessions;
extern tMemPoolId   gCliUserGroupMemPoolId;
extern tMemPoolId   gCliUserMemPoolId;
extern tMemPoolId   gCliGroupMemPoolId;
extern tMemPoolId   gCliPrivilegesMemPoolId;

/* adduser [-u uid] [-p passwd] [-d defaultmode]  [-g grp1,grp2,..] username
 * modeuser 
 * deleteuser username
 * groupadd  [-g grpid ] groupname
 * groupdel  groupname
 */
INT1
CliUserAdd (INT1 *pi1NewUid, INT1 *pi1NewPasswd, INT1 *pi1NewDefMode,
            INT1 *pi1NewGrps, INT1 *pi1NewUsrName)
{
    INT4                i4NewUserId = -1;
    INT4                i4Uid;
    INT4                i4MaxUserId = 0;
    INT2                i2UserIndex;
    INT1               *pi1UserName;
    INT1               *pi1UserId;
    INT1               *pi1Passwd;
    INT1               *pi1Mode;
    INT1               *pi1Buf;
    INT1               *pi1BlockUserRelTime = NULL;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT1                i1UserIdStr[11];
    INT1                ai1DefMode[4];
    INT1                ai1DefPasswd[MAX_PASSWD_LEN];

    STRCPY (ai1DefMode, CLI_DEF_MODE);
    STRCPY (ai1DefPasswd, CLI_DEF_PASSWD);

    if (gCliSessions.i2NofUsers == CLI_MAX_USERS)
    {
        mmi_printf ("\r\n%% No of users exceeded the limit \r\n");
        return (CLI_FAILURE);
    }

    if (pi1NewUid)
    {
        if ((STRLEN (pi1NewUid)) > CLI_MAX_USERID_LEN)
        {
            mmi_printf ("\r\n%%User Id exceeded size limit <1-999>\r\n");
            return (CLI_FAILURE);
        }
        i4NewUserId = CLI_ATOI (pi1NewUid);
    }

    if ((STRLEN (pi1NewUsrName)) > CLI_MAX_USERNAME_LEN)
    {
        mmi_printf ("\r\n%%User Name exceeded size limit\r\n");
        return (CLI_FAILURE);
    }

    if (pi1NewDefMode)
    {
        if ((STRLEN (pi1NewDefMode)) > CLI_MAX_USERMODE_LEN)
        {
            mmi_printf ("\r\n%%User Mode exceeded size limit\r\n");
            return (CLI_FAILURE);
        }
    }
    else
    {
        pi1NewDefMode = ai1DefMode;
    }

    if (pi1NewPasswd)
    {
        if ((STRLEN (pi1NewPasswd)) > CLI_MAX_USERPASSWD_LEN)
        {
            mmi_printf ("\r\n%%User Password exceeded size limit\r\n");
            return (CLI_FAILURE);
        }
    }
    else
    {
        pi1NewPasswd = ai1DefPasswd;
    }

    if (!
        (pi1Buf =
         (INT1 *) CliMemAllocMemBlk (gCliUserMemPoolId,
                                     CLI_MAX_USERS_LINE_LEN + 1)))
    {
        mmi_printf ("\r\n%%Memory allocation failure \r\n");
        return (CLI_FAILURE);
    }

    for (i2UserIndex = 0; i2UserIndex < gCliSessions.i2NofUsers; i2UserIndex++)
    {
        STRCPY (pi1Buf, gCliSessions.pi1CliUsers[i2UserIndex]);
        CliSplitUserTokens (pi1Buf, &pi1UserName, &pi1UserId, &pi1Mode,
                            &pi1Passwd, &pi1BlockUserRelTime);

        if (!STRCMP (pi1UserName, pi1NewUsrName))
        {
            mmi_printf ("\r\n%%username : `%s' already exists\r\n",
                        pi1NewUsrName);
            i1RetStatus = CLI_FAILURE;
            break;
        }
        i4Uid = CLI_ATOI (pi1UserId);
        if (pi1NewUid)
        {
            if (i4Uid == i4NewUserId)
            {
                mmi_printf ("\r\n%%userid : `%s' already exists\r\n",
                            pi1NewUid);
                i1RetStatus = CLI_FAILURE;
                break;
            }
        }
        else
        {
            if (i4Uid > i4MaxUserId)
                i4MaxUserId = i4Uid;
        }
    }
    if (i1RetStatus == CLI_SUCCESS)
    {

        i1RetStatus = CliIsUserInGroup (pi1NewUsrName, pi1NewGrps);
        if (i1RetStatus != CLI_SUCCESS)
        {
            i1RetStatus = CliAddUserToGroups ((CHR1 *) pi1NewUsrName,
                                              pi1NewGrps);

            if (i1RetStatus == CLI_FAILURE)
            {
                mmi_printf ("\r\n%%Error in groups `%s'\r\n", pi1NewGrps);
            }
        }
    }
    if (i1RetStatus == CLI_FAILURE)
    {
        MemReleaseMemBlock (gCliUserMemPoolId, (UINT1 *) pi1Buf);
        return (CLI_FAILURE);
    }
    if (pi1NewUid)
    {
        SPRINTF ((CHR1 *) i1UserIdStr, "%d", i4NewUserId);
    }
    else
    {
        SPRINTF ((CHR1 *) i1UserIdStr, "%d", i4MaxUserId + 1);
    }

    mmi_encript_passwd ((CHR1 *) pi1NewPasswd);
    if (pi1BlockUserRelTime == NULL)
    {
        SPRINTF ((CHR1 *) pi1Buf, "%s:%s:%s:%s", (CHR1 *) pi1NewUsrName,
                 (CHR1 *) i1UserIdStr, (CHR1 *) pi1NewDefMode,
                 (CHR1 *) pi1NewPasswd);
    }
    else
    {
        SPRINTF ((CHR1 *) pi1Buf, "%s:%s:%s:%s:%s", (CHR1 *) pi1NewUsrName,
                 (CHR1 *) i1UserIdStr, (CHR1 *) pi1NewDefMode,
                 (CHR1 *) pi1NewPasswd, (CHR1 *) pi1BlockUserRelTime);
    }
    gCliSessions.pi1CliUsers[gCliSessions.i2NofUsers] = pi1Buf;
    gCliSessions.i2NofUsers++;
    gCliSessions.pi1CliUsers[gCliSessions.i2NofUsers] = NULL;
    CliWriteUsers ();
    CliWriteGroups ();
    mmi_printf ("\r\nAdded User -%s successfully\r\n", pi1NewUsrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CliExecUserName                                      */
/*                                                                           */
/* Description        : This function is called to add or delete user.       */
/*                                                                           */
/* Input(s)           : pCliUserInfo - pointer to the structure pCliUserInfo      */
/*                      which contains username,password,confirm password,   */
/*                      user status,add/delete the user and privilege        */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS, Successfull add/delete                  */
/*                      CLI_FAILURE, Unsuccessfull Operation                 */
/*                                                                           */
/*****************************************************************************/

INT1
CliExecUserName (tCliUserInfo * pCliUserInfo)
{
    tCliContext        *pCliContext = NULL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT1                ai1TempPasswd[MAX_PASSWD_LEN];
    INT2                i2Index = 0;
    INT2                i2OldPrivilege = 0;
    UINT1               u1ActiveUser = CLI_FALSE;
    UINT4               u4NoOfUsers = 0;
    BOOL1               bConfirmPwd = CLI_FALSE;
    tFpamAddUser        FpamAddUser;

    UNUSED_PARAM (pCliUserInfo->u4EncryptionType);
    MEMSET (ai1TempPasswd, 0, MAX_PASSWD_LEN);
    MEMSET (&FpamAddUser, 0, sizeof (tFpamAddUser));
    if (FPAM_SUCCESS == FpamIsSetUserMgmtFeature
        (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        bConfirmPwd = CLI_TRUE;
    }

    /* check to verify any privilege level is set by user */
    if (pCliUserInfo->i4PrivilegeLevel == 0)
    {
        i4RetVal =
            (INT1) mmi_search_user ((INT1 *) pCliUserInfo->pi1NewUsrName);
        /* check to verify any privilege level is set by user */
        if (i4RetVal == CLI_SUCCESS)
        {
            CliGetUserPrivilege ((CONST CHR1 *) (pCliUserInfo->pi1NewUsrName),
                                 &i2OldPrivilege);
            pCliUserInfo->i4PrivilegeLevel = (INT4) (i2OldPrivilege);
        }
        else
        {
            pCliUserInfo->i4PrivilegeLevel = CLI_DEFAULT_PRIVILEGE;
        }
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    i4RetVal = (INT1) mmi_search_user ((INT1 *) pCliUserInfo->pi1NewUsrName);
    if ((pCliUserInfo->i1UsrAdd != OSIX_FALSE) && (i4RetVal != CLI_SUCCESS))
    {
        if (gCliSessions.i2NofUsers == MAX_FPAM_FSUSRMGMTTABLE)
        {
            mmi_printf ("\r\n%% Max number of Users reached !\n");
            return CLI_FAILURE;
        }
    }
    if ((STRCMP (pCliUserInfo->pi1NewUsrName, CLI_ROOT_USER) == 0) &&
        (pCliUserInfo->i1UsrAdd == OSIX_FALSE))
    {
        mmi_printf ("\r\n%%Cannot delete root user !!\r\n");
        return CLI_FAILURE;
    }

    /*This check has to be done only for non-application mode */
    if (pCliContext->gu4CliMode != CLI_MODE_APP)
    {
#ifdef MBSM_WANTED
        if (STRCMP (pCliUserInfo->pi1NewUsrName, CLI_MBSM_CHASSIS_USER) != 0)
        {
#endif
            if (CliCheckSpecialPriv () != CLI_SUCCESS)
            {
                if ((gCliSpecialPriv.b1UsernameBased == TRUE) &&
                    (gCliSpecialPriv.b1PrivilegeBased == TRUE))
                {
                    mmi_printf
                        ("\r\n%%Permission denied !! Need to be %s or %s privileged user\r\n",
                         CLI_ROOT_USER, CLI_ROOT_USER);
                }
                else if (gCliSpecialPriv.b1UsernameBased == TRUE)
                {
                    mmi_printf
                        ("\r\n%%Permission denied !! Need to be %s user\r\n",
                         CLI_ROOT_USER);
                }
                else if (gCliSpecialPriv.b1PrivilegeBased == TRUE)
                {
                    mmi_printf
                        ("\r\n%%Permission denied !! Need to be %s privileged user\r\n",
                         CLI_ROOT_USER);
                }
                return CLI_FAILURE;
            }
#ifdef MBSM_WANTED
        }
#endif
    }

    if (pCliUserInfo->i1UsrAdd == OSIX_TRUE)
    {
        if (pCliUserInfo->i1UserStatus != CLI_USER_STATUS_NA)
        {
            if (FPAM_FAILURE ==
                FpamIsSetUserMgmtFeature (FPAM_USER_STATUS_FEATURE))
            {
                mmi_printf ("\r\n%%User status feature is not enabled\r\n");
                return CLI_FAILURE;
            }
            else
            {
                if (CLI_STRCMP
                    (pCliUserInfo->pi1NewUsrName,
                     pCliContext->ai1UserName) == 0)
                {
                    mmi_printf
                        ("\r\n%%User status cannot be updated by self\r\n");
                    return CLI_FAILURE;
                }
            }
        }
        if (CLI_TRUE == bConfirmPwd)
        {
            if (((pCliUserInfo->pi1NewPasswd != NULL)
                 && (pCliUserInfo->pi1ConfirmPasswd == NULL))
                || ((pCliUserInfo->pi1NewPasswd == NULL)
                    && (pCliUserInfo->pi1ConfirmPasswd != NULL)))
            {
                mmi_printf
                    ("\r\n%%Please enter both Password and Confirm password \r\n");
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (pCliUserInfo->pi1ConfirmPasswd != NULL)
            {
                mmi_printf
                    ("\r\n%%Confirm Password Feature is not enabled\r\n");
                return (CLI_FAILURE);

            }
        }
    }
    if ((pCliUserInfo->pi1NewPasswd == NULL)
        && (pCliUserInfo->i1UsrAdd == OSIX_TRUE))
    {
        if (i4RetVal != CLI_SUCCESS)
        {
            CLI_STRCPY (ai1TempPasswd, CLI_DEF_PASSWD);
            pCliUserInfo->pi1NewPasswd = ai1TempPasswd;
            if (CLI_TRUE == bConfirmPwd)
            {
                pCliUserInfo->pi1ConfirmPasswd = ai1TempPasswd;
            }
        }
        /* skip when passwd is changed for an existing user */
        else if (pCliUserInfo->i4PrivilegeLevel == CLI_DEFAULT_PRIVILEGE)
        {
            mmi_printf ("\r\n%%User already exists\r\n");
            return (CLI_FAILURE);
        }
    }
    /* check only for passwd change */
    if ((pCliUserInfo->i1UsrAdd == OSIX_TRUE)
        && (pCliUserInfo->pi1NewPasswd != NULL))
    {
        if (i4RetVal != CLI_ERROR)
        {
            if (strcmp
                ((const CHR1 *) (pCliUserInfo->pi1NewPasswd),
                 CLI_DEF_PASSWD) != 0)
            {
                if (CliCheckUserPasswd
                    (pCliUserInfo->pi1NewUsrName,
                     pCliUserInfo->pi1NewPasswd) == CLI_SUCCESS)
                {
                    CliGetUserPrivilege ((CONST CHR1 *) (pCliUserInfo->
                                                         pi1NewUsrName),
                                         &i2OldPrivilege);

                    if (((INT4) i2OldPrivilege) ==
                        pCliUserInfo->i4PrivilegeLevel)
                    {
                        mmi_printf ("\r\n%%User already exists\r\n");
                        pCliUserInfo->pi1NewPasswd = NULL;
                        pCliUserInfo->pi1ConfirmPasswd = NULL;
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (CLI_TRUE == bConfirmPwd)
        {
            if ((STRLEN (pCliUserInfo->pi1NewPasswd) !=
                 (STRLEN (pCliUserInfo->pi1ConfirmPasswd)))
                ||
                (STRCMP
                 (pCliUserInfo->pi1NewPasswd,
                  pCliUserInfo->pi1ConfirmPasswd) != 0))
            {
                mmi_printf
                    ("\r\n%%Password and Confirm password is not same\r\n");
                pCliUserInfo->pi1NewPasswd = NULL;
                pCliUserInfo->pi1ConfirmPasswd = NULL;
                return (CLI_FAILURE);
            }
        }
    }
    else if (pCliUserInfo->i1UsrAdd == OSIX_FALSE)
    {
        if (i4RetVal == CLI_ERROR)
        {
            mmi_printf ("\r\n%%User does not exist\r\n");
            pCliUserInfo->pi1NewPasswd = NULL;
            pCliUserInfo->pi1ConfirmPasswd = NULL;
            return (CLI_FAILURE);
        }

    }
    CliContextLock ();
    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i2Index]);
        if ((pCliContext->i1Status == CLI_ACTIVE))
        {
            if (CLI_STRCMP
                (pCliUserInfo->pi1NewUsrName, pCliContext->ai1UserName) == 0)
            {
                u1ActiveUser = CLI_TRUE;
                break;
            }
        }
    }

    if (FPAM_SUCCESS !=
        FpamIsSetUserMgmtFeature (FPAM_CHECK_ACTIVE_USER_TO_EDIT_PSWD))
    {
        if ((u1ActiveUser == CLI_TRUE) &&
            (STRCMP (pCliUserInfo->pi1NewUsrName, CLI_ROOT_USER) != 0)
            && (pCliContext->i1passwdMode != REMOTE_LOGIN_RADIUS)
            && (pCliContext->i1passwdMode != REMOTE_LOGIN_TACACS))
        {
            CliContextUnlock ();
            mmi_printf ("\r\n%%Active user cannot be updated \r\n");
            pCliUserInfo->pi1NewPasswd = NULL;
            pCliUserInfo->pi1ConfirmPasswd = NULL;
            return (CLI_FAILURE);
        }
    }

    if ((u1ActiveUser == CLI_TRUE) && (pCliUserInfo->i1UsrAdd == OSIX_FALSE)
        && (pCliContext->i1passwdMode != REMOTE_LOGIN_RADIUS)
        && (pCliContext->i1passwdMode != REMOTE_LOGIN_TACACS))
    {
        CliContextUnlock ();
        mmi_printf ("\r\n%%Active user cannot be deleted \r\n");
        pCliUserInfo->pi1NewPasswd = NULL;
        pCliUserInfo->pi1ConfirmPasswd = NULL;
        return (CLI_FAILURE);
    }

    if (pCliUserInfo->i1UsrAdd == OSIX_FALSE)
    {
        if (FpamUtlDelUser (pCliUserInfo->pi1NewUsrName) == FPAM_FAILURE)
        {
            CliContextUnlock ();
            mmi_printf ("\r\n%%User Deletion Failed \r\n");
            pCliUserInfo->pi1NewPasswd = NULL;
            pCliUserInfo->pi1ConfirmPasswd = NULL;
            return (CLI_FAILURE);
        }
    }
    else
    {
        FpamAddUser.pi1NewUsrName = pCliUserInfo->pi1NewUsrName;
        FpamAddUser.pi1NewPasswd = pCliUserInfo->pi1NewPasswd;
        FpamAddUser.i4PrivilegeLevel = pCliUserInfo->i4PrivilegeLevel;
        FpamAddUser.i1UserStatus = pCliUserInfo->i1UserStatus;
        if (CLI_TRUE == bConfirmPwd)
        {
            FpamAddUser.pi1ConfirmPasswd = pCliUserInfo->pi1ConfirmPasswd;
        }
        if (FpamUtlAddUser (&FpamAddUser) == FPAM_FAILURE)
        {
            CliContextUnlock ();
            if (i4RetVal != CLI_ERROR)
            {
                mmi_printf ("\r\n%%User Modification Failed\r\n");
            }
            else
            {
                mmi_printf ("\r\n%%User Addition Failed\r\n");
            }
            pCliUserInfo->pi1NewPasswd = NULL;
            pCliUserInfo->pi1ConfirmPasswd = NULL;
            return (CLI_FAILURE);
        }
        if (pCliUserInfo->pi1NewPasswd != NULL)
        {
            pCliContext = CliGetContext ();
            if (pCliContext != NULL)
            {
                pCliContext->u1UserNameCreation = TRUE;
                CliPrivilegeAdd ((INT1) (pCliUserInfo->i4PrivilegeLevel),
                                 pCliUserInfo->pi1NewPasswd);
                pCliContext->u1UserNameCreation = FALSE;
            }
        }
    }
    FpamUtlGetNumOfUsers (&u4NoOfUsers);
    gCliSessions.i2NofUsers = (INT2) u4NoOfUsers;
    CliContextUnlock ();
    pCliUserInfo->pi1NewPasswd = NULL;
    pCliUserInfo->pi1ConfirmPasswd = NULL;
    return (CLI_SUCCESS);
}

  /**************************************************************************
 * Function    : CliFpamPasswdValidate
 * Description : This function is used to manage password rules from Cli.
 * Input       : u4Case : Used to select variables to set value. 
 *               u4Value: Value to be set.
 * Output      : none 
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
CliFpamPasswdValidate (UINT4 u4Case, UINT4 u4Value)
{
    if (FpamPasswordValidateRules (u4Case, u4Value) == FPAM_FAILURE)
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

  /**************************************************************************
 * Function    : CliFpamSetPasswdMaxLifeTime
 * Description : This function is used to set Password Max Life Time
 * Input       : u4Value: Value to be set for PasswdMaxLifeTime.
 * Output      : none 
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
CliFpamSetPasswdMaxLifeTime (UINT4 u4Value)
{
    if (FpamSetPasswdMaxLifeTime (u4Value) == FPAM_FAILURE)
    {
        mmi_printf
            ("\r%% Invalid Password Life Time. The range is 0 to 366.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

  /**************************************************************************
 * Function    : CliFpamShowPasswdMaxLifeTime
 * Description : This function is used to display the max password life time.
 * Input       : none
 * Output      : none 
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
CliFpamShowPasswdMaxLifeTime ()
{
    UINT4               u4PasswordMaxLifeTime = 0;
    FpamShowPasswdMaxLifeTime (&u4PasswordMaxLifeTime);
    mmi_printf ("\rPassword Max Life Time: %d\r\n", u4PasswordMaxLifeTime);

    return (CLI_SUCCESS);
}

/**************************************************************************
 * Function    : CliFpamSetMinPasswordLen
 * Description : This function is used to set the minimum password length.
 * Input       : i4MinPassLen
 * Output      : None
 * Returns     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

INT1
CliFpamSetMinPasswordLen (INT4 i4MinPassLen)
{
    if (FpamSetMinPasswordLen (i4MinPassLen) == FPAM_FAILURE)
    {
        mmi_printf ("\r\n%%Invalid password length. The range is (8->20 )\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/**************************************************************************
* Function    : CliFpamShowMinPasswordLen
* Description : This function is used to show the minimum password length configured.
* Input       : None
* Output      : None
* Returns     : CLI_SUCCESS/CLI_FAILURE
*****************************************************************************/

INT1
CliFpamShowMinPasswordLen ()
{
    UINT4               i4MinPasswordLen;

    FpamDisplayMinPasswordLen (&i4MinPasswordLen);
    mmi_printf ("\r\nMinimum Password length  : %d \r\n", i4MinPasswordLen);

    return (CLI_SUCCESS);
}

INT1
CliGroupAdd (INT4 *pi4NewGid, INT1 *pi1NewGrpName)
{
    INT4                i4GrpId;
    INT4                i4MaxGrpId = 0;
    INT2                i2GroupIndex = 0;
    INT1               *pi1GroupName;
    INT1               *pi1GrpId;
    INT1               *pi1Users;
    INT1               *pi1Buf;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT1                i1GroupIdStr[11];

    if (gCliSessions.i2NofGroups == CLI_MAX_GROUPS)
    {
        mmi_printf ("\r\n%% No of groups exceeded the limit \r\n");
        return (CLI_FAILURE);
    }

    if ((STRLEN (pi1NewGrpName)) > CLI_MAX_GRPNAME_LEN)
    {
        mmi_printf ("\r\n%% Group Name exceeds size limit\r\n");
        return (CLI_FAILURE);
    }

    if (pi4NewGid)
    {
        if (*pi4NewGid >= CLI_MAX_GRPID_LEN)
        {
            mmi_printf ("\r\n%% Group ID exceeds size limit <1-999>\r\n");
            return (CLI_FAILURE);
        }
    }

    if (!
        (pi1Buf =
         (INT1 *) CliMemAllocMemBlk (gCliGroupMemPoolId,
                                     CLI_MAX_GROUPS_LINE_LEN + 1)))
    {
        mmi_printf ("\r\n%% Memory allocation failure \r\n");
        return (CLI_FAILURE);
    }

    for (i2GroupIndex = 0; i2GroupIndex < gCliSessions.i2NofGroups;
         i2GroupIndex++)
    {
        STRCPY (pi1Buf, gCliSessions.pi1CliGroups[i2GroupIndex]);
        CliSplitGroupTokens (pi1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);
        i4GrpId = CLI_ATOI (pi1GrpId);
        if (!STRCMP (pi1GroupName, pi1NewGrpName))
        {
            mmi_printf ("\r\n%% groupname : `%s' already exists\r\n",
                        pi1NewGrpName);
            i1RetStatus = CLI_FAILURE;
            break;
        }

        if (pi4NewGid)
        {
            if (i4GrpId == *pi4NewGid)
            {
                mmi_printf ("\r\n%% GroupId : `%d' already exists\r\n",
                            *pi4NewGid);
                i1RetStatus = CLI_FAILURE;
                break;
            }
        }
        else
        {
            if (i4GrpId > i4MaxGrpId)
                i4MaxGrpId = i4GrpId;
        }
    }
    if (i1RetStatus == CLI_FAILURE)
    {
        MemReleaseMemBlock (gCliGroupMemPoolId, (UINT1 *) pi1Buf);
        return (CLI_FAILURE);
    }
    if (pi4NewGid)
    {
        SPRINTF ((CHR1 *) i1GroupIdStr, "%d", *pi4NewGid);
    }
    else
    {
        SPRINTF ((CHR1 *) i1GroupIdStr, "%d", i4MaxGrpId + 1);
    }
    SPRINTF ((CHR1 *) pi1Buf, "%s:%s:", (CHR1 *) pi1NewGrpName,
             (CHR1 *) i1GroupIdStr);
    gCliSessions.pi1CliGroups[gCliSessions.i2NofGroups] = pi1Buf;
    gCliSessions.i2NofGroups++;
    gCliSessions.pi1CliGroups[gCliSessions.i2NofGroups] = NULL;
    CliWriteGroups ();
    mmi_printf ("\r\nAdded Group -%s successfully\r\n", pi1NewGrpName);
    return (CLI_SUCCESS);
}

INT2
CliGroupDel (INT1 *pi1GrpName)
{
    INT2                i2GroupIndex;
    INT2                i2Index;

    if (!STRCMP (pi1GrpName, CLI_ROOT_USER))
    {
        mmi_printf ("\r\n%% Group root cannot be deleted\r\n");
        return (CLI_FAILURE);
    }
    if ((CliGetGroupIndex (pi1GrpName, &i2GroupIndex)) == CLI_FAILURE)
    {
        mmi_printf ("\r\n%% %s: No Such Group\r\n", pi1GrpName);
        return (CLI_FAILURE);
    }
    MemReleaseMemBlock (gCliGroupMemPoolId, (UINT1 *)
                        (gCliSessions.pi1CliGroups[i2GroupIndex]));
    gCliSessions.i2NofGroups--;
    for (i2Index = i2GroupIndex; i2Index < gCliSessions.i2NofGroups; i2Index++)
    {
        gCliSessions.pi1CliGroups[i2Index] =
            gCliSessions.pi1CliGroups[i2Index + 1];
    }
    gCliSessions.pi1CliGroups[i2Index] = NULL;
    CliWriteGroups ();
    mmi_printf ("\r\nDeleted Group -%s successfully\r\n", pi1GrpName);
    return (CLI_SUCCESS);
}

INT1
CliAddUserToGroups (CHR1 * pi1NewUsrName, INT1 *pi1NewGrps)
{
    INT1               *pi1Buf;
    INT1               *pi1NewBuf;
    INT1               *pi1GrpName;
    INT1               *pi1Ptr;
    INT1                i1Char;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT2                i2GroupIndex;
    INT2                i2Len;

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }
    STRCPY (pi1Buf, pi1NewGrps);

    pi1Ptr = pi1Buf;
    pi1GrpName = pi1Ptr;
    while (1)
    {
        pi1Ptr++;
        if (*pi1Ptr == ',' || *pi1Ptr == '\0')
        {
            i1Char = *pi1Ptr;
            *pi1Ptr = '\0';
            i1RetStatus = CliGetGroupIndex (pi1GrpName, &i2GroupIndex);
            if (i1RetStatus == CLI_FAILURE || i1Char == '\0')
                break;
            pi1GrpName = pi1Ptr + 1;
        }
    }
    if (i1RetStatus != CLI_FAILURE)
    {
        STRCPY (pi1Buf, pi1NewGrps);

        pi1Ptr = pi1Buf;
        pi1GrpName = pi1Ptr;

        while (1)
        {
            pi1Ptr++;
            if (*pi1Ptr == ',' || *pi1Ptr == '\0')
            {
                i1Char = *pi1Ptr;
                *pi1Ptr = '\0';
                if (CliIsUserInGroup ((INT1 *) pi1NewUsrName, pi1GrpName)
                    == CLI_SUCCESS)
                {
                    if (i1Char)
                    {
                        pi1GrpName = pi1Ptr + 1;
                        continue;
                    }
                    else
                        break;
                }
                i1RetStatus = CliGetGroupIndex (pi1GrpName, &i2GroupIndex);
                if ((pi1NewBuf =
                     CliMemAllocMemBlk (gCliGroupMemPoolId,
                                        CLI_MAX_GROUPS_LINE_LEN + 1)) == NULL)
                {
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
                i2Len =
                    (INT2) (SPRINTF ((CHR1 *) pi1NewBuf, "%s",
                                     (CHR1 *) gCliSessions.
                                     pi1CliGroups[i2GroupIndex]));
                if (pi1NewBuf[i2Len - 1] != ':')
                {
                    pi1NewBuf[i2Len] = ',';
                    i2Len++;
                }
                if ((STRLEN (pi1NewUsrName) + i2Len) >= CLI_MAX_GROUPS_LINE_LEN)
                {
                    mmi_printf
                        ("\r\n%% Buffer size exceeded, Unable to add User \
                                 to Group\r\n");
                    MemReleaseMemBlock (gCliGroupMemPoolId,
                                        (UINT1 *) pi1NewBuf);
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
                SPRINTF ((CHR1 *) & pi1NewBuf[i2Len], "%s", pi1NewUsrName);
                MemReleaseMemBlock (gCliGroupMemPoolId, (UINT1 *)
                                    gCliSessions.pi1CliGroups[i2GroupIndex]);
                gCliSessions.pi1CliGroups[i2GroupIndex] = pi1NewBuf;
                if (i1Char == '\0')
                    break;
                pi1GrpName = pi1Ptr + 1;
            }
        }
    }
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    return (i1RetStatus);

}

INT1
CliGetUserPrivilege (CONST CHR1 * pi1UserName, INT2 *pi2UserPrivilege)
{
    CHR1                ai1UserName[MAX_USER_NAME_LEN];
    MEMSET (ai1UserName, 0, MAX_USER_NAME_LEN);
    MEMCPY (ai1UserName, pi1UserName, STRLEN (pi1UserName));
    if (FpamGetPrivilege (ai1UserName, pi2UserPrivilege) == FPAM_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;

}

INT1
CliCheckPrivilegeIndex (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegeIndex)
{
    INT1                i1PrivilegeIndex = 0;
    INT1               *pi1Privilege;
    INT1               *pi1Pswd;
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    for (i1PrivilegeIndex = 0;
         i1PrivilegeIndex < (INT1) gCliSessions.i2NofPrivileges;
         i1PrivilegeIndex++)
    {
        u4Len =
            ((STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]) <
              sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                         pi1CliPrivileges[i1PrivilegeIndex]) :
             sizeof (ai1Buf) - 1);
        STRNCPY (ai1Buf, gCliSessions.pi1CliPrivileges[i1PrivilegeIndex],
                 u4Len);
        ai1Buf[u4Len] = '\0';
        CliSplitPrivilegesTokens (ai1Buf, &pi1Privilege, &pi1Pswd);
        if (i1PrivilegeLevel == CLI_ATOI (pi1Privilege))
        {
            i1RetStatus = CLI_SUCCESS;
            *pi1PrivilegeIndex = i1PrivilegeIndex;
            break;
        }
    }
    return (i1RetStatus);
}

INT1
CliGetPrivilegeIndex (INT1 *pi1PrivilegePswd, INT1 *pi1PrivilegeIndex)
{
    INT1                i1PrivilegeIndex = 0;
    INT1               *pi1Privilege;
    INT1               *pi1Pswd;
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    for (i1PrivilegeIndex = 0;
         i1PrivilegeIndex < (INT1) gCliSessions.i2NofPrivileges;
         i1PrivilegeIndex++)
    {
        u4Len =
            ((STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]) <
              sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                         pi1CliPrivileges[i1PrivilegeIndex]) :
             sizeof (ai1Buf) - 1);

        STRNCPY (ai1Buf, gCliSessions.pi1CliPrivileges[i1PrivilegeIndex],
                 u4Len);
        ai1Buf[u4Len] = '\0';
        CliSplitPrivilegesTokens (ai1Buf, &pi1Privilege, &pi1Pswd);
        if (!STRCMP (pi1PrivilegePswd, pi1Pswd))
        {
            i1RetStatus = CLI_SUCCESS;
            *pi1PrivilegeIndex = i1PrivilegeIndex;
            break;
        }
    }
    return (i1RetStatus);
}

INT1
CliGetUserIndex (CONST CHR1 * pi1UserName, INT2 *pi2UserIndex)
{
    INT2                i2UserIndex = 0;
    INT1               *pi1UsrName;
    INT1               *pi1UserId;
    INT1               *pi1Mode;
    INT1               *pi1Groups;
    INT1               *pi1BlockUserRelTime = NULL;
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    for (i2UserIndex = 0; i2UserIndex < gCliSessions.i2NofUsers; i2UserIndex++)
    {
        u4Len =
            ((STRLEN (gCliSessions.pi1CliUsers[i2UserIndex]) <
              sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                         pi1CliUsers[i2UserIndex]) :
             sizeof (ai1Buf) - 1);
        STRNCPY (ai1Buf, gCliSessions.pi1CliUsers[i2UserIndex], u4Len);
        ai1Buf[u4Len] = '\0';
        CliSplitUserTokens (ai1Buf, &pi1UsrName, &pi1UserId, &pi1Mode,
                            &pi1Groups, &pi1BlockUserRelTime);
        if (!STRCMP (pi1UserName, pi1UsrName))
        {
            i1RetStatus = CLI_SUCCESS;
            *pi2UserIndex = i2UserIndex;
            break;
        }
    }
    return (i1RetStatus);
}

INT1
CliGetGroupIndex (INT1 *pi1GrpName, INT2 *pi2GrpIndex)
{
    INT2                i2GroupIndex = 0;
    INT1               *pi1GroupName;
    INT1               *pi1GrpId;
    INT1               *pi1Users;
    INT1                ai1Buf[CLI_MAX_GROUPS_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    for (i2GroupIndex = 0; i2GroupIndex < gCliSessions.i2NofGroups;
         i2GroupIndex++)
    {
        u4Len =
            ((STRLEN (gCliSessions.pi1CliGroups[i2GroupIndex]) <
              sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                         pi1CliGroups[i2GroupIndex]) :
             sizeof (ai1Buf) - 1);
        STRNCPY (ai1Buf, gCliSessions.pi1CliGroups[i2GroupIndex], u4Len);
        ai1Buf[u4Len] = '\0';
        CliSplitGroupTokens (ai1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);
        if (!STRCMP (pi1GroupName, pi1GrpName))
        {
            i1RetStatus = CLI_SUCCESS;
            *pi2GrpIndex = i2GroupIndex;
            break;
        }
    }

    return (i1RetStatus);
}

INT1
CliInitUsersGroups (VOID)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    INT1                ai1RootPasswd[MAX_PASSWD_LEN];
    INT1                ai1GuestPasswd[MAX_PASSWD_LEN];
    INT1                ai1Pswd[MAX_PASSWD_LEN];
    INT1                i1RetStatus = CLI_FAILURE;
    BOOL1               bIsMemFailFlag = OSIX_FALSE;
    INT1                i1log[256];
    tCliCallBackArgs    sCliCallBackArgs;
    UINT4               u4Len = 0;

    MEMSET (&sCliCallBackArgs, 0, sizeof (tCliCallBackArgs));
    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    MEMSET (ai1RootPasswd, 0, sizeof (ai1RootPasswd));
    MEMSET (ai1GuestPasswd, 0, sizeof (ai1GuestPasswd));
    MEMSET (ai1Pswd, 0, sizeof (ai1Pswd));

    if (CliUtilCallBack (CLI_STRICT_PASSWD_CHK,
                         &sCliCallBackArgs) != CLI_SUCCESS)
    {
        CLI_STRCPY (ai1RootPasswd, CLI_STRICT_ROOT_PASSWD);
        CLI_STRCPY (ai1GuestPasswd, CLI_STRICT_GUEST_PASSWD);
        CLI_STRCPY (ai1Pswd, CLI_STRICT_ROOT_PASSWD);
    }
    else
    {
        u4Len = ((STRLEN (CLI_ROOT_PASSWD) < sizeof (ai1RootPasswd)) ?
                 STRLEN (CLI_ROOT_PASSWD) : sizeof (ai1RootPasswd) - 1);
        CLI_STRNCPY (ai1RootPasswd, CLI_ROOT_PASSWD, u4Len);
        ai1RootPasswd[u4Len] = '\0';
        CLI_STRCPY (ai1GuestPasswd, CLI_GUEST_PASSWD);
        CLI_STRCPY (ai1Pswd, CLI_ROOT_PRIVILEGE_PSWD);
    }

    if (CliReadGroups () == CLI_FAILURE || gCliSessions.i2NofGroups == 0)
    {
        if ((gCliSessions.pi1CliGroups[0] =
             (INT1 *) CliMemAllocMemBlk (gCliGroupMemPoolId,
                                         CLI_MAX_GROUPS_LINE_LEN + 1)))
        {
            STRCPY (gCliSessions.pi1CliGroups[0], CLI_ROOT_GROUP);
            gCliSessions.pi1CliGroups[1] = NULL;
            gCliSessions.i2NofGroups = 1;
            CliWriteGroups ();

            if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
            {
                if ((gCliSessions.pi1CliGroups[1] = (INT1 *)
                     CliMemAllocMemBlk (gCliGroupMemPoolId,
                                        CLI_MAX_GROUPS_LINE_LEN + 1)))
                {
                    STRCPY (gCliSessions.pi1CliGroups[1], CLI_GUEST_GROUP);
                    gCliSessions.pi1CliGroups[2] = NULL;
                    gCliSessions.i2NofGroups++;
                    CliWriteGroups ();
                    i1RetStatus = CLI_SUCCESS;
                }
                else
                {
                    bIsMemFailFlag = OSIX_TRUE;
                }
            }
        }
        else
        {
            bIsMemFailFlag = OSIX_TRUE;
        }
    }
    if ((gCliSessions.bIsClCliEnabled == OSIX_TRUE) &&
        ((CliReadPrivileges () == CLI_FAILURE)
         || (gCliSessions.i2NofPrivileges == 0)))
    {
        /* To set default enable password for privilege level 15 */
        if ((gCliSessions.pi1CliPrivileges[0] =
             (INT1 *) CliMemAllocMemBlk (gCliPrivilegesMemPoolId,
                                         CLI_MAX_PRIVILEGES_LINE_LEN)))
        {
            CLI_STRCPY (gCliSessions.pi1CliPrivileges[0],
                        CLI_ROOT_PRIVILEGE_ID);
            mmi_encript_passwd ((CHR1 *) ai1Pswd);
            CLI_STRCAT (gCliSessions.pi1CliPrivileges[0], ":");
            CLI_STRCAT (gCliSessions.pi1CliPrivileges[0], ai1Pswd);
            gCliSessions.pi1CliPrivileges[1] = NULL;
            gCliSessions.i2NofPrivileges = 1;
            CliWritePrivileges ();

        }
        else
        {
            bIsMemFailFlag = OSIX_TRUE;
        }
    }

    if (bIsMemFailFlag == OSIX_TRUE)
    {
        SNPRINTF ((CHR1 *) i1log, sizeof (i1log),
                  "Memory allocation failure \r\n");
        UtlTrcPrint ((CHR1 *) i1log);
    }

    return (i1RetStatus);
}

VOID
CliSplitPrivilegesTokens (INT1 *pi1Buf, INT1 **ppi1Privilege, INT1 **ppi1Pswd)
{
    INT1               *pi1Ptr;

    pi1Ptr = pi1Buf;
    *ppi1Privilege = pi1Buf;

    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1Pswd = pi1Ptr;
}

VOID
CliSplitUserTokens (INT1 *pi1Buf, INT1 **ppi1UserName, INT1 **ppi1UserId,
                    INT1 **ppi1Mode, INT1 **ppi1Passwd, INT1 **ppi1ReleaseTime)
{
    INT1               *pi1Ptr;
    pi1Ptr = pi1Buf;
    *ppi1UserName = pi1Buf;

    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1UserId = pi1Ptr;

    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1Mode = pi1Ptr;

    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1Passwd = pi1Ptr;
    /*Extracting Wrong User Release time */
    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1ReleaseTime = NULL;
            break;
        }
        else
        {
            pi1Ptr++;
        }
    }
    if (*pi1Ptr == ':')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;
        *ppi1ReleaseTime = pi1Ptr;
    }

}

VOID
CliSplitGroupTokens (INT1 *pi1Buf, INT1 **ppi1GroupName,
                     INT1 **ppi1GroupId, INT1 **ppi1Users)
{
    INT1               *pi1Ptr;

    pi1Ptr = pi1Buf;
    *ppi1GroupName = pi1Buf;
    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1GroupId = pi1Ptr;

    while (*pi1Ptr != ':')
        pi1Ptr++;

    *pi1Ptr = '\0';
    pi1Ptr++;

    *ppi1Users = pi1Ptr;
    return;
}

/*
INT1 CliPrintUsers()
{
    INT2 i2UserIndex = 0;
    INT1 *pi1UserName;
    INT1 *pi1UserId;
    INT1 *pi1Passwd;
    INT1 *pi1Mode;
    INT1 *pi1Buf;

    pi1Buf = (INT1 *)CLI_ALLOC( CLI_MAX_USERS_LINE_LEN+1, INT1);
    for( i2UserIndex = 0; i2UserIndex < i2NofUsers; i2UserIndex++) 
    {
        STRCPY( pi1Buf, pi1CliUsers[ i2UserIndex]);
        CliSplitUserTokens( pi1Buf, &pi1UserName, &pi1UserId, &pi1Mode, 
                                                            &pi1Passwd);
        mmi_printf("%s\n", pi1CliUsers[ i2UserIndex]);
        mmi_printf( "%s | %s | %s | %s\n", pi1UserName, pi1UserId, pi1Mode, 
                                                              pi1Passwd);
    }
    CLI_FREE( pi1Buf);
    return (CLI_SUCCESS);
}

INT1 CliPrintGroups(VOID)
{
    INT2 i2GroupIndex = 0;
    INT1 *pi1GroupName;
    INT1 *pi1GrpId;
    INT1 *pi1Users;
    INT1 *pi1Buf;

    pi1Buf = (INT1 *)CLI_ALLOC( CLI_MAX_GROUPS_LINE_LEN+1, INT1);
    for( i2GroupIndex = 0; i2GroupIndex < i2NofGroups; i2GroupIndex++) 
    {
        STRCPY( pi1Buf, pi1CliGroups[ i2GroupIndex]);
        CliSplitGroupTokens( pi1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);
        mmi_printf("%s\n", pi1CliGroups[ i2GroupIndex]);
        mmi_printf( "%s | %s | %s \n", pi1GroupName, pi1GrpId, pi1Users);
    }
    CLI_FREE( pi1Buf);
    return (CLI_SUCCESS);
}
*/
INT1
CliIsUserInGroup (INT1 *pi1UserName, INT1 *pi1GroupName)
{
    INT1               *pi1GrpId;
    INT1               *pi1Users;
    INT1               *pi1Usr;
    INT1                ai1Buf[CLI_MAX_GROUPS_LINE_LEN + 1];
    INT1               *pi1Ptr;
    INT2                i2GroupIndex;
    INT1                i1RetStatus = CLI_FAILURE;
    INT1                i1Char;
    INT2                i2Priv = 0;
    UINT4               u4Len = 0;
    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    if (!STRCMP (pi1UserName, CLI_ROOT_USER))
        return (CLI_SUCCESS);

    if ((CliGetUserPrivilege ((CONST CHR1 *) pi1UserName, &i2Priv) ==
         CLI_FAILURE) && ((INT4) gu1IssLoginAuthMode != LOCAL_LOGIN))
    {
        i2Priv = (INT2) CLI_ATOI (CLI_ROOT_PRIVILEGE_ID);
    }

    /* Checking logged user has root privilege */
    if (i2Priv == (INT2) CLI_ATOI (CLI_ROOT_PRIVILEGE_ID))
    {
        return (CLI_SUCCESS);
    }

    if (!STRCMP (pi1GroupName, "default"))
        return (CLI_SUCCESS);

    i1RetStatus = CliGetGroupIndex (pi1GroupName, &i2GroupIndex);
    if (i1RetStatus == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    u4Len =
        ((STRLEN (gCliSessions.pi1CliGroups[i2GroupIndex]) <
          sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                     pi1CliGroups[i2GroupIndex]) :
         sizeof (ai1Buf) - 1);
    STRNCPY (ai1Buf, gCliSessions.pi1CliGroups[i2GroupIndex], u4Len);
    ai1Buf[u4Len] = '\0';
    CliSplitGroupTokens (ai1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);

    pi1Ptr = pi1Users;
    pi1Usr = pi1Ptr;

    i1RetStatus = CLI_FAILURE;
    if (STRLEN (pi1Users) > 0)
    {
        while (1)
        {
            pi1Ptr++;
            if (*pi1Ptr == ',' || *pi1Ptr == '\0')
            {
                i1Char = *pi1Ptr;
                *pi1Ptr = '\0';
                if (!STRCMP (pi1UserName, pi1Usr))
                {
                    i1RetStatus = CLI_SUCCESS;
                    break;
                }
                if (i1Char == '\0')
                    break;
                pi1Usr = pi1Ptr + 1;
            }
        }
    }
    return (i1RetStatus);
}

INT1
CliIsUserInPrivilege (INT1 *pi1UserName, INT1 i1CmdPrivilege)
{
    INT2                i2PrivilegeLevel;
    INT1                i1RetStatus = CLI_FAILURE;

    if (!STRCMP (pi1UserName, CLI_ROOT_USER))
        return (CLI_SUCCESS);

    if ((i1CmdPrivilege == 1))
        return (CLI_SUCCESS);

    i1RetStatus =
        CliGetUserPrivilege ((CONST CHR1 *) pi1UserName, &i2PrivilegeLevel);
    if (i1RetStatus != CLI_FAILURE)
    {
        if (i1CmdPrivilege <= i2PrivilegeLevel)
            i1RetStatus = CLI_SUCCESS;
        else
            i1RetStatus = CLI_FAILURE;
    }
    return (i1RetStatus);
}

INT1
CliDeleteUserFromGroup (CONST CHR1 * pi1UserName, INT1 *pi1GroupName)
{
    INT1               *pi1GrpId;
    INT1               *pi1Users;
    INT1               *pi1Usr;
    INT1               *pi1Buf;
    INT1               *pi1Ptr;
    INT2                i2GroupIndex;
    INT1                i1RetStatus = CLI_FAILURE;
    INT1                i1Char;
    INT1               *pi1TempUsers = NULL;

    if (!STRCMP (pi1UserName, CLI_ROOT_USER))
        return (CLI_SUCCESS);

    if (!STRCMP (pi1GroupName, "default"))
        return (CLI_SUCCESS);

    i1RetStatus = CliGetGroupIndex (pi1GroupName, &i2GroupIndex);
    if (i1RetStatus == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1TempUsers, INT1);
    if (pi1TempUsers == NULL)
    {
        CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
        return CLI_FAILURE;
    }
    MEMSET (pi1TempUsers, '\0', CLI_MAX_USERS_LINE_LEN + 1);
    STRCPY (pi1Buf, gCliSessions.pi1CliGroups[i2GroupIndex]);
    CliSplitGroupTokens (pi1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);

    pi1Ptr = pi1Users;
    pi1Usr = pi1Ptr;

    i1RetStatus = CLI_FAILURE;
    if (STRLEN (pi1Users) > 0)
    {
        while (1)
        {
            pi1Ptr++;
            if (*pi1Ptr == ',' || *pi1Ptr == '\0')
            {
                i1Char = *pi1Ptr;
                *pi1Ptr = '\0';
                if (!STRCMP (pi1UserName, pi1Usr))
                {
                    if (i1Char == '\0')
                        break;
                }
                else
                {
                    STRCAT (pi1TempUsers, pi1Usr);
                    STRCAT (pi1TempUsers, ",");
                }
                if (i1Char == '\0')
                    break;
                pi1Usr = pi1Ptr + 1;
            }
        }
        /* To Remove the last ',' */
        pi1TempUsers[STRLEN (pi1TempUsers) - 1] = '\0';
        pi1Users = pi1TempUsers;
        SPRINTF ((CHR1 *) pi1Buf, "%s:%s:%s", (CHR1 *) pi1GroupName,
                 (CHR1 *) pi1GrpId, (CHR1 *) pi1Users);
        STRCPY (gCliSessions.pi1CliGroups[i2GroupIndex], pi1Buf);
        CliWriteGroups ();
        CliReadGroups ();
    }

    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1TempUsers);
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    return (i1RetStatus);
}

INT2
CliPrivilegeAdd (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegePswd)
{
    tCliContext        *pCliContext = NULL;
    INT1               *pi1Privilege = NULL;
    INT1               *pi1Pswd = NULL;
    INT1                i1PrivilegeIndex;
    INT1                i1Status = CLI_FAILURE;
    INT1               *pi1Buf;
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    UINT4               u4Len = 0;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    if (i1PrivilegeLevel >= CLI_MAX_PRIVILEGES)
    {
        mmi_printf
            ("\r\n%%Privilege level should be less than MAX privilege level\r\n");
        return (CLI_FAILURE);
    }

    if ((pi1Buf =
         CliMemAllocMemBlk (gCliPrivilegesMemPoolId,
                            CLI_MAX_PRIVILEGES_LINE_LEN)) == NULL)
    {
        return (CLI_FAILURE);
    }
    if (CLI_STRLEN (pi1PrivilegePswd) > MAX_PASSWD_LEN)
    {
        mmi_printf ("\r\n Long Password string length, password"
                    "string stripped of to length %d\r\n", MAX_PASSWD_LEN);
        pi1PrivilegePswd[MAX_PASSWD_LEN] = '\0';
    }

    SPRINTF ((CHR1 *) pi1Buf, "%d:%s", i1PrivilegeLevel,
             (CHR1 *) pi1PrivilegePswd);
    if ((i1Status =
         CliCheckPrivilegeIndex (i1PrivilegeLevel,
                                 &i1PrivilegeIndex)) == CLI_SUCCESS)
    {
        if (pCliContext->u1UserNameCreation == TRUE)
        {
            u4Len =
                ((STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]) <
                  sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                             pi1CliPrivileges[i1PrivilegeIndex])
                 : sizeof (ai1Buf) - 1);
            STRNCPY (ai1Buf, gCliSessions.pi1CliPrivileges[i1PrivilegeIndex],
                     u4Len);
            ai1Buf[u4Len] = '\0';
            CliSplitPrivilegesTokens (ai1Buf, &pi1Privilege, &pi1Pswd);
            SPRINTF ((CHR1 *) pi1Buf, "%d:%s", i1PrivilegeLevel,
                     (CHR1 *) pi1Pswd);
        }
        MemReleaseMemBlock (gCliPrivilegesMemPoolId, (UINT1 *)
                            (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]));
        gCliSessions.pi1CliPrivileges[i1PrivilegeIndex] = pi1Buf;
    }
    else
    {
        i1PrivilegeIndex = (INT1) gCliSessions.i2NofPrivileges;
        gCliSessions.pi1CliPrivileges[i1PrivilegeIndex] = pi1Buf;
        gCliSessions.i2NofPrivileges++;
        gCliSessions.pi1CliPrivileges[gCliSessions.i2NofPrivileges] = NULL;
    }
    if (pCliContext->u1UserNameCreation == FALSE)
    {
        SPRINTF ((CHR1 *) pi1Buf, "%d:%s", i1PrivilegeLevel,
                 (CHR1 *) pi1PrivilegePswd);
    }
    gCliSessions.pi1CliPrivileges[i1PrivilegeIndex] = pi1Buf;
    CliWritePrivileges ();

    return (CLI_SUCCESS);
}

INT2
CliPrivilegeDel (INT1 i1PrivilegeIndex)
{
    INT2                i2Index;

    if (i1PrivilegeIndex >= CLI_MAX_PRIVILEGES)
    {
        mmi_printf ("\r\n%%Privilege level doesn't exists\r\n");
        return (CLI_FAILURE);
    }

    /* Deleting the privilege */
    if (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex] != NULL)
    {
        MEMSET (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex], '\0',
                STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]));
        CliWritePrivileges ();
        for (i2Index = 0; i2Index < gCliSessions.i2NofPrivileges; i2Index++)
        {
            MemReleaseMemBlock (gCliPrivilegesMemPoolId, (UINT1 *)
                                (gCliSessions.pi1CliPrivileges[i2Index]));
            gCliSessions.pi1CliPrivileges[i2Index] = NULL;
        }
        gCliSessions.i2NofPrivileges = 0;
        CliReadPrivileges ();
    }
    else
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

INT2
CliUserDel (CHR1 * pi1UserName)
{
    INT2                i2GroupIndex;
    INT2                i2Temp;
    INT2                i2UserIndex;
    INT1               *pi1Buf;
    INT1               *pi1GroupName;
    INT1               *pi1GrpId;
    INT1               *pi1Users;

    if ((CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    if (!STRCASECMP (pi1UserName, CLI_ROOT_USER))
    {
        mmi_printf ("\r\n%%User %s cannot be deleted\r\n", pi1UserName);
        return (CLI_FAILURE);
    }
    if ((CliGetUserIndex ((CONST CHR1 *) pi1UserName, &i2UserIndex))
        == CLI_FAILURE)
    {
        mmi_printf ("\r\n%% %s: No Such User\r\n", pi1UserName);
        return (CLI_FAILURE);
    }

    /* Deleting the User from the Groups */
    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }

    for (i2GroupIndex = 0; i2GroupIndex < gCliSessions.i2NofGroups;
         i2GroupIndex++)
    {
        STRCPY (pi1Buf, gCliSessions.pi1CliGroups[i2GroupIndex]);
        CliSplitGroupTokens (pi1Buf, &pi1GroupName, &pi1GrpId, &pi1Users);
        if ((CliIsUserInGroup ((INT1 *) pi1UserName, pi1GroupName))
            == CLI_SUCCESS)
        {
            CliDeleteUserFromGroup ((CONST CHR1 *) pi1UserName, pi1GroupName);
        }
    }

    /* Deleting the User from Users list */
    MEMSET (gCliSessions.pi1CliUsers[i2UserIndex], '\0',
            STRLEN (gCliSessions.pi1CliUsers[i2UserIndex]));
    MemReleaseMemBlock (gCliUserMemPoolId,
                        (UINT1 *) (gCliSessions.pi1CliUsers[i2UserIndex]));
    /*Put users name in continous location */
    for (i2Temp = i2UserIndex; i2Temp < gCliSessions.i2NofUsers - 1; i2Temp++)
    {
        gCliSessions.pi1CliUsers[i2Temp] = gCliSessions.pi1CliUsers[i2Temp + 1];
    }
    gCliSessions.pi1CliUsers[i2Temp] = NULL;
    gCliSessions.i2NofUsers--;
    mmi_printf ("\r\nDeleted user -%s successfully\r\n", pi1UserName);
    CliWriteUsers ();

    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    CliReadUsers ();

    return (CLI_SUCCESS);
}

INT2
CliCheckPrivilegePswd (INT1 i1PrivilegeIndex, INT1 *pi1PrivilegePswd)
{
    INT1               *pi1Privilege;
    INT1               *pi1Pswd;
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    if ((i1PrivilegeIndex >= CLI_MAX_PRIVILEGES) || !(pi1PrivilegePswd))
        return (CLI_FAILURE);

    if (CLI_STRLEN (pi1PrivilegePswd) >= MAX_PASSWD_LEN)
        return CLI_FAILURE;

    u4Len =
        ((STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]) <
          sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                     pi1CliPrivileges[i1PrivilegeIndex]) :
         sizeof (ai1Buf) - 1);
    STRNCPY (ai1Buf, gCliSessions.pi1CliPrivileges[i1PrivilegeIndex], u4Len);
    ai1Buf[u4Len] = '\0';
    CliSplitPrivilegesTokens (ai1Buf, &pi1Privilege, &pi1Pswd);
    if (!(CLI_STRCMP (pi1PrivilegePswd, pi1Pswd)))
    {
        i1RetStatus = CLI_SUCCESS;
    }
    return i1RetStatus;
}

INT2
CliCheckPrivilege (INT1 i1PrivilegeLevel, INT1 *pi1PrivilegePswd)
{
    INT1                i1PrivilegeIndex = 0;
    INT1               *pi1Privilege;
    INT1               *pi1Pswd;
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    INT1                i1RetStatus = CLI_FAILURE;
    INT2                i2Index = 0;
    INT1                ai1TempPasswd[MAX_PASSWD_LEN];
    UINT2               u2PasswdLen = 0;
    UINT4               u4Len = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    if (!(i1PrivilegeLevel))
        return (CLI_SUCCESS);

    if (CLI_STRLEN (pi1PrivilegePswd) >= MAX_PASSWD_LEN)
        return CLI_FAILURE;

    for (i1PrivilegeIndex = 0;
         i1PrivilegeIndex < (INT1) gCliSessions.i2NofPrivileges;
         i1PrivilegeIndex++)
    {
        u4Len =
            ((STRLEN (gCliSessions.pi1CliPrivileges[i1PrivilegeIndex]) <
              sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                         pi1CliPrivileges[i1PrivilegeIndex]) :
             sizeof (ai1Buf) - 1);
        STRNCPY (ai1Buf, gCliSessions.pi1CliPrivileges[i1PrivilegeIndex],
                 u4Len);
        ai1Buf[u4Len] = '\0';
        CliSplitPrivilegesTokens (ai1Buf, &pi1Privilege, &pi1Pswd);
        if ((i1PrivilegeLevel == CLI_ATOI (pi1Privilege)))
        {
            if (CLI_STRLEN (pi1PrivilegePswd) !=
                (u2PasswdLen = (UINT2) CLI_STRLEN (pi1Pswd)))
            {
                break;
            }
            STRCPY (ai1TempPasswd, pi1PrivilegePswd);
            i1RetStatus = CLI_SUCCESS;
            if (u2PasswdLen >= MAX_PASSWD_LEN)
            {
                continue;
            }
            for (i2Index = 0; i2Index < u2PasswdLen; i2Index++)
            {

                if (i2Index != u2PasswdLen - 1)
                {
                    ai1TempPasswd[i2Index] ^= ai1TempPasswd[i2Index + 1];
                }
                else
                {
                    ai1TempPasswd[i2Index] ^= MMI_SPEC_CHAR;
                }
                if (ai1TempPasswd[i2Index] == '\n')
                {
                    ai1TempPasswd[i2Index] = MMI_SPEC_CHAR;
                }
                if (ai1TempPasswd[i2Index] == '\0')
                {
                    ai1TempPasswd[i2Index] = MMI_SPEC_CHAR + 1;
                }
                if (pi1Pswd[i2Index] != ai1TempPasswd[i2Index])
                {
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
            }
            break;
        }
    }
    if ((i1RetStatus != CLI_SUCCESS) && !(gCliSessions.i2NofPrivileges))
    {

    }
    return i1RetStatus;
}

#ifdef PAM_AUTH_WANTED
static INT4
pam_passwd_conv (INT4 i4Input, const struct pam_message **msg,
                 struct pam_response **resp, VOID *data)
{
    struct pam_response *reply;
    INT4                i4Index;

    *resp = NULL;

    if (i4Input <= 0 || i4Input > PAM_MAX_NUM_MSG)
    {
        return (PAM_CONV_ERR);
    }

    if ((reply = calloc (i4Input, sizeof (*reply))) == NULL)
    {
        return (PAM_CONV_ERR);
    }

    for (i4Index = 0; i4Index < i4Input; ++i4Index)
    {
        switch (PAM_MSG_MEMBER (msg, i4Index, msg_style))
        {
            case PAM_PROMPT_ECHO_OFF:
                if (pam_password == NULL)
                    goto fail;
                if ((reply[i4Index].resp = strdup (pam_password)) == NULL)
                    goto fail;
                reply[i4Index].resp_retcode = PAM_SUCCESS;
                break;
            case PAM_ERROR_MSG:
            case PAM_TEXT_INFO:
                if ((reply[i4Index].resp = strdup ("")) == NULL)
                    goto fail;
                reply[i4Index].resp_retcode = PAM_SUCCESS;
                break;
            default:
                goto fail;
        }
    }
    *resp = reply;
    return (PAM_SUCCESS);

  fail:
    for (i4Index = 0; i4Index < i4Input; i4Index++)
    {
        if (reply[i4Index].resp != NULL)
            free (reply[i4Index].resp);
    }
    free (reply);
    UNUSED_PARAM (data);
    return (PAM_CONV_ERR);
}
static struct pam_conv passwd_conv = { pam_passwd_conv, NULL };
#endif

INT2
CliAuthenticateUserPasswd (INT1 *pi1UserName, CONST INT1 *pi1UserPasswd)
{
    tCliContext        *pCliContext = NULL;
    INT1                i1Passwd[MAX_PASSWD_LEN + 1];
    INT1                i1AuthStatus = ISS_TRUE;
    INT2                i2TempPrivId = 0;
#ifdef ISS_WANTED
    tOsixTaskId         CurrTaskId;
    UINT4               u4Event = 0;
    INT1                i1ServerStatus = ISS_TRUE;
    INT4                i4Len;
#endif
    INT4                i4Count;
    UINT4               u4Len = 0;
#ifdef PAM_AUTH_WANTED
    pam_handle_t       *pamh = NULL;
    INT4                i4RetVal = 0;
    CONST INT1         *pi1LocalUserName = NULL;
    struct pam_conv     conv = { LdapPamTtyConv, NULL };
#endif

    if ((pi1UserName == NULL) || (pi1UserPasswd == NULL))
        return (CLI_FAILURE);

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (i1Passwd, 0, MAX_PASSWD_LEN);
    i4Len = STRLEN (pi1UserPasswd);
    if (i4Len > MAX_PASSWD_LEN)
    {
        return (CLI_FAILURE);
    }
    STRCPY (i1Passwd, pi1UserPasswd);

#ifdef ISS_WANTED
    CurrTaskId = OsixGetCurTaskId ();
    if ((gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS)
        || (gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
    {
        /* login authentication mode  is radius */
#ifdef  RADIUS_WANTED
        if (CliRadiusAuthenticate (pi1UserName,
                                   (INT1 *) i1Passwd,
                                   CurrTaskId, &i1ServerStatus) == CLI_FAILURE)
#endif
        {
            /* If the radius server is down and local authentication is enable
             * then local authentication is done*/
            if ((i1ServerStatus == ISS_FALSE)
                && (gu1IssLoginAuthMode ==
                    REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
            {
                if (CliCheckUserPasswd (pi1UserName,
                                        (INT1 *) i1Passwd) == CLI_SUCCESS)
                {
                    /* Local authentication is success */
                    pCliContext->i1UserAuth = TRUE;
                    STRCPY (pCliContext->ai1UserName, pi1UserName);
                    FpamGetPrivilege ((CHR1 *) pi1UserName, &i2TempPrivId);
                    pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
                    if (pCliContext->gu4CliMode == CLI_MODE_SSH)
                    {
                        CliSshResetSessionAuth (OSIX_NTOHL
                                                (pCliContext->
                                                 au4ClientIpAddr[3]));
                    }
                    pCliContext->u1Retries = 0;
                    return (CLI_SUCCESS);
                }
                i1AuthStatus = ISS_FALSE;
            }
            else if ((i1ServerStatus == ISS_FALSE)
                     && (gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS))
            {
                /* In case of radius, since authentication is not done,
                 * make auth status as false
                 */
                i1AuthStatus = ISS_FALSE;
            }
        }
    }
    else if ((gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS)
             || (gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
    {
        /* login authentication mode is radius */
#ifdef TACACS_WANTED
        if (CliTacacsAuthenticate (pi1UserName,
                                   (INT1 *) i1Passwd,
                                   CurrTaskId, &i1ServerStatus) == CLI_FAILURE)
#endif
        {
            /* If the tacacs server is down and local authentication is enable
             * then local authentication is done*/
            if ((i1ServerStatus == ISS_FALSE)
                && (gu1IssLoginAuthMode ==
                    REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
            {
                if (CliCheckUserPasswd
                    (pi1UserName, (INT1 *) i1Passwd) == CLI_SUCCESS)
                {

                    /* Local authentication is success */
                    pCliContext->i1UserAuth = TRUE;
                    STRCPY (pCliContext->ai1UserName, pi1UserName);
                    FpamGetPrivilege ((CHR1 *) pi1UserName, &i2TempPrivId);
                    pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
                    if (pCliContext->gu4CliMode == CLI_MODE_SSH)
                    {
                        CliSshResetSessionAuth (OSIX_NTOHL
                                                (pCliContext->
                                                 au4ClientIpAddr[3]));
                    }
                    pCliContext->u1Retries = 0;
                    return (CLI_SUCCESS);
                }
            }
            i1AuthStatus = ISS_FALSE;
        }
    }
    else if (gu1IssLoginAuthMode == LOCAL_LOGIN)
#endif
    {
        /* login authentication mode is local */
        if (CliCheckUserPasswd (pi1UserName, (INT1 *) i1Passwd) == CLI_SUCCESS)
        {
            /* Local authentication is success */
            pCliContext->i1UserAuth = TRUE;
            u4Len =
                ((STRLEN (pi1UserName) <
                  sizeof (pCliContext->
                          ai1UserName)) ? STRLEN (pi1UserName) :
                 sizeof (pCliContext->ai1UserName) - 1);
            STRNCPY (pCliContext->ai1UserName, pi1UserName, u4Len);
            pCliContext->ai1UserName[u4Len] = '\0';
            FpamGetPrivilege ((CHR1 *) pi1UserName, &i2TempPrivId);
            pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
            if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                CliSshResetSessionAuth (OSIX_NTOHL
                                        (pCliContext->au4ClientIpAddr[3]));
            }
            pCliContext->u1Retries = 0;
            return (CLI_SUCCESS);
        }
        i1AuthStatus = ISS_FALSE;
    }
#ifdef PAM_AUTH_WANTED
    else if (gu1IssLoginAuthMode == PAM_LOGIN)
    {
        pamh = NULL;
        pi1LocalUserName = pi1UserName;
        i4RetVal = pam_start ("system-auth", pi1LocalUserName, &conv, &pamh);
        if (i4RetVal != PAM_SUCCESS)
        {
            return CLI_FAILURE;
        }
        pam_password = pi1UserPasswd;

        i4RetVal = pam_set_item (pamh, PAM_CONV, (const void *) &passwd_conv);
        if (i4RetVal != PAM_SUCCESS)
        {
            return CLI_FAILURE;
        }
        i4RetVal = pam_authenticate (pamh, PAM_AUTH_ERR);
        if (i4RetVal == PAM_SUCCESS)
        {
            if (NULL != pam_getenv (pamh, "issPrivilege"))
            {
                pCliContext->i1PrivilegeLevel =
                    CLI_ATOI (pam_getenv (pamh, "issPrivilege"));
            }
            /* Local authentication is success */
            STRCPY (pCliContext->ai1UserName, pi1UserName);
            if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                CliSshResetSessionAuth (OSIX_NTOHL
                                        (pCliContext->au4ClientIpAddr[3]));
            }
            pCliContext->u1Retries = 0;
            pCliContext->i1UserAuth = TRUE;
            pam_end (pamh, i4RetVal);
            mmi_printf ("\r\n PAM AUTHENTICATION SUCCESS \r\n");
            return (CLI_SUCCESS);
        }
        else
        {
            pam_end (pamh, i4RetVal);
            mmi_printf ("\r\n PAM AUTHENTICATION FAILURE \r\n");
            return (CLI_FAILURE);
        }
    }
#endif
    /* wait for radius or tacacs - server responses 
     * in case of remote authentication.
     * If reject event is received, remote authentication 
     * is failed. Otherwise authentication is success */

    /* To avoid many CliHandleAuthFailure call, used i1AuthStatus 
     * variable. i1AuthStatus may be false and i1ServerStatus
     * may be true in remote auth failed cases */
#ifdef ISS_WANTED
    if ((((gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS)
          || (gu1IssLoginAuthMode ==
              REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
         && (i1ServerStatus == ISS_TRUE) && (i1AuthStatus == ISS_TRUE))
        ||
        (((gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS)
          || (gu1IssLoginAuthMode ==
              REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL))
         && (i1ServerStatus == ISS_TRUE) && (i1AuthStatus == ISS_TRUE)))
    {
        while (1)
        {
            if (OsixEvtRecv (CurrTaskId,
                             (CLI_REJECT_EVENT |
                              CLI_ACCEPT_EVENT | CLI_RADIUS_TIMEOUT_EVENT),
                             OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
            {
                if ((u4Event & CLI_REJECT_EVENT) == CLI_REJECT_EVENT)
                {
                    i1AuthStatus = ISS_FALSE;
                    break;

                }

                if (((u4Event & CLI_RADIUS_TIMEOUT_EVENT) ==
                     CLI_RADIUS_TIMEOUT_EVENT)
                    || ((u4Event & CLI_TACACS_TIMEOUT_EVENT) ==
                        CLI_TACACS_TIMEOUT_EVENT))
                {
                    if ((gu1IssLoginAuthMode ==
                         REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL)
                        || (gu1IssLoginAuthMode ==
                            REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL))
                    {
                        /* since timeout happened in radius, if fallback
                         * option is set, check with local authenticaion
                         */
                        if (CliCheckUserPasswd (pi1UserName,
                                                (INT1 *) i1Passwd) ==
                            CLI_SUCCESS)
                        {
                            /* Local authentication is success */
                            pCliContext->i1UserAuth = TRUE;
                            STRCPY (pCliContext->ai1UserName, pi1UserName);
                            FpamGetPrivilege ((CHR1 *) pi1UserName,
                                              &i2TempPrivId);
                            pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
                            if (pCliContext->gu4CliMode == CLI_MODE_SSH)
                            {
                                CliSshResetSessionAuth (OSIX_NTOHL
                                                        (pCliContext->
                                                         au4ClientIpAddr[3]));
                            }
                            pCliContext->u1Retries = 0;
                            return (CLI_SUCCESS);
                        }
                    }

                    i1AuthStatus = ISS_FALSE;
                    break;

                }

                if ((u4Event & CLI_ACCEPT_EVENT) == CLI_ACCEPT_EVENT)
                {
                    /* Remote authentication is success */
                    pCliContext->i1UserAuth = TRUE;
                    pCliContext->i1passwdMode = gu1IssLoginAuthMode;
                    STRCPY (pCliContext->ai1UserName, pi1UserName);
                    if (pCliContext->gu4CliMode == CLI_MODE_SSH)
                    {
                        CliSshResetSessionAuth (OSIX_NTOHL
                                                (pCliContext->
                                                 au4ClientIpAddr[3]));
                    }
                    pCliContext->u1Retries = 0;
                    return (CLI_SUCCESS);
                }
            }
        }                        /* End of while(1) */
    }
    /* Fallback to local option is avilable to do local 
     * authtication incase of Remote authentication failure */

    /* To handle authentication failures for telnet case and console case
     * used i1AuthStatus. SSH has already taken care of
     * this failure case */
    if (i1AuthStatus == ISS_FALSE)
    {
        /* CLI SSH TIMER */
        CliContextLock ();
        if (pCliContext->gu4CliMode == CLI_MODE_SSH)
        {
            if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
            {
                i4Count =
                    CliGetSessionAuth (OSIX_NTOHL
                                       (pCliContext->au4ClientIpAddr[3]));
            }
            else
            {
                i4Count = CliGetSessionAuthIpv6 (pCliContext->au4ClientIpAddr);
            }
            if ((i4Count != -1) && (gSshSessionAuth[i4Count].u1Blocked != 1))
            {
                gSshSessionAuth[i4Count].u4FailCount++;
                if (gSshSessionAuth[i4Count].u4FailCount >= CLI_MAX_NUM_ATTACK)
                {
                    if (TmrStartTimer
                        (gSshTimerLst, &(gSshSessionAuth[i4Count].timerNode),
                         CLI_SSH_BLOCK_TIME_IN_SECONDS * 100) == TMR_SUCCESS)
                    {
                        gSshSessionAuth[i4Count].u1Blocked = 1;
                        if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
                        {
                            IpMgrBlockTableInsert (OSIX_NTOHL
                                                   (pCliContext->
                                                    au4ClientIpAddr[3]),
                                                   SSH_DEFAULT_PORT);
                        }
                    }
                }

            }
        }
        CliContextUnlock ();

        if ((pCliContext->gu4CliMode == CLI_MODE_TELNET)
            || (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
            || (pCliContext->gu4CliMode == CLI_MODE_SSH))
        {
            CliHandleAuthFailure (pCliContext, pi1UserName);
        }
    }
#endif

    if (pCliContext->i4AuthStatus == 1)
    {
        pCliContext->i4AuthStatus = 0;
        pCliContext->u1Retries = 0;
        return (CLI_SUCCESS);
    }

    return (CLI_FAILURE);
}

INT2
CliCheckUserPasswd (INT1 *pi1UserName, CONST INT1 *pi1UserPasswd)
{
    if (FpamCheckPassword ((CHR1 *) pi1UserName, pi1UserPasswd) != FPAM_SUCCESS)
    {
        FpamSetLoginAttempts ((CHR1 *) pi1UserName,
                              FPAM_INC_FAILED_LOGIN_ATTEMPT);
        return CLI_FAILURE;
    }
    else
    {
        FpamSetLoginAttempts ((CHR1 *) pi1UserName, FPAM_RESET_LOGIN_ATTEMPT);
        FpamUtlUpdateActiveUsers ((CHR1 *) pi1UserName, FPAM_USER_LOGGED_IN);
        return CLI_SUCCESS;
    }
}

INT2
CliSetUserPrivilege (INT2 i2UserIndex, INT1 *pi1PrivilegeLevel)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    INT1               *pi1UsrName;
    INT1               *pi1PrivilegeId;
    INT1               *pi1Mode;
    INT1               *pi1Passwd;
    INT1               *pi1BlockUserRelTime = NULL;
    INT2                i2Retval = CLI_FAILURE;
    UINT4               u4Len = 0;

    if ((i2UserIndex < 0) || (i2UserIndex >= gCliSessions.i2NofUsers))
    {
        return i2Retval;
    }

    u4Len =
        ((STRLEN (gCliSessions.pi1CliUsers[i2UserIndex]) <
          sizeof (ai1Buf)) ? STRLEN (gCliSessions.
                                     pi1CliUsers[i2UserIndex]) : sizeof (ai1Buf)
         - 1);

    STRNCPY (ai1Buf, gCliSessions.pi1CliUsers[i2UserIndex], u4Len);
    ai1Buf[u4Len] = '\0';
    CliSplitUserTokens (ai1Buf, &pi1UsrName, &pi1PrivilegeId, &pi1Mode,
                        &pi1Passwd, &pi1BlockUserRelTime);
    *pi1PrivilegeLevel = (INT1) CLI_ATOI (pi1PrivilegeId);

    return CLI_SUCCESS;
}

INT2
CliChangePasswd (CONST CHR1 * pi1UserName, CHR1 * pi1NewPasswd)
{
    INT1               *pi1Buf;
    INT1               *pi1UsrName;
    INT1               *pi1UsrId;
    INT1               *pi1Mode;
    INT1               *pi1Passwd;
    INT1               *pi1BlockUserRelTime = NULL;
    INT2                i2UserIndex;

    if ((CliGetUserIndex (pi1UserName, &i2UserIndex)) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }

    STRCPY (pi1Buf, gCliSessions.pi1CliUsers[i2UserIndex]);
    CliSplitUserTokens (pi1Buf, &pi1UsrName, &pi1UsrId, &pi1Mode, &pi1Passwd,
                        &pi1BlockUserRelTime);
    mmi_encript_passwd (pi1NewPasswd);
    if (pi1BlockUserRelTime == NULL)
    {
        SPRINTF ((CHR1 *) gCliSessions.pi1CliUsers[i2UserIndex], "%s:%s:%s:%s",
                 (CHR1 *) pi1UsrName, (CHR1 *) pi1UsrId, (CHR1 *) pi1Mode,
                 (CHR1 *) pi1NewPasswd);
    }
    else
    {
        SPRINTF ((CHR1 *) gCliSessions.pi1CliUsers[i2UserIndex],
                 "%s:%s:%s:%s:%s", (CHR1 *) pi1UsrName, (CHR1 *) pi1UsrId,
                 (CHR1 *) pi1Mode, (CHR1 *) pi1NewPasswd,
                 (CHR1 *) pi1BlockUserRelTime);
    }
    CliWriteUsers ();
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);

    return (CLI_SUCCESS);
}

INT2
CliGetUserMode (CONST CHR1 * pi1UserName, INT1 *pi1UserMode)
{
    if (FpamUtlGetUserMode (pi1UserName, pi1UserMode) == FPAM_SUCCESS)
        return (CLI_SUCCESS);
    return (CLI_FAILURE);
}

INT2
CliUserMod (INT1 *pi1NewUid, INT1 *pi1NewPasswd, INT1 *pi1NewDefMode,
            INT1 *pi1NewGrps, CHR1 * pi1NewUsrName)
{
    INT2                i2UserIndex;
    INT1               *pi1UserName;
    INT1               *pi1UserId;
    INT1               *pi1UserPasswd;
    INT1               *pi1UserMode;
    INT1               *pi1Buf;
    INT1               *pi1GrpName;
    INT1               *pi1GrpId;
    INT1               *pi1Users;
    INT1               *pi1BlockUserRelTime = NULL;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT2                i2Index;

    if ((CliGetUserIndex (pi1NewUsrName, &i2UserIndex)) == CLI_FAILURE)
    {
        mmi_printf ("\r\n%% No Such User\r\n");
        return (CLI_FAILURE);
    }

    if (pi1NewUid)
    {
        if ((STRLEN (pi1NewUid)) > CLI_MAX_USERID_LEN)
        {
            mmi_printf ("\r\n%% User Id exceeded size limit <1-999>\r\n");
            return (CLI_FAILURE);
        }
    }

    if (pi1NewDefMode)
    {
        if ((STRLEN (pi1NewDefMode)) > CLI_MAX_USERMODE_LEN)
        {
            mmi_printf ("\r\n%% User Mode exceeded size limit\r\n");
            return (CLI_FAILURE);
        }
    }

    if (pi1NewPasswd)
    {
        if ((STRLEN (pi1NewPasswd)) > CLI_MAX_USERPASSWD_LEN)
        {
            mmi_printf ("\r\n%% User Password exceeded size limit\r\n");
            return (CLI_FAILURE);
        }
    }

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }

    STRCPY (pi1Buf, gCliSessions.pi1CliUsers[i2UserIndex]);
    CliSplitUserTokens (pi1Buf, &pi1UserName, &pi1UserId, &pi1UserMode,
                        &pi1UserPasswd, &pi1BlockUserRelTime);

    if (pi1NewUid)
        pi1UserId = pi1NewUid;

    if (pi1NewPasswd)
    {
        mmi_encript_passwd ((CHR1 *) pi1NewPasswd);
        pi1UserPasswd = pi1NewPasswd;
    }

    if (pi1NewDefMode)
        pi1UserMode = pi1NewDefMode;

    if (pi1BlockUserRelTime == NULL)
    {
        SPRINTF ((CHR1 *) gCliSessions.pi1CliUsers[i2UserIndex], "%s:%s:%s:%s",
                 (CHR1 *) pi1UserName, (CHR1 *) pi1UserId,
                 (CHR1 *) pi1UserMode, (CHR1 *) pi1UserPasswd);
    }
    else
    {
        SPRINTF ((CHR1 *) gCliSessions.pi1CliUsers[i2UserIndex],
                 "%s:%s:%s:%s:%s", (CHR1 *) pi1UserName, (CHR1 *) pi1UserId,
                 (CHR1 *) pi1UserMode, (CHR1 *) pi1UserPasswd,
                 (CHR1 *) pi1BlockUserRelTime);
    }
    CliWriteUsers ();

    if (pi1NewGrps)
    {
        for (i2Index = 0; i2Index < gCliSessions.i2NofGroups; i2Index++)
        {
            STRCPY (pi1Buf, gCliSessions.pi1CliGroups[i2Index]);
            CliSplitGroupTokens (pi1Buf, &pi1GrpName, &pi1GrpId, &pi1Users);
            if ((CliIsUserInGroup ((INT1 *) pi1NewUsrName, pi1GrpName))
                == CLI_SUCCESS)
            {
                CliDeleteUserFromGroup (pi1NewUsrName, pi1GrpName);
            }
        }
        i1RetStatus = CliAddUserToGroups (pi1NewUsrName, pi1NewGrps);
        if (i1RetStatus == CLI_FAILURE)
        {
            mmi_printf ("\r\n%%Error in groups `%s'\r\n", pi1NewGrps);
        }
    }
    CliWriteGroups ();
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    return (CLI_SUCCESS);
}

INT2
CliDisplayId (VOID)
{
    tCliContext        *pCliContext;
    INT1               *pi1UserName;
    CHR1               *pi1Grps = NULL;
    INT1               *pi1Buf = NULL;
    INT1               *pi1UserId = NULL;
    INT1               *pi1UserMode = NULL;
    INT1               *pi1UserPasswd = NULL;
    INT1               *pi1BlockUserRelTime = NULL;
    INT2                i2UserIndex;
    UINT4               u4BufLen = 0;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pi1UserName = pCliContext->ai1UserName;
    if ((CliGetUserIndex ((CHR1 *) pi1UserName, &i2UserIndex)) == CLI_FAILURE)
    {
        mmi_printf ("\r\n%%No Such User\r\n");
        return (CLI_FAILURE);
    }

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);
    if (pi1Buf == NULL)
    {
        return CLI_FAILURE;
    }
    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Grps, CHR1);

    if (pi1Grps == NULL)
    {
        CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
        return CLI_FAILURE;
    }

    u4BufLen = CLI_MAX_GROUPS_LINE_LEN + 1;

    pi1Grps[0] = '\0';
    STRCPY (pi1Buf, gCliSessions.pi1CliUsers[i2UserIndex]);
    CliSplitUserTokens (pi1Buf, (INT1 **) &pi1UserName, &pi1UserId,
                        &pi1UserMode, &pi1UserPasswd, &pi1BlockUserRelTime);

    /* Get all the group names for the user */
    CliGetGroups ((CHR1 *) pi1UserName, pi1Grps, u4BufLen);

    if (*pi1Grps)
    {
        mmi_printf ("UID-%d, Groups-%s\r\n", CLI_ATOI (pi1UserId), pi1Grps);
    }
    else
    {
        mmi_printf ("UID-%d\r\n", CLI_ATOI (pi1UserId));
    }

    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Grps);

    return (CLI_SUCCESS);
}

INT2
CliGetGroups (CHR1 * pi1UserName, CHR1 * pi1Grps, UINT4 u4Len)
{
    INT1               *pi1GrpBuf = NULL;
    INT1               *pi1GrpName = NULL;
    INT1               *pi1GrpId = NULL;
    INT1               *pi1Users = NULL;
    INT2                i2Index = 0;

    /* Allocate Memory for the Group Buffer */
    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1GrpBuf, INT1);

    if (pi1GrpBuf == NULL)
    {
        return CLI_FAILURE;
    }

    /* Walk through all the groups in the Group file */
    for (i2Index = 0; i2Index < gCliSessions.i2NofGroups; i2Index++)
    {
        /* Initialize the GroupBuffers */
        MEMSET (pi1GrpBuf, 0, (CLI_MAX_USERS_LINE_LEN + 1));

        STRCPY (pi1GrpBuf, gCliSessions.pi1CliGroups[i2Index]);

        /* Split the Groups token in Groups file */
        CliSplitGroupTokens (pi1GrpBuf, &pi1GrpName, &pi1GrpId, &pi1Users);

        /* Check whether the given user is in group */
        if ((CliIsUserInGroup ((INT1 *) pi1UserName, pi1GrpName))
            == CLI_SUCCESS)
        {
            STRCAT (pi1Grps, " ");

            if (CLI_STRLEN (pi1Grps) >= u4Len)
            {
                break;
            }

            if ((CLI_STRLEN (pi1Grps) + CLI_STRLEN (pi1GrpName)) >= u4Len)
            {
                break;
            }
            else
            {
                STRCAT (pi1Grps, pi1GrpName);
            }
        }

        MEMSET (pi1GrpName, 0, (CLI_MAX_USERS_LINE_LEN + 1));
    }

    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1GrpBuf);
    return CLI_SUCCESS;
}

  /**************************************************************************
 * Function    : CliGetSessionAuth
 * Description : This function is used to count
 *              failed authentication.
 * Input       : u4ClientAddr
 * Output      : none
 * Returns     : i4Count
 ******************************************************************************/

INT4
CliGetSessionAuth (UINT4 u4ClientAddr)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (gSshSessionAuth[i4Count].u4ClientAddr == u4ClientAddr)
        {
            return i4Count;
        }
    }

    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (gSshSessionAuth[i4Count].u1UseFlag == 0)
        {
            gSshSessionAuth[i4Count].u1UseFlag = 1;
            gSshSessionAuth[i4Count].u4ClientAddr = u4ClientAddr;
            return i4Count;
        }
    }
    return -1;
}

  /**************************************************************************
 * Function    : CliGetSessionAuthIpv6
 * Description : This function is used to count
 *              failed authentication for IPv6 address.
 * Input       : pu4ClientIpAddr
 * Output      : none
 * Returns     : i4Count
 ******************************************************************************/

INT4
CliGetSessionAuthIpv6 (UINT4 *pu4ClientIpAddr)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (STRNCMP (gSshSessionAuth[i4Count].au4ClientIpAddr, pu4ClientIpAddr,
                     sizeof (gSshSessionAuth[i4Count].au4ClientIpAddr)) == 0)
        {
            return i4Count;
        }
    }

    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (gSshSessionAuth[i4Count].u1UseFlag == 0)
        {
            gSshSessionAuth[i4Count].u1UseFlag = 1;
            MEMSET (&gSshSessionAuth[i4Count].au4ClientIpAddr,
                    0, sizeof (gSshSessionAuth[i4Count].au4ClientIpAddr));
            MEMCPY (&gSshSessionAuth[i4Count].au4ClientIpAddr,
                    pu4ClientIpAddr,
                    sizeof (gSshSessionAuth[i4Count].au4ClientIpAddr));
            return i4Count;
        }
    }
    return -1;
}

  /**************************************************************************
 * Function    : CliSshResetSessionAuth
 * Description : This function is used to reset the failed
 *              authentication to zero.
 * Input       : u4ClientAddr
 * Output      : none
 * Returns     : none
 ******************************************************************************/

VOID
CliSshResetSessionAuth (UINT4 u4ClientAddr)
{
    INT4                i4Count = 0;
    CliContextLock ();
    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (gSshSessionAuth[i4Count].u4ClientAddr == u4ClientAddr)
        {
            IpMgrBlockTableDelete (gSshSessionAuth[i4Count].u4ClientAddr,
                                   SSH_DEFAULT_PORT);
            gSshSessionAuth[i4Count].u1Blocked = 0;
            gSshSessionAuth[i4Count].u1UseFlag = 0;
            gSshSessionAuth[i4Count].u4FailCount = 0;
            gSshSessionAuth[i4Count].u4ClientAddr = 0;
            CliContextUnlock ();
            return;
        }
    }
    CliContextUnlock ();
}

 /**************************************************************************
 * Function    : CliSshTimerExpiryCallBkFun
 * Description : CliSshTimerExpiryCallBkFun call back function to will be
 *               returned when the timer is expired.
 * Input       : none
 * Output      : none
 * Returns     : none
 ******************************************************************************/

VOID
CliSshTimerExpiryCallBkFun (VOID)
{
    INT4                i4Count = 0;
    tTmrAppTimer       *pTimerNode;
    CliContextLock ();
    while ((pTimerNode = TmrGetNextExpiredTimer (gSshTimerLst)) != NULL)
    {
        for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
        {
            if (gSshSessionAuth[i4Count].timerNode.TimerListId ==
                pTimerNode->TimerListId)
            {
                IpMgrBlockTableDelete (gSshSessionAuth[i4Count].u4ClientAddr,
                                       SSH_DEFAULT_PORT);
                gSshSessionAuth[i4Count].u1Blocked = 0;
                gSshSessionAuth[i4Count].u1UseFlag = 0;
                gSshSessionAuth[i4Count].u4FailCount = 0;
                gSshSessionAuth[i4Count].u4ClientAddr = 0;
            }
        }
    }
    CliContextUnlock ();
    return;
}

#ifdef PAM_AUTH_WANTED
/*****************************************************************************/
/* Function Name      : interrupt                                            */
/*                                                                           */
/* Description        : Checks if there is any interruption                  */
/*                                                                           */
/* Input(s)           : i4Input                                              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

static VOID
interrupt (INT4 i4Input)
{
    UNUSED_PARAM (i4Input);
    i4SCount = 1;
}

/*****************************************************************************/
/* Function Name      : LdapGetInput                                         */
/*                                                                           */
/* Description        : Gets the user input                                  */
/*                                                                           */
/* Input(s)           : i4EchoInput                                          */
/*                                                                           */
/* Return Value(s)    : input from user will be returned                     */
/*****************************************************************************/
static CHR1        *
LdapGetInput (INT4 i4EchoInput)
{
    struct termio       tty;
    UINT2               u2TtyFlag = 0;
    CHR1                i1Str[PAM_MAX_RESP_SIZE];
    INT4                i4InputChar = 0;
    INT4                i4Index = 0;
    VOID                (*sig) (INT4);

    i4SCount = 0;
    sig = signal (SIGINT, interrupt);
    if (i4EchoInput)
    {
        (VOID) ioctl (fileno (stdin), TCGETA, &tty);
        u2TtyFlag = tty.c_lflag;
        tty.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL);
        (VOID) ioctl (fileno (stdin), TCSETAF, &tty);
    }

    flockfile (stdin);
    while (i4SCount == 0 &&
           (i4InputChar = getchar_unlocked ()) != '\n' &&
           i4InputChar != '\r' && i4InputChar != EOF)
    {
        if (i4Index < PAM_MAX_RESP_SIZE)
        {
            i1Str[i4Index++] = (INT1) i4InputChar;
        }
    }
    funlockfile (stdin);
    i1Str[i4Index] = '\0';
    if (i4EchoInput)
    {
        tty.c_lflag = u2TtyFlag;
        (VOID) ioctl (fileno (stdin), TCSETAW, &tty);
        (VOID) fputc ('\n', stdout);
    }

    (VOID) signal (SIGINT, sig);
    if (i4SCount == 1)
        (VOID) kill (getpid (), SIGINT);

    return (strdup (i1Str));

}

/*****************************************************************************/
/* Function Name      : FreeResponses                                        */
/*                                                                           */
/* Description        : Removes the uncleared responses                      */
/*                                                                           */
/* Input(s)           : i4InputMsg, pInputRes                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
static VOID
FreeResponses (INT4 i4InputMsg, struct pam_response *pInputRes)
{
    INT4                i4Index = 0;
    struct pam_response *pRes;

    pRes = pInputRes;

    if (pInputRes == NULL)
        return;

    for (i4Index = 0; i4Index < i4InputMsg; i4Index++, pRes++)
    {
        if (pRes->resp)
        {
            bzero (pRes->resp, strlen (pRes->resp));
            free (pRes->resp);
            pRes->resp = NULL;
        }
    }
    free (pInputRes);
}

/*****************************************************************************/
/* Function Name      : LdapPamTtyConv                                       */
/*                                                                           */
/* Description        : Performs PAM Processing and returns the output       */
/*                                                                           */
/* Input(s)           : i4InputMsg,pInputMsg,pInputRes,pInputData            */
/*                                                                           */
/* Return Value(s)    : Returns the error message                            */
/*****************************************************************************/

INT4
LdapPamTtyConv (INT4 i4InputMsg, struct pam_message **pInputMsg,
                struct pam_response **pInputRes, VOID *pInputData)
{
    struct pam_message *pMsg;
    struct pam_response *pRes;
    INT4                i4Index = 0;
    INT4                i4EchoFlag = 0;

    UNUSED_PARAM (pInputData);
    pMsg = *pInputMsg;

    if (i4InputMsg <= 0 || i4InputMsg >= PAM_MAX_NUM_MSG)
    {
        (VOID) fprintf (stderr, "bad number of messages %d "
                        "<= 0 || >= %d\n", i4InputMsg, PAM_MAX_NUM_MSG);
        *pInputRes = NULL;
        return (PAM_CONV_ERR);
    }
    if ((*pInputRes = pRes = calloc (i4InputMsg,
                                     sizeof (struct pam_response))) == NULL)
        return (PAM_BUF_ERR);

    errno = 0;

    for (i4Index = 0; i4Index < i4InputMsg; i4Index++)
    {

        if (pMsg->msg == NULL)
        {
            (VOID) fprintf (stderr, "message[%d]: %d/NULL\n",
                            i4Index, pMsg->msg_style);
            goto err;
        }

        if (pMsg->msg[strlen (pMsg->msg)] == '\n')
            pMsg->msg[strlen (pMsg->msg)] == '\0';

        pRes->resp = NULL;
        pRes->resp_retcode = 0;
        i4EchoFlag = 0;
        switch (pMsg->msg_style)
        {

            case PAM_PROMPT_ECHO_OFF:
                i4EchoFlag = 1;
             /*FALLTHROUGH*/ case PAM_PROMPT_ECHO_ON:
                (VOID) fputs (pMsg->msg, stdout);

                pRes->resp = LdapGetInput (i4EchoFlag);
                break;

            case PAM_ERROR_MSG:
                (VOID) fputs (pMsg->msg, stderr);
                (VOID) fputc ('\n', stderr);
                break;

            case PAM_TEXT_INFO:
                (VOID) fputs (pMsg->msg, stdout);
                (VOID) fputc ('\n', stdout);
                break;

            default:
                (VOID) fprintf (stderr, "message[%d]: unknown type "
                                "%d/val=\"%s\"\n",
                                i4Index, pMsg->msg_style, pMsg->msg);
                goto err;
        }
        if (errno == EINTR)
            goto err;

        pMsg++;
        pRes++;
    }
    return (PAM_SUCCESS);

  err:
    FreeResponses (i4Index, pRes);
    *pInputRes = NULL;
    return (PAM_CONV_ERR);
}

#endif
