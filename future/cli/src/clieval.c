/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clieval.c,v 1.27 2015/09/18 10:41:35 siva Exp $
 *
 * Description: This is to validate the input tokens 
 *              according to the address              
 *******************************************************************/

#include "clicmds.h"
#define MSG_OUT_OF_RANGE "Value out of Range"

extern INT1         ai1BSpaceStr[];
extern tMemPoolId   gCliTokenPoolId;

INT4
mmi_atoi (str)
     UINT1              *str;
{
    INT4                i;
    for (i = 0; i < (INT4) CLI_STRLEN ((INT1 *) str); i++)
        if (!isdigit (str[i]))
            return (-1);
    return (mmi_low_atoi ((INT1 *) str));
}

INT4
mmi_atos (INT1 *pi1Str, UINT4 *pu4Val)
{
    UINT2               u2PrevVal = 0;    /* short */
    UINT2               u2CurrVal = 0;    /* short */
    INT2                i2Base = 10;
    INT1                i1Char;

    if (*pi1Str == '0')
    {
        pi1Str++;
        if (*pi1Str == 'x' || *pi1Str == 'X')
        {
            i2Base = 16;
            pi1Str++;
        }
    }

    /* Converts ascii to short */

    for (; *pi1Str; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
            i1Char = (INT1) (*pi1Str - '0');
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
            i1Char = (INT1) (*pi1Str - 'a' + 10);
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
            i1Char = (INT1) (*pi1Str - 'A' + 10);
        else
            return (CLI_FAILURE);

        u2CurrVal = (UINT2) ((u2CurrVal * i2Base) + i1Char);

        if (((u2CurrVal - i1Char) / i2Base) != u2PrevVal)
        {
            return (CLI_FAILURE);
        }
        if ((i1Char != 0) && (u2CurrVal <= u2PrevVal))
        {
            return (CLI_FAILURE);
        }

        u2PrevVal = u2CurrVal;
    }

    if (pu4Val)
        *pu4Val = u2CurrVal;

    return (CLI_SUCCESS);
}

INT4
mmi_atol (INT1 *pi1Str, UINT4 *pu4Val)
{
    UINT4               u4PrevVal = 0;    /* long */
    UINT4               u4CurrVal = 0;    /* long */
    INT2                i2Base = 10;
    INT1                i1Char;

    if (*pi1Str == '0')
    {
        pi1Str++;
        if (*pi1Str == 'x' || *pi1Str == 'X')
        {
            i2Base = 16;
            pi1Str++;
        }
    }

    /* Converts ascii to long */

    for (; *pi1Str; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
            i1Char = (INT1) (*pi1Str - '0');
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
            i1Char = (INT1) (*pi1Str - 'a' + 10);
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
            i1Char = (INT1) (*pi1Str - 'A' + 10);
        else
            return (CLI_FAILURE);

        u4CurrVal = (u4CurrVal * i2Base) + i1Char;

        if (((u4CurrVal - i1Char) / i2Base) != u4PrevVal)
        {
            return (CLI_FAILURE);
        }
        if ((i1Char != 0) && (u4CurrVal <= u4PrevVal))
        {
            return (CLI_FAILURE);
        }

        u4PrevVal = u4CurrVal;
    }

    if (pu4Val)
        *pu4Val = (UINT4) u4CurrVal;

    return (CLI_SUCCESS);
}

INT4
mmi_satoi (INT1 *pi1Str, INT4 *pi4Val)
{
    INT4                i4PrevVal = 0;    /* long */
    INT4                i4CurrVal = 0;    /* long */
    INT2                i2Base = 10;
    INT1                i1Char;
    INT1                i1Flag = 0;

    if (*pi1Str == '0')
    {
        pi1Str++;
        if (*pi1Str == 'x' || *pi1Str == 'X')
        {
            i2Base = 16;
            pi1Str++;
        }
    }

    if (*pi1Str == '-')
    {
        i1Flag = 1;
        pi1Str++;
    }

    /* Converts ascii to long */

    for (; *pi1Str; pi1Str++)
    {
        if (*pi1Str >= '0' && *pi1Str <= '9')
            i1Char = (INT1) (*pi1Str - '0');
        else if (*pi1Str >= 'a' && *pi1Str <= 'f')
            i1Char = (INT1) (*pi1Str - 'a' + 10);
        else if (*pi1Str >= 'A' && *pi1Str <= 'F')
            i1Char = (INT1) (*pi1Str - 'A' + 10);
        else
            return (CLI_FAILURE);

        i4CurrVal = (i4CurrVal * i2Base) + i1Char;

        if (((i4CurrVal - i1Char) / i2Base) != i4PrevVal)
        {
            return (CLI_FAILURE);
        }
        if ((i1Char != 0) && (i4CurrVal <= i4PrevVal))
        {
            return (CLI_FAILURE);
        }

        i4PrevVal = i4CurrVal;
    }

    if (pi4Val)
    {
        if (i1Flag == 1)
        {
            *pi4Val = (i4CurrVal * -1);
        }
        else
        {
            *pi4Val = i4CurrVal;
        }
    }
    return (CLI_SUCCESS);
}

INT4
mmi_convert (base, str)
     INT4                base;
     INT1               *str;
{
    INT4                i;
    INT4                i4pow = 1;
    INT4                value = 0;

    for (i = CLI_STRLEN (str) - 1; i >= 0; i--)
    {
        if (str[i] >= '0' && str[i] <= '9')
            value = value + (str[i] - '0') * i4pow;
        else if (str[i] >= 'a' && str[i] <= 'f')
            value = value + (str[i] - 'a' + 10) * i4pow;
        else if (str[i] >= 'A' && str[i] <= 'F')
            value = value + (str[i] - 'A' + 10) * i4pow;
        else
            return value;
        i4pow = i4pow * base;
    }
    return value;
}

INT4
mmi_low_atoi (s)
     INT1                s[];
{
    INT4                i4RetVal = 0;
    INT4                i4RetValue = 0;
    UNUSED_PARAM (i4RetValue);
    i4RetValue = mmi_atol (s, (UINT4 *) &i4RetVal);
    return ((UINT4) i4RetVal);
}

INT4
mmi_power1 (x, n)
     INT4                x, n;
{
    INT4                total = 1, i;
    if (x == 0)
        return (0);
    if (n == 0)
        return (1);

    for (i = 1; i <= n; i++)
        total = total * x;
    return (total);
}

/*
 * This routine will compare str1, str2 without considering
 * the lower, upper case
 * if equal will return 0
 * else nonzero
 */

INT1
mmi_strcmp_without_case (str1, str2)
     INT1               *str1, *str2;
{
    INT4                i;
    if (CLI_STRLEN (str1) != CLI_STRLEN (str2))
        return -1;
    for (i = 0; i < (INT4) CLI_STRLEN (str1); i++)
    {
        if (tolower (str1[i]) != tolower (str2[i]))
            return -1;
    }
    return 0;
}

INT1
mmi_strcmp (str1, str2)
     INT1               *str1, *str2;
{
    INT4                i;
    if (CLI_STRLEN (str1) != CLI_STRLEN (str2))
        return -1;
    for (i = 0; i < (INT4) CLI_STRLEN (str1); i++)
    {
        if (str1[i] != str2[i])
            return -1;
    }
    return 0;
}

INT1
mmi_strncmp (str1, str2, n)
     INT1               *str1, *str2;
     INT4                n;
{
    INT4                i;

    if (((INT4) CLI_STRLEN (str1) < n) || ((INT4) CLI_STRLEN (str2) < n))
    {
        return -1;
    }

    for (i = 0; i < n; i++)
    {
        if (str1[i] != str2[i])
        {
            return -1;
        }
    }
    return 0;
}

/* This routine will find the occurence tar(target) string
   in sou(source) string and will return the address
   if not found return NULL */
CONST CHR1         *
mmi_strstr (sou, tar)
     CONST CHR1         *sou, *tar;
{
    INT4                i, j, k;
    INT4                source_len, tar_len;

    source_len = CLI_STRLEN (sou);
    tar_len = CLI_STRLEN (tar);

    if (source_len < tar_len)
        return NULL;
    for (i = 0; i <= source_len - tar_len; i++)
    {
        k = i;
        for (j = 0; j < tar_len; j++)
        {
            if (sou[k++] != tar[j])
                break;
        }
        if (j == tar_len)
            return &sou[i];
    }
    return NULL;
}

INT1
mmi_strncmpi (str1, str2, n)
     CONST INT1         *str1, *str2;
     UINT4               n;
{
    UINT4               i;

    if (!str1)
        return -1;
    if (!str2)
        return 1;

    if ((CLI_STRLEN (str1) < n) || (CLI_STRLEN (str2) < n))
    {
        return 1;
    }

    for (i = 0; i < n; i++)
    {
        if (tolower (str1[i]) != tolower (str2[i]))
        {
            return 1;
        }
    }
    return 0;
}

INT1
mmi_strcmpi (str1, str2)
     CONST INT1         *str1, *str2;
{
    UINT4               i;

    if (!str1)
        return -1;
    if (!str2)
        return 1;

    if (CLI_STRLEN (str1) != CLI_STRLEN (str2))
    {
        return 1;
    }

    for (i = 0; i < CLI_STRLEN (str1); i++)
    {
        if (tolower (str1[i]) != tolower (str2[i]))
        {
            return 1;
        }
    }
    return 0;
}

/*********************************************************************
   General routine that waits for keyhit and returns whether it
   was a break or continue.
   Used for Pagewise display.
   Newline is treated as continue ... returns 0.
   Anything else followed by newline is treated as break... returns 1.
*********************************************************************/
INT4
mmi_more (VOID)
{
    return TRUE;
}

INT4
mmi_new_strcmp (str1, str2)
     CONST INT1         *str1, *str2;
{
    INT4                i;

    if (str1 == NULL)
        return -1;

    if (str2 == NULL)
        return 1;

    if (CLI_STRLEN (str1) < CLI_STRLEN (str2))
        return -1;

    if (CLI_STRLEN (str1) > CLI_STRLEN (str2))
        return 1;

    for (i = 0;
         i < CLI_MIN ((INT4) CLI_STRLEN (str1), (INT4) CLI_STRLEN (str2)); i++)
    {
    if (tolower(str1[i]) != tolower(str2[i]))
            return (str1[i] - str2[i]);
    }
    return 0;
}

INT4
mmi_new_strcpy (INT1 *str1, CONST INT1 *str2)
{
    INT4                i;

    if ((str1 == NULL) || (str2 == NULL))
        return (0);

    for (i = 0; str2[i] != '\0'; i++)
    {
        str1[i] = str2[i];
    }
    str1[i] = str2[i];
    return (i - 1);
}

INT4
mmi_substri (CONST CHR1 * pi1Syntax, INT1 *pi1Cmd)
{
    INT4                i4CmdIndex;
    INT4                i4SyntaxIndex;
    INT4                i4CmdLen;
    INT4                i4SyntaxLen;

    i4CmdLen = CLI_STRLEN (pi1Cmd);
    i4SyntaxLen = CLI_STRLEN (pi1Syntax);

    if (i4CmdLen > i4SyntaxLen)
    {
        return 1;
    }

    for (i4CmdIndex = 0, i4SyntaxIndex = 0;
         pi1Cmd[i4CmdIndex]; ++i4CmdIndex, ++i4SyntaxIndex)
    {
        if (pi1Cmd[i4CmdIndex] == ' ')
        {
            while (pi1Syntax[i4SyntaxIndex++] != ' ')
            {
                ;                /* loop till the next token */
            }
            i4CmdIndex++;
        }

        if (tolower (pi1Cmd[i4CmdIndex]) != tolower (pi1Syntax[i4SyntaxIndex]))
        {
            return 1;            /* string mismatch */
        }

    }
    return 0;
}

/* To check the range values of the given integer type token */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_integer_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    UINT4               u4Temp = 0;
    INT4                i4Temp = 0;
    INT1               *pi1RangeStr = NULL;

    i4Temp = mmi_atol (pi1Token, &u4Temp);

    if ((i4Temp == CLI_FAILURE) ||
        (node->pRange->pRangeInt.u4MinRange > u4Temp) ||
        (node->pRange->pRangeInt.u4MaxRange < u4Temp))
    {
        /* Allocates memory for the Range String and is freed in cliparse.c */
        if ((pi1RangeStr =
             CLI_BUDDY_ALLOC (CLI_STRLEN (node->pRange->pRangeInt.pi1RangeStr) +
                              1, INT1)) == NULL)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
            return NULL;
        }

        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_RANGE);
        CLI_STRCPY (pi1RangeStr, node->pRange->pRangeInt.pi1RangeStr);

        return pi1RangeStr;
    }

    return NULL;
}

/* To check the range values of the given signed integer type token */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_sinteger_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    INT4                i4Temp = 0;
    INT4                i4Temp1 = 0;
    INT1               *pi1RangeStr = NULL;

    i4Temp = mmi_satoi (pi1Token, &i4Temp1);

    if ((i4Temp == CLI_FAILURE) ||
        (node->pRange->pRangeSInt.i4MinRange > i4Temp1) ||
        (node->pRange->pRangeSInt.i4MaxRange < i4Temp1))
    {
        /* Allocates memory for the Range String and is freed in cliparse.c */
        if ((pi1RangeStr =
             CLI_BUDDY_ALLOC (CLI_STRLEN (node->pRange->pRangeSInt.pi1RangeStr)
                              + 1, INT1)) == NULL)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
            return NULL;
        }

        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_RANGE);
        CLI_STRCPY (pi1RangeStr, node->pRange->pRangeSInt.pi1RangeStr);

        return pi1RangeStr;
    }

    return NULL;
}

/* To check the Validity of the given Ip Address */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_IpAddress_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    INT1                i1Ch;

    UNUSED_PARAM (node);

    /* checks for correct format of Ip Address with dotted notation */

    while ((i1Ch = *pi1Token++))
    {
        if ((!isxdigit (i1Ch)) && (i1Ch != 'x') &&
            (i1Ch != 'X') && (i1Ch != '.'))
        {
            break;
        }
    }

    while (isspace (i1Ch))
    {
        i1Ch = *pi1Token++;
    }

    if (i1Ch)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_UNKNOWN_HOST);
    }

    return NULL;
}

/* To check the Validity of the given MAC Address */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_MacAddress_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    INT2                i2dotCount = 0;
    INT2                i2charCount = 0;
    INT2                i2MaxcharCount = 0;
    INT1               *pi1RangeStr = NULL;

    UNUSED_PARAM (node);

    /* checks for correct format of Mac Address with dotted notation */

    while (*pi1Token)
    {
        if (*pi1Token == ':')
        {
            i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
                (i2MaxcharCount) : (i2charCount);
            i2charCount = -1;
            i2dotCount++;
        }

        i2charCount++;
        pi1Token++;
    }

    i2MaxcharCount = (i2MaxcharCount > i2charCount) ?
        (i2MaxcharCount) : (i2charCount);

    /* Checks whether the given format is in correct format
     * with 6 octests or 3 octests
     */

    if ((i2MaxcharCount > 4) || (i2dotCount == 5 && i2MaxcharCount > 2) ||
        ((i2dotCount != 2) && (i2dotCount != 5)))
    {
        if ((pi1RangeStr =
             CLI_BUDDY_ALLOC (CLI_STRLEN
                              (mmi_gen_messages
                               [MMI_GEN_ERR_INVALID_MAC_ADDR_FORMAT]) + 1,
                              INT1)) == NULL)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
            return NULL;
        }

        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_MAC_ADDR_FORMAT);
        CLI_STRCPY (pi1RangeStr,
                    mmi_gen_messages[MMI_GEN_ERR_INVALID_MAC_ADDR_FORMAT]);
    }

    return pi1RangeStr;
}

INT4
mmi_atof (INT1 *pi1Str, DBL8 * pd8Val)
{
    DBL8                d8Power = 1.0;    /* float */
    DBL8                d8CurrVal = 0.0;    /* float */
    DBL8                d8PrevVal = 0.0;    /* float */

    /* Converts ascii to float */

    for (d8CurrVal = 0.0; isdigit (*pi1Str); pi1Str++)
    {
        d8CurrVal = (d8CurrVal * 10.0) + ((*pi1Str) - '0');

        /* Checks d8CurrVal bits Wraps or not */

        if (((d8CurrVal - ((*pi1Str) - '0')) / 10) != d8PrevVal)
        {
            return (CLI_FAILURE);
        }
        d8PrevVal = d8CurrVal;
    }

    if (*pi1Str == '.')
        pi1Str++;

    for (d8Power = 1.0; isdigit (*pi1Str); pi1Str++)
    {
        d8CurrVal = (d8CurrVal * 10.0) + ((*pi1Str) - '0');
        d8Power = d8Power * 10.0;

        if (d8Power >= 1000000)    /* restricts precision value to 6 digits */
            break;
    }

    if (pd8Val)
        *pd8Val = d8CurrVal / d8Power;

    return (CLI_SUCCESS);
}

/* To check the range values of the given float type token */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_float_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    DBL8                d8Temp = 0;
    INT4                i4Temp = 0;
    INT1               *pi1RangeStr = NULL;

    i4Temp = mmi_atof (pi1Token, &d8Temp);

    if ((i4Temp == CLI_FAILURE)
        || (node->pRange->pRangeFloat.d8MinRange > d8Temp)
        || (node->pRange->pRangeFloat.d8MaxRange < d8Temp))
    {
        /* Allocates memory for the Range String and is freed in cliparse.c */
        if ((pi1RangeStr =
             CLI_BUDDY_ALLOC (CLI_STRLEN (node->pRange->pRangeFloat.
                                          pi1RangeStr) + 1, INT1)) == NULL)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
            return NULL;
        }

        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_RANGE);
        CLI_STRCPY (pi1RangeStr, node->pRange->pRangeFloat.pi1RangeStr);

        return pi1RangeStr;
    }

    return NULL;
}

/*********************************************************************
   General routine that waits for keyhit and returns whether it
   was a break or continue.
   Used for Pagewise display.
   'q' is taken as  a break while anything else is taken as continue.
*********************************************************************/
INT1
CliMorePrompt (VOID)
{
    tCliContext        *pCliContext;
    UINT1              *pu1TempStr = NULL;
    INT1                i1Ch = 0;
    INT1                i1log[256];
    /* erase the more message by over-writing with space chars */

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return FALSE;
    };

    pu1TempStr = (UINT1 *) i1log;
    /* Displays the MORE messge and waits for user input and
     * then restores default background
     */
    SNPRINTF ((CHR1 *) pu1TempStr, pCliContext->i2MaxCol,
              "%s", CLI_MOVE_CURSOR_TO_END);
    if (pCliContext->fpCliOutput (pCliContext, (CHR1 *) pu1TempStr,
                                  STRLEN (pu1TempStr)) == CLI_FAILURE)
    {
        return FALSE;
    }
    SNPRINTF ((CHR1 *) pu1TempStr, pCliContext->i2MaxCol,
              "\r%s%s%s", ai1BSpaceStr,
              mmi_gen_messages[MMI_GEN_MORE], ai1BSpaceStr);
    if (pCliContext->fpCliOutput (pCliContext, (CHR1 *) pu1TempStr,
                                  STRLEN (pu1TempStr)) == CLI_FAILURE)
    {
        return FALSE;
    }

    i1Ch = pCliContext->fpCliInput (pCliContext);

    SNPRINTF ((CHR1 *) pu1TempStr, pCliContext->i2MaxCol,
              "\33\r                \r%s", ai1BSpaceStr);
    if (pCliContext->fpCliOutput (pCliContext, (CHR1 *) pu1TempStr,
                                  STRLEN (pu1TempStr)) == CLI_FAILURE)
    {
        return FALSE;
    }

    if ((pCliContext->mmi_exit_flag == TRUE) ||
        (pCliContext->i1WinResizeFlag == TRUE) ||
        (pCliContext->i1PipeDisable == OSIX_TRUE))
    {
        pCliContext->i1MoreQuitFlag = TRUE;
        pCliContext->fpCliIoctl (pCliContext, CLI_CLEAR_SCREEN, NULL);
        i1Ch = FALSE;
    }

    return i1Ch;
}

/* To check the String Length of string type tokens */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_String_length (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    UINT4               u4StrLen = 0;
    INT1               *pi1RangeStr = NULL;

    u4StrLen = CLI_STRLEN (pi1Token);

    if (u4StrLen > node->pRange->pStringLen.u4StrLen)
    {
        /* Allocates memmory for the Range String and freed in cliparse.c */

        if ((pi1RangeStr =
             CLI_BUDDY_ALLOC (CLI_STRLEN (node->pRange->pStringLen.pi1RangeStr)
                              + 1, INT1)) == NULL)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
            return NULL;
        }

        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_STRING_LEN);
        CLI_STRCPY (pi1RangeStr, node->pRange->pStringLen.pi1RangeStr);

        return pi1RangeStr;
    }

    return NULL;
}

/* To check the Validity of the given Unicast Ip Address */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_UcastAddress_range (t_MMI_CMD * node, INT1 *pi1Token,
                              INT2 *pi2ErrCode)
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    INT1                i1Ch;

    UNUSED_PARAM (node);

    /* checks for correct format of Unicast Ip Address with dotted notation */

    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        if ((((u4IpAddr & 0xff000000) >> 24) >= 224)&&
            (((u4IpAddr & 0xff000000) >> 24) < 240))
        {
            /* Class D is not allowded */
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_UCAST_ADDR);
            return NULL;
        }
    }
    else
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_UCAST_ADDR);
        return NULL;
    }

    while ((i1Ch = *pi1Token++))
    {
        if ((!isxdigit (i1Ch)) && (i1Ch != 'x') &&
            (i1Ch != 'X') && (i1Ch != '.'))
        {
            break;
        }
    }

    while (isspace (i1Ch))
    {
        i1Ch = *pi1Token++;
    }

    if (i1Ch)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_UNKNOWN_HOST);
    }
    else
    {
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_E (u4IpAddr))))
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_UCAST_ADDR);
            return NULL;
        }
    }

    return NULL;
}

/* To check the Validity of the given Unicast/Loop-back Ip Address */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_LUcastAddress_range (t_MMI_CMD * node, INT1 *pi1Token,
                               INT2 *pi2ErrCode)
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    INT1                i1Ch;

    UNUSED_PARAM (node);

    /* checks for correct format of Unicast/Loop-back Ip Address with dotted notation */

    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        if ((((u4IpAddr & 0xff000000) >> 24) >= 224)&&
            (((u4IpAddr & 0xff000000) >> 24) < 240))
        {
            /* Class D is not allowed*/
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_LUCAST_ADDR);
            return NULL;
        }
    }
    else
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_LUCAST_ADDR);
        return NULL;
    }

    while ((i1Ch = *pi1Token++))
    {
        if ((!isxdigit (i1Ch)) && (i1Ch != 'x') &&
            (i1Ch != 'X') && (i1Ch != '.'))
        {
            break;
        }
    }

    while (isspace (i1Ch))
    {
        i1Ch = *pi1Token++;
    }

    if (i1Ch)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_UNKNOWN_HOST);
    }
    else
    {
        if (!((CLI_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_C (u4IpAddr)) ||
              (CLI_IS_ADDR_CLASS_E (u4IpAddr)) ||
              (CLI_IS_ADDR_LOOPBACK (u4IpAddr))))
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_LUCAST_ADDR);
            return NULL;
        }
    }

    return NULL;
}

/* To check the Validity of the given Multi-cast Ip Address */

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/***************************************************************************/

INT1               *
mmi_check_McastAddress_range (t_MMI_CMD * node, INT1 *pi1Token,
                              INT2 *pi2ErrCode)
{
    tUtlInAddr          UtlInAddr;
    UINT4               u4IpAddr = 0;
    INT1                i1Ch;

    UNUSED_PARAM (node);

    /* checks for correct format of Multi-cast Ip Address
     * with dotted notation
     */

    if (UtlInetAton ((CONST CHR1 *) pi1Token, &UtlInAddr))
    {
        u4IpAddr = OSIX_NTOHL (UtlInAddr.u4Addr);
        u4IpAddr = ((u4IpAddr & 0xff000000) >> 24);
        if ((u4IpAddr < 224) || (u4IpAddr >= 240))
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_MCAST_ADDR);
            return NULL;
        }
    }
    else
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_MCAST_ADDR);
        return NULL;
    }

    while ((i1Ch = *pi1Token++))
    {
        if ((!isxdigit (i1Ch)) && (i1Ch != 'x') &&
            (i1Ch != 'X') && (i1Ch != '.'))
        {
            break;
        }
    }

    while (isspace (i1Ch))
    {
        i1Ch = *pi1Token++;
    }

    if (i1Ch)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_UNKNOWN_HOST);
    }

    return NULL;
}

/* To check the Validity of the given IPv6 Address token*/

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/*  Return Value          : Pointer to the range string or                 */
/*                          NULL if the value is out of range or INVALID.  */
/***************************************************************************/

INT1               *
mmi_check_IP6Address_range (t_MMI_CMD * node, INT1 *pi1Token, INT2 *pi2ErrCode)
{
    /* This function can be modified and used if 
     * range checking support for IPv6 address tokens needed.
     */
    UNUSED_PARAM (node);
    UNUSED_PARAM (pi1Token);
    UNUSED_PARAM (pi2ErrCode);

    return NULL;
}

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1Token   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/*  Return Value          : Pointer to the range string or                 */
/*                          NULL if the value is out of range or INVALID.  */
/***************************************************************************/

INT1               *
MmiIfTypeCompleteFunc (t_MMI_CMD * node, INT1 *pi1IfTypeStr, INT2 *pi2ErrCode)
{

    /* This function is to check the given interface type prefix and will do tab completion
     * for tokens of type <iftype>.
     * It is assumed that pi1IfTypeStr has sufficient memory allocated to copy the longest match
     * prefix in it (i.e sufficient memory is allocated in mmi_convert_value_totype function.
     */

#ifdef CFA_WANTED
    if (CfaCliValidateInterfaceName (pi1IfTypeStr, pi1IfTypeStr, NULL) ==
        CLI_FAILURE)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
        return NULL;
    }
    CLI_STRCAT (pi1IfTypeStr, " ");
    UNUSED_PARAM (node);
#else

    UNUSED_PARAM (node);
    UNUSED_PARAM (pi1IfTypeStr);
    UNUSED_PARAM (pi2ErrCode);
#endif

    return NULL;
}

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1   *pi1IfXTypeStr : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/*  Return Value          : Pointer to the range string or                 */
/*                          NULL if the value is out of range or INVALID.  */
/***************************************************************************/

INT1               *
MmiIfXTypeCompleteFunc (t_MMI_CMD * node, INT1 *pi1IfXTypeStr, INT2 *pi2ErrCode)
{
    /* This function is to check the given interface type prefix and 
     * will do tab completion for tokens of type <ifXtype>.
     * It is assumed that pi1IfTypeStr has sufficient memory allocated 
     * to copy the longest match prefix in it 
     * i.e sufficient memory is allocated in mmi_convert_value_totype function.
     */

#ifdef CFA_WANTED
    if (CfaCliValidateXInterfaceName (pi1IfXTypeStr, pi1IfXTypeStr, NULL) ==
        CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfXTypeStr, pi1IfXTypeStr, NULL,
                                           NULL) == CLI_FAILURE)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
            return NULL;
        }
    }
    CLI_STRCAT (pi1IfXTypeStr, " ");
    UNUSED_PARAM (node);
#else

    UNUSED_PARAM (node);
    UNUSED_PARAM (pi1IfXTypeStr);
    UNUSED_PARAM (pi2ErrCode);
#endif

    return NULL;
}

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1      *pi1IfNum   : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/*  Return Value          : Pointer to the range string or                 */
/*                          NULL if the value is out of range or INVALID.  */
/***************************************************************************/

INT1               *
MmiIfNumTokenCheck (t_MMI_CMD * node, INT1 *pi1IfNum, INT2 *pi2ErrCode)
{
    tCliContext        *pCliContext;
#ifdef CFA_WANTED
    UINT4               u4IfXType = 0;
    UINT4               u4SubType = 0;
    UINT4               u4RetIfNum = 0;
    INT4                i4SlotNo;
    INT4                i4Ret;
#endif
    INT1               *pi1IfXType = NULL;
    INT2                i2TokenIndex = 0;

    /* This function is to check the validity of the given IfNum based
     * on the Interface type which is given as input in the previous token.
     * The previous token index is got from CliContext using the token index
     * as (*pi2Errcode -1).
     */
    i2TokenIndex = (INT2) (*pi2ErrCode - 1);

    if (((pCliContext = CliGetContext ()) == NULL) ||
        (i2TokenIndex <= 0) || (i2TokenIndex >= pCliContext->i2TokenCount))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
        return NULL;
    }

    pi1IfXType = pCliContext->MmiCmdToken[i2TokenIndex];

#ifdef CFA_WANTED
    if (CfaCliValidateXInterfaceName (pi1IfXType, NULL, &u4IfXType) ==
        CLI_FAILURE)
    {
        if (CfaCliValidateXSubTypeIntName (pi1IfXType, NULL, &u4IfXType,
                                           &u4SubType) == CLI_FAILURE)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
            return NULL;
        }
    }

    i4Ret = CfaValidateIfNum (u4IfXType, pi1IfNum, &i4SlotNo, &u4RetIfNum);

    if (i4Ret == OSIX_FAILURE)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFACE_NUM);
        return NULL;
    }

    if (i4Ret == CLI_ERR_INVALID_INTERFACE_NUM)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_WRONG_IFTYPE_FOR_PORT);
        return NULL;
    }

    UNUSED_PARAM (node);
#else

    UNUSED_PARAM (node);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pi2ErrCode);
#endif

    return NULL;
}

/***************************************************************************/
/*  t_MMI_CMD * node      : Command structure containing the token         */
/*  INT1    *pi1IfaceList : token string                                   */
/*  INT2      *pi2ErrCode : pointer returning value of Error Code          */
/*  Return Value          : Pointer to the range string or                 */
/*                          NULL if the value is out of range or INVALID.  */
/***************************************************************************/

INT1               *
MmiIfaceListTokenCheck (t_MMI_CMD * node, INT1 *pi1IfaceList, INT2 *pi2ErrCode)
{
    tCliContext        *pCliContext;
    INT1               *pi1IfXType = NULL;
    INT2                i2TokenIndex = 0;
    UINT4               u4PortArrayLen;
    UINT1              *pu1PortArray = NULL;
    UINT1              *pu1Ptr;

    /* This function is to check the validity of the given Iface_list based
     * on the Interface type which is given as input in the previous token.
     * The previous token index is got from CliContext using the token index
     * as (*pi2Errcode -1).
     */
    i2TokenIndex = (INT2) (*pi2ErrCode - 1);

    if (((pCliContext = CliGetContext ()) == NULL) ||
        (i2TokenIndex <= 0) || (i2TokenIndex >= pCliContext->i2TokenCount))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
        return NULL;
    }

    /* Get MaxPorts supported, max of vlan max ports or Max physical ports
     * and use that in allocation of pu1PortArray */

    if (!(pi1IfaceList) || !(*pi1IfaceList) ||
        !(pu1Ptr = CliMemAllocMemBlk (gCliTokenPoolId, MAX_CHAR_IN_TOKEN)))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
        return NULL;
    }

    CLI_STRCPY (pu1Ptr, pi1IfaceList);
    /* sizeof tPortList is sufficient to include internal, sisp and
     * VIP ports
     */
    u4PortArrayLen = sizeof (tPortList);
    if ((pu1PortArray = FsUtilAllocBitList (sizeof (tPortList))) == NULL)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
        MemReleaseMemBlock (gCliTokenPoolId, (UINT1 *) pu1Ptr);
        return NULL;
    }

    pi1IfXType = pCliContext->MmiCmdToken[i2TokenIndex];

#ifdef CFA_WANTED
    if (CfaCliGetIfList
        (pi1IfXType, (INT1 *) pu1Ptr, pu1PortArray,
         u4PortArrayLen) == CLI_FAILURE)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_IFTYPE);
    }

    UNUSED_PARAM (node);
#else

    UNUSED_PARAM (node);
    UNUSED_PARAM (pi1IfaceList);
    UNUSED_PARAM (pi2ErrCode);
#endif
    MemReleaseMemBlock (gCliTokenPoolId, (UINT1 *) pu1Ptr);
    FsUtilReleaseBitList ((UINT1 *) pu1PortArray);
    return NULL;
}
