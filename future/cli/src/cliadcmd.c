/******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cliadcmd.c,v 1.7 2010/08/04 13:15:41 prabuc Exp $
 *
 * Description: This file is to add a command ptr to a mode or 
 *              command_group. The routines are used in the cmdscan 
 *              generated file.      
 *
 *******************************************************************/

#include "clicmds.h"

/*
 * This routine will get the mode node from mode tree
 * and will link the new created cmd node to the cmd list
 *
 * Will be called from mode-command initialisation
 * ie. mmi_cmd_creat()
 */

extern tMemPoolId   gCliCmdListPoolId;

INT4
mmi_add_mode_cmd (i1pName, pCmdptr, i4ActionNum)
     CONST CHR1         *i1pName;    /* mode name in which cmd to be added */
     t_MMI_CMD          *pCmdptr;
     INT4                i4ActionNum;
{
    t_MMI_MODE_TREE    *pMode_ptr;
    t_MMI_COMMAND_LIST *pCmdlist_ptr;
    t_MMI_CMD          *pCmdtemp;
    t_MMI_CMD          *pCmdinter_ptr;

    if ((pMode_ptr = mmicm_search_parent (i1pName)) == NULL)
    {
        return OSIX_FAILURE;
    }
    pCmdlist_ptr = pMode_ptr->pCommand_tree;
    if (pCmdlist_ptr->pCmd == NULL)
    {
        pMode_ptr->i4ActionNumStartIdx = i4ActionNum;
        pCmdlist_ptr->pCmd = pCmdptr;
        return OSIX_SUCCESS;
    }

    pCmdtemp = pCmdlist_ptr->pCmd;
    pCmdinter_ptr = pCmdtemp->pNext_command;
    while (pCmdinter_ptr != NULL)
    {
        pCmdtemp = pCmdinter_ptr;
        pCmdinter_ptr = pCmdinter_ptr->pNext_command;
    }

    pCmdtemp->pNext_command = pCmdptr;
    return OSIX_SUCCESS;
}

/*
 * This routine will get the mode node from mode tree
 * and will link the new created cmd node to the cmd list
 */

INT4
mmi_add_grp_cmd (pCmdlist, pCmdptr)
     t_MMI_COMMAND_LIST *pCmdlist;
     t_MMI_CMD          *pCmdptr;
{
    t_MMI_COMMAND_LIST *temp_list_ptr;
    t_MMI_CMD          *temp_cmd_ptr;
    t_MMI_CMD          *inter_cmd_ptr;

    if (pCmdlist == NULL)
    {
        return OSIX_FAILURE;
    }
    temp_list_ptr = pCmdlist;
    if (temp_list_ptr->pCmd == NULL)
    {
        temp_list_ptr->pCmd = pCmdptr;
        return OSIX_SUCCESS;
    }

    temp_cmd_ptr = temp_list_ptr->pCmd;
    inter_cmd_ptr = temp_cmd_ptr->pNext_command;
    while (inter_cmd_ptr != NULL)
    {
        temp_cmd_ptr = inter_cmd_ptr;
        inter_cmd_ptr = inter_cmd_ptr->pNext_command;
    }

    temp_cmd_ptr->pNext_command = pCmdptr;
    return OSIX_SUCCESS;
}

/*
 * This routine will get the mode node from mode tree
 * and will link the grp list to the mode_list
 */
INT4
mmi_add_grp_to_mode (i1pName, pCmdlist)
     CONST CHR1         *i1pName;
     t_MMI_COMMAND_LIST *pCmdlist;
{
    t_MMI_MODE_TREE    *mode_ptr;
    t_MMI_COMMAND_LIST *temp_list_ptr;
    t_MMI_COMMAND_LIST *inter_list_ptr;

    if ((mode_ptr = mmicm_search_parent (i1pName)) == NULL)
    {
        return OSIX_FAILURE;
    }
    temp_list_ptr = mode_ptr->pCommand_tree;
    inter_list_ptr = temp_list_ptr->pNext;

    while (inter_list_ptr != NULL)
    {
        temp_list_ptr = inter_list_ptr;
        inter_list_ptr = inter_list_ptr->pNext;
    }

    if ((temp_list_ptr->pNext =
         CliMemAllocMemBlk (gCliCmdListPoolId,
                            sizeof (t_MMI_COMMAND_LIST))) == NULL)
    {
        mmi_printf (" error in memory allocation in mmi_add_grp_to_mode \n");
        return OSIX_FAILURE;
    }
    temp_list_ptr = temp_list_ptr->pNext;
    temp_list_ptr->pCmd = pCmdlist->pCmd;
    return OSIX_SUCCESS;
}
