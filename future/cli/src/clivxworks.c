/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clivxworks.c,v 1.42 2013/05/22 12:35:35 siva Exp $
 *
 * Description: This file is the Vxworks ported version of cliport.c   
 *                           
 *******************************************************************/

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dllLib.h>
#include <lstLib.h>
#include <ioLib.h>
#include <selectLib.h>
#include <timers.h>

#include "clicmds.h"

/* Added API to execute BCM CLI commands from ISS CLI */
#ifdef BCM_SHELL_WANTED
void                bcmShell (void);
extern unsigned char gu1BcmCliEnb;
#endif

VOID                CliInitNpHook (VOID);
VOID                CliExit (VOID);
void                CliSignalHandler (int i4Signal);

extern tMemPoolId   gCliUserMemPoolId;
extern tMemPoolId   gCliGroupMemPoolId;
extern tMemPoolId   gCliPrivilegesMemPoolId;

/* CLI_POLL - which when enabled will scan the input at the console on 
 * polling mode. When the switch is disabled the console will be set
 * to blocking mode for efficiency. Blocking mode will not work for 
 * Single threaded OS (tmo). By default the switch is enabled 
 * (Polling Mode).
 */
#ifdef CLI_POLL
#define  CLI_POLL_SET_NONBLOCK(x,y,z)  CliSerialIoctl(x,y,z)
#define  CLI_POLL_DELAY(u4Delay)  OsixTskDelay(u4Delay)
#define  CLI_SESSION_TIMEOUT_FUNC(x, y, z, t) (x > y->i4SessionTimer ? 1 : 0)
#else
#define  CLI_POLL_SET_NONBLOCK(x,y,z)
#define  CLI_POLL_DELAY(u4Delay)
#define  CLI_SESSION_TIMEOUT_FUNC(x, y, z, t) \
            ( ( (select (y->i4InputFd + 1, z, NULL, NULL, t) ) <= 0) ? 1 : 0)
#endif

/* CLI_SIG_CAP - when enabled will capture Ctrl-C signal, and 
 * will ignore the processing for that SIGNAL. 
 * For unsupported targets this should be disabled
 */
#undef CLI_SIG_CAP
#ifdef CLI_SIG_CAP
#define  SIGNAL(x,y)  signal(x,y)
#else
#define  SIGNAL(x,y)
#ifndef SIGINT
#define  SIGINT       0
#endif
#ifndef SIGWINCH
#define  SIGWINCH     0
#endif
#ifndef SIGQUIT
#define  SIGQUIT     0
#endif
#endif

#ifdef write
#undef write
#endif
#ifdef read
#undef read
#endif

extern tCliSessions gCliSessions;
/***************************************************************************
 *  FUNCTION NAME : CliSerialIoInit 
 *  DESCRIPTION   : This function is used to initialize cli stream i/o to
 *                  default interfaces i.e. stdin and stdout
 *  INPUT         : NONE
 *  OUTPUT        : NONE
 *  RETURNS       : NONE
 ****************************************************************************/

INT4
CliSerialIoInit (pCliContext)
     tCliContext        *pCliContext;
{
    /* Set the BCM CLI to low priority */
    taskPrioritySet (taskNameToId ("CLIC"), 100);
    pCliContext->i4InputFd = fileno (CLI_INPUT_STREAM);
    pCliContext->i4OutputFd = fileno (CLI_OUTPUT_STREAM);
    pCliContext->ReadFp = CLI_INPUT_STREAM;
    pCliContext->WriteFp = CLI_OUTPUT_STREAM;

    CLI_POLL_SET_NONBLOCK (pCliContext, CLI_SET_NONBLOCK, NULL);

    /* Functiop to be called on termination */
    atexit (CliExit);

    SIGNAL (SIGINT, CliSignalHandler);
    SIGNAL (SIGWINCH, CliSignalHandler);
    SIGNAL (SIGQUIT, CliSignalHandler);
    return (CLI_OS_SUCCESS);
}

/***************************************************************************
 *  FUNCTION NAME : CliSerialRead
 *  DESCRIPTION   : This function reads any cli input from the input stream
 *  INPUT         : NONE
 *  OUTPUT        : NONE
 *  RETURNS       : NONE
 ****************************************************************************/

UINT1
CliSerialRead (pCliContext)
     tCliContext        *pCliContext;
{
    struct timeval      CliTimeval;
    struct timeval     *pTimer = NULL;
    fd_set              readfds;
    UINT1               u1ch = 0;
    UINT4               u4Retval = 0;
    UINT1               au1TempChar[1];
    INT4                i4TimerCnt = 0;

#ifdef BCM_SHELL_WANTED
    if (gu1BcmCliEnb == 1)
    {
        return (CLI_SUCCESS);
    }
#endif

    FD_ZERO (&readfds);
    FD_SET (pCliContext->i4InputFd, &readfds);
    CliTimeval.tv_sec = pCliContext->i4SessionTimer;
    CliTimeval.tv_usec = 0;
    if (pCliContext->i4SessionTimer)
    {
        pTimer = &CliTimeval;
    }
    while (1)
    {
        CLI_POLL_DELAY (1);
        i4TimerCnt++;

        if (pCliContext->mmi_exit_flag)
            return CLI_FAILURE;

        if (pCliContext->SessionActive && pCliContext->i4SessionTimer &&
            (CLI_SESSION_TIMEOUT_FUNC
             (i4TimerCnt / SYS_NUM_OF_TIME_UNITS_IN_A_SEC,
              pCliContext, &readfds, pTimer)))
        {
            mmi_printf ("\nIdle Timer expired, Timing Out !!!\n");
            CliTimeoutExpiry (pCliContext);
            return (CLI_FAILURE);
        }
        if ((u4Retval =
             read (pCliContext->i4InputFd, (CHR1 *) & u1ch, (size_t) 1)) == 1)
        {
            if (!pCliContext->SessionActive)
            {
                pCliContext->SessionActive = TRUE;
            }
            au1TempChar[0] = u1ch;
            break;
        }
        else
            continue;
    }
    FD_CLR (pCliContext->i4InputFd, &readfds);
    return (UINT4) u1ch;
}

/***************************************************************************
 *  FUNCTION NAME : CliSerialWrite
 *  DESCRIPTION   : This function writes output message to the cli console on
 *                  the output stream gfpCliOutput       
 *  INPUT         : IfIndex, pu1OutMsg, Length 
 *  OUTPUT        : NONE
 *  RETURNS       : No of Bytes written 
 ****************************************************************************/

INT4
CliSerialWrite (tCliContext * pCliContext, CONST CHR1 * pOutMsg, UINT4 u4Length)
{

    UINT4               u4WriteCount = 0;
    INT2                i2Retval = 0;

#ifdef BCM_SHELL_WANTED
    if (gu1BcmCliEnb == 1)
    {
        return (CLI_SUCCESS);
    }
#endif

    if (0 == u4Length)
        return (CLI_SUCCESS);
    while (1)
    {
        i2Retval =
            write (pCliContext->i4OutputFd, (CHR1 *) pOutMsg, (size_t) 1);
        if (i2Retval == 1)
        {
            pOutMsg++;
            u4WriteCount++;
        }
        if (u4WriteCount == u4Length)
            break;
    }

    return (CLI_SUCCESS);
}

INT4
CliSerialIoctl (tCliContext * pCliContext, INT4 i4Code, VOID *ptr)
{

    INT4                i4Flags;

    switch (i4Code)
    {
        case CLI_SET_NONBLOCK:
            /* CLI expects the Terminal settings always to be in RAW mode
             * (character wise read mode) hence reset the OPT_LINE bit.
             */
            i4Flags =
                ioctl (pCliContext->i4InputFd, FIOSETOPTIONS,
                       (OPT_TERMINAL & ~OPT_LINE));

            if (i4Flags == ERROR)
            {
                return (CLI_OS_FAILURE);
            }
            break;

        case CLI_ECHO_ENABLE:
            /* CLI expects the Terminal settings always to be in RAW mode
             * (character wise read mode) hence reset the OPT_LINE bit.
             */
            i4Flags =
                ioctl (pCliContext->i4InputFd, FIOSETOPTIONS,
                       (OPT_TERMINAL & ~OPT_LINE));

            if (i4Flags == ERROR)
            {
                return (CLI_OS_FAILURE);
            }
            break;

        case CLI_ECHO_DISABLE:
            /* Reset the ECHO bit for disabling the echo and as 
             * CLI expects the Terminal settings always to be in RAW mode
             * (character wise read mode) hence reset the OPT_LINE bit.
             */
            i4Flags =
                ioctl (pCliContext->i4InputFd, FIOSETOPTIONS,
                       ((OPT_TERMINAL & ~OPT_LINE) & ~OPT_ECHO));

            if (i4Flags == ERROR)
            {
                return (CLI_OS_FAILURE);
            }
            break;

        case CLI_SWAP_INPUT:

            /* This directive is used to swap the input file descriptor with a
             * given file. If the file name is console then the descriptor for
             * stdin is used henceforth. Otherwise the specified file is opened
             * and the corresponding  descriptor is set as the input file 
             * descriptor.
             */

            if (strcmp (ptr, "console") == 0)
            {
                if (pCliContext->ReadFp != NULL)
                {
                    fclose (pCliContext->ReadFp);
                    close (pCliContext->i4InputFd);
                }
                pCliContext->i4InputFd = fileno (CLI_INPUT_STREAM);
                pCliContext->ReadFp = CLI_INPUT_STREAM;
                return (CLI_OS_SUCCESS);
            }
            else
            {
                pCliContext->ReadFp = FOPEN (ptr, "r");
                if (pCliContext->ReadFp == NULL)
                {
                    return (CLI_OS_FAILURE);
                }
                pCliContext->i4InputFd = fileno (pCliContext->ReadFp);
                CliSerialIoctl (pCliContext, CLI_SET_NONBLOCK, 0);
                return (CLI_OS_SUCCESS);

            }
            break;

        case CLI_SWAP_OUTPUT:

            if (strcmp (ptr, "console") == 0)
            {
                if (pCliContext->WriteFp != NULL)
                {
                    fclose (pCliContext->WriteFp);
                    close (pCliContext->i4OutputFd);
                }
                pCliContext->WriteFp = CLI_OUTPUT_STREAM;
                pCliContext->i4OutputFd = fileno (CLI_OUTPUT_STREAM);
                return (CLI_OS_SUCCESS);
            }
            else
            {
                pCliContext->WriteFp = FOPEN (ptr, "w");
                if (pCliContext->WriteFp == NULL)
                {
                    return (CLI_OS_FAILURE);
                }
                pCliContext->i4OutputFd = fileno (pCliContext->WriteFp);
                return (CLI_OS_SUCCESS);

            }
            break;

        case CLI_FLUSH_OUTPUT:
            fflush (pCliContext->WriteFp);
            break;

        case CLI_CLEAR_SCREEN:
            system ("clear");
            break;

        case CLI_SET_NONCANON:

            /* Enable the RAW mode by disabling the LINE mode.
             * OPT_TERMINAL sets all the bits. Hence reset the OPT_LINE bit.
             */

            i4Flags =
                ioctl (pCliContext->i4InputFd, FIOSETOPTIONS,
                       (OPT_TERMINAL & ~OPT_LINE));
            if (i4Flags == ERROR)
            {
                return (CLI_OS_FAILURE);
            }
            break;
        case CLI_GET_WINSZ:
            pCliContext->u4Flags = (pCliContext->u4Flags | CLI_TIOCGWINSZ_MASK);
            break;
        default:
            return (CLI_OS_FAILURE);
    }
    return (CLI_OS_SUCCESS);
}

INT4
IsEndofFile (pCliContext)
     tCliContext        *pCliContext;
{
    if (feof (pCliContext->ReadFp) > 0)
    {
        return (CLI_OS_SUCCESS);
    }
    return (CLI_OS_FAILURE);

}

VOID
CliDisplayLogo ()
{
#ifdef ISS_WANTED
    CHR1                au1BannerMsg[CLI_MAX_BANNER_LEN];
    MEMSET (au1BannerMsg, 0, CLI_MAX_BANNER_LEN);
    CLI_GET_ISS_BANNER (au1BannerMsg);
    mmi_printf ("\r%s", CLI_MOVE_CURSOR_TO_END);
    mmi_printf ("\r\n\t\t %s \r\n\r\n", au1BannerMsg);
#endif
    return;
}

VOID
CliGetLoginPrompt (INT1 *pLogStr)
{
#ifdef ISS_WANTED
    STRCPY (pLogStr, CLI_GET_ISS_SWITCH_NAME ());
    STRCAT (pLogStr, " login: ");

#else
    STRCPY (pLogStr, CLI_LOGIN_PROMPT);
#endif
}

INT4
CliGetUsersFromNvRam (t_MMI_PASSWD * pmmi_user)
{

    MEMSET (pmmi_user, 0, sizeof (t_MMI_PASSWD) * MAX_NO_USER);

    /* Sample Code for loading one CLI user information */

    CLI_STRCPY (pmmi_user[0].i1user_name, CLI_ROOT_USER);

    CLI_STRCPY (pmmi_user[0].i1user_passwd, CLI_ROOT_PASSWD);
    mmi_encript_passwd ((CHR1 *) pmmi_user[0].i1user_passwd);

    CLI_STRCPY (pmmi_user[0].i1user_mode, "/");

    pmmi_user[0].i1user_write = TRUE;
    pmmi_user[0].i1user_purge = TRUE;

    return (CLI_OS_SUCCESS);
}

INT4
CliSetUsersInNvRam (t_MMI_PASSWD * pmmi_user)
{
    UNUSED_PARAM (pmmi_user);
    return (CLI_OS_SUCCESS);
}

VOID
CliRemoveUserFromNvRam (INT1 *i1user)
{

    UNUSED_PARAM (i1user);
    return;
}

VOID
CliLogEvent (INT4 i4Code)
{

    /* Use this function to Log any CLI login success/failures */
    UNUSED_PARAM (i4Code);
    return;

}

/* This routine opens a file given as its Argument with the mode
 * specified in the argument and returns the File descriptor of the 
 * file opened
 */
INT4
CliOpenFile (CONST CHR1 * pi1Filename, INT4 i4Flag)
{
    INT4                i4FileFd;

    if (CLI_RDONLY == i4Flag)
        i4Flag = OSIX_FILE_RO;
    else if (CLI_WRONLY == i4Flag)
        i4Flag = OSIX_FILE_WO | OSIX_FILE_CR | OSIX_FILE_AP;

    if ((i4FileFd = FileOpen ((CONST UINT1 *) pi1Filename, i4Flag)) < 0)
    {
        return -1;
    }
    return i4FileFd;
}

/* This routine Closes an open file given as its Argument */
INT2
CliCloseFile (INT4 i4FileFd)
{
    INT2                i2Retval;

    if ((i2Retval = FileClose (i4FileFd)) < 0)
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/* This Routine writes Messages on an Output File */
INT4
CliFileWrite (tCliContext * pCliContext, CONST CHR1 * pOutMsg, UINT4 u4Len)
{
    UINT4               u4WriteCount = 0;
    INT2                i2Retval = 0;

    if (u4Len == 0)
        return (CLI_SUCCESS);
    while (1)
    {
        if (u4WriteCount == u4Len)
            break;
        if (*pOutMsg == '\r')
        {
            pOutMsg++;
            u4WriteCount++;
            continue;
        }

        i2Retval = FileWrite (pCliContext->i4OutputFd, (CHR1 *) pOutMsg, 1);
        if (i2Retval == 1)
        {
            pOutMsg++;
            u4WriteCount++;
        }
    }
    return CLI_SUCCESS;
}

/* This Routine writes Messages on an Output File */
INT1
CliGetLine (INT4 i4Filefd, INT1 *pi1Cmd)
{
    INT2                i2index = 0;
    INT2                i2ReadCount = 0;
    INT1                i1Char;

    while ((i2ReadCount = FileRead (i4Filefd, (CHR1 *) & i1Char, 1)) == 1)
    {
        i2index++;
        if ('\n' == i1Char)
        {
            *pi1Cmd++ = i1Char;
            break;
        }

        if (i2index > MAX_LINE_LEN)
            continue;
        *pi1Cmd++ = i1Char;
    }
    *pi1Cmd = '\0';

    if (i2ReadCount == 0)
        return CLI_EOF;
    else
        return CLI_SUCCESS;
}

INT4
CliTelnetIoInit (tCliContext * pCliContext)
{
    UNUSED_PARAM (pCliContext);

    SIGNAL (SIGINT, CliSignalHandler);
    return (CLI_SUCCESS);
}

/**************************************************************************
  FUNCTION      : CliExit       
  DESCRIPTION   : This function is called to restore the terminal 
                  settings when  CTRL-C is pressed.
                  This is specific to linux users and needs to be 
          ported for other environments.
  INPUT(S)      : NONE
  OUTPUT(S)     : NONE
  RETURN VALUE  : NONE
***************************************************************************/

VOID
CliExit (VOID)
{
    mmi_exit ();
    exit (0);
}

INT1
CliReadUsers (VOID)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    INT2                i2UserIndex = 0;
    INT1                i1Status = CLI_FAILURE;
    BOOL1               bIsMemFailFlag = OSIX_FALSE;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;
    INT2                i2TotLen = 0;
    INT2                i2StrLen = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    i4Fd = CliOpenFile (CLI_USER_FILE_NAME, CLI_RDONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliUsersFileSemId);
    while (CliReadLineFromFile (i4Fd, ai1Buf, CLI_MAX_USERS_LINE_LEN,
                                &i2ReadLen) != CLI_EOF)
    {
        if (i2ReadLen > 0)
        {
            /* The format of a line in users file should contain 3 ':' 
             * ie it is in the format  user:privilage:mode:password */
            if ((CliCheckFormat (ai1Buf, 3)) != CLI_SUCCESS)
            {
                continue;
            }
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            i2StrLen = CLI_STRLEN (ai1Buf);
            i2TotLen += i2ReadLen;
            ai1Buf[i2StrLen - 1] = '\0';
            if (i2UserIndex == CLI_MAX_USERS)
            {
                i1Status = CLI_SUCCESS;
                continue;
            }

            if (gCliSessions.pi1CliUsers[i2UserIndex] == NULL)
            {
                if ((gCliSessions.pi1CliUsers[i2UserIndex] =
                     CliMemAllocMemBlk (gCliUserMemPoolId,
                                        CLI_MAX_USERS_LINE_LEN + 1)) == NULL)
                {
                    i1Status = CLI_FAILURE;
                    bIsMemFailFlag = OSIX_TRUE;
                    i2ReadLen = 0;
                    break;
                }
            }
            STRCPY (gCliSessions.pi1CliUsers[i2UserIndex], ai1Buf);
            i2UserIndex++;
            i1Status = CLI_SUCCESS;
        }
    }
    /* Calculate checksum for the last line */
    i1Status = CliVerifyCheckSum (ai1Buf, i2TotLen, i2ReadLen, u4Sum, u2CkSum);

    OsixSemGive (gCliSessions.CliUsersFileSemId);
    gCliSessions.pi1CliUsers[i2UserIndex] = NULL;
    gCliSessions.i2NofUsers = i2UserIndex;
    if (bIsMemFailFlag == OSIX_TRUE)
    {
        mmi_printf ("Memory allocation failure \r\n");
    }
    close (i4Fd);
    return (i1Status);

}

INT1
CliReadPrivileges ()
{
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    INT2                i2PrivilegeIndex = 0;
    INT1                i1Status = CLI_FAILURE;
    BOOL1               bIsMemFailFlag = OSIX_FALSE;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;
    INT2                i2TotLen = 0;
    INT2                i2StrLen = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    i4Fd = CliOpenFile (CLI_PRIVILEGES_FILE_NAME, CLI_RDONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliPrivilegesFileSemId);
    i1Status = CLI_SUCCESS;
    while (CliReadLineFromFile (i4Fd, ai1Buf, CLI_MAX_PRIVILEGES_LINE_LEN,
                                &i2ReadLen) != CLI_EOF)
    {
        if (i2ReadLen > 0)
        {
            /* The format of a line in privil file should contain 1 ':' 
             * ie it is in the format  privilage:password */
            if ((CliCheckFormat (ai1Buf, 1)) != CLI_SUCCESS)
            {
                continue;
            }
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            i2StrLen = CLI_STRLEN (ai1Buf);
            i2TotLen += i2ReadLen;
            ai1Buf[i2StrLen - 1] = '\0';
            if (i2PrivilegeIndex == CLI_MAX_PRIVILEGES)
            {
                i1Status = CLI_SUCCESS;
                continue;
            }

            if (!(gCliSessions.pi1CliPrivileges[i2PrivilegeIndex] =
                  (INT1 *) CliMemAllocMemBlk (gCliPrivilegesMemPoolId,
                                              CLI_MAX_PRIVILEGES_LINE_LEN)))
            {
                i1Status = CLI_FAILURE;
                bIsMemFailFlag = OSIX_TRUE;
                i2ReadLen = 0;
                break;
            }
            STRCPY (gCliSessions.pi1CliPrivileges[i2PrivilegeIndex], ai1Buf);
            i2PrivilegeIndex++;
        }
    }
    /* Calculate checksum for the last line */
    i1Status = CliVerifyCheckSum (ai1Buf, i2TotLen, i2ReadLen, u4Sum, u2CkSum);

    OsixSemGive (gCliSessions.CliPrivilegesFileSemId);
    gCliSessions.pi1CliPrivileges[i2PrivilegeIndex] = NULL;
    gCliSessions.i2NofPrivileges = i2PrivilegeIndex;
    if (bIsMemFailFlag == OSIX_TRUE)
    {
        mmi_printf ("Memory allocation failure \r\n");
    }

    close (i4Fd);
    return (i1Status);
}

INT1
CliReadGroups ()
{
    INT1                ai1Buf[CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    INT2                i2GroupIndex = 0;
    INT1                i1Status = CLI_SUCCESS;
    BOOL1               bIsMemFailFlag = OSIX_FALSE;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;
    INT2                i2TotLen = 0;
    INT2                i2StrLen = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    i4Fd = CliOpenFile (CLI_GROUP_FILE_NAME, CLI_RDONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliGroupsFileSemId);
    while (CliReadLineFromFile (i4Fd, ai1Buf, CLI_MAX_GROUPS_LINE_LEN,
                                &i2ReadLen) != CLI_EOF)
    {
        if (i2ReadLen > 0)
        {
            /* The format of a line in groups file should contain 2 ':' 
             * ie it is in the format  group:groupid:users */
            if ((CliCheckFormat (ai1Buf, 2)) != CLI_SUCCESS)
            {
                continue;
            }
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            i2StrLen = CLI_STRLEN (ai1Buf);
            i2TotLen += i2ReadLen;
            ai1Buf[i2StrLen - 1] = '\0';
            if (i2GroupIndex == CLI_MAX_GROUPS)
            {
                i1Status = CLI_SUCCESS;
                break;
            }

            if (!(gCliSessions.pi1CliGroups[i2GroupIndex] =
                  (INT1 *) CliMemAllocMemBlk (gCliGroupMemPoolId,
                                              CLI_MAX_GROUPS_LINE_LEN + 1)))
            {
                i1Status = CLI_FAILURE;
                bIsMemFailFlag = OSIX_TRUE;
                i2ReadLen = 0;
                break;
            }
            STRCPY (gCliSessions.pi1CliGroups[i2GroupIndex], ai1Buf);
            i2GroupIndex++;
        }
    }
    /* Calculate checksum for the last line */
    i1Status = CliVerifyCheckSum (ai1Buf, i2TotLen, i2ReadLen, u4Sum, u2CkSum);

    OsixSemGive (gCliSessions.CliGroupsFileSemId);
    gCliSessions.pi1CliGroups[i2GroupIndex] = NULL;
    gCliSessions.i2NofGroups = i2GroupIndex;
    if (bIsMemFailFlag == OSIX_TRUE)
    {
        mmi_printf ("Memory allocation failure \r\n");
    }

    close (i4Fd);
    return (i1Status);
}

INT1
CliWriteUsers ()
{
    INT4                i4Fd;
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN];
    INT2                i2Len;
    INT2                i2UserIndex;
    INT1                i1Status = CLI_SUCCESS;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    INT2                i2Length = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    unlink (CLI_USER_FILE_NAME);    /* remove the existing file */
    i4Fd = CliOpenFile (CLI_USER_FILE_NAME, CLI_WRONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliUsersFileSemId);
    for (i2UserIndex = 0; i2UserIndex < gCliSessions.i2NofUsers; i2UserIndex++)
    {
        if ((gCliSessions.pi1CliUsers[i2UserIndex]) &&
            (STRLEN (gCliSessions.pi1CliUsers[i2UserIndex])))
        {
            i2Len =
                SPRINTF ((CHR1 *) ai1Buf, "%s\n",
                         gCliSessions.pi1CliUsers[i2UserIndex]);
            /* Calculating checksum */
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2CkSum,
                              CKSUM_DATA);
            FileWrite (i4Fd, (CHR1 *) ai1Buf, (UINT4) i2Len);
            i2Length += i2Len;
        }
    }
    if (CliFindCksum (i4Fd, i2Length, u4Sum) == CLI_FAILURE)
    {
        i1Status = CLI_FAILURE;
    }

    OsixSemGive (gCliSessions.CliUsersFileSemId);

    close (i4Fd);
    return (i1Status);
}

INT1
CliWritePrivileges ()
{
    INT1                ai1Buf[CLI_MAX_PRIVILEGES_LINE_LEN];
    INT4                i4Fd;
    INT2                i2Len;
    INT2                i2PrivilegeIndex;
    INT1                i1Status = CLI_SUCCESS;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    INT2                i2Length = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    unlink (CLI_PRIVILEGES_FILE_NAME);    /* remove the existing file */
    i4Fd = CliOpenFile (CLI_PRIVILEGES_FILE_NAME, CLI_WRONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliPrivilegesFileSemId);
    for (i2PrivilegeIndex = 0;
         i2PrivilegeIndex < gCliSessions.i2NofPrivileges; i2PrivilegeIndex++)
    {
        if (STRLEN (gCliSessions.pi1CliPrivileges[i2PrivilegeIndex]))
        {
            i2Len =
                SPRINTF ((CHR1 *) ai1Buf, "%s\n",
                         gCliSessions.pi1CliPrivileges[i2PrivilegeIndex]);
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2CkSum,
                              CKSUM_DATA);
            FileWrite (i4Fd, (CHR1 *) ai1Buf, (size_t) i2Len);
            i2Length += i2Len;
        }
    }
    if (CliFindCksum (i4Fd, i2Length, u4Sum) == CLI_FAILURE)
    {
        i1Status = CLI_FAILURE;
    }

    OsixSemGive (gCliSessions.CliPrivilegesFileSemId);

    CliCloseFile (i4Fd);
    return (i1Status);
}

INT1
CliWriteGroups ()
{
    INT1                ai1Buf[CLI_MAX_GROUPS_LINE_LEN];
    INT4                i4Fd;
    INT2                i2Len;
    INT2                i2GroupIndex;
    INT1                i1Status = CLI_SUCCESS;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    INT2                i2Length = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    unlink (CLI_GROUP_FILE_NAME);    /* remove the existing file */
    i4Fd = CliOpenFile (CLI_GROUP_FILE_NAME, CLI_WRONLY);
    if (i4Fd < 0)
    {
        return (CLI_FAILURE);
    }

    OsixSemTake (gCliSessions.CliGroupsFileSemId);
    for (i2GroupIndex = 0; i2GroupIndex < gCliSessions.i2NofGroups;
         i2GroupIndex++)
    {
        if (STRLEN (gCliSessions.pi1CliGroups[i2GroupIndex]))
        {
            i2Len =
                SPRINTF ((CHR1 *) ai1Buf, "%s\n",
                         gCliSessions.pi1CliGroups[i2GroupIndex]);
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2CkSum,
                              CKSUM_DATA);
            FileWrite (i4Fd, (CHR1 *) ai1Buf, (UINT4) i2Len);
            i2Length += i2Len;
        }
    }
    if (CliFindCksum (i4Fd, i2Length, u4Sum) == CLI_FAILURE)
    {
        i1Status = CLI_FAILURE;
    }

    OsixSemGive (gCliSessions.CliGroupsFileSemId);
    CliCloseFile (i4Fd);
    return (i1Status);
}

t_MMI_BOOLEAN
CliIsSameFile (CONST CHR1 * pu1FileName1, CONST CHR1 * pu1FileName2)
{
    return FALSE;
}

t_MMI_BOOLEAN
CliIsRegularFile (CONST CHR1 * pu1FileName)
{
    return TRUE;
}

VOID
CliNpInit (VOID)
{
    /* Dummy function to support target specific initializations */
    CliInitNpHook ();
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSignalHandler                                    *
 *                                                                         *
 *     Description   : The function takes of control signal entered at     *
 *                     the CLI CONSOLE prompt.                             *
 *                                                                         *
 *     Input(s)      : i4Signal    : Signal Number                         *
 *                                                                         *
 *     Output(s)     : None                                                *
 *                                                                         *
 *     Returns       : None                                                *
 *                                                                         *
 ***************************************************************************/

void
CliSignalHandler (int i4Signal)
{
    tCliContext        *pCliContext = NULL;

    /* Signal handler's will be called only for CLI CONSOLE
     * So we take the Context Id as '0' as it's the
     * Context Id for CLI CONSOLE TASK
     */

    pCliContext = &(gCliSessions.gaCliContext[0]);

    if (i4Signal == SIGINT)
    {
        /* Ctrl-c entered at CLI Console prompt
         * set bit-CLI_CTRL_BRK_MASK in u4Flags which is Ctrl-c flag bit
         */
        pCliContext->u4Flags = (pCliContext->u4Flags | CLI_CTRL_BRK_MASK);

        SIGNAL (SIGINT, CliSignalHandler);
    }
    else if (i4Signal == SIGWINCH)
    {
        pCliContext->fpCliIoctl (pCliContext, CLI_GET_WINSZ, NULL);

        if ((pCliContext->i2MaxRow != pCliContext->i2PrevMaxRow) ||
            (pCliContext->i2MaxCol != pCliContext->i2PrevMaxCol))
        {
            CliHandleWinSizeChange (pCliContext);
        }
        SIGNAL (SIGWINCH, CliSignalHandler);
    }
    else if (i4Signal == SIGQUIT)
    {

        /* Ctrl-\ and Ctrl-4 entered at CLI Console prompt
         * set bit-CLI_CTRL_QUIT_MASK in u4Flags which is Ctrl-\ and Ctrl-4 flag bit
         */
        pCliContext->u4Flags = (pCliContext->u4Flags | CLI_CTRL_QUIT_MASK);
        SIGNAL (SIGQUIT, CliSignalHandler);
    }
    return;
}

INT1
CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (read (i4Fd, (CHR1 *) & i1Char, (size_t) 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (CLI_EOF);
}

VOID
CliInitNpHook (VOID)
{
    /* Delete the Shell task */
    taskDelete (taskNameToId (NP_CLI_TASK_NAME));
}

#ifdef ECFM_WANTED

/***************************************************************************
 *                                                                         *
 *     Function Name : CliEcfmRegisterSignalHandler                        *
 *                                                                         *
 *     Description   : The function takes of control signal entered at     *
 *                     the CLI CONSOLE prompt.                             *
 *                                                                         *
 *     Input(s)      : i4Signal    : Signal Number                         *
 *                                                                         *
 *     Output(s)     : None                                                *
 *                                                                         *
 *     Returns       : None                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliEcfmRegisterSignalHandler (INT4 i4Signal, VOID (*pfSignalHandler) (INT4))
{
#ifdef CLI_SIG_CAP
    SIGNAL (i4Signal, pfSignalHandler);
    return CLI_SUCCESS;
#else
    UNUSED_PARAM (i4Signal);
    UNUSED_PARAM (pfSignalHandler);
    return CLI_FAILURE;
#endif
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliEcfmUnRegisterSignalHandler                      *
 *                                                                         *
 *     Description   : The function gives the control, signal entered at   *
 *                     the CLI CONSOLE prompt.                             *
 *                                                                         *
 *     Input(s)      : i4Signal    : Signal Number                         *
 *                                                                         *
 *     Output(s)     : None                                                *
 *                                                                         *
 *     Returns       : None                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliEcfmUnRegisterSignalHandler (INT4 i4Signal)
{
#ifdef CLI_SIG_CAP
    SIGNAL (i4Signal, CliSignalHandler);
#else
    UNUSED_PARAM (i4Signal);
#endif
    return;
}

#endif

#ifdef BCM_SHELL_WANTED
void
bcmShell (void)
{
    taskPrioritySet (taskNameToId ("CLIC"), 249);
    gu1BcmCliEnb = 1;
}
#endif

/***************************************************************************
 *  FUNCTION NAME : CliSerialSelect
 *  DESCRIPTION   : This function is to wait for an input operation to occur 
 *                  in the CLI console
 *  INPUT         : NONE 
 *  OUTPUT        : NONE
 *  RETURNS       : Returns 1 if CLI console is ready for an input operation, 
 *                  -1 on failure
 ****************************************************************************/
INT4
CliSerialSelect (VOID)
{
    tCliContext        *pCliContext = NULL;
    fd_set              readfds;
    struct timeval      CliTimeval;

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return -1;
    }
    CliTimeval.tv_sec = 0;
    CliTimeval.tv_usec = CLI_MAX_WAIT_TIME_FOR_SELECT;

    FD_ZERO (&readfds);
    FD_SET (pCliContext->i4InputFd, &readfds);

    return select (1, &readfds, NULL, NULL, &CliTimeval);
}

/***************************************************************************
 *     Function Name : CliRestoreConfig                                    *
 *                                                                         *
 *     Description   : This function will restore configuration from saved *
 *                     comfiguration file                                  *
 *                                                                         *
 *     Input(s)      : NONE                                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 ***************************************************************************/
VOID
CliRestoreConfig (UINT1 *pu1RunScriptfile)
{
    UNUSED_PARAM (pu1RunScriptfile);
    return;
}
