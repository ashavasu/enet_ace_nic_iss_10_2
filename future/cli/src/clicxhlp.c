/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clicxhlp.c,v 1.41 2015/09/02 07:32:11 siva Exp $
 *
 * Description: This will have the context sensitive help related 
 * routines
 *              
 ******************************************************************************/

/*********************** HEADER FILES *****************************************/
#include "clicmds.h"

/************************ External variables **********************************/

extern tCliSessions gCliSessions;
extern t_MMI_TOKEN_TYPE mmi_cmd_token[];
extern tMemPoolId   gCliCtxHelpMemPoolId;
extern tMemPoolId   gCliMaxHelpStrMemPoolId;
extern tMemPoolId   gCliMaxLineMemPoolId;
static UINT4        gu4CliMaxAttempts = 0;

/******************************************************************************
 * FUNCTION NAME : CliCxHlpDisplayCxtHelp
 * DESCRIPTION   : This function is used to display context sensitive help
                   from CXT_HLP in def file.
 * INPUT         : Pointer to entered String
 * RETURNS       : NONE
 ******************************************************************************/

UINT4
CliCxHlpDisplayCxtHelp (INT1 *pi1Str, INT1 i1SpaceFlag, INT1 i1HelpCountFlag)
{
    tCliContext        *pCliContext = NULL;
    t_MMI_COMMAND_LIST *pCommand = NULL;
    t_MMI_HELP         *p_thelp_ptr = NULL;
    tCxtHelpParams      CxtHelpParams;
    UINT1             **ppu1HelpStr;
    INT2                i2Index = 0;
    INT2                i2SyntaxCheckFlag = 0;
    INT2                i2EnteredStrLen = 0;
    INT2                i2TotalCmds = 0;
    INT4                i4CheckFlag = 0;
    INT4                i4EnteredTok = 0;
    INT4                i4SameTokenFlag = 0;
    UINT1              *pu1TempHelpStr1 = NULL;
    UINT1              *pu1TempHelpStr2 = NULL;
    UINT1               u1SpcCntr = 0;    /* Counter to find matching cmd 
                                           again when the last token is 
                                           given is an incomplete token */
    UINT4               u4HelpCount = 0;    /*Counter to get num of matching help context */

    if (pi1Str == NULL)
    {
        return 0;
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        return 0;
    }

    pCliContext->pu1TempHelpStr1 =
        CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN + 1);

    if (pCliContext->pu1TempHelpStr1 == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for first Help String\n");
        return 0;
    }
    pu1TempHelpStr1 = pCliContext->pu1TempHelpStr1;
    
    pCliContext->pu1TempHelpStr2 =
        CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN + 1);

    if (pCliContext->pu1TempHelpStr2 == NULL)
    {
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        mmi_printf ("\r\n Cannot allocate memory for second Help String\n");
        return 0;
    }
    pu1TempHelpStr2 = pCliContext->pu1TempHelpStr2;

    MEMSET (&CxtHelpParams, 0, sizeof (tCxtHelpParams));
    CxtHelpParams.ppu1TknList = CliMemAllocMemBlk (gCliCtxHelpMemPoolId,
                                                   (MAX_NO_OF_TOKENS_IN_MMI *
                                                    CLI_MAX_TOKEN_VAL_MEM));

    if (CxtHelpParams.ppu1TknList == NULL)
    {
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        mmi_printf ("\r\n Cannot allocate memory \n");
        return 0;
    }
    pCliContext->pu1TknList = (UINT1 *) CxtHelpParams.ppu1TknList;

    i4EnteredTok =
        CliCxUtlSplitTokens (pi1Str,
                             (UINT1 (*)[CLI_MAX_TOKEN_VAL_MEM])
                             CxtHelpParams.ppu1TknList);

    CxtHelpParams.i2HelpStrIndex = 0;
    if (pCliContext->pMmiCur_mode != NULL)
    {
        p_thelp_ptr = pCliContext->pMmiCur_mode->pHelp;
        if (p_thelp_ptr == NULL)
        {
            CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
            CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
            CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
            return 0;
        }
    }
    else
    {
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }

    pCommand = pCliContext->pMmiCur_mode->pCommand_tree;

    if (pCommand == NULL)
    {
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }

    while (p_thelp_ptr != NULL)
    {
        i2TotalCmds += (p_thelp_ptr->i4ActNumEnd -
                        p_thelp_ptr->i4ActNumStart) + 1;
        p_thelp_ptr = p_thelp_ptr->pNext;
    }
    if (((i2TotalCmds * CLI_CXT_HELP_CHR_LEN) * sizeof (UINT1 *)) >
        CLI_MAX_HELP_BLOCK_SIZE)
    {
        mmi_printf ("\r\n Help String memory not enough \n");
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }

    ppu1HelpStr =
        CliMemAllocMemBlk (gCliMaxHelpStrMemPoolId, CLI_MAX_HELP_BLOCK_SIZE);

    if (ppu1HelpStr == NULL)
    {
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        mmi_printf ("\r\n%% Max simultaneous help sessions reached\n");
        return 0;
    }
    pCliContext->pu1HelpStr = (UINT1 *)ppu1HelpStr;
    if (i1HelpCountFlag == 0)
    {
        mmi_printf ("\r\n");
    }
    CxtHelpParams.ppu1HelpStr = ppu1HelpStr;
    CxtHelpParams.i2TotGrpCnt = CliCxUtlGetGrpCnt (pCommand);
    p_thelp_ptr = pCliContext->pMmiCur_mode->pHelp;
    CxtHelpParams.i4EnteredTok = i4EnteredTok;

    /* If just a ? is entered */
    if (i4EnteredTok == 0)
    {
        ++i4CheckFlag;
        /* First token of all the cmds in this mode will be printed */
        CliCxUtlGetFirstTknHelp (p_thelp_ptr, &CxtHelpParams, &i4CheckFlag);
        i1SpaceFlag = 1;
        /* Set the u1SpcCntr to 2 so that further matching cmd 
         * is not invoked */
        u1SpcCntr = 2;
    }
    else
    {
        i2EnteredStrLen = (INT2) CLI_STRLEN (pi1Str);
        if (pi1Str[i2EnteredStrLen - 1] == CLI_SPACE_CHAR)
        {
            u1SpcCntr = 1;
        }
    }

    if (i4CheckFlag == 1)
    {
        CxtHelpParams.i2HelpStrIndex = 1;
    }

    /* Invoke the find matching function when the i2HelpStrIndex is zero 
     * or once if the last input token is a complete token or twice
     * if the last token is an incomplete token 
     * eg if last token is token? find matching cmd invoked for token1 token2? and token1 ?
     * if the last token is token1 token2 ? then find matching cmd is invoked only for 
     * token1 token2 ? 
     * */
    while ((!(CxtHelpParams.i2HelpStrIndex)) || (u1SpcCntr < 2))
    {
        if (i2SyntaxCheckFlag == 1)
        {
            break;
        }
        u1SpcCntr++;

        /* Initialize constant cxt help params */
        CxtHelpParams.pu1Str = pi1Str;
        CxtHelpParams.i2GrpCnt = 1;
        CxtHelpParams.i1IsGrpCntChanged = OSIX_FALSE;
        CxtHelpParams.i1SpaceFlag = i1SpaceFlag;

        /* For each group, find the matching commands */
        CliCxUtlFindMatchInAllGrps (p_thelp_ptr, &CxtHelpParams,
                                    &i2SyntaxCheckFlag, i1SpaceFlag);
        /* Incase of wrong token entered at Nth position, truncate the 
         * tokens > N and continue with the remaining valid tokens
         * */

        while ((pi1Str) && ((i2EnteredStrLen = (INT2) CLI_STRLEN (pi1Str)) > 0))
        {
            pi1Str[i2EnteredStrLen - 1] = '\0';
            i2EnteredStrLen--;
            if (pi1Str[i2EnteredStrLen - 1] == CLI_SPACE_CHAR)
            {
                break;
            }
        }

        /* exit condition if only one token is entered */
        if (!i2EnteredStrLen)
        {
            break;
        }
    }
    /* if no matched cmd has cxt hlp */
    if ((CxtHelpParams.i2HelpStrIndex == 0) && (i2SyntaxCheckFlag == 1))
    {
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxHelpStrMemPoolId, &pCliContext->pu1HelpStr);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }
    /* if no matched cmd is found */
    if (CxtHelpParams.i2HelpStrIndex == 0)
    {
        if (i1HelpCountFlag == 0)
        {
            if (i2SyntaxCheckFlag == MMI_GEN_ERR_TOKEN_AMBIGUITY)
            {
                mmi_printf ("\r\n");
                mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_AMBIGUITY]);
            }
            else
            {
                mmi_printf ("\r\n");
                mmi_printf (mmi_gen_messages[MMI_GEN_ERR_INVALID_COMMAND]);
            }
        }

        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxHelpStrMemPoolId, &pCliContext->pu1HelpStr);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }
    if ((CxtHelpParams.i2HelpStrIndex == 1) && (i4CheckFlag == 1))
    {
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
        CliMemReleaseMemBlock (gCliMaxHelpStrMemPoolId, &pCliContext->pu1HelpStr);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
        return 0;
    }
    /* Sort the Cxt help strings */
    CliCxUtlSortCxtHelpStrings (CxtHelpParams.ppu1HelpStr,
                                CxtHelpParams.i2HelpStrIndex);
    /* Print sorted Context help strings */
    for (i2Index = 0; i2Index < CxtHelpParams.i2HelpStrIndex; i2Index++)
    {
        if (CxtHelpParams.ppu1HelpStr[i2Index] == NULL)
        {
            break;
        }

        if (i2Index != 0)
        {
            CliCxUtlGetFirstWord (CxtHelpParams.ppu1HelpStr[i2Index],
                                  pu1TempHelpStr1);
            CliCxUtlGetFirstWord (CxtHelpParams.ppu1HelpStr[i2Index -
                                                            1],
                                  pu1TempHelpStr2);
            if (STRCMP (pu1TempHelpStr1, pu1TempHelpStr2) == 0)
            {
                if (STRCMP (pu1TempHelpStr1, "<CR>") == 0)
                {
                    if (STRCMP (CxtHelpParams.ppu1HelpStr[i2Index],
                                CxtHelpParams.ppu1HelpStr[i2Index - 1]) == 0)
                    {
                        continue;
                    }
                    else
                    {
                        if (i1HelpCountFlag)
                        {
                            u4HelpCount++;
                        }
                        else
                        {
                            CliCxUtlPrintTknWithHlpString (ppu1HelpStr
                                                           [i2Index]);
                            if (pCliContext->mmi_exit_flag == TRUE)
                            {
                                break;
                            }
                        }
                    }
                }
                i4SameTokenFlag = 1;
                continue;
            }
            else if ((i4SameTokenFlag == 1) &&
                     ((i1SpaceFlag == 0) && (i1HelpCountFlag == 1)))
            {
                continue;
            }

        }
        if (i1SpaceFlag == 0)
        {
            if (i1HelpCountFlag)
            {
                u4HelpCount++;
            }
            else
            {
                CliCxUtlPrintTkn (ppu1HelpStr[i2Index]);
                if (pCliContext->mmi_exit_flag == TRUE)
                {
                    break;
                }
            }
        }
        else
        {
            if (i1HelpCountFlag)
            {
                u4HelpCount++;
            }
            else
            {
                CliCxUtlPrintTknWithHlpString (ppu1HelpStr[i2Index]);
                if (pCliContext->mmi_exit_flag == TRUE)
                {
                    break;
                }
            }
        }
    }
    if (i1HelpCountFlag == 0)
    {
        mmi_printf ("\r\n");
        mmi_printf ("\n");
    }
    CliMemReleaseMemBlock (gCliCtxHelpMemPoolId, &pCliContext->pu1TknList);
    CliMemReleaseMemBlock (gCliMaxHelpStrMemPoolId, &pCliContext->pu1HelpStr);
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr1);
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempHelpStr2);
    gu4CliMaxAttempts = 0;
    return u4HelpCount;
}

/******************************************************************************
 * FUNCTION NAME : CliCxHlpGetGrpFrmCmdStruct
 * DESCRIPTION   : This function is used to find the required group in
 *                 the command tree structure corresponding to the
 *                 matched syntax
 * INPUT         : Pointer to the cmd tree
 *                 current group count
 *                 total group count
 * RETURNS       : returns pointer to the required group
 ******************************************************************************/
PUBLIC
    t_MMI_COMMAND_LIST * CliCxHlpGetGrpFrmCmdStruct (t_MMI_COMMAND_LIST *
                                                     pTmpCmd, INT2 i2CurGrpCnt,
                                                     INT2 i2TotGrp)
{
    INT2                i2Check = 0;

    i2Check = i2CurGrpCnt;

    if (i2CurGrpCnt == i2TotGrp)
    {
        while (i2Check != 1)
        {
            if (pTmpCmd)
            {
                pTmpCmd = pTmpCmd->pNext;
            }
            --i2Check;
        }
    }
    else
    {
        while (i2Check != 0)
        {
            if (pTmpCmd)
            {
                pTmpCmd = pTmpCmd->pNext;
            }
            --i2Check;
        }
    }

    return (pTmpCmd);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpFindNxtTknAftrParsing
 * DESCRIPTION   : This function is to find the next token( optional/nxtcmd ) 
 *                 after parsing.
 * INPUT         : pointer to the tkn up to which parsing has been done 
 *                 successfully
 *                 Pointer to Cxthelp parameters
 *                 Address of pointers where the next token's address will be 
 *                 saved
 *                 pointer to the array containing optional tokens and its 
 *                 count
 *                 Flag referring to presence of inner node in recurence loop
 *                 Flag refering to whether space is there before question mark                
 *                 or not
 * RETURNS       : returns the variable which is used to keep track of array 
 *                 containing next possible tokens
 ******************************************************************************/
PUBLIC INT2
CliCxHlpFindNxtTknAftrParsing (t_MMI_CMD * pRetToken,
                               tCxtHelpParams * pCxtHelpParams,
                               t_MMI_CMD ** ppNextTkn,
                               t_MMI_CMD ** ppNextOfNextTkn,
                               t_MMI_CMD ** ppRecurToken,
                               INT4 *pi4OptionalTokens,
                               INT4 i4RecurFlag,
                               INT4 i4InnerRecurCmpltFlag,
                               INT2 i2ArrCnt, INT1 i1SpaceFlag)
{
    UINT1              *pu1TempString;
    t_MMI_CMD          *tmp_ptr;
    INT4                i4ConflictTok = 0;
    pu1TempString = (UINT1 *) pCxtHelpParams->ppu1TknList +
        (CLI_MAX_TOKEN_VAL_MEM * (pCxtHelpParams->i4EnteredTok - 1));

    /* Block to detect the ambiguous optional tokens */
    if (i1SpaceFlag == 1)
    {
        tmp_ptr = pRetToken;
        do
        {
            if (tmp_ptr != NULL)
            {
                if (mmi_strcmpi ((CONST INT1 *) tmp_ptr->i1pToken,
                                 (CONST INT1 *) pu1TempString) == 0)
                {
                    i4ConflictTok++;
                    break;
                }
                else if (mmi_strncmpi ((CONST INT1 *) tmp_ptr->i1pToken,
                                       (CONST INT1 *) pu1TempString,
                                       CLI_STRLEN (pu1TempString)) == 0)
                {
                    i4ConflictTok++;
                }
                tmp_ptr = tmp_ptr->pOptional;
            }
        }
        while ((tmp_ptr != NULL) && (tmp_ptr != pRetToken));

        if (i4ConflictTok > 1)
        {
            return (-1);
        }
    }
    /* if recur loop does not have inner node */
    if (i4InnerRecurCmpltFlag == 0)
    {
        if ((i4RecurFlag == 1) && (i1SpaceFlag == 1))
        {
            *ppNextTkn = pRetToken;
            return (0);
        }
    }
    else
    {
        if ((i4RecurFlag == 1) && (i1SpaceFlag == 1))
        {
            /* if recur loop has inner node */
            *ppNextOfNextTkn = pRetToken;
            return (0);
        }
    }
    /* if space has been given after last entered keyword */
    if (i1SpaceFlag == 1)
    {
        /* if last parsed token has next token use it as ppNextTkn whose 
         * optional tokens will be checked for the next possible options*/
        if (pRetToken->pNext_token)
        {
            if ((pRetToken->pNext_token)->i1pToken)
            {
                /* if last parsed token is the starting of recur loop */
                if (!STRCMP ((pRetToken->pNext_token)->i1pToken, "recur"))
                {
                    *ppNextTkn = (pRetToken->pNext_token)->pNext_command;
                    *ppRecurToken = pRetToken->pNext_token;
                }
                else
                {
                    *ppNextTkn = pRetToken->pNext_token;
                }
            }
            else
            {
                *ppNextTkn = pRetToken->pNext_token;
            }
        }
        else if (pRetToken->pOptional)
        {
            /* if last parsed token has optional token use it as ppNextTkn whose optional 
             * tokens will be checked for the next possible options*/
            if ((pRetToken->pOptional)->i1pToken)
            {
                /* if last parsed token is the starting of recur loop */
                if (!STRCMP ((pRetToken->pOptional)->i1pToken, "recur"))
                {
                    *ppNextTkn = (pRetToken->pOptional)->pNext_command;
                    *ppRecurToken = pRetToken->pOptional;
                }
                else
                {
                    *ppNextTkn = pRetToken->pNext_token;
                }
            }
            else
            {
                *ppNextTkn = pRetToken->pOptional;
            }
        }
        /* if ppNextTkn has next command use it as ppNextOfNextTkn whose next command
         * tokens will be checked for the next possible options*/
        if ((*ppNextTkn) && (*ppNextTkn)->pNext_command)
        {
            *ppNextOfNextTkn = (*ppNextTkn)->pNext_command;
        }
        else
        {
            *ppNextOfNextTkn = NULL;
        }
    }
    else
    {
        /* if no space has been given after last entered Non-keyword */
        *ppNextTkn = pRetToken;
        if ((*ppNextTkn)->pNext_command)
        {
            *ppNextOfNextTkn = (*ppNextTkn)->pNext_command;
        }
        else
        {
            *ppNextOfNextTkn = NULL;
        }
        /* To find all optional token */
        CliCxHlpFindMatchingOptionalTkns (*ppNextTkn,
                                          pu1TempString,
                                          pi4OptionalTokens,
                                          &i2ArrCnt, i4RecurFlag, 0);
        /* To find all tokens in the same level */
        CliCxHlpFindMatchingNxtCmdTkns (*ppNextOfNextTkn,
                                        pu1TempString,
                                        pi4OptionalTokens,
                                        &i2ArrCnt, i4RecurFlag, 0);
        /* if entire Nonkeyword has not been entered */
        *ppNextTkn = NULL;
        *ppNextOfNextTkn = NULL;
    }
    return (i2ArrCnt);
}

/******************************************************************************
 * FUNCTION NAME : CliCxHlpParseCxtHlpString
 * DESCRIPTION   : This function is used to find diff options from cxt help
 *                 of the command got from corresponding matched cmd syntax
 * INPUT         : Pointer to structure containing cxt hlp parameters
 *                 Pointer to array containing nxt optional tokens
 *                 Variable containing Array count 
 * RETURNS       : None
 ******************************************************************************/
PUBLIC VOID
CliCxHlpParseCxtHlpString (tCxtHelpParams * pCxtHelpParams,
                           INT4 *pi4OptionalTokens, INT2 i2ActIndArrCnt)
{
    UINT1              *pu1TempHelpStr;
    INT2                i2ArrDescriptor = 0;
    INT4                i4SpaceCnt = 0;
    INT4                i4CxtHelpCheck = 0;
    UINT1               au1TempHelpStr[CLI_MAX_TOKEN_VAL_MEM];
    UINT4               u4Count = 0;

    MEMSET (au1TempHelpStr, 0, CLI_MAX_TOKEN_VAL_MEM);
    for (i2ArrDescriptor = 0;
         i2ArrDescriptor < i2ActIndArrCnt; i2ArrDescriptor++)
    {
        MEMCPY (&pu1TempHelpStr,
                &pCxtHelpParams->p_thelp_ptr->
                pCxtHelp[pCxtHelpParams->i2Index], sizeof (UINT1 *));
        /* handling tokens other than Carriage return */
        if (pi4OptionalTokens[i2ArrDescriptor] != CLI_ACTINDEX_VAL)
        {
            i4CxtHelpCheck = pi4OptionalTokens[i2ArrDescriptor] + 1;
            /* parse to the corresponding token in cxt help string */
            while (i4CxtHelpCheck != 1)
            {
                u4Count++;
                if (u4Count > CLI_MAX_SESSION_MEMORY)
                {
                    u4Count = 0;
                    break;
                }
                if (*(pu1TempHelpStr) == '|')
                {
                    ++pu1TempHelpStr;
                    --i4CxtHelpCheck;
                }
                else
                {
                    ++pu1TempHelpStr;
                }
            }
            while ((*(pu1TempHelpStr) == ' ') ||
                   (*(pu1TempHelpStr) == '\n') || (*(pu1TempHelpStr) == '\r'))
            {
                u4Count++;
                if (u4Count > CLI_MAX_SESSION_MEMORY)
                {
                    u4Count = 0;
                    break;
                }

                ++pu1TempHelpStr;
            }
            if (STRNCMP (pu1TempHelpStr, "DYN", 3) == 0)
            {
                CliCxUtlGetFirstWord (pu1TempHelpStr, au1TempHelpStr);
                CliCxDynGetDynamicHelp (pCxtHelpParams, au1TempHelpStr);
            }
            else
            {
                pCxtHelpParams->
                    ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
                    pu1TempHelpStr;
                ++pCxtHelpParams->i2HelpStrIndex;
            }
        }
        else                    /* handling carriage return token */
        {
            i4SpaceCnt = STRLEN (pu1TempHelpStr) - 1;
            if (i4SpaceCnt >= CLI_MAX_SESSION_MEMORY)
            {
                u4Count = 0;
                break;
            }
            /* parse to the last token in cxt help string */
            while (pu1TempHelpStr[i4SpaceCnt] != '|')
            {
                if (i4SpaceCnt < 0)
                {
                    break;
                }
                i4SpaceCnt--;
            }
            if (i4SpaceCnt < 0)
            {
                continue;
            }
            ++i4SpaceCnt;
            if (i4SpaceCnt >= CLI_MAX_SESSION_MEMORY)
            {
                u4Count = 0;
                break;
            }
            while ((pu1TempHelpStr[i4SpaceCnt] == ' ') ||
                   (pu1TempHelpStr[i4SpaceCnt] == '\n') ||
                   (pu1TempHelpStr[i4SpaceCnt] == '\r'))
            {
                if (i4SpaceCnt == CLI_MAX_SESSION_MEMORY - 1)
                {
                    u4Count = 0;
                    break;
                }
                i4SpaceCnt++;
            }
            pu1TempHelpStr = pu1TempHelpStr + i4SpaceCnt;
            pCxtHelpParams->
                ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] = pu1TempHelpStr;
            ++pCxtHelpParams->i2HelpStrIndex;
        }

        if (u4Count == 0)
        {
            break;
        }
        u4Count = 1;
    }
    return;
}

/******************************************************************************
 * FUNCTION NAME : CliCxHlpParseEnteredCmd
 * DESCRIPTION   : This function is used to parse the user entered command.
 * INPUT         : pointer to the matched command
 *                 Address of the last token that has been parsed successfully
 *                 Address of the starting token of the recurrence loop
 *                 Pointer to store the number of nodes in recur loop
 *                 Flag to find if last entered node is a mamber of recur loop
 *                 Flag to find if space is left btwn last token and "?"
 *                 Address of array where the unentered recur loop tokens are stored 
 * RETURNS       : zero if not even one token is matched
 *                 else the number of tokens matched
 ******************************************************************************/
PUBLIC INT4
CliCxHlpParseEnteredCmd (t_MMI_CMD * pParseCmd,
                         tCxtHelpParams * pCxtHlpParams,
                         t_MMI_CMD ** paRecurNodes,
                         t_MMI_CMD ** pRetAddr,
                         t_MMI_CMD ** ppRecurToken, INT4 *pi4RecurFlag,
                         INT4 *pi4RecurLoopCount,
                         INT4 *pi4InnerRecurCmpltFlag, INT1 i1SpaceFlag)
{
    t_MMI_CMD          *pToken = NULL;
    t_MMI_CMD          *pSameLevel = NULL;
    t_MMI_CMD          *pRecurTkn = NULL;
    t_MMI_CMD          *pRetToken = NULL;
    INT4                i4RecurFlag = 0;
    INT4                i4RetVal = 0;
    INT4                i4TknCnt = 0;
    INT4                i4TempMatchCnt = 0;

    pToken = pParseCmd;
    pSameLevel = pParseCmd;

    while (i4TknCnt < pCxtHlpParams->i4EnteredTok)
    {
        i4RecurFlag = 0;
        /* not present within next cmd tokens */
        i4RetVal = CliCxHlpParseNxtCmdTkns (pSameLevel,
                                            (UINT1 (*)[CLI_MAX_TOKEN_VAL_MEM])
                                            pCxtHlpParams->ppu1TknList,
                                            &pRetToken, i4TknCnt);
        if (gu4CliMaxAttempts == 0)
        {
            break;
        }
        if (i4RetVal == 0)
        {
            /* not present within next optional and its next cmd tokens */
            i4RetVal = CliCxHlpParseOptionalTkns (pToken,
                                                  (UINT1 (*)
                                                   [CLI_MAX_TOKEN_VAL_MEM])
                                                  pCxtHlpParams->ppu1TknList,
                                                  &pRetToken, i4TknCnt);
            if (i4RetVal == 0)
            {
                break;
            }
            else
            {
                if (i4RetVal != CLI_ACTINDEX_VAL)
                    ++i4TknCnt;
                else
                {
                    pRecurTkn = pRetToken;
                }

            }
        }
        else
        {
            if (i4RetVal != CLI_ACTINDEX_VAL)
                ++i4TknCnt;
            else
            {
                pRecurTkn = pRetToken;
            }
        }
        /* if last parsed token is not the starting of a recur loop */
        if (i4RetVal != CLI_ACTINDEX_VAL)
        {
            /* To find the next token to be parsed in the command structure */
            if ((pSameLevel == pRetToken) || (pToken == pRetToken))
            {
                pToken = pToken->pNext_token;
                pSameLevel = pSameLevel->pNext_token;
            }
            else
            {
                pToken = pRetToken->pNext_token;
                pSameLevel = pRetToken->pNext_token;
            }
        }
        else
        {
            i4TempMatchCnt =
                CliCxHlpHandleRecurLoop (pCxtHlpParams, paRecurNodes,
                                         &pRecurTkn, &pRetToken, &pSameLevel,
                                         &pToken, &i4RecurFlag, &i4TknCnt,
                                         pi4RecurLoopCount,
                                         pi4InnerRecurCmpltFlag, i1SpaceFlag);
            if (i4TempMatchCnt == CLI_ACTINDEX_VAL)
            {
                break;
            }
        }
    }
    *pi4RecurFlag = i4RecurFlag;
    *ppRecurToken = pRecurTkn;
    *pRetAddr = pRetToken;
    return (i4TknCnt);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpFindNxtCmdTkns
 * DESCRIPTION   : This function is used to find optional tokens within each
 *                 of next command tokens.
 * INPUT         : pointer to the token
 *                 Array where next possible options will be atored and it's count
 *                 Flag representing recurence loop
 *                 No of tokens in recur loop
 *                 Flag to show if space is there between last token and "?"
 *                 Array containing recur loop nodes
 *                 Address of last sucessfully parsed token
 *                 Address of array containing active index values of matched
 *                 tokens and its count.
 * RETURNS       : NONE
 ******************************************************************************/
PUBLIC VOID
CliCxHlpFindNxtCmdTkns (t_MMI_CMD * pNextCmd,
                        t_MMI_CMD * pRecurToken,
                        t_MMI_CMD ** paRecurNodes,
                        INT4 *pi4TempArray1, INT2 *pi2ArrCnt,
                        INT4 i4RecurFlag, INT4 i4RecurLoopCount,
                        INT1 i1SpaceFlag)
{
    t_MMI_CMD          *pOptNxt = NULL;
    INT2                i2TempCnt = 0;
    i2TempCnt = *pi2ArrCnt;

    while (pNextCmd)
    {
        if (pNextCmd->pOptional && (pNextCmd->pOptional != pRecurToken))
        {
            pOptNxt = pNextCmd->pOptional;
            CliCxHlpFindOptionalTkns (pOptNxt, pRecurToken, paRecurNodes,
                                      pi4TempArray1, &i2TempCnt,
                                      i4RecurFlag, i4RecurLoopCount,
                                      i1SpaceFlag);
        }
        /* Handling keyword type tokens */
        if ((FSAP_U8_FETCH_LO (&(pNextCmd->u8Typeof_token)) == 0) &&
            (FSAP_U8_FETCH_HI (&(pNextCmd->u8Typeof_token)) == 0))
        {
            if (!STRCMP (pNextCmd->i1pToken, "ret"))
            {
                if (i2TempCnt >= CLI_OPTNLTKNS)
                {
                    return;
                }
                pi4TempArray1[i2TempCnt] = CLI_ACTINDEX_VAL;
                ++i2TempCnt;
                break;
            }
            else
            {
                /* if the token is starting of recur loop */
                if (!STRCMP (pNextCmd->i1pToken, "recur"))
                {
                    pNextCmd = pNextCmd->pNext_command;
                }
                else
                {
                    if (i2TempCnt >= CLI_OPTNLTKNS)
                    {
                        return;
                    }
                    pi4TempArray1[i2TempCnt] = pNextCmd->i4Action_number;
                    pNextCmd = pNextCmd->pNext_command;
                    ++i2TempCnt;
                }
            }
        }
        else                    /* Handling Nonkeyword type tokens */
        {
            if (i2TempCnt >= CLI_OPTNLTKNS)
            {
                return;
            }
            pi4TempArray1[i2TempCnt] = pNextCmd->i4Action_number;
            pNextCmd = pNextCmd->pNext_command;
            ++i2TempCnt;
        }
    }
    *pi2ArrCnt = i2TempCnt;
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpFindOptionalTkns
 * DESCRIPTION   : This function is used to find optional tokens within each
                   of optional tokens and their next command tokens.
 * INPUT         : pointer to the token
 *                 Array where next possible options will be atored and it's count
 *                 Flag representing recurence loop
 *                 No of tokens in recur loop
 *                 Flag to show if space is there between last token and "?"
 *                 Array containing recur loop nodes
 *                 Address of last sucessfully parsed token
 *                 Address of array containing active index values of matched
 *                 tokens and its count.
 * RETURNS       : NONE
 ******************************************************************************/
PUBLIC VOID
CliCxHlpFindOptionalTkns (t_MMI_CMD * pOptToken,
                          t_MMI_CMD * pRecurToken,
                          t_MMI_CMD ** paRecurNodes,
                          INT4 *pi4TempArray2, INT2 *pi2ArrCnt,
                          INT4 i4RecurFlag, INT4 i4RecurLoopCount,
                          INT1 i1SpaceFlag)
{

    t_MMI_CMD          *pOptNxt = NULL;
    INT2                i2TempCnt = 0;
    INT2                i2FoundRecur = 0;
    INT2                i2LoopCount = 0;
    i2TempCnt = *pi2ArrCnt;
    if (pOptToken == NULL)
    {
        return;
    }
    /* Finding the next possible tokens within the recur loop */
    if (i4RecurFlag == 1 && i1SpaceFlag == 1)
    {
        for (i2LoopCount = 0; i2LoopCount < i4RecurLoopCount; ++i2LoopCount)
        {
            pOptNxt = paRecurNodes[i2LoopCount];
            if (pOptNxt != NULL)
            {
                if (i2TempCnt >= CLI_OPTNLTKNS)
                {
                    return;
                }
                pi4TempArray2[i2TempCnt] = pOptNxt->i4Action_number;
                ++i2TempCnt;
            }
        }
        *pi2ArrCnt = i2TempCnt;
        return;
    }
    if (pRecurToken != NULL)
    {
        pRecurToken = pOptToken;
    }
    while (pOptToken != NULL)
    {
        /*Handling Keyword type tokens */
        if (((FSAP_U8_FETCH_LO (&(pOptToken->u8Typeof_token)) == 0) &&
             (FSAP_U8_FETCH_HI (&(pOptToken->u8Typeof_token)) == 0)) &&
            (pOptToken->i1pToken))
        {
            if (STRCMP (pOptToken->i1pToken, "ret") == 0)
            {
                if (i2TempCnt >= CLI_OPTNLTKNS)
                {
                    return;
                }
                pi4TempArray2[i2TempCnt] = CLI_ACTINDEX_VAL;
                ++i2TempCnt;
                break;
            }
            else
            {
                /* if the token is starting of recur loop */
                if (STRCMP (pOptToken->i1pToken, "recur") == 0)
                {
                    if (i2FoundRecur == 1)
                    {
                        break;
                    }
                    pOptToken = pOptToken->pNext_command;
                }
                else
                {
                    if (i2TempCnt >= CLI_OPTNLTKNS)
                    {
                        return;
                    }
                    pi4TempArray2[i2TempCnt] = pOptToken->i4Action_number;
                    pOptToken = pOptToken->pOptional;
                    ++i2TempCnt;
                }
            }
        }
        else                    /*Handling NonKeyword type tokens */
        {
            if (i2TempCnt >= CLI_OPTNLTKNS)
            {
                return;
            }
            pi4TempArray2[i2TempCnt] = pOptToken->i4Action_number;
            pOptToken = pOptToken->pOptional;
            ++i2TempCnt;
        }
        if (pOptToken)
        {
            if (pOptToken == pRecurToken)
            {
                break;
            }
            if (pOptToken->i1pToken)
            {
                if (STRCMP (pOptToken->i1pToken, "recur") == 0)
                {
                    pRecurToken = pOptToken;
                    i2FoundRecur = 1;
                }
            }
            if (pOptToken->pNext_command)
            {
                pOptNxt = pOptToken->pNext_command;

                CliCxHlpFindNxtCmdTkns (pOptNxt, pRecurToken,
                                        paRecurNodes, pi4TempArray2,
                                        &i2TempCnt, i4RecurFlag,
                                        i4RecurLoopCount, i1SpaceFlag);
            }
        }
    }
    *pi2ArrCnt = i2TempCnt;
    return;

}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpParseOptionalTkns
 * DESCRIPTION   : This function is used to parse within optional tokens
                   and their nxt cmd tokens
 * INPUT         : pointer to the token
                   token count
                   Array of pointers containing entered tokens
                   pointer to store address upto which parsing has
                   been done successfully.
 * RETURNS       : returns non zero value on SUCCESS
 *                 returns 0 on FAILURE
 ******************************************************************************/

PUBLIC INT4
CliCxHlpParseOptionalTkns (t_MMI_CMD * pCurrToken,
                           UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM],
                           t_MMI_CMD ** ppLastTkn, INT4 i4Cnt)
{
    t_MMI_CMD          *pLastTkncmd = NULL;
    t_MMI_CMD          *pNextCmd = NULL;
    INT4                i4RetVal = 0;
    INT2                i2NonKeyRet = 0;
    INT1                i1Prev = 0;
    INT1               *pi1Token;
    UINT1               au1TempKeyWord[CLI_MAX_TOKEN_VAL_MEM];

    if (gu4CliMaxAttempts == CLI_OPTNLTKNS)
    {
        gu4CliMaxAttempts = 0;
        return 0;
    }
    gu4CliMaxAttempts++;

    while (pCurrToken)
    {
        /* Parsing Keyword type tokens */
        if ((FSAP_U8_FETCH_LO (&(pCurrToken->u8Typeof_token)) == 0) &&
            (FSAP_U8_FETCH_HI (&(pCurrToken->u8Typeof_token)) == 0))
        {
            if (STRCMP (pCurrToken->i1pToken, "ret") == 0)
            {
                if (pCurrToken->pOptional == NULL)
                {
                    i4RetVal = 0;
                    return (i4RetVal);
                }
                else
                {
                    pCurrToken = pCurrToken->pOptional;
                    i1Prev = i4RetVal;
                    continue;
                }
            }
            /* when parsed token is recur keyword token */
            if (STRCMP (pCurrToken->i1pToken, "recur") == 0)
            {
                i4RetVal = CLI_ACTINDEX_VAL;
                *ppLastTkn = pCurrToken;
                return (i4RetVal);
            }

            CliCxUtlMakeLowerCaseToCmp (au1TknList[i4Cnt],
                                        pCurrToken->i1pToken, au1TempKeyWord);

            if (!STRNCMP (au1TknList[i4Cnt], au1TempKeyWord,
                          STRLEN (au1TknList[i4Cnt])))
            {
                ++i4RetVal;

            }
            else
            {
                if (!pCurrToken->pOptional)
                {
                    i4RetVal = 0;
                    return (i4RetVal);
                }
            }
        }
        i2NonKeyRet = CliCxUtlGetBitMaskPosition (pCurrToken->u8Typeof_token);

        if (i2NonKeyRet)
        {
            /* Parsing NonKeyword type tokens */
            pi1Token = (INT1 *) au1TknList[i4Cnt];

            if (mmi_cmd_token[i2NonKeyRet - 1].type_check (pi1Token) == TRUE)
            {
                ++i4RetVal;
            }
            else
            {
                if (!pCurrToken->pOptional)
                {
                    i4RetVal = 0;
                    return (i4RetVal);
                }
            }
        }
        /* if a matching keyword/Nonkeyword is found */
        if (i4RetVal != i1Prev)
        {
            *ppLastTkn = pCurrToken;
            return (i4RetVal);
        }
        else
        {
            if (pCurrToken->pOptional)
            {
                /* check within next commands of optional tokens */
                pCurrToken = pCurrToken->pOptional;
                if (pCurrToken)
                {
                    i4RetVal = CliCxHlpParseNxtCmdTkns
                        (pCurrToken, au1TknList, &pLastTkncmd, i4Cnt);
                    if (gu4CliMaxAttempts == 0)
                    {
                        return 0;
                    }
                    if (i4RetVal != 0)
                    {
                        *ppLastTkn = pLastTkncmd;
                        return (i4RetVal);
                    }
                }

                pNextCmd = pCurrToken->pNext_command;
                if (pNextCmd)
                {
                    i4RetVal = CliCxHlpParseNxtCmdTkns
                        (pNextCmd, au1TknList, &pLastTkncmd, i4Cnt);
                    if (gu4CliMaxAttempts == 0)
                    {
                        return 0;
                    }
                    if (i4RetVal != 0)
                    {
                        *ppLastTkn = pLastTkncmd;
                        return (i4RetVal);
                    }
                }
            }
            else
            {
                i4RetVal = 0;
                return (i4RetVal);
            }
        }
    }
    return (i4RetVal);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpParseNxtCmdTkns
 * DESCRIPTION   : This function is used to parse within next cmd tokens
 * INPUT         : pointer to the token
                   token count
                   Array of pointers containing entered tokens
                   pointer to store the address upto which parsing has
                   been done successfully.
 * RETURNS       : returns non zero value on SUCCESS
 *                 returns 0 on FAILURE
 ******************************************************************************/

PUBLIC INT4
CliCxHlpParseNxtCmdTkns (t_MMI_CMD * pCurrToken,
                         UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM],
                         t_MMI_CMD ** ppLastTkn, INT4 i4Cnt)
{

    INT4                i4RetVal = 0;
    INT2                i2NonKeyRet = 0;
    INT1                i1Prev = 0;
    INT1               *pi1Token;
    UINT1               au1TempKeyWord[CLI_MAX_TOKEN_VAL_MEM];

    if (gu4CliMaxAttempts == CLI_OPTNLTKNS)
    {
        gu4CliMaxAttempts = 0;
        return 0;
    }
    gu4CliMaxAttempts++;

    while (pCurrToken)
    {
        i1Prev = i4RetVal;
        /* Parsing Keyword type tokens */
        if ((FSAP_U8_FETCH_LO (&(pCurrToken->u8Typeof_token)) == 0) &&
            (FSAP_U8_FETCH_HI (&(pCurrToken->u8Typeof_token)) == 0))
        {
            /* if parsed token is recur keyword token */
            if (STRCMP (pCurrToken->i1pToken, "recur") == 0)
            {
                i4RetVal = CLI_ACTINDEX_VAL;
                *ppLastTkn = pCurrToken;
                return (i4RetVal);
            }
            CliCxUtlMakeLowerCaseToCmp (au1TknList[i4Cnt],
                                        pCurrToken->i1pToken, au1TempKeyWord);

            if (!STRNCMP (au1TknList[i4Cnt],
                          au1TempKeyWord, STRLEN (au1TknList[i4Cnt])))
            {
                ++i4RetVal;
            }
            else
            {
                if (!pCurrToken->pNext_command)
                {
                    i4RetVal = 0;
                    return (i4RetVal);
                }
            }
        }
        i2NonKeyRet = CliCxUtlGetBitMaskPosition (pCurrToken->u8Typeof_token);
        /* Parsing Nonkeyword type tokens */
        if (i2NonKeyRet)
        {
            pi1Token = (INT1 *) au1TknList[i4Cnt];
            if (mmi_cmd_token[i2NonKeyRet - 1].type_check (pi1Token) == TRUE)
            {
                ++i4RetVal;
            }
            else
            {
                if (!pCurrToken->pNext_command)
                {
                    i4RetVal = 0;
                    return (i4RetVal);
                }
            }

        }
        /* if a matching keyword/Nonkeyword is found */
        if (i4RetVal != i1Prev)
        {
            *ppLastTkn = pCurrToken;
            return (i4RetVal);
        }
        else
        {
            pCurrToken = pCurrToken->pNext_command;
            if (pCurrToken)
            {
                if (i4Cnt == 0)
                {
                    return (i4Cnt);
                }
                else
                {
                    continue;
                }
            }
            else
            {
                i4RetVal = 0;
                return (i4RetVal);
            }
        }
    }
    return (i4RetVal);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpFindMatchingOptionalTkns
 * DESCRIPTION   : This function is used to find optional tokens within each
                   of optional tokens and their next command tokens for the 
                   current token.
 * INPUT         : pointer to the token
 *                 pointer to the string to be compared
 *                 Address of array containing active index values of matched
 *                 Flag showing if any entered token is from recur loop
 *                 tokens and its count.
 * RETURNS       : NONE
 ******************************************************************************/
PUBLIC VOID
CliCxHlpFindMatchingOptionalTkns (t_MMI_CMD * pOptToken,
                                  UINT1 *pu1TokenToCompare,
                                  INT4 *pi4TempArray2,
                                  INT2 *pi2ArrCnt, INT4 i4RecurFlag,
                                  INT2 i2Loop)
{
    t_MMI_CMD          *pOptNxt = NULL;
    t_MMI_CMD          *pOptNxtTmp = NULL;
    t_MMI_CMD          *pRecurNode = NULL;
    INT2                i2TempCnt = 0;
    INT2                i2NonKeyRet = 0;
    UINT1               au1TempKeyWord[CLI_MAX_TOKEN_VAL_MEM];
    INT1               *pi1Token;
    INT1                i1ChkFlag = FALSE;

    i2TempCnt = *pi2ArrCnt;
    if (i4RecurFlag == 1)
    {
        pRecurNode = pOptToken;
    }
    while ((pOptToken != NULL) && (i2Loop < CLI_OPTNLTKNS))
    {
        i2Loop++;
        /*Handling Keyword type tokens */
        if (pOptToken->i1pToken)
        {
            if (!STRCMP (pOptToken->i1pToken, "ret"))
            {
                break;
            }
            CliCxUtlMakeLowerCaseToCmp (pu1TokenToCompare, pOptToken->i1pToken,
                                        au1TempKeyWord);
            if (!STRNCMP (pu1TokenToCompare,
                          au1TempKeyWord, STRLEN (pu1TokenToCompare)))
            {
                pi4TempArray2[i2TempCnt] = pOptToken->i4Action_number;
                if (i2TempCnt == CLI_OPTNLTKNS - 1)
                {
                    return;
                }
                pOptToken = pOptToken->pOptional;
                ++i2TempCnt;
            }
            else
            {
                pOptToken = pOptToken->pOptional;
            }
        }
        else
        {
            /* Handling nonkeyword type tokens */
            i2NonKeyRet =
                CliCxUtlGetBitMaskPosition (pOptToken->u8Typeof_token);
            if (i2NonKeyRet)
            {
                pi1Token = (INT1 *) pu1TokenToCompare;
                if (mmi_cmd_token[i2NonKeyRet - 1].type_check (pi1Token)
                    == TRUE)
                {
                    pi4TempArray2[i2TempCnt] = pOptToken->i4Action_number;
                    if (i2TempCnt == CLI_OPTNLTKNS - 1)
                    {
                        return;
                    }
                    pOptToken = pOptToken->pOptional;
                    ++i2TempCnt;
                }
                else
                {
                    pOptToken = pOptToken->pOptional;
                }
            }
        }

        if (pOptToken)
        {
            if (pOptToken->pNext_command)
            {
                pOptNxt = pOptToken->pNext_command;
                CliCxHlpFindMatchingNxtCmdTkns (pOptNxt,
                                                pu1TokenToCompare,
                                                pi4TempArray2,
                                                &i2TempCnt, i4RecurFlag,
                                                i2Loop + 1);
            }
        }
        if (pOptToken == pRecurNode)
        {
            break;
        }
        else
        {
            pOptNxtTmp = pOptToken;
            while (pOptNxtTmp != NULL)
            {
                if (pOptNxtTmp == pRecurNode)
                {
                    i1ChkFlag = TRUE;
                    break;
                }
                pOptNxtTmp = pOptNxtTmp->pNext_command;
            }
            pOptNxtTmp = NULL;
            if (i1ChkFlag == TRUE)
            {
                break;
            }
        }

    }
    *pi2ArrCnt = i2TempCnt;
    return;

}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpFindMatchingNxtCmdTkns
 * DESCRIPTION   : This function is used to find optional tokens within each
 *                 of optional tokens and their next command tokens for the 
 *                 current token.
 * INPUT         : pointer to the token
 *                 pointer to the string to be compared
 *                 Flag showing if any entered token is from recur loop
 *                 Address of array containing active index values of matched
 *                 tokens and its count.
 * RETURNS       : NONE
 ******************************************************************************/
PUBLIC VOID
CliCxHlpFindMatchingNxtCmdTkns (t_MMI_CMD * pNextCmd,
                                UINT1 *pu1TokenToCompare,
                                INT4 *pi4TempArray,
                                INT2 *pi2ArrCnt, INT4 i4RecurFlag, INT2 i2Loop)
{
    t_MMI_CMD          *pOptNxt = NULL;
    t_MMI_CMD          *pRecurNode = NULL;
    INT2                i2TempCnt = 0;
    INT2                i2NonKeyRet = 0;
    UINT1               au1TempKeyWord[CLI_MAX_TOKEN_VAL_MEM];
    INT1               *pi1Token;

    i2TempCnt = *pi2ArrCnt;
    if (i4RecurFlag == 1)
    {
        pRecurNode = pNextCmd;
    }
    while (pNextCmd)
    {

        if (pNextCmd->i1pToken)
        {
            if (!STRCMP (pNextCmd->i1pToken, "ret"))
            {
                break;
            }
        }
        if (pNextCmd->pOptional)
        {
            pOptNxt = pNextCmd->pOptional;
            CliCxHlpFindMatchingOptionalTkns (pOptNxt,
                                              pu1TokenToCompare,
                                              pi4TempArray,
                                              &i2TempCnt, i4RecurFlag,
                                              i2Loop + 1);
        }
        /* Handling keyword type tokens */
        if (pNextCmd->i1pToken)
        {
            CliCxUtlMakeLowerCaseToCmp (pu1TokenToCompare, pNextCmd->i1pToken,
                                        au1TempKeyWord);
            if (!STRNCMP (pu1TokenToCompare,
                          au1TempKeyWord, STRLEN (pu1TokenToCompare)))
            {
                pi4TempArray[i2TempCnt] = pNextCmd->i4Action_number;
                pNextCmd = pNextCmd->pNext_command;
                ++i2TempCnt;
            }
            else
            {
                pNextCmd = pNextCmd->pNext_command;
            }
        }
        else                    /* Handling Nonkeyword type token */
        {
            i2NonKeyRet = CliCxUtlGetBitMaskPosition (pNextCmd->u8Typeof_token);
            if (i2NonKeyRet)
            {
                pi1Token = (INT1 *) pu1TokenToCompare;
                if (mmi_cmd_token[i2NonKeyRet - 1].type_check (pi1Token)
                    == TRUE)
                {
                    pi4TempArray[i2TempCnt] = pNextCmd->i4Action_number;
                    pNextCmd = pNextCmd->pNext_command;
                    ++i2TempCnt;
                }
                else
                {
                    pNextCmd = pNextCmd->pNext_command;
                }
            }
        }
        if (pNextCmd == pRecurNode)
        {
            break;
        }
    }
    *pi2ArrCnt = i2TempCnt;
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxHlpHandleRecurLoop
 * DESCRIPTION   : This function is used to handle recur loop while parsing the 
 *                 entered tokens
 * INPUT         : pointer to the tokens
 *                 flags to check outer and inner recurrence of a token
 *                 array to store all recurence loop nodes
 *                 no of matched tokens
 * RETURNS       : NONE
 ******************************************************************************/
PUBLIC INT4
CliCxHlpHandleRecurLoop (tCxtHelpParams * pCxtHlpParams,
                         t_MMI_CMD ** paRecurNodes, t_MMI_CMD ** ppRecurTkn,
                         t_MMI_CMD ** ppRetToken, t_MMI_CMD ** ppSameLevel,
                         t_MMI_CMD ** ppToken, INT4 *pi4RecurFlag,
                         INT4 *pi4TknCnt, INT4 *pi4RecurLoopCount,
                         INT4 *pi4InnerRecurCmpltFlag, INT1 i1SpaceFlag)
{

    t_MMI_CMD          *pTempRecurToken = NULL;
    t_MMI_CMD          *pRetToken = NULL;
    INT4                i4RetVal = 0;
    INT4                i4TempMatchCnt = 0;
    INT4                i4RecurTokenCount = 1;
    INT4                i4OuterRecurFlag = 0;
    INT4                i4InnerRecurFlag = 0;
    INT4                i4FoundInRecurLoopFlag = 1;
    /* if last parsed token is the starting of a recur loop */
    *pi4RecurFlag = 1;
    /* Variable to store no of tokens matched within recur loop */
    i4TempMatchCnt = 0;
    pTempRecurToken = ((*ppRecurTkn)->pNext_command)->pOptional;
    paRecurNodes[i4RecurTokenCount - 1] = (*ppRecurTkn)->pNext_command;
    /* to store address of all recur loop tokens in an array */
    while (pTempRecurToken != (*ppRecurTkn)->pNext_command)
    {
        ++i4RecurTokenCount;
        paRecurNodes[i4RecurTokenCount - 1] = pTempRecurToken;
        pTempRecurToken = pTempRecurToken->pOptional;
    }
    *pi4RecurLoopCount = i4RecurTokenCount;
    i4FoundInRecurLoopFlag = 1;
    for (i4OuterRecurFlag = 0;
         i4OuterRecurFlag < i4RecurTokenCount; ++i4OuterRecurFlag)
    {
        for (i4InnerRecurFlag = 0;
             i4InnerRecurFlag < i4RecurTokenCount; ++i4InnerRecurFlag)
        {
            *ppSameLevel = paRecurNodes[i4InnerRecurFlag];
            /* if all tokens entered are parsed successfully */
            if (*pi4TknCnt == pCxtHlpParams->i4EnteredTok)
            {
                break;
            }
            i4RetVal = CliCxHlpParseNxtCmdTkns (*ppSameLevel,
                                                (UINT1 (*)
                                                 [CLI_MAX_TOKEN_VAL_MEM])
                                                pCxtHlpParams->ppu1TknList,
                                                &pRetToken, *pi4TknCnt);
            *ppRetToken = pRetToken;
            if (i4RetVal == 0)
            {
                i4FoundInRecurLoopFlag = 0;
            }
            else
            {
                paRecurNodes[i4InnerRecurFlag] = NULL;
                ++(*pi4TknCnt);
                ++i4TempMatchCnt;
                ++i4FoundInRecurLoopFlag;
                break;
            }
        }
        /* to find last parsed token's address when all 
         * tokens entered are parsed successfully*/
        if (*pi4TknCnt == pCxtHlpParams->i4EnteredTok)
        {
            if ((*ppRetToken)->pOptional != (*ppRetToken)->pNext_token)
            {
                if ((*ppRetToken)->pOptional != NULL)
                {
                    *pi4InnerRecurCmpltFlag = 1;
                }
                if (i1SpaceFlag == 1)
                {
                    *ppRetToken = (*ppRetToken)->pNext_token;
                }
            }
            break;
        }
        /* if the token entered is not present in the recur loop */
        if (i4FoundInRecurLoopFlag == 0)
        {
            *ppSameLevel = (*ppRecurTkn)->pNext_token;
            *ppToken = (*ppRecurTkn)->pNext_token;
            break;
        }
        else
        {
            /* to find next token to parse with when previous token is 
             * present in the recur loop*/
            if ((*ppRetToken)->pOptional != (*ppRetToken)->pNext_token)
            {
                /* when recur loop has inner node */
                *ppSameLevel = (*ppRetToken)->pNext_token;
                i4RetVal = CliCxHlpParseNxtCmdTkns (*ppSameLevel,
                                                    (UINT1 (*)
                                                     [CLI_MAX_TOKEN_VAL_MEM])
                                                    pCxtHlpParams->ppu1TknList,
                                                    &pRetToken, *pi4TknCnt);
                *ppRetToken = pRetToken;
                if (i4RetVal != 0)
                {
                    ++(*pi4TknCnt);
                    *ppToken = (*ppRecurTkn)->pNext_token;
                    *ppSameLevel = (*ppRecurTkn)->pNext_token;
                }
                else
                {
                    i4TempMatchCnt = CLI_ACTINDEX_VAL;
                    break;
                }
            }
            else
            {
                /* when recur loop dont have inner node */
                if (i4TempMatchCnt != 0)
                {
                    *ppToken = (*ppRecurTkn)->pNext_token;
                    *ppSameLevel = (*ppRecurTkn)->pNext_token;
                }
            }
        }
    }
    /* if all the recur loop tokens have been entered */
    if (i4TempMatchCnt == i4RecurTokenCount)
    {
        if ((*pi4InnerRecurCmpltFlag == 0) && (i1SpaceFlag == 1))
        {
            *pi4RecurFlag = 0;
            *ppRetToken = *ppRecurTkn;
        }
    }
    return (i4TempMatchCnt);

}

/**********************END OF FILE*********************************************/
