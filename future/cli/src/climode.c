/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: climode.c,v 1.12 2014/02/13 12:22:44 siva Exp $
 *
 * Description: This will have the Mode level routines
 *              to create a mode and search mode      
 *******************************************************************/
#include "clicmds.h"

extern t_MMI_MODE_TREE *pMmiCur_mode;
extern t_MMI_MODE_TREE *pMmitemp_cur_mode;

/* Array of Context Structures */
extern tCliContext  gaCliContext[CLI_MAX_SESSIONS];
extern tCliSessions gCliSessions;

/* Protos */
static t_MMI_MODE_TREE *mmicm_get_submode (t_MMI_MODE_TREE *, UINT1 *);
static t_MMI_MODE_TREE *mmicm_getsub_formjust_orroot (UINT1 *);
static t_MMI_MODE_TREE *mmi_get_childmode (t_MMI_MODE_TREE *, INT1 *);
static t_MMI_MODE_TREE *mmicm_get_new_mode (VOID);
static VOID         mmicm_print_tabs_for_tree (INT1);

extern tMemPoolId   gCliCmdModePoolId;
extern tMemPoolId   gCliCmdListPoolId;

/*
 * This routine will get the sub node (name) of tree which have the parent 
 * as 'trace' if present.
 * if not present returns NULL
 *
 * Starting from trace it will search all neighbour list until NULL
 * and putting the child of each node into stack it will backtracks.
 * Thus when stack is empty it will conclude no other node to search
 *  
 *
 */

t_MMI_MODE_TREE    *
mmicm_get_submode (p_parent, p_name_to_search)
     t_MMI_MODE_TREE    *p_parent;    /* (K) parent to start */
     UINT1              *p_name_to_search;    /* (K) */
{
    t_MMI_MODE_TREE    *p_tstack[MAX_MODE_LEVEL];
    t_MMI_MODE_TREE    *p_tsearch = NULL;
    INT1                i1top = 0;

    p_tstack[i1top] = p_parent;
    while (i1top >= 0)
    {
        /* checking the stack empty */
        p_tsearch = p_tstack[i1top];    /* pop stack */
        i1top--;
        while (p_tsearch != NULL)
        {
            /* search through neighbours */
            if (mmi_new_strcmp
                ((INT1 *) p_name_to_search, p_tsearch->pMode_name))
            {
                if (p_tsearch->pChild != NULL)
                {
                    /* if not this node put child of
                       this node if not null in stack */
                    i1top++;    /* push stack */
                    if (i1top >= 0)
                    {
                        p_tstack[i1top] = p_tsearch->pChild;
                    }
                }
                p_tsearch = p_tsearch->pNeighbour;
            }
            else
            {
                i1top = -1;        /*  found node as search; to escape from all loop */
                break;
            }
        }
    }

    return p_tsearch;
}

/*
 * This routine will get the modeptr which has the modename
 * as pName
 * This will start to search from p_mmi_just_added node previously
 * If fails it will start from p_mmi_root_mode
 */

t_MMI_MODE_TREE    *
mmicm_getsub_formjust_orroot (pName)
     UINT1              *pName;
{
    t_MMI_MODE_TREE    *p_tsubnode;

    if (gCliSessions.p_mmi_just_added == NULL)
    {
        p_tsubnode = mmicm_get_submode (gCliSessions.p_mmi_just_added, pName);
        if (p_tsubnode == NULL)
        {
            p_tsubnode =
                mmicm_get_submode (gCliSessions.p_mmi_root_mode, pName);
        }
    }
    else
    {
        p_tsubnode = mmicm_get_submode (gCliSessions.p_mmi_root_mode, pName);
    }

    return p_tsubnode;
}

/*
 * This routine will search the child node of pMode which has the name
 * as pName
 */

t_MMI_MODE_TREE    *
mmi_get_childmode (pMode, pName)
     t_MMI_MODE_TREE    *pMode;
     INT1               *pName;
{
    t_MMI_MODE_TREE    *p_ttrace;

    if (mmi_new_strcmp (pMode->pMode_name, pName))
    {
        if (pMode->pChild != NULL)
        {
            p_ttrace = pMode->pChild;
        }
        else
        {
            mmi_printf ("No such a child\r\n");
            return NULL;
        }
    }
    else
    {
        return pMode;
    }
    while (p_ttrace != NULL)
    {
        if (mmi_new_strcmp (p_ttrace->pMode_name, pName))
        {
            p_ttrace = p_ttrace->pNeighbour;
        }
        else
        {
            break;
        }
    }
    if (p_ttrace == NULL)
    {
        mmi_printf (" no such mode stack_path[i]\r\n");
        return NULL;
    }
    return p_ttrace;
}

/*
 * This is the part of mmi_addmode () to get the parent node
 *
 * the 'parent_tosearch' will be splited to single path by '/'
 * 
 * From single path the first mode will be searched from
 *            1.p_mmi_just_added node
 *              if not success
 *            2. from p_mmi_root_mode (to search mmicm_get_submode 
 *                                        () will be called) 
 * from the result mode the remaining will be traced
 *
 *
 * 
 */

t_MMI_MODE_TREE    *
mmicm_search_parent (pparent_tosearch)
     CONST CHR1         *pparent_tosearch;    /* (K) string to search the parent */
{
    t_MMI_MODE_TREE    *p_tpresent_parent = NULL;    /* will be used to search */
    INT1                i1level_to_search = -1;    /* max level path specified */
    INT1                ptemp_parent_tosearch[MAX_PROMPT_LEN];    /*temporary sto */
    INT1               *p_i1temp1, *p_i1temp2;    /* as temporary */

    if (gCliSessions.p_mmi_root_mode == NULL)
    {
        /* checking the root */
        return NULL;
    }
    if (pparent_tosearch == NULL)
    {
        return NULL;
    }

    /* to split the path by '/' and store into stack */

    mmi_new_strcpy (ptemp_parent_tosearch, (CONST INT1 *) pparent_tosearch);

    p_i1temp1 = ptemp_parent_tosearch;
    do
    {
        p_i1temp2 = (INT1 *) STRCHR (p_i1temp1, (int) '/');
        if (p_i1temp2 != NULL)
        {
            while (*p_i1temp2 == '/')
            {
                *p_i1temp2 = '\0';
                p_i1temp2++;
            }
        }
        i1level_to_search++;
        if (i1level_to_search == 0)
        {
            p_tpresent_parent =
                mmicm_getsub_formjust_orroot ((UINT1 *) p_i1temp1);
        }
        else
        {
            if (p_tpresent_parent != NULL)
                p_tpresent_parent =
                    mmi_get_childmode (p_tpresent_parent, p_i1temp1);
        }
        p_i1temp1 = p_i1temp2;
    }
    while (p_i1temp2 != NULL);

    return p_tpresent_parent;
}

/*
 * This routine will allocate a new mode_node initialise all ptr to NULL
 */
t_MMI_MODE_TREE    *
mmicm_get_new_mode (VOID)
{
    t_MMI_MODE_TREE    *p_tnew_node = NULL;

    p_tnew_node =
        (t_MMI_MODE_TREE *) CliMemAllocMemBlk (gCliCmdModePoolId,
                                               sizeof (t_MMI_MODE_TREE));

    if (p_tnew_node)
    {
        if ((p_tnew_node->pCommand_tree =
             CliMemAllocMemBlk (gCliCmdListPoolId,
                                sizeof (t_MMI_COMMAND_LIST))) == NULL)
        {
            MemReleaseMemBlock (gCliCmdModePoolId, (UINT1 *) (p_tnew_node));
            p_tnew_node = NULL;
        }
    }
    return p_tnew_node;
}

/*
 * This routine will link the new mode in the mode tree as a child for
 * the parent mode node. The parent mode name may be as path or just 
 * single name. If the parentpath not from root
 * (following by mmicm_search_parent () ) 
 * (1) . the parent node will be searched from node that has been 
 * just added (in searching if one or more possible parent the first will
 * be chosen as parent) 
 * (2) . if 1 is fail the parent node will be searched from root.
 * when the new node is going to be added it will be checked tobe already 
 * present.
 *
 * The new node will be added as the last node of childlist of parent
 *
 * 
 */

INT4
mmi_addmode (CONST CHR1 * pName,    /* mode name to be included */
             CONST CHR1 * pParent_Name,    /* parent of new mode may be from 
                                           root or just parent mode name. */
             INT1 i1PromptType,    /* prompt whether STRING or FUNCTION */
             CONST CHR1 * pc1PromptStr,    /*  prompt string */
             INT1                (*pPromptFunc) (INT1 *, INT1 *),    /* prompt function */
             INT4                (*pValidate_Function) (UINT1 *)    /* function to validate prompt */
    )
{
    t_MMI_MODE_TREE    *p_tparent;    /* as temporary ptrs */
    t_MMI_MODE_TREE    *p_tmp_mode;
    t_MMI_MODE_TREE    *p_temp_ptr = NULL;
    t_MMI_MODE_TREE    *p_new_mode;    /* to get new node */
    /* 13-04-95 STK
       Because of Linux not allowing to overwrite into label 
       address "/" statically defined */
    static CONST CHR1   mmi_slash[] = "/";
    UINT4               u4Len = 0;

    p_new_mode = mmicm_get_new_mode ();

    if (p_new_mode == NULL)
    {
        return OSIX_FAILURE;
    }

    p_new_mode->pMode_name = (CONST INT1 *) pName;
    p_new_mode->PromptInfo.u1PromptType = i1PromptType;
    p_new_mode->u4ModeType = CLI_REGULAR_MODE;
    if (i1PromptType == PROMPT_STR)
    {
        u4Len =
            ((STRLEN (pc1PromptStr) <
              sizeof (p_new_mode->PromptInfo.
                      PromptStr)) ? STRLEN (pc1PromptStr) : sizeof (p_new_mode->
                                                                    PromptInfo.
                                                                    PromptStr) -
             1);
        STRNCPY (p_new_mode->PromptInfo.PromptStr, pc1PromptStr, u4Len);
        p_new_mode->PromptInfo.PromptStr[u4Len] = '\0';
    }
    else
    {
        p_new_mode->PromptInfo.PromptFunc = pPromptFunc;
    }
    p_new_mode->pValidate_function = pValidate_Function;

    if (gCliSessions.p_mmi_root_mode == NULL)
    {
        /* creat root node */
        if (pParent_Name != NULL)
        {
            MemReleaseMemBlock (gCliCmdListPoolId,
                                (UINT1 *) p_new_mode->pCommand_tree);
            MemReleaseMemBlock (gCliCmdModePoolId, (UINT1 *) (p_new_mode));
            return OSIX_FAILURE;
        }
        if (i1PromptType == PROMPT_STR)
        {
            if (gCliSessions.bIsClCliEnabled != OSIX_TRUE)
            {
                STRCPY (p_new_mode->PromptInfo.PromptStr, mmi_slash);
            }
        }
        p_new_mode->pParent = p_new_mode;
        gCliSessions.p_mmi_root_mode = p_new_mode;
        return OSIX_SUCCESS;
    }
    p_tparent = mmicm_search_parent (pParent_Name);
    if (p_tparent == NULL)
    {
        MemReleaseMemBlock (gCliCmdListPoolId,
                            (UINT1 *) p_new_mode->pCommand_tree);
        MemReleaseMemBlock (gCliCmdModePoolId, (UINT1 *) (p_new_mode));
        return OSIX_FAILURE;
    }

    if (p_tparent->pChild == NULL)
    {
        /* add as new child */
        p_new_mode->pParent = p_tparent;
        p_tparent->pChild = p_new_mode;
    }
    else
    {
        /* add in the list of child */
        p_tmp_mode = p_tparent->pChild;
        while (p_tmp_mode != NULL)
        {
            if (!mmi_new_strcmp (p_tmp_mode->pMode_name, (CONST INT1 *) pName))
            {
                MemReleaseMemBlock (gCliCmdListPoolId,
                                    (UINT1 *) p_new_mode->pCommand_tree);
                MemReleaseMemBlock (gCliCmdModePoolId, (UINT1 *) (p_new_mode));

                return OSIX_FAILURE;
            }
            p_temp_ptr = p_tmp_mode;    /* for later use to link with neighbour */
            p_tmp_mode = p_tmp_mode->pNeighbour;
        }
        p_tmp_mode = p_temp_ptr;
        p_tmp_mode->pNeighbour = p_new_mode;
        p_new_mode->pParent = p_tparent;
    }
    gCliSessions.p_mmi_just_added = p_new_mode;
    /* change p_mmi_just_added for later use */
    return OSIX_SUCCESS;
}

/*
 * This routine will get the sub node (name) of tree which have the parent 
 * as trace if present.
 * if not present returns NULL
 *
 * Starting from trace it will search all neighbour list until null
 * and putting the child of each node into stack it will backtracks.
 * Thus when stack is empty it will conclude no other node to search
 */

/*
 * This routine will trace the mode_tree from trace and will print
 * the MODE_NAME and MODE_PROMPT
 *
 * The tab print will be used to show the level of a node
 *
 * whenever the node going to be traced to its child its neighbour
 * will be put in the stack . Later the stack will be poped for tracing
 */

VOID
mmi_display_mode_tree (p_i1mode_path)
     CONST CHR1         *p_i1mode_path;
{
    t_MMI_MODE_TREE    *p_tstack[MAX_MODE_LEVEL];
    t_MMI_MODE_TREE    *p_tsearch;
    t_MMI_MODE_TREE    *pTempCurMode = NULL;
    t_MMI_MODE_TREE    *pTempLinkMode = NULL;
    INT1                i1top = 0;
    tCliContext        *pCliContext;
    INT1                ai1DispStr[MAX_PROMPT_LEN];
    INT1               *pi1CurModePrompt = NULL;
    INT1               *pi1BaseCurModePtr = NULL;
    INT2                i2ErrFlag = (INT2) CLI_SUCCESS;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    if ((pi1CurModePrompt = CLI_BUDDY_ALLOC (MAX_PROMPT_LEN, INT1)) == NULL)
    {
        return;
    }
    pi1BaseCurModePtr = pi1CurModePrompt;

    if (p_i1mode_path != NULL)
    {
        pTempCurMode = pCliContext->pMmiCur_mode;
        pCliContext->pMmiCur_mode = gCliSessions.p_mmi_root_mode;
        p_tstack[i1top] = pCliContext->pMmiCur_mode;

        if (STRCMP (p_i1mode_path, "/"))
        {
            CLI_STRCPY (pi1CurModePrompt, p_i1mode_path);
            while (i1top >= 0)
            {
                /* checking the stack empty */
                p_tsearch = p_tstack[i1top];    /* pop stack */
                i1top--;
                while (p_tsearch != NULL)
                {
                    /* Assumption: p_tsearch->pPrompt_function is a
                       pointer to string and not a function pointer */
                    if (p_tsearch->PromptInfo.u1PromptType == PROMPT_FUN)
                    {
                        if (p_tsearch->PromptInfo.PromptFunc
                            (pi1CurModePrompt, ai1DispStr) == TRUE)
                        {
                            pCliContext->pMmiCur_mode = p_tsearch;
                            break;
                        }
                    }
                    else
                    {
                        if (!STRCMP (p_i1mode_path,
                                     p_tsearch->PromptInfo.PromptStr))
                        {
                            pCliContext->pMmiCur_mode = p_tsearch;
                            break;
                        }
                    }
                    if (p_tsearch->pChild != NULL)
                    {
                        i1top++;
                        p_tstack[i1top] = p_tsearch->pNeighbour;
                        if (p_tsearch->u4ModeType == CLI_LINK_MODE)
                            p_tsearch = NULL;
                        else
                            p_tsearch = p_tsearch->pChild;
                    }
                    else
                    {
                        if (i1top < 0)
                        {
                            p_tsearch = NULL;
                        }
                        else
                        {
                            p_tsearch = p_tsearch->pNeighbour;

                            if (p_tsearch
                                && (pCliContext->pMmiCur_mode->pParent ==
                                    p_tsearch->pParent)
                                && (pCliContext->Mmi_i1Cur_modelevel > 0))
                            {
                                break;
                            }
                        }
                    }
                }
                if (i1top <= 0)
                    break;
            }
            if ((pCliContext->pMmiCur_mode == gCliSessions.p_mmi_root_mode))
            {
                pCliContext->pMmiCur_mode = pTempCurMode;
                mmi_printf ("Specify valid mode name\r\n");
                i2ErrFlag = (INT2) CLI_FAILURE;
            }
        }
    }
    else
    {
        if (CliGetCurModePromptStr (pi1CurModePrompt) == CLI_FAILURE)
        {
            i2ErrFlag = (INT2) CLI_FAILURE;
        }
    }
    if (i2ErrFlag == (INT2) CLI_FAILURE)
    {
        CLI_BUDDY_FREE (pi1CurModePrompt);
        return;
    }

    i1top = 0;
    p_tsearch = NULL;

    if (pCliContext->pMmiCur_mode != NULL)
    {
        p_tstack[i1top] = pCliContext->pMmiCur_mode;
    }
    else
    {
        if (CliChmod (pCliContext, "/") == CLI_SUCCESS)
        {
            p_tstack[i1top] = pCliContext->pMmitemp_cur_mode;
        }
        else
        {
            CLI_BUDDY_FREE (pi1CurModePrompt);
            return;
        }
    }

    ai1DispStr[0] = '\0';
    while (i1top >= 0)
    {
        /* checking the stack empty */
        p_tsearch = p_tstack[i1top];    /* pop stack */
        i1top--;
        while (p_tsearch != NULL)
        {
            mmicm_print_tabs_for_tree (i1top);
            /* Assumption: p_tsearch->pPrompt_function is a
             * pointer to string and not a function pointer */
            mmi_printf ("%s : ", p_tsearch->pMode_name);
            if (p_tsearch->u4ModeType == CLI_LINK_MODE)
            {
                pTempLinkMode = p_tsearch;
                p_tsearch = p_tsearch->pChild;
                pi1CurModePrompt = NULL;
            }
            if (p_tsearch->PromptInfo.u1PromptType == PROMPT_FUN)
            {
                if (pCliContext->pMmiCur_mode == gCliSessions.p_mmi_root_mode)
                {
                    p_tsearch->PromptInfo.PromptFunc (NULL, ai1DispStr);
                    mmi_printf ("%s\r\n", ai1DispStr);
                    ai1DispStr[0] = '\0';
                }
                else
                {
                    if ((p_tsearch->PromptInfo.PromptFunc
                         (pi1CurModePrompt, ai1DispStr)) == TRUE)
                    {
                        mmi_printf ("%s\r\n", ai1DispStr);
                        ai1DispStr[0] = '\0';
                        pi1CurModePrompt = NULL;
                    }
                }
            }
            else
            {
                mmi_printf ("%s\r\n", p_tsearch->PromptInfo.PromptStr);
            }
            if (pTempLinkMode)
            {
                p_tsearch = pTempLinkMode;
                pTempLinkMode = NULL;
            }
            if ((p_tsearch->u4ModeType != CLI_LINK_MODE) &&
                (p_tsearch->pChild != NULL))
            {
                i1top++;
                p_tstack[i1top] = p_tsearch->pNeighbour;
                p_tsearch = p_tsearch->pChild;
            }
            else
            {
                if (i1top < 0)
                {
                    p_tsearch = NULL;
                }
                else
                {
                    p_tsearch = p_tsearch->pNeighbour;

                    if (p_tsearch && (pCliContext->pMmiCur_mode->pParent
                                      == p_tsearch->pParent) &&
                        (pCliContext->Mmi_i1Cur_modelevel > 0))
                    {
                        break;
                    }
                }
            }
        }

        if (i1top <= 0)
            break;

        if (i1top != -1)
        {
            mmicm_print_tabs_for_tree (i1top);
            mmi_printf ("\r\n");
        }

        if (pCliContext->Mmi_i1Cur_modelevel > 0)
            break;
    }
    if (pTempCurMode != NULL)
    {
        pCliContext->pMmiCur_mode = pTempCurMode;
    }

    CLI_BUDDY_FREE (pi1BaseCurModePtr);
    return;
}

/*
 * To print the tabs
 */
VOID
mmicm_print_tabs_for_tree (INT1 i1num_tabs)
{
    INT1                i;
    for (i = 0; i <= i1num_tabs; i++)
    {
        mmi_printf ("        ");
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliDisplayPWD                                       *
 *                                                                         *
 *     Description   : Displays the present working mode path.             *
 *                                                                         *
 *     Input(s)      : CliHandle - Current CliContextId                    *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
CliDisplayPWD (tCliHandle CliHandle)
{
    tCliContext        *pCliContext = NULL;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    if ((pCliContext == NULL) || (pCliContext->i1Status != CLI_ACTIVE))
    {
        /* No Task Found */
        return;
    }

    CliPrintf (CliHandle, "%s\r\n", pCliContext->Mmi_i1Cur_prompt);
}
