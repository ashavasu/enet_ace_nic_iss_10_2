/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliparse.c,v 1.161 2017/01/20 13:56:12 siva Exp $
 *
 * Description: This will have the command parse routines 
 *
 *******************************************************************/
#include "clicmds.h"
#include "utilcli.h"
#include <time.h>

/* Array of Context Structures */
extern tCliSessions gCliSessions;
extern t_MMI_TOKEN_TYPE mmi_cmd_token[];
extern tCliAmbMemPoolId gCliAmbMemPoolId;

/* This contains the entire command tree */
extern t_MMI_COMMAND_LIST MMIGROUP_ALL_cmds;
extern t_MMI_COMMAND_LIST MMIMODE_cmds;
extern tMemPoolId   gCliMaxLineMemPoolId;
extern tMemPoolId   gCliAliasMemPoolId;
extern tMemPoolId   gCliAmbArrPoolId;
extern tMemPoolId   gCliAmbPtrPoolId;
extern tMemPoolId   gAuditInfoMemPoolId;

static t_MMI_CMD   *mmi_alt_parse (tCliContext *, INT1 *, t_MMI_CMD *, INT2 *);
static t_MMI_CMD   *mmi_list_parse (tCliContext *, INT1 *, t_MMI_CMD *, INT2 *);
static t_MMI_CMD   *mmi_start_recur_check_parse (tCliContext *, INT1 *,
                                                 t_MMI_CMD *, INT2 *);
static INT4         mmi_node_is_in_recur (tCliContext *, t_MMI_CMD *);
static VOID         mmi_copy_mpenv_to_ambenv (tCliContext *, INT4, INT2 *);
static VOID         mmi_copy_ambenv_to_mpenv (tCliContext *, INT4, INT2 *);
static VOID         mmi_copy_ambenv_to_ambenv (tCliContext *, INT2, INT2);
static INT4         mmi_reorganise_ambenv (tCliContext *, INT4);
static INT4         mmi_parse_continue (tCliContext *, INT1 *, INT2 *);
static INT4         mmi_convert_value_totype (tCliContext *, INT1 *, INT1);
static INT4         mmi_convert_value_totype_check (tCliContext *, INT1 *,
                                                    INT1);
static INT4         mmi_do_action (tCliContext *, INT4 *, INT2 *, INT2 *);
static INT4         MmiDoCheckAllTokens (tCliContext *, INT4 *, INT2 *);
VOID                mmi_print_error_in_token (tCliContext *,
                                              INT2 i2token_count);
UINT4               CliExecuteCliCmdforContext (tCliContext *, CONST CHR1 *);
extern UINT1        gu1FileCopyingStatus;
#ifdef RM_WANTED
INT4                gCliRmRole = RM_INIT;
#else
INT4                gCliRmRole = RM_ACTIVE;
#endif

#ifdef L2RED_WANTED

#define FILTER_STRING_LIST_MAX 4

tCliFilterStringList gFilterStringList[] = {
    {"exit"},
    {"end"},
    {"reload"},
    {"hw"}
};
#endif

/*
 * This file is used to parse the command in mmi from the created 
 * command data structure as a tree
 *
 * To Know the tree structure see cmdcreat.c file
 *
 *
 * Three main functions in this file 
 *     mmi_first_tok_parse () 
 *     mmi_parse_continue () and
 *     mmi_do_action () 
 * will be called from mmi_read.c by mmi_parse () 
 *
 * Aim :
 *      1. The token will be passed to all commands one by one  
 *      2. The matching commands will be collected as ambiguity commands
 *      3. for each ambiguity command there will be a
 *            amb_parse_environment
 *      4. whenever the token passed to the ambiguity command
 *         the corresponding amb_parse_environment will be
 *         copied to the main_parse_environment
 *         if the token is successfully parsed the amb_parse_environment
 *         will be updated
 *         else
 *            the amb_parse_environment will be left
 *      5. At the last token parsing, the number of amb_environments
 *         will be checked 
 *         if the amb_parse_environment is 1 the
 *            mmi_do_action () will do the action
 *         else print error
 */

/*
 * This routine to parse the 'token' by checking all alternative
 * token in list 
 *
 * This will be called from mmi_list_parse () 
 *
 * when searching token by token to alt
 * case A1. if token is recur need not to check alt 
 * (because in start_procedure itself we are checking) 
 *          just count that node for skipping
 *          and skip it no other token will be in list . () |  cant come
 * case A2. if normal node  by passing alt_ptr
 *          call mmi_list_parse () if success increment conflict_flag
 *   
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_alt_parse  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
t_MMI_CMD          *
mmi_alt_parse (pCliContext, token, tmp_parse_ptr, pi2ErrCode)
     tCliContext        *pCliContext;
     INT1               *token;
     t_MMI_CMD          *tmp_parse_ptr;
     INT2               *pi2ErrCode;
{
    t_MMI_CMD          *tmp_ptr;    /* to be used to go through list */
    t_MMI_CMD          *parsed_ptr = NULL;    /* to store the matched one */
    t_MMI_CMD          *alt_ptr;    /*  to pass to the mmi_list_parse () */
    INT2                conflict_flag = 0;    /* to count no of matching */

   /**** $$TRACE_LOG (ENTRY,"") ****/
    tmp_ptr = tmp_parse_ptr;

    while ((tmp_ptr != NULL) && !conflict_flag)
    {
        if (mmi_new_strcmp ((CONST INT1 *) tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            /* A1 */
            break;                /* alt already checked */
        }
        alt_ptr = tmp_ptr->pOptional;
        if (alt_ptr != NULL &&
            (mmi_new_strcmp ((CONST INT1 *) alt_ptr->i1pToken,
                             (CONST INT1 *) "ret")))
        {
            /* A2 */
            if ((alt_ptr =
                 mmi_list_parse (pCliContext, token, alt_ptr, pi2ErrCode))
                != NULL)
            {
                conflict_flag++;
                parsed_ptr = alt_ptr;
            }

        }
        tmp_ptr = tmp_ptr->pNext_command;
    }
    if (conflict_flag == 1)
    {
      /**** $$TRACE_LOG (EXIT,"") ****/
        return parsed_ptr;
    }
    else
    {
      /**** $$TRACE_LOG (EXIT,"") ****/
        return NULL;
    }
}

/*
 * This routine to parse 'token' in tmp_parse_ptr list
 *
 * This will be called from mmi_alt_parse and mmi_start_recur_check_parse () 
 *
 * case A1. if the node is 'ret' illeagal return NULL
 * case A2. if the node is 'recur' call mmi_start_recur_check_parse () 
 *              and return result
 * case A3. if this node is a alternative in recursive
 *          and already came then return NULL
 * search 'token' tmp_parse_ptr
 * when searching if one or more matching increment conflict_flag
 *       case B1. if node is 'recur' call mmi_start_recur_check_parse (); 
 *                break
 *       case B2.if normal node and string is NULL
 *                    compare with i4Type_token
 *               else compare using mmi_strncmpi () 
 * At last
 * according to the value of conflict_flag
 * case D1. if conflict_flag = 1
 *             return result
 * case D2. if conflict_flag = 0 and alt_ptr != NULL
 *             call mmi_alt_parse (token, tmp_parse_ptr); 
 *             return result
 * case D3. if conflict_flag > 1
 *             return NULL
 * case D3. no matching return NULL
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_list_parse  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
t_MMI_CMD          *
mmi_list_parse (pCliContext, token, tmp_parse_ptr, pi2ErrCode)
     tCliContext        *pCliContext;
     INT1               *token;
     t_MMI_CMD          *tmp_parse_ptr;
     INT2               *pi2ErrCode;
{
    t_MMI_CMD          *tmp_ptr;    /* to go through in the list */
    t_MMI_CMD          *parsed_ptr = NULL;    /* to have the matched one */
    INT2                conflict_flag = 0;    /* to count the matching nodes */
    FS_UINT8            u8MinTokenType;
    FS_UINT8            u8RetTokenType;
    FS_UINT8            u8TempTokenType;

    FSAP_U8_ASSIGN_LO (&u8MinTokenType, 0xfffffffe);
    FSAP_U8_ASSIGN_HI (&u8MinTokenType, 0xfffffffe);
    FSAP_U8_CLR (&u8TempTokenType);
    FSAP_U8_CLR (&u8RetTokenType);

    /* Type of token more precisely matching the given token */

    if (tmp_parse_ptr == NULL)
    {
        return NULL;
    }
   /**** $$TRACE_LOG (ENTRY,"") ****/
    if (mmi_new_strcmp ((CONST INT1 *) tmp_parse_ptr->i1pToken,
                        (CONST INT1 *) "ret") == 0)
    {
        /* A1 */
        if ((pCliContext->present_ambig_count == 1) && (token) && (*token))
        {
            /* Extra token in command, return error
             * *pi2ErrCode = 0 - MMI_GEN_ERR_EXTRA_TOKEN;
             */
        }
      /**** $$TRACE_LOG (EXIT,"") ****/
        return NULL;
    }

    if (pCliContext->mp_i2recur_flag)
    {
        /* A2 */
        if (tmp_parse_ptr ==
            pCliContext->recur_repeat_node[pCliContext->mp_i2recur_flag])
        {
            if (pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag] ==
                0)
            {
                pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag]
                    = 1;
            }
            else
            {
            /**** $$TRACE_LOG (EXIT,"") ****/
                return NULL;
            }
        }
    }

    if (mmi_new_strcmp ((CONST INT1 *) tmp_parse_ptr->i1pToken,
                        (CONST INT1 *) "recur") == 0)
    {
        /* A3 */
        parsed_ptr = mmi_start_recur_check_parse (pCliContext, token,
                                                  tmp_parse_ptr, pi2ErrCode);
      /**** $$TRACE_LOG (EXIT,"") ****/
        return parsed_ptr;
    }

    tmp_ptr = tmp_parse_ptr;

    while (tmp_ptr != NULL)
    {
        if (mmi_new_strcmp ((CONST INT1 *) tmp_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            /* B1 */
            if ((parsed_ptr = mmi_start_recur_check_parse (pCliContext, token,
                                                           tmp_ptr,
                                                           pi2ErrCode)) != NULL)
            {
                conflict_flag++;
            }
            break;
        }

        mmi_get_type_token (token, &u8RetTokenType);

        if (tmp_ptr->i1pToken != NULL)
        {
            /* B2 */
            if (mmi_strncmpi ((CONST INT1 *) tmp_ptr->i1pToken,
                              token, CLI_STRLEN (token)) == 0)
            {
                conflict_flag++;
                if (STRLEN (tmp_ptr->i1pToken) == STRLEN (token))
                {
                    conflict_flag = 1;
                    parsed_ptr = tmp_ptr;
                    break;
                }
                parsed_ptr = tmp_ptr;
            }
        }
        else if (((FSAP_U8_FETCH_LO (&u8TempTokenType) =
                   ((FSAP_U8_FETCH_LO (&(tmp_ptr->u8Typeof_token))) &
                    (FSAP_U8_FETCH_LO (&(u8RetTokenType))))) != 0)
                 &&
                 (FSAP_U8_FETCH_LO (&u8TempTokenType) <
                  FSAP_U8_FETCH_LO (&u8MinTokenType)))
        {
            conflict_flag++;
            parsed_ptr = tmp_ptr;
            FSAP_U8_FETCH_LO (&u8MinTokenType) =
                FSAP_U8_FETCH_LO (&u8TempTokenType);
        }

        else if (((FSAP_U8_FETCH_HI (&u8TempTokenType) =
                   ((FSAP_U8_FETCH_HI (&(tmp_ptr->u8Typeof_token))) &
                    (FSAP_U8_FETCH_HI (&u8RetTokenType)))) != 0)
                 &&
                 (FSAP_U8_FETCH_HI (&u8TempTokenType) <
                  (FSAP_U8_FETCH_HI (&u8MinTokenType))))
        {
            conflict_flag++;
            parsed_ptr = tmp_ptr;
            FSAP_U8_FETCH_HI (&u8MinTokenType) =
                FSAP_U8_FETCH_HI (&u8TempTokenType);
        }
        tmp_ptr = tmp_ptr->pNext_command;
    }

    if (conflict_flag == 1)
    {
        /* D1 */
      /**** $$TRACE_LOG (EXIT,"") ****/
        return parsed_ptr;
    }
    else if ((conflict_flag == 0) && (tmp_parse_ptr))
    {
        /* D2 */
        parsed_ptr = mmi_alt_parse (pCliContext, token, tmp_parse_ptr,
                                    pi2ErrCode);
      /**** $$TRACE_LOG (EXIT,"") ****/
        return parsed_ptr;
    }
    else
    {
        /* D4 */
        *pi2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
        return NULL;
    }
}

/*
 * This will parse the "token" starting from "node"
 *
 * This will be called from 
 *       mmi_parse_continue () 
 *       mmi_list_parse () for recursive node only
 *
 *
 * A1. if the node is recursive node then it will update
 *    the parse_environment recur variables
 *    and make node = node->pNext_command
 * A2. if the node is in recursive environment
 * (ie. mp_i2recur_flag > 0) 
 *    case B1. the node may be a alternative node in recursive
 *    case B2. the node may be a subnode of an alternative
 *     for case B1.  update recur_repeat_node[mp_i2recur_flag] 
 *                         recur_repeat_flag[mp_i2recur_flag] = 0;
 *                  call mmi_list_parse (token, node) 
 *                  if it is successful
 *                    C1.  
                           by checking the repeatation of the node
                           return result
 *                  else
 *                    C2.  
 *                         set node = nexttoken of present recur node 
 *                         mp_i2recur_flag--;
 *                         call mmi_list_parse (token, node) 
 *                         return the result
 *    for case B2.
 *                make recur_repeat_node = NULL
 * A3. if the in out of recursive environment
 * (ie. mp_i2recur_flag = 0) 
 *    just call mmi_list_parse (token, node) .
 * 
 * In this valid_esc_recur () to check atleast one node occur
 * if valid_esc_recur () 
 *       parse with next_token of recur
 *       before parsing check the next token with tmp_alt_store  
 *         ^ not neccessary because restriction is there canbe removed
 * if not parse with alt of recur
 *
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_start_recur_check_parse  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
t_MMI_CMD          *
mmi_start_recur_check_parse (pCliContext, token, node, pi2ErrCode)
     tCliContext        *pCliContext;
     INT1               *token;
     t_MMI_CMD          *node;    /* node to be started */
     INT2               *pi2ErrCode;
{
    INT4                i, j;
    INT2                valid_esc_recur = 0;    /*  if recur node failure
                                                   to indicate atleast one
                                                   token in recur occured */
    t_MMI_CMD          *parsed_ptr;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    if (node == NULL)
    {
        mmi_printf ("\rNULL node in mmi_start_recur_check_parse\n");
      /**** $$TRACE_LOG (ABN_EXIT,"") ****/
        return NULL;
    }
    if (mmi_new_strcmp ((CONST INT1 *) node->i1pToken,
                        (CONST INT1 *) "recur") == 0)
    {
        /* A1 */
        pCliContext->mp_i2recur_flag++;
        pCliContext->p_mp_recur_node[pCliContext->mp_i2recur_flag] = node;
        for (i = 0; i < MAX_CMD_REC_PAR; i++)
        {
            pCliContext->mp_i2set_recur_node_flag[pCliContext->
                                                  mp_i2recur_flag][i] = 0;
        }
        pCliContext->recur_repeat_node[pCliContext->mp_i2recur_flag] = NULL;
        pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag] = 1;
        node = node->pNext_command;
    }
    if (pCliContext->mp_i2recur_flag > 0)
    {
        /* A2 */
        if (mmi_node_is_in_recur (pCliContext, node) >= 0)
        {
            /* B1 */
            pCliContext->recur_repeat_node[pCliContext->mp_i2recur_flag] = node;
            pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag] = 0;
            if ((parsed_ptr =
                 mmi_list_parse (pCliContext, token, node, pi2ErrCode)) == NULL)
            {
                node = pCliContext->p_mp_recur_node[pCliContext->mp_i2recur_flag];    /* C2 */
                pCliContext->recur_repeat_node[pCliContext->mp_i2recur_flag]
                    = NULL;
                pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag]
                    = 1;
                j = pCliContext->mp_i2recur_flag;
                pCliContext->mp_i2recur_flag--;
                valid_esc_recur = 0;
                for (i = 0; i < MAX_CMD_REC_PAR; i++)
                {
                    if (pCliContext->mp_i2set_recur_node_flag[j][i] == 1)
                    {
                        valid_esc_recur = 1;
                    }
                }

                if (valid_esc_recur)
                {
                    node = node->pNext_token;
                    parsed_ptr = mmi_list_parse (pCliContext, token, node,
                                                 pi2ErrCode);
               /**** $$TRACE_LOG (EXIT,
                     "skipping the recur node\n") ****/
                    return parsed_ptr;
                }
                else if (node->pOptional != NULL)
                {
                    node = node->pOptional;
                    parsed_ptr = mmi_list_parse (pCliContext, token, node,
                                                 pi2ErrCode);
               /**** $$TRACE_LOG (EXIT,
                    "skipping the recur node\n") ****/
                    return parsed_ptr;
                }
                else
                {
               /**** $$TRACE_LOG (EXIT,"") ****/
                    return NULL;
                }
            }
            else
            {
                /* C1 */
                i = mmi_node_is_in_recur (pCliContext, parsed_ptr);
                if (pCliContext->mp_i2set_flag == 1)
                {
               /**** $$TRACE_LOG (EXIT,"") ****/
                    return parsed_ptr;
                }
                if ((i != -1) && (pCliContext->
                                  mp_i2set_recur_node_flag[pCliContext->
                                                           mp_i2recur_flag][i]
                                  == 0))
                {
                    pCliContext->mp_i2set_recur_node_flag[pCliContext->
                                                          mp_i2recur_flag][i] =
                        1;
                    pCliContext->mp_i2set_flag = 1;
               /**** $$TRACE_LOG (EXIT,"") ****/
                    return parsed_ptr;
                }
                else
                {
                    *pi2ErrCode = -(MMI_GEN_ERR_RECUR_REPEAT);
                    return NULL;
                }
            }
        }
        else
        {
            /* node is not in recur *//* B2 */
            pCliContext->recur_repeat_node[pCliContext->mp_i2recur_flag] = NULL;
            pCliContext->recur_repeat_flag[pCliContext->mp_i2recur_flag] = 1;
            parsed_ptr = mmi_list_parse (pCliContext, token, node, pi2ErrCode);
         /**** $$TRACE_LOG (EXIT,"") ****/
            return parsed_ptr;
        }

    }
    else
    {
        /* no concept of recur *//* A3 */
        parsed_ptr = mmi_list_parse (pCliContext, token, node, pi2ErrCode);
      /**** $$TRACE_LOG (EXIT,"") ****/
        return parsed_ptr;
    }
}

/*
 * This will search the 'node' in the present recur_node
 * whether it is an alternative_starting node or not.
 * if yes it will return the no of position
 * otherwise -1
 *
 * This will be checked by going through alternative tokens of 
 * recur_node
 * when going through alternative tokens OR tokens also willbe
 * taken care (case 1) 
 */
/**** $$TRACE_PROCEDURE_NAME  = mmi_node_is_in_recur  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
INT4
mmi_node_is_in_recur (pCliContext, node)
     tCliContext        *pCliContext;
     t_MMI_CMD          *node;
{
    t_MMI_CMD          *tmp_ptr, *ptr, *temp_ptr;
    INT2                pos_count = 0;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    ptr = tmp_ptr =
        pCliContext->p_mp_recur_node[pCliContext->mp_i2recur_flag]->
        pNext_command;
    do
    {
        temp_ptr = tmp_ptr;
        while (temp_ptr != NULL)
        {
            if (temp_ptr == node)
            {
            /**** $$TRACE_LOG (EXIT,"") ****/
                return pos_count;
            }
            temp_ptr = temp_ptr->pNext_command;    /* for case 1 */
        }
        tmp_ptr = tmp_ptr->pOptional;    /* by going through alt tokens */
        pos_count++;
    }
    while ((tmp_ptr != NULL) && (tmp_ptr != ptr));
   /**** $$TRACE_LOG (EXIT,"") ****/
    return -1;
}

/*
 * This will copy main_parse_environment variables values to
 * amb_parse_environment[num]
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_copy_mpenv_to_ambenv  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
VOID
mmi_copy_mpenv_to_ambenv (pCliContext, num, pi2ErrCode)
     tCliContext        *pCliContext;
     INT4                num;
     INT2               *pi2ErrCode;
{
    INT2                i, j;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    if (num >= MAX_AMBIG_CMD)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
        return;
    }
    pCliContext->pCliAmbVar->amb_ptr_start[num] = pCliContext->env_ptr_start;
    pCliContext->pCliAmbVar->amb_recur_flag[num] = pCliContext->mp_i2recur_flag;
    for (i = 0; i < MAX_NO_OF_TOKENS_IN_MMI; i++)
    {
        pCliContext->pCliAmbVar->amb_vpMmi_values[num][i] =
            pCliContext->vpMmi_values[i];
    }
    for (i = 0; i <= pCliContext->mp_i2recur_flag; i++)
    {
        pCliContext->pCliAmbVar->amb_recur_node[num][i] =
            pCliContext->p_mp_recur_node[i];
        for (j = 0; j < MAX_CMD_REC_PAR; j++)
        {
            pCliContext->pCliAmbVar->amb_set_recur_node_flag[num][i][j] =
                pCliContext->mp_i2set_recur_node_flag[i][j];
        }
        pCliContext->pCliAmbVar->amb_recur_repeat_node[num][i] =
            pCliContext->recur_repeat_node[i];
        pCliContext->pCliAmbVar->amb_recur_repeat_flag[num][i] =
            pCliContext->recur_repeat_flag[i];
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return;
}

/*
 * This will copy the amb_parse_environment[num] variables values
 * to main_parse_environment variables
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_copy_ambenv_to_mpenv  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
VOID
mmi_copy_ambenv_to_mpenv (pCliContext, num, pi2ErrCode)
     tCliContext        *pCliContext;
     INT4                num;
     INT2               *pi2ErrCode;
{
    INT2                i, j;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    if (num >= MAX_AMBIG_CMD)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
        return;
    }
    pCliContext->env_ptr_start = pCliContext->pCliAmbVar->amb_ptr_start[num];
    pCliContext->mp_i2recur_flag = pCliContext->pCliAmbVar->amb_recur_flag[num];

    /* Copy the command tokens vpMmi_values from the ambig vpMmi_values. */
    for (i = 0; i < MAX_NO_OF_TOKENS_IN_MMI; i++)
    {
        pCliContext->vpMmi_values[i] =
            pCliContext->pCliAmbVar->amb_vpMmi_values[num][i];
    }
    for (i = 0; i <= pCliContext->mp_i2recur_flag; i++)
    {
        pCliContext->p_mp_recur_node[i] =
            pCliContext->pCliAmbVar->amb_recur_node[num][i];
        for (j = 0; j < MAX_CMD_REC_PAR; j++)
        {
            pCliContext->mp_i2set_recur_node_flag[i][j] =
                pCliContext->pCliAmbVar->amb_set_recur_node_flag[num][i][j];
        }
        pCliContext->recur_repeat_node[i] =
            pCliContext->pCliAmbVar->amb_recur_repeat_node[num][i];
        pCliContext->recur_repeat_flag[i] =
            pCliContext->pCliAmbVar->amb_recur_repeat_flag[num][i];
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return;
}

/*
 * This will copy the amb_parse_environment[src] variable values to
 * amb_parse_environment[dst] variables
 */
/**** $$TRACE_PROCEDURE_NAME  = mmi_copy_ambenv_to_ambenv  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
VOID
mmi_copy_ambenv_to_ambenv (tCliContext * pCliContext, INT2 dst, INT2 src)
{
    INT2                i, j;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    if ((dst >= MAX_AMBIG_CMD) || (src >= MAX_AMBIG_CMD))
    {
        return;
    }
    if (src == dst)
    {
      /**** $$TRACE_LOG (EXIT,"") ****/
        return;
    }
    pCliContext->pCliAmbVar->amb_ptr_start[dst] =
        pCliContext->pCliAmbVar->amb_ptr_start[src];
    pCliContext->pCliAmbVar->amb_recur_flag[dst] =
        pCliContext->pCliAmbVar->amb_recur_flag[src];
    for (i = 0; i < MAX_NO_OF_TOKENS_IN_MMI; i++)
    {
        pCliContext->pCliAmbVar->amb_vpMmi_values[dst][i] =
            pCliContext->pCliAmbVar->amb_vpMmi_values[src][i];
    }
    for (i = 0; i <= pCliContext->pCliAmbVar->amb_recur_flag[src]; i++)
    {
        pCliContext->pCliAmbVar->amb_recur_node[dst][i] =
            pCliContext->pCliAmbVar->amb_recur_node[src][i];
        for (j = 0; j < MAX_CMD_REC_PAR; j++)
        {
            pCliContext->pCliAmbVar->amb_set_recur_node_flag[dst][i][j] =
                pCliContext->pCliAmbVar->amb_set_recur_node_flag[src][i][j];
        }
        pCliContext->pCliAmbVar->amb_recur_repeat_node[dst][i] =
            pCliContext->pCliAmbVar->amb_recur_repeat_node[src][i];
        pCliContext->pCliAmbVar->amb_recur_repeat_flag[dst][i] =
            pCliContext->pCliAmbVar->amb_recur_repeat_flag[src][i];
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return;
}

/*
 * This will collect the valid amb_parse_environments 
 * and will return the no of valid amb_parse_environments
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_reorganise_ambenv  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
INT4
mmi_reorganise_ambenv (pCliContext, present_amb)
     tCliContext        *pCliContext;
     INT4                present_amb;
{
    INT2                i;
    INT2                i2Index = 0;
    INT2                tmp_value;
    INT4                ambig_count = 0;

   /**** $$TRACE_LOG (ENTRY,"") ****/
    for (i = 0; i < present_amb; i++)
    {
        if (pCliContext->pCliAmbVar->amb_ptr_start_flag[i])
        {
            if (pCliContext->amb_env_ptr_start != NULL)
            {
                for (i2Index = 0; i2Index < pCliContext->i2TokenCount;
                     i2Index++)
                {
                    pCliContext->amb_env_ptr_start[ambig_count][i2Index] =
                        pCliContext->amb_env_ptr_start[i][i2Index];
                }
            }

            mmi_copy_ambenv_to_ambenv (pCliContext, (INT2) ambig_count, i);
            tmp_value = pCliContext->pCliAmbVar->amb_ptr_start_flag[i];
            pCliContext->pCliAmbVar->amb_ptr_start_flag[i] = 0;    /* very important */
            pCliContext->pCliAmbVar->amb_ptr_start_flag[ambig_count] =
                tmp_value;
            ambig_count++;
        }
    }

   /**** $$TRACE_LOG (EXIT,"") ****/
    return ambig_count;
}

/*
 * This is to parse the first token of any command .
 *
 * This will be called from mmi_parse () in mmi_reead.c for 
 * the first token of command
 *
 * This will search the commands in prsent mode command tree
 * those are matching with the token .
 * For each command which is matching this will initialise
 * amb_parse_environment
 *
 * This will return number of ambiguity commands those matched
 */
/**** $$TRACE_PROCEDURE_NAME  = mmi_first_tok_parse  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
INT4
mmi_first_tok_parse (pCliContext, token, pi2ErrCode)
     tCliContext        *pCliContext;
     INT1               *token;
     INT2               *pi2ErrCode;
{
    INT2                i;
    UINT4               pri_value;
    t_MMI_COMMAND_LIST *tmp_mode_list;
    t_MMI_CMD          *tmp_ptr;
    t_MMI_CMD          *pTmp = NULL;
    t_MMI_HELP         *pCliHelpInfo = NULL;
    INT4                i4Result = 0;
    INT4                u4PrivId = 0;
    INT2                ambig_count = 0;
    INT2                i2Index = 0;
    INT2                i2TokIndex = 0;
    INT2                i2TokenIndex = 1;
    INT1                i1ExactMatch = FALSE;

   /**** $$TRACE_LOG (ENTRY,"") ****/

    for (i = 0; i < MAX_AMBIG_CMD; i++)
    {
        pCliContext->pCliAmbVar->amb_ptr_start_flag[i] = 0;
    }
    pCliContext->present_ambig_count = 0;
    if (pCliContext->pMmiCur_mode == NULL)
    {
        return 0;
    }

    tmp_mode_list = pCliContext->pMmiCur_mode->pCommand_tree;

    if (*pi2ErrCode < 0)
        return 0;

    while (tmp_mode_list != NULL)
    {
        tmp_ptr = tmp_mode_list->pCmd;
        while (tmp_ptr != NULL)
        {
            if (mmi_strncmpi ((CONST INT1 *) tmp_ptr->i1pToken,
                              token, CLI_STRLEN (token)) == 0)
            {
                pri_value =
                    CLI_STRLEN (tmp_ptr->i1pToken) - CLI_STRLEN (token) + 1;
                if (ambig_count < MAX_AMBIG_CMD)
                {
                    /* Copy only the commands whose previlige level is <=  
                     * current user previlege*/

                    pTmp = tmp_ptr;
                    while ((pTmp != NULL) && (mmi_new_strcmp
                                              ((CONST INT1 *) pTmp->i1pToken,
                                               (CONST INT1 *) "ret")))
                    {
                        pTmp = pTmp->pNext_token;
                    }

                    if (pTmp == NULL)
                    {
                        if (tmp_ptr == tmp_ptr->pNext_command)
                        {
                            break;
                        }

                        tmp_ptr = tmp_ptr->pNext_command;

                        continue;
                    }
                    i4Result = mmi_new_strcmp (token,
                                               (CONST INT1 *) tmp_ptr->
                                               i1pToken);
                    if ((i4Result != 0) && (i1ExactMatch == TRUE))
                    {
                        if (tmp_ptr == tmp_ptr->pNext_command)
                        {
                            break;
                        }

                        tmp_ptr = tmp_ptr->pNext_command;
                        continue;
                    }
                    pCliHelpInfo = pCliContext->pMmiCur_mode->pHelp;
                    while (pCliHelpInfo)
                    {
                        if ((pCliHelpInfo->i4ActNumStart <=
                             pTmp->i4Action_number)
                            && (pCliHelpInfo->i4ActNumEnd >=
                                pTmp->i4Action_number))
                        {
                            break;
                        }
                        pCliHelpInfo = pCliHelpInfo->pNext;
                    }

                    if ((pCliHelpInfo != NULL)
                        && (pCliContext->i1PrivilegeLevel >=
                            pCliHelpInfo->pPrivilegeId[pTmp->i2ModeIndex - 1]))
                    {
                        /* User privilege is >= command privilege. So copy the 
                         * command.Also init env */
                        pCliContext->pCliAmbVar->amb_ptr_start[ambig_count] =
                            tmp_ptr;
                        pCliContext->pCliAmbVar->
                            amb_ptr_start_flag[ambig_count] = (INT2) pri_value;
                        pCliContext->pCliAmbVar->amb_recur_flag[ambig_count] =
                            0;
                        pCliContext->pCliAmbVar->
                            amb_vpMmi_values[ambig_count][0] = (void *) token;
                        for (i = 1; i < MAX_NO_OF_TOKENS_IN_MMI; i++)
                        {
                            pCliContext->pCliAmbVar->
                                amb_vpMmi_values[ambig_count][i] = NULL;
                        }
                        if (i4Result == 0)
                        {
                            if (((i2TokenIndex) < pCliContext->i2TokenCount) &&
                                (tmp_ptr->pNext_token != NULL) &&
                                (mmi_strncmpi ((CONST INT1 *)
                                               tmp_ptr->pNext_token->i1pToken,
                                               (CONST INT1 *) pCliContext->
                                               MmiCmdToken[i2TokenIndex],
                                               CLI_STRLEN (pCliContext->
                                                           MmiCmdToken
                                                           [i2TokenIndex])) ==
                                 0))
                            {
                                i1ExactMatch = TRUE;
                            }
                        }
                        ambig_count++;
                    }
                    /*check for command PRIVID */
                    if (pCliHelpInfo != NULL)
                    {
                        u4PrivId =
                            pCliHelpInfo->pPrivilegeId[pTmp->i2ModeIndex - 1];
                    }

                }
                else
                {
                    *pi2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
                    return 0;
                }
            }
            if (tmp_ptr == tmp_ptr->pNext_command)
            {
                break;
            }
            tmp_ptr = tmp_ptr->pNext_command;    /* to get next command in group */
        }
        tmp_mode_list = tmp_mode_list->pNext;
        /* to get next group */
    }
    /*if AMBIG =0 and the command PRVID is > USR ID */
    if ((ambig_count == 0) && (pCliHelpInfo != NULL) &&
        (pCliContext->i1PrivilegeLevel < u4PrivId))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INSUFFICIENT_USER_PRIV);
    }
    pCliContext->present_ambig_count = mmi_reorganise_ambenv (pCliContext,
                                                              MAX_AMBIG_CMD);

    if ((pCliContext->present_ambig_count > 0) &&
        (pCliContext->i2TokenCount > 0))
    {
        pCliContext->amb_env_ptr_start =
            CliMemAllocMemBlk (gCliAmbPtrPoolId,
                               (sizeof (t_MMI_CMD **) * MAX_AMBIG_CMD));

        if (pCliContext->amb_env_ptr_start != NULL)
        {
            for (i2TokIndex = 0; i2TokIndex < pCliContext->present_ambig_count;
                 i2TokIndex++)
            {
                pCliContext->amb_env_ptr_start[i2TokIndex] =
                    CliMemAllocMemBlk (gCliAmbArrPoolId,
                                       (sizeof (t_MMI_CMD *) *
                                        MAX_NO_OF_TOKENS_IN_MMI));
                if (pCliContext->amb_env_ptr_start[i2TokIndex] == NULL)
                {
                    break;
                }
            }

            if ((i2TokIndex != pCliContext->present_ambig_count) &&
                (pCliContext->amb_env_ptr_start[i2TokIndex] == NULL))
            {
                for (i2Index = 0; i2Index < i2TokIndex; i2Index++)
                {
                    MemReleaseMemBlock (gCliAmbArrPoolId, (UINT1 *)
                                        pCliContext->
                                        amb_env_ptr_start[i2Index]);
                }
                MemReleaseMemBlock (gCliAmbPtrPoolId,
                                    (UINT1 *) pCliContext->amb_env_ptr_start);
                pCliContext->amb_env_ptr_start = NULL;
                return pCliContext->present_ambig_count;
            }

            for (i2Index = 0; i2Index < pCliContext->present_ambig_count;
                 i2Index++)
            {
                pCliContext->amb_env_ptr_start[i2Index][0] =
                    pCliContext->pCliAmbVar->amb_ptr_start[i2Index];
            }
        }
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return pCliContext->present_ambig_count;
}

/*
 * This is to parse the token in each ambiguous environment
 * This  will be called from mmi_parse () in mmi_read.c
 *
 * for each ambiguous environment it will copy the amb_parse_environment
 * to main_parse_environment and will start to parse
 * if the token succesfully parsed the corresponding 
 * amb_parse_environment will be updated
 * otherwise it will be left (by making pCliAmbVar->amb_ptr_start_flag[num] = 0) 
 *
 * at last those envronmenst made succeess will be collected
 * This will return the of present_amb_parse_environment
 */
/**** $$TRACE_PROCEDURE_NAME  = mmi_parse_continue  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
INT4
mmi_parse_continue (pCliContext, token, pi2ErrCode)
     tCliContext        *pCliContext;
     INT1               *token;
     INT2               *pi2ErrCode;
{
    INT4                i;
    INT4                i4Result = 0;
    INT2                tok_val = 0;    /* This counts number of ambiguity command
                                           calls the value function to convert the value */
    INT2                i2TokenIndex = 0;
    INT1                i1ExactMatch = FALSE;
    INT1                i1Count = 0;

    pCliContext->mp_i2set_flag = 0;

    for (i = 0; i < pCliContext->present_ambig_count; i++)
    {
        mmi_copy_ambenv_to_mpenv (pCliContext, i, pi2ErrCode);
        if ((pCliContext->env_ptr_start) != NULL)
        {
            pCliContext->env_ptr_start =
                pCliContext->env_ptr_start->pNext_token;
        }

        pCliContext->env_ptr_start = mmi_start_recur_check_parse (pCliContext,
                                                                  token,
                                                                  pCliContext->
                                                                  env_ptr_start,
                                                                  pi2ErrCode);

        if (pCliContext->env_ptr_start == NULL)
        {
            pCliContext->pCliAmbVar->amb_ptr_start_flag[i] = 0;
        }
        else
        {
            if (mmi_convert_value_totype_check
                (pCliContext, token, (INT1) tok_val) == 0)
                return 0;
            if (mmi_convert_value_totype_check
                (pCliContext, token, (INT1) tok_val) == -1)
            {
                *pi2ErrCode = MMI_GEN_ERR_TOKEN_AMBIGUITY;
                return -1;
            }

            if((pCliContext->env_ptr_start->i1pToken != NULL))
            {
                 if (0 != mmi_strncmpi ((CONST INT1 *) pCliContext->env_ptr_start->i1pToken,
                             token, CLI_STRLEN (token)))
                 {
                    continue;
                 }
            }
            if ((i4Result != 0) && (i1ExactMatch == FALSE) && (i1Count > 0))
            {
                pCliContext->pCliAmbVar->amb_ptr_start_flag[i] = 0;
            }
            tok_val++;
            mmi_copy_mpenv_to_ambenv (pCliContext, i, pi2ErrCode);
            if (i4Result == 0)
            {
                i1Count = (INT1) (i1Count + 1);
                for (i2TokenIndex = 0; i2TokenIndex < pCliContext->i2TokenCount;
                     i2TokenIndex++)
                {
                    if (mmi_new_strcmp (token,
                                        (CONST INT1 *) pCliContext->
                                        MmiCmdToken[i2TokenIndex]) == 0)
                    {
                        break;
                    }
                }
                i2TokenIndex++;
                if ((i2TokenIndex < pCliContext->i2TokenCount) &&
                    (pCliContext->env_ptr_start->pNext_token != NULL) &&
                    (mmi_strncmpi ((CONST INT1 *) pCliContext->env_ptr_start->
                                   pNext_token->i1pToken,
                                   (CONST INT1 *) pCliContext->
                                   MmiCmdToken[i2TokenIndex],
                                   CLI_STRLEN (pCliContext->
                                               MmiCmdToken[i2TokenIndex])) ==
                     0))
                {
                    i1ExactMatch = TRUE;
                }
            }
        }
    }
    pCliContext->present_ambig_count = mmi_reorganise_ambenv (pCliContext,
                                                              pCliContext->
                                                              present_ambig_count);
    return pCliContext->present_ambig_count;
}

/*
 * This is to convert the token according to its type and checks if its a valid value.
 */
INT4
mmi_convert_value_totype_check (tCliContext * pCliContext, INT1 *token,
                                INT1 flag)
{
    t_MMI_CMD          *tmp_ptr;
    INT4                i4ConflictTok = 0;
    INT4                i2Token_type_index;
    INT4                i4AllocLen = 0;
    INT4               *index_ptr;
    INT2                i;
    UINT2               u2HelpOpt = 0;
    CHR1               *i1pTokVal;
    INT4                i4Count = 0;
   /**** $$TRACE_LOG (ENTRY,"") ****/

    index_ptr = &(pCliContext->env_ptr_start->i4Action_number);
    if (*index_ptr >= MAX_NO_OF_TOKENS_IN_MMI)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_ILLEGAL_NO_TOKEN]);
      /**** $$TRACE_LOG (ABN_EXIT,"") ****/
        if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
        {
            mmi_help (pCliContext->MmiCmdToken[0], u2HelpOpt);
        }
        else
        {
            CliDisplayHelp (pCliContext->MmiCmdToken[0], u2HelpOpt, OSIX_FALSE);
        }

        return 0;
    }

    i2Token_type_index =
        find_pos_in_int (pCliContext->env_ptr_start->u8Typeof_token);

#ifdef CLI_DEBUG
    mmi_printf ("\r check %d %d: %d\r\n ",
                FSAP_U8_FETCH_HI (&
                                  (pCliContext->env_ptr_start->u8Typeof_token)),
                FSAP_U8_FETCH_LO (&
                                  (pCliContext->env_ptr_start->u8Typeof_token)),
                i2Token_type_index);
#endif

    if (pCliContext->i2Preindexvalue0 < *index_ptr)
    {
        for (i = (INT2) (pCliContext->i2Preindexvalue0 + 1); i < *index_ptr;
             i++)

        {
            pCliContext->vpMmi_values[i] = (void *) NULL;
        }
        pCliContext->i2Preindexvalue0 = *index_ptr;

    }
    if (pCliContext->mp_i2recur_flag <= 0)
    {
        pCliContext->i2Preindexvalue0 = *index_ptr;
    }

    if ((i2Token_type_index >= 0) &&
        (mmi_cmd_token[i2Token_type_index].value_function != NULL))
    {

#ifdef CLI_DEBUG
        mmi_printf ("\rVALUE Fun %d : for index %d\r\n", i2Token_type_index,
                    *index_ptr);
#endif
        if (flag)
        {
#ifdef CLI_DEBUG
            mmi_printf
                ("\rWarning more commands having same type of token check the \
         tokentypes\r\n");
#endif
        }

        i4AllocLen = STRLEN (token);
        i4AllocLen = (i4AllocLen > CLI_MAX_TOKEN_VAL_MEM) ?
            (i4AllocLen) : (CLI_MAX_TOKEN_VAL_MEM);
        if (!(CLI_ALLOC_MAXLINE_MEM_BLOCK (i1pTokVal, CHR1)))
        {
            return 0;
        }

        STRCPY (i1pTokVal, token);

        if (mmi_cmd_token[i2Token_type_index].
            value_function ((INT1 *) i1pTokVal) == (INT4) OSIX_FAILURE)
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (i1pTokVal);
            i1pTokVal = NULL;
            return 0;
        }

        CLI_RELEASE_MAXLINE_MEM_BLOCK (i1pTokVal);
        i1pTokVal = NULL;
        pCliContext->vpMmi_values[*index_ptr] = (VOID *) token;
    }
    else
    {
        tmp_ptr = pCliContext->env_ptr_start;
        do
        {
            if (i4Count++ >= MAX_AMBIG_CMD)
            {
                break;
            }
            if (tmp_ptr != NULL)
            {
                if (mmi_strcmpi ((CONST INT1 *) tmp_ptr->i1pToken, token) == 0)
                {
                    break;
                }
                else if (mmi_strncmpi ((CONST INT1 *) tmp_ptr->i1pToken,
                                       token, CLI_STRLEN (token)) == 0)
                {
                    i4ConflictTok++;
                }
                if (i4ConflictTok > 1 && pCliContext->u1InputChar != TAB)
                {
                    return -1;
                }
                if (tmp_ptr == tmp_ptr->pOptional)
                {
                    break;
                }
                if (tmp_ptr->pNext_command)
                {
                    tmp_ptr = tmp_ptr->pNext_command;
                }
                else
                {
                    tmp_ptr = tmp_ptr->pOptional;
                }
            }
        }
        while ((tmp_ptr != NULL) && (tmp_ptr != pCliContext->env_ptr_start));
        pCliContext->vpMmi_values[*index_ptr] = (void *) token;
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return 1;
}

/*
 * This is to convert the token according to its type
 * and make the *vpMmi_values[index] = token;
 */
INT4
mmi_convert_value_totype (tCliContext * pCliContext, INT1 *token, INT1 flag)
{
    INT4                i2Token_type_index;
    INT4               *index_ptr;
    INT2                i;
    UINT2               u2HelpOpt = 0;
    INT4                i4AllocLen = 0;
    CHR1               *i1pTokVal;

   /**** $$TRACE_LOG (ENTRY,"") ****/

    index_ptr = &(pCliContext->env_ptr_start->i4Action_number);
    if (*index_ptr >= MAX_NO_OF_TOKENS_IN_MMI)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_ILLEGAL_NO_TOKEN]);
      /**** $$TRACE_LOG (ABN_EXIT,"") ****/
        if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
        {
            mmi_help (pCliContext->MmiCmdToken[0], u2HelpOpt);
        }
        else
        {
            CliDisplayHelp (pCliContext->MmiCmdToken[0], u2HelpOpt, OSIX_FALSE);
        }
        return 0;
    }

    i2Token_type_index =
        find_pos_in_int (pCliContext->env_ptr_start->u8Typeof_token);
#ifdef CLI_DEBUG
    mmi_printf ("\r check %d %d: %d\r\n ",
                FSAP_U8_FETCH_HI (&
                                  (pCliContext->env_ptr_start->u8Typeof_token)),
                FSAP_U8_FETCH_LO (&
                                  (pCliContext->env_ptr_start->u8Typeof_token)),
                i2Token_type_index);
#endif

    if (pCliContext->i2Preindexvalue1 < *index_ptr)
    {
        for (i = (INT2) (pCliContext->i2Preindexvalue1 + 1); i < *index_ptr;
             i++)
        {
            pCliContext->vpMmi_values[i] = (void *) NULL;
        }
        pCliContext->i2Preindexvalue1 = *index_ptr;
    }
    if (pCliContext->mp_i2recur_flag <= 0)
    {
        pCliContext->i2Preindexvalue1 = *index_ptr;
    }

    if ((i2Token_type_index >= 0) &&
        (mmi_cmd_token[i2Token_type_index].value_function != NULL))
    {

#ifdef CLI_DEBUG
        mmi_printf ("\rVALUE Fun %d : for index %d\r\n", i2Token_type_index,
                    *index_ptr);
#endif
        if (!flag)
        {
            /* Memory needed to convert the token type in to token value for
             * integer, other token tpyes in estimated based on the token types handled.
             * The maximum memory needed is estimated as Max of 'CLI_MAX_TOKEN_VAL_MEM' which is '50' or STRLEN of token. 
             * But the actual value used is less than that nearly 10 bytes
             */
            i4AllocLen = STRLEN (token);
            i4AllocLen = (i4AllocLen > CLI_MAX_TOKEN_VAL_MEM) ?
                (i4AllocLen) : (CLI_MAX_TOKEN_VAL_MEM);
            if (!(CLI_ALLOC_MAXLINE_MEM_BLOCK (i1pTokVal, CHR1)))
            {
                return 0;
            }

            STRCPY (i1pTokVal, token);

            if (mmi_cmd_token[i2Token_type_index].
                value_function ((INT1 *) i1pTokVal) == (INT4) OSIX_FAILURE)
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK (i1pTokVal);
                i1pTokVal = NULL;
                return 0;
            }

            pCliContext->ai1Tokentype_flag[*index_ptr] = 1;
            pCliContext->vpMmi_values[*index_ptr] = (VOID *) i1pTokVal;
        }
        else
        {
#ifdef CLI_DEBUG
            mmi_printf
                ("\rWarning more commands having same type of token check the \
         tokentypes\r\n");
#endif
        }
    }
    else
    {
        pCliContext->vpMmi_values[*index_ptr] = (void *) token;
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
    return 1;
}

/**** $$TRACE_PROCEDURE_NAME  = mmi_print_cmdargs_afterparse  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
VOID
mmi_print_cmdargs_afterparse (pCliContext)
     tCliContext        *pCliContext;
{
    INT2                i;
   /**** $$TRACE_LOG (ENTRY,"") ****/
    for (i = 0; i < MAX_NO_OF_TOKENS_IN_MMI; i++)
    {
        if (pCliContext->vpMmi_values[i] != NULL)
        {
            mmi_printf (" %d : %s\r\n", i, pCliContext->vpMmi_values[i]);
        }
        else
        {
            mmi_printf (" %d : NULL\r\n", i);
        }
    }
   /**** $$TRACE_LOG (EXIT,"") ****/
}

/*
 * This will be called after fully parsed
 * 
 * 1. this will copy amb_parse_environment[1] to main_parse_environ-
 *    ment varibles
 * 2. it will check the next_token of env_ptr_start tobe 'ret'
 * 3. if ok this will pass the action number to
 *    mmi_action_procedure () 
 */

/**** $$TRACE_PROCEDURE_NAME  = mmi_do_action  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
INT4
mmi_do_action (pCliContext, pi4ActionNumber, pi2ModeIndex, pi2ErrCode)
     tCliContext        *pCliContext;
     INT4               *pi4ActionNumber;
     INT2               *pi2ModeIndex;
     INT2               *pi2ErrCode;
{
    t_MMI_CMD          *tmp_ptr = NULL;
    t_MMI_CMD          *inter_ptr = NULL;
    INT1                i1Count;
    UINT1              *pu1Cmd = NULL;
    UINT1              *pu1Temp = NULL;
    INT2                i2Index;
    INT4                pri_select = 0;
    INT4                i4test = MMI_GEN_ERR_INCOMPLETE_COMMAND;
    INT4                i4ConflictCnt = FALSE;
    INT4                i4CmdIndex = -1;
    INT4                i4ExactCmdFound = FALSE;
    INT4                i4ExactCmdIndex = -1;
    INT4                i4_check_optional_flag = 0;
    UINT1               u1Size = 0;
    UINT1               u1Len = 0;
    UINT1              *pu1Pos = NULL;
    INT4                i4OptionalCmd = 0;

    pCliContext->pu1Cmd = CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN+1);
    if (pCliContext->pu1Cmd == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for command String\n");
        return pri_select;
    }
    pu1Cmd = pCliContext->pu1Cmd;

    pCliContext->pu1Temp = CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN+1);
    if (pCliContext->pu1Temp == NULL)
    {
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Cmd);
        mmi_printf ("\r\n Cannot allocate memory for command String\n");
        return pri_select;
    }
    pu1Temp = pCliContext->pu1Temp;

    if (pCliContext->present_ambig_count != 1)
    {
        if (!pCliContext->present_ambig_count)
        {
            *pi2ErrCode = -(MMI_GEN_ERR_INVALID_COMMAND);
            CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Cmd);
            CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Temp);
            return pri_select;
        }

        CLI_MEMSET (pu1Cmd, 0, MAX_LINE_LEN+1);

        /* We copy the last token of the command into 'pu1Cmd'
         * as we compare this token for ambiguity and
         * the ambiguity for complete command is checked in
         * function MmiDoCheckAllTokens.
         */

        for (i2Index = 0; i2Index < pCliContext->present_ambig_count; i2Index++)
        {
            if (!(pCliContext->pCliAmbVar->amb_ptr_start[i2Index]))
            {
                continue;
            }
            if (pCliContext->pCliAmbVar->amb_ptr_start[i2Index]->i1pToken !=
                NULL)
            {
                if (!(mmi_new_strcmp ((CONST INT1 *)
                                      pCliContext->
                                      MmiCmdToken[(pCliContext->i2TokenCount -
                                                   1)],
                                      (CONST INT1 *) pCliContext->
                                      pCliAmbVar->amb_ptr_start[i2Index]->
                                      i1pToken)))
                {
                    if (i4ExactCmdFound == TRUE)
                    {
                        i4ExactCmdFound = FALSE;
                    }
                    else if (i4ExactCmdIndex < 0)
                    {
                        i4ExactCmdFound = TRUE;
                        i4ExactCmdIndex = i2Index;
                    }
                }
            }

            if (pCliContext->mp_i2recur_flag <= 0)
            {
                if (pCliContext->pCliAmbVar->amb_ptr_start[i2Index]->
                    pNext_token)
                {
                    if (!(mmi_new_strcmp ((CONST INT1 *)
                                          pCliContext->pCliAmbVar->
                                          amb_ptr_start[i2Index]->pNext_token->
                                          i1pToken, (CONST INT1 *) "ret")))
                    {
                        if (i4CmdIndex < 0)
                        {
                            i4ConflictCnt = FALSE;
                            i4CmdIndex = i2Index;
                        }
                        else
                            i4ConflictCnt = TRUE;
                    }
                    else if (pCliContext->pCliAmbVar->amb_ptr_start[i2Index]->
                             pNext_token->pOptional)
                    {
                        tmp_ptr =
                            pCliContext->pCliAmbVar->amb_ptr_start[i2Index]->
                            pNext_token->pOptional;
                        i4OptionalCmd = 0;
                        while (tmp_ptr != NULL)
                        {
                            if (i4OptionalCmd > CLI_OPTNLTKNS)
                            {
                                i4ConflictCnt = TRUE;
                                break;
                            }
                            i4OptionalCmd++;
                            if (mmi_new_strcmp
                                ((CONST INT1 *) tmp_ptr->i1pToken,
                                 (CONST INT1 *) "ret") == 0)
                            {
                                if (i4CmdIndex < 0)
                                {
                                    i4ConflictCnt = FALSE;
                                    i4CmdIndex = i2Index;
                                }
                                else
                                    i4ConflictCnt = TRUE;
                                break;
                            }
                            tmp_ptr = tmp_ptr->pOptional;
                        }
                    }
                }
            }
            else
            {
                i4ConflictCnt = TRUE;
            }
        }
        if ((i4ConflictCnt) && (i4ExactCmdFound == FALSE))
        {
            /* check for finding exact match for all/max. no of 
             * tokens in a command in case of ambiguous commands 
             * returns TRUE if any one of the command in the 
             * ambiguity commands has max. no. of tokens matchs
             * exactly with the given input command.
             * else returns FALSE.
             */
            if (pCliContext->mp_i2recur_flag <= 0)
            {
                if (MmiDoCheckAllTokens (pCliContext, &i4ExactCmdIndex,
                                         pi2ErrCode) == FALSE)
                {
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Cmd);
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Temp);
                    return pri_select;
                }
            }
            i4CmdIndex = i4ExactCmdIndex;
        }
        else if ((i4ConflictCnt) && (i4ExactCmdFound == TRUE))
        {
            i4CmdIndex = i4ExactCmdIndex;
        }
        if (i4CmdIndex < 0)
            i4CmdIndex = 0;
        pri_select = i4CmdIndex;
    }

    mmi_copy_ambenv_to_mpenv (pCliContext, pri_select, pi2ErrCode);
    /*   mmi_print_cmdargs_afterparse (); */
    tmp_ptr = pCliContext->env_ptr_start->pNext_token;
    if (pCliContext->mp_i2recur_flag)
    {
        if (mmi_node_is_in_recur (pCliContext, tmp_ptr) >= 0)
        {                        /* check assumed for 
                                   mp_i2recur_flag = 1 */
            tmp_ptr = pCliContext->p_mp_recur_node[1]->pNext_token;
        }
    }
    inter_ptr = tmp_ptr;
    while (inter_ptr != NULL)
    {
        tmp_ptr = inter_ptr;
        while (tmp_ptr->pOptional != NULL)
        {
            i4_check_optional_flag = 1;
            if (tmp_ptr == tmp_ptr->pOptional)
            {
#ifdef CLI_DEBUG
                mmi_printf ("\r\n Parser is looping \r\n");
#endif
                break;
            }
            if (mmi_new_strcmp ((CONST INT1 *) tmp_ptr->i1pToken,
                                (CONST INT1 *) "ret") == 0)
            {
                break;
            }
            tmp_ptr = tmp_ptr->pOptional;
        }
        if (i4_check_optional_flag == 1 && inter_ptr->pNext_command != NULL)
        {
            tmp_ptr = inter_ptr->pNext_command;
        }
        if (mmi_new_strcmp ((CONST INT1 *) tmp_ptr->i1pToken,
                            (CONST INT1 *) "ret") == 0)
        {
            break;
        }
        if (mmi_new_strcmp ((CONST INT1 *) inter_ptr->i1pToken,
                            (CONST INT1 *) "recur") == 0)
        {
            break;
        }
        inter_ptr = inter_ptr->pNext_command;
    }

    if (!mmi_new_strcmp ((CONST INT1 *) tmp_ptr->i1pToken,
                         (CONST INT1 *) "ret"))
    {
        /* Addition for CMD_LOG */

        CLI_MEMSET (pu1Cmd, 0, MAX_LINE_LEN+1);
        CLI_MEMSET (pu1Temp, 0, MAX_LINE_LEN+1);

        for (i1Count = 0; pCliContext->MmiCmdToken[i1Count][0]; ++i1Count)
        {
            SNPRINTF ((CHR1 *) pu1Temp, (sizeof (INT1) * MAX_LINE_LEN) - 1,
                      "%s%s", pCliContext->MmiCmdToken[i1Count], " ");
            pu1Temp[(sizeof (INT1) * MAX_LINE_LEN) - 1] = '\0';

            if ((CLI_STRLEN (pu1Cmd) + CLI_STRLEN (pu1Temp)) <
                ((sizeof (INT1) * MAX_LINE_LEN) - 1))
            {
                u1Len = STRLEN (pu1Cmd);
                u1Size = ((sizeof (INT1) * MAX_LINE_LEN) - 1) - u1Len;
                pu1Pos = &(pu1Cmd[u1Len]);
                STRNCPY (pu1Pos, pu1Temp, u1Size);
                pu1Cmd[(sizeof (INT1) * MAX_LINE_LEN) - 1] = '\0';
            }
            else
            {
                mmi_printf ("\r\nCommand Length Exceeded !!!\r\n");
            }
        }
        pu1Cmd[CLI_STRLEN (pu1Cmd) - 1] = '\0';
        CliValidateSyslogMsg (pCliContext, pu1Cmd);
        /* Addition for syslogs */
        if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
        {
            SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, pCliContext->mmi_i1user_index,
                          "%s %s %s:%s", "Telnet", pCliContext->ai1UserName,
                          "SUCCESS", pu1Cmd));
        }
        else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
        {
            SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, pCliContext->mmi_i1user_index,
                          "%s %s %s:%s", "SSH", pCliContext->ai1UserName,
                          "SUCCESS", pu1Cmd));
        }
        else
        {
            SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, pCliContext->mmi_i1user_index,
                          "%s %s %s:%s", "Console", pCliContext->ai1UserName,
                          "SUCCESS", pu1Cmd));

        }
        *pi4ActionNumber = tmp_ptr->i4Action_number;
        *pi2ModeIndex = tmp_ptr->i2ModeIndex;

    }
    else
    {
        *pi2ErrCode = (INT2) -i4test;
    }
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Cmd);
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1Temp);
    return pri_select;
}

/*
 * After all mmi_variables initialised this will be called.
 *
 * This  will get the token and token type i1Lex_return_type
 * by calling mmi_gettoken (buf) . buf will be the MmiCmdToken[token_count]
 *
 * mmi_gettoken () will return white_space or -1
 *
 * according to the lex_return type cases
 *  1. if -1 break continue to next command
 *  2. if token_count==0 and lex_return = '\n'
 *        if empty token  continue to next command
 *  3. if token_count == 0 
 *        call mmi_first_tok_parse () 
 *        according to the parse_return
 *             if parse_return = 0
 *                read_until '\n' continue to next_command
 *  4. if token_count > 0
 *       call mmi_parse_continue () 
 *            if parse_return = 0
 *               read_until '\n' continue to next_command
 *  5. if lex_return = '\n' 
 *        if parse_return = 1
 *           mmi_do_action () 
 *        else print ambiguity command
 *
 */
INT2
mmi_parse (tCliContext * pCliContext, INT1 *pi1UserCmd, INT4 *pi4ActionNumber,
           INT2 *pi2ModeIndex, INT1 **ppi1RemCommand, INT1 i1ErrFlag)
    /* i1ErrFlag: if set displays error messages */
{
    INT4                parse_return;
    INT4                i4UnAmbigIndex = -1;
    INT4                i4FirstTokenAmbigCount = 0;
    INT4                i4TempWriteFd = 0;
    INT2                i2token_index = 0;
    INT2                i2Index = 0;
    INT2                i2ErrCode = CLI_SUCCESS;
    INT1                lex_return;
    INT1               *pi1TempUserCmd = NULL;
    INT1               *pi1RemCmd = NULL;
    INT1               *pi1CmdHelp = NULL;    /* Used to call mmi_help() */
    INT1               *pi1RangeStr = NULL;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT2               u2HelpOpt = 0;
    INT4                i4Flag = 0;
    UINT4               u4StrLen = CLI_SUCCESS;
    INT1                i1FileFlag = FALSE;
    UINT2               u2Len = 0;


    if (!(*pi1UserCmd))
    {
        CliInitMmiVal (pCliContext);
        return 0;
    }

    do
    {
        CliInitMmiVal (pCliContext);
        pi1TempUserCmd = pi1UserCmd;
        lex_return = mmi_gettoken (pCliContext, pi1UserCmd, &pi1RemCmd);

        if (lex_return == CLI_PIPE_CHAR)
        {
            if ((i4RetVal =
                 CliCheckPipeSyntax (pCliContext, (UINT1 *) pi1RemCmd,
                                     &i2ErrCode)) == CLI_FAILURE)
            {
                CliGetNextCommand (pi1RemCmd, &pi1RemCmd);
                break;
            }
            else
            {
                while (*pi1TempUserCmd != '|')
                    pi1TempUserCmd++;
                if (ISALPHA (*(pi1TempUserCmd - 1)))
                {
                    pCliContext->i4CmdErrCode = TRUE;
                    pCliContext->u1GrepErrorFlag = TRUE;
                    mmi_printf (mmi_gen_messages[MMI_GEN_ERR_INVALID_COMMAND]);
                    pCliContext->u1GrepErrorFlag = FALSE;

                    return pCliContext->i2TokenCount;
                }
            }
            /* Create pipe if command contains MORE and
             * i1DefaultMoreFlag not set to CLI_MORE_ENABLE
             */

            CliCreatePipe (pCliContext);
            CliGetNextCommand (pi1RemCmd, &pi1RemCmd);
        }

        if ((i1FileFlag == FALSE) && ((STRCMP
                                       (pCliContext->
                                        MmiCmdToken[pCliContext->i2TokenCount -
                                                    CLI_REDIR_TOK], ">") == 0)
                                      ||
                                      (STRCMP
                                       (pCliContext->
                                        MmiCmdToken[pCliContext->i2TokenCount -
                                                    CLI_REDIR_TOK],
                                        ">>")) == 0))
        {
            pCliContext->i4FileFlag = TRUE;
            /* to fix klocwork warning */
            i1FileFlag = TRUE;
            MEMSET (pCliContext->au1FileName, '\0', CLI_FILE_LEN);

            if (STRLEN (pCliContext->MmiCmdToken[pCliContext->i2TokenCount - 1])
                >= CLI_FILE_LEN)
            {
                mmi_printf ("\r\nFile size exceeded !!!\r\n");
                pCliContext->i4FileFlag = FALSE;
                pi1RemCmd = NULL;
                pi1UserCmd = pi1RemCmd;
                return -1;
            }
            u4StrLen =
                STRLEN (pCliContext->
                        MmiCmdToken[pCliContext->i2TokenCount - 1]);
            if (u4StrLen <= CLI_FILE_LEN)
            {
                STRNCPY (pCliContext->au1FileName,
                         pCliContext->MmiCmdToken[pCliContext->i2TokenCount -
                                                  1], u4StrLen);
            }
            else
            {
                STRNCPY (pCliContext->au1FileName,
                         pCliContext->MmiCmdToken[pCliContext->i2TokenCount -
                                                  1], CLI_FILE_LEN);
            }

            pCliContext->au1FileName[CLI_FILE_LEN - 1] = '\0';
            if (STRCMP
                (pCliContext->
                 MmiCmdToken[pCliContext->i2TokenCount - CLI_REDIR_TOK],
                 ">") == 0)
            {
                i4Flag = OSIX_FILE_WO | OSIX_FILE_CR;
            }
            else if (STRCMP
                     (pCliContext->
                      MmiCmdToken[pCliContext->i2TokenCount - CLI_REDIR_TOK],
                      ">>") == 0)
            {
                i4Flag = OSIX_FILE_WO | OSIX_FILE_CR | OSIX_FILE_AP;
            }

            if ((i4TempWriteFd =
                 FileOpen ((CONST UINT1 *) pCliContext->au1FileName,
                           i4Flag)) < 0)
            {
                mmi_printf ("\r\nUnable to open file!!!\r\n");
                pCliContext->i4FileFlag = FALSE;
                pi1RemCmd = NULL;
                pi1UserCmd = pi1RemCmd;
                return -1;
            }
            else
            {
                if (!CliIsRegularFile ((CONST CHR1 *) pCliContext->au1FileName))
                {
                    mmi_printf ("\r\nOutput File is not a regular file!!!\r\n");
                    pCliContext->i4FileFlag = FALSE;
                    FileClose (i4TempWriteFd);
                    return -1;
                }

                pCliContext->i4ContextWriteFd = pCliContext->i4OutputFd;
                pCliContext->i4OutputFd = i4TempWriteFd;
                pCliContext->pTempOutputFp = pCliContext->fpCliOutput;
                pCliContext->fpCliOutput = pCliContext->fpCliFileWrite;
            }
            MEMSET (pCliContext->
                    MmiCmdToken[pCliContext->i2TokenCount - CLI_REDIR_TOK],
                    '\0', MAX_CHAR_IN_TOKEN);
            MEMSET (pCliContext->MmiCmdToken[pCliContext->i2TokenCount - 1],
                    '\0', MAX_CHAR_IN_TOKEN);
            pCliContext->i2TokenCount =
                pCliContext->i2TokenCount - CLI_REDIR_TOK;
        }

        for (i2token_index = 0; i2token_index < pCliContext->i2TokenCount;
             i2token_index++)
        {
            if (i2token_index == 0)
            {
                /* case 3 */
                parse_return = mmi_first_tok_parse (pCliContext,
                                                    pCliContext->
                                                    MmiCmdToken[i2token_index],
                                                    &i2ErrCode);

                if (parse_return > MAX_AMBIG_CMD)
                {
                    i2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
                    pCliContext->i4CmdErrCode = TRUE;
                    break;
                }
                if ((!pCliContext->present_ambig_count) &&
                    (i2ErrCode != -(MMI_GEN_ERR_INSUFFICIENT_USER_PRIV)))
                {
                    i2ErrCode = -(MMI_GEN_ERR_INVALID_COMMAND);
                    pCliContext->i4CmdErrCode = TRUE;
                    break;
                }
                else if ((!pCliContext->present_ambig_count) &&
                         (i2ErrCode == -(MMI_GEN_ERR_INSUFFICIENT_USER_PRIV)))
                {
                    break;
                }
                i4FirstTokenAmbigCount = pCliContext->present_ambig_count;
            }
            else
            {
                parse_return = mmi_parse_continue (pCliContext,
                                                   pCliContext->
                                                   MmiCmdToken[i2token_index],
                                                   &i2ErrCode);
                if (parse_return == -1)
                {
                    mmi_printf ("\r\n");
                    mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_AMBIGUITY]);
                    break;
                }
                if ((parse_return == 0) && (lex_return != 0) && (i1ErrFlag))
                {
                    pCliContext->i4CmdErrCode = TRUE;
                    mmi_print_error_in_token (pCliContext, i2token_index);
                    pCliContext->u4CmdStatus = CLI_FAILURE;
                    break;
                }
                if (pCliContext->amb_env_ptr_start != NULL)
                {
                    for (i2Index = 0;
                         i2Index < pCliContext->present_ambig_count; i2Index++)
                    {
                        pCliContext->amb_env_ptr_start[i2Index][i2token_index] =
                            pCliContext->pCliAmbVar->amb_ptr_start[i2Index];
                    }
                }
            }
            if (i2token_index == (pCliContext->i2TokenCount - 1))
            {
                if ((CliIsDelimit (lex_return, CLI_EOC_DELIMITS))
                    || (lex_return == CLI_PIPE_CHAR))
                {
                    /* case 5 */
                    i4UnAmbigIndex =
                        mmi_do_action (pCliContext, pi4ActionNumber,
                                       pi2ModeIndex, &i2ErrCode);

                    /* ai1FirstToken is used to store first token of the 
                     * user command in expanded form. sh will be stored as
                     * show */
                    if (i4UnAmbigIndex >= 0)
                    {

                        MEMSET (pCliContext->ai1FirstToken, '\0', MAX_LINE_LEN);
                        if (pCliContext->amb_env_ptr_start != NULL)
                        {
                            u2Len =
                                (UINT2) ((sizeof(pCliContext->ai1FirstToken)-1) <
                                        STRLEN (pCliContext->
                                            amb_env_ptr_start[i4UnAmbigIndex][0]->
                                            i1pToken) ?
                                        (sizeof(pCliContext->ai1FirstToken)-1) :
                                        STRLEN (pCliContext->
                                            amb_env_ptr_start[i4UnAmbigIndex][0]->
                                            i1pToken));

                            STRNCPY (pCliContext->ai1FirstToken,
                                     pCliContext->
                                     amb_env_ptr_start[i4UnAmbigIndex][0]->
                                     i1pToken,u2Len);
			    pCliContext->ai1FirstToken[u2Len] = '\0';
                        }
                    }
                }
                break;
            }

            /* check valid no of tokens */

            if ((i2token_index >= MAX_NO_OF_TOKENS_IN_MMI) &&
                lex_return != '\n')
            {
                mmi_printf (mmi_gen_messages[MMI_GEN_ERR_ILLEGAL_NO_TOKEN]);
                break;
            }
        }

        pi1UserCmd = pi1RemCmd;
    }
    while (((!CliIsDelimit (lex_return, CLI_EOC_DELIMITS))
            && (lex_return != CLI_PIPE_CHAR))
           && (pi1UserCmd) && (*pi1UserCmd) && (i2ErrCode >= 0));

    if (lex_return == '\n')
        *ppi1RemCommand = NULL;
    else
        *ppi1RemCommand = pi1RemCmd;

    pi1RemCmd = NULL;
    pi1UserCmd = pi1RemCmd;
    pCliContext->fpCliIoctl (pCliContext, CLI_FLUSH_OUTPUT, NULL);

    if (!(i2ErrCode < 0) && (i4UnAmbigIndex >= 0) &&
        (pCliContext->amb_env_ptr_start != NULL))
    {
        MEMSET (pCliContext->ai1OrgCmdToken, 0, MAX_LINE_LEN);
        for (i2Index = 0; i2Index < pCliContext->i2TokenCount; i2Index++)
        {
            pCliContext->env_ptr_start =
                pCliContext->amb_env_ptr_start[i4UnAmbigIndex][i2Index];
            if (pCliContext->env_ptr_start == NULL)
            {
                continue;
            }

            /* Forming expanded Command for isscust.c usage */
            if (i2Index != 0)
            {
                STRCAT (pCliContext->ai1OrgCmdToken, " ");
            }

            if (pCliContext->env_ptr_start->i1pToken != NULL)
            {
            u2Len =
                (UINT2) ((sizeof(pCliContext->ai1OrgCmdToken)-STRLEN(pCliContext->ai1OrgCmdToken)-1) <
                        STRLEN (pCliContext->env_ptr_start->i1pToken) ?
                       (sizeof(pCliContext->ai1OrgCmdToken)-STRLEN(pCliContext->ai1OrgCmdToken)-1) :
                       STRLEN (pCliContext->env_ptr_start->i1pToken) ) ;

                STRNCAT (pCliContext->ai1OrgCmdToken,
                         pCliContext->env_ptr_start->i1pToken,
                         u2Len);
            }
            else
            {
                STRCAT (pCliContext->ai1OrgCmdToken,
                        pCliContext->MmiCmdToken[i2Index]);
            }

            if (mmi_convert_value_totype (pCliContext,
                                          pCliContext->MmiCmdToken[i2Index],
                                          0) == 0)
            {
                if ((lex_return != 0) && (i1ErrFlag))
                {
                    i2ErrCode = -(MMI_GEN_ERR_INVALID_COMMAND);
                    *pi4ActionNumber = -1;
                    pCliContext->i4CmdErrCode = TRUE;
                    mmi_print_error_in_token (pCliContext, i2Index);
                    break;
                }
            }
            /* To call the function to Check for Validity or
             * Range values of the given input                 */

            if ((pCliContext->env_ptr_start->pRangeChkFunc != NULL) &&
                (i1ErrFlag))
            {
                /* Calls the range or validity check function */

                i2ErrCode = i2Index;
                pi1RangeStr = pCliContext->env_ptr_start->pRangeChkFunc
                    (pCliContext->env_ptr_start,
                     pCliContext->MmiCmdToken[i2Index], &i2ErrCode);
                if (i2ErrCode < 0)
                {
                    if ((i2ErrCode != -(MMI_GEN_ERR_INVALID_IFACE_NUM)) &&
                        (i2ErrCode != -(MMI_GEN_ERR_WRONG_IFTYPE_FOR_PORT)))
                    {
                        pCliContext->i4CmdErrCode = TRUE;
                        mmi_print_error_in_token (pCliContext, i2Index);
                    }
                    *pi4ActionNumber = -1;
                    break;
                }
            }
        }
    }
    if (pCliContext->amb_env_ptr_start != NULL)
    {
        for (i2Index = 0; i2Index < i4FirstTokenAmbigCount; i2Index++)
        {
            MemReleaseMemBlock (gCliAmbArrPoolId, (UINT1 *)
                                pCliContext->amb_env_ptr_start[i2Index]);
        }
        MemReleaseMemBlock (gCliAmbPtrPoolId, (UINT1 *)
                            pCliContext->amb_env_ptr_start);
        pCliContext->amb_env_ptr_start = NULL;
    }
    if (i2ErrCode < 0 && i1ErrFlag)
    {
        /* Free the memory alocated for value type tokens (float, int) */
        for (i2token_index = 0; i2token_index < MAX_NO_OF_TOKENS_IN_MMI;
             i2token_index++)
        {
            if (pCliContext->ai1Tokentype_flag[i2token_index])
            {
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                                   vpMmi_values[i2token_index]);
                pCliContext->ai1Tokentype_flag[i2token_index] = 0;
                pCliContext->vpMmi_values[i2token_index] = NULL;
            }
        }

        if (pi1RangeStr != NULL)
        {
            /* Invalid input/Out of range value,
             * pi1RangeStr will contain the error description/valid range
             *
             * mmi_printf (mmi_gen_messages[-i2ErrCode], pi1RangeStr);
             */

            /* Frees memory allocated in RangeCheck Function in clieval.c */
            CLI_BUDDY_FREE (pi1RangeStr);
        }
        else
        {
            pCliContext->i4CmdErrCode = TRUE;
            mmi_printf (mmi_gen_messages[-i2ErrCode]);
            if (-i2ErrCode == MMI_GEN_ERR_INSUFFICIENT_USER_PRIV)
            {
                return 0;
            }
        }
        CliRestoreCxtFileInfo (pCliContext);

        if ((-i2ErrCode == MMI_GEN_ERR_COMMAND_AMBIGUITY) ||
            (-i2ErrCode == MMI_GEN_ERR_INCOMPLETE_COMMAND))
        {
            if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
            {
                CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1CmdHelp, INT1);

                if (pi1CmdHelp != NULL)
                {
                    pi1CmdHelp[0] = '\0';
                    for (i2token_index = 0;
                         pCliContext->MmiCmdToken[i2token_index + 1][0];
                         i2token_index++)
                    {
                        SPRINTF ((CHR1 *) pi1CmdHelp, "%s%s ",
                                 (CHR1 *) pi1CmdHelp,
                                 (CHR1 *) pCliContext->
                                 MmiCmdToken[i2token_index]);
                    }
                    STRCAT (pi1CmdHelp,
                            pCliContext->MmiCmdToken[i2token_index]);

                    u2HelpOpt = 0;
                    if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
                    {
                        mmi_help (pi1CmdHelp, u2HelpOpt);
                    }
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1CmdHelp);
                }
            }
        }
        /* Delete pipe for unsuccessful commands */
        if (pCliContext->i1PipeFlag == TRUE)
        {
            CliClosePipe (pCliContext);
        }
    }
    return pCliContext->i2TokenCount;
}

VOID
mmi_exit (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    CliDeletePipe (pCliContext);
    pCliContext->i1UserAuth = FALSE;
    pCliContext->i1passwdMode = LOCAL_LOGIN;

    /* 
     * In SSH mode, connection will be lost if login is incorrect.
     * So, mmi_printf wont work 
     */
    if (pCliContext->gu4CliMode != CLI_MODE_SSH)
    {
        /* restore the default back ground */
        mmi_printf ("%s", CLI_DEFAULT_BG);
    }
    if (pCliContext->u1CliLogOutMessage == CLI_FORCED_LOGOUT_MSG)
    {
        CliSendAuditLoginInfo (pCliContext, FORCED_LOG_OUT);
        mmicmdlog_user_logout (pCliContext, FORCED_LOG_OUT);
        pCliContext->u1CliLogOutMessage = CLI_IDLE_TIME_EXPIRY_MSG;
    }
    else if (pCliContext->u1CliLogOutMessage == CLI_IDLE_TIME_EXPIRY_MSG)
    {
        CliSendAuditLoginInfo (pCliContext, LOG_OUT);
        mmicmdlog_user_logout (pCliContext, LOG_OUT);
    }
    /* Due to context clean up user name is not available for syslog
     * Hence Context Clean up done after logout*/	
    CliContextLock ();
    CliContextCleanup (pCliContext);
    CliContextUnlock ();
    /*Memset the username for user first */
    CLI_MEMSET (pCliContext->ai1UserName, 0, MAX_USER_NAME_LEN);

}

VOID
mmi_reenter (pCliContext)
     tCliContext        *pCliContext;
{
  /**** $$TRACE_LOG (ENTRY,"") ****/
    pCliContext->mmi_exit_flag = 0;
  /**** $$TRACE_LOG (EXIT,"") ****/
}

/**** $$TRACE_PROCEDURE_NAME  = find_pos_in_int  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW  ****/
INT4
find_pos_in_int (u8number)
     FS_UINT8            u8number;
{
    INT4                j = 1;
    INT4                count = 0;

    if ((FSAP_U8_FETCH_HI (&u8number) == 0) &&
        (FSAP_U8_FETCH_LO (&u8number) == 0))
    {
        return -1;
    }

    if (FSAP_U8_FETCH_HI (&u8number) == 0)
    {
        for (count = 0; count < CLI_LOW_BYTE_MAX_VAL; count++)
        {
            if ((FSAP_U8_FETCH_LO (&u8number) & j) != 0)
            {
                return count;
            }
            j *= 2;
        }
    }
    else
    {
        for (count = CLI_LOW_BYTE_MAX_VAL; count < CLI_HIGH_BYTE_MAX_VAL;
             count++)
        {
            if ((FSAP_U8_FETCH_HI (&u8number) & j) != 0)
            {
                return count;
            }
            j *= 2;
        }
    }
    return -1;
}

VOID
mmi_print_error_in_token (tCliContext * pCliContext, INT2 i2token_count)
{
    INT2                i2Index;
    INT2                i2MarkerPos = 0;
    CHR1               *pc1ErrMsg;

    mmi_printf ("\r\n");

    for (i2Index = 0; i2Index < i2token_count; i2Index++)
    {
        mmi_printf ("%s ", pCliContext->MmiCmdToken[i2Index]);
        i2MarkerPos += CLI_STRLEN (pCliContext->MmiCmdToken[i2Index]) + 1;
        /* for extra space */
    }
    pc1ErrMsg = CLI_BUDDY_ALLOC (i2MarkerPos + 2, CHR1);

    mmi_printf ("%s\r\n", pCliContext->MmiCmdToken[i2Index]);

    if (pc1ErrMsg != NULL)
    {
        for (i2Index = 0; i2Index < i2MarkerPos; i2Index++)
        {
            pc1ErrMsg[i2Index] = ' ';
        }
        pc1ErrMsg[i2Index++] = '^';
        pc1ErrMsg[i2Index] = 0;

        mmi_printf ("\r%s\n%% Invalid input detected at '^' marker\r\n",
                    pc1ErrMsg);

        CLI_BUDDY_FREE (pc1ErrMsg);
    }
}

INT2
CliExecuteCommand (pCliContext, pu1UserCmd)
     CONST CHR1         *pu1UserCmd;
     tCliContext        *pCliContext;
{
    t_MMI_HELP         *pCliHelpInfo;
    tAuditInfo         *pTempAudit = NULL;
    time_t              t;
    struct tm          *tp;
    INT1               *pi1TempUsrCmd;
    INT2                i2NumTokens = 0;
    INT4                i4ActionNumber;
    INT4                i4Index;
    UINT4               u4TempContextId = 0;
    INT2                i2ModeIndex;
    INT2                i2CmdPos = 0;
    INT1               *pi1RemCmd = NULL;
    INT1               *pi1Command = NULL;
    INT1               *pi1CheckCommand = NULL;
    INT2                i2TokenCount = 0;
    INT1               *pi1RemCommand = NULL;
    UINT1               u1TempChar;
    UINT1              *pu1Cmd = NULL;
    INT1               *pi1AliasRepCmd;
    INT1                i1AliasFlag = FALSE;
    INT1                i1CmdPrivilege = -1;
    UINT1               ai1PrevModePath[MAX_PROMPT_LEN];
    CHR1               *pTime = NULL;

    INT1                ai1PromptStr[MAX_PROMPT_LEN];
    INT1                ai1RootPromptStr[MAX_PROMPT_LEN];
    INT1                ai1CurrentPromptStr[MAX_PROMPT_LEN];
    INT1                ai1GetPrompt[MAX_PROMPT_LEN];
    tIssCustCliConfInfo IssCustCliConfInfo;
    UINT4               u4RestrictedCmdFlag = FALSE;
    INT4                i4RangeMode;
    INT4                i4Mode;
    UINT4               u4Len = 0;
#ifdef L2RED_WANTED
    INT4                i4StringCount = 0;
    INT4                i4BulkUpdtResult = RM_SUCCESS;
    INT1                i1BulkUpdateAllowed = FALSE;
#endif

    if ((CliGetIfRangeMode (&i4RangeMode) != CLI_SUCCESS))
    {
        i4RangeMode = CLI_IF_RANGE_MODE_DISABLE;
    }

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1Command, INT1);

    if (pi1Command == NULL)
    {
        return i2NumTokens;
    }
    STRCPY (pi1Command, pu1UserCmd);
    pi1TempUsrCmd = pi1Command;

    while ((CliIsDelimit (*pi1TempUsrCmd, CLI_EOT_DELIMITS)))
        pi1TempUsrCmd++;
    if (*pi1TempUsrCmd == CLI_COMMENT_CHAR)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
        return 0;
    }

    if ('!' == *pi1TempUsrCmd)
    {
        if (pCliContext->i1PrivilegeLevel != CLI_MAX_PRIVILEGES - 1)
        {
            mmi_printf
                ("\rAccess Denied : Insufficient privilege level ! !\r\n");
	    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
            return CLI_FAILURE;
        }
        CliGetPromptFromMode ("CONFIGURE", ai1PromptStr);
        CliGetPromptFromMode ("EXEC", ai1RootPromptStr);
        u4TempContextId = CliGetContextIdInfo ();
        CliSetContextIdInfo (CLI_INVALID_CONTEXT_ID);
        CliGetCurModePromptStr (ai1CurrentPromptStr);

        if (CLI_STRCMP (ai1CurrentPromptStr, ai1RootPromptStr) == 0)
        {
            CliChangePath ((CONST CHR1 *) ai1PromptStr);
            mmi_AddUserCommandToHistory (pCliContext, CLI_CONF_TERM_CMD);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
            return CLI_SUCCESS;
        }
        if (CLI_STRCMP (ai1CurrentPromptStr, ai1PromptStr) != 0)
        {
            CliChangePath ("..");
            CliGetCurModePromptStr (ai1CurrentPromptStr);
            if (CLI_STRCMP (ai1CurrentPromptStr, ai1PromptStr) != 0)
            {
                CliSetContextIdInfo (u4TempContextId);
            }
            mmi_AddUserCommandToHistory (pCliContext, CLI_EXIT_CMD);
        }
	
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
        return CLI_SUCCESS;
    }

    MEMSET (ai1GetPrompt, 0, MAX_PROMPT_LEN);
    CliGetCurModePromptStr (ai1GetPrompt);
    if (STRCMP (ai1GetPrompt, "(config)#") == 0)
    {
        CliSetContextIdInfo (0);
    }
    else if ((STRCMP (ai1GetPrompt, "(config-if)#") == 0)
             || (STRCMP (ai1GetPrompt, "#") == 0))
    {
        CliSetContextIdInfo (CLI_INVALID_CONTEXT_ID);
    }

    pi1TempUsrCmd = pi1Command;

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1AliasRepCmd, INT1);

    if (pi1AliasRepCmd == NULL)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
        return 0;
    }

    pi1AliasRepCmd[0] = '\0';
    pCliContext->i2CmdIndex = 0;

    if (pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE)
    {
        /* Creates the default pipe if i1DefaultMoreFlag is set */
        CliCreatePipe (pCliContext);
    }

    do
    {

        if ((CliGetIfRangeMode (&i4Mode) == CLI_SUCCESS) &&
            (i4RangeMode == CLI_IF_RANGE_MODE_DISABLE) &&
            (i4Mode == CLI_IF_RANGE_MODE_ENABLE))
        {
            mmi_printf
                ("\r%% Commands after interface range command are ignored\r\n");
            break;
        }

        i4ActionNumber = -1;
        i2ModeIndex = -1;
        pCliContext->u4Flags = (pCliContext->u4Flags & (~CLI_CTRL_BRK_MASK));
        pCliContext->i2CmdIndex++;

        if ((CliHandleAlias (pi1TempUsrCmd, pi1AliasRepCmd)) == CLI_SUCCESS)
        {
            i1AliasFlag = TRUE;
            STRCPY (pi1TempUsrCmd, pi1AliasRepCmd);
            u4Len =
                ((STRLEN (pi1TempUsrCmd) <
                  sizeof (pCliContext->
                          ai1CmdString)) ? STRLEN (pi1TempUsrCmd) :
                 sizeof (pCliContext->ai1CmdString) - 1);
            STRNCPY (pCliContext->ai1CmdString, pi1TempUsrCmd, u4Len);
            pCliContext->ai1CmdString[u4Len] = '\0';
        }
        pi1RemCommand = pi1TempUsrCmd;
        if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
        {
            CliSplitPathAndCommand ((UINT1 *) pi1TempUsrCmd, &i2CmdPos);
            if (i2CmdPos >= 0)
            {
                u1TempChar = pi1TempUsrCmd[i2CmdPos];
                pi1TempUsrCmd[i2CmdPos] = '\0';
                mmicm_copy_curconfig_to_inter (pCliContext);
                if ((cli_change_path ((CHR1 *) pi1RemCommand)) == CLI_FAILURE)
                {
                    pi1TempUsrCmd[i2CmdPos] = u1TempChar;
                    mmicm_copy_inter_to_curconfig (pCliContext);
                }
                pi1TempUsrCmd[i2CmdPos] = u1TempChar;
                pi1RemCommand = &pi1TempUsrCmd[i2CmdPos];
            }
        }

        OsixSemTake (gCliSessions.CliAmbVarSemId);
        CLI_ALLOC_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        /* As Sem take of CliAmbVarSemId is successful, mem block allocation
         * will be successful */
        if (pCliContext->pCliAmbVar == NULL)
        {
            OsixSemGive (gCliSessions.CliAmbVarSemId);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1AliasRepCmd);
            CliRestoreCxtFileInfo (pCliContext);
            return 0;
        }

        /* Pipe will be created in mmi_parse() when necessary */
        i2TokenCount = mmi_parse (pCliContext, pi1RemCommand,
                                  &i4ActionNumber, &i2ModeIndex, &pi1RemCmd,
                                  TRUE);
        CLI_RELEASE_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        pCliContext->pCliAmbVar = NULL;
        OsixSemGive (gCliSessions.CliAmbVarSemId);

        i2NumTokens += i2TokenCount;
        /*Checking command is allowed to be Executed or not */
        /*There is no restriction on command executed through application context */
        if (i2TokenCount > 0)
        {
            u4RestrictedCmdFlag = FALSE;
            if (pCliContext->gu4CliMode != CLI_MODE_APP)
            {
                IssCustCliConfInfo.pi1CliCmd = pCliContext->ai1OrgCmdToken;
                IssCustCliConfInfo.pi1ConfMode =
                    pCliContext->pMmiCur_mode->pMode_name;
                IssCustCliConfInfo.i4PromptInfo = pCliContext->i4PromptInfo;
                if (IssCustConfControlCli (&IssCustCliConfInfo) != ISS_SUCCESS)
                {
                    mmi_printf ("\r%%configuration prohibited\r\n");
                    u4RestrictedCmdFlag = TRUE;
                }
            }
    

        }

        if ((i2NumTokens > 0) && ((!pi1RemCmd) || !(*pi1RemCmd)))
            mmi_AddUserCommandToHistory (pCliContext,
                                         (CONST CHR1 *) pu1UserCmd);
        if (i1AliasFlag && !pi1RemCmd)
        {
            STRCPY (pi1TempUsrCmd, pCliContext->ai1CmdString);
            CliGetNextCommand (pi1TempUsrCmd, &pi1RemCmd);
            i1AliasFlag = FALSE;
        }

        if (pi1RemCmd)
        {
            pi1RemCommand = pCliContext->ai1CmdString;
            CliGetNextCommand (pi1RemCommand, &pi1RemCommand);
            if (pi1RemCommand)
            {
                *pi1RemCommand = '\0';
            }
        }
        if (!i2TokenCount && !pi1RemCmd)
            break;

        if (i4ActionNumber < 0)
        {
            if ((i2CmdPos >= 0) && (gCliSessions.bIsClCliEnabled == OSIX_FALSE))
            {
                mmicm_copy_inter_to_curconfig (pCliContext);
            }
            if (pi1RemCmd)
            {
                pi1TempUsrCmd = pi1RemCmd;
                CLI_STRCPY (pCliContext->ai1CmdString, pi1RemCmd);
            }
            continue;
        }

        pCliHelpInfo = pCliContext->pMmiCur_mode->pHelp;
        while (pCliHelpInfo)
        {
            if ((pCliHelpInfo->i4ActNumStart <= i4ActionNumber)
                && (pCliHelpInfo->i4ActNumEnd >= i4ActionNumber))
            {
                break;
            }
            pCliHelpInfo = pCliHelpInfo->pNext;
        }
        if (!pCliHelpInfo)
        {
            mmi_printf
                ("\r\nCannot determine permission for the command!!!\r\n");
            continue;
        }
        else
        {
            if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
            {
                i1CmdPrivilege = pCliHelpInfo->pPrivilegeId[i2ModeIndex - 1];
            }
        }

        if (IssuGetMaintModeOperation () == OSIX_TRUE)
        {

            CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1CheckCommand, INT1);
            if (pi1CheckCommand == NULL)
            {
                break;
            }
            STRCPY (pi1CheckCommand, pu1UserCmd);

            /* When ISSU maintenance mode is in progress allow only  
             * ISSU and related commands*/ 

            if(pCliHelpInfo->pPrivilegeId[i2ModeIndex - 1] > 1)
            {
                /* Checking whether it is an ISSU related 
                 * configuration command */

                if (IssuIsCliCmdAllowed (pi1CheckCommand) != ISSU_SUCCESS)
                {
                    mmi_printf
                    ("%% Command not allowed in ISSU Maintenance Mode ! !\r\n"); 
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1CheckCommand);
                    break;
                }
            } 
            else
            {
                /* Checking whether it is an allowed show 
                 * or debug related command */
                if (IssuIsCliCmdRestricted (pi1CheckCommand) != ISSU_SUCCESS)
                {
                    mmi_printf
                    ("%% Command not allowed in ISSU Maintenance Mode ! !\r\n"); 
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1CheckCommand);
                    break;
                }
            }
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1CheckCommand);
        }
        if ((gCliSessions.bIsClCliEnabled == OSIX_TRUE) &&
                 (i1CmdPrivilege > pCliContext->i1PrivilegeLevel))
        {
            mmi_printf
                ("\rAccess Denied : Insufficient privilege level ! !\r\n");
        }
        else
        {
            pCliContext->fpLock = NULL;
            pCliContext->fpUnLock = NULL;

            /* Save the mode b4 exec cmd */
            STRCPY (ai1PrevModePath, pCliContext->ai1ModePath);

            /* Management lock needs to be taken here to prevent race
             * conditions. 
             * */

#ifdef L2RED_WANTED
            for (i4StringCount = 0; i4StringCount < FILTER_STRING_LIST_MAX;
                 i4StringCount++)
            {
                if (STRNCMP
                    (pCliContext->ai1OrgCmdToken,
                     gFilterStringList[i4StringCount].pCmdString,
                     STRLEN (gFilterStringList[i4StringCount].pCmdString)) == 0)
                {
                    i1BulkUpdateAllowed = TRUE;
                }
            }
            if ((i1CmdPrivilege > 1) && (i1BulkUpdateAllowed == FALSE))
            {
                if (RmGetPeerNodeCount () != 0)
                {
                    i4BulkUpdtResult = RmIsBulkUpdateComplete ();
                    if (i4BulkUpdtResult == RM_FAILURE)
                    {
                        mmi_printf
                            ("\rBulk Update not yet Completed !!!!!\r\n");
		            	break;
                    }
                }
            }
#endif
            if (gu1FileCopyingStatus == SFTP_IN_PROGRESS || gu1FileCopyingStatus == TFTP_IN_PROGRESS)
            {
                 mmi_printf ("\r\n%% File Transfer in Progress\r\n");
		         break;
            }
 
            MGMT_LOCK ();

            /*Resetting the CmdStatus */
            pCliContext->u4CmdStatus = 0;

            if (u4RestrictedCmdFlag == FALSE)
            {
                if (mmi_action_procedure
                    ((tCliHandle) pCliContext->i4ConnIdx,
                     pCliContext->vpMmi_values, i4ActionNumber) != SUCCESS)
                {
                    /* 
                     * Command failures returned from command ACTION part or 
                     * from command ACTION routine is not taken care in CLI 
                     * Framework
                     */
                }
            }
            if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
            {
                do
                {
                    pTempAudit =
                        (tAuditInfo *) MemAllocMemBlk (gAuditInfoMemPoolId);
                    if (pTempAudit == NULL)
                    {
                        break;
                    }
                    pu1Cmd =
                        CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN);
                    if (pu1Cmd == NULL)
                    {
                        mmi_printf
                            ("\r\n Cannot allocate memory for command String\n");
                        break;
                    }

                    MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
		    CLI_MEMSET (pu1Cmd, 0, MAX_LINE_LEN);
                    STRCPY (pTempAudit->TempCmd.ai1UserName, "Audit:");
                    STRCAT (pTempAudit->TempCmd.ai1UserName,
                            pCliContext->ai1UserName);
		    STRCPY (pu1Cmd, pu1UserCmd);
                    t = time (NULL);
		    CliValidateSyslogMsg (pCliContext, pu1Cmd);
                    if ((STRLEN(pu1Cmd) < AUDIT_CMD_LEN) && 
                        (STRLEN (pu1Cmd) != 0))
                    {
                        MEMCPY (pTempAudit->TempCmd.au1TempCmd,
                                pu1Cmd, STRLEN (pu1Cmd));
                    }
                    else
                    {
                        MemReleaseMemBlock (gAuditInfoMemPoolId,
                                            (UINT1 *) pTempAudit);
  			MemReleaseMemBlock (gCliMaxLineMemPoolId, pu1Cmd);
									 break;
                    }
                    pTempAudit->TempCmd.u4CmdStatus = pCliContext->u4CmdStatus;
                    pTempAudit->TempCmd.u4CliMode = pCliContext->gu4CliMode;

                    tp = localtime (&t);
                    if (tp != NULL)
                    {
                        pTime = asctime (tp);
                        if (pTime != NULL)
                            STRCPY (pTempAudit->au1Time, pTime);
                    }
                    pTempAudit->i2Flag = AUDIT_CLI_MSG;
                    if (pCliContext->gu4CliMode != CLI_MODE_CONSOLE)
                    {
                        pTempAudit->TempCmd.u4ClientIpAddr =
                            pCliContext->au4ClientIpAddr[3];
                    }
                    MSRSendAuditInfo (pTempAudit);
                    MemReleaseMemBlock (gAuditInfoMemPoolId,
                                        (UINT1 *) pTempAudit);
		    MemReleaseMemBlock (gCliMaxLineMemPoolId, pu1Cmd);
                }
                while (0);
            }

            if (!pCliContext->mmi_exit_flag)
            {
                CliFlushOutputBuff (pCliContext);
            }
            MGMT_UNLOCK ();
        }
        CliRestoreCxtFileInfo (pCliContext);

        /* Free the memory allocated for value type tokens (float, int) */
        for (i2TokenCount = 0; i2TokenCount < MAX_NO_OF_TOKENS_IN_MMI;
             i2TokenCount++)
        {
            if (pCliContext->ai1Tokentype_flag[i2TokenCount])
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                               vpMmi_values[i2TokenCount]);
                pCliContext->ai1Tokentype_flag[i2TokenCount] = 0;
                pCliContext->vpMmi_values[i2TokenCount] = NULL;
            }
        }
        if (pi1RemCmd)
        {
            pi1TempUsrCmd = pi1RemCmd;
            CLI_STRCPY (pCliContext->ai1CmdString, pi1RemCmd);
        }

        if ((i2CmdPos >= 0) && (gCliSessions.bIsClCliEnabled == OSIX_FALSE))
        {
            mmicm_copy_inter_to_curconfig (pCliContext);
        }
        /* Free the buddy memory allocated for GREP buffer every
         * time in while loop when executing individual commands
         * seperated by end-of-command delimiter.
         */

        for (i4Index = 0;
             (i4Index < CLI_MAX_GREP)
             && (pCliContext->aCliGrep[i4Index].pi1GrepStr); i4Index++)
        {
            pCliContext->aCliGrep[i4Index].i1OptFlag = 0;
            CLI_BUDDY_FREE (pCliContext->aCliGrep[i4Index].pi1GrepStr);
            pCliContext->aCliGrep[i4Index].pi1GrepStr = NULL;
        }
        pCliContext->i1GrepFlag = FALSE;
    }
    while (pi1RemCmd && pi1TempUsrCmd);
    CliRestoreCxtFileInfo (pCliContext);
    /* Free the buddy memory allocated for grep buffer. This case
     * will hit when the while loop is terminated because of any
     * failure.
     */
    if (pCliContext->i1GrepFlag == TRUE)
    {
        for (i4Index = 0;
             (i4Index < CLI_MAX_GREP)
             && (pCliContext->aCliGrep[i4Index].pi1GrepStr); i4Index++)
        {
            pCliContext->aCliGrep[i4Index].i1OptFlag = 0;
            CLI_BUDDY_FREE (pCliContext->aCliGrep[i4Index].pi1GrepStr);
            pCliContext->aCliGrep[i4Index].pi1GrepStr = NULL;
        }
        pCliContext->i1GrepFlag = FALSE;
    }

    if (pCliContext->i1PipeFlag)
        CliClosePipe (pCliContext);

    /*Initialize the mp_i2recur_flag */
    pCliContext->mp_i2recur_flag = 0;

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1Command);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1AliasRepCmd);
    return i2NumTokens;
}

VOID
CliInitMmiVal (tCliContext * pCliContext)
{
    INT2                i2Index;

    pCliContext->amb_env_ptr_start = NULL;
    for (i2Index = 0; i2Index < MAX_NO_OF_TOKENS_IN_MMI; i2Index++)
    {
        /* Free the memory alocated for value type tokens (float, int) */
        if (pCliContext->ai1Tokentype_flag[i2Index])
        {
            if (pCliContext->vpMmi_values[i2Index] != NULL)
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->vpMmi_values[i2Index]);
            }
        }

        pCliContext->vpMmi_values[i2Index] = NULL;
        pCliContext->ai1Tokentype_flag[i2Index] = 0;
        MEMSET (pCliContext->MmiCmdToken[i2Index], '\0', MAX_CHAR_IN_TOKEN);
    }

    return;
}

UINT1
CliExecuteAppCmd (pu1UserCmd)
     CONST CHR1         *pu1UserCmd;
{
    tCliContext        *pCliContext;
    UINT1               u1RetVal = CLI_SUCCESS;
    if (((pCliContext = CliGetContext ()) == NULL) ||
        (pCliContext->mmi_exit_flag == TRUE))
    {
        /* No Task Found */
        return CLI_FAILURE;
    }
    pCliContext->u4CmdStatus = CLI_SUCCESS;
    u1RetVal = (UINT1) CliExecuteCliCmdforContext (pCliContext, pu1UserCmd);
    return u1RetVal;
}

UINT4
CliExecuteCliCmdforContext (pCliContext, pu1UserCmd)
     tCliContext        *pCliContext;
     CONST CHR1         *pu1UserCmd;
{
    UINT1              *pu1TempCmd = NULL;
    UINT4               u4HistTop = 0;

    /* Do not update the commands to the history */
    u4HistTop = pCliContext->mmi_hist_top;
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1TempCmd, UINT1);

    if (pu1TempCmd != NULL)
    {
        pu1TempCmd[0] = '\0';
        SPRINTF ((CHR1 *) pu1TempCmd, "%s;", pu1UserCmd);
        CliExecuteCommand (pCliContext, (CONST CHR1 *) pu1TempCmd);
        pCliContext->fpCliIoctl (pCliContext, CLI_FLUSH_OUTPUT, NULL);
        pCliContext->mmi_hist_top = u4HistTop;
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCmd);
    }
    return (pCliContext->u4CmdStatus);
}

UINT1
CliExecuteCliCmd (pu1UserCmd)
     CONST CHR1         *pu1UserCmd;
{
    tCliContext        *pCliContext;

    if (((pCliContext = CliGetContext ()) == NULL) ||
        (pCliContext->mmi_exit_flag == TRUE))
    {
        /* No Task Found */
        return CLI_FAILURE;
    }
    CliExecuteCliCmdforContext (pCliContext, pu1UserCmd);
    return CLI_SUCCESS;
}

INT2
CliHandleAlias (INT1 *pi1CmdToken, INT1 *pi1AliasRepCmd)
{
    tCliContext        *pCliContext;
    INT1                i1AliasFlag = FALSE;
    INT1                i1TempChar = 0;
    INT1               *pi1TempCmd = NULL;
    INT1               *pi1AliasCmd = NULL;
    INT2                i2RetStatus = CLI_FAILURE;
    INT2                i2Index;
    INT2                i2AliasCnt = 0;
    INT1                i1SpaceFound = FALSE;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (i2RetStatus);
    }

    if (pi1AliasRepCmd)
    {
        i1AliasFlag = TRUE;

        pi1TempCmd = pi1CmdToken;
        while (*pi1TempCmd && (CliIsDelimit (*pi1TempCmd, CLI_EOT_DELIMITS)))
        {
            pi1TempCmd++;
        }

        pi1AliasCmd = pi1TempCmd;

        /* To check if command entered with/without spaces is alias or not 
         * ex. "xx" or "xx xx" */

        while (*pi1TempCmd
               && ((*pi1TempCmd == ' ') || !(CliIsEndOfToken (*pi1TempCmd))))
        {
            if (*pi1TempCmd == ' ')
            {
                i1SpaceFound = TRUE;
            }

            pi1TempCmd++;

        }

        i1TempChar = *pi1TempCmd;
        *pi1TempCmd = '\0';

        /* process selected string/token */
        for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
        {
            if (pCliContext->gCliAlias[i2Index].pi1Token)
            {
                if (!STRCMP
                    (pi1AliasCmd, pCliContext->gCliAlias[i2Index].pi1Token))
                {
                    /* Needs a final ';' for mmi_gettoken() */
                    *pi1TempCmd = i1TempChar;
                    SPRINTF ((CHR1 *) pi1AliasRepCmd, "%s %s;",
                             (CHR1 *) pCliContext->gCliAlias[i2Index].
                             pi1RepToken, (CHR1 *) pi1TempCmd);

                    i2RetStatus = CLI_SUCCESS;
                }
            }
        }

        *pi1TempCmd = i1TempChar;

        /* To check if first token of command is alias */
        if ((i1SpaceFound == TRUE) && (i2RetStatus != CLI_SUCCESS))
        {
            pi1TempCmd = pi1AliasCmd;

            while (*pi1TempCmd && !(CliIsEndOfToken (*pi1TempCmd)))
            {
                pi1TempCmd++;
            }
            i1TempChar = *pi1TempCmd;
            *pi1TempCmd = '\0';
        }
        else
        {
            return (i2RetStatus);
        }
    }

    for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
    {
        if (pCliContext->gCliAlias[i2Index].pi1Token)
        {
            if (!i1AliasFlag)
            {
                mmi_printf ("\r%s -> %s\r\n",
                            pCliContext->gCliAlias[i2Index].pi1Token,
                            pCliContext->gCliAlias[i2Index].pi1RepToken);
                i2AliasCnt++;
                continue;
            }
            if (!STRCMP (pi1AliasCmd, pCliContext->gCliAlias[i2Index].pi1Token))
            {
                /* Needs a final ';' for mmi_gettoken() */
                *pi1TempCmd = i1TempChar;
                SPRINTF ((CHR1 *) pi1AliasRepCmd, "%s %s;",
                         (CHR1 *) pCliContext->gCliAlias[i2Index].pi1RepToken,
                         (CHR1 *) pi1TempCmd);

                i2RetStatus = CLI_SUCCESS;
                break;
            }
        }
    }

    if (!i1AliasFlag && !i2AliasCnt)
    {
        mmi_printf ("\rNo aliases found\r\n");
    }
    if (i1TempChar)
    {
        *pi1TempCmd = i1TempChar;
    }
    return (i2RetStatus);
}

INT2
CliAliasAdd (INT1 *pi1Token, INT1 *pi1RepToken)
{
    tCliContext        *pCliContext;
    INT2                i2RetVal = CLI_FAILURE;
    INT2                i2Index;
    INT1               *pi1TmpToken = NULL;
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    if (!(pi1Token) && !(pi1RepToken))
    {
        /* Display available aliases */
        CliHandleAlias (NULL, NULL);
        return CLI_SUCCESS;
    }
    if (pi1Token != NULL)
    {
        pi1TmpToken = pi1Token;
        /* alias name with # in begining will be rejected, eg: #abcd */
        if (*pi1Token == CLI_COMMENT_CHAR)
        {
            mmi_printf ("\r\n%% Invalid token at input\r\n");
            return CLI_FAILURE;
        }
        while (*pi1TmpToken)
        {
            if (*pi1TmpToken == '!')
            {
                mmi_printf ("\r\n%% Invalid token at input\r\n");
                return CLI_FAILURE;
            }
            pi1TmpToken++;
            continue;
        }
    }
    if (!pi1RepToken)
    {
        /* Remove the alias for the given string */
        for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
        {
            if ((pCliContext->gCliAlias[i2Index].pi1Token) &&
                !(CLI_STRCMP
                  (pi1Token, pCliContext->gCliAlias[i2Index].pi1Token)))
            {
                MemReleaseMemBlock (gCliAliasMemPoolId,
                                    (UINT1 *) (pCliContext->gCliAlias[i2Index].
                                               pi1Token));
                MemReleaseMemBlock (gCliAliasMemPoolId,
                                    (UINT1 *) (pCliContext->gCliAlias[i2Index].
                                               pi1RepToken));
                pCliContext->gCliAlias[i2Index].pi1Token = NULL;
                pCliContext->gCliAlias[i2Index].pi1RepToken = NULL;
                i2RetVal = CLI_SUCCESS;
                break;
            }
        }
        if (i2Index == CLI_MAX_ALIAS)
        {
            mmi_printf ("\r\n%% Alias Not found\r\n");
        }
    }
    else if (pi1Token != NULL)
    {
        /* Add a new alias for the given string */
        for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
        {
            if (!pCliContext->gCliAlias[i2Index].pi1Token ||
                !STRCMP (pi1Token, pCliContext->gCliAlias[i2Index].pi1Token))
            {
                while (*pi1Token == CLI_SPACE_CHAR)
                {
                    mmi_printf
                        ("\r\n%% Alias first character cannot be space \r\n");
                    return CLI_FAILURE;
                }
                if (!(pCliContext->gCliAlias[i2Index].pi1Token =
                      CliMemAllocMemBlk (gCliAliasMemPoolId,
                                         MAX_CHAR_IN_TOKEN)))
                {
                    mmi_printf ("\rMemory allocation failure \r\n");
                    break;
                }
                if (!(pCliContext->gCliAlias[i2Index].pi1RepToken =
                      CliMemAllocMemBlk (gCliAliasMemPoolId,
                                         MAX_CHAR_IN_TOKEN)))
                {
                    MemReleaseMemBlock (gCliAliasMemPoolId,
                                        (UINT1 *) (pCliContext->
                                                   gCliAlias[i2Index].
                                                   pi1Token));
                    pCliContext->gCliAlias[i2Index].pi1Token = NULL;
                    mmi_printf ("\rMemory allocation failure \r\n");
                    break;
                }
                pCliContext->gCliAlias[i2Index].pi1Token[0] = '\0';
                pCliContext->gCliAlias[i2Index].pi1RepToken[0] = '\0';
                STRCPY (pCliContext->gCliAlias[i2Index].pi1Token, pi1Token);
                STRCPY (pCliContext->gCliAlias[i2Index].pi1RepToken,
                        pi1RepToken);
                i2RetVal = CLI_SUCCESS;
                break;
            }
        }
        if (i2Index == CLI_MAX_ALIAS)
        {
            mmi_printf ("\rAlias Count Exceeds Limit !!!\r\n");
        }
    }
    return i2RetVal;
}

INT2
CliAliasDel (INT1 *pi1Token)
{
    INT2                i2Index;
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
    {
        if (!STRCMP (pCliContext->gCliAlias[i2Index].pi1Token, pi1Token))
        {
            MemReleaseMemBlock (gCliAliasMemPoolId,
                                (UINT1 *) (pCliContext->gCliAlias[i2Index].
                                           pi1Token));
            MemReleaseMemBlock (gCliAliasMemPoolId,
                                (UINT1 *) (pCliContext->gCliAlias[i2Index].
                                           pi1RepToken));
            pCliContext->gCliAlias[i2Index].pi1Token = NULL;
            pCliContext->gCliAlias[i2Index].pi1RepToken = NULL;
            return (CLI_SUCCESS);
        }
    }
    mmi_printf ("\rAlias not found\r\n");
    return (CLI_FAILURE);
}

INT4
CliGetNextCommand (INT1 *pi1UserCommand, INT1 **ppi1NextCommand)
{
    if (!pi1UserCommand)
    {
        return (CLI_FAILURE);
    }
    while (*pi1UserCommand)
    {
        if (CliIsDelimit (*pi1UserCommand, CLI_EOC_DELIMITS))
        {
            pi1UserCommand++;
            break;
        }
        pi1UserCommand++;
    }
    if (*pi1UserCommand)
    {
        while (CliIsDelimit (*pi1UserCommand, CLI_EOT_DELIMITS))
            pi1UserCommand++;

        if (*pi1UserCommand)
            *ppi1NextCommand = pi1UserCommand;
        else
            *ppi1NextCommand = NULL;
    }
    else
        *ppi1NextCommand = NULL;

    return (CLI_SUCCESS);
}

INT4
CliCheckPipeSyntax (tCliContext * pCliContext, UINT1 *pu1Command,
                    INT2 *pi2ErrCode)
{
    INT1               *pi1TempCmd = NULL;
    INT1               *pi1TempCmdBase = NULL;    /* Base pointer used to free memory */
    INT1               *pi1Token = NULL;
    INT2                i2Index = 0;
    INT1                i1Delimit = 0;
    INT4                i4RetVal = CLI_FAILURE;

    while (pu1Command && (*pu1Command != '|') &&
           (CliIsDelimit (*pu1Command, CLI_EOT_DELIMITS)))
        pu1Command++;

    if (!(pu1Command) || !(*pu1Command) || (*pu1Command == '|') ||
        !(CliIsDelimit (pu1Command[STRLEN (pu1Command) - 1], CLI_EOC_DELIMITS)))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_COMMAND);
        pCliContext->present_ambig_count = 0;
        return (CLI_FAILURE);
    }

    if (!(pi1TempCmd = CLI_BUDDY_ALLOC (STRLEN (pu1Command) + 1, INT1)))
    {
        *pi2ErrCode = -(MMI_GEN_ERR_MALLOC);
        return (CLI_FAILURE);
    }
    pi1TempCmdBase = pi1TempCmd;

    STRCPY (pi1TempCmd, pu1Command);

    CliGetNextCommand (pi1TempCmd, &pi1TempCmd);

    if (pi1TempCmd)
        *pi1TempCmd = '\0';

    pi1TempCmd = pi1TempCmdBase;

    i2Index = 0;
    for (; pi1TempCmd && *pi1TempCmd;)
    {
        i1Delimit = CliGetNextToken (pi1TempCmd, CLI_EOT_DELIMITS,
                                     &pi1TempCmd, &pi1Token);

        if (i1Delimit == CLI_PIPE_CHAR)
        {
            /* Unsupported pipe command */
            break;
        }
        if ((pi1Token) && (*pi1Token))
        {
            if (!(STRCMP (pi1Token, CLI_MORE)))
            {
                pCliContext->i1MoreFlag = TRUE;
                i4RetVal = CLI_SUCCESS;
            }
            else if (!(STRCMP (pi1Token, CLI_GREP)))
            {
                if (i2Index >= CLI_MAX_GREP)
                {
                    mmi_printf (mmi_gen_messages[MMI_GEN_ERR_GREP_OVERFLOW]);
                    return (CLI_FAILURE);
                }
                i1Delimit =
                    CliGetNextToken (pi1TempCmd, CLI_EOT_DELIMITS,
                                     &pi1TempCmd, &pi1Token);
                if (!pi1Token || !(*pi1Token))
                {
                    /* Unsupported pipe command */
                    mmi_printf ("\rUsage : grep [-v] <search string>\r\n");
                    break;
                }
                if (*pi1Token == '-')
                {
                    pi1Token++;
                    if (*pi1Token != 'v')
                    {
                        /* Invalid Option: Usage grep [-v] <search string> */
                        mmi_printf ("\rUsage : grep [-v] <search string>\r\n");
                        break;
                    }
                    pCliContext->aCliGrep[i2Index].i1OptFlag |=
                        CLI_GREP_INVERSE;

                    if ((CliIsDelimit (i1Delimit, CLI_EOC_DELIMITS))
                        || (i1Delimit == CLI_PIPE_CHAR))
                    {
                        /* Grep command is incomplete */
                        break;
                    }
                    if (pi1TempCmd)
                    {
                        if (CliIsDelimit (*pi1TempCmd, CLI_EOC_DELIMITS))
                        {
                            /* Grep command is incomplete */
                            break;
                        }
                    }
                    i1Delimit = CliGetNextToken (pi1TempCmd,
                                                 CLI_EOT_DELIMITS,
                                                 &pi1TempCmd, &pi1Token);

                    /* Check to be done for possible options */
                }
                if (!pi1Token || !(*pi1Token))
                {
                    break;
                }
                if (*pi1Token == '"')
                    i1Delimit = '"';

                if (!mmi_string_check (pi1Token))
                {
                    /* Unsupported pipe command (missing matching quotes) */
                    mmi_printf ("\rUsage : grep [-v] <search string>\r\n");
                    break;
                }
                if ((pCliContext->aCliGrep[i2Index].pi1GrepStr =
                     CLI_BUDDY_ALLOC (STRLEN (pi1Token) + 1, INT1)))
                {
                    pCliContext->i1GrepFlag = TRUE;
                    STRCPY (pCliContext->aCliGrep[i2Index].pi1GrepStr,
                            pi1Token);
                    i2Index++;
                }
                i4RetVal = CLI_SUCCESS;
            }
            else
            {
                /* Unsupported pipe command */
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (CliIsDelimit (i1Delimit, CLI_EOC_DELIMITS))
            {
                /* End of command */
                break;
            }
            i1Delimit = CliGetNextToken (pi1TempCmd,
                                         CLI_EOT_DELIMITS,
                                         &pi1TempCmd, &pi1Token);
            if ((!(pi1Token) || !(*pi1Token)) &&
                (!(i1Delimit) || (CliIsDelimit (i1Delimit, CLI_EOC_DELIMITS))))
            {
                /* End of command */
                break;
            }
            i4RetVal = CLI_FAILURE;

            if ((pCliContext->i1MoreFlag) || (i1Delimit != CLI_PIPE_CHAR))
            {
                /* Unsupported pipe command */
                break;
            }
        }
    }

    if (i4RetVal == CLI_FAILURE)
    {
        *pi2ErrCode = -(MMI_GEN_ERR_INVALID_COMMAND);
        pCliContext->i1MoreFlag = FALSE;    /* Error condition */
    }
    CLI_BUDDY_FREE (pi1TempCmdBase);
    return (i4RetVal);
}

INT4
MmiDoCheckAllTokens (pCliContext, pi4ExactCmdIndex, pi2ErrCode)
     tCliContext        *pCliContext;
     INT4               *pi4ExactCmdIndex;
     INT2               *pi2ErrCode;
{
    t_MMI_CMD          *tmp_ptr = NULL;
    INT2                i2Index;
    INT2                i2CmdTokenCnt = 0;
    INT4                i4ExactCmdFound = FALSE;
    INT4                i4ExactCmdTokCnt = 0;
    INT4                i4ExactPrvCmdTokCnt = 0;
    INT4                i4CmdIndex = -1;
    INT4                i4OptionalCmd = 0;

    for (i2Index = 0; i2Index < pCliContext->present_ambig_count; i2Index++)
    {
        i4ExactCmdTokCnt = 0;

        for (i2CmdTokenCnt = 0; i2CmdTokenCnt < pCliContext->i2TokenCount;
             i2CmdTokenCnt++)
        {
            if (!(pCliContext->amb_env_ptr_start[i2Index][i2CmdTokenCnt]))
            {
                continue;
            }
            if (pCliContext->
                amb_env_ptr_start[i2Index][i2CmdTokenCnt]->i1pToken != NULL)
            {
                if (!(mmi_new_strcmp
                      ((CONST INT1 *) pCliContext->MmiCmdToken[i2CmdTokenCnt],
                       (CONST INT1 *) pCliContext->
                       amb_env_ptr_start[i2Index][i2CmdTokenCnt]->i1pToken)))
                {
                    i4ExactCmdTokCnt++;
                }
            }
        }

        /* Check for the possibility of command completion at this last token. 
         * If so check if this command has exact match for more tokens than the 
         * previous one from ambiguity pointer list.
         */

        if (pCliContext->amb_env_ptr_start[i2Index][i2CmdTokenCnt - 1])
        {
            if (!(mmi_new_strcmp
                  ((CONST INT1 *) pCliContext->amb_env_ptr_start[i2Index]
                   [i2CmdTokenCnt - 1]->pNext_token->i1pToken,
                   (CONST INT1 *) "ret")))
            {
                if (i4ExactCmdTokCnt > i4ExactPrvCmdTokCnt)
                {
                    i4CmdIndex = i2Index;
                    i4ExactCmdFound = TRUE;
                    i4ExactPrvCmdTokCnt = i4ExactCmdTokCnt;
                }
                else if ((i4ExactCmdTokCnt) &&
                         (i4ExactCmdTokCnt == i4ExactPrvCmdTokCnt))
                {
                    i4ExactCmdFound = FALSE;
                }
            }
            else if (pCliContext->amb_env_ptr_start[i2Index]
                     [i2CmdTokenCnt - 1]->pNext_token->pOptional)
            {
                tmp_ptr = pCliContext->
                    amb_env_ptr_start[i2Index][i2CmdTokenCnt -
                                               1]->pNext_token->pOptional;
                i4OptionalCmd = 0;
                while (tmp_ptr != NULL)
                {
                    if (i4OptionalCmd > CLI_OPTNLTKNS)
                    {
                        i4ExactCmdFound = FALSE;
                        break;
                    }
                    i4OptionalCmd++;

                    if (mmi_new_strcmp
                        ((CONST INT1 *) tmp_ptr->i1pToken,
                         (CONST INT1 *) "ret") == 0)
                    {
                        if (i4ExactCmdTokCnt > i4ExactPrvCmdTokCnt)
                        {
                            i4CmdIndex = i2Index;
                            i4ExactCmdFound = TRUE;
                            i4ExactPrvCmdTokCnt = i4ExactCmdTokCnt;
                        }
                        else if ((i4ExactCmdTokCnt) &&
                                 (i4ExactCmdTokCnt == i4ExactPrvCmdTokCnt))
                        {
                            i4ExactCmdFound = FALSE;
                        }
                        break;
                    }
                    tmp_ptr = tmp_ptr->pOptional;
                }
            }
        }
    }
    if (i4ExactCmdFound == TRUE)
    {
        *pi4ExactCmdIndex = i4CmdIndex;
    }
    else
    {
        *pi2ErrCode = -(MMI_GEN_ERR_COMMAND_AMBIGUITY);
    }
    return i4ExactCmdFound;
}

/******************************************************************************/
/* Function Name     : CliExecuteIfRangeCommand                               */
/*                                                                            */
/* Description       : If a command is executed in the                        */
/*                     interface range mode this function will be called      */
/*                     This function executes the command in all the          */
/*                     specified L2 and L3 Interfaces                         */
/*                                                                            */
/*                                                                            */
/* Input Parameters  : pCliContext : Clicontext Information                   */
/*                     pu1UserCmd  : Usercommand to be executed in range mode */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/ No of Interface on which command failed   */
/******************************************************************************/
INT2
CliExecuteIfRangeCommand (pCliContext, pu1UserCmd)
     CONST CHR1         *pu1UserCmd;
     tCliContext        *pCliContext;
{

    UINT2               u2Port = 0;
    UINT1               au1Command[CLI_MAX_CMD_LENGTH];
    INT4                i4RetVal = CLI_SUCCESS;
    UINT2               u2StartIndex = 0;
    UINT2               u2EndIndex = 0;
    UINT2               u2Slot = 0;
    INT1                i1CurPrompt[MAX_PROMPT_LEN];
    INT1                i1PrevPrompt[MAX_PROMPT_LEN];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT1                au1Range[MAX_TYPE_TOKEN];
    MEMSET (au1Range, 0, MAX_TYPE_TOKEN);
    MEMCPY (au1Range, "port-channel", STRLEN ("port-channel"));
    /* Set No Command Error */
    pCliContext->i4CmdErrCode = FALSE;

    if (STRCMP (pu1UserCmd, "\n") == CLI_SUCCESS)
    {
        /* Do Nothing */
        return CLI_SUCCESS;
    }

    CliSetIfRangeMode ();

    /* handling of "end" or  "exit" command is done here 
     * to avoid creation/deletion of first interface alone  */

    if (!((STRCMP (pu1UserCmd, "exit\n")) && (STRCMP (pu1UserCmd, "exi\n")) &&
          (STRCMP (pu1UserCmd, "ex\n")) && (STRCMP (pu1UserCmd, "end\n")) &&
          (STRCMP (pu1UserCmd, "en\n"))))
    {

        CliGetCurModePromptStr (i1PrevPrompt);
        i4RetVal = CliExecuteAppCmd (pu1UserCmd);
        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;

        }
        if (pCliContext->i4CmdErrCode == TRUE)
        {
            return CLI_FAILURE;
        }
        CliGetCurModePromptStr (i1CurPrompt);
        if (STRCMP (i1CurPrompt, i1PrevPrompt) != 0)
        {
            CliReSetIfRangeMode ();
            CliInitRangeList ();
            CliInitRangeIndex ();
            CliInitIfRangeType ();
            return CLI_SUCCESS;
        }
    }

    /* Add Command to History */

    mmi_AddUserCommandToHistory (pCliContext, (CONST CHR1 *) pu1UserCmd);

    /*  exit out of current mode */
    CliChangePath ("..");
     if (MEMCMP (pCliContext->au1RangeType, au1Range, STRLEN (au1Range)) == 0)
     {
         INT1                ai1Index[MAX_TYPE_TOKEN];
         INT1               *pi1Pos;
         INT1               *pi1Temp;

         MEMSET (ai1Index, 0, MAX_TYPE_TOKEN);
         MEMCPY (ai1Index, pCliContext->au1RangeList, MAX_TYPE_TOKEN);
         u2StartIndex = (UINT2) (ATOI (ai1Index));
         pi1Pos = CliGetToken (ai1Index, CLI_RANGE_DELIMIT, CLI_VALIDPORT_LIST);
         pi1Temp = pi1Pos + 1;
         u2EndIndex = (UINT2) (ATOI (pi1Temp));
         CliSetRangeIndex (u2StartIndex, u2EndIndex);
     } 
    /* Loop for L3  VLAN andconfigure user command in 
     * all the specified vlan interfaces */

    if (pCliContext->u4StartIndex > 0 && pCliContext->u4EndIndex > 0)
    {
        for (u2Port = (UINT2) (pCliContext->u4StartIndex);
             u2Port <= (UINT2) (pCliContext->u4EndIndex); u2Port++)
        {
             if (MEMCMP (pCliContext->au1RangeType, au1Range, STRLEN (au1Range))
                 == 0)
             {
                 SPRINTF ((CHR1 *) au1Command, "interface port-channel %d",
                          (INT4) u2Port);
             }
             else
             {
                 SPRINTF ((CHR1 *) au1Command, "interface vlan %d",
                          (INT4) u2Port);
             } 
            i4RetVal = CliExecuteAppCmd ((CONST CHR1 *) au1Command);

            if (i4RetVal != CLI_SUCCESS)
            {
                CliReSetIfRangeMode ();
                CliInitRangeList ();
                CliInitRangeIndex ();
                CliInitIfRangeType ();
                mmi_printf ("%% Command Failed for vlan%d and will "
                            "not be applied to rest of the ranges \n", u2Port);
                return CLI_FAILURE;
            }

            CliGetCurModePromptStr (i1PrevPrompt);
            i4RetVal = CliExecuteAppCmd (pu1UserCmd);
            if (i4RetVal == CLI_FAILURE)
            {
                mmi_printf ("%% Command Failed for vlan%d and will "
                            "not be applied to rest of the ranges \n", u2Port);
                CliChangePath ("..");
                CliChangePath (CLI_INT_RANGE);

                return CLI_FAILURE;
            }

            /* if mmi parser set the command has error
             * then exit without doing anything */
            if (pCliContext->i4CmdErrCode == TRUE)
            {
                CliChangePath ("..");
                CliChangePath (CLI_INT_RANGE);
                return CLI_FAILURE;
            }
            CliGetCurModePromptStr (i1CurPrompt);
            if (STRCMP (i1CurPrompt, i1PrevPrompt) != 0)
            {
                CliReSetIfRangeMode ();
                CliInitRangeList ();
                CliInitRangeIndex ();
                CliInitIfRangeType ();
                return CLI_SUCCESS;
            }
            CliChangePath ("..");
        }
        /* Reset the Number of attempts variable to avoid the
         * multiple execution of help,clear screen commands
         */
        pCliContext->u1NoOfAttempts = 0;

    }
    /* Loop the PortList and Configure 
     * the user command in all the specified ports */

    if (CliGetSlotAndRange (pCliContext->au1RangeList,
                            &u2StartIndex, &u2EndIndex, &u2Slot) != CLI_FAILURE)
    {
        if ((u2StartIndex != 0) && (u2EndIndex != 0))
        {
            for (u2Port = u2StartIndex; u2Port <= u2EndIndex; u2Port++)
            {
                SPRINTF ((CHR1 *) au1Command, "interface %s  %d/%d",
                         pCliContext->au1RangeType, (INT4) u2Slot,
                         (INT4) u2Port);
                i4RetVal = CliExecuteAppCmd ((CONST CHR1 *) au1Command);

                if (i4RetVal != CLI_SUCCESS)
                {
                    CliReSetIfRangeMode ();
                    CliInitRangeList ();
                    CliInitRangeIndex ();
                    CliInitIfRangeType ();

                    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliGetIfName (u2Port, (INT1 *) au1NameStr);
                    mmi_printf ("%% Command Failed in port %s %d/%d "
                                "and will not be applied to rest of the ranges \n",
                                pCliContext->au1RangeType, (INT4) u2Slot,
                                (INT4) u2Port);
                    return CLI_FAILURE;
                }

                CliGetCurModePromptStr (i1PrevPrompt);
                i4RetVal = CliExecuteAppCmd (pu1UserCmd);
                if (i4RetVal == CLI_FAILURE)
                {
                    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliGetIfName (u2Port, (INT1 *) au1NameStr);
                    mmi_printf ("%% Configuration Failed for "
                                "interface %s %d/%d\n\n",
                                pCliContext->au1RangeType, (INT4) u2Slot,
                                (INT4) u2Port);
                    CliGetCurModePromptStr (i1CurPrompt);
                    if (STRCMP (i1CurPrompt, i1PrevPrompt) != 0)
                    {
                        return CLI_FAILURE;
                    }
                }

                /* if mmi parser set the command has error
                 * then exit without doing anything */
                if (pCliContext->i4CmdErrCode == TRUE)
                {
                    CliChangePath ("..");
                    CliChangePath (CLI_INT_RANGE);
                    return CLI_FAILURE;
                }
                CliGetCurModePromptStr (i1CurPrompt);
                if (STRCMP (i1CurPrompt, i1PrevPrompt) != 0)
                {
                    CliReSetIfRangeMode ();
                    CliInitRangeList ();
                    CliInitRangeIndex ();
                    CliInitIfRangeType ();
                    return CLI_SUCCESS;
                }
                CliChangePath ("..");

            }

            /* Reset the Number of attempts variable to avoid the
             * multiple execution of help,clear screen commands
             */
            pCliContext->u1NoOfAttempts = 0;
        }
    }
    /* Change the mode to Interface Range */
    i4RetVal = CliChangePath (CLI_INT_RANGE);
    if (i4RetVal == CLI_FAILURE)
    {
        CliReSetIfRangeMode ();
        CliInitRangeList ();
        CliInitRangeIndex ();
        CliInitIfRangeType ();
        mmi_printf ("\r%% Entering the range mode failed  \r\n");
        return CLI_FAILURE;
    }
    /* Do not Initialize PortList ,User Still in If Range Mode */
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : CliGetCommand                                          *
*                                                                            *
* Description       : This function returns the command pointer and          *
*                     the actionpointer for the given CLI command            *
*                                                                            *
* Input Parameters  : pCliContext - Clicontext Information                   *
*                                                                            *
* Output Parameters : ppCommand - Pointer to the command pointer             *
*                     pi4ActionNumber - Action number of the command         *
*                                                                            *
* Return Value      : CLI_SUCCESS/ CLI_FAILURE                               *
******************************************************************************/
INT1
CliGetCommand (tCliContext * pCliContext, t_MMI_CMD ** ppCommand,
               INT4 *pi4ActionNumber, INT4 i4CurIndex)
{

    t_MMI_CMD          *pTmp = NULL;
    INT4                i4ParseReturn = CLI_ZERO;
    INT4                i4FirstTokAmbCount = CLI_ZERO;
    INT2                i2TokIndex = CLI_ZERO;
    INT2                i2Index = CLI_ZERO;
    INT2                i2ErrCode = CLI_SUCCESS;
    INT1                i1RetVal = CLI_FAILURE;

    for (i2TokIndex = 0; i2TokIndex < pCliContext->i2TokenCount; i2TokIndex++)
    {
        if (i2TokIndex == 0)
        {
            i4ParseReturn = mmi_first_tok_parse (pCliContext,
                                                 pCliContext->
                                                 MmiCmdToken[i2TokIndex],
                                                 &i2ErrCode);
            i4FirstTokAmbCount = pCliContext->present_ambig_count;
        }
        else
        {
            i4ParseReturn = mmi_parse_continue (pCliContext,
                                                pCliContext->
                                                MmiCmdToken[i2TokIndex],
                                                &i2ErrCode);

            if (pCliContext->amb_env_ptr_start != NULL)
            {
                for (i2Index = 0; i2Index < pCliContext->present_ambig_count;
                     i2Index++)
                {
                    pCliContext->amb_env_ptr_start[i2Index][i2TokIndex] =
                        pCliContext->pCliAmbVar->amb_ptr_start[i2Index];
                }
            }

        }
    }

    if (i4ParseReturn == 1)
    {                            /*Gets the address of the command to be removed */
        if (NULL != pCliContext->amb_env_ptr_start)
        {
            pCliContext->env_ptr_start = pCliContext->amb_env_ptr_start[0][0];
            *ppCommand = pCliContext->env_ptr_start;
        }

        /*Gets the address of the last token in that command */
        pTmp = pCliContext->pCliAmbVar->amb_ptr_start[0];
        while ((pTmp != NULL))
        {
            if (CLI_ZERO == (mmi_new_strcmp
                             ((CONST INT1 *) pTmp->i1pToken,
                              (CONST INT1 *) "ret")))
            {
                *pi4ActionNumber = pTmp->i4Action_number;
                break;
            }
            pTmp = pTmp->pNext_token;
        }
        i1RetVal = CLI_SUCCESS;
    }

    /* Ambiguity for some commands like 'show snmp' and 'spanning-tree'
     * are handled here.  */
    if (i4ParseReturn > 1 && pCliContext->amb_env_ptr_start != NULL)
    {
        for (i2Index = 0; i2Index < pCliContext->present_ambig_count; i2Index++)
        {
            if (pCliContext->
                amb_env_ptr_start[i2Index][pCliContext->i2TokenCount] == NULL)
            {
                if (i4CurIndex == -1)
                {
                    *ppCommand = pCliContext->amb_env_ptr_start[i2Index][0];
                    pTmp = pCliContext->pCliAmbVar->amb_ptr_start[i2Index];
                }
                else
                {
                    *ppCommand = pCliContext->amb_env_ptr_start[i4CurIndex][0];
                    pTmp = pCliContext->pCliAmbVar->amb_ptr_start[i4CurIndex];

                }
                while ((pTmp != NULL))
                {
                    if (CLI_ZERO == (mmi_new_strcmp
                                     ((CONST INT1 *) pTmp->i1pToken,
                                      (CONST INT1 *) "ret")))
                    {
                        *pi4ActionNumber = pTmp->i4Action_number;
                        break;
                    }
                    pTmp = pTmp->pNext_token;
                }
                i1RetVal = CLI_SUCCESS;
                break;
            }

        }
    }

    if (pCliContext->amb_env_ptr_start != NULL)
    {
        for (i2Index = 0; i2Index < i4FirstTokAmbCount; i2Index++)
        {
            MemReleaseMemBlock (gCliAmbArrPoolId, (UINT1 *)
                                pCliContext->amb_env_ptr_start[i2Index]);
        }
        MemReleaseMemBlock (gCliAmbPtrPoolId, (UINT1 *)
                            pCliContext->amb_env_ptr_start);
        pCliContext->amb_env_ptr_start = NULL;
    }

    for (i2TokIndex = 0; i2TokIndex < MAX_NO_OF_TOKENS_IN_MMI; i2TokIndex++)
    {
        if (pCliContext->ai1Tokentype_flag[i2TokIndex])
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                           vpMmi_values[i2TokIndex]);
            pCliContext->ai1Tokentype_flag[i2TokIndex] = 0;
            pCliContext->vpMmi_values[i2TokIndex] = NULL;
        }
    }
    return i1RetVal;
}

/*****************************************************************************
 * Function Name     : CliRestoreCxtFileInfo                                  *
 *                                                                            *
 * Description       : This function ensures that the context write file      *
 *                     descriptor is restored back to the console after       *
 *                     writing to the specified file and the existing file    *
 *                     descriptor is also closed using FileClose().           *
 *                                                                            *
 * Input Parameters  : pCliContext - Clicontext Information                   *
 *                                                                            *
 * Output Parameters : None                                                   *
 *                                                                            *
 *                                                                            *
 * Return Value      : CLI_SUCCESS/ CLI_FAILURE                               *
 ******************************************************************************/

INT2
CliRestoreCxtFileInfo (tCliContext * pCliContext)
{
    if (TRUE == pCliContext->i4FileFlag)
    {
        FileClose (pCliContext->i4OutputFd);
        pCliContext->i4OutputFd = pCliContext->i4ContextWriteFd;
        pCliContext->fpCliOutput = pCliContext->pTempOutputFp;
        pCliContext->i4FileFlag = FALSE;
    }
    return CLI_SUCCESS;
}
