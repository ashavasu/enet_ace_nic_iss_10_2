/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clilxsup.c,v 1.57 2016/05/24 12:26:33 siva Exp $
 *
 * Description: This will have the suplevel routines   
 *              for lex and history updates            
 *******************************************************************/
#include "clicmds.h"

/* Array of Context Structures */
extern tCliSessions gCliSessions;

extern INT1         ai1BSpaceStr[];
extern INT1         ai1MLBSpaceStr[];
extern tMemPoolId   gCliMaxLineMemPoolId;
VOID
mmi_pause (INT1 *i1str)
{
    INT4                i;
    INT1                c;
    t_MMI_BOOLEAN       i1temp;
    tCliContext        *pcurrContext;

    if ((pcurrContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    i1temp = pcurrContext->mmi_file_flag;
    pcurrContext->mmi_file_flag = FALSE;
    if (i1str != NULL)
    {
        mmi_printf ("%s..", i1str);
    }
    else
    {
        mmi_printf ("Press Any Key...");
    }

    i = CliGetInput (pcurrContext);

    c = (INT1) i;

    if (c != '\n')
    {
        CliGetInput (pcurrContext);
    }

    mmi_print_user_prompt (NULL, pcurrContext);
    mmi_printf ("%s", ai1BSpaceStr);
    mmi_print_user_prompt (NULL, pcurrContext);

    pcurrContext->mmi_file_flag = i1temp;
}

VOID
mmi_echo (INT1 *i1str)
{
    mmi_printf ("%s\n", i1str);
}

VOID
mmi_printf (const char *fmt, ...)
{
    va_list             VarArgList;
    tCliContext        *pCliContext = NULL;
    UINT4               u4Len;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Taks Fonud */
        return;
    }
    /*if it is application context, suppress printing */
    if (pCliContext->gu4CliMode == CLI_MODE_APP)
    {
        return;
    }

    if (!(pCliContext->mmi_echo_flag) || (pCliContext->mmi_exit_flag == TRUE))
    {
        return;
    }
 
    if (pCliContext->pu1OutputMessage == NULL)
    {
        return;
    }

    if (pCliContext->u1PrintOngoing == TRUE)
    {
        return;
    }

    else
    {
        pCliContext->u1PrintOngoing = TRUE;
    }

    pCliContext->pu1OutputMessage[0] = '\0';

    va_start (VarArgList, fmt);

    u4Len = VSNPRINTF ((CHR1 *) pCliContext->pu1OutputMessage,
                       MAX_CLI_OUTPUT_BUF_SIZE, fmt, VarArgList);

    u4Len = (u4Len > MAX_CLI_OUTPUT_BUF_SIZE) ? MAX_CLI_OUTPUT_BUF_SIZE : u4Len;

    if (u4Len > 0)
    {
        pCliContext->fpCliOutput (pCliContext,
                                  (CHR1 *) pCliContext->pu1OutputMessage,
                                  u4Len);
    }
    va_end (VarArgList);
    pCliContext->u1PrintOngoing = FALSE;
    return;
}

INT4
mmi_more_printf (INT1 *i1FmtStr)
{
    return (mmi_multi_more_printf (i1FmtStr, TRUE));
}

INT4
mmi_multi_more_printf (INT1 *i1FmtStr, INT1 bIsFirst)
{
    tCliContext        *pCliContext;
    INT2                i2RetVal;

    UNUSED_PARAM (bIsFirst);
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return CLI_ERROR;
    };
    if (i1FmtStr == NULL)
    {
        return CLI_ERROR;
    }
    if (pCliContext->mmi_exit_flag == TRUE)
    {
        return CLI_NO_ERROR;
    }

    /* Calls CliPipeWrite or CliSerialWrite/CliTelnetWrite 
     * based on fpCliOutput function ptr
     */

    i2RetVal = (INT2) pCliContext->fpCliOutput (pCliContext,
                                                (CHR1 *) i1FmtStr,
                                                STRLEN (i1FmtStr));

    i2RetVal = (i2RetVal == CLI_FAILURE) ? (CLI_ERROR) : (CLI_NO_ERROR);
    return (i2RetVal);
}

VOID
mmi_set_echo (INT1 i1EchoStatus)
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    }

    pCliContext->mmi_echo_flag = i1EchoStatus;

    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliMorePrintf        
 * DESCRIPTION   : This function takes care of the page-wise display of the
 *                 output when 'more' is enabled.                       
 *                 
 * INPUT         : pCliContext     - Pointer to the current Cli context
 *                 i4LineIndex     - Index of the 'more' display buffer
 * 
 * OUTPUT        : NULL
 ***************************************************************************/
INT4
CliMorePrintf (tCliContext * pCliContext, INT4 i4LineIndex)
{
    INT1                i1RetChar = 0;
    INT1                i1MoreFlag = FALSE;
    INT1                i1ConResetFlag = FALSE;
    INT1                i1EndOfData = FALSE;
    UINT1               au1LineBuf[MAX_LINE_LEN];
    UINT1              *pu1TmpLineBuf = NULL;
    UINT1              *pu1OutputBuf = NULL;
    INT2                i2RetVal = CLI_SUCCESS;
    INT4                i4LineCount = 0;
    UINT4               u4Counter = 0;
    UINT4               u4TempLen = 0;
    INT4                i4ColWidth = 0;
    INT4                i4LineCountIndex = 0;
    INT4                i4PageTopOffset = -1;
    INT4                (*pCliOutFcnPtr) (struct CliContext *,
                                          CONST CHR1 *, UINT4);
    INT4                (*pCliOutTempFcnPtr) (struct CliContext *,
                                              CONST CHR1 *, UINT4);
    UINT1              *pu1TempCmd = NULL;
    /* Check if 'q' entered at 'more' display */
    if (pCliContext->i1MoreQuitFlag)
        return CLI_FAILURE;

    pCliOutFcnPtr = pCliContext->fpCliOutput;
    pCliContext->fpCliOutput = pCliContext->fpCliTempOutput;

    if (pCliContext->ppu1OutBuf[i4LineIndex] == NULL)
    {
        pCliContext->fpCliOutput = pCliOutFcnPtr;
        return CLI_FAILURE;
    }

    if (!(*pCliContext->ppu1OutBuf[i4LineIndex]))
    {
        i1EndOfData = TRUE;
        i2RetVal = CLI_FAILURE;
    }
    if ((pCliContext->i1GrepFlag) && (pCliContext->u1GrepErrorFlag != TRUE))
    {
        if (CliProcessGrep (pCliContext, pCliContext->ppu1OutBuf[i4LineIndex])
            == CLI_SUCCESS)
        {
            if (pCliContext->u4LineCount < (UINT4) i4LineIndex)
            {

                if (pCliContext->u4ConcatFlag == TRUE)
                {
                    pu1TempCmd =
                        pCliContext->ppu1OutBuf[pCliContext->u4LineCount];
                    u4Counter = pCliContext->u4LineCount;
                    pCliContext->ppu1OutBuf[pCliContext->u4LineCount] =
                        CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN);
                    if (pCliContext->ppu1OutBuf[pCliContext->u4LineCount] ==
                        NULL)
                    {
                        mmi_printf
                            ("\r\n Cannot allocate memory for command String\n");
                        pCliContext->fpCliOutput = pCliOutFcnPtr;
                        return CLI_FAILURE;
                    }

                    i1ConResetFlag = TRUE;
                }
                i4LineCountIndex = pCliContext->u4LineCount;
                pCliContext->u4LineCount++;
		pCliContext->u4LineWriteCount++;
            }
            else
            {
                i4LineCountIndex = i4LineIndex;
                pCliContext->u4LineCount++;
		pCliContext->u4LineWriteCount++;
            }
        }
        else
        {
            pCliContext->ppu1OutBuf[i4LineIndex][0] = '\0';
	    pCliContext->u4LineCount++;
        }
    }
    else if (!i1EndOfData)
    {
        pCliContext->u4LineCount++;
	pCliContext->u4LineWriteCount++;
    }
    if (pCliContext->i1GrepFlag != TRUE)
    {
        if (((pCliContext->u4LineCount >= (UINT4) (pCliContext->i2MaxRow - 1))
            || (i4LineIndex >= (pCliContext->i2MaxRow - 1))) &&
	    (pCliContext->u4LineWriteCount >= (UINT4) (pCliContext->i2MaxRow - 1)))
            i1MoreFlag = TRUE;
    }
    else
    {
        if ((pCliContext->u4LineWriteCount >= (UINT4) (pCliContext->i2MaxRow - 1))
            || (i4LineCountIndex >= (pCliContext->i2MaxRow - 1)))
            i1MoreFlag = TRUE;
    }
    if (pCliContext->i1GrepFlag != TRUE)
    {
        if (i4LineIndex > pCliContext->i2MaxRow)
        {
            i4PageTopOffset = i4LineIndex - pCliContext->i2MaxRow;
        }
        else
        {
            i4PageTopOffset = 0;
        }
    }
    else
    {
        if (i4LineCountIndex > pCliContext->i2MaxRow)
        {
            i4PageTopOffset = i4LineCountIndex - pCliContext->i2MaxRow;
        }
        else
        {
            i4PageTopOffset = 0;
        }
    }

    pCliContext->pu1OutputBuf = CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN);
    if (pCliContext->pu1OutputBuf == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for command String\n");

        if ((pCliContext->u4ConcatFlag == TRUE) && (i1ConResetFlag == TRUE))
        {
            MemReleaseMemBlock (gCliMaxLineMemPoolId,
                                pCliContext->ppu1OutBuf[u4Counter]);
            pCliContext->ppu1OutBuf[u4Counter] = pu1TempCmd;
            i1ConResetFlag = FALSE;

        }
        pCliContext->fpCliOutput = pCliOutFcnPtr;
        return CLI_FAILURE;
    }
    pu1OutputBuf = pCliContext->pu1OutputBuf;

    while (1)
    {
        if ((pCliContext->i1MoreQuitFlag) || (pCliContext->mmi_exit_flag))
        {
            i2RetVal = CLI_FAILURE;
            break;
        }
        if (pCliContext->ppu1OutBuf != NULL)
        {
            if (!(*pCliContext->ppu1OutBuf[i4LineIndex]) && !(i1EndOfData))
                break;
        }
        else
        {
            break;
        }
        MEMSET (au1LineBuf, 0, sizeof (au1LineBuf));
        STRNCPY (au1LineBuf, pCliContext->ppu1OutBuf[i4LineIndex],
                 (sizeof (au1LineBuf) - 1));
        if (i1MoreFlag && !pCliContext->i1MoreQuitFlag)
        {
            if (pCliContext->i2PgDnLineCount)
            {
                i1RetChar = CLI_SPACE;
            }
            if ((i1RetChar != CLI_SPACE)
                && (TOLOWER (i1RetChar) != CLI_QUIT_CHAR)
                && (i1RetChar != CTRL_CHAR_START))
                i1RetChar = CliMorePrompt ();

            if ((pCliContext->mmi_exit_flag == TRUE) ||
                (pCliContext->i1WinResizeFlag == TRUE) ||
                (pCliContext->i1PipeDisable == OSIX_TRUE))
            {
                pCliOutTempFcnPtr = pCliContext->fpCliOutput;
                if (pCliContext->i1WinResizeFlag == TRUE)
                {
                    pCliContext->fpCliOutput = pCliOutFcnPtr;
                }
                else
                {
                    pCliContext->fpCliOutput = pCliOutTempFcnPtr;
                }
                if ((pCliContext->u4ConcatFlag == TRUE)
                    && (i1ConResetFlag == TRUE))
                {
                    MemReleaseMemBlock (gCliMaxLineMemPoolId,
                                        pCliContext->ppu1OutBuf[u4Counter]);
                    pCliContext->ppu1OutBuf[u4Counter] = pu1TempCmd;
                    i1ConResetFlag = FALSE;

                }
                CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1OutputBuf);
                return CLI_FAILURE;
            }

            if (TOLOWER (i1RetChar) == CLI_QUIT_CHAR)
                i1RetChar = CLI_QUIT_CHAR;

            switch (i1RetChar)
            {
                case CLI_QUIT_CHAR:
                    pCliContext->i1MoreQuitFlag = TRUE;
                    i2RetVal = CLI_SUCCESS;
                    break;

                case CTRL_CHAR_START:
                    pCliContext->fpCliInput (pCliContext);
                    if ((i1RetChar = (INT1) pCliContext->fpCliInput
                         (pCliContext)) == CLI_UPARROW)
                    {
                        i1RetChar = 0;
                        if (CliMoreHandleUpArrow (pCliContext, i4LineIndex,
                                                  &i4PageTopOffset) ==
                            CLI_FAILURE)
                        {
                            i1RetChar = CliMorePrompt ();
                            *au1LineBuf = '\0';
                        }
                        else
                        {
                            i4LineCount -= 2;
                            i4LineIndex -= 2;
                            STRNCPY (au1LineBuf,
                                     pCliContext->ppu1OutBuf[i4PageTopOffset],
                                     (sizeof (au1LineBuf) - 1));
                        }
                    }
                    else if (i1RetChar == CLI_DOWNARROW)
                    {
                        i1RetChar = 0;
                        if (CliMoreHandleDownArrow (pCliContext, &i4LineIndex,
                                                    &i1RetChar) == CLI_FAILURE)
                        {
                            pCliContext->i1MoreQuitFlag = TRUE;
                        }
                    }
                    else
                    {
                        i1RetChar = 0;
                        continue;
                    }
                    break;

                case CLI_SPACE:
                    pCliContext->i2PgDnLineCount++;
                    if (pCliContext->i2PgDnLineCount >=
                        (pCliContext->i2MaxRow - 1))
                    {
                        pCliContext->i2PgDnLineCount = 0;
                        i1RetChar = 0;
                    }
                    else
                    {
                        if (CliMoreHandleDownArrow (pCliContext, &i4LineIndex,
                                                    &i1RetChar) == CLI_FAILURE)
                        {
                            pCliContext->i1MoreQuitFlag = TRUE;
                        }
                    }
                    break;

                case CLI_ENTER:
                    i1RetChar = 0;
                    if (CliMoreHandleDownArrow (pCliContext, &i4LineIndex,
                                                &i1RetChar) != CLI_FAILURE)
                    {
                        STRNCPY (au1LineBuf,
                                 pCliContext->ppu1OutBuf[i4LineIndex],
                                 (sizeof (au1LineBuf) - 1));
                    }
                    break;
                default:
                    i1RetChar = 0;
                    CLI_BEEP ();
                    continue;
            }
        }
        if ((*au1LineBuf) && (!pCliContext->i1MoreQuitFlag))
        {

            i4ColWidth = pCliContext->i2MaxCol;

            pu1TmpLineBuf = au1LineBuf;
            if (STRCMP (pu1TmpLineBuf, "\r\n") == 0)
            {
                STRCPY (pu1OutputBuf, pu1TmpLineBuf);
            }
            while (*pu1TmpLineBuf != '\0')
            {
                if (*pu1TmpLineBuf == '\n')
                {
                    break;
                }
                else if (*pu1TmpLineBuf == '\r' && *(pu1TmpLineBuf + 1) != '\0')
                {
                    if (*(pu1TmpLineBuf + 1) == '\n')
                    {
                        break;
                    }
                }

                STRNCAT (pu1OutputBuf, pu1TmpLineBuf, i4ColWidth);
                u4TempLen = STRLEN (pu1OutputBuf);

                if (pu1OutputBuf[u4TempLen - 1] != '\n')
                {
                    /* SPRINTF ((CHR1 *) pu1OutputBuf[u4TempLen], "\r\n"); */

                    STRCAT (pu1OutputBuf, "\r\n");
                }
                pu1TmpLineBuf = (pu1TmpLineBuf + i4ColWidth);
            }
            i2RetVal =
                pCliContext->fpCliOutput (pCliContext,
                                          (CONST CHR1 *) pu1OutputBuf,
                                          STRLEN (pu1OutputBuf));

            pu1TmpLineBuf = NULL;
            CLI_MEMSET (pu1OutputBuf, 0, MAX_LINE_LEN);
            if (i2RetVal == CLI_FAILURE)
            {
                pCliContext->i1MoreQuitFlag = TRUE;
                break;
            }
            i4LineCount++;
            i4LineIndex++;
            i4LineCountIndex++;
            if (pCliContext->i1GrepFlag != TRUE)
            {
                if (i4LineIndex < 0)
                {
                    i4LineIndex += ((pCliContext->i2MaxRow - 1) *
                                    pCliContext->i1PageCount);
                }
                else if (i4LineIndex >= ((pCliContext->i2MaxRow - 1) *
                                         pCliContext->i1PageCount))
                {
                    i4LineIndex %= ((pCliContext->i2MaxRow - 1) *
                                    pCliContext->i1PageCount);
                }
            }
            else
            {
                if (i4LineIndex >= ((pCliContext->i2MaxRow - 1) *
                                    pCliContext->i1PageCount))
                {
                    i4LineIndex %= ((pCliContext->i2MaxRow - 1) *
                                    pCliContext->i1PageCount);
                }
                if (i4LineCountIndex < 0)
                {
                    i4LineCountIndex += ((pCliContext->i2MaxRow - 1) *
                                         pCliContext->i1PageCount);
                }
                else if (i4LineCountIndex >= ((pCliContext->i2MaxRow - 1) *
                                              pCliContext->i1PageCount))
                {
                    i4LineCountIndex %= ((pCliContext->i2MaxRow - 1) *
                                         pCliContext->i1PageCount);
                }
            }
            if (i4LineCount >= (pCliContext->i2MaxRow - 1))
                i1MoreFlag = TRUE;
        }
        if (pCliContext->u4LineWriteCount < (UINT4) (pCliContext->i2MaxRow - 1))
        {
            break;
        }
    }                            /* end while */

    if ((pCliContext->u4ConcatFlag == TRUE) && (i1ConResetFlag == TRUE))
    {
        MemReleaseMemBlock (gCliMaxLineMemPoolId,
                            pCliContext->ppu1OutBuf[u4Counter]);
        pCliContext->ppu1OutBuf[u4Counter] = pu1TempCmd;
        i1ConResetFlag = FALSE;

    }
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1OutputBuf);
    pCliContext->fpCliOutput = pCliOutFcnPtr;
    return (i2RetVal);
}

/***************************************************************************  
 * FUNCTION NAME : CliPrintf        
 * DESCRIPTION   : The message is stored in a separate buffer. If the 
 *                 buffer is full, then the message is written onto the
 *                 output.
 *                 
 * INPUT         : CliHandle       - Index of the current Cli Context 
 *                                   structure in the global array.
 *                 fmt             - format of the output message      
 * 
 * OUTPUT        : NULL
 ***************************************************************************/
INT4
CliPrintf (tCliHandle CliHandle, CONST CHR1 * fmt, ...)
{
    va_list             VarArgList;
    tCliContext        *pCliContext = NULL;
    UINT4               u4Len;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4TempIndex = 0;
    UINT4               u4WriteCnt = 0;
    UINT1              *pu1TempBuf = NULL;

    if ((CliHandle >= CLI_MAX_SESSIONS) || (CliHandle < 0))
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    /*if it is application context, suppress printing */
    if ((pCliContext->gu4CliMode == CLI_MODE_APP) &&
        (FALSE == pCliContext->i4FileFlag))
    {
        return (CLI_SUCCESS);
    }

    if (pCliContext->i1Status != CLI_ACTIVE)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    if (!(pCliContext->mmi_echo_flag) || (pCliContext->mmi_exit_flag == TRUE))
    {
        return (CLI_SUCCESS);
    }

    if ((pCliContext->i1PipeFlag == TRUE)
        && (pCliContext->i1MoreQuitFlag == TRUE))
    {
        return (CLI_FAILURE);
    }

    if (pCliContext->pu1OutputMessage == NULL)
    {
        return (CLI_FAILURE);
    }

    pCliContext->pu1OutputMessage[0] = '\0';

    va_start (VarArgList, fmt);

    u4Len = VSNPRINTF ((CHR1 *) pCliContext->pu1OutputMessage,
                       MAX_CLI_OUTPUT_BUF_SIZE, fmt, VarArgList);

    u4Len =
        (u4Len >
         MAX_CLI_OUTPUT_BUF_SIZE - 1) ? (MAX_CLI_OUTPUT_BUF_SIZE - 1) : u4Len;

    /* in telnet case, if \r is not there, it will not be aligned. so ensure \r is present
     * before the last byte of new line \n
     */

    if ((pu1TempBuf = CliMemAllocMemBlk (gCliCtxPendBuff,
                                         MAX_CLI_OUTPUT_PEND_BUF_SIZE)) == NULL)
    {
        va_end (VarArgList);
        return CLI_FAILURE;
    }

    while (u4TempIndex <= u4Len)
    {
        if ((u4TempIndex > 0)
            && ((pCliContext->pu1OutputMessage[u4TempIndex] == '\n')
                && (pCliContext->pu1OutputMessage[u4TempIndex - 1] != '\r')))
        {
            if (u4WriteCnt < MAX_CLI_OUTPUT_BUF_SIZE - 2)
            {
                pu1TempBuf[u4WriteCnt] = '\r';
                pu1TempBuf[++u4WriteCnt] = '\n';
                u4WriteCnt++;
            }

        }
        else
        {
            if (u4WriteCnt < MAX_CLI_OUTPUT_BUF_SIZE - 1)
            {
                pu1TempBuf[u4WriteCnt] =
                    pCliContext->pu1OutputMessage[u4TempIndex];
                u4WriteCnt++;
            }
        }
        u4TempIndex++;
    }

    if (u4WriteCnt > 0)
    {
        if ((u4WriteCnt - 1)
            <= (MAX_CLI_OUTPUT_PEND_BUF_SIZE - pCliContext->u4PendBuffLen))
        {
            /* 
             * There is enough space in the pend buffer to store
             * the output string.
             */
            CLI_MEMCPY (&pCliContext->pu1PendBuff[pCliContext->u4PendBuffLen],
                        pu1TempBuf, (u4WriteCnt - 1));

            pCliContext->u4PendBuffLen =
                pCliContext->u4PendBuffLen + (u4WriteCnt - 1);

            i4RetStatus = CLI_SUCCESS;
        }
        else
        {
            i4RetStatus = CliFlushOutputBuff (pCliContext);

            if (i4RetStatus == CLI_SUCCESS)
            {

                /*
                 * The message in the buffer is flushed, so there will
                 * be enough space to hold the current message.
                 */
                CLI_MEMCPY (&pCliContext->pu1PendBuff
                            [pCliContext->u4PendBuffLen],
                            pu1TempBuf, (u4WriteCnt - 1));

                pCliContext->u4PendBuffLen =
                    pCliContext->u4PendBuffLen + (u4WriteCnt - 1);
            }
        }
    }
    va_end (VarArgList);
    MemReleaseMemBlock (gCliCtxPendBuff, (UINT1 *) pu1TempBuf);
    pu1TempBuf = NULL;
    return (i4RetStatus);
}

/***************************************************************************
 * FUNCTION NAME : CliFlushOutputBuff ()
 * DESCRIPTION   : This function writes the formatted message onto the    
 *                 Output (dislpay by default).                       
 *                 The message to be written is present in 'pCliContext'
 *                             
 * INPUT         : pCliContext - Pointer to the Cli Context structure 
 * 
 * OUTPUT        : NULL
 *
 * RETURN VALUE  : CLI_FAILURE will be return when the exit flag is set
 ***************************************************************************/
INT4
CliFlushOutputBuff (tCliContext * pCliContext)
{
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4LockRetStatus = CLI_SUCCESS;
    UINT4               u4WriteCnt = 0;
    UINT4               u4TempIndex = 0;
    UINT4               u2EscapeCnt = 1;
    UINT4               u4TmpIndex = 0;
    UINT1              *pu1TempBuf = NULL;

    /* Reset TimedWait failure flag */
    pCliContext->u1IsTimedWaitLockFail = CLI_FALSE;

    /* Block to avoid continuous "!" lines from "show running-config" output */
    if (pCliContext->u1SkipDoubleBang == CLI_HANDLE_BANG ||
        pCliContext->u1SkipDoubleBang == CLI_HANDLE_AND_RESET_BANG)
    {
        if ((pu1TempBuf = CliMemAllocMemBlk (gCliCtxPendBuff,
                                             MAX_CLI_OUTPUT_PEND_BUF_SIZE))
            == NULL)
        {
            return CLI_FAILURE;
        }
        while (u4TempIndex < pCliContext->u4PendBuffLen)
        {
            if (pCliContext->pu1PendBuff[u4TempIndex] == '!')
            {
                /* To save first "!" */
                if (u2EscapeCnt < 3)
                {
                    u2EscapeCnt++;
                    pu1TempBuf[u4WriteCnt] =
                        pCliContext->pu1PendBuff[u4TempIndex];
                    u4WriteCnt++;
                    u4TempIndex++;
                }
                /* Skip continuous "!" */
                else
                {
                    /* To handle "!\r\n" */
                    if ((pCliContext->pu1PendBuff[u4TempIndex + 1] == '\r') &&
                        (pCliContext->pu1PendBuff[u4TempIndex + 2] == '\n'))
                    {
                        u4TempIndex += 3;
                        continue;
                    }
                    /* To handle "! \r\n" */
                    else if ((pCliContext->pu1PendBuff[u4TempIndex + 1] == '\n')
                             || (pCliContext->pu1PendBuff[u4TempIndex + 1] ==
                                 '\r')
                             || (pCliContext->pu1PendBuff[u4TempIndex + 1] ==
                                 ' '))
                    {
                        u4TempIndex += 2;
                        continue;
                    }
                    /* To handle "!!" */
                    else
                    {
                        u4TempIndex++;
                        continue;
                    }
                }
            }
            else
            {
                pu1TempBuf[u4WriteCnt] = pCliContext->pu1PendBuff[u4TempIndex];
                if ((u2EscapeCnt > 1) &&
                    (pCliContext->pu1PendBuff[u4TempIndex] != '\r') &&
                    (pCliContext->pu1PendBuff[u4TempIndex] != '\n') &&
                    (pCliContext->pu1PendBuff[u4TempIndex] != ' '))
                {
                    u2EscapeCnt = 1;
                }
                u4WriteCnt++;
                u4TempIndex++;

            }
        }
        /* Check to remove unwanted Blank space in the output */
        while ((pCliContext->u4PendBuffLen != 0) &&
               (u4TmpIndex < (u4WriteCnt - 3)))
        {
            if (((pu1TempBuf[u4TmpIndex] == '\r') ||
                 (pu1TempBuf[u4TmpIndex] == '\n')) &&
                (pu1TempBuf[u4TmpIndex + 1] == '\n') &&
                ((pu1TempBuf[u4TmpIndex + 2] == '\n') ||
                 (pu1TempBuf[u4TmpIndex + 2] == '\r')) &&
                (pu1TempBuf[u4TmpIndex + 3] == '\n'))
            {
                pu1TempBuf[u4TmpIndex] = ' ';
                pu1TempBuf[u4TmpIndex + 1] = ' ';
            }
            u4TmpIndex++;

        }
    }

    if (pCliContext->u4PendBuffLen != 0)
    {
        /*
         * pCliContext->fpCliOutput () could be a blocking
         * call and hence the protocol and management locks
         * must be released.
         *
         * NOTE: Protocol MUST have taken the Lock before
         * calling this function.
         */
        if (pCliContext->fpUnLock != NULL)
        {
            pCliContext->fpUnLock ();
        }
        MGMT_UNLOCK ();
        if (pCliContext->u1SkipDoubleBang == CLI_HANDLE_BANG ||
            pCliContext->u1SkipDoubleBang == CLI_HANDLE_AND_RESET_BANG)
        {
            i4RetStatus = pCliContext->fpCliOutput (pCliContext,
                                                    (CHR1 *) pu1TempBuf,
                                                    u4WriteCnt);
        }
        else
        {
            i4RetStatus = pCliContext->fpCliOutput (pCliContext,
                                                    (CHR1 *) pCliContext->
                                                    pu1PendBuff,
                                                    pCliContext->u4PendBuffLen);
        }
        MGMT_LOCK ();
        if (pCliContext->fpLock != NULL)
        {
            i4LockRetStatus = pCliContext->fpLock ();
            if (OSIX_ERR_TIMEOUT ==  i4LockRetStatus)
            {
                pCliContext->u1IsTimedWaitLockFail = CLI_TRUE;
                i4RetStatus = CLI_FAILURE;
            }
        }

        /* Exit flag check is done. As there is some possiblity for abrupt
         * close of telnet sessions in fpCliOutput funcion */

        if (!(pCliContext->mmi_exit_flag))
        {
            pCliContext->u4PendBuffLen = 0;
            pCliContext->pu1PendBuff[0] = '\0';
        }
        else
        {
            if (pCliContext->u1SkipDoubleBang == CLI_HANDLE_BANG ||
                pCliContext->u1SkipDoubleBang == CLI_HANDLE_AND_RESET_BANG)
            {
                MemReleaseMemBlock (gCliCtxPendBuff, (UINT1 *) pu1TempBuf);
                pu1TempBuf = NULL;
            }

            return CLI_FAILURE;
        }
    }
    if (pCliContext->u1SkipDoubleBang == CLI_HANDLE_BANG ||
        pCliContext->u1SkipDoubleBang == CLI_HANDLE_AND_RESET_BANG)
    {
        MemReleaseMemBlock (gCliCtxPendBuff, (UINT1 *) pu1TempBuf);
        pu1TempBuf = NULL;
    }
    if (pCliContext->u1SkipDoubleBang == CLI_HANDLE_AND_RESET_BANG)
    {
        pCliContext->u1SkipDoubleBang = CLI_NO_HANDLE_BANG;
    }
    return i4RetStatus;
}

/***************************************************************************  
 * FUNCTION NAME : CliFlush ()
 * DESCRIPTION   : Causes the buffered data to be output.
 * INPUT         : CliHandle       - Index of the current Cli Context 
 *                                   structure in the global array.
 * OUTPUT        : None.
 * RETURN VALUE  : None.
 ***************************************************************************/
void
CliFlush (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);
    CliFlushOutputBuff (pCliContext);
}

/***************************************************************************
 * FUNCTION NAME : CliFlushWithRetCheck ()
 * DESCRIPTION   : Causes the buffered data to be output.
 * INPUT         : CliHandle       - Index of the current Cli Context
 *                                   structure in the global array.
 * OUTPUT        : pu1IsTimedWaitLockFail - Boolean that indicates whether
 *                                          OSIX timed wait failed on timer
 *                                          expiry.
 * RETURN VALUE  : CLI_SUCCESS / CLI_FAILURE.
 ***************************************************************************/
INT4
CliFlushWithRetCheck (tCliHandle CliHandle, UINT1 *pu1IsTimedWaitLockFail)
{
    tCliContext        *pCliContext;
    INT4                i4RetStatus = CLI_FAILURE;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    i4RetStatus = CliFlushOutputBuff (pCliContext);
    *pu1IsTimedWaitLockFail = pCliContext->u1IsTimedWaitLockFail;

    return (i4RetStatus);
}

/***************************************************************************  
 * FUNCTION NAME : CliRegisterLock ()
 * DESCRIPTION   : This function registers the Lock/UnLock functions of
 *                 the Protocol. These functions will be used in CliPrintf
 *                 before calling any blocking calls.
 *                 
 * INPUT         : CliHandle       - Index of the current Cli Context 
 *                                   structure in the global array.
 *                 fpLock          - Pointer to the Protocol Lock function
 *                 fpUnLock        - Pointer to the Protocol UnLock function
 * 
 * OUTPUT        : None
 ***************************************************************************/
VOID
CliRegisterLock (tCliHandle CliHandle,
                 INT4 (*fpLock) (VOID), INT4 (*fpUnLock) (VOID))
{
    tCliContext        *pCliContext;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    pCliContext->fpLock = fpLock;
    pCliContext->fpUnLock = fpUnLock;

    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliUnRegisterLock ()
 * DESCRIPTION   : This function unregisters the Lock/UnLock functions
 *                 registered by CliRegisterLock ().
 *                 
 * INPUT         : CliHandle       - Index of the current Cli Context 
 *                                   structure in the global array.
 * 
 * OUTPUT        : None
 ***************************************************************************/
VOID
CliUnRegisterLock (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    pCliContext->fpLock = NULL;
    pCliContext->fpUnLock = NULL;

    return;
}

/***************************************************************************
 * FUNCTION NAME : CliUnRegisterAndRemoveLock ()
 * DESCRIPTION   : This function checks if there occured a timeout on a sema4
 *                 lock, unlock if required and then unregisters the
 *                 Lock/UnLock functions registered by CliRegisterLock ().
 *
 * INPUT         : CliHandle       - Index of the current Cli Context
 *                                   structure in the global array.
 *
 * OUTPUT        : None
 ***************************************************************************/
VOID
CliUnRegisterAndRemoveLock (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);
    if (CLI_FALSE == pCliContext->u1IsTimedWaitLockFail)
    {
        if (pCliContext->fpUnLock != NULL)
        {
            pCliContext->fpUnLock ();
        }
    }
    else
    {
        pCliContext->u1IsTimedWaitLockFail = CLI_FALSE;
    }
    pCliContext->fpLock = NULL;
    pCliContext->fpUnLock = NULL;

    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliMoreHandleUpArrow
 * DESCRIPTION   : This function takes care of scrolling when a Up arrow  
 *                 is pressed at the 'more' display.                       
 *                 
 * INPUT         : pCliContext     - Current Cli Context structure
 *                 i4LineIndex     - Index of the 'more' display buffer
 *                 i4PageBtmOffset - Index of the bottom line of the 
 *                                   'more' display buffer
 * 
 * OUTPUT        : pi4PageTopOffset - Index of the top line of the 'more'
 *                                    display buffer
 ***************************************************************************/
INT4
CliMoreHandleUpArrow (tCliContext * pCliContext, INT4 i4LineIndex,
                      INT4 *pi4PageTopOffset)
{
    INT4                i4RetVal = CLI_SUCCESS;

    /* If target doesn't supports Ioctl with TIOCGWINSZ then the
     * CLI_TIOCGWINSZ_MASK bit will be set and 
     * if set disables the UP arrow scrolling feature by returning FAILURE.
     */
    if (CLI_ISTIOCGWINSZ_SET (pCliContext->u4Flags))
        return CLI_FAILURE;

    if (!pCliContext->u4LineCount)
    {
        i4RetVal = CLI_FAILURE;
    }
    else
    {
        if (i4LineIndex < pCliContext->i2MaxRow)
        {
            i4LineIndex = (i4LineIndex + ((pCliContext->i2MaxRow - 1) *
                                          pCliContext->i1PageCount)) -
                (pCliContext->i2MaxRow);
        }
        else
        {
            i4LineIndex -= pCliContext->i2MaxRow;
        }

        if (pCliContext->ppu1OutBuf[i4LineIndex] == NULL)
        {
            return CLI_FAILURE;
        }

        if (!(*pCliContext->ppu1OutBuf[i4LineIndex]))
        {
            i4RetVal = CLI_FAILURE;
        }
        else
        {
            *pi4PageTopOffset = i4LineIndex;
            i4RetVal = CLI_SUCCESS;
        }
    }

    if (i4RetVal == CLI_SUCCESS)
    {
        mmi_printf (CLI_SCROLL_UP);
        mmi_printf (CLI_SCROLL_UP1);
    }
    return (i4RetVal);
}

/***************************************************************************  
 * FUNCTION NAME : CliMoreHandleDownArrow
 * DESCRIPTION   : This function takes care of scrolling when a Down arrow  
 *                 is pressed at the 'more' display.                       
 *                 
 * INPUT         : pCliContext  - Current Cli Context structure
 * 
 * OUTPUT        : pi4LineIndex - Index of the More display buffer
 *                 pi1RetChar   - Character entered at mmi_more() display
 ***************************************************************************/
INT4
CliMoreHandleDownArrow (tCliContext * pCliContext, INT4 *pi4LineIndex,
                        INT1 *pi1RetChar)
{
    INT4                i4RetVal = CLI_SUCCESS;

    if (pCliContext->ppu1OutBuf[*pi4LineIndex] == NULL)
    {
        return CLI_FAILURE;
    }

    if (!*pCliContext->ppu1OutBuf[*pi4LineIndex])
    {
        pCliContext->i2PgDnLineCount = 0;    /* No more page scroll down */
        pCliContext->i1MoreQuitFlag = TRUE;
        *pi1RetChar = CLI_QUIT_CHAR;
        i4RetVal = CLI_FAILURE;
        if (pCliContext->fpCliOutput (pCliContext, CLI_DEFAULT_BG,
                                      STRLEN (CLI_DEFAULT_BG)) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        *pi4LineIndex =
            CliGetNextLineOffSet (pCliContext, (INT2) *pi4LineIndex);
    }

    return (i4RetVal);
}

/***************************************************************************  
 * FUNCTION NAME : CliBuddyMalloc
 * DESCRIPTION   : This function takes care of allocating memory from buddy 
 *                 and keeps track of total memory allocated. If allocation 
 *                 fails then kills the current CLI session.
 *                 
 * INPUT         : u4Size     - Size of memory to be allocated
 * 
 * OUTPUT        : VOID *     - A void pointer to the allocated memory area.
 ***************************************************************************/

VOID               *
CliBuddyMalloc (UINT4 u4Size)
{
    tCliContext        *pCliContext = NULL;
    UINT1              *pu1Ptr = NULL;
    INT1                i1Log[256];
#ifdef CLI_MEM_TRC
    INT2                i2Index = 0;
#endif

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (NULL);
    }
    if ((pu1Ptr = MemBuddyAlloc ((UINT1) pCliContext->i4BuddyId, u4Size))
        == NULL)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "Memory Allocation Failure\r\n");
        UtlTrcPrint ((CHR1 *) i1Log);
    }
    else
    {
#ifdef CLI_MEM_TRC
        for (i2Index = 0; i2Index < CLI_MAX_NO_OF_ALLOC; i2Index++)
        {
            if (!pCliContext->MemPoolTrc.pu1MemPtr[i2Index])
                break;
        }
        if (i2Index == CLI_MAX_NO_OF_ALLOC)
        {
#ifdef CLI_DEBUG
            mmi_printf
                ("\r\nTrack count of memory allocated exceeded the MAX value\r\n");
#endif
        }
        else
        {
            pCliContext->MemPoolTrc.pu1MemPtr[i2Index] = pu1Ptr;
            pCliContext->MemPoolTrc.u4MemSize[i2Index] = u4Size;
            pCliContext->MemPoolTrc.u4BufSize += u4Size;
        }
#endif
    }

    return pu1Ptr;
}

/***************************************************************************  
 * FUNCTION NAME : CliBuddyCalloc
 * DESCRIPTION   : This function takes care of allocating memory from buddy
 *                 and keeps track of total memory allocated and 
 *                 also initialises the memory area.
 *                 
 * INPUT         : u4NumBlocks - No. of blocks of memory to be allocated       
 *                 u4Size      - Size of each block of memory 
 *
 * OUTPUT        : VOID *     - A void pointer to the allocated memory area.
 ***************************************************************************/

VOID               *
CliBuddyCalloc (UINT4 u4NumBlocks, UINT4 u4Size)
{
    tCliContext        *pCliContext = NULL;
    UINT1              *pu1Ptr = NULL;
    INT1                i1Log[256];
#ifdef CLI_MEM_TRC
    INT2                i2Index = 0;
#endif
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (NULL);
    }

    if ((pu1Ptr = MemBuddyAlloc
         ((UINT1) pCliContext->i4BuddyId, (u4Size * u4NumBlocks))) == NULL)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "Memory Allocation Failure\r\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        CliDestroySession (pCliContext);
    }
    else
    {
        MEMSET (pu1Ptr, 0, (u4Size * u4NumBlocks));
#ifdef CLI_MEM_TRC
        for (i2Index = 0; i2Index < CLI_MAX_NO_OF_ALLOC; i2Index++)
        {
            if (!pCliContext->MemPoolTrc.pu1MemPtr[i2Index])
                break;
        }
        if (i2Index == CLI_MAX_NO_OF_ALLOC)
        {
#ifdef CLI_DEBUG
            mmi_printf
                ("\r\nTrack count of memory allocated exceeded the MAX value\r\n");
#endif
        }
        else
        {
            pCliContext->MemPoolTrc.pu1MemPtr[i2Index] = pu1Ptr;
            pCliContext->MemPoolTrc.u4MemSize[i2Index] = u4Size * u4NumBlocks;
            pCliContext->MemPoolTrc.u4BufSize += (u4Size * u4NumBlocks);
        }
#endif
    }

    return pu1Ptr;
}

/***************************************************************************  
 * FUNCTION NAME : CliBuddyFree
 * DESCRIPTION   : This function frees memory allocated already. 
 *                 
 * INPUT         : pu1Ptr     - Pointer to the memory area.
 *
 * OUTPUT        : NULL
 ***************************************************************************/

VOID
CliBuddyFree (UINT1 *pu1Ptr)
{
    tCliContext        *pCliContext = NULL;
#ifdef CLI_MEM_TRC
    INT2                i2Index;
#endif

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found, Doesn't frees the memory */
        mmi_printf ("No task found while freeing memory from Buddy\r\n");
        return;
    }

#ifdef CLI_MEM_TRC
    for (i2Index = 0; i2Index < CLI_MAX_NO_OF_ALLOC; i2Index++)
    {
        if ((pu1Ptr) && (pCliContext->MemPoolTrc.pu1MemPtr[i2Index] == pu1Ptr))
            break;
    }
    if (i2Index != CLI_MAX_NO_OF_ALLOC)
    {
        pCliContext->MemPoolTrc.pu1MemPtr[i2Index] = NULL;
        pCliContext->MemPoolTrc.u4BufSize -=
            pCliContext->MemPoolTrc.u4MemSize[i2Index];
        pCliContext->MemPoolTrc.u4MemSize[i2Index] = 0;
    }
#endif
    MemBuddyFree ((UINT1) pCliContext->i4BuddyId, pu1Ptr);
}

/***************************************************************************  
 * FUNCTION NAME : CliGetMemTrcMemAlloc
 * DESCRIPTION   : This function reports of the memory trace.
 *                 Total un-freed memory.
 *                 
 * INPUT         : NULL
 *
 * OUTPUT        : NULL
 ***************************************************************************/

VOID
CliGetMemTrcMemAlloc (VOID)
{
#ifdef CLI_MEM_TRC
    tCliContext        *pCliContext = NULL;
    INT2                i2Index;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    }
#ifdef CLI_DEBUG
    mmi_printf ("Trace of Memory Allocated and Un-Freed\r\n");
#endif

    for (i2Index = 0; i2Index < CLI_MAX_NO_OF_ALLOC; i2Index++)
    {
        if (pCliContext->MemPoolTrc.pu1MemPtr[i2Index])
        {
#ifdef CLI_DEBUG
            mmi_printf ("Address <%u> <---> <%u> Bytes\r\n",
                        pCliContext->MemPoolTrc.pu1MemPtr[i2Index],
                        pCliContext->MemPoolTrc.u4MemSize[i2Index]);
#endif
        }
    }
    if (pCliContext->MemPoolTrc.u4BufSize)
    {
        mmi_printf ("\r\nTotal Un-Freed memory <%u> bytes \r\n",
                    pCliContext->MemPoolTrc.u4BufSize);
    }
#endif
    return;
}

/***********************************************************************************
 *  FUNCTION NAME : CliPrintfShellBuffers
 *  DESCRIPTION   : This function is used to support directing a output to telnet/ssh
 *  when executing "show petra" commands
 *  
 *  INPUT         :  fmt  - format of the output message
 *  VarArgList-variable list of arguments
 *  
 *  OUTPUT        : NULL
 *************************************************************************************/
static INT4 CliPrintfShellBuffers (CONST CHR1 * fmt,va_list VarArgList )
{
    UINT4 u4Len = 0;
    INT4 i4RetStatus = 0;
    INT4 CliHandle = 0;
    tCliContext *pCliContext = NULL;
    /*To get the telnet/ssh session which is currently active*/
    pCliContext = CliGetContext();
    if (pCliContext == NULL)
    {
        return CLI_FAILURE;
    }

    /* If the print is not from "hw-console" return failure *
     * as SDK print to be displayed in default context*/
    if(STRSTR(pCliContext->Mmi_i1Cur_prompt, "config-hw") == NULL)
    {
        return CLI_FAILURE;
    }

    CliHandle = pCliContext->i4ConnIdx;
    if ((CliHandle >= CLI_MAX_SESSIONS) || (CliHandle < 0))
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    if (pCliContext->gu4CliMode == CLI_MODE_APP)
    {
        return (CLI_FAILURE);
    }

    if (pCliContext->i1Status != CLI_ACTIVE)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    if (!(pCliContext->mmi_echo_flag) || (pCliContext->mmi_exit_flag == TRUE))
    {
        return (CLI_FAILURE);
    }
    if ((pCliContext->i1PipeFlag == TRUE)
            && (pCliContext->i1MoreQuitFlag == TRUE))
    {
        return (CLI_FAILURE);
    }

    if(pCliContext->pu1OutputMessage == NULL)
    {
        return (CLI_FAILURE);
    }

    pCliContext->pu1OutputMessage[0] = '\0';

    u4Len = VSNPRINTF ((CHR1 *) pCliContext->pu1OutputMessage,
            MAX_CLI_OUTPUT_BUF_SIZE, fmt, VarArgList);

    u4Len = (u4Len > MAX_CLI_OUTPUT_BUF_SIZE) ? MAX_CLI_OUTPUT_BUF_SIZE : u4Len;

    if (u4Len > 0)
    {
        if (u4Len
                <= (MAX_CLI_OUTPUT_PEND_BUF_SIZE - pCliContext->u4PendBuffLen))
        {
            /*
             * There is enough space in the pend buffer to store
             * the output string.
             */
            CLI_MEMCPY (&pCliContext->pu1PendBuff[pCliContext->u4PendBuffLen],
                    pCliContext->pu1OutputMessage, u4Len);

            pCliContext->u4PendBuffLen = pCliContext->u4PendBuffLen + u4Len;

            i4RetStatus = CLI_SUCCESS;
        }
        else
        {
            i4RetStatus = CliFlushOutputBuff (pCliContext);

            if (i4RetStatus == CLI_SUCCESS)
            {
                /*
                 * The message in the buffer is flushed, so there will
                 * be enough space to hold the current message.
                 */
                CLI_MEMCPY (&pCliContext->pu1PendBuff
                        [pCliContext->u4PendBuffLen],
                        pCliContext->pu1OutputMessage, u4Len);
                pCliContext->u4PendBuffLen = pCliContext->u4PendBuffLen + u4Len;
            }
        }
    }
    return (i4RetStatus);
}

/***********************************************************************************
 * FUNCTION NAME : CliNpPrintf
 * DESCRIPTION   : This function is used to support directing a output to telnet/ssh
 *
 * INPUT         :  fmt  - format of the output message
 * VarArgList-variable list of arguments
 *
 * OUTPUT        : NULL
 *************************************************************************************/
INT4 CliNpPrintf(const char *fmt, va_list varg)
{
    INT4 i4RetVal = -1;
    if (CliPrintfShellBuffers  (fmt, varg) == CLI_SUCCESS) {
        i4RetVal = 0;
    }
    return i4RetVal;
}

/***********************************************************************************
 * FUNCTION NAME : ISSCliCheckAndThrowFatalError
 * DESCRIPTION   : Checks, Whether some CLI Error is already set to the context.
 *                 if NOT, throws FATAL Error.
 *
 * INPUT         :  CliHandle - Handle of the Cli context
 *
 * OUTPUT        : NONE
 *************************************************************************************/
VOID ISSCliCheckAndThrowFatalError (tCliHandle CliHandle)
{
    UINT4 u4ErrCode=0;

    if((CLI_GET_ERR (&u4ErrCode) != CLI_SUCCESS))
    {
        CLI_FATAL_ERROR (CliHandle);
    }
}

/***********************************************************************************
 * FUNCTION NAME : CliGetBangStatus 
 * DESCRIPTION   : Used to fetch the BangStatus from the CliHandle
 *                
 *     
 * INPUT         :  CliHandle - Handle of the Cli context
 * 
 * RETURNS       : VOID 
 *                  
 **************************************************************************************/

VOID
CliGetBangStatus (tCliHandle CliHandle,
                  UINT1 *pu1BangStatus)
{
    tCliContext        *pCliContext = NULL;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    *pu1BangStatus = pCliContext->u1BangStatus;
}


/***********************************************************************************
 * FUNCTION NAME : CliSetBangStatus 
 * DESCRIPTION   : Used to set the BangStatus from the CliHandle
 *                
 *     
 * INPUT         :  CliHandle - Handle of the Cli context
 * 
 * RETURNS       : VOID 
 *                  
 **************************************************************************************/
VOID
CliSetBangStatus (tCliHandle CliHandle,
                  UINT1 u1BangStatus)
{
    tCliContext        *pCliContext = NULL;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    pCliContext->u1BangStatus =  u1BangStatus;
}

