/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clirm.c,v 1.34 2014/02/13 12:22:44 siva Exp $
 *
 * Description: This file contains the functions to support Redundancy 
 *              for CLI module.
 *******************************************************************/

#include "lr.h"
#include "cli.h"
#include "cliport.h"
#include "cliconst.h"
#include "clicmds.h"
#ifdef RADIUS_WANTED
#include "utilipvx.h"
#include "radius.h"
#endif
#include "cliproto.h"

#define CLI_RM_TASK_NAME   ((UINT1 *) "CLRM")

#define CLI_RM_GO_ACTIVE_EVENT                0x01
#define CLI_RM_GO_STANDBY_EVENT               0x02
#define CLI_RM_CONFIG_RESTORE_COMPLETE_EVENT  0x04
#define CLI_RM_STANDBY_UP_EVENT               0x08
#define CLI_RM_STANDBY_DOWN_EVENT             0x10

#define CLI_RED_RM_MSG  1

#define CLI_RED_Q_NAME           ((const UINT1 *)"CRDQ")
#define CLI_RED_Q_DEPTH          100

extern tCliSessions gCliSessions;

extern INT4         CliRcvPktFromRm (UINT1 u1Evt, tRmMsg * pRmMsg,
                                     UINT2 u2DataLen);
static INT4         CliRmRegister (void);
static void         CliCrtCtxt (void);
static INT4         CliRmIoctlEmpty (struct CliContext *pCtxt, INT4 i4val,
                                     VOID *p);
static INT4         CliRmOutputEmpty (struct CliContext *pCtxt, const CHR1 * p,
                                      UINT4 u4val);

extern INT4         gCliRmRole;
INT4                gi4CliPrevRmRole = RM_INIT;

tOsixTaskId         gCliRmTaskId;
tOsixQId            gCliRmQId;
tMemPoolId          gCliRedRmMsgPoolId;
UINT1               gu1CliNoOfPeers;

tCliContext        *gpCliCtxt;
VOID                CliRedHandlePktEnqEvent (VOID);

/* Callbk fn. invoked by RMGR, upon receipt of a RM pkt */
INT4
CliRcvPktFromRm (UINT1 u1Evt, tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tCliRedQMsg        *pMesg = NULL;
    UINT4               u4Events = 0;

    if (((u1Evt == RM_MESSAGE) || (u1Evt == RM_STANDBY_UP) ||
         (u1Evt == RM_STANDBY_DOWN)) && (pRmMsg == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to CLI RED task. */

        return RM_FAILURE;
    }
    else if (u1Evt == RM_MESSAGE)
    {
        /* RM message is not valid for CLI */
        RM_FREE (pRmMsg);
        return RM_FAILURE;
    }

    if (u1Evt == GO_ACTIVE)
    {
        u4Events = CLI_RM_GO_ACTIVE_EVENT;
    }
    else if (u1Evt == GO_STANDBY)
    {
        u4Events = CLI_RM_GO_STANDBY_EVENT;
    }
    else if (u1Evt == RM_CONFIG_RESTORE_COMPLETE)
    {
        u4Events = CLI_RM_CONFIG_RESTORE_COMPLETE_EVENT;
    }
    else if ((u1Evt == RM_STANDBY_UP) || (u1Evt == RM_STANDBY_DOWN))
    {
        pMesg = (tCliRedQMsg *) (MemAllocMemBlk (gCliRedRmMsgPoolId));

        if (pMesg == NULL)
        {
            UtlTrcPrint ("CLIRM Memory allocation failed\n");
            RmReleaseMemoryForMsg ((UINT1 *) pRmMsg);
            return RM_FAILURE;
        }

        MEMSET (pMesg, 0, sizeof (tCliRedQMsg));
        pMesg->u4MsgType = CLI_RED_RM_MSG;
        pMesg->RmData.pFrame = pRmMsg;
        pMesg->RmData.u2Length = u2DataLen;

        if (OsixQueSend (gCliRmQId, (UINT1 *) &pMesg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (gCliRedRmMsgPoolId, (UINT1 *) pMesg);
            RmReleaseMemoryForMsg ((UINT1 *) pRmMsg);
            UtlTrcPrint ("CLIRM send Q failed\n");
            return RM_FAILURE;
        }
        if (u1Evt == RM_STANDBY_UP)
        {
            u4Events = CLI_RM_STANDBY_UP_EVENT;
        }
        else
        {
            u4Events = CLI_RM_STANDBY_DOWN_EVENT;
        }
    }

    if (u4Events != 0)
    {
        if (OsixEvtSend (gCliRmTaskId, u4Events) == OSIX_FAILURE)
        {
            UtlTrcPrint ("CLIRM: Send event failed in CliRcvPktFromRm\n");
            return RM_FAILURE;
        }
    }
    return RM_SUCCESS;
}

/* CLI RM task's main loop.   */
/* Waits for messages from RM */
/* and executes the CLI cmd   */
void
CliRmMain (INT1 *pArg)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);

    OsixTskIdSelf (&gCliRmTaskId);
    /* Create queue to recieve msgs from RM */
    if (OsixCreateQ (CLI_RED_Q_NAME, CLI_RED_Q_DEPTH, 0,
                     &gCliRmQId) != OSIX_SUCCESS)
    {
        UtlTrcPrint ("CLIRM CLI RED Q creation failed\n");
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gCliRedRmMsgPoolId = CLIMemPoolIds[MAX_CLI_RED_Q_DEPTH_SIZING_ID];

    ProtoEvt.u4AppId = RM_CLI_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Register our appln with RM */
    if (CliRmRegister () == OSIX_FAILURE)
    {

        UtlTrcPrint ("CLIRM CliRmRegister failed\n");
        /* Indicate the status of initialization to main routine */
        OsixQueDel (gCliRmQId);
        MemDeleteMemPool (gCliRedRmMsgPoolId);
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    CLI_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gCliRmTaskId,
                         (CLI_RM_GO_ACTIVE_EVENT |
                          CLI_RM_GO_STANDBY_EVENT | CLI_RM_STANDBY_UP_EVENT |
                          CLI_RM_STANDBY_DOWN_EVENT |
                          CLI_RM_CONFIG_RESTORE_COMPLETE_EVENT),
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & CLI_RM_GO_ACTIVE_EVENT)
            {
                if (gCliRmRole == GO_STANDBY)
                {
                    CliInformRmSwitchover ();
                }
                gi4CliPrevRmRole = gCliRmRole;
                gCliRmRole = RM_ACTIVE;
                if (gi4CliPrevRmRole == RM_STANDBY)
                {
                    ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
                }
                else
                {
                    ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
                }

                RmApiHandleProtocolEvent (&ProtoEvt);
            }
            if (u4Events & CLI_RM_GO_STANDBY_EVENT)
            {
                if (gCliRmRole == GO_ACTIVE)
                {
                    CliInformRmSwitchover ();
                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                    RmApiHandleProtocolEvent (&ProtoEvt);

                    /* Moving from Active to Standby. Clear all
                     * telnet and SSH connections. */
                    CliDestroyConnections (CLI_TELNET_SSH_CLEAR, OSIX_FALSE);
                }
                gi4CliPrevRmRole = gCliRmRole;
                gCliRmRole = RM_STANDBY;
            }
            if ((u4Events & CLI_RM_STANDBY_UP_EVENT) ||
                (u4Events & CLI_RM_STANDBY_DOWN_EVENT))
            {
                CliRedHandlePktEnqEvent ();
            }

            if (u4Events & CLI_RM_CONFIG_RESTORE_COMPLETE_EVENT)
            {
                /* "users" file is now synced up with Standby. Update CLI 
                 * data structure with users in "users" file */
                CliReadUsers ();

                /* "privil" file is now synced up with Standby. Update CLI 
                 * data structure with privilages in "privil" file */
                CliReadPrivileges ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
            }
        }
    }                            /*End of While */
}

/* Handles packet received by CLI RED task */
VOID
CliRedHandlePktEnqEvent (VOID)
{
    tCliRedQMsg        *pMesg = NULL;
    tRmNodeInfo        *pData = NULL;

    /* Event received, dequeue messages */
    while (OsixQueRecv
           (gCliRmQId, (UINT1 *) &pMesg, OSIX_DEF_MSG_LEN,
            OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMesg->u4MsgType)
        {
            case CLI_RED_RM_MSG:
                pData = (tRmNodeInfo *) pMesg->RmData.pFrame;
                gu1CliNoOfPeers = pData->u1NumStandby;
                RmReleaseMemoryForMsg ((UINT1 *) pData);
                break;

            default:
                UtlTrcPrint ("CLIRM Invalid Message received\n");
                break;
        }
        /* Now release the buffer to pool */
        MemReleaseMemBlock (gCliRedRmMsgPoolId, (UINT1 *) pMesg);
    }
}

/* Register with RM,
 * create a CLI context to be used
 * in standby module
 */
static INT4
CliRmRegister (void)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_CLI_APP_ID;
    RmRegParams.pFnRcvPkt = CliRcvPktFromRm;

    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliCrtCtxt ();

    /* Set the RM CLI ctxt to top level */
    gpCliCtxt->pMmiCur_mode = gCliSessions.p_mmi_root_mode->pChild;
    gpCliCtxt->i1PrivilegeLevel = 15;
    return OSIX_SUCCESS;
}

/* Function to change CLI RM role INIT
 * This will be used whenever RM is in shut and start in progress case.*/
void
CliRmRoleToInit (void)
{
    gCliRmRole = RM_INIT;
}

/* Function to create a CLI context */
void
CliCrtCtxt (void)
{
    tCliContext        *pCliContext = NULL;
    CONST UINT1        *pu1Tmp = NULL;
    UINT4               u4Len = 0;

    if ((pCliContext = CliGetFreeContext ()) == NULL)
    {
        return;
    }

    pu1Tmp = OsixExGetTaskName (OsixGetCurTaskId ());
    u4Len = ((STRLEN (pu1Tmp) < sizeof (pCliContext->au1TskName)) ?
             STRLEN (pu1Tmp) : sizeof (pCliContext->au1TskName) - 1);
    STRNCPY (pCliContext->au1TskName, pu1Tmp, u4Len);
    pCliContext->au1TskName[u4Len] = '\0';
    pCliContext->TaskId = OsixGetCurTaskId ();
    pCliContext->i1UserAuth = FALSE;

    if (CliContextMemInit (pCliContext) == CLI_FAILURE)
    {
        return;
    }
    CliContextInit (pCliContext);

    gpCliCtxt = pCliContext;

    pCliContext->fpCliOutput = CliRmOutputEmpty;
    pCliContext->i1DefaultMoreFlag = CLI_MORE_DISABLE;
    pCliContext->fpCliIOInit = NULL;
    pCliContext->fpCliInput = NULL;
    pCliContext->fpCliIoctl = CliRmIoctlEmpty;
    pCliContext->fpCliFileWrite = NULL;

    return;
}
static INT4
CliRmIoctlEmpty (struct CliContext *pCtxt, INT4 i4val, VOID *p)
{
    UNUSED_PARAM (pCtxt);
    UNUSED_PARAM (i4val);
    UNUSED_PARAM (p);
    return CLI_SUCCESS;
}

/* CliRmEmpty function to sink any stdout on stanby node  */
/* This is set as the output function in CLI context */
static INT4
CliRmOutputEmpty (struct CliContext *pCtxt, const CHR1 * p, UINT4 u4val)
{
    UNUSED_PARAM (pCtxt);
    UNUSED_PARAM (u4val);
    UNUSED_PARAM (p);
    return 0;
}

/***************************************************************************  
 * FUNCTION NAME : CliInformRmSwitchover
 * DESCRIPTION   : This function is called from RM whenever there is a
 *                 switchover from standby to active to logout "chassisuser"
 *                 
 * INPUT         : NONE.
 * 
 * OUTPUT        : NONE
****************************************************************************/
VOID
CliInformRmSwitchover ()
{
    INT4                i4Index = 0;
    tCliContext        *pCliContext = NULL;

    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i4Index]);
        if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            if (pCliContext->i1UserAuth == TRUE)
            {
                pCliContext->mmi_exit_flag = TRUE;
            }
            pCliContext->i1UserAuth = FALSE;
            /*Clear console buffer before logging out */
            MEMSET (pCliContext->pu1PendBuff, 0, pCliContext->u4PendBuffLen);
            pCliContext->u4PendBuffLen = CLI_ZERO;
            pCliContext->pu1PendBuff[0] = '\0';
            break;
        }
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliBlockConsole
 * DESCRIPTION   : Blocks the CLI console until restoration complete is received
 *                 
 * INPUT         : NONE.
 * 
 * OUTPUT        : NONE
****************************************************************************/
VOID
CliBlockConsole ()
{
    INT4                i4Index = 0;
    tCliContext        *pCliContext = NULL;

    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i4Index]);
        if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            if (pCliContext->i1UserAuth == TRUE)
            {
                pCliContext->mmi_exit_flag = TRUE;
            }
            pCliContext->i1UserAuth = FALSE;
            break;
        }
    }
}

/* TODO: CliSyncCfmCmd need to check if this removal creates any issue.*/
