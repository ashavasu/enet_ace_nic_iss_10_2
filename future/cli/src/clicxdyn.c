/******************************************************************************
 * File Name : CliCxdyn.c
 * 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clicxdyn.c,v 1.19 2016/10/03 10:34:41 siva Exp $   
 *
 * Description: This will have the dynamic context sensitive help related
 *             routines
 ******************************************************************************/

#ifndef _CLI_CX_DYN_C__
#define _CLI_CX_DYN_C__

#include "clicmds.h"
#include "clicxdyn.h"

UINT1               gau1DynamicHelp[MAX_DYN_PARAMS][MAX_DYN_IFTYPE_HLP];
UINT2               gu1DynHlpDsc = 0;

/******************************************************************************
* FUNCTION NAME : CliCxDynGetDynamicHelp
* DESCRIPTION   : This function is used to display dynamic context sensitive 
* help from CXT_HLP in def file.
* INPUT         : Pointer to help parameters structure and pointer to the
*                 entered dynamic token
* RETURNS       : NONE
******************************************************************************/

VOID
CliCxDynGetDynamicHelp (tCxtHelpParams * pCxtHelpParams, UINT1 *pu1TempHelpStr1)
{
    INT4                i4Count = 0;

    for (; i4Count < gi4MaxRegEntries; i4Count++)
    {
        if (STRCMP (pu1TempHelpStr1, gDynHelpRegTable[i4Count].pcToken) == 0)
        {
            gu1DynHlpDsc = 0;
            gDynHelpRegTable[i4Count].CliCxDynAddDynHelpStr (pCxtHelpParams);
            break;
        }
    }
    return;
}

/******************************************************************************
* FUNCTION NAME : CliCxDynInterfaceType
* DESCRIPTION   : This function is used to display dynamic context sensitive 
*                 for interface type.
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynInterfaceType (tCxtHelpParams * pCxtHelpParams)
{
    INT4                i4OuterLoopIndex;
    INT4                i4InnerLoopIndex;
    INT4                i4RetVal = 0;
    INT1                i1Check = 0;
    INT1                i1RepetitionCheck = 0;
    INT1                i1HelpDesc = 0;
    INT1                i1TempHelpDesc = 0;
    UINT1               au1IfName[MAX_DYN_IFTYPE_SIZE];
    INT1                ai1PrevIfName[MAX_DYN_IFTYPE_SIZE];
    UINT1               u1EtherType;
    UINT1               u1IfType = 0;
    INT4                i4BridgePort = 0;
    UINT1               u1ModeType = 0;
    UINT1              *pu1TempString = NULL;
    UINT4               u4TempIf = CFA_NONE;
    UINT1               u1MatchFound = OSIX_TRUE;
    UINT1               au1TempHelpStr1[MAX_DYN_IFTYPE_SIZE];
    UINT1               au1TempHelpStr2[MAX_DYN_IFTYPES][MAX_DYN_IFTYPE_SIZE];
#ifndef CFA_UNIQUE_INTF_NAME
    UINT1               au1IfTypeHelp[MAX_DYN_IFTYPES][MAX_DYN_IFTYPE_HLP] =
        { "Gigabitethernet Gigabit ethernet interface",
        "Fastethernet Fast Ethernet Interface",
        "Extreme-Ethernet Extreme ethernet interface",
        "port-channel Port channel interface",
        "ppp PPP interface",
        "s-channel S-Channel Interface"
    };
#else
    UINT1               au1IfTypeHelp[MAX_DYN_IFTYPES][MAX_DYN_IFTYPE_HLP] =
        { "Ethernet Ethernet interface",
        "port-channel Port channel interface",
        "ppp PPP interface",
        "s-channel S-Channel Interface"
    };
#endif

    /* get string entered by user */
    pu1TempString = (UINT1 *) pCxtHelpParams->ppu1TknList +
        (CLI_MAX_TOKEN_VAL_MEM * (pCxtHelpParams->i4EnteredTok - 1));
    u1ModeType = (UINT1) VcmGetSystemModeExt (CFA_PROTOCOL_ID);

    MEMSET (au1TempHelpStr1, 0, MAX_DYN_IFTYPE_SIZE);
    MEMSET (au1IfName, 0, MAX_DYN_IFTYPE_SIZE);
    STRCPY (ai1PrevIfName, "NULL");
     for (i4OuterLoopIndex = 1;
         ((i4OuterLoopIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS) ||
         ((i4OuterLoopIndex <= CFA_MAX_EVB_SBP_INDEX ) &&
           (i4OuterLoopIndex >=  CFA_MIN_EVB_SBP_INDEX)));
          i4OuterLoopIndex++)
 
    {

        if (i1HelpDesc >= MAX_DYN_IFTYPES)
        {
            break;
        }

            if (CfaValidateIfIndex ((UINT4) i4OuterLoopIndex) != CFA_SUCCESS)
            {
                 if (i4OuterLoopIndex ==  BRG_MAX_PHY_PLUS_LOG_PORTS)
                {
                    i4OuterLoopIndex = CFA_MIN_EVB_SBP_INDEX - 1;
                }
                continue;
            } 
            i4RetVal = CfaGetHLIfFromLLIf ((UINT4) i4OuterLoopIndex, &u4TempIf, &u1IfType);
            if (u1IfType == CFA_PPP)
            {
                STRCPY (au1IfName, "ppp");
            }

            CfaGetIfType ((UINT4) i4OuterLoopIndex, &u1IfType);
            i1RepetitionCheck = 0;
            if (u1IfType == CFA_ENET)
            {
                CfaGetEthernetType ((UINT4)i4OuterLoopIndex, &u1EtherType);
                if (u1EtherType == CFA_GI_ENET)
                {
#ifndef CFA_UNIQUE_INTF_NAME
                    STRCPY (au1IfName, "Gigabitethernet");
#else
                    STRCPY (au1IfName, "Ethernet");
#endif
                }
                else if (u1EtherType == CFA_FA_ENET)
                {
                    STRCPY (au1IfName, "Fastethernet");
                }
                else if (u1EtherType == CFA_XE_ENET)
                {
                    STRCPY (au1IfName, "Extreme-Ethernet");
                }
            }
            else if (u1IfType == CFA_LAGG)
            {
                STRCPY (au1IfName, "port-channel");
            }
             else if (u1IfType == CFA_BRIDGED_INTERFACE)
            {
                
                CfaGetInterfaceBrgPortType ((UINT4) i4OuterLoopIndex, &i4BridgePort);

                if (i4BridgePort == CFA_STATION_FACING_BRIDGE_PORT)
                {
            
                    STRCPY (au1IfName, "s-channel");
                }
                else
                {
                    STRCPY (au1IfName, "Unknown");
                }
            }


            else
            {
                STRCPY (au1IfName, "Unknown");
            }

            if (pCxtHelpParams->i1SpaceFlag == 0)
            {
                /* when ? is given just after string (ex abcd?) in that case we 
                 * match user input string with exiting tokens and its successful 
                 * then only token info will be added to context help*/
                u1MatchFound =
                    CliCxUtlMatchIncompleteToken (pu1TempString, au1IfName);
                if (u1MatchFound == OSIX_FALSE)
                {
                    if (i4OuterLoopIndex ==  BRG_MAX_PHY_PLUS_LOG_PORTS)
                {
                    i4OuterLoopIndex = CFA_MIN_EVB_SBP_INDEX - 1;
                }

                    continue;
                }
            }
        for (i4InnerLoopIndex = 0; i4InnerLoopIndex < MAX_DYN_IFTYPES;
             i4InnerLoopIndex++)
        {
            CliCxUtlGetFirstWord (au1IfTypeHelp[i4InnerLoopIndex],
                                  au1TempHelpStr1);
            
                if (pCxtHelpParams->i1SpaceFlag == 0)
                {
                    /* when ? is given just after string (ex abcd?) in that case we 
                     * match user input string with exiting tokens and its successful 
                     * then only token info will be added to context help*/
                    u1MatchFound =
                        CliCxUtlMatchIncompleteToken (pu1TempString,
                                                      au1TempHelpStr1);
                    if (u1MatchFound == OSIX_FALSE)
                    {
                        continue;
                    }
                
                STRCPY (au1IfName, au1TempHelpStr1);
              }
            if (STRCMP (au1IfName, ai1PrevIfName) != 0)
            {
                if (STRCMP (au1IfName, au1TempHelpStr1) == 0)
                {
                    i1TempHelpDesc = 0;
                    while ((i1HelpDesc != 0) && (i1TempHelpDesc < i1HelpDesc))
                    {
                        if (STRCMP (au1TempHelpStr2[i1TempHelpDesc], au1IfName)
                            == 0)
                        {
                            i1RepetitionCheck = 1;
                            break;
                        }
                        ++i1TempHelpDesc;
                    }
                    if (i1RepetitionCheck == 1)
                    {
                        break;
                    }
                    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
                    {
                        return;
                    }
                    STRCPY (au1TempHelpStr2[i1HelpDesc], au1IfName);
                    STRCPY (gau1DynamicHelp[gu1DynHlpDsc],
                            au1IfTypeHelp[i4InnerLoopIndex]);
                    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->
                                                i2HelpStrIndex] =
                        gau1DynamicHelp[gu1DynHlpDsc];
                    pCxtHelpParams->i2HelpStrIndex =
                        pCxtHelpParams->i2HelpStrIndex + 1;
                    ++gu1DynHlpDsc;
                    ++i1Check;
                    ++i1HelpDesc;
                    STRCPY (ai1PrevIfName, au1IfName);
                    break;
                }
            }
            else
            {
                break;
            }
        }
       if (i4OuterLoopIndex ==  BRG_MAX_PHY_PLUS_LOG_PORTS)
        {
            i4OuterLoopIndex = CFA_MIN_EVB_SBP_INDEX - 1;
        }

    }
    if (u1ModeType == VCM_MI_MODE)
    {
        UNUSED_PARAM (u1IfType);
        UNUSED_PARAM (u1EtherType);
    }
    UNUSED_PARAM (i4RetVal);
}

/******************************************************************************
* FUNCTION NAME : CliCxDynIfNum
* DESCRIPTION   : This function is used to display dynamic context sensitive 
*                 for interface number.
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynIfNum (tCxtHelpParams * pCxtHelpParams)
{
    UINT1               au1IfNum[MAX_IFNUM_SIZE];
    UINT1               au1IfNumHlp[] = " Slot Number/Port Number";
    UINT1              *pu1TempString = NULL;
    UINT1               u1MatchFound = OSIX_FALSE;
    UINT1               au1IfName[] = "port-channel";
    UINT1               au1IfPPPName[] = "ppp";
    UINT1               au1IfSBPName[] = "s-channel";
    UINT1               u1PPPMatchFound = OSIX_FALSE;
    UINT1               u1SBPMatchFound = OSIX_FALSE;

    MEMSET (au1IfNum, 0, MAX_IFNUM_SIZE);

    pu1TempString = (UINT1 *) pCxtHelpParams->ppu1TknList +
        (CLI_MAX_TOKEN_VAL_MEM * (pCxtHelpParams->i4EnteredTok - 1));

    u1PPPMatchFound =
        CliCxUtlMatchIncompleteToken (pu1TempString, au1IfPPPName);
    u1MatchFound = CliCxUtlMatchIncompleteToken (pu1TempString, au1IfName);
    u1SBPMatchFound = CliCxUtlMatchIncompleteToken (pu1TempString, au1IfSBPName); 
    if (u1PPPMatchFound == OSIX_TRUE)
    {
        SPRINTF ((CHR1 *) au1IfNum, "<1-128> PPP Interface id");
    }

    else if (u1MatchFound == OSIX_TRUE)
    {
        SPRINTF ((CHR1 *) au1IfNum, "<1-65535> Port Channel ID");
    }
     else if (u1SBPMatchFound == OSIX_TRUE)
   {
       SPRINTF ((CHR1 *) au1IfNum, "<1-65535> S-Channel ID");
   }

    else
    {
#ifdef MBSM_WANTED
        {
            SPRINTF ((CHR1 *) au1IfNum, "<0-%d>/<1-%d>",
                     (MBSM_MAX_LC_SLOTS - 1), MBSM_MAX_POSSIBLE_PORTS_PER_SLOT);
            STRCAT (au1IfNum, au1IfNumHlp);
        }

#else
        {
            SPRINTF ((CHR1 *) au1IfNum, "<0>/<1-%d>",
                     SYS_DEF_MAX_PHYSICAL_INTERFACES);
            STRCAT (au1IfNum, au1IfNumHlp);
        }
#endif
    }
    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1IfNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;

}

/******************************************************************************
* FUNCTION NAME : CliCxDynIfXNum
* DESCRIPTION   : This function is used to display dynamic context sensitive 
*                 for interface number.
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynIfXNum (tCxtHelpParams * pCxtHelpParams)
{
    UINT1               au1IfNum[MAX_IFNUM_SIZE];
    UINT1               au1IfNumHlp[] = " Slot Number/Port Number.Logical Port Number";
    UINT1              *pu1TempString = NULL;
    UINT1               u1MatchFound = OSIX_FALSE;
    UINT1               au1IfName[] = "port-channel";
    UINT1               au1IfPPPName[] = "ppp";
    UINT1               au1IfSBPName[] = "s-channel";
    UINT1               u1PPPMatchFound = OSIX_FALSE;
    UINT1               u1SBPMatchFound = OSIX_FALSE;

    MEMSET (au1IfNum, 0, MAX_IFNUM_SIZE);

    pu1TempString = (UINT1 *) pCxtHelpParams->ppu1TknList +
        (CLI_MAX_TOKEN_VAL_MEM * (pCxtHelpParams->i4EnteredTok - 1));

    u1PPPMatchFound =
        CliCxUtlMatchIncompleteToken (pu1TempString, au1IfPPPName);
    u1MatchFound = CliCxUtlMatchIncompleteToken (pu1TempString, au1IfName);
    u1SBPMatchFound = CliCxUtlMatchIncompleteToken (pu1TempString, au1IfSBPName); 
    if (u1PPPMatchFound == OSIX_TRUE)
    {
        SPRINTF ((CHR1 *) au1IfNum, "<1-128> PPP Interface id");
    }

    else if (u1MatchFound == OSIX_TRUE)
    {
        SPRINTF ((CHR1 *) au1IfNum, "<1-65535> Port Channel ID");
    }
     else if (u1SBPMatchFound == OSIX_TRUE)
   {
       SPRINTF ((CHR1 *) au1IfNum, "<1-65535> S-Channel ID");
   }

    else
    {
#ifdef MBSM_WANTED
        {
            SPRINTF ((CHR1 *) au1IfNum, "<0-%d>/<1-%d>",
                     (MBSM_MAX_LC_SLOTS - 1), MBSM_MAX_POSSIBLE_PORTS_PER_SLOT);
            STRCAT (au1IfNum, au1IfNumHlp);
        }

#else
        {
            SPRINTF ((CHR1 *) au1IfNum, "<0>/<1-%d>.<1-%d>",
                     SYS_DEF_MAX_PHYSICAL_INTERFACES,SYS_DEF_MAX_L3SUB_IFACES);
            STRCAT (au1IfNum, au1IfNumHlp);
        }
#endif
    }
    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1IfNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;

}



/******************************************************************************
* FUNCTION NAME : CliCxDynVrfName
* DESCRIPTION   : This function is used to display dynamic context sensitive 
*                 for vrf name.
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynVrfName (tCxtHelpParams * pCxtHelpParams)
{
    UINT1               au1VrfName[] = " Name of the VRF instance";

#ifdef VCM_WANTED
    {
        UINT4               u4ContextId = 0;
        UINT4               u4NextContextId = 0;
        UINT4               u4TempCxtId = 0;
        UINT1               u1AliasName[MAX_VRF_HLP];
        MEMSET (u1AliasName, 0, MAX_VRF_HLP);
        if (VcmGetFirstActiveL3Context (&u4ContextId) == VCM_SUCCESS)
        {
            if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
            {
                return;
            }
            if (VcmGetAliasName (u4ContextId, u1AliasName) != VCM_FAILURE)
            {
                STRCAT (u1AliasName, au1VrfName);
                STRCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName);
                pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
                    gau1DynamicHelp[gu1DynHlpDsc];
                ++gu1DynHlpDsc;
                pCxtHelpParams->i2HelpStrIndex =
                    pCxtHelpParams->i2HelpStrIndex + 1;
            }

            while (VcmGetNextActiveL3Context (u4ContextId, &u4NextContextId) !=
                   VCM_FAILURE)
            {
                u4TempCxtId = u4NextContextId;
                if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
                {
                    return;
                }
                if (VcmGetAliasName (u4NextContextId, u1AliasName) !=
                    VCM_FAILURE)
                {
                    u4ContextId = u4TempCxtId;
                    STRCAT (u1AliasName, au1VrfName);
                    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName);
                    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->
                                                i2HelpStrIndex] =
                        gau1DynamicHelp[gu1DynHlpDsc];
                    ++gu1DynHlpDsc;
                    pCxtHelpParams->i2HelpStrIndex =
                        pCxtHelpParams->i2HelpStrIndex + 1;
                }
            }
        }
    }
#else
    {
        STRCPY (u1AliasName, "<string(32)> Name of the VRF instance");
        STRCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName);
        pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
            gau1DynamicHelp[gu1DynHlpDsc];
        ++gu1DynHlpDsc;
        pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
    }
#endif
}

/******************************************************************************
* FUNCTION NAME : CliCxDynSwitchName
* DESCRIPTION   : This function is used to display dynamic context sensitive 
*                 for switch name.
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynSwitchName (tCxtHelpParams * pCxtHelpParams)
{
    UINT1               au1SwitchName[] = " Switch Name/Context Name";

#ifdef VCM_WANTED
    {
        UINT4               u4ContextId = 0;
        UINT4               u4NextContextId = 0;
        UINT4               u4TempCxtId = 0;
        UINT1               u1AliasName[MAX_SWITCH_HLP];
        UINT1              *pu1UsrEnteredStr = NULL;
        UINT1               u1MatchFound = OSIX_TRUE;
        UINT1               u1StrMatchCnt = 0;

        pu1UsrEnteredStr = (UINT1 *) pCxtHelpParams->ppu1TknList +
            (CLI_MAX_TOKEN_VAL_MEM * (pCxtHelpParams->i4EnteredTok - 1));

        MEMSET (u1AliasName, 0, MAX_SWITCH_HLP);
        if (VcmGetFirstActiveContext (&u4ContextId) == VCM_SUCCESS)
        {
            if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
            {
                return;
            }
            if (VcmGetAliasName (u4ContextId, u1AliasName) != VCM_FAILURE)
            {
                if (pCxtHelpParams->i1SpaceFlag == 0)
                {
                    u1MatchFound =
                        CliCxUtlMatchIncompleteToken (pu1UsrEnteredStr,
                                                      u1AliasName);
                }
                if (u1MatchFound == OSIX_TRUE)
                {
                    u1StrMatchCnt++;
                    STRCAT (u1AliasName, au1SwitchName);
                    STRNCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName,
                             MAX_DYN_IFTYPE_HLP - 1);
                    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->
                                                i2HelpStrIndex] =
                        gau1DynamicHelp[gu1DynHlpDsc];
                    ++gu1DynHlpDsc;
                    pCxtHelpParams->i2HelpStrIndex =
                        pCxtHelpParams->i2HelpStrIndex + 1;
                }
            }
            while (VcmGetNextActiveL2Context (u4ContextId, &u4NextContextId)
                   != VCM_FAILURE)
            {
                u4TempCxtId = u4NextContextId;
                if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
                {
                    return;
                }
                if (VcmGetAliasName (u4NextContextId, u1AliasName) !=
                    VCM_FAILURE)
                {
                    u4ContextId = u4TempCxtId;
                    if (pCxtHelpParams->i1SpaceFlag == 0)
                    {
                        u1MatchFound =
                            CliCxUtlMatchIncompleteToken (pu1UsrEnteredStr,
                                                          u1AliasName);
                    }
                    if (u1MatchFound == OSIX_TRUE)
                    {
                        u1StrMatchCnt++;
                        STRCAT (u1AliasName, au1SwitchName);
                        STRNCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName,
                                 MAX_DYN_IFTYPE_HLP - 1);
                        pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->
                                                    i2HelpStrIndex] =
                            gau1DynamicHelp[gu1DynHlpDsc];
                        ++gu1DynHlpDsc;
                        pCxtHelpParams->i2HelpStrIndex =
                            pCxtHelpParams->i2HelpStrIndex + 1;
                    }
                }
            }
        }
        if ((pCxtHelpParams->i1SpaceFlag == 0) && (u1StrMatchCnt == 0))
        {
            MEMSET (u1AliasName, 0, MAX_SWITCH_HLP);
            STRCPY (u1AliasName, "<string(32)> Switch Name/Context Name");
            STRNCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName,
                     MAX_DYN_IFTYPE_HLP - 1);
            pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
                gau1DynamicHelp[gu1DynHlpDsc];
            ++gu1DynHlpDsc;
            pCxtHelpParams->i2HelpStrIndex++;
        }
    }
#else
    {
        STRCPY (u1AliasName, "<string(32)> Switch Name/Context Name");
        STRNCPY (gau1DynamicHelp[gu1DynHlpDsc], u1AliasName,
                 MAX_DYN_IFTYPE_HLP - 1);
        pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
            gau1DynamicHelp[gu1DynHlpDsc];
        ++gu1DynHlpDsc;
        pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
    }
#endif
}

/******************************************************************************
* FUNCTION NAME : CliCxDYNPwIdRange
* DESCRIPTION   : This function is used to display dynamic context sensitive
*                 help for Pseudo-wire Index Range
* INPUT         : Pointer to help parameters structure
* RETURNS       : NONE
******************************************************************************/
VOID
CliCxDynPwIdRange (tCxtHelpParams * pCxtHelpParams)
{
#ifdef MPLS_WANTED
    UINT1               au1PwIdRangeNum[MAX_PWID_SIZE];
    UINT1               au1PwIdRangeHlp[] = " PWIndex Value";
    MEMSET (au1PwIdRangeNum, 0, MAX_PWID_SIZE);

    SPRINTF ((CHR1 *) au1PwIdRangeNum, "<%d-%d>",
             1, gSystemSize.MplsSystemSize.u4MplsMaxPwId);
    STRCAT (au1PwIdRangeNum, au1PwIdRangeHlp);

    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1PwIdRangeNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
#else
    UNUSED_PARAM (pCxtHelpParams);
#endif
    return;
}

/******************************************************************************
 * * FUNCTION NAME : CliCxDynLdpLblRange
 * * DESCRIPTION   : This function is used to display dynamic context sensitive
 * *                 help for LDP Label Range
 * * INPUT         : Pointer to help parameters structure
 * * RETURNS       : NONE
 * ******************************************************************************/
VOID
CliCxDynLdpLblRange (tCxtHelpParams * pCxtHelpParams)
{
#ifdef MPLS_WANTED
    UINT1               au1LblRangeNum[MAX_LBLRANGE_SIZE];
    UINT1               au1LblRangeHlp[] = " Label Value";
    MEMSET (au1LblRangeNum, 0, MAX_LBLRANGE_SIZE);

    SPRINTF ((CHR1 *) au1LblRangeNum, "<%d-%d>",
             gSystemSize.MplsSystemSize.u4MinLdpLblRange,
             gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange);
    STRCAT (au1LblRangeNum, au1LblRangeHlp);

    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1LblRangeNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
#else
    UNUSED_PARAM (pCxtHelpParams);
#endif
    return;
}

/******************************************************************************
 * * FUNCTION NAME : CliCxDynRsvpTeLblRange
 * * DESCRIPTION   : This function is used to display dynamic context sensitive
 * *                 help for RSVP-TE Label Range
 * * INPUT         : Pointer to help parameters structure
 * * RETURNS       : NONE
 * ******************************************************************************/
VOID
CliCxDynRsvpTeLblRange (tCxtHelpParams * pCxtHelpParams)
{
#ifdef MPLS_WANTED
    UINT1               au1LblRangeNum[MAX_LBLRANGE_SIZE];
    UINT1               au1LblRangeHlp[] = " Label Value";
    MEMSET (au1LblRangeNum, 0, MAX_LBLRANGE_SIZE);

    SPRINTF ((CHR1 *) au1LblRangeNum, "<%d-%d>",
             gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange,
             gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange);
    STRCAT (au1LblRangeNum, au1LblRangeHlp);

    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1LblRangeNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
#else
    UNUSED_PARAM (pCxtHelpParams);
#endif
    return;
}

/******************************************************************************
 * * FUNCTION NAME : CliCxDynStaticLblRange
 * * DESCRIPTION   : This function is used to display dynamic context sensitive
 * *                 help for Static Label Range
 * * INPUT         : Pointer to help parameters structure
 * * RETURNS       : NONE
 * ******************************************************************************/
VOID
CliCxDynStaticLblRange (tCxtHelpParams * pCxtHelpParams)
{
#ifdef MPLS_WANTED
    UINT1               au1LblRangeNum[MAX_LBLRANGE_SIZE];
    UINT1               au1LblRangeHlp[] = " Label Value";
    MEMSET (au1LblRangeNum, 0, MAX_LBLRANGE_SIZE);

    SPRINTF ((CHR1 *) au1LblRangeNum, "<%d-%d>",
             gSystemSize.MplsSystemSize.u4MinStaticLblRange,
             gSystemSize.MplsSystemSize.u4MaxStaticLblRange);
    STRCAT (au1LblRangeNum, au1LblRangeHlp);

    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1LblRangeNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
#else
    UNUSED_PARAM (pCxtHelpParams);
#endif
    return;
}
#endif

/******************************************************************************
 * * FUNCTION NAME : CliCxDynPathOptNum
 * * DESCRIPTION   : This function is used to display dynamic context sensitive
 * *                 help for Path-option Number Range
 * * INPUT         : Pointer to help parameters structure
 * * RETURNS       : NONE
 * ******************************************************************************/
VOID
CliCxDynPathOptNum (tCxtHelpParams * pCxtHelpParams)
{
#ifdef MPLS_WANTED
    UINT1               au1PathOptRangeNum[MAX_TE_TUNNEL_INFO];
    UINT1               au1PathOptRangeHlp[] = " Enter Path-option number";
    MEMSET (au1PathOptRangeNum, 0, MAX_TE_TUNNEL_INFO);

    SPRINTF ((CHR1 *) au1PathOptRangeNum, "<%d-%d>",
             1, MAX_TE_TUNNEL_INFO);
    STRCAT (au1PathOptRangeNum, au1PathOptRangeHlp);

    if (gu1DynHlpDsc >= MAX_DYN_PARAMS)
    {
        return;
    }
    STRCPY (gau1DynamicHelp[gu1DynHlpDsc], au1PathOptRangeNum);
    pCxtHelpParams->ppu1HelpStr[pCxtHelpParams->i2HelpStrIndex] =
        gau1DynamicHelp[gu1DynHlpDsc];
    ++gu1DynHlpDsc;
    pCxtHelpParams->i2HelpStrIndex = pCxtHelpParams->i2HelpStrIndex + 1;
#else
    UNUSED_PARAM (pCxtHelpParams);
#endif
    return;
}

