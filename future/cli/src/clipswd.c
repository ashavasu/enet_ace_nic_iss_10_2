/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clipswd.c,v 1.102 2017/12/20 11:06:50 siva Exp $
 *
 * Description: This will have routines related to login validation 
 *              and user information 
 *
 *******************************************************************/
#include "clicmds.h"
#include <time.h>                                                           /** AUDIT **/

/* Array of Context Structures */
extern tCliSessions gCliSessions;
extern tMemPoolId   gCliUserGroupMemPoolId;
extern tMemPoolId   gTacMsgInputMemPoolId;
extern tMemPoolId   gAuditInfoMemPoolId;
extern INT1         ai1BSpaceStr[];
#ifdef RM_WANTED
extern INT4         gCliRmRole;
#include "rmgr.h"
#endif
VOID                save_cur_user_config (tCliContext *, INT2);
#ifdef FIREWALL_WANTED
#define FWL_TCP                   6
#endif

VOID
mmi_list_users (VOID)
{
    INT1               *pi1Buf = NULL;

    CLI_ALLOC_USERGROUP_MEM_BLOCK (pi1Buf, INT1);

    /* Print the banner */
    mmi_printf
        ("\n\rUSER                    MODE                  PRIVILEGE  \n\r");
    FpamUtlListUsers ();
    CLI_RELEASE_USERGROUP_MEM_BLOCK (pi1Buf);
    return;
}

VOID
CliListActiveUsers ()
{
    tCliContext        *pCliContext = NULL;
    INT2                i2Index = 0;
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;

    mmi_printf ("\r\n Line\t\tUser\t\tPeer-Address\r\n");

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i2Index]);

        if ((pCliContext->i1Status == CLI_ACTIVE) &&
            (CLI_STRLEN (pCliContext->ai1UserName) != 0))
        {
            if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
            {
                mmi_printf ("%d con\t\t%s\t\tLocal Peer", i2Index,
                            pCliContext->ai1UserName);
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
            {
                mmi_printf ("%d tel\t\t%s", i2Index, pCliContext->ai1UserName);
                /* Check if peer is a v4 client */
                if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
                {
                    InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
                    mmi_printf ("\t\t%s", UtlInetNtoa (InAddr));
                }
                else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
                {
                    CLI_MEMCPY (In6Addr.u1addr, pCliContext->au4ClientIpAddr,
                                sizeof (pCliContext->au4ClientIpAddr));
                    mmi_printf ("\t\t%s", UtlInetNtoa6 (In6Addr));
                }
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                mmi_printf ("%d ssh\t\t%s", i2Index, pCliContext->ai1UserName);
                /* Check if peer is a v4 client */
                if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
                {
                    InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
                    mmi_printf ("\t\t%s", UtlInetNtoa (InAddr));
                }
                else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
                {
                    CLI_MEMCPY (In6Addr.u1addr, pCliContext->au4ClientIpAddr,
                                sizeof (pCliContext->au4ClientIpAddr));
                    mmi_printf ("\t\t%s", UtlInetNtoa6 (In6Addr));
                }
            }
            mmi_printf ("\r\n");
        }
    }
}

CHR1               *
mmi_encript_passwd (CHR1 * i1passwd)
{
    UINT4               len, i;
    len = CLI_STRLEN (i1passwd);
    for (i = 0; i < len; i++)
    {
        if (i != len - 1)
        {
            i1passwd[i] ^= i1passwd[i + 1];
        }
        else
        {
            i1passwd[i] ^= MMI_SPEC_CHAR;
        }
        if (i1passwd[i] == '\n')
        {
            i1passwd[i] = MMI_SPEC_CHAR;
        }
        if (i1passwd[i] == '\0')
        {
            i1passwd[i] = MMI_SPEC_CHAR + 1;
        }
    }
    i1passwd[i] = '\0';
    return i1passwd;
}

VOID
mmi_current_user (VOID)
{
    tCliContext        *pcurrContext = NULL;

    if ((pcurrContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    mmi_printf ("\rCurrent User is  %s \r\n", pcurrContext->ai1UserName);
    return;
}

VOID
mmi_print_user_prompt (pi1Prompt, pCliContext)
     INT1               *pi1Prompt;
     tCliContext        *pCliContext;
{
    INT1                Buffer[100];

    if (pi1Prompt == NULL)
    {
        Buffer[0] = '\0';

        mmi_print_prompt (pCliContext, Buffer, sizeof (Buffer));

        mmi_printf ("\r%s ", Buffer);
    }
    else
    {
        mmi_printf ("\r%s", pi1Prompt);
    }

    return;
}

INT4
mmi_change_user_passwd (CHR1 * pi1UserName)
{
    CHR1                temp_passwd[MAX_PASSWD_LEN];
    CHR1                new_passwd[MAX_PASSWD_LEN];
    tCliContext        *pCliContext = NULL;
    INT1                i1RootFlag = FALSE;
    INT4                i4Retval;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    if (!pi1UserName)
        pi1UserName = (CHR1 *) pCliContext->ai1UserName;

    if (STRCMP (pi1UserName, pCliContext->ai1UserName))
    {
        if (CliCheckSpecialPriv () != CLI_SUCCESS)
        {
            mmi_printf
                ("\r\n%% Should be %s or %s privileged user\r\n", CLI_ROOT_USER,
                 CLI_ROOT_USER);
            return CLI_FAILURE;

        }
        i1RootFlag = TRUE;
    }

    memset (temp_passwd, 0, MAX_PASSWD_LEN);
    memset (new_passwd, 0, MAX_PASSWD_LEN);

    /* Check if the user is a new user. If it is a root user then do not 
     * ask for the old password. Otherwise request for the old password.
     */

    if (!i1RootFlag)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_OLD_PASSWD]);
        if ((i4Retval = mmi_getpasswd (temp_passwd, TRUE)) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if ((i4Retval = CliCheckUserPasswd ((INT1 *) pi1UserName,
                                            (INT1 *) temp_passwd)) ==
            CLI_FAILURE)
        {
            mmi_printf (mmi_gen_messages[MMI_GEN_ERR_OLD_PASSWD_NOT_MATCH]);
            return (CLI_FAILURE);
        }
    }
    mmi_printf (mmi_gen_messages[MMI_GEN_NEW_PASSWD]);
    if ((i4Retval = mmi_getpasswd (temp_passwd, TRUE)) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (CLI_STRCMP (temp_passwd, "") == 0)
    {
        mmi_printf ("\rNULL password not allowed \r\n");
        return (CLI_FAILURE);
    }

    mmi_printf (mmi_gen_messages[MMI_GEN_REENTER_PASSWD]);
    if ((i4Retval = mmi_getpasswd (new_passwd, TRUE)) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (CLI_STRCMP (temp_passwd, new_passwd) != 0)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_CHANGE_PASSWD_FAIL]);
        return (CLI_FAILURE);
    }
    mmi_printf ("\rPassword updated Successfully \n");

    CliChangePasswd (pi1UserName, new_passwd);
    return (CLI_SUCCESS);
}

VOID
CliRemovePrivilege (INT4 i4PrivilegeLevel)
{
    INT1                i1PrivilegeIndex;
    INT1                i1Privilege;
    tCliContext        *pCliContext = NULL;

    i1Privilege = (INT1) i4PrivilegeLevel;

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }
    if ((STRCMP (pCliContext->ai1UserName, CLI_ROOT_USER) != 0)
        && (i1Privilege >= CLI_ATOI (CLI_ROOT_PRIVILEGE_ID)))
    {
        mmi_printf ("\r\nPermission denied !! \r\n");
        return;
    }

    if ((pCliContext->i1PrivilegeLevel < i1Privilege))

    {
        mmi_printf ("\r%% Cannot disable privilege level\r\n");
        return;
    }
    if ((CliCheckPrivilegeIndex (i1Privilege, &i1PrivilegeIndex) ==
         CLI_SUCCESS))
    {
        if (CliPrivilegeDel (i1PrivilegeIndex) == CLI_FAILURE)
        {
            mmi_printf ("\r\n Not able to disable privilege level\r\n");
        }
    }
}

VOID
CliConfigPassword (INT4 i4PrivilegeLevel, UINT1 u1EncryptionType,
                   INT1 *pi1PswdStr)
{
    tCliContext        *pCliContext = NULL;
    INT1                ai1TempPswd[MAX_PASSWD_LEN + 1];
    INT1                i1Privilege;

    i1Privilege = (INT1) i4PrivilegeLevel;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        return;
    }

    if ((STRCMP (pCliContext->ai1UserName, CLI_ROOT_USER) != 0)
        && (i1Privilege >= CLI_ATOI (CLI_ROOT_PRIVILEGE_ID)))
    {
        mmi_printf ("\r\n%%Permission denied !! Need to be %s user\r\n",
                    CLI_ROOT_USER);
        return;
    }

    if (CLI_STRLEN (pi1PswdStr) >= MAX_PASSWD_LEN)
    {
        mmi_printf ("\r\n Long Password string length,"
                    "password string stripped of to length %d\r\n",
                    MAX_PASSWD_LEN);
        pi1PswdStr[MAX_PASSWD_LEN] = '\0';
    }

    if (CliChkStrictPasswdConfRules (pi1PswdStr) == CLI_FAILURE)
    {
        mmi_printf ("\r\n Password should follow "
                    "Password Configuration rules (should contain "
                    "atleast one uppercase, one lowercase, one "
                    "number and one special character) \r\n");
        return;
    }

    STRCPY (ai1TempPswd, pi1PswdStr);

    /* Encrypted password is not currently supported
     * Only unencrypted passwords are supported */
    if (u1EncryptionType == CLI_UNENCRYPTED_PSWD)
    {
        mmi_encript_passwd ((CHR1 *) ai1TempPswd);
    }

    if (CliPrivilegeAdd (i1Privilege, ai1TempPswd) == CLI_FAILURE)
    {
        mmi_printf ("\r\n Not able to enable privilege level\r\n");
    }
}

INT4
mmi_getpasswd (CHR1 * passwd, INT1 flag)
{
    INT2                i = 0;
    tCliContext        *pCliContext;
    INT1                ai1Prompt[MAX_PROMPT_LEN];
    UINT4               u4Len = 0;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    };

    MEMSET (passwd, '\0', MAX_PASSWD_LEN);

    for (i = 0; i <= MAX_PASSWD_LEN; i++)
    {
        if ((passwd[i] = (INT1) CliGetInput (pCliContext)) == '\n')
        {
            passwd[i] = '\0';
            break;
        }

        if (IS_CONTROL_CHAR (passwd[i]))
        {
            passwd[i--] = '\0';
            continue;
        }
        if (TRUE == flag)
        {
            pCliContext->passwdMode = TRUE;
            u4Len =
                ((STRLEN (mmi_gen_messages[MMI_GEN_PASSWD]) <
                  sizeof (ai1Prompt)) ?
                 STRLEN (mmi_gen_messages[MMI_GEN_PASSWD]) : sizeof (ai1Prompt)
                 - 1);
            STRNCPY (ai1Prompt, mmi_gen_messages[MMI_GEN_PASSWD], u4Len);
            ai1Prompt[u4Len] = '\0';
        }
        else
        {
            STRCPY (ai1Prompt, pCliContext->Mmi_i1Cur_prompt);
        }
        if (IS_BACKSPACE_CHAR (passwd[i]))
        {
            if (i > 0)
            {
                passwd[--i] = '\0';
                i--;
            }
            else if (i == 0)
            {
                passwd[i] = '\0';
                i--;
            }

            if (flag != TRUE)
            {
                /* if flag is FALSE, then print the output */

                mmi_printf ("\r%s", ai1Prompt);
                mmi_printf ("%s", passwd);
                mmi_printf ("%s", ai1BSpaceStr);
                mmi_printf ("\r%s", ai1Prompt);
                mmi_printf ("%s", passwd);
            }

            continue;
        }
        if (IS_NON_PRINTABLE_CHAR (passwd[i]))
        {
            if (pCliContext->mmi_exit_flag == TRUE)
            {
                /* This condition occurs when mmi_exit function
                 * is called. If the console mode is telnet, this
                 * flag will be set if the telnet session is closed
                 * or the idle timer expires
                 */
                return CLI_FAILURE;
            }
            continue;
        }
        if ((passwd[i] == CLI_FAILURE) || (passwd[i] == 0))
        {
            return (CLI_FAILURE);
        }

        if (flag != TRUE)
        {
            if (pCliContext->fpCliOutput (pCliContext, &passwd[i], 1) ==
                CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }
    /* Echoing the '\n' character */
    if (i <= MAX_PASSWD_LEN)
    {
        if (pCliContext->fpCliOutput (pCliContext, "\r\n", 2) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (i > MAX_PASSWD_LEN)
    {
        mmi_printf ("\r\n\n%s",
                    mmi_gen_messages[MMI_GEN_ERR_USER_PASSWD_EXCEEDED]);
        MEMSET (passwd, '\0', MAX_PASSWD_LEN);
    }
    if (TRUE == flag)
    {
        if (pCliContext->fpCliOutput (pCliContext, "\r\n", 2) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
        /*
           mmi_encript_passwd (passwd);
         */
        pCliContext->passwdMode = FALSE;
    }
    return (CLI_SUCCESS);
}

/* Locks the cli console if the user tries to 
 * guess the password */
VOID
cli_lock_console (VOID)
{
    tCliContext        *pcurrContext = NULL;
    INT4                i4LineIndex = 0;
    UINT4               u4Len = 0;
    INT2                i = 0;
    INT1                i1str[MAX_PASSWD_LEN];
    INT1               *pi1TempPasswd = NULL;
    CHR1                au1LockMsg[] = "CLI console locked";
    CHR1                au1UnLockMsg[] =
        "\rEnter Password to unlock the console: ";

    MEMSET (i1str, 0, MAX_PASSWD_LEN);
    if ((pcurrContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    pi1TempPasswd = CLI_BUDDY_ALLOC (MAX_PASSWD_LEN, INT1);

    if (pi1TempPasswd == NULL)
    {
        return;
    }

    u4Len = CLI_STRLEN (au1LockMsg);
    pcurrContext->fpCliOutput (pcurrContext, au1LockMsg, u4Len);

    if (!(pcurrContext->u4LineCount))
    {
        i4LineIndex = 0;
        SPRINTF ((CHR1 *) & pcurrContext->
                 ppu1OutBuf[i4LineIndex][u4Len], "\r\n");
        pcurrContext->u4LineCount++;
        pcurrContext->u4LineWriteCount++;
    }

    /* user should not see any characters that are typed
     *  so disable echo 
     */

    u4Len = CLI_STRLEN (au1UnLockMsg);

    pcurrContext->fpCliOutput (pcurrContext, au1UnLockMsg, u4Len);
    MGMT_UNLOCK ();
    while (TRUE)
    {
        for (i = 0; i < MAX_PASSWD_LEN; i++)
        {
            if ((i1str[i] = (INT1) CliGetInput (pcurrContext)) == '\n')
            {
                i1str[i] = '\0';
                break;
            }
        }

        if (pcurrContext->mmi_exit_flag)
        {
            break;
        }

        if (i >= MAX_PASSWD_LEN)
        {
            i1str[i - 1] = '\0';
        }

        /* CliCheckUserPasswd changes the Password hence store it in a
         * temporary variable
         */
        STRCPY (pi1TempPasswd, i1str);
        if (pcurrContext->mmi_exit_flag == TRUE)
        {
            break;
        }
        else if (CliAuthenticateUserPasswd
                 (pcurrContext->ai1UserName, i1str) == CLI_SUCCESS)
        {
            pcurrContext->i4AuthStatus = 1;
            break;
        }
        else if ((gu1IssLoginAuthMode == LOCAL_LOGIN) &&
                 (pcurrContext->mmi_exit_flag == FALSE) &&
                 (CliCheckUserPasswd ((INT1 *) CLI_ROOT_USER, pi1TempPasswd) ==
                  CLI_SUCCESS))
        {

            break;
        }
        else
        {
            pcurrContext->fpCliOutput (pcurrContext, au1UnLockMsg, u4Len);
        }
    }                            /* while TRUE */

    /* Either a valid user or password guessed correctly */
    MGMT_LOCK ();

    mmi_printf ("\n");
    CLI_BUDDY_FREE (pi1TempPasswd);
}

INT4
mmi_login (tCliContext * pCliContext, INT1 *pi1UserName)
{
    INT1                success_flag = 0;
    CHR1                i1username[MAX_USER_NAME_LEN + 1];
    CHR1                i1passwd[MAX_PASSWD_LEN + 1];
    INT1                i1Prompt[MAX_PROMPT_LEN];
    INT4                i4Retval = CLI_FAILURE;
#ifdef MBSM_WANTED
    INT2                i2PrivVal = 0;
#endif
    INT2                i2TempPrivId = 0;
    CHR1                au1CliBufMsg[CLI_MSG_BUF];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    UINT4               u4Len = 0;

    MEMSET (au1CliBufMsg, '\0', CLI_MSG_BUF);
    MEMSET (&InAddr, CLI_ZERO, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, CLI_ZERO, sizeof (tUtlIn6Addr));

    MEMSET (i1username, '\0', MAX_USER_NAME_LEN);
    MEMSET (i1passwd, '\0', MAX_PASSWD_LEN);
    MEMSET (i1Prompt, '\0', MAX_PROMPT_LEN);

    CliDeletePipe (pCliContext);

    do
    {
        success_flag = 0;
        while (!success_flag && !pCliContext->mmi_exit_flag)
        {
            if (!pi1UserName)
            {
                CliGetLoginPrompt (i1Prompt);
                STRCPY (pCliContext->Mmi_i1Cur_prompt, i1Prompt);
                mmi_printf ("%s", i1Prompt);

                if ((i4Retval = mmi_getpasswd (i1username, FALSE))
                    == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if (!CLI_STRLEN (i1username))
                {
                    mmi_printf ("\r\n\n%s",
                                mmi_gen_messages[MMI_GEN_ERR_PASSWD_NOT_MATCH]);

                    if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
                    {
                        continue;
                    }
                }
            }
            else
            {
                u4Len =
                    ((STRLEN (pi1UserName) <
                      sizeof (i1username)) ? STRLEN (pi1UserName) :
                     sizeof (i1username) - 1);
                STRNCPY (i1username, pi1UserName, u4Len);
                i1username[u4Len] = '\0';
            }

            i4Retval = (INT1) mmi_search_user ((INT1 *) i1username);
            if (i4Retval == CLI_SUCCESS)
            {
                if ((CliUtilCheckAllowUserLogin (i1username)) == CLI_FAILURE)
                {
                    continue;
                }
            }
            mmi_printf ("%s", mmi_gen_messages[MMI_GEN_PASSWD]);
            if (mmi_getpasswd (i1passwd, TRUE) == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
#ifdef MBSM_WANTED
            if ((MbsmIsDefaultInit () == MBSM_SUCCESS)
#ifdef RM_WANTED
                && (gCliRmRole == GO_STANDBY)
#endif
                )
            {

                if (CliMbsmCheckUserPasswd ((INT1 *) i1username,
                                            (INT1 *) i1passwd) != CLI_SUCCESS)
                {
                    mmi_printf ("\r\n%s\r\n", "Login as chassisuser");
                    CliHandleAuthFailure (pCliContext, (INT1 *) i1username);
                    success_flag = 0;
                    continue;
                }
                STRCPY (pCliContext->ai1UserName, i1username);

                if (FpamGetPrivilege
                    ((CHR1 *) pCliContext->ai1UserName,
                     &i2PrivVal) == FPAM_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                pCliContext->i1PrivilegeLevel = (INT1) i2PrivVal;
                pCliContext->i1UserAuth = TRUE;
                return (CLI_SUCCESS);
            }
#endif /* MBSM_WANTED */

#ifdef ISS_WANTED
            /* Tacacs or radius authentication will be done
             * only when local or local fallback is enabled.
             * This is done to ensure the console is no way blocked because of external servers*/
            if ((pCliContext->gu4CliMode != CLI_MODE_CONSOLE) ||
                ((pCliContext->gu4CliMode == CLI_MODE_CONSOLE) &&
                 ((gu1IssLoginAuthMode == LOCAL_LOGIN) ||
                  (gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL)
                  || (gu1IssLoginAuthMode ==
                      REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL)
                  || (gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS)
                  || (gu1IssLoginAuthMode == PAM_LOGIN)
                  || (gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS))))
            {
                if (CliAuthenticateUserPasswd
                    ((INT1 *) i1username, (INT1 *) i1passwd) == CLI_FAILURE)
                {
                    continue;
                }
                else
                {

                    if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
                    {
                        if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
                        {
                            InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
                            SPRINTF (au1CliBufMsg, "%s", UtlInetNtoa (InAddr));
                        }
                        else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
                        {
                            CLI_MEMCPY (In6Addr.u1addr,
                                        pCliContext->au4ClientIpAddr,
                                        sizeof (tUtlIn6Addr));
                            SPRINTF (au1CliBufMsg, "%s",
                                     UtlInetNtoa6 (In6Addr));
                        }
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                                      pCliContext->gu4CliSysLogId,
                                      "Attempt to login as %s via telnet from %s Succeeded",
                                      i1username, au1CliBufMsg));
                    }
                    else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
                    {
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                                      pCliContext->gu4CliSysLogId,
                                      "Attempt to login as %s via console Succeeded",
                                      i1username));
                    }

                    pCliContext->u1Retries = 0;
                    if (CliHandlePasswordExpiry
                        (pCliContext, (INT1 *) i1username) == CLI_FAILURE)
                    {
                        continue;
                    }
                    success_flag = 1;
                    return (CLI_SUCCESS);
                }
            }
            else
#endif /* ISS_WANTED */
            if ((i4Retval != CLI_SUCCESS) ||
                    (CliCheckUserPasswd
                         ((INT1 *) i1username,
                              (INT1 *) i1passwd) != CLI_SUCCESS))
            {
                CliHandleAuthFailure (pCliContext, (INT1 *) i1username);
                continue;
            }

            STRCPY (pCliContext->ai1UserName, i1username);
            if (FpamGetPrivilege
                ((CHR1 *) pCliContext->ai1UserName,
                 &i2TempPrivId) == FPAM_FAILURE)
            {
                return (CLI_FAILURE);
            }
            pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
            pCliContext->u1Retries = 0;
            success_flag = 1;
            pCliContext->i1UserAuth = TRUE;
            return (CLI_SUCCESS);
        }
    }
    while (!pCliContext->mmi_exit_flag);

    return (CLI_SUCCESS);
}

INT2
CliInitUserMode (tCliContext * pCliContext)
{
    UINT1              *pu1UserMode = NULL;
    UINT1              *pu1UserModePtr = NULL;
    INT2                i2Retval = CLI_FAILURE;
    CHR1                au1CliMode[CLI_MODE_LEN] = { CLI_ZERO };
    CHR1                au1CliBufMsg[CLI_MSG_BUF];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;

    MEMSET (au1CliBufMsg, '\0', CLI_MSG_BUF);
    MEMSET (&InAddr, CLI_ZERO, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, CLI_ZERO, sizeof (tUtlIn6Addr));

    pu1UserMode = CLI_BUDDY_ALLOC (MAX_PROMPT_LEN + 1, UINT1);
    if (pu1UserMode == NULL)
    {
        return CLI_FAILURE;
    }
    pu1UserModePtr = pu1UserMode;

    STRCPY (pCliContext->Mmi_i1Cur_prompt, "/");
    CliSetDisplayPrompt (pCliContext);
    STRCPY (pCliContext->au1PromptStr, "\0");

    /* Used to log the source IP address of user logged in from SSH or TELNET */
    if ((pCliContext->gu4CliMode == CLI_MODE_TELNET) ||
        (pCliContext->gu4CliMode == CLI_MODE_SSH))
    {
        if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
        {
            InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
            SPRINTF (au1CliBufMsg, "from %s", UtlInetNtoa (InAddr));
        }
        else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
        {
            CLI_MEMCPY (In6Addr.u1addr, pCliContext->au4ClientIpAddr,
                        sizeof (tUtlIn6Addr));
            SPRINTF (au1CliBufMsg, "from %s", UtlInetNtoa6 (In6Addr));
        }
    }

    if ((CliGetUserMode ((CONST CHR1 *) pCliContext->ai1UserName,
                         (INT1 *) pu1UserMode)) == CLI_SUCCESS)
    {

        if (cli_change_path ((CONST CHR1 *) pu1UserMode) == CLI_SUCCESS)
        {
            if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
            {
                SPRINTF (au1CliMode, "telnet");
#ifdef FIREWALL_WANTED
                FwlLogMessage (FWL_TELNET_SUCCESS, 0, NULL,
                               FWL_LOG_MUST, FWLLOG_INFO_LEVEL, NULL);
#endif
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
            {
                SPRINTF (au1CliMode, "console");
#ifdef FIREWALL_WANTED
                FwlLogMessage (FWL_CLI_LOGIN_SUCCESS, 0, NULL,
                               FWL_LOG_MUST, FWLLOG_INFO_LEVEL, NULL);
#endif
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                SPRINTF (au1CliMode, "ssh");
            }
            CliLogEvent (MMI_LOGIN_SUCCESS);
            CliSendAuditLoginInfo (pCliContext, LOG_IN);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                          pCliContext->gu4CliSysLogId,
                          "User %s logged in via %s %s",
                          pCliContext->ai1UserName, au1CliMode, au1CliBufMsg));

            pCliContext->pmmi_user_mode = pCliContext->pMmiCur_mode;
            i2Retval = CLI_SUCCESS;
        }
        else if (cli_change_path ("/") == OSIX_SUCCESS)
        {
            if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
            {
                SPRINTF (au1CliMode, "telnet");
#ifdef FIREWALL_WANTED
                FwlLogMessage (FWL_TELNET_SUCCESS, 0, NULL,
                               FWL_LOG_MUST, FWLLOG_INFO_LEVEL, NULL);
#endif
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
            {
                SPRINTF (au1CliMode, "console");
#ifdef FIREWALL_WANTED
                FwlLogMessage (FWL_CLI_LOGIN_SUCCESS, 0, NULL,
                               FWL_LOG_MUST, FWLLOG_INFO_LEVEL, NULL);
#endif
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                SPRINTF (au1CliMode, "ssh");
            }
            CliLogEvent (MMI_LOGIN_SUCCESS);
            CliSendAuditLoginInfo (pCliContext, LOG_IN);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                          pCliContext->gu4CliSysLogId,
                          "User %s logged in via %s %s",
                          pCliContext->ai1UserName, au1CliMode, au1CliBufMsg));

            pCliContext->pmmi_user_mode = pCliContext->pMmiCur_mode;
            i2Retval = CLI_SUCCESS;
        }
    }
    else
    {
        if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
        {
            SPRINTF (au1CliMode, "telnet");
        }
        else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            SPRINTF (au1CliMode, "console");
        }
        else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
        {
            SPRINTF (au1CliMode, "ssh");
        }
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                      pCliContext->gu4CliSysLogId,
                      "User %s logged in via %s %s", pCliContext->ai1UserName,
                      au1CliMode, au1CliBufMsg));
    }

    CLI_BUDDY_FREE (pu1UserModePtr);
    return (i2Retval);
}

VOID
mmicmdlog_user_logout (pCliContext, u1LogOutTypeFlag)
     tCliContext        *pCliContext;
     UINT1               u1LogOutTypeFlag;
{
    CHR1                au1CliMode[CLI_MODE_LEN] = { CLI_ZERO };
    CHR1                au1CliBufMsg[CLI_MSG_BUF];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;

    MEMSET (au1CliBufMsg, '\0', CLI_MSG_BUF);
    MEMSET (&InAddr, CLI_ZERO, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, CLI_ZERO, sizeof (tUtlIn6Addr));

    /* Used to log the source IP address of user logged in from SSH or TELNET */
    if ((pCliContext->gu4CliMode == CLI_MODE_TELNET) ||
        (pCliContext->gu4CliMode == CLI_MODE_SSH))
    {
        if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
        {
            InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
            SPRINTF (au1CliBufMsg, "from %s is ", UtlInetNtoa (InAddr));
        }
        else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
        {
            CLI_MEMCPY (In6Addr.u1addr, pCliContext->au4ClientIpAddr,
                        sizeof (tUtlIn6Addr));
            SPRINTF (au1CliBufMsg, "from %s is ", UtlInetNtoa6 (In6Addr));
        }
    }

    if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
    {
        SPRINTF (au1CliMode, "telnet");
    }
    else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
    {
        SPRINTF (au1CliMode, "console");
    }
    else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
    {
        SPRINTF (au1CliMode, "ssh");
    }

    if (u1LogOutTypeFlag == LOG_OUT)
    {
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, pCliContext->gu4CliSysLogId,
                      "User %s %slogged out from %s", pCliContext->ai1UserName,
                      au1CliBufMsg, au1CliMode));
    }
    else if (u1LogOutTypeFlag == FORCED_LOG_OUT)
    {
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, pCliContext->gu4CliSysLogId,
                      "User %s %sforcefully logged out from %s",
                      pCliContext->ai1UserName, au1CliBufMsg, au1CliMode));

    }
}

INT4
mmi_search_user (INT1 *pi1UserName)
{
    tSNMP_OCTET_STRING_TYPE SearchFsusrMgmtUserName;

    MEMSET (&SearchFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    SearchFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    SearchFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);

    if (FpamUtlSearchUserName (&SearchFsusrMgmtUserName) == FPAM_SUCCESS)
    {
        /* User Already exist test */
        return CLI_SUCCESS;
    }
    return CLI_ERROR;

}

/*****************************************************************************/
/* Function Name      : CliHandlePasswordExpiry                              */
/*                                                                           */
/* Description        : This routine takes care of password expiry.          */
/*                                                                           */
/* Input(s)           : pCliContext - Pointre to Cli context                 */
/*                      pi1UserName     - Pointer to UserName                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT1
CliHandlePasswordExpiry (tCliContext * pCliContext, INT1 *pi1UserName)
{
    if (FpamPasswordExpiryCheck (pi1UserName) == FPAM_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, pCliContext->gu4CliSysLogId,
                  "Password expired for user %s", pi1UserName));

    mmi_printf ("\r%% Password has expired. Please change the password\r\n");
    if (CliHandleChangePassword (pCliContext, pi1UserName) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, pCliContext->gu4CliSysLogId,
                  "Password credentials updated successfully for User %s",
                  pi1UserName));

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliHandleChangePassword                              */
/*                                                                           */
/* Description        : This routine takes care of changing password         */
/*                                                                           */
/* Input(s)           : pCliContext - Pointre to Cli context                 */
/*                      pi1UserName     - Pointer to UserName                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT1
CliHandleChangePassword (tCliContext * pCliContext, INT1 *pi1UserName)
{
    UINT4               u4Retries = 0;
    CHR1                a1passwd[MAX_PASSWD_LEN];
    CHR1                a1retype_passwd[MAX_PASSWD_LEN];

    UNUSED_PARAM (pCliContext);
    memset (a1passwd, 0, MAX_PASSWD_LEN);
    memset (a1retype_passwd, 0, MAX_PASSWD_LEN);

    while (u4Retries <= (UINT4) MAX_PASSWORD_TRIES)
    {
        mmi_printf ("Enter old password: ");
        if (mmi_getpasswd (a1passwd, TRUE) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (CliAuthenticateUserPasswd
            (pi1UserName, (INT1 *) a1passwd) == CLI_FAILURE)
        {
            u4Retries++;
            continue;
        }
        u4Retries = 0;
        while (u4Retries <= (UINT4) MAX_PASSWORD_TRIES)
        {
            memset (a1passwd, 0, MAX_PASSWD_LEN);
            mmi_printf ("Enter new password: ");
            if (mmi_getpasswd (a1passwd, TRUE) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (CliValidateUserPasswd (pi1UserName, (INT1 *) a1passwd)
                == CLI_FAILURE)
            {
                u4Retries++;
                continue;
            }

            mmi_printf ("Re-enter new password: ");
            if (mmi_getpasswd (a1retype_passwd, TRUE) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (STRCMP (a1retype_passwd, a1passwd) == 0)
            {
                if (FpamChangePassword (pi1UserName, (INT1 *) a1passwd) ==
                    FPAM_FAILURE)
                {
                    u4Retries++;
                    continue;
                }
                return CLI_SUCCESS;
            }
            else
            {
                mmi_printf ("\r%% Password doesn't match. Please retry\r\n");
                u4Retries++;
                continue;
            }
        }
    }
    mmi_printf ("\r%% Maximum retries exceeded. Returning to prompt\r\n");
    return CLI_FAILURE;
}

/*****************************************************************************/
/* Function Name      : CliValidateUserPasswd                                */
/*                                                                           */
/* Description        : This routine validates the user password when        */
/*             user updates the password                     */
/*                                                                           */
/* Input(s)           : pCliContext - Pointre to Cli context                 */
/*                      pi1UserName     - Pointer to UserName                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT1
CliValidateUserPasswd (INT1 *pi1UserName, INT1 *pi1NewPasswd)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE UserNewPassword;

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserNewPassword, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    UserName.pu1_OctetList = (UINT1 *) pi1UserName;
    UserName.i4_Length = STRLEN (pi1UserName);
    UserNewPassword.pu1_OctetList = (UINT1 *) pi1NewPasswd;
    UserNewPassword.i4_Length = STRLEN (pi1NewPasswd);

    if (FpamUserPasswdConfCheck (&UserName, &UserNewPassword) == FPAM_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliHandleAuthFailure                                 */
/*                                                                           */
/* Description        : This routine keep track of no of login attempt and   */
/*                      stops after three attempt.                           */
/*                                                                           */
/* Input(s)           : pCliContext - Pointre to Cli context                 */
/*                      pi1Name     - Pointer to UserName                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
CliHandleAuthFailure (tCliContext * pCliContext, INT1 *pi1Name)
{
    CHR1                au1CliMode[CLI_MODE_LEN] = { CLI_ZERO };
    CHR1                au1CliBufMsg[CLI_MSG_BUF];
    UINT1               au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    UINT4               u4Len = 0;

    MEMSET (au1Ip6Addr, '\0', IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1CliBufMsg, '\0', CLI_MSG_BUF);
    MEMSET (&InAddr, CLI_ZERO, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, CLI_ZERO, sizeof (tUtlIn6Addr));
    /* Used to log the source IP address of user logged in from SSH or TELNET */
    if ((pCliContext->gu4CliMode == CLI_MODE_TELNET) ||
        (pCliContext->gu4CliMode == CLI_MODE_SSH))
    {
        if (pCliContext->u4IpAddrType == IPV4_ADD_TYPE)
        {
            InAddr.u4Addr = pCliContext->au4ClientIpAddr[3];
            SPRINTF (au1CliBufMsg, "from %s", UtlInetNtoa (InAddr));
        }
        else if (pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
        {
            CLI_MEMCPY (In6Addr.u1addr, pCliContext->au4ClientIpAddr,
                        sizeof (tUtlIn6Addr));
            MEMCPY (au1Ip6Addr, In6Addr.u1addr, sizeof (tUtlIn6Addr));
            SPRINTF (au1CliBufMsg, "from %s",
                     (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));

        }
    }

    /* Intruder Alert ... */
    /*Get the count of wrong login attempts */
    /*Compare the login attemts with blocked login attempts */
    if (++pCliContext->u1Retries >= MAX_PASSWORD_TRIES)
    {
#ifdef FIREWALL_WANTED
        FwlLogMessage (FWL_CLI_LOGIN_FAILED, 0, NULL,
                       FWL_LOG_MUST, FWLLOG_INFO_LEVEL, NULL);
#endif
        CliLogEvent (MMI_LOGIN_FAILURE);

        /* in case the user logout happens due to wrong password credentials,
         * the normal user logout syslog message should not be generated by
         * mmi_exit */
        pCliContext->u1CliLogOutMessage = CLI_USER_LOGIN_FAILED_MSG;

        if (pCliContext->gu4CliMode != CLI_MODE_CONSOLE)
        {
            if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                              pCliContext->gu4CliSysLogId,
                              "Attempt to login as %s via telnet failed %s",
                              pi1Name, au1CliBufMsg));
            }
            else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                              pCliContext->gu4CliSysLogId,
                              "Attempt to login as %s via ssh failed %s",
                              pi1Name, au1CliBufMsg));
            }
            mmi_exit ();
            pCliContext->u1CliLogOutMessage = CLI_IDLE_TIME_EXPIRY_MSG;
            pCliContext->mmi_exit_flag = TRUE;
            pCliContext->u1Retries = 0;
            /* * Blocks the user if condition is satisfies */
            if ((CliUtilCheckBlockUserOrNot ((CHR1 *) pi1Name)) == CLI_FAILURE)
            {
                /* Cli root user cannot be blocked. So skipping the
                 * warning message */
                if (STRCMP (pi1Name, CLI_ROOT_USER) != CLI_ZERO)
                {
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                                  pCliContext->gu4CliSysLogId,
                                  "Attempt to block the user %s failed",
                                  pi1Name));
                }
            }
            else
            {
                /*reset the Login attempts when user blocked */
                FpamSetLoginAttempts ((CHR1 *) pi1Name,
                                      FPAM_RESET_LOGIN_ATTEMPT);
                pCliContext->u1Retries = 0;
            }
            return;
        }
        else
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, pCliContext->gu4CliSysLogId,
                          "Attempt to login as %s via console failed",
                          pi1Name));

        }

        /* Only root user should be allowed to unlock */
        pCliContext->u1Retries = 0;
        if ((CliUtilCheckBlockUserOrNot ((CHR1 *) pi1Name)) == CLI_FAILURE)
        {
            /* Only root privileged user should be allowed to unlock */
            pCliContext->mmi_i1user_index = 0;
            /* set the user name as ROOT, because we block only for
             * root user. since pi1Name is local variable, context name also
             * should be updated
             */
            u4Len =
                ((STRLEN (CLI_ROOT_USER) <
                  sizeof (pCliContext->
                          ai1UserName)) ? STRLEN (CLI_ROOT_USER) :
                 sizeof (pCliContext->ai1UserName) - 1);
            STRNCPY (pCliContext->ai1UserName, CLI_ROOT_USER, u4Len);
            pCliContext->ai1UserName[u4Len] = '\0';

            cli_lock_console ();
        }
        else
        {
            /*reset the Login attempts when user blocked */
            FpamSetLoginAttempts ((CHR1 *) pi1Name, FPAM_RESET_LOGIN_ATTEMPT);
            pCliContext->u1Retries = 0;
        }
    }
    else
    {
        if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
        {
            SPRINTF (au1CliMode, "telnet");
        }
        else if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            SPRINTF (au1CliMode, "console");
        }
        else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
        {
            SPRINTF (au1CliMode, "ssh");
        }

        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL,
                      pCliContext->gu4CliSysLogId,
                      "Login failed : Login incorrect %s via %s %s", pi1Name,
                      au1CliMode, au1CliBufMsg));
        /* 
         * In SSH mode, connection will be lost if login is incorrect.
         * So, mmi_printf wont work 
         */
        if (pCliContext->gu4CliMode != CLI_MODE_SSH)
        {
            mmi_printf ("\n%s", mmi_gen_messages[MMI_GEN_ERR_PASSWD_NOT_MATCH]);

        }
    }
    return;
}

#ifdef  RADIUS_WANTED
/*****************************************************************************/
/* Function Name      : CliSetRadiusUserPriv                                 */
/*                                                                           */
/* Description        : This function is used to set priv level              */
/*                                                                           */
/* Input(s)           : pRadResp     - pointer to structure to receive Radius*/
/*                      status.                                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
CliSetRadiusUserPriv (tRadInterface * pRadResp)
{

    INT1                i1PrivLevel = CLI_DEFAULT_PRIVILEGE;

    /* The privilege level will be updated only if the service-type attribute
     * in the radius response packet is one of the following
     * Attribute        Value       Privilege 
     * =========        =====       =========   
     * Administrative   6           15
     * NAS Prompt       7           1
     * Otherwise default privilege will be given for the user*/

    if (pRadResp->Service_Type == SERT_ADMINISTRATIVE)
    {
        i1PrivLevel = (INT1) CLI_ATOI (CLI_ROOT_PRIVILEGE_ID);
    }
    else if (pRadResp->Service_Type == SERT_NAS_PROMPT)
    {
        i1PrivLevel = (INT1) CLI_DEFAULT_PRIVILEGE;
    }

    OsixTakeSem (0, gCliSessions.au1CliContextSemName, 0, 0);
    CliSetUserPriv (pRadResp->TaskId, i1PrivLevel);
    OsixGiveSem (0, gCliSessions.au1CliContextSemName);
    return;
}

/*****************************************************************************/
/* Function Name      : CliHandleRadiusResponse                              */
/*                                                                           */
/* Description        : Callback routine for radius authentication, This     */
/*                      routine handles the Radius server response           */
/*                                                                           */
/* Input(s)           : pRadRespRcvd - pointer to structure to receive Radius*/
/*                      status.                                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
CliHandleRadiusResponse (VOID *pRadRespRcvd)
{
    tRadInterface      *pRadResp = NULL;

    pRadResp = (tRadInterface *) pRadRespRcvd;

    switch (pRadResp->Access)
    {
        case ACCESS_ACCEPT:

            /* Set the Privelege level for the Radius user 
             * with the value given for service-type attribute 
             * in radius response packet*/
            CliSetRadiusUserPriv (pRadResp);
            OsixEvtSend (pRadResp->TaskId, CLI_ACCEPT_EVENT);
            break;
        case ACCESS_REJECT:
            OsixEvtSend (pRadResp->TaskId, CLI_REJECT_EVENT);
            break;
        case RAD_TIMEOUT:
            OsixEvtSend (pRadResp->TaskId, CLI_RADIUS_TIMEOUT_EVENT);
            break;
        default:
            OsixEvtSend (pRadResp->TaskId, CLI_REJECT_EVENT);
            break;
    }
    CliFreeRadiusMem ((tRadInterface *) pRadRespRcvd);
    return;
}

/*****************************************************************************/
/* Function Name      : CliFreeRadiusMem                                     */
/*                                                                           */
/* Description        : This routine frees the Radius interface data         */
/*                      structure memory.                                    */
/*                                                                           */
/* Input(s)           : pIface - pointer to Radius Client Interface data     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CliFreeRadiusMem (tRadInterface * pIface)
{
    UtlShMemFreeRadInterface ((UINT1 *) pIface);
}

/*****************************************************************************/
/* Function Name      : CliRadiusAuthenticate                                */
/*                                                                           */
/* Description        : This routine send UserName and Password to Radius    */
/*                      Server for authentication                            */
/*                                                                           */
/* Input(s)           : pi1Name    - pointer to UserName                     */
/*                      piPasswd   - pointer to Password                     */
/*                      pu!TskName - pointer to Task                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS - if authenticated                       */
/*                      CLI_FAILURE - if fail to authneticate                */
/*****************************************************************************/

INT1
CliRadiusAuthenticate (INT1 *pi1Name, INT1 *pi1Passwd, tOsixTaskId TaskId,
                       INT1 *pi1ServerStatus)
{
    tRADIUS_INPUT_AUTH  RadiusInputAuth;
    tUSER_INFO_PAP      UserInfoPap;
    INT4                i4Ret = 0;
    UINT4               u4Len = 0;

    if ((pi1Name == NULL) || (pi1Passwd == NULL) || (TaskId == 0))
    {
        return CLI_FAILURE;
    }

    MEMSET (&RadiusInputAuth, 0, sizeof (tRADIUS_INPUT_AUTH));
    RadiusInputAuth.p_UserInfoPAP = &UserInfoPap;

    /* building Radius Input Auth structure */
    RadiusInputAuth.u1_ProtocolType = PRO_PAP;
    RadiusInputAuth.TaskId = TaskId;

    /* building Info Pap structure which is within Radius Input Auth 
     * Structure */
    u4Len = ((STRLEN (pi1Name) < sizeof (UserInfoPap.a_u1UserName)) ?
             STRLEN (pi1Name) : sizeof (UserInfoPap.a_u1UserName) - 1);
    CLI_STRNCPY (UserInfoPap.a_u1UserName, pi1Name, u4Len);
    UserInfoPap.a_u1UserName[u4Len] = '\0';
    u4Len = ((STRLEN (pi1Passwd) < sizeof (UserInfoPap.a_u1UserPasswd)) ?
             STRLEN (pi1Passwd) : sizeof (UserInfoPap.a_u1UserPasswd) - 1);
    CLI_STRNCPY (UserInfoPap.a_u1UserPasswd, pi1Passwd, u4Len);
    UserInfoPap.a_u1UserPasswd[u4Len] = '\0';

    i4Ret = radiusAuthentication (&RadiusInputAuth, NULL,
                                  CliHandleRadiusResponse, 0, 0);

    if (i4Ret == NOT_OK)
    {
        *pi1ServerStatus = ISS_FALSE;
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif /* RADIUS_WANTED */
/*****************************************************************************/
/* Function Name      : CliSetUserPriv                                 */
/*                                                                           */
/* Description        : This function is used to set priv level for          */
/*                      tacacs/radius user                                          */
/*                                                                           */
/* Input(s)           : TaskId    - pointer to Task                          */
/*                      u1PrivilegeLevel - Privelege level                   */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CliSetUserPriv (tOsixTaskId TaskId, INT1 i1PrivilegeLevel)
{
    INT2                i2Index;

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].TaskId == TaskId)
        {
            gCliSessions.gaCliContext[i2Index].i1PrivilegeLevel =
                i1PrivilegeLevel;
            break;
        }
    }
    return;
}

#ifdef TACACS_WANTED
/*****************************************************************************/
/* Function Name      : CliTacacsAuthenticate                                */
/*                                                                           */
/* Description        : This routine send UserName and Password to Radius    */
/*                      Server for authentication                            */
/*                                                                           */
/* Input(s)           : pi1Name    - pointer to UserName                     */
/*                      piPasswd   - pointer to Password                     */
/*                      pu!TskName - pointer to Task                         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS - if authenticated                       */
/*                      CLI_FAILURE - if fail to authneticate                */
/*****************************************************************************/

INT1
CliTacacsAuthenticate (INT1 *pi1Name, INT1 *pi1Passwd, tOsixTaskId TaskId,
                       INT1 *pi1ServerStatus)
{
    tTacMsgInput       *pTacMsgInput = NULL;
    tTacAuthenInput    *pTacAuthenInput;
    INT4                i4Ret = 0;
    INT1                i1RetStatus = CLI_FAILURE;
    UINT1               au1Port[] = "login_auth";
    INT2                i2PrivLevel = 0;
    UINT4               u4Len = 0;

    if ((pi1Name == NULL) || (pi1Passwd == NULL) || (TaskId == 0))
    {
        return CLI_FAILURE;
    }

    pTacMsgInput = (tTacMsgInput *) MemAllocMemBlk (gTacMsgInputMemPoolId);
    if (pTacMsgInput == NULL)
    {
        return CLI_FAILURE;
    }
    i1RetStatus = CliGetUserPrivilege ((CONST CHR1 *) pi1Name, &i2PrivLevel);

    /* No user exists in Cli user databse
     * So assign default PrivilegeLevel 15*/

    if (i1RetStatus == CLI_FAILURE)
    {
        i2PrivLevel = (INT2) CLI_ATOI (CLI_ROOT_PRIVILEGE_ID);
    }

    CliSetUserPriv (TaskId, (INT1) i2PrivLevel);

    MEMSET (pTacMsgInput, 0, sizeof (tTacMsgInput));
    pTacMsgInput->u4MsgType = TAC_AUTHEN;
    pTacAuthenInput = &(pTacMsgInput->TacInput.AuthenInput);
    pTacAuthenInput->u1Action = TAC_AUTHEN_LOGIN;
    pTacAuthenInput->u1PrivilegeLevel = (UINT1) i2PrivLevel;
    pTacAuthenInput->u1Service = TAC_AUTHEN_SVC_LOGIN;
    pTacAuthenInput->u1AuthenType = TAC_AUTHEN_TYPE_PAP;
    STRCPY (pTacAuthenInput->au1Port, au1Port);
    u4Len = ((STRLEN (pi1Name) < sizeof (pTacAuthenInput->au1UserName)) ?
             STRLEN (pi1Name) : sizeof (pTacAuthenInput->au1UserName) - 1);
    STRNCPY (pTacAuthenInput->au1UserName, pi1Name, u4Len);
    pTacAuthenInput->au1UserName[u4Len] = '\0';
    u4Len =
        ((STRLEN (pi1Passwd) <
          sizeof (pTacAuthenInput->UserInfo.UserPAP.
                  au1Password)) ? STRLEN (pi1Passwd) : sizeof (pTacAuthenInput->
                                                               UserInfo.UserPAP.
                                                               au1Password) -
         1);
    STRNCPY (pTacAuthenInput->UserInfo.UserPAP.au1Password, pi1Passwd, u4Len);
    pTacAuthenInput->UserInfo.UserPAP.au1Password[u4Len] = '\0';
    pTacAuthenInput->AppInfo.TaskId = TaskId;

    pTacAuthenInput->u4AppReqId = 1;
    pTacAuthenInput->AppInfo.fpAppCallBackFn = CliHandleTacacsResponse;

    i4Ret = TacacsAuthenticateUser (pTacAuthenInput);

    if (i4Ret != TAC_CLNT_RES_OK)
    {
        *pi1ServerStatus = ISS_FALSE;
        MemReleaseMemBlock (gTacMsgInputMemPoolId, (UINT1 *) pTacMsgInput);
        return CLI_FAILURE;
    }
    MemReleaseMemBlock (gTacMsgInputMemPoolId, (UINT1 *) pTacMsgInput);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliHandleTacacsResponse                              */
/*                                                                           */
/* Description        : Callback routine for tacacs authentication, This     */
/*                      routine handles the tacacs server response           */
/*                                                                           */
/* Input(s)           : pRadRespRcvd - pointer to structure to receive Radius*/
/*                      status.                                              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
CliHandleTacacsResponse (VOID *pTacRespRcvd)
{
    tTacMsgOutput      *pTacResp = NULL;

    pTacResp = (tTacMsgOutput *) pTacRespRcvd;

    switch (pTacResp->TacOutput.AuthenOutput.i4AuthenStatus)
    {
        case TAC_AUTHEN_STATUS_PASS:
            OsixEvtSend (pTacResp->TaskId, CLI_ACCEPT_EVENT);
            break;
        case TAC_AUTHEN_STATUS_FAIL:
            OsixEvtSend (pTacResp->TaskId, CLI_REJECT_EVENT);
            break;
        case TAC_AUTHEN_STATUS_TIMEOUT:
            OsixEvtSend (pTacResp->TaskId, CLI_TACACS_TIMEOUT_EVENT);
            break;
        default:
            OsixEvtSend (pTacResp->TaskId, CLI_REJECT_EVENT);
            break;
    }
    CliFreeTacacsMem ((tTacMsgOutput *) pTacRespRcvd);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliFreeTacacsMem                                     */
/*                                                                           */
/* Description        : This routine frees the Tacacs interface data         */
/*                      structure memory.                                    */
/*                                                                           */
/* Input(s)           : pIface - pointer to Tacacs Client Interface data     */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CliFreeTacacsMem (tTacMsgOutput * pIface)
{
    UtlShMemFreeTacMsgOutput ((UINT1 *) pIface);
}

#endif
#ifdef MBSM_WANTED
INT1
CliMbsmCheckUserPasswd (INT1 *pi1Name, INT1 *pi1Passwd)
{
    if (!STRCMP (pi1Name, CLI_MBSM_CHASSIS_USER))
    {
        if (!STRCMP (pi1Passwd, CLI_MBSM_CHASSIS_PASSWD))
        {
            return (CLI_SUCCESS);
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    else
    {
        return (CLI_FAILURE);
    }
}
#endif /* MBSM_WANTED */
/*****************************************************************************/
/* Function Name      : CliSendAuditLoginInfo                                */
/*                                                                           */
/* Description        : Fills the Audit Log info and posts event to MSR      */
/*                                                                           */
/* Input(s)           : pCliContext,i4LogFlag                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
CliSendAuditLoginInfo (tCliContext * pCliContext, INT4 i4LogFlag)
{
    tAuditInfo         *pTempAudit = NULL;
    time_t              t;
    struct tm          *tp;
    CHR1               *pTime = NULL;
    UINT4               u4Len = 0;

    if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
    {
        pTempAudit = (tAuditInfo *) MemAllocMemBlk (gAuditInfoMemPoolId);
        if (pTempAudit == NULL)
        {
            return;
        }
        MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
        t = time (NULL);
        STRCPY (pTempAudit->TempCmd.ai1UserName, "Audit:");
        STRCAT (pTempAudit->TempCmd.ai1UserName, pCliContext->ai1UserName);
        if (i4LogFlag == LOG_IN)
        {
            u4Len =
                (UINT4) (STRLEN ("Logging in ...!") <
                         (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1) ?
                         STRLEN ("Logging in ...!")
                         : (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1));

            STRNCPY (pTempAudit->TempCmd.au1TempCmd, "Logging in ...!", u4Len);
            pTempAudit->TempCmd.au1TempCmd[u4Len] = '\0';
        }
        if (i4LogFlag == LOG_OUT)
        {
            u4Len =
                (UINT4) (STRLEN ("Logging out ...!") <
                         (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1) ?
                         STRLEN ("Logging out ...!")
                         : (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1));
            STRNCPY (pTempAudit->TempCmd.au1TempCmd, "Logging out ...!", u4Len);
            pTempAudit->TempCmd.au1TempCmd[u4Len] = '\0';
        }

        if (i4LogFlag == FORCED_LOG_OUT)
        {
            u4Len =
                (UINT4) (STRLEN ("Forcefully logged out ...!") <
                         (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1) ?
                         STRLEN ("Forcefully logged out ...!")
                         : (sizeof (pTempAudit->TempCmd.au1TempCmd) - 1));
            STRNCPY (pTempAudit->TempCmd.au1TempCmd,
                     "Forcefully logged out ...!", u4Len);
        }

        pTempAudit->TempCmd.u4CliMode = pCliContext->gu4CliMode;
        tp = localtime (&t);
        if (tp != NULL)
        {
            pTime = asctime (tp);
            if (pTime != NULL)
            {
                u4Len = ((STRLEN (pTime) < sizeof (pTempAudit->au1Time)) ?
                         STRLEN (pTime) : sizeof (pTempAudit->au1Time) - 1);
                STRNCPY (pTempAudit->au1Time, pTime, u4Len);
                pTempAudit->au1Time[u4Len] = '\0';
            }
        }
        pTempAudit->i2Flag = AUDIT_CLI_MSG;
        if (pCliContext->gu4CliMode != CLI_MODE_CONSOLE)
        {
            pTempAudit->TempCmd.u4ClientIpAddr =
                pCliContext->au4ClientIpAddr[3];
        }
        MSRSendAuditInfo (pTempAudit);
        MemReleaseMemBlock (gAuditInfoMemPoolId, (UINT1 *) pTempAudit);
    }
    return;
}
