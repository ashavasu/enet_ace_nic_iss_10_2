/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: clicxutl.c,v 1.27 2015/09/30 11:00:09 siva Exp $
 *
 * Description: This will have the help related routines
 *              for display module
 ******************************************************************************/

/*********************** HEADER FILES *****************************************/
#include "clicmds.h"

INT2 CliCxUtlGetModeIndexFrmCmd PROTO ((t_MMI_CMD * pCmd));

/************************ External variables **********************************/

extern tCliSessions gCliSessions;
extern t_MMI_TOKEN_TYPE mmi_cmd_token[];
extern tMemPoolId   gCliMaxLineMemPoolId;
extern UINT2               gu1DynHlpDsc;

/******************************************************************************
 * FUNCTION NAME : CliCxUtlGetBitMaskPosition
 * DESCRIPTION   : This function is to find the type of the NonKeyword
                   token
 * INPUT         : token type value of the token
 * RETURNS       : returns the value corresponding to the position of this
                   nonkeywod type in the  mmi_cmd_token[] array
 *                 returns 0 on non-existence of this type of keyword
 ******************************************************************************/

INT2
CliCxUtlGetBitMaskPosition (FS_UINT8 u8Check)
{

    INT2                i2Cnt = 0;

    if (FSAP_U8_FETCH_HI (&u8Check) == 0)
    {
        for (i2Cnt = 1; i2Cnt <= CLI_LOW_BYTE_MAX_VAL; i2Cnt++)
        {

            if (FSAP_U8_FETCH_LO (&u8Check) == (UINT4) (1 << (i2Cnt - 1)))
            {
                return (i2Cnt);
            }
        }
    }
    else
    {
        for (i2Cnt = 1; i2Cnt <= CLI_LOW_BYTE_MAX_VAL; i2Cnt++)
        {
            if (FSAP_U8_FETCH_HI (&u8Check) == (UINT4) (1 << (i2Cnt - 1)))
            {
                return (i2Cnt + CLI_LOW_BYTE_MAX_VAL);
            }
        }
    }
    return 0;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlGetFirstTokenInCmd
 * DESCRIPTION   : This function is used to find the first token of the
 *                 corresponding command
 * INPUT         : pointer to the command and address of first token
 * RETURNS       : None
 ******************************************************************************/

VOID
CliCxUtlGetFirstTokenInCmd (t_MMI_COMMAND_LIST * pCmdTkn,
                            t_MMI_CMD ** pRetFirstTkn)
{
    if (pCmdTkn == NULL)
    {
        return;
    }
    if (pCmdTkn->pCmd != NULL)
    {
        *pRetFirstTkn = pCmdTkn->pCmd;
    }
    else
    {
        *pRetFirstTkn = NULL;
    }
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlGetModeIndexFrmCmd
 * DESCRIPTION   : This function is used to find the last token of the
 *                 corresponding command
 * INPUT         : pointer to the command
 * RETURNS       : pointer to last token
 ******************************************************************************/

INT2
CliCxUtlGetModeIndexFrmCmd (t_MMI_CMD * pTravTkn)
{
    while (pTravTkn->pNext_token != NULL)
    {
        pTravTkn = pTravTkn->pNext_token;
    }

    /* to get the last token of the cmd */
    return (pTravTkn->i2ModeIndex);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlSplitTokens
 * DESCRIPTION   : This function is used to split the entered command into
                   seperate tokens
 * INPUT         : Entered command
                   Array of pointers to hold split tokens
 * RETURNS       : returns total number of tokens entered
 ******************************************************************************/

INT4
CliCxUtlSplitTokens (INT1 *i1pInputStr,
                     UINT1 au1TknList[][CLI_MAX_TOKEN_VAL_MEM])
{
    INT1                i1Prev = 1;
    INT2                i2Row = 0;
    INT2                i2Col = 0;
    INT4                i4Cnt = 0;
    INT4                i4EnteredTok = 0;
    INT4                i4Len = 0;
    INT1                i1IsInputString = 0;

    if (i1pInputStr == NULL)
    {
        return i4EnteredTok;
    }
    i4Len = STRLEN (i1pInputStr);
    MEMSET (au1TknList, 0, (MAX_NO_OF_TOKENS_IN_MMI * CLI_MAX_TOKEN_VAL_MEM));

    for (i4Cnt = 0; i4Cnt < i4Len; i4Cnt++)
    {
        if (i1pInputStr[i4Cnt] == '"')
        {
            /* To allow space inside the quotes */
            if (i1IsInputString == 0)
            {
                i1IsInputString = 1;
            }
            else
            {
                i1IsInputString = 0;
            }
        }

        if (i1Prev && (i1pInputStr[i4Cnt] != ' '))
        {
            i4EnteredTok++;
        }
        if ((i1pInputStr[i4Cnt] != ' ') || (i1IsInputString == 1))
        {
            i1Prev = 0;

            /* To ensure, we dont write more than what au1TknList[i2Row] 
             * can store. Also leaving one space for NULL char.
             * */
            if (i2Col < CLI_MAX_TOKEN_VAL_MEM - 1)
            {
                au1TknList[i2Row][i2Col] = i1pInputStr[i4Cnt];
                ++i2Col;
            }
        }
        else if (i1Prev != 1)
        {
            i1Prev = 1;
            au1TknList[i2Row][i2Col] = '\0';
            /* au1TknList stores the tokens */
            ++i2Row;
            i2Col = 0;
        }
    }
    return (i4EnteredTok);

}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlGetGrpCnt
 * DESCRIPTION   : This function is used to find the total number of
 *                 groups in the command tree
 * INPUT         : Pointer to the cmd tree
 * RETURNS       : returns total number of groups in the cmd tree
 ******************************************************************************/
INT2
CliCxUtlGetGrpCnt (t_MMI_COMMAND_LIST * pTmpCmd)
{
    INT2                i2TotGrp = 0;

    if (pTmpCmd == NULL)
    {
        mmi_printf ("no cmd in command tree\n");
        return (0);
    }
    else
    {
        while (pTmpCmd != NULL)
        {
            pTmpCmd = pTmpCmd->pNext;
            i2TotGrp += 1;
        }
    }
    return (i2TotGrp);
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlSortCxtHelpStrings
 * DESCRIPTION   : This function is used to sort all the next possible
 *                 optional tokens.
 * INPUT         : pointer to pointer containing matched tokens & help string
 *                 number of matched tokens
 * RETURNS       : NONE
 ******************************************************************************/

VOID
CliCxUtlSortCxtHelpStrings (UINT1 **ppu1HelpStr, INT2 i2HelpDescriptor)
{
    UINT1              *pu1TempHelpStr = NULL;
    INT2                i2LoopIndex1 = 0;
    INT2                i2LoopIndex2 = 0;

    while (i2LoopIndex1 < i2HelpDescriptor)
    {
        i2LoopIndex2 = i2LoopIndex1 + 1;
        while (i2LoopIndex2 < i2HelpDescriptor)
        {
            if (ppu1HelpStr[i2LoopIndex1] == NULL ||
                ppu1HelpStr[i2LoopIndex2] == NULL)
            {
                i2LoopIndex2++;
                continue;
            }
            if (STRCMP (ppu1HelpStr[i2LoopIndex1],
                        ppu1HelpStr[i2LoopIndex2]) > 0)
            {
                pu1TempHelpStr = ppu1HelpStr[i2LoopIndex2];
                ppu1HelpStr[i2LoopIndex2] = ppu1HelpStr[i2LoopIndex1];
                ppu1HelpStr[i2LoopIndex1] = pu1TempHelpStr;
            }
            i2LoopIndex2++;
        }
        i2LoopIndex1++;
    }

    return;

}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlGetFirstWord
 * DESCRIPTION   : This function is to get the first word  from cxthlp string
 * INPUT         : pointer to parsed cxthlp String
 *                 pointer to store extracted word
 * RETURNS       : none
 ******************************************************************************/
VOID
CliCxUtlGetFirstWord (UINT1 *pu1String, UINT1 *pu1RetString)
{
    INT1                i1Count = 0;
    while (1)
    {
        if ((pu1String[i1Count] == ' ') || (pu1String[i1Count] == '\0') ||
            (pu1String[i1Count] == '|') || (pu1String[i1Count] == '\r') ||
            (pu1String[i1Count] == '\n'))
        {
            pu1RetString[i1Count] = '\0';
            break;
        }
        else
        {
            pu1RetString[i1Count] = pu1String[i1Count];
        }
        i1Count++;
    }
    return;

}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlPrintTknWithHlpString
 * DESCRIPTION   : This function is to print required tkn with its cxthlp 
 *                 string with a constant space in between
 * INPUT         : pointer to parsed cxthlp String
 * RETURNS       : none
 ******************************************************************************/
VOID
CliCxUtlPrintTknWithHlpString (UINT1 *pu1String)
{

    INT4                i4Count = 0;
    INT4                i4Position = 0;
    UINT1               au1ContextHelp[CLI_MAX_HELP_STR_LEN];

    MEMSET (au1ContextHelp, 0, CLI_MAX_HELP_STR_LEN);
    mmi_printf ("\r\n");

    /* Store the first word (Token) and leave space befor help string */
    while ((pu1String[i4Position] != '|') && (pu1String[i4Position] != '\0')
           && (pu1String[i4Position] != '\n')
           && (pu1String[i4Position] != '\r'))
    {
        if (pu1String[i4Position] == ' ')
        {
            while (pu1String[i4Position] != ' ')
            {
                i4Position++;
            }
            for (; i4Count < CLI_OUTPUT_SPACE; i4Count++)
            {
                au1ContextHelp[i4Count] = ' ';
            }
            break;
        }
        au1ContextHelp[i4Count] = pu1String[i4Position];
        i4Count++;
        i4Position++;
    }

    /* Storing the help string (Token help) */
    while ((pu1String[i4Position] != '|') && (pu1String[i4Position] != '\0')
           && (pu1String[i4Position] != '\n')
           && (pu1String[i4Position] != '\r'))
    {
        au1ContextHelp[i4Count] = pu1String[i4Position];
        i4Count++;
        /* for wrap around */
        if (i4Count == 79)
        {
            while (au1ContextHelp[i4Count] != ' ')
            {
                i4Count--;
                i4Position--;
            }

            au1ContextHelp[i4Count] = '\0';
            mmi_printf ("%s\r\n", au1ContextHelp);
            MEMSET (au1ContextHelp, ' ', CLI_OUTPUT_SPACE);
            i4Count = CLI_OUTPUT_SPACE;
        }
        i4Position++;
    }

    au1ContextHelp[i4Count] = '\0';
    mmi_printf ("\r%s", au1ContextHelp);

    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlPrintTkn
 * DESCRIPTION   : This function is to print required tkn alone
 * INPUT         : pointer to parsed cxthlp String
 * RETURNS       : none
 ******************************************************************************/
VOID
CliCxUtlPrintTkn (UINT1 *pu1String)
{

    INT4                i4Count = 0;
    INT4                i4Position = 0;

    UINT1               au1ContextHelp[CLI_MAX_HELP_STR_LEN];

    MEMSET (au1ContextHelp, 0, CLI_MAX_HELP_STR_LEN);
    mmi_printf ("\r");
    /*mmi_printf ("\n"); */
    /* Store the first word (Token) */
    while ((pu1String[i4Position] != '|') && (pu1String[i4Position] != '\0'))
    {
        if (pu1String[i4Position] == ' ')
        {
            au1ContextHelp[i4Count] = '\0';
            break;
        }
        au1ContextHelp[i4Count] = *(pu1String + i4Position);
        i4Count++;
        i4Position++;
    }

    mmi_printf ("%-40s\r\n", au1ContextHelp);
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlGetFirstTknHelp
 * DESCRIPTION   : This function is store the cxt help string 
 *                 for first tokens of all commands in the current mode
 * INPUT         : p_thelp_ptr - Help STructure pointer
 *                 pCxtHelpParams - Pointer to Cxt help params
 *                 pi4CheckFlag  - Pointer to Check flag
 * RETURNS       : none
 ******************************************************************************/
PUBLIC VOID
CliCxUtlGetFirstTknHelp (t_MMI_HELP * p_thelp_ptr,
                         tCxtHelpParams * pCxtHelpParams, INT4 *pi4CheckFlag)
{
    INT2                i2Index = 0;
    INT2                i2TotalCmds = 0;
    tCliContext        *pCliContext = NULL;

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return;
    }
    while (p_thelp_ptr != NULL)
    {
        i2TotalCmds = (p_thelp_ptr->i4ActNumEnd -
                       p_thelp_ptr->i4ActNumStart) + 1;
        for (i2Index = 0; i2Index < i2TotalCmds; ++i2Index)
        {
            if (p_thelp_ptr->pCxtHelp[i2Index] != NULL)
            {
                if (p_thelp_ptr->pPrivilegeId[i2Index] <=
                    pCliContext->i1PrivilegeLevel)
                {
                    MEMCPY (&pCxtHelpParams->ppu1HelpStr
                            [pCxtHelpParams->i2HelpStrIndex],
                            &p_thelp_ptr->pCxtHelp[i2Index], sizeof (UINT1 *));
                    pCxtHelpParams->i2HelpStrIndex++;
                    *pi4CheckFlag = *pi4CheckFlag + 1;

                }
            }
            else
            {
                /* skip if the entry is marked as removed already */
                if ((p_thelp_ptr->pSyntax[i2Index] != NULL) &&
                    (p_thelp_ptr->pSyntax[i2Index] != CLI_REM_CMD))
                {
                    mmi_printf ("\r %s \n", p_thelp_ptr->pSyntax[i2Index]);
                }

            }
        }
        p_thelp_ptr = p_thelp_ptr->pNext;
    }

    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlFindMatchInAllGrps
 * DESCRIPTION   : This function is store the cxt help string
 *                 for first tokens of all commands in the current mode
 * INPUT         : p_thelp_ptr - Help STructure pointer
 *                 pCxtHelpParams - Pointer to Cxt help params
 *                 pi4CheckFlag  - Pointer to Check flag
 * RETURNS       : none
 ******************************************************************************/
PUBLIC VOID
CliCxUtlFindMatchInAllGrps (t_MMI_HELP * p_thelp_ptr,
                            tCxtHelpParams * pCxtHelpParams,
                            INT2 *pi2SyntaxCheckFlag, INT1 i1SpaceFlag)
{
    INT2                i2Index = 0;
    INT2                i2TotalCmds = 0;

    gu1DynHlpDsc = 0;

    while (p_thelp_ptr != NULL)
    {
        i2Index = 0;

        pCxtHelpParams->i1IsGrpCntChanged = OSIX_FALSE;
        /* Initialize constant cxt help params */
        pCxtHelpParams->p_thelp_ptr = p_thelp_ptr;

        /* For each command with in a group, check for match 
         * 1. Parsing the all the syntax available in the group
         * 2. For each syntax, verify if the command matches*/
        i2TotalCmds = (p_thelp_ptr->i4ActNumEnd -
                       p_thelp_ptr->i4ActNumStart) + 1;
        while (i2Index < i2TotalCmds)
        {
            /* skip if the entry is marked as removed already */
            if ((p_thelp_ptr->pSyntax[i2Index] != NULL) &&
                (p_thelp_ptr->pSyntax[i2Index] != CLI_REM_CMD))

            {
                /* Initialize constant cxt help params */
                pCxtHelpParams->i2Index = i2Index;

                /* find cxt help if the cmd structure matches with the syntax
                 * found out from entered tokens*/
                CliCxUtlGetCxtHlpFrmSyntax (pCxtHelpParams,
                                            pi2SyntaxCheckFlag, i1SpaceFlag);
            }
            i2Index++;
        }
        /* incrementing group count */
        pCxtHelpParams->i2GrpCnt += 1;

        p_thelp_ptr = p_thelp_ptr->pNext;
    }

    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlHandleCxtFrmSyntax
 * DESCRIPTION   : This function is used to find diff options from cxt help
 *                 of the command got from corresponding matched cmd syntax
 * INPUT         : Pointer to pointer to the command tree
 *                 Pointer to the structure having cmd params
 *                 Flag referring to whether space is there before question mark
 *                 or not
 * RETURNS       : None
 ******************************************************************************/
PUBLIC VOID
CliCxUtlGetCxtHlpFrmSyntax (tCxtHelpParams * pCxtHelpParams,
                            INT2 *pi2SyntaxFlag, INT1 i1SpaceFlag)
{
    t_MMI_COMMAND_LIST *pCmdToken = NULL;
    tCliContext        *pCliContext = NULL;
    t_MMI_CMD          *pEachCmd = NULL;
    t_MMI_CMD          *pNextTkn = NULL;
    t_MMI_CMD          *pNextOfNextTkn = NULL;
    t_MMI_CMD          *pRetToken = NULL;
    t_MMI_CMD          *pRecurToken = NULL;
    t_MMI_CMD          *paRecurNodes[MAX_INNER_RECUR];
    INT2                i2ModeIndex = 0;
    INT2                i2ArrCnt = 0;
    INT2                i2RetFlag = 0;
    INT4                i4MatchCnt = 0;
    INT4                i4RecurFlag = 0;
    INT4                i4RecurLoopCount = 0;
    INT4                i4InnerRecurCmpltFlag = 0;
    INT4               *pi4OptionalTokens = NULL;
    UINT1               au1TokenCount[CLI_CXT_TKN_LIST_SIZE];
    BOOL1               bCliCxtTokenSetFlag = OSIX_TRUE;
    BOOL1               bTokenSetFlag = OSIX_TRUE;
    UINT1               u1TokenIndex = 1;

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return;
    }
    pi4OptionalTokens =
        CliMemAllocMemBlk (gCliMaxLineMemPoolId,
                           (sizeof (INT4) * CLI_OPTNLTKNS) + 1);
    if (pi4OptionalTokens == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for optional token\n");
        return;
    }
    pCliContext->pu1OptionalTokens = (UINT1 *)pi4OptionalTokens;

    /* To get the matched syntax from help tree with the entered cmd */
    MEMSET (&au1TokenCount, 0, sizeof (au1TokenCount));
    MEMSET (pi4OptionalTokens, 0, (MAX_LINE_LEN +1));
    /* To get the matched syntax from help tree with the entered cmd */
    if ((pCxtHelpParams->pu1Str == NULL) ||
        !(CLI_STRLEN (pCxtHelpParams->pu1Str)) ||
        ((pCxtHelpParams->pu1Str) &&
         (CliCxtTokenMatch ((CONST UINT1 *)
                            pCxtHelpParams->p_thelp_ptr->
                            pSyntax[pCxtHelpParams->i2Index],
                            (UINT1 *) pCxtHelpParams->pu1Str, au1TokenCount)
          == CLI_ZERO)))
    {
        for (u1TokenIndex = 1; u1TokenIndex <= MAX_NO_OF_TOKENS_IN_MMI;
             u1TokenIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (au1TokenCount, u1TokenIndex,
                                     sizeof (au1TokenCount), bTokenSetFlag);
            OSIX_BITLIST_IS_BIT_SET (pCxtHelpParams->i1TknMatchFlag,
                                     u1TokenIndex,
                                     sizeof (pCxtHelpParams->i1TknMatchFlag),
                                     bCliCxtTokenSetFlag);

            /* If it is the first exact match, then clear the Context help string array */
            if ((bTokenSetFlag == OSIX_TRUE)
                && (bCliCxtTokenSetFlag == OSIX_FALSE))
            {
                OSIX_BITLIST_SET_BIT (pCxtHelpParams->i1TknMatchFlag,
                                      u1TokenIndex,
                                      sizeof (pCxtHelpParams->i1TknMatchFlag));
                pCxtHelpParams->i2HelpStrIndex = 0;
            }
            /* If already the exact match is found, then ignore if only a substring is matched */
            if ((bTokenSetFlag == OSIX_FALSE)
                && (bCliCxtTokenSetFlag == OSIX_TRUE))
            {
                CliMemReleaseMemBlock (gCliMaxLineMemPoolId, 
                                       &pCliContext->pu1OptionalTokens);
                return;
            }
        }
        /* Process only if the previlege level matches. */
        if ((pCliContext->i1PrivilegeLevel) >=
            (pCxtHelpParams->p_thelp_ptr)->
            pPrivilegeId[pCxtHelpParams->i2Index])
        {
            if (pCxtHelpParams->i1IsGrpCntChanged == OSIX_FALSE)
            {
                pCmdToken = pCliContext->pMmiCur_mode->pCommand_tree;
                pCmdToken =
                    CliCxHlpGetGrpFrmCmdStruct (pCmdToken,
                                                pCxtHelpParams->i2GrpCnt,
                                                pCxtHelpParams->i2TotGrpCnt);
                pCxtHelpParams->pCurrGrpCmdList = pCmdToken;
                pCxtHelpParams->i1IsGrpCntChanged = OSIX_TRUE;
            }
            else
            {
                pCmdToken = pCxtHelpParams->pCurrGrpCmdList;
            }

            /* To traverse and get the grp in cmd tree corresponding to the
             * grp in help tree to which the matched syntax belongs*/
            CliCxUtlGetFirstTokenInCmd (pCmdToken, &pEachCmd);

            while (pEachCmd != NULL)
            {
                i2ModeIndex = CliCxUtlGetModeIndexFrmCmd (pEachCmd);

                /* cmd with the corresponding cmd no as its mode index */
                if ((pCxtHelpParams->i2Index + 1) == i2ModeIndex)
                {

                    i4MatchCnt =
                        CliCxHlpParseEnteredCmd (pEachCmd,
                                                 pCxtHelpParams,
                                                 paRecurNodes,
                                                 &pRetToken,
                                                 &pRecurToken,
                                                 &i4RecurFlag,
                                                 &i4RecurLoopCount,
                                                 &i4InnerRecurCmpltFlag,
                                                 i1SpaceFlag);
                    if (i4MatchCnt == pCxtHelpParams->i4EnteredTok &&
                        (pRetToken != NULL) &&
                        (pRetToken->pNext_token != NULL) &&
                        (pRecurToken == NULL))
                    {
                        break;
                    }
                }

                pEachCmd = pEachCmd->pNext_command;
                /* To check all the cmds in the cmd group */
            }

            /* when parsing passed */
            if ((i4MatchCnt == pCxtHelpParams->i4EnteredTok)
                && (pRetToken != NULL))
            {
                /* To find which token should be checked for next possible tokens
                 * after the last successfully parsed token*/
                i2ArrCnt = CliCxHlpFindNxtTknAftrParsing (pRetToken,
                                                          pCxtHelpParams,
                                                          &pNextTkn,
                                                          &pNextOfNextTkn,
                                                          &pRecurToken,
                                                          pi4OptionalTokens,
                                                          i4RecurFlag,
                                                          i4InnerRecurCmpltFlag,
                                                          i2ArrCnt,
                                                          i1SpaceFlag);
                if (i2ArrCnt == -1)
                {
                    *pi2SyntaxFlag = MMI_GEN_ERR_TOKEN_AMBIGUITY;
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                    &pCliContext->pu1OptionalTokens);
                    return;
                }
                if (i4InnerRecurCmpltFlag == 0)
                {
                    if ((i4RecurFlag == 1) && (i1SpaceFlag == 1))
                    {
                        pNextOfNextTkn = pRecurToken->pNext_token;
                    }
                }
                else
                {
                    pNextTkn = NULL;
                }
                /* To find all possible next optional tokens */
                CliCxHlpFindOptionalTkns (pNextTkn,
                                          pRecurToken, paRecurNodes,
                                          pi4OptionalTokens, &i2ArrCnt,
                                          i4RecurFlag, i4RecurLoopCount,
                                          i1SpaceFlag);
                /* To find all possible tokens in the same level */
                CliCxHlpFindNxtCmdTkns (pNextOfNextTkn, pRecurToken,
                                        paRecurNodes, pi4OptionalTokens,
                                        &i2ArrCnt, i4RecurFlag,
                                        i4RecurLoopCount, i1SpaceFlag);
                ++i2RetFlag;
            }

            if ((i2RetFlag != 0) &&
                (pCxtHelpParams->p_thelp_ptr->pCxtHelp
                 [pCxtHelpParams->i2Index] == NULL))
            {
                /* help from syntax if cxt help is not present */
                mmi_printf ("  %s\r\n\n",
                            (CONST CHR1 *)
                            ((pCxtHelpParams->p_thelp_ptr)->
                             pSyntax[pCxtHelpParams->i2Index]));
                *pi2SyntaxFlag = 1;
            }

            if ((i2RetFlag != 0) &&
                (pCxtHelpParams->p_thelp_ptr->pCxtHelp
                 [pCxtHelpParams->i2Index] != NULL))
                /* help from cxt help string */
            {
                CliCxHlpParseCxtHlpString (pCxtHelpParams, pi4OptionalTokens,
                                           i2ArrCnt);
            }

        }
    }
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                           &pCliContext->pu1OptionalTokens);
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlMakeLowerCaseToCmp
 * DESCRIPTION   : This function is used to make the given two strings to lower
 *                 cse
 * INPUT         : Pointer to pointer to the entered token
 *                 Pointer to the actual keyword
 *                 Pointer to temporary keyword for comparison  
 * RETURNS       : None
 ******************************************************************************/
PUBLIC VOID
CliCxUtlMakeLowerCaseToCmp (UINT1 *pu1Token, CONST CHR1 * pc1KeyWord,
                            UINT1 *pu1TempKeyWord)
{
    INT4                i4LoopIndex = 0;
    INT4                i4StrLen = 0;

    MEMSET (pu1TempKeyWord, 0, CLI_MAX_TOKEN_VAL_MEM);
    i4StrLen = (INT4) STRLEN (pu1Token);

    for (i4LoopIndex = 0; i4LoopIndex <= i4StrLen; i4LoopIndex++)
    {
        pu1Token[i4LoopIndex] = TOLOWER (pu1Token[i4LoopIndex]);
    }

    if (i4StrLen > (INT4) STRLEN (pc1KeyWord))
    {
        i4StrLen = (INT4) STRLEN (pc1KeyWord);
    }

    for (i4LoopIndex = 0; i4LoopIndex <= i4StrLen; i4LoopIndex++)
    {
        pu1TempKeyWord[i4LoopIndex] = TOLOWER ((UINT1) pc1KeyWord[i4LoopIndex]);
    }

    pu1TempKeyWord[i4LoopIndex] = '\0';
    return;
}

/*******************************************************************************
 * FUNCTION NAME : CliCxUtlMatchIncompleteToken
 * DESCRIPTION   : This function is used to compare the string entered by user  
 *                 with existing tokens
 * INPUT         : Pointer - String entered by user
 *                 Pointer - Existing tokens
 * RETURNS       : OSIX_TRUE /
 *                 OSIX_FALSE  
 ******************************************************************************/
UINT1
CliCxUtlMatchIncompleteToken (UINT1 *pu1IncompleteStr, CONST UINT1 *pu1String)
{
    UINT1              *pu1TempString = NULL;
    UINT1               u1StrLen = 0;
    UINT1               u1MatchResult = OSIX_TRUE;
    UINT1               u1LoopIndex = 0;

    if (pu1IncompleteStr == NULL)
    {
        return u1MatchResult;
    }

    u1StrLen = (UINT1) STRLEN (pu1IncompleteStr);

    pu1TempString = pu1IncompleteStr;

    while ((u1LoopIndex < u1StrLen) && (u1MatchResult != OSIX_FALSE))
    {
        if (tolower (*(pu1TempString)) == tolower (pu1String[u1LoopIndex]))
        {
            u1MatchResult = OSIX_TRUE;
        }
        else
        {
            u1MatchResult = OSIX_FALSE;
        }

        ++pu1TempString;
        u1LoopIndex++;
    }
    return u1MatchResult;
}

/************************END OF FILE*******************************************/
