/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: cliutils.c,v 1.190 2017/11/30 14:02:05 siva Exp $
 *
 * Description:Routines for validating interface index, to get 
 *             current interface index, to get interface index 
 *             if interface name is given                      
 *******************************************************************/
#include "clicmds.h"
#include "utilcli.h"
#include "fssocket.h"

UINT1               gau1PortBitMask[CLI_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

/* Array of Context Structures */
extern tCliSessions gCliSessions;
extern UINT4        gu4CliConsoleBitMap;
unCliCallBackEntry  gaCliCallBack[CLI_MAX_CALLBACK_EVENTS];

extern tCliAmbMemPoolId gCliAmbMemPoolId;
extern tMemPoolId   gCliMaxLineMemPoolId;
extern tMemPoolId   gCliCmdTokenPoolId;
extern tMemPoolId   gAuditInfoMemPoolId;
PUBLIC tMemPoolId   gCliUserMemPoolId;
PUBLIC tMemPoolId   gCliGroupMemPoolId;
PUBLIC tMemPoolId   gCliPrivilegesMemPoolId;
BOOL1               gb1MgmtLockStatus = OSIX_FALSE;
INT1                CliUpdatePriv (t_MMI_MODE_TREE * pTsearch,
                                   CHR1 * pu1CommandName, INT4 *pi4PrivId);
/* Temporarily holds the string entered by User */
static CHR1         au1UsrStr[CLI_MAX_HELP_LINE_LEN + 1];
/* Temporarily holds the strings obtained by search */
static CHR1         au1LocalStr[CLI_MAX_HELP_LINE_LEN + 1];

/* Holds the flag to denote exact match */
static BOOL1        ba1MatchFound[CLI_MAX_HELP_TOKEN_NUM] = { 0 };

/* Temporarily holds the tokens of input and search strings*/
static CHR1        *apu1InputToken[CLI_MAX_HELP_TOKEN_NUM];
static CHR1        *apu1StringToken[CLI_MAX_HELP_TOKEN_NUM];

/**********************************************************************n
 * This function is an api to ssh module which is called to set the ssh  
 * clients window size in  the cli context variables. 
 ************************************************************************/
#ifdef SSH_WANTED
VOID
CliSetSshClientWindowSize (INT2 i2Row, INT2 i2Col)
{

    tCliContext        *pCliContext;

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }

    if ((i2Row != 0) && (i2Col != 0))
    {
        pCliContext->i2PrevMaxCol = pCliContext->i2MaxCol;
        pCliContext->i2PrevMaxRow = pCliContext->i2MaxRow;
        pCliContext->i2MaxRow = i2Row;
        pCliContext->i2MaxCol = i2Col;
    }
    if ((pCliContext->i2MaxRow != pCliContext->i2PrevMaxRow) ||
        (pCliContext->i2MaxCol != pCliContext->i2PrevMaxCol))
    {
        pCliContext->i1WinResizeFlag = TRUE;
    }
    return;

}
#endif

/************************************************************************
 * This function is a wrapper function for the CliChangePath() and  
 * prints the error message based on the return status
 ************************************************************************/
INT4
cli_change_path (CONST CHR1 * pu1Path)
{
    if ((CliChangePath (pu1Path)) == CLI_FAILURE)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_NO_SUCH_MODE]);
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);
}

#ifdef SLI_WANTED
/************************************************************************
 * This function will close the obscure telnet sessions i.e closed sessions
 * which FIN has not arrived, but SOCKET is closed at the client side
 ************************************************************************/
VOID
ClosePassiveConnections (VOID)
{
    tCliContext        *pTelnetCliContext = NULL;
    INT1                i1Index = 0;
    INT4                i4RetVal = 0;
    UINT1               au1NawsDoTest[TS_NAWS_DO_LEN] =
        { TS_IAC, TS_DO, TS_NAWS };

    CliContextLock ();
    for (i1Index = 0; i1Index < CLI_MAX_SESSIONS; i1Index++)
    {
        pTelnetCliContext = &(gCliSessions.gaCliContext[i1Index]);
        if (pTelnetCliContext->i1Status == CLI_ACTIVE)
        {
            if (pTelnetCliContext->gu4CliMode == CLI_MODE_TELNET)
            {
                /* If the session is telnet, then there are chances 
                 * that the session may be still invalid. Hence do a 
                 * select for the same and if it fails, use that fd
                 */
                i4RetVal =
                    send (pTelnetCliContext->i4ClientSockfd,
                          (VOID *) au1NawsDoTest, sizeof (au1NawsDoTest),
                          MSG_PUSH_FLAG);

                if (i4RetVal == -1)
                {
                    if (pTelnetCliContext->TaskId > 0)
                    {
                        OsixEvtSend (pTelnetCliContext->TaskId,
                                     CLI_SELECT_TIMER_EVENT);
                    }
                }

                /* Send is Success ==> Session is active only */
                i4RetVal = 0;
            }                    /* Telnet Session */

        }                        /* Active Session */

    }                            /* end of for  loop */
    CliContextUnlock ();
}
#endif

/**********************************************************************
 * This function will clear the console
 **********************************************************************/
VOID
CliClear (VOID)
{
    tCliContext        *pCliContext;
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    if ((pCliContext->i4Mode == CLI_IF_RANGE_MODE_ENABLE)
        && (pCliContext->u1NoOfAttempts >= 1))
    {
        return;
    }

    if (pCliContext->i4Mode == CLI_IF_RANGE_MODE_ENABLE)
    {
        pCliContext->u1NoOfAttempts++;
    }

    mmi_printf ("[H[J");
    mmi_printf ("\r%s", CLI_MOVE_CURSOR_TO_END);
}

 /*
  * This function reads the command and executes the cli commands
  * By default output is printed on the local console. If output file name
  * is specified, cli output will be redirected to that file in addition
  * to console output
  */
INT2
cli_file_operation (CONST CHR1 * pu1CmdScriptFile, CONST CHR1 * pu1OutputFile)
{

    tCliContext        *pCliContext = NULL;
#ifdef L2RED_WANTED
    UINT1               u1PeerNodeCount = 0;
#endif
    INT1                i1DefaultMoreFlag = CLI_MORE_DISABLE;
    INT4                i4TempWriteFd = -1;
    INT4                i4Mode = 0;
    INT4                i4ContextWriteFd = -1;
    INT4                (*pTempOutputFp) (struct CliContext *, CONST CHR1 *,
                                          UINT4);

    INT4                i4TempFileFlag = FALSE;
#ifdef L2RED_WANTED
    u1PeerNodeCount = RmGetPeerNodeCount ();
#endif

    pTempOutputFp = NULL;
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    MEMCPY (pCliContext->ai1CmdBuf, pu1CmdScriptFile,
            STRLEN (pu1CmdScriptFile));

    if ((pu1OutputFile) && (pCliContext->i1MoreFlag == TRUE))
    {
        mmi_printf ("\rInvalid Command\r\n");
        return (CLI_FAILURE);
    }

    pCliContext->mmi_file_flag = FALSE;
    if (pCliContext->i2ScriptRecCount >= MAX_SCRIPT_REC_COUNT)
    {
        mmi_printf ("\r\nToo many Script recursions\r\n");
        return (CLI_FAILURE);
    }
    pCliContext->i2ScriptRecCount++;
    /*Copying the File Flag temporarily */
    i4TempFileFlag = pCliContext->i4FileFlag;
    if ((pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE))
    {
        i1DefaultMoreFlag = CLI_MORE_ENABLE;
        pCliContext->i1DefaultMoreFlag = CLI_MORE_DISABLE;
        CliDeletePipe (pCliContext);
    }
    if (pu1OutputFile)
    {
        if ((i4TempWriteFd = CliOpenFile (pu1OutputFile, CLI_WRONLY)) < 0)
        {
            mmi_printf ("\rUnable to create output file: %s\r\n",
                        pu1OutputFile);
            pCliContext->i2ScriptRecCount--;
            if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
            {
                pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                CliCreatePipe (pCliContext);
            }

            return (CLI_FAILURE);
        }
        else
        {
            if (!CliIsRegularFile (pu1OutputFile))
            {
                mmi_printf ("\r\nOutput File not a regular file!!!\r\n");
                CliCloseFile (i4TempWriteFd);
                pCliContext->i2ScriptRecCount--;
                if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
                {
                    pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                    CliCreatePipe (pCliContext);
                }

                return (CLI_FAILURE);
            }
            i4ContextWriteFd = pCliContext->i4OutputFd;
            pCliContext->i4OutputFd = i4TempWriteFd;
            pTempOutputFp = pCliContext->fpCliOutput;
            pCliContext->fpCliOutput = pCliContext->fpCliFileWrite;
        }
    }
    pCliContext->mmi_file_flag = TRUE;
    /* File Operation command would have taken the lock. So, unlock here.
     * The following sequence of commands will take the lock each time and
     * then proceed as usual
     * */
    MGMT_UNLOCK ();
    if (i4TempFileFlag == TRUE)
    {
        pCliContext->i4FileFlag = FALSE;
    }

    while (pCliContext->mmi_file_flag)
    {
        if (pCliContext->i1ClearFlagStatus == OSIX_TRUE)
        {
            pCliContext->mmi_exit_flag = OSIX_TRUE;
        }

        if (pCliContext->mmi_exit_flag)
        {
            break;
        }
#ifdef L2RED_WANTED
        if (RmGetPeerNodeCount () != 0)
        {
            if (u1PeerNodeCount == 0)
            {
                mmi_printf
                    ("\rAborting!!!!!!!! run script since Bulk Update is in progress !!!!!\r\n");
                break;
            }
            else if (RmIsBulkUpdateComplete () == RM_FAILURE)
            {
                mmi_printf
                    ("\rAborting run script since Bulk Update is in progress !!!!!\r\n");
                break;
            }
        }
#endif
        mmi_print_user_prompt (NULL, pCliContext);

        if ((CliGetIfRangeMode (&i4Mode) == CLI_SUCCESS) &&
            i4Mode == (INT4) CLI_IF_RANGE_MODE_ENABLE)
        {
            CliExecuteIfRangeCommand (pCliContext,
                                      (CHR1 *) pCliContext->ai1CmdBuf);

        }
        else
        {
            if (CliExecuteCliCmd ((CHR1 *) pCliContext->ai1CmdBuf) ==
                CLI_FAILURE)
            {
                /* Session logged out,
                 * break the loop without executing remaining commands
                 */
                pCliContext->mmi_file_flag = FALSE;
                break;
            }
        }

        pCliContext->mmi_file_flag = FALSE;
    }
    if (i4TempFileFlag == TRUE)
    {
        pCliContext->i4FileFlag = TRUE;
    }
    pCliContext->i2ScriptRecCount--;
    if (pCliContext->i2ScriptRecCount == 0)
    {
        pCliContext->mmi_file_flag = FALSE;
    }
    if (pu1OutputFile)
    {
        pCliContext->i4OutputFd = i4ContextWriteFd;
        pCliContext->fpCliOutput = pTempOutputFp;
        if (CliCloseFile (i4TempWriteFd) == CLI_FAILURE)
        {
            mmi_printf ("\rUnable to close Ouptut File: %s\r\n", pu1OutputFile);
        }
    }
    if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
    {
        pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
        CliCreatePipe (pCliContext);
    }
    return (CLI_SUCCESS);
}

/*
 * This function reads the script file and executes the cli commands
 * By default output is printed on the local console. If output file name
 * is specified, cli output will be redirected to that file in addition 
 * to console output
 */
INT2
cli_run_script (CONST CHR1 * pu1CmdScriptFile, CONST CHR1 * pu1OutputFile)
{

    tCliContext        *pCliContext;
#ifdef L2RED_WANTED
    UINT1               u1PeerNodeCount = 0;
#endif
    INT1                i1DefaultMoreFlag = CLI_MORE_DISABLE;
    INT4                i4TempReadFd = -1;
    INT4                i4TempWriteFd = -1;
    INT4                i4Mode = 0;
    INT4                i4ContextWriteFd = -1;
    INT4                (*pTempOutputFp) (struct CliContext *, CONST CHR1 *,
                                          UINT4);

    INT4                i4TempFileFlag = FALSE;
#ifdef L2RED_WANTED
    u1PeerNodeCount = RmGetPeerNodeCount ();
#endif

    pTempOutputFp = NULL;
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    };

    if ((pu1OutputFile) && (pCliContext->i1MoreFlag == TRUE))
    {
        mmi_printf ("\rInvalid Command\r\n");
        return (CLI_FAILURE);
    }

    pCliContext->mmi_file_flag = FALSE;
    if (pCliContext->i2ScriptRecCount >= MAX_SCRIPT_REC_COUNT)
    {
        mmi_printf ("\r\nToo many Script recursions\r\n");
        return (CLI_FAILURE);
    }
    pCliContext->i2ScriptRecCount++;
    /*Copying the File Flag temporarily */
    i4TempFileFlag = pCliContext->i4FileFlag;
    if ((pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE))
    {
        i1DefaultMoreFlag = CLI_MORE_ENABLE;
        pCliContext->i1DefaultMoreFlag = CLI_MORE_DISABLE;
        CliDeletePipe (pCliContext);
    }

    if (pu1CmdScriptFile)
    {
        if ((i4TempReadFd = CliOpenFile (pu1CmdScriptFile, CLI_RDONLY)) < 0)
        {
            mmi_printf ("\rUnable to open Script File: %s\r\n",
                        pu1CmdScriptFile);
            pCliContext->i2ScriptRecCount--;
            if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
            {
                pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                CliCreatePipe (pCliContext);
            }

            return (CLI_FAILURE);
        }
        else if (!CliIsRegularFile (pu1CmdScriptFile))
        {
            mmi_printf ("\r\nScript File not a regular file!!!\r\n");
            CliCloseFile (i4TempReadFd);
            pCliContext->i2ScriptRecCount--;
            if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
            {
                pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                CliCreatePipe (pCliContext);
            }

            return (CLI_FAILURE);
        }
    }
    else
    {
        mmi_printf ("\rScript File is NULL\r\n");
        pCliContext->i2ScriptRecCount--;
        if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
        {
            pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
            CliCreatePipe (pCliContext);
        }

        return (CLI_FAILURE);
    }

    if (pu1OutputFile)
    {
        if ((i4TempWriteFd = CliOpenFile (pu1OutputFile, CLI_WRONLY)) < 0)
        {
            mmi_printf ("\rUnable to create output file: %s\r\n",
                        pu1OutputFile);
            CliCloseFile (i4TempReadFd);
            pCliContext->i2ScriptRecCount--;
            if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
            {
                pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                CliCreatePipe (pCliContext);
            }

            return (CLI_FAILURE);
        }
        else
        {
            if (CliIsSameFile (pu1CmdScriptFile, pu1OutputFile))
            {
                mmi_printf
                    ("\r\nScript File and Output File are the same!!!\r\n");
                CliCloseFile (i4TempReadFd);
                CliCloseFile (i4TempWriteFd);
                pCliContext->i2ScriptRecCount--;
                if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
                {
                    pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                    CliCreatePipe (pCliContext);
                }

                return (CLI_FAILURE);
            }
            else if (!CliIsRegularFile (pu1OutputFile))
            {
                mmi_printf ("\r\nOutput File not a regular file!!!\r\n");
                CliCloseFile (i4TempReadFd);
                CliCloseFile (i4TempWriteFd);
                pCliContext->i2ScriptRecCount--;
                if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
                {
                    pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
                    CliCreatePipe (pCliContext);
                }

                return (CLI_FAILURE);
            }
            i4ContextWriteFd = pCliContext->i4OutputFd;
            pCliContext->i4OutputFd = i4TempWriteFd;
            pTempOutputFp = pCliContext->fpCliOutput;
            pCliContext->fpCliOutput = pCliContext->fpCliFileWrite;
        }
    }

    pCliContext->mmi_file_flag = TRUE;
    /* Run Script command would have taken the lock. So, unlock here.
     * The following sequence of commands will take the lock each time and
     * then proceed as usual
     * */
    MGMT_UNLOCK ();
    if (i4TempFileFlag == TRUE)
    {
        pCliContext->i4FileFlag = FALSE;
    }
    while ((CliGetLine (i4TempReadFd, pCliContext->ai1CmdBuf)) != CLI_EOF)
    {
        if (pCliContext->i1ClearFlagStatus == OSIX_TRUE)
        {
            pCliContext->mmi_exit_flag = OSIX_TRUE;
        }
        if (pCliContext->mmi_exit_flag)
        {
            break;
        }
#ifdef L2RED_WANTED
        if (RmGetPeerNodeCount () != 0)
        {
            if (u1PeerNodeCount == 0)
            {
                mmi_printf
                    ("\rAborting!!!!!!!! run script since Bulk Update is in progress !!!!!\r\n");
                break;
            }
            else if (RmIsBulkUpdateComplete () == RM_FAILURE)
            {
                mmi_printf
                    ("\rAborting run script since Bulk Update is in progress !!!!!\r\n");
                break;
            }
        }
#endif
        mmi_print_user_prompt (NULL, pCliContext);
        mmi_printf ("%s", pCliContext->ai1CmdBuf);

        if ((CliGetIfRangeMode (&i4Mode) == CLI_SUCCESS) &&
            i4Mode == (INT4) CLI_IF_RANGE_MODE_ENABLE)
        {
            CliExecuteIfRangeCommand (pCliContext,
                                      (CHR1 *) pCliContext->ai1CmdBuf);

        }
        else
        {
            if (CliExecuteCliCmd ((CHR1 *) pCliContext->ai1CmdBuf) ==
                CLI_FAILURE)
            {
                /* Session logged out, 
                 * break the loop without executing remaining commands 
                 */
                pCliContext->mmi_file_flag = FALSE;
                break;
            }
        }
    }
    if (i4TempFileFlag == TRUE)
    {
        pCliContext->i4FileFlag = TRUE;
    }
    pCliContext->i2ScriptRecCount--;
    if (pCliContext->i2ScriptRecCount == 0)
    {
        pCliContext->mmi_file_flag = FALSE;
    }
    if (pu1OutputFile)
    {
        pCliContext->i4OutputFd = i4ContextWriteFd;
        pCliContext->fpCliOutput = pTempOutputFp;
        if (CliCloseFile (i4TempWriteFd) == CLI_FAILURE)
        {
            mmi_printf ("\rUnable to close Ouptut File: %s\r\n", pu1OutputFile);
        }
    }
    if ((i1DefaultMoreFlag == CLI_MORE_ENABLE))
    {
        pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
        CliCreatePipe (pCliContext);
    }

    if (CliCloseFile (i4TempReadFd) == CLI_FAILURE)
    {
        mmi_printf ("\rUnable to close Script File: %s\r\n", pu1CmdScriptFile);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/* API provided for other modules to get the current CLI prompt */
INT1
CliGetCurPrompt (INT1 *pi1CliCurPrompt)
{
    tCliContext        *pCliContext;

    if (pi1CliCurPrompt == NULL)
    {
        return (CLI_FAILURE);
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    STRCPY (pi1CliCurPrompt, pCliContext->Mmi_i1Cur_prompt);
    return CLI_SUCCESS;
}

INT1
CliSetCurMode (INT1 *pi1CliCurPrompt)
{
    tCliContext        *pCliContext;
    UINT4               u4Len = 0;

    if (pi1CliCurPrompt == NULL)
    {
        return (CLI_FAILURE);
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    u4Len = ((STRLEN (pi1CliCurPrompt) < sizeof (pCliContext->ai1ModeName)) ?
             STRLEN (pi1CliCurPrompt) : sizeof (pCliContext->ai1ModeName) - 1);
    STRNCPY (pCliContext->ai1ModeName, pi1CliCurPrompt, u4Len);
    pCliContext->ai1ModeName[u4Len] = '\0';
    return CLI_SUCCESS;
}

INT1
CliSetPoolId (UINT4 u4PoolId)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->u4PoolId = u4PoolId;
    return CLI_SUCCESS;
}

INT1
CliGetPoolId (UINT4 *u4PoolId)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    *u4PoolId = pCliContext->u4PoolId;
    return CLI_SUCCESS;
}

/*****************************************/
INT1
CliSetNatId (UINT4 u4PoolId)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->u4NatPoolId = u4PoolId;
    return CLI_SUCCESS;
}

INT1
CliGetNatId (UINT4 *u4PoolId)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    *u4PoolId = pCliContext->u4NatPoolId;
    return CLI_SUCCESS;
}

/******************************************/

INT1
CliGetCurMode (INT1 *pi1CliCurPrompt)
{
    tCliContext        *pCliContext;

    if (pi1CliCurPrompt == NULL)
    {
        return (CLI_FAILURE);
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    STRCPY (pi1CliCurPrompt, pCliContext->ai1ModeName);
    return CLI_SUCCESS;
}

/* API provided for other modules to set the current Modes prompt info 
 * This may be used by modules which needs to keep track of some index information
 * (like interface index, Dhcp pool config index ...)
 */
INT4
CliSetModeInfo (INT4 i4PromptInfo)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_ERROR);
    }
    if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
    {
        pCliContext->i4PromptInfo = i4PromptInfo;
    }
    return i4PromptInfo;
}

/* API provided for other modules to get the current Modes index info */
INT4
CliGetModeInfo (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_ERROR);
    }
    return pCliContext->i4PromptInfo;
}

/* API provided to set the current context Id
 * This may be used by modules which needs to keep track of context information. */
UINT4
CliSetContextIdInfo (UINT4 u4Context)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return ((UINT4) CLI_ERROR);
    }
    if (gCliSessions.bIsClCliEnabled == OSIX_TRUE)
    {
        pCliContext->u4ContextId = u4Context;
    }
    return u4Context;
}

/* API provided for other modules to get the current Context index info */
UINT4
CliGetContextIdInfo (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return ((UINT4) CLI_ERROR);
    }
    return pCliContext->u4ContextId;
}

/*  Reads a Certificate key as input to the CLI - called by IKE module */
INT1
CliReadLine (INT1 *pi1Prompt, INT1 *pi1Str, UINT4 u4Len)
{
    tCliContext        *pCliContext;
    UINT4               u4Index;

    if (pi1Str == NULL)
    {
        return CLI_FAILURE;
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return CLI_FAILURE;
    }

    if (pCliContext->i1PipeFlag)
    {
        CliDeletePipe (pCliContext);
    }

    if (pi1Prompt != NULL)
    {
        mmi_printf ("\r%s", pi1Prompt);
    }

    pCliContext->i2CursorPos = 0;
    for (u4Index = 0; u4Index < u4Len; u4Index++)
    {
        pi1Str[u4Index] = pCliContext->fpCliInput (pCliContext);

        if (pi1Str[u4Index] == '\n')
        {
            pi1Str[u4Index] = '\0';
            break;
        }

        if ((pi1Str[u4Index] == CLI_FAILURE) || (pi1Str[u4Index] == 0))
        {
            return CLI_FAILURE;
        }

        if (IS_BACKSPACE_CHAR (pi1Str[u4Index]))
        {

            if (u4Index > 0)
            {
                pi1Str[u4Index] = '\0';
                u4Index -= 2;
                pCliContext->i2CursorPos--;
                CliHandleBackspace (pi1Prompt, pCliContext, (UINT1 *) pi1Str,
                                    (UINT2) u4Index);

            }
            else if (u4Index == 0)
            {
                MEMSET (pi1Str, 0, u4Len);
                u4Index--;
            }
            continue;
        }

        if (pCliContext->fpCliOutput (pCliContext,
                                      (CONST CHR1 *) & pi1Str[u4Index],
                                      1) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        pCliContext->i2CursorPos++;
    }

    pi1Str[u4Len] = '\0';
    return CLI_SUCCESS;
}

/************************************************************************
 * This function will split the path into tokens which are basically 
 * interface names, and check whether they are valid. It checks the
 * existence of the interfaces and changes to that mode
 ************************************************************************/
INT4
CliChangePath (CONST CHR1 * pu1Path)
{
    tCliContext        *pCliContext;
    CHR1                ai1Path[MAX_PROMPT_LEN + 1];    /* temporary storage */
    CHR1               *pi1Mode;
    CHR1               *pi1Temp;

    if (!pu1Path)
    {
        CLI_SET_CMD_STATUS (CLI_SUCCESS);
        return CLI_SUCCESS;
    }

    if (STRLEN (pu1Path) > MAX_PROMPT_LEN)
    {
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        return CLI_FAILURE;
    }
    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        return (CLI_FAILURE);
    };

    while (CliIsDelimit (*pu1Path, CLI_EOT_DELIMITS))
        pu1Path++;

    CLI_STRCPY (ai1Path, pu1Path);

    mmicm_copy_curconfig_to_temp (pCliContext);
    if ((CLI_STRCMP (ai1Path, "/") == 0) ||
        (CLI_STRCMP (ai1Path, ".") == 0) || (CLI_STRCMP (ai1Path, "..") == 0))
    {
        if (CliChmod (pCliContext, ai1Path) == CLI_FAILURE)
        {
            mmicm_copy_temp_to_curconfig ();
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            return (CLI_FAILURE);
        }
        CLI_SET_CMD_STATUS (CLI_SUCCESS);
        return (CLI_SUCCESS);
    }

    /* to split the path by '/' and store into stack */

    pi1Mode = ai1Path;
    if (pi1Mode[0] == '/')
    {
        mmicm_copy_root_to_curconfig (pCliContext);
        pi1Mode++;
    }
    do
    {
        pi1Temp = STRCHR (pi1Mode, (int) '/');

        if (pi1Temp != NULL)
        {
            while (*pi1Temp == '/')
            {
                *pi1Temp = '\0';
                pi1Temp++;
            }
        }

        if ((pi1Mode[0] == '/') || (pi1Mode[0] == 0))
        {
            if (CliChmod (pCliContext, "/") == CLI_FAILURE)
            {
                mmicm_copy_temp_to_curconfig ();
                CLI_SET_CMD_STATUS (CLI_FAILURE);
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (CliChmod (pCliContext, pi1Mode) == CLI_FAILURE)
            {
                mmicm_copy_temp_to_curconfig ();
                CLI_SET_CMD_STATUS (CLI_FAILURE);
                return (CLI_FAILURE);
            }
        }
        pi1Mode = pi1Temp;
    }
    while (pi1Temp != NULL && *pi1Temp != '\0');

    CLI_SET_CMD_STATUS (CLI_SUCCESS);
    return (CLI_SUCCESS);
}

/* API provided for other modules to get the current prompt of current mode */

INT4
CliGetCurModePromptStr (INT1 *pi1CliCurPrompt)
{
    tCliContext        *pCliContext;
    INT1               *pi1ptr = NULL;

    if (pi1CliCurPrompt == NULL)
    {
        return (CLI_FAILURE);
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    pi1ptr = (INT1 *) STRRCHR (pCliContext->Mmi_i1Cur_prompt, (int) '/');
    if (pi1ptr == NULL)
    {
        return CLI_FAILURE;
    }

    STRCPY (pi1CliCurPrompt, (pi1ptr + 1));
    return (CLI_SUCCESS);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetContextInfo                                   *
 *                                                                         *
 *     Description   : The function is used by other modules to get        *
 *                     the CLI context parameters.                         *
 *                                                                         *
 *     Input(s)      : pCliParams  : Pointer to CliParams structure        *
 *                                                                         *
 *     Output(s)     : pCliParams  : Copies Cli Context parameters         *
 *                                   to pCliParams structure.              *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE.                            *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetContextInfo (tCliParams * pCliParams)
{
    tCliContext        *pCliContext = NULL;

    if (pCliParams == NULL)
    {
        return CLI_FAILURE;
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return CLI_FAILURE;
    }
#if defined (BSDCOMP_SLI_WANTED) || (defined (SLI_WANTED) && defined (TCP_WANTED) )
    if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
    {
        CliTelnetIsBreak (pCliContext);
    }
#endif
    STRCPY (pCliParams->ai1CliCurPrompt, pCliContext->Mmi_i1Cur_prompt);
    pCliParams->u4Flags = pCliContext->u4Flags;

    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliCommandCompare                                   *
 *                                                                         *
 *     Description   : The function verifies whether the given string      *
 *                     refers to the given command.                        *
 *                                                                         *
 *     Input(s)      : pString     : String to be matched with command     *
 *                     pCommand    : Command to be compared with.          *
 *                     i1SubsetFlag: Flag to indicate if partial match     *
 *                                   (TRUE) or exact match (FALSE) is      *
 *                                   desired.                              *
 *                                                                         *
 *     Output(s)     : None                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE.                            *
 *                                                                         *
 ***************************************************************************/

INT4
CliCommandCompare (CONST UINT1 *pCommand, UINT1 *pString, INT1 i1SubsetFlag,
                   UINT1 *pu1TokenCount)
{
    INT1                i1SpecialCharFlag = 0;
    INT1                i1TokenFlag = 0;
    UINT1               u1TokenCount = 1;
    UINT1               u1EndTmpString = 0;
    UINT1               u1TokenMatch = OSIX_FALSE;
    UINT1               u1TokenStrCount = OSIX_FALSE;
    UINT1               u1IsSplChar = OSIX_FALSE;
    INT1                i1OptionalCharFlag = 0;
    INT1                i1MandatoryCharFlag = 0;
    INT1                i1MandatoryEndFlag = 0;
    INT1                matchfound = OSIX_FALSE;
    CONST UINT1        *pu1TmpCommand = NULL;
    UINT1              *pu1TmpString = NULL;
    UINT1              *pu1TmpString1 = NULL;
    INT4                i4StrPosition = 0;
    INT4                i4StringLen = 0;

    if ((!pString) || (!(STRLEN (pString))))
    {
        return (CLI_FAILURE);
    }

    i4StringLen = STRLEN (pString);
    while (TRUE)
    {
        if ((!*pCommand) || (!*pString))
        {
            break;
        }
        if ((*pCommand != '{') && (*pCommand != '[') &&
            (*pCommand != '|') && (*pCommand != '}') && (*pCommand != ']') &&
            (*pCommand != '<') && (*pCommand != '>'))
        {
            if ((*pString != CLI_SPACE_CHAR) && (*pCommand != CLI_SPACE_CHAR))
            {
                if (TOLOWER (*pString) != TOLOWER (*pCommand))
                {
                    if (i1MandatoryCharFlag == 1)
                    {

                        pu1TmpCommand = pCommand;
                        pu1TmpString = pString;
                        while ((*pu1TmpCommand != CLI_SPACE_CHAR) &&
                               (*pu1TmpCommand != '}'))
                        {
                            pu1TmpCommand++;
                        }
                        pu1TmpCommand++;
                        if ((*pu1TmpCommand == '}')
                            && (matchfound == OSIX_FALSE))
                        {
                            return CLI_FAILURE;
                        }
                        else
                        {
                            if (ISALPHA (*pu1TmpCommand))
                            {
                                return CLI_FAILURE;
                            }
                        }
                        pu1TmpCommand--;

                        matchfound = OSIX_FALSE;
                    }
                    if (i1TokenFlag == 1)
                    {
                        i4StrPosition--;
                        pString--;
                    }
                    if (i1SpecialCharFlag != 0)
                    {
                        if (i1TokenFlag != 1)
                        {
                            pCommand++;
                        }
                        u1TokenStrCount = OSIX_FALSE;
                        continue;
                    }
                    else
                    {
                        return (CLI_FAILURE);
                    }
                }

                if (i1SpecialCharFlag != 0)
                {
                    i1TokenFlag = 1;
                    if (TOLOWER (*pString) == TOLOWER (*pCommand))
                    {

                        pCommand--;
                        if ((*pCommand == ' ') || (*pCommand == '|')
                            || (*pCommand == '{') || (*pCommand == '['))
                        {
                            u1TokenStrCount = OSIX_TRUE;
                        }
                        pCommand++;
                        if (i1MandatoryCharFlag == 1)
                        {
                            pu1TmpCommand = pCommand;
                            pu1TmpString = pString;
                            matchfound = OSIX_TRUE;

                            /* if first character is matching then loop around to 
                             * find whether the entire token is matched */

                            while (matchfound == OSIX_TRUE)
                            {
                                if (TOLOWER (*pu1TmpString) ==
                                    TOLOWER (*pu1TmpCommand))
                                {
                                    pu1TmpCommand++;
                                    pu1TmpString++;
                                    matchfound = OSIX_TRUE;
                                }
                                else
                                {
                                    if (*pu1TmpCommand == ']'
                                        && *pu1TmpString == ' ')
                                    {
                                        pu1TmpString1 = pu1TmpString + 1;
                                        if (!*pu1TmpString1)
                                        {
                                            u1EndTmpString = 1;
                                        }
                                    }
                                    matchfound = OSIX_FALSE;
                                }
                            }

                            if ((*pu1TmpString == CLI_SPACE_CHAR) &&
                                (matchfound == OSIX_TRUE))
                            {
                                /* Token matched up to space in pstring
                                 * so update the pstring and pcommand pointer*/

                                pu1TmpString++;
                                matchfound = OSIX_TRUE;
                                while (*pu1TmpCommand != '}')
                                {
                                    pu1TmpCommand++;
                                }
                                pu1TmpCommand++;
                                i1MandatoryCharFlag--;
                                i1MandatoryEndFlag--;
                                pCommand = pu1TmpCommand;
                                pString = pu1TmpString;
                            }
                            else
                            {
                                matchfound = OSIX_FALSE;
                            }
                        }
                    }
                }
                else
                {
                    i1TokenFlag = 0;
                    if (i1MandatoryEndFlag == 1)
                    {
                        if (matchfound == OSIX_FALSE)
                        {
                            /* If user inputs ab d? and comparision has reached last token d
                             * in the command ab {cgh | efgh} d , in this case cgh or efgh are
                             * mandatory tokens so failure should be returned for this command 
                             * since ab d? is not matched*/

                            return CLI_FAILURE;
                        }
                    }
                }
                if (u1TokenStrCount == OSIX_TRUE)
                {
                    u1TokenMatch = OSIX_TRUE;
                }
                i4StrPosition++;
                if (i4StrPosition >= (INT4) (i4StringLen - 1))
                {
                    return CLI_SUCCESS;
                }
                pString++;
            }
        }
        if ((*pCommand == '{') || (*pCommand == '['))
        {
            i1SpecialCharFlag++;
            i1TokenFlag = 1;
            u1IsSplChar = OSIX_TRUE;
            u1TokenStrCount = OSIX_TRUE;
            if (*pCommand == '[')
            {
                i1OptionalCharFlag++;
            }
            /* increment mandatorychar flag if the command is like 
             * dx {a | b} if the tokens are optional mandatorychar flag should 
             * not be incremented example : dx[{a | b}]*/

            if ((i1OptionalCharFlag == 0) && (*pCommand == '{'))
            {
                i1MandatoryCharFlag++;

                /* To Handle debug {mpls oam {critical .... match not found in first set of
                 * mandatory tokens*/

                if ((i1MandatoryCharFlag == 2) && (matchfound == OSIX_FALSE))
                {
                    return CLI_FAILURE;
                }

            }
        }
        else if ((*pCommand == '}') || (*pCommand == ']'))
        {
            i1SpecialCharFlag--;
            u1TokenStrCount = OSIX_FALSE;
            if (*pCommand == ']')
            {
                i1OptionalCharFlag--;
            }

            if ((i1MandatoryCharFlag == 1) && (*pCommand == '}'))
            {
                i1MandatoryCharFlag--;
                i1MandatoryEndFlag++;
                if (matchfound == OSIX_FALSE)
                {
                    return CLI_FAILURE;
                }
            }
            if (i1SpecialCharFlag == 0)
            {
                pCommand++;
                u1IsSplChar = OSIX_FALSE;
                continue;
            }
        }

        pCommand++;
        if ((*pString == CLI_SPACE_CHAR))
        {
            if ((*pCommand == CLI_SPACE_CHAR)
                && (i1SubsetFlag == CLI_MATCH_CXT_TOKEN))
            {
                if (i1SpecialCharFlag != 0)
                {
                    if (u1TokenStrCount == OSIX_TRUE)
                    {
                        OSIX_BITLIST_SET_BIT (pu1TokenCount, u1TokenCount,
                                              CLI_CXT_TKN_LIST_SIZE);
                        u1TokenCount++;
                    }
                }
                else
                {
                    OSIX_BITLIST_SET_BIT (pu1TokenCount, u1TokenCount,
                                          CLI_CXT_TKN_LIST_SIZE);
                    u1TokenCount++;
                }
            }
            else if ((*pCommand == '\0')
                     && (i1SubsetFlag == CLI_MATCH_CXT_TOKEN))
            {
                OSIX_BITLIST_SET_BIT (pu1TokenCount, u1TokenCount,
                                      CLI_CXT_TKN_LIST_SIZE);
                u1TokenCount++;
            }
            else if (((*pCommand == ']') || (*pCommand == '}'))
                     && (i1SubsetFlag == CLI_MATCH_CXT_TOKEN))
            {
                i1SpecialCharFlag--;
                u1TokenStrCount = OSIX_FALSE;
                if (u1EndTmpString == 1)
                {
                    u1TokenStrCount = OSIX_TRUE;
                }

                if (*pCommand == ']')
                {
                    i1OptionalCharFlag--;
                }

                if ((i1MandatoryCharFlag == 1) && (*pCommand == '}'))
                {
                    i1MandatoryCharFlag--;
                    i1MandatoryEndFlag++;
                    if (matchfound == OSIX_FALSE)
                    {
                        return CLI_FAILURE;
                    }
                }
                if (i1SpecialCharFlag == 0)
                {
                    pCommand++;
                    u1IsSplChar = OSIX_FALSE;
                }
                OSIX_BITLIST_SET_BIT (pu1TokenCount, u1TokenCount,
                                      CLI_CXT_TKN_LIST_SIZE);
                u1TokenCount++;
            }

            while ((*pCommand != CLI_SPACE_CHAR) && (STRLEN (pCommand) != 0)
                   && (*pCommand != ']') && (*pCommand != '}'))
            {

                pCommand++;
            }
        }
        while (*pString == CLI_SPACE_CHAR)
        {
            i4StrPosition++;
            if (i4StrPosition >= (INT4) (i4StringLen - 1))
            {
                return CLI_SUCCESS;
            }
            pString++;
        }

        if (STRCMP (pString, CLI_AMBIG_INTERFACE_TOKEN_FA) == 0)
        {
            if ((STRSTR (pCommand, CLI_AMBIG_MATCH_TOKEN_ONE) != NULL) ||
                (STRSTR (pCommand, CLI_AMBIG_MATCH_TOKEN_TWO) != NULL))
            {
                return (CLI_FAILURE);
            }
        }

        else if ((STRCMP (pString, CLI_AMBIG_INTERFACE_TOKEN_EX) == 0) ||
                 (STRCMP (pString, CLI_AMBIG_INTERFACE_TOKEN_EXT) == 0))
        {
            if (STRSTR (pCommand, CLI_AMBIG_MATCH_TOKEN_THREE) != NULL)
            {
                return (CLI_FAILURE);
            }
        }

        while (*pCommand == CLI_SPACE_CHAR)
        {
            pCommand++;
        }
    }

    if (!*pString)
    {
        if (((!*pCommand) || (i1SubsetFlag != CLI_MATCH_EXACT_TOKEN)) &&
            (((u1IsSplChar == OSIX_TRUE) && (u1TokenMatch == OSIX_TRUE)
              && (u1TokenStrCount == OSIX_TRUE))
             || (u1IsSplChar == OSIX_FALSE)))
        {
            return (CLI_SUCCESS);
        }
    }

    return (CLI_FAILURE);
}

INT4
CliCommandMatch (CONST UINT1 *pCommand, UINT1 *pString)
{
    return (CliCommandCompare (pCommand, pString, CLI_MATCH_EXACT_TOKEN, NULL));
}

INT4
CliCommandMatchSubset (CONST UINT1 *pCommand, UINT1 *pString)
{
    return (CliCommandCompare
            (pCommand, pString, CLI_MATCH_SUBSET_TOKEN, NULL));
}

INT4
CliCxtTokenMatch (CONST UINT1 *pCommand, UINT1 *pString, UINT1 *pu1TokenCount)
{
    return (CliCommandCompare
            (pCommand, pString, CLI_MATCH_CXT_TOKEN, pu1TokenCount));
}

INT1
CliSplitTimeToken (INT1 *pi1Token, UINT1 *pu1Hrs, UINT1 *pu1Mins,
                   UINT1 *pu1Secs)
{
    INT1               *pi1Temp = NULL;
    INT1                i1RetStatus = CLI_FAILURE;

    if ((pi1Token) && ((pi1Temp = (INT1 *) CLI_STRSTR (pi1Token, ":")) != NULL))
    {
        *pi1Temp = '\0';
        *pu1Hrs = (UINT1) CLI_ATOI (pi1Token);
        *pi1Temp = ':';

        pi1Token = pi1Temp + 1;
        if ((pi1Temp = (INT1 *) CLI_STRSTR (pi1Token, ":")) != NULL)
        {
            *pi1Temp = '\0';
            *pu1Mins = (UINT1) CLI_ATOI (pi1Token);
            *pi1Temp = ':';

            pi1Token = pi1Temp + 1;
            *pu1Secs = (UINT1) CLI_ATOI (pi1Token);
            i1RetStatus = CLI_SUCCESS;
        }
    }
    return i1RetStatus;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : CliOctetToIfName                                   */
/*                                                                       */
/* DESCRIPTION      : This function converts octet to port and prints    */
/*                    the Interface name                                 */
/*                                                                       */
/* INPUT            : pu1FirstLine     - First Line Message              */
/*                    pOctet           - Ports as octet list             */
/*                    u4OctetLen       - Length of port list             */
/*                    u4MaxPorts       - Max ports                       */
/*                    u4MaxLen         - Max size of the octet list      */
/*                    u1Flag           - CLI_FALSE/CLI_TRUE              */
/*                                       CLI_TRUE should be given        */
/*                                       if user knows EXACTLY what is   */
/*                                       the pu1FirstLine. If this is    */
/*                                       not known then pass CLI_FALSE   */
/*                                       as well as what is the column   */
/*                                       position on which printing      */
/*                                       should proceed                  */
/*                    u1Column         - starting location of printing   */
/*                    u4MaxPorts       - Max ports                       */
/*                    pu4PagingStatus  - Paging Status                   */
/*                    u1MaxPortsPerLine- Maximum Ports to be printed in  */
/*                                       a line                          */
/* OUTPUT           : CliHandle - Contains error messages                */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
CliOctetToIfName (tCliHandle CliHandle, CHR1 * pu1FirstLine,
                  tSNMP_OCTET_STRING_TYPE * pOctetStr,
                  UINT4 u4MaxPorts, UINT4 u4MaxLen,
                  UINT1 u1Column, UINT4 *pu4PagingStatus,
                  UINT1 u1MaxPortsPerLine)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = CLI_FALSE;
    UINT4               u4Port = 0;
    UINT2               u2Len = 0;
    UINT1               u1Counter = 0;
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1NewLineFlag = CLI_FALSE;

    if (pOctetStr->i4_Length > (INT4) u4MaxLen)
    {
        CliPrintf (CliHandle, "%% Octet Length is too big\r\n");
        return CLI_FAILURE;
    }

    if (pu1FirstLine != NULL)
    {
        u1Flag = CLI_TRUE;
    }

    /* This portion is used if the PortList is EMPTY */
    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    /* This check is introduced to avoid possible array overrun when
       pOctetStr->i4_Length exceeds CLI_MAX_COLS */

    if (pOctetStr->i4_Length > CLI_MAX_COLS)
    {
        for (u2Len = 0; u2Len < pOctetStr->i4_Length; u2Len++)
        {
            if (pOctetStr->pu1_OctetList[u2Len] != '\0')
            {
                break;
            }
        }
        if (u2Len == pOctetStr->i4_Length)
        {
            if (u1Flag == CLI_TRUE)
            {
                *pu4PagingStatus = CliPrintf (CliHandle, "%s", pu1FirstLine);
            }
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }
    }

    else if (MEMCMP (pOctetStr->pu1_OctetList,
                     au1BlankSpace, MEM_MAX_BYTES (pOctetStr->i4_Length,
                                                   CLI_MAX_COLS)) == 0)
    {
        if (u1Flag == CLI_TRUE)
        {
            *pu4PagingStatus = CliPrintf (CliHandle, "%s", pu1FirstLine);
        }
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    /* u1Flag should be CLI_TRUE only if user knows exactly what to be
     * printed as first line pu1FirstLine before PortList
     *
     * if printing is going to happen somewhere in the middle then 
     * u1Flag should be CLI_FALSE with proper u1Column value
     */
    if (u1Flag == CLI_TRUE)
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together. 
         * Say for u1MaxPortsPerLine = 4 , it takes 31(4*8 -1) spaces 
         * for "Gi0/11, Gi0/12, Gi0/13, Gi0/14" 
         */
        if (((UINT1) STRLEN (pu1FirstLine)) >=
            (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% First Line is too big\r\n");
            return CLI_FAILURE;
        }

        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20,
                MEM_MAX_BYTES (sizeof (au1BlankSpace), STRLEN (pu1FirstLine)));

        au1BlankSpace[MEM_MAX_BYTES
                      (((sizeof (au1BlankSpace)) - 1), STRLEN (pu1FirstLine))] =
            '\0';
    }
    else
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together. it 
         * takes 31 spaces "Gi0/11, Gi0/12, Gi0/13, Gi0/14" 
         */
        if (u1Column >= (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% Column position is out of range\r\n");
            return CLI_FAILURE;
        }
        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20, u1Column);
        au1BlankSpace[u1Column] = '\0';
    }

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 pOctetStr->i4_Length, u4Port) == OSIX_SUCCESS)
            {
                if (CfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);

                    return CLI_FAILURE;
                }

                if (u1Counter == 0)
                {
                    if (u1Flag == CLI_TRUE)
                    {
                        *pu4PagingStatus = CliPrintf (CliHandle,
                                                      "%s %s",
                                                      pu1FirstLine, au1NameStr);
                    }
                    else
                    {
                        /* The first line alone needs to be 
                         * printed like this. from next line
                         * onwards follow the same flow of 
                         * known printing path
                         */
                        *pu4PagingStatus = CliPrintf (CliHandle,
                                                      " %s", au1NameStr);
                        u1Flag = CLI_TRUE;

                    }
                }
                else
                {
                    *pu4PagingStatus = CliPrintf (CliHandle,
                                                  ", %s", au1NameStr);
                }
                u1Counter++;
                u1NewLineFlag = CLI_FALSE;

                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u1MaxPortsPerLine)
                {
                    CliPrintf (CliHandle, "\r\n");
                    pu1FirstLine = (CHR1 *) au1BlankSpace;
                    u1NewLineFlag = CLI_TRUE;
                    u1Counter = 0;
                }
            }
        }
    }

    /* Ensure a new line is printed at the end if new line is not
     * printed while coming out
     */
    if (u1NewLineFlag == CLI_FALSE)
    {
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : CliOctetToMirrorList                               */
/*                                                                       */
/* DESCRIPTION      : This function converts octet to port and prints    */
/*                    the Interface name                                 */
/*                                                                       */
/* INPUT            : pu1FirstLine     - First Line Message              */
/*                    pOctet           - Ports as octet list             */
/*                    u4OctetLen       - Length of port list             */
/*                    u4MaxPorts       - Max ports                       */
/*                    u4MaxLen         - Max size of the octet list      */
/*                    u1Flag           - CLI_FALSE/CLI_TRUE              */
/*                                       CLI_TRUE should be given        */
/*                                       if user knows EXACTLY what is   */
/*                                       the pu1FirstLine. If this is    */
/*                                       not known then pass CLI_FALSE   */
/*                                       as well as what is the column   */
/*                                       position on which printing      */
/*                                       should proceed                  */
/*                    u1Column         - starting location of printing   */
/*                    u4MaxPorts       - Max ports                       */
/*                    pu4PagingStatus  - Paging Status                   */
/*                    u1MaxPortsPerLine- Maximum Ports to be printed in  */
/*                                       a line                          */
/* OUTPUT           : CliHandle - Contains error messages                */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
CliOctetToMirrorList (tCliHandle CliHandle, CHR1 * pu1FirstLine,
                      tSNMP_OCTET_STRING_TYPE * pOctetStr,
                      UINT4 u4MaxPorts, UINT4 u4MaxLen,
                      UINT1 u1Column, UINT4 *pu4PagingStatus,
                      UINT1 u1MaxPortsPerLine)
{
    UINT1               u1Flag = CLI_FALSE;
    UINT4               u4Port = 0;
    UINT1               u1Counter = 0;
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1NewLineFlag = CLI_FALSE;

    if (pOctetStr->i4_Length > (INT4) u4MaxLen)
    {
        CliPrintf (CliHandle, "%% Octet Length is too big\r\n");
        return CLI_FAILURE;
    }

    if (pu1FirstLine != NULL)
    {
        u1Flag = CLI_TRUE;
    }

    /* This portion is used if the PortList is EMPTY */
    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    if (MEMCMP (pOctetStr->pu1_OctetList,
                au1BlankSpace, pOctetStr->i4_Length) == 0)
    {
        if (u1Flag == CLI_TRUE)
        {
            *pu4PagingStatus = CliPrintf (CliHandle, "%s", pu1FirstLine);
        }
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    /* u1Flag should be CLI_TRUE only if user knows exactly what to be
     * printed as first line pu1FirstLine before PortList
     *
     * if printing is going to happen somewhere in the middle then 
     * u1Flag should be CLI_FALSE with proper u1Column value
     */
    if (u1Flag == CLI_TRUE)
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together. 
         * Say for u1MaxPortsPerLine = 4 , it takes 31(4*8 -1) spaces 
         * for "Gi0/11, Gi0/12, Gi0/13, Gi0/14" 
         */
        if (((UINT1) STRLEN (pu1FirstLine)) >=
            (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% First Line is too big\r\n");
            return CLI_FAILURE;
        }

        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20, STRLEN (pu1FirstLine));
        au1BlankSpace[STRLEN (pu1FirstLine)] = '\0';
    }
    else
    {
        /* Give minimum space for printing u1MaxPortsPerLine ports together. it 
         * takes 31 spaces "Gi0/11, Gi0/12, Gi0/13, Gi0/14" 
         */
        if (u1Column >= (CLI_MAX_COLS - (u1MaxPortsPerLine * 8) - 1))
        {
            CliPrintf (CliHandle, "%% Column position is out of range\r\n");
            return CLI_FAILURE;
        }
        /* 0x20 is Ascii code for SPACE */
        MEMSET (au1BlankSpace, 0x20, u1Column);
        au1BlankSpace[u1Column] = '\0';
    }

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 pOctetStr->i4_Length, u4Port) == OSIX_SUCCESS)
            {
                if (u1Counter == 0)
                {
                    if (u1Flag == CLI_TRUE)
                    {
                        *pu4PagingStatus = CliPrintf (CliHandle,
                                                      "%s %d",
                                                      pu1FirstLine, u4Port);
                    }
                    else
                    {
                        /* The first line alone needs to be 
                         * printed like this. from next line
                         * onwards follow the same flow of 
                         * known printing path
                         */
                        *pu4PagingStatus = CliPrintf (CliHandle, " %d", u4Port);
                        u1Flag = CLI_TRUE;

                    }
                }
                else
                {
                    *pu4PagingStatus = CliPrintf (CliHandle, ", %d", u4Port);
                }
                u1Counter++;
                u1NewLineFlag = CLI_FALSE;

                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u1MaxPortsPerLine)
                {
                    CliPrintf (CliHandle, "\r\n");
                    pu1FirstLine = (CHR1 *) au1BlankSpace;
                    u1NewLineFlag = CLI_TRUE;
                    u1Counter = 0;
                }
            }
        }
    }

    /* Ensure a new line is printed at the end if new line is not
     * printed while coming out
     */
    if (u1NewLineFlag == CLI_FALSE)
    {
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : CliOctetPortChannelToIfName                                */
/*                                                                       */
/* DESCRIPTION      : This function converts octet to port and prints    */
/*                    the Interface name for IGS                         */
/*                                                                       */
/* INPUT            : pu1FirstLine     - First Line Message              */
/*                    pOctet           - Ports as octet list             */
/*                    u4OctetLen       - Length of port list             */
/*                    u4MaxPorts       - Max ports                       */
/*                    u4MaxLen         - Max size of the octet list      */
/*                    u1Flag           - CLI_FALSE/CLI_TRUE              */
/*                                       CLI_TRUE should be given        */
/*                                       if user knows EXACTLY what is   */
/*                                       the pu1FirstLine. If this is    */
/*                                       not known then pass CLI_FALSE   */
/*                                       as well as what is the column   */
/*                                       position on which printing      */
/*                                       should proceed                  */
/*                    u1Column         - starting location of printing   */
/*                    u4MaxPorts       - Max ports                       */
/*                    pu4PagingStatus  - Paging Status                   */
/*                    u1MaxPortsPerLine- Maximum Ports to be printed in  */
/*                                       a line                          */
/* OUTPUT           : CliHandle - Contains error messages                */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
CliOctetPortChannelToIfName (tCliHandle CliHandle,
                             tSNMP_OCTET_STRING_TYPE * pOctetStr,
                             UINT4 u4MaxPorts, UINT4 u4MaxLen,
                             UINT4 *pu4PagingStatus)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Port = 0;
    UINT1               u1Counter = 0;
    INT4                i4PortNum = 0;
    INT4                i4SlotNum = 0;

    if (pOctetStr->i4_Length > (INT4) u4MaxLen)
    {
        CliPrintf (CliHandle, "%% Octet Length is too big\r\n");
        return CLI_FAILURE;
    }

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= u4MaxPorts; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 pOctetStr->i4_Length, u4Port) == OSIX_SUCCESS)
            {
                if (CfaCliConfGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);

                    return CLI_FAILURE;
                }

                if (u1Counter == 0)
                {
                    *pu4PagingStatus = CliPrintf (CliHandle, " %s", au1NameStr);
                }
                else
                {
                    CfaGetSlotAndPortFromIfIndex (u4Port, &i4SlotNum,
                                                  &i4PortNum);

                    if (u4Port <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
                    {
                        *pu4PagingStatus =
                            CliPrintf (CliHandle, ",%d/%d", i4SlotNum,
                                       i4PortNum);
                    }
                    else if (u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES)
                    {
                        *pu4PagingStatus =
                            CliPrintf (CliHandle, ",%d", i4PortNum);
                    }
                }
                u1Counter++;
                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u4MaxPorts)
                {
                    u1Counter = 0;
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : CliConfOctetToIfName                               */
/*                                                                       */
/* DESCRIPTION      : This function converts octet to port and prints    */
/*                    the Interface name                                 */
/*                                                                       */
/* INPUT            : pu1FirstLine     - First Line Message              */
/*                    pu1DefaultStr    - Default string                  */
/*                    pOctet           - Ports as octet list             */
/*                    u4MaxPorts       - Max ports                       */
/*                    u4MaxLen         - Max size of the octet list      */
/*                    pu4PagingStatus  - Paging Status                   */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
CliConfOctetToIfName (tCliHandle CliHandle,
                      CHR1 * pu1FirstLine,
                      CHR1 * pu1DefaultStr,
                      tSNMP_OCTET_STRING_TYPE * pOctetStr,
                      UINT4 *pu4PagingStatus)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1NameOldStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ConfStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ConfPort[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PrevConfPort[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ConfStartPort[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1CmdStr[CLI_MAX_CMD_LENGTH * 2];
    UINT1               au1TempStr[CLI_MAX_CMD_LENGTH];
    UINT4               u4Port = 0;
    UINT1               u1Counter = 0;
    UINT1               u1Offset = 0;
    INT4                countnousCount = -1;
    INT4                i4More = 0;
    UINT4               u4CopyPort = 2;
    UINT4               u4Len = 0;

#ifdef HVPLS_WANTED
    if (pOctetStr->i4_Length > BRG_PORT_LIST_SIZE_EXT)
    {
        return CLI_FAILURE;
    }
#else
    if (pOctetStr->i4_Length > BRG_PORT_LIST_SIZE)
    {
        return CLI_FAILURE;
    }
#endif
    MEMSET (au1CmdStr, 0, CLI_MAX_CMD_LENGTH * 2);
    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1ConfStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1TempStr, 0, CLI_MAX_CMD_LENGTH);
    MEMSET (au1PrevConfPort, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (FsUtilBitListIsAllZeros (pOctetStr->pu1_OctetList,
                                 pOctetStr->i4_Length) == OSIX_TRUE)
    {
        return CLI_FAILURE;
    }

    if (pOctetStr->pu1_OctetList)
    {
        for (u4Port = 1; u4Port <= BRG_MAX_PHY_PLUS_LAG_INT_PORTS; u4Port++)
        {
            if (CliIsMemberPort (pOctetStr->pu1_OctetList,
                                 pOctetStr->i4_Length, u4Port) == OSIX_SUCCESS)
            {
                if (countnousCount == (MBSM_MAX_POSSIBLE_PORTS_PER_SLOT - 1))
                {
                    if (CfaIsPhysicalInterface (u4Port - 1) == CFA_TRUE)
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");

                        u4CopyPort = 2;
                        if (*(au1ConfPort + u4CopyPort) == '/')
                        {
                            u4CopyPort++;
                        }
                        STRNCAT (au1CmdStr, au1ConfPort + u4CopyPort,
                                 (STRLEN (au1ConfPort)) + u4CopyPort);
                    }
                    else
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");
                        STRCAT (au1CmdStr, au1ConfPort);
                    }
                    countnousCount = -1;
                    i4More++;

                }
                countnousCount++;
                if (CfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                    CLI_FAILURE)
                {

                    return CLI_FAILURE;
                }
                /*Get first two char of interface name. e.g. "gi","po" */
                STRNCPY (au1ConfStr, au1NameStr, 2);
                au1ConfStr[0] = (UINT1) TOLOWER (au1ConfStr[0]);
                au1ConfStr[2] = '\0';

#ifdef CFA_UNIQUE_INTF_NAME
                if (STRCMP (au1ConfStr, "et") == 0)
                {
                    STRCPY (au1ConfStr, "ethernet");
                }
#else
                if (STRCMP (au1ConfStr, "gi") == 0)
                {
                    STRCPY (au1ConfStr, "gigabitethernet");
                }
#endif
                else if (STRCMP (au1ConfStr, "ex") == 0)
                {
                    STRCPY (au1ConfStr, "extreme-ethernet");
                }
                else if (STRCMP (au1ConfStr, "fa") == 0)
                {
                    STRCPY (au1ConfStr, "fastethernet");
                }
                else if (STRCMP (au1ConfStr, "vi") == 0)
                {
                    STRCPY (au1ConfStr, "virtual");
                }
                else if (STRCMP (au1ConfStr, "si") == 0)
                {
                    STRCPY (au1ConfStr, "sisp");
                }
                else if (STRCMP (au1ConfStr, "pw") == 0)
                {
                    STRCPY (au1ConfStr, "pw");
                }
                else if (STRCMP (au1ConfStr, "ac") == 0)
                {
                    STRCPY (au1ConfStr, "ac");
                }
                else if (STRCMP (au1ConfStr, "xl") == 0)
                {
                    STRCPY (au1ConfStr, "Xl");
                }

#ifdef VXLAN_WANTED
                else if (STRCMP (au1ConfStr, "nv") == 0)
                {
                    STRCPY (au1ConfStr, "nve");
                }
#endif
                else if (STRCMP (au1ConfStr, "s-") == 0)
                {
                    STRCPY (au1ConfStr, "s-channel");
                }

#ifdef VCPEMGR_WANTED
                else if (STRCMP (au1ConfStr, "ta") == 0)
                {
                    STRCPY (au1ConfStr, "tap");
                }
#endif

                else
                {
                    STRCPY (au1ConfStr, "port-channel");
                }

                /*copy rest of string as port no. e.g 0/2,0/3 */
                u1Offset = 2;
                if (STRCMP (au1ConfStr, "virtual") == 0)
                {
                    u1Offset = 7;
                }
                else if (STRCMP (au1ConfStr, "sisp") == 0)
                {
                    u1Offset = 4;
                }
#ifdef VXLAN_WANTED
                else if (STRCMP (au1ConfStr, "nve") == 0)
                {
                    u1Offset = 3;
                }
#endif
                if (STRCMP (au1ConfStr, "s-channel") == 0)
                {

                    u1Offset = 9;
                }

#ifdef VCPEMGR_WANTED
                else if (STRCMP (au1ConfStr, "tap") == 0)
                {
                    u1Offset = 3;
                }
#endif
                u4Len =
                    ((STRLEN (au1NameStr + u1Offset) <
                      sizeof (au1ConfPort)) ? STRLEN (au1NameStr +
                                                      u1Offset) :
                     sizeof (au1ConfPort) - 1);
                STRNCPY (au1ConfPort, (au1NameStr + u1Offset), u4Len);
                au1ConfPort[u4Len] = '\0';
                /*remember starting port for countinous ports */
                if (countnousCount == 0)
                {
                    STRCPY (au1ConfStartPort, au1ConfPort);
                }
                if (u1Counter == 0)
                {
                    if (pu1FirstLine != NULL)
                    {
                        if (au1ConfStr[0] != '\0')
                        {
                            /*print with firstline */
                            u4Len =
                                (UINT4) ((sizeof (au1CmdStr) -
                                          STRLEN (au1CmdStr) - 1) <
                                         STRLEN (pu1FirstLine)
                                         ? (sizeof (au1CmdStr) -
                                            STRLEN (au1CmdStr) -
                                            1) : STRLEN (pu1FirstLine));

                            STRNCAT (au1CmdStr, pu1FirstLine, u4Len);
                            STRCAT (au1CmdStr, au1ConfStr);
                            STRCAT (au1CmdStr, " ");
                        }
                    }
                    else
                    {
                        /*print without firstline */
                        STRCAT (au1CmdStr, " ");
                        STRCAT (au1CmdStr, au1ConfStr);
                        STRCAT (au1CmdStr, " ");
                    }
                    /*remember the interface type */
                    STRCPY (au1NameOldStr, au1ConfStr);
                }
                if (STRCMP (au1NameOldStr, au1ConfStr) != 0)
                {
                    if (countnousCount > 1)
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");
                        STRNCAT (au1CmdStr, (au1PrevConfPort + 2),
                                 ((STRLEN (au1PrevConfPort)) + 2));
                        STRCPY (au1ConfStartPort, au1ConfPort);
                    }
                    else if (countnousCount == 1)
                    {
                        STRCAT (au1CmdStr, au1PrevConfPort);
                        STRCPY (au1ConfStartPort, au1ConfPort);
                    }

                    STRCAT (au1CmdStr, " ");
                    STRCAT (au1CmdStr, au1ConfStr);
                    STRCAT (au1CmdStr, " ");
                    STRCPY (au1NameOldStr, au1ConfStr);
                    countnousCount = 0;
                    i4More = 0;
                }

                if (i4More == 1)
                {
                    STRCAT (au1CmdStr, ",");
                    i4More = 0;
                }

                u1Counter++;

                if ((u4Port == BRG_MAX_PHY_PLUS_LAG_INT_PORTS) ||
                    ((CfaIsPhysicalInterface (u4Port) == CFA_TRUE) &&
                     (CfaIsPhysicalInterface (u4Port + 1) == CFA_FALSE)))
                {
                    if (countnousCount == 0)
                    {
                        STRCAT (au1CmdStr, au1ConfPort);
                    }
                    if (countnousCount > 0)
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");
                        STRNCAT (au1CmdStr, au1ConfPort + 2,
                                 (STRLEN (au1ConfPort)) + 2);
                    }
                    countnousCount = -1;
                    i4More = 0;
                }

                if ((CfaIsLaggInterface (u4Port) == CFA_TRUE) ||
                    (CfaIsSispInterface (u4Port) == CFA_TRUE))
                {
                    STRCAT (au1CmdStr, au1ConfPort);
                    i4More++;
                    countnousCount = -1;
                }

                STRCPY (au1PrevConfPort, au1ConfPort);

            }
            else
            {
                if (countnousCount == 0)
                {
                    STRCAT (au1CmdStr, au1ConfPort);
                    i4More++;
                    countnousCount = -1;
                }
                if (countnousCount > 0)
                {
                    if (CfaIsPhysicalInterface (u4Port - 1) == CFA_TRUE)
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");

                        u4CopyPort = 2;
                        if (*(au1ConfPort + u4CopyPort) == '/')
                        {
                            u4CopyPort++;
                        }
                        STRNCAT (au1CmdStr, au1ConfPort + u4CopyPort,
                                 (STRLEN (au1ConfPort)) + u4CopyPort);
                    }
                    else
                    {
                        STRCAT (au1CmdStr, au1ConfStartPort);
                        STRCAT (au1CmdStr, "-");
                        STRCAT (au1CmdStr, au1ConfPort);
                    }

                    countnousCount = -1;
                    i4More++;
                }
            }
        }
    }
    if (pu1DefaultStr != NULL)
    {
        if (pu1FirstLine != NULL)
        {
            u4Len =
                (UINT4) ((sizeof (au1TempStr) - STRLEN (au1TempStr) - 1) <
                         STRLEN (pu1FirstLine) ?
                         (sizeof (au1TempStr) - STRLEN (au1TempStr) -
                          1) : STRLEN (pu1FirstLine));
            STRNCAT (au1TempStr, pu1FirstLine, u4Len);
        }

        u4Len =
            (UINT4) ((sizeof (au1TempStr) - STRLEN (au1TempStr) - 1) <
                     STRLEN (pu1DefaultStr) ?
                     (sizeof (au1TempStr) - STRLEN (au1TempStr) -
                      1) : STRLEN (pu1DefaultStr));
        STRNCAT (au1TempStr, pu1DefaultStr, u4Len);
        if (STRCMP (au1CmdStr, au1TempStr) != 0)
        {
            *pu4PagingStatus = CliPrintf (CliHandle, "%s", au1CmdStr);
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "%s", au1CmdStr);
    }
    return CLI_SUCCESS;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : CliCheckFormat                                     */
/*                                                                       */
/* DESCRIPTION      : This function checks how many times ':'            */
/*                    exists in given input                              */
/*                                                                       */
/* INPUT            : pi1Buf     - Input Line whose format needs to be   */
/*                                 Checked                               */
/*                    i4Count    - No. of times ':' is supposed to be    */
/*                                 in the input line                     */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT1
CliCheckFormat (INT1 *pi1Buf, INT4 i4Count)
{
    INT4                i4TempCount = 0;
    INT1               *pi1TempBuf = pi1Buf;

    while (*pi1TempBuf != '\0')
    {
        if (*pi1TempBuf == ':')
        {
            i4TempCount++;
        }
        pi1TempBuf++;
    }
    if (i4TempCount == i4Count)
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/* This function is used to get the Current CliContext's ErrorCode object parameter
 */
INT4
CliGetErrorCode (UINT4 *pu4ErrCode)
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Taks Fonud */
        return CLI_FAILURE;
    }
    *pu4ErrCode = pCliContext->u4ErrorCode;
    return CLI_SUCCESS;
}

/* This function is used to set the Current CliContext's ErrorCode object parameter
 */
INT4
CliSetErrorCode (UINT4 u4ErrCode)
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Taks Fonud */
        return CLI_FAILURE;
    }
    pCliContext->u4ErrorCode = u4ErrCode;
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetCmdStatus                                     *
 *                                                                         *
 *     Description   : The function is used to get the command status from *
 *                     pCliContext                                         *
 *                                                                         *
 *     Input(s)      : pu4CmdStatus - pointer  to value of command status  *                               
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE.                            *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetCmdStatus (UINT4 *pu4CmdStatus)
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Fonud */
        return CLI_FAILURE;
    }
    *pu4CmdStatus = pCliContext->u4CmdStatus;
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetCmdStatus                                    *
 *                                                                         *
 *     Description   : The function is used to set the command return      *
 *                     status                                              *
 *                                                                         *
 *     Input(s)      : u4CmdStatus    : Command Status
            (CLI_SUCCESS/CLI_FAILURE)                          *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE.                            *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetCmdStatus (UINT4 u4CmdStatus)
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Fonud */
        return CLI_FAILURE;
    }
    pCliContext->u4CmdStatus = u4CmdStatus;
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : ClieStrToPortListExt                                *
 *                                                                         *
 *     Description   : The function is used by other modules to            *
 *                     convert the given Attachement circuit interfce      *
 *                     string to port list.                                *
 *                                                                         *
 *     Input(s)      : pu1Str         : Pointer to the string.             *
 *                     pu1PortArray   : Pointer to the string in which the *
 *                                      bits for the port list will be set *
 *                     u4PortArrayLen : Array Length.                      *
 *                     u4IfType       : Interface Type                     *
 *                     u4SubType      : Interface Sub Type                 *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
CliStrToPortListExt (UINT1 *pu1Str, UINT1 *pu1PortArray,
                     UINT4 u4PortArrayLen, UINT4 u4IfType, UINT4 u4SubType)
{
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT1                ai1TempArr[15];
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetType = 0;
    UINT4               u4Val1 = 0;
    UINT4               u4Val2 = 0;
    UINT4               u4RetPortNum = 0;
    INT1                i1EndFlag = OSIX_FALSE;

    if (!(pi1Temp))
    {
        i4RetStatus = OSIX_FAILURE;
    }

    while ((i4RetStatus != OSIX_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDPORT_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (!(*pi1Pos))
            i1EndFlag = OSIX_TRUE;

        *pi1Pos = '\0';

        /* Get values between the list seperaters based on the type */
        i4RetType = CliGetVal (&CliPortList, pi1Temp);

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                u4Val1 = CliPortList.uPortList.u4PortNum;
                u4Val2 = CliPortList.uPortList.u4PortNum;
                break;
            case CLI_LIST_TYPE_RANGE:
                u4Val1 = CliPortList.uPortList.PortListRange.u4PortFrom;
                u4Val2 = CliPortList.uPortList.PortListRange.u4PortTo;
                break;
            default:
                i4RetStatus = OSIX_FAILURE;
                continue;
        }

        if (u4Val1 > u4Val2)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }
        /* pu1PortArray will be passed as NULL from CLI lex functions.
         * This is just for checking the validity of the given port list string.
         * It doesn't converts the given port list array in to bit masks
         */
        if (pu1PortArray != NULL)
        {
            for (; u4Val1 <= u4Val2; u4Val1++)
            {
                SPRINTF ((CHR1 *) ai1TempArr, "%u", u4Val1);
                /* Set the member port bit in pu1PortArray */
                if ((CliGetIfIndexFromSubTypeAndPortNum
                     (u4IfType, u4SubType, ai1TempArr,
                      &u4RetPortNum) == CLI_FAILURE)
                    ||
                    (CliSetMemberPort
                     (pu1PortArray, u4PortArrayLen,
                      u4RetPortNum) != OSIX_SUCCESS))
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
        pi1Temp = pi1Pos + 1;
    }
    return i4RetStatus;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrToPortList                                    *
 *                                                                         *
 *     Description   : The function is used by other modules to            *
 *                     convert the given string to port list.              *
 *                                                                         *
 *     Input(s)      : pu1Str         : Pointer to the string.             *
 *                     pu1PortArray   : Pointer to the string in which the *
 *                                      bits for the port list will be set *
 *                     u4PortArrayLen : Array Length.                      *
 *                     u4IfType       : Interface Type                     *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
CliStrToPortList (UINT1 *pu1Str, UINT1 *pu1PortArray,
                  UINT4 u4PortArrayLen, UINT4 u4IfType)
{
    return (CliStrToPortListExt (pu1Str, pu1PortArray, u4PortArrayLen,
                                 u4IfType, 0));
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetValFromIfaceType                              *
 *                                                                         *
 *     Description   : The function is used to extract values from the     *
 *                     Iface list string by identifying the type of port   *
 *                     list string.                                        *
 *                                                                         *
 *     Input(s)      : pCliIfaceList : pointer to the structure for        *  
 *                                    returning port list values.          *
 *                     pi1Str       : Pointer to the string.               *
 *                     u4IfType     : Interface type against which the     *
 *                                    slot, port no's should be validated  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : The type of the port list string.                   *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetValFromIfaceType (tCliIfaceList * pCliIfaceList, INT1 *pi1Str,
                        UINT4 u4IfType)
{
    INT1               *pi1Base = pi1Str;
    INT1                ai1TempArr[31];
    INT1                i1TempChar;
    INT4                i4RetType = CLI_LIST_TYPE_INV;
    UINT4               u4PortFrom;

    if ((pi1Str) && (*pi1Str))
    {
        /* Check for Range delimiter */
        pi1Str = CliGetToken (pi1Base, CLI_RANGE_DELIMIT, CLI_VALIDIFACE_LIST);
        if (!pi1Str)
        {
            i4RetType = CLI_LIST_TYPE_INV;
        }
        else if (*pi1Str)
        {
            i1TempChar = *pi1Str;
            *pi1Str = '\0';
            i4RetType = CliGetSlotAndPort (pCliIfaceList, pi1Base);
            *pi1Str = i1TempChar;
            SPRINTF ((CHR1 *) ai1TempArr, "%d/%u", pCliIfaceList->i4SlotNum,
                     pCliIfaceList->uPortList.u4PortNum);

            if ((i4RetType != CLI_LIST_TYPE_INV) && (u4IfType) &&
                (CliGetIfIndexFromTypeAndPortNum (u4IfType, ai1TempArr,
                                                  &pCliIfaceList->uPortList.
                                                  u4PortNum) == CLI_FAILURE))
            {
                i4RetType = CLI_LIST_TYPE_INV;
            }

            if (i4RetType == CLI_LIST_TYPE_VAL)
            {
                pi1Base = pi1Str + 1;
                pi1Str =
                    CliGetToken (pi1Base, CLI_SLOT_PORT_DELIMIT,
                                 CLI_VALIDIFACE_LIST);
                if (!pi1Str || (*pi1Str))
                {
                    i4RetType = CLI_LIST_TYPE_INV;
                }
                else
                {
                    pi1Str =
                        CliGetToken (pi1Base, CLI_RANGE_DELIMIT,
                                     CLI_VALIDIFACE_LIST);
                    if (!pi1Str || (*pi1Str))
                    {
                        i4RetType = CLI_LIST_TYPE_INV;
                    }
                }
                if (i4RetType != CLI_LIST_TYPE_INV)
                {
                    i4RetType = CLI_LIST_TYPE_RANGE;
                    u4PortFrom = pCliIfaceList->uPortList.u4PortNum;
                    pCliIfaceList->uPortList.PortListRange.u4PortFrom =
                        u4PortFrom;
                    pCliIfaceList->uPortList.PortListRange.u4PortTo =
                        ATOI (pi1Base);

                    SPRINTF ((CHR1 *) ai1TempArr, "%d/%u",
                             pCliIfaceList->i4SlotNum,
                             pCliIfaceList->uPortList.PortListRange.u4PortTo);
                    if ((u4IfType)
                        &&
                        (CliGetIfIndexFromTypeAndPortNum
                         (u4IfType, ai1TempArr,
                          &pCliIfaceList->uPortList.PortListRange.u4PortTo) ==
                         CLI_FAILURE))
                    {
                        i4RetType = CLI_LIST_TYPE_INV;
                    }
                }
            }
        }
        else
        {
            i4RetType = CliGetSlotAndPort (pCliIfaceList, pi1Base);
            SPRINTF ((CHR1 *) ai1TempArr, "%d/%u", pCliIfaceList->i4SlotNum,
                     pCliIfaceList->uPortList.u4PortNum);

            if (i4RetType != CLI_LIST_TYPE_INV)
            {
                if ((u4IfType) &&
                    (CliGetIfIndexFromTypeAndPortNum (u4IfType, ai1TempArr,
                                                      &pCliIfaceList->uPortList.
                                                      u4PortNum) ==
                     CLI_FAILURE))
                {
                    i4RetType = CLI_LIST_TYPE_INV;
                }
                else
                {
                    i4RetType = CLI_LIST_TYPE_VAL;
                }
            }
        }
    }
    return i4RetType;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliStrToIfaceList                                   *
 *                                                                         *
 *     Description   : The function is used by other modules to            *
 *                     convert the given string to Iface list.             *
 *                                                                         *
 *     Input(s)      : pu1Str         : Pointer to the string.             *
 *                     pu1PortArray   : Pointer to the string in which the *
 *                                      bits for the iface list will be set*
 *                     u4PortArrayLen : Array Length.                      *
 *                     u4IfType     : Interface type against which the     *
 *                                    slot, port no's should be validated  *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
CliStrToIfaceList (UINT1 *pu1Str, UINT1 *pu1PortArray,
                   UINT4 u4PortArrayLen, UINT4 u4IfType)
{
    tCliIfaceList       CliIfaceList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetType;
    UINT4               u4Val1;
    UINT4               u4Val2;
    INT1                i1TempChar;
    INT1                i1EndFlag = OSIX_FALSE;

    if (!(pi1Temp))
        i4RetStatus = OSIX_FAILURE;

    while ((i4RetStatus != OSIX_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDIFACE_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (!(*pi1Pos))
            i1EndFlag = OSIX_TRUE;

        i1TempChar = *pi1Pos;
        *pi1Pos = '\0';

        /* Get values between the list seperaters based on the type */
        i4RetType = CliGetValFromIfaceType (&CliIfaceList, pi1Temp, u4IfType);
        *pi1Pos = i1TempChar;

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                u4Val1 = CliIfaceList.uPortList.u4PortNum;
                u4Val2 = CliIfaceList.uPortList.u4PortNum;
                break;
            case CLI_LIST_TYPE_RANGE:
                u4Val1 = CliIfaceList.uPortList.PortListRange.u4PortFrom;
                u4Val2 = CliIfaceList.uPortList.PortListRange.u4PortTo;
                break;
            default:
                i4RetStatus = OSIX_FAILURE;
                continue;
        }

        if (u4Val1 > u4Val2)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }
        if (pu1PortArray != NULL)
        {
            for (; u4Val1 <= u4Val2; u4Val1++)
            {
                if (CliSetMemberPort (pu1PortArray, u4PortArrayLen, u4Val1) !=
                    OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                /* Set the member port bit in pu1PortArray */
            }
        }
        pi1Temp = pi1Pos + 1;
    }
    return i4RetStatus;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetSessionBitMap                                 *
 *                                                                         *
 *     Description   : This exported function is called by external        *
 *                     modules to set a bit in global value in CLI         *
 *                     Modules can set a bit to disable CLI console prompt *
 *                     for a particular session                            *
 *                                                                         *
 *     Input(s)      : u1SessionType  : Type of session for which bitmap   *
 *                                      is to be set                       *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/
VOID
CliSetSessionBitMap (UINT4 u4SessionType)
{
    gu4CliConsoleBitMap = u4SessionType;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetRangeList                                      *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to set Port list in CLI specified by range  *
 *                     command                                             *
 *                                                                         *
 *     Input(s)      : pu1PortList    : Portlist with current port range   *
 *                                                                         *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetRangeList (UINT1 *pu1RangeList)
{

    tCliContext        *pCliContext;
    UINT4               u4Len = 0;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    MEMSET (pCliContext->au1RangeList, 0, MAX_CHAR_IN_TOKEN);
    u4Len = ((STRLEN (pu1RangeList) < sizeof (pCliContext->au1RangeList)) ?
             STRLEN (pu1RangeList) : sizeof (pCliContext->au1RangeList) - 1);
    STRNCPY (pCliContext->au1RangeList, pu1RangeList, u4Len);
    pCliContext->au1RangeList[u4Len] = '\0';
    return (CLI_SUCCESS);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliInitRangeList                                     *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to initialize Port list in CLI , used when  *
 *                     user exits the range mode                           *
 *                                                                         *
 *     Input(s)      : NONE                                                *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliInitRangeList (VOID)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {

        /* No Task Found */
        return (CLI_FAILURE);
    }
    MEMSET (pCliContext->au1RangeList, 0, MAX_CHAR_IN_TOKEN);
    return CLI_SUCCESS;

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetRangeIndex                                  *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to set  start and end index of l3 vlan range*
 *                     in CLI                                              *
 *                                                                         *
 *     Input(s)      : u4StartIndex : start index of L3 VLAN Range         *
 *                     u4EndIndex : END index of L3 VLAN Range             *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetRangeIndex (UINT4 u4StartIndex, UINT4 u4EndIndex)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    pCliContext->u4StartIndex = u4StartIndex;
    pCliContext->u4EndIndex = u4EndIndex;
    return (CLI_SUCCESS);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliInitRangeIndex                                 *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to init start and end index of l3 vlan range*
 *                     in CLI                                              *
 *     Input (s)     : NULL                                                *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliInitRangeIndex (VOID)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->u4StartIndex = 0;
    pCliContext->u4EndIndex = 0;
    return CLI_SUCCESS;

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetIfRangeMode                                   *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to set range mode                           *
 *                     in CLI                                              *
 *     Input (s)     : NULL                                                *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetIfRangeMode (VOID)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->i4Mode = CLI_IF_RANGE_MODE_ENABLE;
    return CLI_SUCCESS;

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliReSetIfRangeMode                                 *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to reset range mode                         *
 *                     in CLI                                              *
 *     Input (s)     : NULL                                                *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliReSetIfRangeMode (VOID)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }

    pCliContext->u1NoOfAttempts = 0;

    pCliContext->i4Mode = CLI_IF_RANGE_MODE_DISABLE;
    return CLI_SUCCESS;

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetIfRangeMode                                   *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to get range mode                           *
 *                     in CLI                                              *
 *     Input (s)     : NULL                                                *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetIfRangeMode (INT4 *pi4Mode)
{

    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    *pi4Mode = pCliContext->i4Mode;
    return CLI_SUCCESS;

}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetIfRangeType                                   *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to set if range type                        *
 *                     in CLI                                              *
 *     Input (s)     : i1RangeType  - Interface type for Range             *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetIfRangeType (UINT1 *pu1RangeType)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    MEMCPY (pCliContext->au1RangeType, pu1RangeType, CLI_MAX_IF_TYPE_LEN);
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetIfRangeType                                   *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to Get if range type                        *
 *                     in CLI                                              *
 *     Input (s)     : i1RangeType  - Interface type for Range             *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetIfRangeType (UINT1 *pu1RangeType)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    MEMCPY (pu1RangeType, pCliContext->au1RangeType, CLI_MAX_IF_TYPE_LEN);
    return CLI_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliInitIfRangeType                                  *
 *                                                                         *
 *     Description   : This  function is called by external                *
 *                     modules to set if range type                        *
 *                     in CLI                                              *
 *     Input (s)     : NULL                                                *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/

INT4
CliInitIfRangeType (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    MEMSET (pCliContext->au1RangeType, 0, CLI_MAX_IF_TYPE_LEN);
    return CLI_SUCCESS;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliGetSlotAndRange                                  *
*                                                                         *
*     Description   : This  function is called by external                *
*                     modules to get start and stop index of port Range   * 
*                     in CLI                                              *
*     Input (s)     : pu1PortRange  -Port Range                           *
*     Output(s)     : pu2StartIndex -Start Index                          *
*                     pu2StopIndex  -Stop Index                           *
*     Returns       : CLI_FAILURE /CLI_SUCCESS                            *
*                                                                         *
****************************************************************************/
INT4
CliGetSlotAndRange (UINT1 *pu1PortRange,
                    UINT2 *pu2StartIndex, UINT2 *pu2StopIndex, UINT2 *pu2SlotNo)
{
    INT1               *pi1Pos;
    INT1               *pu1TempPos;
    INT1               *p1BasePtr;
    INT1                au1PortRange[MAX_CHAR_IN_TOKEN];
    MEMSET (au1PortRange, 0, MAX_CHAR_IN_TOKEN);
    if ((pu1PortRange != NULL) && (STRLEN (pu1PortRange) != 0))
    {
        STRCPY (au1PortRange, pu1PortRange);
        pi1Pos =
            CliGetToken (au1PortRange, CLI_SLOT_PORT_DELIMIT,
                         CLI_VALIDIFACE_LIST);
        if (pi1Pos == NULL)
        {
            return CLI_FAILURE;
        }
        pu1TempPos = pi1Pos;
        *pi1Pos = '\0';
        *pu2SlotNo = (UINT2) (ATOI (au1PortRange));

        p1BasePtr = pu1TempPos + 1;
        pi1Pos =
            CliGetToken (p1BasePtr, CLI_RANGE_DELIMIT, CLI_VALIDIFACE_LIST);
        if ((pi1Pos != NULL) && (*pi1Pos != 0))
        {
            pu1TempPos = pi1Pos;
            *pi1Pos = '\0';
            *pu2StartIndex = (UINT2) (ATOI (p1BasePtr));
            p1BasePtr = pu1TempPos + 1;
            *pu2StopIndex = (UINT2) (ATOI (p1BasePtr));
        }
        else
        {
            *pu2StartIndex = (UINT2) (ATOI (p1BasePtr));
            *pu2StopIndex = *pu2StartIndex;
        }
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliMemAllocMemBlk                                   *
*                                                                         *
*     Description   : This  function allocates a Block from the           *
*                     MemPool and Initialises the Block to Zero           * 
*                                                                         *
*     Input (s)     : PoolId - MemPoolId                                  *
*                     i4Size - Block Size                                 *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : Pointer to the Block / NULL                         *
*                                                                         *
****************************************************************************/
VOID               *
CliMemAllocMemBlk (tMemPoolId PoolId, UINT4 u4Size)
{
    UINT1              *pu1Block = NULL;

    pu1Block = MemAllocMemBlk (PoolId);

    if (pu1Block == NULL)
    {
        return NULL;
    }

    MEMSET (pu1Block, 0, u4Size);

    return ((VOID *) pu1Block);
}

/***************************************************************************
*                                                                         *
*     Function Name : CliMemReleaseMemBlock                               *
*                                                                         *
*     Description   : This  function releases a Block if ptr is not null  *
*                     and Initialises the Block Ptr to NULL               *
*                                                                         *
*     Input (s)     : PoolId - MemPoolId                                  *
*                     ppu1Block - Block Pointer to be released            *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS/ CLI_FAILURE                            *
*                                                                         *
****************************************************************************/
INT4
CliMemReleaseMemBlock (tMemPoolId PoolId, UINT1 **ppu1Block)
{
    if ((ppu1Block == NULL) || (*ppu1Block) == NULL)
    {
        return CLI_FAILURE;
    }

    MemReleaseMemBlock (PoolId, *ppu1Block);
    *ppu1Block = NULL;
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliUtilRegCallBackforCLIModule                       */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI__SUCCESS/CLI__FAILURE                            */
/*****************************************************************************/

INT4
CliUtilRegCallBackforCLIModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = CLI_SUCCESS;

    switch (u4Event)
    {
        case CLI_USER_PRIVILEGE_CHK:
            CLI_CALLBACK_FN[u4Event].pCliCustCheckrootLoginAllowed =
                pFsCbInfo->pCliCustCheckrootLoginAllowed;
            break;

        case CLI_STRICT_PASSWD_CHK:
            CLI_CALLBACK_FN[u4Event].pCliCustCheckStrictPasswd =
                pFsCbInfo->pCliCustCheckStrictPasswd;
            break;

        default:
            i4RetVal = CLI_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name        : CliUtilCallBack                                    */
/*                                                                           */
/* Description          : This function is invoked to call the registered    */
/*                        custom function  pointer for specific events.      */
/*                                                                           */
/* Input(s)             : u4Event - Event for which Call back is invoked     */
/*                        pCliCallBackArgs - Pointer to structure            */
/*                                           tCliCallBackArgs                */
/*                                                                           */
/* Output(s)            : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CliUtilCallBack (UINT4 u4Event, tCliCallBackArgs * pCliCallBackArgs)
{
    INT4                i4RetVal = CLI_SUCCESS;

    switch (u4Event)
    {
        case CLI_USER_PRIVILEGE_CHK:
            if (CLI_CALLBACK_FN[u4Event].pCliCustCheckrootLoginAllowed != NULL)
            {
                i4RetVal = CLI_CALLBACK_FN[u4Event].
                    pCliCustCheckrootLoginAllowed
                    (pCliCallBackArgs->i2PrivilegeId);
            }
            break;

        case CLI_STRICT_PASSWD_CHK:
            if (CLI_CALLBACK_FN[u4Event].pCliCustCheckStrictPasswd != NULL)
            {
                i4RetVal = CLI_CALLBACK_FN[u4Event].
                    pCliCustCheckStrictPasswd ();
            }
            break;

        default:
            i4RetVal = CLI_FAILURE;
            break;
    }

    return i4RetVal;
}

/**************************************************************************
*                                                                         *
*     Function Name : CliUtilGetSessionMode                               *
*                                                                         *
*     Description   : This  function returns Cusrrent CLI Session         *         
*                     Serial/Telnet/SSH                                   *  
*                                                                         *
*                                                                         *
*     Input (s)     : None                                                *
*                                                                         *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_ERROR/ Cli Mode                                 *
*                                                                         *
****************************************************************************/
/* API provided for other modules to get the current CLI Mode  */
UINT4
CliUtilGetSessionMode (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_ERROR);
    }
    return pCliContext->gu4CliMode;
}

/***************************************************************************
 *     Function Name : CliFindCksum                                        *
 *                                                                         *
 *     Description   : This function will checksum for the lastline and    *
 *                     append it into the file                             *
 *                                                                         *
 *     Input(s)      : File descriptor, Sum of otherlines and              *
 *                                calculated checksum                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 ***************************************************************************/
INT1
CliFindCksum (INT4 i4Fd, INT2 i2Length, UINT4 u4Sum)
{
    UINT2               u2Csum = 0;
    INT1                i1Temp = 0;
    INT1               *pi1Temp = NULL;
    INT2                i2Len = 0;
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN];

    i2Len = 0;
    CLI_MEMSET (ai1Buf, 0, CLI_MAX_USERS_LINE_LEN);
    pi1Temp = ai1Buf;
    sprintf ((char *) pi1Temp, "CKSUM:");
    i2Len = CLI_STRLEN (pi1Temp);
    pi1Temp += CLI_STRLEN (pi1Temp);

    /* Appending Checksum with the word CKSUM: " */
    CLI_MEMCPY (pi1Temp, &u2Csum, sizeof (UINT2));
    i2Len += sizeof (UINT2);
    pi1Temp += sizeof (UINT2);
    i2Length += i2Len;
    /* Checking the length whether it is odd or even */
    if ((i2Length % 2) == 1)
    {
        CLI_MEMCPY (pi1Temp, &i1Temp, sizeof (INT1));
        i2Length++;
        i2Len++;
    }
    /* Calculating checksum for the line CkSUM */
    UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2Csum, CKSUM_LASTDATA);
    pi1Temp -= 2;
    CLI_MEMCPY (pi1Temp, &u2Csum, sizeof (UINT2));
    pi1Temp = ai1Buf;

    /* appending the checksum value into the file */
    FileWrite (i4Fd, (CHR1 *) pi1Temp, i2Len);
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliVerifyCheckSum                                   *
 *                                                                         *
 *     Description   : This function will verify checksum during           *
 *                     read operation                                      *
 *                                                                         *
 *     Input(s)      : read line from the file, totallength, line length   *
 *                      calculated sum, checksum                           *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 ***************************************************************************/

INT1
CliVerifyCheckSum (INT1 *pData, INT2 i2TotLen, INT2 i2Length,
                   UINT4 u4Sum, UINT2 u2CkSum)
{
    UINT2               u2CheckSum = 0;
    INT1               *pi1Tmp = NULL;

    /* Calculate checksum for the last line */
    if ((CLI_STRNCMP (pData, "CKSUM:", CLI_STRLEN ("CKSUM:")) != 0))
    {
        return CLI_FAILURE;
    }
    i2TotLen += i2Length;
    pi1Tmp = pData;
    pi1Tmp += CLI_STRLEN ("CKSUM:");
    MEMCPY (&u2CheckSum, pi1Tmp, sizeof (UINT2));
    MEMSET (pi1Tmp, 0, sizeof (UINT2));
    if ((i2TotLen % 2) == 1)
    {
        i2Length++;
    }
    UtilCalcCheckSum ((UINT1 *) pData, i2Length, &u4Sum,
                      &u2CkSum, CKSUM_LASTDATA);
    if (u2CkSum == u2CheckSum)
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***************************************************************************
 *     Function Name : CliRemoveCommand                                    *
 *                                                                         *
 *     Description   : This function will remove the cli command from ISS  *
 *                                                                         *
 *     Input(s)      : pModeName - Mode name for the command               *                         *                     pCmdName  - Command to be removed under that mode   *
 *                                                                         *
 *     Output(s)     : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/

INT1
CliRemoveCommand (INT1 *pModeName, INT1 *pCmdName)
{
    tCliContext        *pCliContext = NULL;
    t_MMI_MODE_TREE    *pMode = NULL;
    t_MMI_COMMAND_LIST *pCommandList = NULL;
    t_MMI_CMD          *pCommand = NULL;
    t_MMI_CMD          *prevCmd = NULL;
    t_MMI_CMD          *pTmp = NULL;
    t_MMI_HELP         *pHelp = NULL;
    INT4                i4ActionNumber = CLI_ZERO;
    INT2                i2Count = CLI_ZERO;
    INT1               *pi1RemCmd = NULL;
    INT1                i1Retval = CLI_FAILURE;
    INT1                i1TokReturn = '\0';
    CHR1               *pu1CommandName = NULL;
    UINT1               u1CmdFound = CLI_FALSE;
    INT4                i4Index = -1;
    UINT1               u1Allocated = OSIX_FALSE;

    UNUSED_PARAM (i1TokReturn);

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1CommandName, CHR1);
    if (NULL == pu1CommandName)
    {
        return CLI_FAILURE;
    }
    CLI_MEMSET (pu1CommandName, CLI_ZERO, (MAX_LINE_LEN + 1));
    STRCPY (pu1CommandName, (CHR1 *) pCmdName);

    /* Get the Apllication Context */
    CliTakeAppContext ();
    pCliContext = CliGetApplicationContext ();
    if (NULL == pCliContext)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        pu1CommandName = NULL;
        CliGiveAppContext ();
        return CLI_FAILURE;
    }
    CliInitMmiVal (pCliContext);
    if (pCliContext->pCliAmbVar == NULL)
    {
        u1Allocated = OSIX_TRUE;
        OsixSemTake (gCliSessions.CliAmbVarSemId);
        CLI_ALLOC_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
    }
    i1TokReturn = mmi_gettoken (pCliContext, (INT1 *) pu1CommandName,
                                &pi1RemCmd);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
    pu1CommandName = NULL;
    /* return Mode for the given ModeName */
    pMode = CliGetModeFromName (pModeName);
    if (NULL == pMode)
    {
        /*If mode does not exist return the application context */
        CliGiveAppContext ();
        if (u1Allocated == OSIX_TRUE)
        {
            CLI_RELEASE_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
            pCliContext->pCliAmbVar = NULL;
            OsixSemGive (gCliSessions.CliAmbVarSemId);
        }
        return CLI_FAILURE;
    }
    pCliContext->pMmiCur_mode = pMode;
    pCommandList = pMode->pCommand_tree;
    pHelp = pMode->pHelp;

    /* Removing Command */
    i1Retval = CliGetCommand (pCliContext, &pTmp, &i4ActionNumber, i4Index);

    if (u1Allocated == OSIX_TRUE)
    {
        CLI_RELEASE_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        pCliContext->pCliAmbVar = NULL;
        OsixSemGive (gCliSessions.CliAmbVarSemId);
    }

    /* Releases the Application context */
    CliGiveAppContext ();
    if (CLI_SUCCESS == i1Retval)
    {
        /* clearing the command */
        while (pCommandList != NULL)
        {
            pCommand = pCommandList->pCmd;
            while (pCommand != NULL)
            {
                if (pTmp == pCommand)
                {
                    if (CLI_ZERO == i2Count)
                    {
                        pCommandList->pCmd = pCommand->pNext_command;
                    }
                    else
                    {
                        prevCmd->pNext_command = pCommand->pNext_command;
                    }
                    u1CmdFound = CLI_TRUE;
                    pCommand = NULL;
                    break;
                }
                i2Count++;
                prevCmd = pCommand;
                pCommand = pCommand->pNext_command;
            }
            if (u1CmdFound == CLI_TRUE)
            {
                break;
            }
            i2Count = 0;
            pCommandList = pCommandList->pNext;
        }

        /* clearing help */
        while (pHelp != NULL)
        {
            if ((pHelp->i4ActNumStart <= i4ActionNumber)
                && (pHelp->i4ActNumEnd >= i4ActionNumber))
            {
                break;
            }
            pHelp = pHelp->pNext;
        }

        if (pHelp != NULL)
        {
            /* set the privilege to some higher value */
            pHelp->pCxtHelp[i4ActionNumber - pHelp->i4ActNumStart] = NULL;

            /* NULL is used as delimiter to move to next pHelp pointer during scanning. If null
             * is assigned, then we will miss the remaining chain in the pHelp. So we will
             * make pointer value as Invalid pointer address as CLI_REM_CMD. This will help us
             * such that while scanning if we see pointer as CLI_REM_CMD we can skip and continue
             * other nodes till NULL*/

            pHelp->pSyntax[i4ActionNumber - pHelp->i4ActNumStart] = CLI_REM_CMD;
            pHelp->pPrivilegeId[i4ActionNumber - pHelp->i4ActNumStart]
                = CLI_INVALID_PRIVILEGE_ID;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliGetModeFromName                                  *
 *                                                                         *
 *     Description   : This function will get the mode for the given name  *
 *                                                                         *
 *     Input(s)      : pi1ModeName - Mode name                             *
 *                                                                         *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/

t_MMI_MODE_TREE    *
CliGetModeFromName (INT1 *pi1ModeName)
{
    t_MMI_MODE_TREE    *apTstack[MAX_MODE_LEVEL] = { NULL };
    t_MMI_MODE_TREE    *pTsearch = NULL;
    INT1                i1Top = CLI_ZERO;

    if (!(pi1ModeName))
    {
        return NULL;
    }
    apTstack[i1Top] = gCliSessions.p_mmi_root_mode;
    while (i1Top >= 0)
    {
        /* checking the stack empty */
        pTsearch = apTstack[i1Top];    /* pop stack */
        i1Top--;
        while (pTsearch != NULL)
        {
            if ((STRCASECMP (pTsearch->pMode_name, pi1ModeName) == 0))
            {
                return pTsearch;
            }

            if (pTsearch->pChild != NULL)
            {
                i1Top++;
                apTstack[i1Top] = pTsearch->pNeighbour;
                pTsearch = pTsearch->pChild;
            }
            else
            {
                if (i1Top < 0)
                {
                    pTsearch = NULL;
                }
                else
                {
                    pTsearch = pTsearch->pNeighbour;

                }
            }
        }
    }
    return NULL;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliUtilCheckBlockUserOrNot                          *
*                                                                         *
*     Description   : This function blocks a user if the user is present  *
*                     in user list                
*                                                                           * 
*     Input (s)     : pi1Name - Pointer to user name                      *
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS or CLI_FAILURE                          *
*                                                                         *
****************************************************************************/

INT4
CliUtilCheckBlockUserOrNot (CHR1 * pi1Name)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    UINT4               u4SysTimeSecs = 0;
    tUtlTm              UtlTm;
    UINT4               u4UsrRelTime = 0;

    MEMSET (&UtlTm, 0, sizeof (tUtlTm));
    MEMSET (ai1Buf, 0, CLI_MAX_USERS_LINE_LEN + 1);

    /* Root user should not be blocked 
     * (In Fips mode - Admin user will not be blocked) */
    if (STRCMP (pi1Name, CLI_ROOT_USER) == 0)
    {
        return CLI_FAILURE;
    }
    /* Calculate Release Time of User */
    UtlGetTime (&UtlTm);
    CliGetSecondsSinceBase (UtlTm, &u4SysTimeSecs);
    u4UsrRelTime =
        (UINT4) u4SysTimeSecs + (UINT4) gCliSessions.u4LoginLockOutTime;
    FpamSetReleaseTime (pi1Name, u4UsrRelTime);
    mmi_printf ("\r\n%% Maximum Password tries over,User is blocked\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliUtilCheckAllowUserLogin                          *
*                                                                         *
*     Description   : This function is used to check if the user is       *
*                     allowed to login or not                             * 
*                                                                         *
*     Input (s)     : pi1Name - Pointer to user name                      *
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS or CLI_FAILURE                          *
*                                                                         *
****************************************************************************/
INT4
CliUtilCheckAllowUserLogin (CHR1 * pi1Name)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    UINT4               u4BlockUserRelTime = 0;
    UINT4               u4SysTimeSecs = 0;
    INT4                i4UserStatus = 1;
    tUtlTm              UtlTm;

    MEMSET (&UtlTm, 0, sizeof (tUtlTm));
    MEMSET (ai1Buf, 0, CLI_MAX_USERS_LINE_LEN + 1);

    if (gu1IssLoginAuthMode != LOCAL_LOGIN)
    {
        return CLI_SUCCESS;
    }

    if (FpamGetReleaseTime (pi1Name, &u4BlockUserRelTime) == FPAM_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (FpamGetUserStatus (pi1Name, &i4UserStatus) == FPAM_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* The user must be allowed to login only if Release Time value is 0
     * And Status is enabled (1). By Default the Status will be always 
     * enabled */
    if ((u4BlockUserRelTime == 0) && (i4UserStatus == FPAM_USER_ENABLE))
    {
        return CLI_SUCCESS;
    }
    UtlGetTime (&UtlTm);
    CliGetSecondsSinceBase (UtlTm, &u4SysTimeSecs);
    if ((u4BlockUserRelTime > u4SysTimeSecs)
        || (i4UserStatus == FPAM_USER_DISABLE))
    {
        mmi_printf ("\r\r%%User is blocked, Login Not Allowed\r\n");
        return CLI_FAILURE;
    }
    else
    {
        u4BlockUserRelTime = 0;
        FpamSetReleaseTime (pi1Name, u4BlockUserRelTime);
        return CLI_SUCCESS;
    }
}

/***************************************************************************
*                                                                         *
*     Function Name : CliGetSecondsSinceBase                              *
*                                                                         *
*     Description   : This function is used to get the time in seconds    *
*                     since base year(2000).                              * 
*                                                                         *
*     Input (s)     : UtlTm - Structure containing current time           *
*                                                                    *
*     Output(s)     : pu4SysTimeSecs - Time in seconds since base year    *
*                                                                         *
*     Returns       : None                                                *
*                                                                         *
****************************************************************************/

VOID
CliGetSecondsSinceBase (tUtlTm UtlTm, UINT4 *pu4SysTimeSecs)
{
    UINT4               u4year = UtlTm.tm_year;
    UINT4               u4yday = UtlTm.tm_yday;
    *pu4SysTimeSecs = 0;
    --u4year;
    --u4yday;
    while (u4year >= TM_BASE_YEAR)
    {
        *pu4SysTimeSecs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    *pu4SysTimeSecs += (UtlTm.tm_yday * 86400);
    *pu4SysTimeSecs += (UtlTm.tm_hour * 3600);
    *pu4SysTimeSecs += (UtlTm.tm_min * 60);
    *pu4SysTimeSecs += (UtlTm.tm_sec);
    return;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliUtilUnlockUser                                   *
*                                                                         *
*     Description   : This function is used to release a blocked user     * 
*                                                                         *
*     Input (s)     : pi1Name - Pointer to user name                      *
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS or CLI_FAILURE                          *
*                                                                         *
****************************************************************************/
INT4
CliUtilUnlockUser (CHR1 * pi1Name)
{
    INT1                ai1Buf[CLI_MAX_USERS_LINE_LEN + 1];
    UINT4               u4BlockUserRelTime = 0;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));

    if ((CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return CLI_FAILURE;
    }

    if (CliCheckSpecialPriv () != CLI_SUCCESS)
    {
        mmi_printf
            ("\r\n%%Only %s or %s privileged user can unlock the blocklisted user\r\n",
             CLI_ROOT_USER, CLI_ROOT_USER);
        return CLI_FAILURE;
    }

    FpamGetReleaseTime (pi1Name, &u4BlockUserRelTime);
    if (u4BlockUserRelTime == 0)
    {
        mmi_printf ("\r\n%%The User is not blocklisted\r\n");
        return CLI_SUCCESS;
    }
    else
    {
        FpamSetReleaseTime (pi1Name, u4BlockUserRelTime);
        return CLI_SUCCESS;
    }
}

/***************************************************************************
*                                                                         *
*     Function Name : CliSetLoginLockOutTIme                              *
*                                                                         *
*     Description   : This function is called to set the value of Lock out*
*                   time configure by user                              * 
*                                                                         *
*     Input (s)     : u4LoginLockOutTime - Lock out time value
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : None                                                *
*                                                                         *
****************************************************************************/
VOID
CliSetLoginLockOutTime (UINT4 u4LoginLockTime)
{

    CliTakeAppContext ();
    gCliSessions.u4LoginLockOutTime = u4LoginLockTime;
    CliGiveAppContext ();
    return;
}

/***************************************************************************
*                                                                         *
*     Function Name : CliSetLoginAttempts                                 *
*                                                                         *
*     Description   : This function is called to set the value of Login   *
*                     Attempts                                            * 
*                                                                         *
*     Input (s)     : u4LoginAttempts - Login Attempts value
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : None                                                *
*                                                                         *
****************************************************************************/

VOID
CliSetLoginAttempts (UINT1 u1LoginAttempts)
{
    CliTakeAppContext ();
    gCliSessions.u1LoginAttempts = u1LoginAttempts;
    CliGiveAppContext ();
    return;
}

/*****************************************************************************/
/* Function Name      : CliChkStrictPasswdConfRules                          */
/*                                                                           */
/* Description        : This function is called to check if the password     */
/*                      follows Password Configuration rules                 */
/*                                                                           */
/* Input(s)           : pi1Password   : Password to be checked               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CliChkStrictPasswdConfRules (INT1 *pi1Password)
{
    UINT4               u4Index = 0;
    UINT4               u4PasswdLen = 0;
    INT1                i1UpperCaseFlag = CLI_FALSE;
    INT1                i1LowerCaseFlag = CLI_FALSE;
    INT1                i1NumberFlag = CLI_FALSE;
    INT1                i1SplCharFlag = CLI_FALSE;
    INT4                i4RetVal = CLI_FAILURE;

    u4PasswdLen = CLI_STRLEN (pi1Password);

    for (u4Index = 0; u4Index < u4PasswdLen; u4Index++)
    {
        if ((pi1Password[u4Index] >= CLI_UPPER_CHAR_START) &&
            (pi1Password[u4Index] <= CLI_UPPER_CHAR_END))
        {
            i1UpperCaseFlag = CLI_TRUE;
        }
        else if ((pi1Password[u4Index] >= CLI_LOWER_CHAR_START) &&
                 (pi1Password[u4Index] <= CLI_LOWER_CHAR_END))
        {
            i1LowerCaseFlag = CLI_TRUE;
        }
        else if ((pi1Password[u4Index] >= CLI_NUM_CHAR_START) &&
                 (pi1Password[u4Index] <= CLI_NUM_CHAR_END))
        {
            i1NumberFlag = CLI_TRUE;
        }
        else if ((pi1Password[u4Index] == CLI_SPL_CHAR_1) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_2) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_3) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_4) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_5) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_6) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_7) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_8) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_9) ||
                 (pi1Password[u4Index] == CLI_SPL_CHAR_10))
        {
            i1SplCharFlag = CLI_TRUE;
        }
    }

    if (i1UpperCaseFlag & i1LowerCaseFlag & i1NumberFlag & i1SplCharFlag)
    {
        i4RetVal = CLI_SUCCESS;
    }

    return i4RetVal;
}

/***************************************************************************
 *     Function Name : CliRemoveCmdOpt                                     *
 *                                                                         *
 *     Description   : This function will remove the cli command option.   *
 *                     Optional command position and optional              *
 *                     key word verification will be done only for key     *
 *                     words and not for options containing standard       *
 *                     keyword arguments like <ip_addr>.                   *
 *                                                                         *
 *     Input(s)      : pModeName - Mode name for the command               *  
 *                     pCmdName  - Command to be removed under that mode   *
 *                     i4Option_position - position of the option to be    *
 *                     removed                                             * 
 *                     pOption_name - Name of the command to be removed    *
 *     Output(s)     : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT1
CliRemoveCmdOpt (INT1 *pModeName, INT1 *pCmdName,
                 INT4 *i4Option_position, INT1 *i1Option_name)
{
    tCliContext        *pCliContext = NULL;
    t_MMI_MODE_TREE    *pMode = NULL;
    t_MMI_CMD          *pCommand = NULL;
    t_MMI_CMD          *pPrvCmd = NULL;
    t_MMI_CMD          *pTmp = NULL;
    t_MMI_CMD          *pTmpCmd = NULL;
    t_MMI_HELP         *pHelp = NULL;
    INT1               *pi1RemCmd = NULL;
    INT4                i4ReturnVal = CLI_FAILURE;
    INT4                i4ActionNumber = CLI_ZERO;
    CHR1               *pu1CommandName = NULL;
    UINT1               u1CmdFound = CLI_FALSE;
    UINT4               u4Index = 0;
    INT4                i4CurIndex = 0;
    UINT4               u4Count = 0;
    INT4                i4AmbCmdCnt = 0;
    CHR1               *pu1TempSyntax = NULL;

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1CommandName, CHR1);
    if (NULL == pu1CommandName)
    {
        return CLI_FAILURE;
    }
    CLI_MEMSET (pu1CommandName, CLI_ZERO, (MAX_LINE_LEN + 1));
    STRCPY (pu1CommandName, (CHR1 *) pCmdName);
    /*for syntax */
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1TempSyntax, CHR1);
    if (NULL == pu1TempSyntax)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        return CLI_FAILURE;
    }
    CLI_MEMSET (pu1TempSyntax, CLI_ZERO, (MAX_LINE_LEN + 1));
    /* Get the Application Context */
    CliTakeAppContext ();
    pCliContext = CliGetApplicationContext ();
    if (NULL == pCliContext)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
        return CLI_FAILURE;
    }
    CliInitMmiVal (pCliContext);
    i4ReturnVal = mmi_gettoken (pCliContext, (INT1 *) pu1CommandName,
                                &pi1RemCmd);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
    pu1CommandName = NULL;
    /* return Mode for the given ModeName */
    pMode = CliGetModeFromName (pModeName);
    if (NULL == pMode)
    {
        /*If mode does not exist return the application context */
        CliGiveAppContext ();
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
        return CLI_FAILURE;
    }
    pCliContext->pMmiCur_mode = pMode;
    pHelp = pMode->pHelp;
    CliGetCommand (pCliContext, &pTmp, &i4ActionNumber, i4CurIndex);
    /* Assinging ambiguous commands count */
    i4AmbCmdCnt = pCliContext->present_ambig_count;
    for (i4CurIndex = 0; i4CurIndex < i4AmbCmdCnt; i4CurIndex++)
    {
        /* Removing Command */
        i4ReturnVal =
            CliGetCommand (pCliContext, &pTmp, &i4ActionNumber, i4CurIndex);
        pCommand = pTmp;
        /*This loop is to search for a particular option to be removed
         *from the command
         */
        u1CmdFound = CliSearchOption (&pCommand, &pPrvCmd,
                                      *i4Option_position, i1Option_name);
        if (CLI_TRUE == u1CmdFound)
        {
            break;
        }
        else
        {
            continue;
        }
    }

    if ((NULL == pCommand) || (CLI_TRUE != u1CmdFound))
    {
        mmi_printf ("\r %% Failure: Option name is not found at the "
                    "specified location in command \r\n ");
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
        CliGiveAppContext ();
        return CLI_FAILURE;
    }

    /* Releases the Application context */
    CliGiveAppContext ();

    if (CLI_SUCCESS == i4ReturnVal)
    {
        CliObtainCommandHelp (&pHelp, i4ActionNumber);
        if (NULL != pHelp)
        {
            u4Index = i4ActionNumber - pHelp->i4ActNumStart;

            /* Check for the pSyntax and skip if the syntax is 
             * marked as removed because of command removal */

            if ((pHelp->pSyntax[u4Index] != CLI_REM_CMD) &&
                ((u4Count = STRLEN (pHelp->pSyntax[u4Index])) > MAX_LINE_LEN))

            {
                mmi_printf
                    ("\r%% Failure: Length of the command Syntax %d exceeds "
                     "the value of MAX_LINE_LEN %d \r\n", u4Count,
                     MAX_LINE_LEN);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
                return CLI_FAILURE;
            }
            /* Check if this command is removed already */
            if (pHelp->pSyntax[u4Index] == CLI_REM_CMD)
            {
                mmi_printf ("\r%% Failure: Syntax is already removed \n");
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
                return CLI_FAILURE;

            }

            STRCPY (pu1TempSyntax, pHelp->pSyntax[u4Index]);
        }

        else
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
            return CLI_FAILURE;
        }

        if ((CLI_TRUE == u1CmdFound) && (NULL != pCommand->pOptional))
        {

            pTmpCmd = pCommand;

            /* If the previous command/token has its pOptional as the
             * command which has to be removed, then set the previous command
             * pOption to current command's pOption and also the make the
             * pNext_token of last token of previous command point to
             * current command's poption.
             * e.g a b [c d] [e f] [g h] , where [e f] has to be removed
             */

            if ((NULL != pPrvCmd) && (NULL != pPrvCmd->pOptional))
            {
                CliModifyCmdPrvOptional (&pPrvCmd, &pTmpCmd,
                                         &pCommand, i4ActionNumber);
            }
            /* if previous command has no poptional */
            else
            {
                /*
                 * in case of command like a b {all|[c] [d] [e]},
                 * where [c]  has to be removed
                 */
                CliModifyCmd (&pPrvCmd, &pTmpCmd, &pCommand, i4ActionNumber);
            }
        }

        i4ReturnVal = CliUpdateSyntax (pu1TempSyntax, i1Option_name);
        if (CLI_FAILURE == i4ReturnVal)
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
            return CLI_FAILURE;
        }
        pHelp->pSyntax[u4Index] = pu1TempSyntax;

        /*
         * Freeing memory for the removed option
         */
        CliFreeOption (&pCommand, i4ActionNumber);
    }
    else
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempSyntax);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliUpdateSyntax                                     *
 *                                                                         *
 *     Description   : This function will update syntax for the command    *
 *                     an option is removed                                * 
 *     Input(s)      : pu1Syntax - Syntax of the command                   * 
 *                     pOption_name - option to be removed                 * 
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT4
CliUpdateSyntax (CHR1 * pu1Syntax, INT1 *i1Option_name)
{
    UINT4               u4Startindex = 0;
    UINT4               u4Endindex = 0;
    UINT4               u4Tempindex1 = 0;
    UINT4               u4Tempindex2 = 0;
    UINT4               u4Flag = 0;
    INT4                i4Count = 0;
    for (u4Tempindex1 = 0; pu1Syntax[u4Tempindex1]; u4Tempindex1++)
    {
        pu1Syntax[u4Tempindex1] = TOLOWER (pu1Syntax[u4Tempindex1]);
    }

    for (u4Tempindex1 = 0; pu1Syntax[u4Tempindex1]; u4Tempindex1++)
    {
        i1Option_name[u4Tempindex1] = TOLOWER (i1Option_name[u4Tempindex1]);
    }

    u4Tempindex1 = 0;
    while ('\0' != pu1Syntax[u4Tempindex1])
    {
        if (pu1Syntax[u4Tempindex1] == i1Option_name[u4Tempindex2])
        {
            u4Startindex = u4Tempindex1;
            u4Tempindex1++;
            u4Tempindex2++;

            while (('\0' != i1Option_name[u4Tempindex2]) &&
                   (pu1Syntax[u4Tempindex1++] == i1Option_name[u4Tempindex2]))
            {
                u4Flag = 1;
                u4Tempindex2++;
            }

            if (('\0' == i1Option_name[u4Tempindex2]) && (1 == u4Flag))
            {
                break;
            }
            else
            {
                u4Flag = 0;
                u4Tempindex2 = 0;
                u4Startindex = 0;
                u4Tempindex1++;
                continue;
            }
        }
        else
        {
            u4Tempindex1++;
        }
    }

    if ((1 != u4Flag) && ('\0' == pu1Syntax[u4Tempindex1]))
    {
        return CLI_FAILURE;
    }
    /* To find the starting and ending position
     * of the option to be removed
     */
    while (pu1Syntax[u4Startindex] != '[')
    {
        u4Startindex--;
    }
    i4Count = 1;
    u4Endindex = u4Startindex;

    while (i4Count != 0)
    {
        u4Endindex++;
        if (pu1Syntax[u4Endindex] == ']')
        {
            i4Count--;
        }
        else if (pu1Syntax[u4Endindex] == '[')
        {
            i4Count++;
        }
    }
    /* modifying the contents of array eliminating the option */
    for (u4Tempindex2 = u4Startindex, u4Tempindex1 = u4Endindex + 1;
         (pu1Syntax[u4Tempindex1] != '\0'); u4Tempindex2++, u4Tempindex1++)
    {
        pu1Syntax[u4Tempindex2] = pu1Syntax[u4Tempindex1];
    }
    pu1Syntax[u4Tempindex2] = '\0';
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliValidatePositionName                             *
 *                                                                         *
 *     Description   : This function validates whether the given option    *
 *                     position contains the option name given.            *
 *                     optional command position and optional key word     *
 *                     verification will be done only for key words and    *
 *                     not for options containing input arguments          *
 *     Input(s)      : pCommand   - Command whose action number needs      *
 *                                  matched with name                      *
 *                     pOption_name - option to be removed                 *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT4
CliValidatePositionName (t_MMI_CMD * pCommand, INT1 *i1Option_name)
{

    if (NULL != pCommand->i1pToken)
    {
        if (0 == STRCASECMP (pCommand->i1pToken, (CHR1 *) i1Option_name))
        {
            return CLI_SUCCESS;
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliSearchOption                                     * 
 *     Description   : This function searches whether the option to be     *
 *                     removed is present in the command or not            *
 *     Input(s)      : ppCommand  - Option which needs to be removed       *
 *                     ppPrvCmd   - Previous token of option to            *
 *                                  be removed.                            *
 *                     i4Option_position - Position of option to be        *
 *                                   removed                               *
 *                     pOption_name - option to be removed                 *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_TRUE/CLI_FALSE                                  *
 *                                                                         *
 ***************************************************************************/

INT4
CliSearchOption (t_MMI_CMD ** ppCommand, t_MMI_CMD ** ppPrvCmd,
                 INT4 i4Option_position, INT1 *i1Option_name)
{
    INT4                u4Count = 0;
    INT4                i4ReturnVal = CLI_FALSE;
    t_MMI_CMD          *pCommandTemp = NULL;

    while (NULL != *ppCommand)
    {
        u4Count++;
        /*If option's action number matches with option's
         *position given in remove command, then option to
         *be removed is found
         */
        if (((*ppCommand)->i4Action_number == i4Option_position))
        {
            i4ReturnVal = CliValidatePositionName (*ppCommand, i1Option_name);
            if (CLI_SUCCESS == i4ReturnVal)
            {
                return CLI_TRUE;
            }
            else
            {
                return CLI_FALSE;
            }
            break;
        }
        if (1 != u4Count)
        {
            /*if the token has pOptional(which might be the
             *option we want to remove )whose action number
             *is <= the position given in remove command ,set
             *pcommand to pOptional and proceed.
             */
            if ((NULL != (*ppCommand)->pOptional) &&
                ((*ppCommand)->pOptional->i4Action_number <= i4Option_position))
            {
                *ppPrvCmd = *ppCommand;
                *ppCommand = (*ppCommand)->pOptional;
                continue;
            }
            /*if the token has pNext_command(which might be the option
             *we want to remove) whose action number is <= the position
             *given in remove command , set pcommand to pNextCommand and
             *proceed
             */
            else if ((NULL != (*ppCommand)->pNext_command) &&
                     ((*ppCommand)->pNext_command->i4Action_number <=
                      i4Option_position))
            {
                *ppPrvCmd = *ppCommand;
                pCommandTemp = (*ppCommand)->pNext_token;
                /** ex: a b { c { 1 | { 2 | 3 } { 4 | 5 } } [ f ] [ g ],
                 ** where [f] has to be removed,to find the value of ppPrvCmd 
                    */
                while (pCommandTemp != NULL)
                {
                    if (pCommandTemp->pOptional != NULL &&
                        pCommandTemp->pOptional->i4Action_number <=
                        i4Option_position)
                    {
                        *ppPrvCmd = pCommandTemp;
                        pCommandTemp = pCommandTemp->pOptional;
                        continue;
                    }
                    if (pCommandTemp->i4Action_number == i4Option_position)
                    {
                        i4ReturnVal =
                            CliValidatePositionName (pCommandTemp,
                                                     i1Option_name);
                        if (CLI_SUCCESS == i4ReturnVal)
                        {
                            *ppCommand = pCommandTemp;
                            return CLI_TRUE;
                        }
                    }
                    else
                    {
                        pCommandTemp = pCommandTemp->pNext_token;
                    }
                }
                *ppPrvCmd = *ppCommand;
                *ppCommand = (*ppCommand)->pNext_command;
                continue;
            }
        }
        *ppPrvCmd = *ppCommand;
        *ppCommand = (*ppCommand)->pNext_token;
    }
    return CLI_FALSE;
}

/***************************************************************************
 *     Function Name : CliObtainCommandHelp                                *
 *     Description   : This function obtains the help for the command      *
 *                     from which an option has to be removed              *
 *     Input(s)      : pHelp   - Help structure pointer                    *
 *                     i4ActionNumber - Action number of the command.      *
 *     Output        : NONE                                                *
 *     Returns       : NONE                                                *
 ***************************************************************************/
VOID
CliObtainCommandHelp (t_MMI_HELP ** ppHelp, INT4 i4ActionNumber)
{
    while (NULL != *ppHelp)
    {
        if (((*ppHelp)->i4ActNumStart <= i4ActionNumber)
            && ((*ppHelp)->i4ActNumEnd >= i4ActionNumber))
        {
            break;
        }
        *ppHelp = (*ppHelp)->pNext;
    }
    return;
}

/***************************************************************************
 *     Function Name : CliFreeOption                                       *
 *     Description   : This function frees the memory for the              *
 *                     option which has to be removed from the command     *
 *     Input(s)      : ppCommand - Option whose memory has to be released   *
 *                     i4ActionNumber - Action number of the command.      *
 *     Output        : NONE                                                *
 *     Returns       : NONE                                                *
 ***************************************************************************/
VOID
CliFreeOption (t_MMI_CMD ** ppCommand, INT4 i4ActionNumber)
{
    t_MMI_CMD          *pTmpCmd = NULL;
    t_MMI_CMD          *pTmpNextCmd = NULL;
    if (NULL == (*ppCommand)->pNext_command)
    {

        /* In case of command like a b [c d] [ e  f] 
         * where [c d] is to be removed
         */
        while ((*ppCommand)->pNext_token != (*ppCommand)->pOptional)
        {
            if (NULL == (*ppCommand)->pNext_token)
            {
                break;
            }
            pTmpCmd = (*ppCommand)->pNext_token->pNext_token;
            MemReleaseMemBlock (gCliCmdTokenPoolId,
                                (UINT1 *) (*ppCommand)->pNext_token);
            (*ppCommand)->pNext_token = pTmpCmd;
        }
    }
    else
    {
        pTmpCmd = *ppCommand;

        /*
         * in case of command a b { [c d] [e f] | all},
         * where [c d] is to be removed.
         */
        if ((NULL != pTmpCmd->pOptional) &&
            (pTmpCmd->pOptional->i4Action_number != i4ActionNumber))
        {
            while ((*ppCommand)->pNext_token != (*ppCommand)->pOptional)
            {
                if (NULL == (*ppCommand)->pNext_token)
                {
                    break;
                }
                pTmpCmd = (*ppCommand)->pNext_token->pNext_token;
                MemReleaseMemBlock (gCliCmdTokenPoolId,
                                    (UINT1 *) (*ppCommand)->pNext_token);
                (*ppCommand)->pNext_token = pTmpCmd;
            }
        }
        /* In case of command a b [{c d [e f]|{ [e f] }] ,
         * where entire option between [{]} has to be removed
         */
        else
        {
            while (NULL != pTmpCmd->pNext_command)
            {
                while ((*ppCommand)->pNext_token->i4Action_number !=
                       i4ActionNumber)
                {
                    pTmpNextCmd = (*ppCommand)->pNext_token->pNext_token;
                    MemReleaseMemBlock (gCliCmdTokenPoolId,
                                        (UINT1 *) (*ppCommand)->pNext_token);
                    (*ppCommand)->pNext_token = pTmpNextCmd;
                }
                pTmpCmd = pTmpCmd->pNext_command;
            }
        }
    }
    MemReleaseMemBlock (gCliCmdTokenPoolId, (UINT1 *) (*ppCommand));
    return;
}

/***************************************************************************
 *     Function Name : CliModifyCmdPrvOptional                             *
 *     Description   : This function removes the specified option          *
 *                     from the command when its previous token            *
 *                     has an pOptional                                    *
 *     Input(s)      : ppCommand   - Option which needs to be removed       *
 *                     ppPrvCmd    - Previous token of option to            *
 *                                  be removed.                            *
 *                     ppTmpCmd    - Option which needs to be removed       *
 *                                  removed                                *
 *                     i4ActionNumber - Action Number of the command       *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 ***************************************************************************/

VOID
CliModifyCmdPrvOptional (t_MMI_CMD ** ppPrvCmd,
                         t_MMI_CMD ** ppTmpCmd, t_MMI_CMD ** ppCommand,
                         INT4 i4ActionNumber)
{
    t_MMI_CMD          *pFirsttLevelCmd = NULL;
    t_MMI_CMD          *pFirstLevelCmdTkn = NULL;

    /*  e.g a b [c d] [{[e f] [g h]| x y}], where
     *  option to be removed is [e f].
     */
    if (NULL != (*ppTmpCmd)->pNext_command)
    {
        /* If the optional node of the command to be
         * removed is not a "ret" node and entire command
         * within [{ }] is not to be removed
         */

        if ((*ppTmpCmd)->pOptional->i4Action_number != i4ActionNumber)
        {
            (*ppTmpCmd)->pOptional->pNext_command = (*ppTmpCmd)->pNext_command;
        }
    }
    while ((*ppPrvCmd) != (*ppCommand)->pOptional)
    {
        if ((*ppPrvCmd) != NULL && (*ppPrvCmd)->pNext_command != NULL)
        {
            pFirsttLevelCmd = (*ppPrvCmd);
            /* eg a b [ c { 1 | 2 } | e ] [ f ] [ g ], 
             * where [f]has to be removed */
            while (pFirsttLevelCmd != NULL)
            {
                pFirstLevelCmdTkn = pFirsttLevelCmd;
                if (pFirstLevelCmdTkn->pOptional == (*ppTmpCmd))
                {
                    pFirstLevelCmdTkn->pOptional = (*ppTmpCmd)->pOptional;
                }
                CliRemoveAlternateOpts (&pFirstLevelCmdTkn, ppCommand,
                                        ppTmpCmd);
                pFirsttLevelCmd = pFirsttLevelCmd->pNext_command;
            }
            (*ppPrvCmd) = (*ppPrvCmd)->pNext_token;
        }
        else
        {
            if ((*ppPrvCmd) != NULL)
            {
                if ((*ppPrvCmd)->pOptional == (*ppTmpCmd))
                {
                    (*ppPrvCmd)->pOptional = (*ppTmpCmd)->pOptional;
                }
                if ((*ppPrvCmd)->pNext_token != (*ppCommand))
                {
                    (*ppPrvCmd) = (*ppPrvCmd)->pNext_token;
                }
                else
                {
                    (*ppPrvCmd)->pNext_token = (*ppTmpCmd)->pOptional;
                    break;
                }
            }
        }
    }
    return;
}

/***************************************************************************
 *     Function Name : CliRemoveAlternateOpts                              *
 *     Description   : This function removes the specified option          *
 *                     from the command when its previous token            *
 *                     has Mandatory tokens inside an pOptional            *
 *     Input(s)      : ppCommand   - Option which needs to be removed      *
 *                     pFirstLevelCmdTkn- Previous token of option to           *
 *                                  be removed.                            *
 *                     ppTmpCmd    - Option which needs to be removed      *
 *                                  removed                                *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 ***************************************************************************/

VOID
CliRemoveAlternateOpts (t_MMI_CMD ** pFirstLevelCmdTkn,
                        t_MMI_CMD ** ppTmpCmd, t_MMI_CMD ** ppCommand)
{
    t_MMI_CMD          *pSecondLevelCmd = NULL;

    while ((*pFirstLevelCmdTkn)->pNext_token != (*ppCommand) &&
           (*pFirstLevelCmdTkn)->pNext_token != (*ppCommand)->pOptional)
    {
        if ((*pFirstLevelCmdTkn)->pNext_command != NULL)
        {
            pSecondLevelCmd = (*pFirstLevelCmdTkn);
            /* a b [ c { 1 | 2 {3 | 4} } | e ] [ f ] [ g ],
             * where [f]has to be removed */
            while (pSecondLevelCmd != NULL)
            {
                if (pSecondLevelCmd->pOptional == (*ppTmpCmd))
                {
                    pSecondLevelCmd->pOptional = (*ppTmpCmd)->pOptional;
                }
                while (pSecondLevelCmd->pNext_token != (*ppCommand))
                {
                    pSecondLevelCmd = pSecondLevelCmd->pNext_token;
                }
                pSecondLevelCmd->pNext_token = (*ppTmpCmd)->pOptional;
                pSecondLevelCmd = pSecondLevelCmd->pNext_command;
            }
            (*pFirstLevelCmdTkn) = (*pFirstLevelCmdTkn)->pNext_token;
        }
        else
        {
            (*pFirstLevelCmdTkn) = (*pFirstLevelCmdTkn)->pNext_token;
        }
    }
    (*pFirstLevelCmdTkn)->pNext_token = (*ppTmpCmd)->pOptional;
    return;
}

/***************************************************************************
 *     Function Name : CliModifyCmd                                        *
 *     Description   : This function removes the specified option          *
 *                     from the command when its previous token            *
 *                     has an no pOptional                                 *
 *     Input(s)      : ppCommand   - Option which needs to be removed       *
 *                     ppPrvCmd    - Previous token of option to            *
 *                                  be removed.                            *
 *                     ppTmpCmd    - Option which needs to be removed       *
 *                                  removed                                *
 *                     i4ActionNumber - Action Number of the command       *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 ***************************************************************************/
VOID
CliModifyCmd (t_MMI_CMD ** ppPrvCmd,
              t_MMI_CMD ** ppTmpCmd, t_MMI_CMD ** ppCommand,
              INT4 i4ActionNumber)
{
    t_MMI_CMD          *pFirsttLevelCmd = NULL;
    t_MMI_CMD          *pFirstLevelCmdTkn = NULL;

    /* e g. a b {[1] [2]|all},
     * where [1] has to be removed
     */
    if (NULL != (*ppTmpCmd)->pNext_command)
    {
        if ((*ppTmpCmd)->pOptional->i4Action_number != i4ActionNumber)
        {
            (*ppTmpCmd)->pOptional->pNext_command = (*ppTmpCmd)->pNext_command;
        }
    }

    while ((*ppPrvCmd) != (*ppCommand)->pOptional)
    {
        if ((*ppPrvCmd) != NULL && (*ppPrvCmd)->pNext_command != NULL)
        {
            /*
             * in case of command like a b {all|[c] [d] [e]},
             * where [c]  has to be removed
             */
            if ((*ppPrvCmd)->pNext_command == (*ppCommand))
            {
                (*ppPrvCmd)->pNext_command = (*ppCommand)->pOptional;
                break;
            }
            else
            {
                pFirsttLevelCmd = (*ppPrvCmd);
                /* eg a b { 1 | 2 } [ f ] [ g ],
                 * where [f] has to be removed */
                while (pFirsttLevelCmd != NULL)
                {
                    pFirstLevelCmdTkn = pFirsttLevelCmd;
                    CliModifyPrevCmd (&pFirstLevelCmdTkn, ppCommand, ppTmpCmd);
                    if (NULL != pFirstLevelCmdTkn)
                    {
                        if ((*ppCommand)->pOptional != pFirstLevelCmdTkn)
                        {
                            pFirstLevelCmdTkn->pNext_token =
                                (*ppTmpCmd)->pOptional;
                        }
                        pFirsttLevelCmd = pFirsttLevelCmd->pNext_command;
                    }
                }
            }

            (*ppPrvCmd) = (*ppPrvCmd)->pNext_token;
        }
        else
        {
            if ((*ppPrvCmd) != NULL)
            {
                if ((*ppPrvCmd)->pNext_token != (*ppCommand))
                {
                    (*ppPrvCmd) = (*ppPrvCmd)->pNext_token;
                }
                else
                {
                    (*ppPrvCmd)->pNext_token = (*ppTmpCmd)->pOptional;
                    break;
                }
            }
        }
    }
    return;
}

/***************************************************************************
 *     Function Name : CliModifyPrevCmd                                    *
 *     Description   : This function removes the specified option          *
 *                     from the command when its previous token            *
 *                     has mandatory tokens and no pOptional               *
 *     Input(s)      : ppCommand   - Option which needs to be removed      *
 *                     pFirstLevelCmdTkn - Previous token of option to     *
 *                                  be removed.                            *
 *                     ppTmpCmd    - Option which needs to be removed      *
 *                                  removed                                *
 *                     i4ActionNumber - Action Number of the command       *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 ***************************************************************************/
VOID
CliModifyPrevCmd (t_MMI_CMD ** pFirstLevelCmdTkn,
                  t_MMI_CMD ** ppTmpCmd, t_MMI_CMD ** ppCommand)
{
    t_MMI_CMD          *pSecondLevelCmd = NULL;
    while ((*pFirstLevelCmdTkn)->pNext_token != (*ppCommand) &&
           (*pFirstLevelCmdTkn)->pNext_token != (*ppCommand)->pOptional)

    {
        if ((*pFirstLevelCmdTkn)->pNext_command != NULL)
        {
            /*
             * in case of command like a b {all|[c] [d] [e]},
             * where [c]  has to be removed
             */
            if ((*pFirstLevelCmdTkn)->pNext_command == (*ppCommand))
            {
                (*pFirstLevelCmdTkn)->pNext_command = (*ppCommand)->pOptional;
                break;
            }
            else
            {
                pSecondLevelCmd = (*pFirstLevelCmdTkn);
                /* a b { c { 1 | { 2 | 3 } { 4 | 5 } } [ f ] [ g ],
                 * where [f]has to be removed */
                while (pSecondLevelCmd != NULL)
                {
                    while ((pSecondLevelCmd->pNext_token != NULL) &&
                           (pSecondLevelCmd->pNext_token != (*ppCommand)))
                    {
                        pSecondLevelCmd = pSecondLevelCmd->pNext_token;
                    }
                    if ((*ppCommand)->pOptional != pSecondLevelCmd)
                    {
                        pSecondLevelCmd->pNext_token = (*ppTmpCmd)->pOptional;
                    }
                    pSecondLevelCmd = pSecondLevelCmd->pNext_command;
                }
            }
            (*pFirstLevelCmdTkn) = (*pFirstLevelCmdTkn)->pNext_token;
        }
        else
        {
            (*pFirstLevelCmdTkn) = (*pFirstLevelCmdTkn)->pNext_token;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : CliUtilDeleteAllUsersInfo                            */
/*                                                                           */
/* Description        : This function is to delete all the users. This is    */
/*                      to be used only while switching between FIPS mode    */
/*                      and Non-FIPS mode                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS                                          */
/*                                                                           */
/*****************************************************************************/
INT4
CliUtilDeleteAllUsersInfo (VOID)
{
    INT2                i2UserIndex = 0;
    INT2                i2GroupIndex = 0;
    INT2                i2PrivilegeIndex = 0;

    /* Delete all the users and their related files */

    OsixSemTake (gCliSessions.CliUsersFileSemId);
    issDeleteLocalFile ((UINT1 *) CLI_USER_FILE_NAME);
    OsixSemGive (gCliSessions.CliUsersFileSemId);

    OsixSemTake (gCliSessions.CliGroupsFileSemId);
    issDeleteLocalFile ((UINT1 *) CLI_GROUP_FILE_NAME);
    OsixSemGive (gCliSessions.CliGroupsFileSemId);

    OsixSemTake (gCliSessions.CliPrivilegesFileSemId);
    issDeleteLocalFile ((UINT1 *) CLI_PRIVILEGES_FILE_NAME);
    OsixSemGive (gCliSessions.CliPrivilegesFileSemId);

    for (i2UserIndex = 0; i2UserIndex < gCliSessions.i2NofUsers; i2UserIndex++)
    {
        if (gCliSessions.pi1CliUsers[i2UserIndex] != NULL)
        {
            MemReleaseMemBlock (gCliUserMemPoolId,
                                (UINT1 *) gCliSessions.
                                pi1CliUsers[i2UserIndex]);
        }

        gCliSessions.pi1CliUsers[i2UserIndex] = NULL;
    }

    FpamUtlDelAllUsers ();
    gCliSessions.i2NofUsers = 0;

    for (i2GroupIndex = 0; i2GroupIndex < gCliSessions.i2NofGroups;
         i2GroupIndex++)
    {
        MemReleaseMemBlock (gCliGroupMemPoolId,
                            (UINT1 *) gCliSessions.pi1CliGroups[i2GroupIndex]);
        gCliSessions.pi1CliGroups[i2GroupIndex] = NULL;
    }
    gCliSessions.i2NofGroups = 0;

    for (i2PrivilegeIndex = 0; i2PrivilegeIndex < gCliSessions.i2NofPrivileges;
         i2PrivilegeIndex++)
    {
        MemReleaseMemBlock (gCliPrivilegesMemPoolId,
                            (UINT1 *) gCliSessions.
                            pi1CliPrivileges[i2PrivilegeIndex]);
        gCliSessions.pi1CliPrivileges[i2PrivilegeIndex] = NULL;
    }
    gCliSessions.i2NofPrivileges = 0;

    return CLI_SUCCESS;
}

/*******************************************************************************
 *     Function Name : CliGetCommandPriv                                       *
 *     Description   : This function will change privilege of the cli cmd in ISS*
 *     Input(s)      : pModeName - Mode name for the command                   *
 *                     pCmdName  - Command for which privilege to be found     
 *                                                                             *
 *     Output(s)     : pi4CmdPriv -Gives the privilege of the given command    *
 *                                                                             *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                                 *
 *                                                                             *
 ******************************************************************************/
INT1
CliGetCommandPriv (CHR1 ai1ModeName[CLI_COMMAND_MODE_LEN],
                   CHR1 ai1CmdName[CLI_COMMAND_LEN], INT4 *pi4CmdPriv)
{
    tCliContext        *pCliContext = NULL;
    t_MMI_MODE_TREE    *pMode = NULL;

    CHR1               *pu1CommandName = NULL;
    INT4                i4RetPriv = 0;
    INT4                i4MaxPriv = 0;
    t_MMI_MODE_TREE    *pTempMode = NULL;

    if (STRLEN (ai1CmdName) >= (MAX_LINE_LEN - 1))
    {
        return CLI_FAILURE;
    }
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1CommandName, CHR1);
    if (NULL == pu1CommandName)
    {
        return CLI_FAILURE;
    }
    CLI_MEMSET (pu1CommandName, CLI_ZERO, (MAX_LINE_LEN + 1));
    STRCPY (pu1CommandName, (CHR1 *) ai1CmdName);

    /* Get the Apllication Context */
    CliTakeAppContext ();
    pCliContext = CliGetApplicationContext ();
    if (NULL == pCliContext)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        pu1CommandName = NULL;
        return CLI_FAILURE;
    }

    /* return Mode for the given ModeName */
    pMode = CliGetModeFromName ((INT1 *) ai1ModeName);
    if (NULL == pMode)
    {
        /*If mode does not exist return the application context */
        CliGiveAppContext ();
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        pu1CommandName = NULL;
        return CLI_FAILURE;
    }
    pTempMode = pCliContext->pMmiCur_mode;
    pCliContext->pMmiCur_mode = pMode;
    CliUpdatePriv (pMode, pu1CommandName, &i4RetPriv);
    if (i4RetPriv == 0)
    {
        CliGetExtensiveSearch ((UINT1 *) pu1CommandName, &i4MaxPriv);
    }
    if (i4MaxPriv < i4RetPriv)
    {
        i4MaxPriv = i4RetPriv;
    }
    CliRestoreCxtFileInfo (pCliContext);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
    pCliContext->pMmiCur_mode = pTempMode;
    /* Releases the Application context */
    CliGiveAppContext ();

    *pi4CmdPriv = i4MaxPriv;
    return CLI_SUCCESS;
}

/***************************************************************************
 *     Function Name : CliCommandPrivChange                                *
 *                                                                         *
 *     Description   : This function will change privilege of the cli cmd in ISS*
 *                                                                         *
 *     Input(s)      : pModeName - Mode name for the command               *
 *                     pCmdName  - Command to be removed under that mode   *
 *                     u1NewPriv - Privilege to applied for the cmd        *
 *                                                                         *
 *     Output(s)     : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/

INT1
CliCommandPrivChange (INT1 *pModeName, INT1 *pCmdName, INT1 i1NewPriv)
{
    tCliContext        *pCliContext = NULL;
    t_MMI_MODE_TREE    *pMode = NULL;

    t_MMI_HELP         *pHelp = NULL;
    INT4                i4ActionNumber = CLI_ZERO;

    INT1                i1Retval = CLI_FAILURE;
    CHR1               *pu1CommandName = NULL;
    INT2                i2ModeIndex;
    INT1               *pi1TmpCmd = NULL;

    if (STRLEN (pCmdName) >= (MAX_LINE_LEN - 1))
    {
        return CLI_FAILURE;
    }
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1CommandName, CHR1);
    if (NULL == pu1CommandName)
    {
        return CLI_FAILURE;
    }
    CLI_MEMSET (pu1CommandName, CLI_ZERO, (MAX_LINE_LEN + 1));
    STRCPY (pu1CommandName, (CHR1 *) pCmdName);
    pu1CommandName[STRLEN (pu1CommandName)] = '\n';

    /* Get the Apllication Context */
    CliTakeAppContext ();
    pCliContext = CliGetApplicationContext ();
    if (NULL == pCliContext)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        pu1CommandName = NULL;
        return CLI_FAILURE;
    }
    /* return Mode for the given ModeName */
    pMode = CliGetModeFromName (pModeName);
    if (NULL == pMode)
    {
        /*If mode does not exist return the application context */
        CliGiveAppContext ();
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);
        pu1CommandName = NULL;
        return CLI_FAILURE;
    }
    pCliContext->pMmiCur_mode = pMode;
    pHelp = pMode->pHelp;

    /*Find action number and set corresponding PRVID */
    i1Retval = mmi_parse (pCliContext, (INT1 *) pu1CommandName,
                          &i4ActionNumber, &i2ModeIndex, &pi1TmpCmd, FALSE);
    CliRestoreCxtFileInfo (pCliContext);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CommandName);

    /* Releases the Application context */
    CliGiveAppContext ();
    if (i1Retval > 0)
    {

        while (pHelp != NULL)
        {
            if ((pHelp->i4ActNumStart <= i4ActionNumber)
                && (pHelp->i4ActNumEnd >= i4ActionNumber))
            {
                break;
            }
            pHelp = pHelp->pNext;
        }

        if (pHelp != NULL)
        {
            /* set the privilege to user given value */
            pHelp->pPrivilegeId[i4ActionNumber - pHelp->i4ActNumStart]
                = i1NewPriv;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliSetSpecialPriv                                    */
/*                                                                           */
/* Description        : SpecialPriv with Choice of Privilege Id based or     */
/*                      Username based Authentication in CLI                 */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pCliSpecialPriv -Option to set Privilege Id based or */
/*                      Username based Authentication                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CliSetSpecialPriv (tCliSpecialPriv * pCliSpecialPriv)
{
    if (NULL != pCliSpecialPriv)
    {
        gCliSpecialPriv.b1UsernameBased = pCliSpecialPriv->b1UsernameBased;
        gCliSpecialPriv.b1PrivilegeBased = pCliSpecialPriv->b1PrivilegeBased;
        gCliSpecialPriv.u1PrivilegeId = pCliSpecialPriv->u1PrivilegeId;
        STRCPY (gCliSpecialPriv.au1UserName, pCliSpecialPriv->au1UserName);
        return;
    }
    return;

}

/*****************************************************************************/
/* Function Name      : CliGetSpecialPriv                                    */
/*                                                                           */
/* Description        : SpecialPriv with Choice of Privilege Id based or     */
/*                      Username based Authentication in CLI                 */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pCliSpecialPriv -Option to get Privilege Id based or */
/*                      Username based Authentication                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CliGetSpecialPriv (tCliSpecialPriv * pCliSpecialPriv)
{
    if (NULL != pCliSpecialPriv)
    {
        pCliSpecialPriv->b1UsernameBased = gCliSpecialPriv.b1UsernameBased;
        pCliSpecialPriv->b1PrivilegeBased = gCliSpecialPriv.b1PrivilegeBased;
        pCliSpecialPriv->u1PrivilegeId = gCliSpecialPriv.u1PrivilegeId;
        STRCPY (pCliSpecialPriv->au1UserName, gCliSpecialPriv.au1UserName);
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : CliCheckSpecialPriv                                  */
/*                                                                           */
/* Description        : This function will check if the Authentication in CLI*/
/*                      is Privilege Id based or Username based              */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
CliCheckSpecialPriv (VOID)
{
    tCliContext        *pCliContext;
    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return (CLI_FAILURE);
    }

    if (gCliSpecialPriv.b1UsernameBased == TRUE)
    {
        if (STRCMP (pCliContext->ai1UserName, gCliSpecialPriv.au1UserName) == 0)
        {
            return CLI_SUCCESS;
        }
    }
    if (gCliSpecialPriv.b1PrivilegeBased == TRUE)
    {
        if (pCliContext->i1PrivilegeLevel >= gCliSpecialPriv.u1PrivilegeId)
        {
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;
}

/**************************************************************************
*     Function Name : CliGetInputFd                                       *
*                                                                         *
*     Description   : This function will return the input socket/file     *
*                     descriptor of the current CLI context from where the*
*                     inputs should be read                               *
*                                                                         *
*     Input(s)      : None                                                *
*                                                                         *
*     Output(s)     : Input Socket descriptor                             *
*                                                                         *
*     Returns       : Input Socket descriptor/ -1 on failure              *
*                                                                         *
***************************************************************************/
INT4
CliGetInputFd (VOID)
{
    tCliContext        *pContext = NULL;
    pContext = CliGetContext ();

    if (pContext == NULL)
    {
        return -1;
    }

    if (pContext->gu4CliMode == CLI_MODE_SSH)
    {
#ifdef SSH_WANTED
        return SshArGetSockfd (pContext->i4ClientSockfd);
#else
        return -1;
#endif
    }
    /* For Telnet and Console Mode fetch the InputFd directly from 
     * the Cli context */
    else
    {
        return pContext->i4InputFd;
    }
}

/**************************************************************************
*     Function Name : CliReadFromCliConsole                               *
*                                                                         *
*     Description   : This function is to read the inputs from the        *
*                     current context's ISS console                       *
*                                                                         *
*     Input(s)      : pu1Buf - Pointer to the Buffer to store the read    *
*                              Input message                              *
*                                                                         *
*     Output(s)     : Input message received                              *
*                                                                         *
*     Returns       : Number of bytes read/ -1 on failure                 *
*                                                                         *
***************************************************************************/
INT4
CliReadFromCliConsole (UINT1 *pu1Buf)
{
    tCliContext        *pCliContext = NULL;
    INT4                i4ReadCount = 0;

    pCliContext = CliGetContext ();

    if (pCliContext == NULL)
    {
        return -1;
    }

    if ((pCliContext->gu4CliMode == CLI_MODE_SSH) ||
        (pCliContext->gu4CliMode == CLI_MODE_CONSOLE))
    {
        *pu1Buf = pCliContext->fpCliInput (pCliContext);
        /* The call back functions CliSshRead and CliSerialRead register 
           with CliContext for SSH mode and Console mode will read and 
           return one character at a time */
        i4ReadCount = 1;
    }
    /* The call back function CliTelnetRead registered with CliContext for 
     * telnet mode will be waiting (blocking) till it reads a valid character
     * (excluding '\0', window-size mesages(255)). Since it is not needed in this
     * case the same function has been little bit modified and added here
     */
    else
    {
        i4ReadCount = CliSockRecv (pCliContext->i4InputFd, pu1Buf, 1);
        if (i4ReadCount <= 0)
        {
            mmi_exit ();
            pCliContext->mmi_exit_flag = TRUE;
            return -1;
        }
        if (*pu1Buf == 0)
        {
            i4ReadCount = 0;
        }

        /* For Telnet Sessions, '\n' is represented by '\r' followed by
         * '\n'. So replacing '\r' by '\n' and discard '\n' */

        else if (*pu1Buf == '\r')
        {
            *pu1Buf = '\n';
        }
        else if (*pu1Buf == CLI_WINDOW_SIZE_MESSAGE)
        {
            /* Handle window size negotiation messages for telnet */
            CliHandleOutOfBandData (pCliContext);
            *pu1Buf = 0;
            i4ReadCount = 0;
        }
    }
    return i4ReadCount;
}

/**************************************************************************
*     Function Name : CliWriteInCliConsole                                *
*                                                                         *
*     Description   : This function is to write the outputs in the        *
*                     current context's ISS console                       *
*                                                                         *
*     Input(s)      : pMessage - Pointer to the output message to be      *
*                                written                                  *
*                     i4len    - Length of the message to be written      *
*                                                                         *
*     Output(s)     : Number of bytes written                             *
*                                                                         *
*     Returns       : Number of bytes written/ -1 on failure              *
*                                                                         *
***************************************************************************/
INT4
CliWriteInCliConsole (UINT1 *pMessage, INT4 i4Len)
{
    tCliContext        *pContext = NULL;

    pContext = CliGetContext ();

    if (pContext == NULL)
    {
        return -1;
    }
    if (pContext->fpCliTempOutput (pContext, (CONST CHR1 *) pMessage,
                                   (UINT4) i4Len) == CLI_SUCCESS)
    {
        return i4Len;
    }
    return -1;
}

/***************************************************************/
/*  Function Name   : CliGetSSHSessionId                       */
/*  Description     : This function is to get SSH session-id   */
/*                    from the current context.                */
/*                    In case of failure, return application   */
/*                    id (1). This condition should not occur  */
/*                    or even if occurs this 1 will not be     */
/*                    used in calling locations. This is just  */
/*                    to avoid static analyser tool warnings   */
/*                                                             */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : SSH session id or application id (1)     */
/*                    in case of failure                       */
/***************************************************************/
UINT4
CliGetSSHSessionId (VOID)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found. Since a positive number to be
         * returned, use 1 which is application context
         * Application context will not be impacted though in
         * calling location. but to avoid tool warning, return
         * a session id which will not affect other ssh sessions
         */
        return 1;
    }
    return ((UINT4) (pCliContext->i4ConnIdx));
}

/**************************************************************************
*     Function Name : CliGetSessionId                                     *
*                                                                         *
*     Description   : This function will return the CLI session identifier*
*                                                                         *
*     Input(s)      : None                                                *
*                                                                         *
*     Output(s)     : CLI session identifier                              *
*                                                                         *
*     Returns       : CLI session identifier/ -1 on failure               *
*                                                                         *
***************************************************************************/
INT4
CliGetSessionId (VOID)
{
    tCliContext        *pContext = NULL;

    pContext = CliGetContext ();
    if (pContext == NULL)
    {
        return -1;
    }
    return pContext->i4ConnIdx;
}

/**************************************************************************
*     Function Name : CliContextInitToDefault                             *
*                                                                         *
*     Description   : This function will set the default values for       *
*                     CliContext                                          *
*                                                                         *
*     Input(s)      : None                                                *
*                                                                         *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
*                                                                         *
***************************************************************************/

INT4
CliContextInitToDefault (VOID)
{
    tCliContext        *pCliContext = NULL;
    INT2                i2TempPrivId = 0;
    INT4                i4DefExecTimeOut = 0;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    CliIssGetDefaultExecTimeOut (&i4DefExecTimeOut);

    pCliContext->i4SessionTimer = i4DefExecTimeOut;

    /* Setting the privillage id to Default value */
    if (FpamGetPrivilege
        ((CHR1 *) pCliContext->ai1UserName, &i2TempPrivId) != FPAM_FAILURE)
    {
        pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
    }

    return CLI_SUCCESS;
}/**************************************************************************

*     Function Name : CliLogAuditDisableStatus                            *
*                                                                         *
*     Description   : This function is to log the 'audit-logging disable' *
*                     command, even when audit logging status is disable  *
*     Input(s)      : None                                                *
*                                                                         *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : None                                                *
*                                                                         *
***************************************************************************/

VOID
CliLogAuditDisableStatus (VOID)
{

    tAuditInfo         *pTempAudit = NULL;
    tCliContext        *pCliContext;
    time_t              t;
    struct tm          *tp;
    CHR1               *pTime = NULL;
    UINT4               u4Len = 0;

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        /* No Task Found */
        return;
    }

    pTempAudit = (tAuditInfo *) MemAllocMemBlk (gAuditInfoMemPoolId);
    if (pTempAudit == NULL)
    {
        return;
    }
    MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
    STRCPY (pTempAudit->TempCmd.ai1UserName, "Audit:");
    STRCAT (pTempAudit->TempCmd.ai1UserName, pCliContext->ai1UserName);

    t = time (NULL);

    STRCPY (pTempAudit->TempCmd.au1TempCmd, "audit-logging disable");
    pTempAudit->TempCmd.u4CmdStatus = pCliContext->u4CmdStatus;
    pTempAudit->TempCmd.u4CliMode = pCliContext->gu4CliMode;

    tp = localtime (&t);
    if (tp != NULL)
    {
        pTime = asctime (tp);
        if (pTime != NULL)
        {
            u4Len = ((STRLEN (pTime) < sizeof (pTempAudit->au1Time)) ?
                     STRLEN (pTime) : sizeof (pTempAudit->au1Time) - 1);
            STRNCPY (pTempAudit->au1Time, pTime, u4Len);
            pTempAudit->au1Time[u4Len] = '\0';
        }
    }
    pTempAudit->i2Flag = AUDIT_CLI_MSG;
    if (pCliContext->gu4CliMode != CLI_MODE_CONSOLE)
    {
        pTempAudit->TempCmd.u4ClientIpAddr = pCliContext->au4ClientIpAddr[3];
    }
    MSRSendAuditInfo (pTempAudit);
    MemReleaseMemBlock (gAuditInfoMemPoolId, (UINT1 *) pTempAudit);
}

/***************************************************************************
 *     Function Name : CliGetExtensiveSearch                               *
 *                                                                         *
 *     Description   : This function will get the mode for the given name  *
 *                                                                         *
 *     Input(s)      : pi1ModeName - Mode name                             *
 *                                                                         *
 *     Output        : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/

INT1
CliGetExtensiveSearch (UINT1 *pu1CommandName, INT4 *pi4PrivId)
{
    t_MMI_MODE_TREE    *apTstack[MAX_MODE_LEVEL] = { NULL };
    t_MMI_MODE_TREE    *pTsearch = NULL;
    INT1                i1Top = CLI_ZERO;
    INT4                i4PrivId = -1;

    apTstack[i1Top] = gCliSessions.p_mmi_root_mode;
    while (i1Top >= 0)
    {
        /* checking the stack empty */
        pTsearch = apTstack[i1Top];
        i1Top--;
        while (pTsearch != NULL)
        {
            if (CliUpdatePriv (pTsearch, (CHR1 *) pu1CommandName, &i4PrivId) ==
                CLI_SUCCESS)
            {
                *pi4PrivId = i4PrivId;
                return CLI_SUCCESS;
            }
            if (pTsearch->pChild != NULL)
            {
                i1Top++;
                apTstack[i1Top] = pTsearch->pNeighbour;
                pTsearch = pTsearch->pChild;
            }
            else
            {
                if (i1Top < 0)
                {
                    pTsearch = NULL;
                }
                else
                {
                    pTsearch = pTsearch->pNeighbour;

                }
            }
        }
    }
    return CLI_FAILURE;
}

/**************************************************************************
*     Function Name : CliGetActiveCliUsers                                *
*                                                                         *
*     Description   : This function is to read number of active cli       *
*                     sessions. This includes only console login, telnet  *
*                     login and ssh login sessions.                             *
*     Input(s)      : None                                                *
*                                                                         *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : Active session count                                *
*                                                                         *
***************************************************************************/
UINT4
CliGetActiveSessions (VOID)
{
    INT1                i1Index = 0;
    UINT4               u4ActiveSessions = 0;
    tCliContext        *pCliContext;

    for (i1Index = 0; i1Index < CLI_MAX_SESSIONS; i1Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i1Index]);
        if (pCliContext->i1Status == CLI_ACTIVE)
        {
            if (pCliContext->gu4CliMode != CLI_MODE_APP)
                u4ActiveSessions++;
        }
    }
    return u4ActiveSessions;
}

/**************************************************************************
*     Function Name : CliUpdatePriv                                       *
*                                                                         *
*     Description   : This function checks the privilege for each         *
*                     command and restricts user to access only the       *
*                     pages for which the user is privileged.             *
*                                                                         *
*                                                                         *
*     Input(s)      : pTsearch - Ponter to the search tree                *
*                     pu1CommandName - Command name                       *
*                     pi4PrivId - Pointer to privilege Id                 *
*                                                                         *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS / CLI_FAILURE                           *
*                                                                         *
***************************************************************************/

INT1
CliUpdatePriv (t_MMI_MODE_TREE * pTsearch, CHR1 * pu1CommandName,
               INT4 *pi4PrivId)
{

    INT4                i4ActionNumber = CLI_ZERO;

    t_MMI_HELP         *pHelp = NULL;

    pHelp = pTsearch->pHelp;

    while (pHelp != NULL)
    {
        for (i4ActionNumber = 0; pHelp->pSyntax[i4ActionNumber] != NULL;
             i4ActionNumber++)
        {
            /* Check if this command is removed already */
            if (pHelp->pSyntax[i4ActionNumber] != CLI_REM_CMD)
            {

                if (STRCMP (pHelp->pSyntax[i4ActionNumber], pu1CommandName) ==
                    0)
                {
                    /* Gets the privilege for the given command */
                    *pi4PrivId = pHelp->pPrivilegeId[i4ActionNumber];
                    return CLI_SUCCESS;
                }
            }
        }
        pHelp = pHelp->pNext;
    }
    return CLI_FAILURE;
}

/**************************************************************************
*     Function Name : CliModifyMoreFlag                                   *
*                                                                         *
*     Description   : This function will set the default MoreFlag to      *
*                     either enable or disable depending the input value  *
*                     p1Flag                                               *
*                                                                         *
*                                                                         *
*     Input(s)      : CliHandle                                           *
*                     p1Flag - Pointer containing More Flag Option        *
*                                                                         *
*                                                                         *
*     Output(s)     : p1Flag - Pointer containing More Flag Option        *
*                                                                         *
*     Returns       : NONE                                                *
*                                                                         *
***************************************************************************/

VOID
CliModifyMoreFlag (tCliHandle CliHandle, INT1 *p1Flag)
{
    tCliContext        *pCliContext = NULL;

    pCliContext = CliGetContext ();

    if (pCliContext == NULL)
    {
        return;
    }
    if (*p1Flag == OSIX_FALSE)
    {
        if (pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE)
        {
            *p1Flag = pCliContext->i1DefaultMoreFlag;
            CliSetDefaultMoreFlag (CliHandle, CLI_MORE_DISABLE);
            if ((pCliContext->i1PipeFlag) &&
                (pCliContext->i4FileFlag == OSIX_FALSE))
            {
                CliDeletePipe (pCliContext);
            }
        }
    }
    else
    {
        *p1Flag = CLI_MORE_DISABLE;
        CliSetDefaultMoreFlag (CliHandle, CLI_MORE_ENABLE);
    }
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetRange                                         *
 *                                                                         *
 *     Description   : The function is used by other modules to            *
 *                     get the starting,ending index from given range.     *
 *                                                                         *
 *     Input(s)      : pu1Str         : Pointer to the string.             *
 *                     pu4StartIndex  : Pointer to start index             *
 *                     pu4StopIndex   : Pointer to end index               *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : OSIX_SUCCESS/OSIX_FAILURE.                          *
 *                                                                         *
 ***************************************************************************/
INT4
CliGetRange (UINT1 *pu1Str, UINT4 *pu4StartIndex, UINT4 *pu4StopIndex)
{
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT4                i4RetType;
    INT1                i1EndFlag = OSIX_FALSE;

    if (!(pi1Temp))
        i4RetStatus = OSIX_FAILURE;

    while ((i4RetStatus != OSIX_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDPORT_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }

        if (!(*pi1Pos))
            i1EndFlag = OSIX_TRUE;

        *pi1Pos = '\0';

        /* Get values between the list seperaters based on the type */
        i4RetType = CliGetVal (&CliPortList, pi1Temp);

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                *pu4StartIndex = CliPortList.uPortList.u4PortNum;
                *pu4StopIndex = CliPortList.uPortList.u4PortNum;
                break;
            case CLI_LIST_TYPE_RANGE:
                *pu4StartIndex = CliPortList.uPortList.PortListRange.u4PortFrom;
                *pu4StopIndex = CliPortList.uPortList.PortListRange.u4PortTo;
                break;
            default:
                i4RetStatus = OSIX_FAILURE;
                continue;
        }

        if (*pu4StartIndex > *pu4StopIndex)
        {
            i4RetStatus = OSIX_FAILURE;
            break;
        }
        pi1Temp = pi1Pos + 1;
    }
    return i4RetStatus;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetSkipDoubleBang                                *
 *                                                                         *
 *     Description   : This  function is called from ISS                   *
 *                     module to set the value of SkipDoubleBang           *
 *                     in CLI                                              *
 *     Input (s)     : u1EscBangFlag  - 0 or 1 or 2                        *
 *                     0 - To reset the u1EscBangFlag                      *
 *                     1 - To set the Entering point to check "!" in       *
 *                         "show run" output                               *
 *                     2 - To set the Exiting point to in "show run" output*
 *     Output(s)     : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/

INT4
CliSetSkipDoubleBang (UINT1 u1SkipDoubleBangVal)
{
    tCliContext        *pCliContext;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->u1SkipDoubleBang = u1SkipDoubleBangVal;

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliGetShowCmdOutputToFile                            */
/*                                                                           */
/* Description        : It Handles the execution of show commands and        */
/*                      redirecting the output to the given file.            */
/*                                                                           */
/* Input(s)           : *pu1FileName, *pu1CmdName                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliGetShowCmdOutputToFile (UINT1 *pu1FileName, UINT1 *pu1CmdName)
{
    tCliContext        *pCliContext = NULL;
    UINT4               u4Len;
    UINT1               u1HistTop = 0;

    ISS_UNLOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    STRNCPY (gau1Command, pu1CmdName, CLI_AUDIT_MAX_COMMAND_LEN);
    STRNCPY (gau1Command + STRLEN (pu1CmdName), pu1FileName,
             CLI_AUDIT_MAX_FILE_NAME_LEN);
    u4Len = STRLEN (gau1Command);

    pCliContext = &(gCliSessions.gaCliContext[0]);
    STRCPY (gau1Command + u4Len, ";\n\t");

    u1HistTop = (UINT1) pCliContext->mmi_hist_top;
    if (CliExecuteAppCmd ("end") == CLI_FAILURE)
    {
        MGMT_LOCK ();
        CliGiveAppContext ();
        ISS_LOCK ();
        return CLI_FAILURE;
    }
    if (CliExecuteAppCmd ((CONST CHR1 *) gau1Command) == CLI_FAILURE)
    {
        MGMT_LOCK ();
        CliGiveAppContext ();
        ISS_LOCK ();
        return CLI_FAILURE;
    }

    CliGiveAppContext ();
    MGMT_LOCK ();
    ISS_LOCK ();

    /* Do not update the command in history */
    pCliContext->mmi_hist_top = u1HistTop;
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliExecuteCmdFromFile                                */
/*                                                                           */
/* Description        : This function is used to execute the list of         */
/*                      commands which are available in the input file       */
/*                                                                           */
/* Input(s)           : *pu1CmdFile - List of commands availble in the file  */
/*                                    for execution                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliExecuteCmdFromFile (UINT1 *pu1CmdFile)
{
    INT4                i4TempReadFd = 0;
    INT1                ai1CmdBuf[MAX_LINE_LEN + 4];
    tCliContext        *pCliContext = NULL;
    UINT1               u1HistTop = 0;

    MEMSET (ai1CmdBuf, 0, sizeof (ai1CmdBuf));

    CliTakeAppContext ();
    MGMT_UNLOCK ();

    pCliContext = &(gCliSessions.gaCliContext[1]);

    u1HistTop = (UINT1) pCliContext->mmi_hist_top;

    if ((i4TempReadFd =
         CliOpenFile ((const CHR1 *) pu1CmdFile, CLI_RDONLY)) < 0)
    {
        mmi_printf ("\rUnable to open File: %s\r\n", pu1CmdFile);
        MGMT_LOCK ();
        CliGiveAppContext ();
        return CLI_FAILURE;
    }

    if (CliExecuteAppCmd ("end") == CLI_FAILURE)
    {
        MGMT_LOCK ();
        CliGiveAppContext ();
        return CLI_FAILURE;
    }

    /* if the file is not in the CLI format, return Success */
    if ((CliGetLine (i4TempReadFd, ai1CmdBuf)) != CLI_EOF &&
        (ai1CmdBuf[0] != '\n'))
    {
        MGMT_LOCK ();
        CliGiveAppContext ();
        return CLI_FAILURE;
    }
    while ((CliGetLine (i4TempReadFd, ai1CmdBuf)) != CLI_EOF)
    {
        if (CliExecuteAppCmd ((CONST CHR1 *) ai1CmdBuf) == CLI_FAILURE)
        {
            mmi_printf ("\rUnable to execute the command: %s\r\n", ai1CmdBuf);
        }
    }
    if (CliCloseFile (i4TempReadFd) == CLI_FAILURE)
    {
        MGMT_LOCK ();
        CliGiveAppContext ();
        return CLI_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Do not update the command in history */
    pCliContext->mmi_hist_top = u1HistTop;
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliUtilMgmtLockStatus                                */
/*                                                                           */
/* Description        : This util is used to set the global Boolean variable */
/*                      MGMT_LOCK is acquired and its is set false when      */
/*                      MGMT_LOCK is released.                               */
/*                                                                           */
/* Input(s)           : b1Flag                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
CliUtilMgmtLockStatus (BOOL1 b1Flag)
{
    gb1MgmtLockStatus = b1Flag;
    return;
}

/*****************************************************************************/
/* Function Name      : CliUtilMgmtClearLockStatus                          */
/*                                                                           */
/* Description        : This util is used to clear the mgmt lock status if   */
/*                      the mgmt lock is already acquired.                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
CliUtilMgmtClearLockStatus ()
{
    if (gb1MgmtLockStatus == OSIX_TRUE)
    {
        MGMT_UNLOCK ();
    }
    return;
}

/***************************************************************************
* FUNCTION NAME : CliCompareExactSubset
* DESCRIPTION   : This function is used to compare the user given cli string
*                 with the Input syntax of a command. The following cli
*                 command matches are handled in the function:
*                1. User input exactly matches the syntax.
*                2. single user input : <user_input> is left unchecked.
*                3. user input with space : <user   input>
*                4. optional input : [input]
*                5. optional input with user input : [<userinput>]
*                6. optional input enclosing compulsory and
*                            user input : [text <user_input>]
*                7. predefined input with option : {option_1 | option_2}
*                8. optional but predefined input : [{ option1 | option2 }]
* INPUT         : pointer to entered string
*                 pointer to a string of allowed command
* RETURNS       : TRUE - if command matches
*                 FALSE - if no matched
***************************************************************************/
BOOL1
CliCompareExactSubset (INT1 *i1pUserInput, CONST INT1 *i1pTestInput)
{
    UINT4               u4Count = 0;
    UINT4               u4InputCount = 0;
    UINT4               u4StringCount = 0;
    UINT4               u4LoopCount = 0;
    BOOL1               b1MatchFoundLoop = FALSE;
    BOOL1               b1OpenUserDefined = FALSE;
    BOOL1               b1OpenOptional = FALSE;
    BOOL1               b1OpenOptionalMatchFound = FALSE;
    BOOL1               b1OpenFlower = FALSE;
    BOOL1               b1OpenFlowerMatchFound = FALSE;
    BOOL1               b1OpenOptionalFlower = FALSE;
    BOOL1               b1OpenOptionalFlowerMatchFound = FALSE;

    MEMSET (ba1MatchFound, 0, CLI_MAX_HELP_TOKEN_NUM);
    MEMSET (apu1InputToken, 0, sizeof (apu1InputToken));
    MEMSET (apu1StringToken, 0, sizeof (apu1StringToken));
    MEMSET (au1UsrStr, 0, sizeof (au1UsrStr));
    MEMSET (au1LocalStr, 0, sizeof (au1LocalStr));

    if (NULL != i1pUserInput)
    {
        if (STRLEN (i1pUserInput) >= CLI_MAX_HELP_LINE_LEN)
        {
            STRNCPY (au1UsrStr, i1pUserInput, CLI_MAX_HELP_LINE_LEN);
        }
        else
        {
            STRNCPY (au1UsrStr, i1pUserInput, STRLEN (i1pUserInput));
        }
    }
    au1UsrStr[STRLEN (au1UsrStr) - 1] = '\0';

    if (NULL != i1pTestInput)
    {
        if (STRLEN (i1pTestInput) >= CLI_MAX_HELP_LINE_LEN)
        {
            STRNCPY (au1LocalStr, i1pTestInput, CLI_MAX_HELP_LINE_LEN);
        }
        else
        {
            STRNCPY (au1LocalStr, i1pTestInput, STRLEN (i1pTestInput));
        }
    }

    /* Return true in Scenario of exact match */
    if (0 == STRCMP (au1LocalStr, au1UsrStr))
    {
        return TRUE;
    }
    else
    {
        /* Tokenise the user-input and search string */
        u4InputCount = 0;
        apu1InputToken[u4InputCount] = STRTOK (au1UsrStr, " ");
        while (NULL != apu1InputToken[u4InputCount])
        {
            u4InputCount++;
            apu1InputToken[u4InputCount] = STRTOK (NULL, " ");
        }
        u4StringCount = 0;
        apu1StringToken[u4StringCount] = STRTOK (au1LocalStr, " ");
        while (NULL != apu1StringToken[u4StringCount])
        {
            u4StringCount++;
            apu1StringToken[u4StringCount] = STRTOK (NULL, " ");
        }

        if (u4InputCount > u4StringCount)
        {
            /* The user given commands has more tokens than syntax and so
             * presumably the syntax is not a match */
            return FALSE;
        }
        else
        {
            u4InputCount = 0;
            u4StringCount = 0;
            u4Count = 0;

            /* Loop to compare the user input tokens with the syntax tokens 
             * until the last user input token is matched, as in case of successful match
             * or until the input token that is mismatched with the syntax ,
             * as in case of unmatched syntax */
            while ((NULL != apu1InputToken[u4InputCount])
                   && (NULL != apu1StringToken[u4StringCount]))
            {
                /* Exact match */
                if (0 == STRNCMP (apu1InputToken[u4InputCount],
                                  apu1StringToken[u4StringCount],
                                  STRLEN (apu1StringToken[u4StringCount])))
                {
                    ba1MatchFound[u4InputCount] = TRUE;
                    u4InputCount++;
                    u4StringCount++;
                }
                /* To handle <user_input> case, We dont check and go for the next token */
                else if ((NULL !=
                          STRSTR (apu1StringToken[u4StringCount], "<"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "["))
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount],
                                     ">"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "]")) && (FALSE == b1OpenOptionalFlower))
                {
                    ba1MatchFound[u4InputCount] = TRUE;
                    u4InputCount++;
                    u4StringCount++;
                }
                /* to hand <user   input> case (part 1) */
                else if ((NULL !=
                          STRSTR (apu1StringToken[u4StringCount], "<"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "["))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     ">"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "]")) && (FALSE == b1OpenOptionalFlower))
                {
                    b1OpenUserDefined = TRUE;
                    u4StringCount++;
                }
                /* to handle <user input> case (part 2) */
                else if ((NULL ==
                          STRSTR (apu1StringToken[u4StringCount], "<"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "["))
                         && (TRUE == b1OpenUserDefined)
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount],
                                     ">"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "]")) && (FALSE == b1OpenOptionalFlower))
                {
                    b1OpenUserDefined = FALSE;
                    u4StringCount++;
                    u4InputCount++;
                }
                /* to handle [input] case */
                else if ((NULL !=
                          STRSTR (apu1StringToken[u4StringCount], "["))
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount], "]")))
                {
                    /* handles [<userinput>] */
                    if ((NULL !=
                         STRSTR (apu1StringToken[u4StringCount], "<"))
                        && (NULL !=
                            STRSTR (apu1StringToken[u4StringCount], ">")))
                    {
                        /* Check the user token against all other tokens
                         * if match is found then proceed ,
                         * if not found increement both counts */

                        u4LoopCount = u4StringCount;
                        b1MatchFoundLoop = FALSE;
                        while (NULL != apu1StringToken[u4LoopCount])
                        {
                            /* STRSTR or STRCMP need to ccheck */
                            if (NULL !=
                                STRSTR (apu1StringToken[u4LoopCount], ">"))
                            {
                                b1MatchFoundLoop = TRUE;
                                break;
                            }
                            u4LoopCount++;
                        }

                        if (TRUE == b1MatchFoundLoop)
                        {
                            u4LoopCount++;
                            ba1MatchFound[u4InputCount] = TRUE;
                            u4InputCount++;
                            u4StringCount = u4LoopCount;
                        }
                        else
                        {
                            u4StringCount++;
                            u4InputCount++;
                        }

                    }
                    /* handle [input] */
                    else
                    {
                        if (NULL !=
                            STRSTR (apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            ba1MatchFound[u4InputCount] = TRUE;
                            u4InputCount++;
                        }
                        u4StringCount++;
                    }

                }
                /* to handle [text <user_input>]    part 1 */
                else if ((NULL !=
                          STRSTR (apu1StringToken[u4StringCount], "["))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "]"))
                         && (FALSE == b1OpenUserDefined)
                         && (FALSE == b1OpenOptional)
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     ">"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "<"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "{"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount], "}")))

                {

                    b1OpenOptional = TRUE;
                    if (NULL != STRSTR (apu1StringToken[u4StringCount],
                                        apu1InputToken[u4InputCount]))
                    {
                        b1OpenOptionalMatchFound = TRUE;
                        u4InputCount++;
                    }
                    u4StringCount++;
                }
                /* to handle [text <user_input>]    part 2 */
                else if ((NULL ==
                          STRSTR (apu1StringToken[u4StringCount], "["))
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount],
                                     "]"))
                         && (FALSE == b1OpenUserDefined)
                         && (TRUE == b1OpenOptional)
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount],
                                     ">"))
                         && (NULL !=
                             STRSTR (apu1StringToken[u4StringCount],
                                     "<"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount],
                                     "{"))
                         && (NULL ==
                             STRSTR (apu1StringToken[u4StringCount], "}")))
                {
                    b1OpenOptional = FALSE;
                    if (TRUE == b1OpenOptionalMatchFound)
                    {
                        ba1MatchFound[u4InputCount] = TRUE;
                        u4InputCount++;
                    }
                    u4StringCount++;
                }
                /* Logic to parse { { or  [  is yet to be added so we ignore it for now */
                else if ((((TRUE == b1OpenFlower)
                           || (TRUE == b1OpenOptionalFlower))
                          && (NULL !=
                              STRSTR (apu1StringToken[u4StringCount],
                                      "{")))
                         ||
                         ((NULL !=
                           STRSTR (apu1StringToken[u4StringCount], "["))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      ">"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "<"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "{"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount], "}"))))
                {
                    break;
                }
                /* to  handle simple case {option_1 | option_2}  part 1 */
                else if (((NULL ==
                           STRSTR (apu1StringToken[u4StringCount], "["))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "]"))
                          && (FALSE == b1OpenUserDefined)
                          && (FALSE == b1OpenOptional)
                          && (FALSE == b1OpenFlower)
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      ">"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "<"))
                          && (NULL !=
                              STRSTR (apu1StringToken[u4StringCount],
                                      "{"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "}")))
                         ||
                         ((NULL !=
                           STRSTR (apu1StringToken[u4StringCount], "{"))
                          && (NULL !=
                              STRSTR (apu1StringToken[u4StringCount], "["))))
                {
                    if (NULL != STRSTR (apu1StringToken[u4StringCount], "["))
                    {
                        b1OpenOptional = TRUE;
                    }

                    b1OpenFlower = TRUE;
                    if (NULL != STRSTR (apu1StringToken[u4StringCount],
                                        apu1InputToken[u4InputCount]))
                    {
                        ba1MatchFound[u4InputCount] = TRUE;
                        u4InputCount++;
                        b1OpenFlowerMatchFound = TRUE;
                    }
                    u4StringCount++;
                }

                /* to  handle simple case {option_1 | option_2}  part 2 */
                else if (((TRUE == b1OpenFlower)
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      ">"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "<"))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "["))
                          && (NULL ==
                              STRSTR (apu1StringToken[u4StringCount],
                                      "]")))
                         ||
                         ((NULL !=
                           STRSTR (apu1StringToken[u4StringCount], "}"))
                          && (NULL !=
                              STRSTR (apu1StringToken[u4StringCount],
                                      "]")) && (TRUE == b1OpenFlower)))
                {

                    /* To handle closing } */
                    if (NULL != STRSTR (apu1StringToken[u4StringCount], "}"))
                    {
                        b1OpenFlower = FALSE;
                    }

                    if (NULL != STRSTR (apu1StringToken[u4StringCount], "]"))
                    {
                        b1OpenOptional = FALSE;
                    }
                    /* In Case a match is already there */
                    if (b1OpenFlowerMatchFound == TRUE)
                    {
                        u4StringCount++;
                    }
                    else
                    {
                        if (NULL !=
                            STRSTR (apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            u4InputCount++;
                            b1OpenFlowerMatchFound = TRUE;
                        }
                        u4StringCount++;
                    }
                }
                /* to handle [{ option1 | option2 }] part 1  */
                else if ((NULL !=
                          STRSTR (apu1StringToken[u4StringCount], "[{"))
                         && (FALSE == b1OpenOptionalFlower)
                         && (FALSE == b1OpenOptional))
                {
                    if (NULL != STRSTR (apu1StringToken[u4StringCount],
                                        apu1InputToken[u4InputCount]))
                    {
                        b1OpenOptionalFlowerMatchFound = TRUE;
                        u4InputCount++;
                    }
                    b1OpenOptionalFlower = TRUE;
                    u4StringCount++;
                }
                /* to handle [{ option1 | option2 }] part 2  */
                else if ((TRUE == b1OpenOptionalFlower))
                {
                    if ((NULL !=
                         STRSTR (apu1StringToken[u4StringCount], "<"))
                        && (NULL !=
                            STRSTR (apu1StringToken[u4StringCount], ">")))
                    {
                        /* Handles <user_defined> inside flower optiontioanl */
                        u4LoopCount = u4StringCount;
                        b1MatchFoundLoop = FALSE;
                        while (NULL != apu1StringToken[u4LoopCount])
                        {
                            /* STRSTR or STRCMP need to ccheck */
                            if (NULL !=
                                STRSTR (apu1StringToken[u4LoopCount],
                                        apu1InputToken[u4InputCount]))
                            {
                                b1MatchFoundLoop = TRUE;
                                break;
                            }
                            u4LoopCount++;
                        }

                        if (TRUE == b1MatchFoundLoop)
                        {
                            u4StringCount++;
                        }
                        else
                        {
                            u4StringCount++;
                            u4InputCount++;
                        }
                    }

                    if ((NULL != STRSTR (apu1StringToken[u4StringCount],
                                         apu1InputToken[u4InputCount]))
                        && (FALSE == b1OpenOptionalFlowerMatchFound))
                    {
                        b1OpenOptionalFlowerMatchFound = FALSE;
                        u4InputCount++;
                    }
                    u4StringCount++;
                    if (NULL != STRSTR (apu1StringToken[u4StringCount], "}]"))
                    {
                        b1OpenOptionalFlower = FALSE;
                    }

                }
                else if ((0 ==
                          STRNCMP (apu1StringToken[u4StringCount],
                                   apu1InputToken[u4InputCount],
                                   STRLEN (apu1InputToken[u4InputCount]))))
                {
                    /* Ignore the partial match if the level of token is already
                       a exact match is found in the previous iteration */
                    ba1MatchFound[u4InputCount] = TRUE;
                    u4InputCount++;
                    u4StringCount++;
                }
                else
                {
                    if (NULL == STRSTR (apu1StringToken[u4StringCount], "{"))
                    {
                        ba1MatchFound[u4InputCount] = FALSE;
                        u4InputCount++;
                        break;
                    }
                    u4InputCount++;
                    u4StringCount++;
                }
                u4Count++;
            }                    /* end - if */
        }                        /* end - while */
    }                            /* end - if */

    u4Count = u4InputCount;
    if ((u4Count != 0) && (u4Count < CLI_MAX_HELP_TOKEN_NUM) &&
        TRUE == ba1MatchFound[u4Count - 1])
    {
        /* The match flag upto the last token checked is true */
        return TRUE;
    }

    /* The last token checked is not matched i.e, whole command is not matched */
    return FALSE;
}

/**************************************************************************
*     Function Name : CliDisplayMessageAndUserPromptResponse              *
*                                                                         *
*     Description   : This function is used to display the warning        *
*                     message and get the input from the user as          *
*                     yes/no.                                             *
*                                                                         *
*     Input(s)      : pi1InputMessage - Pointer to the output message     *
*                                to be displayed                          *
*                     i1RepeatFlag    - Number of times to be repeated    *
*                                                                         *
*     Returns       : i4Ret - CLI_SUCCESS if the response is yes          *
*                     i4Ret - CLI_FAILURE if the response is no           *
*                                                                         *
***************************************************************************/

INT4
CliDisplayMessageAndUserPromptResponse (CHR1 * pi1InputMessage,
                                        INT1 i1RepeatFlag,
                                        INT4 (*pLockPointer) (VOID),
                                        INT4 (*pUnlockPointer) (VOID))
{
    const CHR1         *pi1Prompt = "Are you sure? (y/n)";
    const CHR1         *pi1RepeatPrompt = "Please reply with yes or no.";
    CHR1                ai1Prompt[MAX_PROMPT_LEN];
    INT1                ai1UserPrompt[MAX_INPUT_SIZE];
    INT4                i4Ret = CLI_FAILURE;
    MEMSET (ai1Prompt, 0, MAX_PROMPT_LEN);
    MEMSET (ai1UserPrompt, 0, MAX_INPUT_SIZE);
    /* if input string length and prompt message string length is greater than
     *        255,then display prompt message */
    if ((STRLEN (pi1InputMessage) + STRLEN (pi1Prompt)) > MAX_PROMPT_LEN)
    {
        SNPRINTF (ai1Prompt, MAX_PROMPT_LEN, "%s", pi1Prompt);
    }
    else
    {
        SNPRINTF (ai1Prompt, MAX_PROMPT_LEN, "%s%s", pi1InputMessage,
                  pi1Prompt);
    }

    do
    {
        (*pUnlockPointer) ();
        MGMT_UNLOCK ();
        i4Ret =
            CliReadLine ((INT1 *) ai1Prompt, ai1UserPrompt, MAX_INPUT_SIZE - 1);
        MGMT_LOCK ();
        (*pLockPointer) ();

        if (i4Ret == CLI_FAILURE)
        {
            return (i4Ret);
        }
        /* if  user response is "n" or "no" ,then return failure */
        if ((STRCASECMP (ai1UserPrompt, "n") == 0) ||
            (STRCASECMP (ai1UserPrompt, "no") == 0))
        {
            i4Ret = CLI_FAILURE;
            i1RepeatFlag = 0;
        }
        /* if user response is "y" or "yes" ,then return success */
        else if ((STRCASECMP (ai1UserPrompt, "y") == 0) ||
                 (STRCASECMP (ai1UserPrompt, "yes") == 0))
        {
            i4Ret = CLI_SUCCESS;
            i1RepeatFlag = 0;
        }
        else
        {
            i4Ret = CLI_FAILURE;
            /* if user response is other than "y/n/yes/no",
             * then display an info message*/
            SNPRINTF (ai1Prompt, MAX_PROMPT_LEN, "%s%s", pi1RepeatPrompt,
                      pi1Prompt);
        }
    }
    while (i1RepeatFlag == 1);
    return i4Ret;
}

/************************************************************************** *
 *      Function Name : CliValidateSyslogMsg                                *
 *                                                                          *
 *      Description   : This function checks the CLI command with tokens of *
 *                      command to avoid displaying the password            *
 *                      in clear text format in syslog message and          *
 *                      Audit logs.                                         *
 *                                                                          *
 *                                                                          *
 *      Input(s)      : pCliContext - Ponter to Cli context                 *
 *                      pu1StrValuee - Pointer to Cli command and           *
 *                                                                          *
 *      Output(s)     : None                                                *
 *                                                                          *
 *      Returns       : return pu1StrValuee                                 *
 *                                                                          *
 *****************************************************************************/

UINT1              *
CliValidateSyslogMsg (tCliContext * pCliContext, UINT1 *pu1StrValue)
{

    UINT1               au1Buf[MAX_BUF_LEN];
    UINT1               buffer[MAX_BUF_LEN];
    UINT1               au1ReadStr[MAX_BUF_LEN];
    UINT1               au1StrValue[MAX_BUF_LEN];
    UINT1               au1TmpValue[MAX_BUF_LEN];
    UINT1               au1ReplaValue[MAX_PASSWD_LEN];
    UINT1              *au1Cmd;
    UINT1              *pu1TempCmd;

    CLI_ALLOC_MAXLINE_MEM_BLOCK (au1Cmd, UINT1);

    MEMSET (au1Buf, '\0', MAX_BUF_LEN);
    MEMSET (au1ReadStr, '\0', MAX_BUF_LEN);
    MEMSET (au1StrValue, '\0', MAX_BUF_LEN);
    MEMSET (au1ReplaValue, '*', MAX_PASSWD_LEN);
    MEMSET (au1TmpValue, '\0', MAX_BUF_LEN);
    MEMSET (buffer, '\0', MAX_BUF_LEN);
    CLI_MEMSET (au1Cmd, CLI_ZERO, (MAX_LINE_LEN + 1));

    if ((((pCliContext->MmiCmdToken[0][0] == 'u') ||
          (pCliContext->MmiCmdToken[0][0] == 'U')) &&
         (((pCliContext->MmiCmdToken[2][0] == 'p') ||
           (pCliContext->MmiCmdToken[2][0] == 'P')) &&
          ((pCliContext->MmiCmdToken[2][1] == 'a') ||
           (pCliContext->MmiCmdToken[2][1] == 'A')))) ||
        (pCliContext->i2TokenCount == 6) ||
        (((pCliContext->i2TokenCount == 8) &&
          (pCliContext->MmiCmdToken[6][0] == 's')) ||
         (pCliContext->MmiCmdToken[6][0] == 'S')))
    {
        STRNCPY (au1Cmd, pu1StrValue, STRLEN (pu1StrValue));
        CLI_MEMSET (pu1StrValue, '\0', MAX_LINE_LEN);
        SSCANF ((char *) au1Cmd, "%s%s%s%s", au1Buf, au1ReadStr, au1StrValue,
                au1TmpValue);

        if (!(pu1TempCmd = (UINT1 *) STRSTR ((char *) au1Cmd,
                                             (char *) au1TmpValue)))
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (au1Cmd);
            return pu1StrValue;
        }

        STRNCPY (pu1StrValue, au1Cmd, pu1TempCmd - au1Cmd);
        pu1StrValue[pu1TempCmd - au1Cmd] = '\0';
        au1ReplaValue[MAX_PASSWD_LEN - 1] = '\0';

        SPRINTF ((char *) pu1StrValue + (pu1TempCmd - au1Cmd), "%s%s",
                 au1ReplaValue, pu1TempCmd + STRLEN (au1TmpValue));
        CLI_RELEASE_MAXLINE_MEM_BLOCK (au1Cmd);
        return pu1StrValue;
    }
    else
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (au1Cmd);
        return pu1StrValue;
    }
}
