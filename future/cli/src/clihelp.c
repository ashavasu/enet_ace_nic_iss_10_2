/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clihelp.c,v 1.25 2016/05/20 10:17:27 siva Exp $
 *
 * Description: This will have the help related routines
 *              for display module                      
 *******************************************************************/

/*********************** HEADER FILES *********************************/

#include "clicmds.h"

extern tCliSessions gCliSessions;
static t_MMI_HELP  *mmicm_get_new_mmi_help_node (VOID);

/* Temporarily holds the string entered by User */
static CHR1                au1UsrStr[CLI_MAX_HELP_LINE_LEN + 1];
static CHR1                au1UsrStrBkp[CLI_MAX_HELP_LINE_LEN + 1];
/* Temporarily holds the strings obtained by search */
static CHR1                au1LocalStr[CLI_MAX_HELP_LINE_LEN + 1];

/* Holds the flag to denote exact match */
static BOOL1               ba1MatchFound[CLI_MAX_HELP_TOKEN_NUM]= { 0 };

/* Temporarily holds the tokens of input and search strings*/
static CHR1                *apu1InputToken[CLI_MAX_HELP_TOKEN_NUM] ;
static CHR1                *apu1StringToken[CLI_MAX_HELP_TOKEN_NUM];

extern tMemPoolId   gCliCmdHelpPoolId;
extern tMemPoolId   gCliCmdHelpPrivPoolId;
extern tMemPoolId   gCliMaxLineMemPoolId;


/***************************************************************************
 * FUNCTION NAME : mmicm_get_new_mmi_help_node
 * DESCRIPTION   : This function is used to allocate memory for a help tree
 * INPUT         : NONE
 * RETURNS       : returns pointer to the allocated memory
 ***************************************************************************/

t_MMI_HELP         *
mmicm_get_new_mmi_help_node (VOID)
{
    t_MMI_HELP         *p_ptr;

    if ((p_ptr =
         CliMemAllocMemBlk (gCliCmdHelpPoolId, sizeof (t_MMI_HELP))) == NULL)
    {
        return NULL;
    }

    return p_ptr;
}

/***************************************************************************
 * FUNCTION NAME : CliDisplayHelp
 * DESCRIPTION   : This function is used to display help for the commands 
                   entered from the syntax in def file.
 * INPUT         : pointer to entered string
                   OptFlag
 * RETURNS       : NONE
 ***************************************************************************/

VOID
CliDisplayHelp (INT1 *i1pStr, UINT2 u2OptFlag, BOOL1 bLockStatus)
{
    tCliContext        *pCliContext;
    t_MMI_HELP         *p_thelp_ptr = NULL;
    CONST CHR1        **ppu1HelpStr = NULL;
    CONST CHR1        **ppu1SemanticsStr = NULL;
    CONST CHR1         *pu1TempHelpStr = NULL;
    CONST CHR1 	       *pu1TempInputToken = NULL;
    CONST CHR1 	       *pu1TempStringToken = NULL;
    UINT4               u4Count = 0;
    UINT4               u4InputCount = 0;
    UINT4 		u4StringCount = 0;
    UINT4		u4TokenCount = 0;
    INT2                i2Index;
    INT2                i2HelpStrIndex = 0;
    INT2                i2TotalCmds = 0;
    INT2                i2LoopIndex1 = 0;
    INT2                i2LoopIndex2 = 1;
    INT2                i2Len = 0;
    BOOL1		b1TotalPrintFlag = FALSE;
    BOOL1               b1PrintOutput = FALSE;
    UINT4		u4LoopCount = 0 ;
    BOOL1		b1MatchFoundLoop   = FALSE;
    BOOL1		b1OpenUserDefined  = FALSE;
    BOOL1		b1OpenOptional	   = FALSE;
    BOOL1		b1OpenOptionalMatchFound = FALSE;
    BOOL1	        b1OpenFlower	   = FALSE;
    BOOL1	        b1OpenFlowerMatchFound = FALSE;
    BOOL1		b1OpenOptionalFlower = FALSE;
    BOOL1		b1OpenOptionalFlowerMatchFound = FALSE;

    MEMSET(au1UsrStr,0,sizeof(au1UsrStr));
    MEMSET(au1UsrStrBkp,0,sizeof(au1UsrStrBkp));
    MEMSET(au1LocalStr,0,sizeof(au1LocalStr));
    if(NULL != i1pStr)
    {
	    if(STRLEN(i1pStr) >= CLI_MAX_HELP_LINE_LEN)
	    {
		    STRNCPY(au1UsrStr,i1pStr,CLI_MAX_HELP_LINE_LEN);
		    STRNCPY(au1UsrStrBkp,i1pStr,CLI_MAX_HELP_LINE_LEN); 
	    }
	    else
	    {
		    STRNCPY(au1UsrStr,i1pStr,STRLEN(i1pStr));
		    STRNCPY(au1UsrStrBkp,i1pStr,STRLEN(i1pStr)); 
	    }
    }
    
    /*Protocol lock must release during more/help thread */
    if (bLockStatus == OSIX_TRUE)
    {
        MGMT_UNLOCK ();
    }

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        if (bLockStatus == OSIX_TRUE)
        {
            MGMT_LOCK ();
        }
        return;
    };

    if ((pCliContext->i4Mode == CLI_IF_RANGE_MODE_ENABLE)
        && (pCliContext->u1NoOfAttempts >= 1))
    {
        if (bLockStatus == OSIX_TRUE)
        {
            MGMT_LOCK ();
        }
        return;
    }

    if (pCliContext->pMmiCur_mode != NULL)
    {
        p_thelp_ptr = pCliContext->pMmiCur_mode->pHelp;
        if (p_thelp_ptr == NULL)
        {
            mmi_printf ("\rError! \r\n");
            if (bLockStatus == OSIX_TRUE)
            {
                MGMT_LOCK ();
            }
            return;
        }
    }
    else
    {
        mmi_printf ("\rCurrently no mode\r\n");
        if (bLockStatus == OSIX_TRUE)
        {
            MGMT_LOCK ();
        }
        return;
    }

    while (p_thelp_ptr != NULL)
    {
        i2TotalCmds += (p_thelp_ptr->i4ActNumEnd -
                        p_thelp_ptr->i4ActNumStart) + 1;
        p_thelp_ptr = p_thelp_ptr->pNext;
    }

    if (!(ppu1HelpStr = CLI_BUDDY_ALLOC ((i2TotalCmds * sizeof (CONST CHR1 *)),
                                         CONST CHR1 *)))
    {
        if (bLockStatus == OSIX_TRUE)
        {
            MGMT_LOCK ();
        }
        return;
    }
    if (CLI_ISSET_HELP_DESC (u2OptFlag))
    {
        if (!
            (ppu1SemanticsStr =
             CLI_BUDDY_ALLOC ((i2TotalCmds * sizeof (CONST CHR1 *)),
                              CONST CHR1 *)))
        {
            CLI_BUDDY_FREE (ppu1HelpStr);
            if (bLockStatus == OSIX_TRUE)
            {
                MGMT_LOCK ();
            }
            return;
        }
    }

    while (!(i2HelpStrIndex))
    {
        p_thelp_ptr = pCliContext->pMmiCur_mode->pHelp;

        while (p_thelp_ptr != NULL)
        {
            i2Index = 0;
            while (p_thelp_ptr->pSyntax[i2Index] != NULL)
            {
                /* Check if this command is removed already */
                if (p_thelp_ptr->pSyntax[i2Index] != CLI_REM_CMD)
                {
                    if ((i1pStr == NULL) || !(CLI_STRLEN (i1pStr)) ||
                        ((i1pStr) &&
                         (CliCommandMatchSubset ((CONST UINT1 *) p_thelp_ptr->
                                                 pSyntax[i2Index],
                                                 (UINT1 *) i1pStr) ==
                          CLI_SUCCESS)))
                    {

                        if (pCliContext->i1PrivilegeLevel >=
                            p_thelp_ptr->pPrivilegeId[i2Index])
                        {
                            ppu1HelpStr[i2HelpStrIndex] =
                                (CONST CHR1 *) (p_thelp_ptr->pSyntax[i2Index]);
                            if (CLI_ISSET_HELP_DESC (u2OptFlag))
                            {
                                if (ppu1SemanticsStr != NULL)
                                    ppu1SemanticsStr[i2HelpStrIndex] =
                                        (CONST CHR1 *) (p_thelp_ptr->
                                                        pSemantics[i2Index]);
                            }
                            i2HelpStrIndex++;
                        }
                    }
                }
                i2Index++;
            }
            p_thelp_ptr = p_thelp_ptr->pNext;
        }

        while ((i1pStr) && ((i2Len = (INT2) CLI_STRLEN (i1pStr)) > 0))
        {
            i1pStr[i2Len - 1] = '\0';
            i2Len--;
            if (i1pStr[i2Len - 1] == CLI_SPACE_CHAR)
                break;
        }
        if (!i2Len)
        {
            break;
        }
    }
    /* Arranges the Help strings in sorted order */
    while (i2LoopIndex1 < i2HelpStrIndex)
    {
        i2LoopIndex2 = (INT2) (i2LoopIndex1 + 1);
        while (i2LoopIndex2 < i2HelpStrIndex)
        {
            if (STRCMP (ppu1HelpStr[i2LoopIndex1],
                        ppu1HelpStr[i2LoopIndex2]) > 0)
            {
                pu1TempHelpStr = ppu1HelpStr[i2LoopIndex2];
                ppu1HelpStr[i2LoopIndex2] = ppu1HelpStr[i2LoopIndex1];
                ppu1HelpStr[i2LoopIndex1] = pu1TempHelpStr;

                if (CLI_ISSET_HELP_DESC (u2OptFlag))
                {
                    if (ppu1SemanticsStr != NULL)
                    {
                        pu1TempHelpStr = ppu1SemanticsStr[i2LoopIndex2];
                        ppu1SemanticsStr[i2LoopIndex2] =
                            ppu1SemanticsStr[i2LoopIndex1];
                        ppu1SemanticsStr[i2LoopIndex1] = pu1TempHelpStr;
                    }
                }
            }
            i2LoopIndex2++;
        }
        i2LoopIndex1++;
    }

    if (i2HelpStrIndex == 0)
    {
        mmi_printf ("\r\n");
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_INVALID_COMMAND]);
        CLI_BUDDY_FREE (ppu1HelpStr);
        if (CLI_ISSET_HELP_DESC (u2OptFlag))
        {
            CLI_BUDDY_FREE (ppu1SemanticsStr);
        }
        if (bLockStatus == OSIX_TRUE)
        {
            MGMT_LOCK ();
        }
        return;
    }

    mmi_printf ("\r\n%s commands :\r\n", pCliContext->pMmiCur_mode->pMode_name);

    i2Index = 0;
   
    MEMSET (ba1MatchFound, 0, CLI_MAX_HELP_TOKEN_NUM);
 
    while (i2Index < i2HelpStrIndex)
    {
        if (pCliContext->mmi_exit_flag != TRUE)
        { 
            b1PrintOutput = FALSE;
            b1MatchFoundLoop   = FALSE;
            b1OpenUserDefined  = FALSE;
            b1OpenOptional	   = FALSE;
            b1OpenOptionalMatchFound = FALSE;
            b1OpenFlower	   = FALSE;
            b1OpenFlowerMatchFound = FALSE;
            b1OpenOptionalFlower    = FALSE;
            b1OpenOptionalFlowerMatchFound = FALSE;

            MEMSET (au1LocalStr,0,sizeof(au1LocalStr));
            MEMSET (au1UsrStr,0,sizeof(au1UsrStr));

            MEMSET (apu1InputToken,0,sizeof(apu1InputToken));
            MEMSET (apu1StringToken,0,sizeof(apu1StringToken));

            if (STRLEN(ppu1HelpStr[i2Index]) >= CLI_MAX_HELP_LINE_LEN)
            {
                STRNCPY (au1LocalStr,ppu1HelpStr[i2Index],CLI_MAX_HELP_LINE_LEN);
            }
            else
            {
                STRNCPY (au1LocalStr,ppu1HelpStr[i2Index], STRLEN(ppu1HelpStr[i2Index]));
            }	   

            STRNCPY (au1UsrStr, au1UsrStrBkp, STRLEN(au1UsrStrBkp));

            if (NULL == i1pStr)
            {
                b1PrintOutput = TRUE;
            }
            /* Print the Command in Scenario of exact match */
            else if (0 == STRCMP(au1LocalStr,au1UsrStrBkp))
            {
                b1PrintOutput = TRUE;
            }
            /* Print the Command in Scenario of incomplete command */
            else if (!isspace(au1UsrStr[STRLEN(au1UsrStr)-1]))
            {
                u4InputCount = 0;
                apu1InputToken[u4InputCount] = STRTOK(au1UsrStr," ");
                while ( NULL != apu1InputToken[u4InputCount])
                {
                    u4InputCount++;
                    apu1InputToken[u4InputCount] = STRTOK(NULL," ");
                }
                if (u4InputCount > 1)
                {
                    u4InputCount--;
                    pu1TempInputToken = apu1InputToken[u4InputCount];
                    apu1InputToken[u4InputCount] = NULL;
                    u4TokenCount = u4InputCount;
                }

                /* Tokenise the input and the search strings */
                u4StringCount = 0;
                apu1StringToken[u4StringCount] = STRTOK(au1LocalStr," ");
                while ( NULL != apu1StringToken[u4StringCount])
                {
                    u4StringCount++;
                    apu1StringToken[u4StringCount] = STRTOK(NULL," ");
                }
                if (u4StringCount > 1)
                {
                    u4StringCount--;
                    if(NULL != apu1StringToken[u4TokenCount])
                    {
                        pu1TempStringToken = apu1StringToken[u4TokenCount];
                    }
                    apu1StringToken[u4StringCount] = NULL;
                }

                u4Count = 0;
                u4StringCount = 0;
                u4InputCount = 0;

                while((apu1InputToken[u4InputCount]!=NULL) && 
                        (apu1StringToken[u4StringCount] != NULL )) 
                {
                    if (0 == STRNCMP(apu1InputToken[u4InputCount],
                                apu1StringToken[u4StringCount],
                                STRLEN(apu1StringToken[u4StringCount])))
                    {
                        if(u4InputCount != 0)
                        {
                            ba1MatchFound[u4InputCount]=TRUE;	
                        }
                        b1PrintOutput = TRUE;
                        u4InputCount++;
                        u4StringCount++;
                    }

                    /* Logic to parse {< is yet to be done so we ignore it for now */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"{<")
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"}"))))
                    {
                        break;
                    }

                    /* To handle <user_input> case, We dont check and go for the next token */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        u4InputCount++;
                        u4StringCount++;
                    }
                    /* to hand <user   input> case (part 1) */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        b1OpenUserDefined = TRUE;
                        u4StringCount++;
                    }
                    /* to handle <user input> case (part 2) */
                    else if  ((NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (TRUE == b1OpenUserDefined)
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        b1OpenUserDefined = FALSE;	
                        u4StringCount++;
                        u4InputCount++;
                    }
                    /* to handle [input] case */
                    else if((NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"]")))
                    {
                        /* handles [<userinput>] */
                        if((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],">")))
                        {
                            /* Check the user token against all other tokens
                             * if match is found then proceed ,
                             * if not found increement both counts */

                            u4LoopCount = u4StringCount;
                            b1MatchFoundLoop = FALSE;
                            while( NULL != apu1StringToken[u4LoopCount])
                            {
                                /* STRSTR or STRCMP need to ccheck */
                                if(NULL != STRSTR(apu1StringToken[u4LoopCount],
                                            apu1InputToken[u4InputCount]))
                                {
                                    b1MatchFoundLoop = TRUE;
                                    break;
                                }
                                u4LoopCount++;
                            }

                            if (TRUE == b1MatchFoundLoop)
                            {
                                u4StringCount++;
                            }
                            else
                            {
                                u4StringCount++;
                                u4InputCount++;
                            }

                        }
                        /* handle [input]*/
                        else
                        {
                            if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                        apu1InputToken[u4InputCount]))
                            {
                                u4InputCount++;
                            }	
                            u4StringCount++;	
                        }


                    }
                    /* to handle [text <user_input>]    part 1*/
                    else if (( NULL != STRSTR(apu1StringToken[u4StringCount],"["))	
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenUserDefined)
                            && (FALSE == b1OpenOptional)
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))

                    {

                        b1OpenOptional = TRUE;
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            b1OpenOptionalMatchFound = TRUE;
                            u4InputCount++;
                        }
                        u4StringCount++;	
                    }
                    /* to handle [text <user_input>]    part 2*/
                    else if ((NULL == STRSTR(apu1StringToken[u4StringCount],"["))	
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenUserDefined)
                            && (TRUE  == b1OpenOptional)
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))
                    {
                        b1OpenOptional = FALSE;
                        if(TRUE == b1OpenOptionalMatchFound)
                        {
                            u4InputCount++;
                        }
                        u4StringCount++;	
                    }
                    /* Logic to parse { { or  [  is yet to be added so we ignore it for now */
                    else if (((( TRUE  == b1OpenFlower) || (TRUE == b1OpenOptionalFlower))
                             && (NULL != STRSTR(apu1StringToken[u4StringCount],"{")))
                             || ((NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"}"))))
                    {
                        break;
                    }
                    /* to  handle simple case {option_1 | option_2}  part 1 */
                    else if ((( NULL == STRSTR(apu1StringToken[u4StringCount],"["))	
                                && ( NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                                && ( FALSE == b1OpenUserDefined)
                                && ( FALSE  == b1OpenOptional)
                                && ( FALSE  == b1OpenFlower)
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"{"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))
                            || ((NULL != STRSTR(apu1StringToken[u4StringCount],"{"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"["))))	
                    {
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                        {	
                            b1OpenOptional = TRUE;
                        }

                        b1OpenFlower = TRUE;
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            u4InputCount++;
                            b1OpenFlowerMatchFound = TRUE;
                        }
                        u4StringCount++;
                    }

                    /* to  handle simple case {option_1 | option_2}  part 2 */
                    else if ((( TRUE == b1OpenFlower)	
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"]")))
                            || ((NULL != STRSTR(apu1StringToken[u4StringCount],"}"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                                && (TRUE == b1OpenFlower)))
                    {		

                        /* To handle closing }*/
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"}"))
                    {
                        b1OpenFlower = FALSE;
                    }

                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                    {
                        b1OpenOptional = FALSE;
                    }
                    /* In Case a match is already there */
                    if(b1OpenFlowerMatchFound == TRUE)
                    {
                        u4StringCount++;
                    }
                    else
                    {
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            u4InputCount++;
                            b1OpenFlowerMatchFound = TRUE;
                        }
                        u4StringCount++;
                    }
                }
                /* to handle [{ option1 | option2 }] part 1  */
                else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"[{"))
                        && (FALSE == b1OpenOptionalFlower)
                        && (FALSE == b1OpenOptional))
                {
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                apu1InputToken[u4InputCount]))	
                    {
                        b1OpenOptionalFlowerMatchFound = TRUE;
                        u4InputCount++;
                    }
                    b1OpenOptionalFlower = TRUE;
                    u4StringCount++;
                } 
                /* to handle [{ option1 | option2 }] part 2  */
                else if((TRUE == b1OpenOptionalFlower))
                {
                    if((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            &&(NULL != STRSTR(apu1StringToken[u4StringCount],">")))
                    {
                        /* Handles <user_defined> inside flower optiontioanl */
                        u4LoopCount = u4StringCount;
                        b1MatchFoundLoop = FALSE;
                        while( NULL != apu1StringToken[u4LoopCount])
                        {
                            /* STRSTR or STRCMP need to ccheck */
                            if(NULL != STRSTR(apu1StringToken[u4LoopCount],
                                        apu1InputToken[u4InputCount]))
                            {
                                b1MatchFoundLoop = TRUE;
                                break;
                            }
                            u4LoopCount++;
                        }

                        if (TRUE == b1MatchFoundLoop)
                        {
                            u4StringCount++;
                        }
                        else
                        {
                            u4StringCount++;
                            u4InputCount++;
                        }
                    }

                    if((NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                            &&(FALSE == b1OpenOptionalFlowerMatchFound))	
                    {
                        b1OpenOptionalFlowerMatchFound = FALSE;
                        u4InputCount++;
                    }
                    u4StringCount++;
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"}]"))
                    {
                        b1OpenOptionalFlower = FALSE;
                    }

                }



                else if((NULL != STRSTR(apu1StringToken[u4StringCount],
                                apu1InputToken[u4InputCount])))
                {
                    /* Ignore the partial match if the level of token is already
                       a exact match is found in the previous iteration */ 
                    if(ba1MatchFound[u4InputCount] == TRUE)
                    {
                        b1PrintOutput = FALSE;
                        break;
                    }
                    else
                    {
                        ba1MatchFound [u4InputCount] = FALSE; 
                        b1PrintOutput = TRUE;
                    }
                    if(0 != STRNCMP(apu1StringToken[u4StringCount],apu1InputToken[u4InputCount],
                                   STRLEN(apu1InputToken[u4InputCount])))
                    {
                          b1PrintOutput = FALSE;
                    }
                    u4StringCount++;
                    u4InputCount++;
                }
                else
                {
                    b1PrintOutput = FALSE;
                    ba1MatchFound [u4InputCount]=FALSE;
                    u4StringCount++;
                    u4InputCount++;
                    break;
                }
                u4Count++;		   
            }

            /* Dont print the output incase the last token doesnt 
             * even partially match*/

            if (b1PrintOutput == TRUE)
            {
                if((NULL!=pu1TempStringToken)&&(NULL!=pu1TempInputToken))
                {
                    if(NULL == STRSTR(pu1TempStringToken,pu1TempInputToken))
                    {
                        if((NULL == STRSTR(pu1TempStringToken,"{"))
                                &&(NULL == STRSTR(pu1TempStringToken,"<"))
                                &&(NULL == STRSTR(pu1TempStringToken,">"))
                                &&(NULL == STRSTR(pu1TempStringToken,"["))
                                &&(NULL == STRSTR(pu1TempStringToken,"]"))
                                &&(NULL == STRSTR(pu1TempStringToken,"}")))
                        {
                            b1PrintOutput = FALSE;
                        }

                       /* Since logic is not implemented for {< dont consider
                        * the partial match scenario */
                        if (NULL != STRSTR(pu1TempStringToken,"{<"))
                        {
                            b1PrintOutput = TRUE;
                        }

                    }
                }
            }
        }
        else
        {
            /* Tokenise the user-input and search string */
            u4InputCount = 0;
            apu1InputToken[u4InputCount] = STRTOK(au1UsrStr," ");
            while ( NULL != apu1InputToken[u4InputCount])
            {
                u4InputCount++;
                apu1InputToken[u4InputCount] = STRTOK(NULL," ");
            }
            if (u4InputCount > 0)
            {
            pu1TempInputToken = apu1InputToken[u4InputCount-1];
            }
            u4StringCount = 0;
            apu1StringToken[u4StringCount] = STRTOK(au1LocalStr," ");
            while ( NULL != apu1StringToken[u4StringCount])
            {
                u4StringCount++;
                apu1StringToken[u4StringCount] = STRTOK(NULL," ");
            }
            if ((u4InputCount > 0) &&
                (NULL != apu1StringToken[u4InputCount-1]))
            {
                pu1TempStringToken = apu1StringToken[u4InputCount-1];

            }

            if( u4InputCount > u4StringCount)
            {
                b1PrintOutput = FALSE;
            }
            else
            {
                u4InputCount = 0;
                u4StringCount = 0;
                u4Count = 0;
                while( (NULL != apu1InputToken[u4InputCount])
                        && (NULL != apu1StringToken[u4StringCount])) 
                {
                    /* Exact match */
                    if (0 == STRNCMP(apu1InputToken[u4InputCount],
                                apu1StringToken[u4StringCount],
                                STRLEN(apu1StringToken[u4StringCount])))
                    {
                        ba1MatchFound[u4InputCount]=TRUE;	
                        b1PrintOutput = TRUE;
                        u4InputCount++;
                        u4StringCount++;
                    }

                    /* Logic to parse {< is yet to be done so we ignore it for now */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"{<")
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"}"))))
                    {
                        break;
                    }

                    /* To handle <user_input> case, We dont check and go for the next token */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        u4InputCount++;
                        u4StringCount++;
                    }
                    /* to hand <user   input> case (part 1) */
                    else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        b1OpenUserDefined = TRUE;
                        u4StringCount++;
                    }
                    /* to handle <user input> case (part 2) */
                    else if  ((NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                            && (TRUE == b1OpenUserDefined)
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenOptionalFlower))
                    {
                        b1OpenUserDefined = FALSE;	
                        u4StringCount++;
                        u4InputCount++;
                    }
                    /* to handle [input] case */
                    else if((NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"]")))
                    {
                        /* handles [<userinput>] */
                        if((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],">")))
                        {
                            /* Check the user token against all other tokens
                             * if match is found then proceed ,
                             * if not found increement both counts */

                            u4LoopCount = u4StringCount;
                            b1MatchFoundLoop = FALSE;
                            while( NULL != apu1StringToken[u4LoopCount])
                            {
                                /* STRSTR or STRCMP need to ccheck */
                                if(NULL != STRSTR(apu1StringToken[u4LoopCount],
                                            apu1InputToken[u4InputCount]))
                                {
                                    b1MatchFoundLoop = TRUE;
                                    break;
                                }
                                u4LoopCount++;
                            }

                            if (TRUE == b1MatchFoundLoop)
                            {
                                u4StringCount++;
                            }
                            else
                            {
                                u4StringCount++;
                                u4InputCount++;
                            }

                        }
                        /* handle [input]*/
                        else
                        {
                            if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                        apu1InputToken[u4InputCount]))
                            {
                                u4InputCount++;
                            }	
                            u4StringCount++;	
                        }


                    }
                    /* to handle [text <user_input>]    part 1*/
                    else if (( NULL != STRSTR(apu1StringToken[u4StringCount],"["))	
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenUserDefined)
                            && (FALSE == b1OpenOptional)
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))

                    {

                        b1OpenOptional = TRUE;
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            b1OpenOptionalMatchFound = TRUE;
                            u4InputCount++;
                        }
                        u4StringCount++;	
                    }
                    /* to handle [text <user_input>]    part 2*/
                    else if ((NULL == STRSTR(apu1StringToken[u4StringCount],"["))	
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                            && (FALSE == b1OpenUserDefined)
                            && (TRUE  == b1OpenOptional)
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],">"))
                            && (NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                            && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))
                    {
                        b1OpenOptional = FALSE;
                        if(TRUE == b1OpenOptionalMatchFound)
                        {
                            u4InputCount++;
                        }
                        u4StringCount++;	
                    }
                    /* Logic to parse { { or  [  is yet to be added so we ignore it for now */
                    else if (((( TRUE  == b1OpenFlower) || (TRUE == b1OpenOptionalFlower))
                             && (NULL != STRSTR(apu1StringToken[u4StringCount],"{")))
                             || ((NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"{"))
                             && (NULL == STRSTR(apu1StringToken[u4StringCount],"}"))))
                    {
                        break;
                    }
                    /* to  handle simple case {option_1 | option_2}  part 1 */
                    else if ((( NULL == STRSTR(apu1StringToken[u4StringCount],"["))	
                                && ( NULL == STRSTR(apu1StringToken[u4StringCount],"]"))
                                && ( FALSE == b1OpenUserDefined)
                                && ( FALSE  == b1OpenOptional)
                                && ( FALSE  == b1OpenFlower)
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"{"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"}")))
                            || ((NULL != STRSTR(apu1StringToken[u4StringCount],"{"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"["))))	
                    {
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],"["))
                        {	
                            b1OpenOptional = TRUE;
                        }

                        b1OpenFlower = TRUE;
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            u4InputCount++;
                            b1OpenFlowerMatchFound = TRUE;
                        }
                        u4StringCount++;
                    }

                    /* to  handle simple case {option_1 | option_2}  part 2 */
                    else if ((( TRUE == b1OpenFlower)	
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],">"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"<"))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"["))
                                && (NULL == STRSTR(apu1StringToken[u4StringCount],"]")))
                            || ((NULL != STRSTR(apu1StringToken[u4StringCount],"}"))
                                && (NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                                && (TRUE == b1OpenFlower)))
                    {		

                        /* To handle closing }*/
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"}"))
                    {
                        b1OpenFlower = FALSE;
                    }

                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"]"))
                    {
                        b1OpenOptional = FALSE;
                    }
                    /* In Case a match is already there */
                    if(b1OpenFlowerMatchFound == TRUE)
                    {
                        u4StringCount++;
                    }
                    else
                    {
                        if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                        {
                            u4InputCount++;
                            b1OpenFlowerMatchFound = TRUE;
                        }
                        u4StringCount++;
                    }
                }
                /* to handle [{ option1 | option2 }] part 1  */
                else if ((NULL != STRSTR(apu1StringToken[u4StringCount],"[{"))
                        &&(FALSE == b1OpenOptionalFlower)
                        && (FALSE == b1OpenOptional))
                {
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],
                                apu1InputToken[u4InputCount]))	
                    {
                        b1OpenOptionalFlowerMatchFound = TRUE;
                        u4InputCount++;
                    }
                    b1OpenOptionalFlower = TRUE;
                    u4StringCount++;
                } 
                /* to handle [{ option1 | option2 }] part 2  */
                else if((TRUE == b1OpenOptionalFlower))
                {
                    if((NULL != STRSTR(apu1StringToken[u4StringCount],"<"))
                            &&(NULL != STRSTR(apu1StringToken[u4StringCount],">")))
                    {
                        /* Handles <user_defined> inside flower optiontioanl */
                        u4LoopCount = u4StringCount;
                        b1MatchFoundLoop = FALSE;
                        while( NULL != apu1StringToken[u4LoopCount])
                        {
                            /* STRSTR or STRCMP need to ccheck */
                            if(NULL != STRSTR(apu1StringToken[u4LoopCount],
                                        apu1InputToken[u4InputCount]))
                            {
                                b1MatchFoundLoop = TRUE;
                                break;
                            }
                            u4LoopCount++;
                        }

                        if (TRUE == b1MatchFoundLoop)
                        {
                            u4StringCount++;
                        }
                        else
                        {
                            u4StringCount++;
                            u4InputCount++;
                        }
                    }

                    if((NULL != STRSTR(apu1StringToken[u4StringCount],
                                    apu1InputToken[u4InputCount]))
                            &&(FALSE == b1OpenOptionalFlowerMatchFound))	
                    {
                        b1OpenOptionalFlowerMatchFound = FALSE;
                        u4InputCount++;
                    }
                    u4StringCount++;
                    if(NULL != STRSTR(apu1StringToken[u4StringCount],"}]"))
                    {
                        b1OpenOptionalFlower = FALSE;
                    }

                }
                else if((NULL != STRSTR(apu1StringToken[u4StringCount],apu1InputToken[u4InputCount])))
                {
                    /* Ignore the partial match if the level of token is already
                       a exact match is found in the previous iteration */ 
                    if(ba1MatchFound[u4InputCount] == TRUE)
                    {
                        b1PrintOutput = FALSE;
                        break;
                    }
                    else
                    {
                        ba1MatchFound [u4InputCount] = FALSE;
                        b1PrintOutput = TRUE;
                    }
                    u4InputCount++;
                    u4StringCount++;
                }
                else
                {
                    if(NULL ==STRSTR(apu1StringToken[u4StringCount],"{"))
                    {
                        b1PrintOutput = FALSE; 
                        ba1MatchFound [u4InputCount]=FALSE;
                        break;
                    }
                    u4InputCount++;
                    u4StringCount++;
                }
                u4Count++;
            }
        }
    }
    /* Incase an exact match is found on nth token then dont consider 
     * matches in the previous tokens in subsequent iterations*/

    u4Count=u4InputCount;
    while(u4Count < CLI_MAX_HELP_TOKEN_NUM)
    {
        if(TRUE == ba1MatchFound[u4Count])
        {
            b1PrintOutput = FALSE;
            break;
        }
        u4Count++;
    }
    if (TRUE == b1PrintOutput)
    {
        if(i2Index == 0)
        {
            mmi_printf ("\n");
        }
        b1TotalPrintFlag = TRUE;

        mmi_printf ("  %s\r\n",ppu1HelpStr[i2Index]);
        if (CLI_ISSET_HELP_DESC (u2OptFlag))
        {
            if (ppu1SemanticsStr != NULL)
                mmi_printf ("  [Desc]: %s", ppu1SemanticsStr[i2Index]);
        }
    }
}
i2Index++;

}
	
    /* Delete the printed characters if no help strings are printed*/
    if (b1TotalPrintFlag == FALSE)
    {
	mmi_printf("\r                      ");	
	
    } 

    if (pCliContext->i4Mode == CLI_IF_RANGE_MODE_ENABLE)
    {
        pCliContext->u1NoOfAttempts++;
    }

    CLI_BUDDY_FREE (ppu1HelpStr);
    if (CLI_ISSET_HELP_DESC (u2OptFlag))
    {
        CLI_BUDDY_FREE (ppu1SemanticsStr);
    }
    /*Must lock before exiting from more thread,since 
     * MGMT lock is released by protocols */
    if (bLockStatus == OSIX_TRUE)
    {
        MGMT_LOCK ();
    }
    mmi_printf ("\r\n");
    return;
}

/*
 * This routine will print the help information for commands
 *
 * There are two kinds of help
 *        1.printing all command syntax
 *        2.printing the syntax and semantics of matched commands
 * for 1. all commands syntax will be printed
 * for 2. the commands will be compared until a matched command
 * occur (by repeately shortening the compare string)
 */
VOID
mmi_help (INT1 *i1pStr, UINT2 u2OptFlag)
{
    t_MMI_BOOLEAN       i1printed = FALSE;
    tCliContext        *pcurrContext;
    t_MMI_HELP         *p_thelp_ptr = NULL;
    INT2                i2LineCount = 0;
    INT2                i2index;
    INT2                i2CmdLen = 0;
    UINT1               u1found_flag = 0;

    if ((pcurrContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    if ((i1pStr == NULL) || !(CLI_STRLEN (i1pStr)))
    {
        if (pcurrContext->pMmiCur_mode != NULL)
        {
            p_thelp_ptr = pcurrContext->pMmiCur_mode->pHelp;
            if (p_thelp_ptr == NULL)
            {
                mmi_printf ("\rError! \n");
            }
        }
        else
        {
            mmi_printf ("\rcurrently no mode\r\n");
        }

        if (CLI_ISSET_HELP_GRPNAME (u2OptFlag))
        {
            mmi_printf ("\r\nAvailable commands :\r\n");
            mmi_printf ("\r%-10s %-6s %s\r\n", "Group", "ID", "Command");
        }
        else
        {
            mmi_printf ("\r\nAvailable commands :\r\n");

        }
        while (p_thelp_ptr != NULL)
        {
            i2index = 0;

            while (p_thelp_ptr->pSyntax[i2index] != NULL)
            {
                /* skip if the entry is marked as removed already */
                if (p_thelp_ptr->pSyntax[i2index] != CLI_REM_CMD)
                {

                    if (CLI_ISSET_HELP_GRPNAME (u2OptFlag))
                    {
                        mmi_printf ("%-10s %-6d %s\r\n",
                                    p_thelp_ptr->pGrpId[i2index],
                                    p_thelp_ptr->i4ActNumStart + i2index,
                                    p_thelp_ptr->pSyntax[i2index]);
                    }
                    else
                    {
                        mmi_printf ("%s\r\n", p_thelp_ptr->pSyntax[i2index]);
                    }
                    i2LineCount += findnewlines (p_thelp_ptr->pSyntax[i2index]);
                    if (CLI_ISSET_HELP_DESC (u2OptFlag) & MMI_HELP_ALSO)
                    {
                        mmi_printf ("\rHelp: %s\r\n",
                                    p_thelp_ptr->pSemantics[i2index]);
                        i2LineCount +=
                            findnewlines (p_thelp_ptr->pSemantics[i2index]) + 1;
                    }
                }
                i2index++;
            }
            p_thelp_ptr = p_thelp_ptr->pNext;
            if (CLI_ISSET_HELP_DESC (u2OptFlag) & MMI_NOT_ALL_GRP)
            {
                if (p_thelp_ptr->pNext == NULL)
                {
                    break;
                }
            }
            if (CLI_ISSET_HELP_DESC (u2OptFlag) & MMI_GRP_NOTE)
            {
                mmi_printf ("\r\nNEXT GROUP :\n");
                i2LineCount += 2;
            }
        }

    }
    else
    {
#ifdef CLI_DEBUG
        mmi_printf ("\r DEBUG : help one str %s\n", i1pStr);
#endif
        u1found_flag = 0;
        i2CmdLen = (INT2) STRLEN (i1pStr);

        if (CLI_ISSET_HELP_GRPNAME (u2OptFlag))
        {
            mmi_printf ("\r\nAvailable commands :\r\n");
            mmi_printf ("\r%-10s %-6s %s\r\n", "Group", "ID", "Command");
        }
        else
        {
            mmi_printf ("\r\nAvailable commands :\r\n");
        }
        /* Discarding the \" from the command (needed for <quote_str> ) */
        if (*i1pStr == '"')
        {
            i1pStr[i2CmdLen - 1] = '\0';
            i1pStr++;
        }

        while (!u1found_flag && CLI_STRLEN (i1pStr))
        {
            p_thelp_ptr = pcurrContext->pMmiCur_mode->pHelp;
            while (p_thelp_ptr != NULL)
            {
                i2index = 0;
                while (p_thelp_ptr->pSyntax[i2index] != NULL)

                {
                    if (p_thelp_ptr->pSyntax[i2index] != CLI_REM_CMD)
                    {

                        if (CliCommandMatchSubset ((CONST UINT1 *) p_thelp_ptr->
                                                   pSyntax[i2index],
                                                   (UINT1 *) i1pStr) ==
                            CLI_SUCCESS)
                        {
                            if (CLI_ISSET_HELP_DESC (u2OptFlag) &&
                                (i1printed == FALSE))
                            {
                                i1printed = TRUE;
                            }
                            if (CLI_ISSET_HELP_GRPNAME (u2OptFlag))
                            {
                                mmi_printf ("%-10s %-6d %s\r\n",
                                            p_thelp_ptr->pGrpId[i2index],
                                            p_thelp_ptr->i4ActNumStart +
                                            i2index,
                                            p_thelp_ptr->pSyntax[i2index]);
                            }
                            else
                            {
                                mmi_printf ("%s\r\n",
                                            p_thelp_ptr->pSyntax[i2index]);
                            }
                            if (CLI_ISSET_HELP_DESC (u2OptFlag))
                            {
                                mmi_printf ("Description : %s\r",
                                            p_thelp_ptr->pSemantics[i2index]);
                                i2LineCount +=
                                    findnewlines (p_thelp_ptr->
                                                  pSemantics[i2index]);
                            }
                            i2LineCount +=
                                findnewlines (p_thelp_ptr->pSyntax[i2index]);
                            u1found_flag = 1;
                        }
                    }
                    i2index++;
                }
                p_thelp_ptr = p_thelp_ptr->pNext;
            }
            if (CLI_STRLEN (i1pStr) > 0)
            {
                i1pStr[CLI_STRLEN (i1pStr) - 1] = '\0';
            }
        }
    }
    mmi_printf ("\r\n");
    return;
}

/* 
 * for ALL help will be called by add_mode itself
 * for others it will be called from init
 */

/*
 * This routine will get the mode node from mode tree
 * and will link the new created node to the help list
 */
INT4
mmi_addhelp (i1pName, i1pSyntax, i1pSemantics, i1pCxtHelp, i1pPrivilegeId,
             i4ActNumStart)
     CONST CHR1         *i1pName;
     CONST CHR1        **i1pSyntax;
     CONST CHR1        **i1pSemantics;
     CONST CHR1        **i1pCxtHelp;
     CONST INT1         *i1pPrivilegeId;
     INT4                i4ActNumStart;
{
    t_MMI_MODE_TREE    *p_t_mode_ptr;
    t_MMI_HELP         *p_t_newhelp_ptr;
    t_MMI_HELP         *phelp_ptr, *ptemp_ptr = NULL;
    INT4                i4HelpCnt = 0;

    /* Since GrpId has to be changed dynamically we allocate
     * memory and copy the string
     */
    for (i4HelpCnt = 0; i1pPrivilegeId[i4HelpCnt] != -1; i4HelpCnt++);

    if ((p_t_mode_ptr = mmicm_search_parent (i1pName)) != NULL)
    {
        p_t_newhelp_ptr = mmicm_get_new_mmi_help_node ();
        if (!p_t_newhelp_ptr)
        {
            /* Memory allocation failure */
            return (OSIX_FAILURE);
        }
        p_t_newhelp_ptr->pSyntax = i1pSyntax;
        p_t_newhelp_ptr->pSemantics = i1pSemantics;
        p_t_newhelp_ptr->pCxtHelp = i1pCxtHelp;
        /* Since PrivilegeId has to be changed dynamically we allocate
         * memory and copy the string
         */
        if (!
            (p_t_newhelp_ptr->pPrivilegeId =
             CliMemAllocMemBlk (gCliCmdHelpPrivPoolId, CLI_MAX_CMDS_IN_GROUP)))
        {
            /* Memory allocation failure */
            MemReleaseMemBlock (gCliCmdHelpPoolId, (UINT1 *) p_t_newhelp_ptr);
            return (OSIX_FAILURE);
        }
        if (!
            (p_t_newhelp_ptr->pOrigPrivilegeId =
             CliMemAllocMemBlk (gCliCmdHelpPrivPoolId, CLI_MAX_CMDS_IN_GROUP)))
        {
            /* Memory allocation failure */
            MemReleaseMemBlock (gCliCmdHelpPrivPoolId, (UINT1 *)
                                (p_t_newhelp_ptr->pPrivilegeId));
            MemReleaseMemBlock (gCliCmdHelpPoolId, (UINT1 *) p_t_newhelp_ptr);
            return (OSIX_FAILURE);
        }

        for (i4HelpCnt = 0; i1pPrivilegeId[i4HelpCnt] != -1; i4HelpCnt++)
        {
            p_t_newhelp_ptr->pPrivilegeId[i4HelpCnt] =
                i1pPrivilegeId[i4HelpCnt];
            p_t_newhelp_ptr->pOrigPrivilegeId[i4HelpCnt] =
                i1pPrivilegeId[i4HelpCnt];
        }
        if (i4ActNumStart >= 0)
        {
            p_t_newhelp_ptr->i4ActNumStart = i4ActNumStart;
        }
        else
        {
            p_t_newhelp_ptr->i4ActNumStart = p_t_mode_ptr->i4ActionNumStartIdx;
        }
        p_t_newhelp_ptr->i4ActNumEnd = p_t_newhelp_ptr->i4ActNumStart
            + i4HelpCnt - 1;
        p_t_newhelp_ptr->pNext = NULL;
    }
    else
    {
        return OSIX_FAILURE;
    }
    if (p_t_mode_ptr->pHelp == NULL)
    {
        p_t_mode_ptr->pHelp = p_t_newhelp_ptr;
        return OSIX_SUCCESS;
    }

    phelp_ptr = p_t_mode_ptr->pHelp;
    while (phelp_ptr != NULL)
    {
        ptemp_ptr = phelp_ptr;
        phelp_ptr = phelp_ptr->pNext;
    }
    ptemp_ptr->pNext = p_t_newhelp_ptr;

    return OSIX_SUCCESS;
}

INT4
findnewlines (CONST CHR1 * str)
{
    CONST CHR1         *ptr;
    INT2                count = 0;

    ptr = str;
    while ((ptr = mmi_strstr (ptr, "\n")) != NULL)
    {
        ptr++;
        count++;
    }
    return count;
}

INT4
CliCmdPrivilege (UINT1 u1CmdType, INT1 i1PrivilegeLevel, INT1 *pi1Mode,
                 INT1 *pi1Cmd)
{
/* Commented code: Will be used when the feature to change Cmd privileges is supported
 *
    tCliContext        *pCliContext = NULL;
    INT1                ai1PromptStr[MAX_PROMPT_LEN];

 * TODO: After clarification for cmd privileges affected globally over CLI than per CLI session 
 *

    
    if (!(pCliContext = CliGetContext ()))
    {
        return CLI_FAILURE;
    }
    if (CliGetPromptFromMode (pi1Mode, ai1PromptStr) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    mmicm_copy_curconfig_to_inter (pCliContext);

    if (CliChangePath ((CONST CHR1 *) ai1PromptStr) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    mmicm_copy_inter_to_curconfig (pCliContext);
    if (u1CmdType == CLI_CMDPRV_RESET)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_SUCCESS;
    }
    return CLI_SUCCESS;
*/
    UNUSED_PARAM (u1CmdType);
    UNUSED_PARAM (i1PrivilegeLevel);
    UNUSED_PARAM (pi1Mode);
    UNUSED_PARAM (pi1Cmd);
    return CLI_SUCCESS;
}
