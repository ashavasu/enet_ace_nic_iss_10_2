/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: climain.c,v 1.235 2017/12/20 11:06:49 siva Exp $
 *
 * Description: Task creation, Timer creation 
 *
 *******************************************************************/
#ifndef __CLIMAIN_C__
#define __CLIMAIN_C__

#include "clicmds.h"
#include "clignmsg.h"
#include "fssyslog.h"
#include "fssocket.h"

/* structure contains the Context information of the 
 * Cli Sessions */
tOsixTaskId         gCliTaskId;
tCliSessions        gCliSessions;
UINT1               gu1AppContextSemName[OSIX_NAME_LEN];
UINT1               gau1Command[CLI_AUDIT_MAX_FILE_NAME_LEN +
                                CLI_AUDIT_MAX_COMMAND_LEN + 1];
tOsixSemId          gAppContextSemId;
tCliAmbMemPoolId    gCliAmbMemPoolId;
tMemPoolId          gCliMaxLineMemPoolId;
tMemPoolId          gCliMaxHelpStrMemPoolId;
tMemPoolId          gCliUserMemPoolId;
tMemPoolId          gCliGroupMemPoolId;
tMemPoolId          gCliAliasMemPoolId;
tMemPoolId          gCliPrivilegesMemPoolId;
tMemPoolId          gCliUserGroupMemPoolId;
tMemPoolId          gCliCmdListPoolId;
tMemPoolId          gCliCtxOutputBuff;
tMemPoolId          gCliCtxOutputBuffArr;
tMemPoolId          gCliCtxPendBuff;
tMemPoolId          gCliCmdModePoolId;
tMemPoolId          gCliCmdTokenPoolId;
tMemPoolId          gCliTokenPoolId;
tMemPoolId          gCliTokenRangeId;
tMemPoolId          gCliCmdHelpPoolId;
tMemPoolId          gCliAmbPtrPoolId;
tMemPoolId          gCliAmbArrPoolId;
tMemPoolId          gCliCmdHelpPrivPoolId;
tMemPoolId          gCliCtxHelpMemPoolId;
tMemPoolId          gTacMsgInputMemPoolId;
tMemPoolId          gAuditInfoMemPoolId;
tMemPoolId          gCliMaxOutputPoolId;

INT4 CliCreateTask  PROTO ((VOID));

/* This contains the entire command tree */
#ifdef  RM_WANTED
extern INT4         gi4CliPrevRmRole;
#endif
extern t_MMI_COMMAND_LIST MMIGROUP_ALL_cmds;
extern t_MMI_COMMAND_LIST MMIMODE_cmds;

INT1                ai1BSpaceStr[] = { 0x1b, 0x5b, 0x4b, 0 };
INT1                ai1MLBSpaceStr[] = { 0x1b, 0x5b, 0x41, 0 };
UINT4               gu4CliConsoleBitMap = 0;

tOsixTaskId         gCliTelnetTaskId;
UINT4               gu4CliSysLogId;

INT4                FsSshRbCompareMem (tRBElem *, tRBElem *);
VOID                FsFreeSshMemLeaks (VOID);
#define CLI_TELNET_TASK_ID gCliTelnetTaskId
/***************************************************************************  
 * FUNCTION NAME : CliCreateTask
 * DESCRIPTION   : This function is used to create the CLI task
 * INPUT         : NONE 
 * OUTPUT        : NONE
 * RETURNS       : returns 1 on OSIX_SUCESS
 *                 returns 0 on OSIX_FAILURE
 ***************************************************************************/
/*
 * Unused Function
 */
INT4
CliCreateTask (VOID)
{
    UINT4               u4Retval = OSIX_SUCCESS;
    tOsixTaskId         sTaskId = 0;
    tCliContext        *pCliContext;

#if defined (BSDCOMP_SLI_WANTED) || (defined (SLI_WANTED) && defined (TCP_WANTED))
    UINT1               au1TskName[OSIX_NAME_LEN + 4];
#endif

    if ((CliSessionsInit ()) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    CliInitUsersGroups ();
    mmi_mode_init ();
    mmi_cmd_creat ();

    /* Target specific initializations */
    CliNpInit ();

    if ((pCliContext = CliGetFreeContext ()) == NULL)
    {
        /* No free Index in Array */
        return (CLI_FAILURE);
    }

    /* The Following assingment and Increment MUST be ATOMIC
     * for multiple Telnet Sessions
     */

    CliGetTaskName (pCliContext);

    /* Create CLI default serial task only when CLI_CONSOLE switch is defined
     * in cli/make.h file. If not defined then CLI default serial task will
     * will not be created as the exe can be run as background process
     */

#ifdef CLI_CONSOLE
/*  Creating a default Serial Task first */
    u4Retval = OsixTskCrt (pCliContext->au1TskName,
                           CLI_TASK_PRIORITY,
                           CLI_DEFAULT_STACK_SIZE,
                           (OsixTskEntry) CliTaskStart, (INT1 *) pCliContext,
                           &sTaskId);
#endif
#if defined (BSDCOMP_SLI_WANTED) || (defined (SLI_WANTED) && defined (TCP_WANTED))
    if (u4Retval == OSIX_SUCCESS)
    {
        CLI_MEMSET (au1TskName, 0, (OSIX_NAME_LEN + 4));
        STRCPY (au1TskName, CLI_TELNET_TASK_NAME);
        /* Creating a Telnet Session Server Task */
        u4Retval = OsixTskCrt (au1TskName,
                               CLI_TASK_PRIORITY,
                               OSIX_DEFAULT_STACK_SIZE,
                               (OsixTskEntry) CliTelnetTaskMain, 0, &sTaskId);
        if (u4Retval == OSIX_FAILURE)
        {
            return (CLI_FAILURE);
        }

        CLI_TELNET_TASK_ID = sTaskId;
    }
#ifdef SSH_WANTED
    u4Retval = CliSshInit ();
#endif /* SSH_WANTED */
#endif
    if (u4Retval == OSIX_SUCCESS)
    {
        return (CLI_SUCCESS);
    }
    else
    {
        return (CLI_FAILURE);
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliTaskStart 
 * DESCRIPTION   : This function is used to create the CLI Queue, initialize
 *                 cmd mode tree, display CLI logo and prompt for login
 * INPUT         : NONE 
 * OUTPUT        : NONE
 * RETURNS       : NONE
 ***************************************************************************/
VOID
CliTaskStart (INT1 *pi1Param)
{
    tCliContext        *pCliContext = NULL;
    CONST UINT1        *pu1Tmp = NULL;
    UINT4               u4Len = 0;

    /* Dummy pointers for system sizing during run time */
    tCliUserBlock      *pCliUserBlock = NULL;
    tCliGroupBlock     *pCliGroupBlock = NULL;
    tCliPrivilegesBlock *pCliPrivilegesBlock = NULL;
    tCliTokenBlock     *pCliTokenBlock = NULL;
    t_MMI_RANGE        *p_MMI_RANGE = NULL;
    tCliCtxBuffArrBlock *pCliCtxBuffArrBlock = NULL;
    tCliCtxOutputBuffBlock *pCliCtxOutputBuffBlock = NULL;
    tCliCtxPendBufBlock *pCliCtxPendBufBlock = NULL;
    tCliAmbPtrBlock    *pCliAmbPtrBlock = NULL;
    tCliAmbArrBlock    *pCliAmbArrBlock = NULL;
    tCliMaxLineBlock   *pCliMaxLineBlock = NULL;
    tCliCmdHelpPrivBlock *pCliCmdHelpPrivBlock = NULL;
    tCliCtxHelpBlock   *pCliCtxHelpBlock = NULL;

    UNUSED_PARAM (pCliUserBlock);
    UNUSED_PARAM (pCliGroupBlock);
    UNUSED_PARAM (pCliPrivilegesBlock);
    UNUSED_PARAM (pCliTokenBlock);
    UNUSED_PARAM (p_MMI_RANGE);
    UNUSED_PARAM (pCliCtxBuffArrBlock);
    UNUSED_PARAM (pCliCtxOutputBuffBlock);
    UNUSED_PARAM (pCliCtxPendBufBlock);
    UNUSED_PARAM (pCliAmbPtrBlock);
    UNUSED_PARAM (pCliAmbArrBlock);
    UNUSED_PARAM (pCliMaxLineBlock);
    UNUSED_PARAM (pCliCmdHelpPrivBlock);
    UNUSED_PARAM (pCliCtxHelpBlock);

    UNUSED_PARAM (pi1Param);

    OsixTskIdSelf (&gCliTaskId);
    if ((CliSessionsInit ()) == CLI_FAILURE)
    {

        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (CliSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gCliUserMemPoolId = CLIMemPoolIds[MAX_CLI_USERS_BLOCKS_SIZING_ID];
    gCliGroupMemPoolId = CLIMemPoolIds[MAX_CLI_GROUPS_BLOCKS_SIZING_ID];
    gCliPrivilegesMemPoolId = CLIMemPoolIds[MAX_CLI_PRIV_BLOCKS_SIZING_ID];
    gCliAliasMemPoolId = CLIMemPoolIds[MAX_CLI_ALIAS_BLOCKS_SIZING_ID];
    gCliCmdListPoolId = CLIMemPoolIds[MAX_CLI_CMD_LIST_BLOCKS_SIZING_ID];
    gCliCmdTokenPoolId = CLIMemPoolIds[MAX_CLI_CMD_TOKENS_BLOCKS_SIZING_ID];
    gCliTokenPoolId = CLIMemPoolIds[MAX_CLI_TOKENS_SIZING_ID];
    gCliTokenRangeId = CLIMemPoolIds[MAX_CLI_TOKEN_RANGE_BLOCKS_SIZING_ID];
    gCliCmdModePoolId = CLIMemPoolIds[MAX_CLI_CMD_MODES_SIZING_ID];
    gCliCtxOutputBuffArr = CLIMemPoolIds[MAX_CLI_CTX_BUF_ARR_BLOCKS_SIZING_ID];
    gCliCtxOutputBuff = CLIMemPoolIds[MAX_CLI_CTX_OP_BUF_BLOCKS_SIZING_ID];
    gCliCtxPendBuff = CLIMemPoolIds[MAX_CLI_CTX_PEND_BUF_BLOCKS_SIZING_ID];
    gCliAmbPtrPoolId = CLIMemPoolIds[MAX_CLI_AMB_PTR_BLOCKS_SIZING_ID];
    gCliAmbArrPoolId = CLIMemPoolIds[MAX_CLI_AMB_ARR_BLOCKS_SIZING_ID];
    gCliAmbMemPoolId = CLIMemPoolIds[MAX_CLI_AMB_MEM_BLOCKS_SIZING_ID];
    gCliMaxLineMemPoolId = CLIMemPoolIds[MAX_CLI_MAX_LINE_BLOCKS_SIZING_ID];
    gCliUserGroupMemPoolId = CLIMemPoolIds[MAX_CLI_USRGRP_BLOCKS_SIZING_ID];
    gCliCmdHelpPoolId = CLIMemPoolIds[MAX_CLI_CMD_HELP_BLOCKS_SIZING_ID];
    gCliCmdHelpPrivPoolId =
        CLIMemPoolIds[MAX_CLI_CMD_HELP_PRIV_BLOCKS_SIZING_ID];
    gCliCtxHelpMemPoolId = CLIMemPoolIds[MAX_CLI_CTX_HELP_BLOCKS_SIZING_ID];
    gTacMsgInputMemPoolId = CLIMemPoolIds[MAX_CLI_TAC_MSG_INPUT_SIZING_ID];
    gAuditInfoMemPoolId = CLIMemPoolIds[MAX_CLI_AUDIT_INFO_SIZING_ID];
    gCliMaxOutputPoolId = CLIMemPoolIds[MAX_CLI_OUTPUT_BUF_BLOCKS_SIZING_ID];
    gCliMaxHelpStrMemPoolId = CLIMemPoolIds[MAX_CLI_HELP_STRING_SIZING_ID];
    gCliSpecialPriv.b1UsernameBased = OSIX_TRUE;
    gCliSpecialPriv.b1PrivilegeBased = OSIX_FALSE;
    gCliSpecialPriv.u1PrivilegeId = 0;
    gu1IssLoginAuthMode = LOCAL_LOGIN;

    u4Len = ((STRLEN (CLI_ROOT_USER) < sizeof (gCliSpecialPriv.au1UserName)) ?
             STRLEN (CLI_ROOT_USER) : sizeof (gCliSpecialPriv.au1UserName) - 1);
    STRNCPY (gCliSpecialPriv.au1UserName, CLI_ROOT_USER, u4Len);
    gCliSpecialPriv.au1UserName[u4Len] = '\0';

    CliInitUsersGroups ();
    mmi_mode_init ();
    mmi_cmd_creat ();

    if ((pCliContext = CliGetFreeContext ()) == NULL)
    {
        /* No free Index in Array */
        /* Indicate the status of initialization to the main routine */
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliSshSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        OsixSemDel (gCliSessions.CliPrivilegesFileSemId);
        OsixSemDel (gCliSessions.CliAmbVarSemId);
        CliSizingMemDeleteMemPools ();
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    pu1Tmp = OsixExGetTaskName (OsixGetCurTaskId ());
    u4Len = ((STRLEN (pu1Tmp) < sizeof (pCliContext->au1TskName)) ?
             STRLEN (pu1Tmp) : sizeof (pCliContext->au1TskName) - 1);
    STRNCPY (pCliContext->au1TskName, pu1Tmp, u4Len);
    pCliContext->au1TskName[u4Len] = '\0';
    pCliContext->TaskId = OsixGetCurTaskId ();
    pCliContext->i1UserAuth = FALSE;

    if (CliContextMemInit (pCliContext) == CLI_FAILURE)
    {
        /* No free Index in Array */
        /* Indicate the status of initialization to the main routine */
        CliContextLock ();
        CliContextCleanup (pCliContext);
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        CliContextUnlock ();
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliSshSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        OsixSemDel (gCliSessions.CliPrivilegesFileSemId);
        OsixSemDel (gCliSessions.CliAmbVarSemId);
        CliSizingMemDeleteMemPools ();
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    CliContextInit (pCliContext);
    if (!CLI_CONSOLE_MASKED ())
    {
        CliContextSerialInit (pCliContext);
    }
    if (CliApplicationContextInit () == CLI_FAILURE)
    {
        CliContextLock ();
        CliContextCleanup (pCliContext);
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        CliContextUnlock ();
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliSshSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        OsixSemDel (gCliSessions.CliPrivilegesFileSemId);
        OsixSemDel (gCliSessions.CliAmbVarSemId);
        CliSizingMemDeleteMemPools ();
        CLI_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef MRVLLS
    /*To Delete the tShell Task */
    CliNpInit ();
#endif

    if (pi1Param == NULL)
    {

        /*pi1Param shall be NULL only when this function is called during ISS startup */

        gu4CliSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "CLI",
                                           SYSLOG_CRITICAL_LEVEL);

    }

    /* Indicate the status of initialization to the main routine */
    CLI_INIT_COMPLETE (OSIX_SUCCESS);
    if (!CLI_CONSOLE_MASKED ())
    {
        CliSerialIoctl (NULL, CLI_GET_ENV, NULL);

        CliMainProcess (pCliContext);
    }
    else
    {
        CliContextLock ();
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        if (CliGetCurrentContextIndex () != 0)
        {
            pCliContext->i1Status = CLI_INACTIVE;
        }
        CliContextUnlock ();

    }

    return;
}

/***************************************************************************
 * FUNCTION NAME : CliWaitForever
 * DESCRIPTION   : This function waits in an infinite loop
 * INPUT         : pCliContext - pointer to CLI Context
 * OUTPUT        : NONE
 * RETURNS       : NONE
 ***************************************************************************/
VOID
CliWaitForever (tCliContext * pCliContext)
{

    while (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
    {
        OsixTskDelay (5);
    }

    return;
}

VOID
CliMainProcess (pCliContext)
     tCliContext        *pCliContext;
{
    UINT4               u4PrivilLevel;
    INT4                i4Retval;
    INT2                i2TempPrivId = 0;
    INT4                i4SessionTime = 0;
    tCliCallBackArgs    sCliCallBackArgs;

    MEMSET (&sCliCallBackArgs, 0, sizeof (tCliCallBackArgs));

    pCliContext->fpCliIOInit (pCliContext);
    pCliContext->fpCliIoctl (pCliContext, CLI_CLEAR_SCREEN, NULL);
    pCliContext->fpCliIoctl (pCliContext, CLI_SET_NONCANON, NULL);
    pCliContext->fpCliIoctl (pCliContext, CLI_ECHO_DISABLE, NULL);

    CliDisplayLogo ();

    pCliContext->gu4CliSysLogId = gu4CliSysLogId;

    if ((gu1IssLoginAuthMode != REMOTE_LOGIN_RADIUS) &&
        (gu1IssLoginAuthMode != REMOTE_LOGIN_TACACS) &&
        (gu1IssLoginAuthMode != REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL) &&
        (gu1IssLoginAuthMode != REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL) &&
        (gu1IssLoginAuthMode != PAM_LOGIN))
    {
        /* For remote radius/tacacs authentication case, privilge level
         * might have set already. dont overwrite it
         */
        pCliContext->i1PrivilegeLevel = CLI_DEFAULT_PRIVILEGE;
    }

    do
    {
        if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            i4SessionTime = pCliContext->i4SessionTimer;
        }
        CliContextInit (pCliContext);
        if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            pCliContext->i4SessionTimer = i4SessionTime;
        }
        pCliContext->mmi_exit_flag = 0;
        mmicm_init_mode_to_root (pCliContext);

        if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
        {
            /* Wait untill MSR completes. He will inform us when he is done. 
             * Since this wait is done before every login, an event has to be 
             * sent while logging out */

#ifdef MSR_WANTED
            /* As there won't be any restoration happening during node state change *
             * from active to standby and vice-versa, don't wait for this event */
#ifdef    RM_WANTED
            if (gi4CliPrevRmRole == RM_INIT)
#endif
            {
#ifdef RM_WANTED
                /* The extra check added to verify the Previous RM Statue */
                if (RmGetNodePrevState () == RM_INIT)
#endif
                {
                    CliWaitForConfRestoration ();
                }
            }
#endif
        }

        if ((!CLI_CONSOLE_MASKED ()) ||
            (pCliContext->gu4CliMode != CLI_MODE_CONSOLE))
        {
            if (!pCliContext->i1UserAuth)
            {
                pCliContext->prompt_flag = 1;
                STRCPY (pCliContext->Mmi_i1Cur_prompt, "/");
                CliSetDisplayPrompt (pCliContext);

                if ((gCliSessions.bIsClCliEnabled == OSIX_FALSE) ||
                    (pCliContext->gu4CliMode != CLI_MODE_CONSOLE) ||
                    (gCliSessions.i1LoginMethod == CLI_LOCAL_LOGIN))
                {
                    if ((i4Retval =
                         mmi_login (pCliContext, NULL)) == CLI_FAILURE)
                    {
                        mmi_exit ();
                        continue;
                    }
                }
                else
                {
                    mmi_printf
                        ("\r\n Router console is available\r\n\r\nPress RETURN to get started.\r\n");
                    while (TRUE)
                    {
                        if ((CliIsDelimit ((CliGetInput (pCliContext)), "\n"))
                            != 0)
                            break;
                    }
                    STRCPY (pCliContext->ai1UserName, CLI_GUEST_USER);
                }
            }
            else
            {
                /* If the user has already been authorized - e.g via SSH
                 * then set the previlage level and store the user name
                 * in the user name stack.
                 */
                if (CliHandlePasswordExpiry
                    (pCliContext, pCliContext->ai1UserName) == CLI_FAILURE)
                {
                    continue;
                }
                if (gu1IssLoginAuthMode == LOCAL_LOGIN)
                {
                    if (FpamGetPrivilege
                        ((CHR1 *) pCliContext->ai1UserName,
                         &i2TempPrivId) != CLI_SUCCESS)
                    {
                        continue;
                    }
                    pCliContext->i1PrivilegeLevel = (INT1) i2TempPrivId;
                }
            }

            if (pCliContext->mmi_exit_flag)
            {
                continue;
            }
        }
        CliInitUserMode (pCliContext);

#ifdef MBSM_WANTED
#ifdef ISS_WANTED
        if ((gu1IssLoginAuthMode != REMOTE_LOGIN_RADIUS) &&
            (gu1IssLoginAuthMode != REMOTE_LOGIN_TACACS))
#endif
        {
            /* Get the user index for the chassis user */
            if (mmi_search_user ((INT1 *) CLI_MBSM_CHASSIS_USER) == CLI_FAILURE)
            {
                continue;
            }
            /* Get the user index for the current CLI Context user */
            if (mmi_search_user ((INT1 *) pCliContext->ai1UserName) ==
                CLI_FAILURE)
            {
                continue;
            }

            /* Enable Privilege level for privilege level associated with the user.
             * For chassis user, the actual privilege level associated will not be used
             * effectively. The chassis user can execute only BOOTCONF mode commands.
             * Hence no need to enable the PrivilegeLevel for the chassis user.
             */

            /* Check if current user is chassis user.
             * If not enable PrivilegeLevel for that user
             */
        }

        if (((STRCMP (CLI_MBSM_CHASSIS_USER, pCliContext->ai1UserName) != 0))
#ifdef ISS_WANTED
            || ((gu1IssLoginAuthMode == REMOTE_LOGIN_RADIUS)
                || (gu1IssLoginAuthMode == REMOTE_LOGIN_TACACS))
#endif
            )
        {
#endif
            u4PrivilLevel = pCliContext->i1PrivilegeLevel;
            CliEnable (CLI_ZERO, &u4PrivilLevel);
#ifdef MBSM_WANTED
        }
#endif
        if (CLI_CONSOLE_MASKED ())
        {
            /* CLI console NOT required - stay in an infinite loop */
            CliWaitForever (pCliContext);
        }
        CliProcessCommand (pCliContext);
    }
    while (pCliContext->gu4CliMode == CLI_MODE_CONSOLE);
    CliContextLock ();
    if (pCliContext->i4BuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
        pCliContext->i4BuddyId = -1;
    }
    pCliContext->i1Status = CLI_INACTIVE;
    CliContextUnlock ();
    return;
}

/* Initializes the structure containing all the 
   previously used global variables */
INT2
CliSessionsInit (VOID)
{
    INT1                i1Log[256];

    gCliSessions.i4SessCount = 0;
    gCliSessions.p_mmi_root_mode = NULL;
    gCliSessions.pMmiCurcmd_ptr = NULL;
    gCliSessions.i2NofUsers = 0;
    gCliSessions.i2NofGroups = 0;
    gCliSessions.i2NofPrivileges = 0;

    gCliSessions.u4LoginLockOutTime = CLI_DEFAULT_LOCK_OUT_TIME;
    gCliSessions.u1LoginAttempts = CLI_DEFAULT_LOGIN_ATTEMPT;

    gCliSessions.i1LoginMethod = CLI_LOCAL_LOGIN;
    /* Call ISS nmh Routine to get the value of the Login method and set that value.
       gCliSessions.i1LoginMethod = CliGetLoginMethod ();
     */

#ifdef CLCLI_WANTED
    gCliSessions.bIsClCliEnabled = OSIX_TRUE;
#else
    gCliSessions.bIsClCliEnabled = OSIX_FALSE;
#endif
    STRCPY (gCliSessions.au1CliContextSemName, "CLI1");
    STRCPY (gCliSessions.au1CliSshSemName, "CSSH");
    STRCPY (gCliSessions.au1CliUsersFileSemName, "FIL1");
    STRCPY (gCliSessions.au1CliGroupsFileSemName, "FIL2");
    STRCPY (gCliSessions.au1CliPrivilegesFileSemName, "FIL3");
    STRCPY (gCliSessions.au1CliAmbVarSemName, "AMB1");

    if (OsixCreateSem (gCliSessions.au1CliContextSemName, 1, 0,
                       &gCliSessions.CliContextSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create Semaphore\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        return (CLI_FAILURE);
    }

    if (OsixCreateSem (gCliSessions.au1CliUsersFileSemName, 1, 0,
                       &gCliSessions.CliUsersFileSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create Semaphore\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        OsixSemDel (gCliSessions.CliContextSemId);
        return (CLI_FAILURE);
    }

    if (OsixCreateSem (gCliSessions.au1CliGroupsFileSemName, 1, 0,
                       &gCliSessions.CliGroupsFileSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create Semaphore\n");
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        UtlTrcPrint ((CHR1 *) i1Log);
        return (CLI_FAILURE);
    }

    if (OsixCreateSem (gCliSessions.au1CliPrivilegesFileSemName, 1, 0,
                       &gCliSessions.CliPrivilegesFileSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create Semaphore\n");
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        UtlTrcPrint ((CHR1 *) i1Log);
        return (CLI_FAILURE);
    }

    if (OsixCreateSem (gCliSessions.au1CliAmbVarSemName, 1, 0,
                       &gCliSessions.CliAmbVarSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create a Semaphore for Cli Amb Variable\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        OsixSemDel (gCliSessions.CliPrivilegesFileSemId);
        return (CLI_FAILURE);
    }
    if (OsixCreateSem (gCliSessions.au1CliSshSemName, 1, 0,
                       &gCliSessions.CliSshSemId) == OSIX_FAILURE)
    {
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create a Semaphore for CliSsh Variable\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        OsixSemDel (gCliSessions.CliContextSemId);
        OsixSemDel (gCliSessions.CliUsersFileSemId);
        OsixSemDel (gCliSessions.CliGroupsFileSemId);
        OsixSemDel (gCliSessions.CliPrivilegesFileSemId);
        OsixSemDel (gCliSessions.CliAmbVarSemId);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

INT4
CliContextMemInit (tCliContext * pCliContext)
{
    INT1                i1Log[256];

    CliContextLock ();

    CLI_ALLOC_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage, UINT1);
    if (pCliContext->pu1OutputMessage == NULL)
    {
        CliContextUnlock ();
        return CLI_FAILURE;
    }

    pCliContext->pu1OutputMessage[0] = '\0';

    if ((pCliContext->pu1PendBuff = CliMemAllocMemBlk (gCliCtxPendBuff,
                                                       MAX_CLI_OUTPUT_PEND_BUF_SIZE))
        == NULL)
    {
        CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage);
        pCliContext->pu1OutputMessage = NULL;

        CliContextUnlock ();
        return CLI_FAILURE;
    }
    pCliContext->u4ReqUpdated = OSIX_FALSE;

    pCliContext->pu1PendBuff[0] = '\0';
    pCliContext->u4PendBuffLen = 0;

    pCliContext->ppu1OutBuf = NULL;
    pCliContext->i2MaxRow = CLI_DEF_MAX_ROWS;
    pCliContext->i2MaxCol = CLI_DEF_MAX_COLS;
    pCliContext->i2PrevMaxRow = CLI_DEF_MAX_ROWS;
    pCliContext->i2PrevMaxCol = CLI_DEF_MAX_COLS;
    pCliContext->i1PageCount = CLI_MORE_OUT_NUMPAGE;

    if (!(pCliContext->ppu1OutBuf = CliMemAllocMemBlk (gCliCtxOutputBuffArr,
                                                       (CLI_DEF_MAX_ROWS *
                                                        CLI_MORE_OUT_NUMPAGE *
                                                        sizeof (UINT1 *)))))
    {
        CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage);
        pCliContext->pu1OutputMessage = NULL;

        MemReleaseMemBlock (gCliCtxPendBuff,
                            (UINT1 *) pCliContext->pu1PendBuff);
        pCliContext->pu1PendBuff = NULL;

        /* Malloc Failure : cannot allow the session to continue */
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nMalloc Failure :cannot allow the session to continue");
        UtlTrcPrint ((CHR1 *) i1Log);
        CliContextUnlock ();
        return CLI_FAILURE;
    }

    if ((pCliContext->ppu1OutBuf[0] =
         CliMemAllocMemBlk (gCliCtxOutputBuff,
                            ((CLI_DEF_MAX_ROWS - 1) * (CLI_DEF_MAX_COLS +
                                                       3) *
                             CLI_MORE_OUT_NUMPAGE))) == NULL)
    {
        CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage);
        pCliContext->pu1OutputMessage = NULL;

        MemReleaseMemBlock (gCliCtxPendBuff,
                            (UINT1 *) pCliContext->pu1PendBuff);
        MemReleaseMemBlock (gCliCtxOutputBuffArr,
                            (UINT1 *) pCliContext->ppu1OutBuf);
        pCliContext->pu1PendBuff = NULL;

        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nMalloc Failure :cannot allow the session to continue");
        UtlTrcPrint ((CHR1 *) i1Log);
        CliContextUnlock ();
        return CLI_FAILURE;
    }

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pCliContext->pu1UserCommand, UINT1);
    if (pCliContext->pu1UserCommand == NULL)
    {
        CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage);
        pCliContext->pu1OutputMessage = NULL;

        MemReleaseMemBlock (gCliCtxPendBuff,
                            (UINT1 *) pCliContext->pu1PendBuff);
        MemReleaseMemBlock (gCliCtxOutputBuff,
                            (UINT1 *) pCliContext->ppu1OutBuf[0]);
        pCliContext->ppu1OutBuf[0] = NULL;
        MemReleaseMemBlock (gCliCtxOutputBuffArr,
                            (UINT1 *) pCliContext->ppu1OutBuf);
        pCliContext->ppu1OutBuf = NULL;
        pCliContext->pu1PendBuff = NULL;

        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nMalloc Failure  pu1UserCommand :cannot allow the session to continue");
        UtlTrcPrint ((CHR1 *) i1Log);
        CliContextUnlock ();
        return CLI_FAILURE;
    }

    pCliContext->pCliAmbVar = NULL;
    pCliContext->i1PipeDisable = OSIX_FALSE;
    CliHandleWinSizeChange (pCliContext);
    pCliContext->i4PromptInfo = -1;
    pCliContext->u4ContextId = CLI_INVALID_CONTEXT_ID;
    pCliContext->u1SkipDoubleBang = CLI_NO_HANDLE_BANG;
    pCliContext->u4ConcatFlag = FALSE;
    CliContextUnlock ();
    return CLI_SUCCESS;
}

VOID
CliContextInit (tCliContext * pCliContext)
{
    INT2                i2Index;
    INT4                i4Index;
    INT4                i4DefExecTimeOut = 0;

    MEMSET (pCliContext->Mmi_i1Cur_prompt, '\0', MAX_PROMPT_LEN);
    MEMSET (pCliContext->au1PromptStr, '\0', MAX_PROMPT_LEN);
    MEMSET (pCliContext->au1TempPromptStr, '\0', MAX_PROMPT_LEN);
    MEMSET (pCliContext->au1InterPromptStr, '\0', MAX_PROMPT_LEN);

    MEMSET (pCliContext->Mmi_i1Mode_prompt_beg, '\0', MAX_MODE_LEVEL);
    pCliContext->Mmi_i1Cur_modelevel = 0;
    MEMSET (pCliContext->mmi_i1disp_prompt, '\0', MAX_PROMPT_LEN);
    MEMSET (pCliContext->MmiCmdToken, '\0', MAX_NO_OF_TOKENS_IN_MMI *
            MAX_CHAR_IN_TOKEN);
    MEMSET (pCliContext->ai1CmdString, '\0', MAX_LINE_LEN);
    MEMSET (pCliContext->ai1CmdBuf, '\0', MAX_LINE_LEN + 2);
    MEMSET (pCliContext->ai1FirstToken, '\0', MAX_LINE_LEN);

    MEMSET (pCliContext->mmi_i1history_str, '\0', MAX_HIST_COM * MAX_LINE_LEN);
    pCliContext->i1inter_cur_level = 0;
    pCliContext->i1temp_cur_level = 0;
    MEMSET (pCliContext->inter_cur_prompt, '\0', MAX_PROMPT_LEN);

    MEMSET (pCliContext->temp_cur_prompt, '\0', MAX_PROMPT_LEN);
    MEMSET (pCliContext->i1inter_mode_prompt_beg, '\0', MAX_MODE_LEVEL);
    MEMSET (pCliContext->i1temp_mode_prompt_beg, '\0', MAX_MODE_LEVEL);
    MEMSET (pCliContext->au1LineBuff, '\0', CLI_MAX_LINE_BUFF_LEN);

    CliIssGetDefaultExecTimeOut (&i4DefExecTimeOut);

    pCliContext->i1passwdMode = LOCAL_LOGIN;

    pCliContext->mmi_hist_top = 0;
    pCliContext->u1OptionCmd = 0;
    pCliContext->u1LastChar = 0;
    pCliContext->u1NoOfAttempts = 0;
    pCliContext->mmi_i1user_index = 0;
    MEMSET (pCliContext->mp_i2set_recur_node_flag, 0, MAX_INNER_RECUR *
            MAX_CMD_REC_PAR);
    MEMSET (pCliContext->recur_repeat_flag, 0, MAX_INNER_RECUR);
    pCliContext->mmi_cmd_reprint_flag = 0;
    pCliContext->i2HistCurrIndex = 0;
    pCliContext->i2TokenCount = 0;

    pCliContext->mmi_keychar = 0;
    pCliContext->mmi_lineno = 0;
    pCliContext->mmi_trap_count = 0;

    pCliContext->mmi_file_flag = 0;
    pCliContext->pMmiCur_mode = NULL;
    pCliContext->pmmi_user_mode = NULL;
    MEMSET (pCliContext->vpMmi_values, 0, MAX_NO_OF_TOKENS_IN_MMI);
    MEMSET (pCliContext->mmi_user_cmd_token, 0, MAX_NO_OF_TOKENS_IN_MMI);
    MEMSET (pCliContext->recur_repeat_node, 0, MAX_INNER_RECUR);
    pCliContext->env_ptr_start = NULL;
    pCliContext->amb_env_ptr_start = NULL;
    MEMSET (pCliContext->p_mp_recur_node, 0, MAX_INNER_RECUR);
    pCliContext->MMI_CMD_RET = NULL;
    pCliContext->pinter_cur_mode = NULL;
    pCliContext->pMmitemp_cur_mode = NULL;

    pCliContext->i2ScriptRecCount = 0;
    pCliContext->mmi_lineno = 1;
    pCliContext->mmi_out_put_file = FALSE;
    pCliContext->mmi_out_put_file = FALSE;
    pCliContext->mmi_exit_flag = 0;
    pCliContext->mmi_echo_flag = TRUE;
    pCliContext->mp_i2recur_flag = 0;
    pCliContext->mp_i2set_flag = 0;
    pCliContext->u4LineCount = 0;
    pCliContext->u4LineWriteCount = 0;
    pCliContext->u1BangStatus = FALSE;
    pCliContext->present_ambig_count = 0;
    pCliContext->prompt_flag = 1;
    pCliContext->passwdMode = FALSE;
    pCliContext->u1InitWinSize = FALSE;
    pCliContext->i1WinResizeFlag = FALSE;
    pCliContext->u1GrepErrorFlag = FALSE;
    if (!(pCliContext->SessionActive))
    {
        pCliContext->SessionActive = FALSE;
    }
    pCliContext->i4SessionTimer = i4DefExecTimeOut;
    pCliContext->i1PipeFlag = FALSE;
    pCliContext->i4OutBufLineIndex = 0;
    pCliContext->i1DefaultMoreFlag = CLI_MORE_ENABLE;
    pCliContext->u4Flags = 0;
    pCliContext->u4Flags = (pCliContext->u4Flags | CLI_TIOCGWINSZ_MASK);
    *pCliContext->ai1ModePath = '\0';
    pCliContext->i1MoreQuitFlag = FALSE;
    pCliContext->i1MoreFlag = FALSE;
    pCliContext->i2PgDnLineCount = 0;
    pCliContext->i1PipeDisable = OSIX_FALSE;
    pCliContext->bIsUserPrivileged = OSIX_FALSE;
    pCliContext->u1TelnetCharDiscard = OSIX_FALSE;
    pCliContext->u4CmdStatus = CLI_FAILURE;
    pCliContext->i1ClearFlagStatus = OSIX_FALSE;
    pCliContext->u1Retries = 0;
    pCliContext->i2Preindexvalue0 = 0;
    pCliContext->i2Preindexvalue1 = 0;
    pCliContext->i4Mode = 0;
    pCliContext->i1HelpFlag = 0;
    pCliContext->i1SpaceFlag = 0;
    pCliContext->i4LineBuffPos = 0;
    pCliContext->i4LineBuffLen = 0;
    pCliContext->i4AuthStatus = 0;
    pCliContext->i2CursorPos = 0;
    pCliContext->i2HistCurrIndex = 0;
    pCliContext->i2CmdLen = 0;
    pCliContext->u1CliLogOutMessage = CLI_IDLE_TIME_EXPIRY_MSG;

    /* Used to hold the pointer for Memory allocated */
    pCliContext->pu1HelpStr = NULL;
    pCliContext->pu1TknList = NULL;
    pCliContext->pu1TempHelpStr1 = NULL;
    pCliContext->pu1TempHelpStr2 = NULL;
    pCliContext->pu1OptionalTokens = NULL;
    pCliContext->pu1OutputBuf = NULL;
    pCliContext->pu1ResizeCommand = NULL;
    pCliContext->pu1TempString = NULL;
    pCliContext->pu1Temp = NULL;
    pCliContext->pu1Cmd = NULL;

    pCliContext->u1PrintOngoing = FALSE;
    for (i2Index = 0; i2Index < CLI_MAX_ALIAS; i2Index++)
    {
        pCliContext->gCliAlias[i2Index].pi1Token = NULL;
        pCliContext->gCliAlias[i2Index].pi1RepToken = NULL;
    }

    for (i2Index = 0; i2Index < CLI_MAX_GREP; i2Index++)
    {
        pCliContext->aCliGrep[i2Index].pi1GrepStr = NULL;
    }
    /* For telnet registrations, we should not make fresh registrations
     * with CLI. Since CLI console registration is already registered with
     * syslog we try to use the same ID returned by the syslog module from
     * the console session.
     */
    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        if ((gCliSessions.gaCliContext[i4Index].i1Status != CLI_INACTIVE)
            && (gCliSessions.gaCliContext[i4Index].gu4CliMode ==
                CLI_MODE_CONSOLE))
        {
            break;
        }
    }
    if (i4Index < CLI_MAX_SESSIONS)
    {
        pCliContext->gu4CliSysLogId =
            gCliSessions.gaCliContext[i4Index].gu4CliSysLogId;
    }

    CliHandleWinSizeChange (pCliContext);
    CliMemTrcInit (pCliContext);

    return;
}

VOID
CliContextSerialInit (tCliContext * pCliContext)
{

    pCliContext->gu4CliMode = CLI_MODE_CONSOLE;

    pCliContext->fpCliIOInit = CliSerialIoInit;
    pCliContext->fpCliInput = CliSerialRead;
    pCliContext->fpCliOutput = CliSerialWrite;
    pCliContext->fpCliIoctl = CliSerialIoctl;
    pCliContext->fpCliFileWrite = CliFileWrite;

    return;
}

/* Returns a pointer to the free Context structure */
tCliContext        *
CliGetContext (VOID)
{
    INT2                i2Index;
    tOsixTaskId         CurrTaskId;

    CurrTaskId = OsixGetCurTaskId ();

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if ((gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
            && (gCliSessions.gaCliContext[i2Index].i1Status == CLI_ACTIVE))
            return &(gCliSessions.gaCliContext[i2Index]);
        else
            continue;
    }

/* Error Checking */
    if (i2Index == CLI_MAX_SESSIONS)
    {
        /* Task Not Found */
        ;
    }
    return NULL;
}

tCliContext        *
CliGetFreeContext (VOID)
{
    INT1                i1Index;
    INT1                i1Log[256];
    tCliContext        *pCliContext = NULL;

    CliContextLock ();
    for (i1Index = 0; i1Index < CLI_MAX_SESSIONS; i1Index++)
    {
        if (gCliSessions.gaCliContext[i1Index].i1Status == CLI_INACTIVE)
        {
            pCliContext = &(gCliSessions.gaCliContext[i1Index]);
            if ((pCliContext->i4BuddyId =
                 MemBuddyCreate (CLI_MAX_BUDDY_BLOCK_SIZE,
                                 CLI_MIN_SESSION_MEMORY,
                                 CLI_SES_NUM_BLOCKS,
                                 BUDDY_CONT_BUF)) == BUDDY_FAILURE)
            {
                SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                          "\r\nUnable to allocate memory for new session\n");
                UtlTrcPrint ((CHR1 *) i1Log);
                pCliContext->i4BuddyId = -1;
                pCliContext = NULL;
                break;
            }
            pCliContext->i4ConnIdx = i1Index;
            pCliContext->i1Status = CLI_ACTIVE;
            break;
        }
    }
    CliContextUnlock ();
    return pCliContext;
}

INT2
CliGetCurrentContextIndex (VOID)
{
    INT2                i2Index;
    tOsixTaskId         CurrTaskId;

    CurrTaskId = OsixGetCurTaskId ();
    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
            return i2Index;
        else
            continue;
    }

    return -1;
}

VOID
CliTelnetInit (pCliContext)
     tCliContext        *pCliContext;
{
/* Telnet Stuff comes here */
    pCliContext->i4InputFd = pCliContext->i4ClientSockfd;
    pCliContext->i4OutputFd = pCliContext->i4ClientSockfd;
    return;
}

VOID
CliGetTaskName (pCliContext)
     tCliContext        *pCliContext;
{
    UINT1               au1TskName[OSIX_NAME_LEN + 4];
    static UINT2        u2Rand = 0;

    MEMSET (au1TskName, '\0', OSIX_NAME_LEN + 4);
    SNPRINTF ((CHR1 *) au1TskName, OSIX_NAME_LEN + 4,
              "%s%d%02x", CLI_TASK_NAME, pCliContext->i4ConnIdx, u2Rand);
    gCliSessions.i4SessCount++;
    MEMCPY (pCliContext->au1TskName, au1TskName, OSIX_NAME_LEN + 4);
    u2Rand++;
    if (u2Rand == 0xff)
    {
        u2Rand = 0;
    }

    return;
}

/* This function gets the Input character from the Client and
 * returns the same to the calling function
 * Input : Sessions Context of the Caling function
 * Output: Character from the client
 */
UINT1
CliGetInput (pCliContext)
     tCliContext        *pCliContext;
{
    UINT1               u1Input;
    INT1                i1Flag;

    i1Flag = pCliContext->i1MoreFlag;

    if (pCliContext->i1PipeFlag)
    {
        /* Flush the Pipe when a command output is in the pipe buffer
         * before getting an input character
         */
        pCliContext->i1MoreFlag = FALSE;
        CliFlushPipe (pCliContext);
    }
    u1Input = pCliContext->fpCliInput (pCliContext);

    pCliContext->i1MoreFlag = i1Flag;

    return u1Input;
}

/* Checks whether End of Command reached */
t_MMI_BOOLEAN
CliIsEndOfCommand (UINT1 u1Input)
{
    if ('\n' == u1Input || ';' == u1Input || '\r' == u1Input)
    {
        return TRUE;
    }
    return FALSE;
}

/* Checks for End of Token from the User Input 
 * and returns the End of Token character else returns 0
 */
UINT1
CliIsEndOfToken (UINT1 u1Input)
{
    if (';' == u1Input || '\n' == u1Input || '\t' == u1Input || u1Input == ' '
        || '\r' == u1Input)
    {
        return u1Input;
    }
    return 0;
}

VOID
CliProcessOutOfBandData (tCliContext * pCliContext, UINT1 u1Input)
{
    UNUSED_PARAM (u1Input);
    printf ("\nOut of Band Data Processing not yet implemented \n");
    pCliContext->fpCliInput (pCliContext);
    pCliContext->fpCliInput (pCliContext);

    return;
}

INT1               *
CliGetHistCommand (tCliContext * pCliContext, UINT4 u4HistIndex)
{
    UINT4               u4MinHistCmds;

    u4MinHistCmds = ((pCliContext->mmi_hist_top > MAX_HIST_COM) ?
                     pCliContext->mmi_hist_top - MAX_HIST_COM : 0);

    if (u4HistIndex >= pCliContext->mmi_hist_top || u4HistIndex < u4MinHistCmds)
        return NULL;
    u4HistIndex = (u4HistIndex % MAX_HIST_COM);
    return (pCliContext->mmi_i1history_str[u4HistIndex]);
}

INT1               *
CliGetBangCmdFromHist (tCliContext * pCliContext, INT1 *pBangStr, INT1 *pRepStr)
{
    UINT4               u4index = 0;
    INT2                i2BangStrLen;
    INT1                ai1TempStr[MAX_LINE_LEN];
    UINT4               u4MaxHistCmd;
    INT1               *pTempStr = NULL;
    INT1               *pTempHistStr = NULL;

    u4MaxHistCmd = (pCliContext->mmi_hist_top < MAX_HIST_COM) ?
        pCliContext->mmi_hist_top : MAX_HIST_COM;

    if (*pBangStr == '!')
        pBangStr++;

    pTempStr = pBangStr;

    while (*pBangStr && !CliIsEndOfToken (*pBangStr))
        pBangStr++;

    *pBangStr = '\0';

    pBangStr = pTempStr;

    i2BangStrLen = (INT2) STRLEN (pBangStr);
    if (pBangStr[i2BangStrLen - 1] == '\n')
    {
        pBangStr[--i2BangStrLen] = '\0';
    }
    while (u4index < u4MaxHistCmd)
    {
        pTempHistStr = CliGetHistCommand (pCliContext,
                                          (pCliContext->mmi_hist_top - u4index -
                                           1));
        if (pTempHistStr != NULL)
        {
            STRNCPY (ai1TempStr, pTempHistStr, MAX_LINE_LEN - 2);
            ai1TempStr[MAX_LINE_LEN - 1] = '\0';

            if (STRNCMP (pBangStr, ai1TempStr, i2BangStrLen) == 0)
            {
                STRCPY (pRepStr, ai1TempStr);
                return pRepStr;
            }
        }
        u4index++;
    }
    return NULL;
}

INT2
CliPreProcessCommand (tCliContext * pCliContext, INT1 *pi1UsrCmd)
{
    INT1               *pi1TmpCmdLine = NULL;
    INT1               *pi1TmpCmd;
    INT1               *pi1TmpUsrCmd;
    INT1                ai1TempStr[6];
    INT2                i2NumIndex = 0;
    INT1               *pHistCmd = NULL;
    INT1                i1BangFlag = 0;
    INT4                i4Count = 0;
    INT1               *pi1BangStr = NULL;
    INT1               *pi1BangRepStr = NULL;
    INT1               *pi1BangCmd;
    INT1               *pi1BangRepCmd = NULL;
    INT2                i2HistCmdLen;
    INT1               *pi1TempHistCmd = NULL;
    UINT4               u4index;
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1TmpCmdLine, INT1);
    if (pi1TmpCmdLine == NULL)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_MEMORY_ALLOCATION_FAILURE]);
        return CLI_FAILURE;
    }
    MEMSET (pi1TmpCmdLine, '\0', MAX_LINE_LEN);
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1BangStr, INT1);
    if (pi1BangStr == NULL)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_MEMORY_ALLOCATION_FAILURE]);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
        return CLI_FAILURE;
    }
    MEMSET (pi1BangStr, '\0', MAX_LINE_LEN);
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1BangRepStr, INT1);
    if (pi1BangRepStr == NULL)
    {
        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_MEMORY_ALLOCATION_FAILURE]);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
        return CLI_FAILURE;
    }
    MEMSET (pi1BangRepStr, '\0', MAX_LINE_LEN);

    pi1BangCmd = pi1BangStr;
    pi1BangRepCmd = pi1BangRepStr;
    pi1TmpCmd = pi1TmpCmdLine;
    pi1TmpUsrCmd = pi1UsrCmd;
    /* History already stores the command with '\n' */
    while (*pi1TmpUsrCmd)
    {
        if (*pi1TmpUsrCmd == '"')
        {
            if (STRSTR (pi1TmpUsrCmd + 1, "\"") == NULL)
            {
                mmi_printf (mmi_gen_messages[MMI_GEN_ERR_INVALID_USER_INPUT]);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);

                return CLI_FAILURE;

            }
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
            return CLI_SUCCESS;
        }
        if (!CliIsEndOfToken (*pi1TmpUsrCmd))
        {
            i4Count++;
        }
        if (*pi1TmpUsrCmd != '!' || (*(pi1TmpUsrCmd + 1) != '!' &&
                                     !isdigit (*(pi1TmpUsrCmd + 1)) &&
                                     !CliIsEndOfToken (*(pi1TmpUsrCmd + 1))))
        {
            if (*pi1TmpUsrCmd == '!' || CliIsEndOfToken (*pi1TmpUsrCmd))
            {
                if (CliIsEndOfCommand (*pi1TmpUsrCmd) == TRUE)
                {
                    i4Count = 0;
                }
                if (i1BangFlag == 1)
                {
                    i1BangFlag = 0;
                    CliGetBangCmdFromHist (pCliContext, pi1BangStr,
                                           pi1BangRepCmd);
                    STRCPY (pi1BangRepStr, pi1BangRepCmd);
                    i2HistCmdLen = (INT2) STRLEN (pi1BangRepStr);
                    if ((STRLEN (pi1TmpCmdLine) - STRLEN (pi1BangStr) +
                         i2HistCmdLen) > MAX_LINE_LEN)
                    {
                        mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_LONG]);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                        return (CLI_FAILURE);
                    }
                    else
                    {
                        pi1TmpCmd = pi1TmpCmdLine;
                        while (*pi1TmpCmd != '!')
                            pi1TmpCmd++;
                        STRCPY (pi1TmpCmd, pi1BangRepStr);
                        pi1TmpCmd += i2HistCmdLen;
                    }
                    pi1BangStr[0] = '\0';
                    pi1BangCmd = pi1BangStr;
                }
                if (*pi1TmpUsrCmd == '!')
                {
                    if (i4Count == 1)
                    {
                        i1BangFlag = 1;
                    }
                    else
                    {
                        mmi_printf ("\r\n%% Invalid token at input\r\n");
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                        return CLI_FAILURE;
                    }
                }
            }
            else if (i1BangFlag == 1)
            {
                *pi1BangCmd++ = *pi1TmpUsrCmd;
            }

            *pi1TmpCmd = *pi1TmpUsrCmd;
            pi1TmpUsrCmd++;
            pi1TmpCmd++;
            continue;
        }
        else if (('!' == *pi1TmpUsrCmd) &&
                 (CliIsEndOfToken ((UINT1) (*(pi1TmpUsrCmd + 1)))))
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
            return CLI_SUCCESS;
        }
        else if ('!' == *(pi1TmpUsrCmd + 1))
        {
            /*
             *If consecutive two ! symbol is there
             *1.If before that ! symbol any character is identified
             *will print the error message and return failure.
             *2.If two ! symbol are present at the front,will check for
             *any charecter present in the rest of the command.If present,
             * will print the error message and return failure.
             *3.If the command consists of only two !! , will process
             *for history
             */
            if (i4Count > 1)
            {
                mmi_printf ("\r\n%% Invalid token at input\r\n");
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                return (CLI_FAILURE);

            }
            else
            {
                pi1TempHistCmd = pi1TmpUsrCmd + 2;
                while (*pi1TempHistCmd)
                {
                    if ((!CliIsEndOfToken ((UINT1) (*pi1TempHistCmd))))
                    {
                        mmi_printf ("\r\n%% Invalid token at input\r\n");
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                        return (CLI_FAILURE);
                    }
                    pi1TempHistCmd++;
                }
            }

            u4index = pCliContext->mmi_hist_top;
            pi1TmpUsrCmd++;
        }
        else
        {
            i2NumIndex = 0;
            if (CliIsEndOfToken ((UINT1) (*(pi1TmpUsrCmd + 1)))
                || (i4Count > 1))
            {
                mmi_printf ("\r\n%% Invalid token at input\r\n");
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                return (CLI_FAILURE);
            }
            while (isdigit (*(pi1TmpUsrCmd + 1)))
            {
                ai1TempStr[i2NumIndex++] = *(++pi1TmpUsrCmd);
                if (i2NumIndex >= (INT2) sizeof (ai1TempStr))
                {
                    mmi_printf ("\r\n%% Invalid token at input\r\n");
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
                    return (CLI_FAILURE);
                }

            }
            ai1TempStr[i2NumIndex] = '\0';
            u4index = (UINT4) CLI_ATOI (ai1TempStr);
        }

        if (!u4index ||
            (pHistCmd = CliGetHistCommand (pCliContext, (u4index - 1))) == NULL)
        {
            mmi_printf (mmi_gen_messages[MMI_GEN_ERR_HIST_ERR]);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
            return (CLI_FAILURE);
        }

        i2NumIndex = (INT2) STRLEN (pHistCmd);
        if ((i2NumIndex + (INT2) STRLEN (pi1TmpCmdLine)) > MAX_LINE_LEN)
        {
            mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_LONG]);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
            return (CLI_FAILURE);
        }
        STRCPY (pi1TmpCmd, pHistCmd);
        pi1TmpUsrCmd++;
        pi1TmpCmd += i2NumIndex;
    }

    STRCPY (pi1UsrCmd, pi1TmpCmdLine);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TmpCmdLine);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangStr);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1BangRepStr);
    return (CLI_SUCCESS);
}

VOID
CliHandleControlChar (tCliContext * pCliContext, UINT1 *pu1UsrCmd,
                      UINT2 *pu2CharIndex)
{
    UINT1               u1Input;
    INT1               *pi1HistCmd = NULL;
    UINT1              *pu1PreCursorCmd;
    INT2                i2MaxHistCmds;
    INT2                i2PromptLen;
    UINT2               u2TempCurPos;

    i2MaxHistCmds = (pCliContext->mmi_hist_top < MAX_HIST_COM) ?
        pCliContext->mmi_hist_top : MAX_HIST_COM;

    pi1HistCmd = NULL;
    /* Ignoring the escape character following the Control
     * character
     */

    u1Input = (UINT1) pCliContext->fpCliInput (pCliContext);
    u1Input = (UINT1) pCliContext->fpCliInput (pCliContext);

    pCliContext->fpCliIoctl (pCliContext, CLI_GET_WINSZ, NULL);

    /* The 1 stands for the space after the prompt */

    i2PromptLen = (INT2) (STRLEN (pCliContext->mmi_i1disp_prompt) + 1);
    if (gCliSessions.bIsClCliEnabled != OSIX_TRUE)
    {
        i2PromptLen += STRLEN (mmi_gen_messages[MMI_ROOT_PROMPT]);
    }
    switch (u1Input)
    {
        case CLI_UPARROW:
            if (pCliContext->i2HistCurrIndex >= i2MaxHistCmds)
            {
                if (pCliContext->i2HistCurrIndex == i2MaxHistCmds)
                    pCliContext->i2HistCurrIndex++;
            }
            else
            {
                pCliContext->i2HistCurrIndex++;
                pi1HistCmd = CliGetHistCommand (pCliContext,
                                                (UINT4) (pCliContext->
                                                         mmi_hist_top -
                                                         pCliContext->
                                                         i2HistCurrIndex));
            }
            CliDisplayHistCmd (pCliContext, pu2CharIndex, &pu1UsrCmd,
                               pi1HistCmd, NULL);
            break;

        case CLI_DOWNARROW:
            if (pCliContext->i2HistCurrIndex > 0)
            {
                pCliContext->i2HistCurrIndex--;
                pi1HistCmd = CliGetHistCommand (pCliContext,
                                                (UINT4) (pCliContext->
                                                         mmi_hist_top -
                                                         pCliContext->
                                                         i2HistCurrIndex));
            }
            CliDisplayHistCmd (pCliContext, pu2CharIndex, &pu1UsrCmd,
                               pi1HistCmd, NULL);
            break;

        case CLI_RIGHTARROW:
            if (pCliContext->i2CursorPos == *pu2CharIndex)
            {
                CLI_BEEP ();
            }
            else
            {
                CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1PreCursorCmd, UINT1);
                if (pu1PreCursorCmd == NULL)
                {
                    return;
                }
                u2TempCurPos = (UINT2) pCliContext->i2CursorPos++;
                STRNCPY (pu1PreCursorCmd, pu1UsrCmd, pCliContext->i2CursorPos);
                pu1PreCursorCmd[pCliContext->i2CursorPos] = '\0';
                CliMultLineDisplay (pCliContext, u2TempCurPos,
                                    pu2CharIndex, pu1PreCursorCmd, NULL);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PreCursorCmd);
            }
            break;

        case CLI_LEFTARROW:
            if (!pCliContext->i2CursorPos)
            {
                CLI_BEEP ();
            }
            else
            {
                CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1PreCursorCmd, UINT1);
                if (pu1PreCursorCmd == NULL)
                {
                    return;
                }
                u2TempCurPos = --pCliContext->i2CursorPos;
                STRNCPY (pu1PreCursorCmd, pu1UsrCmd, pCliContext->i2CursorPos);
                pu1PreCursorCmd[pCliContext->i2CursorPos] = '\0';
                CliMultLineDisplay (pCliContext, (UINT2) (u2TempCurPos + 1),
                                    pu2CharIndex,
                                    (CONST UINT1 *) pu1PreCursorCmd, NULL);
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PreCursorCmd);
            }
            break;

        case CLI_HOME:
        case CLI_HOME1:
            /* Ignoring the '~' character following CLI_HOME */
            pCliContext->fpCliInput (pCliContext);
            u2TempCurPos = (UINT2) (pCliContext->i2CursorPos + 1);
            CliMultLineDisplay (pCliContext, pCliContext->i2CursorPos,
                                pu2CharIndex, NULL, NULL);
            pCliContext->i2CursorPos = 0;
            break;

        case CLI_END:
        case CLI_END1:
            /* Ignoring the '~' character following CLI_END */
            pCliContext->fpCliInput (pCliContext);
            u2TempCurPos = (UINT2) (pCliContext->i2CursorPos + 1);
            CliMultLineDisplay (pCliContext, pCliContext->i2CursorPos,
                                pu2CharIndex, pu1UsrCmd, NULL);
            pCliContext->i2CursorPos = *pu2CharIndex;
            break;

        default:
            break;

    }

    return;
}

VOID
CliMultLineDisplay (tCliContext * pCliContext, UINT2 u2PrevCmdLen,
                    UINT2 *pu2CharIndex, CONST UINT1 *pu1UsrCmd,
                    INT1 *pi1Prompt)
{
    INT2                i2NumLines = 0;
    INT2                i2PromptLen;
    INT2                i2index;
    UINT2               u2UsrCmdLen;

    if (!pu1UsrCmd)
        u2UsrCmdLen = 0;
    else
        u2UsrCmdLen = (UINT2) STRLEN (pu1UsrCmd);

    if (pi1Prompt == NULL)
    {
        /* The 1 stands for the space after the prompt */
        i2PromptLen = (INT2) (STRLEN (pCliContext->mmi_i1disp_prompt) + 1);
        if (gCliSessions.bIsClCliEnabled != OSIX_TRUE)
        {
            i2PromptLen += (INT2) STRLEN (mmi_gen_messages[MMI_ROOT_PROMPT]);
        }
    }
    else
    {
        i2PromptLen = (INT2) STRLEN (pi1Prompt);
    }

    if ((u2PrevCmdLen <= *pu2CharIndex) || (u2PrevCmdLen >= *pu2CharIndex))
    {
        i2NumLines =
            (INT2) ((i2PromptLen + u2PrevCmdLen) / (pCliContext->i2MaxCol));
    }
    for (i2index = 0; i2index < i2NumLines; i2index++)
    {
        mmi_printf ("\r%s", ai1MLBSpaceStr);
    }
    mmi_print_user_prompt (pi1Prompt, pCliContext);
    if (pu1UsrCmd)
    {
        mmi_printf ("%s", pu1UsrCmd);
    }

    if (!((u2UsrCmdLen + i2PromptLen) % pCliContext->i2MaxCol))
    {
        if (pCliContext->i2CursorPos == *pu2CharIndex)
            mmi_printf (" \r");
    }
    return;
}

VOID
CliHandleBackspace (INT1 *pi1Prompt, tCliContext * pCliContext,
                    UINT1 *pu1command, UINT2 u2CmdLen)
{
    UINT1              *pu1PostCursorCmd = NULL;
    UINT1               u1TempChar;
    UINT2               u2TempCmdLen;

    pCliContext->fpCliIoctl (pCliContext, CLI_GET_WINSZ, NULL);
    pu1PostCursorCmd = &pu1command[pCliContext->i2CursorPos + 1];

    if (pu1command != NULL && pu1command[0] != '\0')
    {
        CliMultiLineErase (pCliContext, pu1command, (UINT2) (u2CmdLen + 1),
                           pi1Prompt);
        pu1command[pCliContext->i2CursorPos] = '\0';
        u2TempCmdLen = u2CmdLen;
        u2CmdLen =
            (UINT2) (SNPRINTF
                     ((CHR1 *) pu1command, (UINT2) (u2TempCmdLen + 1), "%s%s",
                      pu1command, pu1PostCursorCmd));
        CliMultLineDisplay (pCliContext, (UINT2) (u2TempCmdLen + 1), &u2CmdLen,
                            (CONST UINT1 *) pu1command, pi1Prompt);
        if (pCliContext->i2CursorPos < u2CmdLen)
        {
            u1TempChar = pu1command[pCliContext->i2CursorPos];
            pu1command[pCliContext->i2CursorPos] = '\0';
            CliMultLineDisplay (pCliContext, u2CmdLen, &u2CmdLen,
                                pu1command, pi1Prompt);
            pu1command[pCliContext->i2CursorPos] = u1TempChar;
        }
    }
    return;
}

VOID
CliProcessCommand (pCliContext)
     tCliContext        *pCliContext;
{
    UINT1               u1Input;
    UINT1              *pu1UserCommand = pCliContext->pu1UserCommand;
    INT2                i2Retval = 0;
    INT2                i2TempPrevCmdLen = 0;
    UINT2               u2TokenIndex = 0;
    UINT2               u2CharIndex = 0;
    UINT1               au1Command[CLI_MAX_CMD_LENGTH];
    INT4                i4Mode;
    UINT4               u4Len = 0;

    if (pu1UserCommand == NULL)
    {
        return;
    }
    MEMSET (pu1UserCommand, '\0', MAX_LINE_LEN + 1);
    MEMSET (au1Command, 0, sizeof (au1Command));

    CliMemTrcInit (pCliContext);
    mmi_print_user_prompt (NULL, pCliContext);
    do
    {
        u1Input = (UINT1) pCliContext->fpCliInput (pCliContext);

        if (pCliContext->i1ClearFlagStatus == OSIX_TRUE)
        {
            pCliContext->mmi_exit_flag = OSIX_TRUE;
        }

        if (pCliContext->mmi_exit_flag)
        {
            break;
        }

        pCliContext->fpCliIoctl (pCliContext, CLI_GET_WINSZ, NULL);

        if ((pCliContext->i2MaxRow != pCliContext->i2PrevMaxRow) ||
            (pCliContext->i2MaxCol != pCliContext->i2PrevMaxCol))
        {
            CliHandleWinSizeChange (pCliContext);
            CliSerialWinResize (pCliContext, pu1UserCommand,
                                pCliContext->i2PrevMaxCol,
                                pCliContext->i2PrevMaxRow);
        }
        if (u2CharIndex >= MAX_LINE_LEN - 1)
        {
            MEMSET (pu1UserCommand, '\0', MAX_LINE_LEN);
            u2TokenIndex = 0;
            u2CharIndex = 0;
            pCliContext->i2CursorPos = 0;
            pCliContext->i2HistCurrIndex = 0;
            pCliContext->i2CmdLen = 0;
            mmi_printf ("\n");
            mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_LONG]);
            mmi_print_user_prompt (NULL, pCliContext);
            continue;
        }

        if (0 == u1Input)
        {
            continue;
        }
        else if (IS_HELP_CHAR (u1Input))
        {
            if (pCliContext->i2CursorPos < u2CharIndex)
            {
                CLI_BEEP ();
                continue;
            }
            CliAddInputToCmd (pCliContext, u1Input, pu1UserCommand,
                              &u2CharIndex);
            if ((CliHandleHelp (pCliContext, pu1UserCommand)) == CLI_FAILURE)
            {
                continue;
            }
            pCliContext->i2CursorPos--;
            u2CharIndex--;
            pu1UserCommand[u2CharIndex] = '\0';

            /* Passing u2PrevCmdLen as 0 for CliMultLineDisplay, 
             * since u2PrevCmdLen is not required for displaying
             * HELP strings.
             * */
            CliMultLineDisplay (pCliContext, 0,
                                &u2CharIndex, pu1UserCommand, NULL);
            continue;
        }
        else if (IS_TAB_CHAR (u1Input))
        {
            pCliContext->u1InputChar = TAB;
            i2TempPrevCmdLen = pCliContext->i2CursorPos;
            if ((CliHandleTab (pCliContext, pu1UserCommand,
                               &u2CharIndex)) == CLI_FAILURE)
            {
                i2TempPrevCmdLen = pCliContext->i2CmdLen;
                if (u2CharIndex >= MAX_LINE_LEN - 1)
                {
                    mmi_printf (mmi_gen_messages[MMI_GEN_ERR_TOKEN_LONG]);
                    MEMSET (pu1UserCommand, 0, MAX_LINE_LEN);
                    u2CharIndex = 0;
                    pCliContext->i2CursorPos = 0;
                    pCliContext->i2CmdLen = 0;
                }
                else
                {
                    CLI_BEEP ();
                    pCliContext->u1InputChar = 0;
                    continue;
                }
            }
            CliMultLineDisplay (pCliContext, i2TempPrevCmdLen,
                                &u2CharIndex, pu1UserCommand, NULL);
            pCliContext->u1InputChar = 0;
            continue;
        }
        else if (IS_BACKSPACE_CHAR (u1Input))
        {
            if (u2CharIndex && pCliContext->i2CursorPos)
            {
                u2CharIndex--;
                pCliContext->i2CursorPos--;
                CliHandleBackspace (NULL, pCliContext, pu1UserCommand,
                                    u2CharIndex);
                STRCPY (pCliContext->ai1CmdString, pu1UserCommand);
                pCliContext->i2CmdLen--;
            }
            else
            {
                CLI_BEEP ();
            }
            continue;
        }

        else if (IS_CONTROL_CHAR (u1Input))
        {
            CliHandleControlChar (pCliContext, pu1UserCommand, &u2CharIndex);
            continue;
        }
        else if (IS_UNDO_KEY (u1Input))
        {
            while (u2CharIndex != 0)
            {

                if (u2CharIndex && pCliContext->i2CursorPos)
                {
                    u2CharIndex--;
                    pCliContext->i2CursorPos--;
                    CliHandleBackspace (NULL, pCliContext, pu1UserCommand,
                                        u2CharIndex);
                    STRCPY (pCliContext->ai1CmdString, pu1UserCommand);
                    pCliContext->i2CmdLen--;
                }
                else
                {
                    CLI_BEEP ();
                    break;
                }
            }
            continue;
        }
        if (IS_NON_PRINTABLE_CHAR (u1Input))
        {
            continue;
        }
        /* Echoing back the character */
        CliAddInputToCmd (pCliContext, u1Input, pu1UserCommand, &u2CharIndex);
        u4Len =
            ((STRLEN (pu1UserCommand) <
              sizeof (pCliContext->
                      ai1CmdString)) ? STRLEN (pu1UserCommand) :
             sizeof (pCliContext->ai1CmdString) - 1);
        STRNCPY (pCliContext->ai1CmdString, pu1UserCommand, u4Len);
        pCliContext->ai1CmdString[u4Len] = '\0';
        pCliContext->i2CmdLen++;

        if ((CliIsDelimit (u1Input, "\n")) != 0)
        {
            if ((i2Retval = CliPreProcessCommand (pCliContext,
                                                  (INT1 *) pu1UserCommand)) !=
                CLI_FAILURE)
            {
                if (STRLEN (pu1UserCommand) > 0)
                {
                    /* If current mode is range mode */
                    if ((CliGetIfRangeMode (&i4Mode) == CLI_SUCCESS) &&
                        i4Mode == (INT4) CLI_IF_RANGE_MODE_ENABLE)
                    {
                        CliExecuteIfRangeCommand (pCliContext,
                                                  (CONST CHR1 *)
                                                  pu1UserCommand);
                    }
                    else
                    {

                        CliExecuteCommand (pCliContext,
                                           (CONST CHR1 *) pu1UserCommand);
                    }
                }
#ifdef CLI_MEM_TRC
                CliGetMemTrcMemAlloc ();
#endif
                mmi_print_user_prompt (NULL, pCliContext);
            }
            else
            {
                mmi_print_user_prompt (NULL, pCliContext);
            }

            if (NULL != pCliContext->pu1UserCommand)
            {
                MEMSET (pCliContext->pu1UserCommand, '\0', MAX_LINE_LEN);
            }

            u2TokenIndex = 0;
            u2CharIndex = 0;
            pCliContext->i2CursorPos = 0;
            pCliContext->i2HistCurrIndex = 0;
            pCliContext->i2TokenCount = 0;
            continue;
        }
        else if (CliIsEndOfToken (u1Input) != 0)
        {
            u2TokenIndex++;
            continue;
        }

        else                    /* Normal character */
        {
            /*   
               pCliContext->MmiCmdToken[u2TokenIndex][u2CharIndex++] = u1Input;
             */
            continue;
        }
    }
    while (!pCliContext->mmi_exit_flag);

    return;
}

INT2
CliSerialWinResize (tCliContext * pCliContext, UINT1 *pu1UserCmd,
                    INT2 i2PrevMaxCol, INT2 i2PrevMaxRow)
{
    INT2                i2NumLines;
    INT2                i2Index = 0;

    UNUSED_PARAM (i2PrevMaxRow);

    i2NumLines = (INT2) (STRLEN (pu1UserCmd) / i2PrevMaxCol);

    while (i2Index < i2NumLines)
    {
        mmi_printf ("\r%s%s", ai1BSpaceStr, ai1MLBSpaceStr);
        i2Index++;
    }
    mmi_printf ("\r%s", ai1BSpaceStr);
    mmi_print_user_prompt (NULL, pCliContext);
    mmi_printf ("%s", pu1UserCmd);

    return (CLI_SUCCESS);
}

INT2
CliCommandComplete (tCliContext * pCliContext, UINT1 *pu1LongestMatch,
                    INT2 u2CharIndex, INT2 *pi2AmbigCnt, UINT4 u4AmbigTknCnt)
{
    INT1               *pi1RetStr = NULL;
    INT1                ai1TempUserToken[MAX_CHAR_IN_TOKEN];
    INT2                i2Index = 0;
    INT2                i2ErrCode = 0;
    UINT2               u2TokenLen = 0;
    UINT2               u2MaxTokenLen = 0;
    UINT2               u2MatchStrLen = 0;
    UINT2               u2TempStrLen = 0;

    *pi2AmbigCnt = (INT2) pCliContext->present_ambig_count;
    CLI_STRCPY (ai1TempUserToken, "\0");

    if (pCliContext->present_ambig_count == 0)
    {
        CLI_BEEP ();
        return (CLI_SUCCESS);
    }

    if ((pCliContext->present_ambig_count > 1) ||
        ((pCliContext->present_ambig_count == 1) && (u4AmbigTknCnt <= 1)))
    {
        if (pCliContext->pCliAmbVar->amb_ptr_start[0]->i1pToken)
        {
            u2MaxTokenLen =
                (UINT2) STRLEN (pCliContext->pCliAmbVar->amb_ptr_start[0]->
                                i1pToken);
            STRCPY (pu1LongestMatch,
                    pCliContext->pCliAmbVar->amb_ptr_start[0]->i1pToken);
            u2TokenLen =
                (UINT2) STRLEN (pCliContext->
                                MmiCmdToken[pCliContext->i2TokenCount - 1]);

            for (i2Index = 1; i2Index < pCliContext->present_ambig_count;
                 i2Index++)
            {
                if (!pCliContext->pCliAmbVar->amb_ptr_start[i2Index]->i1pToken)
                    break;

                pu1LongestMatch =
                    (UINT1 *) CliStrSpn (pCliContext->pCliAmbVar->
                                         amb_ptr_start[i2Index]->i1pToken,
                                         (INT1 *) pu1LongestMatch,
                                         &u2MatchStrLen);
                u2TempStrLen =
                    (UINT2) STRLEN (pCliContext->pCliAmbVar->
                                    amb_ptr_start[i2Index]->i1pToken);
                u2MaxTokenLen =
                    u2MaxTokenLen > u2TempStrLen ? u2MaxTokenLen : u2TempStrLen;
            }
            u2TempStrLen = (UINT2) STRLEN (pu1LongestMatch);
            if ((u2CharIndex + u2TempStrLen - u2TokenLen) >= MAX_LINE_LEN - 1)
            {
                return (CLI_FAILURE);
            }
            if (u2MaxTokenLen == u2TempStrLen)
            {
                STRCAT (pu1LongestMatch, " ");
            }
        }
        else
        {
            /* Do command completion for tokens which may change the given token value into 
             * some expanded tokens such as <iftype> tokens
             */

            i2ErrCode = (INT2) (pCliContext->i2TokenCount - 1);
            if (i2ErrCode < 0)
            {
                /* Coverity false positive fix */
                return (CLI_FAILURE);
            }
            if ((pCliContext->env_ptr_start != NULL) &&
                (pCliContext->env_ptr_start->pRangeChkFunc != NULL))
            {
                /* Calls the range or validity check function */
                if (CLI_STRLEN
                    (pCliContext->MmiCmdToken[pCliContext->i2TokenCount - 1]) <
                    MAX_CHAR_IN_TOKEN)
                {
                    CLI_STRCPY (ai1TempUserToken,
                                pCliContext->MmiCmdToken[pCliContext->
                                                         i2TokenCount - 1]);

                    pi1RetStr = pCliContext->env_ptr_start->pRangeChkFunc
                        (pCliContext->env_ptr_start,
                         ai1TempUserToken, &i2ErrCode);

                    if (pi1RetStr != NULL)
                    {
                        /* Frees memory allocated in 
                         * RangeCheck Function in clieval.c */
                        CLI_BUDDY_FREE (pi1RetStr);
                    }

                    STRCPY (pu1LongestMatch, ai1TempUserToken);
                    pu1LongestMatch = (UINT1 *) CliStrSpn
                        ((CHR1 *) ai1TempUserToken,
                         (INT1 *) pu1LongestMatch, &u2MatchStrLen);
                }

            }

        }
    }
    UNUSED_PARAM (pu1LongestMatch);
    return (CLI_SUCCESS);
}

INT2
CliHandleTab (tCliContext * pCliContext, UINT1 *pu1UserCmd, UINT2 *u2CharIndex)
{
    INT4                i4ActionNumber;
    INT2                i2ModeIndex;
    INT2                i2Retval = 0;
    INT2                i2Status = CLI_SUCCESS;
    INT2                i2CmdStartIndex = -1;
    INT2                i2CmdAmbigCnt = 0;
    INT2                i2ModeAmbigCnt = 0;
    INT2                i2MatchLen = 0;
    INT2                i2Index;
    INT2                i2CmdPos = -1;
    UINT1               u1TempChar = 0;
    INT1                i1HelpFlag = FALSE;
    UINT1              *pu1TempCommand = NULL;
    UINT1              *pu1PostCursorCmd = NULL;
    UINT1              *pu1CmdPtr = NULL;
    UINT1              *pu1TempCmdBase;
    UINT1              *pu1LongestMatch = NULL;
    UINT1              *pu1CmdLongestMatch = NULL;
    UINT1              *pu1ModeLongestMatch = NULL;
    UINT1              *pu1RemCommand = NULL;
    INT1               *pi1RemCmd = NULL;
    UINT4               u4TokenCount = 0;
    UINT4               u4AmbigTokenCnt = 0;
    UINT1              *pu1TmpUserCmd = NULL;
    INT2                i2CmdLen = 0;
    INT1                i1SpaceFlag = 0;
    UINT1               u1IsSpace = 1;

    if (!pu1UserCmd || !STRLEN (pu1UserCmd))
        return (CLI_SUCCESS);

    /* tab is not handled when pressed after command delimiter ';' */
    if (pu1UserCmd[STRLEN (pu1UserCmd) - 1] == ';')
    {
        return CLI_SUCCESS;
    }

    /* handling of tab when its pressed only after all spaces */
    if (STRLEN (pu1UserCmd))
    {
        i2CmdLen = (INT2) STRLEN (pu1UserCmd);
        i2Index = 0;
        while ((i2CmdLen > 0) && (u1IsSpace))
        {
            if (!isspace (pu1UserCmd[i2Index]))
                u1IsSpace = 0;

            i2Index++;
            i2CmdLen--;
        }
        if (u1IsSpace)
            return (CLI_SUCCESS);
    }

    /*Check for redirect operator in the command */
    for (u4TokenCount = 0; u4TokenCount < STRLEN (pu1UserCmd); u4TokenCount++)
    {
        if (pu1UserCmd[u4TokenCount] == '>')
        {
            return (CLI_FAILURE);
        }
    }

    /* Tab completion supported only at the end of commands */
    if (pCliContext->i2CursorPos < *u2CharIndex)
    {
        return (CLI_FAILURE);
    }
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1TempCommand, UINT1);

    if (pu1TempCommand == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pu1TempCommand, 0, MAX_LINE_LEN + 1);

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1PostCursorCmd, UINT1);

    if (pu1PostCursorCmd == NULL)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCommand);
        return CLI_FAILURE;
    }
    MEMSET (pu1PostCursorCmd, 0, MAX_LINE_LEN + 1);

    pu1TempCmdBase = pu1TempCommand;

    if (pCliContext->i2CursorPos < *u2CharIndex)
    {
        STRCPY (pu1PostCursorCmd, &pu1UserCmd[pCliContext->i2CursorPos]);
        pu1UserCmd[pCliContext->i2CursorPos] = '\0';
    }

    CliGetCommandStartIndex (pCliContext, pu1UserCmd, &i2CmdStartIndex);
    if (i2CmdStartIndex < 0)
    {
        pu1CmdPtr = pu1UserCmd;
    }
    else
    {
        pu1CmdPtr = &pu1UserCmd[i2CmdStartIndex + 1];
    }
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1CmdLongestMatch, UINT1);

    if (pu1CmdLongestMatch == NULL)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCommand);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PostCursorCmd);
        return CLI_FAILURE;
    }
    MEMSET (pu1CmdLongestMatch, 0, MAX_LINE_LEN + 1);

    pu1CmdLongestMatch[0] = '\0';
    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1ModeLongestMatch, UINT1);

    if (pu1ModeLongestMatch == NULL)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCommand);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PostCursorCmd);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CmdLongestMatch);
        return CLI_FAILURE;
    }
    MEMSET (pu1ModeLongestMatch, 0, MAX_LINE_LEN + 1);
    pu1ModeLongestMatch[0] = '\0';

    /* To avoid tab completion on mode names in CLCLI Enabled */
    if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
    {
        CliSplitPathAndCommand (pu1CmdPtr, &i2CmdPos);
        mmicm_copy_curconfig_to_inter (pCliContext);
        if (i2CmdPos >= 0)
        {
            u1TempChar = pu1CmdPtr[i2CmdPos];
            pu1CmdPtr[i2CmdPos] = '\0';
            if ((CliChangePath ((CHR1 *) pu1CmdPtr)) == CLI_FAILURE)
            {
                CLI_BEEP ();
                mmicm_copy_inter_to_curconfig (pCliContext);
                pu1CmdPtr[i2CmdPos] = u1TempChar;
                i2Status = CLI_FAILURE;
            }
            else
            {
                pu1CmdPtr[i2CmdPos] = u1TempChar;
                STRCPY (pu1TempCommand, &pu1CmdPtr[i2CmdPos]);
                pu1RemCommand = &pu1CmdPtr[i2CmdPos];
            }
        }
        else
        {
            MEMCPY (pu1TempCommand, pu1CmdPtr, STRLEN (pu1CmdPtr));
            pu1RemCommand = pu1CmdPtr;
        }
    }
    else
    {
        MEMCPY (pu1TempCommand, pu1CmdPtr, STRLEN (pu1CmdPtr));
        pu1RemCommand = pu1CmdPtr;
    }
    if (i2Status != CLI_FAILURE)
    {
        pi1RemCmd = (INT1 *) pu1TempCommand;

        /* pCliAmbVar will be used by mmi_parse. Hence, take the AmbVar lock
         * and allocate the memory for the variable */
        OsixSemTake (gCliSessions.CliAmbVarSemId);
        CLI_ALLOC_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        if (pCliContext->pCliAmbVar == NULL)
        {
            OsixSemGive (gCliSessions.CliAmbVarSemId);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCommand);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PostCursorCmd);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CmdLongestMatch);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1ModeLongestMatch);
            return CLI_FAILURE;
        }
        if ((i2Retval = mmi_parse (pCliContext, (INT1 *) pu1TempCommand,
                                   &i4ActionNumber, &i2ModeIndex, &pi1RemCmd,
                                   FALSE)) < 0)
        {
            i1HelpFlag = TRUE;
        }
        CliRestoreCxtFileInfo (pCliContext);
        pu1TempCommand = pu1RemCommand;

        /* to calculate number of ambiguous token */
        CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1TmpUserCmd, UINT1);
        if (pu1TmpUserCmd == NULL)
        {
            OsixSemGive (gCliSessions.CliAmbVarSemId);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCommand);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PostCursorCmd);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CmdLongestMatch);
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1ModeLongestMatch);
            return CLI_FAILURE;
        }
        MEMSET (pu1TmpUserCmd, 0, MAX_LINE_LEN + 1);
        i2CmdLen = (INT2) STRLEN (pu1RemCommand);
        STRNCPY (pu1TmpUserCmd, pu1RemCommand, i2CmdLen);
        if (pu1TmpUserCmd[i2CmdLen - 1] == ' ')
        {
            i1SpaceFlag = 1;
        }
        u4AmbigTokenCnt =
            CliCxHlpDisplayCxtHelp ((INT1 *) pu1TmpUserCmd, i1SpaceFlag, 1);

        if (!pi1RemCmd || !(*pi1RemCmd))
        {
            if (!(STRCMP (pCliContext->MmiCmdToken[0], "cd")))
            {
                if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
                {
                    CliModeComplete (pCliContext, pu1TempCommand,
                                     pu1ModeLongestMatch, &i2ModeAmbigCnt);
                }
            }
            else
            {
                if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
                {
                    if ((CliModeComplete (pCliContext, pu1TempCommand,
                                          pu1ModeLongestMatch,
                                          &i2ModeAmbigCnt)) == CLI_FAILURE)
                    {
                        i2Status = CLI_FAILURE;
                    }
                }
                if ((CliCommandComplete (pCliContext, pu1CmdLongestMatch,
                                         *u2CharIndex, &i2CmdAmbigCnt,
                                         u4AmbigTokenCnt)) == CLI_FAILURE)
                {
                    i2Status = CLI_FAILURE;
                }
            }
        }
        CLI_RELEASE_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        pCliContext->pCliAmbVar = NULL;
        OsixSemGive (gCliSessions.CliAmbVarSemId);

        if (i2ModeAmbigCnt && i2CmdAmbigCnt)
        {
            if ((STRCMP (pu1ModeLongestMatch, pu1CmdLongestMatch)) > 0)
            {
                pu1LongestMatch = (UINT1 *) CliStrSpn
                    ((CHR1 *) pu1ModeLongestMatch,
                     (INT1 *) pu1CmdLongestMatch, (UINT2 *) &i2MatchLen);
            }
            else
            {
                pu1LongestMatch = (UINT1 *) CliStrSpn
                    ((CHR1 *) pu1CmdLongestMatch,
                     (INT1 *) pu1ModeLongestMatch, (UINT2 *) &i2MatchLen);
            }
        }
        else if (i2ModeAmbigCnt >= 1)
        {
            pu1LongestMatch = pu1ModeLongestMatch;
        }
        else if (i2CmdAmbigCnt >= 1)
        {
            pu1LongestMatch = pu1CmdLongestMatch;
        }
        if ((i2CmdAmbigCnt + i2ModeAmbigCnt) > 1)
        {
            i1HelpFlag = TRUE;
        }
        if (u4AmbigTokenCnt > 1)
        {
            i1HelpFlag = TRUE;
        }
        CliTabComplete (pCliContext, pu1RemCommand,
                        pu1LongestMatch, i1HelpFlag, u4AmbigTokenCnt);
        if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
        {
            mmicm_copy_inter_to_curconfig (pCliContext);
        }
        if (i2Retval > 0)
        {
            STRCPY (pu1RemCommand, pu1TempCommand);
            i2Status = CLI_SUCCESS;
        }
        else
        {
            i2Retval = CLI_FAILURE;
        }
        if (pCliContext->i2CursorPos < *u2CharIndex)
        {
            STRCAT (pu1UserCmd, pu1PostCursorCmd);
        }
        /* Free the memory alocated for value type tokens (float, int) */
        for (i2Index = 0; i2Index < MAX_NO_OF_TOKENS_IN_MMI; i2Index++)
        {
            if (pCliContext->ai1Tokentype_flag[i2Index])
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                               vpMmi_values[i2Index]);
                pCliContext->ai1Tokentype_flag[i2Index] = 0;
                pCliContext->vpMmi_values[i2Index] = NULL;
            }
        }

        *u2CharIndex = (UINT2) STRLEN (pu1UserCmd);
        pCliContext->i2CmdLen = *u2CharIndex;
    }

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1CmdLongestMatch);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1ModeLongestMatch);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TempCmdBase);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PostCursorCmd);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TmpUserCmd);

    return i2Status;
}

/* Returns the longest match of Str2 from Str1 */
INT1               *
CliStrSpn (CONST CHR1 * pi1Str1, INT1 *pi1Str2, UINT2 *u2MatchLen)
{
    INT1               *pi1TempStr;
    INT1                i1Char1;
    INT1                i1Char2;
    UINT2               u2MatchIndex = 0;

    *u2MatchLen = 0;
    pi1TempStr = pi1Str2;
    while (*pi1Str1 && *pi1Str2)
    {
        i1Char1 = (INT1) TOLOWER (*pi1Str1);
        i1Char2 = (INT1) TOLOWER (*pi1Str2);
        if (i1Char1 == i1Char2)
            u2MatchIndex++;
        else
        {
            break;
        }
        pi1Str1++;
        pi1Str2++;

    }
    *pi1Str2 = '\0';
    pi1Str2 = pi1TempStr;
    *u2MatchLen = u2MatchIndex;
    return pi1Str2;
}

VOID
CliMultiLineErase (tCliContext * pCliContext, UINT1 *pu1UsrCmd,
                   UINT2 u2CmdLen, INT1 *pi1Prompt)
{
    INT2                i2NumLines = 0;
    INT2                i2PromptLen;
    INT2                i2index;
    INT2                i2CmdEndPos;

    if (!pu1UsrCmd || !(*pu1UsrCmd))
        return;

    if (pi1Prompt == NULL)
    {
        /* The 1 stands for the space after the prompt */
        i2PromptLen = (INT2) (STRLEN (pCliContext->mmi_i1disp_prompt) + 1);
        if (gCliSessions.bIsClCliEnabled != OSIX_TRUE)
        {
            i2PromptLen += STRLEN (mmi_gen_messages[MMI_ROOT_PROMPT]);
        }
    }
    else
    {
        i2PromptLen = (INT2) STRLEN (pi1Prompt);
    }

    i2CmdEndPos = (INT2) (u2CmdLen) > pCliContext->i2CursorPos ?
        (INT2) (u2CmdLen) : pCliContext->i2CursorPos;
    i2NumLines = (INT2) ((i2PromptLen + i2CmdEndPos) / pCliContext->i2MaxCol);

    if (pCliContext->i2CursorPos < u2CmdLen - 1)
    {
        mmi_printf ("%s", &pu1UsrCmd[pCliContext->i2CursorPos + 1]);
        if (!((u2CmdLen + i2PromptLen) % pCliContext->i2MaxCol))
        {
            mmi_printf (" \r");
        }
    }
    for (i2index = 0; i2index < i2NumLines; i2index++)
    {
        mmi_printf ("\r%s%s", ai1BSpaceStr, ai1MLBSpaceStr);
    }
    mmi_printf ("\r%s", ai1BSpaceStr);
    if ((i2NumLines > 0)
        || ((i2NumLines == 0) && (i2CmdEndPos == pCliContext->i2MaxCol)))
    {
        mmi_printf ("\r%s%s", ai1BSpaceStr, ai1MLBSpaceStr);
        mmi_printf ("\r\n");
        mmi_printf ("\r");
        /* In console, some of the serial clients dont flush the existing content correctly
         * because if line is more than one, then every key stroke is causing to print a cache
         * entry in the screen, which is not sent by ISS socket.
         */
        if ((pCliContext->gu4CliMode == CLI_MODE_CONSOLE) ||
            (pCliContext->gu4CliMode == CLI_MODE_TELNET) ||
            (pCliContext->gu4CliMode == CLI_MODE_SSH))
        {
            CliClear ();
        }
    }
    else
    {
        mmi_printf ("\r%s", ai1BSpaceStr);
        mmi_printf ("\r");
    }
    return;
}

VOID
CliAddInputToCmd (tCliContext * pCliContext, UINT1 u1Input, UINT1 *pu1UserCmd,
                  UINT2 *pu2CharIndex)
{
    UINT1              *pu1PreCursorCmd = NULL;
    INT2                i2PromptLen;
    INT2                i2Index;

    /* The 1 stands for the space after the prompt */

    i2PromptLen = (INT2) (STRLEN (pCliContext->mmi_i1disp_prompt) + 1);
    if (gCliSessions.bIsClCliEnabled != OSIX_TRUE)
    {
        i2PromptLen += STRLEN (mmi_gen_messages[MMI_ROOT_PROMPT]);
    }
    if (pCliContext->i2CursorPos < *pu2CharIndex)
    {
        if (u1Input == '\n')
        {
            STRCAT (pu1UserCmd, "\n");
            CliMultLineDisplay (pCliContext, pCliContext->i2CursorPos,
                                pu2CharIndex, pu1UserCmd, NULL);
            u1Input = '\r';
            pCliContext->fpCliOutput (pCliContext, (CONST CHR1 *) & u1Input, 1);

            return;
        }
        else
        {
            pCliContext->fpCliOutput (pCliContext, (CONST CHR1 *) & u1Input, 1);
        }
        mmi_printf ("%s", &pu1UserCmd[pCliContext->i2CursorPos]);
        if (!((*pu2CharIndex + 1 + i2PromptLen) % pCliContext->i2MaxCol))
        {
            mmi_printf (" \r");
        }
        CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1PreCursorCmd, UINT1);

        if (pu1PreCursorCmd == NULL)
        {
            return;
        }

        for (i2Index = 0; i2Index < pCliContext->i2CursorPos; i2Index++)
        {
            pu1PreCursorCmd[i2Index] = pu1UserCmd[i2Index];
        }
        pu1PreCursorCmd[i2Index] = u1Input;
        pu1PreCursorCmd[i2Index + 1] = '\0';

        CliMultLineDisplay (pCliContext, (UINT2) (*pu2CharIndex + 1),
                            pu2CharIndex, (CONST UINT1 *) pu1PreCursorCmd,
                            NULL);

        STRCAT (pu1PreCursorCmd, &pu1UserCmd[pCliContext->i2CursorPos]);
        STRCPY (pu1UserCmd, pu1PreCursorCmd);
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1PreCursorCmd);
    }
    else
    {
        if (u1Input == '\n')
        {
            u1Input = '\r';
            pCliContext->fpCliOutput (pCliContext, (CONST CHR1 *) & u1Input, 1);
            u1Input = '\n';
        }

        pCliContext->fpCliOutput (pCliContext, (CONST CHR1 *) & u1Input, 1);
        pu1UserCmd[*pu2CharIndex] = u1Input;
        pu1UserCmd[*pu2CharIndex + 1] = '\0';
    }
    *pu2CharIndex = (UINT2) (*pu2CharIndex + 1);
    pCliContext->i2CursorPos++;
    return;
}

INT2
CliDisplayHistCmd (tCliContext * pCliContext, UINT2 *pu2CharIndex,
                   UINT1 **ppu1UsrCmd, INT1 *pi1HistCmd, INT1 *pi1Prompt)
{
    UINT2               u2HistCmdLen = 0;

    if (pi1HistCmd)
        u2HistCmdLen = (UINT2) STRLEN (pi1HistCmd);

    CliMultiLineErase (pCliContext, *ppu1UsrCmd, *pu2CharIndex, pi1Prompt);
    CliMultLineDisplay (pCliContext, *pu2CharIndex, pu2CharIndex,
                        (UINT1 *) pi1HistCmd, pi1Prompt);
    if (pi1HistCmd)
        STRCPY (*ppu1UsrCmd, pi1HistCmd);
    else
    {
        MEMSET (*ppu1UsrCmd, '\0', MAX_LINE_LEN);
        CLI_BEEP ();
    }
    *pu2CharIndex = u2HistCmdLen;
    pCliContext->i2CursorPos = *pu2CharIndex;

    return (CLI_SUCCESS);
}

INT2
CliNoIdleTimeout ()
{
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    pCliContext->i4SessionTimer = 0;
    return (CLI_SUCCESS);
}

INT2
CliIdleTimeout (INT4 *pi4Timeout)
{
    tCliContext        *pCliContext = NULL;
    INT2                i2Retval = CLI_SUCCESS;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    if (!pi4Timeout)
    {
        if (pCliContext->i4SessionTimer)
        {
            mmi_printf ("\rCurrent Session Timeout (in secs) = %d\n",
                        pCliContext->i4SessionTimer);
        }
        else
        {
            mmi_printf ("\rNo idle time\n");
        }
    }
    else if ((*pi4Timeout < 0) || (*pi4Timeout > CLI_MAX_SESSION_TIMEOUT))
    {
        mmi_printf ("\rSession Timeout Value out of range <0 - %d>\n",
                    CLI_MAX_SESSION_TIMEOUT);
        i2Retval = CLI_FAILURE;
    }
    else
    {
        pCliContext->i4SessionTimer = *pi4Timeout;

#ifdef SSH_WANTED
        SshArUpdateSessionTimeOut (pCliContext->i4ClientSockfd,
                                   pCliContext->i4SessionTimer);
#endif
    }
    return i2Retval;
}

INT4
CliModeComplete (tCliContext * pCliContext, UINT1 *pu1UserCmd,
                 UINT1 *pu1LongestMatch, INT2 *pi2AmbigCnt)
{
    t_MMI_MODE_TREE    *pModeTree = NULL;
    t_MMI_MODE_TREE    *pTempLinkMode = NULL;
    UINT1              *pu1UserToken = NULL;
    UINT1              *pu1Path = NULL;
    UINT1              *pu1RemCommand = NULL;
    UINT1               u1TempChar;
    INT2                i2AmbigIdx = 0;
    INT2                i2Index = 0;
    UINT2               u2TokenLen;
    INT1                ai1ModePathStr[MAX_PROMPT_LEN + 1];

    UNUSED_PARAM (pu1LongestMatch);
    for (i2Index = 0; i2Index < CLI_MAX_MODES; i2Index++)
    {
        pCliContext->pau1ModeName[i2Index] = NULL;
    }
    if (pCliContext->i2TokenCount > 2)
    {
        return (CLI_FAILURE);
    }
    else if ((STRCMP (pCliContext->MmiCmdToken[0], "cd")) &&
             (pCliContext->i2TokenCount > 1))
    {
        return (CLI_FAILURE);
    }
    pu1UserToken =
        (UINT1 *) pCliContext->MmiCmdToken[pCliContext->i2TokenCount - 1];
    if (pu1UserToken == NULL)
    {
        return (CLI_FAILURE);
    }
    u2TokenLen = (UINT2) STRLEN (pu1UserToken);
    pModeTree = pCliContext->pMmiCur_mode->pChild;
    i2AmbigIdx = 0;
    pu1RemCommand = pu1UserToken;
    pu1Path = (UINT1 *) STRCHR (pu1RemCommand, '/');
    while (pu1Path)
    {
        u1TempChar = *pu1Path;
        *pu1Path = '\0';
        if ((CliChangePath ((CHR1 *) pu1RemCommand)) == CLI_FAILURE)
        {
            CLI_BEEP ();
            *pu1Path = u1TempChar;
            return (CLI_FAILURE);
        }
        *pu1Path = u1TempChar;
        pu1RemCommand = pu1Path + 1;
        u2TokenLen = (UINT2) STRLEN (pu1RemCommand);
        pu1UserToken = pu1RemCommand;
        pu1Path = (UINT1 *) STRCHR (pu1RemCommand, '/');
    }
    pModeTree = pCliContext->pMmiCur_mode->pChild;

    if (pModeTree)
    {
        if (pModeTree->u4ModeType == CLI_LINK_MODE)
        {
            pTempLinkMode = pModeTree;
            pModeTree = pModeTree->pChild;
        }
        if (!pModeTree->PromptInfo.u1PromptType)
        {
            if (!STRNCASECMP (pModeTree->PromptInfo.PromptStr,
                              (CHR1 *) pu1UserToken, u2TokenLen))
            {
                STRCPY (ai1ModePathStr, "/");
                STRNCAT (ai1ModePathStr, pModeTree->pMode_name,
                         MAX_PROMPT_LEN - 2);
                STRCAT (ai1ModePathStr, "/");

                if (STRSTR (pCliContext->ai1ModePath, ai1ModePathStr) == NULL)
                {
                    pCliContext->pau1ModeName[i2AmbigIdx] =
                        (UINT1 *) pModeTree->PromptInfo.PromptStr;
                    i2AmbigIdx++;
                }
            }
        }
    }
    else
    {
        pModeTree = pCliContext->pMmiCur_mode;
    }
    if (pModeTree == gCliSessions.p_mmi_root_mode)
        pModeTree = pModeTree->pChild;
    else if (pModeTree == pCliContext->pMmiCur_mode)
        pModeTree = NULL;
    else
    {
        if (pTempLinkMode)
        {
            pModeTree = pTempLinkMode;
            pTempLinkMode = NULL;
        }
        pModeTree = pModeTree->pNeighbour;
    }
    if ((pModeTree) && (pModeTree->u4ModeType == CLI_LINK_MODE))
    {
        pTempLinkMode = pModeTree;
        pModeTree = pModeTree->pChild;
    }
    while (pModeTree)
    {
        if (!pModeTree->PromptInfo.u1PromptType)
        {
            if (!STRNCASECMP (pModeTree->PromptInfo.PromptStr,
                              (CHR1 *) pu1UserToken, u2TokenLen))
            {
                STRCPY (ai1ModePathStr, "/");
                STRNCAT (ai1ModePathStr, pModeTree->pMode_name,
                         MAX_PROMPT_LEN - 2);
                STRCAT (ai1ModePathStr, "/");

                if (STRSTR (pCliContext->ai1ModePath, ai1ModePathStr) == NULL)
                {
                    pCliContext->pau1ModeName[i2AmbigIdx] =
                        (UINT1 *) pModeTree->PromptInfo.PromptStr;
                    i2AmbigIdx++;
                }
            }
        }
        if (pTempLinkMode)
        {
            pModeTree = pTempLinkMode;
            pTempLinkMode = NULL;
        }

        pModeTree = pModeTree->pNeighbour;
        if ((pModeTree) && (pModeTree->u4ModeType == CLI_LINK_MODE))
        {
            pTempLinkMode = pModeTree;
            pModeTree = pModeTree->pChild;
        }
    }
    if (i2AmbigIdx)
    {
        CliGetModeLongestMatch (pCliContext, pu1UserCmd, pu1LongestMatch);
    }
    else
    {
        STRCPY (pu1LongestMatch, "");
    }
    *pi2AmbigCnt = i2AmbigIdx;

    return (CLI_SUCCESS);
}

INT4
CliHandleHelp (tCliContext * pCliContext, UINT1 *pu1UserCmd)
{
    INT1               *pi1TempCmd = NULL;
    INT1               *pi1TempPtr;
    INT1               *pi1HelpCmd = NULL;
    INT1               *pi1Token = NULL;
    INT1               *pi1RemCmd = NULL;
    INT1               *pi1Path = NULL;
    INT1                i1TempChar;
    INT1                i1SpaceFlag = 0;
    INT2                i2Index;
    UINT2               u2HelpOpt = 0;
    UINT2               u2CmdLength = 0;
    INT4                i4RetVal = CLI_SUCCESS;

    if (pu1UserCmd == NULL)
    {
        return CLI_FAILURE;
    }
    u2CmdLength = STRLEN (pu1UserCmd);

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1TempCmd, INT1);
    if (pi1TempCmd == NULL)
    {
        return CLI_FAILURE;
    }
    STRCPY (pi1TempCmd, pu1UserCmd);
    pi1TempPtr = pi1TempCmd;

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pi1HelpCmd, INT1);

    if (pi1HelpCmd == NULL)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TempCmd);
        return CLI_FAILURE;
    }
    if (pu1UserCmd[u2CmdLength - 2] == ' ')
    {
        pCliContext->i1SpaceFlag = 1;
        i1SpaceFlag = 1;
    }
    if (pi1TempCmd[u2CmdLength - 1] == CLI_HELP)
    {
        pCliContext->i1HelpFlag = 1;
    }

    pi1HelpCmd[0] = '\0';
    for (i2Index = 0; i2Index < pCliContext->i2CursorPos; i2Index++)
    {
        if (CliIsDelimit (pi1TempCmd[i2Index], CLI_EOC_DELIMITS))
        {
            pi1TempCmd[i2Index] = '\0';
            pi1TempPtr = &pi1TempCmd[i2Index + 1];
        }
    }
    /* Reconstructing the command for mmi_help() */
    pi1TempCmd[pCliContext->i2CursorPos - 1] = '\0';
    pi1RemCmd = pi1TempPtr;
    if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
    {
        if ((pi1Path = (INT1 *) STRRCHR (pi1TempPtr, '/')))
        {
            mmicm_copy_curconfig_to_temp (pCliContext);
            i1TempChar = *pi1Path;
            *pi1Path = '\0';
            if ((CliChangePath ((CHR1 *) pi1TempPtr)) == CLI_FAILURE)
            {
                CLI_BEEP ();
                mmicm_copy_temp_to_curconfig ();
                pi1TempPtr = NULL;
            }
            *pi1Path = i1TempChar;
            if (pi1TempPtr)
                pi1TempPtr = pi1Path + 1;
        }
    }

    i2Index = 0;
    while (pi1TempPtr)
    {
        CliGetNextToken (pi1TempPtr, CLI_EOT_DELIMITS, &pi1RemCmd, &pi1Token);
        if (pi1Token && *pi1Token)
        {
            if (i2Index != 0)
            {
                STRNCAT (pi1HelpCmd, " ",
                         MEM_MAX_BYTES ((MAX_LINE_LEN - STRLEN (pi1HelpCmd)),
                                        STRLEN (" ")));
            }
            STRNCAT (pi1HelpCmd, pi1Token,
                     MEM_MAX_BYTES ((MAX_LINE_LEN - STRLEN (pi1HelpCmd)),
                                    STRLEN (pi1Token)));
            i2Index++;
        }
        pi1TempPtr = pi1RemCmd;
    }
    if (i1SpaceFlag == 1)
    {
        STRNCAT (pi1HelpCmd, " ",
                 MEM_MAX_BYTES ((MAX_LINE_LEN - STRLEN (pi1HelpCmd)),
                                STRLEN (" ")));
    }
    u2HelpOpt = 0;

    if (pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE)
    {
        /* Creates the default pipe if i1DefaultMoreFlag is set */
        CliCreatePipe (pCliContext);
    }

    if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
    {
        mmi_help (pi1HelpCmd, u2HelpOpt);
        if (pi1Path)
        {
            mmicm_copy_temp_to_curconfig ();
        }
    }
    else
    {
        CliCxHlpDisplayCxtHelp (pi1HelpCmd, i1SpaceFlag, 0);
        if (pCliContext->mmi_exit_flag == TRUE)
        {
            i4RetVal = CLI_FAILURE;
        }

    }

    if (pCliContext->i1PipeFlag)
        CliClosePipe (pCliContext);

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1TempCmd);
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pi1HelpCmd);
    if (pCliContext->i4Mode == CLI_IF_RANGE_MODE_ENABLE)
    {
        pCliContext->u1NoOfAttempts = 0;
    }
    pCliContext->i1HelpFlag = 0;
    pCliContext->i1SpaceFlag = 0;
    return (i4RetVal);
}

INT4
CliHelp (tCliContext * pCliContext, UINT1 *pu1UserCmd)
{
    UINT1               u1TempChar;
    INT2                i2CmdPos;

    CliModeHelp (pCliContext);
    CliSplitPathAndCommand (pu1UserCmd, &i2CmdPos);
    if (i2CmdPos >= 0)
    {
        u1TempChar = pu1UserCmd[i2CmdPos];
        pu1UserCmd[i2CmdPos] = '\0';
        if ((CliChangePath ((CHR1 *) pu1UserCmd)) == CLI_FAILURE)
        {
            pu1UserCmd[i2CmdPos] = u1TempChar;
            return (CLI_FAILURE);
        }
        pu1UserCmd[i2CmdPos] = u1TempChar;
    }
    CliCommandHelp (pCliContext);

    return (CLI_SUCCESS);
}

INT4
CliModeHelp (tCliContext * pCliContext)
{
    INT2                i2Index;

    for (i2Index = 0;
         i2Index < CLI_MAX_MODES && pCliContext->pau1ModeName[i2Index];
         i2Index++)
    {
        mmi_printf ("\r\n%s/", pCliContext->pau1ModeName[i2Index]);
    }

    if (i2Index > 0)
    {
        mmi_printf ("\n");
    }

    return (CLI_SUCCESS);
}

INT4
CliCommandHelp (tCliContext * pCliContext)
{
    UINT1              *pu1HelpCmd = NULL;
    INT2                i2Index;
    UINT2               u2HelpOpt = 0;

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1HelpCmd, UINT1);

    if (pu1HelpCmd == NULL)
    {
        return CLI_FAILURE;
    }

    pu1HelpCmd[0] = '\0';

    /* Reconstructing User command for mmi_help() - Assumption - mmi_parse()
     * has been called and the ambiguity arrays populated and the token 
     * count updated
     * */
    for (i2Index = 0; i2Index < pCliContext->i2TokenCount - 1; i2Index++)
    {
        STRCAT (pu1HelpCmd, pCliContext->MmiCmdToken[i2Index]);
        STRCAT (pu1HelpCmd, " ");
    }
    STRCAT (pu1HelpCmd, pCliContext->MmiCmdToken[i2Index]);
    if (pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE)
    {
        /* Creates the default pipe if i1DefaultMoreFlag is set */
        CliCreatePipe (pCliContext);
    }

    if (gCliSessions.bIsClCliEnabled == OSIX_FALSE)
    {
        mmi_help ((INT1 *) pu1HelpCmd, u2HelpOpt);
    }
    else
    {
        CliDisplayHelp ((INT1 *) pu1HelpCmd, u2HelpOpt, OSIX_FALSE);
    }

    if (pCliContext->i1PipeFlag)
    {
        CliClosePipe (pCliContext);
    }

    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1HelpCmd);
    return (CLI_SUCCESS);
}

INT4
CliTabComplete (tCliContext * pCliContext, UINT1 *pu1UserCmd,
                UINT1 *pu1LongestMatch, INT1 i1HelpFlag, UINT4 u4AmbigTokenCnt)
{
    INT1                i1Status = CLI_FAILURE;
    INT2                i2TokenLen;
    INT2                i2CmdLen = 0;
    INT2                i2Index;
    INT2                i2MatchLen = 0;
    UINT1              *pu1TmpUserCmd = NULL;

    i2CmdLen = (INT2) STRLEN (pu1UserCmd);

    CLI_ALLOC_MAXLINE_MEM_BLOCK (pu1TmpUserCmd, UINT1);
    if (pu1TmpUserCmd == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pu1TmpUserCmd, 0, MAX_LINE_LEN + 1);

    STRNCPY (pu1TmpUserCmd, pu1UserCmd, i2CmdLen);

    for (i2Index = (INT2) (i2CmdLen - 1); i2Index >= 0; i2Index--)
    {
        if ((CliIsDelimit (pu1UserCmd[i2Index], CLI_EOT_DELIMITS))
            || (pu1UserCmd[i2Index] == '/'))
        {
            break;
        }
    }

    i2TokenLen = pCliContext->i2TokenCount;
    if (pu1LongestMatch && *pu1LongestMatch)
    {
        i2TokenLen =
            (INT2) STRLEN (pCliContext->
                           MmiCmdToken[pCliContext->i2TokenCount - 1]);
        if (pCliContext->pau1ModeName[0])
        {
            STRCAT ((CHR1 *) pu1UserCmd,
                    &pu1LongestMatch[i2CmdLen - i2Index - 1]);
            if (!i1HelpFlag)
            {
                STRCAT (pu1UserCmd, "/");
            }
            i1Status = CLI_SUCCESS;
        }
        else if (pCliContext->present_ambig_count)
        {
            STRCAT ((CHR1 *) pu1UserCmd, &pu1LongestMatch[i2TokenLen]);
            i1Status = CLI_SUCCESS;
        }
    }
    i2MatchLen = (INT2) (STRLEN (pu1UserCmd) - i2CmdLen);
    pCliContext->i2CursorPos += i2MatchLen;

    if (u4AmbigTokenCnt >= 1)
    {
        CliHelp (pCliContext, pu1TmpUserCmd);
    }
    else if (i1HelpFlag)
    {
        CliHelp (pCliContext, pu1UserCmd);
    }
    CLI_RELEASE_MAXLINE_MEM_BLOCK (pu1TmpUserCmd);
    return i1Status;
}

INT4
CliGetModeLongestMatch (tCliContext * pCliContext, UINT1 *pu1UserCmd,
                        UINT1 *pu1LongestMatch)
{
    INT2                i2Status = CLI_SUCCESS;
    INT2                i2Index = 0;
    UINT2               u2MaxTokenLen = 0;
    UINT2               u2MatchStrLen = 0;
    UINT2               u2TempStrLen = 0;
    UINT2               u2UserCmdLen = 0;
    UINT2               u2TokenLen = 0;

    u2UserCmdLen = (UINT2) STRLEN (pu1UserCmd);
    u2MaxTokenLen = (UINT2) STRLEN (pCliContext->pau1ModeName[0]);
    STRCPY (pu1LongestMatch, pCliContext->pau1ModeName[0]);

    for (i2Index = 1; pCliContext->pau1ModeName[i2Index]; i2Index++)
    {
        pu1LongestMatch = (UINT1 *) CliStrSpn ((CHR1 *) pCliContext->
                                               pau1ModeName[i2Index],
                                               (INT1 *) pu1LongestMatch,
                                               &u2MatchStrLen);
        u2TempStrLen = (UINT2) STRLEN (pCliContext->pau1ModeName[i2Index]);
        u2MaxTokenLen = u2MaxTokenLen > u2TempStrLen ?
            u2MaxTokenLen : u2TempStrLen;
    }
    if ((u2UserCmdLen + (UINT2) STRLEN (&pu1LongestMatch[u2TokenLen])) >=
        MAX_LINE_LEN - 1)
    {
        i2Status = CLI_FAILURE;
    }

    return (i2Status);
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliGetCommandStartIndex                             *
 *                                                                         *
 *     Description   : The function is used to get the Starting index of   *
 *                     a command from the current cursor position.         *
 *                                                                         *
 *     Input(s)      : pCliContext : Pointer to Context structure          *
 *                     pu1UserCmd  : Pointer to the User command where the *
 *                                   start index have to be extracted.     *
 *     Output(s)     : pi2CmdStartIndex : Index of the Command Start       *
 *                                   position from the cursor position.    *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE.                            *
 *                                                                         *
 ***************************************************************************/

INT4
CliGetCommandStartIndex (tCliContext * pCliContext, UINT1 *pu1UserCmd,
                         INT2 *pi2CmdStartIndex)
{
    INT2                i2Index;
    *pi2CmdStartIndex = -1;
    if (!pu1UserCmd)
    {
        return (CLI_FAILURE);
    }
    for (i2Index = pCliContext->i2CursorPos; i2Index >= 0; i2Index--)
    {
        if ((CliIsDelimit (pu1UserCmd[i2Index], CLI_EOC_DELIMITS))
            && (pu1UserCmd[i2Index] != '\n'))
        {
            break;
        }
    }
    if (i2Index < 0)
        *pi2CmdStartIndex = -1;
    else
        *pi2CmdStartIndex = i2Index;

    return (CLI_SUCCESS);
}

INT4
CliSplitPathAndCommand (UINT1 *pu1UserCommand, INT2 *pu2CmdPos)
{
    UINT1              *pu1UserCmdPtr;
    INT2                i2Index = 0;

    pu1UserCmdPtr = pu1UserCommand;
    *pu2CmdPos = -1;

    /* Eating the EOT delimits at the start of command */
    while (CliIsDelimit (*pu1UserCmdPtr, CLI_EOT_DELIMITS))
    {
        pu1UserCmdPtr++;
        i2Index++;
    }
    while ((*pu1UserCmdPtr) &&
           !(CliIsDelimit (*pu1UserCmdPtr, CLI_EOT_DELIMITS)))
    {
        i2Index++;
        if (*pu1UserCmdPtr == '/')
        {
            *pu2CmdPos = i2Index;
        }
        pu1UserCmdPtr++;
    }

    return (CLI_SUCCESS);
}

INT4
CliCreatePipe (tCliContext * pCliContext)
{
    /* Updating the Cli Pipe index */
    INT4                i4RetVal = CLI_SUCCESS;

    if (pCliContext->i1PipeDisable == OSIX_TRUE)
    {
        i4RetVal = CLI_FAILURE;
    }
    else if (!pCliContext->i1PipeFlag && (pCliContext->mmi_file_flag != TRUE))
    {
        pCliContext->fpCliTempOutput = pCliContext->fpCliOutput;
        pCliContext->fpCliOutput = CliPipeWrite;
        pCliContext->i1PipeFlag = TRUE;
        pCliContext->i1WinResizeFlag = FALSE;
    }

    return i4RetVal;
}

/***************************************************************************
 *  FUNCTION NAME : CliPipeWrite
 *  DESCRIPTION   : This function writes output message to the Pipe buffer on
 *                  the output stream gfpCliOutput       
 *  INPUT         : IfIndex, pu1OutMsg, Length 
 *  OUTPUT        : NONE
 *  RETURNS       : No of Bytes written 
 ****************************************************************************/

INT4
CliPipeWrite (tCliContext * pCliContext, CONST CHR1 * pOutMsg, UINT4 u4Length)
{
    INT4                i4LineIndex = 0;
    UINT4               u4WriteCount = 0;
    INT2                i2Index = 0;
    INT2                i2TempIndex = 0;
    INT2                i2OrigTempIndex = 0;
    INT2                i2RetVal = CLI_SUCCESS;
    INT1                i1EndOfData = FALSE;
    INT4                i4LastIndex = 0;    /* Last index of line displayed */
    INT4                i4NextIndex = 0;
    INT1                i1ResizeFlag = FALSE;
    INT1                i1ConFlag = FALSE;
    UINT4               u4Len = 0;
    UINT4               u4TempLen = 0;
    UINT1              *pu1TempCmd = NULL;

    UINT1              *pu1ResizeCommand = NULL;
    UINT1              *pu1TempString = NULL;
    UINT4               u4MaxLineLen = MAX_LINE_LEN;

    if (!u4Length)
        return (CLI_SUCCESS);

    if (!pCliContext->i1PipeFlag)
        return (CLI_FAILURE);

    /* Check if 'q' entered at 'more' display */
    if (pCliContext->i1MoreQuitFlag)
        return CLI_FAILURE;

    if (pCliContext->u4LineCount)
    {
        i4LineIndex = (pCliContext->u4LineCount %
                       ((pCliContext->i2MaxRow - 1) *
                        pCliContext->i1PageCount));
        if (pCliContext->ppu1OutBuf[i4LineIndex] == NULL)
        {
            pCliContext->i1MoreQuitFlag = TRUE;
            return CLI_FAILURE;
        }

        if (!(*pCliContext->ppu1OutBuf[i4LineIndex]))
        {
            i4LineIndex = ((pCliContext->u4LineCount - 1) %
                           ((pCliContext->i2MaxRow - 1) *
                            pCliContext->i1PageCount));
        }
    }
    else
    {
        i4LineIndex = 0;
    }

    i4LastIndex = i4LineIndex;
    u4WriteCount = STRLEN (pCliContext->ppu1OutBuf[i4LineIndex]);

    /* checks whether end of buffer (ppu1OutBuf)
     *  has a carriage return or not and
     * increments i4LineIndex accordingly to new line
     */
    if (u4WriteCount)
    {
        if (pCliContext->ppu1OutBuf[i4LineIndex][u4WriteCount - 1] == '\n')
        {
            u4WriteCount = 0;
            i4LineIndex++;
        }
    }

    pu1ResizeCommand =
        CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN + 1);
    if (pu1ResizeCommand == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for command String\n");
        return CLI_FAILURE;
    }
    pCliContext->pu1ResizeCommand = pu1ResizeCommand;

    pu1TempString = CliMemAllocMemBlk (gCliMaxLineMemPoolId, MAX_LINE_LEN + 1);
    if (pu1TempString == NULL)
    {
        mmi_printf ("\r\n Cannot allocate memory for command String\n");

        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &pCliContext->pu1ResizeCommand);
        return CLI_FAILURE;
    }
    pCliContext->pu1TempString = pu1TempString;

    while (!i1EndOfData)
    {
        while (1)
        {
            if ((pCliContext->mmi_exit_flag == TRUE) ||
                (pCliContext->i1MoreQuitFlag == TRUE))
            {
                i1EndOfData = TRUE;
                break;
            }
            if ((i2TempIndex == (INT4) u4Length) ||
                (i4LineIndex >= ((pCliContext->i2MaxRow - 1) *
                                 pCliContext->i1PageCount)))
            {
                /* Append a '\0' at the end of data */
                if (i4LineIndex < ((pCliContext->i2MaxRow - 1) *
                                   pCliContext->i1PageCount))
                {
                    pCliContext->ppu1OutBuf[i4LineIndex][u4WriteCount++] = '\0';
                }
                else
                {
                    i4LineIndex = 0;
                }
                break;
            }
            pCliContext->ppu1OutBuf[i4LineIndex][u4WriteCount] =
                pOutMsg[i2TempIndex];
            if (!pOutMsg[i2TempIndex])
            {
                i2TempIndex++;
                i4NextIndex = (i4LineIndex + 1) %
                    ((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount);
                pCliContext->ppu1OutBuf[i4NextIndex][0] = '\0';
                if (CliMorePrintf (pCliContext, i4LineIndex) == CLI_FAILURE)
                {
                    pCliContext->i1MoreQuitFlag = TRUE;

                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                           &pCliContext->pu1ResizeCommand);
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                           &pCliContext->pu1TempString);
                    return CLI_FAILURE;
                }
                pCliContext->fpCliOutput (pCliContext, (CONST CHR1 *) "\n", 1);
                break;
            }

            if (((CliIsDelimit (pOutMsg[i2TempIndex], "\n")) ||
                 (u4WriteCount == (UINT4) (pCliContext->i2MaxCol)))
                && (pCliContext->i1GrepFlag != 1))
            {
                i2OrigTempIndex = i2TempIndex;
                /* checks for end of line, appends \r\n to end of buffer */
                if (CliIsDelimit (pOutMsg[i2TempIndex], "\r\n"))
                    i2TempIndex++;

                if (u4WriteCount != (UINT4) (pCliContext->i2MaxCol))
                {
                    SPRINTF ((CHR1 *) & pCliContext->
                             ppu1OutBuf[i4LineIndex][u4WriteCount], "\r\n");
                }
                else
                {

                    /*maximum column is equal to writecount */
                    if (CliIsDelimit (pOutMsg[i2OrigTempIndex], "\r"))
                    {
                        SPRINTF ((CHR1 *) & pCliContext->
                                 ppu1OutBuf[i4LineIndex][u4WriteCount], "\n");
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) & pCliContext->
                                 ppu1OutBuf[i4LineIndex][u4WriteCount], "\r\n");
                    }
                }

                i4NextIndex = (i4LineIndex + 1) %
                    ((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount);
                pCliContext->ppu1OutBuf[i4NextIndex][0] = '\0';
                if (CliMorePrintf (pCliContext, i4LineIndex) == CLI_FAILURE)
                {
                    pCliContext->i1MoreQuitFlag = TRUE;
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                           &pCliContext->pu1ResizeCommand);
                    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                           &pCliContext->pu1TempString);
                    return CLI_FAILURE;
                }
                i4LineIndex++;
                u4WriteCount = 0;
                i2Index = 0;
            }

            else if (((CliIsDelimit (pOutMsg[i2TempIndex], "\n")) ||
                      (u4WriteCount == (UINT4) (pCliContext->i2MaxCol)))
                     && (pCliContext->i1GrepFlag == 1))
            {
                i2OrigTempIndex = i2TempIndex;

                /* checks for end of line, appends \r\n to end of buffer */
                if (CliIsDelimit (pOutMsg[i2TempIndex], "\r\n"))
                    i2TempIndex++;

                if (u4WriteCount != (UINT4) (pCliContext->i2MaxCol))
                {

                    if ((i1ResizeFlag == TRUE) && (i1ConFlag == TRUE))
                    {

                        /* when command length exceeds the window width
                         * CliMorePrintf will be called only when we reach end
                         * of the command. whenever we reach the writecount as i2MaxCol
                         * the content of pCliContext->ppu1OutBuf[i4LineIndex] will
                         * be copied in to a local array , when End of command is reached
                         * entire command will be copied in to a single array of 
                         * pCliContext->ppu1OutBuf[i4LineIndex], as the size of 
                         * pCliContext->ppu1OutBuf[i4LineIndex] will be small
                         * extra memory allocation will be done for storing the 
                         * entire command in a single array and then CliMorePrintf 
                         * is called. This is done to avoid truncation of command 
                         * display in grep usage. */

                        pCliContext->ppu1OutBuf[i4LineIndex][u4WriteCount + 1] =
                            '\0';
                        if (STRLEN (pCliContext->ppu1OutBuf[i4LineIndex]) >=
                            MAX_LINE_LEN)
                        {
                            STRNCPY (pu1TempString,
                                     pCliContext->ppu1OutBuf[i4LineIndex],
                                     MAX_LINE_LEN - 1);
                            pu1TempString[MAX_LINE_LEN - 1] = '\0';
                        }
                        else
                        {
                            STRCPY (pu1TempString,
                                    pCliContext->ppu1OutBuf[i4LineIndex]);
                        }
                        if ((STRLEN (pu1TempString) +
                             STRLEN (pu1ResizeCommand)) >= MAX_LINE_LEN)
                        {
                            u4TempLen =
                                u4MaxLineLen - STRLEN (pu1ResizeCommand) - 1;
                            STRNCAT (pu1ResizeCommand, pu1TempString,
                                     u4TempLen);
                        }
                        else
                        {
                            STRCAT (pu1ResizeCommand, pu1TempString);
                        }
                        pu1TempCmd = pCliContext->ppu1OutBuf[i4LineIndex];
                        pCliContext->ppu1OutBuf[i4LineIndex] =
                            CliMemAllocMemBlk (gCliMaxLineMemPoolId,
                                               MAX_LINE_LEN);
                        if (pCliContext->ppu1OutBuf[i4LineIndex] == NULL)
                        {
                            mmi_printf
                                ("\r\n Cannot allocate memory for command String\n");
                            CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                                   &pCliContext->
                                                   pu1ResizeCommand);
                            CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                                   &pCliContext->pu1TempString);
                            return CLI_FAILURE;
                        }

                        STRCPY (pCliContext->ppu1OutBuf[i4LineIndex],
                                pu1ResizeCommand);

                        /* To indicate concatenation has been done, u4ConcatFlag is
                         * set */
                        pCliContext->u4ConcatFlag = TRUE;

                        /* Reset i1ResizeFlag as end of command is reached and the
                         * entire command has been concatenated in single array */

                        i1ResizeFlag = FALSE;

                    }
                    if (i1ConFlag == FALSE)
                    {
                        /* Resizing is not done, ie entire commands fits with in the 
                         * window width*/

                        SPRINTF ((CHR1 *) & pCliContext->
                                 ppu1OutBuf[i4LineIndex][u4WriteCount], "\r\n");

                    }
                    else
                    {
                        /* Resizing is done and the entire command has been copied 
                         * in to a single array of pCliContext->ppu1OutBuf[i4LineIndex]
                         **/
                        u4Len = STRLEN (pCliContext->ppu1OutBuf[i4LineIndex]);

                        SPRINTF ((CHR1 *) & pCliContext->
                                 ppu1OutBuf[i4LineIndex][u4Len - 1], "\r\n");
                    }

                }
                else
                {
                    /*maximum column width is equal to writecount */

                    u4Len = STRLEN (pCliContext->ppu1OutBuf[i4LineIndex]);
                    u4Len = u4Len - 1;
                    u4TempLen = u4MaxLineLen - STRLEN (pu1ResizeCommand) - 1;
                    if (u4Len > u4TempLen)
                    {
                        u4Len = u4TempLen;
                    }
                    STRNCAT (pu1ResizeCommand,
                             pCliContext->ppu1OutBuf[i4LineIndex], u4Len);
                    if (CliIsDelimit (pOutMsg[i2OrigTempIndex], "\n"))
                        i2TempIndex--;

                    /*i1ResizeFlag is and i1ConFlag are 
                     * set when writecount is
                     * equal to the window width */

                    /* memset is done since the array is copied to local variable */

                    CLI_MEMSET (pCliContext->ppu1OutBuf[i4LineIndex], 0,
                                STRLEN (pCliContext->ppu1OutBuf[i4LineIndex]));
                    i1ResizeFlag = TRUE;
                    i1ConFlag = TRUE;
                }

                i4NextIndex = (i4LineIndex + 1) %
                    ((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount);
                pCliContext->ppu1OutBuf[i4NextIndex][0] = '\0';

                if (i1ResizeFlag == FALSE)
                {

                    if (CliMorePrintf (pCliContext, i4LineIndex) == CLI_FAILURE)
                    {
                        pCliContext->i1MoreQuitFlag = TRUE;

                        if (i1ConFlag == TRUE)
                        {
                            MemReleaseMemBlock (gCliMaxLineMemPoolId,
                                                pCliContext->
                                                ppu1OutBuf[i4LineIndex]);
                            pCliContext->ppu1OutBuf[i4LineIndex] = pu1TempCmd;

                            /*Reset i1ConFlag to default value as 
                             * CliMorePrintf returned failure*/

                            pCliContext->u4ConcatFlag = FALSE;
                            i1ConFlag = FALSE;
                        }
                        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                               &pCliContext->pu1ResizeCommand);
                        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                                               &pCliContext->pu1TempString);
                        return CLI_FAILURE;

                    }
                    else
                    {
                        /* CliMorePrintf is success */
                        if (i1ConFlag == TRUE)
                        {
                            MemReleaseMemBlock (gCliMaxLineMemPoolId,
                                                pCliContext->
                                                ppu1OutBuf[i4LineIndex]);
                            pCliContext->ppu1OutBuf[i4LineIndex] = pu1TempCmd;
                            /*Reset i1ConFlag as the command has been flushed out
                               in CliMorePrintf */

                            pCliContext->u4ConcatFlag = FALSE;
                            i1ConFlag = FALSE;
                        }
                    }
                }
                /* Check if the copied string to concatenate with the next command is not cleared */
                if (i1ResizeFlag == FALSE)
                {
                    CLI_MEMSET (pu1ResizeCommand, 0, MAX_LINE_LEN);
                }
                CLI_MEMSET (pu1TempString, 0, MAX_LINE_LEN);
                i4LineIndex++;
                u4WriteCount = 0;
            }
            else
            {
                u4WriteCount++;
                i2TempIndex++;
                i2Index++;
            }
            i4LastIndex++;
        }
        u4WriteCount = 0;

        if (i2TempIndex >= (INT4) u4Length)
            i1EndOfData = TRUE;

    }
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                           &pCliContext->pu1ResizeCommand);
    CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &pCliContext->pu1TempString);
    return (i2RetVal);
}

INT4
CliProcessGrep (tCliContext * pCliContext, UINT1 *pu1Buf)
{
    INT2                i2Index = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    for (i2Index = 0; ((i2Index < CLI_MAX_GREP) &&
                       (pCliContext->aCliGrep[i2Index].pi1GrepStr)); i2Index++)
    {
        if ((i4RetStatus = CliGrep (pCliContext, i2Index, pu1Buf)) ==
            CLI_FAILURE)
        {
            /* Pattern match failed */
            break;
        }
    }

    return (i4RetStatus);
}

INT4
CliGrep (tCliContext * pCliContext, INT2 i2GrepIndex, UINT1 *pu1LineBuf)
{
    UINT1              *pu1SearchStr = NULL;
    INT1                i1OptFlag = 0;
    INT4                i4RetVal = CLI_FAILURE;

    pu1SearchStr = (UINT1 *) pCliContext->aCliGrep[i2GrepIndex].pi1GrepStr;
    i1OptFlag = pCliContext->aCliGrep[i2GrepIndex].i1OptFlag;

    if (!pu1SearchStr)
        return (CLI_FAILURE);

    if (*pu1SearchStr == '"')
    {
        if (!CliExtractStringFromQuotes (pu1SearchStr))
        {
            return (CLI_FAILURE);
        }
    }
    switch (i1OptFlag)
    {
        case CLI_GREP_INVERSE:
            if (!CLI_STRSTR (pu1LineBuf, pu1SearchStr))
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        default:
            if (CLI_STRSTR (pu1LineBuf, pu1SearchStr))
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;
    }
    return (i4RetVal);
}

INT4
CliDeletePipe (tCliContext * pCliContext)
{
    INT4                i4Index = 0;

    if (!(pCliContext->i1PipeFlag))
        return CLI_SUCCESS;

    /* Updating the Cli Pipe index */
    pCliContext->fpCliOutput = pCliContext->fpCliTempOutput;
    pCliContext->i1MoreQuitFlag = FALSE;
    pCliContext->i1MoreFlag = FALSE;
    pCliContext->u4LineCount = 0;
    pCliContext->u4LineWriteCount = 0;
    pCliContext->i1PipeFlag = FALSE;
    if ((pCliContext->mmi_exit_flag) ||
        (pCliContext->i1PipeDisable == OSIX_TRUE) ||
        (pCliContext->i1WinResizeFlag == TRUE))
    {
        pCliContext->i1WinResizeFlag = FALSE;
        return CLI_SUCCESS;
    }

    for (i4Index = 0; (i4Index <
                       (pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount);
         i4Index++)
    {
        /* memset for MaxCol + 3, as allocation is done for MaxCol + 3
         * so as to store \r\n at end of each line */

        MEMSET (pCliContext->ppu1OutBuf[i4Index], '\0',
                (pCliContext->i2MaxCol + 3));
    }

    for (i4Index = 0; pCliContext->aCliGrep[i4Index].pi1GrepStr; i4Index++)
    {
        pCliContext->aCliGrep[i4Index].i1OptFlag = 0;
        CLI_BUDDY_FREE (pCliContext->aCliGrep[i4Index].pi1GrepStr);
        pCliContext->aCliGrep[i4Index].pi1GrepStr = NULL;
    }

    return CLI_SUCCESS;
}

INT4
CliClosePipe (tCliContext * pCliContext)
{
    INT4                i4LineIndex = 0;
    INT4                i4TempIndex = 0;

    pCliContext->fpCliOutput = pCliContext->fpCliTempOutput;

    if ((pCliContext->mmi_exit_flag) ||
        (pCliContext->i1PipeDisable == OSIX_TRUE) ||
        (pCliContext->i1WinResizeFlag == TRUE))
    {
        pCliContext->i1MoreQuitFlag = FALSE;
        pCliContext->i1MoreFlag = FALSE;
        pCliContext->u4LineCount = 0;
        pCliContext->u4LineWriteCount = 0;
        pCliContext->i1PipeFlag = FALSE;
        return CLI_SUCCESS;
    }

    if (!pCliContext->u4LineCount)
    {
        while (*pCliContext->ppu1OutBuf[i4TempIndex])
            i4TempIndex++;
    }
    else
        i4TempIndex = pCliContext->u4LineCount % ((pCliContext->i2MaxRow - 1)
                                                  * pCliContext->i1PageCount);

    i4LineIndex = (i4TempIndex < (INT4) pCliContext->u4LineCount) ?
        i4TempIndex : (INT4) pCliContext->u4LineCount;

    /* Clear the Out buffer after the last line of data */
    pCliContext->ppu1OutBuf[i4TempIndex][0] = '\0';
    while (1)
    {
        if (CliMorePrintf (pCliContext, i4LineIndex) == CLI_FAILURE)
        {
            break;
        }
        i4LineIndex++;
    }
    CliDeletePipe (pCliContext);

    return CLI_SUCCESS;
}

INT2
CliGetPrevLineOffSet (tCliContext * pCliContext, INT2 i2Index)
{
    INT2                i2TopIndex = 0;

    i2TopIndex = (INT2) (i2Index - (pCliContext->i2MaxRow - 1));
    if (i2TopIndex < 0)
    {
        i2TopIndex =
            (INT2) (((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount) +
                    i2TopIndex);
    }
    else
    {
        i2TopIndex %=
            (INT2) (((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount));
    }

    return (i2TopIndex);
}

INT2
CliGetNextLineOffSet (tCliContext * pCliContext, INT2 i2TopIndex)
{
    i2TopIndex %=
        (INT2) ((pCliContext->i2MaxRow - 1) * pCliContext->i1PageCount);
    return (i2TopIndex);
}

/***************************************************************************  
 * FUNCTION NAME : CliContextCleanup
 * DESCRIPTION   : This function is used to cleanup the Context structure
 *                 upon exit.
 * INPUT         : NONE 
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliContextCleanup (tCliContext * pCliContext)
{
    if (pCliContext->gu4CliMode != CLI_MODE_CONSOLE)
    {

        if (pCliContext->pu1PendBuff != NULL)
        {
            MemReleaseMemBlock (gCliCtxPendBuff,
                                (UINT1 *) pCliContext->pu1PendBuff);
            pCliContext->pu1PendBuff = NULL;
        }

        if (pCliContext->ppu1OutBuf != NULL)
        {
            if (pCliContext->ppu1OutBuf[0] != NULL)
            {

                MemReleaseMemBlock (gCliCtxOutputBuff,
                                    (UINT1 *) pCliContext->ppu1OutBuf[0]);
                pCliContext->ppu1OutBuf[0] = NULL;
            }
        }

        if (pCliContext->ppu1OutBuf != NULL)
        {
            MemReleaseMemBlock (gCliCtxOutputBuffArr,
                                (UINT1 *) pCliContext->ppu1OutBuf);
            pCliContext->ppu1OutBuf = NULL;
        }

        pCliContext->pu1PendBuff = NULL;
        pCliContext->u4PendBuffLen = 0;

        if (pCliContext->pu1OutputMessage != NULL)
        {
            CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->pu1OutputMessage);
            pCliContext->pu1OutputMessage = NULL;
        }
        if (pCliContext->pu1UserCommand)
        {
            CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->pu1UserCommand);
            pCliContext->pu1UserCommand = NULL;
        }

    }
    else
    {
        CliDeletePipe (pCliContext);
    }

    pCliContext->mmi_exit_flag = TRUE;
    pCliContext->SessionActive = FALSE;

    if ((pCliContext->gu4CliMode == CLI_MODE_SSH)
        || (pCliContext->gu4CliMode == CLI_MODE_TELNET))
    {
        CliMemReleaseMemBlock (gCliMaxHelpStrMemPoolId,
                               &(pCliContext->pu1HelpStr));
        CliMemReleaseMemBlock (gCliCtxHelpMemPoolId,
                               &(pCliContext->pu1TknList));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1TempHelpStr1));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1TempHelpStr2));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1OptionalTokens));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1OutputBuf));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1ResizeCommand));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId,
                               &(pCliContext->pu1TempString));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &(pCliContext->pu1Temp));
        CliMemReleaseMemBlock (gCliMaxLineMemPoolId, &(pCliContext->pu1Cmd));

        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        pCliContext->i1Status = CLI_INACTIVE;
        CLI_MEMSET (pCliContext->ai1UserName, 0, MAX_USER_NAME_LEN);
    }

    return;
}

/***************************************************************************
 *  * FUNCTION NAME : CliSshLock
 *   * DESCRIPTION   : This function takes lock and increments debugcount
 *    *
 *     * INPUT         : NONE
 *      *
 *       * OUTPUT        : SUCCESS?FAILURE
 * ***************************************************************************/
INT4
CliSshLock (VOID)
{
    if (OsixSemTake (gCliSessions.CliSshSemId) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 *   FUNCTION NAME : CliSshUnLock
 *   DESCRIPTION   : This function unlocks and decrements debug count
 *   
 *   INPUT         : NONE
 *  
 *   OUTPUT        : SUCCESS/FAILURE
 * ***************************************************************************/
INT4
CliSshUnLock (VOID)
{
    OsixSemGive (gCliSessions.CliSshSemId);
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliDestroySession
 * DESCRIPTION   : This function destroys the buddy used by the CLI session
 *                 and kills the CLI task. 
 *                 This is used in case of memory failure.
 * INPUT         : pCliContext - Current CLI Context structure.
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliDestroySession (tCliContext * pCliContext)
{
    tOsixTaskId         TaskId;
    CliSshLock ();

    CliContextLock ();
    if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
    {
        close (pCliContext->i4ClientSockfd);
    }
    else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
    {
#ifdef SSH_WANTED
        CliContextUnlock ();
        SshArClose (pCliContext->i4ClientSockfd);
        CliContextLock ();
#endif
    }
    pCliContext->i4ClientSockfd = 0;
    TaskId = pCliContext->TaskId;
    CliContextCleanup (pCliContext);
    if (pCliContext->i4BuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
        pCliContext->i4BuddyId = -1;
    }
    if (CliGetCurrentContextIndex () != 0)
    {
        pCliContext->i1Status = CLI_INACTIVE;
    }
    pCliContext->TaskId = 0;
    CliContextUnlock ();
    CliSshUnLock ();

    OsixTskDel (TaskId);

}

VOID
CliMemTrcInit (tCliContext * pCliContext)
{
#ifdef CLI_MEM_TRC
    INT2                i2Index;

    pCliContext->MemPoolTrc.u4BufSize = 0;
    for (i2Index = 0; i2Index < CLI_MAX_NO_OF_ALLOC; i2Index++)
    {
        pCliContext->MemPoolTrc.u4MemSize[i2Index] = 0;
        pCliContext->MemPoolTrc.pu1MemPtr[i2Index] = NULL;
    }
#endif
    UNUSED_PARAM (pCliContext);
    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliInitialisePipeBuf
 * DESCRIPTION   : This function re-initializes to NULL the memory of the 
 *                 datastructures bounded to window size.
 * INPUT         : pCliContext - Current Cli Context structure
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliInitialisePipeBuf (tCliContext * pCliContext)
{
    INT2                i2Index;
    for (i2Index = 1; i2Index < ((pCliContext->i2PrevMaxRow - 1) *
                                 pCliContext->i1PageCount); i2Index++)
    {
        pCliContext->ppu1OutBuf[i2Index] = NULL;
    }

    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliChangePipeBuf       
 * DESCRIPTION   : This function changes the memory pointers 
 *                 for the datastructures bounded to window size 
 *                 when the window size changes.
 * INPUT         : pCliContext - Current Cli Context structure
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliChangePipeBuf (tCliContext * pCliContext)
{
    UINT1              *pu1Buf = NULL;
    INT2                i2Index;

    if (pCliContext->ppu1OutBuf == NULL)
    {
        return;
    }

    pu1Buf = pCliContext->ppu1OutBuf[0];

    for (i2Index = 0; i2Index < ((pCliContext->i2MaxRow - 1) *
                                 pCliContext->i1PageCount); i2Index++)
    {
        pCliContext->ppu1OutBuf[i2Index] = pu1Buf;
        pCliContext->ppu1OutBuf[i2Index][0] = '\0';

        pu1Buf += (pCliContext->i2MaxCol + 3);
    }

    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliHandleWinSizeChange
 * DESCRIPTION   : This function takes care of re-allocationg memory for 
 *                 datastructures bounded to window size
 * INPUT         : pCliContext - Current Cli Context structure
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliHandleWinSizeChange (tCliContext * pCliContext)
{
    UINT4               u4MemoryAllocated = 0;
    UINT4               u4LinesAllocated = 0;
    UINT4               u4MemoryRequired = 0;
    UINT4               u4LinesRequired = 0;

    u4LinesAllocated = ((CLI_DEF_MAX_ROWS - 1) *
                        (sizeof (UINT1 *)) * (CLI_MORE_OUT_NUMPAGE));
    u4MemoryAllocated = ((CLI_DEF_MAX_ROWS - 1) *
                         (CLI_DEF_MAX_COLS + 3) * (CLI_MORE_OUT_NUMPAGE));

    pCliContext->i1PageCount = CLI_MORE_OUT_NUMPAGE;
    while (1)
    {
        u4LinesRequired = ((pCliContext->i2MaxRow - 1) *
                           (sizeof (UINT1 *)) * (pCliContext->i1PageCount));
        if (u4LinesAllocated >= u4LinesRequired)
        {
            u4MemoryRequired = ((pCliContext->i2MaxRow - 1) *
                                (pCliContext->i2MaxCol + 3) *
                                (pCliContext->i1PageCount));
            if (u4MemoryRequired <= u4MemoryAllocated)
                break;
        }
        pCliContext->i1PageCount--;
    }
    if (pCliContext->i1PageCount > 1)
    {
        pCliContext->i1PipeDisable = OSIX_FALSE;
        CliChangePipeBuf (pCliContext);
    }
    else
    {
        pCliContext->i1PageCount = 0;
        pCliContext->i1PipeDisable = OSIX_TRUE;
    }
    pCliContext->i2PrevMaxCol = pCliContext->i2MaxCol;
    pCliContext->i2PrevMaxRow = pCliContext->i2MaxRow;
    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliFlushPipe
 * DESCRIPTION   : This function flushes the Pipe buffer and the More buffer
 *                 on to the display and clears the buffers.               
 *                 
 * INPUT         : pCliContext - Current Cli Context structure
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
INT2
CliFlushPipe (tCliContext * pCliContext)
{
    INT4                i4LineIndex = 0;
    INT4                (*pCliTempOutFcnPtr) (struct CliContext *,
                                              CONST CHR1 *, UINT4);

    pCliTempOutFcnPtr = pCliContext->fpCliOutput;
    pCliContext->fpCliOutput = pCliContext->fpCliTempOutput;

    if (!pCliContext->i1MoreFlag)
    {
        while (((i4LineIndex < (INT4) ((pCliContext->i2MaxRow - 1) *
                                       pCliContext->i1PageCount))) &&
               (*pCliContext->ppu1OutBuf[i4LineIndex]))
        {
            mmi_printf ("\r%s", pCliContext->ppu1OutBuf[i4LineIndex]);
            pCliContext->ppu1OutBuf[i4LineIndex][0] = '\0';
            i4LineIndex++;
        }
        pCliContext->u4LineCount = 0;
        pCliContext->u4LineWriteCount = 0;
    }
    i4LineIndex = 0;

    pCliContext->fpCliOutput = pCliTempOutFcnPtr;
    return (CLI_SUCCESS);
}

/***************************************************************************  
 * FUNCTION NAME : CliChangeCmdGrp
 * DESCRIPTION   : This function changes the group membership of the command
 *                 given as the argument.                                  
 *                 
 * INPUT         : pi1Command - Command whose membership is to modified
 *                 pi1Group   - New group name
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
INT4
CliChangeCmdGrp (INT4 *pi4ActionNum, INT1 *pi1Command, INT1 *pi1Group)
{
    t_MMI_HELP         *pCliHelpInfo = NULL;
    tCliContext        *pCliContext = NULL;
    INT2                i2CmdLen = 0;
    INT2                i2Index = 0;
    INT2                i2CmdCount = 0;
    INT1                i1MultCmdFlag = FALSE;    /* '*' at the end of command 
                                                   used to change attributes 
                                                   for multile commands */

    if (!(pCliContext = CliGetContext ()))
    {
        /* No Context found */
        return (CLI_FAILURE);
    }

    if (pi1Group)
    {
        if (STRLEN (pi1Group) >= CLI_MAX_GRPNAME_LEN)
        {
            mmi_printf
                ("\r\nCannot change attributes for %s :Group Name too long\r\n",
                 pi1Command);
            return (CLI_FAILURE);
        }
    }
    else
    {
        mmi_printf
            ("\r\nCannot change attributes for %s :Group Name cannot be\r\n"
             " NULL\r\n", pi1Command);
        return (CLI_FAILURE);
    }

    if (!pi4ActionNum)
    {
        /* Discarding the \" from the command (needed for <quote_str> ) */
        CliExtractStringFromQuotes ((UINT1 *) pi1Command);
        i2CmdLen = (INT2) STRLEN (pi1Command);
        if (!i2CmdLen)
        {
            return (CLI_FAILURE);
        }
        if (pi1Command[i2CmdLen - 1] == '*')
        {
            i1MultCmdFlag = TRUE;
            pi1Command[i2CmdLen - 1] = '\0';
        }
    }

    pCliHelpInfo = pCliContext->pMmiCur_mode->pHelp;
    while (pCliHelpInfo != NULL)
    {
        i2Index = 0;
        while (pCliHelpInfo->pSyntax[i2Index] != NULL)
        {
            if (pi4ActionNum)
            {
                if ((pCliHelpInfo->i4ActNumStart + i2Index) == *pi4ActionNum)
                {
                    STRCPY (pCliHelpInfo->pGrpId[i2Index], pi1Group);
                    i2CmdCount++;
                    break;
                }
            }
            else
            {
                if (i1MultCmdFlag)
                {
                    /* Check if part of a string matches the command */
                    if (CliCommandMatchSubset
                        ((CONST UINT1 *) pCliHelpInfo->pSyntax[i2Index],
                         (UINT1 *) pi1Command) == CLI_SUCCESS)
                    {
                        STRCPY (pCliHelpInfo->pGrpId[i2Index], pi1Group);
                        i2CmdCount++;
                    }
                }
                else
                {
                    if (CliCommandMatch
                        ((CONST UINT1 *) pCliHelpInfo->pSyntax[i2Index],
                         (UINT1 *) pi1Command) == CLI_SUCCESS)
                    {
                        STRCPY (pCliHelpInfo->pGrpId[i2Index], pi1Group);
                        i2CmdCount++;
                        break;
                    }
                }
            }
            i2Index++;
        }
        pCliHelpInfo = pCliHelpInfo->pNext;
    }

    mmi_printf ("\r\nAttributes changed for %d commands\r\n", i2CmdCount);

    return (CLI_SUCCESS);
}

/***************************************************************************  
 * FUNCTION NAME : CliChangeUser
 * DESCRIPTION   : This function substitutes the present user with the 
 *                 specified user.
 *                 
 * INPUT         : pi1User    - User name of the nwe user              
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
INT4
CliChangeUser (INT1 *pi1User)
{
    tCliContext        *pCliContext = NULL;
    INT2                i2UserIndex = 0;
    UINT4               u4Len = 0;

    if (!(pCliContext = CliGetContext ()))
    {
        return (CLI_FAILURE);
    }

    if (pCliContext->i2UserIndex >= MAX_USER_SUBSTITUTES)
    {
        mmi_printf ("\r\n%s", mmi_gen_messages[MMI_GEN_ERR_MAX_USER_SUBS]);
        return (CLI_FAILURE);
    }
    if ((CliGetUserIndex ((CHR1 *) pi1User, &i2UserIndex)) == CLI_FAILURE)
    {
        mmi_printf ("\r\n%s", mmi_gen_messages[MMI_GEN_ERR_NO_SUCH_USER]);
        return (CLI_FAILURE);
    }

    if (!STRCMP (pCliContext->ai1UserName, CLI_ROOT_USER))
    {
        u4Len = ((STRLEN (pi1User) < sizeof (pCliContext->ai1UserName)) ?
                 STRLEN (pi1User) : sizeof (pCliContext->ai1UserName) - 1);
        STRNCPY (pCliContext->ai1UserName, pi1User, u4Len);
        pCliContext->ai1UserName[u4Len] = '\0';
    }
    else
    {
        if (mmi_login (pCliContext, pi1User) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    CliInitUserMode (pCliContext);

    return (CLI_SUCCESS);
}

/***************************************************************************  
 * FUNCTION NAME : CliTimeoutExpiry
 * DESCRIPTION   : This function is called whenever the idle timer expires
 *                 
 * INPUT         : pCliContext - Cli Context of the calling function.
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
INT4
CliTimeoutExpiry (tCliContext * pCliContext)
{
    pCliContext->i4PromptInfo = -1;
    pCliContext->u4ContextId = CLI_INVALID_CONTEXT_ID;
    CliFlushOutputBuff (pCliContext);
    MGMT_LOCK ();
    CliLogout ();
    MGMT_UNLOCK ();
    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME : CliSetDefaultMoreFlag
 * DESCRIPTION   : Enables/Disables the DefaultMoreFlag option
 *
 * INPUT         : CliHandle - Current CliContextId.
 *                 i1Flag - set/get Default more flag option.
 *                          CLI_DISPLAY,      Display the current option.
 *                          CLI_MORE_ENABLE,  Enable the option
 *                          CLI_MORE_DISABLE, Disable the option.
 *
 * OUTPUT        : NONE.
 *
 * RETURN        : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/

INT4
CliSetDefaultMoreFlag (tCliHandle CliHandle, INT1 i1Flag)
{
    tCliContext        *pCliContext;

    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);

    if ((pCliContext == NULL) || (pCliContext->i1Status != CLI_ACTIVE))
    {
        /* No Task Found */
        return CLI_FAILURE;
    }

    if (i1Flag == CLI_DISPLAY)
    {
        if (pCliContext->i1DefaultMoreFlag == CLI_MORE_ENABLE)
        {
            CliPrintf (CliHandle, "\r\n Shell more Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Shell more Disabled\r\n");
        }
    }
    else
    {
        pCliContext->i1DefaultMoreFlag = i1Flag;
    }
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliEnable
 * DESCRIPTION   : This function enables the Privileged Exec mode 
 *                 
 * INPUT         : u1Flag   - to enable/disable.
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliEnable (UINT1 u1Flag, VOID *pi1Level)
{
    tCliContext        *pCliContext = NULL;
    INT1                ai1TempStr[MAX_PROMPT_LEN];
    INT1                ai1Pswd[MAX_PASSWD_LEN];
    INT1                ai1BkpPswd[MAX_PASSWD_LEN];
    INT1                i1MaxTries = 0;
    INT1                i1PrivilegeLevel = CLI_MAX_PRIVILEGES - 1;
    INT1                i1PrivilegeIndex = -1;
    INT1                i1Status = CLI_FAILURE;

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }

    if ((pi1Level))
    {
        i1PrivilegeLevel = (INT1) *((UINT4 *) pi1Level);
    }

    CliDeletePipe (pCliContext);

    if (u1Flag == CLI_ZERO)
    {
        if ((pCliContext->i1PrivilegeLevel < i1PrivilegeLevel))
        {
            if ((i1Status =
                 CliCheckPrivilegeIndex (i1PrivilegeLevel,
                                         &i1PrivilegeIndex)) != CLI_SUCCESS)
            {
                if (!(pCliContext->i1PrivilegeLevel))
                {
                    /* Assupmtion made : Passwd for MAX Privilege level cannot be disabled */
                    CliCheckPrivilegeIndex ((CLI_MAX_PRIVILEGES - 1),
                                            &i1PrivilegeIndex);
                }
                if (i1PrivilegeLevel != (CLI_MAX_PRIVILEGES - 1))
                {
                    mmi_printf ("\r%% No password set\r\n");
                    return;
                }
            }
            if ((i1Status == CLI_SUCCESS) || (i1PrivilegeIndex != -1))
            {
                while (i1MaxTries <= MAX_PASSWORD_TRIES)
                {
                    mmi_printf ("%s", mmi_gen_messages[MMI_GEN_PASSWD]);
                    MGMT_UNLOCK ();
                    if (mmi_getpasswd ((CHR1 *) ai1Pswd, TRUE) == CLI_FAILURE)
                    {
                        MGMT_LOCK ();
                        return;
                    }
                    MGMT_LOCK ();

                    memcpy (ai1BkpPswd, ai1Pswd, MAX_PASSWD_LEN);
                    mmi_encript_passwd ((CHR1 *) ai1Pswd);
                    if (CliCheckPrivilegePswd (i1PrivilegeIndex, ai1Pswd) !=
                        CLI_FAILURE)
                    {
                        break;
                    }
                    /* In case of privilege 15,
                       To validate if the user has entered the root user's password
                       * to change their privilege.
                       * In case of FIPS mode, it will validate for admin user's password */
                    else if ((i1PrivilegeLevel == CLI_MAX_PRIVILEGES - 1) &&
                             (FpamCheckPassword
                              ((CHR1 *) CLI_ROOT_USER,
                               ai1BkpPswd) == FPAM_SUCCESS))
                    {
                        break;
                    }

                    i1MaxTries++;
                }
            }
        }
        if (i1MaxTries <= MAX_PASSWORD_TRIES)
        {
            CLI_MEMSET (ai1TempStr, 0, MAX_PROMPT_LEN);

            CliChangePath ("/");
            CliGetPromptFromMode ("USEREXEC", ai1TempStr);

            if (i1PrivilegeLevel > CLI_DEFAULT_PRIVILEGE)
            {
                CliChangePath ((CONST CHR1 *) ai1TempStr);
                CliGetPromptFromMode ("EXEC", ai1TempStr);
            }
            pCliContext->i1PrivilegeLevel = i1PrivilegeLevel;
            if (CliChangePath ((CONST CHR1 *) ai1TempStr) == CLI_SUCCESS)
            {
            }
        }
        else
        {
            mmi_printf ("\r\n%% Access denied\r\n");
        }
    }
    else
    {
        if (!(pi1Level))
        {
            pCliContext->i1PrivilegeLevel = CLI_DEFAULT_PRIVILEGE;
        }
        else
        {
            if ((pCliContext->i1PrivilegeLevel < i1PrivilegeLevel))
            {
                mmi_printf
                    ("\r\nNew privilege level must be less than current privilege level\r\n");
                return;
            }
            pCliContext->i1PrivilegeLevel = i1PrivilegeLevel;
        }
        if (pCliContext->i1PrivilegeLevel <= CLI_DEFAULT_PRIVILEGE)
            CliChangePath ("..");
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliLogOutMessage
 * DESCRIPTION   : This function displays the logout message for SSH and 
 *                 telnet sessions during session logout.
 * INPUT         : none 
 * OUTPUT        : returns the default message to be set in SSH or TELNET
 *                 during user forceful logout or idle timer expiry.
 ***************************************************************************/
CHR1               *
CliLogOutMessage ()
{
    tCliContext        *pCliContext = NULL;

    pCliContext = CliGetContext ();
    if (pCliContext == NULL)
    {
        return ((CHR1 *) "\r\nIdle Timer expired, Timing Out !!!\n");
    }

    if (pCliContext->u1CliLogOutMessage == CLI_FORCED_LOGOUT_MSG)
    {
        /* Incase of SSH connections, the global variable has to be,
         * resetted to Idle time out expiry. */
        if (pCliContext->gu4CliMode == CLI_MODE_SSH)
        {
            pCliContext->u1CliLogOutMessage = CLI_IDLE_TIME_EXPIRY_MSG;
        }

        return ((CHR1 *)
                "\r\nUser login credentials updated. Please relogin!!!\n");
    }

    if (pCliContext->i1ClearFlagStatus == OSIX_TRUE)
    {
        return ((CHR1 *) "\r\nSession has been cleared !!!\n");
    }
    return ((CHR1 *) "\r\nIdle Timer expired, Timing Out !!!\n");
}

/***************************************************************************  
 * FUNCTION NAME : CliLogOutUser
 * DESCRIPTION   : This function is used to logout any user from CLI, SSH or
 *                 telnet.
 * INPUT         : pi1UsrName - name of the user to be logged out 
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliLogOutUser (INT1 *pi1UsrName)
{
    INT2                i2Index = 0;

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        /* scans for the cli sessions by the username to be logged out */
        if ((STRNCMP (gCliSessions.gaCliContext[i2Index].ai1UserName,
                      pi1UsrName, MAX_USER_NAME_LEN) == 0)
            && (gCliSessions.gaCliContext[i2Index].i1Status == CLI_ACTIVE))
        {
            if (gCliSessions.gaCliContext[i2Index].gu4CliMode == CLI_MODE_SSH ||
                gCliSessions.gaCliContext[i2Index].gu4CliMode ==
                CLI_MODE_TELNET)
            {
                /* sets the logout message as FORCED LOGOUT message 
                 * this message will be resetted after user successful
                 * forced logout*/
                gCliSessions.gaCliContext[i2Index].u1CliLogOutMessage =
                    CLI_FORCED_LOGOUT_MSG;

                /* Destroys the SSH or TELNET session */
                CliContextLock ();
                CliDestroyLine (&gCliSessions.gaCliContext[i2Index]);
                CliContextUnlock ();

                if (gCliSessions.gaCliContext[i2Index].gu4CliMode ==
                    CLI_MODE_SSH)
                {
                    /* performs auditLogging and syslog auditing for SSH context 
                     * For telnet context it is handled automatically */
                    CliSendAuditLoginInfo (&gCliSessions.gaCliContext[i2Index],
                                           FORCED_LOG_OUT);
                    mmicmdlog_user_logout (&gCliSessions.gaCliContext[i2Index],
                                           FORCED_LOG_OUT);
                    CLI_MEMSET (gCliSessions.gaCliContext[i2Index].ai1UserName,
                                0, MAX_USER_NAME_LEN);
                }
            }
            else if (gCliSessions.gaCliContext[i2Index].gu4CliMode ==
                     CLI_MODE_CONSOLE)
            {
                CliDeletePipe (&gCliSessions.gaCliContext[i2Index]);
                gCliSessions.gaCliContext[i2Index].i1UserAuth = FALSE;
                gCliSessions.gaCliContext[i2Index].i1passwdMode = LOCAL_LOGIN;
                CliContextLock ();
                CliContextCleanup (&gCliSessions.gaCliContext[i2Index]);
                CliContextUnlock ();
#ifdef    RM_WANTED
                if (gi4CliPrevRmRole == RM_INIT)
#endif
                {
                    OsixEvtSend (gCliSessions.gaCliContext[i2Index].TaskId,
                                 CLI_RESUME_EVENT);
                }

                CliSendAuditLoginInfo (&gCliSessions.gaCliContext[i2Index],
                                       FORCED_LOG_OUT);
                mmicmdlog_user_logout (&gCliSessions.gaCliContext[i2Index],
                                       FORCED_LOG_OUT);

                /* Updates the active user logout count. In case of SSH and TELNET,
                 * CliDestroyLine takes care of it */
                FpamUtlUpdateActiveUsers ((CHR1 *) pi1UsrName,
                                          FPAM_USER_LOGGED_OUT);

                CLI_MEMSET (gCliSessions.gaCliContext[i2Index].ai1UserName, 0,
                            MAX_USER_NAME_LEN);
                /* resets the CLI timeout message */
                gCliSessions.gaCliContext[i2Index].u1CliLogOutMessage =
                    CLI_FORCED_LOGOUT_MSG;
            }
        }
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliLogout
 * DESCRIPTION   : This function exits from the EXEC mode.           
 *                 
 * INPUT         : VOID
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliLogout (VOID)
{
    tCliContext        *pCliContext;
    CHR1                ai1username[MAX_USER_NAME_LEN];

    MEMSET (ai1username, '\0', MAX_USER_NAME_LEN);

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return;
    };

    STRCPY (ai1username, pCliContext->ai1UserName);
    pCliContext->bIsUserPrivileged = OSIX_FALSE;
    MGMT_UNLOCK ();
    if (pCliContext->gu4CliMode != CLI_MODE_APP)
    {
        mmi_exit ();
    }
    CliClear ();
    MGMT_LOCK ();
    CliReSetIfRangeMode ();
    FpamUtlUpdateActiveUsers ((CHR1 *) ai1username, FPAM_USER_LOGGED_OUT);
    /* CLI will be waiting for this event before every login. So post this
     * event when logging out 
     */
    if (pCliContext->gu4CliMode == CLI_MODE_CONSOLE)
    {
        CliInformRestorationComplete ();
    }
    else if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
    {
        CliContextLock ();
        close (pCliContext->i4ClientSockfd);
        pCliContext->i4ClientSockfd = -1;
        CliContextUnlock ();

    }
}

/***************************************************************************  
 * FUNCTION NAME : CliConfigure
 * DESCRIPTION   : This function enters in to Configuration mode.    
 *                 
 * INPUT         : VOID
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliConfigure (VOID)
{
    INT1                ai1PromptStr[MAX_PROMPT_LEN];

    CliGetPromptFromMode ("CONFIGURE", ai1PromptStr);
    CliChangePath ((CONST CHR1 *) ai1PromptStr);
}

/***************************************************************************  
 * FUNCTION NAME : CliShowPrivilege
 * DESCRIPTION   : This function displays the current privilege level of 
 *                 the user.    
 *                 
 * INPUT         : VOID
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliShowPrivilege (VOID)
{
    tCliContext        *pCliContext = NULL;

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }

    mmi_printf
        ("\r\nCurrent privilege level is %d\r\n",
         pCliContext->i1PrivilegeLevel);
}

/***************************************************************************  
 * FUNCTION NAME : CliLineConfigure
 * DESCRIPTION   : This function enters in to LINE Config mode. 
 *                 
 * INPUT         : u1ConsoleFlag - To configure cosole/Vty lines.
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliLineConfigure (UINT1 u1ConsoleFlag)
{
    tCliContext        *pCliContext = NULL;
    INT1                ai1PromptStr[MAX_PROMPT_LEN];

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }
    if (((u1ConsoleFlag) && (pCliContext->gu4CliMode != CLI_CONSOLE)) ||
        (!(u1ConsoleFlag) && (pCliContext->gu4CliMode == CLI_CONSOLE)))
    {
        mmi_printf ("\r\n%% Cannot configure for other terminals\r\n");
    }
    else
    {
        CliGetPromptFromMode ("LINE", ai1PromptStr);
        CliChangePath ((CONST CHR1 *) ai1PromptStr);
    }
}

 /****************************************************************************
 * FUNCTION NAME : CliClearLine
 * DESCRIPTION   : This function clears telnet/ssh session
 *                 
 * INPUT         : CliHandle - Handle to identify the current session
 *                 u1SessionId - Session Id to be deleted.
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliClearLine (tCliHandle CliHandle, UINT1 u1SessionId)
{

    tCliContext        *pCliContext;
    tOsixTaskId         CurrTskId;

    if (!(CliGetContext ()))
    {
        return;
    }
    if (CliCheckSpecialPriv () != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Permission denied !! Only %s or %s privileged user can clear session(s)\r\n",
                   CLI_ROOT_USER, CLI_ROOT_USER);
        return;
    }

    if (u1SessionId >= CLI_MAX_SESSIONS)
    {
        CliPrintf (CliHandle, "\r\n%%Session out of range \r\n");
        return;
    }

    pCliContext = &(gCliSessions.gaCliContext[u1SessionId]);
    if ((pCliContext->gu4CliMode != CLI_MODE_TELNET) &&
        (pCliContext->gu4CliMode != CLI_MODE_SSH))
    {
        CliPrintf (CliHandle,
                   "\r\n %%  Only telnet/ssh sessions can be cleared \r\n");
        return;
    }
    CurrTskId = OsixGetCurTaskId ();
    if (pCliContext->TaskId == CurrTskId)
    {

        MGMT_LOCK ();
        CliLogout ();
        MGMT_UNLOCK ();
        return;
    }

    CliContextLock ();

    if (pCliContext->i1Status == CLI_INACTIVE)
    {
        CliContextUnlock ();
        CliPrintf (CliHandle, "\r\n %% Session Inactive !\r\n");
        return;
    }
    /* To update the i4ClearFlagStatus to OSIX_TRUE to indicate SSH/Telnet session
     * is cleared forcefully */
    pCliContext->i1ClearFlagStatus = OSIX_TRUE;
    CliDestroyLine (pCliContext);
    CliContextUnlock ();
}

/***************************************************************************  
 * FUNCTION NAME : CliDestroyLine
 * DESCRIPTION   : This function destroys telnet/ssh session
 *                 
 * INPUT         : pCliContext - Context information
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliDestroyLine (tCliContext * pCliContext)
{

#ifdef SLI_WANTED
    OsixEvtSend (pCliContext->TaskId, CLI_SELECT_TIMER_EVENT);
#else
    UINT4               u4TaskId = 0;
#ifdef SSH_WANTED
    INT4                i4Sockfd = 0;
#endif

    pCliContext->i1UserAuth = FALSE;

    if (pCliContext->i4BuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
        pCliContext->i4BuddyId = -1;
    }
    if (CliGetCurrentContextIndex () != 0)
    {
        pCliContext->i1Status = CLI_INACTIVE;
    }
    pCliContext->mmi_exit_flag = TRUE;
    pCliContext->SessionActive = FALSE;

    u4TaskId = pCliContext->TaskId;

    if (pCliContext->gu4CliMode == CLI_MODE_TELNET)
    {
        close (pCliContext->i4ClientSockfd);
        pCliContext->i4ClientSockfd = -1;
        pCliContext->TaskId = 0;
        CliContextCleanup (pCliContext);
        /* CliDestroyLine should be called after
         ** taking lock **/
        CliContextUnlock ();
        if (u4TaskId != 0)
        {
            OsixTskDel (u4TaskId);
        }
        CliContextLock ();

    }
    else if (pCliContext->gu4CliMode == CLI_MODE_SSH)
    {
#ifdef SSH_WANTED
        /* CliDestroyLine should be called after
         ** taking lock **/

        CliContextUnlock ();
        i4Sockfd = pCliContext->i4ClientSockfd;
        pCliContext->i4ClientSockfd = -1;
        CliContextCleanup (pCliContext);
        SshArClose (i4Sockfd);
        pCliContext->TaskId = 0;
        CliContextLock ();

#endif
    }
#endif
    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliClearAllLines
 * DESCRIPTION   : This function clears all the telnet/ssh sessions 
 *                 
 * INPUT         : NONE
 * 
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliClearAllLines (tCliHandle CliHandle)
{

    if (!(CliGetContext ()))
    {
        return;
    }
    if (CliCheckSpecialPriv () != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Permission denied !! Only %s or %s privileged user can clear session(s)\r\n",
                   CLI_ROOT_USER, CLI_ROOT_USER);
        return;
    }

    CliDestroyConnections (CLI_TELNET_SSH_CLEAR, OSIX_TRUE);
}

/***************************************************************************
 * FUNCTION NAME : CliExecTimeoutShowRunningConfig
 * DESCRIPTION   : This function displays the Cli Session timeout value.
 * INPUT         : NONE
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliExecTimeoutShowRunningConfig (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;

    if (!(pCliContext = CliGetContext ()))
    {
        return;
    }
    if (pCliContext->i4SessionTimer != CLI_SESSION_TIMEOUT)
    {
        CliPrintf (CliHandle, "\r\nline console");
        CliPrintf (CliHandle, "\r\nexec-timeout %d",
                   pCliContext->i4SessionTimer);
        CliPrintf (CliHandle, "\r\n!\r\n");
    }

}

/***************************************************************************  
 * FUNCTION NAME : CliShowLineInfo
 * DESCRIPTION   : This function displays the LINE info of the given LINE.
 *                 
 * INPUT         : pi4TtyLine   - Indicates the LINE number/
 *                                Console if it is NULL.
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliShowLineInfo (INT4 *pi4TtyLine)
{
    tCliContext        *pCliContext = NULL;
    INT4                i4Index = 0;
    INT4                i4DefExecTimeOut = 0;

    if (pi4TtyLine)
        i4Index = *pi4TtyLine;

    if (i4Index >= CLI_MAX_SESSIONS)
    {
        mmi_printf ("\r%% Line %d out of range\r\n", i4Index);
        return;
    }
    pCliContext = &(gCliSessions.gaCliContext[i4Index]);

    if ((pCliContext->i1Status != CLI_ACTIVE))
    {
        mmi_printf ("\r%% Line %d not active\r\n", i4Index);
    }
    else
    {
        CliIssGetDefaultExecTimeOut (&i4DefExecTimeOut);
        if (pCliContext->i4SessionTimer)
        {
            mmi_printf ("\rCurrent Session Timeout (in secs) = %d\n",
                        pCliContext->i4SessionTimer);
            mmi_printf ("\rDefault Telnet Session Timeout (in secs) = %d\n",
                        i4DefExecTimeOut);
        }
        else
        {
            mmi_printf ("\rNo idle time\n");
        }
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliWaitForConfRestoration
 * DESCRIPTION   : This function waits for event from mib save restoration 
 *                 function .
 *                 
 * INPUT         : NONE.
 * 
 * OUTPUT        : NONE
****************************************************************************/
VOID
CliWaitForConfRestoration ()
{
    UINT4               u4Event;
#ifdef    RM_WANTED
    if (gi4CliPrevRmRole == RM_INIT)
#endif
    {
        OsixEvtRecv (gCliTaskId, CLI_RESUME_EVENT, CLI_EVENT_WAIT_FLAGS,
                     &u4Event);
    }
    return;
}

/***************************************************************************  
 * FUNCTION NAME :  CliInformRestorationComplete
 * DESCRIPTION   : This function sends an event to CLI main task on completion
                   of mib save restoration and this can also be sent from 
                   RM task upon shut and start is completed to resume the 
                   CLI prompt.
 *                 
 * INPUT         : NONE.
 * 
 * OUTPUT        : NONE
****************************************************************************/
VOID
CliInformRestorationComplete ()
{
    OsixEvtSend (gCliTaskId, CLI_RESUME_EVENT);
    return;
}

/***************************************************************************  
 * FUNCTION NAME : CliGetApplicationContext 
 * DESCRIPTION   : This function returns a pointer to the CLI appl. context
 * INPUT         : NONE
 * OUTPUT        : NONE
 * RETURNS       : pCliContext
 
 ***************************************************************************/
tCliContext        *
CliGetApplicationContext (VOID)
{
    INT2                i2Index;
    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].gu4CliMode == CLI_MODE_APP)
            return &(gCliSessions.gaCliContext[i2Index]);
    }
    return NULL;
}

/***************************************************************************  
 * FUNCTION NAME : CliApplicationContextInit 
 * DESCRIPTION   : This function is used to initialize the application context
           for CLI.
 * INPUT         : NONE 
 * OUTPUT        : NONE
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT2
CliApplicationContextInit (VOID)
{
    tCliContext        *pCliContext = NULL;
    INT1                i1Log[256];
    if ((pCliContext = CliGetFreeContext ()) == NULL)
    {
        /* No Free Context Found */
        return (CLI_FAILURE);
    }
    OsixSemTake (gCliSessions.CliAmbVarSemId);
    CLI_ALLOC_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
    if (pCliContext->pCliAmbVar == NULL)
    {
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        OsixSemGive (gCliSessions.CliAmbVarSemId);
        return CLI_FAILURE;
    }

    if (CliContextMemInit (pCliContext) == CLI_FAILURE)
    {
        OsixSemGive (gCliSessions.CliAmbVarSemId);
        return CLI_FAILURE;
    }
    CliContextInit (pCliContext);
    pCliContext->pMmiCur_mode = gCliSessions.p_mmi_root_mode->pChild;
    pCliContext->i1PrivilegeLevel = (INT1) CLI_ATOI (CLI_ROOT_PRIVILEGE_ID);
    pCliContext->fpCliIoctl = CliSerialIoctl;
    pCliContext->fpCliFileWrite = CliFileWrite;
    pCliContext->gu4CliMode = CLI_MODE_APP;
    pCliContext->u4CmdStatus = CLI_FAILURE;

    /*Create semaphore for appln. context. 
       This semaphore is to be taken/released by protocols whenever they want to 
       execute CLI commands in application context.
     */
    STRCPY (gu1AppContextSemName, "APP");
    if (OsixCreateSem (gu1AppContextSemName, 1, 0,
                       &gAppContextSemId) == OSIX_FAILURE)
    {
        CLI_RELEASE_AMB_MEM_BLOCK (pCliContext->pCliAmbVar);
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        SNPRINTF ((CHR1 *) i1Log, sizeof (i1Log),
                  "\r\nUnable to create Semaphore for App. Context\n");
        UtlTrcPrint ((CHR1 *) i1Log);
        OsixSemGive (gCliSessions.CliAmbVarSemId);
        return (CLI_FAILURE);
    }
    OsixSemGive (gCliSessions.CliAmbVarSemId);
    return (CLI_SUCCESS);
}

/***************************************************************************  
 * FUNCTION NAME : CliTakeAppContext 
 * DESCRIPTION   : This function returns a pointer to the application context
           for CLI, to the calling task.
 * INPUT         : NONE 
 * OUTPUT        : NONE
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT2
CliTakeAppContext (VOID)
{
    tCliContext        *pCliContext = NULL;
    OsixSemTake (gAppContextSemId);
    pCliContext = CliGetApplicationContext ();
    if (pCliContext != NULL)
    {
        pCliContext->TaskId = OsixGetCurTaskId ();
    }
    else
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/***************************************************************************  
 * FUNCTION NAME : CliGiveAppContext 
 * DESCRIPTION   : This function is used to release the application context
           for CLI.
 * INPUT         : NONE 
 * OUTPUT        : NONE
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/

INT2
CliGiveAppContext (VOID)
{
    tCliContext        *pCliContext = NULL;
    INT2                i2Index;
    tOsixTaskId         CurrTaskId;

    CurrTaskId = OsixGetCurTaskId ();

    CliContextLock ();

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if ((gCliSessions.gaCliContext[i2Index].gu4CliMode == CLI_MODE_APP) &&
            (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId) &&
            (gCliSessions.gaCliContext[i2Index].i1Status == CLI_ACTIVE))
        {
            pCliContext = &gCliSessions.gaCliContext[i2Index];
            break;
        }
    }
    if (pCliContext != NULL)
    {
        pCliContext->TaskId = 0;
    }
    else
    {
        CliContextUnlock ();
        OsixSemGive (gAppContextSemId);
        return (CLI_FAILURE);
    }

    CliContextUnlock ();
    OsixSemGive (gAppContextSemId);
    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME : CliResetConsoleContextTaskId
 * DESCRIPTION   : This function is to reset the task ID in console context
 *                 to execute the commands in App context. 
 * INPUT         : NONE
 * OUTPUT        : NONE
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/

INT2
CliResetConsoleContextTaskId (VOID)
{
    tOsixTaskId         CurrTaskId = OsixGetCurTaskId ();

    /* To reset the task ID in CLI Console context, 
     * Console context index value is 0 */
    if ((gCliSessions.gaCliContext[0].TaskId == CurrTaskId) &&
        (gCliSessions.gaCliContext[0].i1Status == CLI_ACTIVE))
    {
        gCliSessions.gaCliContext[0].TaskId = 0;
        return (CLI_SUCCESS);
    }
    return (CLI_FAILURE);
}

/***************************************************************************
 * FUNCTION NAME : CliSetConsoleContextTaskId
 * DESCRIPTION   : This function is to set back task ID in console context
 *                 after executing CLI commands in App context.
 * INPUT         : NONE
 * OUTPUT        : NONE
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT2
CliSetConsoleContextTaskId (VOID)
{
    tOsixTaskId         CurrTaskId = OsixGetCurTaskId ();
    /* To set the task ID in CLI Console context, 
     * Console context index value is 0 */
    if ((gCliSessions.gaCliContext[0].TaskId == 0) &&
        (gCliSessions.gaCliContext[0].i1Status == CLI_ACTIVE))
    {
        gCliSessions.gaCliContext[0].TaskId = CurrTaskId;
        return (CLI_SUCCESS);
    }
    return (CLI_FAILURE);
}

/***************************************************************************** 
 * Function Name : CliCloseAllConnections 
 * Description   : This function is used to close all TELNET/SSH connections 
 *                 that are active. This will be called before rebooting the 
 *                 system to avoid a TELNET/SSH session hang 
 * Input(s)      : None 
 * Output(s)     : None. 
 * Return Values : CLI_SUCCESS 
 *****************************************************************************/
INT4
CliCloseAllConnections ()
{
    tCliContext        *pCliContext;
    INT1                i1Index;
    INT4                i4SockDesc;    /*Socket descripter for telnet session or 
                                     *connection ID for SSH session*/
#ifdef SSH_WANTED
    INT4                i4SockFd;    /* Socket descriptor for SSH session */
#endif

    OsixTakeSem (0, gCliSessions.au1CliContextSemName, 0, 0);

    for (i1Index = 0; i1Index < CLI_MAX_SESSIONS; i1Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i1Index]);
        if (pCliContext->i1Status == CLI_ACTIVE)
        {
            i4SockDesc = pCliContext->i4ClientSockfd;
            if ((pCliContext->gu4CliMode == CLI_MODE_TELNET) &&
                (i4SockDesc > 0))
            {
                /* i4SockDesc is the socket descriptor for this session. 
                 * Close it*/
                close (i4SockDesc);
            }
#ifdef SSH_WANTED
            if ((pCliContext->gu4CliMode == CLI_MODE_SSH) && (i4SockDesc >= 0))
            {
                /* i4SockDesc is the connection ID for this session. Get the 
                 * socket descriptor and close it*/
                if ((i4SockFd = SshArGetSockfd (i4SockDesc)) > 0)
                {
                    close (i4SockFd);
                }
            }
#endif
            pCliContext->i4ClientSockfd = -1;
        }
    }
    OsixGiveSem (0, gCliSessions.au1CliContextSemName);
    return CLI_SUCCESS;
}

#ifdef ECFM_WANTED
/* CLI API to disable the __more__ option*/
INT4
CliDisableMore (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;
    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);
    if (pCliContext->i1PipeFlag)
    {
        CliClosePipe (pCliContext);
    }
    return CLI_SUCCESS;
}

/* CLI API to enable the __more__ option*/
INT4
CliEnableMore (tCliHandle CliHandle)
{
    tCliContext        *pCliContext;
    pCliContext = &(gCliSessions.gaCliContext[(INT4) CliHandle]);
    UNUSED_PARAM (pCliContext);
    return CLI_SUCCESS;
}
#endif

/***************************************************************************  
 * FUNCTION NAME : CliDestroyConnections
 * DESCRIPTION   : This function clears all the telnet or ssh or both telnet
 *                 and ssh sessions based on the option given
 *                 
 * INPUT         : pCliContext - Current Cli Context
 *                 u1DestroyOption - Option to destroy telnet/ssh/both 
 *                 connections that are active
 *                 i1ClearFlagStatus - Flag to identify whether ssh/telnet            
 *                 connections have been cleared by clear line command
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliDestroyConnections (UINT4 u4DestroyOption, INT1 i1ClearFlagStatus)
{
    INT4                i4Index = 0;
    tCliContext        *pCliContext = NULL;
    tOsixTaskId         CurrTskId;

    CurrTskId = OsixGetCurTaskId ();

    CliContextLock ();
    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i4Index]);
        if (CLI_ACTIVE == pCliContext->i1Status)
        {
            if (pCliContext->TaskId != CurrTskId)
            {

                /* Clear all active telnet sessions */
                if ((pCliContext->gu4CliMode == CLI_MODE_TELNET)
                    && (pCliContext->SessionActive == TRUE)
                    && ((CLI_TELNET_SSH_CLEAR == u4DestroyOption)
                        || (CLI_TELNET_CLEAR == u4DestroyOption)))
                {
                    pCliContext->i1ClearFlagStatus = i1ClearFlagStatus;
                    CliDestroyLine (pCliContext);
                }
                /* Clear all active ssh sessions */
                if ((pCliContext->gu4CliMode == CLI_MODE_SSH)
                    && (pCliContext->SessionActive == TRUE)
                    && ((CLI_TELNET_SSH_CLEAR == u4DestroyOption)
                        || (CLI_SSH_CLEAR == u4DestroyOption)))
                {
                    pCliContext->i1ClearFlagStatus = i1ClearFlagStatus;
                    CliDestroyLine (pCliContext);
                }
            }
        }
    }
    /* Now, need to delete the current session */
    pCliContext = CliGetContext ();
    if (((pCliContext != NULL) && (pCliContext->SessionActive == TRUE)) &&
        ((pCliContext->gu4CliMode == CLI_MODE_TELNET)
         || (pCliContext->gu4CliMode == CLI_MODE_SSH)))
    {
        if (CLI_TELNET_SSH_CLEAR == u4DestroyOption)
        {
            pCliContext->i1ClearFlagStatus = i1ClearFlagStatus;
            CliDestroyLine (pCliContext);
        }
    }

    CliContextUnlock ();
}

/***************************************************************************
 * FUNCTION NAME : CliDestroyDefaultConnections
 * DESCRIPTION   : This function clears the pending telnet
 *                 or ssh sessions.
 *
 * INPUT         : pCliContext - Current Cli Context
 *                 i1ClearFlagStatus - Flag to identify whether ssh/telnet
 *                 connections have been cleared by clear line command
 *
 * OUTPUT        : NONE
 ***************************************************************************/
VOID
CliDestroyDefaultConnections (INT1 i1ClearFlagStatus)
{
    INT4                i4Index = 0;
    tCliContext        *pCliContext = NULL;

    CliContextLock ();
    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        pCliContext = &(gCliSessions.gaCliContext[i4Index]);
        if ((pCliContext != NULL))
        {
            pCliContext->i1ClearFlagStatus = i1ClearFlagStatus;
            pCliContext->i1Status = CLI_INACTIVE;
            CliContextCleanup (pCliContext);
        }
    }
    CliContextUnlock ();
}

/***************************************************************************
 * FUNCTION NAME : CliContextLock
 * DESCRIPTION   : This function takes lock and increments debugcount
 *
 * INPUT         : NONE
 *
 * OUTPUT        : SUCCESS?FAILURE
 ***************************************************************************/
INT4
CliContextLock (VOID)
{
    if (OsixSemTake (gCliSessions.CliContextSemId) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : CliContextUnlock
 * DESCRIPTION   : This function unlocks and decrements debug count
 *
 * INPUT         : NONE
 *
 * OUTPUT        : SUCCESS/FAILURE
 ***************************************************************************/
INT4
CliContextUnlock (VOID)
{
    OsixSemGive (gCliSessions.CliContextSemId);
    return CLI_SUCCESS;

}

INT4
FsSshRbCompareMem (tRBElem * e1, tRBElem * e2)
{
    tSshMemTraceInfo   *Node1 = e1;
    tSshMemTraceInfo   *Node2 = e2;
    if (Node1->pAddr > Node2->pAddr)
    {
        return 1;
    }
    else if (Node1->pAddr < Node2->pAddr)
    {
        return -1;
    }
    return 0;
}

/***************************************************************************  
 * FUNCTION NAME : CliSetSessionTimer
 * DESCRIPTION   : This function sets the session time out value for the 
 *                 active session
 *                 
 * INPUT         : i4SessionTimer
 *                 
 *                 
 * 
 * OUTPUT        : NONE
 ***************************************************************************/

VOID
CliSetSessionTimer (INT4 i4SessionTimer)
{
    INT2                i2Index;
    tOsixTaskId         CurrTaskId;

    CurrTaskId = OsixGetCurTaskId ();

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if ((gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
            && (gCliSessions.gaCliContext[i2Index].i1Status == CLI_ACTIVE))
        {
            gCliSessions.gaCliContext[i2Index].i4SessionTimer = i4SessionTimer;
        }
        else
        {
            continue;
        }
    }

}

#ifdef free
#undef free
#endif

VOID
FsFreeSshMemLeaks (VOID)
{
    tSshMemTraceInfo   *pSshMemTraceEntry = NULL;
    tSshMemTraceInfo   *pSshMemTraceNextEntry = NULL;
    tCliContext        *pCliContext = NULL;
    tOsixTaskId         CurrTaskId;
    INT2                i2Index;

    CurrTaskId = OsixGetCurTaskId ();
    CliContextLock ();
    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
        {
            pCliContext = &(gCliSessions.gaCliContext[i2Index]);
            break;
        }
        else
            continue;
    }

    if ((pCliContext == NULL) || ((pCliContext->SSHMemRbPoolId == 0) ||
                                  (pCliContext->pSSHMemRoot == 0)))

    {
        CliContextUnlock ();
        return;
    }

    pSshMemTraceEntry = RBTreeGetFirst (pCliContext->pSSHMemRoot);
    while (pSshMemTraceEntry != NULL)
    {
        pSshMemTraceNextEntry = RBTreeGetNext (pCliContext->pSSHMemRoot,
                                               (tRBElem *) pSshMemTraceEntry,
                                               NULL);

        RBTreeRemove (pCliContext->pSSHMemRoot, pSshMemTraceEntry);

#ifdef SLI_WANTED
        if (pSshMemTraceEntry->pAddr != NULL)
        {
            free (pSshMemTraceEntry->pAddr);
            pSshMemTraceEntry->pAddr = NULL;
        }
#endif

        MemReleaseMemBlock (pCliContext->SSHMemRbPoolId,
                            (UINT1 *) pSshMemTraceEntry);
        pSshMemTraceEntry = pSshMemTraceNextEntry;
    }
    RBTreeDelete (pCliContext->pSSHMemRoot);
    MemDeleteMemPool (pCliContext->SSHMemRbPoolId);
    pCliContext->SSHMemRbPoolId = 0;
    pCliContext->pSSHMemRoot = NULL;
    CliContextUnlock ();

}
#endif
