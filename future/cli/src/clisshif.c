/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clisshif.c,v 1.54 2015/11/23 12:07:39 siva Exp $
 *
 * Description: This file contains the main task 
 *              routine for the Ssh server.  
 *******************************************************************/
#if defined (SLI_WANTED) || defined (BSDCOMP_SLI_WANTED)
#include "lr.h"
#include "clicmds.h"

#ifdef read
#undef read
#endif
#ifdef write
#undef write
#endif
#include "fssocket.h"

#define   CLI_SSH_TASK_NAME         ((const UINT1 *) "SSH")
#define   CLI_SSH_SERVER_PORT    22    /* SSH Server listining port */

INT4                gi4SshServSockDesc = -1;
INT4                gi4SshServV6SockDesc = -1;
tOsixTaskId         gSshMainTaskId;
VOID                CliSshCtxtInit (tCliContext *);
INT4                CliSshIoInit (tCliContext * pCliContext);
UINT1               CliSshRead (tCliContext *);
INT4                CliSshWrite (tCliContext *, CONST CHR1 *, UINT4);
INT4                CliSshIoctl (tCliContext * pCliContext, INT4 i4Code,
                                 VOID *ptr);
VOID                CliSshAcceptIncomingConnection (INT4, INT4);
VOID                CliSshTaskStart (INT1 *pContextAddr);
VOID                CliContextSshInit (tCliContext * pCliContext);
VOID                SshTaskInit (VOID);
tCliContext        *CliGetCurrentContext (VOID);

extern tCliSessions gCliSessions;
PUBLIC tMemPoolId          gCliMaxLineMemPoolId;
PUBLIC INT4 FsSshRbCompareMem (tRBElem *, tRBElem *);

extern tSshSession  gSshSessionAuth[CLI_NUM_SSH_CLIENT_WATCH];

#ifdef SNMP_2_WANTED
extern VOID RegisterFSSSHM PROTO ((VOID));
#endif
extern void         SshKeyGen (tOsixTaskId TaskId);
static UINT4        gu4PrevSSHTime = 0;
static UINT4        gu4NoOfSSHReq = 0;

/*************************************************************************
  FUNCTION      : Routine to Spwan SSH Server Task.
  DESCRIPTION   : This Routine is responsible for Spawning SSH Server Task
  INPUT(S)      : None.
  OUTPUT(S)     : None.
  RETURN(S)     : None 
***************************************************************************/
/* Unused function */
INT4
CliSshInit (VOID)
{

    UINT1               au1TskName[OSIX_NAME_LEN + 4];
    UINT4               u4Retval = 0;
    tOsixTaskId         sTaskId;

    MEMSET (&sTaskId, 0, sizeof (tOsixTaskId));
    MEMSET (au1TskName, 0, sizeof (au1TskName));
    STRCPY (au1TskName, CLI_SSH_TASK_NAME);
    /* Creating a SSH Session Server Task */
    u4Retval = OsixTskCrt (au1TskName,
                           CLI_TASK_PRIORITY,
                           (OSIX_DEFAULT_STACK_SIZE),
                           (VOID *) CliSshTaskMain, 0, &sTaskId);
    return (u4Retval);
}

/*************************************************************************
  FUNCTION      : Routine to keep the CliContext For SSH.
  DESCRIPTION   : This Routine is responsible for Initialising CliContext
                : For SSH Server
  INPUT(S)      : pCliContext:-  Pointer to CliContext.
  OUTPUT(S)     : None.
  RETURN(S)     : None 
***************************************************************************/
VOID
CliSshCtxtInit (pCliContext)
     tCliContext        *pCliContext;
{
    pCliContext->i4InputFd = pCliContext->i4ClientSockfd;
    pCliContext->i4OutputFd = pCliContext->i4ClientSockfd;
    return;
}

/**************************************************************************
    FUNCTION         : CliSshSendEvntIncomingConnection
    DESCRIPTION      : This routine is the call back function registered
                       with select library, which will post an event to
                       CLI SSH server task, with an identification about the
                       socket descriptor that is listening for new connections.
    INPUT(S)         : i4SockFd     - Socket Descriptor.
    OUTPUT(S)        : None.
    GLBALS AFFECTED  : GlobalControl Table.
    RETURN(S)        : None.
***************************************************************************/

VOID
CliSshSendEvntIncomingConnection (INT4 i4SockFd)
{
    UINT4               u4Event = 0;
    INT4                i4SockDesc = -1;

    i4SockDesc = SshArGetServSockId ();

    if (i4SockFd == i4SockDesc)
    {
        u4Event = CLI_SSH_AFINET_CONN_EVENT;
    }
    i4SockDesc = SshArGetServ6SockId ();
    if (i4SockFd == i4SockDesc)
    {
        u4Event = CLI_SSH_AFINET6_CONN_EVENT;
    }
    OsixEvtSend (CLI_SSH_TASK_ID, u4Event);
}

/**************************************************************************
    FUNCTION         : CliSshAcceptIncomingConnection 
    DESCRIPTION      : This routine is the main routine for the telnet 
                       server which, accepting connections from various 
               clients ,
    INPUT(S)         : None.
    OUTPUT(S)        : None.
    GLBALS AFFECTED  : GlobalControl Table.
    RETURN(S)        : None.
***************************************************************************/

VOID
CliSshAcceptIncomingConnection (INT4 i4SockFd, INT4 i4Family)
{
    struct sockaddr_in  ClientSockAddr;    /* The address of a client  */
    struct sockaddr_in6 ClientSockAddr6;    /* The address of a V6 client  */
    INT4                i4ClientSockAddrLen;    /* Length of client's addr */
    INT4                i4ClientSockDesc = -1;
    INT4                i4SockDesc;
    INT2                i2SockOptVal = TRUE;
    INT4                i4BytesWritten = -1;
    INT4                i4Index = 0;
    UINT1               au1LoginData[CLI_LOGIN_PROM_LEN] = CLI_LOGIN_PROMPT;
    UINT4               u4Retval;
    INT1                i1Index = 0;

    tCliContext        *pCliContext = NULL;
    tCliContext        *pcurrContext = NULL;
    tOsixTaskId         sTaskId;

    MEMSET (&ClientSockAddr, 0, sizeof (ClientSockAddr));
    MEMSET (&ClientSockAddr6, 0, sizeof (ClientSockAddr6));
    MEMSET (&sTaskId, 0, sizeof (tOsixTaskId));
    UNUSED_PARAM (pcurrContext);
    UNUSED_PARAM (au1LoginData);
    UNUSED_PARAM (i4BytesWritten);
    UNUSED_PARAM (i2SockOptVal);

    if (i4SockFd < 0)
    {
        return;
    }
    CliSshLock ();
    
    if (i4Family == AF_INET)
    {
        i4ClientSockAddrLen = sizeof (ClientSockAddr);
        gi4SshServSockDesc = i4SockFd;

        /* Waiting for Connections   */
        i4ClientSockDesc = SshArAccept (gi4SshServSockDesc, i4Family);
    }
#ifdef IP6_WANTED
    else if (i4Family == AF_INET6)
    {
        i4ClientSockAddrLen = sizeof (ClientSockAddr6);
        gi4SshServV6SockDesc = i4SockFd;

        /* Waiting for Connections   */
        i4ClientSockDesc = SshArAccept (gi4SshServV6SockDesc, i4Family);
    }
#endif
    if (i4ClientSockDesc < 0)
    {
         CliSshUnLock ();
         return;
    }
    else
    {
        /* Check for free index in Context */
        /* Upadte the connection , else close the connection */
        if (((IssGetDownLoadStatus () == MIB_DOWNLOAD_IN_PROGRESS) &&
             (IssGetTransferMode () == ISS_SFTP_TRANSFER_MODE))
            || ((pCliContext = CliGetFreeContext ()) == NULL))
        {
            /* No free ssh connection or already/"issDownLoadTransferMode"  */
            i4SockDesc = SshArGetSockfd (i4ClientSockDesc);
            SshArTearDownConnection (i4ClientSockDesc);
            close (i4SockDesc);
            CliSshUnLock ();
             
            return;
        }
        /* Spawn a new Task */
        CliContextLock ();
        pCliContext->i4ClientSockfd = i4ClientSockDesc;
        CliContextUnlock ();
        pCliContext->i4InputFd = i4ClientSockDesc;
        pCliContext->i4OutputFd = i4ClientSockDesc;

        i4SockDesc = SshArGetSockfd (i4ClientSockDesc);
        if (i4SockDesc == -1)
        {
            SshArTearDownConnection (i4ClientSockDesc);
            close (i4SockDesc);
            CliContextLock ();
            if (pCliContext->i4BuddyId != -1)
            {
                MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                pCliContext->i4BuddyId = -1;
            }

            if (CliGetCurrentContextIndex () != 0)
            {
                pCliContext->i1Status = CLI_INACTIVE;
            }
            CliContextUnlock ();
            CliSshUnLock(); 
            
            return;
        }

        MEMSET (pCliContext->au4ClientIpAddr, 0,
                sizeof (pCliContext->au4ClientIpAddr));
        if (i4Family == AF_INET)
        {
            getpeername (i4SockDesc,
                     (struct sockaddr *) &ClientSockAddr,
                     (socklen_t *) & i4ClientSockAddrLen);
            /* Assign the peer client address as v4 mapped addres 
             * to differentiate it from v6 addresses
             */
            pCliContext->au4ClientIpAddr[2] = 0xffff;
            pCliContext->au4ClientIpAddr[3] = ClientSockAddr.sin_addr.s_addr;
            pCliContext->u4IpAddrType = IPV4_ADD_TYPE;
        }
        else
        {
            getpeername (i4SockDesc,
                         (struct sockaddr *) &ClientSockAddr6,
                         (socklen_t *) & i4ClientSockAddrLen);
            if (ClientSockAddr6.sin6_family == AF_INET6)
            {
                CLI_MEMCPY (pCliContext->au4ClientIpAddr,
                            ClientSockAddr6.sin6_addr.s6_addr,
                            sizeof (ClientSockAddr6.sin6_addr.s6_addr));
            }
            pCliContext->u4IpAddrType = IPV6_ADD_TYPE;
        }

        /*Blocking the maximum tried Unauthorized ipv6 address*/
        if(pCliContext->u4IpAddrType == IPV6_ADD_TYPE)
        {
            for (i4Index = 0;i4Index < CLI_NUM_SSH_CLIENT_WATCH;i4Index++)
            {
                if ((gSshSessionAuth[i4Index].u1Blocked == 1)
                        &&(MEMCMP(&gSshSessionAuth[i4Index].au4ClientIpAddr,
                        pCliContext->au4ClientIpAddr,
                        sizeof(gSshSessionAuth[i4Index].au4ClientIpAddr)) == 0))
                {
                    close (i4SockDesc);
                    CliContextLock ();
                    if (pCliContext->i4BuddyId != -1)
                    {
                        MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                        pCliContext->i4BuddyId = -1;
                    }

                    if (CliGetCurrentContextIndex () != 0)
                    {
                        pCliContext->i1Status = CLI_INACTIVE;
                    }
                    CliContextUnlock ();
                    CliSshUnLock();
                    
                    return ;
                }
            }
        }

        CliGetTaskName (pCliContext);
        CliContextLock ();
        for (i1Index = 2; i1Index < 10; i1Index++)
          {
            if (i1Index != pCliContext->i4ConnIdx)
               {
                 if  ((&(gCliSessions.gaCliContext[i1Index])) == pCliContext)
                 {
                       CliContextUnlock ();
                       CliSshUnLock ();
                       return;
                 }
               }
          }
       CliContextUnlock ();

       CliSshUnLock ();
       
       u4Retval = OsixTskCrt (pCliContext->au1TskName,
                               CLI_TASK_PRIORITY,
                               (CLI_DEFAULT_STACK_SIZE),
                               (VOID *) CliSshTaskStart,
                               (INT1 *) pCliContext, &sTaskId);
        if (sTaskId != 0)
        {
            SshArSendTaskId (i4ClientSockDesc, sTaskId);
        }

        if (u4Retval == OSIX_SUCCESS)
        {
            return;
        }
        else
        {
            CliSshLock ();
            CliContextCleanup (pCliContext);
            CliContextLock ();
            if (pCliContext->i4BuddyId != -1)
            {
                MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                pCliContext->i4BuddyId = -1;
            }
            pCliContext->i1Status = CLI_INACTIVE;
            if (pCliContext->pu1UserCommand)
            {
                CLI_RELEASE_MAXLINE_MEM_BLOCK(pCliContext->pu1UserCommand);
                pCliContext->pu1UserCommand = NULL;
            }
                                    
            i4SockDesc = SshArGetSockfd (i4ClientSockDesc);
            CliContextUnlock ();
            SshArTearDownConnection (i4ClientSockDesc);
            CliContextLock ();
            pCliContext->i4ClientSockfd = -1;
            close (i4SockDesc);
            CliContextUnlock ();
            CliSshUnLock ();
        
        }
    }

    return;
}

/**************************************************************************
  FUNCTION          : SshTaskMain
  DESCRIPTION       : This is the main telnet processing routine.
  INPUT(S)          : None.
  OUTPUT(S)
  RETURN VALUE      : None.
***************************************************************************/

VOID
CliSshTaskMain (INT1 *pi1Param)
{
    UINT4               u4Event = 0;
    INT4                i4SockDesc = -1;
    UINT4               u4Count;
    UINT4               u4Retval = 0;

    UNUSED_PARAM (pi1Param);
    UNUSED_PARAM (u4Retval);

    OsixTskIdSelf (&(CLI_SSH_TASK_ID));
    SshTaskInit ();

    /* Indicate the status of initialization to the main routine */
    CLI_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    RegisterFSSSHM ();
#endif

    for (u4Count = 0; u4Count < CLI_NUM_SSH_CLIENT_WATCH; u4Count++)
    {
        MEMSET (&gSshSessionAuth[u4Count], 0, sizeof (tSshSession));
    }

    u4Retval = TmrCreateTimerList ((CONST UINT1 *) "SSH", CLI_SSH_TIMER_EVENT,
                                   NULL, (tTimerListId *) & (gSshTimerLst));

    while (1)
    {
        OsixTskDelay (1);

        u4Event = 0;
        OsixEvtRecv (CLI_SSH_TASK_ID,
                     CLI_SSH_AFINET_CONN_EVENT | CLI_SSH_AFINET6_CONN_EVENT
                     | CLI_SSH_TIMER_EVENT
                     | CLI_SSH_DISABLE_EVENT,
                     CLI_SSHCONN_EVENT_WAIT_FLAGS, &u4Event);

        SshArKeyGen (CLI_SSH_TASK_ID);

        if (u4Event & CLI_SSH_AFINET_CONN_EVENT)
        {
            i4SockDesc = SshArGetServSockId ();
            CliSshAcceptIncomingConnection (i4SockDesc, AF_INET);
            SelAddFd (i4SockDesc, CliSshSendEvntIncomingConnection);
        }
        if (u4Event & CLI_SSH_AFINET6_CONN_EVENT)
        {
            i4SockDesc = SshArGetServ6SockId ();
            CliSshAcceptIncomingConnection (i4SockDesc, AF_INET6);
            SelAddFd (i4SockDesc, CliSshSendEvntIncomingConnection);
        }
        if (u4Event & CLI_SSH_DISABLE_EVENT)
        {
            CliDestroyConnections (CLI_SSH_CLEAR, OSIX_FALSE);
            SshArDisable ();
        }
        if (u4Event & CLI_SSH_TIMER_EVENT)
        {
            CliSshTimerExpiryCallBkFun ();
        }
    }
}

/**************************************************************************
  FUNCTION      : SshTaskInit
  DESCRIPTION   :
  INPUT(S)      :
  OUTPUT(S)     :
  RETURN VALUE  :
***************************************************************************/

VOID
SshTaskInit (VOID)
{
    INT4                i4SockDesc = -1;

    SshArInit (CLI_SSH_TASK_ID);
    i4SockDesc = SshArConnectPassiveSocket ();
    if (i4SockDesc != -1)
    {
        SelAddFd (i4SockDesc, CliSshSendEvntIncomingConnection);
    }
    i4SockDesc = -1;
#ifdef IP6_WANTED
    i4SockDesc = SshArConnectPassiveV6Socket ();
    if (i4SockDesc != -1)
    {
        SelAddFd (i4SockDesc, CliSshSendEvntIncomingConnection);
    }
#endif
}

INT4
CliGetConnectionId (VOID)
{
    INT2                i2Index;
    UINT4               u4CurrTaskId;

    u4CurrTaskId = OsixGetCurTaskId ();
    
    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].TaskId == u4CurrTaskId)
            return (gCliSessions.gaCliContext[i2Index].i4ClientSockfd);
        else
            continue;
    }

    return (-1);
}

VOID
CliSshTaskStart (INT1 *pContextAddr)
{
    tCliContext        *pCliContext;
    UINT1               u1PubKeyUsr[MAX_USER_NAME_LEN];
    UINT4               u4CurrSSHTime = 0;
    INT4                i4SockDesc = -1;
    INT1                i1Index = 0;


    CliContextLock ();
    /* update the request count */
    gu4NoOfSSHReq++;

    /* if value is 0, it means this is first time connection */
    if (gu4PrevSSHTime == 0)
    {
        gu4PrevSSHTime = OsixGetSysUpTime ();
        CliContextUnlock ();
    }
    else
    {
        /* new request should atleast have gap of 1 sec. if not
         * delay the new connection
         */
        u4CurrSSHTime = OsixGetSysUpTime ();
        if (((u4CurrSSHTime - gu4PrevSSHTime) <= 1) && (gu4NoOfSSHReq != 0))
        {
            CliContextUnlock ();
            OsixDelayTask (SYS_TIME_TICKS_IN_A_SEC * gu4NoOfSSHReq);
            /* increment the previous variable time by number of seconds */
            CliContextLock ();
            gu4PrevSSHTime = gu4PrevSSHTime + gu4NoOfSSHReq;
            CliContextUnlock ();
        }
        else
        {
            /* update previous time variable with this new time */
            gu4PrevSSHTime = u4CurrSSHTime;
            CliContextUnlock ();
        }
    }

    pCliContext = (tCliContext *) (VOID *) pContextAddr;
    CliContextLock ();
    pCliContext->TaskId = OsixGetCurTaskId ();

    for (i1Index = 2; i1Index < 10; i1Index++)
     {
        if (pCliContext->TaskId != gCliSessions.gaCliContext[i1Index].TaskId)
        {
           if  ((&(gCliSessions.gaCliContext[i1Index])) == pCliContext)
            {
                CliContextUnlock ();
                return;
            }
        }
     }
    
    CliContextUnlock ();

    if (pCliContext->TaskId == 0)
    {
        CliContextLock ();
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }

        if (CliGetCurrentContextIndex () != 0)
        {
            pCliContext->i1Status = CLI_INACTIVE;
        }
        CliContextUnlock ();
        return;
    }

    pCliContext->i1UserAuth = FALSE;

    if (CliContextMemInit (pCliContext) != CLI_FAILURE)
    {
        CliContextLock ();
        CliContextInit (pCliContext);

        /* Context Init resets this value, so restore it */
        pCliContext->SessionActive = TRUE;
        CliContextUnlock ();

        CliContextSshInit (pCliContext);

        SshArUpdateSessionTimeOut (pCliContext->i4ClientSockfd,
                                   pCliContext->i4SessionTimer);

        MEMSET (u1PubKeyUsr, 0, sizeof (u1PubKeyUsr));

/* Adding extra "u1PubKeyUsr" as parameter in "SshArSessionStart"
 * function to get the value when public key authentication is
 * successful*/

        SshArSessionStart (pCliContext->i4ClientSockfd,
                           pCliContext->TaskId, pCliContext->au1TskName,
                           u1PubKeyUsr);

        /* update the request count */
        CliContextLock ();
        if (gu4NoOfSSHReq > 0)
        {
            gu4NoOfSSHReq--;
        }
        pCliContext->u4ReqUpdated = OSIX_TRUE;
        CliContextUnlock ();

/* Updateing the Athuser & privilaged ID in the case of public based 
 * authentication,when authentication retrn successful */
        if (STRLEN (u1PubKeyUsr))
        {
            pCliContext->i1UserAuth = TRUE;
            MEMSET (pCliContext->ai1UserName, 0,
                    sizeof (pCliContext->ai1UserName));
            STRNCPY (pCliContext->ai1UserName, u1PubKeyUsr,
                     MAX_USER_NAME_LEN - 1);
        }
        CliMainProcess (pCliContext);
        SshArClose (pCliContext->i4ClientSockfd);
        CliContextLock ();
        /* In SshArClose taskid will be found from pSshParam */
        pCliContext->TaskId = 0;
        pCliContext->i4ClientSockfd = -1;
        CliContextUnlock ();

    }
    else
    {
        CliSshLock ();

        i4SockDesc = SshArGetSockfd (pCliContext->i4ClientSockfd);
        SshArTearDownConnection (pCliContext->i4ClientSockfd);
        close (i4SockDesc);

        CliContextLock ();
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }

        pCliContext->i1Status = CLI_INACTIVE;
        pCliContext->SessionActive = FALSE;
        
       if (pCliContext->pu1UserCommand)
       {
           CLI_RELEASE_MAXLINE_MEM_BLOCK(pCliContext->pu1UserCommand);
           pCliContext->pu1UserCommand = NULL;
       }
                                
                                
        pCliContext->TaskId = 0;
        CliContextUnlock ();
        CliSshUnLock ();
    
    }
    return;

}

INT4
CliSshIoInit (tCliContext * pCliContext)
{
    UNUSED_PARAM (pCliContext);

    return (CLI_SUCCESS);
}

UINT1
CliSshRead (pCliContext)
     tCliContext        *pCliContext;
{
    UINT1               u1Char;

    u1Char = SshArRead (pCliContext->i4ClientSockfd);
    if ('\r' == u1Char)
    {
        /* For SSH Sessions, '\n' is represented by '\r'
         * where we replace by '\n' and discard '\0'
         */
        u1Char = '\n';
    }
    return (u1Char);
}

INT4
CliSshWrite (pCliContext, pMessage, u4Len)
     tCliContext        *pCliContext;
     CONST CHR1         *pMessage;
     UINT4               u4Len;
{

    if (u4Len == 0)
    {
        return CLI_SUCCESS;
    }

    SshArWrite (pCliContext->i4ClientSockfd, (UINT1 *) ((FS_ULONG) pMessage),
                u4Len);
    if ('\n' == *pMessage)
    {
        SshArWrite (pCliContext->i4OutputFd, (UINT1 *) "\r", 1);
    }
    OsixTskDelay (1);

    return CLI_SUCCESS;
}

INT4
CliSshIoctl (tCliContext * pCliContext, INT4 i4Code, VOID *ptr)
{
    UNUSED_PARAM (pCliContext);
    UNUSED_PARAM (i4Code);
    UNUSED_PARAM (ptr);

    return (CLI_OS_SUCCESS);
}

VOID
CliContextSshInit (tCliContext * pCliContext)
{
    pCliContext->gu4CliMode = CLI_MODE_SSH;

    pCliContext->fpCliIOInit = CliSshIoInit;
    pCliContext->fpCliInput = CliSshRead;
    pCliContext->fpCliOutput = CliSshWrite;
    pCliContext->fpCliIoctl = CliSshIoctl;
    pCliContext->fpCliFileWrite = CliFileWrite;

    if (pCliContext->SSHMemRbPoolId == 0)
     {
      if (MemCreateMemPool(sizeof(tSshMemTraceInfo),MAX_SSH_MEM_TRACE_NODE,
        MEM_DEFAULT_MEMORY_TYPE,&(pCliContext->SSHMemRbPoolId)) == MEM_FAILURE)
        {
          return;
        }
     }
     if (pCliContext->pSSHMemRoot == NULL)
      {
         pCliContext->pSSHMemRoot = RBTreeCreate (MAX_SSH_MEM_TRACE_NODE, FsSshRbCompareMem);
      }


    return;
}

INT4
CliDeleteSshContext (UINT1 *pu1TaskName)
{
    INT4                i4Index;
    INT4                i4Retval = CLI_FAILURE;
    if (STRLEN (pu1TaskName) == 0)
    {
        return i4Retval;
    }
    CliContextLock ();
    for (i4Index = 0; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        if (MEMCMP (gCliSessions.gaCliContext[i4Index].au1TskName, pu1TaskName,
                    STRLEN (pu1TaskName)) == 0)
        {
            /* This function can be invoked from a structured clean-up
             * via a close by the user or via an authentication failure.
             * When called via a close, the CLI buddy memory is set free
             * by the CLI functions. Otherwise we need to free the buddy
             * memory. Hence the check.
             */

            if (gCliSessions.gaCliContext[i4Index].i1Status != CLI_INACTIVE)
            {
                CliContextCleanup (&gCliSessions.gaCliContext[i4Index]);
                if (gCliSessions.gaCliContext[i4Index].i4BuddyId != -1)
                {
                    MemBuddyDestroy ((UINT1) gCliSessions.gaCliContext[i4Index].
                                     i4BuddyId);
                    gCliSessions.gaCliContext[i4Index].i4BuddyId = -1;
                }
                if (i4Index != 0)
                {
                    gCliSessions.gaCliContext[i4Index].i1Status = CLI_INACTIVE;
                }
            }
            /* Reset the global variable which tracks the number of ssh request
             * if its not already updated. Note at this moment i1Status will be
             * inactive. so finding the context is enough.
             */
            if (gCliSessions.gaCliContext[i4Index].u4ReqUpdated == OSIX_FALSE)
            {
                if (gu4NoOfSSHReq > 0)
                {
                    gu4NoOfSSHReq--;
                }
                gCliSessions.gaCliContext[i4Index].u4ReqUpdated = OSIX_TRUE;
            }

            i4Retval = CLI_SUCCESS;
            break;
        }
        else
            continue;
    }

    CliContextUnlock ();
    return i4Retval;
}

/*************************************************************************
  FUNCTION      : Routine to delete SSH context by task id.
  DESCRIPTION   : This Routine is responsible for deleting SSH context by task id
  INPUT(S)      : TaskId
  OUTPUT(S)     : None.
  RETURN(S)     : CLI_SUCCESS / CLI_FAILURE
***************************************************************************/
INT4
CliDeleteSshContextByTaskId (tOsixTaskId TaskId)
{
    INT4                i4Index;
    INT4                i4Retval = CLI_FAILURE;

    CliContextLock ();
    for (i4Index = 1; i4Index < CLI_MAX_SESSIONS; i4Index++)
    {
        if (gCliSessions.gaCliContext[i4Index].TaskId == TaskId)
        {
            if (gCliSessions.gaCliContext[i4Index].i1Status != CLI_INACTIVE)
            {
                CliContextCleanup (&gCliSessions.gaCliContext[i4Index]);
                if (gCliSessions.gaCliContext[i4Index].i4BuddyId != -1)
                {
                    MemBuddyDestroy ((UINT1) gCliSessions.gaCliContext[i4Index].
                                     i4BuddyId);
                    gCliSessions.gaCliContext[i4Index].i4BuddyId = -1;
                }
                if (i4Index != 0)
                {
                    gCliSessions.gaCliContext[i4Index].i1Status = CLI_INACTIVE;
                }
            }
            /* Reset the global variable which tracks the number of ssh request
             * if its not already updated. Note at this moment i1Status will be
             * inactive. so finding the context is enough.
             */
            if (gCliSessions.gaCliContext[i4Index].u4ReqUpdated == OSIX_FALSE)
            {
                if (gu4NoOfSSHReq > 0)
                {
                    gu4NoOfSSHReq--;
                }
                gCliSessions.gaCliContext[i4Index].u4ReqUpdated = OSIX_TRUE;
            }

            i4Retval = CLI_SUCCESS;
            break;
        }
        else
            continue;
    }
    CliContextUnlock ();
    return i4Retval;
}

/*************************************************************************
   FUNCTION      : Routine to get current cli context
   DESCRIPTION   : This Routine returns a pointer to the current cli context
   INPUT(S)      : None
   OUTPUT(S)     : tCliContext pointer
   RETURN(S)     : None
***************************************************************************/
tCliContext        *
CliGetCurrentContext (VOID)
{
    INT2                i2Index;
    UINT4               u4currTaskId;

    u4currTaskId = OsixGetCurTaskId ();

    for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
    {
        if (gCliSessions.gaCliContext[i2Index].TaskId == u4currTaskId)
            return &(gCliSessions.gaCliContext[i2Index]);
        else
            continue;
    }

/* Error Checking */
    if (i2Index == CLI_MAX_SESSIONS)
    {
        /* Task Not Found */
        ;
    }
    return NULL;
}

/*************************************************************************
   FUNCTION      : Routine to reset SSH context task id to 0.
   DESCRIPTION   : This Routine is responsible for resetting SSH task id to 0
   INPUT(S)      : None
   OUTPUT(S)     : None.
   RETURN(S)     : None
***************************************************************************/

VOID
CliClearSshTaskId (VOID)
{
    tCliContext        *pCliContext = NULL;
    CliContextLock ();
    pCliContext = CliGetCurrentContext ();
    if (pCliContext != NULL)
    {
        pCliContext->TaskId = 0;
    }
    CliContextUnlock ();
}

/*****************************************************************************
 *  Function Name   : CliSetSshServerStatus
 *  Description     : This will start/stop the ISS SSH daemon
 *  Input           : i4SshStatus - Enable / Disable
 *  Output          : None
 *  Returns         : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CliSetSshServerStatus (INT4 i4SshStatus)
{
    UINT1               u1ServerStatus = SSH_ENABLE;
#ifdef LNXIP4_WANTED
    tCliContext         *pCliContext = NULL;
#endif

    SshArGetSshServerStatus (&u1ServerStatus);
    if (i4SshStatus == ISS_DISABLE)
    {
        /* Don't post if the status is already in same state or
         * transition towards the same */
        if ((u1ServerStatus == i4SshStatus))
        {
            return CLI_SUCCESS;
        }
#ifndef LNXIP4_WANTED
        if (OsixSendEvent (SELF, (CONST UINT1 *) CLI_SSH_TASK_NAME,
                           CLI_SSH_DISABLE_EVENT) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
#else
        pCliContext = CliGetContext ();
        if (( pCliContext != NULL ) && ( pCliContext->gu4CliMode == CLI_MODE_SSH ))
        {
            mmi_printf( "\r\n SSH feature cannot be disabled, as SSH session/s are ACTIVE \r\n");
            return CLI_FAILURE;
        }
        else
        {

       CliDestroyConnections (CLI_SSH_CLEAR, OSIX_FALSE);
       SshArDisable ();
        }
#endif
    }
    return CLI_SUCCESS;
}
#ifdef malloc
#undef malloc
#endif
#ifdef free
#undef free
#endif
#ifdef calloc
#undef calloc
#endif
#ifdef realloc
#undef realloc
#endif
void* FsCustMalloc (size_t size)
{
        tCliContext          *pCliContext = NULL;
        tSshMemTraceInfo     *pSshMemTraceEntry = NULL;
        INT1                 *pi1Node =NULL;
        tOsixTaskId          CurrTaskId;
        INT2                 i2Index;
 
        CliContextLock();
        pi1Node = malloc (size);
        if(pi1Node == NULL)
         {
           CliContextUnlock();
            return NULL;
         }
         CurrTaskId = OsixGetCurTaskId ();

         for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
         {
           if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
            {
                pCliContext = &(gCliSessions.gaCliContext[i2Index]);
                break;
            }
          }

         if ((pCliContext == NULL) || ((pCliContext->SSHMemRbPoolId == 0) ||
                           (pCliContext->pSSHMemRoot == 0)))
         {
             CliContextUnlock();
             return pi1Node;
         }
         if ((pSshMemTraceEntry = (tSshMemTraceInfo *)
                            MemAllocMemBlk (pCliContext->SSHMemRbPoolId)) == NULL)
          {
             CliContextUnlock();
             return pi1Node;
          }

        MEMSET(pSshMemTraceEntry,0,sizeof(tSshMemTraceInfo));
        pSshMemTraceEntry->pAddr = pi1Node;
        if (RBTreeAdd (pCliContext->pSSHMemRoot, pSshMemTraceEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
            pSshMemTraceEntry->pAddr = NULL;
        }

          CliContextUnlock();
     return pi1Node;
}



VOID* FsCustCalloc (size_t nmemb, size_t size)
{
        tCliContext          *pCliContext = NULL;
        tSshMemTraceInfo     *pSshMemTraceEntry = NULL;
        INT1                 *pi1Node=NULL;
        tOsixTaskId         CurrTaskId;
        INT2                i2Index;

        CliContextLock();
        pi1Node = calloc (nmemb, size);
        if(pi1Node == NULL)
        {
           CliContextUnlock();
           return NULL;
        }
        CurrTaskId = OsixGetCurTaskId ();

        for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
         {
            if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
              {
                   pCliContext = &(gCliSessions.gaCliContext[i2Index]);
                   break;
              }
         }

         if ((pCliContext == NULL) || ((pCliContext->SSHMemRbPoolId == 0) ||
                  (pCliContext->pSSHMemRoot == 0)))
         {
               CliContextUnlock();
               return pi1Node;
         }

         if ((pSshMemTraceEntry = (tSshMemTraceInfo *)
                MemAllocMemBlk (pCliContext->SSHMemRbPoolId)) == NULL)
         {
              CliContextUnlock();
              return pi1Node;
         }

         MEMSET(pSshMemTraceEntry,0,sizeof(tSshMemTraceInfo));
         pSshMemTraceEntry->pAddr = pi1Node;

        if (RBTreeAdd (pCliContext->pSSHMemRoot, pSshMemTraceEntry) == RB_FAILURE)
         {
           MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
           pSshMemTraceEntry->pAddr = NULL;
         }

        CliContextUnlock();
        return pi1Node;
}


void* FsCustRealloc (void *ptr, size_t size)
{
        tCliContext          *pCliContext = NULL;
        tSshMemTraceInfo     *pSshMemTraceEntry = NULL;
        tSshMemTraceInfo     SshMemTraceEntry;
        INT1                 *pi1Node = 0;
        tOsixTaskId          CurrTaskId = 0;
        INT2                 i2Index = 0;

        CliContextLock();
        pi1Node = realloc (ptr, size);

        if(pi1Node == NULL)
          {
             CliContextUnlock();
             return NULL;
          }

         CurrTaskId = OsixGetCurTaskId ();
          MEMSET(&SshMemTraceEntry, 0, sizeof(tSshMemTraceInfo));

         for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
          {
            if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
                   {
                    pCliContext = &(gCliSessions.gaCliContext[i2Index]);
                        break;
                    }
          }

          if ((pCliContext == NULL) || ((pCliContext->SSHMemRbPoolId == 0) ||
                    (pCliContext->pSSHMemRoot == 0)))
           {
                 CliContextUnlock();
                return pi1Node;
           }

            SshMemTraceEntry.pAddr = ptr;

           pSshMemTraceEntry = RBTreeGet(pCliContext->pSSHMemRoot, &SshMemTraceEntry);
          if (pSshMemTraceEntry != NULL)
           {
            RBTreeRemove (pCliContext->pSSHMemRoot, pSshMemTraceEntry);

            MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
           }

           if ((pSshMemTraceEntry = (tSshMemTraceInfo *)
                    MemAllocMemBlk (pCliContext->SSHMemRbPoolId)) == NULL)
             {
                     CliContextUnlock();
                     return pi1Node;
             }

          MEMSET(pSshMemTraceEntry,0,sizeof(tSshMemTraceInfo));
          pSshMemTraceEntry->pAddr = pi1Node;

          if (RBTreeAdd (pCliContext->pSSHMemRoot, pSshMemTraceEntry) == RB_FAILURE)
           {
               MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
               pSshMemTraceEntry->pAddr = NULL;
           }

          CliContextUnlock();
          return pi1Node;
}


void FsCustFree (void *pNode)
{
        tCliContext          *pCliContext = NULL;
        tSshMemTraceInfo     SshMemTraceEntry;
        tSshMemTraceInfo     *pSshMemTraceEntry = NULL;
        tOsixTaskId         CurrTaskId;
        INT2                i2Index;

        CliContextLock();
        CurrTaskId = OsixGetCurTaskId ();

        for (i2Index = 0; i2Index < CLI_MAX_SESSIONS; i2Index++)
                                          {
            if (gCliSessions.gaCliContext[i2Index].TaskId == CurrTaskId)
             {
                pCliContext = &(gCliSessions.gaCliContext[i2Index]);
                    break;
             }
       }

     if ((pCliContext == NULL) || ((pCliContext->SSHMemRbPoolId == 0) ||
                             (pCliContext->pSSHMemRoot == 0)))
       {
            if(pNode != NULL)
             {
                free (pNode);
                pNode = NULL;
             }
           CliContextUnlock();
           return ;
       }

        SshMemTraceEntry.pAddr = pNode;

        pSshMemTraceEntry = RBTreeGet(pCliContext->pSSHMemRoot, &SshMemTraceEntry);
       if (pSshMemTraceEntry != NULL)
       {
              RBTreeRemove (pCliContext->pSSHMemRoot, pSshMemTraceEntry);
              pSshMemTraceEntry->pAddr = NULL; 
              MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
       }
       else
       {
           pSshMemTraceEntry = RBTreeGetFirst(pCliContext->pSSHMemRoot);
           while(pSshMemTraceEntry != NULL)
           {

              if(pSshMemTraceEntry->pAddr != pNode)
                {
                   pSshMemTraceEntry=RBTreeGetNext(pCliContext->pSSHMemRoot, (tRBElem *)pSshMemTraceEntry,NULL);
                }
              else
               {
                   RBTreeRemove (pCliContext->pSSHMemRoot, pSshMemTraceEntry);
                   pSshMemTraceEntry->pAddr = NULL;
                   MemReleaseMemBlock(pCliContext->SSHMemRbPoolId, (UINT1 *)pSshMemTraceEntry);
                   break;
               }
           }
        }

 free (pNode);
 pNode = NULL;
 CliContextUnlock();
}


#endif
