/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: clisktif.c,v 1.76 2017/12/27 10:31:19 siva Exp $
 *
 * Description: This file contains the main task 
 *              routine for the Telnet server4.  
 *******************************************************************/
#if defined (BSDCOMP_SLI_WANTED) || (defined (SLI_WANTED) && defined (TCP_WANTED) )
#include "lr.h"
#include "clicmds.h"
#include "fssocket.h"
#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif
PUBLIC tMemPoolId   gCliMaxLineMemPoolId;

#ifdef CLI_POLL
#define  CLI_POLL_SET_SOCK_NONBLOCK(x) fcntl(x,F_SETFL,O_NONBLOCK)
#else
#define  CLI_POLL_SET_SOCK_NONBLOCK(x)
#endif

static UINT1        au1EchoWont[TS_ECHO_WONT_LEN] =
    { TS_IAC, TS_WONT, TS_ECHO };
static UINT1        au1EchoWill[TS_ECHO_WILL_LEN] =
    { TS_IAC, TS_WILL, TS_ECHO };
static UINT1        au1CharWill[TS_CHAR_WILL_LEN] =
    { TS_IAC, TS_WILL, TS_CHAR };
static UINT1        au1NawsDo[TS_NAWS_DO_LEN] = { TS_IAC, TS_DO, TS_NAWS };

extern tCliSessions gCliSessions;
tOsixTaskId         gTelnetTaskId;
INT4                gi4TelnetSockDesc = -1;
INT4                gi4Telnet6SockDesc = -1;
VOID                CliTelnetTaskStart (INT1 *);
INT4                gi4TelnetPort = 23;
static tCliTelnetServerParams gCliTelnetServerParams = { ISS_DISABLE };
extern tMemPoolId   gCliMaxOutputPoolId;

/*************************************************************************
  FUNCTION      : Routine to keep the Socket in passive state.
  DESCRIPTION   : This Routine is responsible for opening Telnet4 Server
                      1. Opens a primary socket for Telnet4 Server, 
                      2. Binds to it ,
                      3. Places it in passive mode thereby listening
  INPUT(S)      : None.
  OUTPUT(S)     : None.
  RETURN(S)     : Socket descriptor of V4 Server. 
***************************************************************************/
INT4
CliConnectPassiveSocket (VOID)
{
    struct sockaddr_in  Ts4SrvAddr;
#ifdef LNXIP4_WANTED
    struct linger       so_linger;
#endif
    INT4                i4RetValue;
    INT4                i4SockDesc = OSIX_FAILURE;
    INT4                i4SrvAddrLen;
#ifdef KERNEL_WANTED
    tSourcePort         TelnetPort;
    INT4                i4CommandStatus;
#endif

    /*Allocate Socket  for TS4 Server  */
    i4SockDesc = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (i4SockDesc < 0)
    {
        return (CLI_FAILURE);
    }

    /*Set the linger time out to 0 to make the linux to close the 
     *socket created for the tcp connection without waiting in the TIME_WAIT state*/
#ifdef LNXIP4_WANTED
    so_linger.l_onoff = TRUE;
    so_linger.l_linger = 0;
    setsockopt (i4SockDesc, SOL_SOCKET, SO_LINGER,
                &so_linger, sizeof so_linger);

    if (setsockopt (i4SockDesc, SOL_SOCKET, SO_REUSEADDR,
                    (INT4 *) &i4RetValue, sizeof (i4RetValue)) < 0)
    {
        close (i4SockDesc);
        return (CLI_FAILURE);
    }

#endif

    /* Bind to the well known telnet port */
    i4SrvAddrLen = sizeof (Ts4SrvAddr);
    MEMSET ((VOID *) &Ts4SrvAddr, 0, i4SrvAddrLen);
    Ts4SrvAddr.sin_family = AF_INET;
    Ts4SrvAddr.sin_addr.s_addr = OSIX_HTONL (gu4IssCliTlntSrvrBindAddr);
    Ts4SrvAddr.sin_port = OSIX_HTONS (gi4IssCliTlntSrvrPrimaryBindPort);

    i4RetValue =
        bind (i4SockDesc, (struct sockaddr *) &Ts4SrvAddr, i4SrvAddrLen);

    if (i4RetValue < 0)
    {
        printf ("\r\nTelnet port(%d) is already in use. Using port no. %d\n",
                gi4IssCliTlntSrvrPrimaryBindPort,
                gi4IssCliTlntSrvrAlternateBindPort);
        Ts4SrvAddr.sin_port = OSIX_HTONS (gi4IssCliTlntSrvrAlternateBindPort);

        i4RetValue =
            bind (i4SockDesc, (struct sockaddr *) &Ts4SrvAddr, i4SrvAddrLen);

        if (i4RetValue < 0)
        {
            printf
                ("\r\nTelnet port(%d) and port %d are in use. Exiting...",
                 gi4IssCliTlntSrvrPrimaryBindPort,
                 gi4IssCliTlntSrvrAlternateBindPort);
            close (i4SockDesc);
            return (CLI_FAILURE);
        }
        else
        {
            gi4TelnetPort = gi4IssCliTlntSrvrAlternateBindPort;
        }
    }
    else
    {
        gi4TelnetPort = gi4IssCliTlntSrvrPrimaryBindPort;
    }
    gCliTelnetServerParams.i4CliTelnetStatus = ISS_ENABLE_IN_PROGRESS;

#ifdef  KERNEL_WANTED
    TelnetPort.i4Cmd = IPAUTH_SET_TELNET_PORT;
    TelnetPort.i4Port = gi4TelnetPort;
    i4CommandStatus = KAPIUpdateInfo (IPAUTH_PORT_IOCTL,
                                      (tKernCmdParam *) & TelnetPort);

    if (i4CommandStatus == OSIX_FAILURE)
    {
        return (CLI_FAILURE);
    }
#endif

    /* Wait for Connections from client */
    i4RetValue = listen (i4SockDesc, MAX_RMT_CONN_REQ_PEND);

    if (i4RetValue < 0)
    {
        close (i4SockDesc);
        return (CLI_FAILURE);
    }

    gi4TelnetSockDesc = i4SockDesc;
    return (CLI_SUCCESS);
}

/************************************************************************ 
 *  Function Name   : CliGetTelnetPort 
 *  Description     : This will get the port on which ISS telnet daemon is
 *                    listening 
 *  Input           : None 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/
INT4
CliGetTelnetPort ()
{
    return (gi4TelnetPort);
}

#ifdef IP6_WANTED
/*************************************************************************
  FUNCTION      : CliConnectPassiveV6Socket
  DESCRIPTION   : This Routine is responsible for opening Telnet6 Server
                      1. Opens a primary socket for Telnet6 Server, 
                      2. Binds to it ,
                      3. Places it in passive mode thereby listening
  INPUT(S)      : None.
  OUTPUT(S)     : None.
  RETURN(S)     : Socket descriptor of V6 Server. 
***************************************************************************/
INT4
CliConnectPassiveV6Socket (VOID)
{
    struct sockaddr_in6 Ts6SrvAddr;
    INT4                i4RetValue;
    INT4                i4SockDesc = OSIX_FAILURE;
    INT4                i4SrvAddrLen;
    INT4                i4OptVal = 0;

    /*Allocate Socket  for TS6 Server  */
    i4SockDesc = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);

    if (i4SockDesc < 0)
    {
        return (CLI_FAILURE);
    }

    i4OptVal = OSIX_TRUE;
    i4RetValue = setsockopt (i4SockDesc, IPPROTO_IPV6, IPV6_V6ONLY,
                             &i4OptVal, sizeof (i4OptVal));
    if (i4RetValue < 0)
    {
        close (i4SockDesc);
        return (CLI_FAILURE);
    }

    i4SrvAddrLen = sizeof (Ts6SrvAddr);
    MEMSET ((VOID *) &Ts6SrvAddr, 0, i4SrvAddrLen);
    Ts6SrvAddr.sin6_family = AF_INET6;
    MEMSET (Ts6SrvAddr.sin6_addr.s6_addr, 0, 16);
    Ts6SrvAddr.sin6_port = OSIX_HTONS (CLI_TELNET_SERVER_PORT);

    i4RetValue =
        bind (i4SockDesc, (struct sockaddr *) &Ts6SrvAddr, i4SrvAddrLen);

    if (i4RetValue < 0)
    {
        printf ("\r\nTelnet port(23) is already in use. Using port no. 6023");
        Ts6SrvAddr.sin6_port = OSIX_HTONS (CLI_TELNET_SERVER_PORT1);

        i4RetValue =
            bind (i4SockDesc, (struct sockaddr *) &Ts6SrvAddr, i4SrvAddrLen);
        if (i4RetValue < 0)
        {
            printf
                ("\r\nTelnet port(23) and port 6023 are in use."
                 "IPv6 Telnet server bind fails ...");
            return (CLI_FAILURE);
        }
    }

    /* Wait for Connections from client */
    i4RetValue = listen (i4SockDesc, MAX_RMT_CONN_REQ_PEND);

    if (i4RetValue < 0)
    {
        return (CLI_FAILURE);
    }
    gi4Telnet6SockDesc = i4SockDesc;
    return (CLI_SUCCESS);
}
#endif

/**************************************************************************
    FUNCTION         : CliSendEvntIncomingConnection
    DESCRIPTION      : This routine is the call back function registered
                       with select library, which will post an event to 
                       CLI telnet server task, with an identification about the
                       socket descriptor that is listening for new connections.
    INPUT(S)         : i4SockFd     - Socket Descriptor.
    OUTPUT(S)        : None.
    GLBALS AFFECTED  : GlobalControl Table.
    RETURN(S)        : None.
***************************************************************************/

VOID
CliSendEvntIncomingConnection (INT4 i4SockFd)
{
    UINT4               u4Event = 0;
    if (i4SockFd == gi4TelnetSockDesc)
    {
        u4Event = CLI_AFINET_CONN_EVENT;
    }
    if (i4SockFd == gi4Telnet6SockDesc)
    {
        u4Event = CLI_AFINET6_CONN_EVENT;
    }
    OsixEvtSend (gTelnetTaskId, u4Event);
}

/**************************************************************************
    FUNCTION         : CliAcceptIncomingConnection
    DESCRIPTION      : This routine is the main routine for the telnet
                       server which, accepting connections from various
                       clients.
    INPUT(S)         : i4SockFd   - Socket Descriptor.
    OUTPUT(S)        : None.
    GLBALS AFFECTED  : GlobalControl Table.
    RETURN(S)        : None.
***************************************************************************/

VOID
CliAcceptIncomingConnection (INT4 i4SockFd)
{
    tCliContext        *pCliContext = NULL;
    tOsixTaskId         sTaskId;
    struct sockaddr_in  ClientSockAddr;    /* The address of a client  */
    struct sockaddr_in6 ClientSockAddr6;    /* The address of a v6 client  */
    UINT4               u4Retval;
    INT4                i4ClientSockAddrLen;    /* Length of client's addr */
    INT4                i4ClientSockDesc = -1;
    CHR1                au1ErrMsg[MAX_LINE_LEN] =
        "\r\n\r\n\r\nExceeding max CLI sessions! ";

    INT1                i1SockOptFlag = 0;

    MEMSET (&ClientSockAddr, 0, sizeof (ClientSockAddr));
    MEMSET (&ClientSockAddr6, 0, sizeof (ClientSockAddr6));
    if (i4SockFd == gi4TelnetSockDesc)
    {
        i4ClientSockAddrLen = sizeof (ClientSockAddr);
        /* Waiting for Connections   */
        i4ClientSockDesc = accept (gi4TelnetSockDesc,
                                   (struct sockaddr *) &ClientSockAddr,
                                   (socklen_t *) & i4ClientSockAddrLen);
    }
#ifdef IP6_WANTED
    else if (i4SockFd == gi4Telnet6SockDesc)
    {
        i4ClientSockAddrLen = sizeof (ClientSockAddr6);
        /* Waiting for Connections   */
        i4ClientSockDesc = accept (gi4Telnet6SockDesc,
                                   (struct sockaddr *) &ClientSockAddr6,
                                   (socklen_t *) & i4ClientSockAddrLen);
    }
#endif
    if (i4ClientSockDesc < 0)
    {
        close (i4ClientSockDesc);
    }
    else
    {
        /* Check for free index in Context */
        /* Upadte the connection , else close the connection */

        if ((pCliContext = CliGetFreeContext ()) == NULL)
        {
            /* Close the new connection */
            CliSockSend (i4ClientSockDesc, au1ErrMsg, STRLEN (au1ErrMsg));
            close (i4ClientSockDesc);
        }
        else
        {
            TCP_SET_NODELAY (i4ClientSockDesc, IPPROTO_TCP, TCP_NODELAY,
                             (INT1 *) &i1SockOptFlag, sizeof (INT1));

            /* Spawn a new Task */
            CliContextLock ();

            pCliContext->i4ClientSockfd = i4ClientSockDesc;

            CliContextUnlock ();
            pCliContext->i4InputFd = i4ClientSockDesc;
            pCliContext->i4OutputFd = i4ClientSockDesc;
            MEMSET (pCliContext->au4ClientIpAddr, 0,
                    sizeof (pCliContext->au4ClientIpAddr));
            if (ClientSockAddr.sin_family == AF_INET)
            {
                getpeername (i4ClientSockDesc,
                             (struct sockaddr *) &ClientSockAddr,
                             (socklen_t *) & i4ClientSockAddrLen);

                /* Assign the peer client address as v4 mapped addres 
                 * to differentiate it from v6 addresses
                 */
                pCliContext->au4ClientIpAddr[2] = 0xffff;
                pCliContext->au4ClientIpAddr[3] =
                    ClientSockAddr.sin_addr.s_addr;
                pCliContext->u4IpAddrType = IPV4_ADD_TYPE;
            }
            else
            {
                getpeername (i4ClientSockDesc,
                             (struct sockaddr *) &ClientSockAddr6,
                             (socklen_t *) & i4ClientSockAddrLen);
                if (ClientSockAddr6.sin6_family == AF_INET6)
                {
                    CLI_MEMCPY (pCliContext->au4ClientIpAddr,
                                ClientSockAddr6.sin6_addr.s6_addr,
                                sizeof (ClientSockAddr6.sin6_addr.s6_addr));
                }
                pCliContext->u4IpAddrType = IPV6_ADD_TYPE;
            }

            CliGetTaskName (pCliContext);

            u4Retval = OsixTskCrt (pCliContext->au1TskName,
                                   CLI_TASK_PRIORITY,
                                   CLI_DEFAULT_STACK_SIZE,
                                   (OsixTskEntry) CliTelnetTaskStart,
                                   (INT1 *) pCliContext, &sTaskId);
            if (u4Retval != OSIX_SUCCESS)
            {
                /* Close the new connection */
                close (i4ClientSockDesc);
                CliContextLock ();
                pCliContext->i4ClientSockfd = -1;
                if (pCliContext->i4BuddyId != -1)
                {
                    MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                    pCliContext->i4BuddyId = -1;
                }
                pCliContext->i4ConnIdx = 0;
                pCliContext->i1Status = CLI_INACTIVE;
                pCliContext->TaskId = 0;

                if (pCliContext->pu1UserCommand)
                {
                    CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->pu1UserCommand);
                    pCliContext->pu1UserCommand = NULL;
                }

                CliContextUnlock ();

            }
        }
    }

    return;
}

/**************************************************************************
  FUNCTION          : TelnetTaskMain
  DESCRIPTION       : This is the main telnet processing routine.
  INPUT(S)          : None.
  OUTPUT(S)
  RETURN VALUE      : None.
***************************************************************************/

VOID
CliTelnetTaskMain (INT1 *pi1Param)
{
    UINT4               u4Event = 0;

    UNUSED_PARAM (pi1Param);

    OsixTskIdSelf (&gTelnetTaskId);

    CliSetTelnetServerStatus (ISS_ENABLE);

    /* Indicate the status of initialization to the main routine */
    CLI_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        CLI_POLL_DELAY (1);
        u4Event = 0;
        OsixEvtRecv (gTelnetTaskId,
                     (CLI_AFINET_CONN_EVENT | CLI_AFINET6_CONN_EVENT |
                      CLI_TELNET_ENABLE_EVENT | CLI_TELNET_DISABLE_EVENT),
                     CLI_TELCONN_EVENT_WAIT_FLAGS, &u4Event);

        if ((u4Event & CLI_TELNET_DISABLE_EVENT) &&
            (ISS_DISABLE_IN_PROGRESS ==
             gCliTelnetServerParams.i4CliTelnetStatus))
        {
            CliDestroyConnections (CLI_TELNET_CLEAR, OSIX_FALSE);
            /* Close the telnet server socket */
            if (gi4TelnetSockDesc >= 0)
            {
                SelRemoveFd (gi4TelnetSockDesc);
                close (gi4TelnetSockDesc);
                gi4TelnetSockDesc = -1;
            }
            if (gi4Telnet6SockDesc >= 0)
            {
                SelRemoveFd (gi4Telnet6SockDesc);
                close (gi4Telnet6SockDesc);
                gi4Telnet6SockDesc = -1;
            }
            gCliTelnetServerParams.i4CliTelnetStatus = ISS_DISABLE;
        }
        if ((u4Event & CLI_TELNET_ENABLE_EVENT) &&
            (ISS_ENABLE_IN_PROGRESS ==
             gCliTelnetServerParams.i4CliTelnetStatus))
        {
            TelnetTaskInit ();
            if (gi4TelnetSockDesc != -1)
            {
                CLI_POLL_SET_SOCK_NONBLOCK (gi4TelnetSockDesc);
            }
            if (gi4Telnet6SockDesc != -1)
            {
                CLI_POLL_SET_SOCK_NONBLOCK (gi4Telnet6SockDesc);
            }
            /* When IPv6 is not wanted, gi4Telnet6SockDesc will always be -1
             * and if IPv6 is wanted, the (gi4Telnet6SockDesc == -1) check 
             * is meaningful. Hence the below condition is valid in all the
             * scenarios */
            if ((gi4TelnetSockDesc == -1) && (gi4Telnet6SockDesc == -1))
            {
                /* Overwriting the ADMIN configuration */
                CliSetTelnetServerStatus (ISS_DISABLE);
                continue;

            }
            gCliTelnetServerParams.i4CliTelnetStatus = ISS_ENABLE;
            /* Since it is an admin event nothing to worry about other
             * event flags though they are set by chance :-) */
            continue;
        }

        if (u4Event & CLI_AFINET_CONN_EVENT)
        {
            CliAcceptIncomingConnection (gi4TelnetSockDesc);
            SelAddFd (gi4TelnetSockDesc, CliSendEvntIncomingConnection);
        }
#ifdef IP6_WANTED
        if (u4Event & CLI_AFINET6_CONN_EVENT)
        {
            CliAcceptIncomingConnection (gi4Telnet6SockDesc);
            SelAddFd (gi4Telnet6SockDesc, CliSendEvntIncomingConnection);
        }
#endif
    }
}

/**************************************************************************
  FUNCTION      : TelnetTaskInit
  DESCRIPTION   :
  INPUT(S)      :
  OUTPUT(S)     :
  RETURN VALUE  :
***************************************************************************/

VOID
TelnetTaskInit (VOID)
{
    if (CliConnectPassiveSocket () != CLI_FAILURE)
    {
        SelAddFd (gi4TelnetSockDesc, CliSendEvntIncomingConnection);
    }
#ifdef IP6_WANTED
    if (CliConnectPassiveV6Socket () != CLI_FAILURE)
    {
        SelAddFd (gi4Telnet6SockDesc, CliSendEvntIncomingConnection);
    }
#endif
}

VOID
CliTelnetTaskStart (INT1 *pContextAddr)
{
    tCliContext        *pCliContext;
    tOsixTaskId         TaskId;
    pCliContext = (tCliContext *) (VOID *) pContextAddr;
    CliContextLock ();
    pCliContext->TaskId = OsixGetCurTaskId ();
    CliContextUnlock ();

    if (pCliContext->TaskId == 0)
    {
        CliContextLock ();
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }

        if (CliGetCurrentContextIndex () != 0)
        {
            pCliContext->i1Status = CLI_INACTIVE;
        }
        CliContextUnlock ();
        return;
    }

    pCliContext->i1UserAuth = FALSE;

    if (CliContextMemInit (pCliContext) != CLI_FAILURE)
    {
        CliContextLock ();
        CliContextInit (pCliContext);
        /* Context Init resets the session active flag. so restore it */
        pCliContext->SessionActive = TRUE;
        CliContextUnlock ();
        CliContextTelnetInit (pCliContext);
        CliMainProcess (pCliContext);
    }
    else
    {
        CliContextLock ();
        if (pCliContext->i4BuddyId != -1)
        {
            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
            pCliContext->i4BuddyId = -1;
        }
        CliContextUnlock ();
    }
    CliContextLock ();
    TaskId = pCliContext->TaskId;
    close (pCliContext->i4ClientSockfd);
    pCliContext->i4ClientSockfd = -1;

    if (pCliContext->pu1UserCommand)
    {
        CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->pu1UserCommand);
        pCliContext->pu1UserCommand = NULL;
    }

    pCliContext->i1Status = CLI_INACTIVE;
    pCliContext->SessionActive = FALSE;
    pCliContext->TaskId = 0;
    CliContextUnlock ();
    if (TaskId != 0)
    {
        CliUtilMgmtClearLockStatus ();
        OsixTskDel (TaskId);
    }
    return;
}

UINT1
CliTelnetRead (pCliContext)
     tCliContext        *pCliContext;
{
    struct timeval      CliTimeval;
    struct timeval     *pTimer = NULL;
    fd_set              i4ReadFds;
    INT4                i4ReadCount = 0;
    INT4                i4Retval = 0;
    UINT1               u1Char = 0;
    tOsixTaskId         TaskId;

    CliTimeval.tv_sec = pCliContext->i4SessionTimer;
    CliTimeval.tv_usec = 0;

    MEMSET (&i4ReadFds, 0, sizeof (fd_set));
    /* FD_ZERO (&i4ReadFds); */
    FD_SET (pCliContext->i4ClientSockfd, &i4ReadFds);

    if (pCliContext->i4SessionTimer)
        pTimer = &CliTimeval;
    while (i4ReadCount <= 0)
    {
        if (pCliContext->i4LineBuffLen > 0)
        {
            u1Char = pCliContext->au1LineBuff[pCliContext->i4LineBuffPos];
            pCliContext->i4LineBuffPos++;
            if (pCliContext->i4LineBuffPos >= pCliContext->i4LineBuffLen)
            {
                pCliContext->i4LineBuffLen = 0;
                pCliContext->i4LineBuffPos = 0;
            }
            i4ReadCount = 1;
        }
        else
        {
            i4Retval = (INT2) select (pCliContext->i4ClientSockfd + 1,
                                      &i4ReadFds, NULL, NULL, pTimer);
            if ((i4Retval < 0) && (errno != EINTR))
            {
                mmi_printf ("Socket Error, Logging Out !!!\n");
                CliTimeoutExpiry (pCliContext);
                return (CLI_FAILURE);
            }
            if (i4Retval == 0 && pTimer != NULL)
            {
                if ((pTimer->tv_sec == 0) && (pTimer->tv_usec == 0))
                {
                    mmi_printf (CliLogOutMessage ());
                    CliTimeoutExpiry (pCliContext);
                    return (CLI_FAILURE);
                }
                else
                {

                    /* If forceful Clear line is given, then read event could have
                     * arrived for that. Need to handle that using EINTR
                     */
                    if (errno == EINTR)
                    {
                        CliContextLock ();
                        TaskId = pCliContext->TaskId;
                        pCliContext->i1UserAuth = FALSE;
                        if (pCliContext->mmi_exit_flag != TRUE)
                        {
                            if (pCliContext->pu1PendBuff != NULL)
                            {
                                MemReleaseMemBlock (gCliCtxPendBuff,
                                                    (UINT1 *) pCliContext->
                                                    pu1PendBuff);
                                pCliContext->pu1PendBuff = NULL;
                            }
                            if (pCliContext->ppu1OutBuf != NULL)
                            {
                                if (pCliContext->ppu1OutBuf[0] != NULL)
                                {

                                    MemReleaseMemBlock (gCliCtxOutputBuff,
                                                        (UINT1 *) pCliContext->
                                                        ppu1OutBuf[0]);
                                    pCliContext->ppu1OutBuf[0] = NULL;
                                }
                            }
                            if (pCliContext->ppu1OutBuf != NULL)
                            {
                                MemReleaseMemBlock (gCliCtxOutputBuffArr,
                                                    (UINT1 *) pCliContext->
                                                    ppu1OutBuf);

                                pCliContext->ppu1OutBuf = NULL;
                            }
                            pCliContext->u4PendBuffLen = 0;
                            if (pCliContext->pu1OutputMessage != NULL)
                            {
                                CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->
                                                                  pu1OutputMessage);
                                pCliContext->pu1OutputMessage = NULL;
                            }

                        }

                        pCliContext->mmi_exit_flag = TRUE;
                        pCliContext->SessionActive = FALSE;

                        if (pCliContext->i4BuddyId != -1)
                        {
                            MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                            pCliContext->i4BuddyId = -1;
                        }
                        if (pCliContext->pu1UserCommand)
                        {
                            CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                                           pu1UserCommand);
                            pCliContext->pu1UserCommand = NULL;
                        }

                        pCliContext->i1Status = CLI_INACTIVE;
                        pCliContext->TaskId = 0;
                        close (pCliContext->i4ClientSockfd);
                        pCliContext->i4ClientSockfd = -1;
                        CliContextUnlock ();

                        CliUtilMgmtClearLockStatus ();
                        OsixTskDel (TaskId);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        FD_SET (pCliContext->i4ClientSockfd, &i4ReadFds);
                        continue;
                    }
                }
            }
            i4ReadCount = CliSockRecv (pCliContext->i4InputFd, &u1Char, 1);
        }
        if (i4ReadCount <= 0)
        {
            mmi_exit ();
            pCliContext->mmi_exit_flag = TRUE;
            return (CLI_FAILURE);
        }
        if (!(u1Char) || (CliIsDelimit (u1Char, "\n")))
        {
            if (pCliContext->u1TelnetCharDiscard == OSIX_TRUE)
            {
                pCliContext->u1TelnetCharDiscard = OSIX_FALSE;
                u1Char = 0;
                i4ReadCount = 0;
                continue;
            }
        }

        while (255 == u1Char)
        {
            if (pCliContext->u1InitWinSize == FALSE)
            {
                pCliContext->fpCliIoctl (pCliContext, CLI_GET_WINSZ, NULL);
                pCliContext->u1InitWinSize = TRUE;
            }

            CliHandleOutOfBandData (pCliContext);
            i4ReadCount = 0;
            u1Char = 0;
        }

        if (CliIsDelimit (u1Char, "\r"))
        {
            /* For Telnet Sessions, '\n' is represented by '\r' followed by 
             * '\n', where we replace '\r' by '\n' and discard '\n'
             * So set the flag to discard the next character if it is '\n'
             * in next read.
             */
            pCliContext->u1TelnetCharDiscard = OSIX_TRUE;
            u1Char = '\n';
            continue;
        }
        pCliContext->u1TelnetCharDiscard = OSIX_FALSE;
    }

    FD_CLR (pCliContext->i4ClientSockfd, &i4ReadFds);
    if (!pCliContext->SessionActive)
    {
        pCliContext->SessionActive = TRUE;
    }
    if (u1Char == 3)
    {
        /* Ctrl-c entered at telnet session CLI prompt
         * set bit-CLI_CTRL_BRK_MASK in u4Flags which is Ctrl-c flag bit
         */
        pCliContext->u4Flags = (pCliContext->u4Flags | CLI_CTRL_BRK_MASK);
    }

    return u1Char;
}

INT4
CliTelnetWrite (pCliContext, pMessage, u4Len)
     tCliContext        *pCliContext;
     CONST CHR1         *pMessage;
     UINT4               u4Len;
{
    if (CliSockSend (pCliContext->i4OutputFd, pMessage, u4Len) <= 0)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
CliTelnetIoctl (tCliContext * pCliContext, INT4 i4Code, VOID *ptr)
{
    INT4                i4RetStatus = CLI_OS_SUCCESS;
    UNUSED_PARAM (ptr);

    switch (i4Code)
    {
        case CLI_SET_NONBLOCK:
            break;

        case CLI_ECHO_ENABLE:
            CliTsEnableEchoMode (pCliContext);
            break;

        case CLI_ECHO_DISABLE:
            CliTsDisableEchoMode (pCliContext);
            break;

        case CLI_SWAP_INPUT:

            /* This directive is used to swap the input file descriptor with a
             * given file. If the file name is console then the descriptor for
             * stdin is used henceforth. Otherwise the specified file is opened
             * and the corresponding  descriptor is set as the input file 
             * descriptor.
             */

            break;

        case CLI_SWAP_OUTPUT:

            break;

        case CLI_FLUSH_OUTPUT:
            break;

        case CLI_CLEAR_SCREEN:
            CliClear ();
            break;

        case CLI_SET_NONCANON:

            /* Setting the terminal in non-canonical 
             * character mode */
            CliTsEnableCharMode (pCliContext);
            break;

        case CLI_GET_WINSZ:

            /* Getting the terminal Size */
            CliTsEnableWinSzMode (pCliContext);
            break;

        default:
            i4RetStatus = CLI_OS_FAILURE;
    }
    return (i4RetStatus);
}

VOID
CliContextTelnetInit (tCliContext * pCliContext)
{
    pCliContext->gu4CliMode = CLI_MODE_TELNET;

    pCliContext->fpCliIOInit = CliTelnetIoInit;
    pCliContext->fpCliInput = CliTelnetRead;
    pCliContext->fpCliOutput = CliTelnetWrite;
    pCliContext->fpCliIoctl = CliTelnetIoctl;
    pCliContext->fpCliFileWrite = CliFileWrite;

    return;
}

/*******************************************************************************
  FUNCTION       : CliTsDisableEchoMode 
  DESCRIPTION    : This routine disables the echo mode of the master terminal
                   descriptor and also sets the terminal echo disabling for 
           the slave side of the descriptor.
  INPUT(S)       : i4ConnIdx - Telnet session Id.
  OUTPUT(S)      : None.
  RETURN VALUE   : TS_OK, If Successful
                   TS_NOT_OK, If fails.
*******************************************************************************/
INT4
CliTsDisableEchoMode (tCliContext * pCliContext)
{
    CliSockSend (pCliContext->i4OutputFd, (VOID *) au1EchoWill,
                 TS_ECHO_WILL_LEN);
    return OSIX_SUCCESS;
}

/*******************************************************************************
  FUNCTION       : CliTsEnableEchoMode 
  DESCRIPTION    : This routine enables the echo mode of the master terminal
                   descriptor and also sets the terminal echo disabling for 
           the slave side of the descriptor.
  INPUT(S)       : i4ConnIdx - Telnet session Id.
  OUTPUT(S)      : None.
  RETURN VALUE   : TS_OK, If Successful
                   TS_NOT_OK, If fails.
*******************************************************************************/
INT4
CliTsEnableEchoMode (tCliContext * pCliContext)
{
    CliSockSend (pCliContext->i4OutputFd, (VOID *) au1EchoWont,
                 TS_ECHO_WONT_LEN);
    return OSIX_SUCCESS;
}

/*******************************************************************************
  FUNCTION       : CliTsEnableCharMode 
  DESCRIPTION    : This routine enables the Char mode of the master terminal
                   descriptor and also sets the terminal echo disabling for 
           the slave side of the descriptor.
  INPUT(S)       : i4ConnIdx - Telnet session Id.
  OUTPUT(S)      : None.
  RETURN VALUE   : TS_OK, If Successful
                   TS_NOT_OK, If fails.
*******************************************************************************/
INT4
CliTsEnableCharMode (tCliContext * pCliContext)
{
    CliSockSend (pCliContext->i4OutputFd, (VOID *) au1CharWill,
                 TS_CHAR_WILL_LEN);
    return OSIX_SUCCESS;
}

/*******************************************************************************
  FUNCTION       : CliTsEnableWinSzMode 
  DESCRIPTION    : This routine enables the Negotiate About Window Size (NAWS)
                   mode of the master terminal.
  INPUT(S)       : pCliContext->Context Structure.
  OUTPUT(S)      : None.
  RETURN VALUE   : TS_OK, If Successful
                   TS_NOT_OK, If fails.
*******************************************************************************/
INT4
CliTsEnableWinSzMode (tCliContext * pCliContext)
{
    CliSockSend (pCliContext->i4OutputFd, (VOID *) au1NawsDo, TS_NAWS_DO_LEN);
    return OSIX_SUCCESS;
}

VOID
CliHandleOutOfBandData (tCliContext * pCliContext)
{
    INT4                i4ReadCount = 0;
    INT4                i4Retval = 0;
    UINT1               u1Char;
    UINT1               au1CtrlData[MAX_TEL_CTRL_DATA];
    UINT1               u1Count = 0;

    if (((i4ReadCount = CliSockRecv (pCliContext->i4InputFd, &u1Char, 1)) == 0)
        || (i4ReadCount == -1))
    {
        mmi_exit ();
        return;
    }

    i4ReadCount = 0;
    if (u1Char == CLI_SUBNEG)
    {
        while ((i4Retval =
                CliSockRecv (pCliContext->i4InputFd, &u1Char, 1)) > 0)
        {
            if (i4ReadCount < (INT4) sizeof (au1CtrlData))
            {
                au1CtrlData[i4ReadCount++] = u1Char;
            }
            u1Count++;
            /* Break either END comes or COUNT expires */
            if ((u1Char == CLI_SBNEND) || (u1Count == CLI_SBNCOUNT))
                break;
        }
        if (i4Retval <= 0)
        {
            mmi_exit ();
            return;
        }

        /* CLI_NAWS Windows Size Negotiation */
        if (au1CtrlData[0] == CLI_NAWS)
        {
            if (au1CtrlData[1] != 0 || au1CtrlData[2] != 0 ||
                au1CtrlData[3] != 0 || au1CtrlData[4] != 0)
            {
                /* The window size values are 2 byte values, hence converting
                 * single byte values to 2 byte values
                 */
                pCliContext->i2PrevMaxCol = pCliContext->i2MaxCol;
                pCliContext->i2PrevMaxRow = pCliContext->i2MaxRow;
                pCliContext->i2MaxCol =
                    (INT2) (au1CtrlData[1] * 256 + au1CtrlData[2]);
                pCliContext->i2MaxRow =
                    (INT2) (au1CtrlData[3] * 256 + au1CtrlData[4]);

                /* Value equal to zero is acceptable for the width (or height), and 
                 * means that no character width (or height) is being sent. In this 
                 * case, the width (or height) that will be assumed by the Telnet 
                 * server is operating system specific - RFC 1073*/

                if (pCliContext->i2MaxCol == 0)
                {
                    /* Column value received is so, so take the default value */
                    pCliContext->i2MaxCol = CLI_DEF_MAX_COLS;
                }
                if (pCliContext->i2MaxRow == 0)
                {
                    /* Row value received is so, so take the default value */
                    pCliContext->i2MaxRow = CLI_DEF_MAX_ROWS;
                }

                pCliContext->i1WinResizeFlag = FALSE;

                if ((pCliContext->i2MaxRow != pCliContext->i2PrevMaxRow) ||
                    (pCliContext->i2MaxCol != pCliContext->i2PrevMaxCol))
                {
                    pCliContext->i1WinResizeFlag = TRUE;
                    CliHandleWinSizeChange (pCliContext);
                }
            }
        }
    }
    else if ((u1Char == TS_WILL) || (u1Char == TS_WONT)
             || (u1Char == TS_DO) || (u1Char == TS_DONT))
    {
        /* Reading the option code and discarding for now */
        if (((i4ReadCount =
              CliSockRecv (pCliContext->i4InputFd, &u1Char, 1)) == 0)
            || (i4ReadCount == -1))
        {
            mmi_exit ();
            return;
        }
    }
    else
    {
        /* 2 Byte Telnet Options */
        return;
    }
}

UINT4
CliSockSend (INT4 i4OutputFd, CONST CHR1 * pMessage, UINT4 u4Len)
{
    struct timeval      CliTimeval;
    struct timeval     *pTimer = NULL;
    fd_set              i4WriteFds;
    tCliContext        *pCliContext = NULL;
    UINT4               u4OctetsWritten = 0;
    INT4                i4RetVal = 0;
    INT4                i4Retries = 0;
    tOsixTaskId         TaskId;

    CliTimeval.tv_sec = 0;
    CliTimeval.tv_usec = 0;
    MEMSET (&i4WriteFds, 0, sizeof (fd_set));
    pTimer = &CliTimeval;
    while (1)
    {
        CliTimeval.tv_sec = 1;
        CliTimeval.tv_usec = 0;
        FD_ZERO (&i4WriteFds);
        FD_SET (i4OutputFd, &i4WriteFds);

        if ((i4RetVal = select (i4OutputFd + 1,
                                NULL, &i4WriteFds, NULL, pTimer)) > 0)
        {
            i4RetVal =
                send (i4OutputFd, ((VOID *) ((FS_ULONG) pMessage)),
                      (u4Len - u4OctetsWritten),
                      (MSG_NOSIGNAL | MSG_PUSH_FLAG));

            if (i4RetVal > 0)
            {
                u4OctetsWritten += i4RetVal;
                if (u4OctetsWritten == u4Len)
                {
                    break;
                }
                pMessage += i4RetVal;
            }
            else if (i4RetVal < 0)
            {
                return CLI_FAILURE;
            }
            else
            {
                if (++i4Retries > CLI_MAX_SEND_RETRY)
                {
                    /* Cannot wait continously for sending */
                    return CLI_FAILURE;
                }
            }
            continue;
        }
        else if (i4RetVal < 0)
        {
            return CLI_FAILURE;
        }
        else
        {
            /* Find the case where select is just returning as timeout 
             * This means unable to write anything on the socket.
             * which inturns means that connection is half closed or just received
             * RST from peer. So close it from application's perspective 
             */
            if (errno == EINTR)
            {
                if ((pCliContext = CliGetContext ()) == NULL)
                {
                    /* No Task Found */
                    return (CLI_FAILURE);
                }
                CliContextLock ();
                TaskId = pCliContext->TaskId;
                pCliContext->i1UserAuth = FALSE;
                if (pCliContext->mmi_exit_flag != TRUE)
                {
                    if (pCliContext->pu1PendBuff != NULL)
                    {
                        MemReleaseMemBlock (gCliCtxPendBuff,
                                            (UINT1 *) pCliContext->pu1PendBuff);
                        pCliContext->pu1PendBuff = NULL;
                    }
                    if (pCliContext->ppu1OutBuf != NULL)
                    {
                        if (pCliContext->ppu1OutBuf[0] != NULL)
                        {
                            MemReleaseMemBlock (gCliCtxOutputBuff,
                                                (UINT1 *) pCliContext->
                                                ppu1OutBuf[0]);
                            pCliContext->ppu1OutBuf[0] = NULL;
                        }
                    }
                    if (pCliContext->ppu1OutBuf != NULL)
                    {
                        MemReleaseMemBlock (gCliCtxOutputBuffArr,
                                            (UINT1 *) pCliContext->ppu1OutBuf);

                        pCliContext->ppu1OutBuf = NULL;
                    }
                    pCliContext->u4PendBuffLen = 0;
                    if (pCliContext->pu1OutputMessage != NULL)
                    {
                        CLI_RELEASE_OUTPUT_MSG_MEM_BLOCK (pCliContext->
                                                          pu1OutputMessage);
                        pCliContext->pu1OutputMessage = NULL;
                    }
                    if (pCliContext->pu1UserCommand)
                    {
                        CLI_RELEASE_MAXLINE_MEM_BLOCK (pCliContext->
                                                       pu1UserCommand);
                        pCliContext->pu1UserCommand = NULL;
                    }

                }

                pCliContext->mmi_exit_flag = TRUE;
                pCliContext->SessionActive = FALSE;

                if (pCliContext->i4BuddyId != -1)
                {
                    MemBuddyDestroy ((UINT1) pCliContext->i4BuddyId);
                    pCliContext->i4BuddyId = -1;
                }

                pCliContext->i1Status = CLI_INACTIVE;
                pCliContext->TaskId = 0;
                close (pCliContext->i4ClientSockfd);
                pCliContext->i4ClientSockfd = -1;
                CliContextUnlock ();

                CliUtilMgmtClearLockStatus ();
                OsixTskDel (TaskId);

                return CLI_FAILURE;
            }

            if (++i4Retries > CLI_MAX_SEND_RETRY)
            {
                /* Cannot wait continously for sending */
                return CLI_FAILURE;
            }
        }
        CLI_POLL_DELAY (10);
    }
    return u4OctetsWritten;
}

INT4
CliSockRecv (INT4 i4InputFd, UINT1 *u1Char, INT4 i4NumToRead)
{
    INT4                i4ReadCount = 0;
    tCliContext        *pCliContext = NULL;

    if ((pCliContext = CliGetContext ()) == NULL)
    {
        /* No Task Found */
        return (CLI_FAILURE);
    }
    UNUSED_PARAM (i4NumToRead);

    if (pCliContext->i4LineBuffLen == 0)
    {
        MEMSET (pCliContext->au1LineBuff, 0, CLI_MAX_LINE_BUFF_LEN);
        i4ReadCount =
            recv (i4InputFd, pCliContext->au1LineBuff, CLI_MAX_LINE_BUFF_LEN,
                  0);
        if (i4ReadCount <= 0)
        {
            return 0;
        }
        pCliContext->i4LineBuffLen = i4ReadCount;
    }

    *u1Char = pCliContext->au1LineBuff[pCliContext->i4LineBuffPos];
    pCliContext->i4LineBuffPos++;
    if (pCliContext->i4LineBuffPos >= pCliContext->i4LineBuffLen)
    {
        pCliContext->i4LineBuffLen = 0;
        pCliContext->i4LineBuffPos = 0;
    }
    return 1;
}

UINT1
CliTelnetIsBreak (tCliContext * pCliContext)
{
    UINT1               u1Input = 0;
    INT4                i4Count = 0;

    do
    {
        i4Count = CliSockRecv (pCliContext->i4InputFd, &u1Input, 1);

        if ((i4Count == 1) && (u1Input == 3))
        {
            /* u1Input value 3 indicates a Ctrl-C pressed at telnet session */

            pCliContext->u4Flags = (pCliContext->u4Flags | CLI_CTRL_BRK_MASK);
            return u1Input;
        }

    }
    while (i4Count > 0);

    return u1Input;
}

/*****************************************************************************
 *  Function Name   : CliSetTelnetServerStatus 
 *  Description     : This will start/stop the ISS telnet daemon
 *  Input           : i4TelnetStatus - Enable / Disable
 *  Output          : None 
 *  Returns         : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CliSetTelnetServerStatus (INT4 i4TelnetStatus)
{
    UINT4               u4Event;
    tCliContext        *pCliContext = NULL;

    /* Don't post if the status is already in same state or
     * transition towards the same */

    pCliContext = CliGetContext ();

    if ((pCliContext != NULL) && (pCliContext->gu4CliMode == CLI_MODE_TELNET))
    {
        mmi_printf
            ("\r\n%% Telnet feature cannot be disabled, as telnet session/s are active \r\n");
        return CLI_FAILURE;
    }

    if (i4TelnetStatus == ISS_ENABLE)
    {
        if ((gCliTelnetServerParams.i4CliTelnetStatus == i4TelnetStatus) ||
            (ISS_ENABLE_IN_PROGRESS ==
             gCliTelnetServerParams.i4CliTelnetStatus))
        {
            return CLI_SUCCESS;
        }

        u4Event = CLI_TELNET_ENABLE_EVENT;
        gCliTelnetServerParams.i4CliTelnetStatus = ISS_ENABLE_IN_PROGRESS;
    }
    else                        /* ISS_DISABLE */
    {
        if ((gCliTelnetServerParams.i4CliTelnetStatus == i4TelnetStatus) ||
            (ISS_DISABLE_IN_PROGRESS ==
             gCliTelnetServerParams.i4CliTelnetStatus))
        {
            return CLI_SUCCESS;
        }
        u4Event = CLI_TELNET_DISABLE_EVENT;
        gCliTelnetServerParams.i4CliTelnetStatus = ISS_DISABLE_IN_PROGRESS;
    }

    if (OsixSendEvent (SELF, (CONST UINT1 *) CLI_TELNET_TASK_NAME, u4Event)
        == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : CliGetTelnetServerStatus 
 *  Description     : This will tells the status of ISS telnet daemon
 *  Input           : None 
 *  Output          : pi4TelnetStatus - Enable / Disable
 *  Returns         : CLI_SUCCESS
 *****************************************************************************/
INT4
CliGetTelnetServerStatus (INT4 *pi4TelnetStatus)
{
    *pi4TelnetStatus = gCliTelnetServerParams.i4CliTelnetStatus;
    return CLI_SUCCESS;
}

#endif
