##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: make.h,v 1.35 2014/06/18 11:09:05 siva Exp $                      #
# DESCRIPTION  : make.h file for VPN                                     #
##########################################################################

#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 26/05/2002                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the CLI                |
# |                                                                          |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | 26/05/2002 | Creation of makefile                              |
# +--------------------------------------------------------------------------+

CLI_BASE_DIR    = $(BASE_DIR)/cli

CLIDEF_DIR     = $(BASE_DIR)/clidefs
CLI_INCD        = ${CLI_BASE_DIR}/inc
CLI_SRCD        = ${CLI_BASE_DIR}/src
CLI_OBJD        = ${CLI_BASE_DIR}/obj
CLI_EXED        = ${BASE_DIR}/cli/obj
CLI_NPTD        = ${BASE_DIR}/internaltools/npapi/pltfmcli

# module includes
FUTURE_INC      = $(BASE_DIR)/inc
FUTURE_IP6UTIL_INC  = $(BASE_DIR)/util/ip6
FUTURE_RMON_INC  = $(BASE_DIR)/rmon/inc
FUTURE_IP_INC  = $(BASE_DIR)/ip/ip/inc
FUTURE_LA_INC  = $(BASE_DIR)/la/inc
FUTURE_CFA_INC  = $(BASE_DIR)/cfa2/inc
FUTURE_DIFFSRV_INC  = $(BASE_DIR)/ISS/diffsrv/inc
FUTURE_SYS_INC  = $(BASE_DIR)/ISS/system/inc
FUTURE_VLAN_INC  = $(BASE_DIR)/vlangarp/vlan/inc
FUTURE_HS_INC   = $(BASE_DIR)/hs/hscli/inc
FUTURE_PBB_INC  = $(BASE_DIR)/bbbridge/pbb/inc
FUTURE_ELMI_INC  = $(BASE_DIR)/elmi/inc
FUTURE_RSTP_INC  = $(BASE_DIR)/stp/rstp/inc
FUTURE_MSTP_INC  = $(BASE_DIR)/stp/mstp/inc
FUTURE_DVMRP_INC = $(BASE_DIR)/dvmrp/inc
FUTURE_OSPF_INC  = $(BASE_DIR)/ospf/inc
FUTURE_OSPF3_INC = $(BASE_DIR)/ospf3/inc
FUTURE_SNMP3_INC = $(BASE_DIR)/snmpv3
EOAM_TEST_INC = $(BASE_DIR)/eoam/test
VPN_INC_DIR = ${BASE_DIR}/vpn/inc

ifeq (${WSSCFG}, YES)
WSSCFG_INC_DIR = ${BASE_DIR}/wss/wsscfg/inc
endif

ifeq (${CAPWAP}, YES)
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
endif

ifeq (${RFMGMT}, YES)
RFMGMT_INC_DIR = ${BASE_DIR}/wss/rfmgmt/inc
endif

ifeq (${OPENFLOW}, YES)
OFC_DIR = ${BASE_DIR}/ofcl/inc
endif
ifeq (${OFC_TEST_SERVER}, YES)
OFC_TEST_SERVER_DIR = $(BASE_DIR)/ofcl/server
endif

###############
# compilation #
###############
SYSTEM_COMPILATION_SWITCHES += -DCLCLI_WANTED
PROJECT_COMPILATION_SWITCHES = -D_POSIX_SOURCE \
                               -DSPIM_SM_CLI   \
                               -UCLI_TRACE_WANTED
                                
ifeq (${CLI_LNXIP},YES)
PROJECT_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
endif
PROJECT_FINAL_INCLUDE_DIRS    = -I$(CLI_INCD) \
                                -I$(FUTURE_INC) \
                                -I$(CLI_INCL_DIR) \
                                -I$(FUTURE_IP6UTIL_INC) \
                                -I$(FUTURE_RMON_INC) \
                                -I$(FUTURE_IP_INC) \
                                -I$(FUTURE_LA_INC) \
                                -I$(FUTURE_CFA_INC) \
                                -I$(FUTURE_DIFFSRV_INC) \
                                -I$(FUTURE_SYS_INC) \
                                -I$(FUTURE_VLAN_INC) \
                                -I$(FUTURE_HS_INC) \
                                -I$(FUTURE_PBB_INC) \
                                -I$(FUTURE_ELMI_INC) \
                                -I$(FUTURE_RSTP_INC) \
                                -I$(FUTURE_MSTP_INC) \
                                -I$(FUTURE_DVMRP_INC) \
                                -I$(FUTURE_OSPF_INC) \
                                -I$(FUTURE_OSPF3_INC) \
                                -I$(FUTURE_SNMP3_INC) \
                                -I$(EOAM_TEST_INC) \
                                -I$(VPN_INC_DIR) \
                                $(COMMON_INCLUDE_DIRS) 



ifeq (${WSSCFG}, YES)
PROJECT_FINAL_INCLUDE_DIRS    += -I$(WSSCFG_INC_DIR) 
endif

ifeq (${CAPWAP}, YES)
PROJECT_FINAL_INCLUDE_DIRS    += -I$(CAPWAP_INC_DIR)
endif

ifeq (${RFMGMT}, YES)
PROJECT_FINAL_INCLUDE_DIRS    += -I$(RFMGMT_INC_DIR) 
endif

ifeq (${OPENFLOW}, YES)
PROJECT_FINAL_INCLUDE_DIRS    += -I$(OFC_DIR)
endif
ifeq (${OFC_TEST_SERVER}, YES)
PROJECT_FINAL_INCLUDE_DIRS    += -I$(OFC_TEST_SERVER_DIR)
endif

C_FLAGS                        = $(CC_FLAGS) \
                                $(GENERAL_COMPILATION_SWITCHES) \
                                ${SYSTEM_COMPILATION_SWITCHES} \
                                ${PROJECT_COMPILATION_SWITCHES} \
                                ${PROJECT_FINAL_INCLUDE_DIRS} \
                                -DCLI_MEM_TRC \
                                -DCLI_CONSOLE

###########
# linking #
###########
LDR                           = ${LINKER} ${LINK_FLAGS}


# Specify the project include directories and dependencies
PROJECT_FINAL_INCLUDE_FILES  = $(CLI_INCD)/clicmds.h \
                         $(CLI_INCD)/cliconst.h \
                         $(CLI_INCD)/cliextn.h \
                         $(CLI_INCD)/climdcmd.h \
                         $(CLI_INCD)/clignmsg.h \
                         $(CLI_INCD)/cligntype.h \
                         $(CLI_INCD)/cligndfs.h \
                         $(CLI_INCD)/cliproto.h \
                         $(CLI_INCD)/climib.h \
                         $(CLI_INCD)/cliport.h \
                         $(CLI_INCD)/clidefs.h

################
# Dependencies #
################

PROJECT_INTERNAL_DEPENDENCIES = \
            $(PROJECT_FINAL_INCLUDE_FILES) \
            $(CLI_BASE_DIR)/make.h \
            $(CLI_BASE_DIR)/Makefile 

PROJECT_EXTERNAL_DEPENDENCIES = \
            $(FUTURE_INC)/ipv6.h \
            $(FUTURE_INC)/rmon.h \
            $(FUTURE_IP6UTIL_INC)/ip6util.h \
            $(UTILCLI_INCLUDE_DIR)/utilcli.h \
            $(FUTURE_INC)/cli.h \
            $(FUTURE_INC)/la.h 

PROJECT_DEPENDENCIES =  \
            ${PROJECT_EXTERNAL_DEPENDENCIES} \
            ${PROJECT_INTERNAL_DEPENDENCIES} \
            ${COMMON_DEPENDENCIES}
            
