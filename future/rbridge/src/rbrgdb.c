/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgdb.c,v 1.6 2012/04/02 13:40:12 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"
#include "rbrgcli.h"
/****************************************************************************
 Function    :  RbrgTestAllFsrbridgeGlobalTable
 Input       :  pu4ErrorCode
                pRbrgSetFsrbridgeGlobalEntry
                pRbrgIsSetFsrbridgeGlobalEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgeGlobalTable (UINT4 *pu4ErrorCode,
                                 tRbrgFsrbridgeGlobalEntry *
                                 pRbrgSetFsrbridgeGlobalEntry,
                                 tRbrgIsSetFsrbridgeGlobalEntry *
                                 pRbrgIsSetFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry GlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pGlobalEntry = NULL;

    MEMSET (&GlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    GlobalEntry.MibObject.u4FsrbridgeContextId =
        pRbrgSetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId;
    pGlobalEntry = (tRbrgFsrbridgeGlobalEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) & GlobalEntry);
    if ((pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl != TRUE) ||
        ((pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl == TRUE) &&
         (pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl !=
          RBRG_START)))
    {
        if (pGlobalEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "RBridge is shut in this context\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgSetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RBRG_CLI_CONTEXT_OUT_OF_RANGE);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Index out of range\r\n"));
        return OSIX_FAILURE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable !=
        OSIX_FALSE)
    {
        if ((pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeUniMultipathEnable != TRUE) &&
            (pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeUniMultipathEnable != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value entered\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeMultiMultipathEnable != TRUE) &&
            (pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeMultiMultipathEnable != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value entered\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pRbrgSetFsrbridgeGlobalEntry->MibObject.
            u4FsrbridgeNicknameNumber >
            FsRBRGSizingParams[MAX_RBRG_FSRBRIDGENICKNAMETABLE_SIZING_ID].
            u4PreAllocatedUnits)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_NICKNAME_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Nickname number exceeded\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeSystemControl != RBRG_SHUT) &&
            (pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeSystemControl != RBRG_START))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value entered\r\n"));
            return OSIX_FAILURE;
        }

    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeModuleStatus != RBRG_ENABLE) &&
            (pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeModuleStatus != RBRG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value entered\r\n"));
            return OSIX_FAILURE;
        }

    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsrbridgeGlobalEntry->MibObject.
             i4FsrbridgeClearCounters != TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value entered\r\n"));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestAllFsrbridgeNicknameTable
 Input       :  pu4ErrorCode
                pRbrgSetFsrbridgeNicknameEntry
                pRbrgIsSetFsrbridgeNicknameEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgeNicknameTable (UINT4 *pu4ErrorCode,
                                   tRbrgFsrbridgeNicknameEntry *
                                   pRbrgSetFsrbridgeNicknameEntry,
                                   tRbrgIsSetFsrbridgeNicknameEntry *
                                   pRbrgIsSetFsrbridgeNicknameEntry)
{
    tRbrgMibFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgMibFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRbrgFsrbridgeNicknameEntry NicknameEntry;
    tRbrgFsrbridgeNicknameEntry *pNicknameEntry = NULL;
    UINT4               u4Count = 0;

    MEMSET (&NicknameEntry, 0, sizeof (tRbrgFsrbridgeNicknameEntry));

    if (pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_CONTEXT_OUT_OF_RANGE);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Index is out of range\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    RbrgGlobalEntry.u4FsrbridgeContextId =
        pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
    pRbrgGlobalEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                                  (tRBElem *) & RbrgGlobalEntry);

    if (pRbrgGlobalEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }

    if ((pRbrgSetFsrbridgeNicknameEntry->MibObject.
         i4FsrbridgeNicknameName < RBRG_MIN_NICKNAME) ||
        (pRbrgSetFsrbridgeNicknameEntry->MibObject.
         i4FsrbridgeNicknameName > RBRG_MAX_NICKNAME))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_NICKNAME_OUT_OF_RANGE);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Index is out of range\r\n"));
        return OSIX_FAILURE;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknamePriority > RBRG_MAX_PRIORITY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_NICKNAME_PRIORITY_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Nickname Priority is out of range\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknameDtrPriority > RBRG_MAX_DTRPRIORITY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_NICKNAME_DTR_PRIORITY_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Nickname distribution tree Priority is out of range\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsrbridgeNicknameEntry->MibObject.
             i4FsrbridgeNicknameStatus != RBRG_STATIC) &&
            (pRbrgSetFsrbridgeNicknameEntry->MibObject.
             i4FsrbridgeNicknameStatus != RBRG_DYNAMIC) &&
            (pRbrgSetFsrbridgeNicknameEntry->MibObject.
             i4FsrbridgeNicknameStatus != RBRG_INVALID))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for status\r\n"));
            return OSIX_FAILURE;
        }
        if (pRbrgSetFsrbridgeNicknameEntry->MibObject.
            i4FsrbridgeNicknameStatus != RBRG_INVALID)
        {
            NicknameEntry.MibObject.u4FsrbridgeContextId =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
            NicknameEntry.MibObject.i4FsrbridgeNicknameName =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.
                i4FsrbridgeNicknameName;
            pNicknameEntry =
                (tRbrgFsrbridgeNicknameEntry *) RBTreeGet (gRbrgGlobals.
                                                           RbrgGlbMib.
                                                           FsrbridgeNicknameTable,
                                                           (tRBElem *) &
                                                           NicknameEntry);
            if (pNicknameEntry == NULL)
            {

                RBTreeCount (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                             &u4Count);
                if (u4Count == pRbrgGlobalEntry->u4FsrbridgeNicknameNumber)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (RBRG_CLI_MAX_NICKNAME_REACHED);
                    RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                               "Rbridge count exceeded\r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            NicknameEntry.MibObject.u4FsrbridgeContextId =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
            NicknameEntry.MibObject.i4FsrbridgeNicknameName =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.
                i4FsrbridgeNicknameName;
            pNicknameEntry =
                (tRbrgFsrbridgeNicknameEntry *) RBTreeGet (gRbrgGlobals.
                                                           RbrgGlbMib.
                                                           FsrbridgeNicknameTable,
                                                           (tRBElem *) &
                                                           NicknameEntry);
            if (pNicknameEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_NICKNAME_ENTRY_NOT_FOUND);
                RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                           "Nickname entry not found\r\n"));
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestAllFsrbridgePortTable
 Input       :  pu4ErrorCode
                pRbrgSetFsRBridgeBasePortEntry
                pRbrgIsSetFsRBridgeBasePortEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgePortTable (UINT4 *pu4ErrorCode,
                               tRbrgFsRBridgeBasePortEntry *
                               pRbrgSetFsRBridgeBasePortEntry,
                               tRbrgIsSetFsRBridgeBasePortEntry *
                               pRbrgIsSetFsRBridgeBasePortEntry)
{
    tRbrgMibFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgMibFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRbrgFsRBridgeBasePortEntry RbrgPortEntry;
    tRbrgFsRBridgeBasePortEntry *pRbrgPortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NickNameCount = 0;
    UINT2               u2LocalPortId = 0;

    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    MEMSET (&RbrgPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    u4IfIndex = (UINT4)
        pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;
    RbrgPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId);
    RbrgGlobalEntry.u4FsrbridgeContextId = u4ContextId;
    pRbrgGlobalEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                                  (tRBElem *) & RbrgGlobalEntry);

    if ((pRbrgGlobalEntry == NULL) ||
        ((pRbrgGlobalEntry != NULL) &&
         (pRbrgGlobalEntry->i4FsrbridgeSystemControl == RBRG_SHUT)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }
    if (pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex >
        MAX_RBRG_PORTS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_PORT_OUT_OF_RANGE);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Index out of range \r\n"));
        return OSIX_FAILURE;
    }

    /* Check whether the node is already present */
    pRbrgFsRBridgeBasePortEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) pRbrgSetFsRBridgeBasePortEntry);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgePortTable: Entry doesn't exist\r\n"));
        CLI_SET_ERR (RBRG_CLI_PORT_ENTRY_NOT_FOUND);
        return OSIX_FAILURE;
    }

    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDisable != TRUE) &&
            (pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDisable != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for PortDisable\r\n"));
            return OSIX_FAILURE;

        }
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortTrunkPort != TRUE) &&
            (pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortTrunkPort != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for TrunkPort\r\n"));
            return OSIX_FAILURE;

        }
        RbrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;
        pRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                       (tRBElem *) & RbrgPortEntry);
        if ((pRbrgPortEntry != NULL) &&
            (pRbrgPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE))
        {
            if (pRbrgPortEntry->u4ReferenceCount != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_DEL_FIB_ENTRIES);
                RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                           "Static FDB and FIB entries should be deleted "
                           "before changing this property\r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortAccessPort != TRUE) &&
            (pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortAccessPort != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for AccessPort\r\n"));
            return OSIX_FAILURE;

        }
        RbrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;
        pRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                       (tRBElem *) & RbrgPortEntry);
        if ((pRbrgPortEntry != NULL) &&
            (pRbrgPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE))
        {
            if (pRbrgPortEntry->u4ReferenceCount != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_DEL_FIB_ENTRIES);
                RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                           "Static FDB and FIB entries should be deleted "
                           "before changing this property\r\n"));
                return OSIX_FAILURE;
            }
        }

        RBTreeCount (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                     &u4NickNameCount);
        if ((u4NickNameCount == 0)
            && (pRbrgSetFsRBridgeBasePortEntry->MibObject.
                i4FsrbridgePortAccessPort == TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RBRG_CLI_NICKNAME_NOT_SET);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Atleast One NickName should be created "
                       "before enabling access port\r\n"));
            return OSIX_FAILURE;
        }

    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDisableLearning != TRUE) &&
            (pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDisableLearning != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for Mac-learning status\r\n"));
            return OSIX_FAILURE;

        }

    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan != OSIX_FALSE)
    {
        /*Fill your check condition */
        RbrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;
        pRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                       (tRBElem *) & RbrgPortEntry);
        if ((pRbrgPortEntry != NULL) && (pRbrgPortEntry->u4ReferenceCount != 0))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RBRG_CLI_STATIC_FIB_FDB_ENTRIES_EXIST);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Static FDB and FIB entries should be deleted "
                       "before changing this property\r\n"));
            return OSIX_FAILURE;
        }

        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDesigVlan > RBRG_MAX_VLAN) ||
            (pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortDesigVlan < RBRG_DEFAULT_DESIGVLAN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_DESIG_VLAN_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for Designated VLAN\r\n"));
            return OSIX_FAILURE;

        }

    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRBridgeBasePortEntry->MibObject.
             i4FsrbridgePortClearCounters != TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for Clear counters \r\n"));
            return OSIX_FAILURE;

        }

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestAllFsrbridgeUniFdbTable
 Input       :  pu4ErrorCode
                pRbrgSetFsRbridgeUniFdbEntry
                pRbrgIsSetFsRbridgeUniFdbEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgeUniFdbTable (UINT4 *pu4ErrorCode,
                                 tRbrgFsRbridgeUniFdbEntry *
                                 pRbrgSetFsRbridgeUniFdbEntry,
                                 tRbrgIsSetFsRbridgeUniFdbEntry *
                                 pRbrgIsSetFsRbridgeUniFdbEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tRBrgFibEntry       FibEntry;
    tRBrgFibEntry      *pFibEntry = NULL;
    tRbrgMibFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgMibFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRBrgFdbEntry      *pFdbEntry = NULL;
    tRBrgFdbEntry       FdbEntry;

    MEMSET (&FdbEntry, 0, sizeof (tRBrgFdbEntry));

    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    MEMSET (&FibEntry, 0, sizeof (tRBrgFibEntry));

    RbrgGlobalEntry.u4FsrbridgeContextId =
        pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
    pRbrgGlobalEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                                  (tRBElem *) & RbrgGlobalEntry);
    if (pRbrgGlobalEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   " RBridge is shutdown in this context %d\r\n",
                   RbrgGlobalEntry.u4FsrbridgeContextId));
        return OSIX_FAILURE;
    }

    if (pRbrgGlobalEntry->i4FsrbridgeSystemControl == RBRG_SHUT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }

    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort >
            (INT4) FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
            u4PreAllocatedUnits)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_PORT_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Wrong value for FDB Port\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbNick < RBRG_MIN_NICKNAME) ||
            (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbNick > RBRG_MAX_NICKNAME))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_NICKNAME_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "FDB Nickname is out of range\r\n"));
            return OSIX_FAILURE;
        }

        /* FIB entry should be created before FDB can be created */
        FibEntry.u4FibEgressRBrgNickname =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        FibEntry.u4FibNextHopRBrgPortId =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        FibEntry.u4ContextId =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        FibEntry.u4FibNextHopRBrgNickname = 1;
        pFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) & FibEntry, RbrgUtlFibTblCmpFn);
        if ((pFibEntry == NULL) ||
            ((pFibEntry != NULL) &&
             ((pFibEntry->u4ContextId != FibEntry.u4ContextId) ||
              (pFibEntry->u4FibNextHopRBrgPortId !=
               FibEntry.u4FibNextHopRBrgPortId) ||
              (pFibEntry->u4FibEgressRBrgNickname !=
               FibEntry.u4FibEgressRBrgNickname))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (RBRG_CLI_FDB_FIB_ENTRY_NOT_PRESENT);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "FIB entry is not created\r\n"));
            return OSIX_FAILURE;
        }

    }
    if ((pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence !=
         OSIX_FALSE) &&
        (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick != OSIX_FALSE))
    {
        /*Fill your check condition */
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
            u4FsrbridgeUniFdbConfidence > RBRG_MAX_CONFIDENCE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_CONFIDENCE_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "FDB Confidence is out of range\r\n"));
            return OSIX_FAILURE;
        }

    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbRowStatus != CREATE_AND_GO) &&
            (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbRowStatus != DESTROY) &&
            (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbRowStatus != ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_INVALID_ROWSTATUS);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "wrong value for row status\r\n"));
            return OSIX_FAILURE;
        }
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
            i4FsrbridgeUniFdbRowStatus == DESTROY)
        {
            MEMSET (&FdbEntry, 0, sizeof (tRBrgFdbEntry));
            FdbEntry.u4ContextId =
                pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
            FdbEntry.u4RBrgFdbId =
                pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
            MEMCPY (FdbEntry.RBrgFdbMac,
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                    MAC_ADDR_LEN);
            pFdbEntry = (tRBrgFdbEntry *) RBTreeGet
                (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                 (tRBElem *) & FdbEntry);
            if (pFdbEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_FDB_ENTRY_NOT_FOUND);
                RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                           " FDB entry not found\r\n"));
                return OSIX_FAILURE;
            }
        }

    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestAllFsrbridgeUniFibTable
 Input       :  pu4ErrorCode
                pRbrgSetFsRbridgeUniFibEntry
                pRbrgIsSetFsRbridgeUniFibEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgeUniFibTable (UINT4 *pu4ErrorCode,
                                 tRbrgFsRbridgeUniFibEntry *
                                 pRbrgSetFsRbridgeUniFibEntry,
                                 tRbrgIsSetFsRbridgeUniFibEntry *
                                 pRbrgIsSetFsRbridgeUniFibEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tRbrgMibFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRBrgFibEntry       RbrgFibEntry;
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;
    tRbrgMibFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRBrgFibEntry      *pFibEntry = NULL;

    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    RbrgGlobalEntry.u4FsrbridgeContextId =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;
    pRbrgGlobalEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                                  (tRBElem *) & RbrgGlobalEntry);

    if (pRbrgGlobalEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }

    if (pRbrgGlobalEntry->i4FsrbridgeSystemControl == RBRG_SHUT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.
             u4FsrbridgeFibMtuDesired < RBRG_MIN_MTU) ||
            (pRbrgSetFsRbridgeUniFibEntry->MibObject.
             u4FsrbridgeFibMtuDesired > RBRG_MAX_MTU))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_MTU_OUT_OF_RANGE);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "MTU out of range\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.
             u4FsrbridgeFibHopCount < RBRG_MIN_HOPCOUNT) ||
            (pRbrgSetFsRbridgeUniFibEntry->MibObject.
             u4FsrbridgeFibHopCount > RBRG_MAX_HOPCOUNT))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_HOP_COUNT_EXCEEDED);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "Hop cont out of range\r\n"));
            return OSIX_FAILURE;
        }
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.
             i4FsrbridgeFibRowstatus != CREATE_AND_GO) &&
            (pRbrgSetFsRbridgeUniFibEntry->MibObject.
             i4FsrbridgeFibRowstatus != DESTROY) &&
            (pRbrgSetFsRbridgeUniFibEntry->MibObject.
             i4FsrbridgeFibRowstatus != ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (RBRG_CLI_INVALID_ROWSTATUS);
            RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                       "wrong value for row status\r\n"));
            return OSIX_FAILURE;
        }
        /* The FIB port should be trunk port */
        if (pRbrgSetFsRbridgeUniFibEntry->MibObject.
            i4FsrbridgeFibRowstatus == CREATE_AND_GO)
        {
            FsPortEntry.MibObject.i4FsrbridgePortIfIndex =
                pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
            pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
                RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                           (tRBElem *) & FsPortEntry);
            if ((pFsPortEntry == NULL) ||
                ((pFsPortEntry != NULL) &&
                 (pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort != TRUE)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_FIB_NO_TRUNK_PORT);
                RBRG_TRC ((ALL_FAILURE_TRC, "Port %d is not a trunk port\r\n",
                           pRbrgSetFsRbridgeUniFibEntry->MibObject.
                           i4FsrbridgeFibPort));
                return OSIX_FAILURE;
            }

        }

        /* Entry should be present for deletion */
        if (pRbrgSetFsRbridgeUniFibEntry->MibObject.
            i4FsrbridgeFibRowstatus == DESTROY)
        {
            RbrgFibEntry.u4ContextId =
                pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;
            RbrgFibEntry.u4FibEgressRBrgNickname =
                pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
            RbrgFibEntry.u4FibNextHopRBrgPortId =
                pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
            RbrgFibEntry.u4FibNextHopRBrgNickname =
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibNextHopRBridge;
            pFibEntry = (tRBrgFibEntry *)
                RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) & RbrgFibEntry);
            if (pFibEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_FIB_ENTRY_NOT_FOUND);
                RBRG_TRC (((ALL_FAILURE_TRC), "Entry not present\r\n"));
                return OSIX_FAILURE;
            }

            /* If static MAC entries on this nickname are present,
             * do not allow deletion of FIB entry */

            if (pFibEntry->RBrgFibFdbList.u4_Count != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_FIB_DELETE_STATIC_FDB_ENTRIES_EXIST);
                RBRG_TRC ((ALL_FAILURE_TRC, "This entry can be deleted only "
                           "after deleting the corresponding static FDB "
                           "entries\r\n"));
                return OSIX_FAILURE;
            }
        }

    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestAllFsrbridgeMultiFibTable
 Input       :  pu4ErrorCode
                pRbrgSetFsrbridgeMultiFibEntry
                pRbrgIsSetFsrbridgeMultiFibEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgTestAllFsrbridgeMultiFibTable (UINT4 *pu4ErrorCode,
                                   tRbrgFsrbridgeMultiFibEntry *
                                   pRbrgSetFsrbridgeMultiFibEntry,
                                   tRbrgIsSetFsrbridgeMultiFibEntry *
                                   pRbrgIsSetFsrbridgeMultiFibEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{

    tRbrgMibFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgMibFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    RbrgGlobalEntry.u4FsrbridgeContextId =
        pRbrgSetFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId;
    pRbrgGlobalEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                                  (tRBElem *) & RbrgGlobalEntry);

    if (pRbrgGlobalEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }

    if (pRbrgGlobalEntry->i4FsrbridgeSystemControl == RBRG_SHUT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RBRG_CLI_MODULE_SHUT);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), " RBridge is shutdown \r\n"));
        return OSIX_FAILURE;
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus !=
        OSIX_FALSE)
    {
        if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibRowStatus == DESTROY)
        {
            pRbrgFsrbridgeMultiFibEntry =
                RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                           (tRBElem *) pRbrgSetFsrbridgeMultiFibEntry);

            if (pRbrgFsrbridgeMultiFibEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (RBRG_CLI_MULTIFIB_ENTRY_NOT_FOUND);
                RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                           "MultiFib entry is not found \r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgTestFsrbridgeGlobalTrace
 Input       :  pu4ErrorCode
                u4FsrbridgeGlobalTrace
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgTestFsrbridgeGlobalTrace (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeGlobalTrace)
{
    if (u4FsrbridgeGlobalTrace > (RBRG_ALL_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (RBRG_CLI_INVALID_TRACE);
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Wrong value for trace\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
