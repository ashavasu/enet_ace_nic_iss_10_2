/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgsrc.c,v 1.9 2014/02/03 12:25:52 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*              for Rbrg module 
*********************************************************************/

#include "rbrginc.h"
#include "rbrgsrcdefn.h"
#include "rbrgsrc.h"

/****************************************************************************
 Function    : RbrgShowRunningConfig
 Description : This function displays the current configuration
                of RBRG module
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    RbrgShowRunningConfigScalar (CliHandle, u4Module);
    CliPrintf (CliHandle, "!\r\n");
    RbrgShowRunningConfigFsrbridgeGlobalTable (CliHandle, u4Module);
    RbrgShowRunningConfigFsrbridgeNicknameTable (CliHandle, u4Module);
    RbrgShowRunningConfigFsrbridgeUniFdbTable (CliHandle, u4Module);
    RbrgShowRunningConfigFsrbridgeUniFibTable (CliHandle, u4Module);
    RbrgShowRunningConfigFsrbridgeMultiFibTable (CliHandle, u4Module);
    RbrgShowRunningConfigFsrbridgePortTable (CliHandle, u4Module);

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigScalar
 Description : This function displays the current configuration
                of the RBRG scalars
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
RbrgShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module)
{
    UINT4               u4FsrbridgeGlobalTrace = 0;
    nmhGetFsrbridgeGlobalTrace (&u4FsrbridgeGlobalTrace);
    if (u4FsrbridgeGlobalTrace != RBRG_DEF_FSRBRIDGEGLOBALTRACE)
    {
        CliPrintf (CliHandle,
                   "debug rbridge all \r\nmgmt \r\nresource \r\nfailure \r\ncontrol-plane \r\ncritical \r\n<init-shut FsrbridgeGlobalTrace CLI_RBRG_DEBUG_INIT_SHUT 6 -1 critical \r\n");
    }
    nmhGetFsrbridgeGlobalTrace (&u4FsrbridgeGlobalTrace);
    if (u4FsrbridgeGlobalTrace != RBRG_DEF_FSRBRIDGEGLOBALTRACE)
    {
        CliPrintf (CliHandle,
                   "no debug rbridge all \r\nmgmt \r\nresource \r\nfailure \r\ncontrol-plane \r\ncritical \r\ninit-shut \r\n");
    }
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgeGlobalTable
 Description : This function displays the current configuration
                of FsrbridgeGlobalTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgeGlobalTable (tCliHandle CliHandle, UINT4 u4Module)
{
    tRbrgFsrbridgeGlobalEntry RbrgFsrbridgeGlobalEntry;
    UINT4               u4NextFsrbridgeContextId = 0;
    UINT4               u4SysMode = 0;
    INT4                i4RetVal = 0;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN] = { 0 };

    MEMSET (&RbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    i4RetVal = nmhGetFirstIndexFsrbridgeGlobalTable (&u4NextFsrbridgeContextId);

    while (SNMP_SUCCESS != i4RetVal)
    {
        MEMSET (&RbrgFsrbridgeGlobalEntry, 0,
                sizeof (tRbrgFsrbridgeGlobalEntry));
        RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeContextId =
            u4NextFsrbridgeContextId;
        if (RbrgGetAllFsrbridgeGlobalTable (&RbrgFsrbridgeGlobalEntry) !=
            OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        u4SysMode = VcmGetSystemModeExt (RBRG_PROTOCOL_ID);
        if (u4SysMode == VCM_MI_MODE)
        {
            MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
            VcmGetAliasName
                (RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeContextId,
                 au1ContextName);
            CliPrintf (CliHandle, "\rswitch  %s \r\n\r\n", au1ContextName);
        }

        if (u4SysMode == VCM_MI_MODE)
        {
            CliPrintf (CliHandle, "! \r\n");
        }

        MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
        if (RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeUniMultipathEnable
            != RBRG_DEF_FSRBRIDGEUNIMULTIPATHENABLE)
        {
            CliPrintf (CliHandle,
                       "set rbridge unicast multipath %d %d \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeUniMultipathEnable,
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeUniMultipathEnable);

        }
        if (RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeMultiMultipathEnable
            != RBRG_DEF_FSRBRIDGEMULTIMULTIPATHENABLE)
        {
            CliPrintf (CliHandle,
                       "set rbridge multicast multipath %d %d \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeMultiMultipathEnable,
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeMultiMultipathEnable);

        }
        if (RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeNicknameNumber
            != RBRG_DEF_FSRBRIDGENICKNAMENUMBER)
        {
            CliPrintf (CliHandle,
                       "set rbridge nickname limit %d \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       u4FsrbridgeNicknameNumber);

        }
        if (RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeSystemControl
            != RBRG_DEF_FSRBRIDGESYSTEMCONTROL)
        {
            CliPrintf (CliHandle,
                       "no shutdown rbridbe %d \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeSystemControl);

        }
        if (RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeSystemControl
            != RBRG_DEF_FSRBRIDGESYSTEMCONTROL)
        {
            CliPrintf (CliHandle,
                       "shutdown rbridge %d \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeSystemControl);

        }
        if (RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeModuleStatus
            != RBRG_DEF_FSRBRIDGEMODULESTATUS)
        {
            CliPrintf (CliHandle,
                       "set rbridge enable \r\n",
                       RbrgFsrbridgeGlobalEntry.MibObject.
                       i4FsrbridgeModuleStatus);

        }

        i4RetVal =
            nmhGetNextIndexFsrbridgeGlobalTable
            (RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeContextId,
             &u4NextFsrbridgeContextId);

    }
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgeNicknameTable
 Description : This function displays the current configuration
                of FsrbridgeNicknameTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgeNicknameTable (tCliHandle CliHandle,
                                             UINT4 u4Module)
{
    tRbrgFsrbridgeNicknameEntry NicknameEntry;
    UINT4               u4ContextId = 0;
    INT4                i4Nickname = 0;
    UINT4               u4NextContext = 0;
    INT4                i4NextNickname = 0;
    INT4                i4RetVal = 0;

    MEMSET (&NicknameEntry, 0, sizeof (tRbrgFsrbridgeNicknameEntry));
    if (SNMP_SUCCESS != nmhGetFirstIndexFsrbridgeNicknameTable
        (&u4NextContext, &i4NextNickname))
    {
        do
        {
            u4ContextId = u4NextContext;
            i4Nickname = i4NextNickname;
            NicknameEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
            NicknameEntry.MibObject.i4FsrbridgeNicknameName = i4Nickname;
            if (RbrgGetAllFsrbridgeNicknameTable (&NicknameEntry) !=
                OSIX_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            if (NicknameEntry.MibObject.u4FsrbridgeNicknamePriority !=
                RBRG_DEFAULT_PRIORITY)
            {
                CliPrintf (CliHandle, "rbridge nickname %d priority %d\r\n",
                           NicknameEntry.MibObject.i4FsrbridgeNicknameName,
                           NicknameEntry.MibObject.u4FsrbridgeNicknamePriority);
            }
            if (NicknameEntry.MibObject.u4FsrbridgeNicknameDtrPriority !=
                RBRG_DEFAULT_DTRPRIORITY)
            {
                CliPrintf (CliHandle, "rbridge nickname %d Dtr-priority %d\r\n",
                           NicknameEntry.MibObject.i4FsrbridgeNicknameName,
                           NicknameEntry.MibObject.
                           u4FsrbridgeNicknameDtrPriority);
            }
            i4RetVal = nmhGetNextIndexFsrbridgeNicknameTable (u4ContextId,
                                                              &u4NextContext,
                                                              i4Nickname,
                                                              &i4NextNickname);

        }
        while (i4RetVal != SNMP_FAILURE);
    }

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgeUniFdbTable
 Description : This function displays the current configuration
                of FsrbridgeUniFdbTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgeUniFdbTable (tCliHandle CliHandle, UINT4 u4Module)
{
    tRbrgFsRbridgeUniFdbEntry FdbEntry;
    tMacAddr            Mac;
    tMacAddr            NextMac;
    UINT4               u4ContextId = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextFdbId = 0;
    INT4                i4RetVal = 0;
    UINT1               au1Mac[RBRG_MAC_STR_LEN] = { 0 };
    UINT1               au1PortString[RBRG_MAX_IFNAME_LEN] = { 0 };

    MEMSET (&Mac, 0, sizeof (tMacAddr));
    MEMSET (&NextMac, 0, sizeof (tMacAddr));

    if (SNMP_FAILURE != nmhGetFirstIndexFsrbridgeUniFdbTable (&u4NextContextId,
                                                              &u4NextFdbId,
                                                              &NextMac))
    {
        do
        {
            u4ContextId = u4NextContextId;
            u4FdbId = u4NextFdbId;
            MEMCPY (Mac, NextMac, sizeof (tMacAddr));

            FdbEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
            FdbEntry.MibObject.u4FsrbridgeFdbId = u4FdbId;
            MEMCPY (FdbEntry.MibObject.FsrbridgeUniFdbAddr, Mac,
                    sizeof (tMacAddr));

            if (RbrgGetAllFsrbridgeUniFdbTable (&FdbEntry) != OSIX_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            PrintMacAddress ((UINT1 *) FdbEntry.MibObject.FsrbridgeUniFdbAddr,
                             au1Mac);
            CfaCliConfGetIfName (FdbEntry.MibObject.i4FsrbridgeUniFdbPort,
                                 (INT1 *) au1PortString);
            if (FdbEntry.MibObject.i4FsrbridgeUniFdbNick != 0)
            {
                CliPrintf (CliHandle, "rbridge fdb %d mac %s nickname %d "
                           "port %s\r\n",
                           FdbEntry.MibObject.u4FsrbridgeFdbId,
                           au1Mac, FdbEntry.MibObject.i4FsrbridgeUniFdbNick,
                           au1PortString);

            }
            if (FdbEntry.MibObject.u4FsrbridgeUniFdbConfidence !=
                RBRG_DEFAULT_CONFIDENCE)
            {
                CliPrintf (CliHandle, "rbridge fdb %d mac %s nickname %d "
                           "port %s confidence %d\r\n",
                           FdbEntry.MibObject.u4FsrbridgeFdbId,
                           au1Mac, FdbEntry.MibObject.i4FsrbridgeUniFdbNick,
                           au1PortString,
                           FdbEntry.MibObject.u4FsrbridgeUniFdbConfidence);
            }

            i4RetVal = nmhGetNextIndexFsrbridgeUniFdbTable
                (u4ContextId, &u4NextContextId, u4FdbId, &u4NextFdbId,
                 Mac, &NextMac);

        }
        while (i4RetVal != SNMP_FAILURE);
    }

    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgeUniFibTable
 Description : This function displays the current configuration
                of FsrbridgeUniFibTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgeUniFibTable (tCliHandle CliHandle, UINT4 u4Module)
{
    tPortList          *pPortList = NULL;
    tRbrgFsRbridgeUniFibEntry FibEntry;
    tSNMP_OCTET_STRING_TYPE Ports;
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    INT4                i4Nickname = 0;
    INT4                i4Port = 0;
    INT4                i4NextHopNick = 0;
    INT4                i4NextNickname = 0;
    INT4                i4NextPort = 0;
    INT4                i4NextNextHopNick = 0;
    INT4                i4RetVal = 0;
    UINT1               au1PortString[RBRG_MAX_IFNAME_LEN] = { 0 };

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pPortList == NULL)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgShowRunningConfigFsrbridgeUniFibTable: Error in Allocating memory for bitlist\r\n"));
        return CLI_FAILURE;
    }
    MEMSET (*pPortList, 0, sizeof (tPortList));

    MEMSET (&FibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&Ports, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    Ports.pu1_OctetList = *pPortList;
    Ports.i4_Length = sizeof (tPortList);

    if (SNMP_FAILURE !=
        nmhGetFirstIndexFsrbridgeUniFibTable (&u4NextContextId, &i4NextNickname,
                                              &i4NextPort, &i4NextNextHopNick))
    {
        do
        {
            u4ContextId = u4NextContextId;
            i4Nickname = i4NextNickname;
            i4Port = i4NextPort;
            i4NextHopNick = i4NextNextHopNick;

            FibEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
            FibEntry.MibObject.i4FsrbridgeFibNickname = i4Nickname;
            FibEntry.MibObject.i4FsrbridgeFibPort = i4Port;
            FibEntry.MibObject.i4FsrbridgeFibNextHopRBridge = i4NextNextHopNick;
            if (RbrgGetAllFsrbridgeUniFibTable (&FibEntry) != OSIX_SUCCESS)
            {
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                return CLI_SUCCESS;
            }

            CfaCliConfGetIfName (FibEntry.MibObject.i4FsrbridgeFibPort,
                                 (INT1 *) au1PortString);

            if (FibEntry.MibObject.u4FsrbridgeFibMtuDesired != RBRG_DEFAULT_MTU)
            {
                CliPrintf (CliHandle, "rbridge fib nickname %d port %s "
                           "next-hop-nickname %d mac %s mtu %d\r\n",
                           FibEntry.MibObject.i4FsrbridgeFibNickname,
                           au1PortString,
                           FibEntry.MibObject.i4FsrbridgeFibNextHopRBridge,
                           FibEntry.MibObject.au1FsrbridgeFibMacAddress,
                           FibEntry.MibObject.u4FsrbridgeFibMtuDesired);
            }

            if (FibEntry.MibObject.u4FsrbridgeFibHopCount !=
                RBRG_DEFAULT_HOPCOUNT)
            {
                CliPrintf (CliHandle, "rbridge fib nickname %d port %s"
                           "next-hop-nickname %d mac %s hop-count %d\r\n",
                           FibEntry.MibObject.i4FsrbridgeFibNickname,
                           au1PortString,
                           FibEntry.MibObject.i4FsrbridgeFibNextHopRBridge,
                           FibEntry.MibObject.au1FsrbridgeFibMacAddress,
                           FibEntry.MibObject.u4FsrbridgeFibHopCount);
            }

            i4RetVal =
                nmhGetNextIndexFsrbridgeUniFibTable (u4ContextId,
                                                     &u4NextContextId,
                                                     i4Nickname,
                                                     &i4NextNickname,
                                                     i4Port,
                                                     &i4NextPort,
                                                     i4NextHopNick,
                                                     &i4NextNextHopNick);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
    UNUSED_PARAM (u4Module);
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgeMultiFibTable
 Description : This function displays the current configuration
                of FsrbridgeMultiFibTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgeMultiFibTable (tCliHandle CliHandle,
                                             UINT4 u4Module)
{
    tPortList          *pPortList = NULL;
    tRbrgFsrbridgeMultiFibEntry MultiFibEntry;
    tSNMP_OCTET_STRING_TYPE Ports;
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextMultiFibNick = 0;
    UINT4               i4MultiFibNick = 0;
    INT4                i4RetVal = 0;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pPortList == NULL)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "pPortList: Error in Allocating memory for bitlist\r\n"));
        return CLI_FAILURE;
    }
    MEMSET (*pPortList, 0, sizeof (tPortList));
    MEMSET (&MultiFibEntry, 0, sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (&Ports, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    Ports.pu1_OctetList = *pPortList;
    Ports.i4_Length = sizeof (tPortList);

    if (SNMP_FAILURE !=
        nmhGetFirstIndexFsrbridgeMultiFibTable (&u4NextContextId,
                                                &i4NextMultiFibNick))
    {
        do
        {
            u4ContextId = u4NextContextId;
            i4MultiFibNick = i4NextMultiFibNick;

            if (OSIX_FAILURE ==
                RbrgGetAllFsrbridgeMultiFibTable (&MultiFibEntry))
            {
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                return CLI_SUCCESS;
            }

            MEMCPY (*pPortList,
                    MultiFibEntry.MibObject.au1FsrbridgeMultiFibPorts,
                    MultiFibEntry.MibObject.i4FsrbridgeMultiFibPortsLen);
            if (FsUtilBitListIsAllZeros (Ports.pu1_OctetList, Ports.i4_Length)
                == OSIX_FALSE)
            {
                CliPrintf (CliHandle, "rbridge multi-fib nickname %d ",
                           i4MultiFibNick);
                CliConfOctetToIfName (CliHandle, " ports ", NULL, &Ports,
                                      &u4PagingStatus);
            }

            i4RetVal = nmhGetNextIndexFsrbridgeMultiFibTable (u4ContextId,
                                                              &u4NextContextId,
                                                              i4MultiFibNick,
                                                              &i4NextMultiFibNick);
        }
        while (i4RetVal != SNMP_FAILURE);
    }

    UNUSED_PARAM (u4Module);
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : RbrgShowRunningConfigFsrbridgePortTable
 Description : This function displays the current configuration
                of FsrbridgePortTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgShowRunningConfigFsrbridgePortTable (tCliHandle CliHandle, UINT4 u4Module)
{
    tRbrgFsRBridgeBasePortEntry PortEntry;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;
    UINT1               au1PortString[RBRG_MAX_IFNAME_LEN] = { 0 };

    MEMSET (&PortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    if (SNMP_FAILURE != nmhGetFirstIndexFsrbridgePortTable (&i4NextIfIndex))
    {
        do
        {
            i4IfIndex = i4NextIfIndex;
            CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1PortString);
            MEMSET (au1PortString, 0, RBRG_MAX_IFNAME_LEN);
            PortEntry.MibObject.i4FsrbridgePortIfIndex = i4IfIndex;
            if (OSIX_FAILURE == RbrgGetAllFsrbridgePortTable (&PortEntry))
            {
                return CLI_SUCCESS;
            }
            if ((PortEntry.MibObject.i4FsrbridgePortDisable != FALSE) ||
                (PortEntry.MibObject.i4FsrbridgePortTrunkPort != FALSE) ||
                (PortEntry.MibObject.i4FsrbridgePortAccessPort != FALSE) ||
                (PortEntry.MibObject.i4FsrbridgePortDisableLearning != FALSE) ||
                (PortEntry.MibObject.i4FsrbridgePortDesigVlan !=
                 RBRG_DEFAULT_DESIGVLAN))
            {
                CliPrintf (CliHandle, "interface %s\r\n", au1PortString);
            }

            if (PortEntry.MibObject.i4FsrbridgePortDisable != FALSE)
            {
                CliPrintf (CliHandle, "no rbridge port disable\r\n");
            }

            if (PortEntry.MibObject.i4FsrbridgePortTrunkPort != FALSE)
            {
                CliPrintf (CliHandle, "set rbridge trunk port enable\r\n");
            }

            if (PortEntry.MibObject.i4FsrbridgePortAccessPort != FALSE)
            {
                CliPrintf (CliHandle, "set rbridge access port enable\r\n");
            }

            if (PortEntry.MibObject.i4FsrbridgePortDisableLearning != FALSE)
            {
                CliPrintf (CliHandle, "set rbridge mac-learning enable\r\n");
            }

            if (PortEntry.MibObject.i4FsrbridgePortDesigVlan !=
                RBRG_DEFAULT_DESIGVLAN)
            {
                CliPrintf (CliHandle, "set designated-vlan %d\r\n",
                           PortEntry.MibObject.i4FsrbridgePortDesigVlan);
            }

            i4RetVal = nmhGetNextIndexFsrbridgePortTable (i4IfIndex,
                                                          &i4NextIfIndex);
            CliPrintf (CliHandle, "!\r\n");
        }
        while (i4RetVal != SNMP_FAILURE);
    }
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}
