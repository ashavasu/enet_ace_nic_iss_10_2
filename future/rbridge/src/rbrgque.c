/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rbrgque.c,v 1.4 2015/07/10 10:25:20 siva Exp $
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "rbrginc.h"

/****************************************************************************
*                                                                           *
* Function     : RbrgQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgQueProcessMsgs ()
{
    tRbrgQMsg          *pRbrgQueMsg = NULL;

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgQueProcessMsgs\n"));

    while (OsixQueRecv (gRbrgGlobals.rbrgQueId,
                        (UINT1 *) &pRbrgQueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        switch (pRbrgQueMsg->u4MsgType)
        {
            case RBRG_CREATE_CONTEXT:
                RbrgUtlCreateContext (pRbrgQueMsg->u4ContextId);
                break;
            case RBRG_DELETE_CONTEXT:
                RbrgUtlDelNicknamesOnContext (pRbrgQueMsg->u4ContextId);
                RbrgUtlDeleteContext (pRbrgQueMsg->u4ContextId);
                break;
            case RBRG_CREATE_PORT:
            case RBRG_MAP_PORT:    /*Intentional fall through (for MI case) */
                RbrgUtlCreatePort (pRbrgQueMsg->u4PortId, RBRG_CREATE_ENTRY);
                break;
            case RBRG_DELETE_PORT:
            case RBRG_UNMAP_PORT:    /*Intentional fall through (for MI case) */
                RbrgUtlDelFdbEntriesOnPort (pRbrgQueMsg->u4PortId,
                                            RBRG_DELETE_ENTRY);
                RbrgUtlDelFibEntriesOnPort (pRbrgQueMsg->u4PortId,
                                            RBRG_DELETE_ENTRY);
                RbrgUtlDeletePort (pRbrgQueMsg->u4PortId, RBRG_DELETE_ENTRY);
                break;
            case RBRG_ADD_LAG_PORT:
                RbrgUtlAddPortToLAG (pRbrgQueMsg->u4ContextId,
                                     pRbrgQueMsg->u4PortId,
                                     pRbrgQueMsg->u4PoIndex);
                break;
            case RBRG_REM_LAG_PORT:
                RbrgUtlRemPortFromLAG (pRbrgQueMsg->u4PortId,
                                       pRbrgQueMsg->u4PoIndex);
                break;
#ifdef MBSM_WANTED
            case RBRG_MBSM_MSG_CARD_INSERT:
                RbrgMbsmProcessMsg (pRbrgQueMsg->pMbsmProtoMsg);
                MemReleaseMemBlock (RBRG_MBSM_POOLID,
                                    (UINT1 *) (pRbrgQueMsg->pMbsmProtoMsg));
                break;
#endif
            default:
                RBRG_TRC ((ALL_FAILURE_TRC, "MsgType not supported\r\n"));
                return;
        }
		MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pRbrgQueMsg);
    }
    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC:Exit RbrgQueProcessMsgs\n"));

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : RbrgQueEnqMsg
 *                                                                          
 *    DESCRIPTION      : Function to post  message to RBRG task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
RbrgQueEnqMsg (tRbrgQMsg * pMsg)
{
    UINT4               u4Event = pMsg->u4MsgType;

    if (OsixQueSend (gRbrgGlobals.rbrgQueId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pMsg);

        RBRG_TRC (((ALL_FAILURE_TRC),
                   "RbrgQueEnqMsg: Osix Queue send FAILED for Msg type %d \r\n",
                   u4Event));
        return OSIX_FAILURE;
    }

    OsixEvtSend (gRbrgGlobals.rbrgTaskId, RBRG_QMSG_EVENT);

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgque.c                      */
/*-----------------------------------------------------------------------*/
