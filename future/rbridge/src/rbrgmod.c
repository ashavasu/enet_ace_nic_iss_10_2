/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgmod.c,v 1.2 2012/01/27 13:06:25 siva Exp $
 *
 * Description: This file contains module start and  module shutdown
 *              routines.
 *********************************************************************/
#include "rbrginc.h"
#include "rbrgsrc.h"

/****************************************************************************
 *                                                                         
 *    FUNCTION NAME    : RbrgModuleInit
 *                                                                          
 *    DESCRIPTION      :  It creates the existing context and ports in the 
 *                        system in RBridge module.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
RbrgModuleInit (VOID)
{
    UINT4               u4ContextId = RBRG_DEFAULT_CONTEXT;

    /* Create first context */
    RbrgPortVcmGetFirstVcId (&u4ContextId);
    if (RbrgUtlCreateContext (u4ContextId) == OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleInit: ContextId %u creation FAILED\r\n",
                   u4ContextId));
        return (OSIX_FAILURE);
    }

    /* Get all the ports under that context and add in RBRG database */
    RbrgUtlCreateAllPorts (u4ContextId);

    return (OSIX_SUCCESS);

}

/****************************************************************************
 *                                                                         
 *    FUNCTION NAME    : RbrgModuleStart
 *                                                                          
 *    DESCRIPTION      :  It creates the context and ports in the mapped to 
 *                        this context
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
RbrgModuleStart (UINT4 u4ContextId)
{

    if (RbrgUtlCreateContext (u4ContextId) == OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleStart: ContextId %u creation FAILED\r\n",
                   u4ContextId));
        return (OSIX_FAILURE);
    }

    /* Get all the ports under that context and add in RBRG database */
    RbrgUtlCreateAllPorts (u4ContextId);

    return (OSIX_SUCCESS);

}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgModuleShut
 *
 *    DESCRIPTION      : This function deletes the context entry and all 
 *                       ports associated with it. It also deletes the 
 *                       FDB and FIB entries on this this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
RbrgModuleShut (UINT4 u4ContextId)
{

    /* Delete the FDB and FIB entries on this context */
    if (RbrgUtlDelFdbEntriesOnContext (u4ContextId, RBRG_DELETE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleShut: RbrgUtlDelFdbEntriesOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }
    if (RbrgUtlDelFibEntriesOnContext (u4ContextId, RBRG_DELETE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleShut: RbrgUtlDelFibEntriesOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }
    if (RbrgUtlDelPortsOnContext (u4ContextId, RBRG_DELETE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleShut: RbrgUtlDelPortsOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }

    if (RbrgUtlDelNicknamesOnContext (u4ContextId) == OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleShut: RbrgUtlDelNicknamesOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }

    /* Delete the context entry */
    if (RbrgUtlDeleteContext (u4ContextId) == OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleShut: ContextId %u deletion FAILED\r\n",
                   u4ContextId));
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : RbrgModuleEnable
 *                                                                          
 *    DESCRIPTION      : This function is called to enable RBRG module
 *                       All the FIB, FDB and port entries on this context 
 *                       should be created on hardware
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
RbrgModuleEnable (UINT4 u4ContextId)
{
    tRBrgNpEntry        RbrgHwInfo;
    tRbrgFsRBridgeBasePortEntry *pNextRbrgPortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry *pRbrgPortEntry = NULL;
    tRbrgFsrbridgeNicknameEntry *pRbrgNicknameEntry = NULL;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRBrgFibEntry      *pNextRbrgFibEntry = NULL;
    tTMO_DLL_NODE      *pFdbNode = NULL;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRbrgFsrbridgeNicknameEntry RbrgNicknameEntry;
    tRbrgFsRBridgeBasePortEntry RBrgPortEntry;

    UINT1               u1Index = 0;
    UINT1               u1Status = 0;
    tMacAddr            SwitchMac;

    /* Create an access trill port */
    MEMSET (&RbrgNicknameEntry, 0, sizeof (tRbrgFsrbridgeNicknameEntry));

    RbrgNicknameEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    RbrgNicknameEntry.MibObject.i4FsrbridgeNicknameName = 0;
    pRbrgNicknameEntry = (tRbrgFsrbridgeNicknameEntry *)
        RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                       (tRBElem *) & RbrgNicknameEntry, NULL);
    /*Enable General Trill configurations on hardware */
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
    RbrgHwInfo.u4Command = RBRG_TRILL_ENABLE;
    RbrgHwWrProgramEntry (&RbrgHwInfo);

    pNextRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable);
    while (pNextRbrgPortEntry != NULL)
    {
        pRbrgPortEntry = pNextRbrgPortEntry;
        pNextRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                           pNextRbrgPortEntry, NULL);
        if (pRbrgPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE)
        {
            /* Enable Trunk Port */
            MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
            RbrgHwInfo.u4Command = RBRG_ENABLE_TRUNK_PORT;
            RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pRbrgPortEntry->MibObject.i4FsrbridgePortIfIndex;
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "RbrgModuleEnable : Setting Port properties "
                           "for port %d in Hw failed\r\n",
                           RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex));
                return OSIX_FAILURE;
            }
        }
        if ((pRbrgPortEntry->MibObject.i4FsrbridgePortAccessPort ==
             TRUE) &&
            (pRbrgNicknameEntry != NULL) &&
            (pRbrgNicknameEntry->MibObject.u4FsrbridgeContextId == u4ContextId))
        {
            /* Enable Access Port */
            MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
            RbrgHwInfo.u4Command = RBRG_ENABLE_ACCESS_PORT;
            RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pRbrgPortEntry->MibObject.i4FsrbridgePortIfIndex;
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "RbrgModuleEnable : Setting Port properties "
                           "for port %d in Hw failed\r\n",
                           RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex));
                return OSIX_FAILURE;
            }
            /* Create Access Trill Port */
            MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
            RbrgHwInfo.u4Command = RBRG_FIB_CREATE;
            RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pRbrgPortEntry->MibObject.i4FsrbridgePortIfIndex;
            RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU = RBRG_DEFAULT_MTU;
            RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
                RBRG_DEFAULT_HOPCOUNT;
            RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
                pRbrgNicknameEntry->MibObject.i4FsrbridgeNicknameName;
            RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_ACCESS_PORT;
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "RbrgModuleEnable Access Trill port creation failed\r\n"));
                return OSIX_FAILURE;
            }
            pRbrgPortEntry->u4AccessTrillPort = (UINT4)
                RbrgHwInfo.RbrgNpRbrgEntry.i4AccessTrillPort;
        }
    }

    pNextRbrgFibEntry = (tRBrgFibEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable);
    while ((pNextRbrgFibEntry != NULL) &&
           (pNextRbrgFibEntry->u4ContextId == u4ContextId))
    {

        pRbrgFibEntry = pNextRbrgFibEntry;
        pNextRbrgFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) pNextRbrgFibEntry, NULL);

        pRbrgPortEntry = NULL;
        MEMSET (&RBrgPortEntry, 0, sizeof (tRbrgMibFsRBridgeBasePortEntry));
        RBrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgFibEntry->u4FibNextHopRBrgPortId;
        pRbrgPortEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                                    (tRBElem *) & RBrgPortEntry);
        if (pRbrgPortEntry == NULL)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgModuleEnable:FIB Hardware Programming Failed \r\n"));
            return OSIX_FAILURE;
        }

        /* Recreate FIB from Hardware */
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
        RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
            pRbrgFibEntry->u4FibNextHopRBrgPortId;
        RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
            pRbrgFibEntry->u4FibEgressRBrgNickname;
        if ((pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC))
        {
            u1Index = 1;
        }
        else
        {
            u1Index = 0;
        }

        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
            pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;
        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU =
            pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;
        /* Get SwitchMac from CFA  and copy it to egress_mac */
        MEMSET (&SwitchMac, 0, sizeof (tMacAddr));
        RbrgPortCfaGetSysMacAddress (SwitchMac);
        MEMCPY (&RbrgHwInfo.RbrgNpRbrgEntry.egress_mac, &SwitchMac,
                MAC_ADDR_LEN);
        RbrgHwInfo.RbrgNpRbrgEntry.u4EgressVlan =
            pRbrgPortEntry->MibObject.i4FsrbridgePortDesigVlan;
        MEMCPY (&(RbrgHwInfo.RbrgNpRbrgEntry.next_hop_mac),
                &(pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac),
                MAC_ADDR_LEN);
        RbrgHwInfo.u4Command = RBRG_FIB_CREATE;
        RBRG_IS_MODULE_ENABLE (pRbrgFibEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgModuleEnable:FIB Hardware Programming Failed \r\n"));
                return OSIX_FAILURE;
            }
            pRbrgFibEntry->u4TrunkTrillPort =
                RbrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort;
            pRbrgFibEntry->i4L3EgressId =
                RbrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId;
            pRbrgPortEntry->i4L3InterfaceIndex =
                RbrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id;
        }

        /* Program All FDB Assocatied with this FIB */
        TMO_DLL_Scan (&(pRbrgFibEntry->RBrgFibFdbList),
                      pFdbNode, tTMO_DLL_NODE *)
        {
            pRbrgFdbEntry = GET_RBRG_FDB_PTR_FROM_FIB_LST (pFdbNode);
            MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
            RbrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort =
                pRbrgFibEntry->u4TrunkTrillPort;
            MEMCPY (&RbrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                    pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
            RbrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan =
                pRbrgFdbEntry->u4RBrgFdbId;
            RbrgHwInfo.u4Command = RBRG_CREATE_UCAST_DEST;
            RBRG_IS_MODULE_ENABLE (pRbrgFdbEntry->u4ContextId, u1Status);
            if (u1Status == TRUE)
            {
                if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((ALL_FAILURE_TRC, "RbrgModuleEnable: "
                               "FDB RbrgHwWrProgramEntry returned Failure\r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : RbrgModuleDisable 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable RBRG module
 *                       All FIB, FDB and port entries on this context 
 *                       should be deleted from hardware
 *                       
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
RbrgModuleDisable (UINT4 u4ContextId)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRBrgNpEntry        RbrgHwInfo;
    tRbrgFsrbridgeGlobalEntry RbrgGlobalEntry;

    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    RbrgGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pRbrgGlobalEntry = (tRbrgFsrbridgeGlobalEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) & RbrgGlobalEntry);

    /* For Klocwork */
    if (pRbrgGlobalEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleDisable: Global Entry Not Found"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }

    pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus = RBRG_ENABLE;
    /* Delete the FDB and FIB entries on this context */
    if (RbrgUtlDelFdbEntriesOnContext (u4ContextId, RBRG_DISABLE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleDisable: RbrgUtlDelFdbEntriesOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }
    if (RbrgUtlDelFibEntriesOnContext (u4ContextId, RBRG_DISABLE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleDisable: RbrgUtlDelFibEntriesOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }
    if (RbrgUtlDelPortsOnContext (u4ContextId, RBRG_DISABLE_ENTRY) ==
        OSIX_FAILURE)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgModuleDisable: RbrgUtlDelPortsOnContext"
                   " returned FAILURE\r\n", u4ContextId));
        return (OSIX_FAILURE);
    }

    /*Disable General Trill configurations on hardware */
    RbrgHwInfo.u4Command = RBRG_TRILL_DISABLE;
    RbrgHwWrProgramEntry (&RbrgHwInfo);
    pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus = RBRG_DISABLE;
    return OSIX_SUCCESS;
}
