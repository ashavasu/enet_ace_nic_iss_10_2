/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgutlg.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains utility functions used by protocol Rbrg
*********************************************************************/

#include "rbrginc.h"

/****************************************************************************
 Function    :  RbrgUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlCreateRBTree ()
{

    if (RbrgFsrbridgeGlobalTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgFsrbridgeNicknameTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgFsrbridgePortTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgFsrbridgeUniFdbTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgFsrbridgeUniFibTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgFsrbridgeMultiFibTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgUniFdbTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (RbrgUniFibTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeGlobalTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgeGlobalTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgeGlobalTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsrbridgeGlobalEntry,
                       MibObject.FsrbridgeGlobalTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgeGlobalTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeNicknameTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgeNicknameTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgeNicknameTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsrbridgeNicknameEntry,
                       MibObject.FsrbridgeNicknameTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgeNicknameTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgePortTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgePortTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgePortTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsRBridgeBasePortEntry,
                       MibObject.FsrbridgePortTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgePortTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeUniFdbTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgeUniFdbTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgeUniFdbTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsRbridgeUniFdbEntry,
                       MibObject.FsrbridgeUniFdbTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgeUniFdbTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgeUniFdbTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeUniFibTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgeUniFibTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgeUniFibTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsRbridgeUniFibEntry,
                       MibObject.FsrbridgeUniFibTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgeUniFibTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgeUniFibTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeMultiFibTableCreate
 Input       :  None
 Description :  This function creates the FsrbridgeMultiFibTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgFsrbridgeMultiFibTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRbrgFsrbridgeMultiFibEntry,
                       MibObject.FsrbridgeMultiFibTableNode);

    if ((gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsrbridgeMultiFibTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsrbridgeGlobalTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgeGlobalTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgeGlobalTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsrbridgeGlobalEntry *pFsrbridgeGlobalEntry1 =
        (tRbrgFsrbridgeGlobalEntry *) pRBElem1;
    tRbrgFsrbridgeGlobalEntry *pFsrbridgeGlobalEntry2 =
        (tRbrgFsrbridgeGlobalEntry *) pRBElem2;

    if (pFsrbridgeGlobalEntry1->MibObject.u4FsrbridgeContextId >
        pFsrbridgeGlobalEntry2->MibObject.u4FsrbridgeContextId)
    {
        return 1;
    }
    else if (pFsrbridgeGlobalEntry1->MibObject.u4FsrbridgeContextId <
             pFsrbridgeGlobalEntry2->MibObject.u4FsrbridgeContextId)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgeNicknameTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgeNicknameTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgeNicknameTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsrbridgeNicknameEntry *pFsrbridgeNicknameEntry1 =
        (tRbrgFsrbridgeNicknameEntry *) pRBElem1;
    tRbrgFsrbridgeNicknameEntry *pFsrbridgeNicknameEntry2 =
        (tRbrgFsrbridgeNicknameEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsrbridgeNicknameEntry1->MibObject.i4FsrbridgeNicknameName >
        pFsrbridgeNicknameEntry2->MibObject.i4FsrbridgeNicknameName)
    {
        return 1;
    }
    else if (pFsrbridgeNicknameEntry1->MibObject.i4FsrbridgeNicknameName <
             pFsrbridgeNicknameEntry2->MibObject.i4FsrbridgeNicknameName)
    {
        return -1;
    }

    if (pFsrbridgeNicknameEntry1->MibObject.u4FsrbridgeContextId >
        pFsrbridgeNicknameEntry2->MibObject.u4FsrbridgeContextId)
    {
        return 1;
    }
    else if (pFsrbridgeNicknameEntry1->MibObject.u4FsrbridgeContextId <
             pFsrbridgeNicknameEntry2->MibObject.u4FsrbridgeContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgePortTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgePortTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgePortTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsRBridgeBasePortEntry *pFsRBridgeBasePortEntry1 =
        (tRbrgFsRBridgeBasePortEntry *) pRBElem1;
    tRbrgFsRBridgeBasePortEntry *pFsRBridgeBasePortEntry2 =
        (tRbrgFsRBridgeBasePortEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsRBridgeBasePortEntry1->MibObject.i4FsrbridgePortIfIndex >
        pFsRBridgeBasePortEntry2->MibObject.i4FsrbridgePortIfIndex)
    {
        return 1;
    }
    else if (pFsRBridgeBasePortEntry1->MibObject.i4FsrbridgePortIfIndex <
             pFsRBridgeBasePortEntry2->MibObject.i4FsrbridgePortIfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgeUniFdbTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgeUniFdbTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgeUniFdbTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsRbridgeUniFdbEntry *pFsRbridgeUniFdbEntry1 =
        (tRbrgFsRbridgeUniFdbEntry *) pRBElem1;
    tRbrgFsRbridgeUniFdbEntry *pFsRbridgeUniFdbEntry2 =
        (tRbrgFsRbridgeUniFdbEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsRbridgeUniFdbEntry1->MibObject.u4FsrbridgeFdbId >
        pFsRbridgeUniFdbEntry2->MibObject.u4FsrbridgeFdbId)
    {
        return 1;
    }
    else if (pFsRbridgeUniFdbEntry1->MibObject.u4FsrbridgeFdbId <
             pFsRbridgeUniFdbEntry2->MibObject.u4FsrbridgeFdbId)
    {
        return -1;
    }

    if (MEMCMP
        (pFsRbridgeUniFdbEntry1->MibObject.FsrbridgeUniFdbAddr,
         pFsRbridgeUniFdbEntry2->MibObject.FsrbridgeUniFdbAddr, 6) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsRbridgeUniFdbEntry1->MibObject.FsrbridgeUniFdbAddr,
              pFsRbridgeUniFdbEntry2->MibObject.FsrbridgeUniFdbAddr, 6) < 0)
    {
        return -1;
    }

    if (pFsRbridgeUniFdbEntry1->MibObject.u4FsrbridgeContextId >
        pFsRbridgeUniFdbEntry2->MibObject.u4FsrbridgeContextId)
    {
        return 1;
    }
    else if (pFsRbridgeUniFdbEntry1->MibObject.u4FsrbridgeContextId <
             pFsRbridgeUniFdbEntry2->MibObject.u4FsrbridgeContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgeUniFibTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgeUniFibTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgeUniFibTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsRbridgeUniFibEntry *pFsRbridgeUniFibEntry1 =
        (tRbrgFsRbridgeUniFibEntry *) pRBElem1;
    tRbrgFsRbridgeUniFibEntry *pFsRbridgeUniFibEntry2 =
        (tRbrgFsRbridgeUniFibEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibNickname >
        pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibNickname)
    {
        return 1;
    }
    else if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibNickname <
             pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibNickname)
    {
        return -1;
    }

    if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibPort >
        pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibPort)
    {
        return 1;
    }
    else if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibPort <
             pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibPort)
    {
        return -1;
    }

    if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibNextHopRBridge >
        pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibNextHopRBridge)
    {
        return 1;
    }
    else if (pFsRbridgeUniFibEntry1->MibObject.i4FsrbridgeFibNextHopRBridge <
             pFsRbridgeUniFibEntry2->MibObject.i4FsrbridgeFibNextHopRBridge)
    {
        return -1;
    }

    if (pFsRbridgeUniFibEntry1->MibObject.u4FsrbridgeContextId >
        pFsRbridgeUniFibEntry2->MibObject.u4FsrbridgeContextId)
    {
        return 1;
    }
    else if (pFsRbridgeUniFibEntry1->MibObject.u4FsrbridgeContextId <
             pFsRbridgeUniFibEntry2->MibObject.u4FsrbridgeContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgeMultiFibTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgeMultiFibTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgeMultiFibTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsrbridgeMultiFibEntry *pFsrbridgeMultiFibEntry1 =
        (tRbrgFsrbridgeMultiFibEntry *) pRBElem1;
    tRbrgFsrbridgeMultiFibEntry *pFsrbridgeMultiFibEntry2 =
        (tRbrgFsrbridgeMultiFibEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsrbridgeMultiFibEntry1->MibObject.i4FsrbridgeMultiFibNickname >
        pFsrbridgeMultiFibEntry2->MibObject.i4FsrbridgeMultiFibNickname)
    {
        return 1;
    }
    else if (pFsrbridgeMultiFibEntry1->MibObject.i4FsrbridgeMultiFibNickname <
             pFsrbridgeMultiFibEntry2->MibObject.i4FsrbridgeMultiFibNickname)
    {
        return -1;
    }

    if (pFsrbridgeMultiFibEntry1->MibObject.u4FsrbridgeContextId >
        pFsrbridgeMultiFibEntry2->MibObject.u4FsrbridgeContextId)
    {
        return 1;
    }
    else if (pFsrbridgeMultiFibEntry1->MibObject.u4FsrbridgeContextId <
             pFsrbridgeMultiFibEntry2->MibObject.u4FsrbridgeContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgePortCounterTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsrbridgePortCounterTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsrbridgePortCounterTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tRbrgFsRbridgePortCounterEntry *pFsRbridgePortCounterEntry1 =
        (tRbrgFsRbridgePortCounterEntry *) pRBElem1;
    tRbrgFsRbridgePortCounterEntry *pFsRbridgePortCounterEntry2 =
        (tRbrgFsRbridgePortCounterEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsRbridgePortCounterEntry1->MibObject.i4FsrbridgePortIfIndex >
        pFsRbridgePortCounterEntry2->MibObject.i4FsrbridgePortIfIndex)
    {
        return 1;
    }
    else if (pFsRbridgePortCounterEntry1->MibObject.i4FsrbridgePortIfIndex <
             pFsRbridgePortCounterEntry2->MibObject.i4FsrbridgePortIfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsrbridgeGlobalTableFilterInputs
 Input       :  The Indices
                pRbrgFsrbridgeGlobalEntry
                pRbrgSetFsrbridgeGlobalEntry
                pRbrgIsSetFsrbridgeGlobalEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgeGlobalTableFilterInputs (tRbrgFsrbridgeGlobalEntry *
                                  pRbrgFsrbridgeGlobalEntry,
                                  tRbrgFsrbridgeGlobalEntry *
                                  pRbrgSetFsrbridgeGlobalEntry,
                                  tRbrgIsSetFsrbridgeGlobalEntry *
                                  pRbrgIsSetFsrbridgeGlobalEntry)
{
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.
            i4FsrbridgeUniMultipathEnable ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.
            i4FsrbridgeUniMultipathEnable)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.
            i4FsrbridgeMultiMultipathEnable ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.
            i4FsrbridgeMultiMultipathEnable)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus = OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.
            u4FsrbridgeUnicastMultipathCount ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.
            u4FsrbridgeUnicastMultipathCount)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.
            u4FsrbridgeMulticastMultipathCount ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.
            u4FsrbridgeMulticastMultipathCount)
            pRbrgIsSetFsrbridgeGlobalEntry->
                bFsrbridgeMulticastMultipathCount = OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters ==
            pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters)
            pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters =
                OSIX_FALSE;
    }
    if ((pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId == OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->
            bFsrbridgeUnicastMultipathCount == OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->
            bFsrbridgeMulticastMultipathCount == OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsrbridgeNicknameTableFilterInputs
 Input       :  The Indices
                pRbrgFsrbridgeNicknameEntry
                pRbrgSetFsrbridgeNicknameEntry
                pRbrgIsSetFsrbridgeNicknameEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgeNicknameTableFilterInputs (tRbrgFsrbridgeNicknameEntry *
                                    pRbrgFsrbridgeNicknameEntry,
                                    tRbrgFsrbridgeNicknameEntry *
                                    pRbrgSetFsrbridgeNicknameEntry,
                                    tRbrgIsSetFsrbridgeNicknameEntry *
                                    pRbrgIsSetFsrbridgeNicknameEntry)
{
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName ==
            pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName)
            pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknamePriority ==
            pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknamePriority)
            pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknameDtrPriority ==
            pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknameDtrPriority)
            pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus ==
            pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus)
            pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId ==
            pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId)
            pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_FALSE;
    }
    if ((pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName == OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsrbridgePortTableFilterInputs
 Input       :  The Indices
                pRbrgFsRBridgeBasePortEntry
                pRbrgSetFsRBridgeBasePortEntry
                pRbrgIsSetFsRBridgeBasePortEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgePortTableFilterInputs (tRbrgFsRBridgeBasePortEntry *
                                pRbrgFsRBridgeBasePortEntry,
                                tRbrgFsRBridgeBasePortEntry *
                                pRbrgSetFsRBridgeBasePortEntry,
                                tRbrgIsSetFsRBridgeBasePortEntry *
                                pRbrgIsSetFsRBridgeBasePortEntry)
{
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex == OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable == OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort == OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort == OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning ==
        OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortDisableLearning ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortDisableLearning)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan == OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters ==
        OSIX_TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortClearCounters ==
            pRbrgSetFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortClearCounters)
            pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters =
                OSIX_FALSE;
    }
    if ((pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex == OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsrbridgeUniFdbTableFilterInputs
 Input       :  The Indices
                pRbrgFsRbridgeUniFdbEntry
                pRbrgSetFsRbridgeUniFdbEntry
                pRbrgIsSetFsRbridgeUniFdbEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgeUniFdbTableFilterInputs (tRbrgFsRbridgeUniFdbEntry *
                                  pRbrgFsRbridgeUniFdbEntry,
                                  tRbrgFsRbridgeUniFdbEntry *
                                  pRbrgSetFsRbridgeUniFdbEntry,
                                  tRbrgIsSetFsRbridgeUniFdbEntry *
                                  pRbrgIsSetFsRbridgeUniFdbEntry)
{
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr == OSIX_TRUE)
    {
        if (MEMCMP (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
                    &(pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr), 6) == 0)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId ==
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId)
            pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_FALSE;
    }
    if ((pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsrbridgeUniFibTableFilterInputs
 Input       :  The Indices
                pRbrgFsRbridgeUniFibEntry
                pRbrgSetFsRbridgeUniFibEntry
                pRbrgIsSetFsRbridgeUniFibEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgeUniFibTableFilterInputs (tRbrgFsRbridgeUniFibEntry *
                                  pRbrgFsRbridgeUniFibEntry,
                                  tRbrgFsRbridgeUniFibEntry *
                                  pRbrgSetFsRbridgeUniFibEntry,
                                  tRbrgIsSetFsRbridgeUniFibEntry *
                                  pRbrgIsSetFsRbridgeUniFibEntry)
{
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge ==
        OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.
            i4FsrbridgeFibNextHopRBridge)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress == OSIX_TRUE)
    {
        if ((MEMCMP
             (pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
              pRbrgSetFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
              pRbrgSetFsRbridgeUniFibEntry->MibObject.
              i4FsrbridgeFibMacAddressLen) == 0)
            && (pRbrgFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibMacAddressLen ==
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibMacAddressLen))
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_FALSE;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        if (pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId ==
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId)
            pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_FALSE;
    }
    if ((pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount == OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus ==
            OSIX_FALSE)
        && (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsrbridgeMultiFibTableFilterInputs
 Input       :  The Indices
                pRbrgFsrbridgeMultiFibEntry
                pRbrgSetFsrbridgeMultiFibEntry
                pRbrgIsSetFsrbridgeMultiFibEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsrbridgeMultiFibTableFilterInputs (tRbrgFsrbridgeMultiFibEntry *
                                    pRbrgFsrbridgeMultiFibEntry,
                                    tRbrgFsrbridgeMultiFibEntry *
                                    pRbrgSetFsrbridgeMultiFibEntry,
                                    tRbrgIsSetFsrbridgeMultiFibEntry *
                                    pRbrgIsSetFsrbridgeMultiFibEntry)
{
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibNickname ==
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibNickname)
            pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts == OSIX_TRUE)
    {
        if ((MEMCMP
             (pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
              pRbrgSetFsrbridgeMultiFibEntry->MibObject.
              au1FsrbridgeMultiFibPorts,
              pRbrgSetFsrbridgeMultiFibEntry->MibObject.
              i4FsrbridgeMultiFibPortsLen) == 0)
            && (pRbrgFsrbridgeMultiFibEntry->MibObject.
                i4FsrbridgeMultiFibPortsLen ==
                pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                i4FsrbridgeMultiFibPortsLen))
            pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus ==
        OSIX_TRUE)
    {
        if (pRbrgFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibRowStatus ==
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibRowStatus)
            pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus =
                OSIX_FALSE;
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId ==
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId)
            pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_FALSE;
    }
    if ((pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname ==
         OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus ==
            OSIX_FALSE)
        && (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeGlobalTableTrigger
 Input       :  The Indices
                pRbrgSetFsrbridgeGlobalEntry
                pRbrgIsSetFsrbridgeGlobalEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeGlobalTableTrigger (tRbrgFsrbridgeGlobalEntry *
                                       pRbrgSetFsrbridgeGlobalEntry,
                                       tRbrgIsSetFsrbridgeGlobalEntry *
                                       pRbrgIsSetFsrbridgeGlobalEntry,
                                       INT4 i4SetOption)
{

    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeContextId, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniMultipathEnable, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      i4FsrbridgeUniMultipathEnable);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeMultiMultipathEnable, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      i4FsrbridgeMultiMultipathEnable);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeNicknameNumber, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeNicknameNumber);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeSystemControl, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      i4FsrbridgeSystemControl);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeModuleStatus, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      i4FsrbridgeModuleStatus);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUnicastMultipathCount, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeUnicastMultipathCount);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeMulticastMultipathCount, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeMulticastMultipathCount);
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeClearCounters, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeGlobalEntry->MibObject.
                      i4FsrbridgeClearCounters);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeNicknameTableTrigger
 Input       :  The Indices
                pRbrgSetFsrbridgeNicknameEntry
                pRbrgIsSetFsrbridgeNicknameEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeNicknameTableTrigger (tRbrgFsrbridgeNicknameEntry *
                                         pRbrgSetFsrbridgeNicknameEntry,
                                         tRbrgIsSetFsrbridgeNicknameEntry *
                                         pRbrgIsSetFsrbridgeNicknameEntry,
                                         INT4 i4SetOption)
{

    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeNicknameName, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %i",
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName);
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeNicknamePriority, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %u",
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeNicknamePriority);
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeNicknameDtrPriority, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %u",
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeNicknameDtrPriority);
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeNicknameStatus, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %i",
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameStatus);
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeContextId, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %u",
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      i4FsrbridgeNicknameName,
                      pRbrgSetFsrbridgeNicknameEntry->MibObject.
                      u4FsrbridgeContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgePortTableTrigger
 Input       :  The Indices
                pRbrgSetFsRBridgeBasePortEntry
                pRbrgIsSetFsRBridgeBasePortEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgePortTableTrigger (tRbrgFsRBridgeBasePortEntry *
                                     pRbrgSetFsRBridgeBasePortEntry,
                                     tRbrgIsSetFsRBridgeBasePortEntry *
                                     pRbrgIsSetFsRBridgeBasePortEntry,
                                     INT4 i4SetOption)
{

    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortIfIndex, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortDisable, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortDisable);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortTrunkPort, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortTrunkPort);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortAccessPort, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortAccessPort);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortDisableLearning, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortDisableLearning);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortDesigVlan, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortDesigVlan);
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgePortClearCounters, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortIfIndex,
                      pRbrgSetFsRBridgeBasePortEntry->MibObject.
                      i4FsrbridgePortClearCounters);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeUniFdbTableTrigger
 Input       :  The Indices
                pRbrgSetFsRbridgeUniFdbEntry
                pRbrgIsSetFsRbridgeUniFdbEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeUniFdbTableTrigger (tRbrgFsRbridgeUniFdbEntry *
                                       pRbrgSetFsRbridgeUniFdbEntry,
                                       tRbrgIsSetFsRbridgeUniFdbEntry *
                                       pRbrgIsSetFsRbridgeUniFdbEntry,
                                       INT4 i4SetOption)
{

    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFdbId, 14, RbrgMainTaskLock, RbrgMainTaskUnLock,
                      0, 0, 3, i4SetOption, "%u %u %m %u",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniFdbAddr, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 3, i4SetOption, "%u %u %m %m",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniFdbPort, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 3, i4SetOption, "%u %u %m %i",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      i4FsrbridgeUniFdbPort);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniFdbNick, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 3, i4SetOption, "%u %u %m %i",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      i4FsrbridgeUniFdbNick);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniFdbConfidence, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 3, i4SetOption, "%u %u %m %u",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeUniFdbConfidence);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeUniFdbRowStatus, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 1, 3, i4SetOption, "%u %u %m %i",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      i4FsrbridgeUniFdbRowStatus);
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeContextId, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 3, i4SetOption, "%u %u %m %u",
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      FsrbridgeUniFdbAddr,
                      pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                      u4FsrbridgeContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeUniFibTableTrigger
 Input       :  The Indices
                pRbrgSetFsRbridgeUniFibEntry
                pRbrgIsSetFsRbridgeUniFibEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeUniFibTableTrigger (tRbrgFsRbridgeUniFibEntry *
                                       pRbrgSetFsRbridgeUniFibEntry,
                                       tRbrgIsSetFsRbridgeUniFibEntry *
                                       pRbrgIsSetFsRbridgeUniFibEntry,
                                       INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsrbridgeFibMacAddressVal;
    UINT1               au1FsrbridgeFibMacAddressVal[256];

    MEMSET (au1FsrbridgeFibMacAddressVal, 0,
            sizeof (au1FsrbridgeFibMacAddressVal));
    FsrbridgeFibMacAddressVal.pu1_OctetList = au1FsrbridgeFibMacAddressVal;
    FsrbridgeFibMacAddressVal.i4_Length = 0;

    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibNickname, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %i",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibPort, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %i",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibNextHopRBridge, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %i",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress == OSIX_TRUE)
    {
        MEMCPY (FsrbridgeFibMacAddressVal.pu1_OctetList,
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                au1FsrbridgeFibMacAddress,
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibMacAddressLen);
        FsrbridgeFibMacAddressVal.i4_Length =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen;

        nmhSetCmnNew (FsrbridgeFibMacAddress, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %s",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge, &FsrbridgeFibMacAddressVal);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibMtuDesired, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %u",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeFibMtuDesired);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibHopCount, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %u",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeFibHopCount);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeFibRowstatus, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 1, 4, i4SetOption,
                      "%u %i %i %i %i",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibRowstatus);
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeContextId, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 4, i4SetOption,
                      "%u %i %i %i %u",
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNickname,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibPort,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      i4FsrbridgeFibNextHopRBridge,
                      pRbrgSetFsRbridgeUniFibEntry->MibObject.
                      u4FsrbridgeContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeMultiFibTableTrigger
 Input       :  The Indices
                pRbrgSetFsrbridgeMultiFibEntry
                pRbrgIsSetFsrbridgeMultiFibEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeMultiFibTableTrigger (tRbrgFsrbridgeMultiFibEntry *
                                         pRbrgSetFsrbridgeMultiFibEntry,
                                         tRbrgIsSetFsrbridgeMultiFibEntry *
                                         pRbrgIsSetFsrbridgeMultiFibEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsrbridgeMultiFibPortsVal;
    UINT1              
        au1FsrbridgeMultiFibPortsVal[pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                                     i4FsrbridgeMultiFibPortsLen];

    MEMSET (au1FsrbridgeMultiFibPortsVal, 0,
            sizeof (au1FsrbridgeMultiFibPortsVal));
    FsrbridgeMultiFibPortsVal.pu1_OctetList = au1FsrbridgeMultiFibPortsVal;
    FsrbridgeMultiFibPortsVal.i4_Length = 0;

    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeMultiFibNickname, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %i",
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibNickname,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibNickname);
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts == OSIX_TRUE)
    {
        MEMCPY (FsrbridgeMultiFibPortsVal.pu1_OctetList,
                pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                au1FsrbridgeMultiFibPorts,
                pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                i4FsrbridgeMultiFibPortsLen);
        FsrbridgeMultiFibPortsVal.i4_Length =
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibPortsLen;

        nmhSetCmnNew (FsrbridgeMultiFibPorts, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %s",
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibNickname, &FsrbridgeMultiFibPortsVal);
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeMultiFibRowStatus, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %i %i",
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibNickname,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibRowStatus);
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsrbridgeContextId, 14, RbrgMainTaskLock,
                      RbrgMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %u",
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      u4FsrbridgeContextId,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      i4FsrbridgeMultiFibNickname,
                      pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                      u4FsrbridgeContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgFsrbridgeGlobalTableCreateApi
 Input       :  pRbrgFsrbridgeGlobalEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsrbridgeGlobalEntry *
RbrgFsrbridgeGlobalTableCreateApi (tRbrgFsrbridgeGlobalEntry *
                                   pSetRbrgFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    if (pSetRbrgFsrbridgeGlobalEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeGlobalTableCreatApi: pSetRbrgFsrbridgeGlobalEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeGlobalTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsrbridgeGlobalEntry, pSetRbrgFsrbridgeGlobalEntry,
                sizeof (tRbrgFsrbridgeGlobalEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
             (tRBElem *) pRbrgFsrbridgeGlobalEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgeGlobalTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                                (UINT1 *) pRbrgFsrbridgeGlobalEntry);
            return NULL;
        }
        return pRbrgFsrbridgeGlobalEntry;
    }
}

/****************************************************************************
 Function    :  RbrgFsrbridgeNicknameTableCreateApi
 Input       :  pRbrgFsrbridgeNicknameEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsrbridgeNicknameEntry *
RbrgFsrbridgeNicknameTableCreateApi (tRbrgFsrbridgeNicknameEntry *
                                     pSetRbrgFsrbridgeNicknameEntry)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    if (pSetRbrgFsrbridgeNicknameEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeNicknameTableCreatApi: pSetRbrgFsrbridgeNicknameEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);
    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeNicknameTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsrbridgeNicknameEntry, pSetRbrgFsrbridgeNicknameEntry,
                sizeof (tRbrgFsrbridgeNicknameEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
             (tRBElem *) pRbrgFsrbridgeNicknameEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgeNicknameTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                                (UINT1 *) pRbrgFsrbridgeNicknameEntry);
            return NULL;
        }
        return pRbrgFsrbridgeNicknameEntry;
    }
}

/****************************************************************************
 Function    :  RbrgFsrbridgePortTableCreateApi
 Input       :  pRbrgFsRBridgeBasePortEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsRBridgeBasePortEntry *
RbrgFsrbridgePortTableCreateApi (tRbrgFsRBridgeBasePortEntry *
                                 pSetRbrgFsRBridgeBasePortEntry)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    if (pSetRbrgFsRBridgeBasePortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgePortTableCreatApi: pSetRbrgFsRBridgeBasePortEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgePortTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsRBridgeBasePortEntry, pSetRbrgFsRBridgeBasePortEntry,
                sizeof (tRbrgFsRBridgeBasePortEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
             (tRBElem *) pRbrgFsRBridgeBasePortEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgePortTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                (UINT1 *) pRbrgFsRBridgeBasePortEntry);
            return NULL;
        }
        return pRbrgFsRBridgeBasePortEntry;
    }
}

/****************************************************************************
 Function    :  RbrgFsrbridgeUniFdbTableCreateApi
 Input       :  pRbrgFsRbridgeUniFdbEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsRbridgeUniFdbEntry *
RbrgFsrbridgeUniFdbTableCreateApi (tRbrgFsRbridgeUniFdbEntry *
                                   pSetRbrgFsRbridgeUniFdbEntry)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    if (pSetRbrgFsRbridgeUniFdbEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeUniFdbTableCreatApi: pSetRbrgFsRbridgeUniFdbEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeUniFdbTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsRbridgeUniFdbEntry, pSetRbrgFsRbridgeUniFdbEntry,
                sizeof (tRbrgFsRbridgeUniFdbEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeUniFdbTable,
             (tRBElem *) pRbrgFsRbridgeUniFdbEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgeUniFdbTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
            return NULL;
        }
        return pRbrgFsRbridgeUniFdbEntry;
    }
}

/****************************************************************************
 Function    :  RbrgFsrbridgeUniFibTableCreateApi
 Input       :  pRbrgFsRbridgeUniFibEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsRbridgeUniFibEntry *
RbrgFsrbridgeUniFibTableCreateApi (tRbrgFsRbridgeUniFibEntry *
                                   pSetRbrgFsRbridgeUniFibEntry)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgFsRBridgeBasePortEntry PortEntry;
    tRbrgFsRBridgeBasePortEntry *pPortEntry = NULL;

    MEMSET (&PortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
    if (pSetRbrgFsRbridgeUniFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeUniFibTableCreatApi: pSetRbrgFsRbridgeUniFibEntry is NULL.\r\n"));
        return NULL;
    }

    PortEntry.MibObject.i4FsrbridgePortIfIndex =
        pSetRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;

    pPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeUniFibTableCreatApi: FIB Port Not Present\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeUniFibTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsRbridgeUniFibEntry, pSetRbrgFsRbridgeUniFibEntry,
                sizeof (tRbrgFsRbridgeUniFibEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeUniFibTable,
             (tRBElem *) pRbrgFsRbridgeUniFibEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgeUniFibTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return NULL;
        }

        /* Increment Port Fib Count */
        pPortEntry->u4ReferenceCount++;

        return pRbrgFsRbridgeUniFibEntry;
    }
}

/****************************************************************************
 Function    :  RbrgFsrbridgeMultiFibTableCreateApi
 Input       :  pRbrgFsrbridgeMultiFibEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tRbrgFsrbridgeMultiFibEntry *
RbrgFsrbridgeMultiFibTableCreateApi (tRbrgFsrbridgeMultiFibEntry *
                                     pSetRbrgFsrbridgeMultiFibEntry)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    if (pSetRbrgFsrbridgeMultiFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeMultiFibTableCreatApi: pSetRbrgFsrbridgeMultiFibEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgFsrbridgeMultiFibTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pRbrgFsrbridgeMultiFibEntry, pSetRbrgFsrbridgeMultiFibEntry,
                sizeof (tRbrgFsrbridgeMultiFibEntry));
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
             (tRBElem *) pRbrgFsrbridgeMultiFibEntry) != RB_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgFsrbridgeMultiFibTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
            return NULL;
        }
        return pRbrgFsrbridgeMultiFibEntry;
    }
}
