/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgtask.c,v 1.1.1.1 2011/12/30 13:29:54 siva Exp $
 * Description:This file contains procedures related to
 *             RBRG - Task Initialization
 *******************************************************************/

#include "rbrginc.h"
#include "rbrgglob.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgTaskSpawnRbrgTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Rbrg Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if RBRG Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RbrgTaskSpawnRbrgTask (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    /*RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTaskSpawnRbrgTask\n"));*/

    /* task initializations */
    if (RbrgMainTaskInit () == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_TASK_TRC, " !!!!! RBRG TASK INIT FAILURE  !!!!! \n"));
        RbrgMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        gbIsRbrgInitialised = FALSE;
        return;
    }

    /* spawn rbrg task */

    RbrgTaskRegisterRbrgMibs ();
    lrInitComplete (OSIX_SUCCESS);
    gbIsRbrgInitialised = TRUE;
    RbrgMainTask ();
    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "EXIT:RbrgTaskSpawnRbrgTask\n"));
    return;
}


PUBLIC INT4 RbrgTaskRegisterRbrgMibs ()
{
    RegisterFSRBRI ();
    return OSIX_SUCCESS;
}


/*------------------------------------------------------------------------*/
/*                        End of the file  rbrgtask.c                     */
/*------------------------------------------------------------------------*/
