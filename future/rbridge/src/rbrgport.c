/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgport.c,v 1.1.1.1 2011/12/30 13:29:54 siva Exp $
 *
 * Description: This file contains the portable routines which calls APIs
 *              given by other modules.
 ******************************************************************************/

#include "rbrginc.h"

/***************************************************************************
 * FUNCTION NAME    : RbrgPortCfaGetIfInfo 
 *
 * DESCRIPTION      : This function returns the interface related params
 *                    to the external modules.Used to get Mac address of a
 *                    port
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pIfInfo   - Pointer to the tCfaIfInfo
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
RbrgPortCfaGetSysMacAddress (tMacAddr SwitchMac)
{

    CfaGetSysMacAddress (SwitchMac);

}
/***************************************************************************
 *FUNCTION NAME    : RbrgPortVcmIsSwitchExist
 *
 *DESCRIPTION      : Routine used to get the context Id for the Alias Name
 *
 *INPUT            : pu1Alias - Context Name
 *
 *OUTPUT           : u4ContextId - Context Identifier
 *
 *RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
RbrgPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if (VcmIsSwitchExist (pu1Alias, pu4ContextId) == VCM_TRUE)
    {
        i4RetVal = OSIX_SUCCESS;
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : RbrgPortVcmGetCxtInfoFromIfIndex 
 *
 * DESCRIPTION      : Routine used to get the Context Information from
 *                    ifIndex.
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pu4ContextId - Context Identifier
 *                    pu2LocalPortId - Local Port Identifier
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
RbrgPortVcmGetCxtInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                UINT2 *pu2LocalPortId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                             pu2LocalPortId);
    if (i4RetVal != VCM_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}
/***************************************************************************
 * FUNCTION NAME    : RbrgPortVcmGetFirstVcId
 *
 * DESCRIPTION      : Routine used to get the first Context ID.
 *
 * INPUT            : pu4ContextId - pointer to context ID
 *
 * OUTPUT           : u4ContextId - Context Identifier
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
RbrgPortVcmGetFirstVcId (UINT4 *pu4ContextId)
{
    VcmGetFirstActiveContext (pu4ContextId);
    return;

}

/***************************************************************************
 * FUNCTION NAME    : RbrgPortVcmGetNextVcId
 *
 * DESCRIPTION      : Routine used to get the next Context ID
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4NextContextId - pointer to next context Identifier
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
RbrgPortVcmGetNextVcId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = VcmGetNextActiveContext (u4ContextId, pu4NextContextId);

    if (i4RetVal == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RbrgPortIsPortInPortChannel                            */
/*                                                                           */
/* Description        : This function calls L2IWF Module and returns         */
/*                      whether the given port is part of any port channel   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
RbrgPortIsPortInPortChannel (UINT4 u4IfIndex)
{
    if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : RbrgPortVcmGetContextPortList
 *
 * DESCRIPTION      : Routine used to get the Context Information from
 *                    ifIndex.
 *
 * INPUT            : u4ContextId - context Identifier
 *
 * OUTPUT           : tPortList - port list array
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
RbrgPortVcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    if (VcmGetContextPortList (u4ContextId, PortList) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RbrgCfaCliGetIfList                                  */
/*                                                                           */
/* Description        : This function calls the functions which convert      */
/*                      the given string to interface list                   */
/*                                                                           */
/* Input(s)           : pi1IfName :Port Number                               */
/*                      pi1IfListStr : Pointer to the string                 */
/*                      pu1IfListArray:Pointer to the string in which the    */
/*                      bits for the port list will be set                   */
/*                      u4IfListArrayLen:Array Length                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

INT4
RbrgCfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                     UINT4 u4IfListArrayLen)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfList (pi1IfName, pi1IfListStr, pu1IfListArray,
                             u4IfListArrayLen));
#else
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
    return CLI_SUCCESS;
#endif

}

