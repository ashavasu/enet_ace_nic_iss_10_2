/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrglw.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains some of low level functions used by protocol in Rbrg module
*********************************************************************/

#include "rbrginc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgeGlobalTable
 Input       :  The Indices
                FsrbridgeContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgeGlobalTable (UINT4 u4FsrbridgeContextId)
{
    tRbrgFsrbridgeGlobalEntry FsGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pFsGlobalEntry = NULL;

    if (u4FsrbridgeContextId >=
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "ContextId is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    FsGlobalEntry.MibObject.u4FsrbridgeContextId = u4FsrbridgeContextId;
    pFsGlobalEntry = (tRbrgFsrbridgeGlobalEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   &FsGlobalEntry);
    if (pFsGlobalEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgeNicknameTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgeNicknameTable (UINT4 u4FsrbridgeContextId,
                                                INT4 i4FsrbridgeNicknameName)
{
    tRbrgMibFsrbridgeNicknameEntry FsNicknameEntry;
    tRbrgMibFsrbridgeNicknameEntry *pFsNicknameEntry = NULL;

    if (u4FsrbridgeContextId >=
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "ContextId is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsrbridgeNicknameName < RBRG_MIN_NICKNAME) ||
        (i4FsrbridgeNicknameName >= RBRG_MAX_NICKNAME))
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "Nickname is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    FsNicknameEntry.u4FsrbridgeContextId = u4FsrbridgeContextId;
    FsNicknameEntry.i4FsrbridgeNicknameName = i4FsrbridgeNicknameName;
    pFsNicknameEntry = (tRbrgMibFsrbridgeNicknameEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                   &FsNicknameEntry);
    if (pFsNicknameEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgePortTable
 Input       :  The Indices
                FsrbridgePortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgePortTable (INT4 i4FsrbridgePortIfIndex)
{
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    if (i4FsrbridgePortIfIndex >
        (INT4) FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "PortIndex is out of Range \r\n"));
    }

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = i4FsrbridgePortIfIndex;
    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, &FsPortEntry);
    if (pFsPortEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgeUniFdbTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgeUniFdbTable (UINT4 u4FsrbridgeContextId,
                                              UINT4 u4FsrbridgeFdbId,
                                              tMacAddr FsRbrgUniFdbAddr)
{
    tRBrgFdbEntry       FsFdbEntry;
    tRBrgFdbEntry      *pFsFdbEntry = NULL;

    MEMSET (&FsFdbEntry, 0, sizeof (tRBrgFdbEntry));

    if (u4FsrbridgeContextId >=
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "ContextId is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    if (u4FsrbridgeFdbId >= RBRG_MAX_VLAN)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "FDBID is out of range \r\n"));
        return SNMP_FAILURE;
    }
    FsFdbEntry.u4RBrgFdbId = u4FsrbridgeFdbId;
    FsFdbEntry.u4ContextId = u4FsrbridgeContextId;
    MEMCPY ((FsFdbEntry.RBrgFdbMac), (FsRbrgUniFdbAddr), MAC_ADDR_LEN);
    pFsFdbEntry = (tRBrgFdbEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, &FsFdbEntry);
    if (pFsFdbEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgeUniFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgeUniFibTable (UINT4 u4FsrbridgeContextId,
                                              INT4 i4FsrbridgeFibNickname,
                                              INT4 i4FsrbridgeFibPort,
                                              INT4 i4FsrbridgeFibNextHopRBridge)
{
    tRBrgFibEntry       FsFibEntry;
    tRBrgFibEntry      *pFsFibEntry = NULL;

    MEMSET (&FsFibEntry, 0, sizeof (tRBrgFibEntry));

    if (u4FsrbridgeContextId >=
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "ContextId is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    if (i4FsrbridgeFibPort >
        (INT4) FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "PortIndex is out of Range \r\n"));
    }
    if ((i4FsrbridgeFibNickname < RBRG_MIN_NICKNAME) ||
        (i4FsrbridgeFibNickname >= RBRG_MAX_NICKNAME))
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "Nickname is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsrbridgeFibNextHopRBridge < RBRG_MIN_NICKNAME) ||
        (i4FsrbridgeFibNextHopRBridge >= RBRG_MAX_NICKNAME))
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "NextHopNickname is out of Range \r\n"));
        return SNMP_FAILURE;
    }

    FsFibEntry.u4ContextId = u4FsrbridgeContextId;
    FsFibEntry.u4FibNextHopRBrgPortId = i4FsrbridgeFibPort;
    FsFibEntry.u4FibEgressRBrgNickname = i4FsrbridgeFibNickname;
    if (i4FsrbridgeFibNextHopRBridge != 0)
    {
        FsFibEntry.u4FibNextHopRBrgNickname = i4FsrbridgeFibNextHopRBridge;
        pFsFibEntry = (tRBrgFibEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable, &FsFibEntry);
    }
    else
    {
        pFsFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           &FsFibEntry, RbrgUtlFibTblCmpFn);
        if ((pFsFibEntry == NULL) ||
            ((pFsFibEntry->u4ContextId != u4FsrbridgeContextId) &&
             (pFsFibEntry->u4FibNextHopRBrgPortId !=
              (UINT4) i4FsrbridgeFibPort)))
        {
            pFsFibEntry = NULL;
        }
    }
    if (pFsFibEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgeMultiFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgeMultiFibTable (UINT4 u4FsrbridgeContextId,
                                                INT4
                                                i4FsrbridgeMultiFibNickname)
{
    tRbrgMibFsrbridgeMultiFibEntry FsMultiFibEntry;
    tRbrgMibFsrbridgeMultiFibEntry *pFsMultiFibEntry = NULL;

    MEMSET (&FsMultiFibEntry, 0, sizeof (tRbrgMibFsrbridgeMultiFibEntry));

    if (u4FsrbridgeContextId >=
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "ContextId is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    if ((i4FsrbridgeMultiFibNickname < RBRG_MIN_NICKNAME) ||
        (i4FsrbridgeMultiFibNickname >= RBRG_MAX_NICKNAME))
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "Nickname is out of Range \r\n"));
        return SNMP_FAILURE;
    }
    FsMultiFibEntry.u4FsrbridgeContextId = u4FsrbridgeContextId;
    FsMultiFibEntry.i4FsrbridgeMultiFibNickname = i4FsrbridgeMultiFibNickname;
    pFsMultiFibEntry = (tRbrgMibFsrbridgeMultiFibEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                   &FsMultiFibEntry);
    if (pFsMultiFibEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrbridgePortCounterTable
 Input       :  The Indices
                FsrbridgePortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrbridgePortCounterTable (INT4 i4FsrbridgePortIfIndex)
{

    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    if (i4FsrbridgePortIfIndex >
        (INT4) FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "PortIndex is out of Range \r\n"));
    }

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = i4FsrbridgePortIfIndex;
    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, &FsPortEntry);
    if (pFsPortEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC), "Entry not found\r\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
