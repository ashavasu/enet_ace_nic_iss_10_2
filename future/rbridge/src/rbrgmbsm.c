/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgmbsm.c,v 1.4 2014/03/11 14:23:34 siva Exp $
 *
 * Description: This file contains RBRG MBSM support routines 
                and utility routines.
 *********************************************************************/
#include "mbsm.h"
#include "rbrginc.h"
/*****************************************************************************/
/* Function Name      : RbrgApiMbsmNotification                              */
/*                                                                           */
/* Description        : This function constructs the RBRG Q Msg and calls    */
/*                      the Q Message event to process this Meg.             */
/*                                                                           */
/*                      This function will be called from the MBSM module    */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                    :  i4Event       - Event to be processed . Currently it*/
/*                                       is MBSM_MSG_CARD_INSERT event       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_FAILURE / MBSM_SUCCESS                          */
/*****************************************************************************/
PUBLIC INT4
RbrgApiMbsmNotification (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tRbrgQMsg          *pMsg = NULL;
    tMbsmProtoMsg      *pMbsmProtoMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }
    if (i4Event == MBSM_MSG_CARD_REMOVE)
    {
        MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    /* Allocates the memory from the queue memory pool */
    if ((pMsg = (tRbrgQMsg *) MemAllocMemBlk (RBRG_QUEUE_MSG_POOLID)) == NULL)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiMbsmNotification:"
                   "RBRG Queue Message memory Allocation Failed !!!\r\n"));
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tRbrgQMsg));
    if ((pMbsmProtoMsg = MemAllocMemBlk (RBRG_MBSM_POOLID)) == NULL)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiMbsmNotification: Message memory "
                   "Allocation Failed for pMbsmProtoMsg !!!\r\n"));
        MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pMsg);
        return MBSM_FAILURE;
    }
    pMsg->pMbsmProtoMsg = pMbsmProtoMsg;
    pMsg->u4MsgType = RBRG_MBSM_MSG_CARD_INSERT;
    MEMCPY (pMsg->pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));

    /* Post it to the RBRG queue */
    if (RbrgQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        RBRG_TRC ((ALL_FAILURE_TRC, "RbrgApiMbsmNotification:"
                   "RBRG EnQueue Message into queue Failed !!!\r\n"));
        MemReleaseMemBlock (RBRG_MBSM_POOLID, (UINT1 *) (pMbsmProtoMsg));
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RbrgMbsmProcessMsg                                   */
/*                                                                           */
/* Description        : This function process the MBSM message and calls the */
/*                      respective function to programm the entries in Hw.   */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
RbrgMbsmProcessMsg (tMbsmProtoMsg * pProtoMsg)
{
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg != NULL)
    {
        RbrgMbsmConfigTrill (&pProtoMsg->MbsmPortInfo,
                             &pProtoMsg->MbsmSlotInfo);

    }

    MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
    MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
    MbsmSendAckFromProto (&MbsmProtoAckMsg);
    return;
}

/*****************************************************************************/
/* Function Name      : RbrgMbsmConfigTrill                                  */
/*                                                                           */
/* Description        : This function scans the port entries for ports       */
/*                      present in the slot info and programs the port       */
/*                      properties in the hardware.                          */
/*                                                                           */
/* Input(s)           : MbsmPortInfo - Port Info.                            */
/*                      MbsmSlotInfo - Slot Info.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
RbrgMbsmConfigTrill (tMbsmPortInfo * pMbsmPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tRbrgFsRBridgeBasePortEntry *pFsRbrgPortEntry = NULL;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRBrgFdbEntry      *pNextRbrgFdbEntry = NULL;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRbrgFsRBridgeBasePortEntry FsRbrgPortEntry;
    tRBrgFdbEntry       RbrgFdbEntry;
    tRBrgFibEntry       RbrgFibEntry;
    tRbrgFsRbridgeUniFibEntry FsOldFibEntry;
    tRbrgFsRbridgeUniFibEntry FsFibEntry;
    tRbrgIsSetFsRbridgeUniFibEntry FsIsSetFibEntry;
    tRbrgFsRbridgeUniFdbEntry FsOldFdbEntry;
    tRbrgFsRbridgeUniFdbEntry FsFdbEntry;
    tRbrgIsSetFsRbridgeUniFdbEntry FsIsSetFdbEntry;
    tRbrgFsRBridgeBasePortEntry FsOldPortEntry;
    tRbrgIsSetFsRBridgeBasePortEntry FsIsSetPortEntry;
    UINT4               u4Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    UNUSED_PARAM (pSlotInfo);

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    MEMSET (&FsOldFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&FsFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&FsIsSetFibEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFibEntry));
    MEMSET (&FsOldFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsIsSetFdbEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));
    MEMSET (&FsOldFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsIsSetFdbEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = pMbsmPortInfo->PortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                  u2BitIndex + 1);

                MEMSET (&FsRbrgPortEntry, 0,
                        sizeof (tRbrgFsRBridgeBasePortEntry));

                FsRbrgPortEntry.MibObject.i4FsrbridgePortIfIndex = u4Port;
                RbrgGetAllUtlFsrbridgePortTable (&FsRbrgPortEntry,
                                                 pFsRbrgPortEntry);
                if (pFsRbrgPortEntry != NULL)
                {
                    /* Loop for FDB and FIB entries on this port and 
                     * program the hardware */
                    RbrgFibEntry.u4FibNextHopRBrgPortId = u4Port;
                    RbrgFibEntry.u4ContextId = 0;
                    RbrgFibEntry.u4FibEgressRBrgNickname = 0;
                    RbrgFibEntry.u4FibNextHopRBrgNickname = 0;

                    RbrgUtilUpdateFsrbridgePortTable (&FsOldPortEntry,
                                                      pFsRbrgPortEntry,
                                                      &FsIsSetPortEntry);
                    pRbrgFibEntry = (tRBrgFibEntry *)
                        RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                       RbrgUniFibTable,
                                       (tRBElem *) & RbrgFibEntry,
                                       RbrgUtlFibTblCmpFn);

                    while ((pRbrgFibEntry != NULL) &&
                           (pRbrgFibEntry->u4FibNextHopRBrgPortId == u4Port))

                    {
                        RBrgUtlCopyRBrgFibToFsFib (&FsFibEntry, pRbrgFibEntry);
                        FsIsSetFibEntry.bFsrbridgeFibRowstatus = TRUE;
                        FsFibEntry.MibObject.i4FsrbridgeFibRowstatus = ACTIVE;
                        RbrgUtilUpdateFsrbridgeUniFibTable (&FsOldFibEntry,
                                                            &FsFibEntry,
                                                            &FsIsSetFibEntry,
                                                            1);
                        UTL_DLL_OFFSET_SCAN (&(pRbrgFibEntry->RBrgFibFdbList),
                                             pRbrgFdbEntry, pNextRbrgFdbEntry,
                                             tRBrgFdbEntry *)
                        {
                            RBrgUtlCopyRBrgFdbToFsFdb (&FsFdbEntry,
                                                       pRbrgFdbEntry);
                            RbrgUtilUpdateFsrbridgeUniFdbTable
                                (&FsOldFdbEntry, &FsFdbEntry, &FsIsSetFdbEntry);
                        }
                        RbrgFibEntry.u4FibNextHopRBrgPortId =
                            pRbrgFibEntry->u4FibNextHopRBrgPortId;
                        RbrgFibEntry.u4ContextId = pRbrgFibEntry->u4ContextId;
                        RbrgFibEntry.u4FibEgressRBrgNickname =
                            pRbrgFibEntry->u4FibEgressRBrgNickname;
                        RbrgFibEntry.u4FibNextHopRBrgNickname =
                            pRbrgFibEntry->u4FibNextHopRBrgNickname;
                        pRbrgFibEntry = (tRBrgFibEntry *)
                            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                           RbrgUniFibTable,
                                           (tRBElem *) & RbrgFibEntry,
                                           RbrgUtlFibTblCmpFn);

                    }

                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return;
}
