/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgdbg.c,v 1.4 2012/04/02 13:40:12 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"
#include "rbrgcli.h"
/****************************************************************************
 Function    :  RbrgGetAllFsrbridgeGlobalTable
 Input       :  pRbrgGetFsrbridgeGlobalEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                                pRbrgGetFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    /* Check whether the node is already present */
    pRbrgFsrbridgeGlobalEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) pRbrgGetFsrbridgeGlobalEntry);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgeGlobalTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pRbrgGetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeTrillVersion =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeTrillVersion;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNumPorts =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNumPorts;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.
        u4FsrbridgeUnicastMultipathCount =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeUnicastMultipathCount;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.
        u4FsrbridgeMulticastMultipathCount =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeMulticastMultipathCount;

    pRbrgGetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters;

    if (RbrgGetAllUtlFsrbridgeGlobalTable
        (pRbrgGetFsrbridgeGlobalEntry,
         pRbrgFsrbridgeGlobalEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgeGlobalTable:"
                   "RbrgGetAllUtlFsrbridgeGlobalTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgeNicknameTable
 Input       :  pRbrgGetFsrbridgeNicknameEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                                  pRbrgGetFsrbridgeNicknameEntry)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    /* Check whether the node is already present */
    pRbrgFsrbridgeNicknameEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                   (tRBElem *) pRbrgGetFsrbridgeNicknameEntry);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgeNicknameTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pRbrgGetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority =
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority;

    pRbrgGetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority =
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority;

    pRbrgGetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus =
        pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus;

    if (RbrgGetAllUtlFsrbridgeNicknameTable
        (pRbrgGetFsrbridgeNicknameEntry,
         pRbrgFsrbridgeNicknameEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgeNicknameTable:"
                   "RbrgGetAllUtlFsrbridgeNicknameTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgePortTable
 Input       :  pRbrgGetFsRBridgeBasePortEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                              pRbrgGetFsRBridgeBasePortEntry)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    /* Check whether the node is already present */
    pRbrgFsRBridgeBasePortEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) pRbrgGetFsRBridgeBasePortEntry);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgePortTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortState =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortState;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan;

    pRbrgGetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters;

    MEMCPY (&(pRbrgGetFsRBridgeBasePortEntry->MibObject.FsrbridgePortMac),
            &(pRbrgFsRBridgeBasePortEntry->MibObject.FsrbridgePortMac), 6);

    if (RbrgGetAllUtlFsrbridgePortTable
        (pRbrgGetFsRBridgeBasePortEntry,
         pRbrgFsRBridgeBasePortEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgePortTable:"
                   "RbrgGetAllUtlFsrbridgePortTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgeUniFdbTable
 Input       :  pRbrgGetFsRbridgeUniFdbEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                pRbrgGetFsRbridgeUniFdbEntry)
{
    tRbrgFsRbridgeUniFdbEntry RbrgFsRbridgeUniFdbEntry;

    /* Calling Util function to get the data structure values */
    MEMSET (&RbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    if (RbrgUtlGetFdbEntry (pRbrgGetFsRbridgeUniFdbEntry,
                            &RbrgFsRbridgeUniFdbEntry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgeUniFibTable
 Input       :  pRbrgGetFsRbridgeUniFibEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                pRbrgGetFsRbridgeUniFibEntry)
{
    tRbrgFsRbridgeUniFibEntry RbrgFsRbridgeUniFibEntry;

    MEMSET (&RbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    if (RbrgUtlGetFibEntry (pRbrgGetFsRbridgeUniFibEntry,
                            &RbrgFsRbridgeUniFibEntry) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pRbrgGetFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            RbrgFsRbridgeUniFibEntry.MibObject.au1FsrbridgeFibMacAddress,
            RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibMacAddressLen);

    pRbrgGetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen =
        RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibMacAddressLen;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        RbrgFsRbridgeUniFibEntry.MibObject.u4FsrbridgeFibMtuDesired;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
        RbrgFsRbridgeUniFibEntry.MibObject.u4FsrbridgeFibHopCount;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
        RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibRowstatus;

    if (RbrgGetAllUtlFsrbridgeUniFibTable
        (pRbrgGetFsRbridgeUniFibEntry,
         &RbrgFsRbridgeUniFibEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgeUniFibTable:"
                   "RbrgGetAllUtlFsrbridgeUniFibTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgeMultiFibTable
 Input       :  pRbrgGetFsrbridgeMultiFibEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                  pRbrgGetFsrbridgeMultiFibEntry)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    /* Check whether the node is already present */
    pRbrgFsrbridgeMultiFibEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                   (tRBElem *) pRbrgGetFsrbridgeMultiFibEntry);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgeMultiFibTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pRbrgGetFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
            pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
            pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen);

    pRbrgGetFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen;

    pRbrgGetFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibStatus =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibStatus;

    pRbrgGetFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus;

    if (RbrgGetAllUtlFsrbridgeMultiFibTable
        (pRbrgGetFsrbridgeMultiFibEntry,
         pRbrgFsrbridgeMultiFibEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgeMultiFibTable:"
                   "RbrgGetAllUtlFsrbridgeMultiFibTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllFsrbridgePortCounterTable
 Input       :  pRbrgGetFsRbridgePortCounterEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllFsrbridgePortCounterTable (tRbrgFsRbridgePortCounterEntry *
                                     pRbrgGetFsRbridgePortCounterEntry)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRbridgePortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry RbrgFsRbridgePortEntry;
    tRbrgFsRbridgePortCounterEntry RbrgFsRbridgePortCounterEntry;

    MEMSET (&RbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));
    MEMSET (&RbrgFsRbridgePortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    RbrgFsRbridgePortEntry.MibObject.i4FsrbridgePortIfIndex =
        pRbrgGetFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex;
    /* Check whether the node is already present */
    pRbrgFsRbridgePortEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) & RbrgFsRbridgePortEntry);

    if (pRbrgFsRbridgePortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgePortCounterTable: "
                   "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }
    RbrgFsRbridgePortCounterEntry.MibObject.i4FsrbridgePortIfIndex =
        pRbrgGetFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex;

    if (RbrgGetAllUtlFsrbridgePortCounterTable
        (pRbrgGetFsRbridgePortCounterEntry,
         &RbrgFsRbridgePortCounterEntry) == OSIX_FAILURE)

    {
        RBRG_TRC ((RBRG_UTIL_TRC, "RbrgGetAllFsrbridgePortCounterTable:"
                   "RbrgGetAllUtlFsrbridgePortCounterTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeGlobalTable
 Input       :  pRbrgSetFsrbridgeGlobalEntry
                pRbrgIsSetFsrbridgeGlobalEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                                pRbrgSetFsrbridgeGlobalEntry,
                                tRbrgIsSetFsrbridgeGlobalEntry *
                                pRbrgIsSetFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgFsrbridgeGlobalEntry RbrgOldFsrbridgeGlobalEntry;
    tRbrgFsrbridgeGlobalEntry RbrgFsrbridgeGlobalEntry;

    MEMSET (&RbrgOldFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (&RbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Check whether the node is already present */
    pRbrgFsrbridgeGlobalEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) pRbrgSetFsrbridgeGlobalEntry);
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl != TRUE)
    {

        if (pRbrgFsrbridgeGlobalEntry == NULL)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeGlobalTable: Entry doesn't exist\r\n"));
            return OSIX_FAILURE;
        }

        /*Function to check whether the given input is same as there in database */
        if (FsrbridgeGlobalTableFilterInputs
            (pRbrgFsrbridgeGlobalEntry, pRbrgSetFsrbridgeGlobalEntry,
             pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_TRUE)
        {
            return OSIX_SUCCESS;
        }

        /* Copy the previous values before setting the new values */
        MEMCPY (&RbrgOldFsrbridgeGlobalEntry, pRbrgFsrbridgeGlobalEntry,
                sizeof (tRbrgFsrbridgeGlobalEntry));

        /* Assign values for the existing node */
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeUniMultipathEnable;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeMultiMultipathEnable =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeMultiMultipathEnable;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                u4FsrbridgeNicknameNumber;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeSystemControl;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.
                u4FsrbridgeUnicastMultipathCount =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                u4FsrbridgeUnicastMultipathCount;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.
                u4FsrbridgeMulticastMultipathCount =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                u4FsrbridgeMulticastMultipathCount;
        }
        if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters !=
            OSIX_FALSE)
        {
            pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters =
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeClearCounters;
        }
    }
    else
    {
        if (pRbrgFsrbridgeGlobalEntry == NULL)
        {

            if (pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl
                == RBRG_SHUT)
            {
                /* Already Module is shutdown */
                return OSIX_SUCCESS;
            }
        }
        else
        {
            if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl ==
                pRbrgSetFsrbridgeGlobalEntry->MibObject.
                i4FsrbridgeSystemControl)
            {
                /* Values are same */
                return OSIX_SUCCESS;
            }
        }
        RbrgFsrbridgeGlobalEntry.MibObject.i4FsrbridgeSystemControl =
            pRbrgSetFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl;
        RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeContextId =
            pRbrgSetFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId;
        pRbrgFsrbridgeGlobalEntry = &RbrgFsrbridgeGlobalEntry;
    }

    if (RbrgUtilUpdateFsrbridgeGlobalTable
        (&RbrgOldFsrbridgeGlobalEntry, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {
        if (RbrgSetAllFsrbridgeGlobalTableTrigger (pRbrgSetFsrbridgeGlobalEntry,
                                                   pRbrgIsSetFsrbridgeGlobalEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pRbrgFsrbridgeGlobalEntry, &RbrgOldFsrbridgeGlobalEntry,
                sizeof (tRbrgFsrbridgeGlobalEntry));
        return OSIX_FAILURE;
    }

    if (RbrgSetAllFsrbridgeGlobalTableTrigger (pRbrgSetFsrbridgeGlobalEntry,
                                               pRbrgIsSetFsrbridgeGlobalEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeNicknameTable
 Input       :  pRbrgSetFsrbridgeNicknameEntry
                pRbrgIsSetFsrbridgeNicknameEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                                  pRbrgSetFsrbridgeNicknameEntry,
                                  tRbrgIsSetFsrbridgeNicknameEntry *
                                  pRbrgIsSetFsrbridgeNicknameEntry)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgFsrbridgeNicknameEntry RbrgOldFsrbridgeNicknameEntry;
    tRbrgFsrbridgeNicknameEntry *pRbrgNickNameEntry;
    tRbrgFsrbridgeNicknameEntry RbrgNickNameEntry;

    MEMSET (&RbrgOldFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    if ((pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus ==
         RBRG_STATIC) ||
        (pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus ==
         RBRG_DYNAMIC))
    {

        RbrgNickNameEntry.MibObject.u4FsrbridgeContextId =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
        RbrgNickNameEntry.MibObject.i4FsrbridgeNicknameName =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName;
        pRbrgNickNameEntry = (tRbrgFsrbridgeNicknameEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                       (tRBElem *) & RbrgNickNameEntry);
        if (pRbrgNickNameEntry == NULL)
        {
            pRbrgNickNameEntry = (tRbrgFsrbridgeNicknameEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);
            if (pRbrgNickNameEntry == NULL)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeNicknameTable: "
                           "Memory allocation failed for nickname entry\r\n"));
                return OSIX_FAILURE;
            }
            pRbrgNickNameEntry->MibObject.u4FsrbridgeContextId =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
            pRbrgNickNameEntry->MibObject.i4FsrbridgeNicknameStatus =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.
                i4FsrbridgeNicknameStatus;
            pRbrgNickNameEntry->MibObject.i4FsrbridgeNicknameName =
                pRbrgSetFsrbridgeNicknameEntry->MibObject.
                i4FsrbridgeNicknameName;
            pRbrgNickNameEntry->MibObject.u4FsrbridgeNicknamePriority =
                RBRG_DEFAULT_PRIORITY;
            pRbrgNickNameEntry->MibObject.u4FsrbridgeNicknameDtrPriority =
                RBRG_DEFAULT_DTRPRIORITY;
            RBTreeAdd (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                       (tRBElem *) pRbrgNickNameEntry);
        }
    }
    else
    {

        RbrgNickNameEntry.MibObject.u4FsrbridgeContextId =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;
        RbrgNickNameEntry.MibObject.i4FsrbridgeNicknameName =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName;
        pRbrgNickNameEntry = (tRbrgFsrbridgeNicknameEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                       (tRBElem *) & RbrgNickNameEntry);
        if (pRbrgNickNameEntry == NULL)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeNicknameTable: "
                       "nickname entry does not exist\r\n"));
            return OSIX_FAILURE;
        }
    }

    pRbrgFsrbridgeNicknameEntry = pRbrgNickNameEntry;

    /*Function to check whether the given input is same as there in database */
    if (FsrbridgeNicknameTableFilterInputs
        (pRbrgFsrbridgeNicknameEntry, pRbrgSetFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&RbrgOldFsrbridgeNicknameEntry, pRbrgFsrbridgeNicknameEntry,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    /* Assign values for the existing node */
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority !=
        OSIX_FALSE)
    {
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknamePriority;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority !=
        OSIX_FALSE)
    {
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.
            u4FsrbridgeNicknameDtrPriority;
    }
    if (pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus !=
        OSIX_FALSE)
    {
        pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus =
            pRbrgSetFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus;
    }

    if (RbrgUtilUpdateFsrbridgeNicknameTable
        (&RbrgOldFsrbridgeNicknameEntry, pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {
        if (RbrgSetAllFsrbridgeNicknameTableTrigger
            (pRbrgSetFsrbridgeNicknameEntry, pRbrgIsSetFsrbridgeNicknameEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pRbrgFsrbridgeNicknameEntry, &RbrgOldFsrbridgeNicknameEntry,
                sizeof (tRbrgFsrbridgeNicknameEntry));
        return OSIX_FAILURE;
    }

    if (RbrgSetAllFsrbridgeNicknameTableTrigger (pRbrgSetFsrbridgeNicknameEntry,
                                                 pRbrgIsSetFsrbridgeNicknameEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgePortTable
 Input       :  pRbrgSetFsRBridgeBasePortEntry
                pRbrgIsSetFsRBridgeBasePortEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                              pRbrgSetFsRBridgeBasePortEntry,
                              tRbrgIsSetFsRBridgeBasePortEntry *
                              pRbrgIsSetFsRBridgeBasePortEntry)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry RbrgOldFsRBridgeBasePortEntry;

    MEMSET (&RbrgOldFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Check whether the node is already present */
    pRbrgFsRBridgeBasePortEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) pRbrgSetFsRBridgeBasePortEntry);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgePortTable: Entry doesn't exist\r\n"));
        CLI_SET_ERR (RBRG_CLI_PORT_ENTRY_NOT_FOUND);
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsrbridgePortTableFilterInputs
        (pRbrgFsRBridgeBasePortEntry, pRbrgSetFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&RbrgOldFsRBridgeBasePortEntry, pRbrgFsRBridgeBasePortEntry,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign values for the existing node */
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable != OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort != OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort !=
        OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning !=
        OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortDisableLearning;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan != OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan;
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters !=
        OSIX_FALSE)
    {
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters =
            pRbrgSetFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortClearCounters;
    }

    if (RbrgUtilUpdateFsrbridgePortTable
        (&RbrgOldFsRBridgeBasePortEntry, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {
        if (RbrgSetAllFsrbridgePortTableTrigger (pRbrgSetFsRBridgeBasePortEntry,
                                                 pRbrgIsSetFsRBridgeBasePortEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pRbrgFsRBridgeBasePortEntry, &RbrgOldFsRBridgeBasePortEntry,
                sizeof (tRbrgFsRBridgeBasePortEntry));
        return OSIX_FAILURE;
    }

    if (RbrgSetAllFsrbridgePortTableTrigger (pRbrgSetFsRBridgeBasePortEntry,
                                             pRbrgIsSetFsRBridgeBasePortEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeUniFdbTable
 Input       :  pRbrgSetFsRbridgeUniFdbEntry
                pRbrgIsSetFsRbridgeUniFdbEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                pRbrgSetFsRbridgeUniFdbEntry,
                                tRbrgIsSetFsRbridgeUniFdbEntry *
                                pRbrgIsSetFsRbridgeUniFdbEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    return (RbrgUtlSetAllFsrbridgeUniFdbTable (pRbrgSetFsRbridgeUniFdbEntry,
                                               pRbrgIsSetFsRbridgeUniFdbEntry,
                                               i4RowStatusLogic,
                                               i4RowCreateOption));
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeUniFibTable
 Input       :  pRbrgSetFsRbridgeUniFibEntry
                pRbrgIsSetFsRbridgeUniFibEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                pRbrgSetFsRbridgeUniFibEntry,
                                tRbrgIsSetFsRbridgeUniFibEntry *
                                pRbrgIsSetFsRbridgeUniFibEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    return (RbrgUtlSetAllFsrbridgeUniFibTable (pRbrgSetFsRbridgeUniFibEntry,
                                               pRbrgIsSetFsRbridgeUniFibEntry,
                                               i4RowStatusLogic,
                                               i4RowCreateOption));
}

/****************************************************************************
 Function    :  RbrgSetAllFsrbridgeMultiFibTable
 Input       :  pRbrgSetFsrbridgeMultiFibEntry
                pRbrgIsSetFsrbridgeMultiFibEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgSetAllFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                  pRbrgSetFsrbridgeMultiFibEntry,
                                  tRbrgIsSetFsrbridgeMultiFibEntry *
                                  pRbrgIsSetFsrbridgeMultiFibEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;
    tRbrgFsrbridgeMultiFibEntry *pRbrgOldFsrbridgeMultiFibEntry = NULL;
    tRbrgFsrbridgeMultiFibEntry *pRbrgTrgFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbrgTrgIsSetFsrbridgeMultiFibEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pRbrgOldFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
    if (pRbrgOldFsrbridgeMultiFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pRbrgTrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
    if (pRbrgTrgFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        return OSIX_FAILURE;
    }
    pRbrgTrgIsSetFsrbridgeMultiFibEntry =
        (tRbrgIsSetFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);
    if (pRbrgTrgIsSetFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pRbrgOldFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgTrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgTrgIsSetFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));

    /* Check whether the node is already present */
    pRbrgFsrbridgeMultiFibEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                   (tRBElem *) pRbrgSetFsrbridgeMultiFibEntry);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pRbrgSetFsrbridgeMultiFibEntry->MibObject.
             i4FsrbridgeMultiFibRowStatus == CREATE_AND_WAIT)
            || (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                i4FsrbridgeMultiFibRowStatus == CREATE_AND_GO)
            ||
            ((pRbrgSetFsrbridgeMultiFibEntry->MibObject.
              i4FsrbridgeMultiFibRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pRbrgFsrbridgeMultiFibEntry =
                (tRbrgFsrbridgeMultiFibEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
            if (pRbrgFsrbridgeMultiFibEntry == NULL)
            {
                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: "
                               "RbrgSetAllFsrbridgeMultiFibTableTrigger function "
                               "fails\r\n"));

                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: "
                           "Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
                    sizeof (tRbrgFsrbridgeMultiFibEntry));
            if ((RbrgInitializeFsrbridgeMultiFibTable
                 (pRbrgFsrbridgeMultiFibEntry)) == OSIX_FAILURE)
            {
                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: "
                               "RbrgSetAllFsrbridgeMultiFibTableTrigger "
                               "function fails\r\n"));

                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: "
                           "Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname !=
                OSIX_FALSE)
            {
                pRbrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibNickname =
                    pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibNickname;
            }

            if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts !=
                OSIX_FALSE)
            {
                MEMCPY (pRbrgFsrbridgeMultiFibEntry->MibObject.
                        au1FsrbridgeMultiFibPorts,
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        au1FsrbridgeMultiFibPorts,
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibPortsLen);

                pRbrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibPortsLen =
                    pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibPortsLen;
            }

            if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus !=
                OSIX_FALSE)
            {
                pRbrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus =
                    pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus;
            }

            if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId !=
                OSIX_FALSE)
            {
                pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
                    pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                    u4FsrbridgeContextId;
            }

            if ((pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                 i4FsrbridgeMultiFibRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibRowStatus == ACTIVE)))
            {
                pRbrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus = ACTIVE;
            }
            else if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                     i4FsrbridgeMultiFibRowStatus == CREATE_AND_WAIT)
            {
                pRbrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                 (tRBElem *) pRbrgFsrbridgeMultiFibEntry) != RB_SUCCESS)
            {
                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                return OSIX_FAILURE;
            }
            if (RbrgUtilUpdateFsrbridgeMultiFibTable
                (NULL, pRbrgFsrbridgeMultiFibEntry,
                 pRbrgIsSetFsrbridgeMultiFibEntry) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: RbrgUtilUpdateFsrbridgeMultiFibTable function returns failure.\r\n"));

                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                           pRbrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                return OSIX_FAILURE;
            }

            if ((pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                 i4FsrbridgeMultiFibRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibRowStatus == ACTIVE)))
            {

                if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus == CREATE_AND_GO)
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibNickname =
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibNickname;
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibRowStatus = CREATE_AND_GO;
                    pRbrgTrgIsSetFsrbridgeMultiFibEntry->
                        bFsrbridgeMultiFibRowStatus = OSIX_TRUE;

                }
                else
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibNickname =
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibNickname;
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                        i4FsrbridgeMultiFibRowStatus = CREATE_AND_WAIT;
                    pRbrgTrgIsSetFsrbridgeMultiFibEntry->
                        bFsrbridgeMultiFibRowStatus = OSIX_TRUE;

                }
                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbrgOldFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock
                        (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                         (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                     i4FsrbridgeMultiFibRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pRbrgTrgFsrbridgeMultiFibEntry->MibObject.
                    i4FsrbridgeMultiFibRowStatus = CREATE_AND_WAIT;
                pRbrgTrgIsSetFsrbridgeMultiFibEntry->
                    bFsrbridgeMultiFibRowStatus = OSIX_TRUE;

                if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                    (pRbrgSetFsrbridgeMultiFibEntry,
                     pRbrgIsSetFsrbridgeMultiFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbrgOldFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock
                        (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                         (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
                    return OSIX_FAILURE;
                }
            }
            if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                (pRbrgSetFsrbridgeMultiFibEntry,
                 pRbrgIsSetFsrbridgeMultiFibEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable:  RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                (pRbrgSetFsrbridgeMultiFibEntry,
                 pRbrgIsSetFsrbridgeMultiFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: Failure.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pRbrgSetFsrbridgeMultiFibEntry->MibObject.
              i4FsrbridgeMultiFibRowStatus == CREATE_AND_WAIT)
             || (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                 i4FsrbridgeMultiFibRowStatus == CREATE_AND_GO))
    {
        if (RbrgSetAllFsrbridgeMultiFibTableTrigger
            (pRbrgSetFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeMultiFibTable: The row is already present.\r\n"));
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pRbrgOldFsrbridgeMultiFibEntry, pRbrgFsrbridgeMultiFibEntry,
            sizeof (tRbrgFsrbridgeMultiFibEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
        i4FsrbridgeMultiFibRowStatus == DESTROY)
    {
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
            DESTROY;

        if (RbrgUtilUpdateFsrbridgeMultiFibTable
            (pRbrgOldFsrbridgeMultiFibEntry, pRbrgFsrbridgeMultiFibEntry,
             pRbrgIsSetFsrbridgeMultiFibEntry) != OSIX_SUCCESS)
        {

            if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                (pRbrgSetFsrbridgeMultiFibEntry,
                 pRbrgIsSetFsrbridgeMultiFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: RbrgUtilUpdateFsrbridgeMultiFibTable function returns failure.\r\n"));
        }
        RBTreeRem (gRbrgGlobals.RbrgGlbMib.FsrbridgeMultiFibTable,
                   pRbrgFsrbridgeMultiFibEntry);
        if (RbrgSetAllFsrbridgeMultiFibTableTrigger
            (pRbrgSetFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsrbridgeMultiFibTableFilterInputs
        (pRbrgFsrbridgeMultiFibEntry, pRbrgSetFsrbridgeMultiFibEntry,
         pRbrgIsSetFsrbridgeMultiFibEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus ==
         ACTIVE)
        && (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibRowStatus != NOT_IN_SERVICE))
    {
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pRbrgTrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
            NOT_IN_SERVICE;
        pRbrgTrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus =
            OSIX_TRUE;

        if (RbrgUtilUpdateFsrbridgeMultiFibTable
            (pRbrgOldFsrbridgeMultiFibEntry, pRbrgFsrbridgeMultiFibEntry,
             pRbrgIsSetFsrbridgeMultiFibEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pRbrgFsrbridgeMultiFibEntry, pRbrgOldFsrbridgeMultiFibEntry,
                    sizeof (tRbrgFsrbridgeMultiFibEntry));
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable:                 RbrgUtilUpdateFsrbridgeMultiFibTable Function returns failure.\r\n"));

            if (RbrgSetAllFsrbridgeMultiFibTableTrigger
                (pRbrgSetFsrbridgeMultiFibEntry,
                 pRbrgIsSetFsrbridgeMultiFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
            return OSIX_FAILURE;
        }

        if (RbrgSetAllFsrbridgeMultiFibTableTrigger
            (pRbrgTrgFsrbridgeMultiFibEntry,
             pRbrgTrgIsSetFsrbridgeMultiFibEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
        }
    }

    if (pRbrgSetFsrbridgeMultiFibEntry->MibObject.
        i4FsrbridgeMultiFibRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts != OSIX_FALSE)
    {
        MEMCPY (pRbrgFsrbridgeMultiFibEntry->MibObject.
                au1FsrbridgeMultiFibPorts,
                pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                au1FsrbridgeMultiFibPorts,
                pRbrgSetFsrbridgeMultiFibEntry->MibObject.
                i4FsrbridgeMultiFibPortsLen);

        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen =
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibPortsLen;
    }
    if (pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus !=
        OSIX_FALSE)
    {
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
            pRbrgSetFsrbridgeMultiFibEntry->MibObject.
            i4FsrbridgeMultiFibRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
            ACTIVE;
    }

    if (RbrgUtilUpdateFsrbridgeMultiFibTable (pRbrgOldFsrbridgeMultiFibEntry,
                                              pRbrgFsrbridgeMultiFibEntry,
                                              pRbrgIsSetFsrbridgeMultiFibEntry)
        != OSIX_SUCCESS)
    {

        if (RbrgSetAllFsrbridgeMultiFibTableTrigger
            (pRbrgSetFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));

        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeMultiFibTable: RbrgUtilUpdateFsrbridgeMultiFibTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pRbrgFsrbridgeMultiFibEntry, pRbrgOldFsrbridgeMultiFibEntry,
                sizeof (tRbrgFsrbridgeMultiFibEntry));
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
        return OSIX_FAILURE;

    }
    if (RbrgSetAllFsrbridgeMultiFibTableTrigger (pRbrgSetFsrbridgeMultiFibEntry,
                                                 pRbrgIsSetFsrbridgeMultiFibEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeMultiFibTable: RbrgSetAllFsrbridgeMultiFibTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgOldFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgTrgFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgTrgIsSetFsrbridgeMultiFibEntry);
    return OSIX_SUCCESS;

}
