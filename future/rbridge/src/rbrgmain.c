/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rbrgmain.c,v 1.3 2012/02/02 13:33:03 siva Exp $
* Description: This file contains the rbrg task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __RBRGMAIN_C__
#include "rbrginc.h"

/* Proto types of the functions private to this file only */

PRIVATE UINT4 RbrgMainMemInit PROTO ((VOID));
PRIVATE VOID RbrgMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : RbrgMainTask                                               *
*                                                                           *
* Description  : Main function of RBRG.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
*
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgMainTask ()
{
    UINT4               u4Event = 0;

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTaskMain\n"));

    if (TmrCreateTimerList ((CONST UINT1 *) RBRG_TASK_NAME, RBRG_TIMER_EVENT,
                            NULL, (tTimerListId *) & (gRbrgGlobals.rbrgTmrLst))
        == TMR_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC, "RBRG Timer List Creation Failed\n"));
        return;
    }
    while (OSIX_TRUE == OSIX_TRUE)
    {

        OsixEvtRecv (gRbrgGlobals.rbrgTaskId, RBRG_ALL_EVENTS,
                     (OSIX_WAIT | OSIX_EV_ANY), (UINT4 *) &(u4Event));
        RbrgMainTaskLock ();

        if ((u4Event & RBRG_TIMER_EVENT) == RBRG_TIMER_EVENT)
        {
            RBRG_TRC ((RBRG_MAIN_TRC, "RBRG_TIMER_EVENT\n"));
            RbrgTmrHandleExpiry ();
        }

        if ((u4Event & RBRG_QMSG_EVENT) == RBRG_QMSG_EVENT)
        {
            RBRG_TRC ((RBRG_MAIN_TRC, "RBRG_QMSG_EVENT\n"));

            RbrgQueProcessMsgs ();
        }

        /* Mutual exclusion flag OFF */
        RbrgMainTaskUnLock ();
    }
}

/****************************************************************************
*                                                                           *
* Function     : RbrgMainTaskInit                                           *
*                                                                           *
* Description  : RBRG initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
RbrgMainTaskInit (VOID)
{

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgMainTaskInit\n"));
#ifdef RBRG_TRACE_WANTED
    RBRG_TRC_FLAG = ((UINT4) 0);
#endif

    MEMSET (&gRbrgGlobals, 0, sizeof (gRbrgGlobals));
    MEMCPY (gRbrgGlobals.au1TaskSemName, RBRG_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);
    if (OsixCreateSem (RBRG_MUT_EXCL_SEM_NAME, RBRG_SEM_CREATE_INIT_CNT, 0,
                       &gRbrgGlobals.rbrgTaskSemId) == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC,
                   "Seamphore Creation failure for %s \n",
                   RBRG_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    /* Create buffer pools for data structures */

    if (RbrgMainMemInit () == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (RBRG_QUEUE_NAME, RBRG_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gRbrgGlobals.rbrgQueId)) == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC, "RBridge Q Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (RbrgUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    RbrgTmrInitTmrDesc ();

    /* Start RBridge module */
    if (RbrgModuleInit () == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC, "RBridge Module start failed\r\n"));
        return OSIX_FAILURE;
    }
    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC:RbrgMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
RbrgMainDeInit (VOID)
{

    RbrgMainMemClear ();

    if (gRbrgGlobals.rbrgTaskSemId)
    {
        OsixSemDel (gRbrgGlobals.rbrgTaskSemId);
    }
    if (gRbrgGlobals.rbrgQueId)
    {
        OsixQueDel (gRbrgGlobals.rbrgQueId);
    }
    TmrDeleteTimerList (gRbrgGlobals.rbrgTmrLst);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Rbrg Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RbrgMainTaskLock (VOID)
{
    /*RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgMainTaskLock\n")); */

    if (OsixSemTake (gRbrgGlobals.rbrgTaskSemId) == OSIX_FAILURE)
    {
        RBRG_TRC ((RBRG_MAIN_TRC,
                   "TakeSem failure for %s \n", RBRG_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    /*RBRG_TRC_FUNC ((RBRG_FN_EXIT, "EXIT:RbrgMainTaskLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the RBRG Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RbrgMainTaskUnLock (VOID)
{
    /*RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgMainTaskUnLock\n")); */

    OsixSemGive (gRbrgGlobals.rbrgTaskSemId);

    /*RBRG_TRC_FUNC ((RBRG_FN_EXIT, "EXIT:RbrgMainTaskUnLock\n")); */
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for RBRG         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
RbrgMainMemInit (VOID)
{
    if (RbrgSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : RbrgMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RbrgMainMemClear (VOID)
{
    RbrgSizingMemDeleteMemPools ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgmain.c                     */
/*-----------------------------------------------------------------------*/
