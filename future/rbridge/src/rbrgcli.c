
/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgcli.c,v 1.3 2012/02/02 13:33:03 siva Exp $
*
* Description: This file contains the RBRG SHOW CMD related routines 
*********************************************************************/
#ifndef __RBRGCLI_C__
#define __RBRGCLI_C__

#include "rbrginc.h"
#include "rbrgcli.h"

PRIVATE VOID        RbrgCliShowAllGlobalInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowCxtGlobalInfo (tCliHandle CliHandle,
                                              UINT4 u4ContextId);
PRIVATE VOID        RbrgCliShowGlobalInfo (tCliHandle CliHandle,
                                           UINT1 *pu1ContextId);
PRIVATE VOID        RbrgCliShowCounters (tCliHandle CliHandle, INT4 *pi4PortId);
PRIVATE VOID        RbrgCliShowAllCounters (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowPortCounters (tCliHandle CliHandle,
                                             INT4 i4IfIndex);
PRIVATE VOID        RbrgCliShowPortInfo (tCliHandle CliHandle, INT4 *pi4PortId);
PRIVATE VOID        RbrgCliShowAllPortInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowIfPortInfo (tCliHandle CliHandle,
                                           INT4 i4IfIndex);
PRIVATE VOID        RbrgCliShowFDBInfo (tCliHandle CliHandle,
                                        UINT1 *pu1ContextId, UINT4 *pu4FdbId,
                                        UINT1 *pu1DestMac);
PRIVATE VOID        RbrgCliShowAllFDBInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowFDBSwitchInfo (tCliHandle CliHandle,
                                              UINT4 u4ContextId);
PRIVATE VOID        RbrgCliShowFDBMacInfo (tCliHandle CliHandle,
                                           UINT4 u4ContextId, UINT4 u4FdbId,
                                           tMacAddr FdbDestMac);
PRIVATE VOID        RbrgCliShowFIBInfo (tCliHandle CliHandle,
                                        UINT1 *pu1ContextId, UINT4 *pu4Nickname,
                                        UINT4 *pu4Port, UINT4 *pu4NextHopNick);
PRIVATE VOID        RbrgCliShowAllFIBInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowFIBSwitchInfo (tCliHandle CliHandle,
                                              UINT4 u4ContextId);
PRIVATE VOID        RbrgCliShowFIBNicknameInfo (tCliHandle CliHandle,
                                                UINT4 u4ContextId,
                                                INT4 u4FibNick, INT4 i4FibPort,
                                                INT4 i4FibNextHopNick);
PRIVATE VOID        RbrgCliShowMultiFIBInfo (tCliHandle CliHandle,
                                             UINT1 *pu1ContextId,
                                             UINT4 *pu4Nickname);
PRIVATE VOID        RbrgCliShowAllMultiFIBInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowMultiFIBSwitchInfo (tCliHandle CliHandle,
                                                   UINT4 u4ContextId);
PRIVATE VOID        RbrgCliShowMultiFIBNicknameInfo (tCliHandle CliHandle,
                                                     UINT4 u4ContextId,
                                                     INT4 i4FibNick);
PRIVATE VOID        RbrgCliShowNicknameInfo (tCliHandle CliHandle,
                                             UINT1 *pu1ContextId,
                                             UINT4 *pu4Nickname);
PRIVATE VOID        RbrgCliShowAllNicknameInfo (tCliHandle CliHandle);
PRIVATE VOID        RbrgCliShowNicknameSwitchInfo (tCliHandle CliHandle,
                                                   UINT4 u4ContextId);
PRIVATE VOID        RbrgCliShowNicknameNameInfo (tCliHandle CliHandle,
                                                 UINT4 u4ContextId,
                                                 INT4 i4Nick);
/*****************************************************************************
 *                                                                           *
 * Function     : cli_process_rbrg_show_cmd                                  *
 *                                                                           *
 * Description  : This function is exported to CLI module to handle the      *
 *                RBRG cli show commands to take the corresponding action    *
 *                                                                           *
 * Input        : Variable arguments                                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
cli_process_rbrg_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RBRG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst;
    UINT4               u4ErrCode = 0;
    CliRegisterLock (CliHandle, RbrgMainTaskLock, RbrgMainTaskUnLock);
    RBRG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RBRG_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
            /* args[0] - ContextId */
        case CLI_RBRG_SHOW_GLOB_CONF:
            RbrgCliShowGlobalInfo (CliHandle, ((UINT1 *) args[0]));
            break;
            /* args[0] - Port Index */
        case CLI_RBRG_SHOW_COUNTERS:
            RbrgCliShowCounters (CliHandle, (INT4 *) &u4IfIndex);
            break;
            /* args[0] - Port Index */
        case CLI_RBRG_SHOW_PORT_INFO:
            RbrgCliShowPortInfo (CliHandle, (INT4 *) &u4IfIndex);
            break;
            /* args[0] - Context Id 
             * args[1] - FDB Id
             * args[2] - Dest Mac */
        case CLI_RBRG_SHOW_FDB_INFO:
            RbrgCliShowFDBInfo (CliHandle, ((UINT1 *) args[0]),
                                ((UINT4 *) args[1]), ((UINT1 *) args[2]));
            break;
            /* args[0] - Context Id 
             * args[1] - Nickname
             * args[2] - port Index 
             * args[3] - NextHop Nickname*/
        case CLI_RBRG_SHOW_FIB_INFO:
            RbrgCliShowFIBInfo (CliHandle, ((UINT1 *) args[0]),
                                ((UINT4 *) args[1]), ((UINT4 *) args[2]),
                                ((UINT4 *) args[3]));
            break;
            /* args[0] - Context Id 
             * args[1] - Nickname */
        case CLI_RBRG_SHOW_MULTI_FIB_INFO:
            RbrgCliShowMultiFIBInfo (CliHandle, ((UINT1 *) args[0]),
                                     ((UINT4 *) args[1]));
            break;
            /* args[0] - Context Id
             * args[1] - Nickname */
        case CLI_RBRG_SHOW_NICKNAME_INFO:
            RbrgCliShowNicknameInfo (CliHandle, ((UINT1 *) args[0]),
                                     ((UINT4 *) args[1]));
            break;
        default:
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RBRG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", RbrgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    RBRG_UNLOCK;

    return i4RetStatus;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowGlobalInfo                                   *
 *                                                                           *
 * Description  : This function is used to display the Global Table Info     *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowGlobalInfo (tCliHandle CliHandle, UINT1 *pu1ContextId)
{
    UINT4               u4ContextId = 0;

    CliPrintf (CliHandle, "R-Bridge Global Info \r\n");
    CliPrintf (CliHandle, "---------------------\r\n");
    if (pu1ContextId == NULL)
    {
        RbrgCliShowAllGlobalInfo (CliHandle);
    }
    else
    {
        if (RbrgPortVcmIsSwitchExist (pu1ContextId, &u4ContextId) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (RBRG_CLI_INVALID_CONTEXT_ID);
        }

        RbrgCliShowCxtGlobalInfo (CliHandle, u4ContextId);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllGlobalInfo                                   *
 *                                                                           *
 * Description  : This function is used to display the Global Table Info     *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllGlobalInfo (tCliHandle CliHandle)
{
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    INT4                i4RetStatus = 0;

    nmhGetFirstIndexFsrbridgeGlobalTable (&u4NextContextId);
    do
    {
        u4ContextId = u4NextContextId;
        RbrgCliShowCxtGlobalInfo (CliHandle, u4ContextId);
        i4RetStatus = nmhGetNextIndexFsrbridgeGlobalTable (u4ContextId,
                                                           &u4NextContextId);
    }
    while (i4RetStatus != SNMP_FAILURE);

}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowCxtGlobalInfo                                   *
 *                                                                           *
 * Description  : This function is used to display the Global Table Info     *
 *                of a particular context                                    *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowCxtGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4RbrgVersion = 0;
    UINT4               u4NumPorts = 0;
    UINT4               u4NickNameNumber = 0;
    UINT4               u4SysMode = 0;
    INT4                i4SysControl = 0;
    INT4                i4ModuleStatus = 0;
    INT4                i4UnicastMultiPathEnable = FALSE;
    INT4                i4MulticastMultiPathEnable = FALSE;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN] = { 0 };

    if (nmhValidateIndexInstanceFsrbridgeGlobalTable (u4ContextId) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "R-Bridge Global Information\r\n");
        CliPrintf (CliHandle, "---------------------------\r\n");

        u4SysMode = VcmGetSystemMode (RBRG_PROTOCOL_ID);
        if (u4SysMode == VCM_MI_MODE)
        {
            MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
            VcmGetAliasName (u4ContextId, au1ContextName);
            CliPrintf (CliHandle, "\rswitch  %s \r\n\r\n", au1ContextName);
        }

        nmhGetFsrbridgeSystemControl (u4ContextId, &i4SysControl);
        if (i4SysControl == RBRG_START)
        {
            CliPrintf (CliHandle, "System Control : STARTED\r\n");
            nmhGetFsrbridgeModuleStatus (u4ContextId, &i4ModuleStatus);
            if (i4ModuleStatus == RBRG_ENABLE)
            {
                CliPrintf (CliHandle, "Module Status : ENABLED\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Module Status : DISABLED\r\n");
            }
            nmhGetFsrbridgeTrillVersion (u4ContextId, &u4RbrgVersion);
            CliPrintf (CliHandle, "RBRG Version : %d\r\n", u4RbrgVersion);
            nmhGetFsrbridgeNumPorts (u4ContextId, &u4NumPorts);
            CliPrintf (CliHandle, "Number of Ports : %d\r\n", u4NumPorts);
            nmhGetFsrbridgeUniMultipathEnable (u4ContextId,
                                               &i4UnicastMultiPathEnable);
            if (i4UnicastMultiPathEnable == TRUE)
            {
                CliPrintf (CliHandle, "Unicast Multipath Status : ENABLED\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Unicast Multipath Status : "
                           "DISABLED\r\n");
            }
            nmhGetFsrbridgeMultiMultipathEnable (u4ContextId,
                                                 &i4MulticastMultiPathEnable);
            if (i4MulticastMultiPathEnable == TRUE)
            {
                CliPrintf (CliHandle, "Multicast Multipath Status : "
                           "ENABLED\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Multicast Multipath Status : "
                           "DISABLED\r\n");
            }
            nmhGetFsrbridgeNicknameNumber (u4ContextId, &u4NickNameNumber);
            CliPrintf (CliHandle, "Number of nicknames : %d\r\n",
                       u4NickNameNumber);
        }
        else
        {
            CliPrintf (CliHandle, "RBridge is shutdown\r\n");
        }
        CliPrintf (CliHandle, "-------------------------------------\r\n");
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowConters                                         *
 *                                                                           *
 * Description  : This function is used to display the port counters in a    *
 *                specific port                                              *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowCounters (tCliHandle CliHandle, INT4 *pi4PortId)
{
    CliPrintf (CliHandle, "R-Bridge Counters\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");
    CliPrintf (CliHandle, "%-9s %-10s %-10s %-10s %-10s %-10s\r\n",
               "IfName", "RPF-Failed", "HopCountExceeded",
               "PortOptions", "TrillInFrames", "TrillOutFrames");
    if (pi4PortId == NULL)
    {
        return;
    }
    if (*pi4PortId == 0)
    {
        RbrgCliShowAllCounters (CliHandle);
    }
    else
    {
        RbrgCliShowPortCounters (CliHandle, *pi4PortId);
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllConters                                      *
 *                                                                           *
 * Description  : This function is used to display the port counters         *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllCounters (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsrbridgePortCounterTable (&i4NextIfIndex);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            i4IfIndex = i4NextIfIndex;
            RbrgCliShowPortCounters (CliHandle, i4IfIndex);
            i4RetVal = nmhGetNextIndexFsrbridgePortCounterTable (i4IfIndex,
                                                                 &i4NextIfIndex);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowConters                                         *
 *                                                                           *
 * Description  : This function is used to display the port counters of all  *
 *                ports on an R-Bridge                                       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                i4IfIndex - Index of the port                              *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowPortCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tSNMP_COUNTER64_TYPE TrillInFrames;
    tSNMP_COUNTER64_TYPE TrillOutFrames;
    FS_UINT8            u8RbrgInFrames;
    FS_UINT8            u8RbrgOutFrames;
    UINT4               u4RPFCheckFailed = 0;
    UINT4               u4HopCountExceeded = 0;
    UINT4               u4PortOptions = 0;
    UINT1               au1TrillInFrames[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1TrillOutFrames[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { 0 };
    INT1               *piIfName;

    MEMSET (&TrillInFrames, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TrillOutFrames, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8RbrgInFrames, 0, sizeof (FS_UINT8));
    MEMSET (&u8RbrgOutFrames, 0, sizeof (FS_UINT8));
    MEMSET (&au1TrillInFrames, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (&au1TrillOutFrames, 0, CFA_CLI_U8_STR_LENGTH);

    piIfName = (INT1 *) &au1IfName[0];

    if (nmhValidateIndexInstanceFsrbridgePortCounterTable (i4IfIndex) !=
        SNMP_FAILURE)
    {
        CfaCliGetIfName (i4IfIndex, piIfName);
        nmhGetFsrbridgePortRpfChecksFailed (i4IfIndex, &u4RPFCheckFailed);
        nmhGetFsrbridgePortHopCountsExceeded (i4IfIndex, &u4HopCountExceeded);
        nmhGetFsrbridgePortOptions (i4IfIndex, &u4PortOptions);
        nmhGetFsrbridgePortTrillInFrames (i4IfIndex, &TrillInFrames);
        nmhGetFsrbridgePortTrillOutFrames (i4IfIndex, &TrillOutFrames);
        u8RbrgInFrames.u4Hi = TrillInFrames.msn;
        u8RbrgInFrames.u4Lo = TrillInFrames.lsn;
        u8RbrgOutFrames.u4Hi = TrillOutFrames.msn;
        u8RbrgOutFrames.u4Lo = TrillOutFrames.lsn;
        FSAP_U8_2STR (&u8RbrgInFrames, (CHR1 *) & au1TrillInFrames);
        FSAP_U8_2STR (&u8RbrgOutFrames, (CHR1 *) & au1TrillOutFrames);
        CliPrintf (CliHandle, "%-9s %-10d %-10d %-10d %-10s %-10s\r\n",
                   piIfName, u4RPFCheckFailed, u4HopCountExceeded,
                   u4PortOptions, au1TrillInFrames, au1TrillOutFrames);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowPortInfo                                        *
 *                                                                           *
 * Description  : This function is used to display the port table info       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                i4IfIndex - Index of the port                              *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PRIVATE VOID
RbrgCliShowPortInfo (tCliHandle CliHandle, INT4 *pi4PortId)
{
    CliPrintf (CliHandle, "R-Bridge Port Details \r\n");
    CliPrintf (CliHandle, "--------------------- \r\n");
    if (*pi4PortId == 0)
    {
        RbrgCliShowAllPortInfo (CliHandle);
    }
    else
    {
        RbrgCliShowIfPortInfo (CliHandle, *pi4PortId);
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllPortInfo                                     *
 *                                                                           *
 * Description  : This function is used to display the port table info of    *
 *                all ports of an R-Bridge                                   *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllPortInfo (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsrbridgePortTable (&i4NextIfIndex);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            i4IfIndex = i4NextIfIndex;
            RbrgCliShowIfPortInfo (CliHandle, i4IfIndex);
            i4RetVal = nmhGetNextIndexFsrbridgePortTable (i4IfIndex,
                                                          &i4NextIfIndex);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowIfPortInfo                                      *
 *                                                                           *
 * Description  : This function is used to display the port table info of    *
 *                a particular port of an R-Bridge                           *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                i4IfIndex - Index of the port                              *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowIfPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4PortDisable = 0;
    INT4                i4TrunkPort = 0;
    INT4                i4AccessPort = 0;
    INT4                i4PortSatate = 0;
    INT4                i4DisableLearning = 0;
    INT4                i4DesigVlan = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { 0 };
    INT1               *piIfName;

    piIfName = (INT1 *) &au1IfName[0];
    if (nmhValidateIndexInstanceFsrbridgePortTable (i4IfIndex) != SNMP_FAILURE)
    {
        CfaCliGetIfName (i4IfIndex, piIfName);
        CliPrintf (CliHandle, " Port Index                : %s\r\n", piIfName);
        nmhGetFsrbridgePortDisable (i4IfIndex, &i4PortDisable);
        if (i4PortDisable == TRUE)
        {
            CliPrintf (CliHandle, " Port Traffic Disable Status        : "
                       "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Port Traffic Disable Status        : "
                       "Disabled\r\n");
        }
        nmhGetFsrbridgePortTrunkPort (i4IfIndex, &i4TrunkPort);
        if (i4TrunkPort == TRUE)
        {
            CliPrintf (CliHandle, " End-station service Disable status : "
                       "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " End-station service Disable status : "
                       "Disabled\r\n");
        }
        nmhGetFsrbridgePortAccessPort (i4IfIndex, &i4AccessPort);
        if (i4AccessPort == TRUE)
        {
            CliPrintf (CliHandle, " TRILL traffic Disable status       : "
                       "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " TRILL traffic Disable status       : "
                       "Disabled\r\n");
        }
        nmhGetFsrbridgePortState (i4IfIndex, &i4PortSatate);
        if (i4PortSatate == RBRG_PORT_DISABLED)
        {
            CliPrintf (CliHandle, " Port State                         : "
                       "Disable\r\n");
        }
        else if (i4PortSatate == RBRG_PORT_UNINHIBITED)
        {
            CliPrintf (CliHandle, " Port State                         : "
                       "UnInhibited\r\n");
        }
        nmhGetFsrbridgePortDisableLearning (i4IfIndex, &i4DisableLearning);
        if (i4DisableLearning == TRUE)
        {
            CliPrintf (CliHandle, " MAC-Learning Disable status        : "
                       "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " MAC-Learning Disable status        : "
                       "Disabled\r\n");
        }
        nmhGetFsrbridgePortDesigVlan (i4IfIndex, &i4DesigVlan);
        CliPrintf (CliHandle, " Designated VLAN                   : "
                   "%d\r\n", i4DesigVlan);
        CliPrintf (CliHandle, "------------------------------------------\r\n");

    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFDBInfo                                         *
 *                                                                           *
 * Description  : This function is used to display the FDB information       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                pu4ContextId - pointer to context Id                       *
 *                pu4FdbId     - pointer to FDB Id                           *
 *                pu1DestMac   - pointer to destinaton MAC                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFDBInfo (tCliHandle CliHandle, UINT1 *pu1ContextId,
                    UINT4 *pu4FdbId, UINT1 *pu1DestMac)
{
    tMacAddr            FdbDestMac;
    tSNMP_OCTET_STRING_TYPE DestMac;
    UINT4               u4ContextId = 0;

    CliPrintf (CliHandle, "R-Bridge Unicast FDB entries\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");
    CliPrintf (CliHandle,
               "%-10s%-7s%8s%20s%12s%12s%8s\r\n",
               "ContextId", "FdbId", "FdbMac", "FdbPort", "EgressNick",
               "Confidence", "Status");

    if ((pu1ContextId == NULL) && (pu4FdbId == NULL))
    {
        RbrgCliShowAllFDBInfo (CliHandle);
    }
    else if (pu1ContextId != NULL)
    {
        if (RbrgPortVcmIsSwitchExist (pu1ContextId, &u4ContextId) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (RBRG_CLI_INVALID_CONTEXT_ID);
        }
        if (pu4FdbId != NULL)
        {
            MEMSET (&FdbDestMac, 0, sizeof (tMacAddr));
            StrToMac (pu1DestMac, (UINT1 *) &FdbDestMac);
            DestMac.pu1_OctetList = (UINT1 *) &FdbDestMac;
            DestMac.i4_Length = sizeof (tMacAddr);
            RbrgCliShowFDBMacInfo (CliHandle, u4ContextId, *pu4FdbId,
                                   (UINT1 *) &DestMac);
        }
        else
        {
            RbrgCliShowFDBSwitchInfo (CliHandle, u4ContextId);
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllFDBInfo                                      *
 *                                                                           *
 * Description  : This function is used to display the FDB information       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllFDBInfo (tCliHandle CliHandle)
{
    UINT4               u4ContextIndex = 0;
    UINT4               u4NextContextIndex = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4NextFdbId = 0;
    tMacAddr            DestMac;
    tMacAddr            NextDestMac;
    INT4                i4RetVal = 0;

    MEMSET (&DestMac, 0, sizeof (tMacAddr));
    MEMSET (&NextDestMac, 0, sizeof (tMacAddr));

    i4RetVal = nmhGetFirstIndexFsrbridgeUniFdbTable (&u4NextContextIndex,
                                                     &u4NextFdbId,
                                                     &NextDestMac);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            u4ContextIndex = u4NextContextIndex;
            u4FdbId = u4NextFdbId;
            MEMCPY (&DestMac, &NextDestMac, MAC_ADDR_LEN);
            RbrgCliShowFDBMacInfo (CliHandle, u4ContextIndex, u4FdbId, DestMac);
            i4RetVal = nmhGetNextIndexFsrbridgeUniFdbTable (u4ContextIndex,
                                                            &u4NextContextIndex,
                                                            u4FdbId,
                                                            &u4NextFdbId,
                                                            DestMac,
                                                            &NextDestMac);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFDBSwitchInfo                                   *
 *                                                                           *
 * Description  : This function is used to display the FDB information on a  *
 *                particular context                                         *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId   - Context Identifier                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFDBSwitchInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tMacAddr            FdbDestMac;
    tMacAddr            NextFdbDestMac;
    UINT4               u4NextContextId = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4NextFdbId = 0;
    INT4                i4RetVal = 0;

    MEMSET (&FdbDestMac, 0, sizeof (tMacAddr));
    MEMSET (&NextFdbDestMac, 0, sizeof (tMacAddr));
    i4RetVal = nmhGetNextIndexFsrbridgeUniFdbTable (u4ContextId,
                                                    &u4NextContextId,
                                                    u4FdbId, &u4NextFdbId,
                                                    FdbDestMac,
                                                    &NextFdbDestMac);
    while ((u4NextContextId == u4ContextId) && (i4RetVal != SNMP_FAILURE))
    {
        u4ContextId = u4NextContextId;
        u4FdbId = u4NextFdbId;
        MEMCPY (&FdbDestMac, &NextFdbDestMac, sizeof (tMacAddr));
        RbrgCliShowFDBMacInfo (CliHandle, u4ContextId, u4FdbId, FdbDestMac);
        i4RetVal = nmhGetNextIndexFsrbridgeUniFdbTable (u4ContextId,
                                                        &u4NextContextId,
                                                        u4FdbId, &u4NextFdbId,
                                                        FdbDestMac,
                                                        &NextFdbDestMac);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFDBMacInfo                                      *
 *                                                                           *
 * Description  : This function is used to display the FDB information       *
 *                of a particular FDB                                        *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId   - Context Identifier                         *
 *                u4fdbId       - FDB Identifier                             *
 *                FdbDestMac    - FDB MAC                                    *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFDBMacInfo (tCliHandle CliHandle,
                       UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr FdbDestMac)
{
    INT4                i4FdbPort = 0;
    UINT4               u4Confidence = 0;
    INT4                i4NickName = 0;
    INT4                i4Status = 0;
    UINT1               au1Mac[RBRG_MAC_STR_LEN];

    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable (u4ContextId, u4FdbId,
                                                      FdbDestMac) !=
        SNMP_FAILURE)
    {
        nmhGetFsrbridgeUniFdbPort (u4ContextId, u4FdbId, FdbDestMac,
                                   &i4FdbPort);
        nmhGetFsrbridgeUniFdbNick (u4ContextId, u4FdbId, FdbDestMac,
                                   &i4NickName);
        nmhGetFsrbridgeUniFdbConfidence (u4ContextId, u4FdbId, FdbDestMac,
                                         &u4Confidence);
        nmhGetFsrbridgeUniFdbStatus (u4ContextId, u4FdbId, FdbDestMac,
                                     &i4Status);
        PrintMacAddress ((UINT1 *) FdbDestMac, au1Mac);
        CliPrintf (CliHandle, "%-9d %-5d %5s %5d %10d %10d %5s",
                   u4ContextId, u4FdbId, au1Mac,
                   i4FdbPort, i4NickName, u4Confidence, "");
        if (i4Status == RBRG_FDB_MGMT)
        {
            CliPrintf (CliHandle, "Management\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Learned\r\n");
        }
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFIBInfo                                         *
 *                                                                           *
 * Description  : This function is used to display the FIB information       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                pu4ContextId - pointer to context Id                       *
 *                pu4Nickname  - pointer to Egress Nickname                  *
 *                pu4Port   - pointer to FIB port                            *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFIBInfo (tCliHandle CliHandle, UINT1 *pu1ContextId,
                    UINT4 *pu4Nickname, UINT4 *pu4Port, UINT4 *pu4NextHopNick)
{
    UINT4               u4ContextId = 0;

    CliPrintf (CliHandle, "R-Bridge Unicast FIB entries\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");
    CliPrintf (CliHandle,
               "%-10s%-15s%10s%20s%15s%15s%10s\r\n",
               "ContextId", "EgressNick", "EgressPort", "NextHopNickname",
               "NextHopMac", "HopCount", "MTU");
    if ((pu1ContextId == NULL) && (pu4Nickname == NULL))
    {
        RbrgCliShowAllFIBInfo (CliHandle);
    }
    else if (pu1ContextId != NULL)
    {
        if (RbrgPortVcmIsSwitchExist (pu1ContextId, &u4ContextId) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (RBRG_CLI_INVALID_CONTEXT_ID);
        }

        if (pu4Nickname != NULL)
        {
            RbrgCliShowFIBNicknameInfo (CliHandle, u4ContextId,
                                        *pu4Nickname, *pu4Port,
                                        *pu4NextHopNick);
        }

        else
        {
            RbrgCliShowFIBSwitchInfo (CliHandle, u4ContextId);
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllFIBInfo                                      *
 *                                                                           *
 * Description  : This function is used to display the FIB information       *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllFIBInfo (tCliHandle CliHandle)
{
    UINT4               u4ContextIndex = 0;
    UINT4               u4NextContextIndex = 0;
    INT4                i4FibNick = 0;
    INT4                i4NextFibNick = 0;
    INT4                i4FibPort = 0;
    INT4                i4NextFibPort = 0;
    INT4                i4FibNextHopNick = 0;
    INT4                i4NextFibNextHopNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsrbridgeUniFibTable (&u4NextContextIndex,
                                                     &i4NextFibNick,
                                                     &i4NextFibPort,
                                                     &i4NextFibNextHopNick);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            u4ContextIndex = u4NextContextIndex;
            i4FibNick = i4NextFibNick;
            i4FibPort = i4NextFibPort;
            i4FibNextHopNick = i4NextFibNextHopNick;
            RbrgCliShowFIBNicknameInfo (CliHandle, u4ContextIndex, i4FibNick,
                                        i4FibPort, i4FibNextHopNick);
            i4RetVal = nmhGetNextIndexFsrbridgeUniFibTable (u4ContextIndex,
                                                            &u4NextContextIndex,
                                                            i4FibNick,
                                                            &i4NextFibNick,
                                                            i4FibPort,
                                                            &i4NextFibPort,
                                                            i4FibNextHopNick,
                                                            &i4NextFibNextHopNick);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFIBSwitchInfo                                   *
 *                                                                           *
 * Description  : This function is used to display the FIB information of a  *
 *                particular R-Bridge                                        *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFIBSwitchInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4NextContextId = 0;
    INT4                i4FibNick = 0;
    INT4                i4NextFibNick = 0;
    INT4                i4FibPort = 0;
    INT4                i4NextFibPort = 0;
    INT4                i4FibNextHopNick = 0;
    INT4                i4NextFibNextHopNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetNextIndexFsrbridgeUniFibTable (u4ContextId,
                                                    &u4NextContextId,
                                                    i4FibNick, &i4NextFibNick,
                                                    i4FibPort, &i4NextFibPort,
                                                    i4FibNextHopNick,
                                                    &i4NextFibNextHopNick);
    while ((u4NextContextId == u4ContextId) && (i4RetVal != SNMP_FAILURE))
    {
        u4ContextId = u4NextContextId;
        i4FibNick = i4NextFibNick;
        i4FibPort = i4NextFibPort;
        i4FibNextHopNick = i4NextFibNextHopNick;
        RbrgCliShowFIBNicknameInfo (CliHandle, u4ContextId, i4FibNick,
                                    i4FibPort, i4FibNextHopNick);
        i4RetVal = nmhGetNextIndexFsrbridgeUniFibTable (u4ContextId,
                                                        &u4NextContextId,
                                                        i4FibNick,
                                                        &i4NextFibNick,
                                                        i4FibPort,
                                                        &i4NextFibPort,
                                                        i4FibNextHopNick,
                                                        &i4NextFibNextHopNick);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowFIBNicknameInfo                                 *
 *                                                                           *
 * Description  : This function is used to display the FIB information of a  *
 *                particular Nickname                                        *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                u4FibNick   - FIB Nickname                                 *
 *                u4FibPort   - FIB port                                     *
 *                u4FibNextHopNick - Next Hop Nickname                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowFIBNicknameInfo (tCliHandle CliHandle,
                            UINT4 u4ContextId, INT4 i4FibNick,
                            INT4 i4FibPort, INT4 i4FibNextHopNick)
{
    tMacAddr            FibNextHopMac;
    tSNMP_OCTET_STRING_TYPE NextHopMac;
    UINT4               u4Mtu = 0;
    UINT4               u4HopCount = 0;
    UINT1               au1Mac[RBRG_MAC_STR_LEN];

    MEMSET (&FibNextHopMac, 0, sizeof (tMacAddr));
    NextHopMac.pu1_OctetList = (UINT1 *) &FibNextHopMac;
    NextHopMac.i4_Length = MAC_ADDR_LEN;

    if (nmhValidateIndexInstanceFsrbridgeUniFibTable (u4ContextId, i4FibNick,
                                                      i4FibPort,
                                                      i4FibNextHopNick) !=
        SNMP_FAILURE)
    {
        nmhGetFsrbridgeFibMacAddress (u4ContextId, i4FibNick,
                                      i4FibPort, i4FibNextHopNick, &NextHopMac);
        nmhGetFsrbridgeFibMtuDesired (u4ContextId, i4FibNick,
                                      i4FibPort, i4FibNextHopNick, &u4Mtu);
        nmhGetFsrbridgeFibHopCount (u4ContextId, i4FibNick,
                                    i4FibPort, i4FibNextHopNick, &u4HopCount);
        CliPrintf (CliHandle, "%-9d %5d %15d %15d ", u4ContextId,
                   i4FibNick, i4FibPort, i4FibNextHopNick);
        PrintMacAddress ((UINT1 *) &FibNextHopMac, au1Mac);
        CliPrintf (CliHandle, "%30s %5d %10d\r\n", au1Mac, u4HopCount, u4Mtu);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowMultiFIBInfo                                    *
 *                                                                           *
 * Description  : This function is used to display the MultiFIB information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                pu4ContextId - pointer to context Id                       *
 *                pu4Nickname  - pointer to Egress Nickname                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/

VOID
RbrgCliShowMultiFIBInfo (tCliHandle CliHandle, UINT1 *pu1ContextId,
                         UINT4 *pu4Nickname)
{
    UINT4               u4ContextId = 0;

    if ((pu1ContextId == NULL) && (pu4Nickname == NULL))
    {
        RbrgCliShowAllMultiFIBInfo (CliHandle);
    }
    else if (pu1ContextId != NULL)
    {
        if (RbrgPortVcmIsSwitchExist (pu1ContextId, &u4ContextId) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (RBRG_CLI_INVALID_CONTEXT_ID);
        }
        if (pu4Nickname != NULL)
        {
            RbrgCliShowMultiFIBNicknameInfo (CliHandle, u4ContextId,
                                             *pu4Nickname);
        }
        else
        {
            RbrgCliShowMultiFIBSwitchInfo (CliHandle, u4ContextId);
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllMultiFIBInfo                                 *
 *                                                                           *
 * Description  : This function is used to display the MultiFIB information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllMultiFIBInfo (tCliHandle CliHandle)
{
    UINT4               u4ContextIndex = 0;
    UINT4               u4NextContextIndex = 0;
    INT4                i4FibNick = 0;
    INT4                i4NextFibNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsrbridgeMultiFibTable (&u4NextContextIndex,
                                                       &i4NextFibNick);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            u4ContextIndex = u4NextContextIndex;
            i4FibNick = i4NextFibNick;
            RbrgCliShowMultiFIBNicknameInfo (CliHandle, u4ContextIndex,
                                             i4FibNick);
            i4RetVal =
                nmhGetNextIndexFsrbridgeMultiFibTable (u4ContextIndex,
                                                       &u4NextContextIndex,
                                                       i4FibNick,
                                                       &i4NextFibNick);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowMultiFIBSwitchInfo                              *
 *                                                                           *
 * Description  : This function is used to display the MultiFIB information  *
 *                of a particular R-Bridge                                   *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowMultiFIBSwitchInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4NextContextId = 0;
    INT4                i4FibNick = 0;
    INT4                i4NextFibNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetNextIndexFsrbridgeMultiFibTable (u4ContextId,
                                                      &u4NextContextId,
                                                      i4FibNick,
                                                      &i4NextFibNick);
    while ((u4NextContextId == u4ContextId) && (i4RetVal != SNMP_FAILURE))
    {
        u4ContextId = u4NextContextId;
        i4FibNick = i4NextFibNick;
        RbrgCliShowMultiFIBNicknameInfo (CliHandle, u4ContextId, i4FibNick);
        i4RetVal = nmhGetNextIndexFsrbridgeMultiFibTable (u4ContextId,
                                                          &u4NextContextId,
                                                          i4FibNick,
                                                          &i4NextFibNick);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowMultiFIBSwitchInfo                              *
 *                                                                           *
 * Description  : This function is used to display the MultiFIB information  *
 *                of a particular Nickname                                   *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                u4FibNick   - Nicknmae of multicast distribution tree      *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowMultiFIBNicknameInfo (tCliHandle CliHandle,
                                 UINT4 u4ContextId, INT4 i4FibNick)
{
    tSNMP_OCTET_STRING_TYPE MultiFibPorts;
    UINT1               FibPortList[RBRG_MAX_PORTLIST_SIZE];
    UINT4               u4MaxPortsPerLine = RBRG_CLI_MAX_PORTS_PER_LINE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4Status = 0;
    UINT1               u1StartLoc = 0;

    MEMSET (&FibPortList, 0, sizeof (tPortList));

    MultiFibPorts.pu1_OctetList = FibPortList;
    MultiFibPorts.i4_Length = RBRG_MAX_PORTLIST_SIZE;
    if (nmhValidateIndexInstanceFsrbridgeMultiFibTable (u4ContextId,
                                                        i4FibNick) !=
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Context Id    : %d\r\n", u4ContextId);
        CliPrintf (CliHandle, "NickName      : %d\r\n", i4FibNick);
        nmhGetFsrbridgeMultiFibStatus (u4ContextId, i4FibNick, &i4Status);
        nmhGetFsrbridgeMultiFibPorts (u4ContextId, i4FibNick, &MultiFibPorts);
        CliOctetToIfName (CliHandle, "FIB PortList : ",
                          &MultiFibPorts, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                          MultiFibPorts.i4_Length, u1StartLoc, &u4PagingStatus,
                          u4MaxPortsPerLine);

        if (i4Status == RBRG_STATIC)
        {
            CliPrintf (CliHandle, "Status       : Static\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Status       : Dynamic\r\n");
        }
        CliPrintf (CliHandle, "-------------------------------------\r\n");

    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowNicknameInfo                                    *
 *                                                                           *
 * Description  : This function is used to display the nickname information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                pu4ContextId - pointer to context Id                       *
 *                pu4Nickname  - pointer to Nickname                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowNicknameInfo (tCliHandle CliHandle, UINT1 *pu1ContextId,
                         UINT4 *pu4Nickname)
{
    UINT4               u4ContextId = 0;

    CliPrintf (CliHandle, "R-Bridge Nickname Info \r\n");
    CliPrintf (CliHandle, "-----------------------\r\n");
    CliPrintf (CliHandle, " %-9s %5s %10s %5s %5s\r\n",
               "ContextId", "Nickname", "Priority", "DtrPriority", "Status");
    if ((pu1ContextId == NULL) && (pu4Nickname == NULL))
    {
        RbrgCliShowAllNicknameInfo (CliHandle);
    }
    else if (pu1ContextId != NULL)
    {
        if (RbrgPortVcmIsSwitchExist (pu1ContextId, &u4ContextId) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (RBRG_CLI_INVALID_CONTEXT_ID);
        }
        if (pu4Nickname != NULL)
        {
            RbrgCliShowNicknameNameInfo (CliHandle, u4ContextId, *pu4Nickname);
        }
        else
        {
            RbrgCliShowNicknameSwitchInfo (CliHandle, u4ContextId);
        }
    }
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowAllNicknameInfo                                 *
 *                                                                           *
 * Description  : This function is used to display the nickname information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowAllNicknameInfo (tCliHandle CliHandle)
{
    UINT4               u4ContextIndex = 0;
    UINT4               u4NextContextIndex = 0;
    INT4                i4Nick = 0;
    INT4                i4NextNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsrbridgeNicknameTable (&u4NextContextIndex,
                                                       &i4NextNick);
    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "-");
    }
    else
    {
        do
        {
            u4ContextIndex = u4NextContextIndex;
            i4Nick = i4NextNick;
            RbrgCliShowNicknameNameInfo (CliHandle, u4ContextIndex, i4Nick);
            i4RetVal = nmhGetNextIndexFsrbridgeNicknameTable (u4ContextIndex,
                                                              &u4NextContextIndex,
                                                              i4Nick,
                                                              &i4NextNick);
        }
        while (i4RetVal != SNMP_FAILURE);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowNicknameSwitchInfo                              *
 *                                                                           *
 * Description  : This function is used to display the nickname information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowNicknameSwitchInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4NextContextId = 0;
    INT4                i4Nick = 0;
    INT4                i4NextNick = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetNextIndexFsrbridgeNicknameTable (u4ContextId,
                                                      &u4NextContextId,
                                                      i4Nick, &i4NextNick);
    while ((u4NextContextId == u4ContextId) && (i4RetVal != SNMP_FAILURE))
    {
        u4ContextId = u4NextContextId;
        i4Nick = i4NextNick;
        RbrgCliShowMultiFIBNicknameInfo (CliHandle, u4ContextId, i4Nick);
        i4RetVal = nmhGetNextIndexFsrbridgeNicknameTable (u4ContextId,
                                                          &u4NextContextId,
                                                          i4Nick, &i4NextNick);
    }
}

/*****************************************************************************
 *                                                                           *
 * Function     : RbrgCliShowNicknameNameInfo                              *
 *                                                                           *
 * Description  : This function is used to display the nickname information  *
 *                                                                           *
 * Input        : CliHandle                                                  *
 *                u4ContextId - Context Identifier                           *
 *                u4Nick - Nickname                                          *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RbrgCliShowNicknameNameInfo (tCliHandle CliHandle,
                             UINT4 u4ContextId, INT4 i4Nick)
{

    if (nmhValidateIndexInstanceFsrbridgeNicknameTable (u4ContextId,
                                                        i4Nick) != SNMP_FAILURE)
    {
        UINT4               u4Priority = 0;
        UINT4               u4DtrPriority = 0;
        INT4                i4Status = 0;

        nmhGetFsrbridgeNicknamePriority (u4ContextId, i4Nick, &u4Priority);
        nmhGetFsrbridgeNicknameDtrPriority (u4ContextId, i4Nick,
                                            &u4DtrPriority);
        nmhGetFsrbridgeNicknameStatus (u4ContextId, i4Nick, &i4Status);
        CliPrintf (CliHandle, "%5d %12d %8d %5d ", u4ContextId,
                   i4Nick, u4Priority, u4DtrPriority);
        if (i4Status == RBRG_DYNAMIC)
        {
            CliPrintf (CliHandle, " %12s\r\n", "Dynamic");
        }
        else
        {
            CliPrintf (CliHandle, " %12s\r\n", "Static");
        }

    }
}
#endif
