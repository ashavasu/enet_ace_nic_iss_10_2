/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgwrg.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains the wrapperfunctions for Rbrg 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "rbrgtdfsg.h"
#include "rbrgtdfs.h"
#include "rbrglwg.h"
#include "rbrgwrg.h"
#include "rbrgprotg.h"
#include "rbrgtmr.h"
#include "rbrgprot.h"
#include "fsrbridb.h"

VOID
RegisterFSRBRI ()
{
    SNMPRegisterMibWithLock (&fsrbriOID, &fsrbriEntry, RbrgMainTaskLock,
                             RbrgMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fsrbriOID, (const UINT1 *) "fsrbri");
}

VOID
UnRegisterFSRBRI ()
{
    SNMPUnRegisterMib (&fsrbriOID, &fsrbriEntry);
    SNMPDelSysorEntry (&fsrbriOID, (const UINT1 *) "fsrbri");
}

INT4
GetNextIndexFsrbridgeGlobalTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgeGlobalTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgeGlobalTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgeNicknameTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgeNicknameTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgeNicknameTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgePortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgePortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgeUniFdbTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgeUniFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgeUniFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgeUniFibTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgeUniFibTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgeUniFibTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgeMultiFibTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgeMultiFibTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgeMultiFibTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsrbridgePortCounterTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsrbridgePortCounterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsrbridgePortCounterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsrbridgeGlobalTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsrbridgeGlobalTrace (&(pMultiData->u4_ULongValue)));
}

INT4
FsrbridgeTrillVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeTrillVersion (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeNumPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeNumPorts (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeUniMultipathEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniMultipathEnable
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeMultiMultipathEnableGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeMultiMultipathEnable
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeNicknameNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeNicknameNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeSystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeUnicastMultipathCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUnicastMultipathCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeMulticastMultipathCountGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeMulticastMultipathCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeClearCounters (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeNicknamePriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeNicknameTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeNicknamePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeNicknameDtrPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeNicknameTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeNicknameDtrPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeNicknameStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeNicknameTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeNicknameStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortDisableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortDisable (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortTrunkPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortTrunkPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortAccessPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortAccessPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortDisableLearningGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortDisableLearning
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortDesigVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortDesigVlan (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortClearCounters
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortMacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsrbridgePortMac (pMultiIndex->pIndex[0].i4_SLongValue,
                                    (tMacAddr *) pMultiData->pOctetStrValue->
                                    pu1_OctetList));

}

INT4
FsrbridgeUniFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniFdbPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[2].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeUniFdbNickGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniFdbNick (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[2].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeUniFdbConfidenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniFdbConfidence
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeUniFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniFdbStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[2].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeUniFdbRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeUniFdbRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeFibMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeFibMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsrbridgeFibMtuDesiredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeFibMtuDesired (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeFibHopCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeFibHopCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgeFibStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeFibStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeFibRowstatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeUniFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeFibRowstatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeMultiFibPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeMultiFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeMultiFibPorts (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsrbridgeMultiFibStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeMultiFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeMultiFibStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgeMultiFibRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgeMultiFibTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgeMultiFibRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsrbridgePortRpfChecksFailedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortRpfChecksFailed
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgePortHopCountsExceededGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortHopCountsExceeded
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgePortOptionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortOptions (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsrbridgePortTrillInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortTrillInFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsrbridgePortTrillOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsrbridgePortCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsrbridgePortTrillOutFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsrbridgeGlobalTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsrbridgeGlobalTrace
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsrbridgeUniMultipathEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUniMultipathEnable (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsrbridgeMultiMultipathEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeMultiMultipathEnable (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsrbridgeNicknameNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeNicknameNumber (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsrbridgeSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeSystemControl (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsrbridgeModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeModuleStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUnicastMultipathCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUnicastMultipathCount (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsrbridgeMulticastMultipathCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeMulticastMultipathCount (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsrbridgeClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeClearCounters (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsrbridgeNicknamePriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeNicknamePriority (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsrbridgeNicknameDtrPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeNicknameDtrPriority (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsrbridgeNicknameStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeNicknameStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortDisable (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortTrunkPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortTrunkPort (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortAccessPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortAccessPort (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDisableLearningTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortDisableLearning (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDesigVlanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortDesigVlan (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgePortClearCounters (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUniFdbPort (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[2].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbNickTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUniFdbNick (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[2].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbConfidenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUniFdbConfidence (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                (*(tMacAddr *) pMultiIndex->
                                                 pIndex[2].pOctetStrValue->
                                                 pu1_OctetList),
                                                pMultiData->u4_ULongValue));

}

INT4
FsrbridgeUniFdbRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeUniFdbRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               (*(tMacAddr *) pMultiIndex->
                                                pIndex[2].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiData->i4_SLongValue));

}

INT4
FsrbridgeFibMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeFibMacAddress (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsrbridgeFibMtuDesiredTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeFibMtuDesired (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsrbridgeFibHopCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeFibHopCount (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsrbridgeFibRowstatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeFibRowstatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsrbridgeMultiFibPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeMultiFibPorts (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsrbridgeMultiFibRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsrbridgeMultiFibRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsrbridgeGlobalTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsrbridgeGlobalTrace (pMultiData->u4_ULongValue));
}

INT4
FsrbridgeUniMultipathEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUniMultipathEnable
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsrbridgeMultiMultipathEnableSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeMultiMultipathEnable
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsrbridgeNicknameNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeNicknameNumber (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsrbridgeSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeSystemControl (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgeModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeModuleStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUnicastMultipathCountSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUnicastMultipathCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsrbridgeMulticastMultipathCountSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeMulticastMultipathCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsrbridgeClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeClearCounters (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgeNicknamePrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeNicknamePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsrbridgeNicknameDtrPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeNicknameDtrPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsrbridgeNicknameStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeNicknameStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDisableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortDisable (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortTrunkPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortTrunkPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortAccessPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortAccessPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDisableLearningSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortDisableLearning
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortDesigVlanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortDesigVlan (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsrbridgePortClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgePortClearCounters
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUniFdbPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[2].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbNickSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUniFdbNick (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[2].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->i4_SLongValue));

}

INT4
FsrbridgeUniFdbConfidenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUniFdbConfidence
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->u4_ULongValue));

}

INT4
FsrbridgeUniFdbRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeUniFdbRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsrbridgeFibMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeFibMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsrbridgeFibMtuDesiredSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeFibMtuDesired (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsrbridgeFibHopCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeFibHopCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsrbridgeFibRowstatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeFibRowstatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsrbridgeMultiFibPortsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeMultiFibPorts (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsrbridgeMultiFibRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsrbridgeMultiFibRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsrbridgeGlobalTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeGlobalTrace
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgeGlobalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeGlobalTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgeNicknameTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeNicknameTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgePortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgePortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgeUniFdbTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeUniFdbTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgeUniFibTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeUniFibTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsrbridgeMultiFibTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsrbridgeMultiFibTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
