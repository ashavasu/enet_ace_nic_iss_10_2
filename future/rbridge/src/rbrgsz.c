#define _RBRGSZ_C
#include "rbrginc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
INT4  RbrgSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RBRG_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsRBRGSizingParams[i4SizingId].u4StructSize,
                          FsRBRGSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(RBRGMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            RbrgSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   RbrgSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsRBRGSizingParams); 
      return OSIX_SUCCESS; 
}


VOID  RbrgSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RBRG_MAX_SIZING_ID; i4SizingId++) {
        if(RBRGMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( RBRGMemPoolIds[ i4SizingId] );
            RBRGMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
