/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgdsg.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"
/****************************************************************************
 Function    :  RbrgGetFsrbridgeGlobalTrace
 Input       :  The Indices
 Input       :  pu4FsrbridgeGlobalTrace
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetFsrbridgeGlobalTrace (UINT4 *pu4FsrbridgeGlobalTrace)
{
    *pu4FsrbridgeGlobalTrace = gRbrgGlobals.RbrgGlbMib.u4FsrbridgeGlobalTrace;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgSetFsrbridgeGlobalTrace
 Input       :  u4FsrbridgeGlobalTrace
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
RbrgSetFsrbridgeGlobalTrace (UINT4 u4FsrbridgeGlobalTrace)
{
    gRbrgGlobals.RbrgGlbMib.u4FsrbridgeGlobalTrace = u4FsrbridgeGlobalTrace;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetFirstFsrbridgeGlobalTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pRbrgFsrbridgeGlobalEntry or NULL
****************************************************************************/
tRbrgFsrbridgeGlobalEntry *
RbrgGetFirstFsrbridgeGlobalTable ()
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *) RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.
                                                      FsrbridgeGlobalTable);

    return pRbrgFsrbridgeGlobalEntry;
}

/****************************************************************************
 Function    :  RbrgGetNextFsrbridgeGlobalTable
 Input       :  pCurrentRbrgFsrbridgeGlobalEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextRbrgFsrbridgeGlobalEntry or NULL
****************************************************************************/
tRbrgFsrbridgeGlobalEntry *
RbrgGetNextFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                                 pCurrentRbrgFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry *pNextRbrgFsrbridgeGlobalEntry = NULL;

    pNextRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *) RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                                     FsrbridgeGlobalTable,
                                                     (tRBElem *)
                                                     pCurrentRbrgFsrbridgeGlobalEntry,
                                                     FsrbridgeGlobalTableRBCmp);

    return pNextRbrgFsrbridgeGlobalEntry;
}

/****************************************************************************
 Function    :  RbrgGetFsrbridgeGlobalTable
 Input       :  pRbrgFsrbridgeGlobalEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetRbrgFsrbridgeGlobalEntry or NULL
****************************************************************************/
tRbrgFsrbridgeGlobalEntry *
RbrgGetFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                             pRbrgFsrbridgeGlobalEntry)
{
    tRbrgFsrbridgeGlobalEntry *pGetRbrgFsrbridgeGlobalEntry = NULL;

    pGetRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.
                                                 FsrbridgeGlobalTable,
                                                 (tRBElem *)
                                                 pRbrgFsrbridgeGlobalEntry);

    return pGetRbrgFsrbridgeGlobalEntry;
}

/****************************************************************************
 Function    :  RbrgGetFirstFsrbridgeNicknameTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pRbrgFsrbridgeNicknameEntry or NULL
****************************************************************************/
tRbrgFsrbridgeNicknameEntry *
RbrgGetFirstFsrbridgeNicknameTable ()
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *) RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.
                                                        FsrbridgeNicknameTable);

    return pRbrgFsrbridgeNicknameEntry;
}

/****************************************************************************
 Function    :  RbrgGetNextFsrbridgeNicknameTable
 Input       :  pCurrentRbrgFsrbridgeNicknameEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextRbrgFsrbridgeNicknameEntry or NULL
****************************************************************************/
tRbrgFsrbridgeNicknameEntry *
RbrgGetNextFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                                   pCurrentRbrgFsrbridgeNicknameEntry)
{
    tRbrgFsrbridgeNicknameEntry *pNextRbrgFsrbridgeNicknameEntry = NULL;

    pNextRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *) RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                                       FsrbridgeNicknameTable,
                                                       (tRBElem *)
                                                       pCurrentRbrgFsrbridgeNicknameEntry,
                                                       NULL);

    return pNextRbrgFsrbridgeNicknameEntry;
}

/****************************************************************************
 Function    :  RbrgGetFsrbridgeNicknameTable
 Input       :  pRbrgFsrbridgeNicknameEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetRbrgFsrbridgeNicknameEntry or NULL
****************************************************************************/
tRbrgFsrbridgeNicknameEntry *
RbrgGetFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                               pRbrgFsrbridgeNicknameEntry)
{
    tRbrgFsrbridgeNicknameEntry *pGetRbrgFsrbridgeNicknameEntry = NULL;

    pGetRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.
                                                   FsrbridgeNicknameTable,
                                                   (tRBElem *)
                                                   pRbrgFsrbridgeNicknameEntry);

    return pGetRbrgFsrbridgeNicknameEntry;
}

/****************************************************************************
 Function    :  RbrgGetFirstFsrbridgePortTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pRbrgFsRBridgeBasePortEntry or NULL
****************************************************************************/
tRbrgFsRBridgeBasePortEntry *
RbrgGetFirstFsrbridgePortTable ()
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *) RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.
                                                        FsrbridgePortTable);

    return pRbrgFsRBridgeBasePortEntry;
}

/****************************************************************************
 Function    :  RbrgGetNextFsrbridgePortTable
 Input       :  pCurrentRbrgFsRBridgeBasePortEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextRbrgFsRBridgeBasePortEntry or NULL
****************************************************************************/
tRbrgFsRBridgeBasePortEntry *
RbrgGetNextFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                               pCurrentRbrgFsRBridgeBasePortEntry)
{
    tRbrgFsRBridgeBasePortEntry *pNextRbrgFsRBridgeBasePortEntry = NULL;

    pNextRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *) RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                                       FsrbridgePortTable,
                                                       (tRBElem *)
                                                       pCurrentRbrgFsRBridgeBasePortEntry,
                                                       NULL);

    return pNextRbrgFsRBridgeBasePortEntry;
}

/****************************************************************************
 Function    :  RbrgGetFsrbridgePortTable
 Input       :  pRbrgFsRBridgeBasePortEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetRbrgFsRBridgeBasePortEntry or NULL
****************************************************************************/
tRbrgFsRBridgeBasePortEntry *
RbrgGetFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                           pRbrgFsRBridgeBasePortEntry)
{
    tRbrgFsRBridgeBasePortEntry *pGetRbrgFsRBridgeBasePortEntry = NULL;

    pGetRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.
                                                   FsrbridgePortTable,
                                                   (tRBElem *)
                                                   pRbrgFsRBridgeBasePortEntry);

    return pGetRbrgFsRBridgeBasePortEntry;
}

/****************************************************************************
 Function    :  RbrgGetFirstFsrbridgeMultiFibTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pRbrgFsrbridgeMultiFibEntry or NULL
****************************************************************************/
tRbrgFsrbridgeMultiFibEntry *
RbrgGetFirstFsrbridgeMultiFibTable ()
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *) RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.
                                                        FsrbridgeMultiFibTable);

    return pRbrgFsrbridgeMultiFibEntry;
}

/****************************************************************************
 Function    :  RbrgGetNextFsrbridgeMultiFibTable
 Input       :  pCurrentRbrgFsrbridgeMultiFibEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextRbrgFsrbridgeMultiFibEntry or NULL
****************************************************************************/
tRbrgFsrbridgeMultiFibEntry *
RbrgGetNextFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                   pCurrentRbrgFsrbridgeMultiFibEntry)
{
    tRbrgFsrbridgeMultiFibEntry *pNextRbrgFsrbridgeMultiFibEntry = NULL;

    pNextRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *) RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.
                                                       FsrbridgeMultiFibTable,
                                                       (tRBElem *)
                                                       pCurrentRbrgFsrbridgeMultiFibEntry,
                                                       FsrbridgeMultiFibTableRBCmp);

    return pNextRbrgFsrbridgeMultiFibEntry;
}

/****************************************************************************
 Function    :  RbrgGetFsrbridgeMultiFibTable
 Input       :  pRbrgFsrbridgeMultiFibEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetRbrgFsrbridgeMultiFibEntry or NULL
****************************************************************************/
tRbrgFsrbridgeMultiFibEntry *
RbrgGetFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                               pRbrgFsrbridgeMultiFibEntry)
{
    tRbrgFsrbridgeMultiFibEntry *pGetRbrgFsrbridgeMultiFibEntry = NULL;

    pGetRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.
                                                   FsrbridgeMultiFibTable,
                                                   (tRBElem *)
                                                   pRbrgFsrbridgeMultiFibEntry);

    return pGetRbrgFsrbridgeMultiFibEntry;
}
