/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgutl.c,v 1.7 2014/02/03 12:25:52 siva Exp $
*
* Description: This file contains utility functions used by protocol Rbrg
*********************************************************************/

#include "rbrginc.h"
#include "rbrgsrc.h"
/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgeGlobalTable
 Input       :  pRbrgGetFsrbridgeGlobalEntry
                pRbrgdsFsrbridgeGlobalEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                                   pRbrgGetFsrbridgeGlobalEntry,
                                   tRbrgFsrbridgeGlobalEntry *
                                   pRbrgdsFsrbridgeGlobalEntry)
{
    UNUSED_PARAM (pRbrgGetFsrbridgeGlobalEntry);
    UNUSED_PARAM (pRbrgdsFsrbridgeGlobalEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgeNicknameTable
 Input       :  pRbrgGetFsrbridgeNicknameEntry
                pRbrgdsFsrbridgeNicknameEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                                     pRbrgGetFsrbridgeNicknameEntry,
                                     tRbrgFsrbridgeNicknameEntry *
                                     pRbrgdsFsrbridgeNicknameEntry)
{
    UNUSED_PARAM (pRbrgGetFsrbridgeNicknameEntry);
    UNUSED_PARAM (pRbrgdsFsrbridgeNicknameEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgePortTable
 Input       :  pRbrgGetFsRBridgeBasePortEntry
                pRbrgdsFsRBridgeBasePortEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                                 pRbrgGetFsRBridgeBasePortEntry,
                                 tRbrgFsRBridgeBasePortEntry *
                                 pRbrgdsFsRBridgeBasePortEntry)
{
    tMacAddr            SwitchMac;
    MEMSET (&SwitchMac, 0, sizeof (tMacAddr));

    pRbrgdsFsRBridgeBasePortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) pRbrgGetFsRBridgeBasePortEntry);

    if (pRbrgdsFsRBridgeBasePortEntry != NULL)
    {

        /* Get SwitchMac from CFA  and copy it to egress_mac */
        RbrgPortCfaGetSysMacAddress (SwitchMac);
        MEMCPY (&(pRbrgGetFsRBridgeBasePortEntry->MibObject.FsrbridgePortMac),
                &SwitchMac, MAC_ADDR_LEN);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgeUniFdbTable
 Input       :  pRbrgGetFsRbridgeUniFdbEntry
                pRbrgdsFsRbridgeUniFdbEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                   pRbrgGetFsRbridgeUniFdbEntry,
                                   tRbrgFsRbridgeUniFdbEntry *
                                   pRbrgdsFsRbridgeUniFdbEntry)
{
    UNUSED_PARAM (pRbrgGetFsRbridgeUniFdbEntry);
    UNUSED_PARAM (pRbrgdsFsRbridgeUniFdbEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgeUniFibTable
 Input       :  pRbrgGetFsRbridgeUniFibEntry
                pRbrgdsFsRbridgeUniFibEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                   pRbrgGetFsRbridgeUniFibEntry,
                                   tRbrgFsRbridgeUniFibEntry *
                                   pRbrgdsFsRbridgeUniFibEntry)
{
    UNUSED_PARAM (pRbrgGetFsRbridgeUniFibEntry);
    UNUSED_PARAM (pRbrgdsFsRbridgeUniFibEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgeMultiFibTable
 Input       :  pRbrgGetFsrbridgeMultiFibEntry
                pRbrgdsFsrbridgeMultiFibEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                     pRbrgGetFsrbridgeMultiFibEntry,
                                     tRbrgFsrbridgeMultiFibEntry *
                                     pRbrgdsFsrbridgeMultiFibEntry)
{
    UNUSED_PARAM (pRbrgGetFsrbridgeMultiFibEntry);
    UNUSED_PARAM (pRbrgdsFsrbridgeMultiFibEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgGetAllUtlFsrbridgePortCounterTable
 Input       :  pRbrgGetFsRbridgePortCounterEntry
                pRbrgdsFsRbridgePortCounterEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgGetAllUtlFsrbridgePortCounterTable (tRbrgFsRbridgePortCounterEntry *
                                        pRbrgGetFsRbridgePortCounterEntry,
                                        tRbrgFsRbridgePortCounterEntry *
                                        pRbrgdsFsRbridgePortCounterEntry)
{
    tRBrgNpEntry        RbrgPortStats;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               u1Status = 0;

    MEMSET (&RbrgPortStats, 0, sizeof (tRBrgNpEntry));
    RbrgPortVcmGetCxtInfoFromIfIndex (pRbrgGetFsRbridgePortCounterEntry->
                                      MibObject.i4FsrbridgePortIfIndex,
                                      &u4ContextId, &u2LocalPortId);

    UNUSED_PARAM (pRbrgdsFsRbridgePortCounterEntry);

    RbrgPortStats.u4Command = RBRG_GET_PORT_STATS;
    RbrgPortStats.RbrgNpPortStats.u4RBrgPortId =
        pRbrgGetFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex;
    RBRG_IS_MODULE_ENABLE (u4ContextId, u1Status);
    if (u1Status == RBRG_ENABLE)
    {
        if (RbrgHwWrProgramEntry (&RbrgPortStats) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Get Port Stats Failed \r\n"));
            return OSIX_FAILURE;
        }
    }
    pRbrgGetFsRbridgePortCounterEntry->MibObject.
        u4FsrbridgePortRpfChecksFailed =
        RbrgPortStats.RbrgNpPortStats.u4RPFChecksFailedCount;
    pRbrgGetFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillInFrames.
        lsn = RbrgPortStats.RbrgNpPortStats.u8TrillInFrames.u4Lo;
    pRbrgGetFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillInFrames.
        msn = RbrgPortStats.RbrgNpPortStats.u8TrillInFrames.u4Hi;
    pRbrgGetFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillOutFrames.
        lsn = RbrgPortStats.RbrgNpPortStats.u8TrillOutFrames.u4Lo;
    pRbrgGetFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillOutFrames.
        msn = RbrgPortStats.RbrgNpPortStats.u8TrillOutFrames.u4Hi;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgeGlobalTable
 * Input       :   pRbrgOldFsrbridgeGlobalEntry
                   pRbrgFsrbridgeGlobalEntry
                   pRbrgIsSetFsrbridgeGlobalEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgeGlobalTable (tRbrgFsrbridgeGlobalEntry *
                                    pRbrgOldFsrbridgeGlobalEntry,
                                    tRbrgFsrbridgeGlobalEntry *
                                    pRbrgFsrbridgeGlobalEntry,
                                    tRbrgIsSetFsrbridgeGlobalEntry *
                                    pRbrgIsSetFsrbridgeGlobalEntry)
{

    UNUSED_PARAM (pRbrgOldFsrbridgeGlobalEntry);
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl ==
            RBRG_SHUT)
        {
            if (RbrgModuleShut
                (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        else                    /*if (pRbrgFsrbridgeGlobalEntry->i4FsrbridgeSystemControl ==
                                   RBRG_START) */
        {
            if (RbrgModuleStart
                (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }

    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus == OSIX_TRUE)
    {
        if (pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus ==
            RBRG_ENABLE)
        {
            if (RbrgModuleEnable
                (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        else                    /* ModuleDisable */
        {
            if (RbrgModuleDisable
                (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    if (pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters == OSIX_TRUE)
    {
        if (RbrgClearAllPortCounters
            (pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters =
            pRbrgOldFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgeNicknameTable
 * Input       :   pRbrgOldFsrbridgeNicknameEntry
                   pRbrgFsrbridgeNicknameEntry
                   pRbrgIsSetFsrbridgeNicknameEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgeNicknameTable (tRbrgFsrbridgeNicknameEntry *
                                      pRbrgOldFsrbridgeNicknameEntry,
                                      tRbrgFsrbridgeNicknameEntry *
                                      pRbrgFsrbridgeNicknameEntry,
                                      tRbrgIsSetFsrbridgeNicknameEntry *
                                      pRbrgIsSetFsrbridgeNicknameEntry)
{

    UNUSED_PARAM (pRbrgOldFsrbridgeNicknameEntry);
    UNUSED_PARAM (pRbrgIsSetFsrbridgeNicknameEntry);

    if (pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus ==
        RBRG_INVALID)
    {
        RBTreeRem (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                   (tRBElem *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        pRbrgFsrbridgeNicknameEntry = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgePortTable
 * Input       :   pRbrgOldFsRBridgeBasePortEntry
                   pRbrgFsRBridgeBasePortEntry
                   pRbrgIsSetFsRBridgeBasePortEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgePortTable (tRbrgFsRBridgeBasePortEntry *
                                  pRbrgOldFsRBridgeBasePortEntry,
                                  tRbrgFsRBridgeBasePortEntry *
                                  pRbrgFsRBridgeBasePortEntry,
                                  tRbrgIsSetFsRBridgeBasePortEntry *
                                  pRbrgIsSetFsRBridgeBasePortEntry)
{

    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;
    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) & FsPortEntry);
    if (pFsPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    /* If current value equal to configured value, then return */

    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort == TRUE)
    {
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortTrunkPort == TRUE)
        {
            /* Trunk Port to Access Port */
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = TRUE;
            /* Delete the trunk port */
            RbrgUtlDeletePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_DISABLE_ENTRY);
            /* Reset the trunk port Status */
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = FALSE;
        }
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortAccessPort == FALSE)
        {

            pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = TRUE;
            /* Enable Access Port */
            RbrgUtlCreatePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_ENABLE_ENTRY);
        }
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortAccessPort == TRUE)
        {

            /* Disable Access Port */
            pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = TRUE;
            /* Delete Access Port */
            RbrgUtlDeletePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_DISABLE_ENTRY);
            /* Reset Access Port */
            pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = FALSE;
        }
    }
    else if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort == TRUE)
    {
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortAccessPort == TRUE)
        {
            /* Access Port to trunk port */
            pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = TRUE;
            RbrgUtlDeletePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_DISABLE_ENTRY);
            /* Reset Access Port */
            pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = FALSE;
        }
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortTrunkPort == FALSE)
        {
            /* Enable Trunk Port */
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = TRUE;
            RbrgUtlCreatePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_ENABLE_ENTRY);
        }
        if (pRbrgOldFsRBridgeBasePortEntry->MibObject.
            i4FsrbridgePortTrunkPort == TRUE)
        {
            /* Disable Trunk Port */
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = TRUE;
            RbrgUtlDeletePort (pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex, RBRG_DISABLE_ENTRY);
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = FALSE;
        }
    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable == TRUE)
    {
        if (pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable ==
            TRUE)
        {
            pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortState =
                RBRG_PORT_DISABLED;
        }
        else
        {
            pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortState =
                RBRG_PORT_UNINHIBITED;
        }

    }
    if (pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters == TRUE)
    {
        RbrgPortVcmGetCxtInfoFromIfIndex ((UINT4) pFsPortEntry->MibObject.
                                          i4FsrbridgePortIfIndex, &u4ContextId,
                                          &u2LocalPortId);
        RbrgClearPortCounters (u4ContextId,
                               (UINT4) pRbrgFsRBridgeBasePortEntry->MibObject.
                               i4FsrbridgePortIfIndex);
        pFsPortEntry->MibObject.i4FsrbridgePortClearCounters = FALSE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgeUniFdbTable
 * Input       :   pRbrgOldFsRbridgeUniFdbEntry
                   pRbrgFsRbridgeUniFdbEntry
                   pRbrgIsSetFsRbridgeUniFdbEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                    pRbrgOldFsRbridgeUniFdbEntry,
                                    tRbrgFsRbridgeUniFdbEntry *
                                    pRbrgFsRbridgeUniFdbEntry,
                                    tRbrgIsSetFsRbridgeUniFdbEntry *
                                    pRbrgIsSetFsRbridgeUniFdbEntry)
{

    tRBrgNpEntry        RBrgHwInfo;
    tRBrgFibEntry       FibEntry;
    tRBrgFdbEntry       FdbEntry;
    tRBrgFdbEntry      *pFdbEntry = NULL;
    tRBrgFibEntry      *pFibEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1Index = 0;
    UINT1               u1Status = 0;

    UNUSED_PARAM (pRbrgOldFsRbridgeUniFdbEntry);
    UNUSED_PARAM (pRbrgIsSetFsRbridgeUniFdbEntry);

    MEMSET (&FdbEntry, 0, sizeof (tRBrgFdbEntry));
    MEMSET (&RBrgHwInfo, 0, sizeof (tRBrgNpEntry));
    MEMSET (&FibEntry, 0, sizeof (tRBrgFibEntry));

    if (pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus ==
        ACTIVE)
    {
        MEMSET (&RBrgHwInfo, 0, sizeof (tRBrgNpEntry));
        MEMSET (&FibEntry, 0, sizeof (tRBrgFibEntry));

        /* Get the corresponding FIB entry */
        FibEntry.u4ContextId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        FibEntry.u4FibNextHopRBrgPortId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        FibEntry.u4FibEgressRBrgNickname =
            pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        FibEntry.u4FibNextHopRBrgNickname = 0;
        pFibEntry = RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                                   (tRBElem *) & FibEntry, RbrgUtlFibTblCmpFn);
        if ((pFibEntry == NULL) ||
            ((pFibEntry != NULL) &&
             ((pFibEntry->u4ContextId != FibEntry.u4ContextId) ||
              (pFibEntry->u4FibNextHopRBrgPortId !=
               FibEntry.u4FibNextHopRBrgPortId) ||
              (pFibEntry->u4FibEgressRBrgNickname !=
               FibEntry.u4FibEgressRBrgNickname))))
        {
            RBRG_TRC (((RBRG_UTIL_TRC), "RbrgUtilUpdateFsrbridgeUniFdbTable: "
                       "FibEntry not found\r\n"));
            return OSIX_FAILURE;
        }

        if (pFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
        {
            u1Index = 1;
        }
        else
        {
            u1Index = 0;
        }
        if (pFibEntry->pRBrgFibData[u1Index] == NULL)
        {
            return OSIX_FAILURE;
        }

        /* Add FDB node from DLL maintained in FIB entry */
        FdbEntry.u4ContextId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        FdbEntry.u4RBrgFdbId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
        MEMCPY (FdbEntry.RBrgFdbMac,
                pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                MAC_ADDR_LEN);
        pFdbEntry = (tRBrgFdbEntry *) RBTreeGet
            (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, (tRBElem *) & FdbEntry);

        /* For Klocwork */
        if (pFdbEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        TMO_DLL_Add (&(pFibEntry->RBrgFibFdbList),
                     &(pFdbEntry->RBrgFibFdbNode));

        /* Create the entry in hardware */

        RBrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort =
            (UINT4) pFibEntry->u4TrunkTrillPort;
        RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
            (UINT4) pFibEntry->u4TrunkTrillPort;

        MEMCPY (&RBrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                MAC_ADDR_LEN);
        RBrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
        RBrgHwInfo.u4Command = RBRG_CREATE_UCAST_DEST;
        u4ContextId = pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        RBRG_IS_MODULE_ENABLE (u4ContextId, u1Status);
        if (u1Status == RBRG_ENABLE)
        {
            if (RbrgHwWrProgramEntry (&RBrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Programming Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
    }

    if (pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus ==
        DESTROY)
    {
        /* Get the corresponding FIB entry */
        FibEntry.u4ContextId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        FibEntry.u4FibNextHopRBrgPortId =
            pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        FibEntry.u4FibEgressRBrgNickname =
            pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        FibEntry.u4FibNextHopRBrgNickname = 0;
        pFibEntry = RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                                   (tRBElem *) & FibEntry, RbrgUtlFibTblCmpFn);
        if ((pFibEntry != NULL) &&
            ((pFibEntry->u4ContextId == FibEntry.u4ContextId) &&
             (pFibEntry->u4FibNextHopRBrgPortId ==
              FibEntry.u4FibNextHopRBrgPortId) &&
             (pFibEntry->u4FibEgressRBrgNickname ==
              FibEntry.u4FibEgressRBrgNickname)))
        {

            if (pFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
            {
                u1Index = 1;
            }
            else
            {
                u1Index = 0;
            }
            if (pFibEntry->pRBrgFibData[u1Index] == NULL)
            {
                return OSIX_FAILURE;
            }
            FdbEntry.u4ContextId =
                pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
            FdbEntry.u4RBrgFdbId =
                pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
            MEMCPY (FdbEntry.RBrgFdbMac,
                    pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                    MAC_ADDR_LEN);
            pFdbEntry = (tRBrgFdbEntry *) RBTreeGet
                (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                 (tRBElem *) & FdbEntry);

            /* For Klocwork */
            if (pFdbEntry == NULL)
            {
                return OSIX_FAILURE;
            }

            /* Delete FDB node from DLL maintained in FIB entry */
            TMO_DLL_Delete (&(pFibEntry->RBrgFibFdbList),
                            &(pFdbEntry->RBrgFibFdbNode));
        }

        /* Delete the FDB entry in Hardware */
        MEMCPY (&RBrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                MAC_ADDR_LEN);
        RBrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan =
            pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
        RBrgHwInfo.u4Command = RBRG_UCAST_DSET_DELETE;
        RBRG_IS_MODULE_ENABLE (u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RBrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Programming Failed \r\n"));
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgeUniFibTable
 * Input       :   pRbrgOldFsRbridgeUniFibEntry
                   pRbrgFsRbridgeUniFibEntry
                   pRbrgIsSetFsRbridgeUniFibEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                    pRbrgOldFsRbridgeUniFibEntry,
                                    tRbrgFsRbridgeUniFibEntry *
                                    pRbrgFsRbridgeUniFibEntry,
                                    tRbrgIsSetFsRbridgeUniFibEntry *
                                    pRbrgIsSetFsRbridgeUniFibEntry,
                                    UINT4 u4FibUpdate)
{
    tRBrgNpEntry        RBrgHwInfo;
    tRbrgFsRBridgeBasePortEntry RBrgPortEntry;
    tMacAddr            SwitchMac;
    tRBrgFibEntry       RbrgFibEntry;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRbrgFsRBridgeBasePortEntry *pRBrgPortEntry = NULL;
    UINT1               u1Status = 0;

    UNUSED_PARAM (pRbrgOldFsRbridgeUniFibEntry);
    UNUSED_PARAM (pRbrgIsSetFsRbridgeUniFibEntry);

    MEMSET (&RBrgPortEntry, 0, sizeof (tRbrgMibFsRBridgeBasePortEntry));
    MEMSET (&SwitchMac, 0, sizeof (tMacAddr));
    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    MEMSET (&RBrgHwInfo, 0, sizeof (tRBrgNpEntry));

    RbrgFibEntry.u4ContextId =
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;
    RbrgFibEntry.u4FibEgressRBrgNickname =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
    RbrgFibEntry.u4FibNextHopRBrgPortId =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
    RbrgFibEntry.u4FibNextHopRBrgNickname =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;
    pRbrgFibEntry = (tRBrgFibEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                   (tRBElem *) & RbrgFibEntry);
    /* For Klocwork */
    if (pRbrgFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    RBrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
    pRBrgPortEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                                (tRBElem *) & RBrgPortEntry);
    if (pRBrgPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus == ACTIVE)
    {
        if (pRBrgPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
        {

            RBrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
            RBrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_ACCESS_PORT;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU =
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired;
            if (u4FibUpdate == 0)
            {
                RBrgHwInfo.u4Command = RBRG_FIB_CREATE;
            }

            else
            {
                RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
                    pRBrgPortEntry->u4AccessTrillPort;
                RBrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId =
                    pRbrgFibEntry->i4L3EgressId;
                RBrgHwInfo.u4Command = RBRG_TRILL_PORT_UPDATE;
            }

            RBRG_IS_MODULE_ENABLE (RbrgFibEntry.u4ContextId, u1Status);

            if (u1Status == TRUE)
            {
                if (RbrgHwWrProgramEntry (&RBrgHwInfo) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "Hardware Programming Failed \r\n"));
                    return OSIX_FAILURE;
                }
                if (u4FibUpdate == 0)
                {

                    pRBrgPortEntry->u4AccessTrillPort =
                        RBrgHwInfo.RbrgNpRbrgEntry.i4AccessTrillPort;

                    pRbrgFibEntry->i4L3EgressId =
                        RBrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId;
                    pRBrgPortEntry->i4L3InterfaceIndex =
                        RBrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id;
                }
            }
        }
        if (pRBrgPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE)
        {
            RBrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
            RBrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU =
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired;

            /* Get SwitchMac from CFA  and copy it to egress_mac */
            RbrgPortCfaGetSysMacAddress (SwitchMac);
            MEMCPY (&RBrgHwInfo.RbrgNpRbrgEntry.egress_mac, &SwitchMac,
                    MAC_ADDR_LEN);
            RBrgHwInfo.RbrgNpRbrgEntry.u4EgressVlan = pRBrgPortEntry->MibObject.
                i4FsrbridgePortDesigVlan;
            MEMCPY (RBrgHwInfo.RbrgNpRbrgEntry.next_hop_mac,
                    pRbrgFsRbridgeUniFibEntry->MibObject.
                    au1FsrbridgeFibMacAddress,
                    pRbrgFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibMacAddressLen);

            if (u4FibUpdate == 0)
            {
                RBrgHwInfo.u4Command = RBRG_FIB_CREATE;

                RBRG_IS_MODULE_ENABLE (RbrgFibEntry.u4ContextId, u1Status);
                if (u1Status == TRUE)
                {
                    if (RbrgHwWrProgramEntry (&RBrgHwInfo) != OSIX_SUCCESS)
                    {
                        RBRG_TRC ((RBRG_UTIL_TRC,
                                   "Hardware Programming Failed \r\n"));
                        return OSIX_FAILURE;
                    }
                    if (u4FibUpdate == 0)
                    {

                        pRbrgFibEntry->u4TrunkTrillPort =
                            RBrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort;

                        pRbrgFibEntry->i4L3EgressId =
                            RBrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId;
                        pRBrgPortEntry->i4L3InterfaceIndex =
                            RBrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id;
                    }
                }

            }

            else
            {

                if (RbrgUtilReconfigureFib (pRbrgFibEntry, pRBrgPortEntry) ==
                    OSIX_FAILURE)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "Hardware Programming Failed \r\n"));
                    return OSIX_FAILURE;
                }
            }

        }
    }
    if (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus == DESTROY)
    {
        RBrgPortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
        pRBrgPortEntry = RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                                    (tRBElem *) & RBrgPortEntry);
        if (pRBrgPortEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        /* Delete FIB entry created in hardware */

        RBrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId = pRbrgFibEntry->i4L3EgressId;
        RBrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id =
            pRBrgPortEntry->i4L3InterfaceIndex;
        RBrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
        RBrgHwInfo.u4Command = RBRG_FIB_DELETE;

        if (pRBrgPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
        {
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
                pRBrgPortEntry->u4AccessTrillPort;
        }
        else if (pRBrgPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE)
        {

            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
                pRbrgFibEntry->u4TrunkTrillPort;
        }

        RBRG_IS_MODULE_ENABLE (RbrgFibEntry.u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RBrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Programming Failed \r\n"));
                return OSIX_FAILURE;
            }
            pRbrgFibEntry->i4L3EgressId = 0;
            pRbrgFibEntry->u4TrunkTrillPort = 0;

        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  RbrgUtilUpdateFsrbridgeMultiFibTable
 * Input       :   pRbrgOldFsrbridgeMultiFibEntry
                   pRbrgFsrbridgeMultiFibEntry
                   pRbrgIsSetFsrbridgeMultiFibEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtilUpdateFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                      pRbrgOldFsrbridgeMultiFibEntry,
                                      tRbrgFsrbridgeMultiFibEntry *
                                      pRbrgFsrbridgeMultiFibEntry,
                                      tRbrgIsSetFsrbridgeMultiFibEntry *
                                      pRbrgIsSetFsrbridgeMultiFibEntry)
{

    UNUSED_PARAM (pRbrgOldFsrbridgeMultiFibEntry);
    UNUSED_PARAM (pRbrgFsrbridgeMultiFibEntry);
    UNUSED_PARAM (pRbrgIsSetFsrbridgeMultiFibEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   RbrgUtlGetAllFsrbridgeUniFdbTable
 * Input       :   pRbrgGetFsRbridgeUniFdbEntry
 * Descritpion :  This Routine gets required value from database
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetAllFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                   pRbrgGetFsRbridgeUniFdbEntry)
{
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    UINT1               u1Index = 0;

    /* Check whether the node is already present */
    pRbrgFdbEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                   (tRBElem *) pRbrgFdbEntry);

    if (pRbrgFdbEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgeUniFdbTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    if ((pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_DYNAMIC) ||
        (pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_BOTH))
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    pRbrgGetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;

    pRbrgGetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;

    pRbrgGetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence;

    pRbrgGetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus = u1Index;

    pRbrgGetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlGetAllFsrbridgeUniFibTable
 Input       :  pRbrgGetFsRbridgeUniFibEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetAllFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                   pRbrgGetFsRbridgeUniFibEntry)
{
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    UINT1               u1Index = 0;

    /* Check whether the node is already present */
    pRbrgFibEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                   (tRBElem *) pRbrgFibEntry);
    if (pRbrgFibEntry == NULL)
    {
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgGetAllFsrbridgeUniFibTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */

    if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    MEMCPY (pRbrgGetFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
            sizeof (tMacAddr));

    pRbrgGetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen =
        MAC_ADDR_LEN;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;

    pRbrgGetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u1FibRowStatus;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlSetAllFsrbridgeUniFdbTable
 Input       :  pRbrgSetFsRbridgeUniFdbEntry
                pRbrgIsSetFsRbridgeUniFdbEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlSetAllFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                   pRbrgSetFsRbridgeUniFdbEntry,
                                   tRbrgIsSetFsRbridgeUniFdbEntry *
                                   pRbrgIsSetFsRbridgeUniFdbEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgOldFsRbridgeUniFdbEntry = NULL;
    tRbrgFsRbridgeUniFdbEntry *pRbrgTrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgTrgIsSetFsRbridgeUniFdbEntry = NULL;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRBrgFibEntry      *pFibEntry = NULL;
    tRBrgFdbEntry       RbrgFdbEntry;
    tRBrgFibEntry       FibEntry;
    tRbrgFsRbridgeUniFdbEntry RbrgFsRbridgeUniFdbEntry;
    INT4                i4RowMakeActive = FALSE;
    UINT1               u1Add = FALSE;
    UINT1               u1Index = FALSE;
    UINT1               u1DeleteNode = FALSE;

    MEMSET (&FibEntry, 0, sizeof (tRBrgFibEntry));
    MEMSET (&RbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));

    pRbrgOldFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgOldFsRbridgeUniFdbEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pRbrgTrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgTrgFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        return OSIX_FAILURE;
    }
    pRbrgTrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);
    if (pRbrgTrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pRbrgOldFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgTrgFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgTrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    RbrgFdbEntry.u4ContextId =
        pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
    RbrgFdbEntry.u4RBrgFdbId =
        pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
    MEMCPY (RbrgFdbEntry.RBrgFdbMac,
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
            MAC_ADDR_LEN);

    /* set the status */
    pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
        RBRG_FDB_STATIC;
    /* Check whether the node is already present */
    pRbrgFdbEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                   (tRBElem *) & RbrgFdbEntry);

    /* check if the entry is static or dynamic */
    if (pRbrgFdbEntry == NULL)
    {
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus ==
            RBRG_FDB_DYNAMIC)
        {
            u1Index = 1;
        }
        else
        {
            u1Index = 0;
        }
    }
    else if ((pRbrgFdbEntry != NULL) &&
             (pRbrgFdbEntry->pRBrgFdbData[RBRG_STATIC] != NULL) &&
             (pRbrgFdbEntry->pRBrgFdbData[RBRG_DYNAMIC] == NULL))
    {
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus ==
            RBRG_FDB_DYNAMIC)
        {
            u1Index = RBRG_DYNAMIC;
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
                RBRG_BOTH;
        }
        else
        {
            u1Index = 0;
        }
    }
    else if ((pRbrgFdbEntry != NULL) &&
             (pRbrgFdbEntry->pRBrgFdbData[RBRG_STATIC] == NULL) &&
             (pRbrgFdbEntry->pRBrgFdbData[RBRG_DYNAMIC] != NULL))
    {
        if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
            i4FsrbridgeUniFdbStatus == RBRG_FDB_STATIC)
        {
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                i4FsrbridgeUniFdbStatus = RBRG_FDB_BOTH;
            u1Index = 1;
        }
        else
        {
            u1Index = RBRG_DYNAMIC;
        }
    }

    if ((pRbrgFdbEntry != NULL) &&
        (pRbrgFdbEntry->pRBrgFdbData[u1Index] != NULL))
    {
        /* Already node is present so cant create again only updation is 
         * allowed */
        /* Node is already present */
        if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbRowStatus == CREATE_AND_WAIT)
            || (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO))
        {
            if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: "
                           "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                           "function returns failure.\r\n"));
            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: The row is "
                       "already present.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pRbrgFdbEntry != NULL) &&
             (pRbrgFdbEntry->pRBrgFdbData[u1Index] == NULL))
    {
        u1Add = FALSE;
        pRbrgFdbEntry->pRBrgFdbData[u1Index] =
            (tRBrgFdbData *) MemAllocMemBlk (RBRG_FDB_DATA_POOLID);
        if (pRbrgFdbEntry->pRBrgFdbData[u1Index] == NULL)
        {
            if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                (pRbrgSetFsRbridgeUniFdbEntry,
                 pRbrgIsSetFsRbridgeUniFdbEntry, SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable:"
                           " RbrgSetAllFsrbridgeUniFdbTableTrigger "
                           "function fails\r\n"));

            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: Fail to Allocate "
                       "Memory.\r\n"));
            MemReleaseMemBlock (RBRG_FDB_TABLE_POOLID, (UINT1 *) pRbrgFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            return OSIX_FAILURE;
        }
        MEMSET (pRbrgFdbEntry->pRBrgFdbData[u1Index], 0, sizeof (tRBrgFdbData));

    }

    else if (pRbrgFdbEntry == NULL)
    {
        u1Add = TRUE;
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or
         * CREATE_AND_GO */
        if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
             i4FsrbridgeUniFdbRowStatus == CREATE_AND_WAIT)
            || (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO)
            ||
            ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
              i4FsrbridgeUniFdbRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pRbrgFdbEntry =
                (tRBrgFdbEntry *) MemAllocMemBlk (RBRG_FDB_TABLE_POOLID);
            if (pRbrgFdbEntry == NULL)
            {
                if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                    (pRbrgSetFsRbridgeUniFdbEntry,
                     pRbrgIsSetFsRbridgeUniFdbEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable:"
                               " RbrgSetAllFsrbridgeUniFdbTableTrigger "
                               "function fails\r\n"));

                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: Fail to Allocate "
                           "Memory.\r\n"));
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pRbrgFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
            pRbrgFdbEntry->i1FdbStatus = (INT1)
                pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus;
            pRbrgFdbEntry->pRBrgFdbData[u1Index] =
                (tRBrgFdbData *) MemAllocMemBlk (RBRG_FDB_DATA_POOLID);
            if (pRbrgFdbEntry->pRBrgFdbData[u1Index] == NULL)
            {
                if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                    (pRbrgSetFsRbridgeUniFdbEntry,
                     pRbrgIsSetFsRbridgeUniFdbEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable:"
                               " RbrgSetAllFsrbridgeUniFdbTableTrigger "
                               "function fails\r\n"));

                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: Fail to Allocate "
                           "Memory.\r\n"));
                MemReleaseMemBlock (RBRG_FDB_TABLE_POOLID,
                                    (UINT1 *) pRbrgFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                return OSIX_FAILURE;
            }
            MEMSET (pRbrgFdbEntry->pRBrgFdbData[u1Index], 0,
                    sizeof (tRBrgFdbData));

            /* Assign values for the new node */
            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId != OSIX_FALSE)
            {
                pRbrgFdbEntry->u4RBrgFdbId =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
                RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeFdbId =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr !=
                OSIX_FALSE)
            {
                MEMCPY (&(pRbrgFdbEntry->RBrgFdbMac),
                        &(pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                          FsrbridgeUniFdbAddr), MAC_ADDR_LEN);
                MEMCPY (&(RbrgFsRbridgeUniFdbEntry.MibObject.
                          FsrbridgeUniFdbAddr),
                        &(pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                          FsrbridgeUniFdbAddr), MAC_ADDR_LEN);
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort !=
                OSIX_FALSE)
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort =
                    (UINT4) pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbPort;
                RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbPort =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbPort;
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick !=
                OSIX_FALSE)
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbNick;
                RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbNick =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbNick;
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence !=
                OSIX_FALSE)
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    u4FsrbridgeUniFdbConfidence;
                RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeUniFdbConfidence =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    u4FsrbridgeUniFdbConfidence;
            }
            else
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence =
                    RBRG_DEFAULT_CONFIDENCE;
                RbrgFsRbridgeUniFdbEntry.MibObject.
                    u4FsrbridgeUniFdbConfidence = RBRG_DEFAULT_CONFIDENCE;
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus !=
                OSIX_FALSE)
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbRowStatus;
                RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbRowStatus;
            }

            if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId !=
                OSIX_FALSE)
            {
                pRbrgFdbEntry->u4ContextId =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    u4FsrbridgeContextId;
                RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeContextId =
                    pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    u4FsrbridgeContextId;

            }

            if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                 i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        i4FsrbridgeUniFdbRowStatus == ACTIVE)))
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus = ACTIVE;
                RbrgFsRbridgeUniFdbEntry.MibObject.
                    i4FsrbridgeUniFdbRowStatus = ACTIVE;
            }
            else if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                     i4FsrbridgeUniFdbRowStatus == CREATE_AND_WAIT)
            {
                pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus =
                    NOT_READY;
                RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus =
                    NOT_READY;
            }
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
                RBRG_FDB_STATIC;
            if (u1Add == TRUE)
            {
                /* Add the new node to the database */
                if (RBTreeAdd
                    (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                     (tRBElem *) pRbrgFdbEntry) != RB_SUCCESS)
                {
                    if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                        (pRbrgSetFsRbridgeUniFdbEntry,
                         pRbrgIsSetFsRbridgeUniFdbEntry,
                         SNMP_FAILURE) != OSIX_SUCCESS)

                    {
                        RBRG_TRC ((RBRG_UTIL_TRC,
                                   "RbrgSetAllFsrbridgeUniFdbTable: "
                                   "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                                   "function returns failure.\r\n"));
                    }
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable: "
                               "RBTreeAdd is failed.\r\n"));

                    MemReleaseMemBlock (RBRG_FDB_TABLE_POOLID,
                                        (UINT1 *) pRbrgFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                    return OSIX_FAILURE;
                }
            }
            if ((pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == TRUE)
                &&
                (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence !=
                 TRUE))
            {
                if (RbrgUtilUpdateFsrbridgeUniFdbTable
                    (NULL, &RbrgFsRbridgeUniFdbEntry,
                     pRbrgIsSetFsRbridgeUniFdbEntry) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable: "
                               "RbrgUtilUpdateFsrbridgeUniFdbTable "
                               "function returns failure.\r\n"));

                    if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                        (pRbrgSetFsRbridgeUniFdbEntry,
                         pRbrgIsSetFsRbridgeUniFdbEntry,
                         SNMP_FAILURE) != OSIX_SUCCESS)

                    {
                        RBRG_TRC ((RBRG_UTIL_TRC,
                                   "RbrgSetAllFsrbridgeUniFdbTable: "
                                   "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                                   "function returns failure.\r\n"));

                    }
                    if (u1Add == TRUE)
                    {
                        RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                                   pRbrgFdbEntry);
                    }
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                    return OSIX_FAILURE;
                }
            }
            if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                 i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        i4FsrbridgeUniFdbRowStatus == ACTIVE)))
            {

                if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO)
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId =
                        pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeFdbId;
                    MEMCPY (&
                            (pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                             FsrbridgeUniFdbAddr),
                            &(pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                              FsrbridgeUniFdbAddr), 6);
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                        i4FsrbridgeUniFdbRowStatus = CREATE_AND_GO;
                    pRbrgTrgIsSetFsRbridgeUniFdbEntry->
                        bFsrbridgeUniFdbRowStatus = OSIX_TRUE;

                }
                else
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId =
                        pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeFdbId;
                    MEMCPY (&
                            (pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                             FsrbridgeUniFdbAddr),
                            &(pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                              FsrbridgeUniFdbAddr), 6);
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                        i4FsrbridgeUniFdbRowStatus = CREATE_AND_WAIT;
                    pRbrgTrgIsSetFsRbridgeUniFdbEntry->
                        bFsrbridgeUniFdbRowStatus = OSIX_TRUE;

                }
                if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                    (pRbrgTrgFsRbridgeUniFdbEntry,
                     pRbrgTrgIsSetFsRbridgeUniFdbEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable: "
                               "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                               "function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                     i4FsrbridgeUniFdbRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pRbrgTrgFsRbridgeUniFdbEntry->MibObject.
                    i4FsrbridgeUniFdbRowStatus = CREATE_AND_WAIT;
                pRbrgTrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus =
                    OSIX_TRUE;

                if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                    (pRbrgTrgFsRbridgeUniFdbEntry,
                     pRbrgTrgIsSetFsRbridgeUniFdbEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable: "
                               "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                               "function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                    return OSIX_FAILURE;
                }
            }
            if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable:  "
                           "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                           "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            return OSIX_SUCCESS;
        }
    }
    else if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
              i4FsrbridgeUniFdbRowStatus == CREATE_AND_WAIT)
             || (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                 i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO))

    {
        if (RbrgSetAllFsrbridgeUniFdbTableTrigger
            (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: "
                       "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                       "function returns failure.\r\n"));
        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFdbTable: Failure.\r\n"));
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    RBrgUtlCopyRBrgFdbToFsFdb (pRbrgOldFsRbridgeUniFdbEntry, pRbrgFdbEntry);

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus ==
        DESTROY)
    {
        MEMCPY (&RbrgFsRbridgeUniFdbEntry, pRbrgOldFsRbridgeUniFdbEntry,
                sizeof (tRbrgFsRbridgeUniFdbEntry));

        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus = DESTROY;
        if (RbrgUtilUpdateFsrbridgeUniFdbTable (pRbrgOldFsRbridgeUniFdbEntry,
                                                &RbrgFsRbridgeUniFdbEntry,
                                                pRbrgIsSetFsRbridgeUniFdbEntry)
            != OSIX_SUCCESS)
        {

            if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: "
                           "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                           "function returns failure.\r\n"));
            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: "
                       "RbrgUtilUpdateFsrbridgeUniFdbTable function "
                       "returns failure.\r\n"));

        }

        /* Delete the node from DLL list maintained in Fib entry */
        if (pRbrgFdbEntry == NULL)
        {
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            CLI_SET_ERR (RBRG_CLI_FDB_ENTRY_NOT_FOUND);
            return OSIX_FAILURE;
        }
        if ((u1Index == RBRG_STATIC) &&
            (pRbrgFdbEntry->pRBrgFdbData[RBRG_DYNAMIC] == NULL))
        {
            u1DeleteNode = TRUE;
        }
        else if ((u1Index == RBRG_DYNAMIC) &&
                 (pRbrgFdbEntry->pRBrgFdbData[RBRG_STATIC] == NULL))
        {
            u1DeleteNode = TRUE;
        }
        else
        {
            u1DeleteNode = FALSE;
        }

        /* Delete the Node from DLL list in FIB entry */
        FibEntry.u4ContextId = pRbrgFdbEntry->u4ContextId;
        FibEntry.u4FibEgressRBrgNickname =
            pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;
        FibEntry.u4FibNextHopRBrgPortId =
            pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;
        pFibEntry = (tRBrgFibEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable, &FibEntry);
        if (pFibEntry != NULL)
        {
            TMO_DLL_Delete (&(pFibEntry->RBrgFibFdbList),
                            &(pRbrgFdbEntry->RBrgFibFdbNode));
            RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, pRbrgFdbEntry);
        }
        /* Delete the data from FDB entry */
        MemReleaseMemBlock (RBRG_FDB_DATA_POOLID,
                            (UINT1 *) pRbrgFdbEntry->pRBrgFdbData[u1Index]);
        pRbrgFdbEntry->pRBrgFdbData[u1Index] = NULL;

        /* If both static and dynamic data are NULL, delete the FDB entry */
        if (u1DeleteNode == TRUE)
        {
            RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                       (tRBElem *) pRbrgFdbEntry);
            MemReleaseMemBlock (RBRG_FDB_TABLE_POOLID, (UINT1 *) pRbrgFdbEntry);
            pRbrgFdbEntry = NULL;
        }
        if (RbrgSetAllFsrbridgeUniFdbTableTrigger
            (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: "
                       "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                       "function returns failure.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsrbridgeUniFdbTableFilterInputs
        (pRbrgOldFsRbridgeUniFdbEntry, pRbrgSetFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying 
     * the values */
    if ((i4RowStatusLogic == TRUE) &&
        (RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus ==
         ACTIVE)
        && (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
            i4FsrbridgeUniFdbRowStatus != NOT_IN_SERVICE))
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pRbrgTrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
            NOT_IN_SERVICE;
        pRbrgTrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus =
            OSIX_TRUE;

        if ((pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == TRUE) &&
            (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence !=
             TRUE))
        {
            if (RbrgUtilUpdateFsrbridgeUniFdbTable
                (pRbrgOldFsRbridgeUniFdbEntry, &RbrgFsRbridgeUniFdbEntry,
                 pRbrgIsSetFsRbridgeUniFdbEntry) != OSIX_SUCCESS)
            {
                /*Restore back with previous values */
                MEMCPY (&RbrgFsRbridgeUniFdbEntry, pRbrgOldFsRbridgeUniFdbEntry,
                        sizeof (tRbrgFsRbridgeUniFdbEntry));
                /* Copy to old ds */
                RBrgUtlCopyFsFdbToRBrgFdb (&RbrgFsRbridgeUniFdbEntry,
                                           pRbrgFdbEntry);
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: "
                           "RbrgUtilUpdateFsrbridgeUniFdbTable "
                           "Function returns failure.\r\n"));

                if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                    (pRbrgSetFsRbridgeUniFdbEntry,
                     pRbrgIsSetFsRbridgeUniFdbEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFdbTable: "
                               "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                               "function returns failure.\r\n"));
                }
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFdbEntry);
                return OSIX_FAILURE;
            }
        }

        if (RbrgSetAllFsrbridgeUniFdbTableTrigger (pRbrgTrgFsRbridgeUniFdbEntry,
                                                   pRbrgTrgIsSetFsRbridgeUniFdbEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: "
                       "RbrgSetAllFsrbridgeUniFdbTableTrigger "
                       "function returns failure.\r\n"));
        }
    }

    if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* For Klocwork */
    if (pRbrgFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
        return OSIX_FAILURE;
    }

    /* Assign values for the existing node */
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort != OSIX_FALSE)
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbPort =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick != OSIX_FALSE)
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbNick =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence !=
        OSIX_FALSE)
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeUniFdbConfidence =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence;
    }
    if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus != OSIX_FALSE)
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the
     * values */
    if (i4RowMakeActive == TRUE)
    {
        RbrgFsRbridgeUniFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus = ACTIVE;
    }

    if (i4RowMakeActive == TRUE)
    {

        if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick == OSIX_FALSE)
        {
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                i4FsrbridgeUniFdbNick =
                pRbrgOldFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        }
        if (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort == OSIX_FALSE)
        {
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.
                i4FsrbridgeUniFdbPort =
                pRbrgOldFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        }

        if (RbrgUtilUpdateFsrbridgeUniFdbTable (pRbrgOldFsRbridgeUniFdbEntry,
                                                pRbrgSetFsRbridgeUniFdbEntry,
                                                pRbrgIsSetFsRbridgeUniFdbEntry)
            != OSIX_SUCCESS)
        {

            if (RbrgSetAllFsrbridgeUniFdbTableTrigger
                (pRbrgSetFsRbridgeUniFdbEntry,
                 pRbrgIsSetFsRbridgeUniFdbEntry, SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFdbTable: "
                           "RbrgSetAllFsrbridgeUniFdbTableTrigger function "
                           "returns failure.\r\n"));

            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFdbTable: "
                       "RbrgUtilUpdateFsrbridgeUniFdbTable function returns "
                       "failure.\r\n"));
            /*Restore back with previous values */
            MEMCPY (pRbrgSetFsRbridgeUniFdbEntry, pRbrgOldFsRbridgeUniFdbEntry,
                    sizeof (tRbrgFsRbridgeUniFdbEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
            return OSIX_FAILURE;

        }
    }
    if (RbrgSetAllFsrbridgeUniFdbTableTrigger (pRbrgSetFsRbridgeUniFdbEntry,
                                               pRbrgIsSetFsRbridgeUniFdbEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFdbTable: "
                   "RbrgSetAllFsrbridgeUniFdbTableTrigger function "
                   "returns failure.\r\n"));
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgOldFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgTrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFdbEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  RBrgUtlCopyFsFibToRBrgFib
 Input       :  pFsFibEntry
                pRBrgFibEntry
 Description :  This Routine copies the contents of 
                tRbrgFsRbridgeUniFibEntry to tRBrgFibEntry
 Output      :  None
 Returns     :  None
****************************************************************************/
PUBLIC VOID
RBrgUtlCopyFsFibToRBrgFib (tRbrgFsRbridgeUniFibEntry * pFsFibEntry,
                           tRBrgFibEntry * pRbrgFibEntry)
{
    UINT1               u1Index = 0;

    if ((pFsFibEntry != NULL) && (pRbrgFibEntry != NULL))
    {
        if (pFsFibEntry->MibObject.i4FsrbridgeUniFibStatus == RBRG_FIB_STATIC)
        {
            u1Index = 0;
        }
        else
        {
            u1Index = 1;
        }
        if (pRbrgFibEntry->pRBrgFibData[u1Index] == NULL)
        {
            /*Allocate memory for FibData */
            pRbrgFibEntry->pRBrgFibData[u1Index] =
                MemAllocMemBlk (RBRG_FIB_DATA_POOLID);
            if (pRbrgFibEntry->pRBrgFibData[u1Index] == NULL)
            {
                return;
            }
        }
        pRbrgFibEntry->u4ContextId =
            pFsFibEntry->MibObject.u4FsrbridgeContextId;
        pRbrgFibEntry->u4FibEgressRBrgNickname =
            pFsFibEntry->MibObject.i4FsrbridgeFibNickname;
        pRbrgFibEntry->u4FibNextHopRBrgPortId =
            pFsFibEntry->MibObject.i4FsrbridgeFibPort;
        pRbrgFibEntry->u4FibNextHopRBrgNickname =
            pFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;
        pRbrgFibEntry->i1FibStatus =
            pFsFibEntry->MibObject.i4FsrbridgeUniFibStatus;
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu =
            pFsFibEntry->MibObject.u4FsrbridgeFibMtuDesired;
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount =
            pFsFibEntry->MibObject.u4FsrbridgeFibHopCount;

        pRbrgFibEntry->pRBrgFibData[u1Index]->u1FibRowStatus = (UINT1)
            pFsFibEntry->MibObject.i4FsrbridgeFibRowstatus;

        MEMCPY (pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
                pFsFibEntry->MibObject.au1FsrbridgeFibMacAddress, MAC_ADDR_LEN);
    }
}

/****************************************************************************
 Function    :  RBrgUtlCopyRBrgFibToFsFib
 Input       :  pFsFibEntry
                pRBrgFibEntry
 Description :  This Routine copies the contents of tRBrgFibEntry to
                tRbrgFsRbridgeUniFibEntry 
 Output      :  None
 Returns     :  None
****************************************************************************/
PUBLIC VOID
RBrgUtlCopyRBrgFibToFsFib (tRbrgFsRbridgeUniFibEntry * pFsFibEntry,
                           tRBrgFibEntry * pRbrgFibEntry)
{
    UINT1               u1Index = 0;

    if ((pFsFibEntry != NULL) && (pRbrgFibEntry != NULL))
    {

        if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
        {
            u1Index = RBRG_DYNAMIC;
        }
        else
        {
            u1Index = RBRG_STATIC;
        }
        if (pRbrgFibEntry->pRBrgFibData[u1Index] == NULL)
        {
            return;
        }
        pFsFibEntry->MibObject.u4FsrbridgeContextId =
            pRbrgFibEntry->u4ContextId;
        pFsFibEntry->MibObject.i4FsrbridgeFibNickname =
            pRbrgFibEntry->u4FibEgressRBrgNickname;
        pFsFibEntry->MibObject.i4FsrbridgeFibPort =
            pRbrgFibEntry->u4FibNextHopRBrgPortId;
        pFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
            pRbrgFibEntry->u4FibNextHopRBrgNickname;
        pFsFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
            pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;
        pFsFibEntry->MibObject.u4FsrbridgeFibHopCount =
            pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;

        pFsFibEntry->MibObject.i4FsrbridgeFibMacAddressLen = MAC_ADDR_LEN;
        pFsFibEntry->MibObject.i4FsrbridgeFibRowstatus = (INT4)
            pRbrgFibEntry->pRBrgFibData[u1Index]->u1FibRowStatus;

        MEMCPY (pFsFibEntry->MibObject.au1FsrbridgeFibMacAddress,
                pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
                MAC_ADDR_LEN);
        pFsFibEntry->MibObject.i4FsrbridgeUniFibStatus =
            pRbrgFibEntry->i1FibStatus;
    }
}

/****************************************************************************
 Function    :  RBrgUtlCopyFsFdbToRBrgFdb
 Input       :  pFsFdbEntry
                pRBrgFdbEntry
 Description :  This Routine copies the contents of 
                tRbrgFsRbridgeUniFdbEntry to tRBrgFdbEntry
 Output      :  None
 Returns     :  None
****************************************************************************/
PUBLIC VOID
RBrgUtlCopyFsFdbToRBrgFdb (tRbrgFsRbridgeUniFdbEntry * pFsFdbEntry,
                           tRBrgFdbEntry * pRbrgFdbEntry)
{
    UINT1               u1Index = 0;

    if ((pFsFdbEntry != NULL) && (pRbrgFdbEntry != NULL))
    {
        if (pFsFdbEntry->MibObject.i4FsrbridgeUniFdbStatus == RBRG_FDB_STATIC)
        {
            u1Index = RBRG_STATIC;
        }
        else
        {
            u1Index = RBRG_DYNAMIC;
        }
        pRbrgFdbEntry->u4RBrgFdbId = pFsFdbEntry->MibObject.u4FsrbridgeFdbId;
        MEMCPY (&(pRbrgFdbEntry->RBrgFdbMac),
                &(pFsFdbEntry->MibObject.FsrbridgeUniFdbAddr), MAC_ADDR_LEN);
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort =
            pFsFdbEntry->MibObject.i4FsrbridgeUniFdbPort;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname =
            pFsFdbEntry->MibObject.i4FsrbridgeUniFdbNick;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence =
            pFsFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence;
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus =
            pFsFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus;
        pRbrgFdbEntry->u4ContextId =
            pFsFdbEntry->MibObject.u4FsrbridgeContextId;

    }
}

/****************************************************************************
 Function    :  RBrgUtlCopyRBrgFdbToFsFdb
 Input       :  pFsFdbEntry
                pRBrgFdbEntry
 Description :  This Routine copies the contents of tRBrgFdbEntry to
                tRbrgFsRbridgeUniFdbEntry
 Output      :  None
 Returns     :  None
****************************************************************************/
PUBLIC VOID
RBrgUtlCopyRBrgFdbToFsFdb (tRbrgFsRbridgeUniFdbEntry * pFsFdbEntry,
                           tRBrgFdbEntry * pRBrgFdbEntry)
{
    UINT1               u1Index = 0;

    if ((pFsFdbEntry != NULL) && (pRBrgFdbEntry != NULL))
    {
        if ((pRBrgFdbEntry->i1FdbStatus == RBRG_FDB_DYNAMIC) ||
            (pRBrgFdbEntry->i1FdbStatus == RBRG_FDB_BOTH))
        {
            u1Index = RBRG_DYNAMIC;
        }
        else
        {
            u1Index = RBRG_STATIC;
        }
        pFsFdbEntry->MibObject.u4FsrbridgeFdbId = pRBrgFdbEntry->u4RBrgFdbId;
        MEMCPY (&(pFsFdbEntry->MibObject.FsrbridgeUniFdbAddr),
                &(pRBrgFdbEntry->RBrgFdbMac), MAC_ADDR_LEN);
        pFsFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
            pRBrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;
        pFsFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
            pRBrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;
        pFsFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
            pRBrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence;
        pFsFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
            pRBrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus;
        pFsFdbEntry->MibObject.u4FsrbridgeContextId =
            pRBrgFdbEntry->u4ContextId;

    }
}

/****************************************************************************
 Function    :  RbrgUtlSetAllFsrbridgeUniFibTable
 Input       :  pRbrgSetFsRbridgeUniFibEntry
                pRbrgIsSetFsRbridgeUniFibEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlSetAllFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                   pRbrgSetFsRbridgeUniFibEntry,
                                   tRbrgIsSetFsRbridgeUniFibEntry *
                                   pRbrgIsSetFsRbridgeUniFibEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tRBrgFibEntry       RBrgFibEntry;
    tRbrgFsRBridgeBasePortEntry PortEntry;
    tRbrgFsRBridgeBasePortEntry *pPortEntry = NULL;
    tRBrgFibEntry      *pRBrgFibEntry = NULL;
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgFsRbridgeUniFibEntry *pRbrgOldFsRbridgeUniFibEntry = NULL;
    tRbrgFsRbridgeUniFibEntry *pRbrgTrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgTrgIsSetFsRbridgeUniFibEntry = NULL;
    INT4                i4RowMakeActive = FALSE;
    UINT4               u4FibUpdate = 0;

    MEMSET (&PortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    PortEntry.MibObject.i4FsrbridgePortIfIndex =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;

    pPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&RBrgFibEntry, 0, sizeof (tRBrgFibEntry));
    pRbrgOldFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgOldFsRbridgeUniFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pRbrgTrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgTrgFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        return OSIX_FAILURE;
    }
    pRbrgTrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);
    if (pRbrgTrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pRbrgOldFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgTrgFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgTrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    if (pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
        ACTIVE)
    {
        u4FibUpdate = 1;
    }

    RBrgFibEntry.u4ContextId =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;
    RBrgFibEntry.u4FibEgressRBrgNickname =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
    RBrgFibEntry.u4FibNextHopRBrgPortId =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
    RBrgFibEntry.u4FibNextHopRBrgNickname =
        pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;
    /* Allocate memory for Fsrbridge entry */
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        if (RbrgSetAllFsrbridgeUniFibTableTrigger
            (pRbrgSetFsRbridgeUniFibEntry,
             pRbrgIsSetFsRbridgeUniFibEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable:"
                       "RbrgSetAllFsrbridgeUniFibTableTrigger "
                       "function fails\r\n"));

        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFibTable: Fail to "
                   "Allocate Memory.\r\n"));
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
        return OSIX_FAILURE;
    }

    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    /* Check whether the node is already present */
    pRBrgFibEntry =
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                   (tRBElem *) & RBrgFibEntry);

    if (pRBrgFibEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT 
         * or CREATE_AND_GO */
        if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
             CREATE_AND_WAIT)
            || (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibRowstatus == CREATE_AND_GO)
            ||
            ((pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            /* Allocated memory for RbrgFibEntry */
            pRBrgFibEntry =
                (tRBrgFibEntry *) MemAllocMemBlk (RBRG_FIB_TABLE_POOLID);
            if (pRBrgFibEntry == NULL)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: Fail to "
                           "Allocate Memory.\r\n"));
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsRbridgeUniFibEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pRBrgFibEntry, 0, sizeof (tRBrgFibEntry));
            TMO_DLL_Init (&(pRBrgFibEntry->RBrgFibFdbList));
            pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeUniFibStatus =
                RBRG_FIB_STATIC;
            /* Assign values for the new node */
            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibNickname;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort != OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibNextHopRBridge =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibNextHopRBridge;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pRbrgFsRbridgeUniFibEntry->MibObject.
                        au1FsrbridgeFibMacAddress,
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        au1FsrbridgeFibMacAddress,
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibMacAddressLen);

                pRbrgFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibMacAddressLen =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibMacAddressLen;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    u4FsrbridgeFibMtuDesired;
            }
            else
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
                    RBRG_DEFAULT_MTU;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    u4FsrbridgeFibHopCount;
            }
            else
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
                    RBRG_DEFAULT_HOPCOUNT;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibRowstatus;
            }

            if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId !=
                OSIX_FALSE)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
                    pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    u4FsrbridgeContextId;
            }
            if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.
                 i4FsrbridgeFibRowstatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibRowstatus == ACTIVE)))
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
                    ACTIVE;
            }
            else if (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                     i4FsrbridgeFibRowstatus == CREATE_AND_WAIT)
            {
                pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
                    NOT_READY;
            }
            RBrgUtlCopyFsFibToRBrgFib (pRbrgFsRbridgeUniFibEntry,
                                       pRBrgFibEntry);

            /* Add the new node to the database */
            if (RBTreeAdd
                (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                 (tRBElem *) pRBrgFibEntry) != RB_SUCCESS)
            {
                if (RbrgSetAllFsrbridgeUniFibTableTrigger
                    (pRbrgSetFsRbridgeUniFibEntry,
                     pRbrgIsSetFsRbridgeUniFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFibTable: "
                               "RbrgSetAllFsrbridgeUniFibTableTrigger "
                               "function returns failure.\r\n"));
                }
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: "
                           "RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRBrgFibEntry->
                                    pRBrgFibData[RBRG_STATIC]);

                MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID,
                                    (UINT1 *) pRBrgFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFibEntry);
                return OSIX_FAILURE;
            }

            /* Increment Port Fib Count */
            pPortEntry->u4ReferenceCount++;

            if (RbrgUtilUpdateFsrbridgeUniFibTable
                (NULL, pRbrgFsRbridgeUniFibEntry,
                 pRbrgIsSetFsRbridgeUniFibEntry, u4FibUpdate) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: "
                           "RbrgUtilUpdateFsrbridgeUniFibTable function "
                           "returns failure.\r\n"));

                if (RbrgSetAllFsrbridgeUniFibTableTrigger
                    (pRbrgSetFsRbridgeUniFibEntry,
                     pRbrgIsSetFsRbridgeUniFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFibTable: "
                               "RbrgSetAllFsrbridgeUniFibTableTrigger "
                               "function returns failure.\r\n"));

                }
                RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           pRBrgFibEntry);

                /* Decrement Port Fib Count */
                pPortEntry->u4ReferenceCount--;

                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRBrgFibEntry->
                                    pRBrgFibData[RBRG_STATIC]);

                MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID,
                                    (UINT1 *) pRBrgFibEntry);

                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pRbrgTrgIsSetFsRbridgeUniFibEntry);
                return OSIX_FAILURE;
            }

            if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.
                 i4FsrbridgeFibRowstatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibRowstatus == ACTIVE)))
            {

                if (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibRowstatus == CREATE_AND_GO)
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNickname =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNickname;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibPort;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNextHopRBridge =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNextHopRBridge;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibRowstatus = CREATE_AND_GO;
                    pRbrgTrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus =
                        OSIX_TRUE;

                }
                else
                {
                    /* For MSR and RM Trigger */
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNickname =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNickname;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibPort;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNextHopRBridge =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibNextHopRBridge;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        u4FsrbridgeContextId =
                        pRbrgSetFsRbridgeUniFibEntry->MibObject.
                        u4FsrbridgeContextId;
                    pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                        i4FsrbridgeFibRowstatus = CREATE_AND_WAIT;
                    pRbrgTrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus =
                        OSIX_TRUE;

                }
                if (RbrgSetAllFsrbridgeUniFibTableTrigger
                    (pRbrgTrgFsRbridgeUniFibEntry,
                     pRbrgTrgIsSetFsRbridgeUniFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFibTable: "
                               "RbrgSetAllFsrbridgeUniFibTableTrigger "
                               "function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID,
                                        (UINT1 *) pRBrgFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFibEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                     i4FsrbridgeFibRowstatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pRbrgTrgFsRbridgeUniFibEntry->MibObject.
                    i4FsrbridgeFibRowstatus = CREATE_AND_WAIT;
                pRbrgTrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus =
                    OSIX_TRUE;

                if (RbrgSetAllFsrbridgeUniFibTableTrigger
                    (pRbrgTrgFsRbridgeUniFibEntry,
                     pRbrgTrgIsSetFsRbridgeUniFibEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    RBRG_TRC ((RBRG_UTIL_TRC,
                               "RbrgSetAllFsrbridgeUniFibTable: "
                               "RbrgSetAllFsrbridgeUniFibTableTrigger "
                               "function returns failure.\r\n"));

                    MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                        (UINT1 *) pRBrgFibEntry->
                                        pRBrgFibData[RBRG_STATIC]);

                    MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID,
                                        (UINT1 *) pRBrgFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                        (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                        (UINT1 *)
                                        pRbrgTrgIsSetFsRbridgeUniFibEntry);
                    return OSIX_FAILURE;
                }
            }
            if (RbrgSetAllFsrbridgeUniFibTableTrigger
                (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable:  "
                           "RbrgSetAllFsrbridgeUniFibTableTrigger "
                           "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (RbrgSetAllFsrbridgeUniFibTableTrigger
                (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: "
                           "RbrgSetAllFsrbridgeUniFibTableTrigger "
                           "function returns failure.\r\n"));
            }
            CLI_SET_ERR (RBRG_CLI_FIB_DELETION_FAILED);
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: Failure.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
              CREATE_AND_WAIT)
             || (pRbrgSetFsRbridgeUniFibEntry->MibObject.
                 i4FsrbridgeFibRowstatus == CREATE_AND_GO))
    {
        RBrgUtlCopyRBrgFibToFsFib (pRbrgFsRbridgeUniFibEntry, pRBrgFibEntry);
        if (RbrgSetAllFsrbridgeUniFibTableTrigger (pRbrgSetFsRbridgeUniFibEntry,
                                                   pRbrgIsSetFsRbridgeUniFibEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgSetAllFsrbridgeUniFibTableTrigger function "
                       "returns failure.\r\n"));
        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFibTable: "
                   "The row is already present.\r\n"));
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return OSIX_FAILURE;
    }
    else
    {
        RBrgUtlCopyRBrgFibToFsFib (pRbrgFsRbridgeUniFibEntry, pRBrgFibEntry);
    }

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
        DESTROY)
    {
        RBrgUtlCopyFsFibToRBrgFib (pRbrgSetFsRbridgeUniFibEntry, &RBrgFibEntry);
        pRBrgFibEntry =
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                       (tRBElem *) & RBrgFibEntry);
        /* For Klocwork */
        if (pRBrgFibEntry == NULL)
        {
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return OSIX_FAILURE;
        }

        MEMCPY (pRbrgFsRbridgeUniFibEntry, pRbrgSetFsRbridgeUniFibEntry,
                sizeof (tRbrgFsRbridgeUniFibEntry));
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus = DESTROY;

        if (RbrgUtilUpdateFsrbridgeUniFibTable (pRbrgOldFsRbridgeUniFibEntry,
                                                pRbrgFsRbridgeUniFibEntry,
                                                pRbrgIsSetFsRbridgeUniFibEntry,
                                                u4FibUpdate) != OSIX_SUCCESS)
        {

            if (RbrgSetAllFsrbridgeUniFibTableTrigger
                (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: "
                           "RbrgSetAllFsrbridgeUniFibTableTrigger "
                           "function returns failure.\r\n"));
            }
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgUtilUpdateFsrbridgeUniFibTable "
                       "function returns failure.\r\n"));
        }
        RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable, pRBrgFibEntry);

        /* Decrement Port Fib Count */
        pPortEntry->u4ReferenceCount--;

        if (pRBrgFibEntry->pRBrgFibData[RBRG_STATIC] != NULL)
        {
            MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                (UINT1 *) pRBrgFibEntry->
                                pRBrgFibData[RBRG_STATIC]);
        }
        if (pRBrgFibEntry->pRBrgFibData[RBRG_DYNAMIC] != NULL)
        {
            MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                (UINT1 *) pRBrgFibEntry->
                                pRBrgFibData[RBRG_DYNAMIC]);
        }

        MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID, (UINT1 *) pRBrgFibEntry);
        pRBrgFibEntry = NULL;
        if (RbrgSetAllFsrbridgeUniFibTableTrigger
            (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgSetAllFsrbridgeUniFibTableTrigger "
                       "function returns failure.\r\n"));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);

        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (pRbrgOldFsRbridgeUniFibEntry, pRbrgFsRbridgeUniFibEntry,
            sizeof (tRbrgFsRbridgeUniFibEntry));

    /*Function to check whether the given input is same as there in database */
    if (FsrbridgeUniFibTableFilterInputs
        (pRbrgFsRbridgeUniFibEntry, pRbrgSetFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying 
     * the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus == ACTIVE)
        && (pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus !=
            NOT_IN_SERVICE))
    {
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pRbrgTrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
            NOT_IN_SERVICE;
        pRbrgTrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_TRUE;

        if (RbrgUtilUpdateFsrbridgeUniFibTable (pRbrgOldFsRbridgeUniFibEntry,
                                                pRbrgFsRbridgeUniFibEntry,
                                                pRbrgIsSetFsRbridgeUniFibEntry,
                                                u4FibUpdate) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pRbrgFsRbridgeUniFibEntry, pRbrgOldFsRbridgeUniFibEntry,
                    sizeof (tRbrgFsRbridgeUniFibEntry));
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgUtilUpdateFsrbridgeUniFibTable Function "
                       "returns failure.\r\n"));

            if (RbrgSetAllFsrbridgeUniFibTableTrigger
                (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                RBRG_TRC ((RBRG_UTIL_TRC,
                           "RbrgSetAllFsrbridgeUniFibTable: "
                           "RbrgSetAllFsrbridgeUniFibTableTrigger "
                           "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbrgFsRbridgeUniFibEntry);
            return OSIX_FAILURE;
        }

        if (RbrgSetAllFsrbridgeUniFibTableTrigger (pRbrgTrgFsRbridgeUniFibEntry,
                                                   pRbrgTrgIsSetFsRbridgeUniFibEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgSetAllFsrbridgeUniFibTableTrigger "
                       "function returns failure.\r\n"));
        }
    }

    if (pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress != OSIX_FALSE)
    {
        MEMCPY (pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                au1FsrbridgeFibMacAddress,
                pRbrgSetFsRbridgeUniFibEntry->MibObject.
                i4FsrbridgeFibMacAddressLen);

        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired != OSIX_FALSE)
    {
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount != OSIX_FALSE)
    {
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount;
    }
    if (pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus != OSIX_FALSE)
    {
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus;
    }
    RBrgUtlCopyFsFibToRBrgFib (pRbrgFsRbridgeUniFibEntry, pRBrgFibEntry);
    /*This condtion is to make the row back to ACTIVE after modifying
     * the values */
    if (i4RowMakeActive == TRUE)
    {
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus = ACTIVE;
    }

    if (RbrgUtilUpdateFsrbridgeUniFibTable (pRbrgOldFsRbridgeUniFibEntry,
                                            pRbrgFsRbridgeUniFibEntry,
                                            pRbrgIsSetFsRbridgeUniFibEntry,
                                            u4FibUpdate) != OSIX_SUCCESS)
    {

        if (RbrgSetAllFsrbridgeUniFibTableTrigger (pRbrgSetFsRbridgeUniFibEntry,
                                                   pRbrgIsSetFsRbridgeUniFibEntry,
                                                   SNMP_FAILURE) !=
            OSIX_SUCCESS)

        {
            RBRG_TRC ((RBRG_UTIL_TRC,
                       "RbrgSetAllFsrbridgeUniFibTable: "
                       "RbrgSetAllFsrbridgeUniFibTableTrigger function returns"
                       " failure.\r\n"));

        }
        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFibTable: "
                   "RbrgUtilUpdateFsrbridgeUniFibTable function returns "
                   "failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pRbrgFsRbridgeUniFibEntry, pRbrgOldFsRbridgeUniFibEntry,
                sizeof (tRbrgFsRbridgeUniFibEntry));
        RBrgUtlCopyFsFibToRBrgFib (pRbrgFsRbridgeUniFibEntry, pRBrgFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return OSIX_FAILURE;

    }
    if (RbrgSetAllFsrbridgeUniFibTableTrigger (pRbrgSetFsRbridgeUniFibEntry,
                                               pRbrgIsSetFsRbridgeUniFibEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        RBRG_TRC ((RBRG_UTIL_TRC,
                   "RbrgSetAllFsrbridgeUniFibTable: "
                   "RbrgSetAllFsrbridgeUniFibTableTrigger function returns "
                   "failure.\r\n"));
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgOldFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgTrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgTrgIsSetFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  RbrgUtlGetFirstFdbEntry
 Input       :  pRbrgFsFdbEntry
 Description :  This routine Gets the first entry from FDB table 
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetFirstFdbTable (tRbrgFsRbridgeUniFdbEntry * pRbrgFsFdbEntry)
{
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    UINT1               u1Index = 0;

    pRbrgFdbEntry = (tRBrgFdbEntry *)
        RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable);
    if (pRbrgFdbEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pRbrgFsFdbEntry->MibObject.u4FsrbridgeContextId =
        pRbrgFdbEntry->u4ContextId;
    pRbrgFsFdbEntry->MibObject.u4FsrbridgeFdbId = pRbrgFdbEntry->u4RBrgFdbId;
    MEMCPY (pRbrgFsFdbEntry->MibObject.FsrbridgeUniFdbAddr,
            pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
    if ((pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_DYNAMIC) ||
        (pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_BOTH))
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    pRbrgFsFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;
    pRbrgFsFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;
    pRbrgFsFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
        pRbrgFdbEntry->i1FdbStatus;
    pRbrgFsFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlGetNextFdbEntry
 Input       :  pCurrentFsFdbEntry
                pNextFsFdbEntrypNextFsFdbEntry
 Description :  This routine Gets the next entry from FDB table 
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetNextFdbEntry (tRbrgFsRbridgeUniFdbEntry * pCurrentFsFdbEntry,
                        tRbrgFsRbridgeUniFdbEntry * pNextFsFdbEntry)
{
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRBrgFdbEntry       RbrgFdbEntry;
    UINT1               u1Index = 0;

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    RbrgFdbEntry.u4ContextId =
        pCurrentFsFdbEntry->MibObject.u4FsrbridgeContextId;
    RbrgFdbEntry.u4RBrgFdbId = pCurrentFsFdbEntry->MibObject.u4FsrbridgeFdbId;
    MEMCPY (&RbrgFdbEntry.RBrgFdbMac,
            &pCurrentFsFdbEntry->MibObject.FsrbridgeUniFdbAddr, MAC_ADDR_LEN);
    pRbrgFdbEntry = (tRBrgFdbEntry *)
        RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, &RbrgFdbEntry,
                       RbrgUtlFdbTblCmpFn);
    if (pRbrgFdbEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pNextFsFdbEntry->MibObject.u4FsrbridgeContextId =
        pRbrgFdbEntry->u4ContextId;
    pNextFsFdbEntry->MibObject.u4FsrbridgeFdbId = pRbrgFdbEntry->u4RBrgFdbId;
    MEMCPY (pNextFsFdbEntry->MibObject.FsrbridgeUniFdbAddr,
            pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
    if ((pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_DYNAMIC) ||
        (pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_BOTH))
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    pNextFsFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;
    pNextFsFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;
    pNextFsFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
        pRbrgFdbEntry->i1FdbStatus;
    pNextFsFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlGetFdbEntry
 Input       :  pFsFdbEntry
                pGetFsFdbEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetFdbEntry (tRbrgFsRbridgeUniFdbEntry * pFsFdbEntry,
                    tRbrgFsRbridgeUniFdbEntry * pGetFsFdbEntry)
{
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRBrgFdbEntry       RbrgFdbEntry;
    UINT1               u1Index = 0;

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    RbrgFdbEntry.u4ContextId = pFsFdbEntry->MibObject.u4FsrbridgeContextId;
    RbrgFdbEntry.u4RBrgFdbId = pFsFdbEntry->MibObject.u4FsrbridgeFdbId;
    MEMCPY (&RbrgFdbEntry.RBrgFdbMac,
            &pFsFdbEntry->MibObject.FsrbridgeUniFdbAddr, MAC_ADDR_LEN);
    pRbrgFdbEntry = (tRBrgFdbEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, &RbrgFdbEntry);
    if (pRbrgFdbEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pGetFsFdbEntry->MibObject.u4FsrbridgeContextId = pRbrgFdbEntry->u4ContextId;
    pGetFsFdbEntry->MibObject.u4FsrbridgeFdbId = pRbrgFdbEntry->u4RBrgFdbId;
    MEMCPY (pGetFsFdbEntry->MibObject.FsrbridgeUniFdbAddr,
            pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
    if ((pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_DYNAMIC) ||
        (pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_BOTH))
    {
        u1Index = 1;
        pGetFsFdbEntry->MibObject.i4FsrbridgeUniFdbStatus = RBRG_FDB_LEARNED;
    }
    else
    {
        u1Index = 0;
        pGetFsFdbEntry->MibObject.i4FsrbridgeUniFdbStatus = RBRG_FDB_MGMT;
    }
    pGetFsFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort;
    pGetFsFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbEgressNickname;
    pGetFsFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4FdbConfidence;
    pGetFsFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
        pRbrgFdbEntry->pRBrgFdbData[u1Index]->u1FdbRowStatus;
    MEMSET (pFsFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMCPY (pFsFdbEntry, pGetFsFdbEntry, sizeof (tRbrgFsRbridgeUniFdbEntry));
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlGetFirstFibTable
 Input       :  pFsFibEntry
 Description :  This routine Gets the first entry in FibTable
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetFirstFibTable (tRbrgFsRbridgeUniFibEntry * pFsFibEntry)
{
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    UINT1               u1Index = 0;

    pRbrgFibEntry = (tRBrgFibEntry *)
        RBTreeGetFirst (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable);
    if (pRbrgFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pFsFibEntry->MibObject.u4FsrbridgeContextId = pRbrgFibEntry->u4ContextId;
    pFsFibEntry->MibObject.i4FsrbridgeFibNickname =
        pRbrgFibEntry->u4FibEgressRBrgNickname;
    pFsFibEntry->MibObject.i4FsrbridgeFibPort =
        pRbrgFibEntry->u4FibNextHopRBrgPortId;
    pFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        pRbrgFibEntry->u4FibNextHopRBrgNickname;
    if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
    {
        u1Index = 1;
        pFsFibEntry->MibObject.i4FsrbridgeUniFibStatus = RBRG_FIB_DYNAMIC;
    }
    else
    {
        u1Index = 0;
        pFsFibEntry->MibObject.i4FsrbridgeUniFibStatus = RBRG_FIB_STATIC;
    }
    pFsFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;
    pFsFibEntry->MibObject.u4FsrbridgeFibHopCount =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;
    MEMCPY (&pFsFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            &pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
            MAC_ADDR_LEN);
    pFsFibEntry->i4L3EgressId = pRbrgFibEntry->i4L3EgressId;

    pFsFibEntry->u4TrunkTrillPort = pRbrgFibEntry->u4TrunkTrillPort;
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  RbrgUtlGetNextFibTable
 Input       :  pCurrFsFibEntry
                pNextFsFibEntry
 Description :  This routine Gets the next entry in FibTable
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetNextFibTable (tRbrgFsRbridgeUniFibEntry * pCurrFsFibEntry,
                        tRbrgFsRbridgeUniFibEntry * pNextFsFibEntry)
{
    tRBrgFibEntry       RbrgFibEntry;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    UINT1               u1Index = 0;

    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    RbrgFibEntry.u4ContextId = pCurrFsFibEntry->MibObject.u4FsrbridgeContextId;
    RbrgFibEntry.u4FibEgressRBrgNickname =
        pCurrFsFibEntry->MibObject.i4FsrbridgeFibNickname;
    RbrgFibEntry.u4FibNextHopRBrgPortId =
        pCurrFsFibEntry->MibObject.i4FsrbridgeFibPort;
    RbrgFibEntry.u4FibNextHopRBrgNickname =
        pCurrFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;
    pRbrgFibEntry = (tRBrgFibEntry *)
        RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                       (tRBElem *) & RbrgFibEntry, RbrgUtlFibTblCmpFn);
    if (pRbrgFibEntry == NULL)
    {
        pNextFsFibEntry = NULL;
        return OSIX_FAILURE;
    }
    pNextFsFibEntry->MibObject.u4FsrbridgeContextId =
        pRbrgFibEntry->u4ContextId;
    pNextFsFibEntry->MibObject.i4FsrbridgeFibNickname =
        pRbrgFibEntry->u4FibEgressRBrgNickname;
    pNextFsFibEntry->MibObject.i4FsrbridgeFibPort =
        pRbrgFibEntry->u4FibNextHopRBrgPortId;
    pNextFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        pRbrgFibEntry->u4FibNextHopRBrgNickname;

    if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    pNextFsFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;
    pNextFsFibEntry->MibObject.u4FsrbridgeFibHopCount =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;
    MEMCPY (&pNextFsFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            &pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
            MAC_ADDR_LEN);
    pNextFsFibEntry->i4L3EgressId = pRbrgFibEntry->i4L3EgressId;

    pNextFsFibEntry->u4TrunkTrillPort = pRbrgFibEntry->u4TrunkTrillPort;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlGetFibEntry
 Input       :  pFsFibEntry
 Description :  This routine Gets the value of the required
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
RbrgUtlGetFibEntry (tRbrgFsRbridgeUniFibEntry * pFsFibEntry,
                    tRbrgFsRbridgeUniFibEntry * pGetFsFibEntry)
{
    tRBrgFibEntry       RbrgFibEntry;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    UINT1               u1Index = 0;

    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    RbrgFibEntry.u4ContextId = pFsFibEntry->MibObject.u4FsrbridgeContextId;
    RbrgFibEntry.u4FibEgressRBrgNickname =
        pFsFibEntry->MibObject.i4FsrbridgeFibNickname;
    RbrgFibEntry.u4FibNextHopRBrgPortId =
        pFsFibEntry->MibObject.i4FsrbridgeFibPort;
    RbrgFibEntry.u4FibNextHopRBrgNickname =
        pFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;

    pRbrgFibEntry = (tRBrgFibEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                   (tRBElem *) & RbrgFibEntry);
    if (pRbrgFibEntry == NULL)
    {
        pGetFsFibEntry = NULL;
        return OSIX_FAILURE;
    }
    pGetFsFibEntry->MibObject.u4FsrbridgeContextId = pRbrgFibEntry->u4ContextId;
    pGetFsFibEntry->MibObject.i4FsrbridgeFibNickname =
        pRbrgFibEntry->u4FibEgressRBrgNickname;
    pGetFsFibEntry->MibObject.i4FsrbridgeFibPort =
        pRbrgFibEntry->u4FibNextHopRBrgPortId;
    pGetFsFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        pRbrgFibEntry->u4FibNextHopRBrgNickname;

    if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }
    pGetFsFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;
    pGetFsFibEntry->MibObject.u4FsrbridgeFibHopCount =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;
    MEMCPY (pGetFsFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac,
            MAC_ADDR_LEN);
    pGetFsFibEntry->MibObject.i4FsrbridgeFibMacAddressLen = MAC_ADDR_LEN;
    pGetFsFibEntry->i4L3EgressId = pRbrgFibEntry->i4L3EgressId;

    pGetFsFibEntry->u4TrunkTrillPort = pRbrgFibEntry->u4TrunkTrillPort;
    pGetFsFibEntry->MibObject.i4FsrbridgeFibRowstatus =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u1FibRowStatus;
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : RbrgUtlFdbTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for 
 *                  FDB table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/

INT4
RbrgUtlFdbTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tRBrgFdbEntry      *pRbrgFdbEntry1 = NULL;
    tRBrgFdbEntry      *pRbrgFdbEntry2 = NULL;

    pRbrgFdbEntry1 = (tRBrgFdbEntry *) pRBElem1;
    pRbrgFdbEntry2 = (tRBrgFdbEntry *) pRBElem2;

    if (pRbrgFdbEntry1->u4ContextId > pRbrgFdbEntry2->u4ContextId)
    {
        return RBRG_GREATER;
    }
    else if (pRbrgFdbEntry1->u4ContextId > pRbrgFdbEntry2->u4ContextId)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFdbEntry1->u4RBrgFdbId < pRbrgFdbEntry2->u4RBrgFdbId)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFdbEntry1->u4RBrgFdbId > pRbrgFdbEntry2->u4RBrgFdbId)
    {
        return RBRG_GREATER;
    }

    else if (MEMCMP
             (pRbrgFdbEntry1->RBrgFdbMac, pRbrgFdbEntry2->RBrgFdbMac,
              MAC_ADDR_LEN) > 0)
    {
        return RBRG_GREATER;
    }
    else if (MEMCMP
             (pRbrgFdbEntry1->RBrgFdbMac, pRbrgFdbEntry2->RBrgFdbMac,
              MAC_ADDR_LEN) < 0)
    {
        return (RBRG_LESSER);
    }

    return (RBRG_EQUAL);
}

/***************************************************************************
 *  FUNCTION NAME : RbrgUtlFibTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for 
 *                  FDB table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/

INT4
RbrgUtlFibTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tRBrgFibEntry      *pRbrgFibEntry1 = NULL;
    tRBrgFibEntry      *pRbrgFibEntry2 = NULL;

    pRbrgFibEntry1 = (tRBrgFibEntry *) pRBElem1;
    pRbrgFibEntry2 = (tRBrgFibEntry *) pRBElem2;

    if (pRbrgFibEntry1->u4ContextId < pRbrgFibEntry2->u4ContextId)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFibEntry1->u4ContextId > pRbrgFibEntry2->u4ContextId)
    {
        return (RBRG_GREATER);
    }
    else if (pRbrgFibEntry1->u4FibEgressRBrgNickname <
             pRbrgFibEntry2->u4FibEgressRBrgNickname)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFibEntry1->u4FibEgressRBrgNickname >
             pRbrgFibEntry2->u4FibEgressRBrgNickname)
    {
        return RBRG_GREATER;
    }
    else if (pRbrgFibEntry1->u4FibNextHopRBrgPortId <
             pRbrgFibEntry2->u4FibNextHopRBrgPortId)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFibEntry1->u4FibNextHopRBrgPortId >
             pRbrgFibEntry2->u4FibNextHopRBrgPortId)
    {
        return RBRG_GREATER;
    }
    else if (pRbrgFibEntry1->u4FibNextHopRBrgNickname <
             pRbrgFibEntry2->u4FibNextHopRBrgNickname)
    {
        return (RBRG_LESSER);
    }
    else if (pRbrgFibEntry1->u4FibNextHopRBrgNickname >
             pRbrgFibEntry2->u4FibNextHopRBrgNickname)
    {
        return RBRG_GREATER;
    }

    return (RBRG_EQUAL);
}

/****************************************************************************
 Function    :  RbrgUtlCreateContext
 Input       :  u4ContextId
 Description :  This routine is set create an global entry when create 
                context  indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlCreateContext (UINT4 u4ContextId)
{
    tRbrgFsrbridgeGlobalEntry *pFsGlobalEntry = NULL;
    tRbrgFsrbridgeGlobalEntry FsGlobalEntry;

    MEMSET (&FsGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));

    if (u4ContextId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlCreateContext: Invalid context Id \r\n"));
    }

    FsGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pFsGlobalEntry = (tRbrgFsrbridgeGlobalEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable, &FsGlobalEntry);
    if (pFsGlobalEntry != NULL)
    {
        /* Context already exists */
        RBRG_TRC ((MGMT_TRC,
                   "RbrgUtlCreateContext: Context already exists\r\n"));
        return OSIX_SUCCESS;
    }
    pFsGlobalEntry = ((tRbrgFsrbridgeGlobalEntry *)
                      MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID));
    if (pFsGlobalEntry == NULL)
    {
        RBRG_TRC (((MGMT_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlCreateContext: Memory allocation Failed "
                   "for context %d\r\n", u4ContextId));
        return OSIX_FAILURE;
    }
    /* Fill default values */
    pFsGlobalEntry->MibObject.u4FsrbridgeContextId = u4ContextId;
    pFsGlobalEntry->MibObject.u4FsrbridgeNicknameNumber = 1;
    pFsGlobalEntry->MibObject.u4FsrbridgeNumPorts = 0;
    pFsGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable = FALSE;
    pFsGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable = FALSE;
    pFsGlobalEntry->MibObject.i4FsrbridgeSystemControl = RBRG_START;
    pFsGlobalEntry->MibObject.i4FsrbridgeModuleStatus = RBRG_DISABLE;
    pFsGlobalEntry->MibObject.i4FsrbridgeClearCounters = FALSE;
    pFsGlobalEntry->MibObject.u4FsrbridgeTrillVersion = RBRG_TRILL_VERSION;
    /* Add to RBTree */
    if (RBTreeAdd
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
         (tRBElem *) pFsGlobalEntry) != RB_SUCCESS)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlCreateContext: RBTreeAdd failed for context %d\r\n",
                   u4ContextId));
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pFsGlobalEntry);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlDeleteContext
 Input       :  u4ContextId
 Description :  This routine is set delete an global entry when delete 
                context  indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlDeleteContext (UINT4 u4ContextId)
{
    tRbrgFsrbridgeGlobalEntry *pFsGlobalEntry = NULL;
    tRbrgFsrbridgeGlobalEntry FsGlobalEntry;

    MEMSET (&FsGlobalEntry, 0, sizeof (tRbrgMibFsrbridgeGlobalEntry));
    FsGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;

    if (u4ContextId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlDeleteContext: Invalid context Id \r\n"));
        return OSIX_FAILURE;
    }

    pFsGlobalEntry = (tRbrgFsrbridgeGlobalEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable, &FsGlobalEntry);
    if (pFsGlobalEntry == NULL)
    {
        /* Context does not exist */
        RBRG_TRC ((MGMT_TRC,
                   "RbrgUtlDeleteContext: Context does not exist\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeRem (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable, pFsGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pFsGlobalEntry);
    pFsGlobalEntry = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlCreatePort
 Input       :  u4ContextId
 Description :  This routine is set create an port entry when create 
                port indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlCreatePort (UINT4 u4IfIndex, UINT1 u1Type)
{
    tRBrgNpEntry        RBrgHwInfo;
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;
    tRbrgFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRbrgFsrbridgeNicknameEntry *pRbrgNicknameEntry = NULL;
    tRbrgFsrbridgeNicknameEntry RbrgNicknameEntry;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgMibFsRBridgeBasePortEntry));
    MEMSET (&RBrgHwInfo, 0, sizeof (tRBrgNpEntry));
    MEMSET (&RbrgNicknameEntry, 0, sizeof (tRbrgFsrbridgeNicknameEntry));

    if (u4IfIndex >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgCreatePort: Invalid port index\r\n"));
        return OSIX_FAILURE;
    }

    if (RbrgPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        RBRG_TRC ((MGMT_TRC,
                   "RbrgUtlCreatePort: Port %d does not exist \r\n",
                   u4IfIndex));
        return OSIX_FAILURE;
    }
    RbrgGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pRbrgGlobalEntry = (tRbrgFsrbridgeGlobalEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) & RbrgGlobalEntry);
    /* For Klocwork */
    if (pRbrgGlobalEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = u4IfIndex;
    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, &FsPortEntry);

    if ((u1Type == RBRG_CREATE_ENTRY) && (pFsPortEntry == NULL))
    {

        pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
        if (pFsPortEntry == NULL)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgCreatePort: Port %d  creation failed\r\n",
                       u4IfIndex));
            return OSIX_FAILURE;
        }
        MEMSET (pFsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
        /* Assign Default values */
        pFsPortEntry->MibObject.i4FsrbridgePortIfIndex = u4IfIndex;

        pFsPortEntry->MibObject.i4FsrbridgePortDisable = FALSE;
        pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = FALSE;
        pFsPortEntry->MibObject.i4FsrbridgePortAccessPort = FALSE;
        pFsPortEntry->MibObject.i4FsrbridgePortDisableLearning = FALSE;

        pFsPortEntry->MibObject.i4FsrbridgePortState = RBRG_PORT_UNINHIBITED;
        pFsPortEntry->MibObject.i4FsrbridgePortDesigVlan = 1;

        /* Add to RBTree */
        if (RBTreeAdd
            (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
             (tRBElem *) pFsPortEntry) != RB_SUCCESS)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgUtlCreatePort: RBTreeAdd failed for port %d\r\n",
                       u4IfIndex));
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                (UINT1 *) pFsPortEntry);
            return OSIX_FAILURE;
        }
        pRbrgGlobalEntry->MibObject.u4FsrbridgeNumPorts++;
    }
    else
    {
        /* For Klocwork */
        if (pFsPortEntry == NULL)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgUtlCreatePort : Port %d do not exist\r\n",
                       u4IfIndex));
            return OSIX_FAILURE;
        }
    }

    if (pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus == RBRG_ENABLE)
    {

        if (pFsPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
        {

            RBrgHwInfo.u4Command = RBRG_ENABLE_ACCESS_PORT;
            RBrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
        }
        else
        {
            RBrgHwInfo.u4Command = RBRG_ENABLE_TRUNK_PORT;
            RBrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
        }

        if (RbrgHwWrProgramEntry (&RBrgHwInfo) == OSIX_FAILURE)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgUtlCreatePort : Setting Port properties "
                       "for port %d in Hw failed\r\n", u4IfIndex));
            return OSIX_FAILURE;
        }
        if (pFsPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
        {
            /* Create an access trill port */
            RbrgNicknameEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
            RbrgNicknameEntry.MibObject.i4FsrbridgeNicknameName = 0;
            pRbrgNicknameEntry = (tRbrgFsrbridgeNicknameEntry *)
                RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                               (tRBElem *) & RbrgNicknameEntry,
                               FsrbridgeNicknameTableRBCmp);
            if ((pRbrgNicknameEntry == NULL) ||
                ((pRbrgNicknameEntry != NULL) &&
                 (pRbrgNicknameEntry->MibObject.u4FsrbridgeContextId !=
                  u4ContextId)))
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "Create an nickname for this rbridge before "
                           "creating an access port\r\n"));
                return OSIX_FAILURE;
            }
            RBrgHwInfo.u4Command = RBRG_FIB_CREATE;
            RBrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
                pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU = RBRG_DEFAULT_MTU;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
                RBRG_DEFAULT_HOPCOUNT;
            RBrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
                pRbrgNicknameEntry->MibObject.i4FsrbridgeNicknameName;
            RBrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_ACCESS_PORT;
            if (RbrgHwWrProgramEntry (&RBrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "Access Trill port creation failed\r\n"));
                return OSIX_FAILURE;
            }
            pFsPortEntry->u4AccessTrillPort =
                (UINT4) RBrgHwInfo.RbrgNpRbrgEntry.i4AccessTrillPort;
            pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort = FALSE;

        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlDeletePort
 Input       :  u4IfIndex
 Description :  This routine is set delete an port entry when delete 
                port indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlDeletePort (UINT4 u4IfIndex, UINT1 u1Type)
{
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgFsrbridgeGlobalEntry RbrgGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pRbrgGlobalEntry = NULL;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;
    tRBrgNpEntry        RbrgHwInfo;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

    if (u4IfIndex >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlDeletePort: Invalid Interface Index \r\n"));
        return OSIX_FAILURE;
    }

    if (RbrgPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        RBRG_TRC ((MGMT_TRC,
                   "RbrgUtlDeletePort: Port %d does not exist \r\n",
                   u4IfIndex));
        return OSIX_FAILURE;
    }
    RbrgGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pRbrgGlobalEntry = (tRbrgFsrbridgeGlobalEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
                   (tRBElem *) & RbrgGlobalEntry);
    /* For Klocwork */
    if (pRbrgGlobalEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = (INT4) u4IfIndex;
    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, (tRBElem *) & FsPortEntry);
    if (pFsPortEntry == NULL)
    {
        /* Context does not exist */
        RBRG_TRC ((MGMT_TRC,
                   "RbrgUtlDeletePort: rbridge Port %d does not exist\r\n",
                   u4IfIndex));
        return OSIX_FAILURE;
    }
    if ((pFsPortEntry->u4ReferenceCount != 0) && (u1Type == RBRG_DELETE_ENTRY))
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "Delete all static FIB entries and FDB entries"
                   " on this port before deleting port %d\r\n",
                   pFsPortEntry->MibObject.i4FsrbridgePortIfIndex));
        return OSIX_FAILURE;
    }

    /* Delete port properties from Hardware */
    MEMSET (&RbrgHwInfo, 0, sizeof (RbrgHwInfo));
    if (pFsPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
    {
        RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
            pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
        RbrgHwInfo.u4Command = RBRG_FIB_DELETE;
        RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_ACCESS_PORT;
        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
            pFsPortEntry->u4AccessTrillPort;
        if (pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus == RBRG_ENABLE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                           "Delete of access port in HW failed\r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    MEMSET (&RbrgHwInfo, 0, sizeof (RbrgHwInfo));
    if (pFsPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE)
    {
        RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
            pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
        RbrgHwInfo.u4Command = RBRG_DISABLE_TRUNK_PORT;
    }
    if (pFsPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
    {
        RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
            pFsPortEntry->MibObject.i4FsrbridgePortIfIndex;
        RbrgHwInfo.u4Command = RBRG_DISABLE_ACCESS_PORT;
    }
    if (pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus == RBRG_ENABLE)
    {
        if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    if (u1Type == RBRG_DELETE_ENTRY)
    {
        /*Delete entry from control plane */
        RBTreeRem (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, pFsPortEntry);
        pRbrgGlobalEntry->MibObject.u4FsrbridgeNumPorts--;
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pFsPortEntry);

        pFsPortEntry = NULL;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlAddPortToLAG
 Input       :  u4IfIndex
 Description :  This routine is used to add a port to a port channel when 
                an indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlAddPortToLAG (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4PortChId)
{
    tRbrgMibFsRBridgeBasePortEntry FsPortEntry;
    tRbrgMibFsRBridgeBasePortEntry *pFsPortEntry = NULL;

    UNUSED_PARAM (u4PortChId);
    UNUSED_PARAM (u4ContextId);

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgMibFsRBridgeBasePortEntry));

    if (u4IfIndex >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlAddPortToLAG: Invalid Interface Index \r\n"));
        return OSIX_FAILURE;
    }

    FsPortEntry.i4FsrbridgePortIfIndex = u4IfIndex;
    pFsPortEntry = (tRbrgMibFsRBridgeBasePortEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable, &FsPortEntry);
    if (pFsPortEntry == NULL)
    {
        /* Context does not exist */
        RBRG_TRC ((MGMT_TRC, "RbrgUtlAddPortToLAG: Port does not exist\r\n"));
        return OSIX_FAILURE;
    }

    /* To add a port to a portchannel, 
     * 1. Delete the port
     * 2. Setting the port channel properties on hardware. - This will be
     *    done while setting port properties on the portchannel. */
    if (RbrgUtlDeletePort (u4IfIndex, RBRG_DELETE_ENTRY) == OSIX_FAILURE)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlAddPortToLAG: port deletion failed\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUtlRemPortFromLAG
 Input       :  u4IfIndex
 Description :  This routine is used to delete a port from a port channel when 
                an indication is received from L2IWF                
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
RbrgUtlRemPortFromLAG (UINT4 u4IfIndex, UINT4 u4PortChId)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    /* When a port is removed from LAG, LA sends delete indication for 
     * port channel and create indication for physical port.So,
     * RbrgUtlCreatePort and RbrgUtlDeletePort handles the scenario.
     * In MI case, If the port is not mapped to any context, port creation is 
     * not required */

    /* Getting Context for given Port */
    /* If port is not mapped to context,  do not create port entry */

    if (RbrgPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (u4PortChId);
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgUtlCreateAllPorts
*
*    DESCRIPTION      : This function creates all valid ports in this
*                       context.
*
*    INPUT            : u4ContextId - Component ID
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
RbrgUtlCreateAllPorts (UINT4 u4ContextId)
{
    tPortList          *pIfPortList = NULL;
    UINT4               u4Port = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pIfPortList == NULL)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlCreateAllPorts: Error in Allocating memory for bitlist\r\n"));
        return;
    }
    MEMSET (*pIfPortList, 0, sizeof (tPortList));

    /* RbrgPortGetNextValidPort retrun success only for SI case */
    if (RbrgPortVcmGetContextPortList (u4ContextId, *pIfPortList) ==
        OSIX_SUCCESS)
    {
        for (u2PortIndex = 0; u2PortIndex < RBRG_IFPORT_LIST_SIZE;
             u2PortIndex++)
        {
            u1PortFlag = (*pIfPortList)[u2PortIndex];
            for (u2BitIndex = 0; ((u2BitIndex < RBRG_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & RBRG_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2PortIndex * RBRG_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);

                    /* Invoke interface creation only if the physical 
                     * port is not part of port channel. */

                    if (RBRG_IS_PORT_CHANNEL (u4Port) != OSIX_TRUE)
                    {
                        if (RbrgPortIsPortInPortChannel (u4Port) ==
                            OSIX_SUCCESS)
                        {
                            /* If the port is present in the port channel
                             * then move to the next port */
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                            continue;
                        }
                    }
                    if (RbrgUtlCreatePort (u4Port, RBRG_CREATE_ENTRY) ==
                        OSIX_FAILURE)
                    {
                        RBRG_TRC ((ALL_FAILURE_TRC,
                                   "RbrgUtlCreateAllPorts: Port %lu"
                                   "Creation Faild in RBRG\r\n", u4Port));
                    }
                    else
                    {
                        RBRG_TRC ((MGMT_TRC,
                                   "RbrgUtlCreateAllPorts: Port %lu "
                                   "Creation SUCCESS in RBRG\r\n", u4Port));
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* FOR LOOP 2 END */

        }                        /* FOR LOOP 1 END */
    }
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
    return;
}

/****************************************************************************
 Function    :  RbrgUniFdbTableCreate
 Input       :  None
 Description :  This function creates the RBrgUniFdbTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgUniFdbTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tRBrgFdbEntry, RBrgFdbRBNode);

    if ((gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, RbrgUtlFdbTblCmpFn)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  RbrgUniFibTableCreate
 Input       :  None
 Description :  This function creates the RBrgUniFibTable

                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
RbrgUniFibTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tRBrgFibEntry, RBrgFibRBNode);

    if ((gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, RbrgUtlFibTblCmpFn)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelFibEntriesOnContext
 *
 *    DESCRIPTION      : This function deletes the Fib entries on this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelFibEntriesOnContext (UINT4 u4ContextId, UINT1 u1Type)
{

    tRBrgNpEntry        RbrgHwInfo;
    tRbrgFsRBridgeBasePortEntry PortEntry;
    tRbrgFsRBridgeBasePortEntry *pPortEntry = NULL;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRBrgFibEntry      *pNextRbrgFibEntry = NULL;
    UINT1               u1Status = 0;

    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
    MEMSET (&PortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    pNextRbrgFibEntry = (tRBrgFibEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable);
    while ((pNextRbrgFibEntry != NULL) &&
           (pNextRbrgFibEntry->u4ContextId == u4ContextId))
    {

        pRbrgFibEntry = pNextRbrgFibEntry;
        pNextRbrgFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) pNextRbrgFibEntry, NULL);

        PortEntry.MibObject.i4FsrbridgePortIfIndex =
            pRbrgFibEntry->u4FibNextHopRBrgPortId;
        pPortEntry = (tRbrgFsRBridgeBasePortEntry *)
            RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                       (tRBElem *) & PortEntry);
        /* For Klocwork */
        if (pPortEntry == NULL)
        {
            return OSIX_FAILURE;
        }
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

        if (pPortEntry->MibObject.i4FsrbridgePortTrunkPort == TRUE)
        {
            RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
                pRbrgFibEntry->u4TrunkTrillPort;
        }

        RbrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id =
            pPortEntry->i4L3InterfaceIndex;

        RbrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId = pRbrgFibEntry->i4L3EgressId;
        RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
        RbrgHwInfo.u4Command = RBRG_FIB_DELETE;

        RBRG_IS_MODULE_ENABLE (pRbrgFibEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgUtlDelFibEntriesOnContext: "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
        }

        pRbrgFibEntry->i4L3EgressId = 0;
        pRbrgFibEntry->u4TrunkTrillPort = 0;

        if (u1Type == RBRG_DELETE_ENTRY)
        {
            RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable, pRbrgFibEntry);

            /* Decrement FIB Count */
            pPortEntry->u4ReferenceCount--;

            if (pRbrgFibEntry->pRBrgFibData[RBRG_STATIC] != NULL)
            {
                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRbrgFibEntry->
                                    pRBrgFibData[RBRG_STATIC]);
            }
            if (pRbrgFibEntry->pRBrgFibData[RBRG_DYNAMIC] != NULL)
            {
                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRbrgFibEntry->
                                    pRBrgFibData[RBRG_DYNAMIC]);
            }
            MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID, (UINT1 *) pRbrgFibEntry);

            pRbrgFibEntry = NULL;
        }

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelFdbEntriesOnContext
 *
 *    DESCRIPTION      : This function deletes the Fdb entries on this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelFdbEntriesOnContext (UINT4 u4ContextId, UINT1 u1Type)
{
    tRBrgFdbEntry       RbrgFdbEntry;
    tRBrgNpEntry        RbrgHwInfo;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    UINT1               u1Status = 0;

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

    pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable);

    while ((pRbrgFdbEntry != NULL) &&
           (pRbrgFdbEntry->u4ContextId == u4ContextId))
    {

        RbrgHwInfo.u4Command = RBRG_UCAST_DSET_DELETE;
        MEMCPY (RbrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
        RbrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan = pRbrgFdbEntry->u4RBrgFdbId;
        RBRG_IS_MODULE_ENABLE (pRbrgFdbEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgUtlDelFdbEntriesOnContext: "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
        }
        RbrgFdbEntry.u4ContextId = pRbrgFdbEntry->u4ContextId;
        RbrgFdbEntry.u4RBrgFdbId = pRbrgFdbEntry->u4RBrgFdbId;
        MEMCPY (RbrgFdbEntry.RBrgFdbMac, pRbrgFdbEntry->RBrgFdbMac,
                MAC_ADDR_LEN);

        if (u1Type == RBRG_DELETE_ENTRY)
        {
            RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                       (tRBElem *) pRbrgFdbEntry);
            pRbrgFdbEntry = NULL;
        }
        pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetNext
            (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
             (tRBElem *) & RbrgFdbEntry, RbrgUtlFdbTblCmpFn);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelPortsOnContext
 *
 *    DESCRIPTION      : This function deletes the port entries on this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelPortsOnContext (UINT4 u4ContextId, UINT1 u1Type)
{
    tPortList          *pIfPortList = NULL;
    UINT4               u4Port = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pIfPortList == NULL)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlDelPortsOnContext: Error in Allocating memory for bitlist\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (*pIfPortList, 0, sizeof (tPortList));
    /* RbrgPortGetNextValidPort retrun success only for SI case */
    if (RbrgPortVcmGetContextPortList (u4ContextId, *pIfPortList) ==
        OSIX_SUCCESS)
    {
        for (u2PortIndex = 0; u2PortIndex < RBRG_IFPORT_LIST_SIZE;
             u2PortIndex++)
        {
            u1PortFlag = (*pIfPortList)[u2PortIndex];
            for (u2BitIndex = 0; ((u2BitIndex < RBRG_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & RBRG_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2PortIndex * RBRG_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);

                    /* Invoke interface deletion only if the physical 
                     * port is not part of port channel. */

                    if (RBRG_IS_PORT_CHANNEL (u4Port) != OSIX_TRUE)
                    {
                        if (RbrgPortIsPortInPortChannel (u4Port) ==
                            OSIX_SUCCESS)
                        {
                            /* If the port is present in the port channel
                             * then move to the next port */
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                            continue;
                        }
                    }
                    if (RbrgUtlDeletePort (u4Port, u1Type) == OSIX_FAILURE)
                    {
                        RBRG_TRC ((ALL_FAILURE_TRC,
                                   "RbrgUtlDelPortsOnContext: Port %lu"
                                   "Deletion Faild in RBRG\r\n", u4Port));
                        FsUtilReleaseBitList ((UINT1 *) pIfPortList);
                        return OSIX_FAILURE;
                    }
                    else
                    {
                        RBRG_TRC ((MGMT_TRC,
                                   "RbrgUtlDelPortsOnContext: Port %lu "
                                   "Deletion SUCCESS in RBRG\r\n", u4Port));
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* FOR LOOP 2 END */

        }                        /* FOR LOOP 1 END */
    }
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlEnableFibEntriesOnContext
 *
 *    DESCRIPTION      : This function creates the Fib entries of this context
 *                       on hardware
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlEnableFibEntriesOnContext (UINT4 u4ContextId)
{
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRBrgFibEntry       RbrgFibEntry;
    tRbrgFsRbridgeUniFibEntry FsRbrgOldFibEntry;
    tRbrgFsRbridgeUniFibEntry FsRbrgFibEntry;
    tRbrgIsSetFsRbridgeUniFibEntry FsIsSetRbrgFibEntry;
    UINT4               u4FibUpdate = 0;

    MEMSET (&RbrgFibEntry, 0, sizeof (tRBrgFibEntry));
    MEMSET (&FsRbrgOldFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&FsRbrgFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&FsIsSetRbrgFibEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    pRbrgFibEntry = (tRBrgFibEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable);

    while ((pRbrgFibEntry != NULL) &&
           (pRbrgFibEntry->u4ContextId == u4ContextId))
    {
        FsRbrgFibEntry.MibObject.i4FsrbridgeFibRowstatus = ACTIVE;
        RBrgUtlCopyRBrgFibToFsFib (&FsRbrgFibEntry, pRbrgFibEntry);
        /* Update entries on hardware */
        if (RbrgUtilUpdateFsrbridgeUniFibTable (&FsRbrgOldFibEntry,
                                                &FsRbrgFibEntry,
                                                &FsIsSetRbrgFibEntry,
                                                u4FibUpdate) == OSIX_FAILURE)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgUtlEnableFibEntriesOnContext: "
                       "RbrgUtilUpdateFsrbridgeUniFibTable returned "
                       "Failure\r\n"));
            return OSIX_FAILURE;
        }
        RbrgFibEntry.u4ContextId = pRbrgFibEntry->u4ContextId;
        RbrgFibEntry.u4FibEgressRBrgNickname =
            pRbrgFibEntry->u4FibEgressRBrgNickname;
        RbrgFibEntry.u4FibNextHopRBrgPortId =
            pRbrgFibEntry->u4FibNextHopRBrgPortId;
        RbrgFibEntry.u4FibNextHopRBrgNickname =
            pRbrgFibEntry->u4FibNextHopRBrgNickname;

        pRbrgFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) & RbrgFibEntry, RbrgUtlFibTblCmpFn);

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlEnableFdbEntriesOnContext
 *
 *    DESCRIPTION      : This function create the Fdb entries of this context
 *                       on hardware
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlEnableFdbEntriesOnContext (UINT4 u4ContextId)
{
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    tRBrgFdbEntry       RbrgFdbEntry;
    tRbrgFsRbridgeUniFdbEntry FsOldFdbEntry;
    tRbrgFsRbridgeUniFdbEntry FsFdbEntry;
    tRbrgIsSetFsRbridgeUniFdbEntry FsIsSetFdbEntry;

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    MEMSET (&FsOldFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsIsSetFdbEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable);

    while ((pRbrgFdbEntry != NULL) &&
           (pRbrgFdbEntry->u4ContextId == u4ContextId))
    {
        RBrgUtlCopyRBrgFdbToFsFdb (&FsFdbEntry, pRbrgFdbEntry);
        if (RbrgUtilUpdateFsrbridgeUniFdbTable (&FsOldFdbEntry, &FsFdbEntry,
                                                &FsIsSetFdbEntry) ==
            OSIX_FAILURE)
        {
            RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                       "RbrgUtlEnableFdbEntriesOnContext: "
                       "RbrgUtilUpdateFsrbridgeUniFdbTable returned "
                       "Failure\r\n"));
            return OSIX_FAILURE;
        }
        RbrgFdbEntry.u4ContextId = pRbrgFdbEntry->u4ContextId;
        RbrgFdbEntry.u4RBrgFdbId = pRbrgFdbEntry->u4RBrgFdbId;
        MEMCPY (RbrgFdbEntry.RBrgFdbMac, pRbrgFdbEntry->RBrgFdbMac,
                MAC_ADDR_LEN);

        pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetNext
            (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
             (tRBElem *) & RbrgFdbEntry, RbrgUtlFdbTblCmpFn);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlEnablePortsOnContext
 *
 *    DESCRIPTION      : This function deletes the port entries on this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlEnablePortsOnContext (UINT4 u4ContextId)
{
    tPortList          *pIfPortList = NULL;
    tRbrgFsRBridgeBasePortEntry *pFsPortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry FsOldPortEntry;
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgIsSetFsRBridgeBasePortEntry FsIsSetPortEntry;
    UINT4               u4Port = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (&FsOldPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (&FsIsSetPortEntry, 0, sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (pIfPortList == NULL)
    {
        RBRG_TRC (((RBRG_UTIL_TRC | ALL_FAILURE_TRC),
                   "RbrgUtlDelPortsOnContext: Error in Allocating memory for bitlist\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (*pIfPortList, 0, sizeof (tPortList));

    if (RbrgPortVcmGetContextPortList (u4ContextId, *pIfPortList) ==
        OSIX_SUCCESS)
    {
        for (u2PortIndex = 0; u2PortIndex < RBRG_IFPORT_LIST_SIZE;
             u2PortIndex++)
        {
            u1PortFlag = (*pIfPortList)[u2PortIndex];
            for (u2BitIndex = 0; ((u2BitIndex < RBRG_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & RBRG_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2PortIndex * RBRG_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);

                    /* Invoke interface deletion only if the physical 
                     * port is not part of port channel. */

                    if (RBRG_IS_PORT_CHANNEL (u4Port) != OSIX_TRUE)
                    {
                        if (RbrgPortIsPortInPortChannel (u4Port) ==
                            OSIX_SUCCESS)
                        {
                            /* If the port is present in the port channel
                             * then move to the next port */
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                            continue;
                        }
                    }

                    /* Get appropriate port entry */
                    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = u4Port;
                    pFsPortEntry = (tRbrgFsRBridgeBasePortEntry *)
                        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                                   (tRBElem *) & FsPortEntry);
                    if (pFsPortEntry == NULL)
                    {
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        continue;
                    }

                    if (RbrgUtilUpdateFsrbridgePortTable
                        (&FsOldPortEntry, &FsPortEntry, &FsIsSetPortEntry) ==
                        OSIX_FAILURE)
                    {
                        RBRG_TRC ((ALL_FAILURE_TRC,
                                   "RbrgUtlEnablePortsOnContext: Port %lu"
                                   "Hardware addition Faild in RBRG\r\n",
                                   u4Port));
                    }
                    else
                    {
                        RBRG_TRC ((MGMT_TRC,
                                   "RbrgUtlEnablePortsOnContext: Port %lu "
                                   "Creation SUCCESS in RBRG\r\n", u4Port));
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* FOR LOOP 2 END */

        }                        /* FOR LOOP 1 END */
    }
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelFibEntriesOnPort
 *
 *    DESCRIPTION      : This function deletes the Fib entries on this port
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelFibEntriesOnPort (UINT4 u4IfIndex, UINT1 u1Type)
{
    tRBrgNpEntry        RbrgHwInfo;
    tRbrgFsRBridgeBasePortEntry PortEntry;
    tRbrgFsRBridgeBasePortEntry *pPortEntry = NULL;
    tRBrgFibEntry      *pRbrgFibEntry = NULL;
    tRBrgFibEntry      *pNextRbrgFibEntry = NULL;

    UINT1               u1Status = 0;

    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
    MEMSET (&PortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));

    PortEntry.MibObject.i4FsrbridgePortIfIndex = u4IfIndex;

    pPortEntry = (tRbrgFsRBridgeBasePortEntry *)
        RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
                   (tRBElem *) & PortEntry);
    /* For Klocwork */
    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pPortEntry->MibObject.i4FsrbridgePortAccessPort == TRUE)
    {
        return OSIX_SUCCESS;
    }

    pNextRbrgFibEntry = (tRBrgFibEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable);

    while ((pNextRbrgFibEntry != NULL) &&
           (pNextRbrgFibEntry->u4FibNextHopRBrgPortId == u4IfIndex))
    {
        pRbrgFibEntry = pNextRbrgFibEntry;
        pNextRbrgFibEntry = (tRBrgFibEntry *)
            RBTreeGetNext (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable,
                           (tRBElem *) pNextRbrgFibEntry, NULL);
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
            pRbrgFibEntry->u4TrunkTrillPort;

        RbrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id =
            pPortEntry->i4L3InterfaceIndex;

        RbrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId = pRbrgFibEntry->i4L3EgressId;
        RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
        RbrgHwInfo.u4Command = RBRG_FIB_DELETE;

        RBRG_IS_MODULE_ENABLE (pRbrgFibEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgUtlDelFibEntriesOnPort: "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
            pRbrgFibEntry->i4L3EgressId = 0;
            pRbrgFibEntry->u4TrunkTrillPort = 0;

        }

        if (u1Type == RBRG_DELETE_ENTRY)
        {
            RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFibTable, pRbrgFibEntry);

            /* Decrement Port Fib Count */
            pPortEntry->u4ReferenceCount--;

            if (pRbrgFibEntry->pRBrgFibData[RBRG_STATIC] != NULL)
            {
                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRbrgFibEntry->
                                    pRBrgFibData[RBRG_STATIC]);
            }
            if (pRbrgFibEntry->pRBrgFibData[RBRG_DYNAMIC] != NULL)
            {
                MemReleaseMemBlock (RBRG_FIB_DATA_POOLID,
                                    (UINT1 *) pRbrgFibEntry->
                                    pRBrgFibData[RBRG_DYNAMIC]);
            }
            MemReleaseMemBlock (RBRG_FIB_TABLE_POOLID, (UINT1 *) pRbrgFibEntry);

            pRbrgFibEntry = NULL;
        }

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelFdbEntriesOnPort
 *
 *    DESCRIPTION      : This function deletes the Fdb entries on this port
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelFdbEntriesOnPort (UINT4 u4IfIndex, UINT1 u1Type)
{
    tRBrgFdbEntry       RbrgFdbEntry;
    tRBrgNpEntry        RbrgHwInfo;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    UINT1               u1Status = 0;
    UINT1               u1Index = 0;

    MEMSET (&RbrgFdbEntry, 0, sizeof (tRBrgFdbEntry));
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

    pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable);

    while (pRbrgFdbEntry != NULL)
    {
        RbrgFdbEntry.u4ContextId = pRbrgFdbEntry->u4ContextId;
        RbrgFdbEntry.u4RBrgFdbId = pRbrgFdbEntry->u4RBrgFdbId;
        MEMCPY (RbrgFdbEntry.RBrgFdbMac, pRbrgFdbEntry->RBrgFdbMac,
                MAC_ADDR_LEN);
        if (pRbrgFdbEntry->i1FdbStatus == RBRG_FDB_STATIC)
        {
            u1Index = RBRG_STATIC;
        }
        else
        {
            u1Index = RBRG_DYNAMIC;
        }
        if (pRbrgFdbEntry->pRBrgFdbData[u1Index]->u4RBrgFdbPort == u4IfIndex)
        {

            RbrgHwInfo.u4Command = RBRG_UCAST_DSET_DELETE;
            MEMCPY (RbrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                    pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
            RbrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan =
                pRbrgFdbEntry->u4RBrgFdbId;
            RBRG_IS_MODULE_ENABLE (pRbrgFdbEntry->u4ContextId, u1Status);
            if (u1Status == TRUE)
            {
                if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
                {
                    RBRG_TRC ((ALL_FAILURE_TRC,
                               "RbrgUtlDelFdbEntriesOnContext: "
                               "RbrgHwWrProgramEntry returned Failure\r\n"));
                    return OSIX_FAILURE;
                }
            }

            if (u1Type == RBRG_DELETE_ENTRY)
            {
                RBTreeRem (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
                           (tRBElem *) pRbrgFdbEntry);
                MemReleaseMemBlock (RBRG_FDB_TABLE_POOLID,
                                    (UINT1 *) pRbrgFdbEntry);
                pRbrgFdbEntry = NULL;
            }
        }
        pRbrgFdbEntry = (tRBrgFdbEntry *) RBTreeGetNext
            (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable,
             (tRBElem *) & RbrgFdbEntry, RbrgUtlFdbTblCmpFn);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : RbrgUtlDelNicknamesOnContext
 *
 *    DESCRIPTION      : This function deletes the Fdb entries on this context
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
RbrgUtlDelNicknamesOnContext (UINT4 u4ContextId)
{
    tRbrgFsrbridgeNicknameEntry *pNicknameEntry = NULL;
    tRbrgFsrbridgeNicknameEntry NicknameEntry;

    MEMSET (&NicknameEntry, 0, sizeof (tRbrgFsrbridgeNicknameEntry));

    pNicknameEntry = (tRbrgFsrbridgeNicknameEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable);

    while ((pNicknameEntry != NULL) &&
           (pNicknameEntry->MibObject.u4FsrbridgeContextId == u4ContextId))
    {
        NicknameEntry.MibObject.u4FsrbridgeContextId =
            pNicknameEntry->MibObject.u4FsrbridgeContextId;
        NicknameEntry.MibObject.i4FsrbridgeNicknameName =
            pNicknameEntry->MibObject.i4FsrbridgeNicknameName;

        RBTreeRemove (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
                      (tRBElem *) pNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pNicknameEntry);
        pNicknameEntry = (tRbrgFsrbridgeNicknameEntry *) RBTreeGetNext
            (gRbrgGlobals.RbrgGlbMib.FsrbridgeNicknameTable,
             (tRBElem *) & NicknameEntry, FsrbridgeNicknameTableRBCmp);
    }

    return OSIX_SUCCESS;
}
