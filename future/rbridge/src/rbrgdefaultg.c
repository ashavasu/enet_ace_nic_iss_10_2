/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: rbrgdefaultg.c 
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"

/****************************************************************************
* Function    : RbrgInitializeMibFsrbridgeUniFdbTable
* Input       : pRbrgFsRbridgeUniFdbEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
RbrgInitializeMibFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                       pRbrgFsRbridgeUniFdbEntry)
{
    UNUSED_PARAM (pRbrgFsRbridgeUniFdbEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : RbrgInitializeMibFsrbridgeUniFibTable
* Input       : pRbrgFsRbridgeUniFibEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
RbrgInitializeMibFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                       pRbrgFsRbridgeUniFibEntry)
{
    UNUSED_PARAM (pRbrgFsRbridgeUniFibEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : RbrgInitializeMibFsrbridgeMultiFibTable
* Input       : pRbrgFsrbridgeMultiFibEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
RbrgInitializeMibFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                         pRbrgFsrbridgeMultiFibEntry)
{
    UNUSED_PARAM (pRbrgFsrbridgeMultiFibEntry);
    return OSIX_SUCCESS;
}
