/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrglwg.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgeGlobalTable
 Input       :  The Indices
                FsrbridgeContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgeGlobalTable (UINT4 *pu4FsrbridgeContextId)
{

    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry = RbrgGetFirstFsrbridgeGlobalTable ();

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsrbridgeContextId =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgeNicknameTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgeNicknameTable (UINT4 *pu4FsrbridgeContextId,
                                        INT4 *pi4FsrbridgeNicknameName)
{

    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry = RbrgGetFirstFsrbridgeNicknameTable ();

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsrbridgeContextId =
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;

    *pi4FsrbridgeNicknameName =
        pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgePortTable
 Input       :  The Indices
                FsrbridgePortIfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgePortTable (INT4 *pi4FsrbridgePortIfIndex)
{

    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry = RbrgGetFirstFsrbridgePortTable ();

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsrbridgePortIfIndex =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgeUniFdbTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsRbrgUniFdbAddr
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgeUniFdbTable (UINT4 *pu4FsrbridgeContextId,
                                      UINT4 *pu4FsrbridgeFdbId,
                                      tMacAddr * pFsRbrgUniFdbAddress)
{
    tRbrgFsRbridgeUniFdbEntry RbrgFsRbridgeUniFdbEntry;
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    MEMSET (&RbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    pRbrgFsRbridgeUniFdbEntry = &RbrgFsRbridgeUniFdbEntry;

    if (RbrgUtlGetFirstFdbTable (pRbrgFsRbridgeUniFdbEntry) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    *pu4FsrbridgeContextId =
        pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;

    *pu4FsrbridgeFdbId = pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;

    MEMCPY (pFsRbrgUniFdbAddress,
            &(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr), 6);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgeUniFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgeUniFibTable (UINT4 *pu4FsrbridgeContextId,
                                      INT4 *pi4FsrbridgeFibNickname,
                                      INT4 *pi4FsrbridgeFibPort,
                                      INT4 *pi4FsrbridgeFibNextHopRBridge)
{
    tRbrgFsRbridgeUniFibEntry RbrgFsRbridgeUniFibEntry;
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;

    MEMSET (&RbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    pRbrgFsRbridgeUniFibEntry = &RbrgFsRbridgeUniFibEntry;
    if (RbrgUtlGetFirstFibTable (pRbrgFsRbridgeUniFibEntry) == OSIX_FAILURE)

    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsrbridgeContextId =
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;

    *pi4FsrbridgeFibNickname =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;

    *pi4FsrbridgeFibPort =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;

    *pi4FsrbridgeFibNextHopRBridge =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgeMultiFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgeMultiFibTable (UINT4 *pu4FsrbridgeContextId,
                                        INT4 *pi4FsrbridgeMultiFibNickname)
{

    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry = RbrgGetFirstFsrbridgeMultiFibTable ();

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsrbridgeContextId =
        pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId;

    *pi4FsrbridgeMultiFibNickname =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrbridgePortCounterTable
 Input       :  The Indices
                FsrbridgePortIfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrbridgePortCounterTable (INT4 *pi4FsrbridgePortIfIndex)
{

    tRbrgFsRBridgeBasePortEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry = (tRbrgFsRBridgeBasePortEntry *)
        RbrgGetFirstFsrbridgePortTable ();

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsrbridgePortIfIndex =
        pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgeGlobalTable
 Input       :  The Indices
                FsrbridgeContextId
                nextFsrbridgeContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgeGlobalTable (UINT4 u4FsrbridgeContextId,
                                     UINT4 *pu4NextFsrbridgeContextId)
{

    tRbrgFsrbridgeGlobalEntry RbrgFsrbridgeGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pNextRbrgFsrbridgeGlobalEntry = NULL;

    MEMSET (&RbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    RbrgFsrbridgeGlobalEntry.MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pNextRbrgFsrbridgeGlobalEntry =
        RbrgGetNextFsrbridgeGlobalTable (&RbrgFsrbridgeGlobalEntry);

    if (pNextRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsrbridgeContextId =
        pNextRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgeNicknameTable
 Input       :  The Indices
                FsrbridgeContextId
                nextFsrbridgeContextId
                FsrbridgeNicknameName
                nextFsrbridgeNicknameName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgeNicknameTable (UINT4 u4FsrbridgeContextId,
                                       UINT4 *pu4NextFsrbridgeContextId,
                                       INT4 i4FsrbridgeNicknameName,
                                       INT4 *pi4NextFsrbridgeNicknameName)
{

    tRbrgFsrbridgeNicknameEntry RbrgFsrbridgeNicknameEntry;
    tRbrgFsrbridgeNicknameEntry *pNextRbrgFsrbridgeNicknameEntry = NULL;

    MEMSET (&RbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    /* Assign the index */
    RbrgFsrbridgeNicknameEntry.MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    RbrgFsrbridgeNicknameEntry.MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;

    pNextRbrgFsrbridgeNicknameEntry =
        RbrgGetNextFsrbridgeNicknameTable (&RbrgFsrbridgeNicknameEntry);

    if (pNextRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsrbridgeContextId =
        pNextRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId;

    *pi4NextFsrbridgeNicknameName =
        pNextRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgePortTable
 Input       :  The Indices
                FsrbridgePortIfIndex
                nextFsrbridgePortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgePortTable (INT4 i4FsrbridgePortIfIndex,
                                   INT4 *pi4NextFsrbridgePortIfIndex)
{

    tRbrgFsRBridgeBasePortEntry RbrgFsRBridgeBasePortEntry;
    tRbrgFsRBridgeBasePortEntry *pNextRbrgFsRBridgeBasePortEntry = NULL;

    MEMSET (&RbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    RbrgFsRBridgeBasePortEntry.MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    pNextRbrgFsRBridgeBasePortEntry =
        RbrgGetNextFsrbridgePortTable (&RbrgFsRBridgeBasePortEntry);

    if (pNextRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsrbridgePortIfIndex =
        pNextRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgeUniFdbTable
 Input       :  The Indices
                FsrbridgeContextId
                nextFsrbridgeContextId
                FsrbridgeFdbId
                nextFsrbridgeFdbId
                FsrbridgeUniFdbAddr
                nextFsrbridgeUniFdbAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgeUniFdbTable (UINT4 u4FsrbridgeContextId,
                                     UINT4 *pu4NextFsrbridgeContextId,
                                     UINT4 u4FsrbridgeFdbId,
                                     UINT4 *pu4NextFsrbridgeFdbId,
                                     tMacAddr FsRbrgUniFdbAddress,
                                     tMacAddr * pNextFsrbridgeUniFdbAddr)
{
    tRbrgFsRbridgeUniFdbEntry RbrgFsRbridgeUniFdbEntry;
    tRbrgFsRbridgeUniFdbEntry NextRbrgFsRbridgeUniFdbEntry;
    tRbrgFsRbridgeUniFdbEntry *pNextRbrgFsRbridgeUniFdbEntry = NULL;

    MEMSET (&RbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&NextRbrgFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    RbrgFsRbridgeUniFdbEntry.MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(RbrgFsRbridgeUniFdbEntry.MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddress, 6);

    pNextRbrgFsRbridgeUniFdbEntry = &NextRbrgFsRbridgeUniFdbEntry;

    if (RbrgUtlGetNextFdbEntry (&RbrgFsRbridgeUniFdbEntry,
                                pNextRbrgFsRbridgeUniFdbEntry) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsrbridgeContextId =
        pNextRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;

    *pu4NextFsrbridgeFdbId =
        pNextRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;

    MEMCPY (pNextFsrbridgeUniFdbAddr,
            &(pNextRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr), 6);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgeUniFibTable
 Input       :  The Indices
                FsrbridgeContextId
                nextFsrbridgeContextId
                FsrbridgeFibNickname
                nextFsrbridgeFibNickname
                FsrbridgeFibPort
                nextFsrbridgeFibPort
                FsrbridgeFibNextHopRBridge
                nextFsrbridgeFibNextHopRBridge
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgeUniFibTable (UINT4 u4FsrbridgeContextId,
                                     UINT4 *pu4NextFsrbridgeContextId,
                                     INT4 i4FsrbridgeFibNickname,
                                     INT4 *pi4NextFsrbridgeFibNickname,
                                     INT4 i4FsrbridgeFibPort,
                                     INT4 *pi4NextFsrbridgeFibPort,
                                     INT4 i4FsrbridgeFibNextHopRBridge,
                                     INT4 *pi4NextFsrbridgeFibNextHopRBridge)
{
    tRbrgFsRbridgeUniFibEntry *pNextRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgFsRbridgeUniFibEntry NextRbrgFsRbridgeUniFibEntry;
    tRbrgFsRbridgeUniFibEntry RbrgFsRbridgeUniFibEntry;

    MEMSET (&RbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    RbrgFsRbridgeUniFibEntry.MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibPort = i4FsrbridgeFibPort;

    RbrgFsRbridgeUniFibEntry.MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    MEMSET (&NextRbrgFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgFsRbridgeUniFibEntry));
    pNextRbrgFsRbridgeUniFibEntry = &NextRbrgFsRbridgeUniFibEntry;
    if (RbrgUtlGetNextFibTable (&RbrgFsRbridgeUniFibEntry,
                                pNextRbrgFsRbridgeUniFibEntry) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Assign the next index */
    *pu4NextFsrbridgeContextId =
        pNextRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;

    *pi4NextFsrbridgeFibNickname =
        pNextRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;

    *pi4NextFsrbridgeFibPort =
        pNextRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;

    *pi4NextFsrbridgeFibNextHopRBridge =
        pNextRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgeMultiFibTable
 Input       :  The Indices
                FsrbridgeContextId
                nextFsrbridgeContextId
                FsrbridgeMultiFibNickname
                nextFsrbridgeMultiFibNickname
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgeMultiFibTable (UINT4 u4FsrbridgeContextId,
                                       UINT4 *pu4NextFsrbridgeContextId,
                                       INT4 i4FsrbridgeMultiFibNickname,
                                       INT4 *pi4NextFsrbridgeMultiFibNickname)
{

    tRbrgFsrbridgeMultiFibEntry RbrgFsrbridgeMultiFibEntry;
    tRbrgFsrbridgeMultiFibEntry *pNextRbrgFsrbridgeMultiFibEntry = NULL;

    MEMSET (&RbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));

    /* Assign the index */
    RbrgFsrbridgeMultiFibEntry.MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    RbrgFsrbridgeMultiFibEntry.MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;

    pNextRbrgFsrbridgeMultiFibEntry =
        RbrgGetNextFsrbridgeMultiFibTable (&RbrgFsrbridgeMultiFibEntry);

    if (pNextRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsrbridgeContextId =
        pNextRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId;

    *pi4NextFsrbridgeMultiFibNickname =
        pNextRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrbridgePortCounterTable
 Input       :  The Indices
                FsrbridgePortIfIndex
                nextFsrbridgePortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsrbridgePortCounterTable (INT4 i4FsrbridgePortIfIndex,
                                          INT4 *pi4NextFsrbridgePortIfIndex)
{

    tRbrgFsRBridgeBasePortEntry RbrgFsRbridgePortCounterEntry;
    tRbrgFsRBridgeBasePortEntry *pNextRbrgFsRbridgePortCounterEntry = NULL;

    MEMSET (&RbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    RbrgFsRbridgePortCounterEntry.MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    pNextRbrgFsRbridgePortCounterEntry =
        RbrgGetNextFsrbridgePortTable (&RbrgFsRbridgePortCounterEntry);

    if (pNextRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsrbridgePortIfIndex =
        pNextRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeGlobalTrace
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsrbridgeGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeGlobalTrace (UINT4 *pu4RetValFsrbridgeGlobalTrace)
{
    if (RbrgGetFsrbridgeGlobalTrace (pu4RetValFsrbridgeGlobalTrace) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeTrillVersion
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                UINT4 *pu4RetValFsrbridgeTrillVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeTrillVersion (UINT4 u4FsrbridgeContextId,
                             UINT4 *pu4RetValFsrbridgeTrillVersion)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeTrillVersion =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeTrillVersion;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeNumPorts
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                UINT4 *pu4RetValFsrbridgeNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeNumPorts (UINT4 u4FsrbridgeContextId,
                         UINT4 *pu4RetValFsrbridgeNumPorts)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeNumPorts =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNumPorts;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                INT4 *pi4RetValFsrbridgeUniMultipathEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniMultipathEnable (UINT4 u4FsrbridgeContextId,
                                   INT4 *pi4RetValFsrbridgeUniMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeUniMultipathEnable =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeMultiMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                INT4 *pi4RetValFsrbridgeMultiMultipathEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeMultiMultipathEnable (UINT4 u4FsrbridgeContextId,
                                     INT4
                                     *pi4RetValFsrbridgeMultiMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeMultiMultipathEnable =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeNicknameNumber
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                UINT4 *pu4RetValFsrbridgeNicknameNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeNicknameNumber (UINT4 u4FsrbridgeContextId,
                               UINT4 *pu4RetValFsrbridgeNicknameNumber)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeNicknameNumber =
        pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeSystemControl
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                INT4 *pi4RetValFsrbridgeSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeSystemControl (UINT4 u4FsrbridgeContextId,
                              INT4 *pi4RetValFsrbridgeSystemControl)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeSystemControl =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeModuleStatus
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                INT4 *pi4RetValFsrbridgeModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeModuleStatus (UINT4 u4FsrbridgeContextId,
                             INT4 *pi4RetValFsrbridgeModuleStatus)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeModuleStatus =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsrbridgeUnicastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                UINT4 *pu4RetValFsrbridgeUnicastMultipathCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUnicastMultipathCount (UINT4 u4FsrbridgeContextId,
                                      UINT4
                                      *pu4RetValFsrbridgeUnicastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbridgeFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeUnicastMultipathCount =
        pRbridgeFsrbridgeGlobalEntry->MibObject.
        u4FsrbridgeUnicastMultipathCount;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeMulticastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                UINT4 *pu4RetValFsrbridgeMulticastMultipathCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeMulticastMultipathCount (UINT4 u4FsrbridgeContextId,
                                        UINT4
                                        *pu4RetValFsrbridgeMulticastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbridgeFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeMulticastMultipathCount =
        pRbridgeFsrbridgeGlobalEntry->MibObject.
        u4FsrbridgeMulticastMultipathCount;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeClearCounters
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                INT4 *pi4RetValFsrbridgeClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeClearCounters (UINT4 u4FsrbridgeContextId,
                              INT4 *pi4RetValFsrbridgeClearCounters)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    if (RbrgGetAllFsrbridgeGlobalTable (pRbrgFsrbridgeGlobalEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeClearCounters =
        pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters;

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeNicknamePriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                UINT4 *pu4RetValFsrbridgeNicknamePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeNicknamePriority (UINT4 u4FsrbridgeContextId,
                                 INT4 i4FsrbridgeNicknameName,
                                 UINT4 *pu4RetValFsrbridgeNicknamePriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;

    if (RbrgGetAllFsrbridgeNicknameTable (pRbrgFsrbridgeNicknameEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeNicknamePriority =
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority;

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeNicknameDtrPriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                UINT4 *pu4RetValFsrbridgeNicknameDtrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeNicknameDtrPriority (UINT4 u4FsrbridgeContextId,
                                    INT4 i4FsrbridgeNicknameName,
                                    UINT4
                                    *pu4RetValFsrbridgeNicknameDtrPriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;

    if (RbrgGetAllFsrbridgeNicknameTable (pRbrgFsrbridgeNicknameEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeNicknameDtrPriority =
        pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority;

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeNicknameStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                INT4 *pi4RetValFsrbridgeNicknameStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeNicknameStatus (UINT4 u4FsrbridgeContextId,
                               INT4 i4FsrbridgeNicknameName,
                               INT4 *pi4RetValFsrbridgeNicknameStatus)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;

    if (RbrgGetAllFsrbridgeNicknameTable (pRbrgFsrbridgeNicknameEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeNicknameStatus =
        pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortDisable
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortDisable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortDisable (INT4 i4FsrbridgePortIfIndex,
                            INT4 *pi4RetValFsrbridgePortDisable)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortDisable =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortTrunkPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortTrunkPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortTrunkPort (INT4 i4FsrbridgePortIfIndex,
                              INT4 *pi4RetValFsrbridgePortTrunkPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortTrunkPort =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortAccessPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortAccessPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortAccessPort (INT4 i4FsrbridgePortIfIndex,
                               INT4 *pi4RetValFsrbridgePortAccessPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortAccessPort =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortState
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortState (INT4 i4FsrbridgePortIfIndex,
                          INT4 *pi4RetValFsrbridgePortState)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortState =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortState;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortDisableLearning
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortDisableLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortDisableLearning (INT4 i4FsrbridgePortIfIndex,
                                    INT4 *pi4RetValFsrbridgePortDisableLearning)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortDisableLearning =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortDesigVlan
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortDesigVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortDesigVlan (INT4 i4FsrbridgePortIfIndex,
                              INT4 *pi4RetValFsrbridgePortDesigVlan)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortDesigVlan =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortClearCounters
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                INT4 *pi4RetValFsrbridgePortClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortClearCounters (INT4 i4FsrbridgePortIfIndex,
                                  INT4 *pi4RetValFsrbridgePortClearCounters)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgePortClearCounters =
        pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortMac
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                tMacAddr * pRetValFsrbridgePortMac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortMac (INT4 i4FsrbridgePortIfIndex,
                        tMacAddr * pRetValFsrbridgePortMac)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortTable (pRbrgFsRBridgeBasePortEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsrbridgePortMac,
            &(pRbrgFsRBridgeBasePortEntry->MibObject.FsrbridgePortMac), 6);

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniFdbPort
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                INT4 *pi4RetValFsrbridgeUniFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniFdbPort (UINT4 u4FsrbridgeContextId, UINT4 u4FsrbridgeFdbId,
                           tMacAddr FsrbridgeUniFdbAddress,
                           INT4 *pi4RetValFsrbridgeUniFdbPort)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);

    if (RbrgGetAllFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeUniFdbPort =
        pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniFdbNick
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                INT4 *pi4RetValFsrbridgeUniFdbNick
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniFdbNick (UINT4 u4FsrbridgeContextId, UINT4 u4FsrbridgeFdbId,
                           tMacAddr FsrbridgeUniFdbAddress,
                           INT4 *pi4RetValFsrbridgeUniFdbNick)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);

    if (RbrgGetAllFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeUniFdbNick =
        pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniFdbConfidence
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                UINT4 *pu4RetValFsrbridgeUniFdbConfidence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniFdbConfidence (UINT4 u4FsrbridgeContextId,
                                 UINT4 u4FsrbridgeFdbId,
                                 tMacAddr FsrbridgeUniFdbAddress,
                                 UINT4 *pu4RetValFsrbridgeUniFdbConfidence)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);

    if (RbrgGetAllFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeUniFdbConfidence =
        pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniFdbStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                INT4 *pi4RetValFsrbridgeUniFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniFdbStatus (UINT4 u4FsrbridgeContextId, UINT4 u4FsrbridgeFdbId,
                             tMacAddr FsrbridgeUniFdbAddress,
                             INT4 *pi4RetValFsrbridgeUniFdbStatus)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);

    if (RbrgGetAllFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeUniFdbStatus =
        pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeUniFdbRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                INT4 *pi4RetValFsrbridgeUniFdbRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeUniFdbRowStatus (UINT4 u4FsrbridgeContextId,
                                UINT4 u4FsrbridgeFdbId,
                                tMacAddr FsrbridgeUniFdbAddress,
                                INT4 *pi4RetValFsrbridgeUniFdbRowStatus)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);

    if (RbrgGetAllFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeUniFdbRowStatus =
        pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeFibMacAddress
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsrbridgeFibMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeFibMacAddress (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeFibNickname,
                              INT4 i4FsrbridgeFibPort,
                              INT4 i4FsrbridgeFibNextHopRBridge,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsrbridgeFibMacAddress)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    if (RbrgGetAllFsrbridgeUniFibTable (pRbrgFsRbridgeUniFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsrbridgeFibMacAddress->pu1_OctetList,
            pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen);
    pRetValFsrbridgeFibMacAddress->i4_Length =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeFibMtuDesired
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                UINT4 *pu4RetValFsrbridgeFibMtuDesired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeFibMtuDesired (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeFibNickname,
                              INT4 i4FsrbridgeFibPort,
                              INT4 i4FsrbridgeFibNextHopRBridge,
                              UINT4 *pu4RetValFsrbridgeFibMtuDesired)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    if (RbrgGetAllFsrbridgeUniFibTable (pRbrgFsRbridgeUniFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeFibMtuDesired =
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeFibHopCount
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                UINT4 *pu4RetValFsrbridgeFibHopCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeFibHopCount (UINT4 u4FsrbridgeContextId,
                            INT4 i4FsrbridgeFibNickname,
                            INT4 i4FsrbridgeFibPort,
                            INT4 i4FsrbridgeFibNextHopRBridge,
                            UINT4 *pu4RetValFsrbridgeFibHopCount)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    if (RbrgGetAllFsrbridgeUniFibTable (pRbrgFsRbridgeUniFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgeFibHopCount =
        pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeFibStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                INT4 *pi4RetValFsrbridgeFibStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeFibStatus (UINT4 u4FsrbridgeContextId,
                          INT4 i4FsrbridgeFibNickname, INT4 i4FsrbridgeFibPort,
                          INT4 i4FsrbridgeFibNextHopRBridge,
                          INT4 *pi4RetValFsrbridgeFibStatus)
{
    tRbrgFsRbridgeUniFibEntry *pRbridgeFsRbridgeUniFibEntry = NULL;

    pRbridgeFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);

    if (pRbridgeFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbridgeFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbridgeFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    pRbridgeFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;

    pRbridgeFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    if (RbrgGetAllFsrbridgeUniFibTable (pRbridgeFsRbridgeUniFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbridgeFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeFibStatus =
        pRbridgeFsRbridgeUniFibEntry->MibObject.i4FsrbridgeUniFibStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbridgeFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeFibRowstatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                INT4 *pi4RetValFsrbridgeFibRowstatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeFibRowstatus (UINT4 u4FsrbridgeContextId,
                             INT4 i4FsrbridgeFibNickname,
                             INT4 i4FsrbridgeFibPort,
                             INT4 i4FsrbridgeFibNextHopRBridge,
                             INT4 *pi4RetValFsrbridgeFibRowstatus)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;

    if (RbrgGetAllFsrbridgeUniFibTable (pRbrgFsRbridgeUniFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeFibRowstatus =
        pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeMultiFibPorts
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsrbridgeMultiFibPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeMultiFibPorts (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeMultiFibNickname,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsrbridgeMultiFibPorts)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;

    if (RbrgGetAllFsrbridgeMultiFibTable (pRbrgFsrbridgeMultiFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsrbridgeMultiFibPorts->pu1_OctetList,
            pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
            RBRG_MAX_PORTLIST_SIZE);
    pRetValFsrbridgeMultiFibPorts->i4_Length = RBRG_MAX_PORTLIST_SIZE;

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeMultiFibStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
                INT4 *pi4RetValFsrbridgeMultiFibStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeMultiFibStatus (UINT4 u4FsrbridgeContextId,
                               INT4 i4FsrbridgeMultiFibNickname,
                               INT4 *pi4RetValFsrbridgeMultiFibStatus)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;

    if (RbrgGetAllFsrbridgeMultiFibTable (pRbrgFsrbridgeMultiFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeMultiFibStatus =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgeMultiFibRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
                INT4 *pi4RetValFsrbridgeMultiFibRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgeMultiFibRowStatus (UINT4 u4FsrbridgeContextId,
                                  INT4 i4FsrbridgeMultiFibNickname,
                                  INT4 *pi4RetValFsrbridgeMultiFibRowStatus)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;

    if (RbrgGetAllFsrbridgeMultiFibTable (pRbrgFsrbridgeMultiFibEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsrbridgeMultiFibRowStatus =
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus;

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortRpfChecksFailed
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                UINT4 *pu4RetValFsrbridgePortRpfChecksFailed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortRpfChecksFailed (INT4 i4FsrbridgePortIfIndex,
                                    UINT4
                                    *pu4RetValFsrbridgePortRpfChecksFailed)
{
    tRbrgFsRbridgePortCounterEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry =
        (tRbrgFsRbridgePortCounterEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID);

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortCounterTable (pRbrgFsRbridgePortCounterEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgePortCounterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgePortRpfChecksFailed =
        pRbrgFsRbridgePortCounterEntry->MibObject.
        u4FsrbridgePortRpfChecksFailed;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgePortCounterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortHopCountsExceeded
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                UINT4 *pu4RetValFsrbridgePortHopCountsExceeded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortHopCountsExceeded (INT4 i4FsrbridgePortIfIndex,
                                      UINT4
                                      *pu4RetValFsrbridgePortHopCountsExceeded)
{
    tRbrgFsRbridgePortCounterEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry =
        (tRbrgFsRbridgePortCounterEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID);

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortCounterTable (pRbrgFsRbridgePortCounterEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgePortCounterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgePortHopCountsExceeded =
        pRbrgFsRbridgePortCounterEntry->MibObject.
        u4FsrbridgePortHopCountsExceeded;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgePortCounterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortOptions
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                UINT4 *pu4RetValFsrbridgePortOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortOptions (INT4 i4FsrbridgePortIfIndex,
                            UINT4 *pu4RetValFsrbridgePortOptions)
{
    tRbrgFsRbridgePortCounterEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry =
        (tRbrgFsRbridgePortCounterEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID);

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortCounterTable (pRbrgFsRbridgePortCounterEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgePortCounterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsrbridgePortOptions =
        pRbrgFsRbridgePortCounterEntry->MibObject.u4FsrbridgePortOptions;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgePortCounterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortTrillInFrames
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsrbridgePortTrillInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortTrillInFrames (INT4 i4FsrbridgePortIfIndex,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValFsrbridgePortTrillInFrames)
{
    tRbrgFsRbridgePortCounterEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry =
        (tRbrgFsRbridgePortCounterEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID);

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortCounterTable (pRbrgFsRbridgePortCounterEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgePortCounterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsrbridgePortTrillInFrames =
        pRbrgFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillInFrames;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgePortCounterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrbridgePortTrillOutFrames
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsrbridgePortTrillOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrbridgePortTrillOutFrames (INT4 i4FsrbridgePortIfIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsrbridgePortTrillOutFrames)
{
    tRbrgFsRbridgePortCounterEntry *pRbrgFsRbridgePortCounterEntry = NULL;

    pRbrgFsRbridgePortCounterEntry =
        (tRbrgFsRbridgePortCounterEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID);

    if (pRbrgFsRbridgePortCounterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgePortCounterEntry, 0,
            sizeof (tRbrgFsRbridgePortCounterEntry));

    /* Assign the index */
    pRbrgFsRbridgePortCounterEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;

    if (RbrgGetAllFsrbridgePortCounterTable (pRbrgFsRbridgePortCounterEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgePortCounterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsrbridgePortTrillOutFrames =
        pRbrgFsRbridgePortCounterEntry->MibObject.u8FsrbridgePortTrillOutFrames;

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgePortCounterEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeGlobalTrace
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsrbridgeGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeGlobalTrace (UINT4 u4SetValFsrbridgeGlobalTrace)
{
    if (RbrgSetFsrbridgeGlobalTrace (u4SetValFsrbridgeGlobalTrace) !=
        OSIX_SUCCESS)
    {

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUniMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  INT4 i4SetValFsrbridgeUniMultipathEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUniMultipathEnable (UINT4 u4FsrbridgeContextId,
                                   INT4 i4SetValFsrbridgeUniMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable =
        i4SetValFsrbridgeUniMultipathEnable;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeMultiMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  INT4 i4SetValFsrbridgeMultiMultipathEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeMultiMultipathEnable (UINT4 u4FsrbridgeContextId,
                                     INT4 i4SetValFsrbridgeMultiMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable =
        i4SetValFsrbridgeMultiMultipathEnable;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeNicknameNumber
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  UINT4 u4SetValFsrbridgeNicknameNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeNicknameNumber (UINT4 u4FsrbridgeContextId,
                               UINT4 u4SetValFsrbridgeNicknameNumber)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber =
        u4SetValFsrbridgeNicknameNumber;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeSystemControl
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  INT4 i4SetValFsrbridgeSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeSystemControl (UINT4 u4FsrbridgeContextId,
                              INT4 i4SetValFsrbridgeSystemControl)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl =
        i4SetValFsrbridgeSystemControl;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeModuleStatus
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  INT4 i4SetValFsrbridgeModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeModuleStatus (UINT4 u4FsrbridgeContextId,
                             INT4 i4SetValFsrbridgeModuleStatus)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus =
        i4SetValFsrbridgeModuleStatus;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUnicastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  UINT4 u4SetValFsrbridgeUnicastMultipathCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUnicastMultipathCount (UINT4 u4FsrbridgeContextId,
                                      UINT4
                                      u4SetValFsrbridgeUnicastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbridgeIsSetFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbridgeIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbridgeIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbridgeIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeUnicastMultipathCount =
        u4SetValFsrbridgeUnicastMultipathCount;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount =
        OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbridgeFsrbridgeGlobalEntry,
         pRbridgeIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeMulticastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  UINT4 u4SetValFsrbridgeMulticastMultipathCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeMulticastMultipathCount (UINT4 u4FsrbridgeContextId,
                                        UINT4
                                        u4SetValFsrbridgeMulticastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbridgeIsSetFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbridgeIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbridgeIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbridgeIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeMulticastMultipathCount =
        u4SetValFsrbridgeMulticastMultipathCount;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount =
        OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbridgeFsrbridgeGlobalEntry,
         pRbridgeIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeClearCounters
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
             :  INT4 i4SetValFsrbridgeClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeClearCounters (UINT4 u4FsrbridgeContextId,
                              INT4 i4SetValFsrbridgeClearCounters)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);
    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters =
        i4SetValFsrbridgeClearCounters;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeNicknamePriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
             :  UINT4 u4SetValFsrbridgeNicknamePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeNicknamePriority (UINT4 u4FsrbridgeContextId,
                                 INT4 i4FsrbridgeNicknameName,
                                 UINT4 u4SetValFsrbridgeNicknamePriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);
    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority =
        u4SetValFsrbridgeNicknamePriority;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeNicknameTable
        (pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeNicknameDtrPriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
             :  UINT4 u4SetValFsrbridgeNicknameDtrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeNicknameDtrPriority (UINT4 u4FsrbridgeContextId,
                                    INT4 i4FsrbridgeNicknameName,
                                    UINT4 u4SetValFsrbridgeNicknameDtrPriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);
    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority =
        u4SetValFsrbridgeNicknameDtrPriority;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeNicknameTable
        (pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeNicknameStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
             :  INT4 i4SetValFsrbridgeNicknameStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeNicknameStatus (UINT4 u4FsrbridgeContextId,
                               INT4 i4FsrbridgeNicknameName,
                               INT4 i4SetValFsrbridgeNicknameStatus)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);
    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus =
        i4SetValFsrbridgeNicknameStatus;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeNicknameTable
        (pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortDisable
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortDisable (INT4 i4FsrbridgePortIfIndex,
                            INT4 i4SetValFsrbridgePortDisable)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable =
        i4SetValFsrbridgePortDisable;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortTrunkPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortTrunkPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortTrunkPort (INT4 i4FsrbridgePortIfIndex,
                              INT4 i4SetValFsrbridgePortTrunkPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort =
        i4SetValFsrbridgePortTrunkPort;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortAccessPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortAccessPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortAccessPort (INT4 i4FsrbridgePortIfIndex,
                               INT4 i4SetValFsrbridgePortAccessPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort =
        i4SetValFsrbridgePortAccessPort;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortDisableLearning
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortDisableLearning
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortDisableLearning (INT4 i4FsrbridgePortIfIndex,
                                    INT4 i4SetValFsrbridgePortDisableLearning)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning =
        i4SetValFsrbridgePortDisableLearning;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortDesigVlan
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortDesigVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortDesigVlan (INT4 i4FsrbridgePortIfIndex,
                              INT4 i4SetValFsrbridgePortDesigVlan)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan =
        i4SetValFsrbridgePortDesigVlan;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgePortClearCounters
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
             :  INT4 i4SetValFsrbridgePortClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgePortClearCounters (INT4 i4FsrbridgePortIfIndex,
                                  INT4 i4SetValFsrbridgePortClearCounters)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);
    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters =
        i4SetValFsrbridgePortClearCounters;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters = OSIX_TRUE;

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUniFdbPort
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
             :  INT4 i4SetValFsrbridgeUniFdbPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUniFdbPort (UINT4 u4FsrbridgeContextId, UINT4 u4FsrbridgeFdbId,
                           tMacAddr FsrbridgeUniFdbAddress,
                           INT4 i4SetValFsrbridgeUniFdbPort)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsrbridgeUniFdbAddress, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        i4SetValFsrbridgeUniFdbPort;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFdbTable
        (pRbrgFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUniFdbNick
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
             :  INT4 i4SetValFsrbridgeUniFdbNick
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUniFdbNick (UINT4 u4FsrbridgeContextId, UINT4 u4FsrbridgeFdbId,
                           tMacAddr FsRbrgUniFdbAddr,
                           INT4 i4SetValFsrbridgeUniFdbNick)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        i4SetValFsrbridgeUniFdbNick;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFdbTable
        (pRbrgFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUniFdbConfidence
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
             :  UINT4 u4SetValFsrbridgeUniFdbConfidence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUniFdbConfidence (UINT4 u4FsrbridgeContextId,
                                 UINT4 u4FsrbridgeFdbId,
                                 tMacAddr FsRbrgUniFdbAddr,
                                 UINT4 u4SetValFsrbridgeUniFdbConfidence)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        u4SetValFsrbridgeUniFdbConfidence;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFdbTable
        (pRbrgFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeUniFdbRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
             :  INT4 i4SetValFsrbridgeUniFdbRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeUniFdbRowStatus (UINT4 u4FsrbridgeContextId,
                                UINT4 u4FsrbridgeFdbId,
                                tMacAddr FsRbrgUniFdbAddr,
                                INT4 i4SetValFsrbridgeUniFdbRowStatus)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
        i4SetValFsrbridgeUniFdbRowStatus;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFdbTable
        (pRbrgFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeFibMacAddress
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsrbridgeFibMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeFibMacAddress (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeFibNickname,
                              INT4 i4FsrbridgeFibPort,
                              INT4 i4FsrbridgeFibNextHopRBridge,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsrbridgeFibMacAddress)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            pSetValFsrbridgeFibMacAddress->pu1_OctetList,
            pSetValFsrbridgeFibMacAddress->i4_Length);
    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen =
        pSetValFsrbridgeFibMacAddress->i4_Length;

    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFibTable
        (pRbrgFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeFibMtuDesired
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
             :  UINT4 u4SetValFsrbridgeFibMtuDesired
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeFibMtuDesired (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeFibNickname,
                              INT4 i4FsrbridgeFibPort,
                              INT4 i4FsrbridgeFibNextHopRBridge,
                              UINT4 u4SetValFsrbridgeFibMtuDesired)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        u4SetValFsrbridgeFibMtuDesired;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFibTable
        (pRbrgFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeFibHopCount
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
             :  UINT4 u4SetValFsrbridgeFibHopCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeFibHopCount (UINT4 u4FsrbridgeContextId,
                            INT4 i4FsrbridgeFibNickname,
                            INT4 i4FsrbridgeFibPort,
                            INT4 i4FsrbridgeFibNextHopRBridge,
                            UINT4 u4SetValFsrbridgeFibHopCount)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
        u4SetValFsrbridgeFibHopCount;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFibTable
        (pRbrgFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeFibRowstatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
             :  INT4 i4SetValFsrbridgeFibRowstatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeFibRowstatus (UINT4 u4FsrbridgeContextId,
                             INT4 i4FsrbridgeFibNickname,
                             INT4 i4FsrbridgeFibPort,
                             INT4 i4FsrbridgeFibNextHopRBridge,
                             INT4 i4SetValFsrbridgeFibRowstatus)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
        i4SetValFsrbridgeFibRowstatus;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeUniFibTable
        (pRbrgFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeMultiFibPorts
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsrbridgeMultiFibPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeMultiFibPorts (UINT4 u4FsrbridgeContextId,
                              INT4 i4FsrbridgeMultiFibNickname,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsrbridgeMultiFibPorts)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbrgIsSetFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeMultiFibEntry =
        (tRbrgIsSetFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgIsSetFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
            pSetValFsrbridgeMultiFibPorts->pu1_OctetList,
            pSetValFsrbridgeMultiFibPorts->i4_Length);
    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen =
        pSetValFsrbridgeMultiFibPorts->i4_Length;

    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeMultiFibTable
        (pRbrgFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrbridgeMultiFibRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
             :  INT4 i4SetValFsrbridgeMultiFibRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrbridgeMultiFibRowStatus (UINT4 u4FsrbridgeContextId,
                                  INT4 i4FsrbridgeMultiFibNickname,
                                  INT4 i4SetValFsrbridgeMultiFibRowStatus)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbrgIsSetFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);
    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRbrgIsSetFsrbridgeMultiFibEntry =
        (tRbrgIsSetFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);
    if (pRbrgIsSetFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgIsSetFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
        i4SetValFsrbridgeMultiFibRowStatus;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus = OSIX_TRUE;

    if (RbrgSetAllFsrbridgeMultiFibTable
        (pRbrgFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeGlobalTrace
 Input       :  The Indices

                The Object 
                testValFsrbridgeGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeGlobalTrace (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsrbridgeGlobalTrace)
{
    if (RbrgTestFsrbridgeGlobalTrace
        (pu4ErrorCode, u4TestValFsrbridgeGlobalTrace) != OSIX_SUCCESS)
    {

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUniMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeUniMultipathEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUniMultipathEnable (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsrbridgeContextId,
                                      INT4 i4TestValFsrbridgeUniMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable =
        i4TestValFsrbridgeUniMultipathEnable;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeMultiMultipathEnable
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeMultiMultipathEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeMultiMultipathEnable (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsrbridgeContextId,
                                        INT4
                                        i4TestValFsrbridgeMultiMultipathEnable)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable =
        i4TestValFsrbridgeMultiMultipathEnable;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeNicknameNumber
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeNicknameNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeNicknameNumber (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsrbridgeContextId,
                                  UINT4 u4TestValFsrbridgeNicknameNumber)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber =
        u4TestValFsrbridgeNicknameNumber;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeSystemControl
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeSystemControl (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsrbridgeContextId,
                                 INT4 i4TestValFsrbridgeSystemControl)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl =
        i4TestValFsrbridgeSystemControl;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeModuleStatus
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeModuleStatus (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeContextId,
                                INT4 i4TestValFsrbridgeModuleStatus)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus =
        i4TestValFsrbridgeModuleStatus;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUnicastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeUnicastMultipathCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUnicastMultipathCount (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsrbridgeContextId,
                                         UINT4
                                         u4TestValFsrbridgeUnicastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbridgeIsSetFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbridgeIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbridgeIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbridgeIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeUnicastMultipathCount =
        u4TestValFsrbridgeUnicastMultipathCount;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount =
        OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbridgeFsrbridgeGlobalEntry,
         pRbridgeIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeMulticastMultipathCount
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeMulticastMultipathCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeMulticastMultipathCount (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsrbridgeContextId,
                                           UINT4
                                           u4TestValFsrbridgeMulticastMultipathCount)
{
    tRbrgFsrbridgeGlobalEntry *pRbridgeFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbridgeIsSetFsrbridgeGlobalEntry = NULL;

    pRbridgeFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbridgeFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbridgeIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbridgeIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbridgeFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbridgeIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbridgeFsrbridgeGlobalEntry->MibObject.u4FsrbridgeMulticastMultipathCount =
        u4TestValFsrbridgeMulticastMultipathCount;
    pRbridgeIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount =
        OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbridgeFsrbridgeGlobalEntry,
         pRbridgeIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbridgeFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeClearCounters
 Input       :  The Indices
                FsrbridgeContextId

                The Object 
                testValFsrbridgeClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeClearCounters (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsrbridgeContextId,
                                 INT4 i4TestValFsrbridgeClearCounters)
{
    tRbrgFsrbridgeGlobalEntry *pRbrgFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbrgIsSetFsrbridgeGlobalEntry = NULL;

    pRbrgFsrbridgeGlobalEntry =
        (tRbrgFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

    if (pRbrgFsrbridgeGlobalEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeGlobalEntry =
        (tRbrgIsSetFsrbridgeGlobalEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeGlobalEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));
    MEMSET (pRbrgIsSetFsrbridgeGlobalEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

    /* Assign the index */
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters =
        i4TestValFsrbridgeClearCounters;
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeGlobalTable
        (pu4ErrorCode, pRbrgFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeGlobalEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeGlobalEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeGlobalEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeNicknamePriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                testValFsrbridgeNicknamePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeNicknamePriority (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsrbridgeContextId,
                                    INT4 i4FsrbridgeNicknameName,
                                    UINT4 u4TestValFsrbridgeNicknamePriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority =
        u4TestValFsrbridgeNicknamePriority;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeNicknameTable
        (pu4ErrorCode, pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeNicknameDtrPriority
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                testValFsrbridgeNicknameDtrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeNicknameDtrPriority (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsrbridgeContextId,
                                       INT4 i4FsrbridgeNicknameName,
                                       UINT4
                                       u4TestValFsrbridgeNicknameDtrPriority)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority =
        u4TestValFsrbridgeNicknameDtrPriority;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeNicknameTable
        (pu4ErrorCode, pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeNicknameStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName

                The Object 
                testValFsrbridgeNicknameStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeNicknameStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsrbridgeContextId,
                                  INT4 i4FsrbridgeNicknameName,
                                  INT4 i4TestValFsrbridgeNicknameStatus)
{
    tRbrgFsrbridgeNicknameEntry *pRbrgFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbrgIsSetFsrbridgeNicknameEntry = NULL;

    pRbrgFsrbridgeNicknameEntry =
        (tRbrgFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

    if (pRbrgFsrbridgeNicknameEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeNicknameEntry =
        (tRbrgIsSetFsrbridgeNicknameEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeNicknameEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgFsrbridgeNicknameEntry));
    MEMSET (pRbrgIsSetFsrbridgeNicknameEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

    /* Assign the index */
    pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName =
        i4FsrbridgeNicknameName;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus =
        i4TestValFsrbridgeNicknameStatus;
    pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeNicknameTable
        (pu4ErrorCode, pRbrgFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeNicknameEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeNicknameEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeNicknameEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortDisable
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortDisable (UINT4 *pu4ErrorCode, INT4 i4FsrbridgePortIfIndex,
                               INT4 i4TestValFsrbridgePortDisable)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable =
        i4TestValFsrbridgePortDisable;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortTrunkPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortTrunkPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortTrunkPort (UINT4 *pu4ErrorCode,
                                 INT4 i4FsrbridgePortIfIndex,
                                 INT4 i4TestValFsrbridgePortTrunkPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort =
        i4TestValFsrbridgePortTrunkPort;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortAccessPort
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortAccessPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortAccessPort (UINT4 *pu4ErrorCode,
                                  INT4 i4FsrbridgePortIfIndex,
                                  INT4 i4TestValFsrbridgePortAccessPort)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort =
        i4TestValFsrbridgePortAccessPort;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortDisableLearning
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortDisableLearning
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortDisableLearning (UINT4 *pu4ErrorCode,
                                       INT4 i4FsrbridgePortIfIndex,
                                       INT4
                                       i4TestValFsrbridgePortDisableLearning)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning =
        i4TestValFsrbridgePortDisableLearning;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortDesigVlan
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortDesigVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortDesigVlan (UINT4 *pu4ErrorCode,
                                 INT4 i4FsrbridgePortIfIndex,
                                 INT4 i4TestValFsrbridgePortDesigVlan)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan =
        i4TestValFsrbridgePortDesigVlan;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgePortClearCounters
 Input       :  The Indices
                FsrbridgePortIfIndex

                The Object 
                testValFsrbridgePortClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgePortClearCounters (UINT4 *pu4ErrorCode,
                                     INT4 i4FsrbridgePortIfIndex,
                                     INT4 i4TestValFsrbridgePortClearCounters)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbrgIsSetFsRBridgeBasePortEntry = NULL;

    pRbrgFsRBridgeBasePortEntry =
        (tRbrgFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

    if (pRbrgFsRBridgeBasePortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRBridgeBasePortEntry =
        (tRbrgIsSetFsRBridgeBasePortEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRBridgeBasePortEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (pRbrgIsSetFsRBridgeBasePortEntry, 0,
            sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    /* Assign the index */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex =
        i4FsrbridgePortIfIndex;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters =
        i4TestValFsrbridgePortClearCounters;
    pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters = OSIX_TRUE;

    if (RbrgTestAllFsrbridgePortTable
        (pu4ErrorCode, pRbrgFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                            (UINT1 *) pRbrgFsRBridgeBasePortEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                        (UINT1 *) pRbrgFsRBridgeBasePortEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRBridgeBasePortEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUniFdbPort
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                testValFsrbridgeUniFdbPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUniFdbPort (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeContextId,
                              UINT4 u4FsrbridgeFdbId,
                              tMacAddr FsRbrgUniFdbAddr,
                              INT4 i4TestValFsrbridgeUniFdbPort)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort =
        i4TestValFsrbridgeUniFdbPort;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFdbTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUniFdbNick
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                testValFsrbridgeUniFdbNick
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUniFdbNick (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeContextId,
                              UINT4 u4FsrbridgeFdbId,
                              tMacAddr FsRbrgUniFdbAddr,
                              INT4 i4TestValFsrbridgeUniFdbNick)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick =
        i4TestValFsrbridgeUniFdbNick;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFdbTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUniFdbConfidence
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                testValFsrbridgeUniFdbConfidence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUniFdbConfidence (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsrbridgeContextId,
                                    UINT4 u4FsrbridgeFdbId,
                                    tMacAddr FsRbrgUniFdbAddr,
                                    UINT4 u4TestValFsrbridgeUniFdbConfidence)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence =
        u4TestValFsrbridgeUniFdbConfidence;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFdbTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeUniFdbRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr

                The Object 
                testValFsrbridgeUniFdbRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeUniFdbRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsrbridgeContextId,
                                   UINT4 u4FsrbridgeFdbId,
                                   tMacAddr FsRbrgUniFdbAddr,
                                   INT4 i4TestValFsrbridgeUniFdbRowStatus)
{
    tRbrgFsRbridgeUniFdbEntry *pRbrgFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbrgIsSetFsRbridgeUniFdbEntry = NULL;

    pRbrgFsRbridgeUniFdbEntry =
        (tRbrgFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFdbEntry =
        (tRbrgIsSetFsRbridgeUniFdbEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFdbEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFdbEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = u4FsrbridgeFdbId;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;

    MEMCPY (&(pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr),
            FsRbrgUniFdbAddr, 6);
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
        i4TestValFsrbridgeUniFdbRowStatus;
    pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFdbTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFdbEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFdbEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeFibMacAddress
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                testValFsrbridgeFibMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeFibMacAddress (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsrbridgeContextId,
                                 INT4 i4FsrbridgeFibNickname,
                                 INT4 i4FsrbridgeFibPort,
                                 INT4 i4FsrbridgeFibNextHopRBridge,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsrbridgeFibMacAddress)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress,
            pTestValFsrbridgeFibMacAddress->pu1_OctetList,
            pTestValFsrbridgeFibMacAddress->i4_Length);
    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen =
        pTestValFsrbridgeFibMacAddress->i4_Length;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFibTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeFibMtuDesired
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                testValFsrbridgeFibMtuDesired
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeFibMtuDesired (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsrbridgeContextId,
                                 INT4 i4FsrbridgeFibNickname,
                                 INT4 i4FsrbridgeFibPort,
                                 INT4 i4FsrbridgeFibNextHopRBridge,
                                 UINT4 u4TestValFsrbridgeFibMtuDesired)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired =
        u4TestValFsrbridgeFibMtuDesired;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFibTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeFibHopCount
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                testValFsrbridgeFibHopCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeFibHopCount (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeContextId,
                               INT4 i4FsrbridgeFibNickname,
                               INT4 i4FsrbridgeFibPort,
                               INT4 i4FsrbridgeFibNextHopRBridge,
                               UINT4 u4TestValFsrbridgeFibHopCount)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount =
        u4TestValFsrbridgeFibHopCount;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFibTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeFibRowstatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge

                The Object 
                testValFsrbridgeFibRowstatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeFibRowstatus (UINT4 *pu4ErrorCode, UINT4 u4FsrbridgeContextId,
                                INT4 i4FsrbridgeFibNickname,
                                INT4 i4FsrbridgeFibPort,
                                INT4 i4FsrbridgeFibNextHopRBridge,
                                INT4 i4TestValFsrbridgeFibRowstatus)
{
    tRbrgFsRbridgeUniFibEntry *pRbrgFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbrgIsSetFsRbridgeUniFibEntry = NULL;
    pRbrgFsRbridgeUniFibEntry =
        (tRbrgFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsRbridgeUniFibEntry =
        (tRbrgIsSetFsRbridgeUniFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsRbridgeUniFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsRbridgeUniFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (pRbrgIsSetFsRbridgeUniFibEntry, 0,
            sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    /* Assign the index */
    pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname =
        i4FsrbridgeFibNickname;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort =
        i4FsrbridgeFibPort;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;

    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge =
        i4FsrbridgeFibNextHopRBridge;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
        i4TestValFsrbridgeFibRowstatus;
    pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeUniFibTable
        (pu4ErrorCode, pRbrgFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsRbridgeUniFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsRbridgeUniFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsRbridgeUniFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeMultiFibPorts
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
                testValFsrbridgeMultiFibPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeMultiFibPorts (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsrbridgeContextId,
                                 INT4 i4FsrbridgeMultiFibNickname,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsrbridgeMultiFibPorts)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbrgIsSetFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeMultiFibEntry =
        (tRbrgIsSetFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgIsSetFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts,
            pTestValFsrbridgeMultiFibPorts->pu1_OctetList,
            pTestValFsrbridgeMultiFibPorts->i4_Length);
    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen =
        pTestValFsrbridgeMultiFibPorts->i4_Length;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeMultiFibTable
        (pu4ErrorCode, pRbrgFsrbridgeMultiFibEntry,
         pRbrgIsSetFsrbridgeMultiFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsrbridgeMultiFibRowStatus
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname

                The Object 
                testValFsrbridgeMultiFibRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsrbridgeMultiFibRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsrbridgeContextId,
                                     INT4 i4FsrbridgeMultiFibNickname,
                                     INT4 i4TestValFsrbridgeMultiFibRowStatus)
{
    tRbrgFsrbridgeMultiFibEntry *pRbrgFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbrgIsSetFsrbridgeMultiFibEntry = NULL;

    pRbrgFsrbridgeMultiFibEntry =
        (tRbrgFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRbrgIsSetFsrbridgeMultiFibEntry =
        (tRbrgIsSetFsrbridgeMultiFibEntry *)
        MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);

    if (pRbrgIsSetFsrbridgeMultiFibEntry == NULL)
    {
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pRbrgFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgFsrbridgeMultiFibEntry));
    MEMSET (pRbrgIsSetFsrbridgeMultiFibEntry, 0,
            sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));

    /* Assign the index */
    pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId =
        u4FsrbridgeContextId;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_TRUE;

    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname =
        i4FsrbridgeMultiFibNickname;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_TRUE;

    /* Assign the value */
    pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus =
        i4TestValFsrbridgeMultiFibRowStatus;
    pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus = OSIX_TRUE;

    if (RbrgTestAllFsrbridgeMultiFibTable
        (pu4ErrorCode, pRbrgFsrbridgeMultiFibEntry,
         pRbrgIsSetFsrbridgeMultiFibEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                            (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
        MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                            (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                        (UINT1 *) pRbrgFsrbridgeMultiFibEntry);
    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                        (UINT1 *) pRbrgIsSetFsrbridgeMultiFibEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsrbridgeGlobalTrace (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeGlobalTable
 Input       :  The Indices
                FsrbridgeContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgeGlobalTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeNicknameTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeNicknameName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgeNicknameTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgePortTable
 Input       :  The Indices
                FsrbridgePortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgePortTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeUniFdbTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFdbId
                FsrbridgeUniFdbAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgeUniFdbTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeUniFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeFibNickname
                FsrbridgeFibPort
                FsrbridgeFibNextHopRBridge
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgeUniFibTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsrbridgeMultiFibTable
 Input       :  The Indices
                FsrbridgeContextId
                FsrbridgeMultiFibNickname
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsrbridgeMultiFibTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
