#ifndef __RBRGCLIG_C__
#define __RBRGCLIG_C__
/********************************************************************
* Copyright (C) 2011  Aricent Inc . All Rights Reserved
*
* $Id: rbrgclig.c,v 1.6 2014/01/31 13:12:29 siva Exp $
*
* Description: This file contains the Rbrg CLI related routines 
*********************************************************************/

#include "rbrginc.h"
#include "rbrgcli.h"

/****************************************************************************
 * Function    :  cli_process_Rbrg_cmd
 * Description :  This function is exported to CLI module to handle the
                RBRG cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Rbrg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1               MemberPorts[RBRG_MAX_PORTLIST_SIZE];
    UINT4              *args[RBRG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4RowStatus = 0;
    INT4                i4PortsLen = 0;

    tRbrgFsrbridgeGlobalEntry *pRbridgeSetFsrbridgeGlobalEntry = NULL;
    tRbrgIsSetFsrbridgeGlobalEntry *pRbridgeIsSetFsrbridgeGlobalEntry = NULL;
    tRbrgFsrbridgeNicknameEntry *pRbridgeSetFsrbridgeNicknameEntry = NULL;
    tRbrgIsSetFsrbridgeNicknameEntry *pRbridgeIsSetFsrbridgeNicknameEntry =
        NULL;
    tRbrgFsRBridgeBasePortEntry *pRbridgeSetFsRBridgeBasePortEntry = NULL;
    tRbrgIsSetFsRBridgeBasePortEntry *pRbridgeIsSetFsRBridgeBasePortEntry =
        NULL;
    tRbrgFsRbridgeUniFdbEntry *pRbridgeSetFsRbridgeUniFdbEntry = NULL;
    tRbrgIsSetFsRbridgeUniFdbEntry *pRbridgeIsSetFsRbridgeUniFdbEntry = NULL;
    tRbrgFsRbridgeUniFibEntry *pRbridgeSetFsRbridgeUniFibEntry = NULL;
    tRbrgIsSetFsRbridgeUniFibEntry *pRbridgeIsSetFsRbridgeUniFibEntry = NULL;
    tRbrgFsrbridgeMultiFibEntry *pRbridgeSetFsrbridgeMultiFibEntry = NULL;
    tRbrgIsSetFsrbridgeMultiFibEntry *pRbridgeIsSetFsrbridgeMultiFibEntry =
        NULL;
    UNUSED_PARAM (u4CmdType);

    MEMSET (&MemberPorts, 0, RBRG_MAX_PORTLIST_SIZE);

    CliRegisterLock (CliHandle, RbrgMainTaskLock, RbrgMainTaskUnLock);
    RBRG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    else
    {
        u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RBRG_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_RBRG_FSRBRIDGEGLOBALTABLE:
            pRbridgeSetFsrbridgeGlobalEntry =
                (tRbrgFsrbridgeGlobalEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_POOLID);

            if (pRbridgeSetFsrbridgeGlobalEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsrbridgeGlobalEntry, 0,
                    sizeof (tRbrgFsrbridgeGlobalEntry));

            pRbridgeIsSetFsrbridgeGlobalEntry =
                (tRbrgIsSetFsrbridgeGlobalEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsrbridgeGlobalEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                                    (UINT1 *) pRbridgeSetFsrbridgeGlobalEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsrbridgeGlobalEntry, 0,
                    sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

            RBRG_FILL_FSRBRIDGEGLOBALTABLE_ARGS
                ((pRbridgeSetFsrbridgeGlobalEntry),
                 (pRbridgeIsSetFsrbridgeGlobalEntry), args[0], args[1],
                 args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
            i4RetStatus =
                RbrgCliSetFsrbridgeGlobalTable (CliHandle,
                                                (pRbridgeSetFsrbridgeGlobalEntry),
                                                (pRbridgeIsSetFsrbridgeGlobalEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsrbridgeGlobalEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsrbridgeGlobalEntry);
            break;

        case CLI_RBRG_FSRBRIDGENICKNAMETABLE:
            pRbridgeSetFsrbridgeNicknameEntry =
                (tRbrgFsrbridgeNicknameEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_POOLID);

            if (pRbridgeSetFsrbridgeNicknameEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsrbridgeNicknameEntry, 0,
                    sizeof (tRbrgFsrbridgeNicknameEntry));

            pRbridgeIsSetFsrbridgeNicknameEntry =
                (tRbrgIsSetFsrbridgeNicknameEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsrbridgeNicknameEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                                    (UINT1 *)
                                    pRbridgeSetFsrbridgeNicknameEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsrbridgeNicknameEntry, 0,
                    sizeof (tRbrgIsSetFsrbridgeNicknameEntry));

            RBRG_FILL_FSRBRIDGENICKNAMETABLE_ARGS ((pRbridgeSetFsrbridgeNicknameEntry), (pRbridgeIsSetFsrbridgeNicknameEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                RbrgCliSetFsrbridgeNicknameTable (CliHandle,
                                                  (pRbridgeSetFsrbridgeNicknameEntry),
                                                  (pRbridgeIsSetFsrbridgeNicknameEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsrbridgeNicknameEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsrbridgeNicknameEntry);

            break;

        case CLI_RBRG_FSRBRIDGEPORTTABLE:
            pRbridgeSetFsRBridgeBasePortEntry =
                (tRbrgFsRBridgeBasePortEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

            if (pRbridgeSetFsRBridgeBasePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsRBridgeBasePortEntry, 0,
                    sizeof (tRbrgFsRBridgeBasePortEntry));

            pRbridgeIsSetFsRBridgeBasePortEntry =
                (tRbrgIsSetFsRBridgeBasePortEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsRBridgeBasePortEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                    (UINT1 *)
                                    pRbridgeSetFsRBridgeBasePortEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsRBridgeBasePortEntry, 0,
                    sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

            RBRG_FILL_FSRBRIDGEPORTTABLE_ARGS ((pRbridgeSetFsRBridgeBasePortEntry), (pRbridgeIsSetFsRBridgeBasePortEntry), (VOID *) &u4IfIndex, args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                RbrgCliSetFsrbridgePortTable (CliHandle,
                                              (pRbridgeSetFsRBridgeBasePortEntry),
                                              (pRbridgeIsSetFsRBridgeBasePortEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsRBridgeBasePortEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsRBridgeBasePortEntry);

            break;

        case CLI_RBRG_FSRBRIDGEUNIFDBTABLE:
            pRbridgeSetFsRbridgeUniFdbEntry =
                (tRbrgFsRbridgeUniFdbEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID);

            if (pRbridgeSetFsRbridgeUniFdbEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsRbridgeUniFdbEntry, 0,
                    sizeof (tRbrgFsRbridgeUniFdbEntry));

            pRbridgeIsSetFsRbridgeUniFdbEntry =
                (tRbrgIsSetFsRbridgeUniFdbEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsRbridgeUniFdbEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                    (UINT1 *) pRbridgeSetFsRbridgeUniFdbEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsRbridgeUniFdbEntry, 0,
                    sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

            pRbridgeSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
                RBRG_STATIC;
            if (args[6] != NULL)
            {
                i4RowStatus = DESTROY;
            }
            else
            {
                i4RowStatus = CREATE_AND_GO;
            }
            RBRG_FILL_FSRBRIDGEUNIFDBTABLE_ARGS ((pRbridgeSetFsRbridgeUniFdbEntry), (pRbridgeIsSetFsRbridgeUniFdbEntry), args[0], args[1], args[3], args[4], args[5], (VOID *) &i4RowStatus, args[2]);
            i4RetStatus =
                RbrgCliSetFsrbridgeUniFdbTable (CliHandle,
                                                (pRbridgeSetFsRbridgeUniFdbEntry),
                                                (pRbridgeIsSetFsRbridgeUniFdbEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsRbridgeUniFdbEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsRbridgeUniFdbEntry);

            break;

        case CLI_RBRG_FSRBRIDGEUNIFIBTABLE:

            pRbridgeSetFsRbridgeUniFibEntry =
                (tRbrgFsRbridgeUniFibEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID);

            if (pRbridgeSetFsRbridgeUniFibEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsRbridgeUniFibEntry, 0,
                    sizeof (tRbrgFsRbridgeUniFibEntry));

            pRbridgeIsSetFsRbridgeUniFibEntry =
                (tRbrgIsSetFsRbridgeUniFibEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsRbridgeUniFibEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                    (UINT1 *) pRbridgeSetFsRbridgeUniFibEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsRbridgeUniFibEntry, 0,
                    sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

            if (args[8] == NULL)
            {
                i4RowStatus = CREATE_AND_GO;
            }
            else if ((args[6] != NULL) || (args[7] != NULL))
            {
                i4RowStatus = ACTIVE;
            }
            else
            {
                i4RowStatus = DESTROY;
            }
            RBRG_FILL_FSRBRIDGEUNIFIBTABLE_ARGS ((pRbridgeSetFsRbridgeUniFibEntry), (pRbridgeIsSetFsRbridgeUniFibEntry), args[0], args[1], args[2], args[4], args[5], args[6], args[7], (VOID *) &i4RowStatus, args[3]);

            i4RetStatus =
                RbrgCliSetFsrbridgeUniFibTable (CliHandle,
                                                (pRbridgeSetFsRbridgeUniFibEntry),
                                                (pRbridgeIsSetFsRbridgeUniFibEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsRbridgeUniFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsRbridgeUniFibEntry);

            break;

        case CLI_RBRG_FSRBRIDGEMULTIFIBTABLE:
            pRbridgeSetFsrbridgeMultiFibEntry =
                (tRbrgFsrbridgeMultiFibEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID);

            if (pRbridgeSetFsrbridgeMultiFibEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsrbridgeMultiFibEntry, 0,
                    sizeof (tRbrgFsrbridgeMultiFibEntry));

            pRbridgeIsSetFsrbridgeMultiFibEntry =
                (tRbrgIsSetFsrbridgeMultiFibEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsrbridgeMultiFibEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                    (UINT1 *)
                                    pRbridgeSetFsrbridgeMultiFibEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsrbridgeMultiFibEntry, 0,
                    sizeof (tRbrgIsSetFsrbridgeMultiFibEntry));
            if (args[5] != NULL)
            {
                i4RowStatus = DESTROY;
            }
            else
            {
                i4RowStatus = CREATE_AND_GO;
            }

            if (args[2] != NULL)
            {
                i4RetStatus = RbrgCfaCliGetIfList ((INT1 *) args[2],
                                                   (INT1 *) args[3],
                                                   MemberPorts,
                                                   RBRG_MAX_PORTLIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbridgeSetFsrbridgeMultiFibEntry);
                    MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                        (UINT1 *)
                                        pRbridgeIsSetFsrbridgeMultiFibEntry);
                    break;
                }
            }
            i4PortsLen = RBRG_MAX_PORTLIST_SIZE;
            RBRG_FILL_FSRBRIDGEMULTIFIBTABLE_ARGS ((pRbridgeSetFsrbridgeMultiFibEntry), (pRbridgeIsSetFsrbridgeMultiFibEntry), args[0], (VOID *) &MemberPorts, &i4PortsLen, (VOID *) &i4RowStatus, args[1]);

            i4RetStatus =
                RbrgCliSetFsrbridgeMultiFibTable (CliHandle,
                                                  (pRbridgeSetFsrbridgeMultiFibEntry),
                                                  (pRbridgeIsSetFsrbridgeMultiFibEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsrbridgeMultiFibEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsrbridgeMultiFibEntry);

            break;

        case CLI_RBRG_FSRBRIDGEGLOBALTRACE:

            i4RetStatus = RbrgCliSetFsrbridgeGlobalTrace (CliHandle, args[0],
                                                          (UINT1 *) args[1]);
            break;

        case CLI_RBRG_CLEAR_ALL_COUNTER:

            i4RetStatus = RbrgCliClearAllCounter (CliHandle);
            break;

        case CLI_RBRG_CLEAR_PORT_COUNTER:
            pRbridgeSetFsRBridgeBasePortEntry =
                (tRbrgFsRBridgeBasePortEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_POOLID);

            if (pRbridgeSetFsRBridgeBasePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeSetFsRBridgeBasePortEntry, 0,
                    sizeof (tRbrgFsRBridgeBasePortEntry));

            pRbridgeIsSetFsRBridgeBasePortEntry =
                (tRbrgIsSetFsRBridgeBasePortEntry *)
                MemAllocMemBlk (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID);

            if (pRbridgeIsSetFsRBridgeBasePortEntry == NULL)
            {
                MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                    (UINT1 *)
                                    pRbridgeSetFsRBridgeBasePortEntry);

                return SNMP_FAILURE;
            }
            MEMSET (pRbridgeIsSetFsRBridgeBasePortEntry, 0,
                    sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

            RBRG_FILL_FSRBRIDGEPORTTABLE_ARGS ((pRbridgeSetFsRBridgeBasePortEntry), (pRbridgeIsSetFsRBridgeBasePortEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                RbrgCliSetFsrbridgePortTable (CliHandle,
                                              (pRbridgeSetFsRBridgeBasePortEntry),
                                              (pRbridgeIsSetFsRBridgeBasePortEntry));
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_POOLID,
                                (UINT1 *) pRbridgeSetFsRBridgeBasePortEntry);
            MemReleaseMemBlock (RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID,
                                (UINT1 *) pRbridgeIsSetFsRBridgeBasePortEntry);
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RBRG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", RbrgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    RBRG_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeGlobalTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsrbridgeGlobalEntry
*            pRbrgIsSetFsrbridgeGlobalEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeGlobalTable (tCliHandle CliHandle,
                                tRbrgFsrbridgeGlobalEntry *
                                pRbrgSetFsrbridgeGlobalEntry,
                                tRbrgIsSetFsrbridgeGlobalEntry *
                                pRbrgIsSetFsrbridgeGlobalEntry)
{
    UINT4               u4ErrorCode;

    if (RbrgTestAllFsrbridgeGlobalTable
        (&u4ErrorCode, pRbrgSetFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgeGlobalTable
        (pRbrgSetFsrbridgeGlobalEntry,
         pRbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeNicknameTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsrbridgeNicknameEntry
*            pRbrgIsSetFsrbridgeNicknameEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeNicknameTable (tCliHandle CliHandle,
                                  tRbrgFsrbridgeNicknameEntry *
                                  pRbrgSetFsrbridgeNicknameEntry,
                                  tRbrgIsSetFsrbridgeNicknameEntry *
                                  pRbrgIsSetFsrbridgeNicknameEntry)
{
    UINT4               u4ErrorCode;

    if (RbrgTestAllFsrbridgeNicknameTable
        (&u4ErrorCode, pRbrgSetFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgeNicknameTable
        (pRbrgSetFsrbridgeNicknameEntry,
         pRbrgIsSetFsrbridgeNicknameEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgePortTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsRBridgeBasePortEntry
*            pRbrgIsSetFsRBridgeBasePortEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgePortTable (tCliHandle CliHandle,
                              tRbrgFsRBridgeBasePortEntry *
                              pRbrgSetFsRBridgeBasePortEntry,
                              tRbrgIsSetFsRBridgeBasePortEntry *
                              pRbrgIsSetFsRBridgeBasePortEntry)
{
    UINT4               u4ErrorCode;

    if (RbrgTestAllFsrbridgePortTable
        (&u4ErrorCode, pRbrgSetFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgePortTable
        (pRbrgSetFsRBridgeBasePortEntry,
         pRbrgIsSetFsRBridgeBasePortEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeUniFdbTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsRbridgeUniFdbEntry
*            pRbrgIsSetFsRbridgeUniFdbEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeUniFdbTable (tCliHandle CliHandle,
                                tRbrgFsRbridgeUniFdbEntry *
                                pRbrgSetFsRbridgeUniFdbEntry,
                                tRbrgIsSetFsRbridgeUniFdbEntry *
                                pRbrgIsSetFsRbridgeUniFdbEntry)
{
    tRBrgFdbEntry      *pFdbEntry = NULL;
    tRBrgFdbEntry       FdbEntry;
    UINT4               u4ErrorCode;

    MEMSET (&FdbEntry, 0, sizeof (tRBrgFdbEntry));

    /* This is to make row status as active if the entry already 
     * exists */
    if (pRbrgSetFsRbridgeUniFdbEntry->MibObject.
        i4FsrbridgeUniFdbRowStatus == CREATE_AND_GO)
    {
        MEMSET (&FdbEntry, 0, sizeof (tRBrgFdbEntry));
        FdbEntry.u4ContextId =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId;
        FdbEntry.u4RBrgFdbId =
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId;
        MEMCPY (FdbEntry.RBrgFdbMac,
                pRbrgSetFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr,
                MAC_ADDR_LEN);
        pFdbEntry = (tRBrgFdbEntry *) RBTreeGet
            (gRbrgGlobals.RbrgGlbMib.RbrgUniFdbTable, (tRBElem *) & FdbEntry);
        if (pFdbEntry != NULL)
        {
            pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
                ACTIVE;
        }
    }
    if ((pRbrgSetFsRbridgeUniFdbEntry->MibObject.
         i4FsrbridgeUniFdbRowStatus == DESTROY) &&
        (pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence == TRUE))
    {
        pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus =
            ACTIVE;
    }
    pRbrgSetFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbStatus =
        RBRG_FDB_STATIC;
    /* Test the values before setting */
    if (RbrgTestAllFsrbridgeUniFdbTable
        (&u4ErrorCode, pRbrgSetFsRbridgeUniFdbEntry,
         pRbrgIsSetFsRbridgeUniFdbEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgeUniFdbTable
        (pRbrgSetFsRbridgeUniFdbEntry, pRbrgIsSetFsRbridgeUniFdbEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeUniFibTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsRbridgeUniFibEntry
*            pRbrgIsSetFsRbridgeUniFibEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeUniFibTable (tCliHandle CliHandle,
                                tRbrgFsRbridgeUniFibEntry *
                                pRbrgSetFsRbridgeUniFibEntry,
                                tRbrgIsSetFsRbridgeUniFibEntry *
                                pRbrgIsSetFsRbridgeUniFibEntry)
{
    tRBrgFibEntry       FibEntry;
    tRBrgFibEntry      *pFibEntry = NULL;
    UINT4               u4ErrorCode;

    if (pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus ==
        CREATE_AND_GO)
    {
        MEMSET (&FibEntry, 0, sizeof (tRBrgFibEntry));
        FibEntry.u4ContextId =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId;
        FibEntry.u4FibEgressRBrgNickname =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname;
        FibEntry.u4FibNextHopRBrgPortId =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort;
        FibEntry.u4FibNextHopRBrgNickname =
            pRbrgSetFsRbridgeUniFibEntry->MibObject.
            i4FsrbridgeFibNextHopRBridge;
        pFibEntry =
            (tRBrgFibEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.
                                         RbrgUniFibTable,
                                         (tRBElem *) & FibEntry);
        if (pFibEntry != NULL)
        {
            pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus =
                ACTIVE;
        }
    }
    pRbrgSetFsRbridgeUniFibEntry->MibObject.i4FsrbridgeUniFibStatus =
        RBRG_FIB_STATIC;
    if (RbrgTestAllFsrbridgeUniFibTable
        (&u4ErrorCode, pRbrgSetFsRbridgeUniFibEntry,
         pRbrgIsSetFsRbridgeUniFibEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgeUniFibTable
        (pRbrgSetFsRbridgeUniFibEntry, pRbrgIsSetFsRbridgeUniFibEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeMultiFibTable
* Description :
* Input       :  CliHandle 
*            pRbrgSetFsrbridgeMultiFibEntry
*            pRbrgIsSetFsrbridgeMultiFibEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeMultiFibTable (tCliHandle CliHandle,
                                  tRbrgFsrbridgeMultiFibEntry *
                                  pRbrgSetFsrbridgeMultiFibEntry,
                                  tRbrgIsSetFsrbridgeMultiFibEntry *
                                  pRbrgIsSetFsrbridgeMultiFibEntry)
{
    UINT4               u4ErrorCode;

    if (RbrgTestAllFsrbridgeMultiFibTable
        (&u4ErrorCode, pRbrgSetFsrbridgeMultiFibEntry,
         pRbrgIsSetFsrbridgeMultiFibEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetAllFsrbridgeMultiFibTable
        (pRbrgSetFsrbridgeMultiFibEntry, pRbrgIsSetFsrbridgeMultiFibEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliSetFsrbridgeGlobalTrace
* Description :
* Input       :  CliHandle 
*            pRbrgSetScalar
*            pRbrgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliSetFsrbridgeGlobalTrace (tCliHandle CliHandle,
                                UINT4 *pFsrbridgeGlobalTrace, UINT1 *pu1SetFlag)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsrbridgeGlobalTrace = 0;
    UINT4               u4OriginatTrace = 0;

    RBRG_FILL_FSRBRIDGEGLOBALTRACE (u4FsrbridgeGlobalTrace,
                                    pFsrbridgeGlobalTrace);

    RbrgGetFsrbridgeGlobalTrace (&u4OriginatTrace);
    if (pu1SetFlag == NULL)
    {
        u4FsrbridgeGlobalTrace = (u4OriginatTrace | u4FsrbridgeGlobalTrace);
    }
    else
    {
        u4FsrbridgeGlobalTrace = u4OriginatTrace & (~u4FsrbridgeGlobalTrace);
    }

    if (RbrgTestFsrbridgeGlobalTrace (&u4ErrorCode, u4FsrbridgeGlobalTrace) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RbrgSetFsrbridgeGlobalTrace (u4FsrbridgeGlobalTrace) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RbrgCliClearAllCounter
* Description :
* Input       :  CliHandle 
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RbrgCliClearAllCounter (tCliHandle CliHandle)
{
    tRbrgFsrbridgeGlobalEntry RbrgSetFsrbridgeGlobalEntry;
    tRbrgIsSetFsrbridgeGlobalEntry RbrgIsSetFsrbridgeGlobalEntry;
    UINT4               u4ErrorCode;
    UINT4               u4ContextId;
    UINT4               u4NextContextId;
    UINT4               u4ClearAllCounter = TRUE;

    if (nmhGetFirstIndexFsrbridgeGlobalTable (&u4NextContextId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        u4ContextId = u4NextContextId;

        MEMSET (&RbrgSetFsrbridgeGlobalEntry, 0,
                sizeof (tRbrgFsrbridgeGlobalEntry));
        MEMSET (&RbrgIsSetFsrbridgeGlobalEntry, 0,
                sizeof (tRbrgIsSetFsrbridgeGlobalEntry));

        RBRG_FILL_FSRBRIDGEGLOBALTABLE_ARGS ((&RbrgSetFsrbridgeGlobalEntry),
                                             (&RbrgIsSetFsrbridgeGlobalEntry),
                                             (VOID *) &u4ContextId, NULL, NULL,
                                             NULL, NULL, NULL,
                                             NULL, NULL,
                                             (VOID *) &u4ClearAllCounter);

        if (RbrgTestAllFsrbridgeGlobalTable
            (&u4ErrorCode, &RbrgSetFsrbridgeGlobalEntry,
             &RbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (RbrgSetAllFsrbridgeGlobalTable
            (&RbrgSetFsrbridgeGlobalEntry,
             &RbrgIsSetFsrbridgeGlobalEntry) != OSIX_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    while (nmhGetNextIndexFsrbridgeGlobalTable (u4ContextId, &u4NextContextId)
           != SNMP_FAILURE);

    return CLI_SUCCESS;
}
#endif
