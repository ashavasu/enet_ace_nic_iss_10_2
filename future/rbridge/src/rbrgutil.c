/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 * $Id: rbrgutil.c,v 1.3 2012/02/22 12:44:28 siva Exp $
 * Description : This file contains the utility 
 *               functions of the RBRG module
 *****************************************************************************/

#include "rbrginc.h"
#include "rbrgsrc.h"

/******************************************************************************
 * Function   : RbrgUtilFindRbrgStruct
 * Description: 
 * Input      :
 * Output     : 
 * Returns    : Pointer to the tRbrgStruct ,if exists
 *              NULL otherwise
 *****************************************************************************/
/*
PUBLIC tRbrgStruct    *
RbrgUtilFindRbrgStruct ( strcutIndex )
{
}
*/
PUBLIC INT4
RbrgUtilReconfigureFib (tRBrgFibEntry * pRbrgFibEntry,
                        tRbrgFsRBridgeBasePortEntry * pRBrgPortEntry)
{
    tTMO_DLL_NODE      *pFdbNode = NULL;
    tRBrgFdbEntry      *pRbrgFdbEntry = NULL;
    UINT1               u1Index = 0;
    UINT1               u1Status = 0;
    tRBrgNpEntry        RbrgHwInfo;
    tMacAddr            SwitchMac;
    MEMSET (&SwitchMac, 0, sizeof (tMacAddr));

    /* First Delete All FDB Assocatied with this FIB */
    TMO_DLL_Scan (&(pRbrgFibEntry->RBrgFibFdbList), pFdbNode, tTMO_DLL_NODE *)
    {
        pRbrgFdbEntry = GET_RBRG_FDB_PTR_FROM_FIB_LST (pFdbNode);
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
        RbrgHwInfo.u4Command = RBRG_UCAST_DSET_DELETE;
        MEMCPY (RbrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
        RbrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan = pRbrgFdbEntry->u4RBrgFdbId;
        RBRG_IS_MODULE_ENABLE (pRbrgFdbEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) == OSIX_FAILURE)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgUtilReconfigureFib: "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    /* Delete FIB from Hardware */
    if ((pRbrgFibEntry->i4L3EgressId != 0) &&
        (pRbrgFibEntry->u4TrunkTrillPort != 0))
    {
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
        RbrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId = pRbrgFibEntry->i4L3EgressId;
        RbrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id =
            pRBrgPortEntry->i4L3InterfaceIndex;
        RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
        RbrgHwInfo.u4Command = RBRG_FIB_DELETE;
        RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortIfIndex =
            pRbrgFibEntry->u4TrunkTrillPort;
        RBRG_IS_MODULE_ENABLE (pRbrgFibEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Programming Failed \r\n"));
                return OSIX_FAILURE;
            }
            pRbrgFibEntry->i4L3EgressId = 0;
            pRbrgFibEntry->u4TrunkTrillPort = 0;
        }

    }
    /* Recreate FIB from Hardware */
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
    RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex =
        pRbrgFibEntry->u4FibNextHopRBrgPortId;
    RbrgHwInfo.RbrgNpRbrgEntry.u1TrillPortType = RBRG_TRUNK_PORT;
    RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortName =
        pRbrgFibEntry->u4FibEgressRBrgNickname;
    if (pRbrgFibEntry->i1FibStatus == RBRG_FIB_DYNAMIC)
    {
        u1Index = 1;
    }
    else
    {
        u1Index = 0;
    }

    RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortHopCount =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibHopCount;
    RbrgHwInfo.RbrgNpRbrgEntry.u4TrillPortMTU =
        pRbrgFibEntry->pRBrgFibData[u1Index]->u4RBrgFibMtu;

    /* Get SwitchMac from CFA  and copy it to egress_mac */
    RbrgPortCfaGetSysMacAddress (SwitchMac);
    MEMCPY (&RbrgHwInfo.RbrgNpRbrgEntry.egress_mac, &SwitchMac, MAC_ADDR_LEN);
    RbrgHwInfo.RbrgNpRbrgEntry.u4EgressVlan =
        pRBrgPortEntry->MibObject.i4FsrbridgePortDesigVlan;
    MEMCPY (&(RbrgHwInfo.RbrgNpRbrgEntry.next_hop_mac),
            &(pRbrgFibEntry->pRBrgFibData[u1Index]->RBrgFibNextHopMac),
            MAC_ADDR_LEN);

    RbrgHwInfo.u4Command = RBRG_FIB_CREATE;
    RBRG_IS_MODULE_ENABLE (pRbrgFibEntry->u4ContextId, u1Status);
    if (u1Status == TRUE)
    {
        if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
        {
            RBRG_TRC ((RBRG_UTIL_TRC, "Hardware Programming Failed \r\n"));
            return OSIX_FAILURE;
        }
        pRbrgFibEntry->u4TrunkTrillPort =
            RbrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort;
        pRbrgFibEntry->i4L3EgressId = RbrgHwInfo.RbrgNpRbrgEntry.i4L3EgressId;
        pRBrgPortEntry->i4L3InterfaceIndex =
            RbrgHwInfo.RbrgNpRbrgEntry.i4L3_Intf_Id;
    }

    /* Program All FDB Assocatied with this FIB */
    TMO_DLL_Scan (&(pRbrgFibEntry->RBrgFibFdbList), pFdbNode, tTMO_DLL_NODE *)
    {
        pRbrgFdbEntry = GET_RBRG_FDB_PTR_FROM_FIB_LST (pFdbNode);
        MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));
        RbrgHwInfo.RbrgNpRbrgEntry.i4NetworkTrillPort =
            pRbrgFibEntry->u4TrunkTrillPort;
        MEMCPY (&RbrgHwInfo.RbrgNpRbrgEntry.unicast_mac,
                pRbrgFdbEntry->RBrgFdbMac, MAC_ADDR_LEN);
        RbrgHwInfo.RbrgNpRbrgEntry.u4UnicastVlan = pRbrgFdbEntry->u4RBrgFdbId;
        RbrgHwInfo.u4Command = RBRG_CREATE_UCAST_DEST;
        RBRG_IS_MODULE_ENABLE (pRbrgFdbEntry->u4ContextId, u1Status);
        if (u1Status == TRUE)
        {
            if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgUtilReconfigureFib: "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}
PUBLIC INT4
RbrgClearAllPortCounters (UINT4 u4CxtId)
{
    tRbrgFsRBridgeBasePortEntry *pRbrgPortEntry = NULL;
    tRbrgFsRBridgeBasePortEntry *pNextRbrgPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    pNextRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *) RBTreeGetFirst
        (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable);

    while (pNextRbrgPortEntry != NULL)
    {
        pRbrgPortEntry = pNextRbrgPortEntry;

        pNextRbrgPortEntry = (tRbrgFsRBridgeBasePortEntry *) RBTreeGetNext
            (gRbrgGlobals.RbrgGlbMib.FsrbridgePortTable,
             (tRBElem *) pNextRbrgPortEntry, NULL);

        RbrgPortVcmGetCxtInfoFromIfIndex ((UINT4) pRbrgPortEntry->MibObject.
                                          i4FsrbridgePortIfIndex, &u4ContextId,
                                          &u2LocalPortId);
        if (u4ContextId == u4CxtId)
        {
            if (RbrgClearPortCounters (u4ContextId, (UINT4)
                                       pRbrgPortEntry->MibObject.
                                       i4FsrbridgePortIfIndex) == OSIX_FAILURE)
            {
                RBRG_TRC ((ALL_FAILURE_TRC, "RbrgClearPortCounters "
                           "RbrgHwWrProgramEntry returned Failure\r\n"));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}
PUBLIC INT4
RbrgClearPortCounters (UINT4 u4ContextId, UINT4 u4Port)
{
    UINT1               u1Status = 0;
    tRBrgNpEntry        RbrgHwInfo;
    MEMSET (&RbrgHwInfo, 0, sizeof (tRBrgNpEntry));

    RbrgHwInfo.RbrgNpRbrgEntry.u4IfIndex = u4Port;
    RbrgHwInfo.u4Command = RBRG_CLEAR_PORT_STATS;
    RBRG_IS_MODULE_ENABLE (u4ContextId, u1Status);
    if (u1Status == TRUE)
    {
        if (RbrgHwWrProgramEntry (&RbrgHwInfo) != OSIX_SUCCESS)
        {
            RBRG_TRC ((ALL_FAILURE_TRC, "RbrgClearPortCounters: "
                       "RbrgHwWrProgramEntry returned Failure\r\n"));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgutil.c                     */
/*-----------------------------------------------------------------------*/
