/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description : This file contains procedures containing Rbrg Timer
 *               related operations
 *****************************************************************************/
#include "rbrginc.h"

PRIVATE tRbrgTmrDesc gaRbrgTmrDesc[RBRG_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : RbrgTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Rbrg Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgTmrInitTmrDesc ()
{

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTmrInitTmrDesc\n"));

/*
    gaRbrgTmrDesc[RBRG_PMR_TMR].pTmrExpFunc = RbrgTmrPmr;
    gaRbrgTmrDesc[RBRG_PMR_TMR].i2Offset =
        (INT2) (RBRG_GET_OFFSET (tRbrgPmr, pmrTmr));
*/

    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC:EXIT RbrgTmrInitTmrDesc\n"));

}

/****************************************************************************
*                                                                           *
* Function     : RbrgTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Rbrg Timer                                          *
*                                                                           *
* Input        : pRbrgTmr - pointer to RbrgTmr structure                    *
*                eRbrgTmrId - RBRG timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgTmrStartTmr (tRbrgTmr * pRbrgTmr, enRbrgTmrId eRbrgTmrId, UINT4 u4Secs)
{

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTmrStartTmr\n"));

    pRbrgTmr->eRbrgTmrId = eRbrgTmrId;

    if (TmrStartTimer (gRbrgGlobals.rbrgTmrLst, &(pRbrgTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {
        RBRG_TRC ((RBRG_TMR_TRC, "Tmr Start Failure\n"));
    }

    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC: Exit RbrgTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RbrgTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Rbrg Timer                                       *
*                                                                           *
* Input        : pRbrgTmr - pointer to RbrgTmr structure                    *
*                eRbrgTmrId - RBRG timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgTmrRestartTmr (tRbrgTmr * pRbrgTmr, enRbrgTmrId eRbrgTmrId, UINT4 u4Secs)
{

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTmrRestartTmr\n"));

    TmrStopTimer (gRbrgGlobals.rbrgTmrLst, &(pRbrgTmr->tmrNode));
    RbrgTmrStartTmr (pRbrgTmr, eRbrgTmrId, u4Secs);

    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC: Exit RbrgTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RbrgTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Rbrg Timer                                        *
*                                                                           *
* Input        : pRbrgTmr - pointer to RbrgTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
RbrgTmrStopTmr (tRbrgTmr * pRbrgTmr)
{

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC:RbrgTmrStopTmr\n"));

    TmrStopTimer (gRbrgGlobals.rbrgTmrLst, &(pRbrgTmr->tmrNode));

    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC: Exit RbrgTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RbrgTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
RbrgTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enRbrgTmrId         eRbrgTmrId = RBRG_TMR1;
    INT2                i2Offset = 0;

    RBRG_TRC_FUNC ((RBRG_FN_ENTRY, "FUNC: RbrgTmrHandleExpiry\n"));
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gRbrgGlobals.rbrgTmrLst))
           != NULL)
    {

        eRbrgTmrId = ((tRbrgTmr *) pExpiredTimers)->eRbrgTmrId;
        i2Offset = gaRbrgTmrDesc[eRbrgTmrId].i2Offset;

        if (i2Offset < 0)
        {

            /* The timer function does not take any parameter */

            (*(gaRbrgTmrDesc[eRbrgTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaRbrgTmrDesc[eRbrgTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    RBRG_TRC_FUNC ((RBRG_FN_EXIT, "FUNC: Exit RbrgTmrHandleExpiry\n"));
    return;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgtmr.c                      */
/*-----------------------------------------------------------------------*/
