/***********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgnpwr.c,v 1.2 2012/01/27 13:06:25 siva Exp $
*
* Description: This file contains NP wrappers given by R-Bridge protocol 
************************************************************************/

#include "rbrgnp.h"

/****************************************************************************
*
*    FUNCTION NAME    : RbrgHwWrProgramEntry
*
*    DESCRIPTION      : This function is invoked by R-Bridge module to 
*                       call the appropriate hardware functions 
*
*    INPUT            : pRbrgHwInfo  - pointer to tRBrgEntry
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
PUBLIC INT4
RbrgHwWrProgramEntry (tRBrgNpEntry * pRbrgHwInfo)
{
#ifdef NPAPI_WANTED
    if (RbrgNpProgramEntry (pRbrgHwInfo) != FNP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRbrgHwInfo);
#endif
    return OSIX_SUCCESS;
}
