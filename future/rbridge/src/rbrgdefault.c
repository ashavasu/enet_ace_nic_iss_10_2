/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: rbrgdefault.c 
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Rbrg 
*********************************************************************/

#include "rbrginc.h"

/****************************************************************************
* Function    : RbrgInitializeFsrbridgeUniFdbTable
* Input       : pRbrgFsRbridgeUniFdbEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
RbrgInitializeFsrbridgeUniFdbTable (tRbrgFsRbridgeUniFdbEntry *
                                    pRbrgFsRbridgeUniFdbEntry)
{
    if (pRbrgFsRbridgeUniFdbEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((RbrgInitializeMibFsrbridgeUniFdbTable (pRbrgFsRbridgeUniFdbEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : RbrgInitializeFsrbridgeUniFibTable
* Input       : pRbrgFsRbridgeUniFibEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
RbrgInitializeFsrbridgeUniFibTable (tRbrgFsRbridgeUniFibEntry *
                                    pRbrgFsRbridgeUniFibEntry)
{
    if (pRbrgFsRbridgeUniFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((RbrgInitializeMibFsrbridgeUniFibTable (pRbrgFsRbridgeUniFibEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : RbrgInitializeFsrbridgeMultiFibTable
* Input       : pRbrgFsrbridgeMultiFibEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
RbrgInitializeFsrbridgeMultiFibTable (tRbrgFsrbridgeMultiFibEntry *
                                      pRbrgFsrbridgeMultiFibEntry)
{
    if (pRbrgFsrbridgeMultiFibEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((RbrgInitializeMibFsrbridgeMultiFibTable (pRbrgFsrbridgeMultiFibEntry))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
