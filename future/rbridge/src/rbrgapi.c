/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgapi.c,v 1.3 2012/03/05 13:04:50 siva Exp $
*
* Description: This file contains APIs given by R-Bridge protocol 
*********************************************************************/
#include "rbrginc.h"
#include "rbrgcli.h"
/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiEditFdbEntry 
*
*    DESCRIPTION      : This function is invoked by TRILL to give
*                       the dynamically learnt FDB entries to r-bridge database 
*
*    INPUT            : pRbrgFdbData - pointer to FDBEntry
*                       u4ContextId  - Context Id
*                       u4Type       - Add/Update/Delete
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
INT4
RbrgApiEditFdbEntry (tRBrgFdb * pRbrgFdbData, UINT4 u4ContextId, UINT4 u4Type)
{
    tRbrgFsRbridgeUniFdbEntry FsFdbEntry;
    tRbrgIsSetFsRbridgeUniFdbEntry FsIsSetFdbEntry;

    MEMSET (&FsFdbEntry, 0, sizeof (tRbrgFsRbridgeUniFdbEntry));
    MEMSET (&FsIsSetFdbEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFdbEntry));

    FsFdbEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    FsFdbEntry.MibObject.u4FsrbridgeFdbId = pRbrgFdbData->u4FdbId;
    FsIsSetFdbEntry.bFsrbridgeFdbId = TRUE;
    FsFdbEntry.MibObject.i4FsrbridgeUniFdbPort = pRbrgFdbData->u4FdbPort;
    FsIsSetFdbEntry.bFsrbridgeUniFdbPort = TRUE;
    MEMCPY (&(FsFdbEntry.MibObject.FsrbridgeUniFdbAddr),
            pRbrgFdbData->UnicastFdbMac, MAC_ADDR_LEN);
    FsIsSetFdbEntry.bFsrbridgeUniFdbAddr = TRUE;
    FsFdbEntry.MibObject.u4FsrbridgeUniFdbConfidence =
        pRbrgFdbData->u1FdbConfidence;
    FsIsSetFdbEntry.bFsrbridgeUniFdbConfidence = TRUE;
    FsFdbEntry.MibObject.i4FsrbridgeUniFdbNick =
        pRbrgFdbData->u4FdbEgressNickname;
    FsIsSetFdbEntry.bFsrbridgeUniFdbNick = TRUE;
    if (u4Type == RBRG_ADD)
    {
        FsFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus = CREATE_AND_GO;
    }
    else if (u4Type == RBRG_DEL)
    {
        FsFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus = DESTROY;
    }
    else
    {
        FsFdbEntry.MibObject.i4FsrbridgeUniFdbRowStatus = ACTIVE;
    }
    FsIsSetFdbEntry.bFsrbridgeUniFdbRowStatus = TRUE;
    FsFdbEntry.MibObject.i4FsrbridgeUniFdbStatus = RBRG_DYNAMIC;
    if (RbrgUtlSetAllFsrbridgeUniFdbTable (&FsFdbEntry, &FsIsSetFdbEntry,
                                           FALSE, FALSE) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiEditFibEntry 
*
*    DESCRIPTION      : This function is invoked by TRILL to give
*                       the dynamically learnt FIB entries to r-bridge database 
*
*    INPUT            : pRbrgFibData - pointer to FIBEntry
*                       u4ContextId  - Context Id
*                       u4Type       - Add/Update/Delete
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
INT4
RbrgApiEditFibEntry (tRBrgFib * pRbrgFibData, UINT4 u4ContextId, UINT4 u4Type)
{
    tRbrgFsRbridgeUniFibEntry FsFibEntry;
    tRbrgIsSetFsRbridgeUniFibEntry FsIsSetFibEntry;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&FsFibEntry, 0, sizeof (tRbrgFsRbridgeUniFibEntry));
    MEMSET (&FsIsSetFibEntry, 0, sizeof (tRbrgIsSetFsRbridgeUniFibEntry));

    FsFibEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    FsIsSetFibEntry.bFsrbridgeContextId = TRUE;
    FsFibEntry.MibObject.i4FsrbridgeFibNickname =
        pRbrgFibData->u4EgressNickname;
    FsIsSetFibEntry.bFsrbridgeFibNickname = TRUE;
    FsFibEntry.MibObject.i4FsrbridgeFibPort = pRbrgFibData->u4EgressPort;
    FsIsSetFibEntry.bFsrbridgeFibPort = TRUE;
    FsFibEntry.MibObject.i4FsrbridgeFibNextHopRBridge =
        pRbrgFibData->u4NextHopNickname;
    FsIsSetFibEntry.bFsrbridgeFibNextHopRBridge = TRUE;
    if (u4Type == RBRG_DEL)
    {
        FsFibEntry.MibObject.i4FsrbridgeFibRowstatus = DESTROY;
        FsIsSetFibEntry.bFsrbridgeFibRowstatus = TRUE;
        i4RetVal = RbrgUtlSetAllFsrbridgeUniFibTable (&FsFibEntry,
                                                      &FsIsSetFibEntry,
                                                      FALSE, FALSE);
        return i4RetVal;

    }
    FsFibEntry.MibObject.u4FsrbridgeFibMtuDesired = pRbrgFibData->u4Mtu;
    FsIsSetFibEntry.bFsrbridgeFibMtuDesired = TRUE;
    FsFibEntry.MibObject.u4FsrbridgeFibHopCount = pRbrgFibData->u4HopCount;
    FsIsSetFibEntry.bFsrbridgeFibHopCount = TRUE;
    MEMCPY (&FsFibEntry.MibObject.au1FsrbridgeFibMacAddress,
            &(pRbrgFibData->NextHopMac), MAC_ADDR_LEN);
    FsFibEntry.MibObject.i4FsrbridgeFibMacAddressLen = MAC_ADDR_LEN;
    FsIsSetFibEntry.bFsrbridgeFibMacAddress = TRUE;
    if (u4Type == RBRG_ADD)
    {
        FsFibEntry.MibObject.i4FsrbridgeFibRowstatus = CREATE_AND_GO;
    }
    else
    {
        FsFibEntry.MibObject.i4FsrbridgeFibRowstatus = ACTIVE;
    }
    FsIsSetFibEntry.bFsrbridgeFibRowstatus = TRUE;
    i4RetVal = RbrgUtlSetAllFsrbridgeUniFibTable (&FsFibEntry,
                                                  &FsIsSetFibEntry,
                                                  FALSE, FALSE);
    return i4RetVal;
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiEditPortEntry 
*
*    DESCRIPTION      : This function is invoked by TRILL to give
*                       the dynamically learnt Port table entries
*                       to r-bridge database 
*
*    INPUT            : pRBrgConfigPort - pointer to Port entry
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
INT4
RbrgApiEditPortEntry (tRBrgConfigPort * pRBrgConfigPort)
{
    tRbrgFsRBridgeBasePortEntry FsPortEntry;
    tRbrgIsSetFsRBridgeBasePortEntry FsIsSetPortEntry;
    INT4                i4RetVal = 0;

    MEMSET (&FsPortEntry, 0, sizeof (tRbrgFsRBridgeBasePortEntry));
    MEMSET (&FsIsSetPortEntry, 0, sizeof (tRbrgIsSetFsRBridgeBasePortEntry));

    FsPortEntry.MibObject.i4FsrbridgePortIfIndex = pRBrgConfigPort->u4Port;
    FsIsSetPortEntry.bFsrbridgePortIfIndex = TRUE;
    FsPortEntry.MibObject.i4FsrbridgePortDisable =
        pRBrgConfigPort->bPortDisable;
    FsIsSetPortEntry.bFsrbridgePortDisable = TRUE;
    FsPortEntry.MibObject.i4FsrbridgePortTrunkPort =
        pRBrgConfigPort->bTrunkPort;
    FsIsSetPortEntry.bFsrbridgePortTrunkPort = TRUE;
    FsPortEntry.MibObject.i4FsrbridgePortAccessPort =
        pRBrgConfigPort->bAccessPort;
    FsIsSetPortEntry.bFsrbridgePortAccessPort = TRUE;
    FsPortEntry.MibObject.i4FsrbridgePortDisableLearning =
        pRBrgConfigPort->bDisableMacLearning;
    FsIsSetPortEntry.bFsrbridgePortDisableLearning = TRUE;
    FsPortEntry.MibObject.i4FsrbridgePortDesigVlan =
        pRBrgConfigPort->u4DesigVlan;
    FsIsSetPortEntry.bFsrbridgePortDesigVlan = TRUE;
    i4RetVal = RbrgSetAllFsrbridgePortTable (&FsPortEntry, &FsIsSetPortEntry);
    return i4RetVal;

}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiEditPortEntry 
*
*    DESCRIPTION      : This function is invoked by TRILL to give
*                       the dynamically learnt entries
*                       to r-bridge database 
*
*    INPUT            : pRBrgInfo - pointer to RBrgConfig
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
PUBLIC INT4
RbrgApiConfigRbridge (tRBrgConfig * pRBrgInfo)
{

    if (pRBrgInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    switch (pRBrgInfo->u4Command)
    {
        case RBRG_ADD_FDB:
            if (RbrgApiEditFdbEntry (pRBrgInfo->pRBrgConfigFdb,
                                     pRBrgInfo->u4ContextId, RBRG_ADD) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_DEL_FDB:
            if (RbrgApiEditFdbEntry (pRBrgInfo->pRBrgConfigFdb,
                                     pRBrgInfo->u4ContextId, RBRG_DEL)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_UPDATE_FDB:
            if (RbrgApiEditFdbEntry (pRBrgInfo->pRBrgConfigFdb,
                                     pRBrgInfo->u4ContextId, RBRG_UPDATE)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_ADD_FIB:
            if (RbrgApiEditFibEntry (pRBrgInfo->pRBrgConfigFib,
                                     pRBrgInfo->u4ContextId, RBRG_ADD) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_DEL_FIB:
            if (RbrgApiEditFibEntry (pRBrgInfo->pRBrgConfigFib,
                                     pRBrgInfo->u4ContextId, RBRG_DEL)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_UPDATE_FIB:
            if (RbrgApiEditFibEntry (pRBrgInfo->pRBrgConfigFib,
                                     pRBrgInfo->u4ContextId, RBRG_UPDATE)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        case RBRG_EDIT_PORT:
            if (RbrgApiEditPortEntry (pRBrgInfo->pRBrgConfigPort) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            break;
        default:
            break;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiContextRequest 
*
*    DESCRIPTION      : This function is invoked by L2IWF to posts messages 
*                       to RBRG task for the following events,
*                       1. Creation of context
*                       2. Deletion of context 
*
*    INPUT            : u4ContextId - ComponentId
*                       u1Request - event from l2iwf
*                       1. For context creation - RBRG_CREATE_CONTEXT
*                       2. For context deletion - RBRG_DELETE_CONTEXT
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
PUBLIC INT4
RbrgApiContextRequest (UINT4 u4ContextId, UINT1 u1Request)
{

    tRbrgQMsg          *pMsg = NULL;

    if (gbIsRbrgInitialised != TRUE)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiContextRequest: " "Module not initialised\r\n"));
        return OSIX_SUCCESS;
    }
    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tRbrgQMsg *) MemAllocMemBlk (RBRG_QUEUE_MSG_POOLID)) == NULL)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiContextRequest: Context creation Msg "
                   "ALLOC_MEM_BLOCK "
                   " FAILED for ContextId %d \r\n", u4ContextId));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tRbrgQMsg));

    pMsg->u4ContextId = u4ContextId;

    switch (u1Request)
    {
        case RBRG_CREATE_CONTEXT:
            pMsg->u4MsgType = RBRG_CREATE_CONTEXT;
            break;
        case RBRG_DELETE_CONTEXT:
            pMsg->u4MsgType = RBRG_DELETE_CONTEXT;
            break;
        default:
            MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pMsg);
            RBRG_TRC ((ALL_FAILURE_TRC,
                       "RbrgApiContextRequest: "
                       "Invalid ContextId %d request received\r\n",
                       u4ContextId));
            return OSIX_FAILURE;
    }

    return (RbrgQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiPortRequest 
*
*    DESCRIPTION      : This function is called by L2IWF module to post message
*                       to RBRG module to indicate the following events, 
*                       1. Creation of a port
*                       2. Deletion of a port
*                       3. Mapping a port to a context 
*                       4. UnMapping a port from a context
*
*    INPUT            : u4ContextId - ComponentId
*                       u4PortId    - Port Id
*                       u1Request - event from l2iwf
*                          For port creation - RBRG_CREATE_PORT
*                          For port deletion - RBRG_DELETE_PORT
*                          For port mapping  - RBRG_MAP_PORT
*                          For port unmapping- RBRG_UNMAP_PORT
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
RbrgApiPortRequest (UINT4 u4ContextId, UINT4 u4PortId, UINT1 u1Request)
{
    tRbrgFsrbridgeGlobalEntry FsGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pFsGlobalEntry = NULL;
    tRbrgQMsg          *pMsg = NULL;

    MEMSET (&FsGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    if (gbIsRbrgInitialised != TRUE)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortRequest: " "Module not initialised\r\n"));
        return OSIX_SUCCESS;
    }
    FsGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pFsGlobalEntry = (tRbrgFsrbridgeGlobalEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
         (tRBElem *) & FsGlobalEntry);

    /*check for the RBRG module start or shutdown */
    if ((pFsGlobalEntry == NULL) ||
        ((pFsGlobalEntry != NULL) &&
         (pFsGlobalEntry->MibObject.i4FsrbridgeSystemControl == RBRG_SHUT)))
    {
        RBRG_TRC ((MGMT_TRC,
                   "RbrgApiPortRequest: RBridge is shut on this context %d\r\n",
                   u4ContextId));
        return OSIX_SUCCESS;
    }

    if (u4PortId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortRequest invoked with invalid port"
                   " %d \r\n", u4PortId));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tRbrgQMsg *) MemAllocMemBlk (RBRG_QUEUE_MSG_POOLID)) == NULL)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortRequest: ContextId %d Port %d"
                   " Msg ALLOC_MEM_BLOCK FAILED\r\n", u4ContextId, u4PortId));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tRbrgQMsg));

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4PortId = u4PortId;

    switch (u1Request)
    {
        case RBRG_CREATE_PORT:
            pMsg->u4MsgType = RBRG_CREATE_PORT;
            break;
        case RBRG_DELETE_PORT:
            pMsg->u4MsgType = RBRG_DELETE_PORT;
            break;
        case RBRG_MAP_PORT:
            pMsg->u4MsgType = RBRG_MAP_PORT;
            break;
        case RBRG_UNMAP_PORT:
            pMsg->u4MsgType = RBRG_UNMAP_PORT;
            break;
        default:
            MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pMsg);
            RBRG_TRC ((ALL_FAILURE_TRC,
                       "RbrgApiPortRequest: ContextId %d  Port %d "
                       "Invalid request %d received\r\n",
                       u4ContextId, u4PortId, u1Request));
            return OSIX_FAILURE;
    }

    return (RbrgQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : RbrgApiPortToPortChannelRequest
*
*    DESCRIPTION      : This function is called by L2IWF module to post message
*                       to RBRG module to indicate the following events.
*                       1. Port becoming member of LAG
*                       2. Port removed from a LAG
*
*    INPUT            : u4ContextId - ComponentId
*                       u4PortId - Port Id
*                       u4PortChId - LAG Id
*                       u1Request - event from l2iwf  
*                          1. Member port addition - RBRG_ADD_LA_MEM_MSG
*                          2. Member port deletion - RBRG_REM_LA_MEM_MSG
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
RbrgApiPortToPortChannelRequest (UINT4 u4ContextId, UINT4 u4PortId,
                                 UINT4 u4PortChId, UINT1 u1Request)
{
    tRbrgFsrbridgeGlobalEntry FsGlobalEntry;
    tRbrgFsrbridgeGlobalEntry *pFsGlobalEntry = NULL;
    tRbrgQMsg          *pMsg = NULL;

    MEMSET (&FsGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));

    if (gbIsRbrgInitialised != TRUE)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortToPortChannelRequest: "
                   "Module not initialised\r\n"));
        return OSIX_SUCCESS;
    }

    FsGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;
    pFsGlobalEntry = (tRbrgFsrbridgeGlobalEntry *) RBTreeGet
        (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable,
         (tRBElem *) & FsGlobalEntry);

    /*check for the RBRG module start or shutdown */
    if ((pFsGlobalEntry == NULL) ||
        ((pFsGlobalEntry != NULL) &&
         (pFsGlobalEntry->MibObject.i4FsrbridgeSystemControl == RBRG_SHUT)))
    {
        RBRG_TRC ((MGMT_TRC,
                   "RbrgApiPortRequest: RBridge is shut on this context %d\r\n",
                   u4ContextId));
        return OSIX_SUCCESS;
    }

    if (u4PortId >
        FsRBRGSizingParams[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID].
        u4PreAllocatedUnits)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortToPortChannelRequest invoked for "
                   "invalid port %d \r\n", u4PortId));
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tRbrgQMsg *) MemAllocMemBlk (RBRG_QUEUE_MSG_POOLID)) == NULL)
    {
        RBRG_TRC ((ALL_FAILURE_TRC,
                   "RbrgApiPortToPortChannelRequest: ContextId %d Port %d"
                   " ALLOC_MEM_BLOCK FAILED\r\n", u4ContextId, u4PortId));
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tRbrgQMsg));

    pMsg->u4PortId = u4PortId;
    pMsg->u4ContextId = u4ContextId;
    pMsg->u4PoIndex = u4PortChId;

    switch (u1Request)
    {
        case RBRG_ADD_LAG_PORT:
            pMsg->u4MsgType = RBRG_ADD_LAG_PORT;
            break;
        case RBRG_REM_LAG_PORT:
            pMsg->u4MsgType = RBRG_REM_LAG_PORT;
            break;
        default:
            MemReleaseMemBlock (RBRG_QUEUE_MSG_POOLID, (UINT1 *) pMsg);
            RBRG_TRC ((ALL_FAILURE_TRC,
                       "RbrgApiPortToPortChannelRequest : ContextId %d Port %d"
                       " Invalid request %d received\r\n",
                       u4ContextId, u4PortId, u1Request));
            return OSIX_FAILURE;
    }

    return (RbrgQueEnqMsg (pMsg));
}
