/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains declaration of global variables 
 *              of rbrg module.
 *******************************************************************/

#ifndef __RBRGGLOBAL_H__
#define __RBRGGLOBAL_H__
#ifdef __RBRGMAIN_C__
tRbrgGlobals gRbrgGlobals;

BOOL1       gbIsRbrgInitialised;
#else
extern tRbrgGlobals gRbrgGlobals;
extern BOOL1       gbIsRbrgInitialised;
#endif
#endif  /* __RBRGGLOBAL_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file rbrgglob.h                      */
/*-----------------------------------------------------------------------*/
