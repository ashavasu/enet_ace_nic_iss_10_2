/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgwrg.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: This file contains the prototype(wr) for Rbrg 
*********************************************************************/
#ifndef _RBRGWR_H
#define _RBRGWR_H


INT4 GetNextIndexFsrbridgeGlobalTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgeNicknameTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgePortTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgeUniFdbTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgeUniFibTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgeMultiFibTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsrbridgePortCounterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsrbridgeGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeTrillVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNumPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniMultipathEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiMultipathEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUnicastMultipathCountGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMulticastMultipathCountGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknamePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameDtrPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortTrunkPortGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortAccessPortGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableLearningGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDesigVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortMacGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbNickGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbConfidenceGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMtuDesiredGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibHopCountGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibRowstatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortRpfChecksFailedGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortHopCountsExceededGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortOptionsGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortTrillInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortTrillOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniMultipathEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiMultipathEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUnicastMultipathCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMulticastMultipathCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknamePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameDtrPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortTrunkPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortAccessPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableLearningTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDesigVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbNickTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbConfidenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMacAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMtuDesiredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibHopCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibRowstatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsrbridgeGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniMultipathEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiMultipathEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUnicastMultipathCountSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMulticastMultipathCountSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknamePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameDtrPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeNicknameStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortTrunkPortSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortAccessPortSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDisableLearningSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortDesigVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgePortClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbPortSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbNickSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbConfidenceSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeUniFdbRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMacAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibMtuDesiredSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibHopCountSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeFibRowstatusSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeMultiFibRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsrbridgeGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsrbridgeGlobalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsrbridgeNicknameTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsrbridgePortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsrbridgeUniFdbTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsrbridgeUniFibTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsrbridgeMultiFibTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterFSRBRI PROTO ((VOID));
VOID  UnRegisterFSRBRI PROTO ((VOID));

#endif /* _RBRGWR_H */
