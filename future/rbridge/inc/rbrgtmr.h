/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains definitions for rbrg Timer
 *******************************************************************/

#ifndef __RBRGTMR_H__
#define __RBRGTMR_H__



/* constants for timer types */
typedef enum {
	RBRG_TMR1 =0,
	RBRG_MAX_TMRS = 1 
} enRbrgTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _RBRG_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tRbrgTmrDesc;

typedef struct _RBRG_TIMER {
       tTmrAppTimer    tmrNode;
       enRbrgTmrId     eRbrgTmrId;
} tRbrgTmr;




#endif  /* __RBRGTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgtmr.h                      */
/*-----------------------------------------------------------------------*/
