/********************************************************************
* Copyright (C) 20112006 Aricent Inc . All Rights Reserved
*
* $Id: rbrglwg.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsrbridgeGlobalTable. */
INT1
nmhValidateIndexInstanceFsrbridgeGlobalTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsrbridgeNicknameTable. */
INT1
nmhValidateIndexInstanceFsrbridgeNicknameTable ARG_LIST((UINT4  , INT4 ));

/* Proto Validate Index Instance for FsrbridgePortTable. */
INT1
nmhValidateIndexInstanceFsrbridgePortTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsrbridgeUniFdbTable. */
INT1
nmhValidateIndexInstanceFsrbridgeUniFdbTable ARG_LIST((UINT4  , UINT4  , tMacAddr ));

/* Proto Validate Index Instance for FsrbridgeUniFibTable. */
INT1
nmhValidateIndexInstanceFsrbridgeUniFibTable ARG_LIST((UINT4  , INT4  , INT4  , INT4 ));

/* Proto Validate Index Instance for FsrbridgeMultiFibTable. */
INT1
nmhValidateIndexInstanceFsrbridgeMultiFibTable ARG_LIST((UINT4  , INT4 ));

/* Proto Validate Index Instance for FsrbridgePortCounterTable. */
INT1
nmhValidateIndexInstanceFsrbridgePortCounterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsrbridgeGlobalTable  */

INT1
nmhGetFirstIndexFsrbridgeGlobalTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsrbridgeNicknameTable  */

INT1
nmhGetFirstIndexFsrbridgeNicknameTable ARG_LIST((UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsrbridgePortTable  */

INT1
nmhGetFirstIndexFsrbridgePortTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsrbridgeUniFdbTable  */

INT1
nmhGetFirstIndexFsrbridgeUniFdbTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr * ));

/* Proto Type for Low Level GET FIRST fn for FsrbridgeUniFibTable  */

INT1
nmhGetFirstIndexFsrbridgeUniFibTable ARG_LIST((UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsrbridgeMultiFibTable  */

INT1
nmhGetFirstIndexFsrbridgeMultiFibTable ARG_LIST((UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsrbridgePortCounterTable  */

INT1
nmhGetFirstIndexFsrbridgePortCounterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgeGlobalTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgeNicknameTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgeUniFdbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgeUniFibTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgeMultiFibTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrbridgePortCounterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeGlobalTrace ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeTrillVersion ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeNumPorts ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniMultipathEnable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeMultiMultipathEnable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeNicknameNumber ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeSystemControl ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeModuleStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUnicastMultipathCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeMulticastMultipathCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeClearCounters ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeNicknamePriority ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeNicknameDtrPriority ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeNicknameStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortDisable ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortTrunkPort ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortAccessPort ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortState ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortDisableLearning ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortDesigVlan ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortClearCounters ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortMac ARG_LIST((INT4 ,tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniFdbPort ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniFdbNick ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniFdbConfidence ARG_LIST((UINT4  , UINT4  , tMacAddr ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniFdbStatus ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeUniFdbRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeFibMacAddress ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeFibMtuDesired ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeFibHopCount ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeFibStatus ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeFibRowstatus ARG_LIST((UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeMultiFibPorts ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeMultiFibStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgeMultiFibRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortRpfChecksFailed ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortHopCountsExceeded ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortOptions ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortTrillInFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrbridgePortTrillOutFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeGlobalTrace ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUniMultipathEnable ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeMultiMultipathEnable ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeNicknameNumber ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeSystemControl ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeModuleStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUnicastMultipathCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeMulticastMultipathCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeClearCounters ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeNicknamePriority ARG_LIST((UINT4  , INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeNicknameDtrPriority ARG_LIST((UINT4  , INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeNicknameStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortDisable ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortTrunkPort ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortAccessPort ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortDisableLearning ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortDesigVlan ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgePortClearCounters ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUniFdbPort ARG_LIST((UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUniFdbNick ARG_LIST((UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUniFdbConfidence ARG_LIST((UINT4  , UINT4  , tMacAddr  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeUniFdbRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeFibMacAddress ARG_LIST((UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeFibMtuDesired ARG_LIST((UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeFibHopCount ARG_LIST((UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeFibRowstatus ARG_LIST((UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeMultiFibPorts ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrbridgeMultiFibRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeGlobalTrace ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUniMultipathEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeMultiMultipathEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeNicknameNumber ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeSystemControl ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeModuleStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUnicastMultipathCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeMulticastMultipathCount ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeClearCounters ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeNicknamePriority ARG_LIST((UINT4 * , UINT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeNicknameDtrPriority ARG_LIST((UINT4 * , UINT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeNicknameStatus ARG_LIST((UINT4 * , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortDisable ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortTrunkPort ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortAccessPort ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortDisableLearning ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortDesigVlan ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgePortClearCounters ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUniFdbPort ARG_LIST((UINT4 * , UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUniFdbNick ARG_LIST((UINT4 * , UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUniFdbConfidence ARG_LIST((UINT4 * , UINT4  , UINT4  , tMacAddr  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeUniFdbRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeFibMacAddress ARG_LIST((UINT4 * , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeFibMtuDesired ARG_LIST((UINT4 * , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeFibHopCount ARG_LIST((UINT4 * , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeFibRowstatus ARG_LIST((UINT4 * , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeMultiFibPorts ARG_LIST((UINT4 * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsrbridgeMultiFibRowStatus ARG_LIST((UINT4 * , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeNicknameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeUniFdbTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeUniFibTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsrbridgeMultiFibTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
