/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgsrcdefn.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: This file contains the default values for Rbrg module.
*********************************************************************/


#ifndef __RBRGSRCDEFN_H__
#define __RBRGSRCDEFN_H__

/*********** macros for scalar objects*********************/

#define RBRG_DEF_FSRBRIDGEGLOBALTRACE             0

/*********** macros for tabular objects*********************/

#define RBRG_DEF_FSRBRIDGEUNIMULTIPATHENABLE             FALSE
#define RBRG_DEF_FSRBRIDGEMULTIMULTIPATHENABLE           FALSE
#define RBRG_DEF_FSRBRIDGENICKNAMENUMBER             1
#define RBRG_DEF_FSRBRIDGESYSTEMCONTROL             1
#define RBRG_DEF_FSRBRIDGEMODULESTATUS             2
#define RBRG_DEF_FSRBRIDGECLEARCOUNTERS             2
#define RBRG_DEF_FSRBRIDGENICKNAMEPRIORITY             192
#define RBRG_DEF_FSRBRIDGENICKNAMEDTRPRIORITY             32768
#define RBRG_DEF_FSRBRIDGENICKNAMESTATUS             1
#define RBRG_DEF_FSRBRIDGEPORTDISABLE             2
#define RBRG_DEF_FSRBRIDGEPORTTRUNKPORT             2
#define RBRG_DEF_FSRBRIDGEPORTACCESSPORT             2
#define RBRG_DEF_FSRBRIDGEPORTDISABLELEARNING             2
#define RBRG_DEF_FSRBRIDGEPORTDESIGVLAN             1
#define RBRG_DEF_FSRBRIDGEPORTCLEARCOUNTERS             2
#define RBRG_DEF_FSRBRIDGEUNIFDBPORT             0
#define RBRG_DEF_FSRBRIDGEUNIFDBNICK             0
#define RBRG_DEF_FSRBRIDGEUNIFDBCONFIDENCE       0
#define RBRG_DEF_FSRBRIDGEUNIFDBROWSTATUS        0
#define RBRG_DEF_FSRBRIDGEFIBMACADDRESS               "NULL"
#define RBRG_DEF_FSRBRIDGEFIBMACADDRESS_LEN               4
#define RBRG_DEF_FSRBRIDGEFIBMTUDESIRED             1500
#define RBRG_DEF_FSRBRIDGEFIBHOPCOUNT             10
#define RBRG_DEF_FSRBRIDGEFIBROWSTATUS             0
#define RBRG_DEF_FSRBRIDGEMULTIFIBPORTS            32
#define RBRG_DEF_FSRBRIDGEMULTIFIBPORTS_LEN               4
#define RBRG_DEF_FSRBRIDGEMULTIFIBROWSTATUS            0

#endif  /* __RBRGDEFN_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file rbrgdefn.h                      */
/*-----------------------------------------------------------------------*/

