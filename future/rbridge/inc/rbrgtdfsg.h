/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgtdfsg.h,v 1.3 2012/02/07 12:13:56 siva Exp $
*
* Description: This file contains data structures defined for Rbrg module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsrbridgeGlobalEntry */

typedef struct
{
 BOOL1  bFsrbridgeContextId;
 BOOL1  bFsrbridgeUniMultipathEnable;
 BOOL1  bFsrbridgeMultiMultipathEnable;
 BOOL1  bFsrbridgeNicknameNumber;
 BOOL1  bFsrbridgeSystemControl;
 BOOL1  bFsrbridgeModuleStatus;
 BOOL1  bFsrbridgeClearCounters;
 BOOL1  bFsrbridgeUnicastMultipathCount;
 BOOL1  bFsrbridgeMulticastMultipathCount;
} tRbrgIsSetFsrbridgeGlobalEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsrbridgeNicknameEntry */

typedef struct
{
 BOOL1  bFsrbridgeNicknameName;
 BOOL1  bFsrbridgeNicknamePriority;
 BOOL1  bFsrbridgeNicknameDtrPriority;
 BOOL1  bFsrbridgeNicknameStatus;
 BOOL1  bFsrbridgeContextId;
} tRbrgIsSetFsrbridgeNicknameEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsRBridgeBasePortEntry */

typedef struct
{
 BOOL1  bFsrbridgePortIfIndex;
 BOOL1  bFsrbridgePortDisable;
 BOOL1  bFsrbridgePortTrunkPort;
 BOOL1  bFsrbridgePortAccessPort;
 BOOL1  bFsrbridgePortDisableLearning;
 BOOL1  bFsrbridgePortDesigVlan;
 BOOL1  bFsrbridgePortClearCounters;
} tRbrgIsSetFsRBridgeBasePortEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsRbridgeUniFdbEntry */

typedef struct
{
 BOOL1  bFsrbridgeFdbId;
 BOOL1  bFsrbridgeUniFdbAddr;
 BOOL1  bFsrbridgeUniFdbPort;
 BOOL1  bFsrbridgeUniFdbNick;
 BOOL1  bFsrbridgeUniFdbConfidence;
 BOOL1  bFsrbridgeUniFdbRowStatus;
 BOOL1  bFsrbridgeContextId;
} tRbrgIsSetFsRbridgeUniFdbEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsRbridgeUniFibEntry */

typedef struct
{
 BOOL1  bFsrbridgeFibNickname;
 BOOL1  bFsrbridgeFibPort;
 BOOL1  bFsrbridgeFibNextHopRBridge;
 BOOL1  bFsrbridgeFibMacAddress;
 BOOL1  bFsrbridgeFibMtuDesired;
 BOOL1  bFsrbridgeFibHopCount;
 BOOL1  bFsrbridgeFibRowstatus;
 BOOL1  bFsrbridgeContextId;
} tRbrgIsSetFsRbridgeUniFibEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsrbridgeMultiFibEntry */

typedef struct
{
 BOOL1  bFsrbridgeMultiFibNickname;
 BOOL1  bFsrbridgeMultiFibPorts;
 BOOL1  bFsrbridgeMultiFibRowStatus;
 BOOL1  bFsrbridgeContextId;
} tRbrgIsSetFsrbridgeMultiFibEntry;



/* Structure used by Rbrg protocol for FsrbridgeGlobalEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgeGlobalTableNode;
 UINT4 u4FsrbridgeContextId;
 UINT4 u4FsrbridgeTrillVersion;
 UINT4 u4FsrbridgeNumPorts;
 UINT4 u4FsrbridgeNicknameNumber;
 INT4 i4FsrbridgeUniMultipathEnable;
 INT4 i4FsrbridgeMultiMultipathEnable;
 INT4 i4FsrbridgeSystemControl;
 INT4 i4FsrbridgeModuleStatus;
 INT4 i4FsrbridgeClearCounters;
 UINT4 u4FsrbridgeUnicastMultipathCount;
 UINT4 u4FsrbridgeMulticastMultipathCount;
} tRbrgMibFsrbridgeGlobalEntry;

/* Structure used by Rbrg protocol for FsrbridgeNicknameEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgeNicknameTableNode;
 UINT4 u4FsrbridgeNicknamePriority;
 UINT4 u4FsrbridgeNicknameDtrPriority;
 UINT4 u4FsrbridgeContextId;
 INT4 i4FsrbridgeNicknameName;
 INT4 i4FsrbridgeNicknameStatus;
} tRbrgMibFsrbridgeNicknameEntry;

/* Structure used by Rbrg protocol for FsRBridgeBasePortEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgePortTableNode;
 tMacAddr FsrbridgePortMac;
 UINT1 au1Pad[2];
 INT4 i4FsrbridgePortIfIndex;
 INT4 i4FsrbridgePortDisable;
 INT4 i4FsrbridgePortTrunkPort;
 INT4 i4FsrbridgePortAccessPort;
 INT4 i4FsrbridgePortState;
 INT4 i4FsrbridgePortDisableLearning;
 INT4 i4FsrbridgePortDesigVlan;
 INT4 i4FsrbridgePortClearCounters;
} tRbrgMibFsRBridgeBasePortEntry;

/* Structure used by Rbrg protocol for FsRbridgeUniFdbEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgeUniFdbTableNode;
 tMacAddr FsrbridgeUniFdbAddr;
 UINT1 au1Pad[2];
 UINT4 u4FsrbridgeFdbId;
 UINT4 u4FsrbridgeUniFdbConfidence;
 UINT4 u4FsrbridgeContextId;
 INT4 i4FsrbridgeUniFdbPort;
 INT4 i4FsrbridgeUniFdbNick;
 INT4 i4FsrbridgeUniFdbStatus;
 INT4 i4FsrbridgeUniFdbRowStatus;
} tRbrgMibFsRbridgeUniFdbEntry;

/* Structure used by Rbrg protocol for FsRbridgeUniFibEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgeUniFibTableNode;
 UINT4 u4FsrbridgeFibMtuDesired;
 UINT4 u4FsrbridgeFibHopCount;
 UINT4 u4FsrbridgeContextId;
 INT4 i4FsrbridgeFibNickname;
 INT4 i4FsrbridgeFibPort;
 INT4 i4FsrbridgeFibNextHopRBridge;
 INT4 i4FsrbridgeFibRowstatus;
 INT4 i4FsrbridgeFibMacAddressLen;
 INT4 i4FsrbridgeUniFibStatus;
 UINT1 au1FsrbridgeFibMacAddress[256];
} tRbrgMibFsRbridgeUniFibEntry;

/* Structure used by Rbrg protocol for FsrbridgeMultiFibEntry */

typedef struct
{
 tRBNodeEmbd  FsrbridgeMultiFibTableNode;
 UINT4 u4FsrbridgeContextId;
 INT4 i4FsrbridgeMultiFibNickname;
 INT4 i4FsrbridgeMultiFibStatus;
 INT4 i4FsrbridgeMultiFibRowStatus;
 INT4 i4FsrbridgeMultiFibPortsLen;
 UINT1 au1FsrbridgeMultiFibPorts[256];
} tRbrgMibFsrbridgeMultiFibEntry;

/* Structure used by Rbrg protocol for FsRbridgePortCounterEntry */

typedef struct
{
 UINT4 u4FsrbridgePortRpfChecksFailed;
 UINT4 u4FsrbridgePortHopCountsExceeded;
 UINT4 u4FsrbridgePortOptions;
 tSNMP_COUNTER64_TYPE u8FsrbridgePortTrillInFrames;
 tSNMP_COUNTER64_TYPE u8FsrbridgePortTrillOutFrames;
 INT4 i4FsrbridgePortIfIndex;
} tRbrgMibFsRbridgePortCounterEntry;
