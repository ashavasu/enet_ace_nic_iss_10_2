/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgdefg.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define RBRG_FSRBRIDGEGLOBALTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEGLOBALTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEGLOBALTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEGLOBALTABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGENICKNAMETABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGENICKNAMETABLE_SIZING_ID]


#define RBRG_FSRBRIDGENICKNAMETABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGENICKNAMETABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGEPORTTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEPORTTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEPORTTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEPORTTABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGEUNIFDBTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEUNIFDBTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEUNIFDBTABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGEUNIFIBTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEUNIFIBTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEUNIFIBTABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGEMULTIFIBTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEMULTIFIBTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEMULTIFIBTABLE_ISSET_SIZING_ID]


#define RBRG_FSRBRIDGEPORTCOUNTERTABLE_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEPORTCOUNTERTABLE_SIZING_ID]


#define RBRG_FSRBRIDGEPORTCOUNTERTABLE_ISSET_POOLID   RBRGMemPoolIds[MAX_RBRG_FSRBRIDGEPORTCOUNTERTABLE_ISSET_SIZING_ID]

#define RBRG_FDB_DATA_POOLID   RBRGMemPoolIds[MAX_RBRG_FSFDB_DATA_SIZING_ID]
#define RBRG_FIB_DATA_POOLID   RBRGMemPoolIds[MAX_RBRG_FSFIB_DATA_SIZING_ID]
#define RBRG_FDB_TABLE_POOLID  RBRGMemPoolIds[MAX_RBRG_FSFDB_ENTRY_SIZING_ID]
#define RBRG_FIB_TABLE_POOLID  RBRGMemPoolIds[MAX_RBRG_FSFIB_ENTRY_SIZING_ID]
#define RBRG_QUEUE_MSG_POOLID      RBRGMemPoolIds[MAX_RBRG_QUEUE_SIZING_ID]
#define RBRG_MBSM_POOLID      RBRGMemPoolIds[MAX_RBRG_MBSM_SIZING_ID]
/* Macro used to fill the CLI structure for FsrbridgeGlobalEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEGLOBALTABLE_ARGS(pRbrgFsrbridgeGlobalEntry,\
   pRbrgIsSetFsrbridgeGlobalEntry,\
   pau1FsrbridgeContextId,\
   pau1FsrbridgeUniMultipathEnable,\
   pau1FsrbridgeMultiMultipathEnable,\
   pau1FsrbridgeNicknameNumber,\
   pau1FsrbridgeSystemControl,\
   pau1FsrbridgeModuleStatus,\
          pau1FsrbridgeUnicastMultipathCount,\
   pau1FsrbridgeMulticastMultipathCount,\
   pau1FsrbridgeClearCounters)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeContextId = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniMultipathEnable != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeUniMultipathEnable = *(INT4 *) (pau1FsrbridgeUniMultipathEnable);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUniMultipathEnable = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeMultiMultipathEnable != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeMultiMultipathEnable = *(INT4 *) (pau1FsrbridgeMultiMultipathEnable);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMultiMultipathEnable = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeNicknameNumber != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeNicknameNumber = *(UINT4 *) (pau1FsrbridgeNicknameNumber);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeNicknameNumber = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeSystemControl != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeSystemControl = *(INT4 *) (pau1FsrbridgeSystemControl);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeSystemControl = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeModuleStatus != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeModuleStatus = *(INT4 *) (pau1FsrbridgeModuleStatus);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeModuleStatus = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeClearCounters != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.i4FsrbridgeClearCounters = *(INT4 *) (pau1FsrbridgeClearCounters);\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeClearCounters = OSIX_FALSE;\
  }\
        if (pau1FsrbridgeUnicastMultipathCount != NULL)\
{\
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeUnicastMultipathCount = *(UINT4 *) (pau1FsrbridgeUnicastMultipathCount);\
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount = OSIX_TRUE;\
}\
else\
{\
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeUnicastMultipathCount = OSIX_FALSE;\
}\
if (pau1FsrbridgeMulticastMultipathCount != NULL)\
{\
    pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeMulticastMultipathCount = *(UINT4 *) (pau1FsrbridgeMulticastMultipathCount);\
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount = OSIX_TRUE;\
}\
else\
{\
    pRbrgIsSetFsrbridgeGlobalEntry->bFsrbridgeMulticastMultipathCount = OSIX_FALSE;\
}\
}

/* Macro used to fill the CLI structure for FsrbridgeNicknameEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGENICKNAMETABLE_ARGS(pRbrgFsrbridgeNicknameEntry,\
   pRbrgIsSetFsrbridgeNicknameEntry,\
   pau1FsrbridgeNicknameName,\
   pau1FsrbridgeNicknamePriority,\
   pau1FsrbridgeNicknameDtrPriority,\
   pau1FsrbridgeNicknameStatus,\
   pau1FsrbridgeContextId)\
  {\
  if (pau1FsrbridgeNicknameName != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName = *(INT4 *) (pau1FsrbridgeNicknameName);\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameName = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeNicknamePriority != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknamePriority = *(UINT4 *) (pau1FsrbridgeNicknamePriority);\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknamePriority = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeNicknameDtrPriority != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeNicknameDtrPriority = *(UINT4 *) (pau1FsrbridgeNicknameDtrPriority);\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameDtrPriority = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeNicknameStatus != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameStatus = *(INT4 *) (pau1FsrbridgeNicknameStatus);\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeNicknameStatus = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeNicknameEntry->bFsrbridgeContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsRBridgeBasePortEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEPORTTABLE_ARGS(pRbrgFsRBridgeBasePortEntry,\
   pRbrgIsSetFsRBridgeBasePortEntry,\
   pau1FsrbridgePortIfIndex,\
   pau1FsrbridgePortDisable,\
   pau1FsrbridgePortTrunkPort,\
   pau1FsrbridgePortAccessPort,\
   pau1FsrbridgePortDisableLearning,\
   pau1FsrbridgePortDesigVlan,\
   pau1FsrbridgePortClearCounters)\
  {\
  if (pau1FsrbridgePortIfIndex != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex = *(INT4 *) (pau1FsrbridgePortIfIndex);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortDisable != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisable = *(INT4 *) (pau1FsrbridgePortDisable);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisable = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortTrunkPort != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortTrunkPort = *(INT4 *) (pau1FsrbridgePortTrunkPort);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortTrunkPort = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortAccessPort != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortAccessPort = *(INT4 *) (pau1FsrbridgePortAccessPort);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortAccessPort = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortDisableLearning != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDisableLearning = *(INT4 *) (pau1FsrbridgePortDisableLearning);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDisableLearning = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortDesigVlan != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortDesigVlan = *(INT4 *) (pau1FsrbridgePortDesigVlan);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortDesigVlan = OSIX_FALSE;\
  }\
  if (pau1FsrbridgePortClearCounters != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortClearCounters = *(INT4 *) (pau1FsrbridgePortClearCounters);\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRBridgeBasePortEntry->bFsrbridgePortClearCounters = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsRbridgeUniFdbEntry 
 using the input given in def file */
/*#endif *//* ISS_PB-SAYE-FEB5 */ 

#define  RBRG_FILL_FSRBRIDGEUNIFDBTABLE_ARGS(pRbrgFsRbridgeUniFdbEntry,\
   pRbrgIsSetFsRbridgeUniFdbEntry,\
   pau1FsrbridgeFdbId,\
   pau1FsrbridgeUniFdbAddr,\
   pau1FsrbridgeUniFdbPort,\
   pau1FsrbridgeUniFdbNick,\
   pau1FsrbridgeUniFdbConfidence,\
   pau1FsrbridgeUniFdbRowStatus,\
   pau1FsrbridgeContextId)\
  {\
  if (pau1FsrbridgeFdbId != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = *(UINT4 *) (pau1FsrbridgeFdbId);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeFdbId = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniFdbAddr != NULL)\
  {\
   StrToMac ((UINT1 *)pau1FsrbridgeUniFdbAddr, pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbAddr = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniFdbPort != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbPort = *(INT4 *) (pau1FsrbridgeUniFdbPort);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbPort = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniFdbNick != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbNick = *(INT4 *) (pau1FsrbridgeUniFdbNick);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbNick = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniFdbConfidence != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeUniFdbConfidence = *(UINT4 *) (pau1FsrbridgeUniFdbConfidence);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbConfidence = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeUniFdbRowStatus != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.i4FsrbridgeUniFdbRowStatus = *(INT4 *) (pau1FsrbridgeUniFdbRowStatus);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeUniFdbRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFdbEntry->bFsrbridgeContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsRbridgeUniFibEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEUNIFIBTABLE_ARGS(pRbrgFsRbridgeUniFibEntry,\
   pRbrgIsSetFsRbridgeUniFibEntry,\
   pau1FsrbridgeFibNickname,\
   pau1FsrbridgeFibPort,\
   pau1FsrbridgeFibNextHopRBridge,\
   pau1FsrbridgeFibMacAddress,\
   pau1FsrbridgeFibMacAddressLen,\
   pau1FsrbridgeFibMtuDesired,\
   pau1FsrbridgeFibHopCount,\
   pau1FsrbridgeFibRowstatus,\
   pau1FsrbridgeContextId)\
  {\
  if (pau1FsrbridgeFibNickname != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname = *(INT4 *) (pau1FsrbridgeFibNickname);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNickname = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibPort != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort = *(INT4 *) (pau1FsrbridgeFibPort);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibPort = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibNextHopRBridge != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge = *(INT4 *) (pau1FsrbridgeFibNextHopRBridge);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibNextHopRBridge = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibMacAddress != NULL)\
  {\
   StrToMac ((UINT1 *) pau1FsrbridgeFibMacAddress, (UINT1 *) pRbrgFsRbridgeUniFibEntry->MibObject.au1FsrbridgeFibMacAddress);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress = OSIX_TRUE;\
            pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibMacAddressLen = MAC_ADDR_LEN;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMacAddress = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibMtuDesired != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibMtuDesired = *(UINT4 *) (pau1FsrbridgeFibMtuDesired);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibMtuDesired = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibHopCount != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeFibHopCount = *(UINT4 *) (pau1FsrbridgeFibHopCount);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibHopCount = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeFibRowstatus != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibRowstatus = *(INT4 *) (pau1FsrbridgeFibRowstatus);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeFibRowstatus = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsRbridgeUniFibEntry->bFsrbridgeContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsrbridgeMultiFibEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEMULTIFIBTABLE_ARGS(pRbrgFsrbridgeMultiFibEntry,\
   pRbrgIsSetFsrbridgeMultiFibEntry,\
   pau1FsrbridgeMultiFibNickname,\
   pau1FsrbridgeMultiFibPorts,\
   pau1FsrbridgeMultiFibPortsLen,\
   pau1FsrbridgeMultiFibRowStatus,\
   pau1FsrbridgeContextId)\
  {\
  if (pau1FsrbridgeMultiFibNickname != NULL)\
  {\
   pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname = *(INT4 *) (pau1FsrbridgeMultiFibNickname);\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibNickname = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeMultiFibPorts != NULL)\
  {\
   MEMCPY (pRbrgFsrbridgeMultiFibEntry->MibObject.au1FsrbridgeMultiFibPorts, pau1FsrbridgeMultiFibPorts, *(INT4 *)pau1FsrbridgeMultiFibPortsLen);\
        pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibPortsLen = *(INT4 *)pau1FsrbridgeMultiFibPortsLen;\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibPorts = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeMultiFibRowStatus != NULL)\
  {\
   pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibRowStatus = *(INT4 *) (pau1FsrbridgeMultiFibRowStatus);\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeMultiFibRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pRbrgIsSetFsrbridgeMultiFibEntry->bFsrbridgeContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsrbridgeGlobalEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEGLOBALTABLE_INDEX(pRbrgFsrbridgeGlobalEntry,\
   pau1FsrbridgeContextId)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeGlobalEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsrbridgeNicknameEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGENICKNAMETABLE_INDEX(pRbrgFsrbridgeNicknameEntry,\
   pau1FsrbridgeContextId,\
   pau1FsrbridgeNicknameName)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
  }\
  if (pau1FsrbridgeNicknameName != NULL)\
  {\
   pRbrgFsrbridgeNicknameEntry->MibObject.i4FsrbridgeNicknameName = *(INT4 *) (pau1FsrbridgeNicknameName);\
  }\
 }
/* Macro used to fill the index values in  structure for FsRBridgeBasePortEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEPORTTABLE_INDEX(pRbrgFsRBridgeBasePortEntry,\
   pau1FsrbridgePortIfIndex)\
  {\
  if (pau1FsrbridgePortIfIndex != NULL)\
  {\
   pRbrgFsRBridgeBasePortEntry->MibObject.i4FsrbridgePortIfIndex = *(INT4 *) (pau1FsrbridgePortIfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsRbridgeUniFdbEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEUNIFDBTABLE_INDEX(pRbrgFsRbridgeUniFdbEntry,\
   pau1FsrbridgeContextId,\
   pau1FsrbridgeFdbId,\
   pau1FsrbridgeUniFdbAddr)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
  }\
  if (pau1FsrbridgeFdbId != NULL)\
  {\
   pRbrgFsRbridgeUniFdbEntry->MibObject.u4FsrbridgeFdbId = *(UINT4 *) (pau1FsrbridgeFdbId);\
  }\
  if (pau1FsrbridgeUniFdbAddr != NULL)\
  {\
   StrToMac (pau1FsrbridgeUniFdbAddr, pRbrgFsRbridgeUniFdbEntry->MibObject.FsrbridgeUniFdbAddr);\
  }\
 }
/* Macro used to fill the index values in  structure for FsRbridgeUniFibEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEUNIFIBTABLE_INDEX(pRbrgFsRbridgeUniFibEntry,\
   pau1FsrbridgeContextId,\
   pau1FsrbridgeFibNickname,\
   pau1FsrbridgeFibPort,\
   pau1FsrbridgeFibNextHopRBridge)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
  }\
  if (pau1FsrbridgeFibNickname != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNickname = *(INT4 *) (pau1FsrbridgeFibNickname);\
  }\
  if (pau1FsrbridgeFibPort != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibPort = *(INT4 *) (pau1FsrbridgeFibPort);\
  }\
  if (pau1FsrbridgeFibNextHopRBridge != NULL)\
  {\
   pRbrgFsRbridgeUniFibEntry->MibObject.i4FsrbridgeFibNextHopRBridge = *(INT4 *) (pau1FsrbridgeFibNextHopRBridge);\
  }\
 }
/* Macro used to fill the index values in  structure for FsrbridgeMultiFibEntry 
 using the input given in def file */

#define  RBRG_FILL_FSRBRIDGEMULTIFIBTABLE_INDEX(pRbrgFsrbridgeMultiFibEntry,\
   pau1FsrbridgeContextId,\
   pau1FsrbridgeMultiFibNickname)\
  {\
  if (pau1FsrbridgeContextId != NULL)\
  {\
   pRbrgFsrbridgeMultiFibEntry->MibObject.u4FsrbridgeContextId = *(UINT4 *) (pau1FsrbridgeContextId);\
  }\
  if (pau1FsrbridgeMultiFibNickname != NULL)\
  {\
   pRbrgFsrbridgeMultiFibEntry->MibObject.i4FsrbridgeMultiFibNickname = *(INT4 *) (pau1FsrbridgeMultiFibNickname);\
  }\
 }
/* Macro used to convert the input 
  to required datatype for FsrbridgeGlobalTrace*/

#define  RBRG_FILL_FSRBRIDGEGLOBALTRACE(u4FsrbridgeGlobalTrace,\
   pau1FsrbridgeGlobalTrace)\
  {\
  if (pau1FsrbridgeGlobalTrace != NULL)\
  {\
   u4FsrbridgeGlobalTrace = *(UINT4 *) (pau1FsrbridgeGlobalTrace);\
  }\
  }


