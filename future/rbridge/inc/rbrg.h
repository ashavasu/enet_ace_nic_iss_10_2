/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: rbrg.h,v 1.2 2012/01/27 13:06:23 siva Exp $
 * Description: This header file contains all the fsrbridge OID with
 *              name mapping used in RBridge Module.
****************************************************************************/
/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
"ccitt",        "0",
"iso",        "1",
"lldpExtensions",        "1.0.8802.1.1.2.1.5",
"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000",
"org",        "1.3",
"dod",        "1.3.6",
"internet",        "1.3.6.1",
"directory",        "1.3.6.1.1",
"mgmt",        "1.3.6.1.2",
"mib-2",        "1.3.6.1.2.1",
"ip",        "1.3.6.1.2.1.4",
"transmission",        "1.3.6.1.2.1.10",
"mplsStdMIB",        "1.3.6.1.2.1.10.166",
"rmon",        "1.3.6.1.2.1.16",
"statistics",        "1.3.6.1.2.1.16.1",
"history",        "1.3.6.1.2.1.16.2",
"hosts",        "1.3.6.1.2.1.16.4",
"matrix",        "1.3.6.1.2.1.16.6",
"filter",        "1.3.6.1.2.1.16.7",
"tokenRing",        "1.3.6.1.2.1.16.10",
"dot1dBridge",        "1.3.6.1.2.1.17",
"dot1dStp",        "1.3.6.1.2.1.17.2",
"dot1dTp",        "1.3.6.1.2.1.17.4",
"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1",
"experimental",        "1.3.6.1.3",
"private",        "1.3.6.1.4",
"enterprises",        "1.3.6.1.4.1",
"issExt",        "1.3.6.1.4.1.2076.81.8",
"fsDot1dBridge",        "1.3.6.1.4.1.2076.116",
"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2",
"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4",
"fsrbridgeMIB",        "1.3.6.1.4.1.29601.2.66",
"fsrbridgeObjects",        "1.3.6.1.4.1.29601.2.66.0",
"fsrbridge",        "1.3.6.1.4.1.29601.2.66.0.1",
"fsrbridgeGlobalTrace",        "1.3.6.1.4.1.29601.2.66.0.1.1",
"fsrbridgeGlobalTable",        "1.3.6.1.4.1.29601.2.66.0.1.2",
"fsrbridgeGlobalEntry",        "1.3.6.1.4.1.29601.2.66.0.1.2.1",
"fsrbridgeContextId",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.1",
"fsrbridgeTrillVersion",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.2",
"fsrbridgeNumPorts",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.3",
"fsrbridgeUniMultipathEnable",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.4",
"fsrbridgeMultiMultipathEnable",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.5",
"fsrbridgeNicknameNumber",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.6",
"fsrbridgeSystemControl",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.7",
"fsrbridgeModuleStatus",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.8",
"fsrbridgeUnicastMultipathCount",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.9",
"fsrbridgeMulticastMultipathCount",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.10",
"fsrbridgeClearCounters",        "1.3.6.1.4.1.29601.2.66.0.1.2.1.11",
"fsrbridgeNicknameTable",        "1.3.6.1.4.1.29601.2.66.0.1.3",
"fsrbridgeNicknameEntry",        "1.3.6.1.4.1.29601.2.66.0.1.3.1",
"fsrbridgeNicknameName",        "1.3.6.1.4.1.29601.2.66.0.1.3.1.1",
"fsrbridgeNicknamePriority",        "1.3.6.1.4.1.29601.2.66.0.1.3.1.2",
"fsrbridgeNicknameDtrPriority",        "1.3.6.1.4.1.29601.2.66.0.1.3.1.3",
"fsrbridgeNicknameStatus",        "1.3.6.1.4.1.29601.2.66.0.1.3.1.4",
"fsrbridgePortTable",        "1.3.6.1.4.1.29601.2.66.0.1.4",
"fsrbridgePortEntry",        "1.3.6.1.4.1.29601.2.66.0.1.4.1",
"fsrbridgePortIfIndex",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.1",
"fsrbridgePortDisable",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.2",
"fsrbridgePortTrunkPort",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.3",
"fsrbridgePortAccessPort",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.4",
"fsrbridgePortState",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.5",
"fsrbridgePortDisableLearning",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.6",
"fsrbridgePortDesigVlan",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.7",
"fsrbridgePortClearCounters",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.8",
"fsrbridgePortMac",        "1.3.6.1.4.1.29601.2.66.0.1.4.1.9",
"fsrbridgeFdb",        "1.3.6.1.4.1.29601.2.66.0.2",
"fsrbridgeUniFdbTable",        "1.3.6.1.4.1.29601.2.66.0.2.1",
"fsrbridgeUniFdbEntry",        "1.3.6.1.4.1.29601.2.66.0.2.1.1",
"fsrbridgeFdbId",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.1",
"fsrbridgeUniFdbAddr",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.2",
"fsrbridgeUniFdbPort",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.3",
"fsrbridgeUniFdbNick",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.4",
"fsrbridgeUniFdbConfidence",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.5",
"fsrbridgeUniFdbStatus",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.6",
"fsrbridgeUniFdbRowStatus",        "1.3.6.1.4.1.29601.2.66.0.2.1.1.7",
"fsrbridgeUniFibTable",        "1.3.6.1.4.1.29601.2.66.0.2.2",
"fsrbridgeUniFibEntry",        "1.3.6.1.4.1.29601.2.66.0.2.2.1",
"fsrbridgeFibNickname",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.1",
"fsrbridgeFibPort",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.2",
"fsrbridgeFibNextHopRBridge",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.3",
"fsrbridgeFibMacAddress",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.4",
"fsrbridgeFibMtuDesired",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.5",
"fsrbridgeFibHopCount",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.6",
"fsrbridgeFibStatus",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.7",
"fsrbridgeFibRowstatus",        "1.3.6.1.4.1.29601.2.66.0.2.2.1.8",
"fsrbridgeMultiFibTable",        "1.3.6.1.4.1.29601.2.66.0.2.4",
"fsrbridgeMultiFibEntry",        "1.3.6.1.4.1.29601.2.66.0.2.4.1",
"fsrbridgeMultiFibNickname",        "1.3.6.1.4.1.29601.2.66.0.2.4.1.1",
"fsrbridgeMultiFibPorts",        "1.3.6.1.4.1.29601.2.66.0.2.4.1.2",
"fsrbridgeMultiFibStatus",        "1.3.6.1.4.1.29601.2.66.0.2.4.1.3",
"fsrbridgeMultiFibRowStatus",        "1.3.6.1.4.1.29601.2.66.0.2.4.1.4",
"fsrbridgeCounter",        "1.3.6.1.4.1.29601.2.66.0.3",
"fsrbridgePortCounterTable",        "1.3.6.1.4.1.29601.2.66.0.3.1",
"fsrbridgePortCounterEntry",        "1.3.6.1.4.1.29601.2.66.0.3.1.1",
"fsrbridgePortRpfChecksFailed",        "1.3.6.1.4.1.29601.2.66.0.3.1.1.1",
"fsrbridgePortHopCountsExceeded",        "1.3.6.1.4.1.29601.2.66.0.3.1.1.2",
"fsrbridgePortOptions",        "1.3.6.1.4.1.29601.2.66.0.3.1.1.3",
"fsrbridgePortTrillInFrames",        "1.3.6.1.4.1.29601.2.66.0.3.1.1.4",
"fsrbridgePortTrillOutFrames",        "1.3.6.1.4.1.29601.2.66.0.3.1.1.5",
"security",        "1.3.6.1.5",
"snmpV2",        "1.3.6.1.6",
"snmpDomains",        "1.3.6.1.6.1",
"snmpProxys",        "1.3.6.1.6.2",
"snmpModules",        "1.3.6.1.6.3",
"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1",
"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2",
"ieee802dot1mibs",        "1.111.2.802.1",
"joint-iso-ccitt",        "2",
0,0
};

#endif /* _SNMP_MIB_H */
