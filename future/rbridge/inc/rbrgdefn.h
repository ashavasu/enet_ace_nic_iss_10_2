/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rbrgdefn.h,v 1.3 2012/03/16 12:34:22 siva Exp $
 * Description: This file contains definitions for rbrg module.
 *******************************************************************/

#ifndef __RBRGDEFN_H__
#define __RBRGDEFN_H__

#define RBRG_SUCCESS        (OSIX_SUCCESS)
#define RBRG_FAILURE        (OSIX_FAILURE)

#define  RBRG_TASK_PRIORITY              100
#define  RBRG_TASK_NAME                  "RBRG"
#define  RBRG_QUEUE_NAME                 (const UINT1 *) "RBRGQ"
#define  RBRG_MUT_EXCL_SEM_NAME          (const UINT1 *) "RBRGM"
#define  RBRG_QUEUE_DEPTH                100
#define  RBRG_SEM_CREATE_INIT_CNT        1


#define  RBRG_ENABLED                    1
#define  RBRG_DISABLED                   2

#define  RBRG_TIMER_EVENT             0x00000001 
#define  RBRG_QMSG_EVENT             0x00000002
#define  RBRG_ALL_EVENTS             0xFFFFFFF
#define  MAX_RBRG_DUMMY                   1


#define RBRG_DEFAULT_CONTEXT 0
#define RBRG_MIN_NICKNAME 1
#define RBRG_MAX_NICKNAME 65535
#define RBRG_MAX_VLAN 4094
#define RBRG_SHUT 2
#define RBRG_START 1
#define RBRG_ENABLE 1
#define RBRG_DISABLE 2
#define RBRG_MIN_CONTEXT 0
#define RBRG_MAX_NICKNAME_COUNT 4
#define RBRG_MAX_PRIORITY 255
#define RBRG_MAX_DTRPRIORITY 65535
#define RBRG_MIN_CONFIDENCE 0
#define RBRG_MAX_CONFIDENCE 254
#define RBRG_TRILL_VERSION 1 
#define RBRG_LESSER (-1)
#define RBRG_EQUAL 0
#define RBRG_GREATER 1

#define RBRG_MAC_STR_LEN 21
#define RBRG_CLI_MAX_PORTS_PER_LINE 6
#define RBRG_IFPORT_LIST_SIZE BRG_PORT_LIST_SIZE
#define RBRG_PORTS_PER_BYTE 8
#define RBRG_BIT8 0x80
#define RBRG_MAX_PORTLIST_SIZE 256
#define RBRG_IS_PORT_CHANNEL(u4Port) \
    (((u4Port > BRG_MAX_PHY_PORTS) && (u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) \
     ? OSIX_TRUE : OSIX_FALSE)
#define RBRG_IS_MODULE_ENABLE(u4ContextId, u1Status)\
{\
    tRbrgFsrbridgeGlobalEntry      RbrgGlobalEntry;\
    tRbrgFsrbridgeGlobalEntry     *pRbrgGlobalEntry = NULL;\
    MEMSET (&RbrgGlobalEntry, 0, sizeof (tRbrgFsrbridgeGlobalEntry));\
    RbrgGlobalEntry.MibObject.u4FsrbridgeContextId = u4ContextId;\
    pRbrgGlobalEntry = (tRbrgFsrbridgeGlobalEntry *) RBTreeGet (gRbrgGlobals.RbrgGlbMib.FsrbridgeGlobalTable, (tRBElem *) &RbrgGlobalEntry);\
    if ((pRbrgGlobalEntry != NULL)&& (pRbrgGlobalEntry->MibObject.i4FsrbridgeModuleStatus == RBRG_ENABLE))\
        u1Status = TRUE;\
        else\
        u1Status = FALSE;\
}
enum
{
    RBRG_ADD = 1,
    RBRG_DEL,
    RBRG_UPDATE
};
enum
{
    RBRG_DISABLE_ENTRY =1,
    RBRG_DELETE_ENTRY
};
enum
{
    RBRG_ENABLE_ENTRY = 1,
    RBRG_CREATE_ENTRY
};
enum
{
    RBRG_PORT_UNINHIBITED = 1,
    RBRG_PORT_PORTINHIBITED,
    RBRG_PORT_VLANINHIBITED,
    RBRG_PORT_DISABLED,
    RBRG_PORT_BROKEN
};
enum
{
    RBRG_FDB_STATIC = 1,
    RBRG_FDB_DYNAMIC,
    RBRG_FDB_BOTH
};
enum
{
    RBRG_FIB_STATIC = 1,
    RBRG_FIB_DYNAMIC
};
enum
{
    RBRG_FDB_OTHER = 1,
    RBRG_FDB_INVALID,
    RBRG_FDB_LEARNED,
    RBRG_FDB_SELF,
    RBRG_FDB_MGMT,
    RBRG_FDB_ESADI
};
#define GET_RBRG_FDB_PTR_FROM_FIB_LST(x) (tRBrgFdbEntry *)(VOID *)(((UINT1*)x) \
                                        - FSAP_OFFSETOF(tRBrgFdbEntry, RBrgFibFdbNode))
#endif  /* __RBRGDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file rbrgdefn.h                      */
/*-----------------------------------------------------------------------*/

