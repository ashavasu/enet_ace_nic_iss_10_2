/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrbridb.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRBRIDB_H
#define _FSRBRIDB_H

UINT1 FsrbridgeGlobalTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsrbridgeNicknameTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsrbridgePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsrbridgeUniFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsrbridgeUniFibTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsrbridgeMultiFibTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsrbridgePortCounterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsrbri [] ={1,3,6,1,4,1,29601,2,66};
tSNMP_OID_TYPE fsrbriOID = {9, fsrbri};


UINT4 FsrbridgeGlobalTrace [ ] ={1,3,6,1,4,1,29601,2,66,0,1,1};
UINT4 FsrbridgeContextId [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,1};
UINT4 FsrbridgeTrillVersion [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,2};
UINT4 FsrbridgeNumPorts [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,3};
UINT4 FsrbridgeUniMultipathEnable [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,4};
UINT4 FsrbridgeMultiMultipathEnable [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,5};
UINT4 FsrbridgeNicknameNumber [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,6};
UINT4 FsrbridgeSystemControl [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,7};
UINT4 FsrbridgeModuleStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,8};
UINT4 FsrbridgeUnicastMultipathCount [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,9};
UINT4 FsrbridgeMulticastMultipathCount [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,10};
UINT4 FsrbridgeClearCounters [ ] ={1,3,6,1,4,1,29601,2,66,0,1,2,1,11};
UINT4 FsrbridgeNicknameName [ ] ={1,3,6,1,4,1,29601,2,66,0,1,3,1,1};
UINT4 FsrbridgeNicknamePriority [ ] ={1,3,6,1,4,1,29601,2,66,0,1,3,1,2};
UINT4 FsrbridgeNicknameDtrPriority [ ] ={1,3,6,1,4,1,29601,2,66,0,1,3,1,3};
UINT4 FsrbridgeNicknameStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,1,3,1,4};
UINT4 FsrbridgePortIfIndex [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,1};
UINT4 FsrbridgePortDisable [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,2};
UINT4 FsrbridgePortTrunkPort [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,3};
UINT4 FsrbridgePortAccessPort [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,4};
UINT4 FsrbridgePortState [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,5};
UINT4 FsrbridgePortDisableLearning [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,6};
UINT4 FsrbridgePortDesigVlan [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,7};
UINT4 FsrbridgePortClearCounters [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,8};
UINT4 FsrbridgePortMac [ ] ={1,3,6,1,4,1,29601,2,66,0,1,4,1,9};
UINT4 FsrbridgeFdbId [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,1};
UINT4 FsrbridgeUniFdbAddr [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,2};
UINT4 FsrbridgeUniFdbPort [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,3};
UINT4 FsrbridgeUniFdbNick [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,4};
UINT4 FsrbridgeUniFdbConfidence [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,5};
UINT4 FsrbridgeUniFdbStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,6};
UINT4 FsrbridgeUniFdbRowStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,1,1,7};
UINT4 FsrbridgeFibNickname [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,1};
UINT4 FsrbridgeFibPort [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,2};
UINT4 FsrbridgeFibNextHopRBridge [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,3};
UINT4 FsrbridgeFibMacAddress [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,4};
UINT4 FsrbridgeFibMtuDesired [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,5};
UINT4 FsrbridgeFibHopCount [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,6};
UINT4 FsrbridgeFibStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,7};
UINT4 FsrbridgeFibRowstatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,2,1,8};
UINT4 FsrbridgeMultiFibNickname [ ] ={1,3,6,1,4,1,29601,2,66,0,2,4,1,1};
UINT4 FsrbridgeMultiFibPorts [ ] ={1,3,6,1,4,1,29601,2,66,0,2,4,1,2};
UINT4 FsrbridgeMultiFibStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,4,1,3};
UINT4 FsrbridgeMultiFibRowStatus [ ] ={1,3,6,1,4,1,29601,2,66,0,2,4,1,4};
UINT4 FsrbridgePortRpfChecksFailed [ ] ={1,3,6,1,4,1,29601,2,66,0,3,1,1,1};
UINT4 FsrbridgePortHopCountsExceeded [ ] ={1,3,6,1,4,1,29601,2,66,0,3,1,1,2};
UINT4 FsrbridgePortOptions [ ] ={1,3,6,1,4,1,29601,2,66,0,3,1,1,3};
UINT4 FsrbridgePortTrillInFrames [ ] ={1,3,6,1,4,1,29601,2,66,0,3,1,1,4};
UINT4 FsrbridgePortTrillOutFrames [ ] ={1,3,6,1,4,1,29601,2,66,0,3,1,1,5};




tMbDbEntry fsrbriMibEntry[]= {

{{12,FsrbridgeGlobalTrace}, NULL, FsrbridgeGlobalTraceGet, FsrbridgeGlobalTraceSet, FsrbridgeGlobalTraceTest, FsrbridgeGlobalTraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsrbridgeContextId}, GetNextIndexFsrbridgeGlobalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeTrillVersion}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeTrillVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeNumPorts}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeNumPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeUniMultipathEnable}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeUniMultipathEnableGet, FsrbridgeUniMultipathEnableSet, FsrbridgeUniMultipathEnableTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeMultiMultipathEnable}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeMultiMultipathEnableGet, FsrbridgeMultiMultipathEnableSet, FsrbridgeMultiMultipathEnableTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeNicknameNumber}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeNicknameNumberGet, FsrbridgeNicknameNumberSet, FsrbridgeNicknameNumberTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, "1"},

{{14,FsrbridgeSystemControl}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeSystemControlGet, FsrbridgeSystemControlSet, FsrbridgeSystemControlTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, "1"},

{{14,FsrbridgeModuleStatus}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeModuleStatusGet, FsrbridgeModuleStatusSet, FsrbridgeModuleStatusTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, "2"},
{{14,FsrbridgeUnicastMultipathCount}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeUnicastMultipathCountGet, FsrbridgeUnicastMultipathCountSet, FsrbridgeUnicastMultipathCountTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeMulticastMultipathCount}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeMulticastMultipathCountGet, FsrbridgeMulticastMultipathCountSet, FsrbridgeMulticastMultipathCountTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeClearCounters}, GetNextIndexFsrbridgeGlobalTable, FsrbridgeClearCountersGet, FsrbridgeClearCountersSet, FsrbridgeClearCountersTest, FsrbridgeGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeGlobalTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgeNicknameName}, GetNextIndexFsrbridgeNicknameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsrbridgeNicknameTableINDEX, 2, 0, 0, NULL},

{{14,FsrbridgeNicknamePriority}, GetNextIndexFsrbridgeNicknameTable, FsrbridgeNicknamePriorityGet, FsrbridgeNicknamePrioritySet, FsrbridgeNicknamePriorityTest, FsrbridgeNicknameTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeNicknameTableINDEX, 2, 0, 0, "192"},

{{14,FsrbridgeNicknameDtrPriority}, GetNextIndexFsrbridgeNicknameTable, FsrbridgeNicknameDtrPriorityGet, FsrbridgeNicknameDtrPrioritySet, FsrbridgeNicknameDtrPriorityTest, FsrbridgeNicknameTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeNicknameTableINDEX, 2, 0, 0, "32768"},

{{14,FsrbridgeNicknameStatus}, GetNextIndexFsrbridgeNicknameTable, FsrbridgeNicknameStatusGet, FsrbridgeNicknameStatusSet, FsrbridgeNicknameStatusTest, FsrbridgeNicknameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeNicknameTableINDEX, 2, 0, 0, "1"},

{{14,FsrbridgePortIfIndex}, GetNextIndexFsrbridgePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsrbridgePortTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortDisable}, GetNextIndexFsrbridgePortTable, FsrbridgePortDisableGet, FsrbridgePortDisableSet, FsrbridgePortDisableTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgePortTrunkPort}, GetNextIndexFsrbridgePortTable, FsrbridgePortTrunkPortGet, FsrbridgePortTrunkPortSet, FsrbridgePortTrunkPortTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgePortAccessPort}, GetNextIndexFsrbridgePortTable, FsrbridgePortAccessPortGet, FsrbridgePortAccessPortSet, FsrbridgePortAccessPortTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgePortState}, GetNextIndexFsrbridgePortTable, FsrbridgePortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsrbridgePortTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortDisableLearning}, GetNextIndexFsrbridgePortTable, FsrbridgePortDisableLearningGet, FsrbridgePortDisableLearningSet, FsrbridgePortDisableLearningTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgePortDesigVlan}, GetNextIndexFsrbridgePortTable, FsrbridgePortDesigVlanGet, FsrbridgePortDesigVlanSet, FsrbridgePortDesigVlanTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortClearCounters}, GetNextIndexFsrbridgePortTable, FsrbridgePortClearCountersGet, FsrbridgePortClearCountersSet, FsrbridgePortClearCountersTest, FsrbridgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgePortTableINDEX, 1, 0, 0, "2"},

{{14,FsrbridgePortMac}, GetNextIndexFsrbridgePortTable, FsrbridgePortMacGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsrbridgePortTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgeFdbId}, GetNextIndexFsrbridgeUniFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbAddr}, GetNextIndexFsrbridgeUniFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbPort}, GetNextIndexFsrbridgeUniFdbTable, FsrbridgeUniFdbPortGet, FsrbridgeUniFdbPortSet, FsrbridgeUniFdbPortTest, FsrbridgeUniFdbTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbNick}, GetNextIndexFsrbridgeUniFdbTable, FsrbridgeUniFdbNickGet, FsrbridgeUniFdbNickSet, FsrbridgeUniFdbNickTest, FsrbridgeUniFdbTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbConfidence}, GetNextIndexFsrbridgeUniFdbTable, FsrbridgeUniFdbConfidenceGet, FsrbridgeUniFdbConfidenceSet, FsrbridgeUniFdbConfidenceTest, FsrbridgeUniFdbTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbStatus}, GetNextIndexFsrbridgeUniFdbTable, FsrbridgeUniFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsrbridgeUniFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsrbridgeUniFdbRowStatus}, GetNextIndexFsrbridgeUniFdbTable, FsrbridgeUniFdbRowStatusGet, FsrbridgeUniFdbRowStatusSet, FsrbridgeUniFdbRowStatusTest, FsrbridgeUniFdbTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeUniFdbTableINDEX, 3, 0, 1, NULL},

{{14,FsrbridgeFibNickname}, GetNextIndexFsrbridgeUniFibTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsrbridgeUniFibTableINDEX, 4, 0, 0, NULL},

{{14,FsrbridgeFibPort}, GetNextIndexFsrbridgeUniFibTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsrbridgeUniFibTableINDEX, 4, 0, 0, NULL},

{{14,FsrbridgeFibNextHopRBridge}, GetNextIndexFsrbridgeUniFibTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsrbridgeUniFibTableINDEX, 4, 0, 0, NULL},

{{14,FsrbridgeFibMacAddress}, GetNextIndexFsrbridgeUniFibTable, FsrbridgeFibMacAddressGet, FsrbridgeFibMacAddressSet, FsrbridgeFibMacAddressTest, FsrbridgeUniFibTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsrbridgeUniFibTableINDEX, 4, 0, 0, NULL},

{{14,FsrbridgeFibMtuDesired}, GetNextIndexFsrbridgeUniFibTable, FsrbridgeFibMtuDesiredGet, FsrbridgeFibMtuDesiredSet, FsrbridgeFibMtuDesiredTest, FsrbridgeUniFibTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeUniFibTableINDEX, 4, 0, 0, "1470"},

{{14,FsrbridgeFibHopCount}, GetNextIndexFsrbridgeUniFibTable, FsrbridgeFibHopCountGet, FsrbridgeFibHopCountSet, FsrbridgeFibHopCountTest, FsrbridgeUniFibTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsrbridgeUniFibTableINDEX, 4, 0, 0, "10"},

{{14,FsrbridgeFibStatus}, GetNextIndexFsrbridgeUniFibTable, FsrbridgeFibStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsrbridgeUniFibTableINDEX, 4, 0, 0, NULL},

{{14,FsrbridgeFibRowstatus}, GetNextIndexFsrbridgeUniFibTable, FsrbridgeFibRowstatusGet, FsrbridgeFibRowstatusSet, FsrbridgeFibRowstatusTest, FsrbridgeUniFibTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeUniFibTableINDEX, 4, 0, 1, NULL},

{{14,FsrbridgeMultiFibNickname}, GetNextIndexFsrbridgeMultiFibTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsrbridgeMultiFibTableINDEX, 2, 0, 0, NULL},

{{14,FsrbridgeMultiFibPorts}, GetNextIndexFsrbridgeMultiFibTable, FsrbridgeMultiFibPortsGet, FsrbridgeMultiFibPortsSet, FsrbridgeMultiFibPortsTest, FsrbridgeMultiFibTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsrbridgeMultiFibTableINDEX, 2, 0, 0, NULL},

{{14,FsrbridgeMultiFibStatus}, GetNextIndexFsrbridgeMultiFibTable, FsrbridgeMultiFibStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsrbridgeMultiFibTableINDEX, 2, 0, 0, NULL},

{{14,FsrbridgeMultiFibRowStatus}, GetNextIndexFsrbridgeMultiFibTable, FsrbridgeMultiFibRowStatusGet, FsrbridgeMultiFibRowStatusSet, FsrbridgeMultiFibRowStatusTest, FsrbridgeMultiFibTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsrbridgeMultiFibTableINDEX, 2, 0, 1, NULL},

{{14,FsrbridgePortRpfChecksFailed}, GetNextIndexFsrbridgePortCounterTable, FsrbridgePortRpfChecksFailedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsrbridgePortCounterTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortHopCountsExceeded}, GetNextIndexFsrbridgePortCounterTable, FsrbridgePortHopCountsExceededGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsrbridgePortCounterTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortOptions}, GetNextIndexFsrbridgePortCounterTable, FsrbridgePortOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsrbridgePortCounterTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortTrillInFrames}, GetNextIndexFsrbridgePortCounterTable, FsrbridgePortTrillInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsrbridgePortCounterTableINDEX, 1, 0, 0, NULL},

{{14,FsrbridgePortTrillOutFrames}, GetNextIndexFsrbridgePortCounterTable, FsrbridgePortTrillOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsrbridgePortCounterTableINDEX, 1, 0, 0, NULL},
};
tMibData fsrbriEntry = { 49, fsrbriMibEntry };

#endif /* _FSRBRIDB_H */

