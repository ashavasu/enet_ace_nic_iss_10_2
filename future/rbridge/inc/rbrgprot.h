/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrgprot.h,v 1.3 2012/02/02 13:33:01 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of rbrg module.
 *******************************************************************/

#ifndef __RBRGPROT_H__
#define __RBRGPROT_H__ 

/* Add Prototypes here */

/**************************rbrgmain.c*******************************/
PUBLIC VOID RbrgMainTask             PROTO ((VOID));
PUBLIC UINT4 RbrgMainTaskInit         PROTO ((VOID));
PUBLIC VOID RbrgMainDeInit            PROTO ((VOID));
PUBLIC INT4 RbrgMainTaskLock          PROTO ((VOID));
PUBLIC INT4 RbrgMainTaskUnLock        PROTO ((VOID));



/**************************rbrgque.c********************************/
PUBLIC VOID RbrgQueProcessMsgs        PROTO ((VOID));
PUBLIC INT4 RbrgQueEnqMsg PROTO ((tRbrgQMsg * pMsg));

/**************************rbrgtmr.c*******************************/
PUBLIC VOID  RbrgTmrInitTmrDesc      PROTO(( VOID));
PUBLIC VOID  RbrgTmrStartTmr         PROTO((tRbrgTmr * pRbrgTmr, 
                       enRbrgTmrId eRbrgTmrId, 
         UINT4 u4Secs));
PUBLIC VOID  RbrgTmrRestartTmr       PROTO((tRbrgTmr * pRbrgTmr, 
                 enRbrgTmrId eRbrgTmrId, 
          UINT4 u4Secs));
PUBLIC VOID  RbrgTmrStopTmr          PROTO((tRbrgTmr * pRbrgTmr));
PUBLIC VOID  RbrgTmrHandleExpiry     PROTO((VOID));

/**************************rbrgtrc.c*******************************/


CHR1  *RbrgTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   RbrgTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   RbrgTrcWrite      PROTO(( CHR1 *s));

/**************************rbrgtask.c*******************************/


/*******************rbrgutl.c**************************************/
PUBLIC INT4
RbrgUtlGetAllFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *
                                          pRbrgGetFsRbridgeUniFdbEntry));
PUBLIC INT4
RbrgUtlGetAllFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *
                                          pRbrgGetFsRbridgeUniFibEntry));
PUBLIC INT4
RbrgUtlSetAllFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *
                                pRbrgSetFsRbridgeUniFdbEntry,
                                tRbrgIsSetFsRbridgeUniFdbEntry *
                                pRbrgIsSetFsRbridgeUniFdbEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption));
PUBLIC VOID RBrgUtlCopyFsFdbToRBrgFdb PROTO ((tRbrgFsRbridgeUniFdbEntry *pFsFdbEntry,
                                              tRBrgFdbEntry *pRBrgFdbEntry));
PUBLIC VOID RBrgUtlCopyRBrgFdbToFsFdb PROTO ((tRbrgFsRbridgeUniFdbEntry *pFsFdbEntry,
                                              tRBrgFdbEntry *pRBrgFdbEntry));
PUBLIC INT4
RbrgUtlSetAllFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *
                                pRbrgSetFsRbridgeUniFibEntry,
                                tRbrgIsSetFsRbridgeUniFibEntry *
                                pRbrgIsSetFsRbridgeUniFibEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption));
PUBLIC INT4 RbrgUtlGetFirstFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *pRbrgFsFdbEntry));
PUBLIC INT4 
RbrgUtlGetNextFdbEntry PROTO ((tRbrgFsRbridgeUniFdbEntry *pCurrentFsFdbEntry,
                               tRbrgFsRbridgeUniFdbEntry *pNextFsFdbEntry));
PUBLIC INT4 
RbrgUtlGetFdbEntry PROTO ((tRbrgFsRbridgeUniFdbEntry *pFsFdbEntry,
                           tRbrgFsRbridgeUniFdbEntry *pGetFsFdbEntry));
PUBLIC INT4
RbrgUtlGetFirstFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *pFsFibEntry));
PUBLIC INT4
RbrgUtlGetNextFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *pCurrFsFibEntry, 
                               tRbrgFsRbridgeUniFibEntry *pNextFsFibEntry));
PUBLIC INT4
RbrgUtlGetFibEntry PROTO ((tRbrgFsRbridgeUniFibEntry *pFsFibEntry, 
                           tRbrgFsRbridgeUniFibEntry *pGetFsFibEntry));
INT4
RbrgUtlFdbTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4
RbrgUtlFibTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2);
PUBLIC VOID RBrgUtlCopyFsFibToRBrgFib (tRbrgFsRbridgeUniFibEntry *pFsFibEntry,
                                       tRBrgFibEntry  *pRbrgFibEntry);

INT4
RbrgUtlCreateContext PROTO ((UINT4 u4ContextId));
INT4
RbrgUtlDeleteContext PROTO ((UINT4 u4ContextId));
INT4
RbrgUtlCreatePort PROTO ((UINT4 u4IfIndex, UINT1 u1Type));
INT4
RbrgUtlDeletePort PROTO ((UINT4 u4IfIndex, UINT1 u1Type));
INT4
RbrgUtlAddPortToLAG PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                            UINT4 u4PortChId));
INT4
RbrgUtlRemPortFromLAG PROTO ((UINT4 u4IfIndex, UINT4 u4PortChId));
PUBLIC VOID
RbrgUtlCreateAllPorts PROTO ((UINT4 u4ContextId));
PUBLIC INT4 RbrgUniFdbTableCreate PROTO ((VOID));
PUBLIC INT4 RbrgUniFibTableCreate PROTO ((VOID));
INT4 
RbrgUtlEnablePortsOnContext PROTO ((UINT4 u4ContextId));
INT4 
RbrgUtlEnableFdbEntriesOnContext PROTO ((UINT4 u4ContextId));
INT4 
RbrgUtlEnableFibEntriesOnContext PROTO ((UINT4 u4ContextId));
INT4 
RbrgUtlDelPortsOnContext PROTO ((UINT4 u4ContextId, UINT1 u1Type));
INT4 
RbrgUtlDelFdbEntriesOnContext PROTO ((UINT4 u4ContextId, UINT1 u1Type));
INT4 
RbrgUtlDelFibEntriesOnContext PROTO ((UINT4 u4ContextId, UINT1 u1Type));
INT4
RbrgUtlDelFibEntriesOnPort PROTO ((UINT4 u4IfIndex, UINT1 u1Type));
INT4 
RbrgUtlDelFdbEntriesOnPort PROTO ((UINT4 u4IfIndex, UINT1 u1Type));
/************rbrgport.c***********************************/
PUBLIC VOID
RbrgPortCfaGetSysMacAddress PROTO ((tMacAddr SwitchMac));
PUBLIC INT4
RbrgPortVcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4ContextId));
PUBLIC INT4
RbrgPortVcmGetCxtInfoFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId));
PUBLIC VOID
RbrgPortVcmGetFirstVcId PROTO ((UINT4 *pu4ContextId));
PUBLIC INT4
RbrgPortVcmGetNextVcId PROTO ((UINT4 u4ContextId, UINT4 *pu4NextContextId));
PUBLIC INT4
RbrgPortVcmGetContextPortList PROTO ((UINT4 u4ContextId, tPortList PortList));
INT4
RbrgPortIsPortInPortChannel PROTO ((UINT4 u4IfIndex));
INT4
RbrgCfaCliGetIfList PROTO ((INT1 *pi1IfName, INT1 *pi1IfListStr,
                            UINT1 *pu1IfListArray,
                            UINT4 u4IfListArrayLen));
/******************************rbrgmod.c*****************************/
PUBLIC INT4
RbrgModuleInit PROTO ((VOID));
PUBLIC INT4
RbrgModuleStart PROTO ((UINT4 u4contextId));
PUBLIC INT4
RbrgModuleShut PROTO ((UINT4 u4ContextId));
PUBLIC INT4
RbrgModuleEnable PROTO ((UINT4 u4ContextId));
PUBLIC INT4
RbrgModuleDisable PROTO ((UINT4 u4ContextId));
PUBLIC VOID
RBrgUtlCopyRBrgFibToFsFib PROTO ((tRbrgFsRbridgeUniFibEntry * pFsFibEntry,
                                  tRBrgFibEntry * pRbrgFibEntry));
INT4 RbrgUtlDelNicknamesOnContext PROTO ((UINT4 u4ContextId));
/****************************rbrgutil.c**********************************/
PUBLIC INT4
RbrgUtilReconfigureFib PROTO ((tRBrgFibEntry *pRbrgFibEntry, tRbrgFsRBridgeBasePortEntry *pRBrgPortEntry));
PUBLIC INT4 
RbrgClearAllPortCounters PROTO ((UINT4 u4ContextId));
PUBLIC INT4 
RbrgClearPortCounters PROTO ((UINT4 u4ContextId, UINT4 u4Port));
#endif   /* __RBRGPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgprot.h                     */
/*-----------------------------------------------------------------------*/
