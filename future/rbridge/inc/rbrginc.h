/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rbrginc.h,v 1.2 2013/09/28 07:36:19 siva Exp $
 *
 * Description: This file contains include files of rbrg module.
 *******************************************************************/

#ifndef __RBRGINC_H__
#define __RBRGINC_H__ 

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "vcm.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"

#include "rbrgdefn.h"
#include "rbrgtdfsg.h"
#include "rbrgtdfs.h"
#include "rbrgtmr.h"
#include "rbrgtrc.h"
#include "rbridge.h"
#include "rbrgglob.h"
#ifdef __RBRGMAIN_C__

#include "rbrglwg.h"
#include "rbrgdefg.h"
#include "rbrgsz.h"
#include "rbrgwrg.h"

#else

#include "rbrgmibclig.h"
#include "rbrglwg.h"
#include "rbrgdefg.h"
#include "rbrgsz.h"
#include "rbrgwrg.h"

#endif /* __RBRGMAIN_C__*/

#include "rbrgprot.h"
#include "rbrgprotg.h"
#endif   /* __RBRGINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file rbrginc.h                       */
/*-----------------------------------------------------------------------*/

