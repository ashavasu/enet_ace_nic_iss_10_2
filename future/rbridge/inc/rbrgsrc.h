/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgsrc.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*     for Rbrg module 
*********************************************************************/

#include "rbrgcli.h"
#include "rbrgsrcdefn.h"
#define RBRG_MAX_IFNAME_LEN 35
INT4 RbrgShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgeGlobalTable(tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgeNicknameTable(tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgeUniFdbTable(tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgeUniFibTable(tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgeMultiFibTable(tCliHandle CliHandle, UINT4 u4Module);
INT4 RbrgShowRunningConfigFsrbridgePortTable(tCliHandle CliHandle, UINT4 u4Module);

