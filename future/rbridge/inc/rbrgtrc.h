/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rbrgtrc.h,v 1.2 2012/02/10 10:26:57 siva Exp $
*
* Description:This file contains procedures and definitions 
*             used for debugging.
*********************************************************************/

#ifndef __RBRGTRC_H__
#define __RBRGTRC_H__

#define  RBRG_TRC_FLAG  gRbrgGlobals.RbrgGlbMib.u4FsrbridgeGlobalTrace
#define  RBRG_NAME      "RBRG"               

#ifdef RBRG_TRACE_WANTED



/*#define  RBRG_TRC(TraceTypr, str)       RbrgTrc (TraceType, str)*/
#define  RBRG_TRC(x)       RbrgTrcPrint( __FILE__, __LINE__, RbrgTrc x)
#define  RBRG_TRC_FUNC(x)  RbrgTrcPrint( __FILE__, __LINE__, RbrgTrc x)
#define  RBRG_TRC_CRIT(x)  RbrgTrcPrint( __FILE__, __LINE__, RbrgTrc x)
#define  RBRG_TRC_PKT(x)   RbrgTrcWrite( RbrgTrc x)




#else /* RBRG_TRACE_WANTED */

#define  RBRG_TRC(x) 
#define  RBRG_TRC_FUNC(x)
#define  RBRG_TRC_CRIT(x)
#define  RBRG_TRC_PKT(x)

#endif /* RBRG_TRACE_WANTED */


#define  RBRG_FN_ENTRY  (0x1 << 9)
#define  RBRG_FN_EXIT   (0x1 << 10)
#define  RBRG_CLI_TRC   (0x1 << 11)
#define  RBRG_MAIN_TRC  (0x1 << 12)
#define  RBRG_PKT_TRC   (0x1 << 13)
#define  RBRG_QUE_TRC   (0x1 << 14)
#define  RBRG_TASK_TRC  (0x1 << 15)
#define  RBRG_TMR_TRC   (0x1 << 16)
#define  RBRG_UTIL_TRC  (0x1 << 17)
#define  RBRG_ALL_TRC   RBRG_FN_ENTRY |\
                        RBRG_FN_EXIT  |\
                        RBRG_CLI_TRC  |\
                        RBRG_MAIN_TRC |\
                        RBRG_PKT_TRC  |\
                        RBRG_QUE_TRC  |\
                        RBRG_TASK_TRC |\
                        RBRG_TMR_TRC  |\
                        RBRG_UTIL_TRC


#endif /* _RBRGTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  rbrgtrc.h                      */
/*-----------------------------------------------------------------------*/
