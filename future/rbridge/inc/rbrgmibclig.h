/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgmibclig.h,v 1.2 2012/01/27 13:06:23 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsrbridgeGlobalTrace[12];

extern UINT4 FsrbridgeContextId[14];

extern UINT4 FsrbridgeUniMultipathEnable[14];

extern UINT4 FsrbridgeMultiMultipathEnable[14];

extern UINT4 FsrbridgeNicknameNumber[14];

extern UINT4 FsrbridgeSystemControl[14];

extern UINT4 FsrbridgeModuleStatus[14];

extern UINT4 FsrbridgeUnicastMultipathCount[14];

extern UINT4 FsrbridgeMulticastMultipathCount[14];

extern UINT4 FsrbridgeClearCounters[14];

extern UINT4 FsrbridgeNicknameName[14];

extern UINT4 FsrbridgeNicknamePriority[14];

extern UINT4 FsrbridgeNicknameDtrPriority[14];

extern UINT4 FsrbridgeNicknameStatus[14];

extern UINT4 FsrbridgePortIfIndex[14];

extern UINT4 FsrbridgePortDisable[14];

extern UINT4 FsrbridgePortTrunkPort[14];

extern UINT4 FsrbridgePortAccessPort[14];

extern UINT4 FsrbridgePortDisableLearning[14];

extern UINT4 FsrbridgePortDesigVlan[14];

extern UINT4 FsrbridgePortClearCounters[14];

extern UINT4 FsrbridgeFdbId[14];

extern UINT4 FsrbridgeUniFdbAddr[14];

extern UINT4 FsrbridgeUniFdbPort[14];

extern UINT4 FsrbridgeUniFdbNick[14];

extern UINT4 FsrbridgeUniFdbConfidence[14];

extern UINT4 FsrbridgeUniFdbRowStatus[14];

extern UINT4 FsrbridgeFibNickname[14];

extern UINT4 FsrbridgeFibPort[14];

extern UINT4 FsrbridgeFibNextHopRBridge[14];

extern UINT4 FsrbridgeFibMacAddress[14];

extern UINT4 FsrbridgeFibMtuDesired[14];

extern UINT4 FsrbridgeFibHopCount[14];

extern UINT4 FsrbridgeFibRowstatus[14];

extern UINT4 FsrbridgeMultiFibNickname[14];

extern UINT4 FsrbridgeMultiFibPorts[14];

extern UINT4 FsrbridgeMultiFibRowStatus[14];
