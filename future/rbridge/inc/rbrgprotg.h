/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
* Aricent Inc . All Rights Reserved
* $Id: rbrgprotg.h,v 1.3 2012/02/10 10:26:57 siva Exp $
* This file contains prototypes for functions defined in Rbrg.
*********************************************************************/


#include "cli.h"

INT4 RbrgCliSetFsrbridgeGlobalTrace (tCliHandle CliHandle, 
                                     UINT4 *pFsrbridgeGlobalTrace, 
                                     UINT1 *pu1SetFlag);


INT4 RbrgCliSetFsrbridgeGlobalTable (tCliHandle CliHandle,tRbrgFsrbridgeGlobalEntry * pRbrgSetFsrbridgeGlobalEntry,tRbrgIsSetFsrbridgeGlobalEntry * pRbrgIsSetFsrbridgeGlobalEntry);

INT4 RbrgCliSetFsrbridgeNicknameTable (tCliHandle CliHandle,tRbrgFsrbridgeNicknameEntry * pRbrgSetFsrbridgeNicknameEntry,tRbrgIsSetFsrbridgeNicknameEntry * pRbrgIsSetFsrbridgeNicknameEntry);

INT4 RbrgCliSetFsrbridgePortTable (tCliHandle CliHandle,tRbrgFsRBridgeBasePortEntry * pRbrgSetFsRBridgeBasePortEntry,tRbrgIsSetFsRBridgeBasePortEntry * pRbrgIsSetFsRBridgeBasePortEntry);

INT4 RbrgCliSetFsrbridgeUniFdbTable (tCliHandle CliHandle,tRbrgFsRbridgeUniFdbEntry * pRbrgSetFsRbridgeUniFdbEntry,tRbrgIsSetFsRbridgeUniFdbEntry * pRbrgIsSetFsRbridgeUniFdbEntry);

INT4 RbrgCliSetFsrbridgeUniFibTable (tCliHandle CliHandle,tRbrgFsRbridgeUniFibEntry * pRbrgSetFsRbridgeUniFibEntry,tRbrgIsSetFsRbridgeUniFibEntry * pRbrgIsSetFsRbridgeUniFibEntry);

INT4 RbrgCliSetFsrbridgeMultiFibTable (tCliHandle CliHandle,tRbrgFsrbridgeMultiFibEntry * pRbrgSetFsrbridgeMultiFibEntry,tRbrgIsSetFsrbridgeMultiFibEntry * pRbrgIsSetFsrbridgeMultiFibEntry);


PUBLIC INT4 RbrgUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 RbrgLock (VOID);

PUBLIC INT4 RbrgUnLock (VOID);
PUBLIC INT4 FsrbridgeGlobalTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgeGlobalTableCreate PROTO ((VOID));

tRbrgFsrbridgeGlobalEntry * RbrgFsrbridgeGlobalTableCreateApi PROTO ((tRbrgFsrbridgeGlobalEntry *));

PUBLIC INT4 FsrbridgeNicknameTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgeNicknameTableCreate PROTO ((VOID));

tRbrgFsrbridgeNicknameEntry * RbrgFsrbridgeNicknameTableCreateApi PROTO ((tRbrgFsrbridgeNicknameEntry *));

PUBLIC INT4 FsrbridgePortTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgePortTableCreate PROTO ((VOID));

tRbrgFsRBridgeBasePortEntry * RbrgFsrbridgePortTableCreateApi PROTO ((tRbrgFsRBridgeBasePortEntry *));

PUBLIC INT4 FsrbridgeUniFdbTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgeUniFdbTableCreate PROTO ((VOID));

tRbrgFsRbridgeUniFdbEntry * RbrgFsrbridgeUniFdbTableCreateApi PROTO ((tRbrgFsRbridgeUniFdbEntry *));

PUBLIC INT4 FsrbridgeUniFibTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgeUniFibTableCreate PROTO ((VOID));

tRbrgFsRbridgeUniFibEntry * RbrgFsrbridgeUniFibTableCreateApi PROTO ((tRbrgFsRbridgeUniFibEntry *));

PUBLIC INT4 FsrbridgeMultiFibTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 RbrgFsrbridgeMultiFibTableCreate PROTO ((VOID));

tRbrgFsrbridgeMultiFibEntry * RbrgFsrbridgeMultiFibTableCreateApi PROTO ((tRbrgFsrbridgeMultiFibEntry *));

PUBLIC INT4 FsrbridgePortCounterTableRBCmp PROTO ((tRBElem *, tRBElem *));
INT4 RbrgGetFsrbridgeGlobalTrace PROTO ((UINT4 *));
INT4 RbrgGetAllFsrbridgeGlobalTable PROTO ((tRbrgFsrbridgeGlobalEntry *));
INT4 RbrgGetAllUtlFsrbridgeGlobalTable PROTO((tRbrgFsrbridgeGlobalEntry *, tRbrgFsrbridgeGlobalEntry *));
INT4 RbrgGetAllFsrbridgeNicknameTable PROTO ((tRbrgFsrbridgeNicknameEntry *));
INT4 RbrgGetAllUtlFsrbridgeNicknameTable PROTO((tRbrgFsrbridgeNicknameEntry *, tRbrgFsrbridgeNicknameEntry *));
INT4 RbrgGetAllFsrbridgePortTable PROTO ((tRbrgFsRBridgeBasePortEntry *));
INT4 RbrgGetAllUtlFsrbridgePortTable PROTO((tRbrgFsRBridgeBasePortEntry *, tRbrgFsRBridgeBasePortEntry *));
INT4 RbrgGetAllFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *));
INT4 RbrgGetAllUtlFsrbridgeUniFdbTable PROTO((tRbrgFsRbridgeUniFdbEntry *, tRbrgFsRbridgeUniFdbEntry *));
INT4 RbrgGetAllFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *));
INT4 RbrgGetAllUtlFsrbridgeUniFibTable PROTO((tRbrgFsRbridgeUniFibEntry *, tRbrgFsRbridgeUniFibEntry *));
INT4 RbrgGetAllFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *));
INT4 RbrgGetAllUtlFsrbridgeMultiFibTable PROTO((tRbrgFsrbridgeMultiFibEntry *, tRbrgFsrbridgeMultiFibEntry *));
INT4 RbrgGetAllFsrbridgePortCounterTable PROTO ((tRbrgFsRbridgePortCounterEntry *));
INT4 RbrgGetAllUtlFsrbridgePortCounterTable PROTO((tRbrgFsRbridgePortCounterEntry *, tRbrgFsRbridgePortCounterEntry *));
INT4 RbrgSetFsrbridgeGlobalTrace PROTO ((UINT4 ));
INT4 RbrgSetAllFsrbridgeGlobalTable PROTO ((tRbrgFsrbridgeGlobalEntry *, tRbrgIsSetFsrbridgeGlobalEntry *));
INT4 RbrgSetAllFsrbridgeNicknameTable PROTO ((tRbrgFsrbridgeNicknameEntry *, tRbrgIsSetFsrbridgeNicknameEntry *));
INT4 RbrgSetAllFsrbridgePortTable PROTO ((tRbrgFsRBridgeBasePortEntry *, tRbrgIsSetFsRBridgeBasePortEntry *));
INT4 RbrgSetAllFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *, tRbrgIsSetFsRbridgeUniFdbEntry *, INT4 , INT4 ));
INT4 RbrgSetAllFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *, tRbrgIsSetFsRbridgeUniFibEntry *, INT4 , INT4 ));
INT4 RbrgSetAllFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *, tRbrgIsSetFsrbridgeMultiFibEntry *, INT4 , INT4 ));

INT4 RbrgInitializeFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *));

INT4 RbrgInitializeMibFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *));

INT4 RbrgInitializeFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *));

INT4 RbrgInitializeMibFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *));

INT4 RbrgInitializeFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *));

INT4 RbrgInitializeMibFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *));
INT4 RbrgTestFsrbridgeGlobalTrace PROTO ((UINT4 *,UINT4 ));
INT4 RbrgTestAllFsrbridgeGlobalTable PROTO ((UINT4 *, tRbrgFsrbridgeGlobalEntry *, tRbrgIsSetFsrbridgeGlobalEntry *));
INT4 RbrgTestAllFsrbridgeNicknameTable PROTO ((UINT4 *, tRbrgFsrbridgeNicknameEntry *, tRbrgIsSetFsrbridgeNicknameEntry *));
INT4 RbrgTestAllFsrbridgePortTable PROTO ((UINT4 *, tRbrgFsRBridgeBasePortEntry *, tRbrgIsSetFsRBridgeBasePortEntry *));
INT4 RbrgTestAllFsrbridgeUniFdbTable PROTO ((UINT4 *, tRbrgFsRbridgeUniFdbEntry *, tRbrgIsSetFsRbridgeUniFdbEntry *, INT4 , INT4));
INT4 RbrgTestAllFsrbridgeUniFibTable PROTO ((UINT4 *, tRbrgFsRbridgeUniFibEntry *, tRbrgIsSetFsRbridgeUniFibEntry *, INT4 , INT4));
INT4 RbrgTestAllFsrbridgeMultiFibTable PROTO ((UINT4 *, tRbrgFsrbridgeMultiFibEntry *, tRbrgIsSetFsrbridgeMultiFibEntry *, INT4 , INT4));
tRbrgFsrbridgeGlobalEntry * RbrgGetFsrbridgeGlobalTable PROTO ((tRbrgFsrbridgeGlobalEntry *));
tRbrgFsrbridgeNicknameEntry * RbrgGetFsrbridgeNicknameTable PROTO ((tRbrgFsrbridgeNicknameEntry *));
tRbrgFsRBridgeBasePortEntry * RbrgGetFsrbridgePortTable PROTO ((tRbrgFsRBridgeBasePortEntry *));
tRbrgFsrbridgeMultiFibEntry * RbrgGetFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *));
tRbrgFsRbridgePortCounterEntry * RbrgGetFsrbridgePortCounterTable PROTO ((tRbrgFsRbridgePortCounterEntry *));
tRbrgFsrbridgeGlobalEntry * RbrgGetFirstFsrbridgeGlobalTable PROTO ((VOID));
tRbrgFsrbridgeNicknameEntry * RbrgGetFirstFsrbridgeNicknameTable PROTO ((VOID));
tRbrgFsRBridgeBasePortEntry * RbrgGetFirstFsrbridgePortTable PROTO ((VOID));
tRbrgFsrbridgeMultiFibEntry * RbrgGetFirstFsrbridgeMultiFibTable PROTO ((VOID));
tRbrgFsRbridgePortCounterEntry * RbrgGetFirstFsrbridgePortCounterTable PROTO ((VOID));
tRbrgFsrbridgeGlobalEntry * RbrgGetNextFsrbridgeGlobalTable PROTO ((tRbrgFsrbridgeGlobalEntry *));
tRbrgFsrbridgeNicknameEntry * RbrgGetNextFsrbridgeNicknameTable PROTO ((tRbrgFsrbridgeNicknameEntry *));
tRbrgFsRBridgeBasePortEntry * RbrgGetNextFsrbridgePortTable PROTO ((tRbrgFsRBridgeBasePortEntry *));
tRbrgFsrbridgeMultiFibEntry * RbrgGetNextFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *));
tRbrgFsRbridgePortCounterEntry * RbrgGetNextFsrbridgePortCounterTable PROTO ((tRbrgFsRbridgePortCounterEntry *));
INT4 FsrbridgeGlobalTableFilterInputs PROTO ((tRbrgFsrbridgeGlobalEntry *, tRbrgFsrbridgeGlobalEntry *, tRbrgIsSetFsrbridgeGlobalEntry *));
INT4 FsrbridgeNicknameTableFilterInputs PROTO ((tRbrgFsrbridgeNicknameEntry *, tRbrgFsrbridgeNicknameEntry *, tRbrgIsSetFsrbridgeNicknameEntry *));
INT4 FsrbridgePortTableFilterInputs PROTO ((tRbrgFsRBridgeBasePortEntry *, tRbrgFsRBridgeBasePortEntry *, tRbrgIsSetFsRBridgeBasePortEntry *));
INT4 FsrbridgeUniFdbTableFilterInputs PROTO ((tRbrgFsRbridgeUniFdbEntry *, tRbrgFsRbridgeUniFdbEntry *, tRbrgIsSetFsRbridgeUniFdbEntry *));
INT4 FsrbridgeUniFibTableFilterInputs PROTO ((tRbrgFsRbridgeUniFibEntry *, tRbrgFsRbridgeUniFibEntry *, tRbrgIsSetFsRbridgeUniFibEntry *));
INT4 FsrbridgeMultiFibTableFilterInputs PROTO ((tRbrgFsrbridgeMultiFibEntry *, tRbrgFsrbridgeMultiFibEntry *, tRbrgIsSetFsrbridgeMultiFibEntry *));
INT4 RbrgUtilUpdateFsrbridgeGlobalTable PROTO ((tRbrgFsrbridgeGlobalEntry *, tRbrgFsrbridgeGlobalEntry *, tRbrgIsSetFsrbridgeGlobalEntry *));
INT4 RbrgUtilUpdateFsrbridgeNicknameTable PROTO ((tRbrgFsrbridgeNicknameEntry *, tRbrgFsrbridgeNicknameEntry *, tRbrgIsSetFsrbridgeNicknameEntry *));
INT4 RbrgUtilUpdateFsrbridgePortTable PROTO ((tRbrgFsRBridgeBasePortEntry *, tRbrgFsRBridgeBasePortEntry *, tRbrgIsSetFsRBridgeBasePortEntry *));
INT4 RbrgUtilUpdateFsrbridgeUniFdbTable PROTO ((tRbrgFsRbridgeUniFdbEntry *, tRbrgFsRbridgeUniFdbEntry *, tRbrgIsSetFsRbridgeUniFdbEntry *));
INT4 RbrgUtilUpdateFsrbridgeUniFibTable PROTO ((tRbrgFsRbridgeUniFibEntry *, tRbrgFsRbridgeUniFibEntry *, tRbrgIsSetFsRbridgeUniFibEntry *, UINT4));
INT4 RbrgUtilUpdateFsrbridgeMultiFibTable PROTO ((tRbrgFsrbridgeMultiFibEntry *, tRbrgFsrbridgeMultiFibEntry *, tRbrgIsSetFsrbridgeMultiFibEntry *));
INT4 RbrgSetAllFsrbridgeGlobalTableTrigger PROTO ((tRbrgFsrbridgeGlobalEntry *, tRbrgIsSetFsrbridgeGlobalEntry *, INT4));
INT4 RbrgSetAllFsrbridgeNicknameTableTrigger PROTO ((tRbrgFsrbridgeNicknameEntry *, tRbrgIsSetFsrbridgeNicknameEntry *, INT4));
INT4 RbrgSetAllFsrbridgePortTableTrigger PROTO ((tRbrgFsRBridgeBasePortEntry *, tRbrgIsSetFsRBridgeBasePortEntry *, INT4));
INT4 RbrgSetAllFsrbridgeUniFdbTableTrigger PROTO ((tRbrgFsRbridgeUniFdbEntry *, tRbrgIsSetFsRbridgeUniFdbEntry *, INT4));
INT4 RbrgSetAllFsrbridgeUniFibTableTrigger PROTO ((tRbrgFsRbridgeUniFibEntry *, tRbrgIsSetFsRbridgeUniFibEntry *, INT4));
INT4 RbrgSetAllFsrbridgeMultiFibTableTrigger PROTO ((tRbrgFsrbridgeMultiFibEntry *, tRbrgIsSetFsrbridgeMultiFibEntry *, INT4));
INT4 RbrgCliClearAllCounter PROTO ((tCliHandle CliHandle));


