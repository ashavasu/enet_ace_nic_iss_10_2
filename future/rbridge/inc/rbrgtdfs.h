/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: rbrgtdfs.h,v 1.4 2012/02/07 12:13:56 siva Exp $
*
* Description: This file contains type definitions for Rbrg module.
*********************************************************************/

#include "mbsm.h"
#define RBRG_MAX_FDB_DATA_PER_FDB_ENTRY 2
#define RBRG_MAX_FIB_DATA_PER_FIB_ENTRY 2
typedef struct RBrgFdbData
{
    
    UINT4                     u4RBrgFdbPort;            /* Port Id */
    UINT4                     u4RBrgFdbEgressNickname;  /* Egress R-Bridge
                                                           Nickname */
    UINT4                     u4FdbConfidence;          /* Confidence level
                                                           attached to this
                                                           entry */
    UINT1                     u1FdbOwner;    /* Owner of this FDB entry 
                                                1 - RBridge 2 - RBRG 3 - BOTH
                                                Corresponding bit will be set*/
    UINT1                     u1FdbRowStatus;/* Rowstatus of this entry */
    UINT1                     au1Pad[2];        /* Padding for 4-Byte
                                                   alignment */
}tRBrgFdbData;

/* RBridge FDB Entry */
typedef struct RBrgFdbEntry
{
    tRBNodeEmbd               RBrgFdbRBNode;    /* Pointer to FDB Node */

    tTMO_DLL_NODE             RBrgFibFdbNode; /* FibFdbNode */ 

    tMacAddr                  RBrgFdbMac;       /* FDB MAC address */
    INT1                      i1FdbStatus;   /* FDB status of this entry
                                                (how this entry is populated 
                                                0 - static 1- Dynamic 2- Both)*/
    UINT1                     au1Pad[1];
    UINT4                     u4ContextId;
    tRBrgFdbData             *pRBrgFdbData[RBRG_MAX_FDB_DATA_PER_FDB_ENTRY]; 
                                                /* Array containing pointers to 
                                                   static and dynamic FDB info.
                                                   pRBrgFdbData[0] - static FDB
                                                                     entry
                                                   pRBrgFdbData[1] - Dynamic FDB
                                                                     entry */
    UINT4                     u4RBrgFdbId;   /* RBridge FDB Id */
}tRBrgFdbEntry;

/* RBridge Unicast FIB data. This structure contains FIB information other than
 * the indices. */
typedef struct RBrgFibData
{
    tMacAddr      RBrgFibNextHopMac;     /* Next-Hop MAC address */
    UINT1         u1FibOwner;            /* Owner of this entry (how this
                                            entry got populated)
                                            1 - RBridge 2 - TRILL 3 - BOTH
                                            Corresponding bit will be set */
    UINT1         u1FibRowStatus;        /* Row status object for this entry */
    UINT4         u4RBrgFibMtu;          /* MTU Desired */
    UINT4         u4RBrgFibHopCount;            /* Hop Count */

}tRBrgFibData;

/* RBridge Unicast FIB information */
typedef struct RBrgFibEntry
{
    tRBNodeEmbd               RBrgFibRBNode;     /* Pointer to Next FIB Entry */
    tRBrgFibData             *pRBrgFibData[RBRG_MAX_FIB_DATA_PER_FIB_ENTRY];
                                                /* Array containing pointers to 
                                                   static and dynamic FIB info.
                                                   pRBrgFibData[0] - static FIB
                                                                     entry
                                                   pRBrgFibData[1] - Dynamic FIB
                                                                     entry */
    tTMO_DLL                  RBrgFibFdbList;    /* List of Back Pointers to 
                                                    Fdb entries that correspond 
                                                    to this nickname entry */
    UINT4                     u4ContextId;
    UINT4                     u4FibEgressRBrgNickname;  /* R-Bridge nickname */
    UINT4                     u4FibNextHopRBrgPortId;  /* Port Index attached to
                                                          next-hop R-Bridge */
    UINT4                     u4FibNextHopRBrgNickname;/* Next-Hop R-Bridge
                                                          Nickname */
    /* Caution: The data type for i4L3EgressId, u4AccessTrillPort, 
     * u4TrunkTrillPort may change if there is a change of data type in 
     * data plane */
    INT4          i4L3EgressId;          /* L3Egress created for this entry 
                                          * This value will be returned
                                          * from data plane*/
    UINT4         u4TrunkTrillPort;      /* TrunkTrill port created for this
                                            entry. This will be returned from 
                                            data plane */
    INT1         i1FibStatus;   /* FIB status of this entry
                                                (how this entry is populated 
                                                0 - static 1- Dynamic 2- Both)*/
    UINT1      au1Pad [3];
}tRBrgFibEntry;


/* Generated code starts from here */
typedef struct

{
 tRBTree   FsrbridgeGlobalTable;
 tRBTree   FsrbridgeNicknameTable;
 tRBTree   FsrbridgePortTable;
 tRBTree   RbrgUniFdbTable;
 tRBTree   RbrgUniFibTable;
 tRBTree   FsrbridgeUniFdbTable;
 tRBTree   FsrbridgeUniFibTable;
 tRBTree   FsrbridgeMultiFibTable;
 
    UINT4     u4FsrbridgeGlobalTrace;

} tRbrgGlbMib;

typedef struct
{
 tRbrgMibFsrbridgeGlobalEntry       MibObject;
} tRbrgFsrbridgeGlobalEntry;


typedef struct
{
 tRbrgMibFsrbridgeNicknameEntry       MibObject;
} tRbrgFsrbridgeNicknameEntry;


typedef struct
{
 tRbrgMibFsRBridgeBasePortEntry       MibObject;
    INT4                                i4L3InterfaceIndex;
    UINT4                               u4AccessTrillPort;
    UINT4                               u4ReferenceCount;
} tRbrgFsRBridgeBasePortEntry;


typedef struct
{
 tRbrgMibFsRbridgeUniFdbEntry       MibObject;
} tRbrgFsRbridgeUniFdbEntry;


typedef struct
{
 tRbrgMibFsRbridgeUniFibEntry       MibObject; 
    INT4                              i4L3EgressId;
    UINT4                             u4TrunkTrillPort;
} tRbrgFsRbridgeUniFibEntry;


typedef struct
{
 tRbrgMibFsrbridgeMultiFibEntry        MibObject;
 INT4                                  i4FsrbridgeMultiFibDynPortsLen;
 UINT1                                 au1FsrbridgeMultiFibDynPorts[256];
} tRbrgFsrbridgeMultiFibEntry;


typedef struct
{
 tRbrgMibFsRbridgePortCounterEntry       MibObject;
} tRbrgFsRbridgePortCounterEntry;



#ifndef __RBRGTDFS_H__
#define __RBRGTDFS_H__

/* -------------------------------------------------------
 *
 *                RBRG   Data Structures
 *
 * ------------------------------------------------------*/


typedef struct RBRG_GLOBALS {
 tTimerListId        rbrgTmrLst;
 UINT4               u4RbrgTrc;
 tOsixTaskId         rbrgTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          rbrgTaskSemId;
 tOsixQId            rbrgQueId;
 tRbrgGlbMib         RbrgGlbMib;
} tRbrgGlobals;

typedef struct RBrgQMsg
{
    UINT4      u4MsgType;
    UINT4      u4ContextId;
    UINT4      u4PortId;
    UINT4      u4PoIndex;
#ifdef MBSM_WANTED
    tMbsmProtoMsg *pMbsmProtoMsg;
#endif
}tRbrgQMsg;

#endif  /* __RBRGTDFS_H__ */
/*-----------------------------------------------------------------------*/


