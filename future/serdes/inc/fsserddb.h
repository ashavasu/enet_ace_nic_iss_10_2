/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsserddb.h,v 1.1 2010/11/02 14:02:55 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSERDDB_H
#define _FSSERDDB_H

UINT1 FsSerdesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsserd [] ={1,3,6,1,4,1,29601,2,50};
tSNMP_OID_TYPE fsserdOID = {9, fsserd};


UINT4 FsSerdesIfIndex [ ] ={1,3,6,1,4,1,29601,2,50,1,1,1,1};
UINT4 FsSerdesType [ ] ={1,3,6,1,4,1,29601,2,50,1,1,1,2};
UINT4 FsSerdesValue [ ] ={1,3,6,1,4,1,29601,2,50,1,1,1,3};


tMbDbEntry fsserdMibEntry[]= {

{{13,FsSerdesIfIndex}, GetNextIndexFsSerdesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSerdesTableINDEX, 2, 0, 0, NULL},

{{13,FsSerdesType}, GetNextIndexFsSerdesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSerdesTableINDEX, 2, 0, 0, NULL},

{{13,FsSerdesValue}, GetNextIndexFsSerdesTable, FsSerdesValueGet, FsSerdesValueSet, FsSerdesValueTest, FsSerdesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSerdesTableINDEX, 2, 0, 0, NULL},
};
tMibData fsserdEntry = { 3, fsserdMibEntry };
#endif /* _FSSERDDB_H */

