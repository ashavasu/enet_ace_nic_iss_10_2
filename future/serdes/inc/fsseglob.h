/********************************************************************
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
* 
*  $Id: fsseglob.h,v 1.1 2010/11/02 14:02:55 prabuc Exp $
* 
*  Description:
*
********************************************************************/
#ifndef _FS_GLOB_H
#define _FS_GLOB_H
#ifdef _SERDES_C_
tSerDesInfo SerDesInfo[SYS_DEF_MAX_PHYSICAL_INTERFACES];
#else
extern tSerDesInfo SerDesInfo[SYS_DEF_MAX_PHYSICAL_INTERFACES];
#endif /*End _CWSERDES_C_*/
#endif /*End _CW_GLOB_H */
