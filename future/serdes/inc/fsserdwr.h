
/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fsserdwr.h,v 1.1 2010/11/02 14:02:55 prabuc Exp $
*
*  Description:
*
********************************************************************/

#ifndef _FSSERDWR_H
#define _FSSERDWR_H
INT4 GetNextIndexFsSerdesTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSSERD(VOID);

VOID UnRegisterFSSERD(VOID);
INT4 FsSerdesValueGet(tSnmpIndex *, tRetVal *);
INT4 FsSerdesValueSet(tSnmpIndex *, tRetVal *);
INT4 FsSerdesValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSerdesTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSSERDWR_H */
