/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fssemacs.h,v 1.1 2010/11/02 14:02:55 prabuc Exp $
*
*  Description:
*
********************************************************************/

#ifndef FS_MACS_H
#define FS_MACS_H
#define PHY_CTL_PRE_EMPHASIS       1
#define PHY_CTL_DRIVER_CURRENT     2
#define PHY_CTL_PRE_DRIVER_CURRENT 3
#define MIN_PHY_CONTROL_VALUE      0
#define MAX_PHY_CONTROL_VALUE      0xFFFF
#endif
