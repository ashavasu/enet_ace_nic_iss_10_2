/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fssetdfs.h,v 1.1 2010/11/02 14:02:55 prabuc Exp $
*
*  Description:
*
********************************************************************/

#ifndef _FSTDFS_H
#define _FSTDFS_H

typedef struct {
  UINT4       u4preemphasis;
  UINT4       u4predriver_current;
  UINT4       u4driver_current;
}tSerDesInfo;

#endif
