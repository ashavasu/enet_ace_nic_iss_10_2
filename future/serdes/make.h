
##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                                                   # ------------------------------------------
# $Id: make.h,v 1.1 2010/11/02 14:02:32 prabuc Exp $
#   DESCRIPTION            : The makefile for builing the complete Linux
#                            Router.                                                                     ##########################################################################


#!/bin/csh
# (C) 2001 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 06/17/2005                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the SERDES module.     |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# +--------------------------------------------------------------------------+



# Set the SERDES_BASE_DIR as the directory where you untar the project files

SERDES_NAME		= FutureSERDES

SERDES_BASE_DIR	   = ${BASE_DIR}/serdes

SERDES_SRC_DIR		= ${SERDES_BASE_DIR}/src
SERDES_CO_INC_DIR   = ${SERDES_BASE_DIR}/inc

SERDES_OBJ_DIR		= ${SERDES_BASE_DIR}/obj


# Specify the project level compilation switches here

ifeq (${NPAPI}, YES)
SERDES_COMPILATION_SWITCHES += -DNPAPI_WANTED
endif

# Specify the project include directories and dependencies
SERDES_CO_INC_FILES	= $(SERDES_CO_INC_DIR)/fsserinc.h \
                      $(SERDES_CO_INC_DIR)/fsserdes.h \
                      $(SERDES_CO_INC_DIR)/fsserdlw.h \
                      $(SERDES_CO_INC_DIR)/fsserdwr.h 

SERDES_ALL_INC_FILES	= $(SERDES_CO_INC_FILES)

SERDES_INC_DIR		= -I$(SERDES_CO_INC_DIR)


SERDES_FINAL_INCLUDES_DIRS	= $(SERDES_INC_DIR) \
				  $(COMMON_INCLUDE_DIRS)

SERDES_DEPENDENCIES	= $(COMMON_DEPENDENCIES) \
				$(SERDES_ALL_INC_FILES) \
				$(SERDES_BASE_DIR)/Makefile \
				$(SERDES_BASE_DIR)/make.h

