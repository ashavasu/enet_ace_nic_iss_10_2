
/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fsserdes.c,v 1.1 2010/11/02 14:03:13 prabuc Exp $
*
*  Description:
*
********************************************************************/

/*
 *
 *  -------------------------------------------------------------------------
 * |  FILE  NAME             :  fsserdes.c                                   |
 * |                                                                         |
 * |  PRINCIPAL AUTHOR       :  Future Software                              |
 * |                                                                         |
 * |  SUBSYSTEM NAME         :  SERDES                                       |
 * |                                                                         |
 * |  MODULE NAME            :  COMMON                                       |
 * |                                                                         |
 * |  LANGUAGE               :  C                                            |
 * |                                                                         |
 * |  TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                         |
 * |  DATE OF FIRST RELEASE  :                                               |
 * |                                                                         |
 * |  DESCRIPTION            :   This file provides routines called          |
 * |                             by low level routines to read/write OID     |
 * |                             and to get the value of an object given     |
 * |                             the OID.                                    |
 *  -------------------------------------------------------------------------
 *
 *     CHANGE RECORD :
 *     ---------------
 *
 *  -------------------------------------------------------------------------
 * |  VERSION       |        AUTHOR/      |          DESCRIPTION OF          |
 * |                |        DATE         |             CHANGE               |
 * |----------------|---------------------|----------------------------------|
 * |  1.0           |    SUNDHAR          |         Create Original          |
 * |                |    11-AUG-2010      |                                  |
 *  -------------------------------------------------------------------------
 *
 */
#define _SERDES_C_
#include "fsserinc.h"

tSerDesInfo SerDesInfo[SYS_DEF_MAX_PHYSICAL_INTERFACES];
tOsixSemId          gSerdesSemId;

/******************************************************************************
*      Function Name        : InitHwtest                                      *
*                                                                             *
*      Role of the function : Initializes hwtest global variables             *
*      Formal Parameters    : None                                            *
*       Global Variables    : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
VOID
InitSerdes (VOID)
{
    MEMSET(SerDesInfo,0,sizeof(tSerDesInfo));
    if (OsixCreateSem (SERDES_PROTOCOL_SEM, 1, 0, &gSerdesSemId) != OSIX_SUCCESS)
    {
        PRINTF("Failed to Create Semaphore ..\n");
        return;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : SerdesLock                                           */
/*                                                                           */
/* Description        : This function is used to take the Hwtest mutual      */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SERDES_SUCCESS or SERDES_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
INT4
SerdesLock (VOID)
{
    if (OsixSemTake (gSerdesSemId) != OSIX_SUCCESS)
    {
        PRINTF ("Failed to take Serdes sem");
        return SERDES_FAILURE;
    }
    return SERDES_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : SerDesUnLock                                         */
/*                                                                           */
/* Description        : This function is used to give the Hwtest mutual      */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SERDES_SUCCESS or SERDES_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
INT4
SerdesUnLock (VOID)
{
    if (OsixSemGive (gSerdesSemId))
    {
        PRINTF( "Failed to release HWTEST sem");
        return SERDES_FAILURE;
    }
    return SERDES_SUCCESS;
} 
