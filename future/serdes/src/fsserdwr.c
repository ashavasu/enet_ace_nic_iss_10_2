
/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fsserdwr.c,v 1.1 2010/11/02 14:03:13 prabuc Exp $
*
*  Description:
*
********************************************************************/

# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fsserdlw.h"
# include  "fsserdwr.h"
# include  "fsserddb.h"

INT4 GetNextIndexFsSerdesTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsSerdesTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsSerdesTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterFSSERD ()
{
	SNMPRegisterMib (&fsserdOID, &fsserdEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fsserdOID, (const UINT1 *) "fsserdes");
}



VOID UnRegisterFSSERD ()
{
	SNMPUnRegisterMib (&fsserdOID, &fsserdEntry);
	SNMPDelSysorEntry (&fsserdOID, (const UINT1 *) "fsserdes");
}

INT4 FsSerdesValueGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsSerdesTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsSerdesValue(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsSerdesValueSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsSerdesValue(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsSerdesValueTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsSerdesValue(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsSerdesTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsSerdesTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

