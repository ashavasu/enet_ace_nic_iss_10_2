/********************************************************************                                    *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fssercli.c,v 1.1 2010/11/02 14:03:13 prabuc Exp $
*
*  Description:
*
********************************************************************/


/* SOURCE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) Future Software, 2001-2001                                 |
 * |  Licensee Future Communications Software, 2001-2001                       |
 * |                                                                           |
 * |  FILE NAME                   :  fssercli.c                                |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Future Software Ltd                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  SERDES                                    |
 * |                                                                           |
 * |  MODULE NAME                 :  hwtest                                    |
 * |                                                                           |
 * |  LANGUAGE                    :   C                                        |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :   Linux                                    |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  This file contains the Protocol Action    |
 * |                                 routines for cli SET/GET destined         |
 * |                                 for SERDES Module CLI Commands.           |
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */
/*     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
 * |         | DATE       |                                                   |
 * +---------|------------|---------------------------------------------------+
 * |   1     | SUNDHAR    |                                                   |
 * |         | 11/08/2010 | Creation of fssercli.c for SERDES CLI commands    |
 * +--------------------------------------------------------------------------+
 */

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/
#define  __FSSERCLI_C__
#include "fsserinc.h"
#include  "cli.h"
#include  "sercli.h"
#include  "fsserdcli.h"
/*********************************************************************/
/*  Function Name : cli_process_serdes_cmd                             */
/*  Description   : This function is called by commands in Command   */
/*                  definition files.This function retrieves all     */
/*                  Input parameters from the command and assigns to */
/*                  Input Message structure for the Protocol Action  */
/*                  Routines.                                        */
/*                                                                   */
/*  Input(s)      : va_alist - Variable no. of arguments             */
/*  Output(s)     : NONE                                             */
/*                                                                   */
/*  Return Values : None.                                            */
/*********************************************************************/

void
cli_process_serdes_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[16];
    INT1                argno = 0;
    UINT4               u4Index = 0;
    UINT4               u4ErrCode = 0;
    /* Maximum no. of Arguments for SERDES commands */
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PhyControlType = 0;
    UINT4               u4PhyControlValue = 0;
    /* Variable Argument extraction */
    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    /* Walk through the list of the arguments and store in args array. */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno ==  SERDES_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);
    CliRegisterLock (CliHandle, SerdesLock, SerdesUnLock);
    SERDES_LOCK ();
    switch (u4Command)
    {
        case CLI_SERDES_PHY_CTL_SET:

          /*****************************
          * args[0] - PhyControl Type *
          * args[1] - Value           *
          *****************************/

		u4IfIndex =       CLI_GET_IFINDEX ();
		u4PhyControlType = * (UINT4 *)args[0];
		u4PhyControlValue = (UINT1) CLI_HEXSTRTOUL(args[1]);
		i4RetStatus =
			SerDefSetPhyControl(CliHandle,u4IfIndex,
                                u4PhyControlType,u4PhyControlValue);
		break;

	case CLI_SERDES_PHY_CTL_SHOW:

         /****************************
          * args[0] - IfIndex        *
          * 
          * *************************/

		u4IfIndex = *(UINT4 *)args[0];

		i4RetStatus = SerDefShowPhyControl(CliHandle,u4IfIndex);

		break;

	default:

		CliPrintf (CliHandle, "\r\nInvalid Command\r\n");
		SERDES_UNLOCK ();
		return;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SERDES_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r\n%s", SerdesCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }


     CliUnRegisterLock (CliHandle);

     SERDES_UNLOCK ();
}

/*************************************************************************/
/*  Function Name : SerDefSetPhyControl                                  */
/*  Description   :  This function sets Phy control value for each port  */
/*                                                                       */
/*                                                                       */
/*  Input(s)      : CliHandle                                            */
/*                  u4IfIndex        - Interface Index                   */
/*                  u4PhyControlType - PhyControlType                    */
/*                  u4PhyControlValue - PhyControlalue                   */
/*                                                                       */
/*  Output(s)     :  CLI_SUCCESS/CLI_FAILURE                             */
/*************************************************************************/
INT4
SerDefSetPhyControl(tCliHandle CliHandle,
                 UINT4      u4IfIndex,
                 UINT4      u4PhyControlType,
                 UINT4      u4PhyControlValue
                )
{

  INT4   i4RetVal;
  UINT4  u4ErrorCode = 0;
  i4RetVal = nmhTestv2FsSerdesValue (&u4ErrorCode,u4IfIndex,u4PhyControlType,u4PhyControlValue);
  if(i4RetVal == SNMP_FAILURE)
  {
      return CLI_FAILURE;
  }

  i4RetVal = nmhSetFsSerdesValue(u4IfIndex,u4PhyControlType,u4PhyControlValue);
  if(i4RetVal == SNMP_FAILURE)
  {
      CLI_FATAL_ERROR (CliHandle);
       return (CLI_FAILURE);
  }

  return CLI_SUCCESS;
}

/*************************************************************************/
/*  Function Name : SerDefShowPhyControl                                  */
/*                                                                       */
/*  Description   :  This function displaus Phy control value for each   */
/*                   Port                                                */
/*                                                                       */
/*                                                                       */
/*  Input(s)      : CliHandle                                            */                              /*                                                                       */
/*                  u4IfIdex -Interface Index                            */
/*                                                                       */
/*                                                                       */
/*  Output(s)     :  CLI_SUCCESS/CLI_FAILURE                             */
/*************************************************************************/

INT4
SerDefShowPhyControl (tCliHandle CliHandle,
                   UINT4      u4IfIndex
                  )
{

    UINT4 u4PhyControlType = 0;
    UINT4 u4PhyControlValue = 0;
    UINT4 u4PrevIfIndex = 0;
    UINT4 u4PrevPhyControlType = 0;
    UINT4 u4Counter = 0;
    INT1  i1RetVal = 0;
    INT1  *piIfName;
    UINT1  au1IfName[CFA_MAX_PORT_NAME_LENGTH];
     CliPrintf (CliHandle, "\r\n%-13s%-18s%-18s%-20s\r\n",
                "Interface","Pre-emphasis","Driver-Current","Pre-Driver-Current");
     CliPrintf (CliHandle, "%-13s%-18s%-18s%-20s",
                "---------","------------","--------------","------------------");
    if(u4IfIndex == 0)
    {
       i1RetVal = nmhGetFirstIndexFsSerdesTable(&u4IfIndex,&u4PhyControlType);
       if(i1RetVal == SNMP_FAILURE)
       {
           return CLI_FAILURE;
       }
       do
       {
           MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
           piIfName = (INT1 *) &au1IfName[0];

           nmhGetFsSerdesValue(u4IfIndex,u4PhyControlType,&u4PhyControlValue);
           if(u4PrevIfIndex != u4IfIndex)
           {
               CfaCliGetIfName ((UINT4) u4IfIndex, piIfName);
               CliPrintf(CliHandle,"\r\n%-13s",piIfName);
           }
           CliPrintf(CliHandle,"0x%02x  \t\t  ",u4PhyControlValue);
           u4PrevIfIndex = u4IfIndex;
           u4PrevPhyControlType = u4PhyControlType;

           i1RetVal = nmhGetNextIndexFsSerdesTable(u4PrevIfIndex,&u4IfIndex,
                                                       u4PrevPhyControlType,&u4PhyControlType);
        }while(i1RetVal != SNMP_FAILURE);

           CliPrintf(CliHandle,"\r\n");

       return CLI_SUCCESS;
    }
    else
    {
          MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
          piIfName = (INT1 *) &au1IfName[0];
          CfaCliGetIfName ((UINT4) u4IfIndex, piIfName);
          CliPrintf(CliHandle,"\r\n%-13s",piIfName);
          for(u4Counter = 0;u4Counter < PHY_CTL_PRE_DRIVER_CURRENT ;
              u4Counter ++)
          {
              u4PhyControlType = u4PhyControlType+1;
              nmhGetFsSerdesValue(u4IfIndex,u4PhyControlType,&u4PhyControlValue);
              CliPrintf(CliHandle,"0x%02x  \t\t  ",u4PhyControlValue);
          }

          CliPrintf(CliHandle,"\r\n");
          return CLI_SUCCESS;
     }


 }







