/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsserdlw.c,v 1.1 2010/11/02 14:03:13 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h" 
#include  "fssnmp.h" 
#include  "fsserinc.h"
#include  "serdes.h"
#include  "fsserdlw.h"
#include  "sercli.h"
#include  "iss.h"
#include  "issnp.h"
#include  "npapi.h"

/* LOW LEVEL Routines for Table : CwSerDesTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSerdesTable
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsSerdesTable(INT4 i4FsSerdesIfIndex , INT4 i4FsSerdesType)
{
    UINT4 u4TotalPorts = 0;
    u4TotalPorts = IssGetFrontPanelPortCountFromNvRam();
    if (i4FsSerdesIfIndex > u4TotalPorts)
    {
        CLI_SET_ERR (CLI_SERDES_INVALID_PORT);
        return SNMP_FAILURE;
    }
    
    if((i4FsSerdesType != PHY_CTL_PRE_EMPHASIS) && (i4FsSerdesType != PHY_CTL_DRIVER_CURRENT)
       && (i4FsSerdesType != PHY_CTL_PRE_DRIVER_CURRENT))
    {
        CLI_SET_ERR (CLI_SERDES_INVALID_PHY_CTL_TYPE);
         return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsSerdesTable
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsSerdesTable(INT4 *pi4FsSerdesIfIndex , INT4 *pi4FsSerdesType)
{
    INT1 i1RetVal;
    INT4 i4FirstFsSerDesIfIndex = 0;
    INT4 i4FirstFsSerDesType = 0;
    i1RetVal = nmhGetNextIndexFsSerdesTable(i4FirstFsSerDesIfIndex,pi4FsSerdesIfIndex,
                                            i4FirstFsSerDesType,pi4FsSerdesType);
    if(i1RetVal == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsSerdesTable
 Input       :  The Indices
                CwSerDesIfIndex
                nextCwSerDesIfIndex
                CwSerDesType
                nextCwSerDesType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsSerdesTable(INT4 i4FsSerdesIfIndex ,INT4 *pi4NextFsSerdesIfIndex  , INT4 i4FsSerdesType ,INT4 *pi4NextFsSerdesType )
{
    UINT4 u4TotalPorts = 0;
    UINT4 u4IfIndex = 0;
    INT4  i4SerdesType = PHY_CTL_PRE_EMPHASIS;
    INT4  *i4SerdesValue;
    u4TotalPorts = IssGetFrontPanelPortCountFromNvRam();
    if (i4FsSerdesIfIndex > u4TotalPorts)
    {
        CLI_SET_ERR (CLI_SERDES_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if(i4FsSerdesIfIndex == 0 && i4FsSerdesType == 0)
    {
            u4IfIndex = (UINT4)i4FsSerdesIfIndex;
            for(u4IfIndex = u4IfIndex +1;u4IfIndex <= u4TotalPorts; u4IfIndex++)
            {
                if(CfaValidateIfIndex(u4IfIndex) == CFA_SUCCESS &&
                   nmhGetFsSerdesValue(u4IfIndex,i4SerdesType,i4SerdesValue) == SNMP_SUCCESS)
                {
                    *pi4NextFsSerdesIfIndex = u4IfIndex;
                    *pi4NextFsSerdesType = PHY_CTL_PRE_EMPHASIS;
                     return SNMP_SUCCESS;
                }
            }

                return SNMP_FAILURE;
    }
    else
    {
        i4FsSerdesType = i4FsSerdesType +1;
        if(i4FsSerdesType <= PHY_CTL_PRE_DRIVER_CURRENT)
        {
            *pi4NextFsSerdesIfIndex = i4FsSerdesIfIndex;
            *pi4NextFsSerdesType = i4FsSerdesType;
             return SNMP_SUCCESS;
        }
        else
        {
            u4IfIndex = (UINT4)i4FsSerdesIfIndex;
            for(u4IfIndex = u4IfIndex +1;u4IfIndex <= u4TotalPorts; u4IfIndex++)
            {
                if(CfaValidateIfIndex(u4IfIndex) == CFA_SUCCESS)
                {
                    *pi4NextFsSerdesIfIndex = u4IfIndex;
                    *pi4NextFsSerdesType = PHY_CTL_PRE_EMPHASIS;
                     return SNMP_SUCCESS;
                }
            }
           return SNMP_FAILURE;
        }
    }

}




/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSerdesValue
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType

                The Object 
                retValCwSerDesValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsSerdesValue(INT4 i4FsSerdesIfIndex , INT4 i4FsSerdesType , INT4 *pi4RetValFsSerdesValue)
{

    INT4 i4RetVal = FNP_SUCCESS ;
    switch(i4FsSerdesType)
    {
        case PHY_CTL_PRE_EMPHASIS:
        #ifdef NPAPI_WANTED
             i4RetVal = IssNpGetPhyCtlValue(i4FsSerdesIfIndex,
                                          i4FsSerdesType,
                                          &(SerDesInfo[i4FsSerdesIfIndex].u4preemphasis));
         #endif
             if(i4RetVal == FNP_SUCCESS)
             {
                 *pi4RetValFsSerdesValue =  SerDesInfo[i4FsSerdesIfIndex].
                                                u4preemphasis;
                 return SNMP_SUCCESS;
             }
             break;
        case PHY_CTL_DRIVER_CURRENT:
        #ifdef NPAPI_WANTED
             i4RetVal = IssNpGetPhyCtlValue(i4FsSerdesIfIndex,
                                         i4FsSerdesType,
                                         &(SerDesInfo[i4FsSerdesIfIndex].
                                           u4predriver_current));
        #endif
             if(i4RetVal == FNP_SUCCESS)
             {
                 *pi4RetValFsSerdesValue =  SerDesInfo[i4FsSerdesIfIndex].
                                                u4predriver_current;
                 return SNMP_SUCCESS;
             }
             break;

        case PHY_CTL_PRE_DRIVER_CURRENT:
        #ifdef NPAPI_WANTED

             i4RetVal = IssNpGetPhyCtlValue(i4FsSerdesIfIndex,
                                          i4FsSerdesType,
                                         &( SerDesInfo[i4FsSerdesIfIndex].
                                           u4driver_current));
         #endif
             if(i4RetVal == FNP_SUCCESS)
             {
                 *pi4RetValFsSerdesValue =  SerDesInfo[i4FsSerdesIfIndex].
                                               u4driver_current;
                 return SNMP_SUCCESS;
             }
             break;
    }
              return SNMP_FAILURE;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSerdesValue
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType

                The Object 
                setValCwSerDesValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsSerdesValue(INT4 i4FsSerdesIfIndex , INT4 i4FsSerdesType , INT4 i4SetValFsSerdesValue)
{
    INT4 i4RetVal;
#ifdef NPAPI_WANTED
    i4RetVal = IssNpSetPhyCtlValue(i4FsSerdesIfIndex,i4FsSerdesType,
                                 i4SetValFsSerdesValue);
#endif
    if(i4RetVal != FNP_SUCCESS)
    {
         return SNMP_FAILURE;
    }
    else
    {
        switch(i4FsSerdesType)
        {
            case PHY_CTL_PRE_EMPHASIS:

                SerDesInfo[i4FsSerdesIfIndex].u4preemphasis =
                    i4SetValFsSerdesValue;
                break;

            case PHY_CTL_DRIVER_CURRENT:

                SerDesInfo[i4FsSerdesIfIndex].u4predriver_current=
                    i4SetValFsSerdesValue;
                break;

            case PHY_CTL_PRE_DRIVER_CURRENT:

                SerDesInfo[i4FsSerdesIfIndex].u4driver_current =
                    i4SetValFsSerdesValue;
                break;
        }
        return SNMP_SUCCESS;
    }

}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSerdesValue
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType

                The Object 
                testValCwSerDesValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsSerdesValue(UINT4 *pu4ErrorCode , INT4 i4FsSerdesIfIndex , INT4 i4FsSerdesType , INT4 i4TestValFsSerdesValue)
{
    UINT4 u4TotalPorts = 0;
    u4TotalPorts = IssGetFrontPanelPortCountFromNvRam();
    if (i4FsSerdesIfIndex > u4TotalPorts)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SERDES_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if((i4FsSerdesType != PHY_CTL_PRE_EMPHASIS) && (i4FsSerdesType != PHY_CTL_DRIVER_CURRENT)
       && (i4FsSerdesType != PHY_CTL_PRE_DRIVER_CURRENT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SERDES_INVALID_PHY_CTL_TYPE);
         return SNMP_FAILURE;
    }
    /*Allowed Values for PHY CONTROL TYPES***
     * GE PORTS
     * --------
     * PREEMPHASIS : 0-7
     * DRIVERCURRENT : 0-F
     * PREDRIVERCURRENT: 0-7
     *
     * XE PORTS
     * --------
     * PREEMPHASIS : 0-F
     * DRIVERCURRENT : 0-F
     * PREDRIVERCURRENT: 0-F
     * **********************************/
    if(i4FsSerdesIfIndex <= MAX_GE_INTERFACES)
    {
        if(i4FsSerdesType != PHY_CTL_DRIVER_CURRENT)
        {
            if((i4TestValFsSerdesValue < MIN_PHY_CONTROL_VALUE) ||
               (i4TestValFsSerdesValue > MAX_PRE_VALUE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                UtlTrcPrint("\nTrying to configure Wrong value for SERDES Interface  for GE Ports\n");
                CLI_SET_ERR (CLI_SERDES_INVALID_PHY_CTL_VALUE);
                return SNMP_FAILURE;
            }
        }
        else
        {

            if((i4TestValFsSerdesValue < MIN_PHY_CONTROL_VALUE) ||
               (i4TestValFsSerdesValue > MAX_PHY_CONTROL_VALUE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                UtlTrcPrint("\nTrying to configure Wrong value for SERDES Interface for GE ports\n");
                CLI_SET_ERR (CLI_SERDES_INVALID_PHY_CTL_DRI_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
          if((i4TestValFsSerdesValue < MIN_PHY_CONTROL_VALUE) ||
               (i4TestValFsSerdesValue > MAX_PHY_CONTROL_VALUE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                UtlTrcPrint("\nTrying to configure Wrong value for SERDES Interface for XE ports\n");
                CLI_SET_ERR (CLI_SERDES_INVALID_PHY_CTL_XE_VALUE);
                return SNMP_FAILURE;
            }
    }

    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSerdesTable
 Input       :  The Indices
                CwSerDesIfIndex
                CwSerDesType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsSerdesTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
