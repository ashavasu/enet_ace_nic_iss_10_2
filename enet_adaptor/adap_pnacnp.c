/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_pnacnp.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_enetConvert.h"
#include "EnetHal_L2_Api.h"

static MEA_Bool bPnacHwEnable=MEA_FALSE;

ADAP_Int32  EnetHal_PnacHwEnable (void)
{
	bPnacHwEnable=MEA_TRUE;
    return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_PnacHwDisable (void)
{
	bPnacHwEnable=MEA_FALSE;
    return ENET_SUCCESS;
}

ADAP_Int32  EnetHal_PnacHwSetAuthStatus (ADAP_Uint16 u2PortNum, tEnetHal_MacAddr *pu1SuppAddr, ADAP_Uint8 u1AuthMode, ADAP_Uint8 u1AuthStatus, ADAP_Uint8 u1CtrlDir)
{
    MEA_Filter_t 					FilterId;
    MEA_OutPorts_Entry_dbt          Action_outPorts;
    ADAP_Uint32 					IssMasterPortId;
    ADAP_Uint8						isPortTagged;
    ADAP_Uint16						VlanId;
    tPnacAutorizedDb				*pnacDb = NULL;
	tBridgeDomain 			    	*pBridgeDomain=NULL;
	tServiceDb 						*pServiceId=NULL;

    pnacDb = adap_getPnacAutorized(u2PortNum);
    if(pnacDb != NULL)
    {
    	pnacDb->u1AuthMode=u1AuthMode;
    	pnacDb->u1AuthStatus=u1AuthStatus;
    	pnacDb->u1CtrlDir=u1CtrlDir;

    	if(pu1SuppAddr != NULL)
    	{
    		adap_memcpy(&pnacDb->u1SuppAddr,pu1SuppAddr,sizeof(tEnetHal_MacAddr));
    	}
    }

    if(pu1SuppAddr != NULL)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"mac:%s\n", adap_mac_to_string(pu1SuppAddr));
    }

    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	IssMasterPortId = u2PortNum;
	if(adap_IsPortChannel(IssMasterPortId) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(IssMasterPortId) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"IfIndex:%d is not port channel\n", u2PortNum);
			return ENET_SUCCESS;
		}
	}

	MeaAdapSetIngressInterfaceType (IssMasterPortId,MEA_FILTER_KEY_INTERFACE_TYPE_SID);
	if( (u1AuthStatus == ADAP_PNAC_PORTSTATUS_AUTHORIZED) && (u1AuthMode == ADAP_PNAC_PORT_AUTHMODE_MACBASED) && (pu1SuppAddr != NULL ) )
	{
		MeaAdapSetIngressInterfaceType (IssMasterPortId,MEA_FILTER_KEY_INTERFACE_TYPE_SOURCE_PORT);
		if(MeaDrvPortMacSetFilterSrcMacAddress(MEA_PLAT_GENERATE_NEW_ID,IssMasterPortId,1,1,pu1SuppAddr,MEA_EGRESS_HEADER_PROC_CMD_TRANS,&FilterId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create filter\r\n");
		}
	}

	if( (u1AuthMode == ADAP_PNAC_PORT_AUTHMODE_PORTBASED) && (u1AuthStatus == ADAP_PNAC_PORTSTATUS_AUTHORIZED) )
	{
		MeaDrvSetBpdu3Lxcp(IssMasterPortId,MEA_LXCP_PROTOCOL_ACTION_CPU);
	}

	for(VlanId=1;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
	{
		Adap_bridgeDomain_semLock();
		pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		if( pBridgeDomain == NULL)
		{
			Adap_bridgeDomain_semUnlock();
			continue;
		}

		for(isPortTagged=MEA_PARSING_L2_KEY_Untagged; isPortTagged<MEA_PARSING_L2_KEY_Ctag_Ctag; isPortTagged++)
		{
			pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,IssMasterPortId,isPortTagged);
			if(pServiceId != NULL)
			{
				MeaDrvVlanHwUpdatePnac(pServiceId);
			}
		}
		Adap_bridgeDomain_semUnlock();
	}
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"PnacHwSetAuthStatus u2PortNum:%d\n",u2PortNum);

    MeaDrvSetPort802Dot1Lxcp(IssMasterPortId,MEA_LXCP_PROTOCOL_ACTION_CPU);
    MeaDrvSetBpdu3Lxcp(IssMasterPortId,MEA_LXCP_PROTOCOL_ACTION_CPU);

    return ENET_SUCCESS;
}
