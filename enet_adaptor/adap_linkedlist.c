/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#include <stdlib.h>
#include <pthread.h>
#include "adap_linkedlist.h"
#include "adap_utils.h"

struct adap_linkedlist {
	adap_linkedlist_element *head;
	adap_linkedlist_element *tail;

	int (*compare)(adap_linkedlist_element* element1, adap_linkedlist_element* element2);

	ADAP_Bool thread_safe;
    pthread_mutex_t mutex;
    ADAP_Uint32 capacity;
    ADAP_Uint32 size;
};

EnetHal_Status_t adap_linkedlist_create(adap_linkedlist** list, adap_linkedlist_params *params)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	adap_linkedlist * linkedlist = NULL;
	int err;

    if ((params == NULL) || (list == NULL)) {
        rc = ENETHAL_STATUS_NULL_PARAM;
        goto bail;
    }

    linkedlist = (adap_linkedlist *)adap_malloc(sizeof(adap_linkedlist));
    if (linkedlist == NULL) {
        rc = ENETHAL_STATUS_MEM_ALLOC;
        goto bail;
    }
    
    linkedlist->head = NULL;
    linkedlist->tail = NULL;

    if (params->thread_safe) {
		err = pthread_mutex_init(&(linkedlist->mutex),NULL);
		if (err != 0) {
			rc = ENETHAL_STATUS_GENERIC_ERROR;
			goto bail;
		}
    }

    linkedlist->compare = params->compare;
    linkedlist->thread_safe = params->thread_safe;
    linkedlist->capacity = params->capacity;
    linkedlist->size = 0;

    *list = linkedlist;
    linkedlist = NULL;

bail:
	adap_safe_free(linkedlist);
    return rc;
}

EnetHal_Status_t adap_linkedlist_destroy(adap_linkedlist* list)
{
	EnetHal_Status_t rc = ENETHAL_STATUS_SUCCESS;
	int err;

    if (list == NULL) {
        rc = ENETHAL_STATUS_NULL_PARAM;
        goto bail;
    }

    if (list->size != 0) {
        rc = ENETHAL_STATUS_NOT_SUPPORTED;
        goto bail;
    }

    if (list->thread_safe) {
		err = pthread_mutex_destroy(&(list->mutex));
		if (err != 0) {
			rc = ENETHAL_STATUS_GENERIC_ERROR;
			goto bail;
		}
    }

    adap_safe_free(list);

bail:
  	return rc;
}


EnetHal_Status_t adap_linkedlist_add_first(adap_linkedlist* list, adap_linkedlist_element* element)
{
    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if ((list->capacity > 0) && (list->size == list->capacity)) {
    	return ENETHAL_STATUS_LIMIT_REACHED;
    }

    element->prev = NULL;
    if (list->size == 0) {
    	element->next = NULL;
    	list->tail = element;
    } else {
    	element->next = list->head;
    }
    list->head = element;

    list->size++;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_linkedlist_add_last(adap_linkedlist* list, adap_linkedlist_element* element)
{
    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if ((list->capacity > 0) && (list->size == list->capacity)) {
    	return ENETHAL_STATUS_LIMIT_REACHED;
    }

    element->next = NULL;
    if (list->size == 0) {
    	element->prev = NULL;
    	list->head = element;
    } else {
    	element->prev = list->tail;
    }
    list->tail = element;

    list->size++;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_linkedlist_remove_first(adap_linkedlist* list, adap_linkedlist_element** element)
{
    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (list->size == 0) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    *element = list->head;

    if (list->size == 1) {
    	list->head = NULL;
    	list->tail = NULL;
    } else {
    	list->head = list->head->next;
    }

    list->size--;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_linkedlist_remove_last(adap_linkedlist* list, adap_linkedlist_element** element)
{
    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (list->size == 0) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    *element = list->tail;
    if (list->size == 1) {
    	list->head = NULL;
    	list->tail = NULL;
    } else {
    	list->tail = list->head->prev;
    }

    list->size--;

    return ENETHAL_STATUS_SUCCESS;
}

EnetHal_Status_t adap_linkedlist_remove(adap_linkedlist* list, adap_linkedlist_element* element)
{
	adap_linkedlist_element * current;

    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (list->size == 0) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    if (element == list->head) {
        if (list->size == 1) {
        	list->head = NULL;
        	list->tail = NULL;
        } else {
        	list->head = list->head->next;
        }
        list->size--;
        return ENETHAL_STATUS_SUCCESS;
    }

    if (element == list->tail) {
    	list->tail = list->tail->prev;
    	list->size--;
        return ENETHAL_STATUS_SUCCESS;
    }

   	current = list->head;
    while (current != NULL) {
    	if (current == element) {
    		current->next->prev = current->prev;
    		current->prev->next = current->next;
    		list->size--;
    		return ENETHAL_STATUS_SUCCESS;
    	}
    }

    return ENETHAL_STATUS_NOT_FOUND;
}

ADAP_Uint32 adap_linkedlist_size(adap_linkedlist* list)
{
    if (list == NULL) {
        return 0;
    }

    return list->size;
}

EnetHal_Status_t adap_linkedlist_iter(adap_linkedlist *list, adap_linkedlist_element** element)
{
    if ((list == NULL) || (element == NULL)) {
        return ENETHAL_STATUS_NULL_PARAM;
    }

    if (list->size == 0) {
    	return ENETHAL_STATUS_NOT_FOUND;
    }

    if (*element == NULL) {
    	*element = list->head;
    } else {
    	if (*element == list->tail) {
    		return ENETHAL_STATUS_NOT_FOUND;
    	} else {
    		*element = (*element)->next;
    	}
    }

	return ENETHAL_STATUS_SUCCESS;
}
