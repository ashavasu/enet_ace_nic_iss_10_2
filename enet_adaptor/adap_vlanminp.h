/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_VLANMINP_H
#define ADAP_VLANMINP_H

#include "EnetHal_L2_Api.h"

ADAP_Int32 adap_ReconfigureHwVlan(tEnetHal_VlanId VlanId,ADAP_Uint32 u4ContextId);
ADAP_Int32 adap_VlanHwUpdatePriorityVlan(tEnetHal_VlanId primaryVlan, ADAP_Uint32 u4IfIndex,tAdap_PortConfigSetting *pPortArry, eAdap_EditPriorityTag type,int bLearning, ADAP_Uint16 seconderyVlan);
ADAP_Int32 adap_ReconfigureVlanPriority(ADAP_Uint32 u4IfIndex);
ADAP_Int32 adap_ConfigPb(tEnetHal_VlanId primaryVlan);

ADAP_Uint32 adap_SetLimiterIdToPort(ADAP_Uint32 u4IfIndex, MEA_Limiter_t limiterId);
ADAP_Uint32 adap_UpdateMacLimiterProfile(MEA_Limiter_t limiterId, ADAP_Uint32 limit, MEA_Limit_Action_te action_type);
ADAP_Uint32 adap_CreateMacLimiterProfile(ADAP_Uint32 limit, MEA_Limit_Action_te action_type, MEA_Limiter_t *pLimiterId);
#endif
