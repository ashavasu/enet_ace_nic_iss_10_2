/*
 * adap_rbtree.h
 *
 *  Created on: Sep 16, 2018
 *      Author: devops
 */
#ifndef __ADAP_RBTREE_H__
#define __ADAP_RBTREE_H__

#include "adap_types.h"
#include "EnetHal_status.h"

typedef struct adap_rbtree adap_rbtree;

typedef struct adap_rbtree_element {  /* TODO - need to hide the struct content in some way */
	ADAP_Bool red;
	struct adap_rbtree_element *left;
	struct adap_rbtree_element *right;
	struct adap_rbtree_element *parent;
} adap_rbtree_element;

struct adap_rbtree_params {
	ADAP_Uint32 capacity;
	int (*compare)(adap_rbtree_element* element1, adap_rbtree_element* element2);
	void (*swap)(adap_rbtree_element* element1, adap_rbtree_element* element2);
};


/**
 * @brief Create a new Red-Black tree with given parameters.
 * @return
 */
EnetHal_Status_t adap_rbtree_init(adap_rbtree** tree, struct adap_rbtree_params *params);


/**
 * @brief Destroy Red-Black tree
 * @return
 */
EnetHal_Status_t adap_rbtree_deinit(adap_rbtree* tree);


/**
 * @brief Insert {key,value} to Red-Black tree
 * @return
 */
EnetHal_Status_t adap_rbtree_insert(adap_rbtree* tree, adap_rbtree_element* element);


/**
 * @brief Remove {key,value} from Red-Black tree
 * @return
 */
EnetHal_Status_t adap_rbtree_remove(adap_rbtree* tree, adap_rbtree_element* c_element, adap_rbtree_element** element);


/**
 * @brief Find {key,value} in Red-Black tree
 * @return
 */
EnetHal_Status_t adap_rbtree_find(adap_rbtree* tree, adap_rbtree_element* c_element, adap_rbtree_element **element);


/**
 * @brief Iterate {key,value} a Red-Black tree
 * @return
 */
EnetHal_Status_t adap_rbtree_iter(adap_rbtree* tree, adap_rbtree_element* c_element, adap_rbtree_element **element);


/**
 * @brief Get number of {key,value} in Red-Black tree
 * @return
 */
ADAP_Uint32 adap_rbtree_size(adap_rbtree* tree);


#endif /* __ADAP_RBTREE_H__ */
