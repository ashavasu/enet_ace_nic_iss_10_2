/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_stp.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_enetConvert.h"
#include "EnetHal_L2_Api.h"

static MEA_Action_t  gAction_id=MEA_PLAT_GENERATE_NEW_ID;

static tEnetHal_MacAddr gMac_mvrp_all_routes = {.ether_addr_octet = {0x01,0x80,0xC2,0x00,0x00,0x0D}};
static tEnetHal_MacAddr gMac_gmrp_all_routes = {.ether_addr_octet = {0x01,0x80,0xC2,0x00,0x00,0x20}};
static tEnetHal_MacAddr gMac_gvrp_all_routes = {.ether_addr_octet = {0x01,0x80,0xC2,0x00,0x00,0x21}};

static tEnetHal_MacAddr APS_dest_mac = {.ether_addr_octet = {0x01,0x19,0xA7,0x00,0x00,0x01}};

static ADAP_Uint8 insert_mac_to_forwarder[ADAP_NUM_OF_SUPPORTED_VLANS];

ADAP_Uint8   adapBitMaskMap[ADAP_BITS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

ADAP_Uint32 MeaDrvAddMvrpMacAddrToFwdTable(ADAP_Uint16 vpn)
{
	MEA_OutPorts_Entry_dbt tOutPorts;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	if(gAction_id==MEA_PLAT_GENERATE_NEW_ID)
	{
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}
	else if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0, gAction_id) != MEA_TRUE)
	{
		gAction_id=MEA_PLAT_GENERATE_NEW_ID;
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}

	MEA_SET_OUTPORT(&tOutPorts, 127);
	if(MeaDrvCreateTxForwarder(gAction_id,&gMac_mvrp_all_routes,vpn,/*DEFAULT_PROTOCOLS_TRAFFIC*/&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"mvrp dest mac entry failed to insert\n");
	}

	return ENET_SUCCESS;

}

ADAP_Uint32 MeaDrvAddApsMacAddrToFwdTable(ADAP_Uint16 vpn)
{
	MEA_OutPorts_Entry_dbt tOutPorts;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	if(gAction_id==MEA_PLAT_GENERATE_NEW_ID)
	{
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}
	else if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0, gAction_id) != MEA_TRUE)
	{
		gAction_id=MEA_PLAT_GENERATE_NEW_ID;
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}

	MEA_SET_OUTPORT(&tOutPorts, 127);
	if(MeaDrvCreateTxForwarder(gAction_id,&APS_dest_mac,vpn,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"APS dest mac entry failed to insert\n");
	}

	return ENET_SUCCESS;
}

ADAP_Uint32 MeaDrvAddGvrpGmrpMacAddrToFwdTable(ADAP_Uint16 vpn)
{
	MEA_OutPorts_Entry_dbt tOutPorts;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	if(gAction_id==MEA_PLAT_GENERATE_NEW_ID)
	{
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}
	else if(ENET_ADAPTOR_IsExist_ActionID (MEA_UNIT_0, gAction_id) != MEA_TRUE)
	{
		gAction_id=MEA_PLAT_GENERATE_NEW_ID;
		MeaDrvMstpCreateCpuAction(&gAction_id);
	}

	MEA_SET_OUTPORT(&tOutPorts, 127);
	if(MeaDrvCreateTxForwarder(gAction_id,&gMac_gmrp_all_routes,vpn,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"gmrp dest mac entry failed to insert\n");
	}
	MEA_SET_OUTPORT(&tOutPorts, 127);
	if(MeaDrvCreateTxForwarder(gAction_id,&gMac_gvrp_all_routes,vpn,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"gvrp dest mac entry failed to insert\n");
	}
	return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiStpNpHwInit (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Uint32               ifIndex;

	/* Create the LxCp entry per port for G[M|V]RP forwarding to the host
         * and L2 tunneling */

    /* Create the LxCP entry */
	if (MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_STP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiStpNpHwInit Failed on MeaDrvVlanHwLxcpUpdate\n" );
 		return ENET_FAILURE;
    }

	if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) )
    {
		MeaDrvAddMvrpMacAddrToFwdTable(ADAP_DEFAULT_PROTOCOLS_TRAFFIC);
    }

    for(ifIndex=1;ifIndex<MeaAdapGetNumOfPhyPorts();ifIndex++)
    {
		if(adap_SetSpanningTreeMode(ifIndex,ENET_SPANNING_TREE_RSTP_MODE, ENET_AST_PORT_STATE_DISCARDING) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n EnetHal_MiStpNpHwInit Error: Failed ifIndex=%d \n",ifIndex);
			return ENET_FAILURE;
		}
    }

	for(ifIndex=1; ifIndex<MeaAdapGetNumOfPhyPorts(); ifIndex++)
	{
		MeaDrvSetPort802Dot1Lxcp(ifIndex,MEA_LXCP_PROTOCOL_ACTION_DISCARD);
	}

    return ENET_SUCCESS;
}

void EnetHal_MiRstpNpInitHw (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint32 		ifIndex;
	ADAP_Uint16			VlanIdx;

    adap_memset(&insert_mac_to_forwarder[0],0,ADAP_NUM_OF_SUPPORTED_VLANS);

    /* Create the LxCP entry with */
    if(MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_STP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiRstpNpInitHw: set ENET_VLAN_NP_STP_PROTO_ID to MEA_LXCP_PROTOCOL_ACTION_CPU fail! \n");
	}

	if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) )
    {
    	// create forwarder for MVRP
		MeaDrvAddMvrpMacAddrToFwdTable(ADAP_DEFAULT_PROTOCOLS_TRAFFIC);
    }

    for(ifIndex=1;ifIndex<MeaAdapGetNumOfPhyPorts();ifIndex++)
    {
		if(adap_SetSpanningTreeMode(ifIndex,ENET_SPANNING_TREE_RSTP_MODE, ENET_AST_PORT_STATE_FORWARDING) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n EnetHal_MiRstpNpInitHw Error: Failed ifIndex=%d \n",ifIndex);
			return;
		}
		for(VlanIdx=1;VlanIdx<ADAP_NUM_OF_SUPPORTED_VLANS;VlanIdx++)
		{
			if(adap_SetCepPortState(ifIndex,VlanIdx, ENET_AST_PORT_STATE_DISABLED) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n EnetHal_MiRstpNpInitHw Error: Failed ifIndex=%d \n",ifIndex);
				return;
			}
		}
    }

    return;
}

static ADAP_Int8 adap_StpServiceConfiguration(ADAP_Uint32 u4ContextId,tBridgeDomain *pBridgeDomain,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Status)
{
	MEA_Port_t          					enetPort;
	MEA_Bool                                valid;
	ADAP_Uint16								VpnIndex;
	MEA_VP_State_t 							enetState;
	MEA_MSTP_State_t 						enetServiceState;
	ADAP_Uint32								masterPort;
	MEA_Bool								lag_port=MEA_FALSE;
	ADAP_Uint32							    Index;
	tServiceDb 								*pServiceInfo=NULL;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    ADAP_Bool 								service_found = ADAP_FALSE;

    if(pBridgeDomain == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"NULL pBridgeDomain\n");
		return ENET_FAILURE;
    }

	if (u1Status == ENET_AST_PORT_STATE_DISCARDING)
	{
		enetState = MEA_VP_STATE_BLK_REGULAR;
	    enetServiceState = MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0;//MEA_MSTP_TYPE_STATE_BLOCKING;
	}
	else if (u1Status == ENET_AST_PORT_STATE_FORWARDING)
	{
		enetState = MEA_VP_STATE_FWD;
	   enetServiceState = MEA_MSTP_TYPE_STATE_FORWARD;
	}
	else if (u1Status == ENET_AST_PORT_STATE_LEARNING){
		enetState = MEA_VP_STATE_FWD;
	   enetServiceState = MEA_MSTP_TYPE_STATE_LEARNING;
	}
	else if(u1Status == ENET_AST_PORT_STATE_DISABLED)
	{
		enetState = MEA_VP_STATE_FWD;
		enetServiceState = MEA_MSTP_TYPE_STATE_FORWARD;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration issState=%d ENET state not defined \n",u1Status);
		return ENET_FAILURE;
	}

	masterPort = u4IfIndex;
	lag_port=MEA_FALSE;
	if(adap_IsPortChannel(masterPort) == ENET_SUCCESS)
	{
		if( (adap_NumOfLagPortFromAggrId(u4IfIndex)) > 0)
		{
			masterPort = adap_GetLagMasterPort(u4IfIndex);
			if(masterPort == ADAP_END_OF_TABLE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"ifIndex:%d not have master port\n",u4IfIndex);
				return ENET_FAILURE;
			}

			if(MeaAdapGetPhyPortFromLogPort(masterPort,&enetPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",masterPort);
				return ENET_FAILURE;
			}

			lag_port=MEA_TRUE;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"lagPort port:%d found but no phy assigned yet\n",u4IfIndex);
			return ENET_FAILURE;
		}

	}
	else
	{
		if(MeaAdapGetPhyPortFromLogPort(masterPort,&enetPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",masterPort);
			return ENET_FAILURE;
		}
	}

   /* For all services of this VLAN and port*/
    pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapGetFirstServicePort pServiceInfo:%d \n",(pServiceInfo != NULL) ? pServiceInfo->serviceId : 65000);
	while(pServiceInfo != NULL)
	{
		if( (pServiceInfo->L2_protocol_type != MEA_PARSING_L2_KEY_DEF_SID) && (pServiceInfo->vpnId == pBridgeDomain->vpn) )
		{
			if( ( (pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Untagged) &&
				(getPortToPvid(u4IfIndex) == pBridgeDomain->vlanId) ) ||
				(pServiceInfo->L2_protocol_type != MEA_PARSING_L2_KEY_Untagged) )
			{
				if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,pServiceInfo->serviceId, &valid) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiRstpNpSetPortState ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", pServiceInfo->serviceId);
					return ENET_FAILURE;
				}
				if(valid == MEA_TRUE)
				{
					if (ENET_ADAPTOR_Service_Set_MSTP_State(MEA_UNIT_0, pServiceInfo->serviceId, enetServiceState) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "EnetHal_MiRstpNpSetPortState Error: ENET_ADAPTOR_Service_Set_MSTP_State for service Id %d failed\n", pServiceInfo->serviceId);
						return ENET_FAILURE;
					}
					service_found = ADAP_TRUE;
				}
			}
		}
		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaAdapGetNextServicePort pServiceInfo:%d \n",(pServiceInfo != NULL) ? pServiceInfo->serviceId : 65000);

    }

	if(service_found == ADAP_FALSE)
		return ENET_SUCCESS;

	//get vpn
	VpnIndex = pBridgeDomain->vpn;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_MiRstpNpSetPortState: vpn-id=%d set to\n",VpnIndex,enetState);

	if(AdapGetBridgeMode() != ADAP_VLAN_PROVIDER_BRIDGE_MODE)
	{
		if(enetServiceState == MEA_MSTP_TYPE_STATE_BLOCKING_SRV_VPN0)
		{
			if(insert_mac_to_forwarder[0] == 0)
			{
				MeaDrvAddGvrpGmrpMacAddrToFwdTable(0);
				MeaDrvAddMvrpMacAddrToFwdTable(0);
				insert_mac_to_forwarder[0]=1;
			}
		}
		if(insert_mac_to_forwarder[VpnIndex] == 0)
		{
			MeaDrvAddGvrpGmrpMacAddrToFwdTable(VpnIndex);
			MeaDrvAddMvrpMacAddrToFwdTable(VpnIndex);
			MeaDrvAddApsMacAddrToFwdTable(VpnIndex);
			insert_mac_to_forwarder[VpnIndex]=1;
		}
	}
	else
	{
		if(insert_mac_to_forwarder[VpnIndex] == 0)
		{
			MeaDrvAddApsMacAddrToFwdTable(VpnIndex);
			insert_mac_to_forwarder[VpnIndex]=1;
		}
	}

	/* for default VLAN */
	if(insert_mac_to_forwarder[ADAP_DEFAULT_PROTOCOLS_TRAFFIC] == 0)
	{
		MeaDrvAddGvrpGmrpMacAddrToFwdTable(ADAP_DEFAULT_PROTOCOLS_TRAFFIC);
		MeaDrvAddMvrpMacAddrToFwdTable(ADAP_DEFAULT_PROTOCOLS_TRAFFIC);
		insert_mac_to_forwarder[ADAP_DEFAULT_PROTOCOLS_TRAFFIC]=1;
	}

	if(MEA_API_Get_VP_State(MEA_UNIT_0,VpnIndex,enetState,&Service_outPorts) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiRstpNpSetPortState ERROR: MEA_API_Get_VP_State failed for VlanId:%d\n", pBridgeDomain->vlanId);
	   return ENET_FAILURE;
	}

	if(MEA_API_Set_VP_State (MEA_UNIT_0,VpnIndex,enetPort,enetState) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiRstpNpSetPortState ERROR: MEA_API_Set_VP_State failed for VpnIndex:%d VlanId:%d\n", VpnIndex,pBridgeDomain->vlanId);
	   return ENET_FAILURE;
	}
	if(lag_port == MEA_TRUE)
	{
		for(Index=1;Index<MeaAdapGetNumOfPhyPorts();Index++)
		{
			if(adap_isLagPortFromAggrId(u4IfIndex,Index) == ENET_SUCCESS)
			{
				MeaAdapGetPhyPortFromLogPort(Index, &enetPort);
				if(MEA_API_Set_VP_State (MEA_UNIT_0,VpnIndex,enetPort,enetState) != MEA_OK)
				{
				   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiRstpNpSetPortState ERROR: MEA_API_Set_VP_State failed for VlanId:%d\n", pBridgeDomain->vlanId);
				   return ENET_FAILURE;
				}
			}
		}
	}
	return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiRstpNpSetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint32 							modeType=0;
	ADAP_Uint16 							VlanId;
	ADAP_Uint8 								oldStatus=ENET_AST_PORT_STATE_DISCARDING;
	eAdap_StpMode 							mode=ENET_SPANNING_TREE_UNKNOWN_MODE;
	tBridgeDomain 							*pBridgeDomain=NULL;

	AdapGetProviderCoreBridgeMode(&modeType, u4IfIndex);

	if( ( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) ) &&
			(modeType == ADAP_CUSTOMER_EDGE_PORT) )
	{
		return ENET_SUCCESS;
	}

	adap_GetSpanningTreeMode (u4IfIndex,&mode,&oldStatus);

	if(oldStatus != u1Status)
	{
		adap_SetSpanningTreeMode (u4IfIndex, ENET_SPANNING_TREE_RSTP_MODE, u1Status);

		for(VlanId=0;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
		{
	    	Adap_bridgeDomain_semLock();
	    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	    	if(pBridgeDomain != NULL)
	        {
	   			if( adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) == ADAP_TRUE)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Vlan Id:%d ifIndex:%d move to state:%d\n",VlanId,u4IfIndex,u1Status);
					if(adap_StpServiceConfiguration(u4ContextId, pBridgeDomain, u4IfIndex, u1Status) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration failed for vlan:%d ifIndex:%d\r\n",VlanId, u4IfIndex);
						Adap_bridgeDomain_semUnlock();
						return ENET_FAILURE;
					}
				}
	        }
	    	Adap_bridgeDomain_semUnlock();
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiRstNpGetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint32					issMasterPort;
	eAdap_StpMode 			mode;
	ADAP_Uint8					status;

	issMasterPort = u4IfIndex;
	if(adap_IsPortChannel(issMasterPort) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(issMasterPort) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_MiRstNpGetPortState ifIndex :%d \n",issMasterPort);
			return ENET_FAILURE;
		}
		issMasterPort = adap_GetLagMasterPort(u4IfIndex);
	}

	if(adap_GetSpanningTreeMode (u4IfIndex,&mode,&status) != ENET_SUCCESS)
	{
		*pu1Status = 0;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get stapping tree ifIndex:%d\n", u4IfIndex);
		return ENET_FAILURE;
	}
	(*pu1Status) = status;

    return ENET_SUCCESS;
}

/************************************* MSTP *************************/
ADAP_Int8 EnetHal_MiMstpNpCreateInstance (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint16 index=0;

	if(u2InstId >= ADAP_MAX_NUM_OF_MSTP_INSTANCES)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpCreateInstance instance:%d out of range \n",u2InstId);
		return ENET_FAILURE;
	}

	/* : Initializing the ports to discarding */
	for(index=1;index<MeaAdapGetNumOfPhyPorts();index++)
	{
		adap_MstpSetInstanceParams(u2InstId, 1, index, ENET_AST_PORT_STATE_FORWARDING);
	}

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiMstpNpAddVlanInstMapping (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2InstId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Uint32 u4IfIndex;
	tMstpInstDb*	mstpInstance = NULL;
	tBridgeDomain 	*pBridgeDomain=NULL;

	if (u2InstId >= (ADAP_MAX_NUM_OF_MSTP_INSTANCES))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpAddVlanInstMapping ERROR u2InstId>63 :%d \n",	u2InstId);
		return ENET_FAILURE;
	}

	if (VlanId >= (ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpAddVlanInstMapping ERROR VlanId>4095 :%d \n",VlanId);
		return ENET_FAILURE;
	}

	if(VlanId > ADAP_NUMBER_OF_MSTP_VLANS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"EnetHal_MiMstpNpAddVlanInstMapping  VlanId> %d temporary is not supported for MSTP:%d \n", VlanId,ADAP_NUMBER_OF_MSTP_VLANS);
	}

	mstpInstance = adap_MstpGetInstance(u2InstId);

	if( mstpInstance!= NULL)
	{
		if(mstpInstance->MSTPInst_valid != 0)
		{
			adap_MstpSetVlanInstanceParams(VlanId, 1, u2InstId);

			for(u4IfIndex=1;u4IfIndex<MeaAdapGetNumOfPhyPorts();u4IfIndex++)
			{
		    	Adap_bridgeDomain_semLock();
		    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		    	if(pBridgeDomain != NULL)
		        {
		   			if( adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) == ADAP_TRUE)
					{
						if(adap_StpServiceConfiguration(u4ContextId, pBridgeDomain, u4IfIndex, mstpInstance->port_state[u4IfIndex]) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration failed for vlan:%d ifIndex:%d\r\n",VlanId, u4IfIndex);
							Adap_bridgeDomain_semUnlock();
							return ENET_FAILURE;
						}
					}
		        }
		    	Adap_bridgeDomain_semUnlock();
			}
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiMstpNpDeleteInstance (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstId)
{
	ADAP_Uint16		VlanId=0;
	ADAP_Uint32 	u4IfIndex;
	tMstpVlanDb*	vlanInstance = NULL;
	ADAP_Uint32		modeType = 0;
	tBridgeDomain 	*pBridgeDomain=NULL;

	if (u2InstId > (ADAP_MAX_NUM_OF_MSTP_INSTANCES - 1))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDeleteInstance ERROR InstanceNum is OutOfRange:%d \n",u2InstId);
		return ENET_FAILURE;
	}

	for(VlanId=0;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
	{
		vlanInstance = adap_MstpGetVlanInstance(VlanId);

		if( vlanInstance!= NULL)
		{
			if( (vlanInstance->MSTPInstId == u2InstId) && (vlanInstance->isValid == 1) )
			{
				adap_MstpSetVlanInstanceParams(VlanId, 0, ADAP_END_OF_TABLE);
				for(u4IfIndex=1;u4IfIndex<MeaAdapGetNumOfPhyPorts();u4IfIndex++)
				{
			    	Adap_bridgeDomain_semLock();
			    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

			    	if(pBridgeDomain != NULL)
			        {
			   			if( adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) == ADAP_TRUE)
						{
							//initial state as forward
							adap_MstpSetInstanceParams(u2InstId, 0, u4IfIndex, ENET_AST_PORT_STATE_DISABLED);

							AdapGetProviderCoreBridgeMode(&modeType, u4IfIndex);
							if( ( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) ) &&
								  (modeType == ADAP_CUSTOMER_EDGE_PORT) )
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"no need to configure\n");
							}
							else
							{
								if(adap_StpServiceConfiguration(u4ContextId, pBridgeDomain, u4IfIndex, ENET_AST_PORT_STATE_DISABLED) != ENET_SUCCESS)
								{
									ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration failed for vlan:%d ifIndex:%d\r\n",VlanId, u4IfIndex);
									Adap_bridgeDomain_semUnlock();
									return ENET_FAILURE;
								}
							}
				        }
			        }
				    Adap_bridgeDomain_semUnlock();
				}
			}
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiMstpNpDelVlanInstMapping (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2InstId)
{
	ADAP_Uint32 u4IfIndex;
	tMstpVlanDb*	vlanInstance = NULL;
	tMstpInstDb*	mstpInstance = NULL;
	tBridgeDomain 	*pBridgeDomain=NULL;

	if (u2InstId >= (ADAP_MAX_NUM_OF_MSTP_INSTANCES))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDelVlanInstMapping ERROR u2InstId>63 :%d \n",	u2InstId);
		return ENET_FAILURE;
	}
	if (VlanId >= (ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDelVlanInstMapping ERROR VlanId>4095 :%d \n",	VlanId);
		return ENET_FAILURE;
	}

	if(VlanId > ADAP_NUMBER_OF_MSTP_VLANS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"EnetHal_MiMstpNpDelVlanInstMapping  VlanId> %d temporary is not supported for MSTP:%d \n", VlanId,ADAP_NUMBER_OF_MSTP_VLANS);
	}

	vlanInstance = adap_MstpGetVlanInstance(VlanId);
	if( vlanInstance == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDelVlanInstMapping VlanId:%d instance not exists\n",	VlanId);
		return ENET_FAILURE;
	}
	else
	{
		if( (vlanInstance->MSTPInstId == u2InstId) && (vlanInstance->isValid == 1) )
		{
			adap_MstpSetVlanInstanceParams(VlanId, (u2InstId == 0) ? 0 : 1, ADAP_END_OF_TABLE);
			mstpInstance = adap_MstpGetInstance(u2InstId);
			if( mstpInstance!= NULL)
			{
				for(u4IfIndex=1;u4IfIndex<MeaAdapGetNumOfPhyPorts();u4IfIndex++)
				{
			    	if(pBridgeDomain != NULL)
			        {
			   			if( adap_VlanCheckPortsInDomain(pBridgeDomain, u4IfIndex) == ADAP_TRUE)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Vlan Id:%d ifIndex:%d move to state:%d\n",pBridgeDomain->vlanId,u4IfIndex,mstpInstance->port_state[u4IfIndex]);
							if(adap_StpServiceConfiguration(u4ContextId, pBridgeDomain, u4IfIndex, mstpInstance->port_state[u4IfIndex]) != ENET_SUCCESS)
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration failed for vlan:%d ifIndex:%d\r\n",VlanId, u4IfIndex);
								return ENET_FAILURE;
							}
						}
			        }
				}
			}
		}
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_MiMstpNpSetInstancePortState (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
		ADAP_Uint16 u2InstanceId, ADAP_Uint8 u1PortState)
{
	ADAP_Uint16                             VlanId;
	ADAP_Uint32								modeType = 0;
	tMstpVlanDb*							vlanInstance = NULL;
	tBridgeDomain 							*pBridgeDomain=NULL;

	AdapGetProviderCoreBridgeMode(&modeType, u4IfIndex);
	if( ( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) ) &&
		  (modeType == ADAP_CUSTOMER_BRIDGE_PORT) )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"no need to configure\n");
		return ENET_SUCCESS;
	}

	if (u2InstanceId >= ADAP_MAX_NUM_OF_MSTP_INSTANCES)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpSetInstancePortState ERROR InstanceId:%d \n", u2InstanceId);
		return ENET_FAILURE;
	}

	for(VlanId=0;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
	{
		vlanInstance = adap_MstpGetVlanInstance(VlanId);
		if( vlanInstance != NULL)
		{
			if( (vlanInstance->MSTPInstId == u2InstanceId) && (vlanInstance->isValid == 1) )
			{
		    	Adap_bridgeDomain_semLock();
		    	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

		    	if(pBridgeDomain != NULL)
		        {
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_MiMstpNpSetInstancePortState set stp state port:%d vlan:%d\n",u4IfIndex,VlanId);
					if(adap_StpServiceConfiguration(u4ContextId, pBridgeDomain, u4IfIndex, u1PortState) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_StpServiceConfiguration failed for vlan:%d ifIndex:%d\r\n",VlanId, u4IfIndex);
						Adap_bridgeDomain_semUnlock();
						return ENET_FAILURE;
					}
				}
		    	Adap_bridgeDomain_semUnlock();
			}
		}
	}

	adap_MstpSetInstanceParams(u2InstanceId, 1, u4IfIndex, u1PortState);

    return ENET_SUCCESS;
}

void EnetHal_MiMstpNpInitHw (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Uint16 portId=0;
	ADAP_Uint16 Index=0, VlanIdx = 0;

	adap_memset(&insert_mac_to_forwarder[0],0,ADAP_NUM_OF_SUPPORTED_VLANS);

    /* Create the LxCP entry with */
    if(MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_STP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpInitHw: set ENET_VLAN_NP_STP_PROTO_ID to MEA_LXCP_PROTOCOL_ACTION_CPU fail! \n");
	}
    if( (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE) )
    {
    	MeaDrvAddMvrpMacAddrToFwdTable(ADAP_DEFAULT_PROTOCOLS_TRAFFIC);
    }

	for (Index = 0; Index < ADAP_MAX_NUM_OF_MSTP_INSTANCES; Index++)
	{
		for(portId=1;portId<MeaAdapGetNumOfPhyPorts();portId++)
		{
			adap_MstpSetInstanceParams(Index, 0, portId, ENET_AST_PORT_STATE_DISABLED);
		}
	}
	for (Index = 0; Index < ADAP_NUM_OF_SUPPORTED_VLANS; Index++)
	{
		adap_MstpSetVlanInstanceParams(Index, 0, ADAP_END_OF_TABLE);
	}

	adap_MstpSetInstanceParams(0, 1, 0, ENET_AST_PORT_STATE_DISABLED);
	adap_MstpSetVlanInstanceParams(1, 1, 0);

	for (Index = 1; Index < MeaAdapGetNumOfPhyPorts(); Index++)
	{
		adap_SetSpanningTreeMode(Index,ENET_SPANNING_TREE_MSTP_MODE,ENET_AST_PORT_STATE_FORWARDING);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_MiRstpNpInitHw set mstp enable port:%d\n",Index);
		for(VlanIdx=1;VlanIdx<ADAP_NUM_OF_SUPPORTED_VLANS;VlanIdx++)
		{
			if(adap_SetCepPortState(Index,VlanIdx, ENET_AST_PORT_STATE_DISABLED) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n EnetHal_MiRstpNpInitHw Error: Failed ifIndex=%d \n",Index);
				return;
			}
		}
	}

    return;
}

void EnetHal_MiMstpNpDeInitHw (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
    ADAP_Uint16 portId=0;
    ADAP_Uint16 Index=0;

    for(Index=1;Index<MeaAdapGetNumOfPhyPorts();Index++)
    {
		if(adap_SetSpanningTreeMode(Index,ENET_SPANNING_TREE_UNKNOWN_MODE,ENET_AST_PORT_STATE_FORWARDING) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n EnetHal_MiMstpNpDeInitHw Error: Failed ifIndex=%d \n",Index);
			return;
		}
    }

	for (Index = 0; Index < ADAP_MAX_NUM_OF_MSTP_INSTANCES; Index++)
	{
		for(portId=1;portId<MeaAdapGetNumOfPhyPorts();portId++)
		{
			adap_MstpSetInstanceParams(Index, 0, portId, ENET_AST_PORT_STATE_DISCARDING);
		}
	}
	for (Index = 0; Index < ADAP_NUM_OF_SUPPORTED_VLANS; Index++)
	{
		adap_MstpSetVlanInstanceParams(Index, 0, ADAP_END_OF_TABLE);
	}

	adap_MstpSetInstanceParams(0, 1, 0, ENET_AST_PORT_STATE_DISCARDING);
	adap_MstpSetVlanInstanceParams(1, 1, 0);

    /* Create the LxCP entry with */
    if(MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_STP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDeInitHw:set ENET_VLAN_NP_STP_PROTO_ID to MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT fail! \n");
	}

    return;
}

ADAP_Int8 EnetHal_MiMstNpGetPortState (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2InstanceId, ADAP_Uint32 u4IfIndex,
		ADAP_Uint8 *pu1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tMstpInstDb*	mstpInstance = NULL;

	if (u2InstanceId >= ADAP_MAX_NUM_OF_MSTP_INSTANCES)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstNpGetPortState ERROR InstanceId:%d \n", u2InstanceId);
		return ENET_FAILURE;
	}

	mstpInstance = adap_MstpGetInstance(u2InstanceId);
	if( mstpInstance!= NULL)
	{
		if(mstpInstance->MSTPInst_valid != 0)
		{
			(*pu1Status) = mstpInstance->port_state[u4IfIndex];
		}
		else
		{
			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"===EnetHal_MiMstNpGetPortState ERROR InstanceId:%d \n", u2InstanceId);
			return ENET_FAILURE;

		}
	}

    return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiMstpNpAddVlanListInstMapping (ADAP_Uint32 u4ContextId, ADAP_Uint8 *pu1VlanList,
												ADAP_Uint16 u2InstId, ADAP_Uint16 u2NumVlans, ADAP_Uint16 *pu2LastVlan)
{
    ADAP_UNUSED_PARAM (pu2LastVlan);
    ADAP_Uint16               u2ByteCount = 0;
    ADAP_Uint16               u2VlanCount = 0;
    ADAP_Uint16               u2StartVlan = 0;
    ADAP_Uint16               u2EndVlan = 0;
    ADAP_Uint8                bResult = 0;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"pu1VlanList:0 - %d 1 - %d 2 - %d 3 - %d 4 - %d 5 - %d\n",	pu1VlanList[0], pu1VlanList[1], pu1VlanList[2], pu1VlanList[3], pu1VlanList[4], pu1VlanList[5]);

	if (u2InstId >= (ADAP_MAX_NUM_OF_MSTP_INSTANCES))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpAddVlanListInstMapping ERROR u2InstId>63 :%d \n", u2InstId);
		return ENET_FAILURE;
	}

	if (u2NumVlans >= (ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpAddVlanListInstMapping ERROR u2NumVlans>4095 :%d \n", u2NumVlans);
		return ENET_FAILURE;
	}

	while ((u2ByteCount < ADAP_VLAN_BIT_MAP_SIZE) && (u2VlanCount < u2NumVlans))
    {
        if (pu1VlanList[u2ByteCount] == 0)
        {
            u2ByteCount++;
            continue;
        }

        u2StartVlan = (u2ByteCount * ADAP_BITS_PER_BYTE) + 1;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"u2StartVlan:%d\n", u2StartVlan);
        for (u2EndVlan = u2StartVlan; (u2VlanCount < u2NumVlans) && (u2EndVlan < (u2StartVlan + ADAP_BITS_PER_BYTE)); u2EndVlan++)
        {
        	ADAP_IS_SET_OUTPORT(pu1VlanList, u2EndVlan, bResult);
        	if (bResult == 0)
        	{
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"u2EndVlan:%d\n", u2EndVlan);
                continue;
            }

            EnetHal_MiMstpNpAddVlanInstMapping (u4ContextId, u2EndVlan, u2InstId);

            u2VlanCount++;
        }

        u2ByteCount++;
    }

	return ENET_SUCCESS;
}

ADAP_Int8 EnetHal_MiMstpNpDelVlanListInstMapping (ADAP_Uint32 u4ContextId, ADAP_Uint8 *pu1VlanList,
												ADAP_Uint16 u2InstId, ADAP_Uint16 u2NumVlans, ADAP_Uint16 *pu2LastVlan)
{
    ADAP_UNUSED_PARAM (pu2LastVlan);
    ADAP_Uint16               u2ByteCount = 0;
    ADAP_Uint16               u2VlanCount = 0;
    ADAP_Uint16               u2StartVlan = 0;
    ADAP_Uint16               u2EndVlan = 0;
    ADAP_Uint8                bResult = 0;

	if (u2InstId >= (ADAP_MAX_NUM_OF_MSTP_INSTANCES))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDelVlanListInstMapping ERROR u2InstId>63 :%d \n", u2InstId);
		return ENET_FAILURE;
	}

	if (u2NumVlans >= (ADAP_NUM_OF_SUPPORTED_VLANS))
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_MiMstpNpDelVlanListInstMapping ERROR u2NumVlans>4095 :%d \n",	u2NumVlans);
		return ENET_FAILURE;
	}

	while ((u2ByteCount < ADAP_VLAN_BIT_MAP_SIZE) && (u2VlanCount < u2NumVlans))
    {
        if (pu1VlanList[u2ByteCount] == 0)
        {
            u2ByteCount++;
            continue;
        }

        u2StartVlan = (u2ByteCount * ADAP_BITS_PER_BYTE) + 1;
        for (u2EndVlan = u2StartVlan; (u2VlanCount < u2NumVlans) &&
             (u2EndVlan < (u2StartVlan + ADAP_BITS_PER_BYTE)); u2EndVlan++)
        {
        	ADAP_IS_SET_OUTPORT(pu1VlanList, u2EndVlan, bResult);
        	if (bResult == 0)
        	{
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"u2EndVlan:%d\n", u2EndVlan);
                continue;
            }
            EnetHal_MiMstpNpDelVlanInstMapping (u4ContextId, u2EndVlan, u2InstId);

            u2VlanCount++;
        }

        u2ByteCount++;
    }

	return ENET_SUCCESS;
}

