#include "adap_taskManager.h"
#include "adap_hashmap.h"
#include "adap_utils.h"
#include <pthread.h>
#include <limits.h>

static adap_hashmap *tasks = NULL;
static ADAP_Bool g_initilaized = ADAP_FALSE;

struct adap_task {
	adap_hashmap_element hash_elem;
};

#define MAX_TASKS 1024

static
EnetHal_Status_t flush_task(ADAP_Uint64 key, adap_hashmap_element* element)
{
	int err;
	struct adap_task *task = container_of(element, struct adap_task, hash_elem);

	err = pthread_cancel(key);
	if (err != 0) {
        return ENETHAL_STATUS_THREAD_ERROR;
	}

	adap_safe_free(task);

	return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_task_manager_init()
{
	EnetHal_Status_t status = ENETHAL_STATUS_SUCCESS;
	struct adap_hashmap_params params;

	if (g_initilaized == ADAP_FALSE) {
		params.capacity = MAX_TASKS;
		params.hash = identity_func;
		params.flush = flush_task;
		params.num_of_buckets = 16;
		params.thread_safe = ADAP_TRUE;
		status = adap_hashmap_init(&tasks, &params);

		g_initilaized = ADAP_TRUE;
	}

	return status;
}

EnetHal_Status_t adap_task_manager_deinit()
{
	EnetHal_Status_t status = ENETHAL_STATUS_SUCCESS;

	if (g_initilaized == ADAP_TRUE) {
		status = adap_hashmap_deinit(tasks);
		tasks = NULL;

		g_initilaized = ADAP_FALSE;
	}

	return status;
}


EnetHal_Status_t adap_task_manager_run(adap_task_func func, void* context, adap_task_id* id)
{
	EnetHal_Status_t status;
	struct adap_task* task;
	int err;

	if (g_initilaized == ADAP_FALSE) {
		return ENETHAL_STATUS_NOT_INITIALIZED;
	}

	if((func == NULL) || (id == NULL)) {
		return ENETHAL_STATUS_NULL_PARAM;
	}

    task = (struct adap_task*)adap_malloc(sizeof(struct adap_task));
    if(task == NULL) {
    	return ENETHAL_STATUS_MEM_ALLOC;
    }

 	err = pthread_create(id, NULL, func, context);
	if (err != 0) {
		adap_safe_free(task);
        return ENETHAL_STATUS_THREAD_ERROR;
	}

 	status = adap_hashmap_insert(tasks, *id, &task->hash_elem);
 	if (status != ENETHAL_STATUS_SUCCESS) {
 		pthread_cancel(*id);
    	adap_safe_free(task);
    	return status;
 	}

	return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_task_manager_join(adap_task_id id)
{
	EnetHal_Status_t status;
	adap_hashmap_element *hash_elem;
	int err;

	if (g_initilaized == ADAP_FALSE) {
		return ENETHAL_STATUS_NOT_INITIALIZED;
	}


	status = adap_hashmap_find(tasks, id, NULL);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return status;
	}

	err = pthread_join(id, NULL);
	if (err != 0) {
        return ENETHAL_STATUS_THREAD_ERROR;
	}

	status = adap_hashmap_remove(tasks, id, &hash_elem);
	if(status != ENETHAL_STATUS_SUCCESS) {
		return status;
	}

	adap_safe_free(container_of(hash_elem, struct adap_task, hash_elem));

	return ENETHAL_STATUS_SUCCESS;
}


EnetHal_Status_t adap_task_manager_cancel(adap_task_id id)
{
	EnetHal_Status_t status;
	adap_hashmap_element *hash_elem;
	int err;

	if (g_initilaized == ADAP_FALSE) {
		return ENETHAL_STATUS_NOT_INITIALIZED;
	}

	status = adap_hashmap_find(tasks, id, NULL);
	if (status != ENETHAL_STATUS_SUCCESS) {
		return status;
	}

	err = pthread_cancel(id);
	if (err != 0) {
        return ENETHAL_STATUS_THREAD_ERROR;
	}

	status = adap_hashmap_remove(tasks, id, &hash_elem);
	if(status != ENETHAL_STATUS_SUCCESS) {
		return status;
	}

	adap_safe_free(container_of(hash_elem, struct adap_task, hash_elem));

	return ENETHAL_STATUS_SUCCESS;
}

