/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
MEA_Status Adap_CreateLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 *id);
MEA_Status Adap_DeleteLagEntry(MEA_Uint16 id);
MEA_Status Adap_AddLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 id);
MEA_Status Adap_RemoveLagEntry(ENET_QueueId_t clusterId,MEA_Uint16 id,ENET_QueueId_t *pMasterclusterId);
MEA_Status Adap_SetLagGlobalLag(MEA_Uint32 enable);
MEA_Status Adap_SetLagSetLagSelection (MEA_Port_t enetPort,MEA_Uint32 selection);
MEA_Status Adap_SetIngressMasterLag (MEA_Port_t enetPort,MEA_Port_t masterLag);
MEA_Status Adap_clearIngressMasterLag (MEA_Port_t enetPort);

