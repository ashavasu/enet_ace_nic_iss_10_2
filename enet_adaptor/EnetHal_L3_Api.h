/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ENETHAL_L3_API_H
#define ENETHAL_L3_API_H

#include "EnetHal_L2_Api.h"

/* This file contains prototypes for all ENET HAL Layer 3 external APIs */

/* IGMP part */
ADAP_Int32 EnetHal_FsMiIgsHwEnableIgmpSnooping (ADAP_Uint32 u4Instance);
ADAP_Int32 EnetHal_FsMiIgsHwDisableIgmpSnooping (ADAP_Uint32 u4Instance);

ADAP_Int32 EnetHal_FsMiIgsHwEnableIpIgmpSnooping (ADAP_Uint32 u4Instance);

ADAP_Int32 EnetHal_FsMiIgsHwDisableIpIgmpSnooping (ADAP_Uint32 u4Instance);

ADAP_Int32 EnetHal_FsIgmpHwEnableIgmp (void);

/* PIM part */
typedef struct
{
	ADAP_Uint32	GrpAddr;
	ADAP_Uint32	SrcIpAddr;
	ADAP_Uint32     IfIndex;   /* Reverse path forwarding ifIndex */
    tEnetHal_VlanId VlanId;
	ADAP_Uint16	NoOfDownStreamIf;
}tEnetHal_McRouteInfo;

typedef struct
{
	ADAP_Uint32     	IfIndex;   /* Reverse path forwarding ifIndex */
    tEnetHal_VlanId 	VlanId;
	ADAP_Uint8      	McFwdPortList[MAX_NUM_OF_SUPPORTED_PORTS];
}tEnetHal_McDownStreamIf;

ADAP_Int32 EnetHal_FsPimNpInitHw (void);
ADAP_Int32 EnetHal_FsNpIpv4VlanMcInit (ADAP_Uint32 vlanid);
ADAP_Int32 EnetHal_FsNpIpv4McAddRouteEntry (tEnetHal_McRouteInfo *pMcRouteInfo,
					    tEnetHal_McDownStreamIf *pDownStreamIf);

ADAP_Int32 EnetHal_FsNpIpv4McDeleteRouteEntry (tEnetHal_McRouteInfo *pMcRouteInfo);

/* IPv6 part */
#define ADAP_IPV6_NH_INCOMPLETE           1
#define ADAP_IPV6_NH_REACHABLE            2
#define ADAP_IPV6_NH_STALE                3

#define ADAP_IP6_ADDR_LEN		  128

typedef ADAP_Uint8 tIPv6Addr[16];

typedef struct 
{
    ADAP_Uint32 u4VrId;
    tIPv6Addr   Ip6Addr;
    ADAP_Uint32 u4CfaIndex;
    ADAP_Uint8  u1ReachState;
    tEnetHal_MacAddr MacAddr;
} EnetHal_NpIpv6Neighbor;

typedef struct 
{
        tIPv6Addr      tunlSrc;  /* Tunnel source endpoint address */

        tIPv6Addr      tunlDst;   /* Tunnel dest endpoint address */

        ADAP_Uint8     u1TunlType;    /* Type of tunnel - For Fs Code, Tunnel Type
                                      * is as defined in inc/cfa.h. */
        ADAP_Uint8     u1TunlFlag;        /* Flag Defining whether tunnel
                                      * is unidirectional or bidirectional.
                                      * if 1 = Unidirectional.
                                      * else if 2 = Bidirectional.
                                      */
        ADAP_Uint8     u1TunlDir;         /* Incase if tunnel is unidirectional
                                      * flag defining it is out going or incoming.
                                      * if 1 = Incoming
                                      * else if 2 = Outgoing.
                                      */
        ADAP_Uint8     u1Reserved;
} tEnetHal_NpTunnelInfo;

typedef struct
{
        ADAP_Uint32       u4PhyIfIndex;    /* Index of vlan for L3 Interface (or)
                                      * Index of vlan over which tunnel is configured */
        ADAP_Uint32       u4TnlIfIndex;  /* Index of vlan for L3 Interface (or)
                                      * Index of vlan over which tunnel is configured */
        tEnetHal_MacAddr  au1MacAddr;
        ADAP_Uint16       u2VlanId;
        ADAP_Uint32       u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
        ADAP_Uint8        u1IfType;
        ADAP_Uint8        u1NDProxyOperStatus;
        ADAP_Uint8        u1RouteCount;
        tEnetHal_NpTunnelInfo   TunnelInfo;

} tEnetHal_NpIntInfo;

typedef struct 
{
       ADAP_Uint32       u4HwIntfId[2];/*To store Hardware IntfId for active and Standby*/
       ADAP_Uint8        u1RouteCount;/* Updated for ECMP*/
       ADAP_Uint8        u1NHType;
       ADAP_Uint8        u1Reserved[2];

} tEnetHal_NpRouteInfo;

typedef struct 
{
    ADAP_Uint32        u4CxtId;   /* Context Id */
    tIPv6Addr          Ip6DestAddr; /* Destination address */
    tIPv6Addr          Ip6NextHopAddr; /* Next hop address */
}tEnetHal_NpRtm6Input;

typedef struct 
{
    ADAP_Uint32   u4CxtId;   /* Context Id */
    tIPv6Addr     Ip6DestAddr; /* Destination address */
    tIPv6Addr     Ip6NextHopAddr; /* Next hop address */
}tEnetHal_NpRtm6Output;

/* IP part */
typedef struct
{
	ADAP_Uint32     u4NextHopGt; /* Ip addr of next hop[ */
	ADAP_Uint32     u4IfIndex;   /* cfa ifIndex */
    tEnetHal_VlanId     u2VlanId;
}tEnetHal_NpNextHopInfo;

typedef struct
{
    ADAP_Uint32   u4VrId;        /* VrfId of a L3 Interface */
    ADAP_Uint32   u4CfaIfIndex;  /* Cfa Interface Index */
    ADAP_Uint32   u4IpAddr;      /* IPv4 Address */
    ADAP_Uint32   u4IpSubNetMask;/* Address mask */
    ADAP_Uint16   u2VlanId;      /* Outer Vlan of L3 Interface */
    ADAP_Uint16   u2InnerVlan;   /* Inner Vlan of L3 Interface */
    ADAP_Uint8    pu1IfName[24]; /* Alias Name of L3 Interface */
    tEnetHal_MacAddr    au1MacAddr; /* Mac Address of L3 Interface */
    ADAP_Uint8    u1OperStatus;  /* Oper Status of L3 Interface */
}tEnetHal_NpIpInterface;

typedef struct
{
	ADAP_Uint32	valid;
	ADAP_Uint32 u4VrId;      /* Virtual Route Identifier*/
	ADAP_Uint32 u4IfIndex;   /* CFA IfIndex */
	ADAP_Uint32 u4IpAddr;    /* Ip Address of this L3 interface */
	ADAP_Uint32 u4IpSubnet;  /* Subnet of this L3 interface */
	tEnetHal_MacAddr au1MacAddr; /*Interface Mac */
	ADAP_Uint32 u2PortVlanId; /* Holds the VLAN Identifier used for Routerport Implementation */
	ADAP_Uint16 u2ErrCode; /* Error code set by NP */
	ADAP_Uint8  IfType; /* for PPP interface iftype=23 */
	ADAP_Uint8  u1Port; /* for PPP interface underlying physical port */
}tEnetHal_FsNpL3IfInfo;

typedef enum EnetL3Action
{
	ENETHAL_L3_HW_CREATE_IF_INFO =1,
	ENETHAL_L3_HW_MODIFY_IF_INFO,
	ENETHAL_L3_HW_DELETE_IF_INFO
}eEnetHal_L3Action;

void EnetHal_FsNpIpInit (void);
ADAP_Uint8 EnetHal_FsNpOspfInit (void);
ADAP_Uint8 EnetHal_FsNpOspfDeInit (void);

ADAP_Uint8 EnetHal_FsNpDhcpSrvInit (void);
ADAP_Uint8 EnetHal_FsNpDhcpRlyInit (void);

ADAP_Uint8 EnetHal_FsNpMLDEnable (void);
ADAP_Uint8 EnetHal_FsNpMLDDisable (void);

ADAP_Uint8 EnetHal_CtrlProto_Device_DhcpSrv_DeInit (void);
ADAP_Uint8 EnetHal_CtrlProto_Device_DhcpRly_DeInit (void);
ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpRly_Init (ADAP_Uint32 u4IfIndex);
ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpSrv_Init (ADAP_Uint32 u4IfIndex);
ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpRly_DeInit (ADAP_Uint32 u4IfIndex);
ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpSrv_DeInit (ADAP_Uint32 u4IfIndex);

ADAP_Uint8 EnetHal_FsNpRipInit(void);
ADAP_Uint8 EnetHal_FsNpRipDeInit(void);

ADAP_Uint32 EnetHal_FsNpIpv4CreateIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4CfaIfIndex,
											ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
											ADAP_Uint16 u2VlanId, ADAP_Uint8 *au1MacAddr);

ADAP_Uint32 EnetHal_FsNpIpv4ModifyIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4CfaIfIndex,
											ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
											ADAP_Uint16 u2VlanId, ADAP_Uint8 *au1MacAddr);

ADAP_Uint32 EnetHal_FsNpIpv4DeleteIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4IfIndex, ADAP_Uint16 u2VlanId);

ADAP_Uint32 EnetHal_FsNpIpv4UcDelRoute (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr,
									ADAP_Uint32 u4IpSubNetMask, tEnetHal_NpNextHopInfo routeEntry,
									ADAP_Int32 *pi4FreeDefIpB4Del);

ADAP_Uint32 EnetHal_FsNpIpv4ArpAdd (tEnetHal_VlanId u2VlanId, ADAP_Uint32 u4IfIndex,
									ADAP_Uint32 u4IpAddr, tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,
									ADAP_Int32 i1State, ADAP_Uint32 *pu4TblFull);

ADAP_Uint32 EnetHal_FsNpIpv4ArpModify (tEnetHal_VlanId u2VlanId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4PhyIfIndex,
									ADAP_Uint32 u4IpAddr, tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,ADAP_Int32 i1State);

ADAP_Uint32 EnetHal_FsNpIpv4ArpDel (ADAP_Uint32 u4IpAddr, ADAP_Uint8 *pu1IfName, ADAP_Int8 i1State);

void EnetHal_FsNpIpv4ClearArpTable (void);

ADAP_Uint8 EnetHal_FsNpIpv4CheckHitOnArpEntry (ADAP_Uint32 u4IpAddress, ADAP_Uint8 u1NextHopFlag);

ADAP_Uint32 EnetHal_FsNpIpv4UcAddRoute (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr, ADAP_Uint32 u4IpSubNetMask,
		tEnetHal_NpNextHopInfo * pRouteEntry, ADAP_Uint8 *pbu1TblFull);

ADAP_Uint32 EnetHal_FsNpIpv4CreateL3IpInterface (tEnetHal_NpIpInterface *pL3Interface);
ADAP_Uint32 EnetHal_FsNpIpv4ModifyL3IpInterface (tEnetHal_NpIpInterface *pL3Interface);

/* Router Port part */
ADAP_Uint32  EnetHal_FsNpIpv4L3IpInterface (eEnetHal_L3Action L3Action, tEnetHal_FsNpL3IfInfo * pFsNpL3IfInfo);

ADAP_Uint32 EnetHal_FsNpL3Ipv4ArpAdd (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4IpAddr, 
                                      tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,
                                      ADAP_Int32 i1State, ADAP_Uint32 *pu4TblFull);

ADAP_Uint32 EnetHal_FsNpL3Ipv4ArpModify (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4IpAddr, 
                                      tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName, ADAP_Int32 i1State);
/* Ipv6 part */
ADAP_Uint32 EnetHal_FsNpIpv6Init(void);
void EnetHal_FsNpIpv6Deinit(void);
ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheAdd (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex,
    ADAP_Uint8 *pu1HwAddr, ADAP_Uint8 u1HwAddrLen, ADAP_Uint8 u1ReachStatus, ADAP_Uint16 u2VlanId);
ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheDel (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex);
ADAP_Uint8 EnetHal_FsNpIpv6CheckHitOnNDCacheEntry (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint32 u4IfIndex);
ADAP_Uint32 EnetHal_FsNpIpv6UcRouteAdd (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix, ADAP_Uint8 u1PrefixLen,
    ADAP_Uint8 *pu1NextHop, ADAP_Uint8 u4NHType, tEnetHal_NpIntInfo *pIntInfo);
ADAP_Uint32 EnetHal_FsNpIpv6UcRouteDelete (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix, ADAP_Uint8 u1PrefixLen,
    ADAP_Uint8 *pu1NextHop, ADAP_Uint32 u4IfIndex, tEnetHal_NpRouteInfo *pRouteInfo);
ADAP_Uint32 EnetHal_FsNpIpv6RtPresentInFastPath (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Prefix,
    ADAP_Uint8 u1PrefixLen, ADAP_Uint8 *pu1NextHop,  ADAP_Uint32 u4IfIndex);
ADAP_Uint32 EnetHal_FsNpIpv6UcRouting (ADAP_Uint32 u4VrId, ADAP_Uint32 u4RoutingStatus);
ADAP_Uint32 EnetHal_FsNpIpv6GetStats (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4StatsFlag,
    ADAP_Uint32 *pu4StatsValue);
ADAP_Uint32 EnetHal_FsNpIpv6IntfStatus (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfStatus, tEnetHal_NpIntInfo *pIntfInfo);
ADAP_Uint32 EnetHal_FsNpIpv6AddrCreate (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4AddrType,
    ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint8 u1PrefixLen);
ADAP_Uint32 EnetHal_FsNpIpv6AddrDelete (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4AddrType,
    ADAP_Uint8 *pu1Ip6Addr, ADAP_Uint8 u1PrefixLen);
ADAP_Uint32 EnetHal_FsNpIpv6TunlParamSet (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4TunlType,
    ADAP_Uint32 u4SrcAddr, ADAP_Uint32 u4DstAddr);
ADAP_Uint32 EnetHal_FsNpIpv6AddMcastMAC (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex,
    ADAP_Uint8 *pu1MacAddr, ADAP_Uint8 u1MacAddrLen);
ADAP_Uint32 EnetHal_FsNpIpv6DelMcastMAC (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IfIndex,
    ADAP_Uint8 *pu1MacAddr, ADAP_Uint8 u1MacAddrLen);
ADAP_Uint32 EnetHal_FsNpIpv6HandleFdbMacEntryChange (ADAP_Uint8 *pu1L3Nexthop, ADAP_Uint32 u4IfIndex,
    ADAP_Uint8 *pu1MacAddr, ADAP_Uint8 u4IsRemoved);
ADAP_Uint32 EnetHal_FsNpOspf3Init (void);
void EnetHal_FsNpOspf3DeInit (void);
ADAP_Uint32 EnetHal_FsNpRip6Init (void);
ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheGet (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr,
    EnetHal_NpIpv6Neighbor *Ipv6Neigbor);
ADAP_Uint32 EnetHal_FsNpIpv6NeighCacheGetNext (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1Ip6Addr,
    EnetHal_NpIpv6Neighbor *Ipv6Neigbor);
ADAP_Uint32 EnetHal_FsNpIpv6UcGetRoute (tEnetHal_NpRtm6Input Rtm6NpInParam,
    tEnetHal_NpRtm6Output *pRtm6NpOutParam);
ADAP_Uint32 EnetHal_FsNpIpv6Enable (ADAP_Uint32 u4Index, ADAP_Uint16  u2VlanId);
ADAP_Uint32 EnetHal_FsNpIpv6Disable (ADAP_Uint32 u4Index, ADAP_Uint16 u2VlanId);

#endif
