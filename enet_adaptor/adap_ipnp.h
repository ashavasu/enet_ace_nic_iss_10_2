/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_IPNP_H
#define ADAP_IPNP_H
#include "mea_api.h"

ADAP_Int32 adap_FsNpIpv4SetForwardingStatus (ADAP_Uint32 u4VrId, ADAP_Uint8 u1Status);

ADAP_Uint32 adap_GetOutInterfaceAddrFromIpClassL3Interface(ADAP_Uint32 ipAddr,ADAP_Uint16 internalPort,ADAP_Uint32 *pIfIndex);

ADAP_Uint32 MiFiDivideIpClass(ADAP_Uint32 u4IpDestAddr,ADAP_Uint32 u4IpSubNetMask,ADAP_Uint32 *pIpClass,ADAP_Uint32 *pIpAddr,ADAP_Uint32 *pMaxNumTable);

ADAP_Uint32 MiFiHwCreateRouterEhp(MEA_EHP_Info_dbt *pEhp_info,tEnetHal_MacAddr *pSrcMac,tEnetHal_MacAddr *destMac,ADAP_Uint32 command,
					ADAP_Uint32 vlanId, ADAP_Uint32 InnervlanId);
ADAP_Uint32 adap_GetVlanAddrFromIpClassL3Interface(ADAP_Uint32 ipAddr,ADAP_Uint16 *pVlanId);

ADAP_Uint32 EnetHal_FsNpIpv4CreatePPPInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
						ADAP_Uint32 u4CfaIfIndex,
						ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
						ADAP_Uint16 u2VlanId, ADAP_Uint8 u1Port);
ADAP_Uint32 EnetHal_FsNpIpv4ModifyPPPInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
						ADAP_Uint32 u4CfaIfIndex,
						ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
						ADAP_Uint16 u2VlanId, ADAP_Uint8 u1Port);
MEA_Status MeaDrvDeleteActionIp(tIpInterfaceTable *pInterfaceTable,ADAP_Uint32 routeIpAddr);
#endif
