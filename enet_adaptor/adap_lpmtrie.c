#include "adap_lpminc.h"
#include "adap_lpmtrie.h"
#include "adap_utils.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
static tUtilSemId   gTrieDbSem;
tRadixNodeHead      gaTrieDbInstance[MAX_NUM_TRIE];


#define    TRIE_SEM_NAME      (const UINT1 *)"tris"
#define    TRIE_SEM_ID         gTrieDbSem


/**************************************************************************/

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbLibInit ()                                           */
/*                                                                         */
/*  Description   This function initialises an array. Each member of this  */
/*                array is a tRadixNodeHead structure and represents       */
/*                different instance of TrieDb. It also initialises integer  */
/*                fields of all members. It creates global semaphore.      */
/*                This ensures that even in presence of more applications  */
/*                TrieDbCrt () is always called in  mutual exclusion.        */
/*                                                                         */
/*  Call          This function should be called once                      */
/*  condition     when the TrieDb library needs to be initialised.           */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS - If successful in initialising TrieDb family.*/
/*                LPM_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbLibInit (VOID)
{
    adap_memset((tRadixNodeHead *) gaTrieDbInstance, 0,
            MAX_NUM_TRIE * sizeof (tRadixNodeHead));

    if (UtilCreateSem (TRIE_SEM_NAME, TRIE_SEM_COUNT, 0, &TRIE_SEM_ID)
        != LPM_SUCCESS)
    {
        TrieDbError (SEM_CREATE_FAIL);
        return (LPM_FAILURE);
    }
    return (LPM_SUCCESS);
}


/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbLibShut ()                                           */
/*                                                                         */
/*  Description   This function frees all the resources  occupied by all   */
/*                the instances of TrieDb.                                   */
/*                                                                         */
/*  Call          When all the resources are to be                         */
/*  condition     freed.                                                   */
/*                                                                         */
/*  Input(s)      None.                                                    */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*  Access        Global task - as applicable                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
VOID
TrieDbLibShut (VOID)
{
    UINT4               u4Instance = 0;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        TrieDb2DelSem (u4Instance, gaTrieDbInstance[u4Instance].SemId);
    }
    UtilSemDel (TRIE_SEM_ID);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbCrt ()                                               */
/*                                                                         */
/*  Description   This function is used to register the application as     */
/*                well as to create a TrieDb instance. It returns pRoot      */
/*                parameter  with the root of the selected TrieDb instance.  */
/*                It returns unique identifier for each application as     */
/*                application identity number.                             */
/*                                                                         */
/*  Call          Should be called once before using                       */
/*  condition     any other TrieDb library function.                         */
/*                                                                         */
/*  Input(s)      pCreateParams - Pointer to the tTrieDbCrtParams            */
/*                                structure.                               */
/*                                                                         */
/*  Output(s)     pRoot -         Pointer to the root of the instance of   */
/*                                the TrieDb (the tRadixNodeHead             */
/*                                structure), if successful.               */
/*                                                                         */
/*  Access        Applications who want to use the TrieDb library.           */
/*  privileges                                                             */
/*                                                                         */
/*  Return        Valid unique positive application identifier,            */
/*                if  successful.                                          */
/*                LPM_FAILURE - Otherwise. (A negative number represents  */
/*                              LPM_FAILURE.)                             */
/*                                                                         */
/* Status        This function is Obseleted. Recommended to use            */
/*               TrieDbCreateInstance                                        */
/*                                                                         */
/***************************************************************************/
tRadixNodeHead     *
TrieDbCrt (tTrieDbCrtParams * pCreateParams)
{
    UINT1               u1AppId;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance = 0;
    UINT1               au1SemName[MAX_TRIE_SEM_NAME_SIZE];
    tUtilSemId          TrieDbSemId = 0;

    UtilSemTake (TRIE_SEM_ID);

    if (NULL == pCreateParams->AppFns)
    {
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->u2KeySize > MAX_KEY_SIZE)
    {
        TrieDbError (MAX_KEY_SIZE_OVERFLOW);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    u1AppId = (UINT1) pCreateParams->u1AppId;
    if ((u1AppId > TRIE2_MAX_ROUTING_PROTOCOLS) || (u1AppId == 0))
    {
        TrieDbError (MAX_APP_OVERFLOW);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    pHead =
        TrieDbGetInstance ((UINT4) pCreateParams->u4Type,
                         (UINT2) pCreateParams->u2KeySize, &u4Instance);

    if (pHead == NULL)
    {
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    /* Verify whether the TrieDb instance is already initialized or not. If
     * instance not initialized then initialize the new instance. */
    if (gaTrieDbInstance[u4Instance].u2KeySize == 0)
    {
        gaTrieDbInstance[u4Instance].bPoolPerInst = TRUE;
        gaTrieDbInstance[u4Instance].bSemPerInst = TRUE;
        /* Instance not initialized earlier. */
        TrieDbCreateSemName ((INT4) u4Instance, au1SemName);
        if (UtilCreateSem (au1SemName, TRIE_SEM_COUNT, 0, &TrieDbSemId)
            != LPM_SUCCESS)
        {
            TrieDbError (SEM_CREATE_FAIL);
            UtilSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieDbInstance[u4Instance].pRadixNodeTop =
            TrieDbAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                 RADIX_NODE, u4Instance);
        if (gaTrieDbInstance[u4Instance].pRadixNodeTop == NULL)
        {
            TrieDb2DelSem (u4Instance, TrieDbSemId);
            UtilSemGive (TRIE_SEM_ID);
            return (NULL);
        }

        gaTrieDbInstance[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pLeft = NULL;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pRight = NULL;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pParent = NULL;
        gaTrieDbInstance[u4Instance].u4Type = pCreateParams->u4Type;
        gaTrieDbInstance[u4Instance].u2KeySize = (UINT2) pCreateParams->u2KeySize;
        gaTrieDbInstance[u4Instance].u4RouteCount = 0;
        gaTrieDbInstance[u4Instance].AppFns = pCreateParams->AppFns;
        gaTrieDbInstance[u4Instance].SemId = TrieDbSemId;
    }

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))
    {
        TrieDbError (ALREADY_USED_APP_ID);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    UtilSemGive (TRIE_SEM_ID);
    return (pHead);
}

/****************************************************************************/
/*                                                                          */
/*  Function      TrieDbCreateInstance                                        */
/*                                                                          */
/*  Description   This function is used to register the application as      */
/*                well as to create a TrieDb instance. It returns pRoot       */
/*                parameter  with the root of the selected TrieDb instance.   */
/*                It returns unique identifier for each application as      */
/*                application identity number.                              */
/*                   bSemPerInst -  Sem will be created per trie instance,  */
/*                                  if it is set to true.                   */
/*                   bValidateType- If there is any trie instance is present*/
/*                                  with the same type and key, same trie   */
/*                                  be used in case of true. Otherwise,     */
/*                                  New trie instance will created          */
/*                                                                          */
/*  Call          Should be called once before using                        */
/*  condition     any other TrieDb library function.                          */
/*                                                                          */
/*  Input(s)      pCreateParams - Pointer to the tTrieDbCrtParams             */
/*                                structure.                                */
/*                                                                          */
/*  Output(s)     pRoot -         Pointer to the root of the instance of    */
/*                                the TrieDb (the tRadixNodeHead              */
/*                                structure), if successful.                */
/*                                                                          */
/*  Return        Pointer to the root node of created trie instance         */
/*                if  successful, NULL  Otherwise.                          */
/*                                                                          */
/****************************************************************************/
tRadixNodeHead     *
TrieDbCreateInstance (tTrieDbCrtParams * pCreateParams)
{
    tUtilSemId          TrieDbSemId = 0;
    tRadixNodeHead     *pHead;
    UINT4               u4Instance = 0;
    UINT1               au1SemName[MAX_TRIE_SEM_NAME_SIZE];
    UINT1               u1AppId;

    if ((pCreateParams->bPoolPerInst == TRUE) &&
        (pCreateParams->bSemPerInst == TRUE))
    {
        return (TrieDbCrt (pCreateParams));
    }

    UtilSemTake (TRIE_SEM_ID);
    if (NULL == pCreateParams->AppFns)
    {
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->u2KeySize > MAX_TRIE2_KEY_SIZE)
    {
        TrieDbError (MAX_KEY_SIZE_OVERFLOW);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    u1AppId = (UINT1) pCreateParams->u1AppId;
    if ((u1AppId > TRIE2_MAX_ROUTING_PROTOCOLS) || (u1AppId == 0))
    {
        TrieDbError (MAX_APP_OVERFLOW);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    if (pCreateParams->bValidateType == TRUE)
    {
        /* If any trie instance matches with this
         * type and key value, get the same entry
         * else get free instance */
        pHead =
            TrieDbGetInstance ((UINT4) pCreateParams->u4Type,
                             (UINT2) pCreateParams->u2KeySize, &u4Instance);
    }
    else
    {
        /* No need to validate u4Type field,
         * separate trie needs to be created */
        pHead = TrieDbGetFreeInstance (&u4Instance);

    }

    if (pHead == NULL)
    {
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    /* Verify whether the TrieDb instance is already initialized or not. If
     * instance not initialized then initialize the new instance. */
    if (gaTrieDbInstance[u4Instance].u2KeySize == 0)
    {
        if (pCreateParams->bSemPerInst == TRUE)
        {
            /* Creating sem for this particular trie instance */
            TrieDbCreateSemName ((INT4) u4Instance, au1SemName);
            if (UtilCreateSem (au1SemName, TRIE_SEM_COUNT, 0, &TrieDbSemId)
                != LPM_SUCCESS)
            {
                TrieDbError (SEM_CREATE_FAIL);
                UtilSemGive (TRIE_SEM_ID);
                return (NULL);
            }
            gaTrieDbInstance[u4Instance].bSemPerInst = TRUE;
        }
        else
            gaTrieDbInstance[u4Instance].bSemPerInst = FALSE;

        if (pCreateParams->bPoolPerInst == TRUE)
        {

            gaTrieDbInstance[u4Instance].pRadixNodeTop =
                TrieDbAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                     RADIX_NODE, u4Instance);
            if (gaTrieDbInstance[u4Instance].pRadixNodeTop == NULL)
            {
                TrieDb2DelSem (u4Instance, TrieDbSemId);
                UtilSemGive (TRIE_SEM_ID);
                return (NULL);
            }
        }
        else
        {
            gaTrieDbInstance[u4Instance].pRadixNodeTop =
                TrieDbAllocateateNode ((UINT2) (pCreateParams->u2KeySize),
                                     RADIX_NODE, u4Instance);
            if (gaTrieDbInstance[u4Instance].pRadixNodeTop == NULL)
            {
                TrieDb2DelSem (u4Instance, TrieDbSemId);
                UtilSemGive (TRIE_SEM_ID);
                return (NULL);
            }

        }

        gaTrieDbInstance[u4Instance].pRadixNodeTop->u1BitToTest = 0x80;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->u1ByteToTest = 0;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pLeft = NULL;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pRight = NULL;
        gaTrieDbInstance[u4Instance].pRadixNodeTop->pParent = NULL;
        gaTrieDbInstance[u4Instance].u2KeySize = (UINT2) pCreateParams->u2KeySize;
        gaTrieDbInstance[u4Instance].u4RouteCount = 0;
        gaTrieDbInstance[u4Instance].AppFns = pCreateParams->AppFns;
        gaTrieDbInstance[u4Instance].SemId = TrieDbSemId;

        if (pCreateParams->bValidateType == TRUE)
            gaTrieDbInstance[u4Instance].u4Type = pCreateParams->u4Type;
        else
            gaTrieDbInstance[u4Instance].u4Type = 0xffffffff;

    }

    /* one bit offset as application id starts from 1 as per RFC 2096 */
    if (CHECK_BIT ((u1AppId - 1), pHead->u2AppMask))
    {
        TrieDbError (ALREADY_USED_APP_ID);
        UtilSemGive (TRIE_SEM_ID);
        return (NULL);
    }

    SET_BIT ((u1AppId - 1), pHead->u2AppMask);
    UtilSemGive (TRIE_SEM_ID);
    return (pHead);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbDel ()                                               */
/*                                                                         */
/*  Description   This function may delete internal structures of TrieDb     */
/*                after giving application specific information to         */
/*                AppSpecDelFunc () for deletion. *AppSpecDelFunc () is    */
/*                called after traversing the fixed number of application  */
/*                specific entries.                                        */
/*                                                                         */
/*  Call          When there is urgent need for shutdown operation.        */
/*  condition     (A typical condition is when resources are               */
/*                blocked and freeing the resources might help.)           */
/*                                                                         */
/*  Input(s)      pInputParams -         Pointer to the                    */
/*                                       tInputParams structure.           */
/*                (AppSpecDelFunc ()) -  A function to delete the          */
/*                                       application specific              */
/*                                       information.                      */
/*                pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Output(s)     pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        None.                                                    */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbDel (tInputParams * pInputParams,
         void (*AppSpecDelFunc) (VOID *), VOID *pDeleteOutParams)
{
    UINT4               u4StopFlag;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pDeleteNode = NULL;
    tRadixNodeHead     *pHead;
    INT4                i4RetVal;
    UINT4               u4Instance = 0;
    tUtilSemId          TrieDbSemId;
    VOID               *pDelCtx;

    VOID               *(*pInitDeleteCtx) (tInputParams *,
                                           VOID (*AppSpecDelFunc1) (VOID *),
                                           VOID *);
    INT4                (*pDelete) (VOID *pDelCtx1, VOID **ppAppPtr, tKey Key);
    VOID                (*pDeInitDeleteCtx) (VOID *pDelCtx1);

    UtilSemTake (TRIE_SEM_ID);
    pHead = pInputParams->pRoot;

    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;

    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pInitDeleteCtx = pHead->AppFns->pInitDeleteCtx;
    pDelete = pHead->AppFns->pDelete;
    pDeInitDeleteCtx = pHead->AppFns->pDeInitDeleteCtx;

    if (NULL == (pDelCtx = (pInitDeleteCtx) (pInputParams, AppSpecDelFunc,
                                             pDeleteOutParams)))
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    pHead = pInputParams->pRoot;

    u4StopFlag = 0;

    pPrevNode = (tRadixNode *) pHead->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            u4StopFlag = 1;
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    while (!(u4StopFlag))
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            if (NULL != pDeleteNode)
            {
                TrieDbDelRadixLeaf (pHead, pDeleteNode, u4Instance);
                pDeleteNode = NULL;
                pPrevNode = ((tLeafNode *) pCurrNode)->pParent;
            }

            i4RetVal =
                (pDelete) (pDelCtx,
                           &((tLeafNode *) pCurrNode)->pAppSpecPtr,
                           ((tLeafNode *) pCurrNode)->Key);
            if (i4RetVal == LPM_FAILURE)
            {
                pDeInitDeleteCtx (pDelCtx);
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return i4RetVal;
            }

            if (NULL == ((tLeafNode *) pCurrNode)->pAppSpecPtr)
            {
                /* App has cleared AppSpecPtr ..
                   meaning no use for this TrieDbNode */
                pHead->u4RouteCount--;
                pDeleteNode = pCurrNode;
            }
            else
            {
                pDeleteNode = NULL;
            }

            /* pointer updation to reach the next leaf node */
            if (pPrevNode == NULL)
            {
                pDeInitDeleteCtx (pDelCtx);
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return LPM_FAILURE;
            }
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (VOID *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        /*  the node is either the null node or the radix node */
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            pPrevNode = (tRadixNode *) pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
    }

    if (NULL != pDeleteNode)
    {
        TrieDbDelRadixLeaf (pHead, pDeleteNode, u4Instance);
        pDeleteNode = NULL;
    }

    pDeInitDeleteCtx (pDelCtx);

    if (pInputParams->i1AppId == -1)
    {
        /* Clearing all the Application Specific info from the TRIE */
        pHead->u2AppMask = 0;
    }
    else
    {
        /* Clearing a Specific Application Info from the TRIE */
        RESET_BIT (pInputParams->i1AppId - 1, pHead->u2AppMask);
    }

    /* no entries are there in this instance */
    if (pHead->u2AppMask == 0)
    {
        if ((gaTrieDbInstance[u4Instance].u2KeySize) > U4_SIZE)
        {
            TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pHead->pRadixNodeTop->Mask.pKey);
        }

        TrieDbReleaseNode (RADIX_NODE, u4Instance,
                         (UINT1 *) pHead->pRadixNodeTop);
        pHead->u2KeySize = 0;
        pHead->u4Type = 0;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        TrieDb2DelSem (u4Instance, TrieDbSemId);
    }
    else
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
    }

    return (LPM_SUCCESS);
}


/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbDelInstance ()                                       */
/*                                                                         */
/*  Description   This function may delete internal structures of TrieDb     */
/*                after giving application specific information to         */
/*                AppSpecDelFunc () for deletion. *AppSpecDelFunc () is    */
/*                called after traversing the fixed number of application  */
/*                specific entries.                                        */
/*                                                                         */
/*  Call          When there is urgent need for shutdown operation.        */
/*  condition     (A typical condition is when resources are               */
/*                blocked and freeing the resources might help.)           */
/*                                                                         */
/*  Input(s)      pInputParams -         Pointer to the                    */
/*                                       tInputParams structure.           */
/*                (AppSpecDelFunc ()) -  A function to delete the          */
/*                                       application specific              */
/*                                       information.                      */
/*                pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Output(s)     pDeleteOutParams -     Pointer to the tDeleteOutParams   */
/*                                       structure.                        */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS/LPM_FAILURE                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbDelInstance (tInputParams * pInputParams,
                 void (*AppSpecDelFunc) (VOID *), VOID *pDeleteOutParams)
{
    UINT4               u4StopFlag;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pDeleteNode = NULL;
    tRadixNodeHead     *pHead;
    INT4                i4RetVal;
    UINT4               u4Instance = 0;
    tUtilSemId          TrieDbSemId;
    VOID               *pDelCtx;

    VOID               *(*pInitDeleteCtx) (tInputParams *,
                                           VOID (*AppSpecDelFunc1) (VOID *),
                                           VOID *);
    INT4                (*pDelete) (VOID *pDelCtx1, VOID **ppAppPtr, tKey Key);
    VOID                (*pDeInitDeleteCtx) (VOID *pDelCtx1);

    UtilSemTake (TRIE_SEM_ID);
    pHead = pInputParams->pRoot;

    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;

    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pInitDeleteCtx = pHead->AppFns->pInitDeleteCtx;
    pDelete = pHead->AppFns->pDelete;
    pDeInitDeleteCtx = pHead->AppFns->pDeInitDeleteCtx;

    if (NULL == (pDelCtx = (pInitDeleteCtx) (pInputParams, AppSpecDelFunc,
                                             pDeleteOutParams)))
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    pHead = pInputParams->pRoot;

    u4StopFlag = 0;

    pPrevNode = (tRadixNode *) pHead->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            u4StopFlag = 1;
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    while (!(u4StopFlag))
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            if (NULL != pDeleteNode)
            {
                TrieDbDelRadixLeaf (pHead, pDeleteNode, u4Instance);
                pDeleteNode = NULL;
                pPrevNode = ((tLeafNode *) pCurrNode)->pParent;
            }

            i4RetVal =
                (pDelete) (pDelCtx,
                           &((tLeafNode *) pCurrNode)->pAppSpecPtr,
                           ((tLeafNode *) pCurrNode)->Key);
            if (i4RetVal == LPM_FAILURE)
            {
                pDeInitDeleteCtx (pDelCtx);
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return i4RetVal;
            }

            if (NULL == ((tLeafNode *) pCurrNode)->pAppSpecPtr)
            {
                /* App has cleared AppSpecPtr ..
                   meaning no use for this TrieDbNode */
                pHead->u4RouteCount--;
                pDeleteNode = pCurrNode;
            }
            else
            {
                pDeleteNode = NULL;
            }

            /* pointer updation to reach the next leaf node */
            if (pPrevNode == NULL)
            {
                pDeInitDeleteCtx (pDelCtx);
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return LPM_FAILURE;
            }
            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (VOID *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        /*  the node is either the null node or the radix node */
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            pPrevNode = (tRadixNode *) pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
    }

    if (NULL != pDeleteNode)
    {
        TrieDbDelRadixLeaf (pHead, pDeleteNode, u4Instance);
        pDeleteNode = NULL;
    }

    pDeInitDeleteCtx (pDelCtx);

    if (pInputParams->i1AppId == -1)
    {
        /* Clearing all the Application Specific info from the TRIE */
        pHead->u2AppMask = 0;
    }
    else
    {
        /* Clearing a Specific Application Info from the TRIE */
        RESET_BIT (pInputParams->i1AppId - 1, pHead->u2AppMask);
    }

    /* no entries are there in this instance */
    if (pHead->u2AppMask == 0)
    {
        if ((gaTrieDbInstance[u4Instance].u2KeySize) > U4_SIZE)
        {
            TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pHead->pRadixNodeTop->Mask.pKey);
        }

        TrieDbReleaseNode (RADIX_NODE, u4Instance,
                         (UINT1 *) pHead->pRadixNodeTop);
        pHead->u2KeySize = 0;
        pHead->u4Type = 0;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        TrieDb2DelSem (u4Instance, TrieDbSemId);
    }
    else
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
    }
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbAdd ()                                               */
/*                                                                         */
/*  Description   This function adds the pAppSpecInfo in an instance of    */
/*                TrieDb. If the application specific information is         */
/*                already present in the leaf node, it is replaced with    */
/*                the new information and the old information is           */
/*                returned using tOutputParams. If this operation is       */
/*                aborted due to resource problems, it has the return      */
/*                value LPM_FAILURE.                                      */
/*                                                                         */
/*  Call          Whenever an application wants to add the entry or        */
/*  condition     update already existing entry.                           */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                pAppSpecInfo -    Pointer to the application specific    */
/*                                  information.                           */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS  -  If  adding  operation   is              */
/*                            successful.                                  */
/*                LPM_FAILURE -   If it fails due to any                  */
/*                            reasons.                                     */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbAdd (tInputParams * pInputParams, VOID *pAppSpecInfo, VOID *pOutputParams)
{
    UINT1               u1AppId = 0;
    UINT2               u2KeySize = 0;
    UINT1               u1DiffBit = 0;
    UINT1               u1BitToTest = 0;
    UINT1               u1DiffByteVal = 0;
    UINT4               u4DiffByte = 0;
    UINT4		u4Metric = 0;
    INT4                i4Idx = 0;
    tKey                InKey;
    tRadixNode         *pRadix = NULL;
    tRadixNode         *pParentNode = NULL;
    tLeafNode          *pLeaf = NULL;
    tLeafNode          *pPrevLeaf = NULL;
    tLeafNode          *pNewLeaf = NULL;
    tTrieDbApp           *pCurrApp;
    tTrieDbApp           *pTmpApp;
    tTrieDbApp          **pPrevApp;
    tTrieDbApp           *pPrevPath = NULL;
    tUtilSemId          TrieDbSemId;
    UINT4               u4Instance;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return (LPM_FAILURE);
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pRadix = pInputParams->pRoot->pRadixNodeTop;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;
    u1AppId = (UINT1) pInputParams->i1AppId;

    pLeaf = NULL;

    /* When TrieDbTravese fails to find the nearest leaf node
     * this function tries to add a leaf.
     */
    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        if ((TrieDbInsertLeaf (u2KeySize, pInputParams, pAppSpecInfo,
                             ((tRadixNode *) pLeaf),
                             pOutputParams, &pNewLeaf)) == LPM_FAILURE)
        {
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_FAILURE);
        }

        /* instead of calling TrieDbSetMask () with u2KeySize
         * It is called here with size of the mask.
         */

        /* pLeaf represents parent of the leaf node here */
        TrieDbSetMask (u2KeySize, pLeaf);
        /* this is done in TrieDbInsertLeaf()
           pOutputParams->pAppSpecInfo = (VOID *) NULL;
         */

        if (NULL == (pPrevLeaf = TrieDbGetPrevLeafNode (pNewLeaf)))
        {
            /* means we are the left most node of the trie */
            DLIST_INSERT_HEAD ((pInputParams->pRoot), NodeHead, pNewLeaf,
                               NodeLink);
        }
        else
        {
            DLIST_INSERT ((pInputParams->pRoot), NodeHead, pNewLeaf, NodeLink,
                          pPrevLeaf);
        }
        pInputParams->pRoot->u4RouteCount++;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_SUCCESS);
    }

    /* TrieDbDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    DIFFBYTE (pLeaf->Key, InKey, u2KeySize, u4DiffByte, u1DiffByteVal);

    if (u4DiffByte == u2KeySize)
    {
        /* reach to the specific entry */
        pCurrApp = (tTrieDbApp *) (pLeaf->pAppSpecPtr);
        pPrevApp = (tTrieDbApp **) (VOID *) &(pLeaf->pAppSpecPtr);
        for (i4Idx = 0; i4Idx < u1AppId; i4Idx++)
        {
            if (CHECK_BIT (i4Idx, pLeaf->u2AppMask))
            {
                pPrevApp = (tTrieDbApp **) (VOID *) &(pCurrApp->pNextApp);
                pCurrApp = (tTrieDbApp *) (pCurrApp->pNextApp);
            }
        }

        /* Check whether the application is present or not */
        if (CHECK_BIT ((INT4) pInputParams->i1AppId, pLeaf->u2AppMask))
        {
            /* Application is present. Either add or replace the entry */
            pTmpApp = (tTrieDbApp *) (pCurrApp);
            if (pTmpApp == NULL)
            {
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
            	return LPM_FAILURE;
	    }
	    u4Metric = ((tTrieDbApp *) pAppSpecInfo)->u4RtMetric;

	    /* Incoming Route's metric is lesser than the first entry */
	    if (pTmpApp->u4RtMetric > u4Metric)
	    {
		((tTrieDbApp *) pAppSpecInfo)->pNextApp = pTmpApp->pNextApp;
		((tTrieDbApp *) pAppSpecInfo)->pNextAlternatepath = pTmpApp;
		*pPrevApp = (tTrieDbApp *) pAppSpecInfo;
		pTmpApp->pNextApp = NULL;
	    }
	    else
	    {
		while ((pTmpApp != NULL)
		    && (pTmpApp->u4RtMetric <= u4Metric))
		{
		 	/*Avoid adding duplicate nodes */
		    if (((tTrieDbApp *) pAppSpecInfo)->u4NxtHop ==
			    pTmpApp->u4NxtHop)
		    {
            		TrieDb2GiveSem (u4Instance, TrieDbSemId);
            		return LPM_FAILURE;
		    }
		    pPrevPath = pTmpApp;
		    pTmpApp = pTmpApp->pNextAlternatepath;
		}

		((tTrieDbApp *) pAppSpecInfo)->pNextApp = NULL;
		((tTrieDbApp *) pAppSpecInfo)->pNextAlternatepath = pTmpApp;
		if (pPrevPath == NULL)
		{
            	    TrieDb2GiveSem (u4Instance, TrieDbSemId);
            	    return LPM_FAILURE;
		}
		pPrevPath->pNextAlternatepath = (tTrieDbApp *) pAppSpecInfo;
	    }
	}
    }
    else
    {
        DIFFBIT (u1DiffBit, u1DiffByteVal);
        u1BitToTest = (UINT1) (0x80 >> u1DiffBit);

        /*u1DiffBit   += BYTE_LENGTH*u4DiffByte; */
        pParentNode = pLeaf->pParent;
        while ((u4DiffByte < pParentNode->u1ByteToTest) ||
               ((u4DiffByte == pParentNode->u1ByteToTest) &&
                (u1BitToTest > pParentNode->u1BitToTest)))
        {
            pParentNode = pParentNode->pParent;
        }
        if (TrieDbInsertRadix
            (u2KeySize, u1BitToTest, (UINT1) u4DiffByte, pInputParams,
             pAppSpecInfo, pParentNode, pOutputParams) == LPM_FAILURE)
        {
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_FAILURE);
        }
    }

    /* Make output parameters null to represent fresh addition */
    /* this is done in TrieDbInsertLeaf()
       pOutputParams->pAppSpecInfo = (VOID *) NULL;
     */

    (pInputParams->pRoot)->u4RouteCount++;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbRemove()                                             */
/*                                                                         */
/*  Description   Delete operation is always preceded by an exact search   */
/*                internally. After finding the required information, the  */
/*                application specific information and the key is          */
/*                returned to the application using the tOutputParams      */
/*                structure to delete actually. If search operation is     */
/*                unsuccessful, this function returns with LPM_FAILURE.   */
/*                This function deletes unnecessary radix/leaf node,if any.*/
/*                                                                         */
/*  Call          Whenever an application wants to remove an entry from    */
/*  condition     this instance of TrieDb.                                   */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*                                                                         */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS  -  If  deleting  operation  is successful. */
/*                LPM_FAILURE  -  If  it  fails  due  to  any reasons.    */
/*                                                                         */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbRemove (tInputParams * pInputParams, VOID *pOutputParams, VOID *pNxtHop)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    INT4                i4RetVal = LPM_SUCCESS;
    UINT4               u4Instance = 0;
    tUtilSemId          TrieDbSemId;

    INT4                (*pDeleteEntry) (tInputParams * pInputParams1,
                                         VOID **ppAppPtr, VOID *pOutputParams1,
                                         VOID *pNxtHop1, tKey Key);

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pDeleteEntry = pInputParams->pRoot->AppFns->pDeleteEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    /* TrieDbDoTraverse returned a leaf node. */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    /* TODO ::if the leaf is found .. 
       1. call app spec delete fn 
       2. if the function returns NULL, delete LeafNode
       3. else assign the return value to leafnode->appspecinfo
     */
    if (LPM_FAILURE == (pDeleteEntry) (pInputParams, &pLeaf->pAppSpecPtr,
                                        pOutputParams, pNxtHop, pLeaf->Key))
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_FAILURE;
    }

    if (NULL == (tLeafNode *) pLeaf->pAppSpecPtr)
    {
        /* App has cleared AppSpecPtr .. meaning no use for this TrieDbNode */
        TrieDbDelRadixLeaf ((tRadixNodeHead *) pInputParams->pRoot, pLeaf,
                          u4Instance);
        pInputParams->pRoot->u4RouteCount--;
    }

    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*  Function      INT4 TrieDbSearch ()                                       */
/*                                                                         */
/*  Description   This function searches for the specified key given by    */
/*                the tInputParams in an instance of TrieDb. If the key is   */
/*                found, this function copies the application specific     */
/*                information and key of the searched entry to the         */
/*                tOutputParams. If the  matching key is not found,        */
/*                it returns LPM_FAILURE. This is an exact match operation*/
/*                The result of this operation should not be used to       */
/*                delete entries present in the TrieDb instance.             */
/*                TrieDbRemove () must be called for deleting entries.       */
/*                                                                         */
/*  Call          whenever an application needs to search for              */
/*  condition     information associated with the given key only.          */
/*                                                                         */
/*  Input(s)      pInputParams -  Pointer to the tInputParams structure.   */
/*                                                                         */
/*  Output(s)     pOutputParams - Pointer to the tOutputParams structure.  */
/*                ppNode - TrieDb Node where this route is found. Application*/
/*                         should not do any processing with this node. It */
/*                         can just pass this info in the next Search      */
/*                         request before the TrieDb is updated for enabling */
/*                         a faster search.                                */
/*                                                                         */
/*  Access        The applications using the TrieDb library.                 */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS - If the key is found.                      */
/*                LPM_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbSearch (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf = NULL;
    tUtilSemId          TrieDbSemId;
    INT4                (*pSearchEntry) (tInputParams * pInputParams1,
                                         VOID *pOutputParams1, VOID *pLeaf1);

    UINT4               u4Instance;

    *ppNode = NULL;
    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;

    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pSearchEntry = pInputParams->pRoot->AppFns->pSearchEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (pInputParams->pLeafNode != NULL)
    {
        /* Input parameter is carrying the pointer to the Leaf node. Validate
         * whether it is a valid pointer or not. If yes, then use it else
         * traverse and find appropriate leaf */
        pLeaf = (tLeafNode *) pInputParams->pLeafNode;
        i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
        if (i4Eql != 0)
        {
            /* Seems the input leaf node is not valid. */
            pLeaf = NULL;
        }
    }

    if (pLeaf == NULL)
    {
        if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
        {
            *ppNode = NULL;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_FAILURE);
        }
    }

    /* TrieDbDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    if (LPM_FAILURE ==
        (pSearchEntry) (pInputParams, pOutputParams, pLeaf->pAppSpecPtr))
    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_FAILURE;
    }

    *ppNode = (VOID *) pLeaf;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbLookup()                                             */
/*                                                                         */
/*  Description   This function first searches for an exact match in an    */
/*                instance of TrieDb. However, if it is not successful in    */
/*                finding exact match, it looks for the best match. If it  */
/*                fails in finding the best match also, it returns         */
/*                LPM_FAILURE. The result of this operation should not be */
/*                used to delete entries which are present in TrieDb. If     */
/*                the return value is LPM_SUCCESS, tOutputParams will     */
/*                contain related information of exact/best match including*/
/*                the key.                                                 */
/*                                                                         */
/*  Call          This function is mostly appropriate to routing           */
/*  condition     tables. Here, a key of the tInputParams represents the   */
/*                IP address. The application, by calling this function,   */
/*                requests best match.                                     */
/*                                                                         */
/*  Input(s)      pInputParams -    Pointer to the tInputParams            */
/*                                  structure.                             */
/*  Output(s)     pOutputParams -   Pointer to the tOutputParams           */
/*                                  structure.                             */
/*                ppNode - TrieDb Node where this route is found. Application*/
/*                         should not do any processing with this node. It */
/*                         can just pass this info in the next Search      */
/*                         request before the TrieDb is updated for enabling */
/*                         a faster search.                                */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS  -  If it finds the exact/best match.       */
/*                LPM_FAILURE  -  If it fails in finding  the             */
/*                            best match also.                             */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbLookup (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Eql;
    INT4                i4Return;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    tUtilSemId          TrieDbSemId;

    INT4                (*pLookupEntry) (tInputParams * pInputParams1,
                                         VOID *pOutputParams1, VOID *pAppPtr,
                                         UINT2 u2KeySize1, tKey Key1);

    UINT4               u4Instance;
    *ppNode = NULL;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;

    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pLookupEntry = pInputParams->pRoot->AppFns->pLookupEntry;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        if (u2KeySize > U4_SIZE)
        {
            i4Return =
                TrieDbGetBestMatch (u2KeySize, pInputParams, (VOID *) pLeaf,
                                  pOutputParams);
        }
        else
        {
            i4Return =
                TrieDbGetBestMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf,
                                   pOutputParams);
        }
        if (i4Return == LPM_FAILURE)
        {
            *ppNode = NULL;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_FAILURE);
        }
        else
        {
            *ppNode = pLeaf;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_SUCCESS);
        }
    }

    /* TrieDbDoTraverse returned a leaf node. Copy the key from it */

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql == 0)
    {
        /* call pLookupentry (), if success return if failure continue 
         */
        i4Return =
            (pLookupEntry) (pInputParams, pOutputParams, pLeaf->pAppSpecPtr,
                            u2KeySize, pLeaf->Key);
        if (LPM_SUCCESS == i4Return)
        {
            *ppNode = pLeaf;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return LPM_SUCCESS;
        }
    }
    /* else continue !! */

    if (u2KeySize > U4_SIZE)
    {
        i4Return =
            TrieDbGetBestMatch (u2KeySize, pInputParams, (VOID *) pLeaf,
                              pOutputParams);
    }
    else
    {
        i4Return =
            TrieDbGetBestMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf,
                               pOutputParams);
    }
    if (i4Return == LPM_SUCCESS)
    {
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_SUCCESS);
    }
    else
    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbWalk ()                                              */
/*                                                                         */
/*  Description   This function scans entire instance of TrieDb for a        */
/*                given application. AppSpecScanFunc () is called after    */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns LPM_FAILURE, this*/
/*                function also returns LPM_FAILURE.                      */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     TrieDb has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS -  If this operation  is  done successfully.*/
/*                LPM_FAILURE -  If this operation is aborted due to the  */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbWalk (tInputParams * pInputParams,
          INT4 (*AppSpecScanFunc) (VOID *), VOID *pScanOutParams)
{
    VOID               *pCurrNode;
    VOID               *pNextNode = NULL;
    VOID               *pScanCtx;
    UINT4               u4Instance = 0;
    tUtilSemId          TrieDbSemId;
    VOID               *(*pInitScanCtx) (tInputParams * pInputParams1,
                                         VOID (*AppSpecScanFunc1) (VOID *),
                                         VOID *pScanOutParams1);
    INT4                (*pScan) (VOID *pScanCtx1, VOID **ppAppPtr1, tKey Key1);
    VOID                (*pDeInitScanCtx) (VOID *pScanCtx1);

    UtilSemTake (TRIE_SEM_ID);

    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pInitScanCtx = pInputParams->pRoot->AppFns->pInitScanCtx;
    pScan = pInputParams->pRoot->AppFns->pScan;
    pDeInitScanCtx = pInputParams->pRoot->AppFns->pDeInitScanCtx;

    if (NULL ==
        (pScanCtx = (pInitScanCtx) (pInputParams,
                                    (VOID (*)(VOID *)) AppSpecScanFunc,
                                    pScanOutParams)))
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    pCurrNode = pInputParams->pRoot->NodeHead.pNext;
    if (pCurrNode == NULL)
    {
        /* TRIE is EMPTY */
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_SUCCESS);
    }

    while (pCurrNode != NULL)
    {
        /* Use the DLIST_NODE to Scan through the TRIE. First Get and
         * store the next Leaf Node information from the current
         * leaf node and process the current node. */

        pNextNode = ((tLeafNode *) pCurrNode)->NodeLink.pNext;

        if (LPM_FAILURE ==
            (pScan) (pScanCtx, &(((tLeafNode *) pCurrNode)->pAppSpecPtr),
                     ((tLeafNode *) pCurrNode)->Key))
        {
            pDeInitScanCtx (pScanCtx);
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return LPM_FAILURE;
        }

        if (((tLeafNode *) pCurrNode)->pAppSpecPtr == NULL)
        {
            /* All the AppSpec information within this node has been
             * cleared, within the Scan Call-back Routine.
             * This node can be removed. */
            TrieDbDelRadixLeaf ((tRadixNodeHead *) pInputParams->pRoot,
                              pCurrNode, u4Instance);
            pInputParams->pRoot->u4RouteCount--;
        }
        pCurrNode = pNextNode;
    }
    pDeInitScanCtx (pScanCtx);

    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbGetNext                                              */
/*                                                                         */
/*  Description   This function finds the next key and the associated      */
/*                application specfic entry  using a given key and         */
/*                application id. The input key need not be present        */
/*                inside the trie structure. If input id is a generic id,  */
/*                it returns every application specific information along  */
/*                with the key.                                            */
/*                                                                         */
/*  Call         When next entry in the routing table is needed.           */
/*  condition    typically for SNMP.                                       */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*               ppNode - TrieDb Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the TrieDb is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the TrieDb library.                 */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the next entry.   */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbGetNext (tInputParams * pInputParams, VOID *pOutputParams, VOID **ppNode)
{
    UINT2               u2KeySize;
    INT4                i4Cmp;
    tKey                InKey;
    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    tLeafNode          *pLeaf;
    tUtilSemId          TrieDbSemId;
    INT4                i4RetVal = LPM_SUCCESS;
    INT4                i4Eql = 0;
    INT4                (*pGetNextEntry) (tInputParams * pInputParams1,
                                          VOID *pOutputParams1, VOID *pAppPtr1,
                                          tKey Key1);

    UINT4               u4Instance = 0;

    *ppNode = NULL;
    pLeaf = NULL;

    UtilSemTake (TRIE_SEM_ID);

    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pGetNextEntry = pInputParams->pRoot->AppFns->pGetNextEntry;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    pPrevNode = pInputParams->pRoot->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    InKey = pInputParams->Key;
    if ((pPrevNode->pRight == NULL) && (pPrevNode->pLeft == NULL))

    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    if (pInputParams->pLeafNode != NULL)
    {
        /* Input parameter is carrying the pointer to the Leaf node. Validate
         * whether it is a valid pointer or not. If yes, then use it else
         * traverse and find appropriate leaf */
        pLeaf = (tLeafNode *) pInputParams->pLeafNode;
        i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
        if (i4Eql != 0)
        {
            /* Seems the input leaf node is not valid. */
            pLeaf = NULL;
        }
    }

    if (pLeaf == NULL)
    {
        i4RetVal = TrieDbDoTraverse (u2KeySize, pPrevNode, InKey,
                                   (VOID *) &(pLeaf));
    }

    if (i4RetVal == LPM_FAILURE)
    {
        pCurrNode = NULL;
        pPrevNode = (tRadixNode *) pLeaf;
    }
    else
    {
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u2KeySize);
        if (i4Cmp < 0)
        {
            if (LPM_SUCCESS == (pGetNextEntry) (pInputParams, pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
            {
                *ppNode = pLeaf;
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return LPM_SUCCESS;
            }
        }

        pPrevNode = pLeaf->pParent;
        pCurrNode = pLeaf;
    }

    if (pPrevNode == NULL)
    {
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }
    for (;;)
    {

        /* here logic is same as that used in TrieDbWalk */
        if (pPrevNode->pLeft == pCurrNode)
        {
            pCurrNode = pPrevNode->pRight;
        }
        else
        {
            while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
            {
                pCurrNode = (VOID *) pPrevNode;
                pPrevNode = pPrevNode->pParent;
            }
            if (pPrevNode == NULL)
            {
                *ppNode = NULL;
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return (LPM_FAILURE);
            }
            else
            {
                pCurrNode = pPrevNode->pRight;
            }
        }

        /* find out the next leaf node or NULL */
        while ((pCurrNode != NULL) &&
               (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE))

        {
            pPrevNode = pCurrNode;
            pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
        }
        if (pCurrNode == NULL)

        {
            *ppNode = NULL;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_FAILURE);
        }
        pLeaf = (tLeafNode *) pCurrNode;
        i4Cmp = KEYCMP (InKey, pLeaf->Key, u2KeySize);
        if (i4Cmp < 0)
        {
            if (LPM_SUCCESS == (pGetNextEntry) (pInputParams, pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
            {
                *ppNode = pLeaf;
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return LPM_SUCCESS;
            }
        }
    }

}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbRevise                                               */
/*                                                                         */
/*  Description   This function updates the alternate path list.           */
/*                Whenever there is a change in Metric or Row Status, this */
/*                function should be called. This sorts based on the new   */
/*                metric. This function helps to aVOID the row status check*/
/*                in Forwarding path.                                      */
/*                                                                         */
/*  Call         Whenever there is a change in Metric for the existing     */
/*  condition    route.                                                    */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pOutputParams - Pointer to the tOutputParams structure.   */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The appllications using the TrieDb library.                 */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the next entry.   */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbRevise (tInputParams * pInputParams,
            VOID *pAppSpecInfo, UINT4 u4NewMetric, VOID *pOutputParams)
{

    UINT2               u2KeySize;
    tKey                InKey;
    tRadixNode         *pRadix;
    tLeafNode          *pLeaf;
    tUtilSemId          TrieDbSemId;

    INT4                (*pUpdate) (tInputParams * pInputParams1,
                                    VOID *pOutputParams1, VOID **ppAppPtr1,
                                    VOID *pAppSpecInfo1, UINT4 u4NewMetric1);

    UINT4               u4Instance = 0;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pUpdate = pInputParams->pRoot->AppFns->pUpdate;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    pLeaf = NULL;
    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    /* TrieDbDoTraverse returned a leaf node. */
    if (LPM_FAILURE ==
        pUpdate (pInputParams, pOutputParams, &pLeaf->pAppSpecPtr, pAppSpecInfo,
                 u4NewMetric))

    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbWalkAll ()                                           */
/*                                                                         */
/*  Description   This function scans entire instance of TrieDb for all      */
/*                applications. AppSpecScanFunc () is called after         */
/*                traversing the fixed number of the application specific  */
/*                entries. If AppSpecScanFunc () returns LPM_FAILURE, this*/
/*                function also returns LPM_FAILURE.                      */
/*                                                                         */
/*  Call          This function is invoked when the instance of the        */
/*  condition     TrieDb has to be scanned.                                  */
/*                                                                         */
/*  Input(s)      pInputParams -          Pointer to the tInputParams      */
/*                                        structure.                       */
/*                (AppSpecScanFunc ()) -  Function to  process the         */
/*                                        application specific             */
/*                                        information.                     */
/*                pScanOutParams -        Pointer to the tScanOutParams    */
/*                                        structure.                       */
/*                                                                         */
/*  Output(s)     pScanOutParams -        Pointer to the tScanOutParams    */
/*  Access        Applications using the TrieDb library.                     */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS -  If this operation  is  done successfully.*/
/*                LPM_FAILURE -  If this operation is aborted due to the  */
/*                           application needs.                            */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbWalkAll (tInputParams * pInputParams,
             INT4 (*AppSpecScanFunc) (VOID *), VOID *pScanOutParams)
{

    VOID               *pCurrNode;
    tRadixNode         *pPrevNode;
    VOID               *pScanCtx;
    tUtilSemId          TrieDbSemId;

    VOID               *(*pInitScanCtx) (tInputParams * pInputParams1,
                                         VOID (*AppSpecScanFunc1) (VOID *),
                                         VOID *pScanOutParams1);
    INT4                (*pScanAll) (VOID *pScanCtx1, VOID *pAppPtr, tKey Key);
    VOID                (*pDeInitScanCtx) (VOID *pScanCtx1);

    UINT4               u4Instance = 0;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pInitScanCtx = pInputParams->pRoot->AppFns->pInitScanCtx;
    pScanAll = pInputParams->pRoot->AppFns->pScanAll;
    pDeInitScanCtx = pInputParams->pRoot->AppFns->pDeInitScanCtx;

    if (NULL == (pScanCtx = (pInitScanCtx) (pInputParams,
                                            (VOID (*)(VOID *)) AppSpecScanFunc,
                                            pScanOutParams)))
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }

    pPrevNode = pInputParams->pRoot->pRadixNodeTop;
    pCurrNode = (VOID *) pPrevNode;

    if (pPrevNode == NULL)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return (LPM_FAILURE);
    }
    if (pPrevNode->pLeft == NULL)
    {
        if (pPrevNode->pRight == NULL)
        {
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return (LPM_SUCCESS);
        }
        else
        {
            pCurrNode = pPrevNode->pRight;
        }
    }

    for (;;)
    {
        if ((pCurrNode != NULL) &&
            (((tLeafNode *) pCurrNode)->u1NodeType == LEAF_NODE))
        {
            /* give the leafnode, fnptr, out blah blah..
               to be freed by the code !! */
            if (LPM_FAILURE ==
                (pScanAll) (pScanCtx, ((tLeafNode *) pCurrNode)->pAppSpecPtr,
                            ((tLeafNode *) pCurrNode)->Key))
            {
                pDeInitScanCtx (pScanCtx);
                TrieDb2GiveSem (u4Instance, TrieDbSemId);
                return LPM_FAILURE;
            }

            if (pPrevNode->pLeft == pCurrNode)
            {
                pCurrNode = pPrevNode->pRight;
            }
            else
            {
                while ((pPrevNode != NULL) && (pPrevNode->pRight == pCurrNode))
                {
                    pCurrNode = (void *) pPrevNode;
                    pPrevNode = pPrevNode->pParent;
                }
                if (pPrevNode == NULL)
                {
                    break;
                }
                else
                {
                    pCurrNode = pPrevNode->pRight;
                }
            }
        }
        else
        {
            if (pCurrNode == NULL)
            {
                break;
            }

            /* radix node */
            while (((tRadixNode *) pCurrNode)->u1NodeType == RADIX_NODE)
            {
                pPrevNode = (tRadixNode *) pCurrNode;
                pCurrNode = ((tRadixNode *) pCurrNode)->pLeft;
            }
        }
    }

    pDeInitScanCtx (pScanCtx);

    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return (LPM_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbGetFirstNode                                         */
/*                                                                         */
/*  Description   This function finds the first node in the TRIE and       */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When first entry in a TRIE is needed.                     */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the AppSpec Information          */
/*               ppNode - TrieDb Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the TrieDb is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the TrieDb library.                  */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the first entry.  */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbGetFirstNode (tInputParams * pInputParams, VOID **pAppSpecPtr,
                  VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tUtilSemId          TrieDbSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    if (NULL != (pLeaf = DLIST_FIRSTNODE (pInputParams->pRoot, NodeHead)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return LPM_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbGetLastNode                                          */
/*                                                                         */
/*  Description   This function finds the last node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When last entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the AppSpec Information          */
/*               ppNode - TrieDb Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the TrieDb is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the TrieDb library.                  */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the last entry.   */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbGetLastNode (tInputParams * pInputParams, VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tUtilSemId          TrieDbSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    if (NULL != (pLeaf = DLIST_LASTNODE (pInputParams->pRoot, NodeHead)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return LPM_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbGetNextNode                                          */
/*                                                                         */
/*  Description   This function finds the next node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When next entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*               pGetCtx      -  Pointer to the node where the current     */
/*                               entry is present.                         */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the next AppSpec Information     */
/*               ppNode - TrieDb Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the TrieDb is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the TrieDb library.                  */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the next entry.   */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbGetNextNode (tInputParams * pInputParams, VOID *pGetCtx,
                 VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tKey                InKey;
    UINT2               u2KeySize;
    tRadixNode         *pRadix;
    INT4                i4Eql;
    tUtilSemId          TrieDbSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    if (NULL != pGetCtx)
    {
        if (NULL != (pLeaf = DLIST_NEXT ((tLeafNode *) pGetCtx, NodeLink)))
        {
            *pAppSpecPtr = pLeaf->pAppSpecPtr;
            *ppNode = pLeaf;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return LPM_SUCCESS;
        }
        else
        {
            *pAppSpecPtr = NULL;
            *ppNode = NULL;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return LPM_FAILURE;
        }
    }

    /* we should use the key to scan now */
    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_FAILURE;
    }

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_FAILURE;
    }

    if (NULL != (pLeaf = DLIST_NEXT ((tLeafNode *) pLeaf, NodeLink)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_SUCCESS;
    }

    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return LPM_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*  Function      TrieDbGetNextNode                                          */
/*                                                                         */
/*  Description   This function finds the prev node in the TRIE and        */
/*                returns the AppSpec Info and the Node.                   */
/*                                                                         */
/*  Call         When prev entry in a TRIE is needed.                      */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)     pInputParams -  Pointer to the tInputParams structure.    */
/*               pGetCtx      -  Pointer to the node where the current     */
/*                               entry is present.                         */
/*                                                                         */
/*  Output(s)    pAppSpecPtr - Pointer to the prev AppSpec Information     */
/*               ppNode - TrieDb Node where this route is found. Application */
/*                        should not do any processing with this node. It  */
/*                        can just pass this info in the next Search       */
/*                        request before the TrieDb is updated for enabling  */
/*                        a faster search.                                 */
/*                                                                         */
/*  Access                                                                 */
/*  privileges   The applications using the TrieDb library.                  */
/*                                                                         */
/*  Return       LPM_SUCCESS - If successful in finding the prev entry.   */
/*               LPM_FAILURE - Otherwise.                                 */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbGetPrevNode (tInputParams * pInputParams, VOID *pGetCtx,
                 VOID **pAppSpecPtr, VOID **ppNode)
{
    tLeafNode          *pLeaf;
    tKey                InKey;
    UINT2               u2KeySize;
    tRadixNode         *pRadix;
    INT4                i4Eql;
    tUtilSemId          TrieDbSemId;
    UINT4               u4Instance = 0;

    *pAppSpecPtr = NULL;
    *ppNode = NULL;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    /* If incoming Context is valid, use it */
    if (NULL != pGetCtx)
    {
        if (NULL != (pLeaf = DLIST_PREV ((tLeafNode *) pGetCtx, NodeLink)))
        {
            *pAppSpecPtr = pLeaf->pAppSpecPtr;
            *ppNode = pLeaf;
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            return LPM_SUCCESS;
        }
        else
        {
            TrieDb2GiveSem (u4Instance, TrieDbSemId);
            *pAppSpecPtr = NULL;
            *ppNode = NULL;
            return LPM_FAILURE;
        }
    }

    /* we should use the key to scan now */
    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;
    InKey = pInputParams->Key;

    if (TrieDbDoTraverse (u2KeySize, pRadix, InKey, &pLeaf) == LPM_FAILURE)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        return LPM_FAILURE;
    }

    /* check if keys  match  */
    i4Eql = KEYEQUAL (pInputParams->Key, pLeaf->Key, u2KeySize);
    if (i4Eql != 0)
    {
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        *pAppSpecPtr = NULL;
        *ppNode = NULL;
        return LPM_FAILURE;
    }

    if (NULL != (pLeaf = DLIST_PREV ((tLeafNode *) pLeaf, NodeLink)))
    {
        *pAppSpecPtr = pLeaf->pAppSpecPtr;
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return LPM_SUCCESS;
    }

    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    *pAppSpecPtr = NULL;
    *ppNode = NULL;
    return LPM_FAILURE;
}

INT4
TrieDbLookupOverlap (tInputParams * pInputParams, VOID *pOutputParams,
                   VOID **ppNode)
{
    /* pseudo code:
     * ~~~~~~~~~~~~
     * Get (FirstMatch)
     * if (FirstMatch)
     *   return FirstMatch;
     * else
     *   return NULL;
     */

    UINT2               u2KeySize;
    INT4                i4Ret;
    tLeafNode          *pLeaf = NULL;
    tUtilSemId          TrieDbSemId;

    INT4                (*pLookupEntryOverlap) (tInputParams * pInputParams1,
                                                VOID *pOutputParams1,
                                                VOID *AppSpecPtr1,
                                                UINT2 u2KeySize1, tKey Key);

    UINT4               u4Instance = 0;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        *ppNode = NULL;
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pLookupEntryOverlap =
        ((tRadixNodeHead *) pInputParams->pRoot)->AppFns->pLookupEntryOverlap;
    u2KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u2KeySize;
    pLeaf = TrieDbFirstMatch (pInputParams);
    if (NULL != pLeaf)
    {
        /* not an exact match... a first match return it */
        i4Ret = pLookupEntryOverlap (pInputParams, pOutputParams,
                                     pLeaf->pAppSpecPtr, u2KeySize, pLeaf->Key);
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return i4Ret;
    }

    /* No matching entry present. */
    *ppNode = NULL;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return LPM_FAILURE;
}

INT4
TrieDbLookupOverlapLessMoreSpecific (tInputParams * pInputParams,
                                   VOID *pOutputParams, VOID **ppNode)
{
    /* pseudo code:
     * ~~~~~~~~~~~~
     * Get (FirstMatch)
     * if (FirstMatch)
     *   return FirstMatch;
     * else
     *   return NULL;
     */

    UINT2               u2KeySize;
    INT4                i4Ret;
    tLeafNode          *pLeaf = NULL;
    tUtilSemId          TrieDbSemId;

    INT4                (*pLookupEntryOverlap) (tInputParams * pInputParams1,
                                                VOID *pOutputParams1,
                                                VOID *AppSpecPtr1,
                                                UINT2 u2KeySize1, tKey Key);

    UINT4               u4Instance = 0;

    UtilSemTake (TRIE_SEM_ID);
    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        *ppNode = NULL;
        UtilSemGive (TRIE_SEM_ID);
        return LPM_FAILURE;
    }
    UtilSemGive (TRIE_SEM_ID);
    TrieDbSemId = gaTrieDbInstance[u4Instance].SemId;
    TrieDb2TakeSem (u4Instance, TrieDbSemId);

    pLookupEntryOverlap =
        ((tRadixNodeHead *) pInputParams->pRoot)->AppFns->pLookupEntryOverlap;
    u2KeySize = ((tRadixNodeHead *) pInputParams->pRoot)->u2KeySize;
    pLeaf = TrieDbDoTraverseLessMoreSpecific (pInputParams);
    if (NULL != pLeaf)
    {
        /* not an exact match... a first match return it */
        i4Ret = pLookupEntryOverlap (pInputParams, pOutputParams,
                                     pLeaf->pAppSpecPtr, u2KeySize, pLeaf->Key);
        *ppNode = pLeaf;
        TrieDb2GiveSem (u4Instance, TrieDbSemId);
        return i4Ret;
    }

    /* No matching entry present. */
    *ppNode = NULL;
    TrieDb2GiveSem (u4Instance, TrieDbSemId);
    return LPM_FAILURE;
}

/************************************************************************/
/*  Function Name   : TrieDb2TakeSem                                      */
/*  Description     : This function is used to acquire a semaphore.     */
/*  Input(s)        : The semaphore Id.                                   */
/*  Output(s)       : None                                              */
/*  Returns         : None                                    */
/************************************************************************/

VOID
TrieDb2TakeSem (UINT4 u4Inst, tUtilSemId TrieDbSemId)
{
    if (gaTrieDbInstance[u4Inst].bSemPerInst == TRUE)
    {
        UtilSemTake (TrieDbSemId);
    }
}

/************************************************************************/
/*  Function Name   : TrieDb2GiveSem                                      */
/*  Description     : This function is used to release a semaphore.     */
/*  Input(s)        : The semaphore Id.                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                */
/************************************************************************/

VOID
TrieDb2GiveSem (UINT4 u4Inst, tUtilSemId TrieDbSemId)
{
    if (gaTrieDbInstance[u4Inst].bSemPerInst == TRUE)
    {
        UtilSemGive (TrieDbSemId);
    }
}

/************************************************************************/
/*  Function Name   : TrieDb2DelSem                                       */
/*  Description     : This function is used to delete a semaphore.      */
/*  Input(s)        : The semaphore Id.                           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                    */
/************************************************************************/

VOID
TrieDb2DelSem (UINT4 u4Inst, tUtilSemId TrieDbSemId)
{
    if (TrieDbSemId != NULL)
    {
        if (gaTrieDbInstance[u4Inst].bSemPerInst == TRUE)
        {
            UtilSemDel (TrieDbSemId);
        }
    }
}


 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbGetInstance ()                                    */
 /*                                                                         */
 /*  Description   This function selects the instance of TrieDb from the      */
 /*                array pointed to by tTrieDbFamily. It may create a new     */
 /*                instance and the associated semaphore. This function     */
 /*                returns the pointer to the tRadixNodeHead of the         */
 /*                associated instance. It returns NULL, if it fails due    */
 /*                to any reasons.                                          */
 /*                                                                         */
 /*  Call          This function is called to select the matching           */
 /*  condition     instance of TrieDb.                                        */
 /*                                                                         */
 /*  Input(s)      u4Type -        Type of the TrieDb instance.               */
 /*                u2KeySize -     Size of the key.                         */
 /*                                                                         */
 /*  Output(s)     pu4Instance - The TrieDb instance                          */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieDbCrt ().                                              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        tRadixNodeHead * - Pointer to the tRadixNodeHead         */
 /*                                   structure.                            */
 /*                                                                         */
 /*                                                                         */
 /***************************************************************************/

tRadixNodeHead     *
TrieDbGetInstance (UINT4 u4Type, UINT2 u2KeySize, UINT4 *pu4Instance)
{
    UINT4               u4Instance;

    *pu4Instance = 0;
    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if ((gaTrieDbInstance[u4Instance].u4Type == u4Type) &&
            (gaTrieDbInstance[u4Instance].u2KeySize == u2KeySize))
        {
            *pu4Instance = u4Instance;
            return (&(gaTrieDbInstance[u4Instance]));
        }
    }

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (gaTrieDbInstance[u4Instance].u2KeySize == 0)
        {
            break;
        }
    }

    if (u4Instance == MAX_NUM_TRIE)
    {
        TrieDbError (MAX_INSTANCE_OVERFLOW);
        return ((tRadixNodeHead *) NULL);
    }
    *pu4Instance = u4Instance;

    return (&(gaTrieDbInstance[u4Instance]));
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbGetFreeInstance ()                                   */
 /*                                                                         */
 /*  Description   This function selects the instance of TrieDb from the      */
 /*                array pointed to by tTrieDbFamily. It may create a new     */
 /*                instance and the associated semaphore. This function     */
 /*                returns the pointer to the tRadixNodeHead of the         */
 /*                associated instance. It returns NULL, if it fails due    */
 /*                to any reasons.                                          */
 /*                                                                         */
 /*  Call          This function is called to select the matching           */
 /*  condition     instance of TrieDb.                                        */
 /*                                                                         */
 /*  Input(s)      None                                                     */
 /*                                                                         */
 /*  Output(s)     pu4Instance - The TrieDb instance                          */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieDbCrtInstance ().                                      */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        tRadixNodeHead * - Pointer to the tRadixNodeHead         */
 /*                                   structure.                            */
 /*                                                                         */
 /*                                                                         */
 /***************************************************************************/

tRadixNodeHead     *
TrieDbGetFreeInstance (UINT4 *pu4Instance)
{
    UINT4               u4Instance;

    *pu4Instance = 0;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (gaTrieDbInstance[u4Instance].u2KeySize == 0)
        {
            break;
        }
    }

    if (u4Instance == MAX_NUM_TRIE)
    {
        TrieDbError (MAX_INSTANCE_OVERFLOW);
        return ((tRadixNodeHead *) NULL);
    }
    *pu4Instance = u4Instance;

    return (&(gaTrieDbInstance[u4Instance]));
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      INT4 TrieDbDoTraverse ()                                     */
 /*                                                                         */
 /*  Description   This function traverse an instance of TrieDb pointed to    */
 /*                by the parameter pRoot. When it reaches the leaf node    */
 /*          or NULL, it returns LPM_SUCCESS or LPM_FAILURE respectively. */
 /*          In case of LPM_FAILURE, it also returns the pointer to the    */
 /*           parent node. In case of LPM_SUCCESS, it returns the pointer  */
 /*                to the leaf node.                                        */
 /*                                                                         */
 /*  Call          When the functions want to search for the key.           */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize      Size of the key.                          */
 /*                pRoot -        Pointer to the tRadixNode structure.      */
 /*                pKey -         Pointer to the key.                       */
 /*                                                                         */
 /*  Output(s)     pNode -        Pointer to the  chosen node.              */
 /*                                                                         */
 /*  Access   TrieDbAdd (), TrieRemove (),TrieLookup ()                       */
 /*  privileges    TrieDbSearch ().                                           */
 /*                                                                         */
 /*  Return        LPM_SUCCESS - If the leaf node is found.                */
 /*                LPM_FAILURE - Otherwise.                                */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieDbDoTraverse (UINT2 u2KeySize, tRadixNode * pRoot, tKey Key,
                tLeafNode ** ppNode)
{
    UINT1               u1ByteVal;
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pParent = NULL;
    pCurrNode = pParent = pRoot;
    while ((pCurrNode != NULL) && (pCurrNode->u1NodeType == RADIX_NODE))

    {
        pParent = pCurrNode;
        u1ByteVal = (UINT1) GETBYTE (Key, pCurrNode->u1ByteToTest, u2KeySize);
        pCurrNode = (BIT_TEST (u1ByteVal, pCurrNode->u1BitToTest)) ?
            (tRadixNode *) pCurrNode->pRight : (tRadixNode *) pCurrNode->pLeft;
    }

    /* fails in finding the nearest leaf node */
    if (pCurrNode == NULL)

    {
        *((tRadixNode **) ppNode) = pParent;
        return (LPM_FAILURE);
    }
    *ppNode = (tLeafNode *) pCurrNode;
    return (LPM_SUCCESS);
}

/****************************************************************************/
/*                                                                         */
/*  Function      INT4 TrieDbInsertLeaf (                                       */
/*                UINT2   u2KeySize,                                       */
/*                tInputParams     *pInputParams,                          */
/*                tTrieDbApp         *pAppSpecInfo,                          */
/*                tRadixNode       *pParentNode)                           */
/*                                                                         */
/*  Description   This function creates the leaf node. It copies from      */
/*                tInputParams to relevant fields of the leaf node. It     */
/*                also assigns the parent pointer of the leaf node to      */
/*                the pParentNode.                                         */
/*                                                                         */
/*  Call          When a leaf node is to be added.                         */
/*  condition                                                              */
/*                                                                         */
/*  Input(s)      u2KeySize       Size of the key.                         */
/*                pInputParams -  Pointer to the tInputParams structure.   */
/*                pAppSpecInfo -  Pointer to the application specific      */
/*                                information.                             */
/*                pParentNode -   Pointer to the parent node.              */
/*                                                                         */
/*  Output(s)     None.                                                    */
/*                                                                         */
/*  Access        TrieDbAdd (), TrieInsertRadix ()                              */
/*  privileges                                                             */
/*                                                                         */
/*  Return        LPM_SUCCESS - If successful creation of the leaf node.  */
/*                LPM_FAILURE - Otherwise.                                */
/*                                                                         */
/***************************************************************************/
INT4
TrieDbInsertLeaf (UINT2 u2KeySize, tInputParams * pInputParams,
                VOID *pAppSpecInfo, tRadixNode * pParentNode,
                VOID *pOutputParams, tLeafNode ** ppLeafNode)
{
    UINT1               u1ByteVal;
    tLeafNode          *pLeafNode = NULL;
    UINT4               u4Instance;

    INT4                (*pAddAppSpecInfo) (tInputParams * pInputParams1,
                                            VOID *pOutputParams1,
                                            VOID **ppAppPtr,
                                            VOID *pAppSpecInfo1);

    pAddAppSpecInfo = pInputParams->pRoot->AppFns->pAddAppSpecInfo;

    if (u2KeySize * 8 < pInputParams->u1PrefixLen)
        return LPM_FAILURE;

    for (u4Instance = 0; u4Instance < MAX_NUM_TRIE; u4Instance++)
    {
        if (&(gaTrieDbInstance[u4Instance]) ==
            (tRadixNodeHead *) pInputParams->pRoot)
        {
            break;
        }
    }
    if (u4Instance == MAX_NUM_TRIE)
    {
        /* There's something wrong */
        return (LPM_FAILURE);
    }

    pLeafNode =
        (tLeafNode *) TrieDbAllocateateNode (u2KeySize, LEAF_NODE, u4Instance);
    if (pLeafNode == NULL)
    {
        return (LPM_FAILURE);
    }

    {
        INT1                i1Ctr = (INT1) (pInputParams->u1PrefixLen / 8);
        INT4                i4Idx = 0;

        if (u2KeySize > U4_SIZE)
        {
            adap_memset(pLeafNode->Mask.pKey, 0, u2KeySize);
            while (i1Ctr--)
            {
                pLeafNode->Mask.pKey[i4Idx++] = 0xff;
            }
            i1Ctr = (INT1) (pInputParams->u1PrefixLen % 8);
            while (i1Ctr--)
            {
                pLeafNode->Mask.pKey[i4Idx] =
                    (pLeafNode->Mask.pKey[i4Idx] | (UINT1) (1 << (7 - i1Ctr)));
            }
        }
        else
        {
            pLeafNode->Mask.u4Key = 0;
            while (i1Ctr--)
            {
                pLeafNode->Mask.u4Key =
                    (UINT4) (pLeafNode->Mask.
                             u4Key | (UINT4) (1 << (31 - i1Ctr)));
            }
        }
    }

    KEYCOPY (pLeafNode->Key, pInputParams->Key, u2KeySize);

    /* call app specific add function to add this
       pLeafNode->pAppSpecInfo = pAppSpecInfo; */

    pLeafNode->pAppSpecPtr = NULL;    /* Initialising *** */
    if (LPM_FAILURE == (pAddAppSpecInfo) (pInputParams, pOutputParams,
                                           &pLeafNode->pAppSpecPtr,
                                           pAppSpecInfo))
    {
        TrieDbReleaseNode (LEAF_NODE, u4Instance, (UINT1 *) pLeafNode);
        return LPM_FAILURE;
    }

    /* pointer updation */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))
    {
        pParentNode->pRight = (void *) pLeafNode;
    }
    else
    {
        pParentNode->pLeft = (void *) pLeafNode;
    }
    pLeafNode->pParent = pParentNode;

    *ppLeafNode = pLeafNode;

    return (LPM_SUCCESS);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbInsertRadix ()                                          */
 /*                                                                         */
 /*  Description   This function creates the radix node. It modifies the    */
 /*                pointers to reflect this change. It also creates a       */
 /*                leaf node by calling another function. If it fails due   */
 /*                to resource problems, it returns LPM_FAILURE.           */
 /*                                                                         */
 /*  Call          When a radix node needs to be added.                     */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize       Size of the key.                         */
 /*                u1BitToTest -   Bit mask of the selected byte.           */
 /*                u1ByteToTest -  Selected Byte.                           */
 /*                pInputParams -  Pointer to the tInputParams structure.   */
 /*                pAppSpecInfo -  Pointer to the application specific      */
 /*                                information.                             */
 /*                pParentNode -   Pointer to the parent node.              */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*                                                                         */
 /*  Access        TrieDbAdd ().                                              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        LPM_SUCCESS - If successful creation of the radix node. */
 /*                LPM_FAILURE - Otherwise.                                */
 /*                                                                         */
 /***************************************************************************/
INT4
TrieDbInsertRadix (UINT2 u2KeySize, UINT1 u1BitToTest, UINT1 u1ByteToTest,
                 tInputParams * pInputParams, VOID *pAppSpecInfo,
                 tRadixNode * pParentNode, VOID *pOutputParams)
{
    tRadixNode         *pRadixNode = NULL;
    void               *pTmpNode = NULL;
    tLeafNode          *pNewLeaf = NULL;
    tLeafNode          *pPrevLeaf = NULL;
    UINT1               u1ByteVal;
    INT4                i4Cmp;
    UINT4               u4Instance;

    if (TrieDbGetTrieInstance (pInputParams->pRoot, &u4Instance) == LPM_FAILURE)
    {
        return LPM_FAILURE;
    }

    pRadixNode =
        (tRadixNode *) TrieDbAllocateateNode (u2KeySize, RADIX_NODE, u4Instance);
    if (pRadixNode == NULL)
    {
        return LPM_FAILURE;
    }
    pRadixNode->u1ByteToTest = u1ByteToTest;
    pRadixNode->u1BitToTest = u1BitToTest;

    /* add the leaf node */
    if ((TrieDbInsertLeaf (u2KeySize, pInputParams, pAppSpecInfo, pRadixNode,
                         pOutputParams, &pNewLeaf)) == LPM_FAILURE)
    {
        if (u2KeySize > U4_SIZE)
        {
            TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pRadixNode->Mask.pKey);
        }

        TrieDbReleaseNode (RADIX_NODE, u4Instance, (UINT1 *) pRadixNode);
        return (LPM_FAILURE);
    }

    u1ByteVal =
        GETBYTE (pInputParams->Key, pParentNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pParentNode->u1BitToTest))
    {
        pTmpNode = pParentNode->pRight;
        pParentNode->pRight = pRadixNode;
    }
    else
    {
        pTmpNode = pParentNode->pLeft;
        pParentNode->pLeft = pRadixNode;
    }
    pRadixNode->pParent = pParentNode;

    /* update the pointers in the child of new radix node */
    u1ByteVal =
        GETBYTE (pInputParams->Key, pRadixNode->u1ByteToTest, u2KeySize);
    if (BIT_TEST (u1ByteVal, pRadixNode->u1BitToTest))
    {
        pRadixNode->pLeft = pTmpNode;
    }
    else
    {
        pRadixNode->pRight = pTmpNode;
    }

    i4Cmp = KEYCMP (((tRadixNode *) pRadixNode->pLeft)->Mask,
                    ((tRadixNode *) pRadixNode->pRight)->Mask, u2KeySize);

    if (i4Cmp < 0)
    {
        KEYCOPY (pRadixNode->Mask, ((tRadixNode *) pRadixNode->pLeft)->Mask,
                 u2KeySize);
    }
    else
    {
        KEYCOPY (pRadixNode->Mask, ((tRadixNode *) pRadixNode->pRight)->Mask,
                 u2KeySize);
    }

    ((tRadixNode *) pTmpNode)->pParent = pRadixNode;

    /* update the mask 
     * Here TrieDbSetMask () is supposed to be called with u2KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieDbSetMask (u2KeySize, pRadixNode->pParent);

    if (NULL == (pPrevLeaf = TrieDbGetPrevLeafNode (pNewLeaf)))
    {
        /* means we are the left most node of the trie */
        DLIST_INSERT_HEAD (((tRadixNodeHead *) pInputParams->pRoot), NodeHead,
                           pNewLeaf, NodeLink);
    }
    else
    {
        DLIST_INSERT (((tRadixNodeHead *) pInputParams->pRoot), NodeHead,
                      pNewLeaf, NodeLink, pPrevLeaf);
    }

    return (LPM_SUCCESS);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbDelRadixLeaf ()                                      */
 /*                                                                         */
 /*  Description   This function deletes the leaf node and the associated   */
 /*                radix node if any, pointed to by the parameters. it      */
 /*                copies the deleted leaf node information in the          */
 /*                relevant fields of tOutputParams structure.              */
 /*                                                                         */
 /*  Call          When the only residing application in the leaf node      */
 /*  condition     is also deleted.                                         */
 /*                                                                         */
 /*  Input(s)      u2KeySize -      Size of the key.                        */
 /*                pLeafNode -      Pointer to the tLeafNode to be          */
 /*                                 deleted.                                */
 /*                                                                         */
 /*  Output(s)     pOutputParams -  Pointer to the tOutputParams            */
 /*                                 structure.                              */
 /*                                                                         */
 /*  Access        TrieDbRemove (), TrieDel ().                               */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/
VOID
TrieDbDelRadixLeaf (tRadixNodeHead * pRoot, tLeafNode * pLeafNode,
                  UINT4 u4Instance)
{
    tRadixNode         *pTmpParent = NULL;
    tRadixNode         *pPredNode = NULL;
    tRadixNode         *pOtherTree = NULL;
    tLeafNode          *pPrevLeaf = NULL;
    UINT2               u2KeySize = pRoot->u2KeySize;

    if (NULL == (pPrevLeaf = TrieDbGetPrevLeafNode (pLeafNode)))
    {
        /* means we are the left most node of the trie */
        DLIST_REMOVE_HEAD (pRoot, NodeHead, NodeLink);
    }
    else
    {
        DLIST_REMOVE (pRoot, NodeHead, pLeafNode, NodeLink, pPrevLeaf);
    }

    pTmpParent = pLeafNode->pParent;
    if (pTmpParent->pRight == pLeafNode)
    {
        pTmpParent->pRight = NULL;
        pOtherTree = pTmpParent->pLeft;
    }
    else
    {
        pTmpParent->pLeft = NULL;
        pOtherTree = pTmpParent->pRight;
    }

    /* free the memory of the key, if allocated */
    if (u2KeySize > U4_SIZE)
    {
        TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                         (UINT1 *) pLeafNode->Key.pKey);
        TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                         (UINT1 *) pLeafNode->Mask.pKey);
    }

    /* free the leaf node */
    TrieDbReleaseNode (LEAF_NODE, u4Instance, (UINT1 *) pLeafNode);

    /* free the parent node except root */
    pPredNode = pTmpParent->pParent;

    if (pPredNode != NULL)
    {
        /* update the pointers */
        if (pPredNode->pRight == pTmpParent)
        {
            pPredNode->pRight = pOtherTree;
        }
        else
        {
            pPredNode->pLeft = pOtherTree;
        }
        pOtherTree->pParent = pPredNode;

        /* free the parent radix node */

        if (u2KeySize > U4_SIZE)
        {
            TrieDbReleaseNode (KEY_POOL_ID, u4Instance,
                             (UINT1 *) pTmpParent->Mask.pKey);
        }

        TrieDbReleaseNode (RADIX_NODE, u4Instance, (UINT1 *) pTmpParent);
        pTmpParent = pPredNode;
    }

    /* update the mask 
     * Here TrieDbSetMask () is supposed to be called with u2KeySize
     * but this is a peculiar situation. 8 byte key and 4 byte mask
     */
    TrieDbSetMask (u2KeySize, (void *) pTmpParent);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbSetMask ()                                        */
 /*                                                                         */
 /*  Description   This function assigns mask irrespective of the node      */
 /*                type. In every assignment of the mask, it selects the    */
 /*                mask with the least number of leading ones.              */
 /*                                                                         */
 /*  Call          When mask needs to be reassigned.                        */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -  Size of the key.                            */
 /*                pNode -      Pointer to the node whose mask needs to     */
 /*                             be reassigned.                              */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        TrieDbAdd (), TrieDelAppAndUpdateLeaf (),                  */
 /*  privileges    TrieDbDelRadixLeaf ().                                     */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

void
TrieDbSetMask (UINT2 u2KeySize, void *pNode)
{
    INT4                i4Cmp;
    INT4                i4Eql;
    tKey                PrvMask;
    tRadixNode         *pRadix = NULL;

    pRadix = (tRadixNode *) pNode;
    while (pRadix != NULL)
    {
        PrvMask = pRadix->Mask;
        if ((pRadix->pRight != NULL) && (pRadix->pLeft != NULL))
        {
            i4Cmp = KEYCMP (((tRadixNode *) pRadix->pRight)->Mask,
                            ((tRadixNode *) pRadix->pLeft)->Mask, u2KeySize);

            if (i4Cmp < 0)
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pRight)->Mask,
                         u2KeySize);
            }
            else
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pLeft)->Mask,
                         u2KeySize);
            }
        }
        else
        {
            if ((pRadix->pRight == NULL) && (pRadix->pLeft == NULL))
            {
                break;
            }

            if (pRadix->pRight == NULL)
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pLeft)->Mask,
                         u2KeySize);
            }
            else
            {
                KEYCOPY (pRadix->Mask, ((tRadixNode *) pRadix->pRight)->Mask,
                         u2KeySize);
            }
        }
        i4Eql = KEYEQUAL (pRadix->Mask, PrvMask, u2KeySize);
        if (i4Eql == 0)
        {
            break;
        }
        pRadix = pRadix->pParent;
    }
}

INT4                (*pTrieDbBestMatch) (UINT2 u2KeySize,
                                       tInputParams * pInputParams,
                                       VOID *pOutputParams, VOID *pAppPtr,
                                       tKey Key);

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbGetBestMatch ()                                     */
 /*                                                                         */
 /*  Description   This functions starts to find the best match with the    */
 /*                given node and in the worst case, goes to the root. If   */
 /*                this operation is successful, it returns LPM_SUCCESS. In */
 /*                case of successful return, it also copies relevant       */
 /*                fields in the tOutputParams structure.                   */
 /*                                                                         */
 /*  Call          When best match needs to be found.                       */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -       Size of the key.                       */
 /*                pInputParams -    Pointer to the tInputParams            */
 /*                                  structure.                             */
 /*                pNode -           Pointer to the  node from where        */
 /*                                  search  for the best match begins.     */
 /*                                                                         */
 /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
 /*                                  structure.                             */
 /*                                                                         */
 /*  Access        TrieDbLookup ().                                           */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        LPM_SUCCESS  - If it is able to  find  the best match.  */
 /*                LPM_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieDbGetBestMatch (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode,
                  VOID *pOutputParams)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tKey                LeafKey;
    UINT1              *pTmpKey = NULL;
    UINT1              *pau1Mask = NULL;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;

    UINT4               u4TmpNumBytes;
    UINT1               au1Key[MAX_KEY_SIZE];
    UINT1               au1PrvKey[MAX_KEY_SIZE];

    pTrieDbBestMatch = pInputParams->pRoot->AppFns->pBestMatch;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
        LeafKey.pKey = &(au1PrvKey[0]);

        /* a temporary solution once mask size is more than 
         *  4 byte shouls not be problem
         */
        pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
        /* here assumption is IP address is of 4 byte (U4_SIZE) 
         * this logic can be made more efficient by diretly ANDING with
         * 4 byte mask only. Here it is byte by byte.
         */
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)

        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
            LeafKey.pKey[u4TmpNumBytes] =
                (pLeaf->Key).pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        if (memcmp (LeafKey.pKey, CurrKey.pKey, u2KeySize) == 0)

        {
            if (LPM_SUCCESS == pTrieDbBestMatch (u2KeySize, pInputParams,
                                                pOutputParams,
                                                pLeaf->pAppSpecPtr, pLeaf->Key))
                return LPM_SUCCESS;
        }

        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
    }

    PrvKey.pKey = &(au1PrvKey[0]);
    pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

    au1Key[0] = (UINT1) (~(au1PrvKey[0] & pau1Mask[0]));
    while (pCurrNode != NULL)
    {
        pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

        /* change the arrays to reflect correct value */
        pTmpKey = CurrKey.pKey;
        CurrKey.pKey = PrvKey.pKey;
        PrvKey.pKey = pTmpKey;
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        /* ideally upper limit should be 2*U4_SIZE, but here both are same
         * (u2KeySize and 2*U4_SIZE)
         */
        for (u4TmpNumBytes = u2KeySize; u4TmpNumBytes < u2KeySize;
             u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] = pau1Mask[u4TmpNumBytes - u2KeySize];
        }

        /* to prevent reduntant lookup */
        if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) != 0)
        {
            if (TrieDbDoTraverse (u2KeySize, pCurrNode, CurrKey,
                                &pLeaf) == LPM_SUCCESS)
            {
                pau1Mask = (UINT1 *) pLeaf->Mask.pKey;

                for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize;
                     u4TmpNumBytes++)
                {
                    PrvKey.pKey[u4TmpNumBytes] =
                        pLeaf->Key.
                        pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                    CurrKey.pKey[u4TmpNumBytes] =
                        InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                }

                /* if keys match */
                if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) == 0)
                {
                    if (LPM_SUCCESS == pTrieDbBestMatch (u2KeySize, pInputParams,
                                                        pOutputParams,
                                                        pLeaf->pAppSpecPtr,
                                                        pLeaf->Key))
                        return LPM_SUCCESS;
                }
            }
            else
            {
                return (LPM_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (LPM_FAILURE);
}

INT4                (*pTrieDbBestMatch4) (UINT2 u2KeySize,
                                        tInputParams * pInputParams,
                                        VOID *pOutputParams, VOID *pAppPtr,
                                        tKey Key);

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbGetBestMatch4 ()                                    */
 /*                                                                         */
 /*  Description   This functions starts to find the best match with the    */
 /*                given node and in the worst case, goes to the root. If   */
 /*                this operation is successful, it returns LPM_SUCCESS. In */
 /*                case of successful return, it also copies relevant       */
 /*                fields in the tOutputParams structure. This function     */
 /*                handles only 4 byte key.                                 */
 /*                                                                         */
 /*  Call          When best match needs to be found.                       */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -       Size of the key.                       */
 /*                pInputParams -    Pointer to the tInputParams            */
 /*                                  structure.                             */
 /*                pNode -           Pointer to the  node from where        */
 /*                                  search  for the best match begins.     */
 /*                                                                         */
 /*  Output(s)     pOutputParams -   Pointer to the  tOutputParams          */
 /*                                  structure.                             */
 /*                                                                         */
 /*  Access        TrieDbLookup ().                                           */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        LPM_SUCCESS  - If it is able to  find  the best match.  */
 /*                LPM_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

INT4
TrieDbGetBestMatch4 (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode,
                   VOID *pOutputParams)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;

    pTrieDbBestMatch4 = pInputParams->pRoot->AppFns->pBestMatch;

    InKey = pInputParams->Key;
    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.u4Key = InKey.u4Key & (pLeaf->Mask).u4Key;
        if (CurrKey.u4Key == ((pLeaf->Key).u4Key & (pLeaf->Mask).u4Key))

        {
            if (LPM_SUCCESS == pTrieDbBestMatch4 (u2KeySize, pInputParams,
                                                 pOutputParams,
                                                 pLeaf->pAppSpecPtr,
                                                 pLeaf->Key))
                return LPM_SUCCESS;
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
    }

    CurrKey.u4Key = ~(InKey.u4Key & (pCurrNode->Mask).u4Key);
    while (pCurrNode != NULL)
    {
        /* change the arrays to reflect correct value */
        PrvKey.u4Key = CurrKey.u4Key;
        CurrKey.u4Key = InKey.u4Key & (pCurrNode->Mask).u4Key;
        if (CurrKey.u4Key != PrvKey.u4Key)
        {
            if (TrieDbDoTraverse (u2KeySize, pCurrNode, CurrKey, &(pLeaf))
                == LPM_SUCCESS)
            {
                if (CurrKey.u4Key == (pLeaf->Key.u4Key & pLeaf->Mask.u4Key))
                {
                    if (LPM_SUCCESS == pTrieDbBestMatch4 (u2KeySize,
                                                         pInputParams,
                                                         pOutputParams,
                                                         pLeaf->pAppSpecPtr,
                                                         pLeaf->Key))
                    {
                        return LPM_SUCCESS;
                    }
                }
            }
            else
            {
                return (LPM_FAILURE);
            }
        }
        pCurrNode = pCurrNode->pParent;
    }
    return (LPM_FAILURE);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbAllocateateNode ()                                   */
 /*                                                                         */
 /*  Description   This function allocates the memory for the node as       */
 /*                requested by the u1NodeType parameter. It also           */
 /*                initialises the fields of the structure. If it fails     */
 /*                in allocating memory, it returns LPM_FAILURE. In case of */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create either the leaf node   */
 /*  condition     or the radix node.                                       */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u1NodeType -    Indicates the type of the node.          */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        TrieDbInsertLeaf (), BgpTrieInsertRadix ().                */
 /*  privileges    TrieDbGetInstance ().                                      */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                LPM_FAILURE -  Otherwise.                               */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieDbAllocateateNode (UINT2 u2KeySize, UINT1 u1NodeType, UINT4 u4Instance)
{
    VOID               *pTrieDbNode = NULL;

    if (u1NodeType == RADIX_NODE)
    {
        /* Allocating radix node memory from 
         * Radix memory pool which is created for 
         * this particular instance */
        pTrieDbNode = TrieDbAllocateRadixNode (u2KeySize, u4Instance);
        return (pTrieDbNode);
    }
    else
    {
        /* Allocating leaf node memory from 
         * Leaf memory pool which is created for 
         * this particular instance */
        pTrieDbNode = TrieDbAllocateLeafNode (u2KeySize, u4Instance);
        return (pTrieDbNode);
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbAllocateRadixNode ()                                 */
 /*                                                                         */
 /*  Description   This function allocates the memory for radix node and    */
 /*                keyId and initialises the fields of the structure. If it */
 /*                fails in allocating memory, it returns NULL. In case of  */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create the radix node         */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u4Instance -    Identifier of TrieDb instance.             */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                NULL -  Otherwise.                                       */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieDbAllocateRadixNode (UINT2 u2KeySize, UINT4 u4Instance)
{

    tRadixNode         *pRadix = NULL;

    pRadix = (tRadixNode *)adap_malloc(sizeof(tRadixNode));
    if (pRadix == NULL)
    {
        TrieDbError (RADIX_ALLOC_FAIL);
        return ((void *) NULL);
    }
    if (u2KeySize > U4_SIZE)
    {
        if ((pRadix->Mask.pKey = (UINT1 *)adap_malloc(MAX_KEY_SIZE)) == 0)
        {
            free((UINT1 *) pRadix);
            TrieDbError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }
    }
    pRadix->u1NodeType = RADIX_NODE;
    return ((void *) pRadix);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbAllocateLeafNode ()                                  */
 /*                                                                         */
 /*  Description   This function allocates the memory for leaf node and     */
 /*                keyId and initialises the fields of the structure. If it */
 /*                fails in allocating memory, it returns NULL. In case of  */
 /*                the successful return, it passes a pointer to the        */
 /*                structure created.                                       */
 /*                                                                         */
 /*  Call          When the function wants to create the radix node         */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u2KeySize -     Size of the key.                         */
 /*                u4Instance -    Identifier of TrieDb instance.             */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Return        Pointer to the created node - If it is successful.       */
 /*                NULL -  Otherwise.                                       */
 /*                                                                         */
 /***************************************************************************/

VOID               *
TrieDbAllocateLeafNode (UINT2 u2KeySize, UINT4 u4Instance)
{
    tLeafNode          *pLeaf = NULL;

    pLeaf =
        (tLeafNode *)adap_malloc(sizeof(tLeafNode));
    if (pLeaf == NULL)
    {
        TrieDbError (LEAF_ALLOC_FAIL);
        return ((void *) NULL);
    }
    if (u2KeySize > U4_SIZE)
    {
        if ((pLeaf->Key.pKey = (UINT1 *)adap_malloc(MAX_KEY_SIZE)) == 0)
        {
            free ((UINT1 *) pLeaf);
            TrieDbError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }

        if ((pLeaf->Mask.pKey = (UINT1 *)adap_malloc(MAX_KEY_SIZE)) == 0)
        {
            free ((UINT1 *) pLeaf->Key.pKey);
            free ((UINT1 *) pLeaf);
            TrieDbError (KEY_ALLOC_FAIL);
            return ((void *) NULL);
        }
    }

    pLeaf->u1NodeType = LEAF_NODE;
    return ((void *) pLeaf);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      TrieDbReleaseNode ()                                       */
 /*                                                                         */
 /*  Description   This function releases the memory of the incoming trie   */
 /*                node to corresponding mem pool as per u1NodeType.        */
 /*                                                                         */
 /*  Input(s)      u1NodeType - Indicates the type of the node.             */
 /*                u4Instance - Identifier of trie instance                 */
 /*                pNode      - Reference to the trie node which            */
 /*                              is to be released                          */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

VOID
TrieDbReleaseNode (UINT1 u1NodeType, UINT4 u4Instance, UINT1 *pNode)
{
    if (u1NodeType == RADIX_NODE)
    {
        /* Releasing radix node memory to  Radix memory pool
         * which is created for this particular instance */
        free (pNode);
        return;
    }
    else if (u1NodeType == LEAF_NODE)
    {
        /* Releasing leaf node memory to  leaf memory pool
         * which is created for this particular instance */
        free (pNode);
        return;
    }
    else
    {
        /* Releasing memory of key to  key memory pool
         * which is created for this particular instance */
        free (pNode);
        return;
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function      void TrieDbError ()                                  */
 /*                                                                         */
 /*  Description   This function reports the error encountered by the       */
 /*                different functions. As per the u1NumError field, it     */
 /*                sends error message to the standard error device.        */
 /*                                                                         */
 /*  Call          When calling function encounters errors.                 */
 /*  condition                                                              */
 /*                                                                         */
 /*  Input(s)      u1NumError -     Error number                            */
 /*                                                                         */
 /*  Output(s)     None.                                                    */
 /*                                                                         */
 /*  Access        Higher level functions of the TrieDb library.              */
 /*  privileges                                                             */
 /*                                                                         */
 /*  Return        None.                                                    */
 /*                                                                         */
 /***************************************************************************/

void
TrieDbError (UINT1 u1NumError)
{
    UINT1               u1StrLength;
    CHR1                buf[100];
    const CHR1         *aErr[] =
        { "Memory allocation failure for the leaf node.",
        "Memory allocation failure for the radix node.",
        "Memory allocation failure for the key.",
        "Key size can not be accommodated.",
        "Application id can not be accommodated.",
        "Number of instances are more than the maximum value.",
        "Semaphore creation failure.",
        "Application id is already taken before.",
        "Memory allocation failure."
    };
    u1StrLength = (UINT1) STRLEN (aErr[u1NumError]);
    if (u1StrLength > 99)
    {
        u1StrLength = 99;
    }
    sprintf (buf, "TRIE - %s\n", aErr[u1NumError]);
    buf[u1StrLength] = 0;
}

tLeafNode          *
TrieDbGetPrevLeafNode (tLeafNode * pLeaf)
{
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pParent = NULL;

    /* we go up the trie once and come down ... */
    pCurrNode = (tRadixNode *) pLeaf;
    pParent = pLeaf->pParent;

    /* UP on the left */
    while (pParent != NULL)
    {
        if ((pParent->pLeft == pCurrNode) || (NULL == pParent->pLeft))
        {
            pCurrNode = pParent;
            pParent = pParent->pParent;
        }
        else
            break;
    }

    if (NULL == pParent)
        return NULL;            /* the left most node was given !! */

    /* switch to the leftie */
    pCurrNode = pParent->pLeft;

    /* and come DOWN on the right */
    while (RADIX_NODE == pCurrNode->u1NodeType)
    {
        pCurrNode =
            (NULL == pCurrNode->pRight) ? pCurrNode->pLeft : pCurrNode->pRight;
    }
    return (tLeafNode *) pCurrNode;
}

#define OTHER_NODE(x)                      \
           ((((tRadixNode*)((tLeafNode*)(x))->pParent)->pLeft == (void*)(x))?    \
            ((tRadixNode*)((tLeafNode*)(x))->pParent)->pRight :                \
            ((tRadixNode*)((tLeafNode*)(x))->pParent)->pLeft                    \
           )
#define TRIE_OVERLAP_LESS      1
#define TRIE_OVERLAP_MORE      2

INT4
TrieDbOverlapMatch (tKey Key, UINT2 u2KeySize, tLeafNode * pNode,
                  UINT1 *pu1OvlType)
{
    UINT2               u2InPrefixLen, u2NodePrefixLen, u2Len;

    u2InPrefixLen = TrieDbPrefixLen (Key, u2KeySize);
    u2NodePrefixLen = TrieDbPrefixLen (pNode->Key, u2KeySize);

    if (u2NodePrefixLen > u2InPrefixLen)
    {
        *pu1OvlType = TRIE_OVERLAP_MORE;
        u2Len = u2InPrefixLen;
    }
    else if (u2NodePrefixLen < u2InPrefixLen)
    {
        *pu1OvlType = TRIE_OVERLAP_LESS;
        u2Len = u2NodePrefixLen;
    }
    else
    {
        /* exact match not needed !! */
        return 0;
    }

    if (TrieDbKeyMatches (Key, pNode->Key, u2Len, u2KeySize))
    {
        return 1;
    }
    return 0;
}

INT4
TrieDbDefaultRoute (tKey Key, UINT2 u2KeySize)
{

    if (u2KeySize <= sizeof (UINT4))
    {
        if (Key.u4Key == 0)
            return 1;
    }
    else
    {
        while (u2KeySize--)
        {
            if (Key.pKey[u2KeySize])
            {
                return 0;
            }
            return 1;
        }
    }
    return 0;
}

INT4
TrieDbKeyMatches (tKey Key1, tKey Key2, UINT2 u2PrefixLen, UINT2 u2KeySize)
{
    UINT4               u4Mask = (UINT4) ~0;
    UINT1               u1Mask = (UINT1) ~0;
    UINT2               u2PrefixBytes;

    if (u2KeySize <= sizeof (UINT4))
    {
        u2PrefixLen = (UINT2) (u2PrefixLen ? (32 - u2PrefixLen) : 32);
        while (u2PrefixLen--)
        {
            u4Mask <<= 1;
        }

        if ((Key1.u4Key & u4Mask) == (Key2.u4Key & u4Mask))
        {
            return 1;
        }
    }
    else
    {
        u2PrefixBytes = (UINT2) ((u2PrefixLen) >> 3);    /* bytes */
        u2PrefixLen = u2PrefixLen & (0x7);    /* bits */

        u2PrefixLen = (UINT2) (u2PrefixLen ? (8 - u2PrefixLen) : 8);
        while (u2PrefixLen--)
            u1Mask = (UINT1) (u1Mask << 1);

        if ((Key1.pKey[u2PrefixBytes] & u1Mask) == (Key2.pKey[u2PrefixBytes] &
                                                    u1Mask))
        {
            while (u2PrefixBytes--)
            {
                if (Key1.pKey[u2PrefixBytes] != Key2.pKey[u2PrefixBytes])
                    return 0;
            }
            return 1;
        }
    }

    return 0;
}

const UINT1         TrieDbBitsSetTable256[] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

UINT2
TrieDbPrefixLen (tKey Key, UINT2 u2KeySize)
{
    UINT2               u2Count = 0;
    UINT4               u2MaskOff;

    if (u2KeySize <= U4_SIZE)
    {
        u2Count = (UINT2) (u2Count +
                           TrieDbBitsSetTable256[(UINT1)
                                               (Key.
                                                u4Key & (UINT4) 0x000000ff)]);
        if (u2KeySize == U4_SIZE)
        {
            u2Count = (UINT2) (u2Count +
                               TrieDbBitsSetTable256[(UINT1)
                                                   ((Key.
                                                     u4Key & (UINT4) 0x0000ff00)
                                                    >> 8)]);
        }
    }
    else
    {
        u2MaskOff = u2KeySize >> 1;
        u2KeySize = 0;
        while ((u2KeySize < u2MaskOff) && Key.pKey[u2KeySize + u2MaskOff])
        {
            u2Count =
                (UINT2) (u2Count +
                         TrieDbBitsSetTable256[Key.pKey[u2MaskOff + u2KeySize]]);
            u2KeySize++;
        }
    }

    return u2Count;
}

#define SET_LASTBESTMATCH() do {\
           if (TrieDbOverlapMatch (pInputParams->Key, u2KeySize, pLeaf, &u1OvlType))\
               pLastBestMatch = pLeaf;                    \
           else \
               return NULL;\
    } while (0)

tLeafNode          *
TrieDbFindFirstMatch (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tKey                LeafKey;
    UINT1              *pTmpKey = NULL;
    UINT1              *pau1Mask = NULL;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;
    tLeafNode          *pLastBestMatch = NULL;
    UINT4               u4TmpNumBytes;
    UINT1               au1Key[MAX_KEY_SIZE];
    UINT1               au1PrvKey[MAX_KEY_SIZE];
    UINT1               u1OvlType;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)

    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
        LeafKey.pKey = &(au1PrvKey[0]);

        /* a temporary solution once mask size is more than 
         *  4 byte shouls not be problem
         */
        pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
        /* here assumption is IP address is of 4 byte (U4_SIZE) 
         * this logic can be made more efficient by diretly ANDING with
         * 4 byte mask only. Here it is byte by byte.
         */
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
            LeafKey.pKey[u4TmpNumBytes] =
                (pLeaf->Key).pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        if (memcmp (LeafKey.pKey, CurrKey.pKey, u2KeySize) == 0)
        {
            SET_LASTBESTMATCH ();
        }

        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
    {
        pCurrNode = (tRadixNode *) pNode;
        CurrKey.pKey = &(au1Key[0]);
    }

    PrvKey.pKey = &(au1PrvKey[0]);
    pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

    au1Key[0] = (UINT1) (~(au1PrvKey[0] & pau1Mask[0]));
    while (pCurrNode != NULL)
    {
        pau1Mask = (UINT1 *) pCurrNode->Mask.pKey;

        /* change the arrays to reflect correct value */
        pTmpKey = CurrKey.pKey;
        CurrKey.pKey = PrvKey.pKey;
        PrvKey.pKey = pTmpKey;
        for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize; u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] =
                InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
        }

        /* ideally upper limit should be 2*U4_SIZE, but here both are same
         * (u2KeySize and 2*U4_SIZE)
         */
        for (u4TmpNumBytes = u2KeySize; u4TmpNumBytes < u2KeySize;
             u4TmpNumBytes++)
        {
            CurrKey.pKey[u4TmpNumBytes] = pau1Mask[u4TmpNumBytes - u2KeySize];
        }

        /* to prevent reduntant lookup */
        if (memcmp (CurrKey.pKey, PrvKey.pKey, u2KeySize) != 0)
        {
            if (TrieDbDoTraverse (u2KeySize, pCurrNode, CurrKey, &pLeaf)
                == LPM_SUCCESS)
            {

                pau1Mask = (UINT1 *) pLeaf->Mask.pKey;
                for (u4TmpNumBytes = 0; u4TmpNumBytes < u2KeySize;
                     u4TmpNumBytes++)
                {
                    PrvKey.pKey[u4TmpNumBytes] =
                        pLeaf->Key.
                        pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                    CurrKey.pKey[u4TmpNumBytes] =
                        InKey.pKey[u4TmpNumBytes] & pau1Mask[u4TmpNumBytes];
                }
                SET_LASTBESTMATCH ();
            }
            else
                return pLastBestMatch;
        }

        pCurrNode = pCurrNode->pParent;
    }

    return pLastBestMatch;
}

tLeafNode          *
TrieDbFindFirstMatch4 (UINT2 u2KeySize, tInputParams * pInputParams, void *pNode)
{
    tKey                CurrKey;
    tKey                PrvKey;
    tKey                InKey;
    tRadixNode         *pCurrNode = NULL;
    tLeafNode          *pLeaf = NULL;
    tLeafNode          *pLastBestMatch = NULL;
    UINT1               u1OvlType;

    InKey = pInputParams->Key;

    if (((tLeafNode *) pNode)->u1NodeType == LEAF_NODE)
    {
        pLeaf = (tLeafNode *) pNode;
        CurrKey.u4Key = InKey.u4Key & (pLeaf->Mask).u4Key;
        if (CurrKey.u4Key == ((pLeaf->Key).u4Key & (pLeaf->Mask).u4Key))
        {
            SET_LASTBESTMATCH ();
        }
        pCurrNode = (tRadixNode *) pLeaf->pParent;
    }
    else
        pCurrNode = (tRadixNode *) pNode;

    CurrKey.u4Key = ~(InKey.u4Key & (pCurrNode->Mask).u4Key);
    while (pCurrNode != NULL)
    {
        /* change the arrays to reflect correct value */
        PrvKey.u4Key = CurrKey.u4Key;
        CurrKey.u4Key = InKey.u4Key & (pCurrNode->Mask).u4Key;

        if (CurrKey.u4Key != PrvKey.u4Key)
        {
            if (TrieDbDoTraverse (u2KeySize, pCurrNode, CurrKey, &pLeaf)
                == LPM_SUCCESS)
            {

                if (CurrKey.u4Key == (pLeaf->Key.u4Key & pLeaf->Mask.u4Key))
                    SET_LASTBESTMATCH ();
            }
            else
                return pLastBestMatch;
        }
        pCurrNode = pCurrNode->pParent;
    }

    return pLastBestMatch;
}

tLeafNode          *
TrieDbFirstMatch (tInputParams * pInputParams)
{
    UINT2               u2KeySize;
    INT4                i4Return;
    tRadixNode         *pRadix = NULL;
    tLeafNode          *pLeaf = NULL;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
        return NULL;            /* empty trie */

    i4Return = TrieDbDoTraverse (u2KeySize, pRadix, pInputParams->Key, &(pLeaf));
    if (i4Return == LPM_FAILURE)
    {
        return NULL;
    }

    /* else continue !! */
    if (u2KeySize > U4_SIZE)
        return TrieDbFindFirstMatch (u2KeySize, pInputParams, (VOID *) pLeaf);
    else
        return TrieDbFindFirstMatch4 (u2KeySize, pInputParams, (VOID *) pLeaf);
}

tRadixNode         *
TrieDbDoTraverseLessSpecific (tInputParams * pInputParams)
{
    UINT2               u2KeySize, u2InPrefixLen, u2CurPrefixLen;
    UINT1               u1ByteVal;
    tRadixNode         *pCurrNode = NULL;
    tRadixNode         *pTmpParent = NULL;

    u2KeySize = pInputParams->pRoot->u2KeySize;
    pCurrNode = pTmpParent = pInputParams->pRoot->pRadixNodeTop;

    /*
     *  for an empty trie, return NULL
     */
    if ((NULL == pCurrNode->pRight) && (NULL == pCurrNode->pLeft))
    {
        return NULL;
    }

    u2InPrefixLen = pInputParams->u1PrefixLen;
    while ((pCurrNode != NULL) && (pCurrNode->u1NodeType == RADIX_NODE))
    {
        u2CurPrefixLen = TrieDbPrefixLen (pCurrNode->Mask, u2KeySize);
        if (u2InPrefixLen <= u2CurPrefixLen)
        {
            break;
        }
        pTmpParent = pCurrNode;
        u1ByteVal =
            (UINT1) GETBYTE (pInputParams->Key, pCurrNode->u1ByteToTest,
                             u2KeySize);
        pCurrNode =
            (BIT_TEST (u1ByteVal, pCurrNode->u1BitToTest)) ? (tRadixNode *)
            pCurrNode->pRight : (tRadixNode *) pCurrNode->pLeft;
    }

    if ((pCurrNode == NULL) || (pCurrNode->u1NodeType != RADIX_NODE))
        return pTmpParent;

    return pCurrNode;
}

tLeafNode          *
TrieDbLeftmost (tRadixNode * pNode)
{
    while ((NULL != pNode) && (RADIX_NODE == pNode->u1NodeType))
        pNode = (tRadixNode *) pNode->pLeft;

    return (tLeafNode *) pNode;
}

tLeafNode          *
TrieDbRightmost (tRadixNode * pNode)
{
    while ((NULL != pNode) && (RADIX_NODE == pNode->u1NodeType))
        pNode = (tRadixNode *) pNode->pRight;

    return (tLeafNode *) pNode;
}

INT4
TrieDbCreateSemName (INT4 i4Type, UINT1 *pu1SemName)
{
    UINT2               u2SemNameLen = 0;

    sprintf ((CHR1 *) (pu1SemName + u2SemNameLen), "t%d", i4Type);
    return LPM_SUCCESS;

}

INT4
TrieDbGetSemId (VOID *pRoot, tUtilSemId * pSemId)
{
    UINT4               u4Instance;
    if (TrieDbGetTrieInstance (pRoot, &u4Instance) == LPM_FAILURE)
    {
        return LPM_FAILURE;
    }
    *pSemId = gaTrieDbInstance[u4Instance].SemId;
    return LPM_SUCCESS;
}

INT4
TrieDbGetTrieInstance (VOID *pRoot, UINT4 *pu4Instance)
{
    UINT4               u4TempInstance;

    for (u4TempInstance = 0; u4TempInstance < MAX_NUM_TRIE; u4TempInstance++)
    {
        if (&(gaTrieDbInstance[u4TempInstance]) == pRoot)
        {
            *pu4Instance = u4TempInstance;
            return LPM_SUCCESS;
        }
    }
    return LPM_FAILURE;
}

tLeafNode          *
TrieDbDoTraverseLessMoreSpecific (tInputParams * pInputParams)
{
    UINT2               u2KeySize;
    INT4                i4Return;
    tRadixNode         *pRadix = NULL;
    tLeafNode          *pLeaf = NULL;

    pRadix = pInputParams->pRoot->pRadixNodeTop;
    u2KeySize = pInputParams->pRoot->u2KeySize;

    if ((pRadix->pLeft == NULL) && (pRadix->pRight == NULL))
        return NULL;            /* empty trie */
    i4Return = TrieDbDoTraverse (u2KeySize, pRadix, pInputParams->Key, &(pLeaf));
    if (i4Return == LPM_FAILURE)
    {
        return NULL;
    }
    else
    {
        return (pLeaf);
    }
}
