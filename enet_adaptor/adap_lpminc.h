#ifndef _ADAP_LPM_INC_H
#define _ADAP_LPM_INC_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>

#include <sys/statfs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/times.h>
#include <sys/resource.h>
#include <sys/wait.h>

#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>



/************************************************************************
*                                                                       *
*                         Basic Types                                   *
*                                                                       *
*************************************************************************/

typedef char            BOOLEAN;
typedef char            BOOL1;
typedef char            CHR1;
typedef signed char     INT1;
typedef unsigned char   UINT1;
typedef UINT1           BYTE;
#undef VOID
#define VOID            void
typedef signed short    INT2;
typedef unsigned short  UINT2;
typedef signed int      INT4;
typedef unsigned int    UINT4;
typedef float           FLT4;
typedef double          DBL8;
typedef unsigned long   FS_ULONG;
typedef signed long     FS_LONG;
typedef long long unsigned int AR_UINT8;
typedef uintptr_t       UINTPTR;

struct sigaction    sigact;

/* For driver writers, and in case you are using longjmp etc. */
typedef volatile signed char     VINT1;
typedef volatile unsigned char   VUINT1;
typedef volatile signed short    VINT2;
typedef volatile unsigned short  VUINT2;
typedef volatile signed int      VINT4;
typedef volatile unsigned long   VUINT4;

#define U1MAX   254
#define FALSE  (0)
#define TRUE   (1)

#define LPM_SUCCESS               0
#define LPM_FAILURE               1

/* convert IPv4 mask to mask length */
#define IPV4_MASK_TO_MASKLEN(masklen, mask)  \
{ \
    UINT4 u4TmpMask  = (mask); \
    UINT4 u4TmpMasklen = 32;      \
    for ( ; u4TmpMasklen > 0; u4TmpMasklen--, u4TmpMask >>= 1) { \
         if (u4TmpMask & 0x1) { \
             break; \
         } \
    }\
    (masklen) = (UINT1) u4TmpMasklen; \
}


#define UTIL_NTOHL(x) (UINT4)(((x & 0xFF000000)>>24) | \
                              ((x & 0x00FF0000)>>8)  | \
                              ((x & 0x0000FF00)<<8 ) | \
                              ((x & 0x000000FF)<<24)   \
                             )
#define UTIL_NTOHS(x) (UINT2)(((x & 0xFF00)>>8) | ((x & 0x00FF)<<8))
#define UTIL_HTONL(x) (UINT4)(UTIL_NTOHL(x))
#define UTIL_HTONS(x) (UINT2)(UTIL_NTOHS(x))

#define log(s) fprintf(stderr, "%s:%d (%s) %s\n", __FILE__, __LINE__, __FUNCTION__, (s));

#define UTIL_OFFSETOF(StructType,Member)  (UINT4)(FS_ULONG)(&(((StructType*)0)->Member))

#define  ISDIGIT(c)        isdigit((int)c)
#define  ISALPHA(c)        isalpha((int)c)
#define  ISSPACE(ch)       isspace(ch)

#define  STRCPY(d,s)       strcpy ((char *)(d),(const char *)(s))
#define  STRNCPY(d,s,n)    strncpy ((char *)(d), (const char *)(s), (size_t) (n))

#define  STRCAT(d,s)       strcat((char *)(d),(const char *)(s))
#define  STRNCAT(d,s,n)    strncat((char *)(d),(const char *)(s), (size_t) (n))

#define  STRCMP(s1,s2)     strcmp  ((const char *)(s1), (const char *)(s2))
#define  STRNCMP(s1,s2,n)  strncmp ((const char *)(s1), (const char *)(s2), (size_t) (n))

#define  STRCHR(s,c)       strchr((const char *)(s), (int)c)
#define  STRRCHR(s,c)      strrchr((const char *)(s), (int)c)

#define  STRLEN(s)         strlen((const char *)(s))

#define MAX_RED_BLK_TREE_INST 100
#define MAC_ADDR_LEN        6
#define ETHERTYPE_OFFSET    12
#define TAG_OFFSET          ETHERTYPE_OFFSET + 2



typedef UINT1 tMacAddr [MAC_ADDR_LEN];

typedef struct _IP6_ADDRESS
{
    union
    {
        UINT1  u1ByteAddr[16];
        UINT2  u2ShortAddr[8];
        UINT4  u4WordAddr[4];
    }
    ip6_addr_u;

#define  u4_addr  ip6_addr_u.u4WordAddr
#define  u2_addr  ip6_addr_u.u2ShortAddr
#define  u1_addr  ip6_addr_u.u1ByteAddr

} tIp6Address;




/***************** UTIL ********************/
typedef struct UtilRscSemStruct
{
    sem_t               SemId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[8];
}
tUtilSem;

typedef sem_t *             tUtilSemId;
extern tUtilSemId gu4SemArray[];

tUtilSem            gaUtilSem[1001];
pthread_mutex_t     gUtilMutex;

UINT4 UtilRscAdd (UINT1[], UINT4, UINT4 *);
VOID UtilRscDel (UINT4, VOID *);
UINT4
UtilCreateSem (const UINT1 au1SemName [4],
               UINT4 u4InitialCount,
               UINT4 u4Flags,
               tUtilSemId *pSemId);

UINT4      UtilSemCrt (UINT1[], tUtilSemId*);
VOID       UtilSemDel (tUtilSemId);
UINT4      UtilSemGive (tUtilSemId);
UINT4      UtilSemTake (tUtilSemId);

#endif
