/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_vlanminp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_acl.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "mea_drv_common.h"
#include "adap_brgnp.h"
#include "EnetHal_L2_Api.h"



static ADAP_Int32 ADAP_ConfigVlanEntry(tBridgeDomain *pBridgeDomain);

static MEA_SE_Entry_dbt      last_mac_entry;
static ADAP_Uint32			 mac_addr_learning_index=0;

static void ADAP_callback_DeleteServices(void *data)
{
	tServiceDb *pServiceInfo = (tServiceDb *)data;
	if(MeaDrvDeleteVlanBridging(pServiceInfo) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvDeleteVlanBridging\r\n");
	}
}

static ADAP_Int32 ADAP_DeleteVlanEntry(tBridgeDomain *pBridgeDomain)
{
	ADAP_Int32 ret=ENET_SUCCESS;

	ret = Adap_deleteAllServiceFromLinkList(pBridgeDomain,ADAP_callback_DeleteServices);

	return ret;
}

static
ADAP_Int32 ADAP_AssignAclToService(tAclHierarchy *pHierarchy)
{
	tServiceDb *pServiceId=NULL;
	tUpdateServiceParam tParams;
	ADAP_Uint32 index;

	pServiceId = MeaAdapGetServiceIdByEditType(pHierarchy->L2_protocol_type,pHierarchy->ifIndex);

	if(pServiceId != NULL)
	{
			// update the service with filter
		printf("ADAP_AssignAclToService set filter params\r\n");
		adap_memset(&tParams,0,sizeof(tParams));
		tParams.x.filterPararms.ACL_filter_info.filter_mode_OR_AND_type = pHierarchy->filter_mode_OR_AND_type;
		tParams.mode = E_SET_SERVICE_CONFIGURATION;
		tParams.Service = pServiceId->serviceId;
		tParams.type = E_UPDATE_FILTER_PARAMETERS;
		tParams.x.filterPararms.filter_enable=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof_valid=ADAP_TRUE;
		tParams.x.filterPararms.ACL_Prof=pHierarchy->acl_profile_Hier.acl_profile;
		tParams.x.filterPararms.ACL_filter_info.filter_Whitelist_enable=MEA_FALSE;

		for(index=0;index<MEA_ACL_HIERARC_NUM_OF_FLOW;index++)
		{
			if(pHierarchy->priority_valid[index] == ADAP_TRUE)
			{
				tParams.x.filterPararms.ACL_filter_info.data_info[index].filter_key_type=pHierarchy->filter_key_type[index];
				tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_0_31 = pHierarchy->filter_mask[index].mask_0_31;
				tParams.x.filterPararms.ACL_filter_info.data_info[index].mask.mask_32_63 = pHierarchy->filter_mask[index].mask_32_63;
			}
		}
		if(MeaDrvUpdateService (&tParams) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to update service\r\n");
			return ENET_FAILURE;
		}
		pHierarchy->serviceId = pServiceId->serviceId;
	}
	return ENET_SUCCESS;
}

static ADAP_Int32 ADAP_ConfigVlanEntryPb(tBridgeDomain *pBridgeDomain)
{

	ADAP_Uint32 		idx=0;
	ADAP_Uint32 		idx1=0;
	ADAP_Int32 			ret=ENET_SUCCESS;
	ADAP_Uint8			u1PepAccepFrameType=ADAP_VLAN_ADMIT_ALL_FRAMES;
	ADAP_Uint32 		modeType=0;
	tEnetHal_VlanSVlanMap 	tVlanSVlanMapArr[10];
	ADAP_Uint32 		maxNumOfEntries=10;
	tEnetHal_VlanSVlanMap 	*pVlanSVlanMapChoosen=NULL;
	ADAP_Uint16			biggetCVlanId=0;

	for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
	{
		AdapGetProviderCoreBridgeMode(&modeType,pBridgeDomain->ifIndex[idx]);
		if( (pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) && (modeType == ADAP_CUSTOMER_EDGE_PORT) )
		{
			maxNumOfEntries=10;
			Adap_getAllSVlanMapListLinkList(&tVlanSVlanMapArr[0],&maxNumOfEntries,pBridgeDomain->vlanId,pBridgeDomain->ifIndex[idx]);
			if(maxNumOfEntries > 0)
			{
				for(idx1=0;idx1<maxNumOfEntries;idx1++)
				{
					// create services from customer side to service side according to pep/cep configuration
					if(MeaDrvCreatePbVlanCEP(pBridgeDomain->ifIndex[idx],pBridgeDomain,&tVlanSVlanMapArr[idx1],1) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreatePbVlanCEP ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
					if(Adap_IsPepPvidFoundLinkList(pBridgeDomain->vlanId,
												tVlanSVlanMapArr[idx1].CVlanId,pBridgeDomain->ifIndex[idx]) == ENET_SUCCESS)
					{
						pVlanSVlanMapChoosen = &tVlanSVlanMapArr[idx1];
					}
				}

				if(pVlanSVlanMapChoosen == NULL)
				{
					for(idx1=0;idx1<maxNumOfEntries;idx1++)
					{
						if(tVlanSVlanMapArr[idx1].CVlanId == biggetCVlanId)
						{
							pVlanSVlanMapChoosen = &tVlanSVlanMapArr[idx1];
						}
					}
				}
				if(pVlanSVlanMapChoosen != NULL)
				{
					if(MeaDrvCreatePbVlanCEP(pBridgeDomain->ifIndex[idx],pBridgeDomain,&tVlanSVlanMapArr[idx1],0) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreatePbVlanCEP ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
				}
			}
			// check if pvid configured - so PNP with S-VLAN sound swap to this C-VLAN
			else if(pBridgeDomain->vlanId == getPortToPvid(pBridgeDomain->ifIndex[idx]) )
			{

			}
		}
	}

	for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
	{
		u1PepAccepFrameType=ADAP_VLAN_ADMIT_ALL_FRAMES;
		AdapGetAccepFrameTypeMode(pBridgeDomain->ifIndex[idx],pBridgeDomain->vlanId,&u1PepAccepFrameType);
		AdapGetProviderCoreBridgeMode(&modeType,pBridgeDomain->ifIndex[idx]);
		if(pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT)
		{
			if(modeType == ADAP_CNP_PORTBASED_PORT)
			{
				// create tag service for all configuration
				if(MeaDrvCreatePbVlanCNP(pBridgeDomain->ifIndex[idx],pBridgeDomain,pVlanSVlanMapChoosen) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreatePbVlanCNP ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
					ret = ENET_FAILURE;
					break;
				}
			}
		}
		else
		{
			if( (modeType == ADAP_PROVIDER_NETWORK_PORT) && (modeType == ADAP_CNP_STAGGED_PORT) )
			{
				if(MeaDrvCreatePbVlanPNP(pBridgeDomain->ifIndex[idx],pBridgeDomain) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreatePbVlanPNP ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
					ret = ENET_FAILURE;
					break;
				}
				if(pVlanSVlanMapChoosen != NULL)
				{
					// create S-VLAN
					if(MeaDrvCreatePbVlanPNPsMap(pBridgeDomain->ifIndex[idx],pBridgeDomain,pVlanSVlanMapChoosen) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreatePbVlanPNPsMap ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
				}
			}
		}
	}
	return ret;
}

ADAP_Int32 adap_ConfigPb(tEnetHal_VlanId primaryVlan)
{
	tBridgeDomain *pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomain(primaryVlan,0);

	if(pBridgeDomain != NULL)
	{
		ADAP_ConfigVlanEntryPb(pBridgeDomain);
	}
	Adap_bridgeDomain_semUnlock();

	return ENET_SUCCESS;
}

static ADAP_Int32 ADAP_ConfigVlanEntry(tBridgeDomain *pBridgeDomain)
{
	ADAP_Uint32 idx=0;
//	ADAP_Int32 pvId=0;
	ADAP_Int32 ret=ENET_SUCCESS;
	ADAP_Uint8 acceptableType;
	tAclHierarchy *pHierarchy=NULL;
	ADAP_Bool blockStatus;
	tIpInterfaceTable *pIpInterface=NULL;
	ADAP_Uint32 providerMode = ADAP_VLAN_ACCESS_PORT;
	tServiceDb	*pServiceId = NULL;

	// delete any services on this bridgeDomain
	ADAP_DeleteVlanEntry(pBridgeDomain);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"num_of_ports:%d vlanId:%d\n",pBridgeDomain->num_of_ports,pBridgeDomain->vlanId);

	switch(AdapGetBridgeMode())
	{
		case ADAP_VLAN_CUSTOMER_BRIDGE_MODE:
		case ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE:
		case ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE:
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] != 0)
				{
					adap_GetTrafficBlockStatusPerPort(pBridgeDomain->ifIndex[idx], &blockStatus);
					if(blockStatus == ADAP_TRUE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to configure, traffic is blocked for ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
				}
				// Because acceptableType is not a part of BridgeDomain (it's part of AdapDatabase)
				// we need some additional check
				if(pBridgeDomain->vlanId == 0)
				{
					acceptableType = ADAP_VLAN_ADMIT_ALL_FRAMES;
				}
				else
				{
					AdapGetAccepFrameType(pBridgeDomain->ifIndex[idx],&acceptableType);
				}

				if( (pBridgeDomain->ifType[idx] != 0) && (acceptableType != ADAP_VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES) )
				{
					/* For L3 interface, separate service needed for L2 domain and L3 */
					pIpInterface = AdapGetIpInterfaceByVlanId(pBridgeDomain->vlanId);
					if ((pIpInterface != NULL) && (pIpInterface->valid == ADAP_TRUE))
					{
						pIpInterface->valid = ADAP_FALSE;
						if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[idx],
							pBridgeDomain,VLAN_TAGGED_MEMBER_PORT) != MEA_OK)
						{
							pIpInterface->valid = ADAP_TRUE;
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
								"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",
								pBridgeDomain->ifIndex[idx]);
							ret = ENET_FAILURE;
							break;
						}
						pIpInterface->valid = ADAP_TRUE;
					}
					if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[idx],pBridgeDomain,VLAN_TAGGED_MEMBER_PORT) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
					// if there is filters need to update the service
					//printf("ADAP_ConfigVlanEntry vlanId:%d ifIndex:%d\r\n",pBridgeDomain->vlanId,pBridgeDomain->ifIndex[idx]);
					pHierarchy = AdapGetAclHierarchyByType(pBridgeDomain->ifIndex[idx],MEA_PARSING_L2_KEY_Ctag,pBridgeDomain->vlanId);
					if(pHierarchy != NULL)
					{
						ADAP_AssignAclToService(pHierarchy);
					}

					// we need update dot1x service
					pServiceId = MeaAdapGetServiceIdByVlanIfIndex(pBridgeDomain->vlanId, pBridgeDomain->ifIndex[idx], MEA_PARSING_L2_KEY_Ctag);

					if (pServiceId != NULL)
					{
						MeaDrvVlanHwUpdatePnac (pServiceId);
					}
					else
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to get ServiceId\r\n");
					}
				}
				// if port configured as untagged and PVID
				if( (pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) &&
					(getPortToPvid(pBridgeDomain->ifIndex[idx]) == pBridgeDomain->vlanId) &&
					(acceptableType != ADAP_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES) )
				{
					/* For L3 interface, separate service needed for L2 domain and L3 */
					pIpInterface = AdapGetIpInterfaceByVlanId(pBridgeDomain->vlanId);
					if ((pIpInterface != NULL) && (pIpInterface->valid == ADAP_TRUE))
					{
						pIpInterface->valid = ADAP_FALSE;
						if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[idx],
							pBridgeDomain,VLAN_UNTAGGED_MEMBER_PORT) != MEA_OK)
						{
							pIpInterface->valid = ADAP_TRUE;
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
								"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",
								pBridgeDomain->ifIndex[idx]);
							ret = ENET_FAILURE;
							break;
						}
						pIpInterface->valid = ADAP_TRUE;
					}
					if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[idx],pBridgeDomain,VLAN_UNTAGGED_MEMBER_PORT) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
					// if there is filters need to update the service
					pHierarchy = AdapGetAclHierarchyByType(pBridgeDomain->ifIndex[idx],MEA_PARSING_L2_KEY_Untagged,pBridgeDomain->vlanId);
					if(pHierarchy != NULL)
					{
						ADAP_AssignAclToService(pHierarchy);
					}

					// we need update dot1x service
					pServiceId = MeaAdapGetServiceIdByVlanIfIndex(pBridgeDomain->vlanId, pBridgeDomain->ifIndex[idx], MEA_PARSING_L2_KEY_Untagged);

					if (pServiceId != NULL)
					{
						MeaDrvVlanHwUpdatePnac (pServiceId);
					}
					else
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to get ServiceId\r\n");
					}

					// TODO in case of pvid so it need to create service with priority swap to VLAN id
				}
			}
			if ((AdapGetBridgeMode () == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
			    (AdapGetBridgeMode () == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
			{
				// need to check acceptable frame type , pep and cep configuration , pvid, and pep pvid
				ret = ADAP_ConfigVlanEntryPb(pBridgeDomain);
			}	
			break;
		case ADAP_VLAN_PROVIDER_BRIDGE_MODE: //QinQ
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if (AdapGetProviderBridgeMode(&providerMode, pBridgeDomain->ifIndex[idx]) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "failed to get BridgeMode at ifIndex:%d\r\n", pBridgeDomain->ifIndex[idx]);
					ret = ENET_FAILURE;
				}

				// if port configured as untagged and PVID or tarrged then it can configured
				if( (pBridgeDomain->ifType[idx] == VLAN_UNTAGGED_MEMBER_PORT) &&
					(getPortToPvid(pBridgeDomain->ifIndex[idx]) == pBridgeDomain->vlanId) ) 
				{
					if(MeaDrvCreateQinQVlanBridging(pBridgeDomain->ifIndex[idx],
							pBridgeDomain,VLAN_UNTAGGED_MEMBER_PORT) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
							"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
					if (providerMode == ADAP_VLAN_TRUNK_PORT)
					{
						if(MeaDrvCreateQinQVlanBridging(pBridgeDomain->ifIndex[idx],
								pBridgeDomain,VLAN_TAGGED_MEMBER_PORT) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
								"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
							ret = ENET_FAILURE;
							break;
						}
					}
				}
				else if (pBridgeDomain->ifType[idx] == VLAN_TAGGED_MEMBER_PORT) 
				{
					if (MeaDrvCreateQinQVlanBridging(pBridgeDomain->ifIndex[idx],
							pBridgeDomain,VLAN_TAGGED_MEMBER_PORT) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
							"failed to create QinQ service for ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
						ret = ENET_FAILURE;
						break;
					}
					if (providerMode == ADAP_VLAN_TRUNK_PORT)
					{
						if (MeaDrvCreateQinQVlanBridging(pBridgeDomain->ifIndex[idx],
								pBridgeDomain,VLAN_UNTAGGED_MEMBER_PORT) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
								"failed to create QinQ service for ifIndex:%d\r\n",pBridgeDomain->ifIndex[idx]);
							ret = ENET_FAILURE;
							break;
						}
					}
				}
			}
			break;
		case ADAP_VLAN_PBB_ICOMPONENT_BRIDGE_MODE:
		case ADAP_VLAN_PBB_BCOMPONENT_BRIDGE_MODE:
			break;
		case ADAP_VLAN_INVALID_BRIDGE_MODE:
		default:
			break;
	}
	return ret;
}

ADAP_Int32
EnetHal_FsMiVlanHwInit (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Int32 	ret = ENET_SUCCESS;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32	index=0;
	ADAP_Uint32           issPort;

	// init the vlan database vpn
	ret = initVlanDatabase();
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to init vlan database\r\n");
	}
	else
	{

	}
#ifdef	MEA_NAT_DEMO
	// interface 105 forward dhcp server messages to 101
	EnetAdapDhcpRedirectDemo(1,3,MEA_LXCP_PROTOCOL_DHCP_SERVER);
	// interface 101 forward dhcp client messahes to 105
	EnetAdapDhcpRedirectDemo(3,1,MEA_LXCP_PROTOCOL_DHCP_CLIENT);

	// interface 104 forward message of dhcp to CPU
	ret = getLxcpId(2,&lxcp);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
	}

	ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp,NULL,NULL);
	if(ret != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MeaDrvSetLxcpParamaters\r\n");
		ret = ENET_FAILURE;
	}
	ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp,NULL,NULL);
	if(ret != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MeaDrvSetLxcpParamaters\r\n");
		ret = ENET_FAILURE;
	}
#endif
	// need to create priority tag configuration
	ret = Adap_allocateBridgeDomain(0,u4ContextId);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call allocateBridgeDomain\r\n");
	}


	pBridgeDomain = Adap_getBridgeDomain(0,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		pBridgeDomain->vlanId = 0;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"bridge domain associate  VLAN Id:%d\r\n",pBridgeDomain->vlanId);

		for(index=1;index<MeaAdapGetNumOfPhyPorts();index++)
		{
			pBridgeDomain->ifIndex[pBridgeDomain->num_of_ports] = index;
			pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_TAGGED_MEMBER_PORT;
			pBridgeDomain->num_of_ports++;
		}

		for(index = 0; index < pBridgeDomain->num_of_ports; index++)
		{
			if(MeaDrvCreateVlanBridging(pBridgeDomain->ifIndex[index], pBridgeDomain, VLAN_TAGGED_MEMBER_PORT) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvCreateVlanBridging ifIndex:%d\r\n",pBridgeDomain->ifIndex[index]);
				ret = ENET_FAILURE;
				break;
			}
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"bridge domain not found\r\n");
		ret = ENET_FAILURE;
	}

    /* Create default service to be used by all ports (except for 127) */
    /* Set the upstream service key */
    for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
    {
		if(adap_GetDefaultSid(issPort) != ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ISS port %d allready configured default service\n",  issPort);
			continue;
		}
		MeaDrvLxcpUpdateProtocols(issPort,MEA_LXCP_PROTOCOL_ACTION_DISCARD,MEA_LXCP_PROTOCOL_IN_BPDU_1);

		if(MeaDrvCreateDefaultService(issPort) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Failed to create default service for port:%d\r\n", issPort);
			ret = ENET_FAILURE;
		}
    }

    return ret;
}

ADAP_Int32
EnetHal_FsMiVlanHwDeInit (ADAP_Uint32 u4ContextId)
{
    MEA_Service_t   Service_id;
    MEA_Bool        valid;
    ADAP_Uint16			issPort=0;

    /* Create default service to be used by all ports (except for 127) */
    /* Set the upstream service key */
    for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
    {
		Service_id = adap_GetDefaultSid(issPort);
	   /* Delete default service = 1*/
		/* check if the service exist */
		if(Service_id == ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeInit default service over port:%d not found\n", issPort);
			continue;
		}

		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeInit ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}
		if(valid == MEA_FALSE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeInit ERROR: ServiceId %d does not exists\n", Service_id);
			return ENET_FAILURE;
		}

		/* Delete the service */
		MeaDeleteEnetServiceId(Service_id);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",Service_id);
		adap_SetDefaultSid(issPort,ADAP_END_OF_TABLE);
	}

    /* Delete default Vlan = 0 */
    if(EnetHal_FsMiVlanHwDelVlanEntry(u4ContextId, 0) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeInit FAIL upon updating of VlanId 0\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwAddStaticUcastEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_MacAddr *MacAddr,
		ADAP_Uint32 u4Port, tEnetHal_HwPortArray * pHwAllowedToGoPorts,ADAP_Uint8 u1Status)
{
    MEA_Port_t 			enetAllowedPort;
    MEA_SE_Entry_dbt	entry;
	int					portIdx;
	ADAP_Uint16 		VpnIndex;
	tBridgeDomain 		*pBridgeDomain=NULL;
	ADAP_Uint32 		u4IfIndex = 0;

    /* Clear the SE entry struct */
    adap_memset(&entry, 0, sizeof(entry));

    /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;

    adap_mac_to_arr(MacAddr, entry.key.mac_plus_vpn.MAC.b);

	// get vpnId from fdb
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByFdb(u4Fid);

	if(pBridgeDomain != NULL)
	{
		VpnIndex = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database\r\n");
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	entry.key.mac_plus_vpn.VPN = VpnIndex;

    /* Fill in the scalar data parameters */
    entry.data.aging = 3; //Default
    entry.data.static_flag  = (u1Status == ENET_VLAN_DELETE_ON_TIMEOUT? MEA_FALSE : MEA_TRUE);
    entry.data.limiterId_valid = MEA_FALSE;
    entry.data.limiterId       = 0;
    entry.data.actionId_valid  = MEA_FALSE;

    /* Fill in the destination ports list */
    for ( portIdx = 0; portIdx < pHwAllowedToGoPorts->i4Length; portIdx ++ )
    {
        u4IfIndex = pHwAllowedToGoPorts->pu4PortArray[portIdx];

		if (adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
		{
			if( (adap_NumOfLagPortFromAggrId(u4IfIndex)) > 0)
			{
				u4IfIndex = adap_GetLagMasterPort(u4IfIndex);
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"LAG Id:%d has no any physical ports\n", u4IfIndex);
			}
		}

		if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetAllowedPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n", u4IfIndex);
			return ENET_FAILURE;
		}

    	//enetClusterNum = DPLGetVlanPortCluster(VlanId, pHwAllowedToGoPorts->pu4PortArray[portIdx]);
   		//MEA_SET_OUTPORT(&entry.data.OutPorts, (enetClusterNum == pHwAllowedToGoPorts->pu4PortArray[portIdx]) ? enetAllowedPort : enetClusterNum);
   		MEA_SET_OUTPORT(&entry.data.OutPorts, enetAllowedPort);
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwAddStaticUcastEntry - portIdx:%d ISSAllowedPorts:%d, enetAllowedPort:%d\n", portIdx, u4IfIndex, enetAllowedPort);
    }

    /* Commit the new entry to the forwarder */
    if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddStaticUcastEntry ERROR: ENET_ADAPTOR_Create_SE_Entry FAIL!!! ===\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwDelStaticUcastEntry (ADAP_Uint32 u4ContextId,ADAP_Uint32 u4Fid, tEnetHal_MacAddr *MacAddr,ADAP_Int32 u4Port)
{
    MEA_SE_Entry_dbt	entry;
    ADAP_Uint16			VpnIndex;
	tBridgeDomain 		*pBridgeDomain=NULL;

    /* Clear the SE entry struct */
    adap_memset(&entry, 0, sizeof(entry));

    /* Fill the key parameters (using the the Dest MAC + VPN key type; VPN == VLAN ID) */
	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(MacAddr, entry.key.mac_plus_vpn.MAC.b);

	// get vpnId from fdb
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByFdb(u4Fid);

	if(pBridgeDomain != NULL)
	{
		VpnIndex = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database\r\n");
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

    entry.key.mac_plus_vpn.VPN = VpnIndex;

	/* Delete the SE entry */
    if(ENET_ADAPTOR_Delete_SE_Entry (MEA_UNIT_0, &entry.key) != MEA_OK) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDelStaticUcastEntry ERROR: ENET_ADAPTOR_Delete_SE_Entry FAIL!!! ===\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwGetFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwUnicastMacEntry * pEntry)
{
    MEA_Port_t                  port;
    MEA_SE_Entry_dbt            entry;
    ADAP_Uint32                 IssPortId;
    ADAP_Uint16                 PortId;
    ADAP_Uint16                 IssMasterPortId;
	tMacLearningTable 			macLearning;

	ADAP_UNUSED_PARAM(u4ContextId);

    if(adap_getEntryFromMacLearning(&macLearning,u4FdbId,MacAddr) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_FsMiVlanHwGetFdbEntry for MAC:%s FDB:%d not found in the Enet adaptor DB\n",
        		adap_mac_to_string(MacAddr), u4FdbId);
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(MacAddr, entry.key.mac_plus_vpn.MAC.b);
	entry.key.mac_plus_vpn.VPN = macLearning.vpnId;


	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_FsMiVlanHwGetFdbEntry ENET_ADAPTOR_Get_SE_Entry: failed to get entry mac:%s vpn:%d\r\n",
        		adap_mac_to_string(MacAddr), macLearning.vpnId);
        return ENET_SUCCESS;
	}

    for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
    {
        if (((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] & (1 << (port%32)))
        {
 			if(MeaAdapGetLogPortFromPhyPort(&IssPortId, port) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGetFdbEntry: invalid newMasterclusterId:%d\r\n",port);
				return ENET_FAILURE;
			}

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsMiVlanHwGetFdbEntry - issPort:%d, enetPortId:%d\n",IssPortId, port);

			IssMasterPortId = adap_GetMasterPort(IssPortId);
			if(IssMasterPortId != ADAP_END_OF_TABLE)
			{
				PortId = adap_GetLagAggrFromMasterPort(IssMasterPortId);
				if(PortId == ADAP_END_OF_TABLE)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGetFdbEntry ERROR: ENET port:%d issPortId:%d\n", port,IssPortId);
				}
				IssPortId = PortId;
            }

            pEntry->u4Port = IssPortId;
            pEntry->u1EntryType = (entry.data.static_flag) ? ENET_VLAN_FDB_MGMT : ENET_VLAN_FDB_LEARNT;
            pEntry->u1HitStatus = entry.data.valid;
            adap_memset(&pEntry->ConnectionId, 0, sizeof(tEnetHal_MacAddr));

            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwGetFdbEntry - entry is found for Enet port:%d, ISS port:%d EntryType:%d Status:%d\n", port, IssPortId, pEntry->u1EntryType, pEntry->u1HitStatus);
            return ENET_SUCCESS;
        }
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGetFdbCount (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, ADAP_Uint32 *pu4Count)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    MEA_SE_Entry_dbt    entry;
    unsigned long		countFWD;
    MEA_Bool    	    found;
	tBridgeDomain 	  	*pBridgeDomain=NULL;

    adap_memset(&entry, 0, sizeof(entry));
    *pu4Count = 0;

#ifdef USE_ENET_MUTEX
    ENET_FWD_Lock();
#endif
    countFWD=0;

    if (ENET_ADAPTOR_GetFirst_SE_Entry( MEA_UNIT_0, &entry, &found) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGetFdbCount ERROR: ENET_ADAPTOR_GetFirst_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
        ENET_FWD_Unlock();
#endif
        return ENET_FAILURE;
    }

    while (found == MEA_TRUE)
    {
    	if(entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN)
    	{
    		// get fdbId from vpn
    		Adap_bridgeDomain_semLock();
    		pBridgeDomain = Adap_getBridgeDomainByVpn(entry.key.mac_plus_vpn.VPN);
    		if(pBridgeDomain != NULL)
    		{
				if(pBridgeDomain->swFdb == u4FdbId)
					countFWD++;
    		}

    		Adap_bridgeDomain_semUnlock();
    	}

        if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,
                                     &entry,
                                     &found) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGetFdbCount ERROR: ENET_ADAPTOR_GetNext_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
            ENET_FWD_Unlock();
#endif
            return ENET_FAILURE;
        }
     }
    *pu4Count = countFWD;
#ifdef USE_ENET_MUTEX
    ENET_FWD_Unlock();
#endif

	return ENET_SUCCESS;
}

static ADAP_Uint32 FsMiVlanHwGetAllTpFdbEntry (MEA_Bool get_next)
{
	tMacLearningTable 	macLearning;
	MEA_SE_Entry_dbt    entry;
	MEA_Bool    	    found;
    ADAP_Uint32			iss_port;
    ADAP_Uint16			IssMasterPortId,port;
    ADAP_Uint16			vlanId;
    ADAP_Uint32			total_mac_addr_learning=0;
	tBridgeDomain 		*pBridgeDomain=NULL;

	/* Reset the mac learning db*/
    adap_resetMacLearningTable();
    mac_addr_learning_index=0;

    adap_memset(&entry,0,sizeof(MEA_SE_Entry_dbt));
#ifdef USE_ENET_MUTEX
	ENET_FWD_Lock();
#endif
	if(get_next == MEA_TRUE)
	{
		adap_memcpy(&entry,&last_mac_entry,sizeof(MEA_SE_Entry_dbt));
		if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,
									 &entry,
									 &found) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_GetNext_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
			ENET_FWD_Unlock();
#endif
			 return ENET_FAILURE;
		}
	}
	else
	{
		total_mac_addr_learning=0;
		if (ENET_ADAPTOR_GetFirst_SE_Entry(MEA_UNIT_0,
									 &entry,
									 &found) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_GetFirst_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
			ENET_FWD_Unlock();
#endif
			 return ENET_FAILURE;
		}
		if(found == MEA_FALSE)
		{
			printf("table empty\n");
		}
	}
#ifdef USE_ENET_MUTEX
	ENET_FWD_Unlock();
#endif

	while(found)
	{
		// if its MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN and not VLAN priority then give MAC parameters
		if( (entry.key.type == MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN) /*&& (entry.key.mac_plus_vpn.VPN != 1)*/ )
		{
		    for (port = 0;port <ENET_MAX_NUM_OF_EXTERNAL_QUEUE_ELEMENT; port++)
		    {
		        if (((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[port/32] & (1 << (port%32)))
		        {
					if(MeaAdapGetLogPortFromPhyPort(&iss_port, port) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwGetAllTpFdbEntry: invalid port:%d\r\n",port);
						continue;
					}
		            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsMiVlanHwGetAllTpFdbEntry - issPort:%d, enetPortId:%d\n",iss_port, port);

					IssMasterPortId = adap_GetMasterPort(iss_port);
					if(IssMasterPortId != ADAP_END_OF_TABLE)
					{
						iss_port = adap_GetLagAggrFromMasterPort(IssMasterPortId);
		            }
					break;
		        }
		    }

		    macLearning.vpnId=entry.key.mac_plus_vpn.VPN;

			if(entry.key.mac_plus_vpn.VPN == 1)
			{
				vlanId = getPortToPvid(iss_port);
				Adap_bridgeDomain_semLock();
				pBridgeDomain = Adap_getBridgeDomainByVlan(vlanId,0);
				if(pBridgeDomain != NULL)
				{
					macLearning.u4FdbId = pBridgeDomain->swFdb;
				}
				Adap_bridgeDomain_semUnlock();
			}
			else
			{
				Adap_bridgeDomain_semLock();
				pBridgeDomain = Adap_getBridgeDomainByVpn(entry.key.mac_plus_vpn.VPN);
				if(pBridgeDomain != NULL)
				{
					macLearning.u4FdbId = pBridgeDomain->swFdb;
				}

				Adap_bridgeDomain_semUnlock();
			}

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"sMiVlanHwGetAllTpFdbEntry, MAC:%x:%x:%x:%x:%x:%x found fdb:%d\n",
								entry.key.mac_plus_vpn.MAC.b[0],
								entry.key.mac_plus_vpn.MAC.b[1],
								entry.key.mac_plus_vpn.MAC.b[2],
								entry.key.mac_plus_vpn.MAC.b[3],
								entry.key.mac_plus_vpn.MAC.b[4],
								entry.key.mac_plus_vpn.MAC.b[5],
								macLearning.u4FdbId);

			if (!(((ENET_Uint32*)(&(entry.data.OutPorts.out_ports_0_31)))[MEA_CPU_PORT/32] & (1 << (MEA_CPU_PORT%32))))
			{
				adap_mac_from_arr(&macLearning.MacAddr, entry.key.mac_plus_vpn.MAC.b);
				if(macLearning.u4FdbId != ADAP_END_OF_TABLE)
				{
					adap_insertEntryToMacLearning(&macLearning);
					total_mac_addr_learning++;
				}
			}
			adap_memcpy(&last_mac_entry,&entry,sizeof(MEA_SE_Entry_dbt));

			if(adap_IsMacLearningTableFull() == ENET_SUCCESS)
			{
				return ENET_SUCCESS;
			}
		}
#ifdef USE_ENET_MUTEX
		ENET_FWD_Lock();
#endif
		if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,
									 &entry,
									 &found) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_GetFirst_SE_Entry failed \n");
#ifdef USE_ENET_MUTEX
			ENET_FWD_Unlock();
#endif
			 return ENET_FAILURE;
		}
		if(found == MEA_FALSE)
		{
			adap_memcpy(&entry,&last_mac_entry,sizeof(MEA_SE_Entry_dbt));
			// try again
			if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0,
										 &entry,
										 &found) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_GetNext_SE_Entry failed \n");
	#ifdef USE_ENET_MUTEX
				ENET_FWD_Unlock();
	#endif
				 return ENET_FAILURE;
			}
			if(found == MEA_FALSE)
			{
				printf("end of table\r\n");
			}
		}
#ifdef USE_ENET_MUTEX
		ENET_FWD_Unlock();
#endif
	}
	return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwGetFirstTpFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 *pu4FdbId,tEnetHal_MacAddr *MacAddr)
{
	tMacLearningTable 	macLearning;

	ADAP_UNUSED_PARAM(u4ContextId);

	FsMiVlanHwGetAllTpFdbEntry(MEA_FALSE);

	if(adap_getEntryFromMacLearningFromIndex(&macLearning,mac_addr_learning_index) != ENET_SUCCESS)
	{
		if (mac_addr_learning_index == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"mac address table is empty\n");
			return ENET_FAILURE;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"get_entry_from_mac_learning_from_index\n");
			return ENET_FAILURE;
		}
	}

	mac_addr_learning_index++;

	adap_memcpy(MacAddr, &macLearning.MacAddr, sizeof(tEnetHal_MacAddr));

	(*pu4FdbId) = macLearning.u4FdbId;

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwGetNextTpFdbEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4FdbId, tEnetHal_MacAddr *MacAddr,
		ADAP_Uint32 *pu4NextContextId, ADAP_Uint32 *pu4NextFdbId,tEnetHal_MacAddr *NextMacAddr)
{
    ADAP_UNUSED_PARAM (pu4NextContextId);
    tMacLearningTable 	macLearing;

    if (adap_is_mac_zero(MacAddr) == ADAP_TRUE)
    {
        if(EnetHal_FsMiVlanHwGetFirstTpFdbEntry (u4ContextId, &u4FdbId, MacAddr) != ENET_SUCCESS)
        {
        	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"FsMiVlanHwGetNextTpFdbEntry ERROR: FsMiVlanHwGetFirstTpFdbEntry\n");
            return ENET_FAILURE;
		}
		/* Copy the results to NextMacAddr*/
		adap_memcpy(NextMacAddr, MacAddr, sizeof(tEnetHal_MacAddr));
		*pu4NextFdbId = u4FdbId ;
		return ENET_SUCCESS;
    }

	if(adap_getEntryFromMacLearningFromIndex(&macLearing,mac_addr_learning_index) != ENET_SUCCESS)
	{
		FsMiVlanHwGetAllTpFdbEntry(MEA_TRUE);
		if(adap_getEntryFromMacLearningFromIndex(&macLearing,mac_addr_learning_index) != ENET_SUCCESS)
		{
			return ENET_FAILURE;
		}
	}

	mac_addr_learning_index++;

	adap_memcpy(NextMacAddr, &macLearing.MacAddr, sizeof(tEnetHal_MacAddr));
	(*pu4NextFdbId) = macLearing.u4FdbId;

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwAddMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwPortArray * pHwMcastPorts)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tMulticastMac 							*multicastEntry=NULL;
	ADAP_Int32		 						index=0;
	tBridgeDomain 	  						*pBridgeDomain=NULL;
	ADAP_Uint16		  						vpn;
	MEA_Action_t      						action_Id = MEA_PLAT_GENERATE_NEW_ID;
    MEA_OutPorts_Entry_dbt                  Action_outPorts;
	MEA_Port_t								enetOutPort;

	if(pHwMcastPorts->i4Length > 0)
	{
		for(index = 0; index < pHwMcastPorts->i4Length; index++)
			 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"Index:%d McastPort:%d\r\n", pHwMcastPorts->pu4PortArray[index]);
	}

	multicastEntry = adap_getMulticastMacEntry(MacAddr);
	if(multicastEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Multicast entry is not found!\n");
		return ENET_FAILURE;
	}

	for(index = 0; index < pHwMcastPorts->i4Length; index++)
	{
		multicastEntry->portbitmap |= 1 << pHwMcastPorts->pu4PortArray[index];
	}
	// get vpnId from vlan
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pBridgeDomain != NULL)
	{
		vpn = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database VlanId:%d\r\n",VlanId);
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	// check if entry found in forwarder
	if((MeaDrvHwGetDestMacForwarder(MacAddr, vpn, &action_Id) == MEA_OK) && (action_Id != MEA_PLAT_GENERATE_NEW_ID))
	{
		// if exist
		// take the action id and do action ID get/set
		if (MeaDrvHwSetAction (action_Id, pHwMcastPorts) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MeaDrvHwSetAction action_Id:%d\n", action_Id);
			return ENET_FAILURE;
		}
	}
	else
	{
		// if not exist
		// create action
		action_Id = MEA_PLAT_GENERATE_NEW_ID;
		if( MeaDrvCreateMulticastAction(&action_Id,pHwMcastPorts->pu4PortArray,pHwMcastPorts->i4Length) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create action\n");
			return ENET_FAILURE;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action_Id:%d created\n", action_Id);
	}

    adap_memset(&Action_outPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	for(index = 0; index < pHwMcastPorts->i4Length; index++)
	{
		MeaAdapGetPhyPortFromLogPort(pHwMcastPorts->pu4PortArray[index],&enetOutPort);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enetOutPort:%d\n", enetOutPort);
		MEA_SET_OUTPORT(&Action_outPorts,enetOutPort);
	}
	// create entry at the forwarder
	MeaDrvHwCreateDestMacForwarder(action_Id, MEA_TRUE, MacAddr, vpn, &Action_outPorts);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwAddStMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,
		ADAP_Int32 i4RcvPort, tEnetHal_HwPortArray * pHwMcastPorts)
{
	ADAP_UNUSED_PARAM (i4RcvPort);

     if(EnetHal_FsMiVlanHwAddMcastEntry(u4ContextId, VlanId, MacAddr, pHwMcastPorts) != ENET_SUCCESS)
     {
    	 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwAddStMcastEntry failed for VlanId:%d MAC:%s\n",
    			 VlanId, adap_mac_to_string(MacAddr));
    	 return ENET_FAILURE;
     }

    return ENET_SUCCESS;

}

ADAP_Int32 EnetHal_FsMiVlanHwSetMcastPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Uint32 u4IfIndex)
{

	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);

	/* Implemented as EnetHal_FsMiVlanHwSetMcastIndex */
	tMulticastMac *multicastEntry;

	multicastEntry = adap_createMulticastMacEntry(MacAddr);
	if(multicastEntry != NULL)
	{
		multicastEntry->vlanId=VlanId;
		multicastEntry->portbitmap=0;
	}

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwResetMcastPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Uint32 u4IfIndex)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tBridgeDomain 	  		*pBridgeDomain=NULL;
	MEA_Action_t   			Action_id=MEA_PLAT_GENERATE_NEW_ID;
	tMulticastMac 			*multicastEntry=NULL;
	MEA_OutPorts_Entry_dbt 	tOutPorts;
	ADAP_Uint16				vpn=0;
	tEnetHal_HwPortArray 		HwMcastPorts;
	ADAP_Uint32				pu4PortArray[32];
	ADAP_Uint32				index=0;
	MEA_Port_t				enetOutPort;

	HwMcastPorts.pu4PortArray = &pu4PortArray[0];
	HwMcastPorts.i4Length=0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	multicastEntry = adap_getMulticastMacEntry(MacAddr);
	if(multicastEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"multicastEntry not exist for MAC:%x:%x:%x:%x:%x:%x\r\n",MacAddr[0],MacAddr[1],MacAddr[2],MacAddr[3],MacAddr[4],MacAddr[5]);
		return ENET_FAILURE;
	}
	multicastEntry->portbitmap &= ~(1 << u4IfIndex);

	// get vpnId from vlan
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pBridgeDomain != NULL)
	{
		vpn = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database VlanId:%d\r\n",VlanId);
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	// if portbitmap is 0
	// delete entry from forwarder and then delete action
	// else
	// set forwarder and set to action
	if(multicastEntry->portbitmap == 0)
	{
		MeaDrvHwDeleteDestMacForwarder(MacAddr,vpn,&Action_id);
		if(Action_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0,Action_id) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Delete_Action failed for Action_id:%d\r\n",Action_id);
				return ENET_FAILURE;
			}
		}
	}
	else
	{
		for(index=1;index<MeaAdapGetNumOfPhyPorts();index++)
		{
			if(multicastEntry->portbitmap & (1 <<index) )
			{
				HwMcastPorts.pu4PortArray[HwMcastPorts.i4Length++] = index;
				MeaAdapGetPhyPortFromLogPort(index,&enetOutPort);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enetOutPort:%d\n", enetOutPort);
				MEA_SET_OUTPORT(&tOutPorts,enetOutPort);
			}
		}

	    if(MeaDrvHwSetDestMacForwarder(MacAddr, vpn, &tOutPorts, &Action_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwResetMcastPort FAIL: MeaDrvHwSetDestMacForwarder!!!\n");
			return ENET_FAILURE;
		}

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvHwSetDestMacForwarder Action_id\n", Action_id);

		//set action
		if(Action_id != MEA_PLAT_GENERATE_NEW_ID)
		{
			if (MeaDrvHwSetAction (Action_id, &HwMcastPorts) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MeaDrvHwSetAction action_Id:%d\n", Action_id);
				return ENET_FAILURE;
			}
		}
	}
    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwDelMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tBridgeDomain 	  	*pBridgeDomain=NULL;
	MEA_Action_t   		Action_id=MEA_PLAT_GENERATE_NEW_ID;
	tMulticastMac 		*multicastEntry=NULL;
	ADAP_Uint16			vpn=0;

	multicastEntry = adap_getMulticastMacEntry(MacAddr);
	if(multicastEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"multicastEntry not exist for MAC:%s\r\n",adap_mac_to_string(MacAddr));
		return ENET_FAILURE;
	}

	// get vpnId from vlan
	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pBridgeDomain != NULL)
	{
		vpn = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database VlanId:%d\r\n",VlanId);
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	MeaDrvHwDeleteDestMacForwarder(MacAddr,vpn,&Action_id);
	if(Action_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		if (ENET_ADAPTOR_Delete_Action(MEA_UNIT_0,Action_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET_ADAPTOR_Delete_Action failed for Action_id:%d\r\n",Action_id);
			return ENET_FAILURE;
		}
	}

	adap_freeMulticastMacEntry(MacAddr);

    return ENET_SUCCESS;
}

ADAP_Int32
EnetHal_FsMiVlanHwDelStMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,ADAP_Int32 i4RcvPort)
{
	ADAP_UNUSED_PARAM (i4RcvPort);

    if (EnetHal_FsMiVlanHwDelMcastEntry (u4ContextId, VlanId, MacAddr) != ENET_SUCCESS)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"EnetHal_FsMiVlanHwDelStMcastEntry failed\n");
            return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

static ADAP_Int32 ADAP_DeleteIfIndexVlanEntry(tBridgeDomain *pBridgeDomain,ADAP_Uint32 u4IfIndex,ADAP_Uint32 L2_protocol_type,ADAP_Uint16 vlanId)
{
	tServiceDb *pDelService=NULL;
	ADAP_Int32 ret = ENET_SUCCESS;

	pDelService = Adap_freeServiceFromLinkList(pBridgeDomain,u4IfIndex,L2_protocol_type,vlanId);

	if(pDelService != NULL)
	{
		if(MeaDrvDeleteVlanBridging(pDelService) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvDeleteVlanBridging ifIndex:%d\r\n",u4IfIndex);
			ret = ENET_FAILURE;
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service for ifIndex:%d protocol_tpye:%d not found\r\n",u4IfIndex,L2_protocol_type);
	}
	return ret;
}



ADAP_Int32 adap_ReconfigureHwVlan(tEnetHal_VlanId VlanId,ADAP_Uint32 u4ContextId)
{
	ADAP_Int32 ret = ENET_SUCCESS;

	tBridgeDomain *pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		if ((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_BRIDGE_MODE) &&
			(AdapIsVlanIpInterface(VlanId) == ENET_SUCCESS))
		{
			Adap_bridgeDomain_semUnlock();
			return ret;
		}
		else
		{
			ret = ADAP_ConfigVlanEntry(pBridgeDomain);
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by get bridge domain on VLAN:%d\r\n",VlanId);
		ret = ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();
	return ret;
}

ADAP_Int32
EnetHal_FsMiVlanHwAddVlanEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,tEnetHal_HwPortArray * pHwEgressPorts,tEnetHal_HwPortArray * pHwUnTagPorts)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (pHwEgressPorts);
	ADAP_UNUSED_PARAM (pHwUnTagPorts);

	ADAP_Int32 ret = ENET_SUCCESS;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Int8 		printMsg[100];
	ADAP_Int32		taggedPortIndex=0;
	ADAP_Int32     untaggedPortIndex=0;
	ADAP_Uint32		len=0;

	adap_memset(printMsg,0,sizeof(printMsg));

	/* ************* print VLAN configuration ******************/
	len += sprintf(&printMsg[len],"%s","Ports:");

    for(taggedPortIndex=0; taggedPortIndex < pHwEgressPorts->i4Length; taggedPortIndex++)
    {
		len += sprintf(&printMsg[len],"%d ",pHwEgressPorts->pu4PortArray[taggedPortIndex]);
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," %s\r\n", printMsg);

	len = 0;
	adap_memset(printMsg,0,sizeof(printMsg));
	len += sprintf(&printMsg[len],"%s","Untagged ports::");
    for(untaggedPortIndex=0; untaggedPortIndex < pHwUnTagPorts->i4Length; untaggedPortIndex++)
    {
		len += sprintf(&printMsg[len],"%d ",pHwUnTagPorts->pu4PortArray[untaggedPortIndex]);
    }
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," %s\r\n", printMsg);
	/* **************** end print VLAN configuration ************* */

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		// add IfIndex configuration
		pBridgeDomain->num_of_ports=0;
		adap_memset(&pBridgeDomain->ifIndex,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
		adap_memset(&pBridgeDomain->ifType,0,sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
		if(pHwEgressPorts->i4Length > 0)
		{
				for(taggedPortIndex=0; taggedPortIndex < pHwEgressPorts->i4Length; taggedPortIndex++)
				{
					pBridgeDomain->ifIndex[pBridgeDomain->num_of_ports] = pHwEgressPorts->pu4PortArray[taggedPortIndex];
					pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_TAGGED_MEMBER_PORT;

					 for(untaggedPortIndex=0; untaggedPortIndex < pHwUnTagPorts->i4Length; untaggedPortIndex++)
					 {

						 if(pHwUnTagPorts->pu4PortArray[untaggedPortIndex] == pHwEgressPorts->pu4PortArray[taggedPortIndex])
						 {
							 pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_UNTAGGED_MEMBER_PORT;
						 }
					 }
					 pBridgeDomain->num_of_ports++;
				}
		}
		if(pBridgeDomain->num_of_ports > 0)
		{
			ADAP_ConfigVlanEntry(pBridgeDomain);
		}
		else
		{
			// remove all services if needed
			ret = ADAP_DeleteVlanEntry(pBridgeDomain);
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database VlanId:%d u4ContextId:%d\r\n",VlanId,u4ContextId);
		ret = ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwDelVlanEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Int32 ret=0;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		ret = ADAP_DeleteVlanEntry(pBridgeDomain);
	}
	Adap_bridgeDomain_semUnlock();
    return ret;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetVlanMemberPort (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged)
{
	ADAP_Int32 			ret = ENET_SUCCESS;
	ADAP_Uint32			idx=0;
	tBridgeDomain 		*pBridgeDomain=NULL;
	ADAP_Bool			found=ADAP_FALSE;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( (pBridgeDomain->ifType[idx] != 0) && (pBridgeDomain->ifIndex[idx] == u4IfIndex) )
			{
				found=ADAP_TRUE;
			}
		}
		if(found == ADAP_FALSE)
		{
			if(u1IsTagged > 0)
			{
				pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_TAGGED_MEMBER_PORT;
			}
			else
			{
				pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_UNTAGGED_MEMBER_PORT;
			}
			pBridgeDomain->ifIndex[pBridgeDomain->num_of_ports] = u4IfIndex;
			pBridgeDomain->num_of_ports++;
			ret = ADAP_ConfigVlanEntry(pBridgeDomain);
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by get bridge domain on VLAN:%d\r\n",VlanId);
		ret = ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwResetVlanMemberPort(ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (u4IfIndex);

	ADAP_Int32 			ret = ENET_SUCCESS;
	ADAP_Uint32			idx=0;
	tBridgeDomain 		*pBridgeDomain=NULL;
	ADAP_Uint32	    	ifType[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32	    	ifIndex[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32			num_of_ports=0;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
		{
			if( (pBridgeDomain->ifType[idx] != 0) && (pBridgeDomain->ifIndex[idx] != u4IfIndex) )
			{
				ifType[num_of_ports] = pBridgeDomain->ifType[idx];
				ifIndex[num_of_ports] = pBridgeDomain->ifIndex[idx];
				num_of_ports++;
			}
		}

		adap_memset(pBridgeDomain->ifType, 0, sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);
		adap_memset(pBridgeDomain->ifIndex, 0, sizeof(ADAP_Uint32)*MAX_NUM_OF_SUPPORTED_PORTS);

		for(idx=0;idx<num_of_ports;idx++)
		{
			pBridgeDomain->ifType[idx] = ifType[idx];
			pBridgeDomain->ifIndex[idx] = ifIndex[idx];
		}

		pBridgeDomain->num_of_ports = num_of_ports;

		ret = ADAP_ConfigVlanEntry(pBridgeDomain);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by get bridge domain on VLAN:%d\r\n",VlanId);
		ret = ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();
	return ret;

}


ADAP_Int32 EnetHal_FsMiVlanHwSetPortPvid (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, tEnetHal_VlanId VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Int32 ret=ENET_SUCCESS;
	//ADAP_Uint32 idx=0;
	ADAP_Uint16 pvid=0;
	//ADAP_Bool		changed=ADAP_FALSE;
	//tServiceDb *pServiceInfo;
	//ADAP_Int32 ret_check=ENET_SUCCESS;
	tServiceDb *serviceDb=NULL;

	pvid = getPortToPvid(u4IfIndex);

	if(pvid != VlanId)
	{
		// check whether pvid configured on interface - if so delete
		if(pvid != ADAP_END_OF_TABLE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete older VLAN:%d ifIndex:%d\r\n",pvid,u4IfIndex);
			Adap_bridgeDomain_semLock();
			pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);
			if(pBridgeDomain != NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"check if need to remove untagged service\r\n");

				ADAP_DeleteIfIndexVlanEntry(pBridgeDomain,u4IfIndex,MEA_PARSING_L2_KEY_Untagged,pvid);
				serviceDb = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Untagged, u4IfIndex);
				if(serviceDb != NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"check if need from service DB serviceId:%d\r\n",serviceDb->serviceId);
					MeaDrvDeleteVlanBridging(serviceDb);
				}
			}
			Adap_bridgeDomain_semUnlock();
		}
		if(setPortToPvid(u4IfIndex,VlanId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to setPortToPvid: port%d\r\n",u4IfIndex);
			ret = ENET_FAILURE;
		}
		else
		{

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"get bridge domain VLAN:%d ifIndex:%d u4ContextId:%d\r\n",VlanId,u4IfIndex,u4ContextId);
			ret = adap_ReconfigureHwVlan (VlanId,u4ContextId);
		}
	}

    return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetDefaultVlanId (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	setDefaultVlanId(VlanId);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortAccFrameType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int8 u1AccFrameType)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint8							  u1OldAccFrameType;
	ADAP_Uint16 						  VlanId;
	ADAP_Uint32							  idx=0;
	tBridgeDomain 						  *pBridgeDomain=NULL;
	ADAP_Bool							  found=ADAP_FALSE;

	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is out of index\n", u4IfIndex);
		return ENET_SUCCESS;
	}

	AdapGetAccepFrameType(u4IfIndex,&u1OldAccFrameType);
	AdapSetAccepFrameType(u4IfIndex,u1AccFrameType);

	if((ADAP_Int8)u1OldAccFrameType != u1AccFrameType)
	{
		for(VlanId=1;VlanId<ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
		{
			Adap_bridgeDomain_semLock();
			pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

			if(pBridgeDomain != NULL)
			{
				for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
				{
					if( (pBridgeDomain->ifType[idx] != 0) && (pBridgeDomain->ifIndex[idx] == u4IfIndex) )
					{
						found=ADAP_TRUE;
					}
				}
				if(found == ADAP_FALSE)
				{
					if(ADAP_ConfigVlanEntry(pBridgeDomain) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPortAccFrameType: failed to reconfigure vlan:%d\r\n",VlanId);
						Adap_bridgeDomain_semUnlock();
						return ENET_FAILURE;
					}
				}
			}
			Adap_bridgeDomain_semUnlock();

		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetDefUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4DefPriority)
{
	ADAP_Uint32								i4DbDefPriority=0;
    ADAP_Uint16 							cosPriority = ADAP_END_OF_TABLE;
	MEA_FlowCoSMappingProfile_Entry_dbt 	entry_cos;
	ADAP_Uint16								idx;
    MEA_Port_t 								enetPort=0;
	tServiceDb 								*pServiceInfo=NULL;

	ADAP_UNUSED_PARAM (u4ContextId);

	if(AdapGetUserPriority(u4IfIndex, &i4DbDefPriority) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetDefUserPriority ERROR: AdapGetUserPriority for u4IfIndex %d failed\n", u4IfIndex);
		return ENET_FAILURE;
	}

	if((i4DbDefPriority != i4DefPriority) || (i4DefPriority == 0))
	{
		if(AdapSetUserPriority(u4IfIndex, i4DefPriority) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetDefUserPriority ERROR: AdapGetUserPriority for u4IfIndex %d failed\n", u4IfIndex);
			return ENET_FAILURE;
		}

		if(adap_GetPortCosPriority(u4IfIndex, &cosPriority) == ENET_FAILURE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetDefUserPriority ERROR: failed to get CosPriority from port:%d\n", u4IfIndex);
			return ENET_FAILURE;
		}

		if(cosPriority == ADAP_END_OF_TABLE)
		{
			adap_memset(&entry_cos,0,sizeof(entry_cos));

			for(idx=0;idx<ADAP_MAX_NUMBER_OF_VLAN_PRIORITY;idx++)
			{
				entry_cos.item_table[idx].valid = 1;
				entry_cos.item_table[idx].COS   = (MEA_Uint8)idx;
			}
			entry_cos.type = MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN;
			if(MEA_API_Create_FlowCoSMappingProfile_Entry(MEA_UNIT_0,&entry_cos,&cosPriority) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetDefUserPriority ERROR: failed to call MEA_API_Create_FlowCoSMappingProfile_Entry port:%d\n", u4IfIndex);
				return ENET_FAILURE;
			}
			if(adap_SetPortCosPriority(u4IfIndex, cosPriority) == ENET_FAILURE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetDefUserPriority ERROR: failed to set CosPriority:%d for port:%d\n", cosPriority, u4IfIndex);
				return ENET_FAILURE;
			}
		}

		if(adap_IsPortChannel(u4IfIndex) != ENET_SUCCESS)
		{
			if(MeaAdapGetPhyPortFromLogPort(u4IfIndex,&enetPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
				return ENET_FAILURE;
			}
			MeaAdapSetIngressPortDefaultPriority (enetPort, i4DefPriority);
		}

        // Apply i4DefPriority for all Services
        pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
        while (pServiceInfo)
        {

            if ((pServiceInfo->L2_protocol_type < D_PROVIDER_SERVICE_TAG) &&
                (pServiceInfo->valid == ADAP_TRUE) &&
                (pServiceInfo->serviceId != ADAP_END_OF_TABLE))
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId:%d\r\n",pServiceInfo->serviceId);
                if(MeaDrvUpdateUserPriority (u4IfIndex, pServiceInfo->serviceId, i4DefPriority) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to update service\r\n");
                    return ENET_FAILURE;
                }
            }

            pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
        }
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetRegenUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4UserPriority, ADAP_Int32 i4RegenPriority)
{
    MEA_FlowMarkingMappingProfile_Entry_dbt FlowMarkingMappingProfileEntry;
    ADAP_Bool                               NewProfile = ADAP_FALSE;
    ADAP_Uint16                             ItemTableIdx;
	ADAP_Uint32							   	IssMasterPortId;
	ADAP_Uint16								flowMarkProfile;
	tServiceDb 							    *pServiceInfo=NULL;

	ADAP_UNUSED_PARAM (u4ContextId);

	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is out of index\n", u4IfIndex);
		return ENET_FAILURE;
	}

	IssMasterPortId = u4IfIndex;
	if(adap_IsPortChannel(IssMasterPortId) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(IssMasterPortId) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is not port channel\n", u4IfIndex);
			return ENET_FAILURE;
		}
	}

	if(adap_GetFlowMarkingProfile(u4IfIndex, &flowMarkProfile) == ENET_FAILURE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: failed to get flowMarkProfile from port:%d\n", u4IfIndex);
		return ENET_FAILURE;
	}

    if( flowMarkProfile == ADAP_END_OF_TABLE)
    {
        /* Create a new Flow Marking profile */
        if(adap_AllocateFlowMarkingProfile (&flowMarkProfile, u4IfIndex) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: adap_AllocateFlowMarkingProfile fail for ISS port %d\n",
                    u4IfIndex);
            return ENET_FAILURE;
        }

        adap_memset(&FlowMarkingMappingProfileEntry, 0, sizeof(FlowMarkingMappingProfileEntry));
        FlowMarkingMappingProfileEntry.type = MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN;
        for (ItemTableIdx=0; ItemTableIdx < MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS; ItemTableIdx++)
        {
            FlowMarkingMappingProfileEntry.item_table[ItemTableIdx].valid=1;
            FlowMarkingMappingProfileEntry.item_table[ItemTableIdx].L2_PRI = ItemTableIdx;
        }
        if(MEA_API_Create_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, &FlowMarkingMappingProfileEntry, &flowMarkProfile) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: MEA_API_Create_FlowMarkingMappingProfile_Entry fail\n");
            return ENET_FAILURE;
        }
        NewProfile = ADAP_TRUE;
    }
    else
    {
        /* Update the existing Flow Marking profile */
        if(MEA_API_Get_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, flowMarkProfile, &FlowMarkingMappingProfileEntry) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: MEA_API_Get_FlowMarkingMappingProfile_Entry fail for profileId:%d\n",
            		flowMarkProfile);
            return ENET_FAILURE;
        }
    }

    /* Add/replace/Remove the priority translation entry */
    FlowMarkingMappingProfileEntry.item_table[i4UserPriority].L2_PRI = i4RegenPriority;

    /* Is there any active entries in the item table? */
    for(ItemTableIdx = 0; ItemTableIdx < MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS; ItemTableIdx++)
    {
        if(FlowMarkingMappingProfileEntry.item_table[ItemTableIdx].L2_PRI != ItemTableIdx)
        { /* There are active entries in the table - update the profile */
            /* Update the ENET with the new Flow Marking Profile */
           if(MEA_API_Set_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, flowMarkProfile, &FlowMarkingMappingProfileEntry) != MEA_OK)
           {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: MEA_API_Set_FlowMarkingMappingProfile_Entry fail for profileId:%d\n",
                		flowMarkProfile);
                return ENET_FAILURE;
           }

			if(adap_SetFlowMarkingProfile(u4IfIndex, flowMarkProfile) == ENET_FAILURE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: failed to set flowMarkProfile:%d for port:%d\n", flowMarkProfile, u4IfIndex);
				return ENET_FAILURE;
			}
           break;
        }
    }
    if(ItemTableIdx == MEA_FLOW_MARKING_MAPPING_MAX_NUM_OF_ITEMS)
    {/* There is no any active entry in the table */
        /* Release the Flow Marking Profile */
        /* For all Services of the u4IfIndex port:
         * Detach the Flow Marking Profile to the service */
        pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
    	while(pServiceInfo != NULL)
    	{
    		if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
    		{
				if(MeaDrvSetFlowMarkingProfile (u4IfIndex, pServiceInfo->serviceId, flowMarkProfile) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set flowMarkProfile:%d to service:%d\r\n", flowMarkProfile, pServiceInfo->serviceId);
					return ENET_FAILURE;
				}
    		}
    		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
        }

        /* Release the Flow Marking Profile in DPL*/
        switch(adap_FreeFlowMarkingProfile (flowMarkProfile))
        {
            case MEA_OK:
                /*  The Flow Marking Profile is not in use any more and may be safely released in HW */
                if(MEA_API_Delete_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, flowMarkProfile) != MEA_OK)
                {
                    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetRegenUserPriority ERROR: MEA_API_Delete_FlowMarkingMappingProfile_Entry fail!\n");
                    return ENET_FAILURE;
                }
            break;
            case MEA_BUSY:
                /* The Flow Marking Profile is still in use somewhere else. */
            break;
            case MEA_ERROR:
            default:
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetRegenUserPriority ERROR: DPLFreeFlowMarkingProfile fail!\n");
                return ENET_FAILURE;
        }
        return ENET_SUCCESS;
    }

    /* Update the ENET with the new Flow Marking Profile */
    if(NewProfile)
    {
        /* For all Services of the u4IfIndex port:
         * Attach the Flow Marking Profile to the service */
        pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
    	while(pServiceInfo != NULL)
    	{
    		if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
    		{
				if(MeaDrvSetFlowMarkingProfile (u4IfIndex, pServiceInfo->serviceId, MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set flowMarkProfile:%d to service:%d\r\n", flowMarkProfile, pServiceInfo->serviceId);
					return ENET_FAILURE;
				}
    		}
    		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
        }
    }

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetTraffClassMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4UserPriority, ADAP_Int32 i4TraffClass)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Uint16 						cosPriProfileId = ADAP_END_OF_TABLE;
	MEA_FlowCoSMappingProfile_Entry_dbt entry_cos;
	ADAP_Uint16							idx=0;

	adap_memset(&entry_cos,0,sizeof(entry_cos));

	if(adap_GetPortCosPriority(u4IfIndex, &cosPriProfileId) == ENET_FAILURE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to get CosPriority from port:%d\n", u4IfIndex);
		return ENET_FAILURE;
	}

	if(cosPriProfileId == ADAP_END_OF_TABLE)
	{
		for(idx=0;idx<ADAP_MAX_NUMBER_OF_VLAN_PRIORITY;idx++)
		{
			entry_cos.item_table[idx].valid = 1;
			entry_cos.item_table[idx].COS   = (MEA_Uint8)idx;
		}
		entry_cos.type = MEA_FLOW_COS_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN;
		entry_cos.item_table[i4UserPriority].valid = 1;
		entry_cos.item_table[i4UserPriority].COS   = (MEA_Uint8)i4TraffClass;
		if(MEA_API_Create_FlowCoSMappingProfile_Entry(MEA_UNIT_0,&entry_cos,&cosPriProfileId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to call MEA_API_Create_FlowCoSMappingProfile_Entry port:%d\n", u4IfIndex);
			return ENET_FAILURE;
		}
		if(adap_SetPortCosPriority(u4IfIndex, cosPriProfileId) == ENET_FAILURE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to set CosPriority:%d for port:%d\n", cosPriProfileId, u4IfIndex);
			return ENET_FAILURE;
		}
	}
	else
	{
		if(MEA_API_Get_FlowCoSMappingProfile_Entry(MEA_UNIT_0,cosPriProfileId,&entry_cos) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to call MEA_API_Get_FlowCoSMappingProfile_Entry id:%d\n", cosPriProfileId);
			return ENET_FAILURE;
		}
		entry_cos.item_table[i4UserPriority].valid = 1;
		entry_cos.item_table[i4UserPriority].COS   = (MEA_Uint8)i4TraffClass;
		if(MEA_API_Set_FlowCoSMappingProfile_Entry(MEA_UNIT_0,cosPriProfileId,&entry_cos) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to call MEA_API_Set_FlowCoSMappingProfile_Entry id:%d\n", cosPriProfileId);
			return ENET_FAILURE;
		}
		if(adap_SetPortCosPriority(u4IfIndex, cosPriProfileId) == ENET_FAILURE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTraffClassMap ERROR: failed to set CosPriority:%d for port:%d\n", cosPriProfileId, u4IfIndex);
			return ENET_FAILURE;
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGmrpEnable (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    /* Create the LxCP entry */
	if (MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_GMRP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGmrpEnable Failed on MeaDrvVlanHwLxcpUpdate\n" );
 		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGmrpDisable (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    /* Create the LxCP entry */
	if (MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_GMRP_PROTO_ID, MEA_FALSE) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGmrpEnable Failed on MeaDrvVlanHwLxcpUpdate\n" );
 		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwGvrpEnable (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    /* Create the LxCP entry */
	if (MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_GVRP_PROTO_ID, MEA_TRUE) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGmrpEnable Failed on MeaDrvVlanHwLxcpUpdate\n" );
 		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwGvrpDisable (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    /* Create the LxCP entry */
	if (MeaDrvVlanHwLxcpUpdate(ENETHAL_VLAN_NP_GVRP_PROTO_ID, MEA_FALSE) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwGmrpEnable Failed on MeaDrvVlanHwLxcpUpdate\n" );
 		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanBlockTrafficExceptLacp (ADAP_Uint32 u4IfIndex, ADAP_Bool enableBlocking)
{
	tServiceDb 		*pServiceInfo=NULL;
	MEA_LxCp_t   LxCp_Id;

    if(enableBlocking == ADAP_TRUE)
    {
    	//Delete all services related to the given interface except of default service
        pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
    	while(pServiceInfo != NULL)
    	{
    		if ((pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID) && (pServiceInfo->L2_protocol_type != MEA_PARSING_L2_KEY_DEF_SID))
    		{
    			if(MeaDeleteEnetServiceId(pServiceInfo->serviceId) != ENET_SUCCESS)
    			{
    		        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanBlockTrafficExceptLacp Failed on MeaDeleteEnetServiceId for serviceId:%d\n", pServiceInfo->serviceId);
    		 		return ENET_FAILURE;
    			}

    			if(getLxcpId(u4IfIndex,&LxCp_Id) != ENET_SUCCESS)
    			{
    				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
    				return ENET_FAILURE;
    			}
    			MeaDrvVlanHwLacpLxcpUpdate (MEA_TRUE,LxCp_Id);
    		}
    		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
        }
    }
    else
    {
    	//services per interface can be created again
    }

	adap_SetTrafficBlockStatusPerPort(u4IfIndex, enableBlocking);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiNpDeleteAllFdbEntries (ADAP_Uint32 u4ContextId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	ADAP_Uint32				CurrentAgingTime;

	CurrentAgingTime = adap_GetAgingTime();

	if (EnetHal_FsBrgSetAgingTime(5) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiNpDeleteAllFdbEntries Failed on EnetHal_FsBrgSetAgingTime reset \n" );
		return ENET_FAILURE;
    }

	sleep(5);

	if (EnetHal_FsBrgSetAgingTime(CurrentAgingTime) != ENET_SUCCESS)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiNpDeleteAllFdbEntries Failed on EnetHal_FsBrgSetAgingTime  restore time\n" );
 		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwAddVlanProtocolMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint32 u4GroupId,
		tEnetHal_VlanProtoTemplate * pProtoTemplate, tEnetHal_VlanId VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4GroupId);
	ADAP_UNUSED_PARAM (VlanId);

	ADAP_Uint32 index;
	ADAP_Uint32 ArrPorts[MAX_NUM_OF_SUPPORTED_PORTS];
	ADAP_Uint32 numOfPorts=0;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"enter for ifIndex:%d\n",u4IfIndex);

     for(index=1;index<MeaAdapGetNumOfPhyPorts();index++)
    {
    	if(index != u4IfIndex)
    	{
    		ArrPorts[numOfPorts] = index;
    		numOfPorts++;
    	}
    }

    if(pProtoTemplate->u1TemplateProtoFrameType == ENET_VLAN_PORT_PROTO_ETHERTYPE)
    {
    	if(pProtoTemplate->u1Length > 1)
    	{
    		if( (pProtoTemplate->au1ProtoValue[0] == 0x08) && (pProtoTemplate->au1ProtoValue[1] == 0x06) )
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create ARP LXCP\n");
    			EnetAdapLxcpAction(u4IfIndex, ArrPorts, numOfPorts, ADAP_TRUE,MEA_LXCP_PROTOCOL_ARP, MEA_LXCP_PROTOCOL_ACTION_ACTION);
    		}
    		if( (pProtoTemplate->au1ProtoValue[0] == 0x08) && (pProtoTemplate->au1ProtoValue[1] == 0x00) )
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create IPV4 LXCP\n");
    			EnetAdapLxcpAction(u4IfIndex, ArrPorts, numOfPorts, ADAP_TRUE,MEA_LXCP_PROTOCOL_IPV4, MEA_LXCP_PROTOCOL_ACTION_ACTION);
    		}
    	}
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwDelVlanProtocolMap (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
												ADAP_Uint32 u4GroupId,tEnetHal_VlanProtoTemplate * pProtoTemplate)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u4GroupId);
	ADAP_Uint32 ArrPorts[MAX_NUM_OF_SUPPORTED_PORTS];

    adap_memset(ArrPorts, 0, sizeof (ArrPorts[MAX_NUM_OF_SUPPORTED_PORTS]));

    if(pProtoTemplate->u1TemplateProtoFrameType == ENET_VLAN_PORT_PROTO_ETHERTYPE)
    {
    	if(pProtoTemplate->u1Length > 1)
    	{
    		if( (pProtoTemplate->au1ProtoValue[0] == 0x08) && (pProtoTemplate->au1ProtoValue[1] == 0x06) )
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create ARP LXCP\n");
    			EnetAdapLxcpAction(u4IfIndex, ArrPorts, 0, ADAP_TRUE,MEA_LXCP_PROTOCOL_ARP, MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT);
    		}
    		if( (pProtoTemplate->au1ProtoValue[0] == 0x08) && (pProtoTemplate->au1ProtoValue[1] == 0x00) )
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create IPV4 LXCP\n");
    			EnetAdapLxcpAction(u4IfIndex, ArrPorts, 0, ADAP_TRUE,MEA_LXCP_PROTOCOL_IPV4, MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT);
    		}
    	}
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGetPortStats (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, tEnetHal_VlanId VlanId,
		ADAP_Uint8 u1StatsType, ADAP_Uint32 *pu4PortStatsValue)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	MEA_Counters_PM_dbt         MEA_Stats;
    MEA_PmId_t                  PmId = MEA_PLAT_GENERATE_NEW_ID;
	tServiceDb 					*pServiceId=NULL;

    MEA_uint64                  sum_green_fwd_pkts = 0;
    MEA_uint64                  sum_green_dis_pkts = 0;
    MEA_uint64                  sum_yellow_fwd_pkts = 0;
    MEA_uint64                  sum_yellow_dis_pkts = 0;
    MEA_uint64                  sum_red_dis_pkts = 0;
    MEA_uint64                  sum_other_dis_pkts = 0;
    ADAP_Uint16					protocolType;
	MEA_Bool					valid=MEA_FALSE;

     *pu4PortStatsValue = 0;

     for(protocolType = MEA_PARSING_L2_KEY_Untagged; protocolType <= MEA_PARSING_L2_KEY_Ctag; protocolType++)
     {
		 pServiceId = MeaAdapGetServiceIdByEditType(protocolType,u4Port);
		 if(pServiceId != NULL)
		 {
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,pServiceId->serviceId,&valid) == MEA_OK)
			{
				if(valid == MEA_TRUE)
				{
					PmId = pServiceId->pmId;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"pmId %d\n",PmId);
				}
			}

			if(PmId != MEA_PLAT_GENERATE_NEW_ID)
			{
				if (MEA_API_Get_Counters_PM(MEA_UNIT_0, PmId, &MEA_Stats) != MEA_OK)
				{
	//				return (FNP_FAILURE);
				}
			}
			sum_green_fwd_pkts += MEA_Stats.green_fwd_pkts.val;
			sum_green_dis_pkts += MEA_Stats.green_dis_pkts.val;
			sum_yellow_fwd_pkts += MEA_Stats.yellow_fwd_pkts.val;
			sum_yellow_dis_pkts += MEA_Stats.yellow_dis_pkts.val;
			sum_red_dis_pkts += MEA_Stats.red_dis_pkts.val;
			sum_other_dis_pkts += MEA_Stats.other_dis_pkts.val;
		}
     }

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Vlan %d Port %d Statistics:\n"
		   "green_fwd_pkts:%lld\n"
		   "green_dis_pkts:%lld\n"
		   "yellow_fwd_pkts:%lld\n"
		   "yellow_dis_pkts:%lld\n"
		   "red_dis_pkts:%lld\n"
		   "other_dis_pkts:%lld\n",
			VlanId, u4Port,
			sum_green_fwd_pkts,
			sum_green_dis_pkts,
			sum_yellow_fwd_pkts,
			sum_yellow_fwd_pkts,
			sum_red_dis_pkts,
			sum_red_dis_pkts);

	/* Fill the statistics*/
	switch(u1StatsType)
	{
		case ENET_VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS:
		{
			*pu4PortStatsValue = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_IN_FRAMES:
		{
			*pu4PortStatsValue = MEA_Stats.green_fwd_pkts.val + MEA_Stats.green_dis_pkts.val + MEA_Stats.yellow_fwd_pkts.val + MEA_Stats.yellow_dis_pkts.val + MEA_Stats.red_dis_pkts.val + MEA_Stats.other_dis_pkts.val;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_IN_FRAMES statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_OUT_FRAMES:
		{
			*pu4PortStatsValue = MEA_Stats.green_fwd_pkts.val + MEA_Stats.yellow_fwd_pkts.val;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_OUT_FRAMES statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS:
		{
			*pu4PortStatsValue = MEA_Stats.green_dis_pkts.val + MEA_Stats.yellow_dis_pkts.val + MEA_Stats.red_dis_pkts.val + MEA_Stats.other_dis_pkts.val;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_IN_OVERFLOW:
		{
			*pu4PortStatsValue = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_IN_OVERFLOW statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_OUT_OVERFLOW:
		{
			*pu4PortStatsValue = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_OUT_OVERFLOW statistic type:%d \n", u1StatsType);
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW:
		{
			*pu4PortStatsValue = 0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW statistic type:%d \n", u1StatsType);
		}
		break;
		default:
		{
			*pu4PortStatsValue = 0;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Port Statistics for type:%d - %d\n", u1StatsType, *pu4PortStatsValue);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGetPortStats64 (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, tEnetHal_VlanId VlanId,
		ADAP_Uint8 u1StatsType, tEnetHal_COUNTER64_TYPE * pValue)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4Port);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (u1StatsType);

    adap_memset(pValue, 0, sizeof (tEnetHal_COUNTER64_TYPE));

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGetVlanStats (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Uint8 u1StatsType, ADAP_Uint32 *pu4VlanStatsValue)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    MEA_Counters_PM_dbt						MEA_Stats;
    MEA_Counter_t         					sum_green_fwd_pkts;
    MEA_Counter_t         					sum_green_dis_pkts;
    MEA_Counter_t        					sum_yellow_fwd_pkts;
    MEA_Counter_t        					sum_yellow_dis_pkts;
    MEA_Counter_t         					sum_red_dis_pkts;
    MEA_Counter_t   						sum_other_dis_pkts;
    MEA_PmId_t 								PmId;
	tBridgeDomain							*pBridgeDomain=NULL;
	ADAP_Int32 								ret_check=ENET_SUCCESS;
	tServiceDb 								*pServiceInfo=NULL;
	ADAP_Uint32 							LogPort = 0;

    *pu4VlanStatsValue = 0;
    adap_memset(&sum_green_fwd_pkts , 0 , sizeof(sum_green_fwd_pkts ));
    adap_memset(&sum_green_dis_pkts , 0 , sizeof(sum_green_dis_pkts ));
    adap_memset(&sum_yellow_fwd_pkts , 0 , sizeof(sum_yellow_fwd_pkts ));
    adap_memset(&sum_yellow_dis_pkts , 0 , sizeof(sum_yellow_dis_pkts ));
    adap_memset(&sum_red_dis_pkts , 0 , sizeof(sum_red_dis_pkts ));
    adap_memset(&sum_other_dis_pkts , 0 , sizeof(sum_other_dis_pkts ));

 	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);

	if(pBridgeDomain != NULL)
	{
		ret_check = Adap_getFirstServiceFromLinkList(&pServiceInfo,pBridgeDomain);

		while( (ret_check == ENET_SUCCESS) && (pServiceInfo != NULL) )
		{
			if( (pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Untagged && (pServiceInfo->vpnId == (MEA_Uint32) adap_GetVpnByVlan(VlanId))) ||
				(pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Ctag && (pServiceInfo->outerVlan == VlanId)) ||
				(pServiceInfo->L2_protocol_type != MEA_PARSING_L2_KEY_Ctag && (pServiceInfo->outerVlan == VlanId || pServiceInfo->innerVlan == VlanId)) )
			{
				PmId = pServiceInfo->pmId;

				if((PmId != MEA_PLAT_GENERATE_NEW_ID) && (MeaAdapGetLogPortFromPhyPort(&LogPort, pServiceInfo->inPort) == ENET_SUCCESS))
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId:%d pmId:%d\r\n",pServiceInfo->serviceId, pServiceInfo->pmId);

					if (MEA_API_Get_Counters_PM(MEA_UNIT_0, PmId, &MEA_Stats) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Can't get PM Counters for pmId:%d!\n", pServiceInfo->pmId);
						Adap_bridgeDomain_semUnlock();
						return ENET_FAILURE;
					}

					/* Sum the counters */
					sum_green_fwd_pkts.val += MEA_Stats.green_fwd_pkts.val;
					sum_green_dis_pkts.val +=  MEA_Stats.green_dis_pkts.val;
					sum_yellow_fwd_pkts.val += MEA_Stats.yellow_fwd_pkts.val;
					sum_yellow_dis_pkts.val += MEA_Stats.yellow_dis_pkts.val;
					sum_red_dis_pkts.val += MEA_Stats.red_dis_pkts.val;
					sum_other_dis_pkts.val += MEA_Stats.other_dis_pkts.val;
				}
			}
			ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
		}
	}
	Adap_bridgeDomain_semUnlock();

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Vlan %d Statistics:\n"
		   "green_fwd_pkts:%lld\n"
		   "green_dis_pkts:%lld\n"
		   "yellow_fwd_pkts:%lld\n"
		   "yellow_dis_pkts:%lld\n"
		   "red_dis_pkts:%lld\n"
		   "other_dis_pkts:%lld\n",
			VlanId,
			sum_green_fwd_pkts.val,
			sum_green_dis_pkts.val,
			sum_yellow_fwd_pkts.val,
			sum_yellow_dis_pkts.val,
			sum_red_dis_pkts.val,
			sum_other_dis_pkts.val);

	/* Fill the statistics*/
	switch(u1StatsType)
	{
		case ENET_VLAN_STAT_VLAN_PORT_IN_FRAMES:
       case ENET_NP_STAT_VLAN_UCAST_IN_FRAMES:
		{
			*pu4VlanStatsValue = sum_green_fwd_pkts.val + sum_green_dis_pkts.val + sum_yellow_fwd_pkts.val + sum_yellow_dis_pkts.val + sum_red_dis_pkts.val + sum_other_dis_pkts.val;
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_OUT_FRAMES:
       case ENET_NP_STAT_VLAN_UCAST_OUT_FRAMES:
		{
			*pu4VlanStatsValue = sum_green_fwd_pkts.val + sum_yellow_fwd_pkts.val;
		}
		break;
		case ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS:
		case ENET_NP_STAT_IF_HC_IN_DISCARDS:
		{
			*pu4VlanStatsValue = sum_green_dis_pkts.val + sum_yellow_dis_pkts.val + sum_red_dis_pkts.val + sum_other_dis_pkts.val;
		}
		break;
   		case ENET_VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS:
		case ENET_VLAN_STAT_VLAN_PORT_IN_OVERFLOW:
		case ENET_VLAN_STAT_VLAN_PORT_OUT_OVERFLOW:
		case ENET_VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW:
       case ENET_NP_STAT_VLAN_MCAST_BCAST_IN_FRAMES:
       case ENET_NP_STAT_VLAN_UNKNOWN_UCAST_OUT_FRAMES:
       case ENET_NP_STAT_VLAN_BCAST_OUT_FRAMES:
       case ENET_NP_STAT_IF_HC_IN_ERRORS:
       case ENET_NP_STAT_IF_HC_IN_UNKNOWN_PROTOS:
       case ENET_NP_STAT_IF_HC_OUT_DISCARDS:
       case ENET_NP_STAT_IF_HC_IN_OUT_ERRORS:
       case ENET_NP_STAT_ETHER_OUT_FCS_ERRORS:
		{
			*pu4VlanStatsValue = 0;
		}
		break;
		default:
		{
			*pu4VlanStatsValue = 0;
           ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unrecognized statistic type:%d \n", u1StatsType);
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Vlan Statistics for type:%d - %d\n", u1StatsType, *pu4VlanStatsValue);

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwResetVlanStats (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    MEA_PmId_t 					PmId;
	tBridgeDomain				*pBridgeDomain=NULL;
	ADAP_Uint32					ret_check=ENET_SUCCESS;
	tServiceDb 					*pServiceInfo=NULL;

  	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,0);
	if(pBridgeDomain != NULL)
	{
		ret_check = Adap_getFirstServiceFromLinkList(&pServiceInfo,pBridgeDomain);

		while( (ret_check == ENET_SUCCESS) && (pServiceInfo != NULL) )
		{
			if( (pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Untagged && (pServiceInfo->outerVlan == VlanId)) ||
				(pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Ctag && (pServiceInfo->outerVlan == VlanId)) ||
				(pServiceInfo->L2_protocol_type != MEA_PARSING_L2_KEY_Ctag && (pServiceInfo->outerVlan == VlanId || pServiceInfo->innerVlan == VlanId)) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId:%d pmId:%d\r\n",pServiceInfo->serviceId, pServiceInfo->pmId);

				PmId = pServiceInfo->pmId;

				if(PmId != MEA_PLAT_GENERATE_NEW_ID)
				{
					if (MEA_API_Clear_Counters_PM(MEA_UNIT_0, PmId) != MEA_OK)
					{
						continue;
					}
				}
				else
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"PM Id for Vlan:%d with ServiceId:%d is not exist!\n", VlanId, pServiceInfo->serviceId);
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}
			}
			ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
		}
	}

	Adap_bridgeDomain_semUnlock();

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortTunnelMode (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Mode)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	if(AdapSetProviderBridgeMode(u4Mode,u4IfIndex) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set PB mode for ifIndex:%d\r\n",u4IfIndex);
		return MEA_ERROR;
	}

	if(adap_ReconfigureHwVlan(getPortToPvid(u4IfIndex), 0) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPortTunnelMode: failed to reconfigure vlan:%d\r\n",getPortToPvid(u4IfIndex));
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetTunnelFilter (ADAP_Uint32 u4ContextId, ADAP_Int32 i4BridgeMode)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    if(i4BridgeMode == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        /* Add the Customer->Trunk port translation (Tunnel In) */
        /* For all VLANS: */
        /* Allocate the LxCp for the VLAN MAC translation */
        /* For all ports in VLAN: */
        /* If the port is of VLAN_TRUNK_PORT type*/
        /* Add the port to the flooding domain lists */
        /* Else the port is of VLAN_ACCESS_PORT type*/
        /* Add the port to the source ports list */
        /* Create the action which translates: */
        // STP D-MAC -> gVlanProviderStpAddr
        // GMRP D-MAC -> gVlanProviderGmrpAddr
        // GVRP D-MAC -> gVlanProviderGvrpAddr
        /* Commit the action */
        /* Commit the LxCp */
        /* Attach the LxCp to all the ports in the source ports list */

        if (MeaDrvAddTunnelInTranslation() != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelFilter FAIL: FsMiVlanHwAddTunnelInTranslation failed!\n");
            return ENET_FAILURE;
        }

        /* Add the Trunk->Customer port translation (Tunnel Out) */
        if (MeaDrvAddTunnelOutTranslation() != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelFilter FAIL: FsMiVlanHwAddTunnelOutTranslation failed!\n");
            return ENET_FAILURE;
        }
        adap_SetTunnelTranslation(ADAP_TRUE);
    }
    else
    {
        /* Remove the Customer->Trunk port translation (Tunnel In) */
//        if (MeaDrvRemoveTunnelInTranslation() != ENET_SUCCESS)
//        {
//            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelFilter FAIL: FsMiVlanHwRemoveTunnelInTranslation failed!\n");
//            return ENET_FAILURE;
//        }
        /* Remove the the Trunk->Customer port translation (Tunnel Out) */
        if (MeaDrvRemoveTunnelOutTranslation() != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelFilter FAIL: FsMiVlanHwRemoveTunnelOutTranslation failed!\n");
            return ENET_FAILURE;
        }
        adap_SetTunnelTranslation(ADAP_FALSE);
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwCreateFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4Fid);
	ADAP_Int32 ret = ENET_SUCCESS;

	ret = Adap_allocateBridgeDomain(u4Fid,u4ContextId);

	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call allocateBridgeDomain\r\n");
	}

    return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwDeleteFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid)
{
	tBridgeDomain *pBridgeDomain=NULL;

	Adap_bridgeDomain_semLock();

	pBridgeDomain = Adap_getBridgeDomain(u4Fid,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE1,"bridge domain VLAN Id:%d is going to deleted\r\n",pBridgeDomain->vlanId);

		// remove services if still exist
		if (ADAP_DeleteVlanEntry(pBridgeDomain) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete services on bridge domain\r\n");
			return ENET_FAILURE;
		}

		if (Adap_freeBridgeDomain(u4Fid,u4ContextId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete bridge domain\r\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"bridge domain not found\r\n");
		return ENET_FAILURE;
	}

	Adap_bridgeDomain_semUnlock();

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwAssociateVlanFdb (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_VlanId VlanId)
{
	ADAP_Int32 ret = ENET_SUCCESS;
	tBridgeDomain *pBridgeDomain=NULL;

	pBridgeDomain = Adap_getBridgeDomain(u4Fid,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		pBridgeDomain->vlanId = VlanId;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE1,"bridge domain associate  VLAN Id:%d\r\n",pBridgeDomain->vlanId);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"bridge domain not found\r\n");
		ret = ENET_FAILURE;
	}

    return ret;
}
ADAP_Int32 EnetHal_FsMiVlanHwDisassociateVlanFdb (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid, tEnetHal_VlanId VlanId)
{

	ADAP_UNUSED_PARAM (VlanId);

	ADAP_Int32 ret = ENET_SUCCESS;
	tBridgeDomain *pBridgeDomain=NULL;


	pBridgeDomain = Adap_getBridgeDomain(u4Fid,u4ContextId);

	if(pBridgeDomain != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE1,"bridge domain dissociate  VLAN Id:%d\r\n",pBridgeDomain->vlanId);
		pBridgeDomain->vlanId = 0;
		// remove services if needed
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"bridge domain not found\r\n");
		ret = ENET_FAILURE;
	}

    return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwFlushPortFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Uint32 u4Fid,ADAP_Int32 i4OptimizeFlag)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (i4OptimizeFlag);

    MEA_Port_t               	EnetPortId;
    ADAP_Uint16					masterPort=0;
	tBridgeDomain 				*pBridgeDomain=NULL;
	ADAP_Uint16 				VpnIndex;

 	masterPort = u4Port;
	if(adap_IsPortChannel(u4Port) == ENET_SUCCESS)
	{
		masterPort = adap_GetLagMasterPort(u4Port);
	}
    /* Convert u4IfIndex to the ENET portId */
	if(MeaAdapGetPhyPortFromLogPort(masterPort,&EnetPortId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",masterPort);
		return ENET_FAILURE;
	}

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByFdb(u4Fid);

	if(pBridgeDomain != NULL)
	{
		VpnIndex = pBridgeDomain->vpn;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get bridge database\r\n");
		Adap_bridgeDomain_semUnlock();
		return ENET_FAILURE;
	}
	Adap_bridgeDomain_semUnlock();

	if(MEA_API_FWD_BrgFlushPortFid (EnetPortId , VpnIndex, MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed MEA_API_FWD_BrgFlushPortFid for EnetPortId:%d \r\n",EnetPortId, VpnIndex);
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwFlushPort (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Int32 i4OptimizeFlag)
{
	ADAP_Int32	ret_check = ENET_SUCCESS;
	tBridgeDomain *pBridgeDomain = NULL;

	ret_check = Adap_getFirstBridgeFromLinkList(&pBridgeDomain);

	while((ret_check == ENET_SUCCESS) && (pBridgeDomain != NULL))
	{
		EnetHal_FsMiVlanHwFlushPortFdbId(u4ContextId, u4IfIndex, pBridgeDomain->swFdb, i4OptimizeFlag);

		ret_check = Adap_getNextBridgeFromLinkList(&pBridgeDomain);
	}

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwFlushFdbId (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Fid)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4Fid);
	ADAP_Uint32	CurrentAgingTime=0;

	CurrentAgingTime = adap_GetAgingTime();

	if (EnetHal_FsBrgSetAgingTime(3) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwFlushFdbId Failed on EnetHal_FsBrgSetAgingTime reset \n" );
		return ENET_FAILURE;
	}

	sleep(3);

	if (EnetHal_FsBrgSetAgingTime(CurrentAgingTime) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwFlushFdbId Failed on EnetHal_FsBrgSetAgingTime reset \n" );
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetShortAgeout (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Int32 i4AgingTime)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    if (EnetHal_FsBrgSetAgingTime(i4AgingTime) != ENET_SUCCESS)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error on EnetHal_FsBrgSetAgingTime\n");
		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwResetShortAgeout (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4Port, ADAP_Int32 i4LongAgeout)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    if (EnetHal_FsBrgSetAgingTime(i4LongAgeout) != ENET_SUCCESS)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error on EnetHal_FsBrgSetAgingTime\n");
		return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwGetMcastEntry (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, tEnetHal_MacAddr *MacAddr,tEnetHal_HwPortArray * pHwMcastPorts)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);

	tMulticastMac 							*multicastEntry=NULL;
	ADAP_Uint16								issPort;

	multicastEntry = adap_getMulticastMacEntry(MacAddr);
	if(multicastEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Multicast entry is not found!\n");
		return ENET_FAILURE;
	}

    /* Get the  Port list */
    pHwMcastPorts->i4Length = 0;


	for(issPort=1;issPort<MeaAdapGetNumOfPhyPorts();issPort++)
	{
		if(multicastEntry->portbitmap & (1 << issPort) )
		{
			pHwMcastPorts->pu4PortArray[pHwMcastPorts->i4Length++] = issPort;
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetBrgMode (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4BridgeMode)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	AdapSetBridgeMode(u4BridgeMode);

    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetBaseBridgeMode (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4Mode)
{
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u4Mode);


    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwAddPortMacVlanEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
                               tEnetHal_MacAddr *MacAddr, tEnetHal_VlanId VlanId,ADAP_Int8 bSuppressOption)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    MEA_Service_t                         Service_id;
    tEnetHal_VlanId						  vlan=0;
	MEA_Filter_t 						  FilterId;
	tServiceDb 							  *pServiceId=NULL;
	MEA_Bool							  valid=MEA_FALSE;

    MeaAdapSetIngressInterfaceType (u4IfIndex,MEA_FILTER_KEY_INTERFACE_TYPE_SID);

	// get untagged traffic
	pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Untagged,u4IfIndex);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddPortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}

		if(valid == MEA_TRUE)
		{
			if(MeaDrvPortMacSetFilterMask(Service_id,u4IfIndex,1,VlanId,MacAddr,MEA_EGRESS_HEADER_PROC_CMD_APPEND,&FilterId, MEA_PARSING_L2_KEY_Untagged) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create whitelist filter  for service:%d\n",Service_id);
				return ENET_FAILURE;
			}
			adap_InsertEntryToFilterList(FilterId,Service_id,&MacAddr[0]);
		}
	}
	// get priority tag
	pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Ctag, u4IfIndex);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddPortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}

		if(valid == MEA_TRUE)
		{
			if(MeaDrvPortMacSetFilterMask(Service_id,u4IfIndex,0,VlanId,MacAddr,MEA_EGRESS_HEADER_PROC_CMD_SWAP,&FilterId, MEA_PARSING_L2_KEY_Ctag) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create whitelist filter  for service:%d\n",Service_id);
				return ENET_FAILURE;
			}
			adap_InsertEntryToFilterList(FilterId,Service_id,&MacAddr[0]);
		}
	}

	for(vlan=2;vlan<ADAP_NUM_OF_SUPPORTED_VLANS;vlan++)
	{
		pServiceId = MeaAdapGetServiceIdByVlanIfIndex(vlan,u4IfIndex,MEA_PARSING_L2_KEY_Ctag);
		if(pServiceId != NULL)
		{
			Service_id = pServiceId->serviceId;
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddPortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
				return ENET_FAILURE;
			}

			if(valid == MEA_TRUE)
			{
				if(MeaDrvPortMacSetFilterMask(Service_id,u4IfIndex,vlan,vlan,MacAddr,MEA_EGRESS_HEADER_PROC_CMD_SWAP,&FilterId, MEA_PARSING_L2_KEY_Ctag) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create whitelist filter  for service:%d\n",Service_id);
					return ENET_FAILURE;
				}
				adap_InsertEntryToFilterList(FilterId,Service_id,&MacAddr[0]);
			}
		}
		else
		{
			pServiceId = MeaAdapGetServiceIdByVlanIfIndex(vlan,u4IfIndex,MEA_PARSING_L2_KEY_Untagged);
			if(pServiceId != NULL)
			{
				Service_id = pServiceId->serviceId;
				if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddPortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
					return ENET_FAILURE;
				}

				if(valid == MEA_TRUE)
				{
					if(MeaDrvPortMacSetFilterMask(Service_id,u4IfIndex,vlan,vlan,MacAddr,MEA_EGRESS_HEADER_PROC_CMD_APPEND,&FilterId, MEA_PARSING_L2_KEY_Untagged) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create whitelist filter  for service:%d\n",Service_id);
						return ENET_FAILURE;
					}
					adap_InsertEntryToFilterList(FilterId,Service_id,&MacAddr[0]);
				}
			}
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwDeletePortMacVlanEntry(ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_MacAddr *MacAddr)
{

	ADAP_UNUSED_PARAM (u4ContextId);
    MEA_Service_t                         Service_id;
    tEnetHal_VlanId						  vlan=0;
	MEA_Filter_t 						  FilterId;
	tServiceDb 							  *pServiceId=NULL;
	MEA_Bool							  valid=MEA_FALSE;

	// get untagged traffic
	pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Untagged,u4IfIndex);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeletePortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}

		if(valid == MEA_TRUE)
		{
			FilterId = adap_DeleteEntryFromFilterListMacAddr(Service_id,MacAddr);
			if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
			{
				if(MeaDrvPortMacDeleteFilterMask(Service_id,FilterId)!= ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete whitelist filter  for service:%d\n",Service_id);
					return ENET_FAILURE;
				}
			}
		}
	}
	// get priority tag
	pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Ctag, u4IfIndex);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeletePortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}

		if(valid == MEA_TRUE)
		{
			FilterId = adap_DeleteEntryFromFilterListMacAddr(Service_id,MacAddr);
			if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
			{
				if(MeaDrvPortMacDeleteFilterMask(Service_id,FilterId)!= ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete whitelist filter  for service:%d\n",Service_id);
					return ENET_FAILURE;
				}
			}
		}
	}

	for(vlan=2;vlan<ADAP_NUM_OF_SUPPORTED_VLANS;vlan++)
	{
		pServiceId = MeaAdapGetServiceIdByVlanIfIndex(vlan,u4IfIndex,MEA_PARSING_L2_KEY_Ctag);
		if(pServiceId != NULL)
		{
			Service_id = pServiceId->serviceId;
			if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeletePortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
				return ENET_FAILURE;
			}

			if(valid == MEA_TRUE)
			{
				FilterId = adap_DeleteEntryFromFilterListMacAddr(Service_id,&MacAddr[0]);
				if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
				{
					if(MeaDrvPortMacDeleteFilterMask(Service_id,FilterId)!= ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete whitelist filter  for service:%d\n",Service_id);
						return ENET_FAILURE;
					}
				}
			}
		}
		else
		{
			pServiceId = MeaAdapGetServiceIdByVlanIfIndex(vlan,u4IfIndex,MEA_PARSING_L2_KEY_Untagged);
			if(pServiceId != NULL)
			{
				Service_id = pServiceId->serviceId;
				if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeletePortMacVlanEntry ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
					return ENET_FAILURE;
				}

				if(valid == MEA_TRUE)
				{
					FilterId = adap_DeleteEntryFromFilterListMacAddr(Service_id,&MacAddr[0]);
					if(FilterId != MEA_PLAT_GENERATE_NEW_ID)
					{
						if(MeaDrvPortMacDeleteFilterMask(Service_id,FilterId)!= ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete whitelist filter  for service:%d\n",Service_id);
							return ENET_FAILURE;
						}
					}
				}
			}
		}
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwMacLearningStatus (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2FdbId,
                             tEnetHal_HwPortArray * pHwEgressPorts, ADAP_Uint8 u1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (u2FdbId);
	ADAP_UNUSED_PARAM (pHwEgressPorts);
	ADAP_UNUSED_PARAM (u1Status);
	tBridgeDomain 	*pBridgeDomain=NULL;
	ADAP_Int32		Index=0;
	ADAP_Bool		changed=ADAP_FALSE;
	ADAP_Int32		ret=ENET_SUCCESS;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(VlanId,u4ContextId);

	if(pBridgeDomain != NULL)
	{
	    for(Index=0;Index<pHwEgressPorts->i4Length;Index++)
	    {
	    	if(pHwEgressPorts->pu4PortArray[Index] < MAX_NUM_OF_SUPPORTED_PORTS)
	    	{
				if(pBridgeDomain->learningMode[pHwEgressPorts->pu4PortArray[Index]] != u1Status)
				{
					pBridgeDomain->learningMode[pHwEgressPorts->pu4PortArray[Index]] = u1Status;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"enter portId:%d vlan:%d status:%s\r\n",
							pHwEgressPorts->pu4PortArray[Index],
							VlanId,
							pBridgeDomain->learningMode[pHwEgressPorts->pu4PortArray[Index]]==ADAP_VLAN_LEARNING_ENABLED?"Enable":"disable");
					changed=ADAP_TRUE;
				}
	    	}
	    }
	
	
        if(changed == ADAP_TRUE)
        {
    	    ret = ADAP_ConfigVlanEntry(pBridgeDomain);
        }
	}
    Adap_bridgeDomain_semUnlock();
    return ret;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
												eEnetHal_VlanHwTunnelFilters ProtocolId,ADAP_Uint32 u4TunnelStatus)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	MEA_LxCP_Protocol_Action_te action=MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
	tEnetHal_VlanId				VlanIndex;
	ADAP_Uint32					bridgeMode;
	tBridgeDomain 			    *pBridgeDomain=NULL;
	ADAP_Uint16                 VpnIndex;
	ADAP_Uint32 				modeType=0;

	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is out of index\n", u4IfIndex);
		return ENET_SUCCESS;
	}

	if(adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(u4IfIndex) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"IfIndex:%d is port channel but no interface assigned\n", u4IfIndex);
			return ENET_FAILURE;
		}
		else
		{
			u4IfIndex = adap_GetLagMasterPort(u4IfIndex);
		}
	}

	bridgeMode = AdapGetBridgeMode();
	switch(bridgeMode)
	{
		case ADAP_VLAN_PROVIDER_BRIDGE_MODE:
		{
			switch(u4TunnelStatus)
			{
				case ENET_VLAN_TUNNEL_PROTOCOL_PEER:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_CPU;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_DISCARD:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_DISCARD;
					break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort unknown tunnel option:%d\n",u4TunnelStatus);
					break;
			}

			switch(ProtocolId)
			{
				case ENETHAL_VLAN_NP_STP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_STP);
					// change all services to forwarding disable
					// change all services to forwarding disable
					MeaDrvRemProviderTunnel(&adapVlanProviderStpAddr);
					MeaDrvRemCustomerTunnel(&adapStpAddress);
					// change all services to forwarding disable
					if(u4TunnelStatus == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL)
					{
						MeaDrvAddProviderTunnel(&adapVlanProviderStpAddr,&adapStpAddress,ProtocolId);
						MeaDrvAddCustomerTunnel(&adapStpAddress,&adapVlanProviderStpAddr,ProtocolId);
					}
					break;
				case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
					MeaDrvRemProviderTunnel(&adapVlanProviderGvrpAddr);
					MeaDrvRemCustomerTunnel(&adapGvrpAddress);
					// change all services to forwarding disable
					if(u4TunnelStatus == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL)
					{
						MeaDrvAddProviderTunnel(&adapVlanProviderGvrpAddr,&adapGvrpAddress,ProtocolId);
						MeaDrvAddCustomerTunnel(&adapGvrpAddress,&adapVlanProviderGvrpAddr,ProtocolId);
					}
					break;
				case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_GMRP);
					break;
				case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_LACP_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
					// change all services to forwarding disable
					break;
				case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_GMRP);
					// change all services to forwarding disable
					break;
				case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_LLDP_PROTO_ID:

					MeaDrvLxcpUpdateProtocols(u4IfIndex, action, MEA_LXCP_PROTOCOL_IN_BPDU_14);
	                 break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort unknown protocol option:%d\n",ProtocolId);
					break;
			}
		}
		break;
		case ADAP_VLAN_CUSTOMER_BRIDGE_MODE:
		{
			switch(u4TunnelStatus)
			{
				case ENET_VLAN_TUNNEL_PROTOCOL_PEER:
					action=MEA_LXCP_PROTOCOL_ACTION_CPU;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL:
					action=MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_DISCARD:
					action=MEA_LXCP_PROTOCOL_ACTION_DISCARD;
					break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort unknown tunnel option:%d\n",u4TunnelStatus);
					break;
			}

			switch(ProtocolId)
			{
				case ENETHAL_VLAN_NP_STP_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
					break;
				case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_GMRP);
					break;
				case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
				case ENETHAL_VLAN_NP_LACP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols(u4IfIndex, action, MEA_LXCP_PROTOCOL_IN_BPDU_2);
					break;
				case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
				case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
				case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
				case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
				case ENETHAL_VLAN_NP_ECFM_PROTO_ID:
				case ENETHAL_VLAN_NP_EOAM_PROTO_ID:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort currently not support protocol:%d\n",ProtocolId);
					break;
				case ENETHAL_VLAN_NP_LLDP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols(u4IfIndex, action, MEA_LXCP_PROTOCOL_IN_BPDU_14);
	                break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort unknown protocol option:%d\n",ProtocolId);
					break;
			}

			switch(u4TunnelStatus)
			{
				case ENET_VLAN_TUNNEL_PROTOCOL_PEER:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_DISCARD:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetProtocolTunnelStatusOnPort unknown tunnel option:%d\n",u4TunnelStatus);
					break;
			}
		}
		break;
		case ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE:
		case ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE:
		{
			switch(u4TunnelStatus)
			{
				case ENET_VLAN_TUNNEL_PROTOCOL_PEER:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_CPU;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
					break;
				case ENET_VLAN_TUNNEL_PROTOCOL_DISCARD:
					adap_SetTunnelOption(u4IfIndex,ProtocolId,u4TunnelStatus);
					action=MEA_LXCP_PROTOCOL_ACTION_DISCARD;
					break;
				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProtocolTunnelStatusOnPort unknown tunnel option:%d\n",u4TunnelStatus);
					break;
			}

			switch(ProtocolId)
			{
				case ENETHAL_VLAN_NP_STP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_STP);
					// if its customer side and provider side is is not discard then do tunnel back
					AdapGetProviderCoreBridgeMode(&modeType,u4IfIndex);

					if( (modeType == ADAP_CUSTOMER_EDGE_PORT) && (u4TunnelStatus == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL) )
					{
						MeaDrvAddPnpTunnel(ENETHAL_VLAN_NP_STP_PROTO_ID);
					}
					break;
				case ENETHAL_VLAN_NP_GVRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
					// change all services to forwarding disable
					if( (modeType == ADAP_CUSTOMER_EDGE_PORT) && (u4TunnelStatus == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL) )
					{
						MeaDrvAddPnpTunnel(ENETHAL_VLAN_NP_GVRP_PROTO_ID);
					}
					break;
				case ENETHAL_VLAN_NP_GMRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_GMRP);
					if( (modeType == ADAP_CUSTOMER_EDGE_PORT) && (u4TunnelStatus == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL) )
					{
						MeaDrvAddPnpTunnel(ENETHAL_VLAN_NP_GMRP_PROTO_ID);
					}
					break;
				case ENETHAL_VLAN_NP_DOT1X_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_LACP_PROTO_ID:
					for(VlanIndex = 1; VlanIndex < ADAP_NUM_OF_SUPPORTED_VLANS; VlanIndex ++)
					{
						Adap_bridgeDomain_semLock();
						pBridgeDomain = Adap_getBridgeDomainByVlan(VlanIndex,0);

						if( pBridgeDomain != NULL)
                                                {
						    VpnIndex = pBridgeDomain->vpn;
						    MeaDrvSetLacpTunnelConfiguration(u4ContextId, u4IfIndex, VpnIndex);
                                                }
						Adap_bridgeDomain_semUnlock();
					}
					break;
				case ENETHAL_VLAN_NP_IGMP_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_MVRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_IN_BPDU_MVRP);
					// change all services to forwarding disable
					break;
				case ENETHAL_VLAN_NP_MMRP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols (u4IfIndex,action,MEA_LXCP_PROTOCOL_GMRP);
					// change all services to forwarding disable
					break;
				case ENETHAL_VLAN_NP_ELMI_PROTO_ID:
					break;
				case ENETHAL_VLAN_NP_LLDP_PROTO_ID:
					MeaDrvLxcpUpdateProtocols(u4IfIndex, action, MEA_LXCP_PROTOCOL_IN_BPDU_14);
	                break;

				default:
					break;
			}
		}
		break;
		default:
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetProtocolTunnelStatusOnPort wrong bridge mode:%d\n",bridgeMode);
		break;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwPortMacLearningStatus (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	tServiceDb 							  *pServiceInfo=NULL;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt                Service_outPorts;
    MEA_Policer_Entry_dbt                 Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;

    pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
	while(pServiceInfo != NULL)
	{
		// Skip default services (including LAG's), because they don't have actions
		if (pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_DEF_SID)
		{
			pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
			continue;
		}

		if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
		{
			/* Get the service from the ENET DB */
			adap_memset(&Service_data  , 0 , sizeof(Service_data ));
			adap_memset(&Service_outPorts  , 0 , sizeof(Service_outPorts ));
			adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
			adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

			if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
									pServiceInfo->serviceId,
									NULL,
									&Service_data,
									&Service_outPorts,
									&Service_policer,
									&Service_editing) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwPortMacLearningStatus: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", pServiceInfo->serviceId);
				return ENET_FAILURE;
			}
			Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

			if (Service_editing.ehp_info == NULL )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwPortMacLearningStatus: FAIL to malloc ehp_info\n");
				return ENET_FAILURE;
			}

			if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
									pServiceInfo->serviceId,
									NULL,
									&Service_data,
									&Service_outPorts, /* outPorts */
									&Service_policer, /* policer  */
									&Service_editing) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsBrgUpdateServiceLearningStatus: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",pServiceInfo->serviceId);
				MEA_OS_free(Service_editing.ehp_info);
				return ENET_FAILURE;
			}

			/* Enable / Disable learning on this service */
			switch(u1Status){
				case ENET_VLAN_ENABLED :
					/* Enable learning on this service */
					Service_data.DSE_learning_enable     = MEA_TRUE;
					Service_data.DSE_learning_actionId_valid = MEA_TRUE;
					Service_data.DSE_learning_srcPort_force = MEA_TRUE;

					if(pServiceInfo->actionId != MEA_PLAT_GENERATE_NEW_ID)
					{
						Service_data.DSE_learning_actionId_valid = MEA_TRUE;
						Service_data.DSE_learning_actionId = pServiceInfo->actionId;
					}
				break;

				case ENET_VLAN_DISABLED :
					/* Disable learning on this service */
					Service_data.DSE_learning_enable     = MEA_FALSE;
					Service_data.DSE_learning_actionId_valid = MEA_FALSE;
					Service_data.DSE_learning_srcPort_force = MEA_FALSE;
				break;

				default:
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Invalid u1Status:%d\n", u1Status);
					MEA_OS_free(Service_editing.ehp_info);
					return ENET_FAILURE;
			}

			/* Update the service back to the ENET DB */
			if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
									pServiceInfo->serviceId,
									&Service_data,
									&Service_outPorts,
									&Service_policer,
									&Service_editing) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsBrgUpdateServiceLearningStatus: failed for service:%d\n", pServiceInfo->serviceId);
				MEA_OS_free(Service_editing.ehp_info);
				return ENET_FAILURE;
			}
			MEA_OS_free(Service_editing.ehp_info);
		}

		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
	} // end of while

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsVlanHwGetMacLearningMode (ADAP_Uint32 *pu4LearningMode)
{
	*pu4LearningMode = ENET_VLAN_HW_UPDATED_CPU_LEARNING;

	return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetMcastIndex (tEnetHal_HwMcastIndexInfo *pHwMcastIndexInfo,
		ADAP_Uint32 *pu4McastIndex)
{
	ADAP_UNUSED_PARAM (pu4McastIndex);
	tMulticastMac *multicastEntry;

	multicastEntry = adap_createMulticastMacEntry(&pHwMcastIndexInfo->MacAddr);
	if(multicastEntry != NULL)
	{
		multicastEntry->vlanId=pHwMcastIndexInfo->VlanId;
		multicastEntry->portbitmap=0;
	}

	return ENET_SUCCESS;

}
ADAP_Int32 EnetHal_FsMiVlanHwSetPortIngressEtherType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2EtherType)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u2EtherType);


    return ENET_SUCCESS;

}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortEgressEtherType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2EtherType)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u2EtherType);


    return ENET_SUCCESS;

}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortProperty (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
		tEnetHal_HwVlanPortProperty *pVlanPortProperty)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (*pVlanPortProperty);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetVlanLoopbackStatus (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId,ADAP_Int32 i4LoopbackStatus)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (i4LoopbackStatus);


    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwPortUnicastMacSecType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Int32 i4MacSecType)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (i4MacSecType);
	ADAP_UNUSED_PARAM (u4IfIndex);

    return ENET_SUCCESS;
}

ADAP_Int32 adap_VlanHwUpdatePriorityVlan(tEnetHal_VlanId primaryVlan, ADAP_Uint32 u4IfIndex,tAdap_PortConfigSetting *pPortArry, eAdap_EditPriorityTag type,int bLearning, ADAP_Uint16 seconderyVlan)
{
	MEA_Service_t     					  Service_id;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt				  OutPorts_Entry;
    MEA_Policer_Entry_dbt          		  Policer_Entry;
    MEA_Action_Entry_Data_dbt             Action_data;
    MEA_OutPorts_Entry_dbt                Action_outPorts;
    MEA_Policer_Entry_dbt                 Action_policer;
    MEA_EHP_Info_dbt                      Action_editing_info;
    MEA_EgressHeaderProc_Array_Entry_dbt  Action_editing;
    MEA_Action_t                          Action_id;
	MEA_Port_t                            EnetPortId, enetId;
	MEA_EHP_Info_dbt                      Service_editing_info[3];

	MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	ADAP_Uint16							  Index;
	ADAP_Uint32							  port_type=0;
	ADAP_Uint32							  issMasterPort=0;
	MEA_Service_Entry_Key_dbt             Service_key;
	MEA_Bool							valid=MEA_FALSE;
	MEA_Bool							exist;
	tServiceDb 							*pServiceId=NULL;
    tServiceDb							*pServiceAlloc=NULL;
    tBridgeDomain 						*pBridgeDomain=NULL;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_VlanHwUpdatePriorityVlan -  Vlan:%d ISS Port:%d IsTagged:%d\n",
            primaryVlan, u4IfIndex, type);

	adap_memset(&Service_data     , 0 , sizeof(Service_data));
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Policer_Entry, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Service_editing, 0, sizeof(Service_editing));
	adap_memset(&Service_editing_info, 0, sizeof(MEA_EHP_Info_dbt)*3);

	issMasterPort = u4IfIndex;
	if(adap_IsPortChannel(issMasterPort) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(u4IfIndex) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_VlanHwUpdatePriorityVlan lag configured without phy ports :%d \n",u4IfIndex);
			return ENET_SUCCESS;
		}
		issMasterPort = adap_GetLagMasterPort(u4IfIndex);
	}

	if(MeaAdapGetPhyPortFromLogPort(issMasterPort, &EnetPortId) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",issMasterPort, EnetPortId);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", issMasterPort);
		return ENET_FAILURE;
    }

	pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Ctag,issMasterPort);
	if(pServiceId != NULL)
	{
		Service_id = pServiceId->serviceId;
		if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,Service_id, &valid) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan ERROR: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", Service_id);
			return ENET_FAILURE;
		}
	}
	else
	{
		// if there's no available service on BridgeDomain we need to create it
		// by adding of needed port to BridgeDomain
		if (adap_IsPortChannel(u4IfIndex) != ENET_SUCCESS)
		{
			if(EnetHal_FsMiVlanHwSetVlanMemberPort(0,1,issMasterPort, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan ERROR: failed to create service\n");
				return ENET_FAILURE;
			}

			valid = MEA_TRUE;
			pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Ctag,issMasterPort);
			if(pServiceId != NULL)
			{
				Service_id = pServiceId->serviceId;
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan ERROR: can't find service\n");
				return ENET_FAILURE;
			}
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"adap_VlanHwUpdatePriorityVlan : Addition of master %d port to LAG", issMasterPort);
			return ENET_SUCCESS;
		}
	}

	if(valid == MEA_TRUE)
	{

		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								Service_id,
								&Service_key,
								&Service_data,
								&OutPorts_Entry,
								&Policer_Entry,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan : ERROR by getting service id from Enet issPort:%d,  Port type:%d Vlan:%d \n",
					issMasterPort, type, primaryVlan);
			return ENET_FAILURE;
		}
		Service_editing.ehp_info = &Service_editing_info[0];
		if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
								Service_id,
								&Service_key,
								&Service_data,
								&OutPorts_Entry,
								&Policer_Entry,
								&Service_editing) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan : ERROR by getting service id from Enet issPort:%d,  Port type:%d Vlan:%d \n",
					issMasterPort, type, primaryVlan);
			return ENET_FAILURE;
		}

//		if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
//		{
//			if(ENET_ADAPTOR_Delete_Action (MEA_UNIT_0,Service_data.DSE_learning_actionId) != MEA_OK)
//			{
//				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan ERROR: failed by delete action id %d\n", Service_data.DSE_learning_actionId);
//				return ENET_FAILURE;
//			}
//		}
	}

	if(bLearning == ENET_VLAN_ENABLED)
	{
		adap_memset(&Action_data     , 0 , sizeof(Action_data    ));
		Action_data.output_ports_valid = MEA_TRUE;
		Action_data.force_color_valid  = MEA_TRUE;
		Action_data.COLOR              = 2;
		Action_data.force_cos_valid    = MEA_FALSE;
		Action_data.COS                = 0;
		Action_data.force_l2_pri_valid = MEA_FALSE;
		Action_data.L2_PRI             = 0;

		Action_data.pm_id_valid        = MEA_TRUE;
		Action_data.pm_id              = 0; /* Request new id */
		Action_data.tm_id_valid        = MEA_FALSE;
		Action_data.tm_id              = 0;
		Action_data.ed_id_valid        = MEA_TRUE;
		Action_data.ed_id              = 0; /* Request new id */
		Action_data.protocol_llc_force = MEA_TRUE;
		Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
		Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
		Action_data.Llc                = MEA_FALSE;

		adap_memset(&Action_outPorts,0,sizeof(Action_outPorts));
		MEA_SET_OUTPORT(&Action_outPorts, EnetPortId);

		/* Build the action policer */
		adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
		/* Build the action editing */
		adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
		adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));

		Action_editing.num_of_entries = 1;               /* Number of the entries in the Editing table */
		Action_editing.ehp_info = &Action_editing_info;
		adap_memcpy(&Action_editing_info.output_info,  /* Flooding domain for this Editing entry */
					  &Action_outPorts,
					  sizeof(Action_editing_info.output_info));


		switch(type)
		{
		case ENET_EditVlanPriorityTagged:
			Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
			Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
			Action_editing.ehp_info[0].ehp_data.eth_info.val.vlan.vid = primaryVlan;//== Ingress PVID
			Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
			Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
			Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
			break;
		default:
			break;
		}

		Action_id = MEA_PLAT_GENERATE_NEW_ID;

		if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
								   &Action_data,
								   &Action_outPorts,
								   &Action_policer,
								   &Action_editing,
								   &Action_id) != MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"%s FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n", __FILE__,Action_id);
		   return ENET_FAILURE;
		}
	}

	adap_memset(&Service_editing_info, 0, sizeof(MEA_EHP_Info_dbt)*3);
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Service_editing, 0, sizeof(Service_editing));

	AdapGetProviderCoreBridgeMode(&port_type, issMasterPort);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap__VlanHwUpdatePriorityVlan editing type: :%d \n",type);
	switch(type)
	{
		case ENET_EditVlanPriorityTagged:
			MeaDrvVlanSetEHPInfoData(&Service_editing_info[0],ADAP_EHP_APPEND_CTAG_EDITING,bLearning,primaryVlan,seconderyVlan);
			break;
		case ENET_EditVlanPriorityQTagged:
			MeaDrvVlanSetEHPInfoData(&Service_editing_info[0],ADAP_EHP_SWAP_SVLAN_EDITING,bLearning,primaryVlan,seconderyVlan);
			Service_key.L2_protocol_type =MEA_PARSING_L2_KEY_Stag; /* Ethertypes: 88a8/8100*/
			Service_key.net_tag          = 0;
			Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
			Service_key.evc_enable = MEA_TRUE;
			break;
		case ENET_EditVlanPrioritySwapInnerAddOuter:
			MeaDrvVlanSetEHPInfoData(&Service_editing_info[0],ADAP_EHP_APPEND_STAG_TO_CTAG_EDITING,bLearning,seconderyVlan,primaryVlan);
			break;
		case ENET_EditVlanPrioritySwapOuter:
			MeaDrvVlanSetEHPInfoData(&Service_editing_info[0],ADAP_EHP_SWAP_SVLAN_EDITING,bLearning,seconderyVlan,seconderyVlan);
			break;
		case ENET_EditVlanPrioritySwapCvlan:
			MeaDrvVlanSetEHPInfoData(&Service_editing_info[0],ADAP_EHP_SWAP_CVLAN_EDITING,bLearning,primaryVlan,seconderyVlan);
			Service_key.net_tag = 0;
			break;
		default:
			break;
	}


	Service_editing.num_of_entries = 1;
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	for(Index = 0; Index < MAX_NUM_OF_SUPPORTED_PORTS; Index ++)
	{
		if(pPortArry[Index].port_defined != 0)
		{
			if(pPortArry[Index].iss_port_id != issMasterPort)
			{
				issMasterPort = pPortArry[Index].iss_port_id;
				if(adap_IsPortChannel(issMasterPort) == ENET_SUCCESS)
				{
					if(adap_NumOfLagPortFromAggrId(issMasterPort) == 0)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_VlanHwUpdatePriorityVlan lag configured without phy ports :%d \n",issMasterPort);
						continue;
					}
					issMasterPort = adap_GetLagMasterPort(pPortArry[Index].iss_port_id);
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_VlanHwUpdatePriorityVlan port index %d iss_port_id %d\n",Index,issMasterPort);
				if(MeaAdapGetPhyPortFromLogPort(issMasterPort, &EnetPortId) == ENET_SUCCESS)
				{
					printf("logPort:%d, enetPortId:%d\n",issMasterPort, EnetPortId);
				}
				else
				{
					printf("Unable to convert u4IfIndex:%d to the ENET portId\n", issMasterPort);
					return ENET_FAILURE;
			    }
				MEA_SET_OUTPORT(&OutPorts_Entry, EnetPortId);
				MEA_SET_OUTPORT(&Service_editing_info[0].output_info,EnetPortId);
			}
		}
	}

	Service_editing.ehp_info = &Service_editing_info[0];

   if(bLearning == ENET_VLAN_ENABLED)
    {
        Service_data.DSE_forwarding_enable   = MEA_TRUE;
        Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
        Service_data.DSE_learning_enable     = MEA_TRUE;
        Service_data.DSE_learning_key_type   = MEA_DSE_LEARNING_KEY_TYPE_SA_PLUS_VPN;
    	Service_data.DSE_learning_actionId   = Action_id;
    	Service_data.DSE_learning_actionId_valid = MEA_TRUE;
        Service_data.DSE_learning_srcPort	 = EnetPortId;
        Service_data.DSE_learning_srcPort_force = MEA_TRUE;
    }
    else
    {
		Service_data.DSE_forwarding_enable   = MEA_TRUE;
		Service_data.DSE_forwarding_key_type = MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
		Service_data.DSE_learning_enable     = MEA_FALSE;
		Service_data.DSE_learning_actionId_valid = MEA_FALSE;
		Service_data.DSE_learning_srcPort_force = MEA_FALSE;
    }

	Service_data.editId=0;
	if(adap_IsPortChannel(issMasterPort) == ENET_SUCCESS)
	{
		issMasterPort = adap_GetLagMasterPort(u4IfIndex);
		Service_key.src_port = adap_PortGetVirtualPort(issMasterPort);
		Service_key.virtual_port = MEA_TRUE;
		Service_data.DSE_learning_srcPort_force=MEA_TRUE;
		Service_data.DSE_learning_srcPort = EnetPortId;
		if(setPortToPvid(issMasterPort,0) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to setPortToPvid: port%d\r\n",issMasterPort);
			return ENET_FAILURE;
		}
	}

	pBridgeDomain = Adap_getBridgeDomainByVlan(1, 0);

	if(Service_id != MEA_PLAT_GENERATE_NEW_ID)
	{
		// It's also important to remove the service from linked list in database,
		// not just delete the service.
		ADAP_DeleteIfIndexVlanEntry(pBridgeDomain, u4IfIndex, MEA_PARSING_L2_KEY_Ctag, 1);
		Service_id = MEA_PLAT_GENERATE_NEW_ID;
	}

	Service_data.flowCoSMappingProfile_force = MEA_FALSE;
	Service_data.flowMarkingMappingProfile_force = MEA_FALSE;

	if(ENET_ADAPTOR_IsExist_Service_ByKey(MEA_UNIT_0,&Service_key,&exist,&Service_id) == MEA_OK)
	{
		if(exist == MEA_TRUE)
		{
			ADAP_DeleteIfIndexVlanEntry(pBridgeDomain, u4IfIndex, MEA_PARSING_L2_KEY_Ctag, 1);
		}
	}

	pServiceAlloc = MeaAdapAllocateServiceId();

	if(pServiceAlloc == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
		return MEA_ERROR;
	}

	pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;
    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &OutPorts_Entry,
                               &Policer_Entry,
                               &Service_editing,
                               &pServiceAlloc->serviceId) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan FAIL: Unable to create the new service %d\n", pServiceAlloc->serviceId);
        MeaAdapFreeServiceId(pServiceAlloc->serviceId);
        return ENET_FAILURE;
    }

    for(Index=1; Index<MeaAdapGetNumOfPhyPorts(); Index++)
    {
	MeaAdapGetPhyPortFromLogPort(issMasterPort, &enetId);
		if(MEA_API_Set_VP_State (MEA_UNIT_0,1,enetId,MEA_VP_STATE_FWD) != MEA_OK)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwUpdatePriorityVlan ERROR: MEA_API_Set_VP_State failed for VlanId:%d\n", 0);
		   return ENET_FAILURE;
		}
    }

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &EnetPortId) == ENET_SUCCESS)
	{
		printf("logPort:%d, enetPortId:%d\n",u4IfIndex, EnetPortId);
	}
	else
	{
		printf("Unable to convert u4IfIndex:%d to the ENET portId\n", u4IfIndex);
		return ENET_FAILURE;
	}


	// save configuration
    pServiceAlloc->ifIndex=u4IfIndex;
	pServiceAlloc->inPort=EnetPortId;
	pServiceAlloc->outerVlan=Service_key.net_tag & (~MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL);
	pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
	pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
	pServiceAlloc->sub_protocol_type = Service_key.sub_protocol_type;
	pServiceAlloc->vpnId = Service_data.vpn;
	pServiceAlloc->pmId = Service_data.pmId;
	pServiceAlloc->actionId = Action_id;

	// add the service to bridge domain
	if(Adap_addServiceToLinkList(pServiceAlloc,pBridgeDomain) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by Adap_addServiceToLinkList serviceId:%d\r\n",pServiceAlloc->serviceId);
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"===end adap_VlanHwUpdatePriorityVlan Vlan:%d ISS Port:%d IsTagged:%d\n",
            primaryVlan, issMasterPort, type);

	return ENET_SUCCESS;
}

ADAP_Int32 adap_ReconfigureVlanPriority(ADAP_Uint32 u4IfIndex)
{
	if (EnetHal_FsMiVlanHwSetVlanMemberPort(0, 0, u4IfIndex, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_ReconfigureVlanPriority: can't add port %d to vlan id %d\r\n", u4IfIndex, 0);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_CreateMacLimiterProfile(ADAP_Uint32 limit, MEA_Limit_Action_te action_type, MEA_Limiter_t *pLimiterId)
{
    MEA_Limiter_Entry_dbt entry;
    *pLimiterId = ENET_PLAT_GENERATE_NEW_ID;
    MEA_OS_memset(&entry,0,sizeof(entry));
    entry.action_type = action_type;
    entry.limit = limit;

    if (MEA_API_Create_Limiter(ENET_UNIT_0,&entry,pLimiterId)!=MEA_OK){
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to create limiter \n",
            __FUNCTION__);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 adap_UpdateMacLimiterProfile(MEA_Limiter_t limiterId, ADAP_Uint32 limit, MEA_Limit_Action_te action_type)
{
    MEA_Limiter_Entry_dbt entry;
    MEA_Bool exist;

    MEA_OS_memset(&entry,0,sizeof(entry));

    if (MEA_API_IsExist_Limiter_ById  (ENET_UNIT_0, limiterId, &exist) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "%s - failed to read limiter info \n",
                    __FUNCTION__);
                return ENET_FAILURE;
    }

    if (exist != MEA_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,
                    "%s - Limiter %d do not exist \n",
                    __FUNCTION__, limiterId);
                return ENET_FAILURE;
    }

    if (MEA_API_Get_Limiter (ENET_UNIT_0, limiterId, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "%s - failed to get limiter info \n",
                    __FUNCTION__);
        return ENET_FAILURE;
    }

    entry.action_type = (action_type != MEA_LIMIT_ACTION_LAST) ? action_type: entry.action_type;
    entry.limit = (limit != 0) ? limit : entry.limit;

    if (MEA_API_Set_Limiter(ENET_UNIT_0,&entry,limiterId)!=MEA_OK){
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to update limiter \n",
            __FUNCTION__);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}


ADAP_Uint32 adap_AssignLimiterToService(MEA_Limiter_t limiterId, MEA_Service_t serviceId)
{
    tUpdateServiceParam params;
    adap_memset(&params, 0, sizeof(params));
    params.Service = serviceId;
    params.type = E_UPDATE_LIMITER;
    params.x.limiterId = limiterId;
    if (MeaDrvUpdateService (&params) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to update service %d \n",
            __FUNCTION__, serviceId);
        return ENET_FAILURE;
    }
    return ENET_SUCCESS;
}

ADAP_Uint32 adap_SetLimiterIdToVlan(ADAP_Uint32 VlanId, MEA_Limiter_t limiterId)
{
    tServiceDb * srv;

    // Getting vlan list
    srv = MeaAdapGetFirstServiceVlan(VlanId);
    while (srv)
    {
       if (srv->L2_protocol_type < D_PROVIDER_SERVICE_TAG)
           if (adap_AssignLimiterToService(limiterId, srv->serviceId) != ENET_SUCCESS)
           {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                   "%s - failed assign limiter to service %d \n",
                   __FUNCTION__, srv->serviceId);
               return ENET_FAILURE;
           }

       srv = MeaAdapGetNextServiceVlan(VlanId, srv->serviceId);
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 adap_SetLimiterIdToPort(ADAP_Uint32 u4IfIndex, MEA_Limiter_t limiterId)
{
    tServiceDb * srv;

    // Getting vlan list
    srv = MeaAdapGetFirstServicePort(u4IfIndex);
    while (srv)
    {
        if (srv->L2_protocol_type < D_PROVIDER_SERVICE_TAG)
           if (adap_AssignLimiterToService(limiterId, srv->serviceId) != ENET_SUCCESS)
           {
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                   "%s - failed assign limiter to service %d \n",
                   __FUNCTION__, srv->serviceId);
               return ENET_FAILURE;
           }
       srv = MeaAdapGetNextServicePort(u4IfIndex, srv->serviceId);
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_VlanHwSwitchMacLearningLimit (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4MacLimit)
{
    ADAP_UNUSED_PARAM (u4ContextId);
    int i;
    MEA_Limiter_t limiterId;

    if (AdapGetDefaultLimiterId(&limiterId) == ENET_SUCCESS)
    {
        // Updating existing mac limiter
        if(adap_UpdateMacLimiterProfile(limiterId, u4MacLimit, MEA_LIMIT_ACTION_LAST)
                != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed update limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }
    else
    {
        // Creating new limiterId
        if(adap_CreateMacLimiterProfile(u4MacLimit, MEA_LIMIT_ACTION_DISCARD, &limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }

        // Allocating new profile
        if(AdapAllocLimiterProf(0, 0, LIMITER_PROF_DEFAULT, limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter profile id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }
    for(i=1;i<MeaAdapGetNumOfPhyPorts();i++)
    {
        // Updating limiterid on services
        if(adap_SetLimiterIdToPort(i, limiterId)
                      != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed to set limiter id %d to port\n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_VlanHwMacLearningLimit (ADAP_Uint32 u4ContextId, tEnetHal_VlanId VlanId, ADAP_Uint16 u2FdbId, ADAP_Uint32 u4MacLimit)
{
    ADAP_UNUSED_PARAM (u4ContextId);
    ADAP_UNUSED_PARAM (u2FdbId);
    MEA_Limiter_t limiterId;

    if (AdapGetLimiterIdByVlan(VlanId, &limiterId) == ENET_SUCCESS)
    {
        // Updating existing mac limiter
        if(adap_UpdateMacLimiterProfile(limiterId, u4MacLimit, MEA_LIMIT_ACTION_LAST)
                != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed update limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }
    else
    {
        // Creating new limiterId
        if(adap_CreateMacLimiterProfile(u4MacLimit, MEA_LIMIT_ACTION_DISCARD, &limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }

        // Allocating new profile
        if(AdapAllocLimiterProf(VlanId, 0, LIMITER_PROF_VLAN, limiterId)
             != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "%s - failed create limiter profile id %d \n",
                __FUNCTION__, limiterId);
            return ENET_FAILURE;
        }
    }

    // Updating limiterid on services
    if(adap_SetLimiterIdToVlan(VlanId, limiterId)
                != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
            "%s - failed to set limiter id %d to vlan\n",
            __FUNCTION__, limiterId);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}
