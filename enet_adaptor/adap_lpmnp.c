#include "adap_lpminc.h"
#include "adap_lpmrb.h"
#include "adap_lpmtrie.h"
#include "adap_lpmnp.h"

#include "adap_types.h"
#include "mea_api.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_acl.h"
#include "adap_cfanp.h"
#include "adap_ipnp.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_enetConvert.h"
#include "mea_drv_common.h"
#include <sys/ioctl.h> /* ioctl() */
#include <net/if.h>

#define  MAX_PACKET_LEN                                         2000

int lpmpkt=0;
#ifdef LPM_ADAPTOR_WANTED
extern char *tapif;
#endif
UINT4
Enet_Lpm_Arp_Add (UINT4 u4VrId, UINT4 u4IpAddr, UINT4 u4IfIdx,
                    UINT2 u2VlanId, UINT1 *pMacAddr)
{
    tArpEntry        *pL3ArpElem = NULL;
    tArpEntry         L3ArpElem;
    UINT4             u4Status = LPM_FAILURE;
    
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
            "Enet_Lpm_Arp_Add DestIp(%x) IfIndex %d \n", u4IpAddr, u4IfIdx);

    adap_memset(&L3ArpElem, 0, sizeof (tArpEntry));
    L3ArpElem.DstIp = u4IpAddr;
    L3ArpElem.u4VrId = u4VrId;

    pL3ArpElem = (tArpEntry *)
        RedBlkTreeGet (gpL3ArpTbl, (tRedBlkElem *) & L3ArpElem);

    if (pL3ArpElem == NULL)
    {
        pL3ArpElem = adap_malloc(sizeof (tArpEntry));
        if (pL3ArpElem != NULL)
        {
            pL3ArpElem->DstIp = u4IpAddr;
            pL3ArpElem->u2vlanId = u2VlanId;
            pL3ArpElem->u4VrId = u4VrId;
            pL3ArpElem->u4IfIndex = u4IfIdx;
            adap_memcpy(pL3ArpElem->MacAddr, pMacAddr, sizeof (tMacAddr));
            u4Status = RedBlkTreeAdd (gpL3ArpTbl, (tRedBlkElem *) pL3ArpElem);
            if (u4Status == LPM_FAILURE)
            {
                ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
                    "Failed to add ARP (%x) IfIndex %d \n", u4IpAddr, u4IfIdx);
                return (LPM_FAILURE);
            }
        }
    }
    else
    {
        pL3ArpElem->u2vlanId = u2VlanId;
        pL3ArpElem->u4IfIndex = u4IfIdx;
        adap_memcpy(pL3ArpElem->MacAddr, pMacAddr, sizeof (tMacAddr));
    }
    return LPM_SUCCESS;
}

UINT4
Enet_Lpm_Arp_Delete (UINT4 u4VrId, UINT4 u4IpAddr)
{
    tRedBlkElem          *pRedBlkElem = NULL;
    tArpEntry        *pL3ArpElem = NULL;
    tArpEntry         L3ArpElem;
    
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
            "Enet_Lpm_Arp_Delete DestIp(%x) \n", u4IpAddr);

    adap_memset(&L3ArpElem, 0, sizeof (tArpEntry));
    L3ArpElem.DstIp = u4IpAddr;
    L3ArpElem.u4VrId = u4VrId;

    pRedBlkElem = RedBlkTreeGet (gpL3ArpTbl, (tRedBlkElem *) & L3ArpElem);

    pL3ArpElem = (tArpEntry *) pRedBlkElem;
    if (pL3ArpElem != NULL)
    {
        /* When ARP entry is removed, corresponding
         * routes from FPGA also has to be deleted */
        if (Enet_Lpm_Handle_FPGA_Arp_Delete (u4VrId, u4IpAddr) != LPM_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to remove route from FPGA\n");
        }               
        RedBlkTreeRemove (gpL3ArpTbl, pRedBlkElem);
        free (pL3ArpElem);
        return LPM_SUCCESS;
    }
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
            "Failed to remove ARP (%x)  \n", u4IpAddr);
    return LPM_FAILURE;
}


tArpEntry        *
Enet_Lpm_Arp_Get (UINT4 u4VrId, UINT4 u4IpAddr)
{
    tArpEntry        *pL3ArpElem = NULL;
    tArpEntry         L3ArpElem;
    
    adap_memset(&L3ArpElem, 0, sizeof (tArpEntry));
    L3ArpElem.DstIp = u4IpAddr;
    L3ArpElem.u4VrId = u4VrId;

    pL3ArpElem = (tArpEntry *)
            RedBlkTreeGet (gpL3ArpTbl, (tRedBlkElem *) & L3ArpElem);

    return pL3ArpElem;
}


UINT4
Enet_Lpm_Route_Add (tRtNextHopEntry  *prouteEntry)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtNextHopEntry    *pNextHop=NULL;
    UINT4               au4Indx[2];

    if (prouteEntry->u1AddrType == 2)
    {
	if (Enet_Lpm_Ipv6Route_Add(prouteEntry) == LPM_FAILURE)
    	{
            ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR,
                "FAILED to install route Dest %s to LPM database \n",prouteEntry->Ip6Prefix);
            return LPM_FAILURE;
        }
	else
	{
	    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
		"Enet_Lpm_Ipv6Route_Add DestIp(%s) succeeds\n",	prouteEntry->Ip6Prefix);
            return LPM_SUCCESS;
	}
    }

    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO,
            "Enet_Lpm_Route_Add DestIp(%x) prefixlen(%x) \n",
		prouteEntry->u4IpDestAddr, prouteEntry->u4SubNetMask);
    
    if ((prouteEntry->u4NextHopGt != 0) && 
        (Enet_Lpm_Arp_Get (prouteEntry->u4VrId, 
				prouteEntry->u4NextHopGt) == NULL))
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %x to LPM database due to ARP miss\n",prouteEntry->u4IpDestAddr);
        return LPM_FAILURE;
    }

    adap_memset(&au4Indx, 0, sizeof(au4Indx));
    pNextHop = (tRtNextHopEntry *)adap_malloc(sizeof(tRtNextHopEntry));
    if (pNextHop == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %x to LPM database\n",prouteEntry->u4IpDestAddr);
        return LPM_FAILURE;
    }

    adap_memset(pNextHop, 0, sizeof(tRtNextHopEntry));
    adap_memset(&inParams, 0, sizeof (tInputParams));
    adap_memset(&outParams, 0, sizeof (tOutputParams));

    pNextHop->u4NextHopGt = prouteEntry->u4NextHopGt;
    pNextHop->u4IfIndex = prouteEntry->u4IfIndex;
    pNextHop->u2VlanId = prouteEntry->u2VlanId;
    pNextHop->u4IpDestAddr = prouteEntry->u4IpDestAddr;
    pNextHop->u4SubNetMask = prouteEntry->u4SubNetMask;
    pNextHop->u4VrId = prouteEntry->u4VrId;
    pNextHop->u1AddrType = prouteEntry->u1AddrType;

    inParams.pRoot = gpLpmRouteTbl;
    inParams.i1AppId = (prouteEntry->u4VrId) + 1;
    inParams.pLeafNode = NULL;
    IPV4_MASK_TO_MASKLEN (inParams.u1PrefixLen, (prouteEntry->u4SubNetMask));

    au4Indx[0] = UTIL_HTONL ((prouteEntry->u4IpDestAddr) & (prouteEntry->u4SubNetMask));
    au4Indx[1] = UTIL_HTONL (prouteEntry->u4SubNetMask);

    inParams.Key.pKey = (UINT1 *) au4Indx;

    outParams.pAppSpecInfo = NULL;
    outParams.Key.u4Key = 0;

    if ((TrieDbAdd (&inParams, pNextHop, &outParams)) != LPM_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %x to LPM database\n",prouteEntry->u4IpDestAddr);
        return (LPM_FAILURE);
    }
    return LPM_SUCCESS;
}



UINT4
Enet_Lpm_Route_Delete (tRtNextHopEntry  *prouteEntry)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtNextHopEntry     *pRtNextHop = NULL;
    UINT4               u4NextHop = 0;
    UINT4               au4Indx[2];

    if (prouteEntry->u1AddrType == 2)
    { 
	if (Enet_Lpm_Ipv6Route_Delete(prouteEntry) == LPM_FAILURE)
    	{
            ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
                "FAILED to delete route Dest %s to LPM database \n",prouteEntry->Ip6Prefix);
            return LPM_FAILURE;
        }
	else
	{
	    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
		"Enet_Lpm_Ipv6Route_Delete DestIp(%s) succeeds\n",	prouteEntry->Ip6Prefix);
            return LPM_SUCCESS;
	}
    }

    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
                "Enet_Lpm_Route_Delete DestIp(%x) prefixlen(%x) \n",
                prouteEntry->u4IpDestAddr, prouteEntry->u4SubNetMask);

    adap_memset(&au4Indx, 0, sizeof(au4Indx));
    adap_memset(&inParams, 0, sizeof (tInputParams));
    adap_memset(&outParams, 0, sizeof (tOutputParams));

    inParams.pLeafNode = NULL;
    inParams.pRoot = gpLpmRouteTbl;
    inParams.i1AppId = (prouteEntry->u4VrId) + 1;
    IPV4_MASK_TO_MASKLEN (inParams.u1PrefixLen, (prouteEntry->u4SubNetMask));
    au4Indx[0] = UTIL_HTONL ((prouteEntry->u4IpDestAddr) & (prouteEntry->u4SubNetMask));
    au4Indx[1] = UTIL_HTONL (prouteEntry->u4SubNetMask);

    inParams.Key.pKey = (UINT1 *) au4Indx;

    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if (TrieDbRemove (&inParams, &outParams, &u4NextHop) == LPM_FAILURE)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to delete route Dest %x from LPM database\n",prouteEntry->u4IpDestAddr);
        return (LPM_FAILURE);
    }

    pRtNextHop = ((tRtNextHopEntry *) outParams.pAppSpecInfo);
    free (pRtNextHop);

    if (Enet_Lpm_Delete_FP_Entry (prouteEntry->u4VrId, prouteEntry->u4IpDestAddr, 
					prouteEntry->u4SubNetMask) != LPM_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete entry in FP table and forwarder\n");
        return LPM_FAILURE;
    }
    return LPM_SUCCESS;
}


UINT4
Enet_Lpm_Route_Get (tRtNextHopEntry *pRtInput, tRtNextHopEntry **pRtNextHop)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tNextHopInfo        *pNextHop = NULL;
    UINT4               au4Indx[2];

    adap_memset(&au4Indx, 0, sizeof(au4Indx));
    inParams.pRoot = gpLpmRouteTbl;
    inParams.pLeafNode = NULL;
    inParams.i1AppId = (pRtInput->u4VrId) + 1;
    inParams.u1PrefixLen = 0;
    au4Indx[0] = UTIL_HTONL ((pRtInput->u4IpDestAddr) & (pRtInput->u4SubNetMask));
    au4Indx[1] = UTIL_HTONL (pRtInput->u4SubNetMask);

    inParams.Key.pKey = (UINT1 *) au4Indx;

    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if ((TrieDbLookup (&inParams, &outParams,
                     (VOID **) &pNextHop)) == LPM_FAILURE)
    {
        return LPM_FAILURE;
    }
    *pRtNextHop = ((tRtNextHopEntry *) outParams.pAppSpecInfo);

    return LPM_SUCCESS;
}

UINT4
Enet_Lpm_Add_FP_Entry (UINT4 u4VrId, UINT4 u4IpDestAddr,
                       UINT4 u4NextHop, UINT4 u4IfIndex,
                       UINT2 u2VlanId, UINT2 u2VpnId, UINT1 *pDestMacAddr)
{
    tFPRouteEntry         *pRoute = NULL;
    tFPRouteEntry         RouteElem;
    MEA_OutPorts_Entry_dbt tOutPorts;
    ADAP_Uint32         logPort = 0;
    ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    ADAP_Uint32         modeType = 0;
    MEA_Port_t          PhyPort;
    MEA_Action_t        Action_id=0;
    MEA_EHP_Info_dbt tEhp_info;
    tArpEntry           *pL3ArpEntry = NULL;
    ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
    ADAP_Uint32 vlanIdTag=0;
    tEnetHal_MacAddr srcMac;
    tEnetHal_MacAddr daMac;
    MEA_Bool action_valid=MEA_FALSE;
    MEA_SE_Entry_dbt                      	entry;
        
    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
    adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
    adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));


    adap_memset(&RouteElem, 0, sizeof (tFPRouteEntry));
    RouteElem.u4DestAddr = u4IpDestAddr;
    RouteElem.u4VrId = u4VrId;

    pRoute = (tFPRouteEntry *)
        RedBlkTreeGet (gpFPShadowRouteTbl, (tRedBlkElem *) & RouteElem);

    if (pRoute != NULL)
    {
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," entry already present in LPM FP & forwarder\n");
	    return LPM_FAILURE;
    }

    if (u4NextHop != 0)
    {
	logPort = adap_VlanGetPortByVlanId (u2VlanId);
	MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
	MEA_SET_OUTPORT(&tOutPorts,PhyPort);

    	pL3ArpEntry = Enet_Lpm_Arp_Get (0, u4NextHop);
	if(pL3ArpEntry != NULL)
	{
	    adap_mac_from_arr(&daMac, pL3ArpEntry->MacAddr);
	}
	MeaDrvGetFirstGlobalMacAddress (&srcMac);

        if (adap_VlanCheckTaggedPort(u2VlanId,logPort) == ADAP_TRUE)
	{
            if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
               (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
            {
                AdapGetProviderCoreBridgeMode(&modeType,logPort);
		if (modeType == ADAP_PROVIDER_NETWORK_PORT)
		{
                    EtherType = 0x88a8;
		}
            }
	    vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	    tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
	    vlanIdTag = (EtherType << 16);
	    vlanIdTag |= u2VlanId;
	}
	else
        {
	    vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	    tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
	}
	MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
	/* create action for the route */
	MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
			    &daMac, vlanCommand, vlanIdTag, 0);

	if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
				    &tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
	}
	action_valid = MEA_TRUE;
    }

    adap_memset(&entry, 0, sizeof(entry));

    entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.IPv4=u4IpDestAddr;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.L4port=0;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.mask_type=MEA_DSE_MASK_IP_32;
    entry.key.DstIPv4_plus_L4_DstPort_plus_vpn.VPN = START_L3_VPN;
    entry.data.aging = 3; //Default
    entry.data.static_flag  = MEA_FALSE;//MEA_TRUE;

    if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
    {
	    /* Delete the drop entry and add valid entry in forwarder */
	    if (MeaDrvHwDeleteDestIpForwarder (u4IpDestAddr, 1, START_L3_VPN) != MEA_OK)
	    {
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
	    }
	    if(MeaDrvHwCreateDestIpForwarder(Action_id,action_valid,u4IpDestAddr, 1,
				    START_L3_VPN,&tOutPorts) != MEA_OK)
	    {
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to create entry in forwarder\n");
		    return LPM_FAILURE;
	    }
    }
    
    if (pRoute == NULL)
    {
        pRoute = adap_malloc(sizeof(tFPRouteEntry));
        if (pRoute != NULL)
        {
            pRoute->u4VrId = u4VrId;
            pRoute->u4DestAddr = u4IpDestAddr;
            pRoute->u4NextHopGt = u4NextHop;
            pRoute->u4IfIndex = u4IfIndex;
            pRoute->u2VlanId = u2VlanId;
            adap_memcpy(pRoute->DestMacAddr, pDestMacAddr, sizeof (tMacAddr));
            if (RedBlkTreeAdd (gpFPShadowRouteTbl, (tRedBlkElem *) pRoute) != LPM_SUCCESS)
            {
                return (LPM_FAILURE);
            }
        }
    }
    return LPM_SUCCESS;
}


UINT4
Enet_Lpm_Delete_FP_Entry (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask)
{
    tFPRouteEntry         *pRoute = NULL;
    tFPRouteEntry         RouteElem;
    ADAP_Uint32 arrIpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
    ADAP_Uint32 numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
    ADAP_Uint32 IpClass=0;
    MEA_OutPorts_Entry_dbt tOutPorts;
        
    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

    if(MiFiDivideIpClass(u4IpDestAddr, u4IpSubNetMask, &IpClass,
                        &arrIpAddr[0],&numOfEntries) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to calculate the subnetting\n");
        return LPM_FAILURE;
    }    

        /* delete from forwarder if alrdy exists */
        if( MeaDrvHwDeleteDestIpForwarder(u4IpDestAddr,IpClass,START_L3_VPN) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete entry in forwarder\n");
            return LPM_FAILURE;
        }
    
    adap_memset(&RouteElem, 0, sizeof (tFPRouteEntry));
    RouteElem.u4DestAddr = u4IpDestAddr;
    RouteElem.u4VrId = u4VrId;

    pRoute = (tFPRouteEntry *)
        RedBlkTreeGet (gpFPShadowRouteTbl, (tRedBlkElem *) & RouteElem);

    if (pRoute != NULL)
    {
        RedBlkTreeRemove (gpFPShadowRouteTbl, (tRedBlkElem *) pRoute);
        free (pRoute);
    }
    return LPM_SUCCESS;
}


INT4
Enet_Lpm_Handle_FPGA_Arp_Delete (UINT4 u4VrId, UINT4 u4IpAddr)
{
    tFPRouteEntry         *pRoute = NULL;
    tFPRouteEntry         *pRouteNext = NULL;
    UINT4                  u4IpSubNetMask = 0xffffffff;
    
    pRoute = (tFPRouteEntry *) RedBlkTreeGetFirst (gpFPShadowRouteTbl);

    while (pRoute != NULL)
    {
        pRouteNext = (tFPRouteEntry *) RedBlkTreeGetNext (gpFPShadowRouteTbl,
                            (tRedBlkElem *) pRoute, NULL);
        if (pRoute->u4NextHopGt == u4IpAddr)
        {
           if (Enet_Lpm_Delete_FP_Entry (u4VrId, pRoute->u4DestAddr, 
                u4IpSubNetMask) != LPM_SUCCESS)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                    "Failed to delete route from FPGA\r\n");
            }
        }
        pRoute = pRouteNext;
    }
    return LPM_SUCCESS;
}


INT4
EnetAdapProcessLpmPacket (UINT4 u4IfIndex, void *pBuf, UINT4 len)
{
    MEA_OutPorts_Entry_dbt tOutPorts;
    ADAP_Uint32         logPort = 0;
    tArpEntry           *pL3ArpEntry = NULL;
    tRtNextHopEntry     *pNextHop = NULL;
    tRtNextHopEntry     RtInput;
    tProtocl_sniff      ProtoSniff;
    UINT2               u2VpnId = 0;
    UINT2               u2ValToPtr = 0;
    UINT4               u4Index = 0;
    UINT4               u4DefIP = 0;
    UINT1               u1ChkDefRt = MEA_FALSE;
    UINT1               aRcvStaticBuf[MAX_PACKET_LEN];    /*Buffer used during packet Rx */

    adap_memset(&ProtoSniff,0,sizeof(ProtoSniff));
    adap_memset(&RtInput, 0, sizeof(tRtNextHopEntry));
    adap_memset(aRcvStaticBuf, 0, sizeof (aRcvStaticBuf));

    adap_memcpy(aRcvStaticBuf, pBuf, len);
    /* Extract Dst MAC, VLAN, DestIP from the packet */
    if(get_sourceip_from_ipPacket(aRcvStaticBuf, len-4, &ProtoSniff, &u4Index) != ADAP_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"unable to parse the packet\r\n");
        return MEA_FALSE;
    }

    /* replace the source mac with interface mac */
    if( MeaDrvGetFirstGlobalMacAddress (&ProtoSniff.ethernet.ether_shost) != MEA_FALSE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvGetFirstGlobalMacAddress\r\n");
        return MEA_FALSE;
    }

    if (adap_memcmp(&ProtoSniff.ethernet.ether_dhost,
                    &ProtoSniff.ethernet.ether_shost,
					sizeof(tEnetHal_MacAddr)) != 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"DestMAC lookup failed, Packet not destined to this router\r\n");
        return MEA_FALSE;
    }

    u2VpnId = adap_GetVpnByVlan (ProtoSniff.ethernet.vlanId);

    adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
    if (MeaAdapGetLpmHWStatus () == MEA_TRUE)
    {
    	/* create entry in forwarder to drop further packets */
    	if(MeaDrvHwCreateDestIpForwarder(0,MEA_TRUE,ProtoSniff.destIp, 1,
			    START_L3_VPN,&tOutPorts) != MEA_OK)
    	{
	    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to create drop route entry in forwarder\n");
    	}
    }

    pL3ArpEntry = Enet_Lpm_Arp_Get (0, ProtoSniff.destIp);
    if (pL3ArpEntry == NULL)
    {
	RtInput.u4VrId = 0;
	RtInput.u4IpDestAddr = ProtoSniff.destIp;
	RtInput.u4SubNetMask = 0xffffffff;
   	RtInput.u1AddrType = 1;
        if (Enet_Lpm_Route_Get (&RtInput, &pNextHop) == LPM_SUCCESS)
        {
            pL3ArpEntry = Enet_Lpm_Arp_Get (0, pNextHop->u4NextHopGt);
            if (pL3ArpEntry != NULL)
            {
            	adap_mac_from_arr(&ProtoSniff.ethernet.ether_dhost, pL3ArpEntry->MacAddr);
            }
            else
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                        "Next hop resolution failed, unable to route\r\n");
                u1ChkDefRt = MEA_TRUE;
            }
        }
        else
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "Route lookup failed, unable to route\r\n");
            u1ChkDefRt = MEA_TRUE;
        }
    }
    else
    {
    	adap_mac_from_arr(&ProtoSniff.ethernet.ether_dhost, pL3ArpEntry->MacAddr);
    }

    if(u1ChkDefRt == MEA_TRUE)
    {
        pNextHop = NULL;
	RtInput.u4VrId = 0;
	RtInput.u4IpDestAddr = u4DefIP;
	RtInput.u4SubNetMask = 0;
   	RtInput.u1AddrType = 1;
        if (Enet_Lpm_Route_Get (&RtInput, &pNextHop) == LPM_SUCCESS)
        {
            pL3ArpEntry = Enet_Lpm_Arp_Get (0, pNextHop->u4NextHopGt);
            if (pL3ArpEntry != NULL)
            {
                adap_memcpy(&(ProtoSniff.ethernet.ether_dhost),
                            pL3ArpEntry->MacAddr, MAC_ADDR_LEN);
            }
            else
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                        "Route lookup failed, drop the packet\r\n");
		if (MeaDrvHwDeleteDestIpForwarder (ProtoSniff.destIp, 1, START_L3_VPN) != MEA_OK)
		{
		    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
		}
                return MEA_FALSE;
            }
        }
        else
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "Route lookup failed, unable to route\r\n");
	    if (MeaDrvHwDeleteDestIpForwarder (ProtoSniff.destIp, 1, START_L3_VPN) != MEA_OK)
	    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete drop entry in forwarder\n");
	    }
            return MEA_FALSE;
        }
    }

    if (MeaAdapGetLpmHWStatus () == MEA_TRUE)
    {
        /* Update the FP route table for received route */
        if ((u1ChkDefRt == MEA_FALSE) &&
            (Enet_Lpm_Add_FP_Entry (pL3ArpEntry->u4VrId, ProtoSniff.destIp,
                       pNextHop->u4NextHopGt, pNextHop->u4IfIndex,
                       pL3ArpEntry->u2vlanId, u2VpnId, 
                        pL3ArpEntry->MacAddr) != LPM_SUCCESS))
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                "Route addition to FPGA shadow table failed\r\n");
        }
    }

    /* Update the packet Dest MAC, Vlan for forwarding */
    adap_mac_to_arr(&ProtoSniff.ethernet.ether_dhost, pBuf);
    adap_mac_to_arr(&ProtoSniff.ethernet.ether_shost, pBuf + ETH_ALEN);
   
    if ((aRcvStaticBuf[ENET_VLAN_TAG_OFFSET] == 0x81) &&
	(aRcvStaticBuf[ENET_VLAN_TAG_OFFSET+1] == 0x00))
    {
	u2ValToPtr = UTIL_HTONS (0x8100);
	adap_memcpy(&aRcvStaticBuf[ENET_VLAN_TAG_OFFSET], (CHR1 *) &u2ValToPtr, sizeof(UINT2));
	u2ValToPtr = UTIL_HTONS (pL3ArpEntry->u2vlanId);
        adap_memcpy(&aRcvStaticBuf[ENET_VLAN_TAG_OFFSET+2], (CHR1 *) &u2ValToPtr, sizeof(UINT2));
    }

    adap_memcpy(pBuf, aRcvStaticBuf, len);
    logPort = adap_VlanGetPortByVlanId (pL3ArpEntry->u2vlanId);
	
    MeaAdapSendPacket (pBuf, logPort, len-4);
    return MEA_TRUE;
}


VOID
DumpLPMRouteTable (VOID)
{
    tInputParams        inParams;
    tRtNextHopEntry    *pRtEntry = NULL;

    inParams.pRoot = gpLpmRouteTbl;
    inParams.pLeafNode = NULL;
    inParams.i1AppId = 1;
    inParams.u1PrefixLen = 0;
    inParams.Key.u4Key = 0;

    printf(" ==================================== \n");
    printf("          LPM Route Table  \n");
    printf(" ==================================== \n");
    printf("  DestIp/Mask     VlanId   NextHop  \n");
    printf(" ==================================== \n");
    for (TrieDbGetFirstNode (&inParams, (VOID **) &pRtEntry,
                           (VOID **) &inParams.pLeafNode); inParams.pLeafNode;)
    {
        printf ("%.8x/%4x %4d %12.8x ",
                   pRtEntry->u4IpDestAddr, pRtEntry->u4SubNetMask,
                   pRtEntry->u2VlanId, pRtEntry->u4NextHopGt);
        printf ("\n");

        TrieDbGetNextNode (&inParams, (VOID *) inParams.pLeafNode,
                         (VOID **) &pRtEntry, (VOID **) &inParams.pLeafNode);
    }
    return;
}

VOID
DumpArpTbl (VOID)
{
    tArpEntry        *pArpEntry = NULL;
    tArpEntry        *pNextEntry = NULL;

    printf("Packet received in LPM %d\n",lpmpkt);
    printf(" ==================================== \n");
    printf("          LPM ARP Table  \n");
    printf(" ==================================== \n");
    printf ("\n  DstIp   VlanId   MacAddr\n");
    printf ("  ==============================\n");

    pArpEntry = (tArpEntry *) RedBlkTreeGetFirst (gpL3ArpTbl);
    while (pArpEntry != NULL)
    {
        pNextEntry = (tArpEntry *) RedBlkTreeGetNext (gpL3ArpTbl, pArpEntry, NULL);

        printf ("%.8x %8hd ",
                   pArpEntry->DstIp, pArpEntry->u2vlanId);
	printf ("%.2x:%.2x:%.2x:%.2x:%.2x:%.2x \r\n", pArpEntry->MacAddr[0],pArpEntry->MacAddr[1],
			pArpEntry->MacAddr[2], pArpEntry->MacAddr[3],
			pArpEntry->MacAddr[4], pArpEntry->MacAddr[5]);

        pArpEntry = pNextEntry;
    }
    return;
}


VOID
DumpFPShadowRouteTbl (VOID)
{
    tFPRouteEntry        *pRtEntry = NULL;
    tFPRouteEntry        *pNextRtEntry = NULL;

    printf(" ==================================== \n");
    printf("      LPM FP Shadow Route Table  \n");
    printf(" ==================================== \n");
    printf ("\n  DstIp     NextHopIp   VlanId    DestMacAddr\n");
    printf (" ================================================\n");

    pRtEntry = (tFPRouteEntry *) RedBlkTreeGetFirst (gpFPShadowRouteTbl);
    while (pRtEntry != NULL)
    {
        pNextRtEntry = (tFPRouteEntry *) RedBlkTreeGetNext (gpFPShadowRouteTbl, pRtEntry, NULL);

        printf ("%.8x %.8x %8hd ",
                   pRtEntry->u4DestAddr, pRtEntry->u4NextHopGt, pRtEntry->u2VlanId);
	printf ("%.2x:%.2x:%.2x:%.2x:%.2x:%.2x \r\n", pRtEntry->DestMacAddr[0],pRtEntry->DestMacAddr[1],
			pRtEntry->DestMacAddr[2], pRtEntry->DestMacAddr[3],
			pRtEntry->DestMacAddr[4], pRtEntry->DestMacAddr[5]);

        pRtEntry = pNextRtEntry;
    }
    return;
}

void* LpmThreadHandler(void *arg)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    INT4		i4LpmSocketId = 0;
    INT4		i4BytesRead = 0;
    unsigned char 	buf[MAX_PACKET_LEN];
    MEA_Port_t 		PhyPort=0;
    
    adap_memset(buf, 0, MAX_PACKET_LEN);

    i4LpmSocketId = socket (PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (i4LpmSocketId < 0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
             "%s - socket creation failed (errno=%d '%s')\n",
             __func__, errno, strerror(errno));
	return NULL;
    }

    adap_memset(&ifr, 0, sizeof(ifr));
#ifdef LPM_ADAPTOR_WANTED
    sprintf (ifr.ifr_name, "%s", tapif);
    if (ioctl (i4LpmSocketId, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        close (i4LpmSocketId);
        return NULL;
    }
#endif

    adap_memset(&Enet, 0, sizeof (Enet));
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    if (bind (i4LpmSocketId, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        close (i4LpmSocketId);
        return NULL;
    }

    /* Initialise the LPM specific databases */
    LpmInitialize ();

    while (1)
    {
        i4BytesRead = recvfrom (i4LpmSocketId, buf, MAX_PACKET_LEN, 0, NULL, NULL);
        if (i4BytesRead == -1 && errno == EAGAIN)
        {
            continue;
        }

	if (i4BytesRead > 18)
	{
            if(buf[12]==0x91 && buf[13]==0x00 && 
               buf[16]==0x81 && buf[17]==0x00 &&
               buf[14]==0x0f && buf[15]==0xac) 
            {
		lpmpkt++;
		/* Ethernity-attached VLAN 4012 tag is present for LPM */
                PhyPort = (buf[19])|((buf[18]&0xf)<<8);
		memmove(buf+12,buf+20,i4BytesRead-20);
		EnetAdapProcessLpmPacket (PhyPort,buf,i4BytesRead-4);
	    }
	}
    }   
 
    ADAP_UNUSED_PARAM (arg);
    return NULL;
}

UINT4
Enet_Lpm_ND_Add (UINT4 u4VrId, UINT1 *pIp6Addr, UINT4 u4IfIdx,
                    UINT2 u2VlanId, UINT1 *pMacAddr)
{
    tNDEntry        *pL3NDElem = NULL;
    tNDEntry         L3NDElem;
    UINT4            u4Status = LPM_FAILURE;
    
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
            "Enet_Lpm_ND_Add DestIp(%s) IfIndex %d \n", pIp6Addr, u4IfIdx);

    adap_memset(&L3NDElem, 0, sizeof (tNDEntry));
    adap_memcpy(&(L3NDElem.DstIp6), pIp6Addr, sizeof (tIp6Address));
    L3NDElem.u4VrId = u4VrId;

    pL3NDElem = (tNDEntry *)
        RedBlkTreeGet (gpL3NDTbl, (tRedBlkElem *) & L3NDElem);

    if (pL3NDElem == NULL)
    {
        pL3NDElem = adap_malloc(sizeof(tNDEntry));
        if (pL3NDElem != NULL)
        {
    	    adap_memcpy(&(pL3NDElem->DstIp6), pIp6Addr, sizeof (tIp6Address));
            pL3NDElem->u2vlanId = u2VlanId;
            pL3NDElem->u4VrId = u4VrId;
            pL3NDElem->u4IfIndex = u4IfIdx;
            adap_memcpy(pL3NDElem->MacAddr, pMacAddr, sizeof (tMacAddr));
            u4Status = RedBlkTreeAdd (gpL3NDTbl, (tRedBlkElem *) pL3NDElem);
            if (u4Status == LPM_FAILURE)
            {
                ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
                    "Failed to add ND (%s) IfIndex %d \n", pIp6Addr, u4IfIdx);
                return (LPM_FAILURE);
            }
        }
    }
    else
    {
        pL3NDElem->u2vlanId = u2VlanId;
        pL3NDElem->u4IfIndex = u4IfIdx;
        adap_memcpy(pL3NDElem->MacAddr, pMacAddr, sizeof (tMacAddr));
    }
    return LPM_SUCCESS;
}

UINT4
Enet_Lpm_ND_Delete (UINT4 u4VrId, UINT1 *pIp6Addr)
{
    tRedBlkElem     *pRedBlkElem = NULL;
    tNDEntry        *pL3NDElem = NULL;
    tNDEntry         L3NDElem;
    
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
            "Enet_Lpm_ND_Delete DestIp(%s) \n", pIp6Addr);

    adap_memset(&L3NDElem, 0, sizeof (tNDEntry));
    adap_memcpy(&(L3NDElem.DstIp6), pIp6Addr, sizeof (tIp6Address));
    L3NDElem.u4VrId = u4VrId;

    pRedBlkElem = RedBlkTreeGet (gpL3NDTbl, (tRedBlkElem *) & L3NDElem);

    pL3NDElem = (tNDEntry *) pRedBlkElem;
    if (pL3NDElem != NULL)
    {
        RedBlkTreeRemove (gpL3NDTbl, pRedBlkElem);
        free (pL3NDElem);
        return LPM_SUCCESS;
    }
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "Failed to remove ND (%s)  \n", pIp6Addr);
    return LPM_FAILURE;
}


tNDEntry        *
Enet_Lpm_ND_Get (UINT4 u4VrId, UINT1 *pIp6Addr)
{
    tNDEntry        *pL3NDElem = NULL;
    tNDEntry         L3NDElem;
    
    adap_memset(&L3NDElem, 0, sizeof (tNDEntry));
    adap_memcpy(&(L3NDElem.DstIp6), pIp6Addr, sizeof (tIp6Address));
    L3NDElem.u4VrId = u4VrId;

    pL3NDElem = (tNDEntry *) 
            RedBlkTreeGet (gpL3NDTbl, (tRedBlkElem *) & L3NDElem);

    return pL3NDElem;
}



UINT4
Enet_Lpm_Ipv6Route_Add (tRtNextHopEntry  *prouteEntry)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtNextHopEntry    *pIp6Route=NULL;
    tIp6Address 	ZeroNH;
    tIp6Address 	Ip6Mask;

    adap_memset(&ZeroNH, 0, sizeof(tIp6Address));
    if ((memcmp(&prouteEntry->Ip6NextHop,&ZeroNH,sizeof(tIp6Address)) != 0) && 
        (Enet_Lpm_ND_Get (prouteEntry->u4VrId, 
				(prouteEntry->Ip6NextHop.u1_addr)) == NULL))
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %s to LPM database due to ARP miss\n",prouteEntry->Ip6Prefix);
        return LPM_FAILURE;
    }

    pIp6Route = (tRtNextHopEntry *)adap_malloc(sizeof(tRtNextHopEntry));
    if (pIp6Route == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %s to LPM database\n",prouteEntry->Ip6Prefix);
        return LPM_FAILURE;
    }

    adap_memset(pIp6Route, 0, sizeof(tRtNextHopEntry));
    adap_memset(&inParams, 0, sizeof (tInputParams));
    adap_memset(&outParams, 0, sizeof (tOutputParams));
    adap_memset((UINT1 *) &Ip6Mask, 0xFF, sizeof (tIp6Address));

    adap_memcpy(&pIp6Route->Ip6Prefix,&prouteEntry->Ip6Prefix,sizeof (tIp6Address));
    adap_memcpy(&pIp6Route->Ip6NextHop,&prouteEntry->Ip6NextHop,sizeof (tIp6Address));
    pIp6Route->u4IfIndex = prouteEntry->u4IfIndex;
    pIp6Route->u2VlanId = prouteEntry->u2VlanId;
    pIp6Route->u4VrId = prouteEntry->u4VrId;
    pIp6Route->u1AddrType = prouteEntry->u1AddrType;
    pIp6Route->u1PrefixLen = prouteEntry->u1PrefixLen;

    inParams.pRoot = gpLpmIp6RouteTbl;
    inParams.i1AppId = (prouteEntry->u4VrId) + 1;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = prouteEntry->u1PrefixLen;

    inParams.Key.pKey = (UINT1 *)calloc(sizeof (UINT2) * 16, 1);

    UtilIpv6CopyAddrBits ((tIp6Address *) (void *) inParams.Key.pKey,
                           &prouteEntry->Ip6Prefix, prouteEntry->u1PrefixLen);
    UtilIpv6CopyAddrBits ((tIp6Address *) (void *) (inParams.Key.pKey + 16),
                           &Ip6Mask, prouteEntry->u1PrefixLen);

    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if ((TrieDbAdd (&inParams, pIp6Route, &outParams)) != LPM_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %s to LPM database\n",prouteEntry->Ip6Prefix);
        return (LPM_FAILURE);
    }
    ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_INFO, 
		    "Enet_Lpm_Ipv6Route_Add DestIp(%s) succeeds\n",	prouteEntry->Ip6Prefix);
    
    return LPM_SUCCESS;
}


UINT4
Enet_Lpm_Ipv6Route_Delete (tRtNextHopEntry  *prouteEntry)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    tRtNextHopEntry    *pIp6Route=NULL;
    tIp6Address 	Ip6Mask;


    pIp6Route = (tRtNextHopEntry *)adap_malloc(sizeof(tRtNextHopEntry));
    if (pIp6Route == NULL)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %s to LPM database\n",prouteEntry->Ip6Prefix);
        return LPM_FAILURE;
    }

    adap_memset(pIp6Route, 0, sizeof(tRtNextHopEntry));
    adap_memset(&inParams, 0, sizeof(tInputParams));
    adap_memset(&outParams, 0, sizeof(tOutputParams));
    adap_memset((UINT1 *)&Ip6Mask, 0xFF, sizeof(tIp6Address));

    adap_memcpy(&pIp6Route->Ip6Prefix,&prouteEntry->Ip6Prefix,sizeof (tIp6Address));
    adap_memcpy(&pIp6Route->Ip6NextHop,&prouteEntry->Ip6NextHop,sizeof (tIp6Address));
    pIp6Route->u4IfIndex = prouteEntry->u4IfIndex;
    pIp6Route->u2VlanId = prouteEntry->u2VlanId;
    pIp6Route->u4VrId = prouteEntry->u4VrId;
    pIp6Route->u1AddrType = prouteEntry->u1AddrType;
    pIp6Route->u1PrefixLen = prouteEntry->u1PrefixLen;

    inParams.pRoot = gpLpmIp6RouteTbl;
    inParams.i1AppId = (prouteEntry->u4VrId) + 1;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = prouteEntry->u1PrefixLen;

    inParams.Key.pKey = (UINT1 *)calloc(sizeof (UINT2) * 16, 1);

    UtilIpv6CopyAddrBits ((tIp6Address *) (void *) inParams.Key.pKey,
                           &prouteEntry->Ip6Prefix, prouteEntry->u1PrefixLen);
    UtilIpv6CopyAddrBits ((tIp6Address *) (void *) (inParams.Key.pKey + 16),
                           &Ip6Mask, prouteEntry->u1PrefixLen);

    outParams.pAppSpecInfo = NULL;
    outParams.Key.pKey = NULL;

    if ((TrieDbRemove (&inParams, pIp6Route, &outParams)) != LPM_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG (ENET_ADAP_ERR, 
            "FAILED to install route Dest %s to LPM database\n",prouteEntry->Ip6Prefix);
        return (LPM_FAILURE);
    }
    return LPM_SUCCESS;
}



