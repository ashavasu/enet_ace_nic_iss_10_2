/*
 * adap_trie.h
 *
 *  Created on: Sep 16, 2018
 *      Author: devops
 */
#ifndef __ADAP_TRIE_H__
#define __ADAP_TRIE_H__

#include "adap_types.h"
#include "EnetHal_status.h"

typedef struct adap_trie adap_trie;

typedef struct adap_trie_element {  /* TODO - need to hide the struct content in some way */
	struct adap_trie_element *left;
	struct adap_trie_element *right;
	struct adap_trie_element *parent;

	ADAP_Bool glue_node;
	ADAP_Uint8 *prefix;
	ADAP_Uint32 prefix_length;
} adap_trie_element;

struct adap_trie_params {
	ADAP_Uint32 capacity;
};


/**
 * @brief Create a new LPC-Trie with given parameters.
 * @return
 */
EnetHal_Status_t adap_trie_init(adap_trie** trie, struct adap_trie_params *params);


/**
 * @brief Destroy LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_deinit(adap_trie* trie);


/**
 * @brief Insert {key,value} to LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_insert(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element* element);


/**
 * @brief Remove {key,value} from LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_remove(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element** element);


/**
 * @brief Find best match to {key,value} in LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_find_best(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element **element);


/**
 * @brief Find {key,value} in LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_find_exact(adap_trie* trie, ADAP_Uint8 *prefix, ADAP_Uint32 prefix_length, adap_trie_element **element);


/**
 * @brief Iterate {key,value} a LPC-Trie
 * @return
 */
EnetHal_Status_t adap_trie_iter(adap_trie* trie, ADAP_Uint8 *c_prefix, ADAP_Uint32 c_prefix_length, ADAP_Uint8 **prefix, ADAP_Uint32 *prefix_length, adap_trie_element **element);


/**
 * @brief Get number of {key,value} in LPC-Trie
 * @return
 */
ADAP_Uint32 adap_trie_size(adap_trie* trie);


#endif /* __ADAP_TRIE_H__ */
