#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "mea_drv_common.h"
#include "adap_enetConvert.h"
#include "adap_ecfmminp.h"
#include "adap_brgnp.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "EnetHal_L2_Api.h"

static pEcfmCcmOffHandleRxCallBack adap_EcfmHandlerCb = NULL;

static pthread_t adap_thread_EcfmHandler;
static pthread_t adap_idTimer_EcfmHandler;
static pthread_mutex_t cpu_mutex;

static tAdap_InterfaceRemoteSide tAdap_IntRmtSide[MAX_NUM_OF_SUPPORTED_PORTS];
static tAdap_PacketGenService PacketGenService[ADAP_D_MAX_DATABASE_CCM_CONFIGURATION];
static tAdap_PacketGenAction packetGenAction[ADAP_D_MAX_DATABASE_CCM_CONFIGURATION];
static tAdap_LbmPacketGen lbmPacketGenDb[ADAP_D_MAX_DATABASE_LBM_CONFIGURATION];
static tAdap_DmmPacketGen DmmPacketGenDb[ADAP_D_MAX_DATABASE_DMM_CONFIGURATION];
static t1Adap_DmPacketGen Dm1PacketGenDb[ADAP_D_MAX_DATABASE_DMM_CONFIGURATION];
static tAdap_LmmPacketGen LmmPacketGenDb[ADAP_D_MAX_DATABASE_LMM_CONFIGURATION];
static tAdap_LmIdConfig   lmidDb[ADAP_D_MAX_DATABASE_LMID_CONFIGURATION];
static tAdap_InternalVlan internalVlanDb[ADAP_D_MAX_CCM_INTERNAL_VLANS];
static tAdap_PacketGenData 	adap_packetBigGenData[ADAP_D_MAX_NUM_OF_GENERATORS];
static tAdap_PacketGenData	adap_packetSmallGenData[ADAP_D_MAX_NUM_OF_GENERATORS];
static tAdap_PacketGenPrfile adap_PacketGenPrfiles[ADAP_D_MAX_NUM_OF_GEN_PROFILES];
static ADAP_Uint32 adap_PacketCounterPerPort[MAX_NUM_OF_SUPPORTED_PORTS];
static tAdap_CondVarCcmMachine adap_CondVarCcmMachine;
static MEA_Bool arps_block_status[MAX_NUM_OF_SUPPORTED_PORTS];
static tAdap_ErpsVInstanceDb adap_ErpsInstance[ADAP_MAX_NUM_OF_MSTP_INSTANCES];
static ADAP_Bool adap_GenVpns[ADAP_MAX_NUM_OF_VPN_GENERATORS];
static tAdap_FdbFlushTimes fdbFulshTimesDb[MAX_NUM_OF_SUPPORTED_PORTS];
static tAdap_PacketAnalyzerData	adap_PacketAnalyzerData[ADAP_D_MAX_NUM_OF_ANALYZERS];
tAdap_ECFMCcmInst tAdap_EcfmCcmDb[ADAP_D_MAX_DATABASE_CCM_CONFIGURATION];

static void adap_initEcfmDatabase(void)
{
	ADAP_Int32 idx;
	ADAP_Int32 idx1;
	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		adap_memset(&tAdap_EcfmCcmDb[idx],0,sizeof(tAdap_ECFMCcmInst));
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.actionid = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.internalVlan = UNDEFINED_VALUE16;
		tAdap_EcfmCcmDb[idx].EcfmDbParams.num_of_subEntry=0;
		for(idx1=0;idx1<ADAP_D_MAX_OF_CCM_SUB_ENTRIES;idx1++)
		{
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.Service_id = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.actionid = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.packetGenId = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketGenDb.packetGenProfileId = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].enetPacketAnaDb.id_io = UNDEFINED_VALUE16;
			tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[idx1].internalVlan = UNDEFINED_VALUE16;
		}
	}


	adap_memset(&tAdap_IntRmtSide[0],0,sizeof(tAdap_InterfaceRemoteSide)*MAX_NUM_OF_SUPPORTED_PORTS);
	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		adap_memset(&fdbFulshTimesDb[idx].vpnCurrFlushTime[0],0,sizeof(ADAP_Uint32)*ADAP_MAX_NUM_OF_VPNS);
		adap_memset(&fdbFulshTimesDb[idx].vpnLastFlushTime[0],0,sizeof(ADAP_Uint32)*ADAP_MAX_NUM_OF_VPNS);

		tAdap_IntRmtSide[idx].tDmrDatabase.bConfigured = ADAP_FALSE;
		tAdap_IntRmtSide[idx].tLbrDatabase.bConfigured = ADAP_FALSE;
		tAdap_IntRmtSide[idx].tLmrDatabase.bConfigured = ADAP_FALSE;
		tAdap_IntRmtSide[idx].tApsDatabase.bConfigured = ADAP_FALSE;
		tAdap_IntRmtSide[idx].tRpsDatabase.bConfigured = ADAP_FALSE;
	}
	adap_memset(&PacketGenService[0],0,sizeof(tAdap_PacketGenService)*ADAP_D_MAX_DATABASE_CCM_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		PacketGenService[idx].valid=ADAP_FALSE;
	}
	adap_memset(&packetGenAction[0],0,sizeof(tAdap_PacketGenAction)*ADAP_D_MAX_DATABASE_CCM_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		packetGenAction[idx].valid=ADAP_FALSE;
	}
	adap_memset(&lbmPacketGenDb[0],0,sizeof(tAdap_LbmPacketGen)*ADAP_D_MAX_DATABASE_LBM_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_LBM_CONFIGURATION;idx++)
	{
		lbmPacketGenDb[idx].packetGen.PacketGeId = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.action_id = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.analyzer_action_id = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.serviceId = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.analyzerVpnIndex = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.tx_action_id = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].packetGen.rx_action_id = UNDEFINED_VALUE16;
		lbmPacketGenDb[idx].valid=ADAP_FALSE;
		lbmPacketGenDb[idx].packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	adap_memset(&DmmPacketGenDb[0],0,sizeof(tAdap_DmmPacketGen)*ADAP_D_MAX_DATABASE_DMM_CONFIGURATION);
	adap_memset(&Dm1PacketGenDb[0],0,sizeof(t1Adap_DmPacketGen)*ADAP_D_MAX_DATABASE_DMM_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_DMM_CONFIGURATION;idx++)
	{
		DmmPacketGenDb[idx].packetGen.PacketGeId = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.action_id = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.analyzer_action_id = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.serviceId = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.analyzerVpnIndex = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.tx_action_id = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].packetGen.rx_action_id = UNDEFINED_VALUE16;
		DmmPacketGenDb[idx].valid=ADAP_FALSE;
		DmmPacketGenDb[idx].packetGen.internalVlan = UNDEFINED_VALUE16;

		Dm1PacketGenDb[idx].packetGen.PacketGeId = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.action_id = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.analyzer_action_id = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.serviceId = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.analyzerVpnIndex = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.tx_action_id = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].packetGen.rx_action_id = UNDEFINED_VALUE16;
		Dm1PacketGenDb[idx].valid=ADAP_FALSE;
		Dm1PacketGenDb[idx].packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	adap_memset(&LmmPacketGenDb[0],0,sizeof(tAdap_LmmPacketGen)*ADAP_D_MAX_DATABASE_LMM_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_DMM_CONFIGURATION;idx++)
	{
		LmmPacketGenDb[idx].packetGen.PacketGeId = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.action_id = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.analyzer_action_id = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.serviceId = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.analyzerVpnIndex = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.tx_action_id = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].packetGen.rx_action_id = UNDEFINED_VALUE16;
		LmmPacketGenDb[idx].valid=ADAP_FALSE;
		LmmPacketGenDb[idx].packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	adap_memset(&lmidDb[0],0,sizeof(tAdap_LmIdConfig)*ADAP_D_MAX_DATABASE_LMID_CONFIGURATION);
	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMID_CONFIGURATION;idx++)
	{
		lmidDb[idx].valid=ADAP_FALSE;
		lmidDb[idx].lmId = idx+1;
		lmidDb[idx].vlanId=UNDEFINED_VALUE16;
		lmidDb[idx].u4IfIndex=0;
		lmidDb[idx].numOfOwners=0;
	}
	adap_memset(&internalVlanDb[0],0,sizeof(tAdap_InternalVlan)*ADAP_D_MAX_CCM_INTERNAL_VLANS);
	for(idx=0;idx<ADAP_D_MAX_CCM_INTERNAL_VLANS;idx++)
	{
		internalVlanDb[idx].valid = ADAP_FALSE;
		internalVlanDb[idx].Vlan = idx+2;
	}
	for(idx=0;idx<MAX_NUM_OF_SUPPORTED_PORTS;idx++)
	{
		arps_block_status[idx] = ADAP_FALSE;
	}
	for (idx = 0; idx < ADAP_D_MAX_NUM_OF_GEN_PROFILES; idx++)
	{
		adap_PacketGenPrfiles[idx].Id_io = UNDEFINED_VALUE16;
		adap_PacketGenPrfiles[idx].ref_count=0;
		adap_PacketGenPrfiles[idx].header_length = UNDEFINED_VALUE16;
	}
	for (idx = 0; idx < ADAP_D_MAX_NUM_OF_GENERATORS; idx++)
	{
		adap_packetBigGenData[idx].Id_io = UNDEFINED_VALUE16;
		adap_packetSmallGenData[idx].Id_io = UNDEFINED_VALUE16;
	}
	for(idx = 0; idx < ADAP_MAX_NUM_OF_VPN_GENERATORS; idx ++)
	{
		adap_GenVpns[idx] = MEA_FALSE;
	}
	for (idx = 0; idx < ADAP_D_MAX_NUM_OF_ANALYZERS; idx++)
	{
		adap_PacketAnalyzerData[idx].bFreeAnalyzer = MEA_TRUE;
	}
	adap_CondVarCcmMachine.event_flags=0;
	pthread_mutex_init(&adap_CondVarCcmMachine.cond_mtx, NULL);
    pthread_cond_init(&adap_CondVarCcmMachine.cond_var, NULL);
    pthread_mutex_init(&cpu_mutex, NULL);

}

MEA_Status adap_FsMiHwPacketGenActive(MEA_PacketGen_t  Id_io,MEA_Bool active)
{
	MEA_PacketGen_Entry_dbt         entry;


	if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,Id_io, &entry)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't get PacketGen \n");
        return MEA_ERROR;
    }

	entry.control.active = active;

	if(MEA_API_Set_PacketGen_Entry(MEA_UNIT_0,Id_io, &entry)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't set PacketGen \n");
        return MEA_ERROR;
    }

	return MEA_OK;

}

MEA_Status adap_FsMiHwDeleteRxForwarder(ADAP_Uint16 vlanId,ADAP_Uint16 VpnIndex, ADAP_Uint8 u1MdLevel,MEA_Uint32 op_code,MEA_Uint32 Src_port,MEA_Action_t *paction_id)
{
	MEA_SE_Entry_key_dbt key;
	MEA_SE_Entry_dbt     entry;
	ADAP_Uint32			 num_of_try=0;

	adap_memset(&key,0,sizeof(MEA_SE_Entry_key_dbt));
	key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
	key.oam_vpn.label_1 = vlanId;
	key.oam_vpn.VPN = VpnIndex;
	key.oam_vpn.MD_level = u1MdLevel;
	key.oam_vpn.op_code = op_code; //D_CCM_OPCODE_VALUE;
	key.oam_vpn.packet_is_untag_mpls = 0;
	key.oam_vpn.Src_port = Src_port;

	adap_memcpy(&entry.key,&key,sizeof(MEA_SE_Entry_key_dbt));

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteRxForwarder ===>entry not found vlanId:%d VpnIndex:%d u1MdLevel:%d op_code:%d srcPort:%d!!!\n",
								vlanId,VpnIndex,u1MdLevel,op_code,Src_port);

		return MEA_ERROR;
	}

	if(entry.data.actionId_valid  == MEA_TRUE)
	{
		(*paction_id) = entry.data.actionId;
	}

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwDeleteRxForwarder ===>entry not found vlanId:%d VpnIndex:%d u1MdLevel:%d op_code:%d!!!\n",
								vlanId,VpnIndex,u1MdLevel,op_code);
		return MEA_ERROR;
	}

	for(num_of_try=0;num_of_try<3;num_of_try++)
	{
		adap_usSleepFunc(20*1000);

		if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
		{
			return MEA_OK;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteRxForwarder ===> entry still found vlanId:%d VpnIndex:%d u1MdLevel:%d op_code:%d srcPort:%d try:%d!!!\n",
									vlanId,VpnIndex,u1MdLevel,op_code,Src_port,num_of_try);
		}
	}
	return MEA_OK;

}

static MEA_Status adap_FsMiHwCreateRxRecoveryAction(tAdap_ECFMCcmInst *ccmDb,MEA_Bool bSendToCpu)
{
	MEA_EHP_Info_dbt 		tEhp_info[2];
	int 					num_of_entries=0;
	MEA_OutPorts_Entry_dbt 	tOutPorts;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info[0],0,sizeof(MEA_EHP_Info_dbt)*2);

	tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmDbParams.enetPacketAnaDb.id_io;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,ADAP_D_ANALYZER_ETHERNITY_PORT);
	MEA_SET_OUTPORT(&tOutPorts, ADAP_D_ANALYZER_ETHERNITY_PORT);

	num_of_entries++;
	if(bSendToCpu == MEA_TRUE)
	{
		MEA_SET_OUTPORT(&tOutPorts, 127);
		tEhp_info[num_of_entries].ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info[num_of_entries].ehp_data.eth_info.val.vlan.vid      = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
		MEA_SET_OUTPORT(&tEhp_info[num_of_entries].output_info,127);
		tEhp_info[num_of_entries].ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info[num_of_entries].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		num_of_entries++;

		ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu = UNDEFINED_VALUE16;
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,
								&tEhp_info[0],
								num_of_entries,
								&tOutPorts,
								MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxRecoveryAction: failed to create action\n");
			return MEA_ERROR;
		}
	}
	else
	{
		ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana = UNDEFINED_VALUE16;
		if(adap_FsMiHwCreateCfmAction(&ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,
									&tEhp_info[0],
									num_of_entries,
									&tOutPorts,
									MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxRecoveryAction: failed to create action\n");
			return MEA_ERROR;
		}
	}
	return MEA_OK;
}


MEA_Status adap_FsMiHwSetRxForwarder(tAdap_ECFMCcmInst *ccmDb,eAnaTraffic packetToCpu,int entry_num,MEA_Bool bSilence)
{
	MEA_SE_Entry_dbt                      entry;
	MEA_Bool							  bExist=MEA_FALSE;
	ADAP_Uint32							    searchTry=0;
	MEA_Action_t 							get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t								enetPort;


	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu == packetToCpu)
		{
			return MEA_OK;
		}
	}
	else
	{
		if(ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu == packetToCpu)
		{
			return MEA_OK;
		}
	}

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
	entry.key.oam_vpn.label_1 = ccmDb->EcfmHwMaParams.u4VlanIdIsid;
	entry.key.oam_vpn.VPN = ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex;
	entry.key.oam_vpn.MD_level = ccmDb->EcfmDbParams.u1MdLevel;
	if(ccmDb->isDestMulticast == MEA_TRUE)
	{
		entry.key.oam_vpn.MD_level |= 0x08;
	}

	entry.key.oam_vpn.op_code = ADAP_D_CCM_OPCODE_VALUE;
	entry.key.oam_vpn.packet_is_untag_mpls = 0;
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
		entry.key.oam_vpn.Src_port = enetPort;
		if(	(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana == UNDEFINED_VALUE16) ||
			(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu == UNDEFINED_VALUE16) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"cpu_action:%d ana_action:%d\n",
									ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana,
									ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu);
			return MEA_OK;
		}
		entry.data.actionId = ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana;
		if(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu == PACKET_ANALYZER_SEND_TO_CPU)
		{
			entry.data.actionId = ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu;
		}
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetCcmAppDb.priodicSendPacketToCpu=5;
	}
	else
	{
		MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
		entry.key.oam_vpn.Src_port = enetPort;
		// if someone delete the actions
		if(	(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana == UNDEFINED_VALUE16) ||
			(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu == UNDEFINED_VALUE16) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"cpu_action:%d ana_action:%d\n",
									ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,
									ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu);
			return MEA_OK;
		}
		entry.data.actionId = ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana;
		if(ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu == PACKET_ANALYZER_SEND_TO_CPU)
		{
			entry.data.actionId = ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu;
		}
		ccmDb->EcfmDbParams.enetCcmAppDb.priodicSendPacketToCpu=5;

	}

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;

	for(searchTry=0;searchTry<5;searchTry++)
	{
		// check unicast MAC
		if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) == MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwSetRxForwarder : key:%d vpn:%d label:%d mdlvl:%d src_port:%d exist!!!\n",
																entry.key.type,
																entry.key.oam_vpn.VPN,
																entry.key.oam_vpn.label_1,
																entry.key.oam_vpn.MD_level,
																entry.key.oam_vpn.Src_port);
		    break;
		}
		// check if internal VLAN
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwSetRxForwarder:try:%d key:%d vpn:%d label:%d mdlvl:%d src_port:%d not exist!!!\n",
															searchTry,
															entry.key.type,
															entry.key.oam_vpn.VPN,
															entry.key.oam_vpn.label_1,
															entry.key.oam_vpn.MD_level,
															entry.key.oam_vpn.Src_port);
	}

	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
		if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
								entry.key.oam_vpn.MD_level,
								ADAP_D_CCM_OPCODE_VALUE,
								enetPort,
								&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwDeleteRxForwarder FAIL: L!!!\n");
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"delete up mep ifIndex:%d\n",ccmDb->EcfmDbParams.u4PortArray[entry_num]);
	}
	else
	{
		MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
		if(adap_FsMiHwDeleteRxForwarder(ccmDb->EcfmHwMaParams.u4VlanIdIsid,
								ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
								entry.key.oam_vpn.MD_level,
								ADAP_D_CCM_OPCODE_VALUE,
								enetPort,
								&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwDeleteRxForwarder FAIL: L!!!\n");
			return MEA_ERROR;
		}
	}


	adap_memset(&entry.data.OutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	MEA_SET_OUTPORT(&entry.data.OutPorts,ADAP_D_ANALYZER_ETHERNITY_PORT);



	if(packetToCpu == PACKET_ANALYZER_SEND_TO_CPU)
	{
		MEA_SET_OUTPORT(&entry.data.OutPorts,127);
	}


	entry.data.limiterId_valid=MEA_FALSE;

	if(packetToCpu == PACKET_ANALYZER_SEND_TO_CPU)
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
		{
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu,
												ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
												&entry.data.OutPorts,
												entry.key.oam_vpn.MD_level,
												ADAP_D_CCM_OPCODE_VALUE,
												ccmDb->EcfmHwMaParams.u4VlanIdIsid,
												enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder FAIL: L!!!\n");
				return MEA_ERROR;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"create up mep ifIndex:%d send to cpu actionId:%d\n",
										ccmDb->EcfmDbParams.u4PortArray[entry_num],
										ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_cpu);
		}
		else
		{
			if(MEA_API_IsExist_Action(MEA_UNIT_0,
										ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,
										&bExist) == MEA_OK)
			{
				if(bExist == MEA_FALSE)
				{
					// create action for send to CPU
					adap_FsMiHwCreateRxRecoveryAction(ccmDb,MEA_TRUE);
				}
			}
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_cpu,
												ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
												&entry.data.OutPorts,
												entry.key.oam_vpn.MD_level,
												ADAP_D_CCM_OPCODE_VALUE,
												ccmDb->EcfmHwMaParams.u4VlanIdIsid,
												enetPort)!=MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder FAIL: L!!!\n");
				return MEA_ERROR;
			}
		}
	}
	else
	{
		if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
		{
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.u4PortArray[entry_num], &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana,
												ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
												&entry.data.OutPorts,
												entry.key.oam_vpn.MD_level,
												ADAP_D_CCM_OPCODE_VALUE,
												ccmDb->EcfmHwMaParams.u4VlanIdIsid,
												enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder FAIL: L!!!\n");
				return MEA_ERROR;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"create up mep ifIndex:%d not send to cpu actionId:%d\n",
											ccmDb->EcfmDbParams.u4PortArray[entry_num],
											ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.actionid_send_to_ana);
		}
		else
		{
			if(MEA_API_IsExist_Action(MEA_UNIT_0,
										ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,
										&bExist) == MEA_OK)
			{
				if(bExist == MEA_FALSE)
				{
					// create action for send to CPU
					adap_FsMiHwCreateRxRecoveryAction(ccmDb,MEA_FALSE);
				}
			}
			MeaAdapGetPhyPortFromLogPort (ccmDb->EcfmDbParams.enetPacketAnaDb.ifIndex, &enetPort);
			if(adap_FsMiHwCreateRxForwarder(ccmDb->EcfmDbParams.enetPacketAnaDb.actionid_send_to_ana,
												ccmDb->EcfmDbParams.enetPacketAnaDb.VpnIndex,
												&entry.data.OutPorts,
												entry.key.oam_vpn.MD_level,
												ADAP_D_CCM_OPCODE_VALUE,
												ccmDb->EcfmHwMaParams.u4VlanIdIsid,
												enetPort) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder FAIL: L!!!\n");
				return MEA_ERROR;
			}
		}
	}
	if(ccmDb->EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		ccmDb->EcfmDbParams.tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu = packetToCpu;
	}
	else
	{
		ccmDb->EcfmDbParams.enetPacketAnaDb.sendPacketToCpu = packetToCpu;
	}

	if(bSilence == MEA_FALSE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"================> packet to :%s <============\n",packetToCpu==PACKET_ANALYZER_SEND_TO_CPU?"CPU":"ANA");
	}
	return MEA_OK;
}

MEA_Status adap_FsMiHwCreateCfmAction(MEA_Action_t *pAction_id,MEA_EHP_Info_dbt *pEhp_info,int num_of_entries,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Ingress_TS_type_t type)
{
	MEA_Action_Entry_Data_dbt               Action_data;
	MEA_Policer_Entry_dbt                   Action_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt   	Action_editing;
	//ADAP_Uint32								num_of_try;

	adap_memset(&Action_data,0,sizeof(MEA_Action_Entry_Data_dbt));
	adap_memset(&Action_policer,0,sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&Action_editing,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));

	(*pAction_id) = MEA_PLAT_GENERATE_NEW_ID;

	Action_data.proto_llc_valid = MEA_TRUE;
	Action_data.Llc = 0;
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.Protocol = 1;
	Action_data.ed_id_valid  =  1;
	Action_data.ed_id        =  0;
	Action_data.pm_id_valid = 1;
	Action_data.pm_id        =  0;
	Action_data.fwd_ingress_TS_type = type;
	Action_data.tm_id        =  0;
	Action_data.tm_id_valid=1;

	Action_policer.CIR = 100000000;  //100M
	Action_policer.CBS = 32000;
	Action_policer.EIR = 0;
	Action_policer.EBS = 0;
	Action_policer.comp = 0;
	Action_policer.gn_type = 1;

	Action_editing.ehp_info = pEhp_info;
	Action_editing.num_of_entries = num_of_entries;

	/*for(num_of_try=0;num_of_try<3;num_of_try++)
	{
		if(MEA_Action_drv_type != MEA_ACTION_TYPE_FWD)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MEA_Action_drv_type:%d try:%d\n",MEA_Action_drv_type,num_of_try);
			adap_usSleepFunc(20*1000);
		}
		else
		{
			break;
		}
	}*/
	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   pOutPorts,
							   &Action_policer,
							   &Action_editing,
							   pAction_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwCreateForwarderCfmAction FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n",(*pAction_id));
	   return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"succeeded to create action:%d command:%d vlanid:%d num of entries:%d\n",
											(*pAction_id),
											Action_editing.ehp_info->ehp_data.eth_info.command,
											Action_editing.ehp_info->ehp_data.eth_info.val.vlan.vid,
											Action_editing.num_of_entries);
		return MEA_OK;
}

MEA_Status adap_FsMiHwDeleteTxForwarder(tEnetHal_MacAddr *pMac,ADAP_Uint16 VpnIndex)
{
	MEA_SE_Entry_key_dbt key;
	MEA_SE_Entry_dbt                      entry;

	adap_memset(&entry, 0, sizeof(entry));


	key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, key.mac_plus_vpn.MAC.b);
	key.mac_plus_vpn.VPN = VpnIndex;

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	adap_mac_to_arr(pMac, entry.key.mac_plus_vpn.MAC.b);

	entry.key.mac_plus_vpn.VPN = VpnIndex;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;

	if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwDeleteTxForwarder entry not found!!!\n");
		return MEA_OK;
	}

	if(ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,&key) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwDeleteTxForwarder entry not found!!!\n");
		return MEA_OK;
	}
	return MEA_OK;


}

MEA_Status adap_FsMiHwCreateRxForwarder(MEA_Action_t action_id,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts,MEA_Uint32  MD_level,MEA_Uint32 op_code,MEA_Uint16 vlanId,MEA_Uint32 Src_port)
{
	MEA_SE_Entry_dbt                      	entry;
	ADAP_Uint32									num_of_try;

	adap_memset(&entry, 0, sizeof(entry));

	entry.key.type = MEA_SE_FORWARDING_KEY_TYPE_OAM_VPN;
	entry.key.oam_vpn.label_1 = vlanId;
	entry.key.oam_vpn.VPN = VpnIndex;
	entry.key.oam_vpn.MD_level = MD_level;
	entry.key.oam_vpn.op_code = op_code;
	entry.key.oam_vpn.packet_is_untag_mpls = 0;
	entry.key.oam_vpn.Src_port = Src_port;

	entry.data.aging = 3; //Default
	entry.data.static_flag  = MEA_TRUE;
	entry.data.actionId_valid  = MEA_TRUE;

	entry.data.actionId = action_id;

	adap_memcpy(&entry.data.OutPorts,pOutPorts,sizeof(MEA_OutPorts_Entry_dbt));

   if(ENET_ADAPTOR_Create_SE_Entry (MEA_UNIT_0, &entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateRxForwarder FAIL: ENET_ADAPTOR_Create_SE_Entry FAIL!!!\n");
		return MEA_ERROR;
	}

	for(num_of_try=0;num_of_try<3;num_of_try++)
	{

		adap_usSleepFunc(20*1000);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateRxForwarder with opcode:%d\n",op_code);

		if(ENET_ADAPTOR_Get_SE_Entry(MEA_UNIT_0,&entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"===================> key opCode:%d src_port:%d not exist num of time:%d!!!\n",
																op_code,Src_port,num_of_try);
		}
		else
		{
			break;
		}
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateRxForwarder :entry created vlanId:%d VpnIndex:%d u1MdLevel:%d op_code:%d src_port:%d!!!\n",
							vlanId,VpnIndex,MD_level,op_code,Src_port);


	return MEA_OK;
}

static tAdap_DmmPacketGen *adap_getDmmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_DMM_CONFIGURATION;idx++)
	{
		if( (DmmPacketGenDb[idx].valid==MEA_TRUE) &&
			(DmmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				return &DmmPacketGenDb[idx];
			}
	}
	return NULL;
}


static ADAP_Uint32 adap_freeDmmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_DMM_CONFIGURATION;idx++)
	{
		if( (DmmPacketGenDb[idx].valid==MEA_TRUE) &&
			(DmmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				DmmPacketGenDb[idx].valid = MEA_FALSE;
				return ENET_SUCCESS;
			}
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 adap_allocDmmPacketGenEntry(tEnetLbmPacketGen *packetGen,tAdap_EcfmMepDmmInfoParams *pEcfmMepInfoParams)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_DMM_CONFIGURATION;idx++)
	{
		if(DmmPacketGenDb[idx].valid==MEA_FALSE)
		{
			adap_memcpy(&DmmPacketGenDb[idx].packetGen,packetGen,sizeof(tEnetDmmPacketGen));
			adap_memcpy(&DmmPacketGenDb[idx].tEcfmMepInfoParams,pEcfmMepInfoParams,sizeof(tAdap_EcfmMepDmmInfoParams));
			DmmPacketGenDb[idx].valid=MEA_TRUE;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_allocLbmPacketGenEntry not enough room\n");
	return ENET_FAILURE;
}

ADAP_Uint16 adap_AllocateNewGenVpnId(void)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_MAX_NUM_OF_VPN_GENERATORS;idx++)
	{
		if(adap_GenVpns[idx] == ADAP_FALSE)
		{
			adap_GenVpns[idx] = ADAP_TRUE;
			return ADAP_START_VPN_GENERATORS+idx;
		}
	}
	return UNDEFINED_VALUE16;
}

ADAP_Uint32 adap_FreeGenVpnId(ADAP_Uint16 vpnId)
{
	ADAP_Uint32 idx;

	if( (vpnId < ADAP_START_VPN_GENERATORS) || (vpnId > 511) )
	{
		return ENET_FAILURE;
	}

	idx = vpnId-ADAP_START_VPN_GENERATORS;
	adap_GenVpns[idx] = MEA_FALSE;
	return ENET_SUCCESS;
}

ADAP_Uint16 adap_allocateInternalVlanId(void)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_D_MAX_CCM_INTERNAL_VLANS;idx++)
	{
		if(internalVlanDb[idx].valid == MEA_FALSE)
		{
			internalVlanDb[idx].valid = MEA_TRUE;
			return internalVlanDb[idx].Vlan;
		}
	}
	return UNDEFINED_VALUE16;
}

void adap_freeInternalVlanId(ADAP_Uint16 VlanId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_D_MAX_CCM_INTERNAL_VLANS;idx++)
	{
		if( (internalVlanDb[idx].valid==MEA_TRUE) && (internalVlanDb[idx].Vlan == VlanId) )
		{
			internalVlanDb[idx].valid=MEA_FALSE;
		}
	}
}

ADAP_Uint32 adap_freePacketSmallGenTable(ADAP_Uint16 Id_io)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GENERATORS;idx++)
	{
		if(adap_packetSmallGenData[idx].Id_io == Id_io)
		{
			adap_packetSmallGenData[idx].Id_io = UNDEFINED_VALUE16;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_AllocPacketSmallGenTable(ADAP_Uint16 Id_io)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GENERATORS;idx++)
	{
		if(adap_packetSmallGenData[idx].Id_io == UNDEFINED_VALUE16)
		{
			adap_packetSmallGenData[idx].Id_io = Id_io;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

MEA_Bool adap_isPacketBigGenTableFull(void)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GENERATORS;idx++)
	{
		if(adap_packetBigGenData[idx].Id_io == UNDEFINED_VALUE16)
		{
			return MEA_FALSE;
		}
	}
	return MEA_TRUE;
}

ADAP_Uint32 adap_freePacketBigGenTableFull(ADAP_Uint16 Id_io)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GENERATORS;idx++)
	{
		if(adap_packetBigGenData[idx].Id_io == Id_io)
		{
			adap_packetBigGenData[idx].Id_io = UNDEFINED_VALUE16;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_AllocPacketBigGenTableFull(ADAP_Uint16 Id_io)
{
	ADAP_Uint32 idx;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GENERATORS;idx++)
	{
		if(adap_packetBigGenData[idx].Id_io == UNDEFINED_VALUE16)
		{
			adap_packetBigGenData[idx].Id_io = Id_io;
			return ENET_SUCCESS;
		}
	}
	return ENET_FAILURE;
}

ADAP_Uint32 adap_freePacketGenProfile(ADAP_Uint32 Id_io,MEA_Bool *freeProfile)
{
	ADAP_Uint32 idx=0;

	(*freeProfile) = MEA_FALSE;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GEN_PROFILES;idx++)
	{
		if(adap_PacketGenPrfiles[idx].Id_io == Id_io)
		{
			adap_PacketGenPrfiles[idx].ref_count--;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"profile:%d ref_count:%d\r\n",Id_io,adap_PacketGenPrfiles[idx].ref_count);
			if(adap_PacketGenPrfiles[idx].ref_count == 0)
			{
				(*freeProfile) = MEA_TRUE;
				adap_PacketGenPrfiles[idx].Id_io=UNDEFINED_VALUE16;
				adap_PacketGenPrfiles[idx].header_length = UNDEFINED_VALUE16;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"profile:%d ref_count free\r\n",Id_io);
			}
			return idx;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"packet generator:%d not found\r\n",Id_io);
	return UNDEFINED_VALUE16;
}

ADAP_Uint32 adap_allocPacketGenProfile(ADAP_Uint32 header_length,ADAP_Uint32 Id_io)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GEN_PROFILES;idx++)
	{
		if( (adap_PacketGenPrfiles[idx].Id_io != UNDEFINED_VALUE16) &&
			(adap_PacketGenPrfiles[idx].header_length == header_length) )
		{
			adap_PacketGenPrfiles[idx].ref_count++;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"header:%lu ref_count:%d \r\n",header_length,adap_PacketGenPrfiles[idx].ref_count);
			return adap_PacketGenPrfiles[idx].Id_io;
		}
	}
	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GEN_PROFILES;idx++)
	{
		if(adap_PacketGenPrfiles[idx].Id_io == UNDEFINED_VALUE16)
		{
			adap_PacketGenPrfiles[idx].ref_count=1;
			adap_PacketGenPrfiles[idx].Id_io = Id_io;
			adap_PacketGenPrfiles[idx].header_length = header_length;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"new header:%lu id:%d \r\n",header_length,Id_io);
			return idx;
		}
	}
	return UNDEFINED_VALUE16;
}

ADAP_Uint32 adap_getPacketGenProfile(ADAP_Uint32 header_length)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GEN_PROFILES;idx++)
	{
		if( (adap_PacketGenPrfiles[idx].Id_io != UNDEFINED_VALUE16) &&
			(adap_PacketGenPrfiles[idx].header_length == header_length) )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"header:%d id:%d\r\n",header_length,adap_PacketGenPrfiles[idx].Id_io);
			adap_PacketGenPrfiles[idx].ref_count++;
			return adap_PacketGenPrfiles[idx].Id_io;
		}
	}
	return UNDEFINED_VALUE16;
}

MEA_Bool adap_isPacketGenProfileTableFull(void)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_GEN_PROFILES;idx++)
	{
		if(adap_PacketGenPrfiles[idx].Id_io == UNDEFINED_VALUE16)
		{
			return MEA_FALSE;
		}
	}
	return MEA_TRUE;
}

static ADAP_Uint32 adap_PacketAnalyzerAllocTable(void)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_NUM_OF_ANALYZERS;idx++)
	{
		if(adap_PacketAnalyzerData[idx].bFreeAnalyzer == MEA_TRUE)
		{
			adap_PacketAnalyzerData[idx].bFreeAnalyzer = MEA_FALSE;
			return idx;
		}
	}
	return UNDEFINED_VALUE16;
}

MEA_Status adap_FsMiHwCreateCfmService(MEA_Service_t *pService_id,tRmtSrvInfo *srcInfo)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_OutPorts_Entry_dbt 				  OutPorts;
	MEA_EHP_Info_dbt 					  tInfo;
	MEA_Policer_Entry_dbt                 Service_policer;

	srcInfo->vpn=adap_AllocateNewGenVpnId();


	adap_memset(&Service_editing , 0 , sizeof(MEA_EgressHeaderProc_Array_Entry_dbt ));
	adap_memset(&Service_key    , 0 , sizeof(MEA_Service_Entry_Key_dbt ));
	adap_memset(&Service_data , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	adap_memset(&OutPorts    , 0 , sizeof(MEA_OutPorts_Entry_dbt ));
	adap_memset(&tInfo    , 0 , sizeof(MEA_EHP_Info_dbt ));
	adap_memset(&Service_policer    , 0 , sizeof(MEA_Policer_Entry_dbt ));


	Service_data.policer_prof_id = srcInfo->EnetPolilcingProfile;
     if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                        srcInfo->EnetPolilcingProfile,
                                        0,/* ACM_Mode */
                                        &Service_policer) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateCfmService Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile:%d failed\n",srcInfo->EnetPolilcingProfile);
        return MEA_ERROR;
    }


	Service_key.src_port         = srcInfo->src_port;
	Service_key.L2_protocol_type =MEA_PARSING_L2_KEY_Ctag; /* Ethertypes: 88a8/8100*/
	Service_key.net_tag          = srcInfo->vlanId; //configure vlan
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.evc_enable = MEA_TRUE;
	Service_key.pri              = 0;

	Service_data.DSE_forwarding_enable   = MEA_TRUE;
	Service_data.DSE_forwarding_key_type = srcInfo->keyType;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;

	Service_data.CFM_OAM_ME_Level_Threshold_valid = srcInfo->me_valid;
	Service_data.CFM_OAM_ME_Level_Threshold_value = srcInfo->me_level;



	Service_data.vpn = srcInfo->vpn; //vpn

    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;


	(*pService_id) = MEA_PLAT_GENERATE_NEW_ID;

	Service_editing.num_of_entries = 1;

	tInfo.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	tInfo.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_TRANS;
	tInfo.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tInfo.ehp_data.eth_info.val.vlan.vid      = 0;

	tInfo.ehp_data.eth_info.stamp_color       = MEA_FALSE;
	tInfo.ehp_data.eth_info.stamp_priority    = MEA_FALSE;

	Service_editing.ehp_info = &tInfo;

    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               &OutPorts,
                               &Service_policer,
                               &Service_editing,
                               pService_id) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateCfmService FAIL: Unable to create the new service %d\n", (*pService_id));
        /*T.B.D. Rollback some of the previous actions if fail! */
        return MEA_ERROR;

    }

	return (MEA_OK);
}

MEA_Status adap_FsMiHwGetInterfaceCfmOamUCMac (ADAP_Int32 i4Port,tEnetHal_MacAddr *macAddr)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t				enetPort;

	MeaAdapGetPhyPortFromLogPort (i4Port, &enetPort);

	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwGetInterfaceCfmOamUCMac: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	adap_mac_from_arr(macAddr, entry.parser_info.my_Mac.b);

    return (MEA_OK);
}

MEA_Status adap_FsMiHwSetInterfaceCfmOamUCMac (ADAP_Int32 i4Port,tEnetHal_MacAddr *macAddr)
{
 	MEA_IngressPort_Entry_dbt entry;
	MEA_Port_t				enetPort;

	MeaAdapGetPhyPortFromLogPort (i4Port, &enetPort);

	if (MEA_API_Get_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwSetInterfaceCfmOamUCMac: MEA_API_Get_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	adap_mac_to_arr(macAddr, entry.parser_info.my_Mac.b);

	if (MEA_API_Set_IngressPort_Entry(MEA_UNIT_0, enetPort, &entry) != MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwSetInterfaceCfmOamUCMac: MEA_API_Set_IngressPort_Entry FAIL\n");
		return MEA_ERROR;
	}

	return (MEA_OK);
}


MEA_Status adap_FsMiHwPacketGenProfInfoCreate(int header_length,MEA_PacketGen_profile_info_t  *pId_io,MEA_Uint32 stream_type)
{
    MEA_PacketGen_drv_Profile_info_entry_dbt      entry;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_drv_Profile_info_entry_dbt));

	entry.header_length = header_length;
	entry.stream_type = stream_type; //CCM=3 LBM/LBR=1 DMM/DMR=2
	if( (stream_type == MEA_PACKETGEN_STREAM_TYPE_CCM) || (stream_type == MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) )
	{
		entry.seq_stamping_enable  = 1;
	}
	else
	{
		entry.seq_stamping_enable  = 0;
	}
	entry.seq_offset_type     = MEA_PACKETGEN_SEQ_TYPE_2;



	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;

   if (MEA_API_Create_PacketGen_Profile_info(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Create_PacketGen_Profile_info fail\n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

static ADAP_Uint32 adap_freeLmmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMM_CONFIGURATION;idx++)
	{
		if( (LmmPacketGenDb[idx].valid==MEA_TRUE) &&
			(LmmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				LmmPacketGenDb[idx].valid = MEA_FALSE;
				return ENET_SUCCESS;
			}
	}
	return ENET_FAILURE;
}

static ADAP_Uint32 adap_allocLmmPacketGenEntry(tEnetLbmPacketGen *packetGen,tAdap_EcfmMepLmmInfoParams *pEcfmMepInfoParams)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMM_CONFIGURATION;idx++)
	{
		if(LmmPacketGenDb[idx].valid==MEA_FALSE)
		{
			adap_memcpy(&LmmPacketGenDb[idx].packetGen,packetGen,sizeof(tEnetLmmPacketGen));
			adap_memcpy(&LmmPacketGenDb[idx].tEcfmMepInfoParams,pEcfmMepInfoParams,sizeof(tAdap_EcfmMepLmmInfoParams));
			LmmPacketGenDb[idx].valid=MEA_TRUE;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_allocLmmPacketGenEntry not enough room\n");
	return ENET_FAILURE;
}

static MEA_Status adap_clear_ccm_counter(MEA_CcmId_t Id)
{
	MEA_Bool exist;

   if (MEA_API_IsExist_CCM(MEA_UNIT_0,
							 Id,
							 &exist) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_collect_ccm_counter failed get MEA_API_IsExist_CCM\n");
		return MEA_ERROR;
	}

	if(exist == MEA_TRUE)
	{
		if (MEA_API_Clear_Counters_CCM(MEA_UNIT_0,Id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Collect_Counters_CCM failed for id %d \n",Id);
            return MEA_ERROR;
        }
	}
	return MEA_OK;
}

static MEA_Status adap_collect_ccm_counter(MEA_CcmId_t Id)
{
	MEA_Bool exist;

   if (MEA_API_IsExist_CCM(MEA_UNIT_0,
							 Id,
							 &exist) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_collect_ccm_counter failed get MEA_API_IsExist_CCM\n");
		return MEA_ERROR;
	}

	if(exist == MEA_TRUE)
	{
		if (MEA_API_Collect_Counters_CCM(MEA_UNIT_0,Id,NULL) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Collect_Counters_CCM failed for id %d \n",Id);
            return MEA_ERROR;
        }
	}
	return MEA_OK;
}

MEA_Status adap_read_ccm_counter(MEA_CcmId_t Id,MEA_Counters_CCM_Defect_dbt *entry)
{
	MEA_Bool exist;

   if (MEA_API_IsExist_CCM(MEA_UNIT_0,
							 Id,
							 &exist) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_read_ccm_counter failed get MEA_API_IsExist_CCM\n");
		return MEA_ERROR;
	}

	if(exist == MEA_TRUE)
	{
		if (MEA_API_Get_Counters_CCM(MEA_UNIT_0,
									Id,
									entry) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_read_ccm_counter failed get MEA_API_Get_Counters_CCM\n");
			return MEA_ERROR;
		}
	}
	return MEA_OK;
}

void adap_freeLmId(ADAP_Uint32 lmId)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMID_CONFIGURATION;idx++)
	{
		if( (lmidDb[idx].valid==MEA_TRUE) && (lmidDb[idx].lmId == lmId) )
		{
			lmidDb[idx].numOfOwners--;
			if(lmidDb[idx].numOfOwners == 0)
			{
				lmidDb[idx].valid=MEA_FALSE;
			}
		}
	}
}

ADAP_Uint32 adap_allocateLmId(ADAP_Uint16 vlanId,ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;
	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMID_CONFIGURATION;idx++)
	{
		if( (lmidDb[idx].valid==MEA_TRUE) && (lmidDb[idx].vlanId == vlanId) && (lmidDb[idx].u4IfIndex == u4IfIndex) )
		{
			lmidDb[idx].numOfOwners++;
			return lmidDb[idx].lmId;
		}
	}

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMID_CONFIGURATION;idx++)
	{
		if(lmidDb[idx].valid==MEA_FALSE)
		{
			lmidDb[idx].valid=MEA_TRUE;
			lmidDb[idx].numOfOwners=1;
			lmidDb[idx].vlanId = vlanId;
			lmidDb[idx].u4IfIndex = u4IfIndex;
			return lmidDb[idx].lmId;
		}
	}
	return UNDEFINED_VALUE16;
}

static MEA_Status adap_FsMiHwPacketGenLmmCreateiNFO(tAdap_PacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond)
{
	MEA_PacketGen_Entry_dbt entry;
	MEA_Uint32  oamInfo=0;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =MEA_PACKET_GEN_HEADER_TYPE_1; //120 bytes

	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->DA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //DA
	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->SA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //SA

	if(pPacketGen->num_of_vlans)
	{
		adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pPacketGen->num_of_vlans > 1)
		{
			adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pPacketGen->meg_level & 0x000007) << 5);
	oamInfo |=  (pPacketGen->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->flags & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->tlvOffset & 0xFF;

	// TXTimestamp
	entry.header.length	+= sizeof(MEA_Uint32);

	// Reserved for LMM receiving equipment (0)
	entry.header.length	+= sizeof(MEA_Uint32);

	// Reserved for LMM
	entry.header.length	+= sizeof(MEA_Uint32);

	// Reserved for LMM receiving equipment
	entry.header.length	+= sizeof(MEA_Uint32);


	entry.header.length++; //end of tlv

	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

void adap_usSleepFunc(ADAP_Uint32 usec)
{
	MEA_OS_sleep(0,usec);
}

static ADAP_Uint32 adap_signal_condtimer_eventFlag(void)
{
	return adap_CondVarCcmMachine.event_flags;
}

static void adap_signal_condtimer_event(void)
{
    pthread_mutex_lock(&adap_CondVarCcmMachine.cond_mtx);
    adap_CondVarCcmMachine.event_flags |= ADAP_FLAG_EVENT_TIMER;
    pthread_cond_signal(&adap_CondVarCcmMachine.cond_var);
    pthread_mutex_unlock(&adap_CondVarCcmMachine.cond_mtx);
}

void adap_signal_condtimer_clearFlag(ADAP_Uint32 flag)
{
	adap_CondVarCcmMachine.event_flags ^= flag;
}

void adap_signal_condMutexLock_ccm(void)
{
	pthread_mutex_lock(&adap_CondVarCcmMachine.cond_mtx);
}

void adap_signal_condMutexUnLock_ccm(void)
{
	pthread_mutex_unlock(&adap_CondVarCcmMachine.cond_mtx);
}

void adap_signal_condWait_ccm(void)
{
	pthread_cond_wait(&adap_CondVarCcmMachine.cond_var, &adap_CondVarCcmMachine.cond_mtx);
}

void adap_cfaLockMutex(void)
{
    pthread_mutex_lock(&cpu_mutex);
}
void adap_cfaUnlockMutex(void)
{
	pthread_mutex_unlock(&cpu_mutex);
}



static tAdap_LmmPacketGen *adap_getLmmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMM_CONFIGURATION;idx++)
	{
		if( (LmmPacketGenDb[idx].valid==MEA_TRUE) &&
			(LmmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				return &LmmPacketGenDb[idx];
			}
	}
	return NULL;
}

static MEA_Status adap_FsMiDeleteLmmPacketGen(tAdap_LmmPacketGen *pPacketGen)
{
	MEA_Bool bValid=MEA_FALSE;
	MEA_Action_t get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t	 enetPort;


	if(pPacketGen->packetGen.serviceId != UNDEFINED_VALUE16)
	{
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, pPacketGen->packetGen.serviceId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: Unable to delete the service %d\n", pPacketGen->packetGen.serviceId);
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",pPacketGen->packetGen.serviceId);
		pPacketGen->packetGen.serviceId = UNDEFINED_VALUE16;
	}
	adap_FreeGenVpnId(pPacketGen->packetGen.VpnIndex);

	if(pPacketGen->packetGen.PacketGeId != UNDEFINED_VALUE16)
	{
		if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,pPacketGen->packetGen.PacketGeId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: Unable to delete the packetGen %d\n", pPacketGen->packetGen.PacketGeId);
			return MEA_ERROR;
		}
		adap_freePacketSmallGenTable(pPacketGen->packetGen.PacketGeId);
		pPacketGen->packetGen.PacketGeId = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteLmmPacketGen: profileId=%d\n",pPacketGen->packetGen.packetGenProfileId);
		adap_freePacketGenProfile(pPacketGen->packetGen.packetGenProfileId,&bValid);
		if(bValid == MEA_TRUE)
		{
			if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pPacketGen->packetGen.packetGenProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: Unable to delete the packetGen profile:%d\n", pPacketGen->packetGen.packetGenProfileId);
				return MEA_ERROR;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteLmmPacketGen: profileId=%d deleted\n",pPacketGen->packetGen.packetGenProfileId);
			pPacketGen->packetGen.packetGenProfileId = UNDEFINED_VALUE16;
		}
	}
	if(pPacketGen->packetGen.internalVlan != UNDEFINED_VALUE16)
	{
		if(adap_FsMiHwDeleteRxForwarder(pPacketGen->packetGen.internalVlan,
									pPacketGen->packetGen.VpnIndex,
									pPacketGen->tEcfmMepInfoParams.meLevel,
									ADAP_D_DMM_OPCODE_VALUE,
									120,
									&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen: delete forwarder: entry not found\n");
		}
		adap_freeInternalVlanId(pPacketGen->packetGen.internalVlan);
		pPacketGen->packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	MeaAdapGetPhyPortFromLogPort (pPacketGen->tEcfmMepInfoParams.u4IfIndex, &enetPort);
	if(adap_FsMiHwDeleteRxForwarder(pPacketGen->tEcfmMepInfoParams.VlanTag.u2VlanId,
								pPacketGen->packetGen.analyzerVpnIndex,
								pPacketGen->tEcfmMepInfoParams.meLevel,
								ADAP_D_DMR_OPCODE_VALUE,
								enetPort,
								&get_paction_id)!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen: delete forwarder: entry not found\n");
	}
	if(adap_FsMiHwDeleteTxForwarder(&pPacketGen->tEcfmMepInfoParams.destMac,pPacketGen->packetGen.analyzerVpnIndex) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen: delete forwarder: entry not found\n");
	}

	if(adap_FsMiHwDeleteTxForwarder(&pPacketGen->tEcfmMepInfoParams.srcMac,pPacketGen->packetGen.analyzerVpnIndex) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen: delete forwarder: entry not found\n");
	}

	if(pPacketGen->packetGen.action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.action_id = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.analyzer_action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.analyzer_action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.analyzer_action_id = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.rx_action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.rx_action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.rx_action_id = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.tx_action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.tx_action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteLmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.tx_action_id = UNDEFINED_VALUE16;
	}

	if(pPacketGen->packetGen.id_io != UNDEFINED_VALUE16)
	{
		if( MEA_API_Delete_CCM_Entry(MEA_UNIT_0,pPacketGen->packetGen.id_io) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsDeletePacketAnalyzer: failed to delete MEA_API_Delete_CCM_Entry\r\n");
			return ENET_FAILURE;
		}
	}
	pPacketGen->packetGen.id_io=UNDEFINED_VALUE16;
	pPacketGen->packetGen.analyzerId = UNDEFINED_VALUE16;
	adap_freeLmmPacketGenEntry(pPacketGen->tEcfmMepInfoParams.u4IfIndex);
	adap_freeLmId(pPacketGen->tEcfmMepInfoParams.lmId);
	return MEA_OK;
}

static MEA_Status adap_FsMiCompareLmmPacketGen(tAdap_LmmPacketGen *pPacketGen,tAdap_EcfmMepLmmInfoParams *pDmmInfo,MEA_Bool *delEntry)
{
	(*delEntry) = MEA_FALSE;
	if( (pPacketGen->tEcfmMepInfoParams.VlanTag.u2VlanId != pDmmInfo->VlanTag.u2VlanId) ||
	  (pPacketGen->tEcfmMepInfoParams.meLevel != pDmmInfo->meLevel) ||
	  (adap_memcmp(&pPacketGen->tEcfmMepInfoParams.destMac,&pDmmInfo->destMac,sizeof(tEnetHal_MacAddr)) != 0) )
	  {
		  (*delEntry) = MEA_TRUE;
		  return adap_FsMiDeleteLmmPacketGen(pPacketGen);
	  }
	return MEA_OK;
}

ADAP_Int32 adap_MiVlanSetServiceMdLevel (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,ADAP_Uint16 level)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt				  OutPorts_Entry;
    MEA_Policer_Entry_dbt          		  Policer_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
	MEA_Service_t   					  Service_id;
	tServiceDb 							  *pServiceId=NULL;

    pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,u4IfIndex,u1IsTagged);
    if (pServiceId == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId from ifIndex:%d vlanId:%d tagged:%d not found\n", u4IfIndex,VlanId,u1IsTagged);
		return ENET_SUCCESS;
    }

    Service_id = pServiceId->serviceId;

	if(Service_id==UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId from ifIndex:%d vlanId:%d tagged:%d not found\n", u4IfIndex,VlanId,u1IsTagged);
		return ENET_SUCCESS;
	}


	adap_memset(&Service_data, 0, sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Policer_Entry, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&EHP_Entry, 0, sizeof(EHP_Entry));

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceMdLevel ERROR: failed by getting service id%d\n", Service_id);
		return ENET_FAILURE;
	}


	EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) *
																EHP_Entry.num_of_entries);

	if (EHP_Entry.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceMdLevel ERROR: FAIL to malloc ehp_info\n");
		return ENET_FAILURE;
	}
	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry, /* outPorts */
							&Policer_Entry, /* policer  */
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceMdLevel ERROR: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",Service_id);
		MEA_OS_free(EHP_Entry.ehp_info);
		return ENET_FAILURE;
	}
	Service_data.CFM_OAM_ME_Level_Threshold_valid = valid;
	Service_data.CFM_OAM_ME_Level_Threshold_value = level;
#ifdef 	USE_UPDATE_ALLOWED
	// update allowed
	Service_data.DSE_learning_update_allow_enable=MEA_TRUE;
#endif
    Service_data.editId = 0; /*create new Id*/
	if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
							Service_id,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceMdLevel ERROR: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
		MEA_OS_free(EHP_Entry.ehp_info);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);
	MEA_OS_free(EHP_Entry.ehp_info);
	return ENET_SUCCESS;
}

ADAP_Int32 adap_MiVlanSetServiceLmId (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,MEA_Uint32 LmId,MEA_Bool bPartner)
{
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_OutPorts_Entry_dbt				  OutPorts_Entry;
    MEA_Policer_Entry_dbt          		  Policer_Entry;
    MEA_EgressHeaderProc_Array_Entry_dbt  EHP_Entry;
	MEA_Service_t   					  Service_id;
	tServiceDb 							  *pServiceId=NULL;;
	MEA_Action_Entry_Data_dbt               Action_data;
	ADAP_Uint32								i=0;

    pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,u4IfIndex,u1IsTagged);
    if (pServiceId == NULL)
    {
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"serviceId from ifIndex:%d vlanId:%d tagged:%d not found\n", u4IfIndex,VlanId,u1IsTagged);
		return ENET_FAILURE;
    }

	Service_id = pServiceId->serviceId;

	if(Service_id==UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: failed to get serviceId from ifIndex:%d vlanId:%d\n", u4IfIndex,VlanId);
		return ENET_FAILURE;
	}


	adap_memset(&Service_data, 0, sizeof(MEA_Service_Entry_Data_dbt));
	adap_memset(&OutPorts_Entry, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Policer_Entry, 0, sizeof(MEA_Policer_Entry_dbt));
	adap_memset(&EHP_Entry, 0, sizeof(EHP_Entry));

	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: failed by getting service id%d\n", Service_id);
		return ENET_FAILURE;
	}

	EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) *
																EHP_Entry.num_of_entries);

	if (EHP_Entry.ehp_info == NULL )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: FAIL to malloc ehp_info\n");
		return ENET_FAILURE;
	}
	if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
							Service_id,
							NULL,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: failed by getting service id%d\n", Service_id);
		return ENET_FAILURE;
	}

	for(i=0;i<EHP_Entry.num_of_entries;i++)
	{
		EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.valid = valid;
		EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.LmId = LmId;
		EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_dasa=0;
		EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_COUNT_CCM_RX;
		if(bPartner == MEA_TRUE)
		{
			EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_COUNT_CCM_TX;
		}
	}

    Service_data.editId = 0; /*create new Id*/
	if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
							Service_id,
							&Service_data,
							&OutPorts_Entry,
							&Policer_Entry,
							&EHP_Entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: ENET_ADAPTOR_Set_Service failed for service:%d\n", Service_id);
		MEA_OS_free(EHP_Entry.ehp_info);
		return ENET_FAILURE;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", Service_id);
	MEA_OS_free(EHP_Entry.ehp_info);


	if(Service_data.DSE_learning_actionId_valid == MEA_TRUE)
	{
		adap_memset(&Policer_Entry  , 0 , sizeof(Policer_Entry ));
        adap_memset(&EHP_Entry  , 0 , sizeof(EHP_Entry ));


        if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                            Service_data.DSE_learning_actionId,
                            &Action_data,
                            &OutPorts_Entry,
                            &Policer_Entry,
                            &EHP_Entry)!= MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n",Service_data.DSE_learning_actionId);
            return ENET_FAILURE;
        }
       EHP_Entry.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) *
                                                                    EHP_Entry.num_of_entries);
        if (EHP_Entry.ehp_info == NULL )
        {
        	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: FAIL to malloc ehp_info\n");
            return ENET_FAILURE;
        }

        if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
                            Service_data.DSE_learning_actionId,
                            &Action_data,
                            &OutPorts_Entry,
                            &Policer_Entry,
                            &EHP_Entry)!= MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n",Service_data.DSE_learning_actionId);
            MEA_OS_free(EHP_Entry.ehp_info);
            return ENET_FAILURE;
        }


		for(i=0;i<EHP_Entry.num_of_entries;i++)
		{
			EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.valid = valid;
			EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.LmId = LmId;
			EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_dasa=0;
			EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_COUNT_CCM_TX;
			if(bPartner == MEA_TRUE)
			{
				EHP_Entry.ehp_info[i].ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_COUNT_CCM_RX;
			}
		}

        Action_data.ed_id_valid = MEA_TRUE;
		Action_data.ed_id=0;
		if(ENET_ADAPTOR_Set_Action (MEA_UNIT_0,
								Service_data.DSE_learning_actionId,
								&Action_data,
								&OutPorts_Entry,
								&Policer_Entry,
								&EHP_Entry) != 	MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_MiVlanSetServiceLmId ERROR: FAIL to ENET_ADAPTOR_Set_Action\n");
			MEA_OS_free(EHP_Entry.ehp_info);
            return ENET_FAILURE;
		}
		MEA_OS_free(EHP_Entry.ehp_info);
	}
	return ENET_SUCCESS;
}

ADAP_Int32 adap_MiVlanSetPartnerServiceLmId (tEnetHal_VlanId VlanId,ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1IsTagged,MEA_Bool valid,MEA_Uint32 LmId)
{
	ADAP_Uint32 IssPort=0;
	ADAP_Bool bFound;

	for(IssPort = 1; IssPort < MAX_NUM_OF_SUPPORTED_PORTS;IssPort++)
	{
		if(u4IfIndex == IssPort)
		{
			continue;
		}
		bFound = adap_VlanCheckUntaggedPort(VlanId,IssPort);
		if(bFound)
		{

			if(adap_MiVlanSetServiceLmId (VlanId,IssPort,D_UNTAGGED_SERVICE_TAG,MEA_TRUE,LmId,MEA_TRUE) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"failed to set lmId over interface:%d\n",IssPort);
				return ENET_FAILURE;
			}
		}
		bFound = adap_VlanCheckTaggedPort(VlanId,IssPort);
		if(bFound)
		{
			if(adap_MiVlanSetServiceLmId (VlanId,IssPort,D_TAGGED_SERVICE_TAG,MEA_TRUE,LmId,MEA_TRUE) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"failed to set lmId over interface:%d\n",IssPort);
				return ENET_FAILURE;
			}
		}
	}
	return ENET_SUCCESS;
}

MEA_Status adap_FsMiHwCreateCcmAnalyzer(MEA_CcmId_t  *id_io,int mepId,int period,int seqNumber,char *megData)
{
	MEA_CCM_Configure_dbt      entry;

	adap_memset(&entry,0,sizeof(entry));
	(*id_io) = MEA_PLAT_GENERATE_NEW_ID;


	entry.expected_MEP_Id=mepId;
	entry.mode_ccm_lmr=0; //ccm only
	entry.expected_period=period; //3.3
	entry.period_event=period; // 3.3
	entry.enable_clearSeq=0;
	entry.value_nextSeq=seqNumber;
	if(megData != NULL)
	{
		adap_memcpy(&entry.MEG_data[0],megData,MEA_ANALYZER_CCM_MAX_MEG_DATA);
	}
	else
	{
		entry.mode_ccm_lmr=1; //ccm only
	}

   if(MEA_API_Create_CCM_Entry( MEA_UNIT_0,&entry,id_io)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't create CCM_Entry \n");
         return MEA_ERROR;
    }

	return MEA_OK;
}

MEA_Status adap_FsMiHwPacketGenDmmCreateiNFO(tAdap_PacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond,MEA_Bool bDmmPacket)
{
	MEA_PacketGen_Entry_dbt entry;
	MEA_Uint32  oamInfo=0;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));


	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =MEA_PACKET_GEN_HEADER_TYPE_1; //120 bytes

	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->DA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //DA
	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->SA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //SA

	if(pPacketGen->num_of_vlans)
	{
		adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pPacketGen->num_of_vlans > 1)
		{
			adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pPacketGen->meg_level & 0x000007) << 5);
	oamInfo |=  (pPacketGen->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->flags & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->tlvOffset & 0xFF;

	// TXTimestamp
	entry.header.length	+= sizeof(MEA_Uint32);
	entry.header.length	+= sizeof(MEA_Uint32);

	if(bDmmPacket == MEA_TRUE)
	{
		// Reserved for DMM receiving equipment (0)
		entry.header.length	+= sizeof(MEA_Uint32);
		entry.header.length	+= sizeof(MEA_Uint32);

		// Reserved for DMR
		entry.header.length	+= sizeof(MEA_Uint32);
		entry.header.length	+= sizeof(MEA_Uint32);
	}

	// Reserved for DMR receiving equipment
	entry.header.length	+= sizeof(MEA_Uint32);
	entry.header.length	+= sizeof(MEA_Uint32);


	entry.header.length++; //end of tlv

	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_0000;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

static MEA_Status adap_FsMiHwCreateLmmCfm(tAdap_EcfmMepLmmInfoParams *pLmmInfo)
{
	tEnetHal_MacAddr srcmacAddr;
	tAdap_PacketGenHeader PacketGen;
	tRmtSrvInfo Info;
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 vpnIndex=0;
	tAdap_LmmPacketGen *pPacketGen=NULL;
	tEnetDmmPacketGen enetpacketGen;
	ADAP_Uint16 genVlan=0;
	MEA_Bool newConfig=MEA_TRUE;
	MEA_Port_t enetPort;


	genVlan = adap_allocateInternalVlanId();

	pPacketGen = adap_getLmmPacketGenEntry(pLmmInfo->u4IfIndex);
	if(pPacketGen != NULL)
	{
		if(adap_FsMiCompareLmmPacketGen(pPacketGen,pLmmInfo,&newConfig) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete lmm configuration\n");
		}
		if(newConfig == MEA_FALSE)
		{
			adap_memcpy(&enetpacketGen,&pPacketGen->packetGen,sizeof(tEnetLmmPacketGen));
		}

	}

	if(newConfig == MEA_TRUE)
	{
		genVlan = adap_allocateInternalVlanId();
		enetpacketGen.packetGenProfileId = adap_getPacketGenProfile(35);
		enetpacketGen.internalVlan = genVlan;

		if(enetpacketGen.packetGenProfileId != UNDEFINED_VALUE16)
		{
			adap_allocPacketGenProfile(35,enetpacketGen.packetGenProfileId);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateLmmCfm: profile:%d allready exist\n",enetpacketGen.packetGenProfileId);

		}
		else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
		{
			// there is no stream type for LMM
			if(adap_FsMiHwPacketGenProfInfoCreate(35,&enetpacketGen.packetGenProfileId,MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm: create packet gen profile failed\n");
				return MEA_ERROR;
			}
			adap_allocPacketGenProfile(35,enetpacketGen.packetGenProfileId);
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm: not enough room to create new packet gen profile\n");
			return MEA_ERROR;
		}

		// create packet generator
		if(adap_FsMiHwGetInterfaceCfmOamUCMac (pLmmInfo->u4IfIndex,&srcmacAddr) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwGetInterfaceCfmOamUCMac\n");
			return MEA_ERROR;
		}

		adap_mac_to_arr(&pLmmInfo->destMac, PacketGen.DA.b);
		adap_mac_to_arr(&srcmacAddr, PacketGen.SA.b);
		PacketGen.num_of_vlans=1;
		PacketGen.outerVlan=0x81000000 | genVlan | ( (pLmmInfo->VlanTag.u1Priority & 0x7) << 13);
		PacketGen.innerVlan=0x81000000;
		PacketGen.cfmEtherType=0x8902;
		PacketGen.meg_level=pLmmInfo->meLevel; //3 bits
		PacketGen.version=0; //5 bits
		PacketGen.opCode=ADAP_D_LMM_OPCODE_VALUE; // 8 bits
		PacketGen.flags=pLmmInfo->flags;
		PacketGen.tlvOffset=pLmmInfo->tlvOffset;
		PacketGen.initSeqNum=0;

		if(adap_FsMiHwPacketGenLmmCreateiNFO(&PacketGen,enetpacketGen.packetGenProfileId,&enetpacketGen.PacketGeId,1) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwPacketGenLbmCreateiNFO\n");
			return MEA_ERROR;
		}

		adap_AllocPacketSmallGenTable(enetpacketGen.PacketGeId);

		Info.b_lmEnable = MEA_FALSE;
		Info.lmId=0;
		Info.keyType=MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
		Info.me_level=pLmmInfo->meLevel; //3 bit
		Info.me_valid=MEA_TRUE;
		Info.src_port=120;
		Info.vlanId=genVlan;
		Info.EnetPolilcingProfile=adap_GetDefaultPolicerId(1);

		if(adap_FsMiHwCreateCfmService(&enetpacketGen.serviceId,&Info) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwCreateCfmService\n");
			return MEA_ERROR;
		}
		vpnIndex = Info.vpn;
		enetpacketGen.VpnIndex = vpnIndex;

		adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = pLmmInfo->VlanTag.u2VlanId;

		tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		pLmmInfo->lmId = adap_allocateLmId(pLmmInfo->VlanTag.u2VlanId,pLmmInfo->u4IfIndex);

		tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info.ehp_data.LmCounterId_info.LmId = pLmmInfo->lmId; // create lmId pConfig->lmId;
		tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
		tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_TX;
		//create action
		if(adap_FsMiHwCreateCfmAction(&enetpacketGen.action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwCreateCfmAction\n");
			return MEA_ERROR;
		}
		MeaAdapGetPhyPortFromLogPort (pLmmInfo->u4IfIndex, &enetPort);
		MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
		MEA_SET_OUTPORT(&tOutPorts, enetPort);


		if(adap_FsMiHwCreateRxForwarder(enetpacketGen.action_id,
										vpnIndex,
										&tOutPorts,pLmmInfo->meLevel,
										ADAP_D_LMM_OPCODE_VALUE,
										genVlan,
										enetPort) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call FsMiHwCreateTxForwarder\n");
			return MEA_ERROR;
		}

		vpnIndex = adap_GetVpnByVlan (pLmmInfo->VlanTag.u2VlanId);
		if(vpnIndex == ADAP_END_OF_TABLE)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm FAIL: adap_GetVpnByVlan failed for VlanId:%d\n", pLmmInfo->VlanTag.u2VlanId);
		   return ENET_FAILURE;
		}
		if(adap_FsMiHwPacketGenActive(enetpacketGen.PacketGeId,MEA_TRUE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwPacketGenActive\n");
			return MEA_ERROR;
		}

		// packet analyzer
		if(adap_MiVlanSetServiceMdLevel (pLmmInfo->VlanTag.u2VlanId,
										pLmmInfo->u4IfIndex,
										D_TAGGED_SERVICE_TAG,
										MEA_TRUE,
										pLmmInfo->meLevel) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_MiVlanSetServiceMdLevel\n");
			return MEA_ERROR;
		}

		if(adap_MiVlanSetServiceLmId (pLmmInfo->VlanTag.u2VlanId,
										pLmmInfo->u4IfIndex,
										D_TAGGED_SERVICE_TAG,
										MEA_TRUE,
										pLmmInfo->lmId,
										MEA_FALSE) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_MiVlanSetServiceLmId\n");
			return MEA_ERROR;
		}


		if(adap_MiVlanSetPartnerServiceLmId (pLmmInfo->VlanTag.u2VlanId,
												pLmmInfo->u4IfIndex,
												D_TAGGED_SERVICE_TAG,
												MEA_TRUE,
												pLmmInfo->lmId) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_MiVlanSetPartnerServiceLmId\n");
			return MEA_ERROR;
		}

		adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

		if(adap_FsMiHwCreateCcmAnalyzer(&enetpacketGen.id_io,
									0,
									ADAP_D_CCM_1SEC_PERIOD,
									0,
									NULL) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm: failed to create analyzer profile\r\n");
			return ENET_FAILURE;
		}


		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = enetpacketGen.id_io;


		tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info.ehp_data.LmCounterId_info.LmId = pLmmInfo->lmId; // create lmId pConfig->lmId;
		tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
		tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_RX;

		MEA_SET_OUTPORT(&tEhp_info.output_info,120);
		MEA_SET_OUTPORT(&tOutPorts, 120);
		MEA_SET_OUTPORT(&tEhp_info.output_info,127);
		MEA_SET_OUTPORT(&tOutPorts, 127);


		//create action
		if(adap_FsMiHwCreateCfmAction(&enetpacketGen.analyzer_action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwCreateCfmAction\n");
			return MEA_ERROR;
		}

		enetpacketGen.analyzerVpnIndex=vpnIndex;
		MeaAdapGetPhyPortFromLogPort (pLmmInfo->u4IfIndex, &enetPort);
		if(adap_FsMiHwCreateRxForwarder(enetpacketGen.analyzer_action_id,
									vpnIndex,
									&tOutPorts,
									pLmmInfo->meLevel,
									ADAP_D_LMR_OPCODE_VALUE,
									pLmmInfo->VlanTag.u2VlanId,
									enetPort) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLmmCfm failed to call adap_FsMiHwCreateRxForwarder\n");
			return MEA_ERROR;
		}

		adap_allocLmmPacketGenEntry(&enetpacketGen,pLmmInfo);
	}

	return MEA_OK;

}

tAdap_InterfaceRemoteSide *adap_getIntRemoteSide(ADAP_Uint32 u4IfIndex)
{
	return &tAdap_IntRmtSide[u4IfIndex];
}

static MEA_Status adap_read_rdi_event_group(MEA_Uint32 *pBitMaskEvent)
{
	 MEA_Uint32                 Event;
	 MEA_Uint32					analyzerId;
	 MEA_Uint32					value;

	 if(!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
		 (*pBitMaskEvent) = 0;

		 return MEA_OK;
	 }

	if(MEA_API_Get_RDI_Event_GroupId(MEA_UNIT_0,
									0,
									&Event) !=MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_read_rdi_event_group faied to read from MEA_API_Get_RDI_Event_GroupId\n");
		return MEA_ERROR;

	}
	(*pBitMaskEvent) = 0;
	for(analyzerId=0;analyzerId<32;analyzerId++)
	{
		value=((Event>>analyzerId) & 0x00000001);
		if(value) //
		{
			(*pBitMaskEvent) |= (1 << analyzerId);
		}
	}
	return MEA_OK;
}

MEA_Status adap_read_event_group(MEA_Uint32 *pBitMaskEvent)
{
	 MEA_Uint32                 Event;
	 MEA_Uint32					analyzerId;
	 MEA_Uint32					value;

	 if(!MEA_PACKET_ANALYZER_TYPE2_SUPPORT){
		 (*pBitMaskEvent) = 0;

		 return MEA_OK;
	 }

	if(MEA_API_Get_CC_Event_GroupId(MEA_UNIT_0,
									0,
									&Event) !=MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_read_event_group faied to read from MEA_API_Get_CC_Event_GroupId\n");
		return MEA_ERROR;

	}
	(*pBitMaskEvent) = 0;
	for(analyzerId=0;analyzerId<32;analyzerId++)
	{
		value=((Event>>analyzerId) & 0x00000001);
		if(value)
		{
			(*pBitMaskEvent) |= (1 << analyzerId);
		}
	}

	return MEA_OK;

}

ADAP_Uint32 adap_CompareCurrFdbFlush(ADAP_Uint32 ifIndex,ADAP_Uint32 vpnId)
{
	if( (vpnId < ADAP_MAX_NUM_OF_VPNS) && (ifIndex < MAX_NUM_OF_SUPPORTED_PORTS) )
	{
		return abs(fdbFulshTimesDb[ifIndex].vpnCurrFlushTime[vpnId]-fdbFulshTimesDb[ifIndex].vpnLastFlushTime[vpnId]);
	}
	return 0;
}

// Ask Alex
#if 0
void adap_update_iss_with_link_status(void)
{
	ADAP_Uint16 issPort=0;
	ADAP_Uint16 EnetPort=0;
	ADAP_Uint16 prevState;
	ADAP_Uint16 currState;
	MEA_Bool state;
	eEnetHal_PortStatus portState;

	for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
	{
        MeaAdapGetPhyPortFromLogPort (issPort, &EnetPort);
        if(EnetPort == UNDEFINED_VALUE16)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_update_iss_with_link_status FAIL: Unable to find the ENET port for the ISS port %d\n",  issPort);
            return;
        }

// Ask Alex if code was ported correctly from 6.1
        if(getPortSpape(issPort, &portState) == ENET_SUCCESS)
        {
        	if(portState == ADAP_DISABLE)
        	{
				if(MeaAdapSetIngressPort(EnetPort,ADAP_FALSE,ADAP_TRUE) != MEA_OK)
		    	{
		    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",issPort);
		    	}
		    	if(MeaAdapSetEgressPort (EnetPort,ADAP_FALSE,ADAP_TRUE) != MEA_OK)
		    	{
		    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",issPort);
		    	}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"set link status down because admin down ifIndex:%d\r\n",issPort) ;
			}
        	continue;
		}

        state=MEA_FALSE;

        //if(MEA_API_Get_Interface_CurrentLinkStatus(MEA_UNIT_0,EnetPort, &state ,MEA_TRUE) == MEA_OK)
        if(getPortSpape(issPort, &state) == ENET_SUCCESS)
		{
			if(state == ADAP_ENABLE)
			{
				currState = ADAP_PORT_LINK_UP;
			}
			else
			{
				currState = ADAP_PORT_LINK_DOWN;

			}
			prevState = DPLGetPortLinkStatus(issPort);

			if(prevState != currState)
			{
				if(currState == ADAP_PORT_LINK_DOWN)
				{
					link_down_cause_ccm_loss(issPort);

					// close the rx
					IssHwSetPortIngressStatus (issPort,ISS_DISABLE);
					// close the TX
					FsMiHwSetEgressInterface(EnetPort,0,1);

					NpFmFailureIndicationCallbackFuncLinkFault (issPort,OSIX_TRUE);

				}
				else
				{
					// open the Tx
					FsMiHwSetEgressInterface(EnetPort,1,1);

					// open the Rx
					IssHwSetPortIngressStatus (issPort,ISS_ENABLE);

					link_up_cause_ccm_loss(issPort);

					//link_inactive_packet_generator(issPort);
					link_active_packet_generator(issPort);


					NpFmFailureIndicationCallbackFuncLinkFault (issPort,OSIX_FALSE);

				}


				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_update_iss_with_link_status issPort:%d prev:%s curr:%s\n",
							issPort,
							(prevState==ADAP_PORT_LINK_UP)?"Up":"Down",
							(currState==ADAP_PORT_LINK_UP)?"Up":"Down") ;
				if(currState == ADAP_PORT_LINK_UP)
				{
					NpCallBackInterfaceStatusChange (issPort,ADAP_PORT_LINK_UP);
				}
				else
				{
					NpCallBackInterfaceStatusChange (issPort,ADAP_PORT_LINK_DOWN);
				}
				DPLSetPortLinkStatus(issPort,currState);
			}

        }
	}
}
#endif /* if 0 */

void adap_copyLastToCurrFdbFlush(ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx=0;
	if(ifIndex < MAX_NUM_OF_SUPPORTED_PORTS)
	{
		for(idx=0;idx<ADAP_MAX_NUM_OF_VPNS;idx++)
		{
			fdbFulshTimesDb[ifIndex].vpnLastFlushTime[idx] = fdbFulshTimesDb[ifIndex].vpnCurrFlushTime[idx];
		}
	}
}

static ADAP_Uint32 adap_getNumberIfOfPackets(ADAP_Uint32 u2IfIndex)
{
	return adap_PacketCounterPerPort[u2IfIndex];
}

static void adap_clrearNumberIfOfPackets(ADAP_Uint32 u2IfIndex)
{
	adap_PacketCounterPerPort[u2IfIndex]=0;
}

static void adap_set_arps_block_status(ADAP_Uint32 IfIndex,MEA_Bool status)
{
	arps_block_status[IfIndex] = status;
}

static void adap_FsCcmCompareDefectCounter(MEA_Counters_CCM_Defect_dbt *pCurrEntry,MEA_Counters_CCM_Defect_dbt *pLastEntry,ADAP_Uint32 *pLastEvents)
{
	(*pLastEvents) = 0;

	if(pCurrEntry->LastSequenc != pLastEntry->LastSequenc)
	{
		(*pLastEvents) |= ADAP_E_CCM_SEQUENCE_NUMBER_INCREMENT;
	}
	if(pCurrEntry->Unexpected_MEG_ID.val != pLastEntry->Unexpected_MEG_ID.val)
	{
		//(*pLastEvents) |= E_CCM_UNEXPECTED_MEG_ID_EVENT;
	}
	if(pCurrEntry->Unexpected_MEP_ID.val != pLastEntry->Unexpected_MEP_ID.val)
	{
		//(*pLastEvents) |= E_CCM_UNEXPECTED_MEP_ID_EVENT;
	}
	if(pCurrEntry->Unexpected_period.val != pLastEntry->Unexpected_period.val)
	{
		(*pLastEvents) |= ADAP_E_CCM_UNEXPECTED_PERIOD_EVENT;
	}
	if(pCurrEntry->reorder.val != pLastEntry->reorder.val)
	{
		//(*pLastEvents) |= E_CCM_REORDER_EVENT;
	}

}

static void adap_FsMiCheckForwarderState(tAdap_ECFMCcmDb *pEcfmDbParams,int entry_num)
{

	if(pEcfmDbParams->EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		if(pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu == PACKET_ANALYZER_SEND_TO_ANA)
		{
			if(adap_getNumberIfOfPackets(pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.ifIndex) > 50)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"received packet to CPU ifIndex:%d\n",pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.ifIndex);
				pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu = PACKET_ANALYZER_SEND_TO_CPU;
			}
			adap_clrearNumberIfOfPackets(pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.ifIndex);
		}
	}
	else
	{
		if(pEcfmDbParams->enetPacketAnaDb.sendPacketToCpu == PACKET_ANALYZER_SEND_TO_ANA)
		{
			if(pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu == PACKET_ANALYZER_SEND_TO_ANA)
			{
				if(adap_getNumberIfOfPackets(pEcfmDbParams->enetPacketAnaDb.ifIndex) > 50)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"received packet to CPU ifIndex:%d\n",pEcfmDbParams->enetPacketAnaDb.ifIndex);
					pEcfmDbParams->tSubEntry[entry_num].enetPacketAnaDb.sendPacketToCpu = PACKET_ANALYZER_SEND_TO_CPU;
				}
			}
			adap_clrearNumberIfOfPackets(pEcfmDbParams->enetPacketAnaDb.ifIndex);
		}
	}
}

static MEA_Status adap_FsMiHwPacketGenModifyiNFO(MEA_PacketGen_t  Id_io,MEA_Bool rdiState)
{
	MEA_PacketGen_Entry_dbt entry;
	ADAP_Uint16				vlanTag;
	MEA_Uint32				idx=0;

	if(Id_io == UNDEFINED_VALUE16)
		return MEA_OK;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));

	if(MEA_API_Get_PacketGen_Entry(MEA_UNIT_0,Id_io,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get packet gen id:%d\n",Id_io);
		return MEA_ERROR;
	}
	idx=12;
	vlanTag = ((ADAP_Uint16)(entry.header.data[idx]) ) << 8;
	vlanTag += ((ADAP_Uint16)(entry.header.data[idx+1]) );

	if( (vlanTag == 0x8100) || (vlanTag == 0x88a8) )
	{
		idx=16;
		vlanTag = ((ADAP_Uint16)(entry.header.data[idx]) ) << 8;
		vlanTag += ((ADAP_Uint16)(entry.header.data[idx+1]) );
		if( (vlanTag == 0x8100) || (vlanTag == 0x88a8) )
		{
			idx=20;
		}
	}
	idx += 2; //ethertype
	idx++; //md level + version
	idx++; //opcode
	if(rdiState == MEA_TRUE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"set rdi bit id:%d\n",Id_io);
		entry.header.data[idx] |= 0x80;
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"clear rdi bit id:%d\n",Id_io);
		entry.header.data[idx] &= ~0x80;
	}

	if(MEA_API_Set_PacketGen_Entry(MEA_UNIT_0,Id_io,&entry) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set packet gen id:%d\n",Id_io);
		return MEA_ERROR;
	}

	return MEA_OK;
}

/****************************************************************************
 * Function Name      : adap_EcfmCcmOffHandleRxCallBack
 *
 * Description        : This rotuine is used to handle the call back recevied
 *                      from hardware. This will posts an even to ECFM CC Task.
 *
 * Input(s)           : u4ContextId - Context Id
 *                    : u4RxHandle - Receive Handle for the remote MEP for which
 *                                   the reception timer expired.
 *                      u4IfIndex - Interface Index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : ADAP_ECFM_SUCCESS / ADAP_ECFM_FAILURE
 *****************************************************************************/
ADAP_Int32 adap_EcfmCcmOffHandleRxCallBack (ADAP_Uint32 u4ContextId, ADAP_Uint16 u2RxHandle, ADAP_Uint32 u4IfIndex)
{
	if (!adap_EcfmHandlerCb)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "ERR: %s == NULL", adap_EcfmHandlerCb);
		return ENET_FAILURE;
	}

	adap_EcfmHandlerCb(u4ContextId, u2RxHandle, u4IfIndex);

    return ADAP_ECFM_SUCCESS;
}

void adap_FsCallBackEventLossOfCCM(ADAP_Uint16 u2RMepId,ADAP_Uint32 u4IfIndex)
{
	if(u2RMepId != 0)
	{
		if(adap_EcfmCcmOffHandleRxCallBack (0,u2RMepId,u4IfIndex) != ADAP_ECFM_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to send loss of CCM u2RxHandle:%d ifIndex:%d contectId:0\n",
									u2RMepId,
									u4IfIndex);
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EcfmCcmOffHandleRxCallBack: loss of CCM u2RxHandle:%d ifIndex:%d contectId:0\n",
								u2RMepId,
								u4IfIndex);
	}

}

static ADAP_Int32 adap_enetErpsConfiguration(ADAP_Uint32 u4IfIndex, ADAP_Uint16 vlanId)
{
    MEA_Action_Entry_Data_dbt              Action_data;
    MEA_OutPorts_Entry_dbt                 Action_outPorts;
    MEA_Policer_Entry_dbt                  Action_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt   Action_editing;
	MEA_Action_t                           Action_id;
	MEA_LxCp_t                             LxCp_Id;
    MEA_LxCP_Protocol_key_dbt              LxCp_key;
    MEA_LxCP_Protocol_data_dbt             LxCp_data;
    MEA_EHP_Info_dbt                       Action_editing_info[2];
    ADAP_Uint16								issPort=0;
    MEA_SE_Entry_dbt                      	entry;
    MEA_Port_t								enetPort;


    adap_memset(&LxCp_key, 0, sizeof(LxCp_key));
    adap_memset(&LxCp_data, 0, sizeof(LxCp_data));
    adap_memset(&Action_data, 0, sizeof(Action_data));
    adap_memset(&Action_policer, 0, sizeof(Action_policer));
	adap_memset(&Action_outPorts, 0, sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&Action_editing, 0, sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
	adap_memset(&entry, 0, sizeof(MEA_SE_Entry_dbt));
	adap_memset(&Action_editing_info[0],0,sizeof(MEA_EHP_Info_dbt)*2);

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_enetErpsConfiguration: IfIndex:%d start\n",u4IfIndex);

	if(getLxcpErpsId(u4IfIndex, &LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"getLxcpErpsId FAIL: LxCp Id NOT FOUND\n");
		return ENET_FAILURE;
	}
	LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_R_APS;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: MEA_API_Get_LxCP_Protocol FAIL! LxCp ID:%d\n",
			   LxCp_Id);
		return ENET_FAILURE;
	}

	if(LxCp_data.ActionId_valid == MEA_TRUE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_NOTICE,"adap_enetErpsConfiguration: Action:%d already exists for LxCp ID:%d\n",
				LxCp_data.ActionId, LxCp_Id);
		return ENET_SUCCESS;
	}

	Action_editing.ehp_info = &Action_editing_info[0];


	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.LmId = 0;
	Action_editing.ehp_info[0].ehp_data.LmCounterId_info.Command_cfm = 0;


	Action_editing.ehp_info[0].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	Action_editing.ehp_info[0].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_TRANS;

	Action_editing.ehp_info[1].ehp_data.eth_info.stamp_priority            = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_info.stamp_color               = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	Action_editing.ehp_info[1].ehp_data.LmCounterId_info.valid = MEA_TRUE;
	Action_editing.ehp_info[1].ehp_data.LmCounterId_info.Command_dasa = 1; /* routing MAC*/
	Action_editing.ehp_info[1].ehp_data.LmCounterId_info.LmId = 0;
	Action_editing.ehp_info[1].ehp_data.LmCounterId_info.Command_cfm = 0;


	Action_editing.ehp_info[1].ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_NONE;
	Action_editing.ehp_info[1].ehp_data.eth_info.command = MEA_EGRESS_HEADER_PROC_CMD_APPEND;

	Action_editing.ehp_info[1].ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	Action_editing.ehp_info[1].ehp_data.eth_info.val.vlan.vid = vlanId;

    for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
    {
		if(issPort == u4IfIndex)
			continue;

		if( (adap_VlanCheckUntaggedPort(vlanId,issPort) > 0) || (adap_VlanCheckTaggedPort(vlanId,issPort) > 0) )
		{
			MeaAdapGetPhyPortFromLogPort (issPort, &enetPort);
			MEA_SET_OUTPORT(&Action_outPorts, enetPort);
			MEA_SET_OUTPORT(&Action_editing_info[0].output_info, enetPort);

			Action_editing.num_of_entries = 1;
		}
    }

    if(Action_editing.num_of_entries == 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_enetErpsConfiguration no output port found");
		return ENET_SUCCESS;
	}

	MEA_SET_OUTPORT(&Action_outPorts, 127);
	MEA_SET_OUTPORT(&Action_editing_info[1].output_info, 127);
	Action_editing.num_of_entries = 2;

    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_enetErpsConfiguration - Ports:%08lX %08lX %08lX %08lX\n",
            Action_outPorts.out_ports_0_31,
            Action_outPorts.out_ports_32_63,
            Action_outPorts.out_ports_64_95,
            Action_outPorts.out_ports_96_127);


	Action_data.output_ports_valid  = MEA_TRUE;
	Action_data.force_color_valid = MEA_FALSE;
	Action_data.COLOR             = 0;
	Action_data.force_cos_valid   = MEA_FALSE;
	Action_data.COS               = 0;
	Action_data.force_l2_pri_valid = MEA_FALSE;
	Action_data.L2_PRI             = 0;

	Action_data.pm_id_valid        = MEA_TRUE;
	Action_data.pm_id              = 0; /* Request new id */
	Action_data.tm_id_valid        = MEA_TRUE;
	Action_data.tm_id              = 0; /* Request new id */
	Action_data.ed_id_valid        = MEA_TRUE;
	Action_data.ed_id              = 0; /* Request new id */
	Action_data.protocol_llc_force = MEA_TRUE;
	Action_data.proto_llc_valid    = MEA_TRUE;  /* force protocol and llc */
	Action_data.Protocol           = 1;         /* 0-TRANS/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA*/
	Action_data.Llc                = MEA_FALSE;

	/* Build the action policer */
	adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
	Action_data.tm_id              = 0;
	Action_data.policer_prof_id    = adap_GetDefaultPolicerId(u4IfIndex);
	if(Action_data.policer_prof_id == UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: Default policer profile NOT FOUND\n");
		return ENET_FAILURE;
	}
	if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
										Action_data.policer_prof_id,
										0,/* ACM_Mode */
										&Action_policer)
		!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: MEA_API_Get_Policer_ACM_Profile for PolilcingProfile %d failed\n",
				Action_data.policer_prof_id);
		return ENET_FAILURE;
	}


	LxCp_key.protocol = MEA_LXCP_PROTOCOL_IN_R_APS;

	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: MEA_API_Get_LxCP_Protocol FAIL! LxCp ID:%d\n",
			   LxCp_Id);
		return ENET_FAILURE;
	}

	/* Create the action */
	Action_id = MEA_PLAT_GENERATE_NEW_ID;

	if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
							   &Action_data,
							   &Action_outPorts,
							   &Action_policer,
							   &Action_editing,
							   &Action_id) != MEA_OK)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: ENET_ADAPTOR_Create_Action failed for action:%d\n",
			  Action_id);
	   return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_enetErpsConfiguration: Action created ActionId: %d\n", Action_id);

	/* Get the LxCp entry */

	LxCp_data.action_type = MEA_LXCP_PROTOCOL_ACTION_ACTION;
	LxCp_data.ActionId_valid = MEA_TRUE;
	LxCp_data.ActionId = Action_id;
	LxCp_data.OutPorts_valid = MEA_TRUE;
	adap_memcpy(&LxCp_data.OutPorts,&Action_outPorts,sizeof(LxCp_data.OutPorts));

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &LxCp_key, &LxCp_data) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_enetErpsConfiguration FAIL: ENET_ADAPTOR_Set_LxCP_Protocol FAIL!\n");
		return ENET_FAILURE;
	}

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_enetErpsConfiguration: ENET_ADAPTOR_Set_LxCP_Protocol succeeded protocol:%d\n", LxCp_key.protocol);

	return ENET_SUCCESS;

}

ADAP_Int32 adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_Bool/*BOOL1*/ add,MEA_LxCp_t   LxCp_Id)
{
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

    /* Update the action for the specific protocol */
	key_pi.protocol = MEA_LXCP_PROTOCOL_CFM_OAM;

    /* Update all ports' LxCp */
	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

	switch (add)
	{
        case ADAP_FALSE :
			// if allready then do nothing
			if(data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT)
			{
				return ENET_SUCCESS;
			}
            /* Remove LxCp protocol by defining it as "Transparent" */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        break;
        case ADAP_TRUE :
			// if allready then do nothing
			if(data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_CPU)
			{
				return ENET_SUCCESS;
			}
            /* Forward the LxCp protocol to CPU */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
        break;
        default:
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
            return ENET_FAILURE;
	}

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Int32 adap_FsMiVlanHwLacpLxcpApsUpdate (ADAP_Bool/*BOOL1*/ add,ADAP_Uint32 ifIndex)
{
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;
	MEA_LxCp_t   				  LxCp_Id;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

    /* Update the action for the specific protocol */
	key_pi.protocol = MEA_LXCP_PROTOCOL_IN_R_APS;

	if(getLxcpId(ifIndex,&LxCp_Id) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLxcpUpdate FAIL: LxCp Id NOT FOUND for ISS port %d\n", ifIndex);
		return ENET_FAILURE;
	}

    /* Update all ports' LxCp */
	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

	switch (add)
	{
        case ADAP_FALSE :
            /* Remove LxCp protocol by defining it as "Transparent" */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        break;
        case ADAP_TRUE :
            /* Forward the LxCp protocol to CPU */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
        break;
        default:
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
            return ENET_FAILURE;
	}

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

static ADAP_Int32 adap_FsMiVlanHwSetEgressServiceState (tAdap_ProtectVlanId *pVlanProtect,MEA_VP_State_t  enetState,int bWorkingPath)
{
	ADAP_Uint16								VpnIndex=0;
	MEA_Port_t                              enetPort;

	if(bWorkingPath == ADAP_E_APS_WORKING_PATH)
	{
		MeaAdapGetPhyPortFromLogPort (pVlanProtect->u4WorkingIfIndex, &enetPort);
	}
	else if(bWorkingPath == ADAP_E_APS_PROTECTED_PATH)
	{
		MeaAdapGetPhyPortFromLogPort (pVlanProtect->u4ProtectionIfIndex, &enetPort);
	}
	else
	{
		MeaAdapGetPhyPortFromLogPort (pVlanProtect->u4IngressIfIndex, &enetPort);
	}
	VpnIndex = adap_GetVpnByVlan (pVlanProtect->workingVlanId);
	if (VpnIndex == ADAP_END_OF_TABLE)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsMiVlanHwSetServiceState VlanId:%d not found\n", pVlanProtect->workingVlanId);
	   return ENET_SUCCESS;
	}

	MEA_API_Set_VP_State (MEA_UNIT_0,VpnIndex,enetPort,enetState);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsMiVlanHwSetServiceState VlanId:%d port:%d move to :%s\n",
													pVlanProtect->workingVlanId,
													enetPort,
													enetState==MEA_VP_STATE_BLK_REGULAR?"Block":"Forward");
	return ENET_SUCCESS;
}

static ADAP_Int32 adap_FsMiVlanHwSetIngressServiceState (tAdap_ProtectVlanId *pVlanProtect,MEA_MSTP_State_t state,int bWorkingPath)
{
	ADAP_Uint32								u1IsTagged;
	MEA_Service_t                           Service_id;
	ADAP_Uint32								CurrentAgingTime;
	MEA_LxCp_t 								LxCp_Id;
	ADAP_Uint32								master;
	tServiceDb 							  	*pServiceId=NULL;

    master=pVlanProtect->u4WorkingIfIndex;
	if(adap_IsPortChannel(pVlanProtect->u4WorkingIfIndex) == ENET_SUCCESS)
	{
		if( (adap_NumOfLagPortFromAggrId(pVlanProtect->u4WorkingIfIndex)) > 0)
		{
			master=adap_GetLagMasterPort(pVlanProtect->u4WorkingIfIndex);
		}
	}

	for(u1IsTagged=D_UNTAGGED_SERVICE_TAG;u1IsTagged<=D_PROVIDER_SERVICE_TAG;u1IsTagged++)
	{
		pServiceId = MeaAdapGetServiceIdByVlanIfIndex(pVlanProtect->workingVlanId,pVlanProtect->u4WorkingIfIndex,u1IsTagged);
		if(pServiceId != NULL)
		{

			Service_id = pServiceId->serviceId;

			if(Service_id != UNDEFINED_VALUE16)
			{
				printf("adap_FsMiVlanHwSetIngressServiceState serviceId:%d vlanId %d MSTP state:%d portId:%d\n", Service_id, pVlanProtect->workingVlanId, state, pVlanProtect->u4WorkingIfIndex);
				if (ENET_ADAPTOR_Service_Set_MSTP_State(MEA_UNIT_0, Service_id, state) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "DPL_StpServiceConfiguration Error: ENET_ADAPTOR_Service_Set_MSTP_State for service Id %d failed\n", Service_id);
					continue;
				}

				if(state == MEA_MSTP_TYPE_STATE_FORWARD)
				{
					if(getLxcpErpsId(master, &LxCp_Id) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"getLxcpErpsId FAIL: LxCp Id NOT FOUND\n");
						return ENET_FAILURE;
					}

					printf("adap_FsMiVlanHwSetIngressServiceState - adap_GetSecondPortLxCpId LxCp:%d\n", LxCp_Id);

					// Create action for forwarding packet both to CPU and to the output port
					adap_enetErpsConfiguration(master,pVlanProtect->workingVlanId);
					adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_TRUE,LxCp_Id);

				}
				else
				{
					getLxcpId(master,&LxCp_Id);

					adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_TRUE,LxCp_Id);
					adap_FsMiVlanHwLacpLxcpApsUpdate (ADAP_TRUE,LxCp_Id);

					adap_FsMiVlanHwLacpLxcpOamUpdate (ADAP_TRUE,1);
					adap_FsMiVlanHwLacpLxcpApsUpdate (ADAP_TRUE,1);
				}

				if (MEA_API_Service_Set_LXCP(MEA_UNIT_0,
											  Service_id,
											  MEA_TRUE,
											  LxCp_Id) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: adap_FsMiVlanHwSetIngressServiceState FAIL for serviceId:%d LxCp_Id:%d\n", Service_id, LxCp_Id);
					return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsMiVlanHwSetIngressServiceState set  seviceId:%d LxCp_Id:%d\n", Service_id, LxCp_Id);
				printf("adap_FsMiVlanHwSetIngressServiceState - MEA_API_Service_Set_LXCP  serviceId:%d LxCp_Id:%d\n", Service_id, LxCp_Id);
			}
		}
	}

	CurrentAgingTime = adap_GetAgingTime();

	if (EnetHal_FsBrgSetAgingTime(5) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiNpDeleteAllFdbEntries Failed on EnetHal_FsBrgSetAgingTime reset \n" );

		return ENET_FAILURE;
	}
	if (EnetHal_FsBrgSetAgingTime(CurrentAgingTime) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiNpDeleteAllFdbEntries Failed on EnetHal_FsBrgSetAgingTime reset \n" );

		return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}

static void adap_change_erps_protection_state(ADAP_Uint32 ifIndex)
{
	ADAP_Uint32 idx=0;
	ADAP_Int32 issPortList=0;
	MEA_Uint32 bitMaskEvent=0;
	MEA_Uint32 rdiBitMaskEvent=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.packetAnaParams_active != MEA_TRUE)
		{
			continue;
		}
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
		{
			for(issPortList=0;issPortList < tAdap_EcfmCcmDb[idx].EcfmDbParams.i4Length;issPortList++)
			{
				if(tAdap_EcfmCcmDb[idx].EcfmDbParams.u4PortArray[issPortList] == ifIndex)
				{
					tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[issPortList].enetCcmAppDb.priodicSendPacketToCpu=5;
					tAdap_EcfmCcmDb[idx].EcfmDbParams.tSubEntry[issPortList].enetCcmAppDb.sendLossCCm=0;
					return;
				}
			}
		}
		else
		{
			if(tAdap_EcfmCcmDb[idx].EcfmDbParams.EcfmHwCcTxParams.u4IfIndex == ifIndex)
			{
				tAdap_EcfmCcmDb[idx].EcfmDbParams.enetCcmAppDb.priodicSendPacketToCpu=5;
				tAdap_EcfmCcmDb[idx].EcfmDbParams.enetCcmAppDb.sendLossCCm=0;
				return;
			}
		}
	}
	MEA_API_Lock();
	adap_read_event_group(&bitMaskEvent);
	adap_read_rdi_event_group(&rdiBitMaskEvent);
	MEA_API_Unlock();
}

static ADAP_Uint32 adap_UpdatePortIdProtection(ADAP_Uint32 u4Port1IfIndex,ADAP_Uint32 u1Port1Action)
{
	tAdap_ProtectVlanId 		tVlanProtect;
	ADAP_Uint16 				VlanId=0;
	ADAP_Uint16					u1IsTagged=0;
	ADAP_Uint16					Service_id=0;
	ADAP_Uint8					found=0;
	tServiceDb 					*pServiceId=NULL;

	tVlanProtect.u4ProtectionIfIndex = u4Port1IfIndex;
	tVlanProtect.u4WorkingIfIndex = u4Port1IfIndex;


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"portProtection --------> ifIndex:%d state:%s\r\n",u4Port1IfIndex,
									(u1Port1Action == ADAP_ERPS_PORT_STATE_BLOCKING)?"Block":"Unblock");

	if(u1Port1Action == ADAP_ERPS_PORT_STATE_BLOCKING)
	{
		adap_set_arps_block_status(u4Port1IfIndex,MEA_TRUE);
	}
	else
	{
		adap_set_arps_block_status(u4Port1IfIndex,MEA_FALSE);
	}

	for(VlanId =0;VlanId <ADAP_NUM_OF_SUPPORTED_VLANS;VlanId++)
	{
		tVlanProtect.protectionVlanId = VlanId;
		tVlanProtect.workingVlanId = VlanId;
		found=0;
		for(u1IsTagged=0; u1IsTagged<=2; u1IsTagged++)
		{
			pServiceId = MeaAdapGetServiceIdByVlanIfIndex(VlanId,u4Port1IfIndex,u1IsTagged);
			if (pServiceId != NULL)
			{

				Service_id = pServiceId->serviceId;
				if(Service_id != UNDEFINED_VALUE16)
				{
					found=1;
				}
			}
		}
		if(found == 1)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"portProtection vlanId:%d\r\n",VlanId);


			if(u1Port1Action == ADAP_ERPS_PORT_STATE_BLOCKING)
			{
				adap_FsMiVlanHwSetEgressServiceState (&tVlanProtect,MEA_VP_STATE_BLK_REGULAR,ADAP_E_APS_WORKING_PATH);
				adap_FsMiVlanHwSetIngressServiceState (&tVlanProtect,MEA_MSTP_TYPE_STATE_BLOCKING_SRV,ADAP_E_APS_WORKING_PATH);
			}
			else if(u1Port1Action == ADAP_ERPS_PORT_STATE_UNBLOCKING)
			{
				adap_FsMiVlanHwSetEgressServiceState (&tVlanProtect,MEA_VP_STATE_FWD,ADAP_E_APS_WORKING_PATH);
				adap_FsMiVlanHwSetIngressServiceState (&tVlanProtect,MEA_MSTP_TYPE_STATE_FORWARD,ADAP_E_APS_WORKING_PATH);
			}
		}
	}
	adap_change_erps_protection_state(u4Port1IfIndex);

	return ENET_SUCCESS;
}

ADAP_Uint32 adap_ForwardOtherPort(ADAP_Uint32 u4PortIfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_MAX_NUM_OF_MSTP_INSTANCES;idx++)
	{
		if(adap_ErpsInstance[idx].u2VlanGroupId != UNDEFINED_VALUE16)
		{
			if(	(adap_ErpsInstance[idx].u4Port1IfIndex != UNDEFINED_VALUE16) &&
				(adap_ErpsInstance[idx].u4Port2IfIndex != UNDEFINED_VALUE16) )
			{
				if( (adap_ErpsInstance[idx].u4Port1IfIndex == u4PortIfIndex) && (adap_ErpsInstance[idx].u1Port1Action == ADAP_ERPS_PORT_STATE_UNBLOCKING) )
				{
					//move the other port to unblock
					return adap_UpdatePortIdProtection(adap_ErpsInstance[idx].u4Port2IfIndex,ADAP_ERPS_PORT_STATE_UNBLOCKING);
				}
				else if( (adap_ErpsInstance[idx].u4Port2IfIndex == u4PortIfIndex) && (adap_ErpsInstance[idx].u1Port2Action == ADAP_ERPS_PORT_STATE_UNBLOCKING) )
				{
					//move the other port to unblock
					return adap_UpdatePortIdProtection(adap_ErpsInstance[idx].u4Port1IfIndex,ADAP_ERPS_PORT_STATE_UNBLOCKING);
				}
			}
		}
	}
	return ENET_SUCCESS;
}

static void adap_ccm_state_machine(MEA_Uint32 ccm_id,MEA_Uint32 bitMaskEvent,MEA_Uint32 rdiBitMaskEvent,int entry_num)
{
	tAdap_ECFMCcmDb *pCcmEntry=NULL;
	MEA_Counters_CCM_Defect_dbt DefectEntry;
	MEA_Bool rcv_bad_traffic=MEA_FALSE;
	ADAP_Uint32    lastEvent;
	MEA_CcmId_t  id_io;
	eAdap_CcmStateMachine	*pCcmState=NULL;
	ADAP_Uint32					*pGood3CCmPackets=NULL;
	ADAP_Uint32					*pPriodicSendPacketToCpu=NULL;
	//ADAP_Uint32               u4IfIndex;
	//ADAP_Uint16				linkState;
	MEA_Bool					rdi_enable=MEA_FALSE;


	if(tAdap_EcfmCcmDb[ccm_id].EcfmDbParams.packetAnaParams_active != MEA_TRUE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"packet analyzer not active\n");
		return;
	}
	if(tAdap_EcfmCcmDb[ccm_id].tPduFromIss.flags & 0x80)
	{
		rdi_enable=MEA_TRUE;
	}
	pCcmEntry = &tAdap_EcfmCcmDb[ccm_id].EcfmDbParams;

	if(pCcmEntry->EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		id_io = pCcmEntry->tSubEntry[entry_num].enetPacketAnaDb.id_io;
		pCcmState = (eAdap_CcmStateMachine *)&pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.ccmState;
		pGood3CCmPackets = (ADAP_Uint32 *)&pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.Good3CCmPackets;
		pPriodicSendPacketToCpu = (ADAP_Uint32 *)&pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.priodicSendPacketToCpu;
		//u4IfIndex = pCcmEntry->u4PortArray[entry_num];
		adap_FsMiCheckForwarderState(pCcmEntry,entry_num);
	}
	else
	{
		id_io = pCcmEntry->enetPacketAnaDb.id_io;
		pCcmState = &pCcmEntry->enetCcmAppDb.ccmState;
		pGood3CCmPackets = &pCcmEntry->enetCcmAppDb.Good3CCmPackets;
		pPriodicSendPacketToCpu = (ADAP_Uint32 *)&pCcmEntry->enetCcmAppDb.priodicSendPacketToCpu;
		//u4IfIndex = pCcmEntry->EcfmHwCcTxParams.u4IfIndex;
		adap_FsMiCheckForwarderState(pCcmEntry,0);
	}
	adap_collect_ccm_counter(id_io);

	// if we move from packet to cpu so it could be that LOSS can happend.
	// if some configuration changed by ISS so wait 1 second till system stable
	if( ((*pPriodicSendPacketToCpu) != 0) || (pCcmEntry->countTimeBeforeStart < ADAP_D_THRESHOULD_START_TIME) )
	{
		if((*pPriodicSendPacketToCpu) > 0)
		{
			(*pPriodicSendPacketToCpu)--;
		}
		if(pCcmEntry->countTimeBeforeStart < ADAP_D_THRESHOULD_START_TIME)
		{
			pCcmEntry->countTimeBeforeStart++;
			(*pCcmState) = ADAP_E_WAIT_ON_PACKET_STATE;
		}
		adap_clear_ccm_counter(id_io);
		if(pCcmEntry->EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
		{
			adap_memset(&pCcmEntry->tSubEntry[entry_num].enetPacketAnaDb.DefectEntry,0,sizeof(MEA_Counters_CCM_Defect_dbt));
		}
		else
		{
			adap_memset(&pCcmEntry->enetPacketAnaDb.DefectEntry,0,sizeof(MEA_Counters_CCM_Defect_dbt));
		}
		return;
	}

	//linkState = DPLGetPortLinkStatus(u4IfIndex);
	// wait a while at CCM start
	if(pCcmEntry->EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
	{
		// collect ccm counter
		//collect_ccm_counter(id_io);
		// read ccm counter
		adap_read_ccm_counter(id_io,&DefectEntry);
		if(bitMaskEvent & (1 << id_io) )
		{
			// wait 3 seconds till the system is stable

			// if the rMep valid and LossCcm not send since last time
			pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCm++;

			if(pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCm == pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCmThreshould)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"send loss IfIndex:%d id_io:%d bitMask:%d rdiBitMask:%d!!\n",
												pCcmEntry->EcfmHwCcTxParams.u4IfIndex,
												id_io,
												bitMaskEvent,
												rdiBitMaskEvent);

				pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCmThreshould = ADAP_E_COUNT_TIME_BEFORE_START_CCM;

			}

			if(pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.callback_loss_called == ADAP_FALSE)
			{
				// check if need to move port to forward
				adap_FsCallBackEventLossOfCCM(pCcmEntry->EcfmHwCcRxParams.u2RxFilterId,pCcmEntry->u4PortArray[entry_num]);
				pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.callback_loss_called = MEA_TRUE;
			}

			(*pCcmState) = ADAP_E_WAIT_ON_PACKET_STATE;
			//pCcmEntry->tSubEntry[entry_num].countTimeBeforeStart=0;
			//pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCm=1;
			// wait some times
			if(rdi_enable == MEA_TRUE)
			{
				if(pCcmEntry->tSubEntry[entry_num].rdiState == ADAP_FALSE)
				{
					adap_FsMiHwPacketGenModifyiNFO(pCcmEntry->tSubEntry[entry_num].enetPacketGenDb.packetGenId,MEA_TRUE);
					pCcmEntry->tSubEntry[entry_num].rdiState=MEA_TRUE;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"=====> RDI bit set:%d!!\n",entry_num);

				}
			}
		}
		else
		{
			// traffic back to service
			pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCm=0;
			pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.callback_loss_called = MEA_FALSE;
			if(pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCmThreshould > 3)
			{
				pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.sendLossCCmThreshould--;
			}
			if(pCcmEntry->tSubEntry[entry_num].rdiState == ADAP_TRUE)
			{
				adap_FsMiHwPacketGenModifyiNFO(pCcmEntry->tSubEntry[entry_num].enetPacketGenDb.packetGenId,MEA_FALSE);
				pCcmEntry->tSubEntry[entry_num].rdiState=MEA_FALSE;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"=====> RDI bit clear:%d!!\n",entry_num);
			}

		}
		pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.timeCheckCcmTraffic += ADAP_D_ENET_CCM_TASK_PERIOD;

		pCcmEntry->tSubEntry[entry_num].enetCcmAppDb.timeCheckCcmTraffic=0;

		// check defects
		adap_FsCcmCompareDefectCounter(&DefectEntry,&pCcmEntry->tSubEntry[entry_num].enetPacketAnaDb.DefectEntry,&lastEvent);
		// found received rdi bit set
		if( ( rdiBitMaskEvent & (1 << id_io) ) != 0)
		{
			lastEvent |= ADAP_E_CCM_RDI_BIT_SET_EVENT;
		}

		// save the last counters
		adap_memcpy(&pCcmEntry->tSubEntry[entry_num].enetPacketAnaDb.DefectEntry,&DefectEntry,sizeof(MEA_Counters_CCM_Defect_dbt));

	}
	else
	{
		// collect ccm counter
		//collect_ccm_counter(id_io);
		// read ccm counter
		adap_read_ccm_counter(id_io,&DefectEntry);

		// we are only start point no need to check the loss

		if(bitMaskEvent & (1 << id_io) )
		{
			// if the rMep valid and LossCcm not send since last time
			pCcmEntry->enetCcmAppDb.sendLossCCm++;

			if(pCcmEntry->enetCcmAppDb.sendLossCCm == pCcmEntry->enetCcmAppDb.sendLossCCmThreshould)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"send loss IfIndex:%d id_io:%d bitMask:%d rdiBitMask:%d threshould:%d!!\n",
												pCcmEntry->EcfmHwCcTxParams.u4IfIndex,
												id_io,
												bitMaskEvent & (1 << id_io),
												rdiBitMaskEvent,
												pCcmEntry->enetCcmAppDb.sendLossCCmThreshould);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IfIndex:%d id_io:%d bitMask:%d rdiBitMask:%d loss continue time:%d!!\n",
												pCcmEntry->EcfmHwCcTxParams.u4IfIndex,
												id_io,
												bitMaskEvent,
												rdiBitMaskEvent,
												pCcmEntry->enetCcmAppDb.sendLossCCm);

				pCcmEntry->enetCcmAppDb.sendLossCCmThreshould = ADAP_E_COUNT_TIME_BEFORE_START_CCM;
			}

			if(pCcmEntry->enetCcmAppDb.callback_loss_called == ADAP_FALSE)
			{
				// check if need to move port to forward
				adap_ForwardOtherPort(pCcmEntry->EcfmHwCcTxParams.u4IfIndex);
				adap_FsCallBackEventLossOfCCM(pCcmEntry->EcfmHwCcRxParams.u2RxFilterId,pCcmEntry->EcfmHwCcTxParams.u4IfIndex);
				pCcmEntry->enetCcmAppDb.callback_loss_called = MEA_TRUE;
			}

			(*pCcmState) = ADAP_E_WAIT_ON_PACKET_STATE;
			//pCcmEntry->countTimeBeforeStart=0;

			if( (rdi_enable == MEA_TRUE) && (pCcmEntry->rdiState == ADAP_FALSE) )
			{
				// set the packet gen rdi bit

				adap_FsMiHwPacketGenModifyiNFO(pCcmEntry->enetPacketGenDb.packetGenId,MEA_TRUE);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"=====> RDI bit set!!\n");
				pCcmEntry->rdiState=MEA_TRUE;
			}
		}
		else
		{
			// traffic back to service
			pCcmEntry->enetCcmAppDb.callback_loss_called = MEA_FALSE;
			pCcmEntry->enetCcmAppDb.sendLossCCm=0;

			if(pCcmEntry->rdiState == ADAP_TRUE)
			{
				adap_FsMiHwPacketGenModifyiNFO(pCcmEntry->enetPacketGenDb.packetGenId,MEA_FALSE);
				pCcmEntry->rdiState=MEA_FALSE;
				//pCcmEntry->countTimeBeforeStart=0;
			}
		}
		pCcmEntry->enetCcmAppDb.timeCheckCcmTraffic += ADAP_D_ENET_CCM_TASK_PERIOD;

		pCcmEntry->enetCcmAppDb.timeCheckCcmTraffic=0;


		// check defects
		adap_FsCcmCompareDefectCounter(&DefectEntry,&pCcmEntry->enetPacketAnaDb.DefectEntry,&lastEvent);

		// found received rdi bit set
		if( ( rdiBitMaskEvent & (1 << id_io) ) != 0)
		{
			lastEvent |= ADAP_E_CCM_RDI_BIT_SET_EVENT;
		}

		// save the last counters
		adap_memcpy(&pCcmEntry->enetPacketAnaDb.DefectEntry,&DefectEntry,sizeof(MEA_Counters_CCM_Defect_dbt));
	}

	switch((*pCcmState))
	{
		case ADAP_E_WAIT_ON_PACKET_STATE:
			(*pGood3CCmPackets)=0;
			if( (lastEvent & ADAP_E_CCM_DEFECTS) != 0)
			{
				// flag tell that we have a problem
				rcv_bad_traffic=MEA_TRUE;
			}

			// check if we don't have loss of continue
			if( ( bitMaskEvent & (1 << id_io) ) == 0)
			{
				// if the there is not defect
				if(rcv_bad_traffic == MEA_FALSE)
				{
					// if the receive counter increments
					if(lastEvent & ADAP_E_CCM_SEQUENCE_NUMBER_INCREMENT)
					{
						// we are in good packet

						/*ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"move to good packet state rcvPacket mepId:%d entry:%d link_state:%s!!\n",
														tAdap_EcfmCcmDb[ccm_id].EcfmDbParams.EcfmHwMepParams.u2MepId,
														entry_num,
														(linkState == ADAP_PORT_LINK_UP)?"Up":"Down");*/

						adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_CPU/*MEA_FALSE*/,entry_num,MEA_FALSE);
						(*pCcmState) = ADAP_E_GOOD_PACKETS_STATE;
						if(tAdap_EcfmCcmDb[ccm_id].EcfmHwMaParams.u4CcmInterval <= 10000)
						{
							adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_ANA,entry_num,MEA_FALSE);
						}

					}
				}
			}
			if(rcv_bad_traffic == MEA_TRUE)
			{
				// if we are in bad packets and the packet is comming then we are in bad traffic state
				if(lastEvent & ADAP_E_CCM_SEQUENCE_NUMBER_INCREMENT)
				{
					//(*pGood3CCmPackets)=0;
					// open the gate packet to cpu send packet to cpu
					adap_cfaLockMutex();
					adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_CPU/*MEA_FALSE*/,entry_num,MEA_FALSE);
					// next time don't check bit event

					if(tAdap_EcfmCcmDb[ccm_id].EcfmHwMaParams.u4CcmInterval <= 10000)
					{
						//usSleepFunc(10*1000);
						adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_ANA,entry_num,MEA_FALSE);
					}
					adap_cfaUnlockMutex();
					(*pCcmState) = ADAP_E_BAD_PACKETS_STATE;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"move to bad packet state mepId:%d entry:%d!!!\n",tAdap_EcfmCcmDb[ccm_id].EcfmDbParams.EcfmHwMepParams.u2MepId,entry_num);
				}
			}
			break;
		case ADAP_E_GOOD_PACKETS_STATE:

			(*pGood3CCmPackets)++;
			adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_ANA/*MEA_TRUE*/,entry_num,MEA_FALSE);

			if( (lastEvent & ADAP_E_CCM_DEFECTS) != 0)
			{
				rcv_bad_traffic=MEA_TRUE;
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"received with bad packets mepId:%d entry:%d event:%x!!!\n",
													tAdap_EcfmCcmDb[ccm_id].EcfmDbParams.EcfmHwMepParams.u2MepId,entry_num,
													lastEvent);
				(*pCcmState) = ADAP_E_WAIT_ON_PACKET_STATE;
			}
			// send a packets after 1 second of good traffic

			else if( (*pGood3CCmPackets) > 100)
			{
				if(tAdap_EcfmCcmDb[ccm_id].EcfmHwMaParams.u4CcmInterval <= 10000)
				{
					adap_cfaLockMutex();
					adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_CPU,entry_num,MEA_TRUE);
					adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_ANA,entry_num,MEA_TRUE);
					adap_cfaUnlockMutex();
					(*pGood3CCmPackets)=0;

				}
			}

			break;
		case ADAP_E_BAD_PACKETS_STATE:
			adap_FsMiHwSetRxForwarder(&tAdap_EcfmCcmDb[ccm_id],PACKET_ANALYZER_SEND_TO_ANA/*MEA_TRUE*/,entry_num,MEA_FALSE);
			if( (lastEvent & ADAP_E_CCM_DEFECTS) == 0)
			{
				// traffic not comming or good traffic came
				if( (bitMaskEvent & (1 << id_io)) == 0)
				{
					(*pGood3CCmPackets)++;
					if((*pGood3CCmPackets) > 2)
					{
						(*pCcmState) = ADAP_E_WAIT_ON_PACKET_STATE;
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"good traffic came mepId:%d entry:%d!!!\n",tAdap_EcfmCcmDb[ccm_id].EcfmDbParams.EcfmHwMepParams.u2MepId,entry_num);
					}
					break;
				}
			}
			//bad traffic came stay at this state
			(*pGood3CCmPackets)=0;
			break;
		default:
			break;
	}
}

static ADAP_Uint16 adap_getFirstIdxLmmPacketGenEntry(void)
{
	ADAP_Uint16 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LMM_CONFIGURATION;idx++)
	{
		if(LmmPacketGenDb[idx].valid==MEA_TRUE)
			{
				return idx;
			}
	}
	return UNDEFINED_VALUE16;
}

static tAdap_LmmPacketGen *adap_getLmmPacketGenEntryByIndex(ADAP_Uint16 idx)
{
	return &LmmPacketGenDb[idx];
}

static ADAP_Uint16 adap_getNextIdxLmmPacketGenEntry(ADAP_Uint16 idxStart)
{
	ADAP_Uint16 idx=0;

	idxStart++;
	if(idxStart >= ADAP_D_MAX_DATABASE_LMM_CONFIGURATION)
	{
		return UNDEFINED_VALUE16;
	}

	for(idx=idxStart;idx<ADAP_D_MAX_DATABASE_LMM_CONFIGURATION;idx++)
	{
		if(LmmPacketGenDb[idx].valid==MEA_TRUE)
			{
				return idx;
			}
	}
	return UNDEFINED_VALUE16;
}

static void adap_collect_lmm_counters(void)
{
	ADAP_Uint16 idx=0;
	tAdap_LmmPacketGen *lmmPacketGen=NULL;
	MEA_Bool exist=MEA_FALSE;
	MEA_Counters_LM_dbt entry;

	idx = adap_getFirstIdxLmmPacketGenEntry();

	while(idx != UNDEFINED_VALUE16)
	{
		lmmPacketGen = adap_getLmmPacketGenEntryByIndex(idx);

		if(lmmPacketGen != NULL)
		{
			if (MEA_API_IsExist_CCM(MEA_UNIT_0,lmmPacketGen->packetGen.id_io,&exist) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MEA_API_IsExist_CCM\n");
				return;
			}

			if(exist == MEA_FALSE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"failed by call MEA_API_IsExist_CCM\n");
			}
			else
			{
				if (MEA_API_Collect_Counters_LM(MEA_UNIT_0,lmmPacketGen->packetGen.id_io,NULL) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MEA_API_Collect_Counters_LM\n");
					return;
				}
				if (MEA_API_Get_Counters_LM(MEA_UNIT_0,lmmPacketGen->packetGen.id_io,&entry) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MEA_API_Get_Counters_LM\n");
					return;
				}
			}
		}
		idx = adap_getNextIdxLmmPacketGenEntry(idx);
	}

}

/*******************************************************************************
* adap_FsBrgFlushPortVpn
*
* DESCRIPTION:
* This will delete all the FDB entries learned in this logical port according to the VPN number
*
* INPUTS:
* u4IfIndex  - logical Interface Index
* VpnId	  -  VPN Id
* OUTPUTS:
* None
*
* RETURNS:
* FNP_SUCCESS - on success
* FNP_FAILURE - on error
*
* COMMENTS:
*  None

*
*******************************************************************************/
ADAP_Int8 adap_FsBrgFlushPortVpn (ADAP_Uint32 u4IfIndex, ADAP_Uint16 VpnId)
{
    MEA_Uint16               	index;
    MEA_SE_Entry_dbt          	entry;
    MEA_SE_filter_Entry_dbt     filter_entry;
    MEA_Port_t               	EnetPortId;
    MEA_Bool                 	found;
    ADAP_Uint16					PortsCount = 0, PortsIndex;
    ADAP_Uint16					masterPort=0;

    /* Now set the filter (filter is created during initialization) */
    index = 1;

    adap_memset(&filter_entry, 0, sizeof(filter_entry));

    if(MEA_API_SE_Entry_IsExistFilter (MEA_UNIT_0, index)!=MEA_TRUE)
    {
        printf("Error on MEA_API_SE_Entry_IsExistFilter index %d\n",index);
        return ENET_FAILURE;
    }
 	masterPort = u4IfIndex;
	if(adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
		masterPort = adap_GetLagMasterPort(u4IfIndex);
	}
    /* Convert u4IfIndex to the ENET portId */
	MeaAdapGetPhyPortFromLogPort(masterPort, &EnetPortId);
    printf("issPort:%d, enetPortId:%d\n",u4IfIndex, EnetPortId);

    filter_entry.fwd_fields_key.type =  MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
    filter_entry.fwd_fields_key.mac_plus_vpn.VPN = VpnId;
    filter_entry.fwd_fields_data.static_flag = 0;
    filter_entry.fwd_mask_key_enable = MEA_TRUE;
    filter_entry.fwd_mask_key.type = 7; //MEA_SE_FORWARDING_KEY_TYPE_DA;
    filter_entry.fwd_mask_key.mac_plus_vpn.VPN = 0x1ff;
    filter_entry.fwd_mask_data.static_flag = 1;


    if(MEA_API_SE_Entry_SetFilter (MEA_UNIT_0, index, &filter_entry)!=MEA_OK)
    {
        printf("Error on MEA_API_SE_Entry_CreateFilter index %d\n",index);
        return ENET_FAILURE;
    }
#ifdef USE_ENET_MUTEX
    ENET_FWD_Lock();
#endif
    entry.filterEnable 	= MEA_TRUE;
    entry.filterId      = index;

    if (ENET_ADAPTOR_GetFirst_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK)
    {
            printf("Error on ENET_ADAPTOR_GetFirst_SE_Entry\n");
#ifdef USE_ENET_MUTEX
            ENET_FWD_Unlock();
#endif
            return ENET_FAILURE;
    }

    while (found == MEA_TRUE)
    {
            /* delete mac which is dynamic and learnt in portIdx */
            if (! entry.data.static_flag)
            {
                    if(MEA_IS_SET_OUTPORT(&(entry.data.OutPorts), EnetPortId))
                    {
                            PortsCount = 0;
                            MEA_OUTPORT_FOR_LOOP(&entry.data.OutPorts, PortsIndex)
                            {
                                    PortsCount++;
                            }
                    }
                    printf("PortsCount: %d\n", PortsCount);
                    if(PortsCount == 1)
                    {
                            if (ENET_ADAPTOR_Delete_SE_Entry(MEA_UNIT_0,	&(entry.key)) != MEA_OK)
                            {
                                    printf("Error on ENET_ADAPTOR_Delete_SE_Entry\n");
#ifdef USE_ENET_MUTEX
                                    ENET_FWD_Unlock();
#endif
                                    return ENET_FAILURE;
                            }
                            printf("FsBrgFlushPortVpn - entry  for ISS port:%d is deleted!\n", u4IfIndex);
                    }
            }

        if (ENET_ADAPTOR_GetNext_SE_Entry(MEA_UNIT_0, &entry, &found) != MEA_OK)
        {
            printf("Error on ENET_ADAPTOR_GetNext_SE_Entry\n");
#ifdef USE_ENET_MUTEX
            ENET_FWD_Unlock();
#endif
            return ENET_FAILURE;
        }
    }

#ifdef USE_ENET_MUTEX
    ENET_FWD_Unlock();
#endif

    return ENET_SUCCESS;
}

static void* adap_FsMiEcfmHandler(void *arg)
{
	ADAP_Uint16 idx;
	ADAP_Uint32 ifIndex;
	ADAP_Uint32 vpnIndex;
	//ADAP_Uint32 linkStatusChange;
	ADAP_Uint32 collectLmCounter=0;
	MEA_Uint32 bitMaskEvent=0;
	MEA_Uint32 lastBitMaskEvent=0xFFFFFFFF;
	MEA_Uint32 rdiBitMaskEvent=0;
	MEA_Uint32 lastrdiBitMaskEvent=0xFFFFFFFF;
	ADAP_Int32 issPortList=0;
	id_t pid;
	int priority = -20;
	int ret=0;
	ADAP_Uint32 timer_execute=0;


	pid = getpid();
	ret = setpriority(PRIO_PROCESS, pid, priority);
	if(ret != 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"setpriority:%d return:%d\n",ret);
	}


    // Here should be race-condition prevention in real code.
    while(1)
    {

    	if(adap_isProcessTerminated() == ADAP_TRUE)
    	{
    		return NULL;
    	}

	adap_signal_condMutexLock_ccm();
        if (adap_signal_condtimer_eventFlag() != 0)
        {
            if (adap_signal_condtimer_eventFlag() & ADAP_FLAG_EVENT_TIMER)
            {
				adap_signal_condtimer_clearFlag(ADAP_FLAG_EVENT_TIMER);
				timer_execute++;
            }

            if (adap_signal_condtimer_eventFlag() & ADAP_FLAG_EVENT_CCM)
            {
				adap_signal_condtimer_clearFlag(ADAP_FLAG_EVENT_CCM);
            }

// Ask Alex
#if 0
			linkStatusChange += ADAP_D_ENET_CCM_TASK_PERIOD;
			if(linkStatusChange >= ADAP_D_ENET_LINK_STATUS_PERIOD)
			{
				adap_update_iss_with_link_status();
				linkStatusChange=0;
			}
#endif /* if 0 */

			bitMaskEvent=0;
			rdiBitMaskEvent=0;
			MEA_API_Lock();
			adap_read_event_group(&bitMaskEvent);
			adap_read_rdi_event_group(&rdiBitMaskEvent);
			MEA_API_Unlock();

			if(lastBitMaskEvent != bitMaskEvent)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"CCM loss lastBitMask:%x currBitMask:%d\n",lastBitMaskEvent,bitMaskEvent);
				lastBitMaskEvent = bitMaskEvent;
			}

			if(lastrdiBitMaskEvent != rdiBitMaskEvent)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"CCM rdi lastBitMask:%x currBitMask:%d\n",lastrdiBitMaskEvent,rdiBitMaskEvent);
				lastrdiBitMaskEvent = rdiBitMaskEvent;
			}

			for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
			{
				if(tAdap_EcfmCcmDb[idx].EcfmDbParams.packetAnaParams_active != MEA_TRUE)
				{
					continue;
				}

				if(tAdap_EcfmCcmDb[idx].EcfmDbParams.EcfmHwMepParams.u1Direction == 2) //ECFM_NP_MP_DIR_UP
				{
					for(issPortList=0;issPortList < tAdap_EcfmCcmDb[idx].EcfmDbParams.i4Length;issPortList++)
					{
						MEA_API_Lock();
						adap_ccm_state_machine(idx,bitMaskEvent,rdiBitMaskEvent,issPortList);
						MEA_API_Unlock();
					}
				}
				else
				{

					MEA_API_Lock();
					adap_ccm_state_machine(idx,bitMaskEvent,rdiBitMaskEvent,0);
					MEA_API_Unlock();
				}
			}
			// in case there event that not cleaned
			MEA_API_Lock();
			adap_read_event_group(&bitMaskEvent);
			adap_read_rdi_event_group(&rdiBitMaskEvent);
			MEA_API_Unlock();

			collectLmCounter += ADAP_D_ENET_CCM_TASK_PERIOD;
			if(collectLmCounter >= ADAP_D_ENET_LM_COLLECT_PERIOD)
			{
				adap_collect_lmm_counters();
				collectLmCounter=0;
			}
		}

		// Mutex is locked. It is unlocked while we are waiting.
		// waiting on signal
		adap_signal_condWait_ccm();
		// Mutex is locked wait on signal

		// We unlock mutex after dealing with all events
		adap_signal_condMutexUnLock_ccm();


		/* every 2 seconds */
		if( (timer_execute % 40) == 0)
		{
			for(ifIndex=1;ifIndex<MeaAdapGetNumOfPhyPorts();ifIndex++)
			{
				for(vpnIndex=0;vpnIndex<ADAP_MAX_NUM_OF_VPNS;vpnIndex++)
				{
					if(adap_CompareCurrFdbFlush(ifIndex,vpnIndex) > 0)
					{
						adap_FsBrgFlushPortVpn (ifIndex,vpnIndex);
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"flush MAC IfIndex:%d vpn:%d\n",ifIndex,vpnIndex);
					}
				}
				adap_copyLastToCurrFdbFlush(ifIndex);
			}
		}
    }

    // ... as we are dying.
    return NULL;
}

ADAP_Uint32 adap_PacketAnalyzerFreeTable(ADAP_Uint32 entry)
{

	if(entry >= ADAP_D_MAX_NUM_OF_ANALYZERS)
	{
		return ENET_FAILURE;
	}

	adap_PacketAnalyzerData[entry].bFreeAnalyzer = MEA_TRUE;
	return ENET_SUCCESS;
}

tAdap_LbmPacketGen *adap_getLbmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LBM_CONFIGURATION;idx++)
	{
		if( (lbmPacketGenDb[idx].valid==MEA_TRUE) &&
			(lbmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				return &lbmPacketGenDb[idx];
			}
	}
	return NULL;
}

ADAP_Uint32 adap_allocLbmPacketGenEntry(tEnetLbmPacketGen *packetGen,tEnetHal_EcfmMepInfoParams *pEcfmMepInfoParams)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LBM_CONFIGURATION;idx++)
	{
		if(lbmPacketGenDb[idx].valid==MEA_FALSE)
		{
			adap_memcpy(&lbmPacketGenDb[idx].packetGen,packetGen,sizeof(tEnetLbmPacketGen));
			adap_memcpy(&lbmPacketGenDb[idx].tEcfmMepInfoParams,pEcfmMepInfoParams,sizeof(tEnetHal_EcfmMepInfoParams));
			lbmPacketGenDb[idx].valid=MEA_TRUE;
			return ENET_SUCCESS;
		}
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_allocLbmPacketGenEntry not enough room\n");
	return ENET_FAILURE;
}

ADAP_Uint32 adap_freeLbmPacketGenEntry(ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint32 idx=0;

	for(idx=0;idx<ADAP_D_MAX_DATABASE_LBM_CONFIGURATION;idx++)
	{
		if( (lbmPacketGenDb[idx].valid==MEA_TRUE) &&
			(lbmPacketGenDb[idx].tEcfmMepInfoParams.u4IfIndex == u4IfIndex) )
			{
				lbmPacketGenDb[idx].valid = MEA_FALSE;
				return ENET_SUCCESS;
			}
	}
	return ENET_FAILURE;
}

MEA_Status adap_FsMiHwSetPortShaper(MEA_Port_t	enetPort,MEA_EirCir_t CIR,MEA_EbsCbs_t CBS)
{
	MEA_EgressPort_Entry_dbt entry;

   if (MEA_API_Get_EgressPort_Entry(MEA_UNIT_0,enetPort,&entry) != MEA_OK) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Get_EgressPort_Entry for port %d failed\n",enetPort);
        return MEA_ERROR;
    }

	entry.shaper_enable = ENET_TRUE;
	entry.shaper_info.CIR = CIR;
	entry.shaper_info.CBS = CBS;
	entry.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_BIT;
	entry.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper_info.overhead = 0;
	entry.shaper_info.Cell_Overhead = 0;
	entry.shaper_info.mode=0;

    if (MEA_API_Set_EgressPort_Entry(MEA_UNIT_0,enetPort,&entry) != MEA_OK) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: MEA_API_Set_EgressPort_Entry for port %d failed\n",enetPort);
        return MEA_ERROR;
    }
	return MEA_OK;
}

static void* adap_FsMiTimerEcfmHandler(void *arg)
{
    struct timeval tv;
	int timerOut=0;

	for(;;)
	{
		tv.tv_sec = 0;
		tv.tv_usec = ADAP_D_ENET_CCM_TASK_PERIOD;
		select(0,NULL,NULL,NULL,&tv);
		adap_signal_condtimer_event();

    	if(adap_isProcessTerminated() == ADAP_TRUE)
    	{
    		return NULL;
    	}

		timerOut++;
	}
	return NULL;
}

static ADAP_Int32 adap_FsMiInitEcfmHandler(void)
{

    pthread_create(&adap_thread_EcfmHandler, NULL, adap_FsMiEcfmHandler, NULL);
	pthread_create(&adap_idTimer_EcfmHandler, NULL, adap_FsMiTimerEcfmHandler, NULL);
	adap_FsMiHwSetPortShaper(120,2000000,640);

	return ENET_SUCCESS;
}

static MEA_Status adap_FsMiDeleteDmmPacketGen(tAdap_DmmPacketGen *pPacketGen)
{
	MEA_Bool bValid=MEA_FALSE;
	MEA_Action_t 	get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t		enetPort;

	if(pPacketGen->packetGen.serviceId != UNDEFINED_VALUE16)
	{
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, pPacketGen->packetGen.serviceId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the service %d\n", pPacketGen->packetGen.serviceId);
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",pPacketGen->packetGen.serviceId);
		pPacketGen->packetGen.serviceId = UNDEFINED_VALUE16;
	}
	adap_FreeGenVpnId(pPacketGen->packetGen.VpnIndex);

	if(pPacketGen->packetGen.PacketGeId != UNDEFINED_VALUE16)
	{
		if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,pPacketGen->packetGen.PacketGeId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the packetGen %d\n", pPacketGen->packetGen.PacketGeId);
			return MEA_ERROR;
		}
		adap_freePacketSmallGenTable(pPacketGen->packetGen.PacketGeId);
		pPacketGen->packetGen.PacketGeId = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteDmmPacketGen: profileId=%d\n",pPacketGen->packetGen.packetGenProfileId);
		adap_freePacketGenProfile(pPacketGen->packetGen.packetGenProfileId,&bValid);
		if(bValid == MEA_TRUE)
		{
			if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pPacketGen->packetGen.packetGenProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the packetGen profile:%d\n", pPacketGen->packetGen.packetGenProfileId);
				return MEA_ERROR;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteDmmPacketGen: profileId=%d deleted\n",pPacketGen->packetGen.packetGenProfileId);
			pPacketGen->packetGen.packetGenProfileId = UNDEFINED_VALUE16;
		}
	}
	if(pPacketGen->packetGen.internalVlan != UNDEFINED_VALUE16)
	{
		if(adap_FsMiHwDeleteRxForwarder(pPacketGen->packetGen.internalVlan,
									pPacketGen->packetGen.VpnIndex,
									pPacketGen->tEcfmMepInfoParams.meLevel,
									ADAP_D_DMM_OPCODE_VALUE,
									120,
									&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen: delete forwarder: entry not found\n");
		}
		adap_freeInternalVlanId(pPacketGen->packetGen.internalVlan);
		pPacketGen->packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	MeaAdapGetPhyPortFromLogPort(pPacketGen->tEcfmMepInfoParams.u4IfIndex, &enetPort);
	if(adap_FsMiHwDeleteRxForwarder(pPacketGen->tEcfmMepInfoParams.VlanTag.OuterVlanTag.u2VlanId,
								pPacketGen->packetGen.analyzerVpnIndex,
								pPacketGen->tEcfmMepInfoParams.meLevel,
								ADAP_D_DMR_OPCODE_VALUE,
								enetPort,
								&get_paction_id) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen: delete forwarder: entry not found\n");
	}


	if(pPacketGen->packetGen.action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.action_id = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.analyzer_action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.analyzer_action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.analyzer_action_id = UNDEFINED_VALUE16;
	}
	adap_PacketAnalyzerFreeTable(pPacketGen->packetGen.analyzerId);
	pPacketGen->packetGen.analyzerId = UNDEFINED_VALUE16;
	adap_freeDmmPacketGenEntry(pPacketGen->tEcfmMepInfoParams.u4IfIndex);

	return MEA_OK;
}


static MEA_Status adap_FsMiCompareDmmPacketGen(tAdap_DmmPacketGen *pPacketGen,tAdap_EcfmMepDmmInfoParams *pDmmInfo,MEA_Bool *delEntry)
{
	(*delEntry) = MEA_FALSE;
	if( (pPacketGen->tEcfmMepInfoParams.VlanTag.InnerVlanTag.u2VlanId != pDmmInfo->VlanTag.InnerVlanTag.u2VlanId) ||
	  (pPacketGen->tEcfmMepInfoParams.VlanTag.OuterVlanTag.u2VlanId != pDmmInfo->VlanTag.OuterVlanTag.u2VlanId) ||
	  (pPacketGen->tEcfmMepInfoParams.meLevel != pDmmInfo->meLevel) ||
	  (adap_memcmp(&pPacketGen->tEcfmMepInfoParams.destMac,&pDmmInfo->destMac,sizeof(tEnetHal_MacAddr)) != 0) )
	  {
		  (*delEntry) = MEA_TRUE;
		  return adap_FsMiDeleteDmmPacketGen(pPacketGen);
	  }
	return MEA_OK;
}

MEA_Status adap_FsMiHwSetGenericAnalyzer(MEA_Analyzer_t id,int seqNumber)
{
    MEA_Analyzer_configure_dbt    entry;

	 adap_memset(&entry,0,sizeof(entry));


	if(MEA_API_Get_Analyzer(MEA_UNIT_0,id,&entry) !=  MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't get MEA_API_Get_Analyzer entry:%d \n",id);
         return MEA_ERROR;
	}

	entry.activate = 1;
	entry.enable_clearSeq=1;
	entry.resetPrbs=0;
	entry.value_nextSeq=seqNumber;

    if(MEA_API_Set_Analyzer(MEA_UNIT_0,id,&entry)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't get MEA_API_Set_Analyzer entry:%d \n",id);
         return MEA_ERROR;
    }
	return MEA_OK;
}

static MEA_Status adap_FsMiHwCreateDmmCfm(tAdap_EcfmMepDmmInfoParams *pDmmInfo)
{
	tEnetHal_MacAddr srcmacAddr;
	tAdap_PacketGenHeader PacketGen;
	tRmtSrvInfo Info;
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 vpnIndex=0;
	tAdap_DmmPacketGen *pPacketGen=NULL;
	tEnetDmmPacketGen enetpacketGen;
	ADAP_Uint16 genVlan=0;
	MEA_Bool newConfig=MEA_TRUE;
	MEA_Port_t	enetPort;

	pPacketGen = adap_getDmmPacketGenEntry(pDmmInfo->u4IfIndex);
	if(pPacketGen != NULL)
	{
		if(adap_FsMiCompareDmmPacketGen(pPacketGen,pDmmInfo,&newConfig) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to delete dmm configuration\n");
		}
		if(newConfig == MEA_FALSE)
		{
			adap_memcpy(&enetpacketGen,&pPacketGen->packetGen,sizeof(tEnetDmmPacketGen));
		}

	}

	if(newConfig == MEA_TRUE)
	{
		genVlan = adap_allocateInternalVlanId();
		enetpacketGen.packetGenProfileId = adap_getPacketGenProfile(55);
		enetpacketGen.internalVlan = genVlan;

		if(enetpacketGen.packetGenProfileId != UNDEFINED_VALUE16)
		{
			adap_allocPacketGenProfile(55,enetpacketGen.packetGenProfileId);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateDmmCfm: profile:%d allready exist\n",enetpacketGen.packetGenProfileId);

		}
		else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
		{
			if(adap_FsMiHwPacketGenProfInfoCreate(55,&enetpacketGen.packetGenProfileId,MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm: create packet gen profile failed\n");
				return MEA_ERROR;
			}
			adap_allocPacketGenProfile(55,enetpacketGen.packetGenProfileId);
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreatedmmCfm: not enough room to create new packet gen profile\n");
			return MEA_ERROR;
		}

		// create packet generator
		if(adap_FsMiHwGetInterfaceCfmOamUCMac(pDmmInfo->u4IfIndex,&srcmacAddr) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwGetInterfaceCfmOamUCMac\n");
			return MEA_ERROR;
		}

		adap_mac_to_arr(&pDmmInfo->destMac, PacketGen.DA.b);
		adap_mac_to_arr(&srcmacAddr, PacketGen.SA.b);
		PacketGen.num_of_vlans=1;
		PacketGen.outerVlan=0x81000000 | genVlan | ( (pDmmInfo->VlanTag.OuterVlanTag.u1Priority & 0x7) << 13);
		PacketGen.innerVlan=0x81000000;
		PacketGen.cfmEtherType=0x8902;
		PacketGen.meg_level=pDmmInfo->meLevel; //3 bits
		PacketGen.version=0; //5 bits
		PacketGen.opCode=ADAP_D_DMM_OPCODE_VALUE; // 8 bits
		PacketGen.flags=pDmmInfo->flags;
		PacketGen.tlvOffset=pDmmInfo->tlvOffset;
		PacketGen.initSeqNum=0;

		if(adap_FsMiHwPacketGenDmmCreateiNFO(&PacketGen,enetpacketGen.packetGenProfileId,&enetpacketGen.PacketGeId,1,MEA_TRUE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwPacketGenLbmCreateiNFO\n");
			return MEA_ERROR;
		}

		adap_AllocPacketSmallGenTable(enetpacketGen.PacketGeId);

		Info.b_lmEnable = MEA_FALSE;
		Info.lmId=0;
		Info.keyType=MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
		Info.me_level=pDmmInfo->meLevel; //3 bit
		Info.me_valid=MEA_TRUE;
		Info.src_port=120;
		Info.vlanId=genVlan;
		Info.EnetPolilcingProfile=adap_GetDefaultPolicerId(1);

		pDmmInfo->lmId = adap_allocateLmId(pDmmInfo->VlanTag.OuterVlanTag.u2VlanId,pDmmInfo->u4IfIndex) ;



		if(adap_FsMiHwCreateCfmService(&enetpacketGen.serviceId,&Info) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwCreateCfmService\n");
			return MEA_ERROR;
		}
		vpnIndex = Info.vpn;
		enetpacketGen.VpnIndex = vpnIndex;

		adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

		tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info.ehp_data.LmCounterId_info.LmId = pDmmInfo->lmId; // create lmId pConfig->lmId;
		tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
		tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_TX;

		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = pDmmInfo->VlanTag.OuterVlanTag.u2VlanId;

		tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
		//create action
		if(adap_FsMiHwCreateCfmAction(&enetpacketGen.action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_ENABLE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwCreateCfmAction\n");
			return MEA_ERROR;
		}

		MeaAdapGetPhyPortFromLogPort(pDmmInfo->u4IfIndex, &enetPort);
		MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
		MEA_SET_OUTPORT(&tOutPorts, enetPort);


		if(adap_FsMiHwCreateRxForwarder(enetpacketGen.action_id,
									vpnIndex,
									&tOutPorts,pDmmInfo->meLevel,
									ADAP_D_DMM_OPCODE_VALUE,
									genVlan,
									120) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwCreateTxForwarder\n");
			return MEA_ERROR;
		}

		if(adap_FsMiHwPacketGenActive(enetpacketGen.PacketGeId,MEA_TRUE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwPacketGenActive\n");
			return MEA_ERROR;
		}


		// packet analyzer
		if(adap_MiVlanSetServiceMdLevel (pDmmInfo->VlanTag.OuterVlanTag.u2VlanId,
										pDmmInfo->u4IfIndex,
										D_TAGGED_SERVICE_TAG,
										MEA_TRUE,
										pDmmInfo->meLevel) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_MiVlanSetServiceMdLevel\n");
			return MEA_ERROR;
		}

		adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
		adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

		enetpacketGen.analyzerId = adap_PacketAnalyzerAllocTable();
		if(enetpacketGen.analyzerId == UNDEFINED_VALUE16)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to allocate packet analyzer id\n");
			return MEA_ERROR;
		}
		adap_FsMiHwSetGenericAnalyzer(enetpacketGen.analyzerId,0);

		tEhp_info.ehp_data.LmCounterId_info.valid = MEA_TRUE;
		tEhp_info.ehp_data.LmCounterId_info.LmId = pDmmInfo->lmId; // create lmId pConfig->lmId;
		tEhp_info.ehp_data.LmCounterId_info.Command_dasa=0;
		tEhp_info.ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_RX;


		tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
		tEhp_info.ehp_data.eth_info.val.vlan.vid      = enetpacketGen.analyzerId;


		tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
		tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
		tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
		tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

		MEA_SET_OUTPORT(&tEhp_info.output_info,120);
		MEA_SET_OUTPORT(&tOutPorts, 120);
		MEA_SET_OUTPORT(&tEhp_info.output_info,127);
		MEA_SET_OUTPORT(&tOutPorts, 127);


		//create action
		if(adap_FsMiHwCreateCfmAction(&enetpacketGen.analyzer_action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwCreateCfmAction\n");
			return MEA_ERROR;
		}

		vpnIndex = adap_GetVpnByVlan (pDmmInfo->VlanTag.OuterVlanTag.u2VlanId);
		if (vpnIndex == ADAP_END_OF_TABLE)
		{
		   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm FAIL: adap_GetVpnByVlan failed for VlanId:%d\n", pDmmInfo->VlanTag.OuterVlanTag.u2VlanId);
		   return MEA_ERROR;
		}
		MeaAdapGetPhyPortFromLogPort(pDmmInfo->u4IfIndex, &enetPort);
		enetpacketGen.analyzerVpnIndex=vpnIndex;
		if(adap_FsMiHwCreateRxForwarder(enetpacketGen.analyzer_action_id,
									vpnIndex,
									&tOutPorts,
									pDmmInfo->meLevel,
									ADAP_D_DMR_OPCODE_VALUE,
									pDmmInfo->VlanTag.OuterVlanTag.u2VlanId,
									enetPort) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwCreateRxForwarder\n");
			return MEA_ERROR;
		}

		adap_allocDmmPacketGenEntry(&enetpacketGen,pDmmInfo);
	}

	return MEA_OK;

}

ADAP_Int32 adap_FsMiLacpLxcpOamPduUpdate(ADAP_Bool/*BOOL1*/ add,MEA_LxCp_t   LxCp_Id)
{
    MEA_LxCP_Protocol_key_dbt     key_pi;
    MEA_LxCP_Protocol_data_dbt    data_pi;

    adap_memset(&key_pi, 0, sizeof(key_pi))	;
    adap_memset(&data_pi, 0, sizeof(data_pi))	;

    /* Update the action for the specific protocol */
	key_pi.protocol = MEA_LXCP_PROTOCOL_IN_BPDU_2; //  OAM PDU discovery , with EtherType 8809 --> EFM 802.1 ah*/

    /* Update all ports' LxCp */
	if(MEA_API_Get_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: MEA_API_Get_LxCP_Protocol FAIL for LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

	switch (add)
	{
        case ADAP_FALSE :
			// if allready then do nothing
			if(data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT)
			{
				return ENET_SUCCESS;
			}
            /* Remove LxCp protocol by defining it as "Transparent" */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT;
        break;
        case ADAP_TRUE :
			// if allready then do nothing
			if(data_pi.action_type == MEA_LXCP_PROTOCOL_ACTION_CPU)
			{
				return ENET_SUCCESS;
			}
            /* Forward the LxCp protocol to CPU */
            data_pi.action_type = MEA_LXCP_PROTOCOL_ACTION_CPU;
        break;
        default:
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate FAIL: Invalid Add/Delete opcode:%d \n", add);
            return ENET_FAILURE;
	}

	/* Update the ENET */
	if(ENET_ADAPTOR_Set_LxCP_Protocol(MEA_UNIT_0, LxCp_Id, &key_pi, &data_pi) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiVlanHwLacpLxcpUpdate:  ENET_ADAPTOR_Set_LxCP_Protocol FAIL! LxCp Id:%d\n",LxCp_Id);
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

static MEA_Status adap_FsMiHwCreate1DmCfm(tAdap_EcfmMepDmmInfoParams *p1DmInfo)
{
	tEnetHal_MacAddr srcmacAddr;
	tAdap_PacketGenHeader PacketGen;
	tRmtSrvInfo Info;
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 vpnIndex=0;
	tEnetDmmPacketGen enetpacketGen;
	ADAP_Uint16 genVlan=0;
	MEA_Bool bValid=MEA_FALSE;
	MEA_Action_t get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t	 enetPort;

	adap_memset(&enetpacketGen,0,sizeof(tEnetDmmPacketGen));

	genVlan = adap_allocateInternalVlanId();

	enetpacketGen.packetGenProfileId = adap_getPacketGenProfile(39);

	if(enetpacketGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		adap_allocPacketGenProfile(39,enetpacketGen.packetGenProfileId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreate1DmCfm: profile:%d allready exist\n",enetpacketGen.packetGenProfileId);

	}
	else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
	{
		if(adap_FsMiHwPacketGenProfInfoCreate(39,&enetpacketGen.packetGenProfileId,MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm: create packet gen profile failed\n");
			return MEA_ERROR;
		}
		adap_allocPacketGenProfile(39,enetpacketGen.packetGenProfileId);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm: not enough room to create new packet gen profile\n");
		return MEA_ERROR;
	}

	// create packet generator
	if(adap_FsMiHwGetInterfaceCfmOamUCMac(p1DmInfo->u4IfIndex, &srcmacAddr) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm failed to call adap_FsMiHwGetInterfaceCfmOamUCMac\n");
		return MEA_ERROR;
	}

	adap_mac_to_arr(&p1DmInfo->destMac, PacketGen.DA.b);
	adap_mac_to_arr(&srcmacAddr, PacketGen.SA.b);
	PacketGen.num_of_vlans=1;
	PacketGen.outerVlan=0x81000000 | genVlan | ( (p1DmInfo->VlanTag.OuterVlanTag.u1Priority & 0x7) << 13);
	PacketGen.innerVlan=0x81000000;
	PacketGen.cfmEtherType=0x8902;
	PacketGen.meg_level=p1DmInfo->meLevel; //3 bits
	PacketGen.version=0; //5 bits
	PacketGen.opCode=ADAP_D_1DM_OPCODE_VALUE; // 8 bits
	PacketGen.flags=p1DmInfo->flags;
	PacketGen.tlvOffset=p1DmInfo->tlvOffset;
	PacketGen.initSeqNum=0;

	if(adap_FsMiHwPacketGenDmmCreateiNFO(&PacketGen,enetpacketGen.packetGenProfileId,&enetpacketGen.PacketGeId,1,MEA_FALSE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm failed to call adap_FsMiHwPacketGenLbmCreateiNFO\n");
		return MEA_ERROR;
	}

	adap_AllocPacketSmallGenTable(enetpacketGen.PacketGeId);

	Info.b_lmEnable = MEA_FALSE;
	Info.lmId=0;
	Info.keyType=MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
	Info.me_level=p1DmInfo->meLevel; //3 bit
	Info.me_valid=MEA_TRUE;
	Info.src_port=120;
	Info.vlanId=genVlan;
	Info.EnetPolilcingProfile=adap_GetDefaultPolicerId(1);



	if(adap_FsMiHwCreateCfmService(&enetpacketGen.serviceId,&Info) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm failed to call adap_FsMiHwCreateCfmService\n");
		return MEA_ERROR;
	}
	vpnIndex = Info.vpn;
	enetpacketGen.VpnIndex = vpnIndex;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = p1DmInfo->VlanTag.OuterVlanTag.u2VlanId;

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	//create action
	if(adap_FsMiHwCreateCfmAction(&enetpacketGen.action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm failed to call adap_FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}

	MeaAdapGetPhyPortFromLogPort(p1DmInfo->u4IfIndex, &enetPort);
	MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);



	if(adap_FsMiHwCreateRxForwarder(enetpacketGen.action_id,
								vpnIndex,
								&tOutPorts,p1DmInfo->meLevel,
								ADAP_D_1DM_OPCODE_VALUE,
								genVlan,
								enetPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreate1DmCfm failed to call adap_FsMiHwCreateTxForwarder\n");
		return MEA_ERROR;
	}

	if(adap_FsMiHwPacketGenActive(enetpacketGen.PacketGeId,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateDmmCfm failed to call adap_FsMiHwPacketGenActive\n");
		return MEA_ERROR;
	}

	// wait 10milisecond

	// delete the session
	if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, enetpacketGen.serviceId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the service %d\n", enetpacketGen.serviceId);
		return MEA_ERROR;
	}
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",enetpacketGen.serviceId);
	adap_FreeGenVpnId(enetpacketGen.VpnIndex);

	if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,enetpacketGen.PacketGeId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the packetGen %d\n", enetpacketGen.PacketGeId);
		return MEA_ERROR;
	}
	adap_freePacketSmallGenTable(enetpacketGen.PacketGeId);


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteDmmPacketGen: profileId=%d\n",enetpacketGen.packetGenProfileId);
	adap_freePacketGenProfile(enetpacketGen.packetGenProfileId,&bValid);
	if(bValid == MEA_TRUE)
	{
		if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,enetpacketGen.packetGenProfileId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: Unable to delete the packetGen profile:%d\n", enetpacketGen.packetGenProfileId);
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiDeleteDmmPacketGen: profileId=%d deleted\n",enetpacketGen.packetGenProfileId);
	}

	if(adap_FsMiHwDeleteRxForwarder(genVlan,
								enetpacketGen.VpnIndex,
								p1DmInfo->meLevel,
								ADAP_D_1DM_OPCODE_VALUE,
								enetPort,
								&get_paction_id) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen: delete forwarder: entry not found\n");
	}
	adap_freeInternalVlanId(genVlan);


	if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, enetpacketGen.action_id) != MEA_OK )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiDeleteDmmPacketGen ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
		return MEA_ERROR;
	}


	return ENET_SUCCESS;
}

MEA_Status adap_FsMiHwPacketGenLbmCreateiNFO(tAdap_PacketGenHeader *pPacketGen,MEA_PacketGen_profile_info_t Id_io,MEA_PacketGen_t  *pId_io,MEA_Uint32 packetPerSecond)
{
	MEA_PacketGen_Entry_dbt entry;
	MEA_Uint32  oamInfo=0;

	adap_memset(&entry,0,sizeof(MEA_PacketGen_Entry_dbt));

	entry.profile_info.valid =1;
	entry.profile_info.id =Id_io;
	entry.profile_info.headerType =MEA_PACKET_GEN_HEADER_TYPE_1; //120 bytes

	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->DA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //DA
	adap_memcpy(&entry.header.data[entry.header.length],pPacketGen->SA.b,ETH_ALEN);
	entry.header.length	+= ETH_ALEN; //SA

	if(pPacketGen->num_of_vlans)
	{
		adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->outerVlan,sizeof(MEA_Uint32));
		entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		if(pPacketGen->num_of_vlans > 1)
		{
			adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->innerVlan,sizeof(MEA_Uint32));
			entry.header.length	+= sizeof(MEA_Uint32); //VLAN
		}
	}
	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->cfmEtherType,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType

	oamInfo =  ( ( pPacketGen->meg_level & 0x000007) << 5);
	oamInfo |=  (pPacketGen->version & 0x0000F8);
	entry.header.data[entry.header.length++] = oamInfo & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->opCode & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->flags & 0xFF;
	entry.header.data[entry.header.length++] = pPacketGen->tlvOffset & 0xFF;


	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->initSeqNum,sizeof(MEA_Uint32));
	entry.header.length	+= sizeof(MEA_Uint32); //seq number

	entry.header.data[entry.header.length++] = pPacketGen->type & 0xFF;
	adap_memcpy(&entry.header.data[entry.header.length],&pPacketGen->Length,sizeof(MEA_Uint16));
	entry.header.length	+= sizeof(MEA_Uint16); //etherType
	entry.header.data[entry.header.length++] = pPacketGen->Pattern_Type & 0xFF;


	entry.header.length++; //end of tlv


	entry.control.numOf_packets	= 0xFFFF; //forever

	entry.control.active = 0; //active=1 start transmit

	entry.control.loop = 0; //

	entry.control.Packet_error=0; // insert error to packet

	entry.control.Burst_size=1;

	entry.setting.resetPrbs	= 1; //case send prbs it could to reset the prbs


	entry.data.pattern_type   = MEA_PACKET_GEN_DATA_PATTERN_TYPE_VALUE; // MEA_PacketGen_DataPatternType_te


    entry.data.pattern_value.s  = MEA_PACKET_GEN_DATA_PATTERN_VALUE_AA55;

	entry.data.withCRC        = 0; // done by HW


	entry.length.type  = MEA_PACKET_GEN_LENGTH_TYPE_FIX_VALUE; //MEA_PacketGen_LengthType_te
	entry.length.start = 128;
	entry.length.end   = 128;
	entry.length.step = 1;

	entry.shaper.shaper_enable = ENET_TRUE;
	entry.shaper.shaper_info.CIR  = packetPerSecond ;
	entry.shaper.shaper_info.CBS = 0;
	entry.shaper.shaper_info.resolution_type = ENET_SHAPER_RESOLUTION_TYPE_PKT;
	entry.shaper.Shaper_compensation = ENET_SHAPER_COMPENSATION_TYPE_PPACKET;
	entry.shaper.shaper_info.overhead = 0 ;
	entry.shaper.shaper_info.Cell_Overhead = 0;


	(*pId_io)=MEA_PLAT_GENERATE_NEW_ID;
	if(MEA_API_Create_PacketGen_Entry(MEA_UNIT_0,
                                      &entry,
                                      pId_io)!=MEA_OK)
	{
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error: can't create PacketGen \n");
        return MEA_ERROR;
    }
	return MEA_OK;
}

static MEA_Status adap_FsMiHwCreateLbmCfm(tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams,tEnetHal_MacAddr *DestMacAddr,ADAP_Uint32 u4LbmInterval)
{
	tEnetHal_MacAddr srcmacAddr;
	tAdap_PacketGenHeader PacketGen;
	tRmtSrvInfo Info;
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 vpnIndex=0;
	tAdap_LbmPacketGen *pPacketGen;
	tEnetLbmPacketGen enetpacketGen;
	ADAP_Uint16 genVlan=0;
	MEA_Port_t	enetPort;

	pPacketGen = adap_getLbmPacketGenEntry(pEcfmMepInfoParams->u4IfIndex);
	if(pPacketGen != NULL)
	{
		return MEA_OK;
	}
	genVlan = adap_allocateInternalVlanId();
	enetpacketGen.packetGenProfileId = adap_getPacketGenProfile(30);
	enetpacketGen.internalVlan = genVlan;

	if(enetpacketGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		adap_allocPacketGenProfile(30,enetpacketGen.packetGenProfileId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateLbmCfm: profile:%d allready exist\n",enetpacketGen.packetGenProfileId);

	}
	else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
	{
		if(adap_FsMiHwPacketGenProfInfoCreate(30,&enetpacketGen.packetGenProfileId,MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm: create packet gen profile failed\n");
			return MEA_ERROR;
		}
		adap_allocPacketGenProfile(30,enetpacketGen.packetGenProfileId);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm: not enough room to create new packet gen profile\n");
		return MEA_ERROR;
	}

	// create packet generator
	if(adap_FsMiHwGetInterfaceCfmOamUCMac(pEcfmMepInfoParams->u4IfIndex,&srcmacAddr) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call FsMiHwGetInterfaceCfmOamUCMac\n");
		return MEA_ERROR;
	}

	adap_mac_to_arr(DestMacAddr, PacketGen.DA.b);
	adap_mac_to_arr(&srcmacAddr, PacketGen.SA.b);
	PacketGen.num_of_vlans=1;
	PacketGen.outerVlan=0x81000000 | genVlan | ( (0x07 & 0x7) << 13);
	PacketGen.innerVlan=0x81000000;
	PacketGen.cfmEtherType=0x8902;
	PacketGen.meg_level=pEcfmMepInfoParams->u1MdLevel; //3 bits
	PacketGen.version=0; //5 bits
	PacketGen.opCode=ADAP_D_LBM_OPCODE_VALUE; // 8 bits
	PacketGen.type=32;
	PacketGen.Length=0;
	PacketGen.Pattern_Type=2;
	PacketGen.tlvOffset=4; // 8 bits
	PacketGen.initSeqNum=0;

	if(adap_FsMiHwPacketGenLbmCreateiNFO(&PacketGen,enetpacketGen.packetGenProfileId,&enetpacketGen.PacketGeId,u4LbmInterval) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call adap_FsMiHwPacketGenLbmCreateiNFO\n");
		return MEA_ERROR;
	}

	adap_AllocPacketSmallGenTable(enetpacketGen.PacketGeId);

	Info.b_lmEnable = MEA_FALSE;
	Info.lmId=0;
	Info.keyType=MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
	Info.me_level=pEcfmMepInfoParams->u1MdLevel; //3 bit
	Info.me_valid=MEA_TRUE;
	Info.src_port=120;
	Info.vlanId=genVlan;
	Info.EnetPolilcingProfile=adap_GetDefaultPolicerId(1);

	if(adap_FsMiHwCreateCfmService(&enetpacketGen.serviceId,&Info) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call FsMiHwCreateCfmService\n");
		return MEA_ERROR;
	}
	vpnIndex = Info.vpn;


	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pEcfmMepInfoParams->u4VlanIdIsid;

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;
	//create action
	if(adap_FsMiHwCreateCfmAction(&enetpacketGen.action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call adap_FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}

	MeaAdapGetPhyPortFromLogPort (pEcfmMepInfoParams->u4IfIndex, &enetPort);
	MEA_SET_OUTPORT(&tEhp_info.output_info, enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);


	if(adap_FsMiHwCreateRxForwarder(enetpacketGen.action_id,
								vpnIndex,
								&tOutPorts,pEcfmMepInfoParams->u1MdLevel,
								ADAP_D_LBM_OPCODE_VALUE,
								genVlan,
								Info.src_port /*DPLIssPort2EnetPort(pEcfmMepInfoParams->u4IfIndex)*/) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call FsMiHwCreateTxForwarder\n");
		return MEA_ERROR;
	}


	if(adap_FsMiHwPacketGenActive(enetpacketGen.PacketGeId,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call FsMiHwPacketGenActive\n");
		return MEA_ERROR;
	}


	// packet analyzer
	if(adap_MiVlanSetServiceMdLevel (pEcfmMepInfoParams->u4VlanIdIsid,
									pEcfmMepInfoParams->u4IfIndex,
									D_TAGGED_SERVICE_TAG,
									MEA_TRUE,
									pEcfmMepInfoParams->u1MdLevel) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call DPL_MiVlanSetServiceMdLevel\n");
		return MEA_ERROR;
	}

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	enetpacketGen.analyzerId = adap_PacketAnalyzerAllocTable();
	if(enetpacketGen.analyzerId == UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to allocate packet analyzer id\n");
		return MEA_ERROR;
	}
	adap_FsMiHwSetGenericAnalyzer(enetpacketGen.analyzerId,0);

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = enetpacketGen.analyzerId;


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info.output_info,120);
	MEA_SET_OUTPORT(&tOutPorts, 120);
	MEA_SET_OUTPORT(&tEhp_info.output_info,127);
	MEA_SET_OUTPORT(&tOutPorts, 127);


	//create action
	if(adap_FsMiHwCreateCfmAction(&enetpacketGen.analyzer_action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}

	vpnIndex = adap_GetVpnByVlan (pEcfmMepInfoParams->u4VlanIdIsid);
	if (vpnIndex == ADAP_END_OF_TABLE)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm FAIL: adap_GetVpnByVlan failed for VlanId:%d\n", pEcfmMepInfoParams->u4VlanIdIsid);
	   return MEA_ERROR;
	}

	MeaAdapGetPhyPortFromLogPort (pEcfmMepInfoParams->u4IfIndex, &enetPort);
	if(adap_FsMiHwCreateRxForwarder(enetpacketGen.analyzer_action_id,
								vpnIndex,
								&tOutPorts,
								pEcfmMepInfoParams->u1MdLevel,
								ADAP_D_LBR_OPCODE_VALUE,
								pEcfmMepInfoParams->u4VlanIdIsid,
								enetPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateLbmCfm failed to call adap_FsMiHwCreateRxForwarder\n");
		return MEA_ERROR;
	}

	adap_allocLbmPacketGenEntry(&enetpacketGen,pEcfmMepInfoParams);


	return MEA_OK;

}

static MEA_Status adap_FsMiLbmDeleteParameters(tAdap_LbmPacketGen *pPacketGen)
{
	MEA_Bool bValid=MEA_FALSE;
	MEA_Action_t 	get_paction_id=MEA_PLAT_GENERATE_NEW_ID;
	MEA_Port_t		enetPort;

	if(pPacketGen->packetGen.serviceId != UNDEFINED_VALUE16)
	{
		if (ENET_ADAPTOR_Delete_Service(MEA_UNIT_0, pPacketGen->packetGen.serviceId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters ERROR: Unable to delete the service %d\n", pPacketGen->packetGen.serviceId);
			return MEA_ERROR;
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",pPacketGen->packetGen.serviceId);
		pPacketGen->packetGen.serviceId = UNDEFINED_VALUE16;
	}
	adap_FreeGenVpnId(pPacketGen->packetGen.VpnIndex);

	if(pPacketGen->packetGen.PacketGeId != UNDEFINED_VALUE16)
	{
		if( MEA_API_Delete_PacketGen_Entry(MEA_UNIT_0,pPacketGen->packetGen.PacketGeId) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters ERROR: Unable to delete the packetGen %d\n", pPacketGen->packetGen.PacketGeId);
			return MEA_ERROR;
		}
		adap_freePacketSmallGenTable(pPacketGen->packetGen.PacketGeId);
		pPacketGen->packetGen.PacketGeId = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiLbmDeleteParameters: profileId=%d\n",pPacketGen->packetGen.packetGenProfileId);
		adap_freePacketGenProfile(pPacketGen->packetGen.packetGenProfileId,&bValid);
		if(bValid == MEA_TRUE)
		{
			if(MEA_API_Delete_PacketGen_Profile_info_Entry(MEA_UNIT_0,pPacketGen->packetGen.packetGenProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters ERROR: Unable to delete the packetGen profile:%d\n", pPacketGen->packetGen.packetGenProfileId);
				return MEA_ERROR;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiLbmDeleteParameters: profileId=%d deleted\n",pPacketGen->packetGen.packetGenProfileId);
			pPacketGen->packetGen.packetGenProfileId = UNDEFINED_VALUE16;
		}
	}
	if(pPacketGen->packetGen.internalVlan != UNDEFINED_VALUE16)
	{
		if(adap_FsMiHwDeleteRxForwarder(pPacketGen->packetGen.internalVlan,
									pPacketGen->packetGen.VpnIndex,
									pPacketGen->tEcfmMepInfoParams.u1MdLevel,
									ADAP_D_LBM_OPCODE_VALUE,
									120,
									&get_paction_id) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters: delete forwarder: entry not found\n");
		}
		adap_freeInternalVlanId(pPacketGen->packetGen.internalVlan);
		pPacketGen->packetGen.internalVlan = UNDEFINED_VALUE16;
	}
	MeaAdapGetPhyPortFromLogPort (pPacketGen->tEcfmMepInfoParams.u4IfIndex, &enetPort);
	if(adap_FsMiHwDeleteRxForwarder(pPacketGen->tEcfmMepInfoParams.u4VlanIdIsid,
								pPacketGen->packetGen.analyzerVpnIndex,
								pPacketGen->tEcfmMepInfoParams.u1MdLevel,
								ADAP_D_LBR_OPCODE_VALUE,
								enetPort,
								&get_paction_id)!= MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters: delete forwarder: entry not found\n");
	}


	if(pPacketGen->packetGen.action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.action_id = UNDEFINED_VALUE16;
	}
	if(pPacketGen->packetGen.analyzer_action_id != UNDEFINED_VALUE16)
	{
		if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pPacketGen->packetGen.analyzer_action_id) != MEA_OK )
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiLbmDeleteParameters ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
			return MEA_ERROR;
		}
		pPacketGen->packetGen.analyzer_action_id = UNDEFINED_VALUE16;
	}
	adap_PacketAnalyzerFreeTable(pPacketGen->packetGen.analyzerId);
	pPacketGen->packetGen.analyzerId = UNDEFINED_VALUE16;
	adap_freeLbmPacketGenEntry(pPacketGen->tEcfmMepInfoParams.u4IfIndex);
	return MEA_OK;

}

MEA_Status adap_FsMiHwCreateCfmServiceWithOutPut(MEA_Service_t *pService_id,tRmtSrvInfo *srcInfo,MEA_OutPorts_Entry_dbt 	*pOutPorts,MEA_EHP_Info_dbt *pInfo)
{
    MEA_Service_Entry_Key_dbt             Service_key;
    MEA_Service_Entry_Data_dbt            Service_data;
    MEA_EgressHeaderProc_Array_Entry_dbt  Service_editing;
	MEA_Policer_Entry_dbt                 Service_policer;

	srcInfo->vpn=adap_AllocateNewGenVpnId();


	adap_memset(&Service_editing , 0 , sizeof(MEA_EgressHeaderProc_Array_Entry_dbt ));
	adap_memset(&Service_key    , 0 , sizeof(MEA_Service_Entry_Key_dbt ));
	adap_memset(&Service_data , 0 , sizeof(MEA_Service_Entry_Data_dbt ));
	adap_memset(&Service_policer    , 0 , sizeof(MEA_Policer_Entry_dbt ));


	Service_data.policer_prof_id = srcInfo->EnetPolilcingProfile;
    if (MEA_API_Get_Policer_ACM_Profile(MEA_UNIT_0,
                                        srcInfo->EnetPolilcingProfile,
                                        0,/* ACM_Mode */
                                        &Service_policer) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateCfmService Error: MEA_API_Get_Policer_ACM_Profile FAIL for default PolilcingProfile:%d failed\n",srcInfo->EnetPolilcingProfile);
        return MEA_ERROR;
    }


	Service_key.src_port         = srcInfo->src_port;
	Service_key.L2_protocol_type =MEA_PARSING_L2_KEY_Ctag; /* Ethertypes: 88a8/8100*/
	Service_key.net_tag          = srcInfo->vlanId; //configure vlan
	Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
	Service_key.evc_enable = MEA_TRUE;
	Service_key.pri              = 0;

	Service_data.DSE_forwarding_enable   = MEA_FALSE; //MEA_TRUE;
	Service_data.DSE_forwarding_key_type = srcInfo->keyType;
	Service_data.DSE_learning_enable     = MEA_FALSE;
	Service_data.DSE_learning_actionId_valid = MEA_FALSE;
	Service_data.DSE_learning_srcPort_force = MEA_FALSE;

	Service_data.CFM_OAM_ME_Level_Threshold_valid = srcInfo->me_valid;
	Service_data.CFM_OAM_ME_Level_Threshold_value = srcInfo->me_level;

	Service_data.vpn = srcInfo->vpn; //vpn

    /* Set the upstream service data attribute for policing */
    Service_data.tmId   = 0; /* generate new id */
    Service_data.tmId_disable = MEA_SRV_RATE_MEETRING_ENABLE_DEF_VAL; /* No upstream policing */

    /* Set the upstream service data attribute for pm (counters) */
    Service_data.pmId   = 0; /* generate new id */

    /* Set the upstream service data attribute for policing */
    Service_data.editId = 0; /* generate new id */

    /* Set the other upstream service data attributes to defaults */
    Service_data.ADM_ENA            = MEA_FALSE;
    Service_data.CM                 = MEA_FALSE;
    Service_data.L2_PRI_FORCE       = MEA_FALSE;
    Service_data.L2_PRI             = 0;
    Service_data.COLOR_FORSE        = MEA_TRUE;
    Service_data.COLOR              = 2;
    Service_data.COS_FORCE          = MEA_FALSE;
    Service_data.COS                = 0;
    Service_data.protocol_llc_force = MEA_TRUE;
    Service_data.Protocol           = 1; /* 0-Trans/EoA,1-VLAN/IPoA,2-MPLS/PPPoEoA,3-MART/PPPoA */
    Service_data.Llc                = MEA_FALSE;


	(*pService_id) = MEA_PLAT_GENERATE_NEW_ID;

	Service_editing.num_of_entries = 1;
	Service_editing.ehp_info = pInfo;

    if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
                               &Service_key,
                               &Service_data,
                               pOutPorts,
                               &Service_policer,
                               &Service_editing,
                               pService_id) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiHwCreateCfmService FAIL: Unable to create the new service %d\n", (*pService_id));
        /*T.B.D. Rollback some of the previous actions if fail! */
        return MEA_ERROR;

    }

	return (MEA_OK);

}


static MEA_Status adap_FsMiHwCreateTstCfm(tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams,tEnetHal_MacAddr *DestMacAddr,ADAP_Uint32 u4LbmInterval)
{
	tEnetHal_MacAddr srcmacAddr;
	tAdap_PacketGenHeader PacketGen;
	tRmtSrvInfo Info;
	MEA_EHP_Info_dbt tEhp_info;
	MEA_OutPorts_Entry_dbt tOutPorts;
	ADAP_Uint16 vpnIndex=0;
	tAdap_LbmPacketGen *pPacketGen;
	tEnetLbmPacketGen enetpacketGen;
	ADAP_Uint16 genVlan=0;
	MEA_Port_t enetPort;

	pPacketGen = adap_getLbmPacketGenEntry(pEcfmMepInfoParams->u4IfIndex);
	if(pPacketGen != NULL)
	{
		return MEA_OK;
	}
	genVlan = adap_allocateInternalVlanId();
	enetpacketGen.packetGenProfileId = adap_getPacketGenProfile(30);
	enetpacketGen.internalVlan = genVlan;

	if(enetpacketGen.packetGenProfileId != UNDEFINED_VALUE16)
	{
		adap_allocPacketGenProfile(30,enetpacketGen.packetGenProfileId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"adap_FsMiHwCreateTstCfm: profile:%d allready exist\n",enetpacketGen.packetGenProfileId);

	}
	else if(adap_isPacketGenProfileTableFull() == MEA_FALSE)
	{
		if(adap_FsMiHwPacketGenProfInfoCreate(30,&enetpacketGen.packetGenProfileId,MEA_PACKETGEN_STREAM_TYPE_LBM_LBR) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm: create packet gen profile failed\n");
			return MEA_ERROR;
		}
		adap_allocPacketGenProfile(30,enetpacketGen.packetGenProfileId);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm: not enough room to create new packet gen profile\n");
		return MEA_ERROR;
	}

	// create packet generator
	if(adap_FsMiHwGetInterfaceCfmOamUCMac (pEcfmMepInfoParams->u4IfIndex,&srcmacAddr) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call adap_FsMiHwGetInterfaceCfmOamUCMac\n");
		return MEA_ERROR;
	}

	adap_mac_to_arr(DestMacAddr, PacketGen.DA.b);
	if(adap_is_mac_zero(DestMacAddr) == ADAP_TRUE)
	{
		// use multicast
		PacketGen.DA.b[0] = 0x01;
		PacketGen.DA.b[1] = 0x80;
		PacketGen.DA.b[2] = 0xc2;
		PacketGen.DA.b[3] = 0x00;
		PacketGen.DA.b[4] = 0x00;
		PacketGen.DA.b[5] = 0x30+pEcfmMepInfoParams->u1MdLevel;

	}
	adap_mac_to_arr(&srcmacAddr, PacketGen.SA.b);
	PacketGen.num_of_vlans=1;
	PacketGen.outerVlan=0x81000000 | genVlan | ( (0x07 & 0x7) << 13);
	PacketGen.innerVlan=0x81000000;
	PacketGen.cfmEtherType=0x8902;
	PacketGen.meg_level=pEcfmMepInfoParams->u1MdLevel; //3 bits
	PacketGen.version=0; //5 bits
	PacketGen.opCode=ADAP_D_TST_OPCODE_VALUE; // 8 bits
	PacketGen.type=32;
	PacketGen.Length=0;
	PacketGen.Pattern_Type=6;
	PacketGen.tlvOffset=4; // 8 bits
	PacketGen.initSeqNum=0;


	if(adap_FsMiHwPacketGenLbmCreateiNFO(&PacketGen,enetpacketGen.packetGenProfileId,&enetpacketGen.PacketGeId,u4LbmInterval) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call FsMiHwPacketGenLbmCreateiNFO\n");
		return MEA_ERROR;
	}

	adap_AllocPacketSmallGenTable(enetpacketGen.PacketGeId);

	Info.b_lmEnable = MEA_FALSE;
	Info.lmId=0;
	Info.keyType=MEA_DSE_FORWARDING_KEY_TYPE_DA_PLUS_VPN; //MEA_DSE_FORWARDING_KEY_TYPE_OAM_VPN;
	Info.me_level=pEcfmMepInfoParams->u1MdLevel; //3 bit
	Info.me_valid=MEA_FALSE; //MEA_TRUE;
	Info.src_port=120;
	Info.vlanId=genVlan;
	Info.EnetPolilcingProfile=adap_GetDefaultPolicerId(1);


	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = pEcfmMepInfoParams->u4VlanIdIsid;

	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MeaAdapGetPhyPortFromLogPort (pEcfmMepInfoParams->u4IfIndex, &enetPort);
	MEA_SET_OUTPORT(&tEhp_info.output_info,enetPort);
	MEA_SET_OUTPORT(&tOutPorts, enetPort);

	if(adap_FsMiHwCreateCfmServiceWithOutPut(&enetpacketGen.serviceId,
										&Info,&tOutPorts,&tEhp_info) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call adap_FsMiHwCreateCfmServiceWithOutPut\n");
		return MEA_ERROR;
	}

	if(adap_FsMiHwPacketGenActive(enetpacketGen.PacketGeId,MEA_TRUE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call FsMiHwPacketGenActive\n");
		return MEA_ERROR;
	}


	// packet analyzer
	if(adap_MiVlanSetServiceMdLevel (pEcfmMepInfoParams->u4VlanIdIsid,
									pEcfmMepInfoParams->u4IfIndex,
									D_TAGGED_SERVICE_TAG,
									MEA_TRUE,
									pEcfmMepInfoParams->u1MdLevel) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call DPL_MiVlanSetServiceMdLevel\n");
		return MEA_ERROR;
	}

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));

	enetpacketGen.analyzerId = adap_PacketAnalyzerAllocTable();
	if(enetpacketGen.analyzerId == UNDEFINED_VALUE16)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to allocate packet analyzer id\n");
		return MEA_ERROR;
	}
	adap_FsMiHwSetGenericAnalyzer(enetpacketGen.analyzerId,0);

	tEhp_info.ehp_data.eth_info.command           = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
	tEhp_info.ehp_data.eth_info.val.vlan.eth_type = 0x8100;
	tEhp_info.ehp_data.eth_info.val.vlan.vid      = enetpacketGen.analyzerId;


	tEhp_info.ehp_data.eth_info.stamp_color       = MEA_TRUE;
	tEhp_info.ehp_data.eth_info.stamp_priority    = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_valid = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit0_loc   = 12;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
	tEhp_info.ehp_data.eth_stamping_info.color_bit1_loc   = 0;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit0_loc     = 13;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit1_loc     = 14;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_TRUE;
	tEhp_info.ehp_data.eth_stamping_info.pri_bit2_loc     = 15;

	MEA_SET_OUTPORT(&tEhp_info.output_info,120);
	MEA_SET_OUTPORT(&tOutPorts, 120);
	MEA_SET_OUTPORT(&tEhp_info.output_info,127);
	MEA_SET_OUTPORT(&tOutPorts, 127);


	//create action
	if(adap_FsMiHwCreateCfmAction(&enetpacketGen.analyzer_action_id,&tEhp_info,1,&tOutPorts,MEA_INGGRESS_TS_TYPE_NONE) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call adap_FsMiHwCreateCfmAction\n");
		return MEA_ERROR;
	}

	vpnIndex = adap_GetVpnByVlan (pEcfmMepInfoParams->u4VlanIdIsid);
	if (vpnIndex == ADAP_END_OF_TABLE)
	{
	   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm FAIL: adap_GetVpnByVlan failed for VlanId:%d\n", pEcfmMepInfoParams->u4VlanIdIsid);
	   return MEA_ERROR;
	}

	if(adap_FsMiHwCreateRxForwarder(enetpacketGen.analyzer_action_id,
								vpnIndex,
								&tOutPorts,
								pEcfmMepInfoParams->u1MdLevel,
								ADAP_D_TST_OPCODE_VALUE,
								pEcfmMepInfoParams->u4VlanIdIsid,
								enetPort) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_FsMiHwCreateTstCfm failed to call adap_FsMiHwCreateRxForwarder\n");
		return MEA_ERROR;
	}

	adap_allocLbmPacketGenEntry(&enetpacketGen,pEcfmMepInfoParams);

	return MEA_OK;

}

static MEA_Status adap_FsMiHwGlobalsCfmOamValid(int valid)
{
    MEA_Globals_Entry_dbt entry;

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_OK;
    }
        entry.CFM_OAM.valid = valid;
    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_OK;
    }
    return MEA_OK;
}

static MEA_Status adap_FsMiHwSetGlobalsPacketGen(int generator_enable)
{
	MEA_Globals_Entry_dbt entry;

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    entry.if_global0.val.generator_enable = generator_enable;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
       ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_ERROR;
    }

    return MEA_OK;

}

MEA_Status adap_FsMiHwSetGlobalsPacketAnalyzerEnable(int enable)
{
	MEA_Globals_Entry_dbt entry;

    if (MEA_API_Get_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Get_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    entry.PacketAnalyzer.val.enable = enable;

    if (MEA_API_Set_Globals_Entry (MEA_UNIT_0,&entry) != MEA_OK)
	{
    	fprintf(stdout,"MEA_API_Set_Globals_Entry failed\n");
       return MEA_ERROR;
    }
    return MEA_OK;

}

void wait_for_end_cfm_threads(void)
{
	pthread_join( adap_thread_EcfmHandler, NULL);
	pthread_join( adap_idTimer_EcfmHandler, NULL);
}




/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmHwInit
 *
 * Description        : This function takes care of initializing the
 *                      hardware related parameters.
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmHwInit (ADAP_Uint32 u4ContextId)
{
    ADAP_Uint32	issPortList=0;
	MEA_LxCp_t	LxCp_Id=0;

	adap_initEcfmDatabase();

	//set by default the CFM to CPU

	adap_FsMiHwSetGlobalsPacketGen(1);
	adap_FsMiHwSetGlobalsPacketAnalyzerEnable(1);
	adap_FsMiHwGlobalsCfmOamValid(1);
	MeaAdapSetIngressPort(ADAP_D_ANALYZER_ETHERNITY_PORT,ADAP_TRUE,ADAP_FALSE);
	MeaAdapSetEgressPort (ADAP_D_ANALYZER_ETHERNITY_PORT,ADAP_TRUE,ADAP_TRUE);

	for(issPortList=1; issPortList<MeaAdapGetNumOfPhyPorts(); issPortList++)
	{
		if(getLxcpId(issPortList,&LxCp_Id) != ENET_SUCCESS)
		{
			continue;
		}
		// move all port to non offload
		adap_FsMiLacpLxcpOamPduUpdate (ADAP_TRUE,LxCp_Id);
	}

	adap_FsMiInitEcfmHandler();

    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmHwDeInit
 *
 * Description        : This function takes care of de-initializing the
 *                       hardware related parameters.
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmHwDeInit (ADAP_Uint32 u4ContextId)
{
	ADAP_Uint32 idx=0;
	tAdap_InterfaceRemoteSide *pRemoteSide=NULL;

	adap_signal_condMutexLock_ccm();
	for(idx=0;idx<ADAP_D_MAX_DATABASE_CCM_CONFIGURATION;idx++)
	{
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.packetGenParams_active == ADAP_TRUE)
		{
			adap_FsDeleteHwPacketGenerator(&tAdap_EcfmCcmDb[idx],0);
		}
		if(tAdap_EcfmCcmDb[idx].EcfmDbParams.packetAnaParams_active == ADAP_TRUE)
		{
			adap_FsDeletePacketAnalyzer(&tAdap_EcfmCcmDb[idx],0);
		}
	}
	// delete lbr/dmr if needed
	for(idx=0;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		pRemoteSide = adap_getIntRemoteSide(idx);
		if(pRemoteSide->tDmrDatabase.bConfigured == ADAP_TRUE)
		{
			adap_delete_rmtSide_Configuration(&pRemoteSide->tDmrDatabase);
		}
		if(pRemoteSide->tLbrDatabase.bConfigured == ADAP_TRUE)
		{
			adap_delete_rmtSide_Configuration(&pRemoteSide->tLbrDatabase);
		}
		pRemoteSide->tDmrDatabase.bConfigured = ADAP_FALSE;
		pRemoteSide->tLbrDatabase.bConfigured = ADAP_FALSE;
	}
	adap_signal_condMutexUnLock_ccm();
    return ENET_SUCCESS;

}


/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmTransmitDmm
 *
 * Description        : This function is used to trigger the HW to transmit
 *                      a DMM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1DmmPdu   - Pointer to the DMM-PDU.
 *                      u2PduLength - Length of the DMM-PDU
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmTransmitDmm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
										ADAP_Uint8 *pu1DmmPdu, ADAP_Uint16 u2PduLength,
										tEnetHal_VlanTag VlanTag, ADAP_Uint8 u1Direction)
{
	tAdap_EcfmMepDmmInfoParams Info;

	ADAP_Uint32 idx;
	ADAP_Uint32 len=0;
	char buff[100];

	for(idx=0;idx<u2PduLength;idx++)
	{
		if( (idx%16) == 0)
		{
			if(len)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
			}
			len=0;
		}
		len += sprintf(&buff[len],"%2.2x-",pu1DmmPdu[idx]);
	}
	if(len)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
	}

	Info.VlanTag.InnerVlanTag = VlanTag.InnerVlanTag;
	Info.VlanTag.OuterVlanTag = VlanTag.OuterVlanTag;
	Info.u4IfIndex = u4IfIndex;
	Info.u1Direction = u1Direction;

	if(u2PduLength > 5)
	{
		adap_mac_from_arr(&Info.destMac, pu1DmmPdu);
	}
	if(u2PduLength > 11)
	{
		adap_mac_from_arr(&Info.srcMac, pu1DmmPdu + ETH_ALEN);
	}
	if(u2PduLength > 17)
	{
		Info.meLevel = ( (pu1DmmPdu[14] & 0xE0) >> 5);
		Info.version = (pu1DmmPdu[14] & 0x1F);
		Info.opCode = pu1DmmPdu[15];
		Info.flags = pu1DmmPdu[16];
		Info.tlvOffset = pu1DmmPdu[17];
	}
	adap_FsMiHwCreateDmmCfm(&Info);
    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmTransmitLmm
 *
 * Description        : This function is used to trigger the HW to transmit
 *                      a LMM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1LmmPdu   - Pointer to the LMM-PDU.
 *                      u2PduLength - Length of the LMM-PDU
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmTransmitLmm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 *pu1LmmPdu,
										ADAP_Uint16 u2PduLength, tEnetHal_VlanTagInfo VlanTag, ADAP_Uint8 u1Direction)
{
	tAdap_EcfmMepLmmInfoParams Info;

    ADAP_Uint32 idx;
    ADAP_Uint32 len=0;
	char buff[100];

	for(idx=0;idx<u2PduLength;idx++)
	{
		if( (idx%16) == 0)
		{
			if(len)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
			}
			len=0;
		}
		len += sprintf(&buff[len],"%2.2x-",pu1LmmPdu[idx]);
	}
	if(len)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
	}

	adap_memcpy(&Info.VlanTag,&VlanTag,sizeof(tAdap_EcfmMepLmmInfoParams));
	Info.u1Direction = u1Direction;
	Info.u4IfIndex = u4IfIndex;

	if(u2PduLength > 5)
	{
		adap_mac_from_arr(&Info.destMac, pu1LmmPdu);
	}
	if(u2PduLength > 11)
	{
		adap_mac_from_arr(&Info.srcMac, pu1LmmPdu + ETH_ALEN);
	}
	if(u2PduLength > 17)
	{
		Info.meLevel = ( (pu1LmmPdu[14] & 0xE0) >> 5);
		Info.version = (pu1LmmPdu[14] & 0x1F);
		Info.opCode = pu1LmmPdu[15];
		Info.flags = pu1LmmPdu[16];
		Info.tlvOffset = pu1LmmPdu[17];
	}
	adap_FsMiHwCreateLmmCfm(&Info);

    return ENET_SUCCESS;
}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmTransmit1Dm
 *
 * Description        : This function is used to trigger the HW to transmit
 *                      a 1DM PDU.
 * Input(s)           : u4ContextId - Context Idenifier
 *                      u4IfIndex   - Interface Index
 *                      pu1DmPdu    - Pointer to the DM-PDU.
 *                      u2PduLength - Length of the DM-PDU.
 *                      VlanTag     - Information to be filled in Vlan-Tag
 *                      u1Direction - Direction of the transmitting MEP
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmTransmit1Dm (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,
										ADAP_Uint8 *pu1DmPdu, ADAP_Uint16 u2PduLength,
										tEnetHal_VlanTag VlanTag, ADAP_Uint8 u1Direction)
{
	tAdap_EcfmMepDmmInfoParams Info;

	ADAP_Uint32 idx;
	ADAP_Uint32 len=0;
	char buff[100];

	adap_assert(u2PduLength >= 17);

	for(idx=0;idx<u2PduLength;idx++)
	{
		if( (idx%16) == 0)
		{
			if(len)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
			}
			len=0;
		}
		len += sprintf(&buff[len],"%2.2x-",pu1DmPdu[idx]);
	}
	if(len)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"%s\n",buff);
	}

	Info.VlanTag.InnerVlanTag = VlanTag.InnerVlanTag;
	Info.VlanTag.OuterVlanTag = VlanTag.OuterVlanTag;
	Info.u4IfIndex = u4IfIndex;
	Info.u1Direction = u1Direction;

	adap_mac_from_arr(&Info.destMac, pu1DmPdu);
	adap_mac_from_arr(&Info.srcMac, pu1DmPdu + ETH_ALEN);
	Info.meLevel = ( (pu1DmPdu[14] & 0xE0) >> 5);
	Info.version = (pu1DmPdu[14] & 0x1F);
	Info.opCode = pu1DmPdu[15];
	Info.flags = pu1DmPdu[16];
	Info.tlvOffset = pu1DmPdu[17];

	adap_FsMiHwCreate1DmCfm(&Info);
    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmStartLbmTransaction
 *
 * Description        : This function is used to trigger the HW to start
 *                      Lbm Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo
 *                      pEcfmConfigLbmInfo - pointer to Lbm Info
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmStartLbmTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams,
												tEnetHal_EcfmConfigLbmInfo * pEcfmConfigLbmInfo)
{
	if(pEcfmMepInfoParams != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u1Direction:%d\n",pEcfmMepInfoParams->u1Direction);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u1MdLevel:%d\n",pEcfmMepInfoParams->u1MdLevel);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u1Version:%d\n",pEcfmMepInfoParams->u1Version);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u2MepId:%d\n",pEcfmMepInfoParams->u2MepId);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u4IfIndex:%d\n",pEcfmMepInfoParams->u4IfIndex);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmMepInfoParams->u4VlanIdIsid:%d\n",pEcfmMepInfoParams->u4VlanIdIsid);
	}
	if(pEcfmConfigLbmInfo != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"DestMacAddr:%s\n",
												adap_mac_to_string(&pEcfmConfigLbmInfo->DestMacAddr));

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->b1DropEnable:%d\n",pEcfmConfigLbmInfo->b1DropEnable);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->b1VariableByte:%d\n",pEcfmConfigLbmInfo->b1VariableByte);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->u1LbmMode:%d\n",pEcfmConfigLbmInfo->u1LbmMode);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->u1Status:%d\n",pEcfmConfigLbmInfo->u1Status);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->u1TstTlvPatterType:%d\n",pEcfmConfigLbmInfo->u1TstTlvPatterType);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"pEcfmConfigLbmInfo->u4LbmInterval:%d\n",pEcfmConfigLbmInfo->u4LbmInterval);
	}

	if( (pEcfmMepInfoParams != NULL) && (pEcfmConfigLbmInfo != NULL) )
	{
		if(adap_FsMiHwCreateLbmCfm(pEcfmMepInfoParams,&pEcfmConfigLbmInfo->DestMacAddr,pEcfmConfigLbmInfo->u4LbmInterval) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiEcfmStartLbmTransaction failed to call adap_FsMiHwCreateLbmCfm\n");
			return ENET_FAILURE;
		}
	}
    return ENET_SUCCESS;
}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmStopLbmTransaction
 *
 * Description        : This function is used to trigger the HW to stop
 *                      Lbm Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmStopLbmTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams)
{
	tAdap_LbmPacketGen *pPacketGen;

	pPacketGen = adap_getLbmPacketGenEntry(pEcfmMepInfoParams->u4IfIndex);
	if(pPacketGen != NULL)
	{
		if(adap_FsMiLbmDeleteParameters(pPacketGen) != MEA_OK)
		{
			return ENET_FAILURE;
		}
	}

    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmStartTstTransaction
 *
 * Description        : This function is used to trigger the HW to start
 *                      Tst Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo
 *                      pEcfmConfigTstInfo - pointer to Tst Info
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmStartTstTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams,
												tEnetHal_EcfmConfigTstInfo * pEcfmConfigTstInfo)
{
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DestMacAddr %s\r\n",
			adap_mac_to_string(&pEcfmConfigTstInfo->DestMacAddr));
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2TstTlvPatterSize %d\r\n",pEcfmConfigTstInfo->u2TstTlvPatterSize); /* Size of Test Pattern */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4Deadline %d\r\n",pEcfmConfigTstInfo->u4Deadline); /* Duration fof TST transmission */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4TxTstMessages %d\r\n",pEcfmConfigTstInfo->u4TxTstMessages); /* No of TST Messages to be transmitted */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4TstInterval %d\r\n",pEcfmConfigTstInfo->u4TstInterval); /* Interval between successive TST frames */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1Status %d\r\n",pEcfmConfigTstInfo->u1Status); /* Status of TST transmission */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1TstTlvPatterType %d\r\n",pEcfmConfigTstInfo->u1TstTlvPatterType); /* Type of Test Pattern */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"b1VariableByte %d\r\n",pEcfmConfigTstInfo->b1VariableByte); /* Variable byte in TST PDU */
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"b1DropEnable %d\r\n",pEcfmConfigTstInfo->b1DropEnable); /* Drop eligibility of TST frames */

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4IfIndex %d\r\n",pEcfmMepInfoParams->u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4VlanIdIsid %d\r\n",pEcfmMepInfoParams->u4VlanIdIsid);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4MdIndex %d\r\n",pEcfmMepInfoParams->u4MdIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u4MaIndex %d\r\n",pEcfmMepInfoParams->u4MaIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2MepId %d\r\n",pEcfmMepInfoParams-> u2MepId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1MdLevel %d\r\n",pEcfmMepInfoParams->u1MdLevel);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1Direction %d\r\n",pEcfmMepInfoParams->u1Direction);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u1Version %d\r\n",pEcfmMepInfoParams->u1Version);


	if(adap_FsMiHwCreateTstCfm(pEcfmMepInfoParams,&pEcfmConfigTstInfo->DestMacAddr,pEcfmConfigTstInfo->u4TstInterval) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiEcfmStartTstTransaction failed to call adap_FsMiHwCreateTstCfm\n");
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmStopTstTransaction
 *
 * Description        : This function is used to trigger the HW to stop
 *                      Tst Transmission.
 * Input(s)           : pEcfmMepInfoParams - pointer to MepInfo
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmStopTstTransaction (tEnetHal_EcfmMepInfoParams * pEcfmMepInfoParams)
{
	tAdap_LbmPacketGen *pPacketGen;

	pPacketGen = adap_getLbmPacketGenEntry(pEcfmMepInfoParams->u4IfIndex);
	if(pPacketGen != NULL)
	{
		if(adap_FsMiLbmDeleteParameters(pPacketGen) != MEA_OK)
		{
			return ENET_FAILURE;
		}
	}
    return ENET_SUCCESS;

}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmHwGetCapability
 *
 * Description        : This function is used to get the hardware
 *                      capabilities
 * Input(s)           : UINT4 ContextId

 *
 * Output(s)          : pu4HwCapability - HW capabilities

 *                      BIT 0 - LBM transmission and LBR reception
 *                      capability
 *                      BIT 1 - DMM and DMR transmission, reception
 *                      capability
 *                      BIT 2 - 1DM tranmission and reception
 *                      capability
 *                      BIT 3 - LMM and LMR transmission,
 *                      reception capability
 *                      BIT 4 - TST transmission, reception
 *                      capability
 *                      BIT 5 - CCM Offloading capability
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ************************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmHwGetCapability (ADAP_Uint32 u4ContextId, ADAP_Uint32 *pu4HwCapability)
{
	*pu4HwCapability=0;
    *pu4HwCapability |= ADAP_D_ENET_LBM_LBR_OFFLOADING;
	*pu4HwCapability |= ADAP_D_ENET_DMM_DMR_OFFLOADING;
	*pu4HwCapability |= ADAP_D_ENET_1DM_OFFLOADING;
	*pu4HwCapability |= ADAP_D_ENET_LMM_LMR_OFFLOADING;
	*pu4HwCapability |= ADAP_D_ENET_TST_OFFLOADING;
	*pu4HwCapability |= ADAP_D_ENET_CCM_OFFLOADING;

    return ENET_SUCCESS;
}

/***********************************************************************
 * Function Name      : EnetHal_FsMiEcfmHwRegisterCb
 *
 * Description        : This function is used to register a callback
 * Input(s)           : ecfmEventHandlerCb - Callback to register
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : ENET_SUCCESS/ENET_FAILURE
 ***********************************************************************/
ADAP_Int32 EnetHal_FsMiEcfmHwRegisterCb(pEcfmCcmOffHandleRxCallBack ecfmEventHandlerCb)
{
	if (!ecfmEventHandlerCb)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR, "ERR: %s == NULL", ecfmEventHandlerCb);
		return ENET_FAILURE;
	}

	//Updating the CB
	adap_EcfmHandlerCb = ecfmEventHandlerCb;

	return ENET_SUCCESS;
}

