/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

/*
 * ovsdb_vtep.c
 *
 *  Created on: Apr 3, 2017
 *      Author: root
 */


#include <stdlib.h>


#include "adap_types.h"
#include "mea_api.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_enetConvert.h"
#include "adap_service_config.h"
#include "adap_acl.h"
#include "adap_linklist.h"
#include "adap_vlanminp.h"
#include "EnetHal_L2_Api.h"

ADAP_Uint32 create_vxlan_default_service(void)
{
	tServiceAdapDb ServiceAdapDb;
	MEA_Def_SID_dbt DefSIDentry;
	tUpdateServiceParam tParams;

	// configure default service from port 127 only
	adap_memset(&ServiceAdapDb,0,sizeof(ServiceAdapDb));


	ServiceAdapDb.Service_id = MEA_PLAT_GENERATE_NEW_ID;
	ServiceAdapDb.forwadingKeyType=MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_IP_SOURCE_PLUS_VPN;
	ServiceAdapDb.inPort=127;
	ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_DEF_SID;
	ServiceAdapDb.vxlan_enable =ADAP_FALSE;
	ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
	ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);
	ServiceAdapDb.editing = ADAP_EHP_TRANSPARENT_ONE_TAG;
	ServiceAdapDb.outervlanId=0;
	ServiceAdapDb.editOutervlanId=0;
	ServiceAdapDb.editInnervlanId=0;
	ServiceAdapDb.num_of_output_ports=0;
	//ServiceAdapDb.outports[0] = 0;

	if(MeaDrvCreatePointToPointServices (&ServiceAdapDb) != MEA_OK)
	{
		printf("failed to create default servicer\n");
		return ENET_FAILURE;
	}

	adap_memset(&tParams,0,sizeof(tParams));
	tParams.Service = ServiceAdapDb.Service_id;
	tParams.type = E_UPDATE_VPLS_INSTANCE;
	tParams.x.vplsInst.vpls_Ins_Id=0;
	tParams.x.vplsInst.en_vpls_Ins=ADAP_TRUE;
//	if(MeaDrvUpdateService (&tParams) != MEA_OK)
//	{
//		printf("failed to update vpls instance\n");
//		return ENET_FAILURE;
//	}

	adap_memset(&DefSIDentry,0,sizeof(DefSIDentry));
    DefSIDentry.def_sid = ServiceAdapDb.Service_id;
    DefSIDentry.action  = 2;
    DefSIDentry.valid   = MEA_TRUE;

    if(ENET_ADAPTOR_Set_IngressPort_Default_L2Type_Sid(MEA_UNIT_0,127,1, &DefSIDentry )!= MEA_OK)
	{
    	printf("failed to set ingress portr\n");
		return ENET_FAILURE;
	}

    if(ENET_ADAPTOR_Set_IngressPort_Default_L2Type_Sid(MEA_UNIT_0,24,1, &DefSIDentry )!= MEA_OK)
	{
    	printf("failed to set ingress portr\n");
		return ENET_FAILURE;
	}

    return ENET_SUCCESS;
}

ADAP_Uint32 create_service_vlan_dest_ip(tAdapVxlanSnoop *pVxLanSnoop)
{
	tServiceAdapDb ServiceAdapDb;
	tUpdateServiceParam serviceParams;
	tBridgeDomain *pBridgeDomain=NULL;

	adap_memset(&ServiceAdapDb,0,sizeof(ServiceAdapDb));
	adap_memset(&serviceParams,0,sizeof(tUpdateServiceParam));

	ServiceAdapDb.Service_id = MEA_PLAT_GENERATE_NEW_ID;
	ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
	ServiceAdapDb.inPort=ADAP_CPU_PORT_ID;
	ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Ctag;
	ServiceAdapDb.vxlan_enable =ADAP_FALSE;
	//ServiceAdapDb.vxlan_vni = (ADAP_Uint32)((pVxLanSnoop->vni) & 0xFFFFFFFF); // get vni from database
	ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
	ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);
	ServiceAdapDb.editing = ADAP_EHP_APPEND_VXLAN; //ADAP_EHP_EXTRACT_VXLAN_AND_APPEND_VLAN_TAG;
	ServiceAdapDb.outervlanId=pVxLanSnoop->vlan;
	ServiceAdapDb.editInnervlanId=0;
	ServiceAdapDb.num_of_output_ports=1;
	ServiceAdapDb.outports[0] = 24;   //um-match on fwd
	ServiceAdapDb.ip_plus_vlan=MEA_TRUE;
	ServiceAdapDb.da_ip_addr=pVxLanSnoop->src_ipv4_int;

	adap_memcpy(&ServiceAdapDb.editVxLan.destMac,&pVxLanSnoop->sa_mac_ext,sizeof(tEnetHal_MacAddr));
	ServiceAdapDb.editVxLan.destIp = pVxLanSnoop->src_ipv4_ext;
	ServiceAdapDb.editVxLan.tunnel_id = pVxLanSnoop->vni;
	ServiceAdapDb.editVxLan.dest_port = 4789;
	ServiceAdapDb.editVxLan.source_port = 50000;
	ServiceAdapDb.editVxLan.ttl=64;
	ServiceAdapDb.editVxLan.sourceIp=0;

	if(MeaDrvCreatePointToPointServices (&ServiceAdapDb) != MEA_OK)
	{
		printf("failed to create vxlan configuration\r\n");
		return ENET_FAILURE;
	}

	pBridgeDomain = Adap_getBridgeDomain(1,0);
	if(pBridgeDomain == NULL)
	{
		printf("failed failed to get bridge\r\n");
		// delete service in case bridge not exist
		return ENET_FAILURE;
	}



	// set forwarding based on vpn+mac
	serviceParams.type=E_UPDATE_VPN_PARAMETERS;
	serviceParams.Service=ServiceAdapDb.Service_id;
	serviceParams.x.vpnParam.vpn=pBridgeDomain->vpn;
	serviceParams.x.vpnParam.DSE_forwarding_enable=MEA_TRUE;
	serviceParams.x.vpnParam.forwadingKeyType=MEA_SE_FORWARDING_KEY_TYPE_DA_PLUS_VPN;
	if(MeaDrvUpdateService (&serviceParams) != MEA_OK)
	{
		printf("failed to set service:%d\r\n",ServiceAdapDb.Service_id);
		// delete service in case bridge not exist
		return ENET_FAILURE;
	}
	pVxLanSnoop->isEntrySetToHw=ADAP_TRUE;
	pVxLanSnoop->Service=ServiceAdapDb.Service_id;

	return ENET_SUCCESS;
}

ADAP_Uint32 create_vxlan_flooding_entry(tVxLanAdap *pVxLan)
{
	MEA_VPLS_UE_Pdn_dl_dbt  pdn_entry;
	MEA_Service_Entry_Data_dbt	serviceData;
	MEA_Policer_Entry_dbt		tPolicer;
	MEA_EgressHeaderProc_Array_Entry_dbt  	tEHP_Entry;
	MEA_EHP_Info_dbt 						ehp_entry;
	MEA_AcmMode_t               acm_mode=0;
	tVxLanFlooding 				tVxlanDb;
	tVxLanFlooding 				*pCrtVxlanDb;
	MEA_VPLS_dbt				vplsEntry;
	MEA_Bool 					VPLSexist=MEA_FALSE;
	MEA_Uint16 					vpls_Ins_Id=0;
	MEA_OutPorts_Entry_dbt		outputport;


	tVxlanDb.destIp=pVxLan->destIp;
	tVxlanDb.tunnel_id = pVxLan->tunnel_id;
	adap_memcpy(&tVxlanDb.destMac,&pVxLan->destMac,sizeof(tEnetHal_MacAddr));
	if( adap_is_vxlan_flooding_exist(&tVxlanDb) != ENET_SUCCESS)
	{

		pCrtVxlanDb = adap_create_vxlan_flooding_exist();
		if(pCrtVxlanDb == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get entry from database\n");
			return ENET_FAILURE;
		}
		// create action for flooding
		adap_memset(&pdn_entry,0,sizeof(MEA_VPLS_UE_Pdn_dl_dbt));
		adap_memset(&serviceData,0,sizeof(MEA_Service_Entry_Data_dbt));
		adap_memset(&tPolicer,0,sizeof(MEA_Policer_Entry_dbt));
		adap_memset(&outputport,0,sizeof(MEA_OutPorts_Entry_dbt));

		adap_memset(&ehp_entry,0,sizeof(MEA_EHP_Info_dbt));
		adap_memset(&tEHP_Entry,0,sizeof(MEA_EgressHeaderProc_Array_Entry_dbt));
		tEHP_Entry.ehp_info = &ehp_entry;

		pdn_entry.Service_Entry_data = &serviceData;
		pdn_entry.Service_Entry_Policer=&tPolicer;
		pdn_entry.EHP_Entry = &tEHP_Entry;
		pdn_entry.en_service_TFT = MEA_TRUE;
		pdn_entry.Access_port_id=1;

		pdn_entry.EHP_Entry->num_of_entries=1;
//		MEA_SET_OUTPORT(&ehp_entry.output_info, 104);
//		MEA_SET_OUTPORT(&ehp_entry.output_info, 105);
//		MEA_SET_OUTPORT(&ehp_entry.output_info, 106);
//		MEA_SET_OUTPORT(&ehp_entry.output_info, 107);

		serviceData.policer_prof_id = MeaAdapGetDefaultPolicer(1);
		serviceData.tmId = MeaAdapGetDefaultTm(1);
		serviceData.pmId=0;

		if (ENET_ADAPTOR_Get_Policer_ACM_Profile(MEA_UNIT_0,
											serviceData.policer_prof_id,
											acm_mode,/* ACM_Mode */
											&tPolicer) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by MEA_API_Get_Policer_ACM_Profiler\n");
			return ENET_FAILURE;
		}

		tEHP_Entry.ehp_info->ehp_data.VxLan_info.valid = MEA_TRUE;
		adap_mac_to_arr(&pVxLan->destMac, tEHP_Entry.ehp_info->ehp_data.VxLan_info.vxlan_Da.b);
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.vxlan_dst_Ip = pVxLan->destIp;
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.vxlan_vni = pVxLan->tunnel_id;
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.l4_dest_Port = pVxLan->dest_port;
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.l4_src_Port = pVxLan->source_port;
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.ttl = pVxLan->ttl;
		tEHP_Entry.ehp_info->ehp_data.VxLan_info.sgw_ipId=0; // according to source port

		printf("vtep mac address:%s\r\n", adap_mac_to_string(&pVxLan->destMac));

		printf("vtep ip address:%d.%d.%d.%d\r\n",
							(pVxLan->destIp & 0xFF000000)>>24,
							(pVxLan->destIp & 0x00FF0000)>>16,
							(pVxLan->destIp & 0x0000FF00)>>8,
							(pVxLan->destIp & 0x000000FF));


		tEHP_Entry.num_of_entries=1;

        // create vplsi for
		ENET_ADAPTOR_VPLSi_isExist(MEA_UNIT_0,vpls_Ins_Id,&VPLSexist);

		if(VPLSexist == MEA_FALSE)
		{
			adap_memset(&vplsEntry,0,sizeof(MEA_VPLS_dbt));
			vplsEntry.vpn=0;
			vplsEntry.max_of_fooding=64;
			vplsEntry.SM_mode=MEA_CHARACTER_TYPE_TEID_MODE_Vlan_To_Vxlan; //MEA_CHARACTER_TYPE_TEID_MODE_SM1;


			if(ENET_ADAPTOR_VPLSi_Create_Entry(MEA_UNIT_0, &vpls_Ins_Id, &vplsEntry) != MEA_OK)
			{
				printf("failed to create vpls entry\r\n");
				return ENET_FAILURE;
			}
			if(create_vxlan_default_service() != ENET_SUCCESS)
			{
				printf("failed to create default service\r\n");
				return ENET_FAILURE;
			}
			//printf("vpls entry created\r\n");
		}
		pdn_entry.en_output=MEA_TRUE;
		pdn_entry.Service_Entry_OutPorts = &outputport;
		MEA_SET_OUTPORT(pdn_entry.Service_Entry_OutPorts, 104);
		MEA_SET_OUTPORT(pdn_entry.Service_Entry_OutPorts, 105);
		MEA_SET_OUTPORT(pdn_entry.Service_Entry_OutPorts, 106);
		MEA_SET_OUTPORT(pdn_entry.Service_Entry_OutPorts, 107);
		if(ENET_ADAPTOR_VPLSi_UE_PDN_DL_Create_Entry(MEA_UNIT_0,vpls_Ins_Id,pCrtVxlanDb->UE_pdn_Id,&pdn_entry) != MEA_OK)
		{
			printf("failed to create vxlan editing\r\n");
			return ENET_FAILURE;
		}
		pCrtVxlanDb->destIp=pVxLan->destIp;
		pCrtVxlanDb->tunnel_id = pVxLan->tunnel_id;
		adap_memcpy(&pCrtVxlanDb->destMac,&pVxLan->destMac,sizeof(tEnetHal_MacAddr));
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 upset_ovsdb_local_vtep(void)
{
	tEnetOvsdbTunnel *localTep;
	ADAP_Uint32 ipAddress=0;
	ADAP_Uint32 hwIpAddress=0;
	tEnetHal_MacAddr hwMacAddr;

	localTep = ovsdb_get_local_mac_entry();
	if(localTep != NULL)
	{
		ipAddress |= ((ADAP_Uint32)(localTep->vtep_ipv_dest[0] & 0xFF) << 24);
		ipAddress |= ((ADAP_Uint32)(localTep->vtep_ipv_dest[1] & 0xFF) << 16);
		ipAddress |= ((ADAP_Uint32)(localTep->vtep_ipv_dest[2] & 0xFF) << 8);
		ipAddress |= ((ADAP_Uint32)(localTep->vtep_ipv_dest[3] & 0xFF));

		if(MeaDrvGetFirstGlobalIpAddress(&hwIpAddress) == MEA_OK)
		{
			if(hwIpAddress == ipAddress)
			{
				//printf("ip address found in the hw\r\n");
			}
			else if(hwIpAddress != 0)
			{
				MeaDrvSetGlobalIpAddress(hwIpAddress,MEA_FALSE);
			}
			MeaDrvSetGlobalIpAddress(ipAddress,MEA_TRUE);
			printf("set local ip address\r\n");
		}

		if(MeaDrvGetFirstGlobalMacAddress(&hwMacAddr) == MEA_OK)
		{
			if(adap_memcmp(&hwMacAddr,&localTep->vtep_mac_dest,sizeof(tEnetHal_MacAddr)) == 0)
			{
				//printf("mac address found in the hw\r\n");
			}
			else if(adap_is_mac_zero(&hwMacAddr) == ADAP_FALSE)
			{
				MeaDrvSetGlobalMacAddress(&localTep->vtep_mac_dest,MEA_FALSE);
			}
			MeaDrvSetGlobalMacAddress(&localTep->vtep_mac_dest,MEA_TRUE);
			printf("set local mac address\r\n");
		}
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 update_ovsdb_vtep_tunnel(void)
{
	tEnetOvsdbTunnel *entry;
	//tEnetVtepGlobal *globalEntry;
//	int i=0;
	int j=0;
	tBridgeDomain *pBridgeDomain=NULL;
	tBridgeDomain localBridgeDomain;
	ADAP_Uint32 ret=0;
	tServiceAdapDb ServiceAdapDb;
	tEnetVtepSerDb *pServiceDb;
	ENET_QueueId_t tClusterId=0;

	tVxLanAdap tVxLan;



	if(adapGetFirstCluster(ADAP_CPU_PORT_ID ,&tClusterId) != ENET_SUCCESS)
	{
		printf("failed to cluster of 127\r\n");
		return ENET_FAILURE;
	}

	// create bridge on vlan 1
	pBridgeDomain = Adap_getBridgeDomain(1,0);
	if(pBridgeDomain == NULL)
	{
		adap_memset(&localBridgeDomain,0,sizeof(tBridgeDomain));
		localBridgeDomain.num_of_ports=0;

		ret = EnetHal_FsMiVlanHwCreateFdbId (0,1);
		if(ret != ENET_SUCCESS)
		{
			printf("failed to create fdb\r\n");
			return ENET_FAILURE;
		}
		ret = EnetHal_FsMiVlanHwAssociateVlanFdb (0,1,1);

		if(ret != ENET_SUCCESS)
		{
			printf("failed to associate vlan\r\n");
			return ENET_FAILURE;
		}

		pBridgeDomain = Adap_getBridgeDomain(1,0);
		if(pBridgeDomain == NULL)
		{
			printf("failed failed to get bridge\r\n");
			return ENET_FAILURE;
		}

		// just for debuging
		pBridgeDomain->vpn=0;
		pBridgeDomain->learn_without_action=ADAP_TRUE;


		for(j=0;j<MeaAdapGetNumOfPhyPorts();j++)
		{
			pBridgeDomain->ifIndex[pBridgeDomain->num_of_ports] = j+1;
			pBridgeDomain->ifType[pBridgeDomain->num_of_ports] = VLAN_UNTAGGED_MEMBER_PORT;
			pBridgeDomain->num_of_ports++;
			EnetHal_FsMiVlanHwSetPortPvid (0,j+1,1);
		}

		ret = adap_ReconfigureHwVlan(1,0);

		if(ret != ENET_SUCCESS)
		{
			printf("failed to create bridge\r\n");
			return ENET_FAILURE;
		}

		MeaAdapSetGwGlobalSetVxlanUdpDest(D_DEFAULT_VXLAN_DEST_UDP_PORT);
		MeaAdapSetGwGlobalSetVxlanVlanRange(4000);

	}

	// check all vxlan remote servers
	entry = ovsdb_getfirst_entry();
	while(entry)
	{
		printf("entry found:%d vni:%d\r\n",entry->num_of_entries,entry->vniId[0]);
		if(entry->num_of_entries > 0)
		{
			pServiceDb = ovsdb_get_entry_by_vni(entry->vniId[0]);
			if(pServiceDb == NULL)
			{
				pServiceDb = ovsdb_alloc_entry_by_vni();
				if(pServiceDb == NULL)
				{
					printf("failed to allocate vxlan service entry\r\n");
					return ENET_FAILURE;
				}
				pServiceDb->refCount++;
				for(j=0;j<MAX_NUM_OF_NIC_PORTS;j++)
				{
					if(pServiceDb->vtepService.valid[j] == ADAP_FALSE)
					{
						// create services
						// create vxlan configuration
						adap_memset(&ServiceAdapDb,0,sizeof(ServiceAdapDb));

						ServiceAdapDb.Service_id = MEA_PLAT_GENERATE_NEW_ID;
						ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
						ServiceAdapDb.inPort=104+j;
						ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Untagged;
						ServiceAdapDb.vxlan_enable =ADAP_TRUE;
						ServiceAdapDb.vxlan_vni = (ADAP_Uint32)((entry->vniId[0]) & 0xFFFFFFFF); // get vni from database
						ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
						ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);
						ServiceAdapDb.editing = ADAP_EHP_EXTRACT_VXLAN; //ADAP_EHP_EXTRACT_VXLAN_AND_APPEND_VLAN_TAG;
						ServiceAdapDb.editOutervlanId=10;
						ServiceAdapDb.editInnervlanId=0;
						ServiceAdapDb.num_of_output_ports=1;
						ServiceAdapDb.outports[0]=tClusterId;

						if(MeaDrvCreatePointToPointServices (&ServiceAdapDb) != MEA_OK)
						{
							printf("failed to create vxlan configuration\r\n");
							return ENET_FAILURE;
						}

						pServiceDb->vtepService.valid[j] = ADAP_TRUE;
						pServiceDb->vtepService.serviceId[j] = ServiceAdapDb.Service_id;
					}
				}
			}
			else
			{
				pServiceDb->refCount++;
			}
		}

		adap_memset(&tVxLan,0,sizeof(tVxLanAdap));
		adap_memcpy(&tVxLan.destMac,&entry->vtep_mac_dest,sizeof(tEnetHal_MacAddr));

		tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[0] & 0xFF) << 24);
		tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[1] & 0xFF) << 16);
		tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[2] & 0xFF) << 8);
		tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[3] & 0xFF) );

		tVxLan.tunnel_id=(ADAP_Uint32)((entry->vniId[0]) & 0xFFFFFFFF); // get vni from database
		tVxLan.dest_port=4789;
		tVxLan.source_port=10000;
		tVxLan.ttl=64;

		if(create_vxlan_flooding_entry(&tVxLan) != ENET_SUCCESS)
		{
			printf("failed to create vxlan flooding entry\r\n");
			return ENET_FAILURE;
		}


		entry = ovsdb_getnext_entry(entry);
	}

#if 0
	pServiceDb = ovsdb_get_global_evxlanService_ntry();

	globalEntry = ovsdb_get_global_entry();
	for(i=0;i<globalEntry->num_of_entries;i++)
	{
		printf("vtep vni:%d\r\n",globalEntry->vniId[i]);
		for(j=0;j<MAX_NUM_OF_NIC_PORTS;j++)
		{
			if(pServiceDb[i].valid[j] == ADAP_FALSE)
			{
				// create vxlan configuration
				adap_memset(&ServiceAdapDb,0,sizeof(ServiceAdapDb));

				ServiceAdapDb.Service_id = MEA_PLAT_GENERATE_NEW_ID;
				ServiceAdapDb.forwadingKeyType=0xFFFFFFFF;
				ServiceAdapDb.inPort=104+j;
				ServiceAdapDb.L2_protocol_type=MEA_PARSING_L2_KEY_Untagged;
				ServiceAdapDb.vxlan_enable =ADAP_TRUE;
				ServiceAdapDb.vxlan_vni = (ADAP_Uint32)((globalEntry->vniId[i]) & 0xFFFFFFFF); // get vni from database
				ServiceAdapDb.policer_id = MeaAdapGetDefaultPolicer(1);
				ServiceAdapDb.tmId=MeaAdapGetDefaultTm(1);
				ServiceAdapDb.editing = ADAP_EHP_EXTRACT_VXLAN; //ADAP_EHP_EXTRACT_VXLAN_AND_APPEND_VLAN_TAG;
				ServiceAdapDb.editOutervlanId=10;
				ServiceAdapDb.editInnervlanId=0;
				ServiceAdapDb.num_of_output_ports=1;
				ServiceAdapDb.outports[0]=tClusterId;

				if(MeaDrvCreatePointToPointServices (&ServiceAdapDb) != MEA_OK)
				{
					printf("failed to create vxlan configuration\r\n");
					return ENET_FAILURE;
				}

				pServiceDb[i].valid[j] = ADAP_TRUE;
				pServiceDb[i].serviceId[j] = ServiceAdapDb.Service_id;
			}
			entry = ovsdb_getfirst_entry();
			while(entry)
			{
				printf("vtep mac address:%x:%x:%x:%x:%x:%x\r\n",
									entry->vtep_mac_dest[0],
									entry->vtep_mac_dest[1],
									entry->vtep_mac_dest[2],
									entry->vtep_mac_dest[3],
									entry->vtep_mac_dest[4],
									entry->vtep_mac_dest[5]);

				printf("vtep ip address:%d.%d.%d.%d\r\n",
									entry->vtep_ipv_dest[0],
									entry->vtep_ipv_dest[1],
									entry->vtep_ipv_dest[2],
									entry->vtep_ipv_dest[3]);


				adap_memset(&tVxLan,0,sizeof(tVxLanAdap));
				adap_memcpy(tVxLan.destMac,entry->vtep_mac_dest,ENETHAL_MAC_ADDR_LEN);

				tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[0] & 0xFF) << 24);
				tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[1] & 0xFF) << 16);
				tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[2] & 0xFF) << 8);
				tVxLan.destIp |= ((ADAP_Uint32)(entry->vtep_ipv_dest[3] & 0xFF) );

				tVxLan.tunnel_id=(ADAP_Uint32)((globalEntry->vniId[i]) & 0xFFFFFFFF); // get vni from database
				tVxLan.dest_port=4789;
				tVxLan.source_port=10000;
				tVxLan.ttl=64;

				if(create_vxlan_flooding_entry(&tVxLan) != ENET_SUCCESS)
				{
					printf("failed to create vxlan flooding entry\r\n");
					return ENET_FAILURE;
				}


				entry = ovsdb_getnext_entry(entry);
			}

		}

	}
#endif
	return ENET_SUCCESS;
}




