/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_SEARCH_ENGINE_H
#define ADAP_SEARCH_ENGINE_H


MEA_Status MeaDrvHwDeleteDestIpForwarder(ADAP_Uint32 ipAddr,ADAP_Uint32 ipclass,ADAP_Uint16 VpnIndex);
MEA_Status MeaDrvHwCreateDestIpForwarder(MEA_Action_t action_id,MEA_Bool action_valid,ADAP_Uint32 ipAddr,ADAP_Uint32 IpClass,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvHwCreateNatForwarder(MEA_Action_t action_id,ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_Port_t enetPort,MEA_SE_Forwarding_key_type_te type);
MEA_Status MeaDrvHwGetNatForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_SE_Forwarding_key_type_te type);
MEA_Status MeaDrvHwDeleteNatForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 l4Port,ADAP_Uint16 VpnIndex,MEA_SE_Forwarding_key_type_te type);
MEA_Status MeaDrvHwCreateDestIpSaDaForwarder(MEA_Action_t action_id,MEA_Bool action_valid,ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvHwDeleteDestIpSaDaForwarder(ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex);
MEA_Status MeaDrvHwGetDestIpSaDaForwarder(ADAP_Uint32 srcIpAddr,ADAP_Uint32 destIpAddr,ADAP_Uint16 VpnIndex);
MEA_Status MeaDrvHwSetDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts, MEA_Action_t  *Action_id);
MEA_Status MeaDrvHwGetDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex, MEA_Action_t   *Action_id);
MEA_Status MeaDrvHwDeleteDestMacForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_Action_t   *Action_id);
MEA_Status MeaDrvHwCreateDestMacForwarder(MEA_Action_t action_id,MEA_Bool action_valid,tEnetHal_MacAddr *macAddr,ADAP_Uint16 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvHwGetAllDestIpForwarder(MEA_SE_Entry_key_dbt  *pEntryKey,ADAP_Uint32 *maxNumOfEntries);
MEA_Bool MeaDrvIsIpAddressInIpForwarder(ADAP_Uint32 ipAddr,ADAP_Uint16 VpnIndex);
MEA_Status MeaDrvHwCreatePPPoEForwarder(MEA_Action_t action_id,tEnetHal_MacAddr *macAddr,ADAP_Uint32 sessionId,ADAP_Uint32 VpnIndex,MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvHwDeletePPPoEForwarder(tEnetHal_MacAddr *macAddr,ADAP_Uint32 sessionId,ADAP_Uint32 VpnIndex);
MEA_Status MeaDrvHwCreateDestIpv6Forwarder(MEA_Action_t action_id, MEA_Bool toCPU, tIPv6Addr ipv6Addr,
        MEA_IPCS_IPV6_MASK_Type_t mask_type, ADAP_Uint16 VpnIndex, MEA_OutPorts_Entry_dbt *pOutPorts);
MEA_Status MeaDrvHwDeleteDestIpv6Forwarder(tIPv6Addr ipv6Addr, MEA_IPCS_IPV6_MASK_Type_t mask_type,
        ADAP_Uint16 VpnIndex);
#endif
