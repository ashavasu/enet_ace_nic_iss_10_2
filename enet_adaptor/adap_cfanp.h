/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include "EnetHal_L2_Api.h"

#define ADAP_VLAN_TAG_OFFSET				12
#define ADAP_VLAN_TAGGED_HEADER_SIZE		16

#define  ENET_VLAN_TAG_OFFSET            	ADAP_VLAN_TAG_OFFSET
#define  ENET_VLAN_TAGGED_HEADER_SIZE       ADAP_VLAN_TAGGED_HEADER_SIZE

#define ADAP_ARP_PROTOCOL			0x806
#define ADAP_IP_PROTOCOL			0x800

#define ADAP_ICMP_ECHOREPLY			0
#define ADAP_ICMP_DEST_UNREACH		3
#define ADAP_ICMP_SOURCE_QUENCH		4
#define ADAP_ICMP_REDIRECT			5
#define ADAP_ICMP_ECHO				8
#define ADAP_ICMP_TIME_EXCEEDED		11
#define ADAP_ICMP_PARAMETERPROB		12
#define ADAP_ICMP_TIMESTAMP			13
#define ADAP_ICMP_TIMESTAMPREPLY		14
#define ADAP_ICMP_INFO_REQUEST		15
#define ADAP_ICMP_INFO_REPLY			16
#define ADAP_ICMP_ADDRESS			17
#define ADAP_ICMP_ADDRESSREPLY		18



typedef struct
{
	tEnetHal_MacAddr  ether_dhost;    /* destination host address */
	tEnetHal_MacAddr  ether_shost;    /* source host address */
    ADAP_Uint16 vlanTpid;
    ADAP_Uint16 vlanId;
    ADAP_Uint16 ether_type;                     /* IP? ARP? RARP? etc */
}sniff_ethernet;

/* IP header */
typedef struct
{
	ADAP_Uint8  ip_vhl;                 /* version << 4 | header length >> 2 */
	ADAP_Uint8  ip_tos;                 /* type of service */
	ADAP_Uint16 ip_len;                 /* total length */
	ADAP_Uint16 ip_id;                  /* identification */
	ADAP_Uint16 ip_off;                 /* fragment offset field */
        #define IP_RF 0x8000            /* reserved fragment flag */
        #define IP_DF 0x4000            /* dont fragment flag */
        #define IP_MF 0x2000            /* more fragments flag */
        #define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
	ADAP_Uint8  ip_ttl;                 /* time to live */
	ADAP_Uint8  ip_p;                   /* protocol */
	ADAP_Uint16 ip_sum;                 /* checksum */
	ADAP_Uint8  ip_src[4];
	ADAP_Uint8 ip_dst[4];  /* source and dest address */
}sniff_ip;

typedef struct
{
	ADAP_Uint8 type;		/* message type */
	ADAP_Uint8 code;		/* type sub-code */
	ADAP_Uint16 checksum;
  union
  {
    struct
    {
    	ADAP_Uint16	id;
    	ADAP_Uint16	sequence;
    } echo;			/* echo datagram */
    u_int32_t	gateway;	/* gateway address */
    struct
    {
    	ADAP_Uint16	__unused;
    	ADAP_Uint16	mtu;
    } frag;			/* path mtu discovery */
  } un;
}sniff_icmp;



typedef struct
{
	ADAP_Uint8 ea_hdr[8];			/* fixed-size header */
	tEnetHal_MacAddr arp_sha;	/* sender hardware address */
	ADAP_Uint8 src_ipaddr[4];			/* sender protocol address */
	tEnetHal_MacAddr arp_tha;	/* target hardware address */
	ADAP_Uint8 dst_ipaddr[4];			/* target protocol address */
}sniff_arp;

typedef struct {

	ADAP_Uint16 udph_srcport;
	ADAP_Uint16 udph_destport;
	ADAP_Uint16 udph_len;
	ADAP_Uint16 udph_chksum;

}sniff_udp;

typedef struct {
	ADAP_Uint16 src_port;
	ADAP_Uint16 dst_port;
	ADAP_Uint32 seq;
	ADAP_Uint32 ack;
	ADAP_Uint8  data_offset;  // 4 bits
	ADAP_Uint8  flags;
	ADAP_Uint16 window_size;
	ADAP_Uint16 checksum;
	ADAP_Uint16 urgent_p;
} sniff_tcp;



typedef struct
{
	sniff_ethernet ethernet;
	sniff_ip	   ip;
	sniff_arp	   arp;
	sniff_icmp	   icmp;
	sniff_udp	   udp;
	sniff_tcp	   tcp;

	ADAP_Uint32		sourceIp;
	ADAP_Uint32		destIp;
}tProtocl_sniff;


ADAP_Int32 MeaAdapSendPacket (ADAP_Uint8 *pu1DataBuf, ADAP_Uint16 LogIndex, ADAP_Uint32 PktSize);
ADAP_Uint32 init_socket_task(void);

ADAP_Bool get_sourceip_from_ipPacket(unsigned char *buf, int len,tProtocl_sniff *pProtoSniff,ADAP_Uint32 *pIndex_data);

ADAP_Uint32 EnetAdapHandleNATForPktFromWAN (tProtocl_sniff *pProtoSniff);
ADAP_Uint32 EnetAdapHandleNATForPktFromLAN (tProtocl_sniff *pProtoSniff);


