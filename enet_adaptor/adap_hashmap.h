/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others,
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002
*/
#ifndef __ADAP_HASHMAP_H__
#define __ADAP_HASHMAP_H__

#include "adap_types.h"
#include "adap_linkedlist.h"
#include "EnetHal_status.h"

typedef struct adap_hashmap adap_hashmap;

typedef struct {  /* TODO - need to hide the struct content in some way */
	adap_linkedlist_element list_item;
	ADAP_Uint64 key;
} adap_hashmap_element;


struct adap_hashmap_params {
	ADAP_Uint32 capacity;
	ADAP_Uint16 num_of_buckets;
	ADAP_Bool thread_safe;
	ADAP_Uint64 (*hash)(ADAP_Uint64 key);
	EnetHal_Status_t (*flush)(ADAP_Uint64 key, adap_hashmap_element* element);
};

/**
 * @brief Create a new hash map with given parameters.
 * @return
 */
EnetHal_Status_t adap_hashmap_init(adap_hashmap** map, struct adap_hashmap_params *params);


/**
 * @brief Destroy hash map
 * @return
 */
EnetHal_Status_t adap_hashmap_deinit(adap_hashmap* map);


/**
 * @brief Insert {key,value} to hash map
 * @return
 */
EnetHal_Status_t adap_hashmap_insert(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element* value);


/**
 * @brief Remove {key,value} from hash map
 * @return
 */
EnetHal_Status_t adap_hashmap_remove(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element** value);



/**
 * @brief Find {key,value} in hash map
 * @return
 */
EnetHal_Status_t adap_hashmap_find(adap_hashmap* map, ADAP_Uint64 key, adap_hashmap_element **value);


/**
 * @brief Get number of {key,value} in hash map
 * @return
 */
ADAP_Uint32 adap_hashmap_size(adap_hashmap* map);


#endif /* __ADAP_HASHMAP_H__ */





