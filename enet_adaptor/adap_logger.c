/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef __ENET_LOGGER_C__
#define __ENET_LOGGER_C__

#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_utils.h"
#include "adap_logger.h"

static char *log_level="sed -e '1,/start-print-log-level/d' -e '/end-print-log-level/,$d' ./enet_log_level.txt";
static char *syslog_level="sed -e '1,/start-print-syslog-level/d' -e '/end-print-syslog-level/,$d' ./enet_log_level.txt";


static ADAP_Uint32 logOpen=0;
static ADAP_Uint32 logLevelbitmask=ENET_ADAP_NOTICE;
static ADAP_Uint32 syslogLevelbitmask=0;


static MEA_Bool isLogLevelBitSet(unsigned int log_level_bitmask)
{
	if(logLevelbitmask & (1 << log_level_bitmask) )
	{
		return MEA_TRUE;
	}
	return MEA_FALSE;
}

static MEA_Bool isSysLogLevelBitSet(unsigned int log_level_bitmask)
{
	if(syslogLevelbitmask & (1 << log_level_bitmask) )
	{
		return MEA_TRUE;
	}
	return MEA_FALSE;
}



static int read_log_level(char *log_level_name,ADAP_Uint32 *plogLevel)
{

    FILE *in;
    char buff[512];

    (*plogLevel) = 0;

    if(!(in = popen(log_level_name, "r")))
    {
    	fprintf(stdout,"file not found\r\n");
    	return 0;
    }
    while(fgets(buff, sizeof(buff), in)!=NULL)
    {
    	//fprintf(stdout,"%s\r\n",buff);
    	if(strstr(buff,"ENET_ADAP_ALERT") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_ALERT;
    		//fprintf(stdout,"alert log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_CRIT") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_CRIT;
    		//fprintf(stdout,"critical log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_ERR") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_ERR;
    		//fprintf(stdout,"error log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_WARNING") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_WARNING;
    		//fprintf(stdout,"warning log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_INFO") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_INFO;
    		//fprintf(stdout,"info log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_DEBUG") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_DEBUG;
    		//fprintf(stdout,"debug log open\r\n");
    	}
    	if(strstr(buff,"ENET_ADAP_NOTICE") != NULL)
    	{
    		(*plogLevel) |= 1 << ENET_ADAP_NOTICE;
    		//fprintf(stdout,"notice log open\r\n");
    	}
    }
    (*plogLevel) |= 1 << ENET_ADAP_ERR;
    fprintf(stdout,"bitmask open log level:0%x\r\n",(*plogLevel));
    pclose(in);
    //end of log level
    return 1;
}

int read_pring_log_level(void)
{

	read_log_level(log_level,&logLevelbitmask);
	read_log_level(syslog_level,&syslogLevelbitmask);

	return 0;
}




ADAP_Uint32 DplOpenSyslogApplication(int LogMask)
{
	openlog ("EnetAdap", LogMask,LOG_LOCAL0);
	logOpen=1;
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"openLog");
	return ENET_SUCCESS;
}

ADAP_Uint32 DplSetLogSyslogApplication(int LogMask)
{
	setlogmask(LogMask);
	return ENET_SUCCESS;
}



ADAP_Uint32 DplCloseSyslogApplication(void)
{
	logOpen=0;
	closelog();
	return ENET_SUCCESS;
}
/*
 * LOG_ALERT  , LOG_CRIT , LOG_ERR , LOG_WARNING ,LOG_NOTICE , LOG_INFO , LOG_DEBUG
*
*/

void DplPrintLog(char *fileName,const char *functionName,int line,int logLevel,char *fmt, ...)
{
	va_list ap,ap1;
	char *strFileName;
	char msg[200];
	int idx=0;


	adap_memset(msg,0,sizeof(msg));
#if 0
	if(logOpen == 0)
	{
		printf("log is closed\r\n");
		return;
	}
#endif
//	if(logLevel == ENET_ADAP_INFO)
//	{
//		printf("file name:%s function:%s line:%d\r\n",fileName,functionName,line);
//	}

	va_start(ap, fmt);
	va_copy(ap1,ap);

	strFileName = strstr(fileName,"enet_adaptor");
	if(strFileName == NULL)
	{
		strFileName = strstr(fileName,"enetadaptor");
		if(strFileName == NULL)
		{
			strFileName = fileName;
		}
		else
		{
			strFileName += strlen("enetadaptor");
		}

	}
	else
	{
		strFileName += strlen("enet_adaptor");
	}

	idx = strlen(strFileName) + strlen(functionName);


	sprintf(msg,"FILE:%s FUNC:%s line:%d ",strFileName,functionName,line);
	if(idx < 100)
	{
		sprintf(msg,"FILE:%s FUNC:%s line:%d ",strFileName,functionName,line);
		if(isSysLogLevelBitSet((unsigned int)logLevel) == MEA_TRUE)
		{
			syslog(logLevel,"%s",msg);
		}
	}

	if(isSysLogLevelBitSet((unsigned int)logLevel) == MEA_TRUE)
	{
		vsyslog(logLevel,fmt,ap);
	}

	if(isLogLevelBitSet((unsigned int)logLevel) == MEA_TRUE)
	//if( (logLevel == ENET_ADAP_CRIT) || (logLevel == ENET_ADAP_ERR) || (logLevel == ENET_ADAP_WARNING) || (logLevel == ENET_ADAP_INFO) )
//	if( (strstr(strFileName,"lanp.c") != NULL) || (strstr(strFileName,"vlanminp.c") != NULL))
	{
		switch(logLevel)
		{
			case ENET_ADAP_ALERT:
				fprintf(stdout,"ALERT: ");
				break;
			case ENET_ADAP_CRIT:
				fprintf(stdout,"CRIT: ");
				break;
			case ENET_ADAP_ERR:
				fprintf(stdout,"ERR: ");
				break;
			case ENET_ADAP_WARNING:
				fprintf(stdout,"WARNING: ");
				break;
			case ENET_ADAP_INFO:
				fprintf(stdout,"INFO: ");
				break;
			case ENET_ADAP_DEBUG:
				fprintf(stdout,"DEBUG: ");
				break;
			case ENET_ADAP_NOTICE:
				fprintf(stdout,"NOTICE: ");
				break;
			case ENET_ADAP_NOTICE1:
				fprintf(stdout,"NOTICE_ADAP: ");
				break;
			default:
				fprintf(stdout,"Unknown: ");
				break;
		}
		fprintf(stdout,"%s", msg);
		//vprintf( fmt, ap );
		while( *fmt != '\0' )
		{
			if(strncmp(fmt,"%s",2) == 0)
			{
				fprintf(stdout,"%s",va_arg( ap1, char * ));
				fmt+=2;
			}
			else if(strncmp(fmt,"%lu",3) == 0)
			{
				fprintf(stdout,"%lu",va_arg( ap1,unsigned long ));
				fmt+=3;
			}
			else if(strncmp(fmt,"%d",2) == 0)
			{
				fprintf(stdout,"%d",va_arg( ap1,int ));
				fmt+=2;
			}
			else if(strncmp(fmt,"%x",2) == 0)
			{
				fprintf(stdout,"%x",va_arg( ap1,int ));
				fmt+=2;
			}
			else
			{
				fprintf(stdout,"%c", *fmt);
				fmt++;
			}

		}
	}
    va_end(ap);
    va_end(ap1);

}
#if 0
void syslog_example(int logLevel,char *fmt, ...)
{
	va_list ap;
//	char *msg[400];
//	UINT4 idx=0;


	if(adp_GlobalScalars.logOpen == 0)
	{
		return;
	}

	va_start(ap, fmt);

	vsyslog(logLevel,fmt,ap);


	if( (logLevel == ENET_ADAP_CRIT) || (logLevel == ENET_ADAP_ERR) || (logLevel == ENET_ADAP_WARNING) )
	{
		printf(fmt, ap);
	}
    va_end(ap);

}

#endif

#endif
