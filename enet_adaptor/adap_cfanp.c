/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/ioctl.h>
//#include <net/if.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_cfanp.h"
#include "adap_enetnp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_service_config.h"
#include "adap_acl.h"
#include "EnetHal_L2_Api.h"

static EnetHal_linkstatus_callback_func EnetHal_linkstatus_callback_ptr=NULL;

inline void register_linkstatus_callback(void *func)
{
  MEA_API_set_Interface_ext_Link_callBack(MEA_UNIT_0, (pGetIntExtLinkSttCallBack)func);
}

ADAP_Int32 Adap_LinkStatus(MEA_Unit_t unit, MEA_Port_t interfaceId, MEA_Bool *status)
{
  ADAP_Uint32 logport;
  ADAP_Uint8 linkstatus;

  if(EnetHal_linkstatus_callback_ptr != NULL)
  {
    if(status == NULL)
      return ENET_FAILURE;

    linkstatus = (*status == MEA_TRUE)? ADAP_PORT_LINK_UP: ADAP_PORT_LINK_DOWN;

    if(MeaAdapGetLogPortFromPhyPort (&logport, interfaceId) == ENET_SUCCESS)
    {
      EnetHal_linkstatus_callback_ptr ((ADAP_Uint32)logport, linkstatus);
    }
    else
      ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"can't get logical port from phy port\r\n");
  }

  return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_FsCfaHwClearStats (ADAP_Uint32 u4Port)
{
   	MEA_Port_t						enetPort;

    if(u4Port < MAX_NUM_OF_SUPPORTED_PORTS)
    {
        if(u4Port >= MeaAdapGetNumOfPhyPorts())
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Skip statistics clear for ifIndex:%d\r\n",u4Port);
            return ENET_SUCCESS;
        }

    	if(MeaAdapGetPhyPortFromLogPort(u4Port, &enetPort) != ENET_SUCCESS)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4Port);
    		return ENET_FAILURE;
    	}

        if (MEA_API_Get_IsRmonPortValid(enetPort, MEA_TRUE) == MEA_FALSE){
               ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsCfaHwClearStats - ISS port: %d is not in the range \n", u4Port);
           return ENET_SUCCESS;
        }

    	if (MEA_API_Clear_Counters_RMON(MEA_UNIT_0, enetPort) != MEA_OK)
    	{
    		return ENET_FAILURE;
    	}
    }
    else
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsCfaHwClearStats - ISS port:%d is out of range!!!\n", u4Port);
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 adap_HwSetAdminStatus (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1EgressEnable)
{
   ADAP_Int32 ret = ENET_SUCCESS;
    MEA_Port_t enetPort=0;
    ADAP_Bool adminStatus = ADAP_FALSE;

    ret = MeaAdapGetPhyPortFromLogPort(u4IfIndex,&enetPort);
	if(ret != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		if(u1EgressEnable == ADAP_ENABLE)
			adminStatus = ADAP_TRUE;
		else
			adminStatus = ADAP_FALSE;

		// at disable set egress first
		if(MeaAdapSetEgressPort (enetPort,adminStatus,adminStatus) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetEgressPort ifIndex:%d\r\n",u4IfIndex);
			ret = ENET_FAILURE;
		}
		if(MeaAdapSetIngressPortAdminStatus (enetPort,adminStatus,adminStatus) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapSetIngressPort ifIndex:%d\r\n",u4IfIndex);
			ret = ENET_FAILURE;
		}
	}
	//save the current state
	setPortSpape(u4IfIndex,(eAdap_PortStatus)u1EgressEnable);

    return ret;
}

ADAP_Int32 EnetHal_FsHwUpdateAdminStatusChange (ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1AdminEnable)
{
	ADAP_Uint32		masterPort = 0;
	ADAP_Uint16		lagId = 0;
	MEA_Port_t		EnetPortId = 0;

	masterPort = u4IfIndex;

	if (adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
   		if( (adap_NumOfLagPortFromAggrId(u4IfIndex)) > 0)
		{
			masterPort = adap_GetLagMasterPort(u4IfIndex);
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"LAG Id:%d has not any physical ports\n",u4IfIndex);
			return ENET_SUCCESS;
		}
	}

    if (adap_HwSetAdminStatus (masterPort,u1AdminEnable) != ENET_SUCCESS)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set admin status for port%d\r\n", u4IfIndex);
    	return ENET_FAILURE;
    }

    if (adap_isPortPartOfLag(u4IfIndex) == ENET_SUCCESS)
	{
		lagId = adap_PortGetEnetLagId(u4IfIndex);

		if(lagId != ADAP_END_OF_TABLE)
		{

			if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &EnetPortId) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n", u4IfIndex);
				return ENET_FAILURE;
			}

			if (u1AdminEnable == ADAP_ENABLE)
			{
				if(MEA_API_LAG_Set_ClusterMask(MEA_UNIT_0, lagId, EnetPortId, MEA_TRUE) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set cluster musk of EnetPort:%d\r\n", EnetPortId);
					return ENET_FAILURE;
				}
			}
			else if (u1AdminEnable == ADAP_DISABLE)
			{
				if(MEA_API_LAG_Set_ClusterMask(MEA_UNIT_0, lagId, EnetPortId, MEA_FALSE) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set cluster musk of EnetPort:%d\r\n", EnetPortId);
					return ENET_FAILURE;
				}
			}
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lagId:%d\r\n", u4IfIndex);
			return ENET_FAILURE;
		}
	}

    return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_CfaRegisterWithNpDrv (EnetHal_rx_packet_callback_func func)
{
	// create service from CPU to output port
	init_socket_task();
	register_rx_packet_callback(func);
    return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_CfaHwL3VlanIntfWrite (ADAP_Uint8 *pu1DataBuf, ADAP_Uint32 u4PktSize,tEnetHal_VlanId VlanId,
						ADAP_Uint32 ifIndex, ADAP_Uint16 u2InnerVlan, ADAP_Uint16 u2EtherType)
{
	ADAP_UNUSED_PARAM (*pu1DataBuf);
	ADAP_UNUSED_PARAM (u4PktSize);
	ADAP_UNUSED_PARAM (VlanId);
	ADAP_UNUSED_PARAM (ifIndex);
	ADAP_Uint8 *newPacket=NULL;
	ADAP_Int32 ret=ENET_SUCCESS;

	if(VlanId != 0)
	{
		if( ((pu1DataBuf[12] == 0x81) && (pu1DataBuf[13] == 0x00)) ||
		    ((pu1DataBuf[12] == 0x88) && (pu1DataBuf[13] == 0xa8)) )
		{
			return MeaAdapSendPacket (pu1DataBuf,ifIndex,u4PktSize);
		}
		// add vlanTag
		newPacket = (ADAP_Uint8 *)adap_malloc(u4PktSize+4);

		if(newPacket == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"\n=failed to allocate memory\n");
			return ENET_FAILURE;
		}

		adap_memcpy(newPacket, pu1DataBuf,ADAP_VLAN_TAG_OFFSET);

		adap_memcpy(&newPacket[ADAP_VLAN_TAG_OFFSET], (ADAP_Uint8 *)&u2EtherType, 2);
		newPacket[ADAP_VLAN_TAG_OFFSET+2]=(ADAP_Uint8)((VlanId & 0xFF00) >> 8);
		newPacket[ADAP_VLAN_TAG_OFFSET+3]=(ADAP_Uint8)((VlanId & 0x00FF));

		adap_memcpy(newPacket+ADAP_VLAN_TAGGED_HEADER_SIZE,
				    pu1DataBuf+ADAP_VLAN_TAG_OFFSET,
					u4PktSize-ADAP_VLAN_TAG_OFFSET);

		if (u2InnerVlan != 0)
		{
			newPacket[ADAP_VLAN_TAG_OFFSET]=0x81;
			newPacket[ADAP_VLAN_TAG_OFFSET+1]=0x00;
			newPacket[ADAP_VLAN_TAG_OFFSET+2]=(ADAP_Uint8)((u2InnerVlan & 0xFF00) >> 8);
			newPacket[ADAP_VLAN_TAG_OFFSET+3]=(ADAP_Uint8)((u2InnerVlan & 0x00FF));

			adap_memcpy(newPacket+ADAP_VLAN_TAGGED_HEADER_SIZE,
					    pu1DataBuf+ADAP_VLAN_TAG_OFFSET,
						u4PktSize-ADAP_VLAN_TAG_OFFSET);

		}

	    ret = MeaAdapSendPacket (newPacket,ifIndex, u4PktSize+4);

	    //ret = CfaNpPortWrite (newPacket,ifIndex,u4PktSize+4);
	    free(newPacket);
	}
	else
	{
		 ret = MeaAdapSendPacket (pu1DataBuf,ifIndex,u4PktSize);
	}

    return ret;
}

static ADAP_Int32 adap_SetClusterMtu (MEA_Port_t QueueId, ADAP_Uint32 u4MtuSize)
{
	ADAP_Uint16   	PriQIdx;
    ENET_Queue_dbt  enetQueue;

    adap_memset(&enetQueue, 0, sizeof(enetQueue));
    if(ENET_IsValid_Queue(ENET_UNIT_0, QueueId, ENET_FALSE)!=ENET_TRUE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ENET Queue %d is invalid \n",QueueId);
        return ENET_FAILURE;
    }
    if(ENET_Get_Queue (ENET_UNIT_0, QueueId, &enetQueue)!=ENET_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Can\'t get the ENET Queue %d!!\n",QueueId);
        return ENET_FAILURE;
    }

    for(PriQIdx = 0; PriQIdx < ENET_PLAT_QUEUE_NUM_OF_PRI_Q; PriQIdx++)
    {
        enetQueue.pri_queues[PriQIdx].mtu = u4MtuSize;
    }

    if(ENET_Set_Queue (ENET_UNIT_0, QueueId, &enetQueue)!=ENET_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Can not update the Queue %d  \n", QueueId);
        return ENET_FAILURE;
    }

	return ENET_SUCCESS;
}

static
ADAP_Int32 adap_HwSetMtuPhyPort (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4MtuSize)
{
    MEA_Port_t      enetPort;
    ADAP_Uint16     ClusterIdx;
    ENET_QueueId_t  QueueId;

	/* set the MTU size */
	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}

    /* First update  the first cluster for this port (enetPort) */
    if(adap_SetClusterMtu ((ENET_QueueId_t)enetPort, u4MtuSize)!=ENET_SUCCESS)
     {
         ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Can't set the ENET Queue %d!!\n",enetPort);
         return ENET_FAILURE;
     }

    for(ClusterIdx=0; ClusterIdx<NUM_OF_CLUSTERS_PER_PORT; ClusterIdx++)
    {
        QueueId = adap_GetPortQueueID(u4IfIndex, ClusterIdx);
        if(adap_SetClusterMtu (QueueId, u4MtuSize)!=ENET_SUCCESS)
		{
			 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Can't set the ENET Queue %d!!\n",QueueId);
			 return ENET_FAILURE;
		}
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_CfaRegisterLinkStatus (EnetHal_linkstatus_callback_func func)
{
  EnetHal_linkstatus_callback_ptr = func;
  register_linkstatus_callback((void*)Adap_LinkStatus);
  return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_FsCfaHwSetMtu (ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4MtuSize)
{
    tIpInterfaceTable *pInterfaceTable = NULL;
    tBridgeDomain *pBridgeDomain = NULL;
    tServiceDb * srv = NULL;
    tUpdateServiceParam params;
    MEA_AC_Mtu_dbt entry_pi;

    ADAP_Uint16 mtuProfId;

    memset (&params, 0, sizeof(params));

    if(u4MtuSize%64 != 0)
    {
        u4MtuSize = (u4MtuSize/64)*64;
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"MTU new size %d is\n",u4MtuSize);
    }

    //Check if we set mtu for physical port
    if(u4IfIndex < MeaAdapGetNumOfPhyPorts())
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Setting MTU for phy port\n",u4IfIndex);
        return adap_HwSetMtuPhyPort(u4IfIndex, u4MtuSize);
    }
    else
    {
        pInterfaceTable = AdapGetIpInterface(u4IfIndex);

        if(pInterfaceTable == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n", u4IfIndex);
            return ENET_FAILURE;
        }
        // Getting bridge domain
        pBridgeDomain = Adap_getBridgeDomainByVlan(pInterfaceTable->u2VlanId,0);
        if(pBridgeDomain == NULL)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pBridgeDomain not found for vlanId:%d not exist\n",
                 pInterfaceTable->u2VlanId);
            return ENET_FAILURE;
        }

        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"Creating mtu profile for vlan:%d num of ports:%d\n",
             pInterfaceTable->u2VlanId,pBridgeDomain->num_of_ports);

        entry_pi.mtu_size = u4MtuSize;

        if (AdapGetOrAddMtuProfForVlan(pInterfaceTable->u2VlanId, &mtuProfId) != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR, Can't get mtu flow profile\n");
            return ENET_FAILURE;
        }

        // Creating MTU profile using vlanID as profId
        if (MEA_API_Set_Flow_MTU_Prof(MEA_UNIT_0, mtuProfId, &entry_pi) != MEA_OK)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ERROR: unable to set MTU prof\n");
            return ENET_FAILURE;
        }

        // Getting vlan list
        srv = MeaAdapGetFirstServiceVlan(pInterfaceTable->u2VlanId);
        while (srv)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,
                    "Setting mtu for the service %d\n", srv->serviceId);

            params.Service = srv->serviceId;
            params.type = E_UPDATE_MTU_PROF;
            params.x.mtuFlowPolicerId = mtuProfId;
            if (MeaDrvUpdateService(&params) != MEA_OK)
            {
                ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,
                        "ERROR: unable to set mtu profile for service %d\n", srv->serviceId);
                return ENET_FAILURE;
            }
            srv = MeaAdapGetNextServiceVlan(pInterfaceTable->u2VlanId, srv->serviceId);
        }
    }
    return ENET_SUCCESS;
}


ADAP_Int32 EnetHal_FsCfaHwSetMacAddr (ADAP_Uint32 u4IfIndex, tEnetHal_MacAddr *PortMac)
{
	MEA_Port_t			PhyPort=0;

	if(u4IfIndex > 0)
	{
		if(MeaAdapGetPhyPortFromLogPort(u4IfIndex,&PhyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		MeaDrvHwSetInterfaceCfmOamUCMac (PhyPort,PortMac);
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"u4IfIndex:%d\r\n", u4IfIndex);
		return ENET_FAILURE;
	}
#ifdef	MEA_NAT_DEMO
	//MeaDrvSetGlobalMacAddress(PortMac,ADAP_TRUE);
	if(MeaDrvHwSetGlobalUCMac((char *)PortMac) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwSetGlobalUCMac\r\n");
		return ENET_FAILURE;
	}
#endif
    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsHwGetStat (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, ADAP_Uint32 *pu4Value)
{
	tEnetHal_COUNTER64_TYPE Value64;

	if(EnetHal_FsHwGetStat64(u4IfIndex, i1StatType, &Value64) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get RMON statistics for Port%d StatType:%d\n", u4IfIndex, i1StatType);
		return ENET_FAILURE;
	}

	*pu4Value = Value64.lsn;

    return ENET_SUCCESS;

}

ADAP_Int32 EnetHal_FsHwGetStat64 (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, tEnetHal_COUNTER64_TYPE * pPortValue)
{
	tEnetHal_counter_64			Value64 = {0};
	tEnetHal_counter_64			LagValue64 = {0};
	tDbaseHwAggEntry			*pLagEntry = NULL;
	ADAP_Uint8					i = 0;

	if (adap_IsPortChannel(u4IfIndex) == ENET_SUCCESS)
	{
		if( (adap_NumOfLagPortFromAggrId(u4IfIndex)) > 0)
		{
			pLagEntry = adap_getLagInstanceFromAggrId(u4IfIndex);
			if (pLagEntry != NULL)
			{
				for (i = 1; i < MeaAdapGetNumOfPhyPorts(); i++)
				{
					if (((pLagEntry->portbitmask >> i ) & 1)  == 1)
					{
						if(EnetHal_FsHwGetPortStat64(i, i1StatType, &Value64.s) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get RMON statistics for Port%d StatType:%d\n", i, i1StatType);
							return ENET_FAILURE;
						}

						LagValue64.val += Value64.val;
					}
				}

				pPortValue->msn = LagValue64.s.msn;
				pPortValue->lsn = LagValue64.s.lsn;
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lag entry by AggrId:%d\r\n", u4IfIndex);
				return MEA_ERROR;
			}
		}
		else
		{
			pPortValue->msn = 0;
			pPortValue->lsn = 0;
			return ENET_SUCCESS;
		}
	}
	else
	{
		if(EnetHal_FsHwGetPortStat64(u4IfIndex, i1StatType, &Value64.s) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unable to get RMON statistics for Port%d StatType:%d\n", u4IfIndex, i1StatType);
			return ENET_FAILURE;
		}

		pPortValue->msn = Value64.s.msn;
		pPortValue->lsn = Value64.s.lsn;
	}

	return ENET_SUCCESS;

}

ADAP_Int32 EnetHal_FsHwGetPortStat64 (ADAP_Uint32 u4IfIndex, ADAP_Uint8 i1StatType, tEnetHal_COUNTER64_TYPE * pValue)
{
    MEA_Counters_RMON_dbt 			MEA_Stats;
    MEA_Counters_IngressPort_dbt 	MEA_Stats_Ingress;
 	MEA_Port_t						enetPort;

 	MEA_Uint64						Value;

    if ((i1StatType == ENET_NP_STAT_UNSUPPORTED)||(i1StatType == ENET_NP_STAT_MAX))
    {
        return ENET_FAILURE;
    }

//    if (NpCfaGetPortsForTrunk ((UINT2) u4IfIndex, au2ConfPorts, &u2NumPorts) ==	ENET_FAILURE)
//    {
//        return ENET_FAILURE;
//    }

    if(u4IfIndex < MeaAdapGetNumOfPhyPorts())
    {
 		if(MeaAdapGetPhyPortFromLogPort(u4IfIndex, &enetPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
			return ENET_FAILURE;
		}

    	if (MEA_API_Get_Counters_RMON(MEA_UNIT_0, enetPort, &MEA_Stats) != MEA_OK)
    	{
    		return (ENET_FAILURE);
    	}

    	if (MEA_API_Get_Counters_IngressPort(MEA_UNIT_0, enetPort, &MEA_Stats_Ingress) != MEA_OK)
    	{
    		return (ENET_FAILURE);
    	}

    	switch(i1StatType)
    	{
    		case ENET_NP_STAT_IF_IN_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_Bytes.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_UCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_UC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_NUCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_BC_Pkts.val + MEA_Stats.Rx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_DISCARDS:
    		{
    			Value.val = MEA_Stats_Ingress.L2CP_Discard.val +
    				        MEA_Stats_Ingress.OAM_Discard.val +
    				        MEA_Stats_Ingress.Mismatch_Discard.val +
    					    MEA_Stats_Ingress.Rx_Fifo_Full_Discard.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_ERRORS:
    		{
    			Value.val = MEA_Stats.Rx_CRC_Err.val + MEA_Stats.Rx_Error_Pkts.val +
                           MEA_Stats.Rx_Mac_Drop_Pkts.val + MEA_Stats.Rx_fragmant_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_UNKNOWN_PROTOS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_OCTETS:
    		{
    			Value.val = MEA_Stats.Tx_Bytes.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_UCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_UC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_NUCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_BC_Pkts.val + MEA_Stats.Tx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_DISCARDS:
    		case ENET_NP_STAT_IF_OUT_ERRORS:
    		case ENET_NP_STAT_IF_OUT_QLEN:
    		case ENET_NP_STAT_IP_IN_RECIEVES:
    		case ENET_NP_STAT_IP_IN_HDR_ERRORS:
    		case ENET_NP_STAT_IP_IN_FORW_DATAGRAMS:
    		case ENET_NP_STAT_IP_IN_DISCARDS:
    		case ENET_NP_STAT_DOT1D_BASE_PORT_DLY_EXCEEDED_DISCARDS:
    		case ENET_NP_STAT_DOT1D_BASE_PORT_MTU_EXCEEDED_DISCARDS:
    		case ENET_NP_STAT_DOT1D_TP_PORT_IN_FRAMES:
    		case ENET_NP_STAT_DOT1D_TP_PORT_OUT_FRAMES:
    		case ENET_NP_STAT_DOT1D_TP_PORT_IN_DISCARDS:
    		case ENET_NP_STAT_ETHER_DROP_EVENTS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_MCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_BCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_BC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_UNDERSIZE_PKTS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_FRAGMENTS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_64_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_64Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_65_TO_127_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_65to127Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_128_TO_255_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_128to255Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_256_TO_511_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_256to511Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_512_TO_1023_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_512to1023Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS_1024_TO_1518_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_1024to1518Octets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_OVERSIZE_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_1519to2047Octets_Pkts.val +
                            MEA_Stats.Rx_2048toMaxOctets_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_JABBERS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_OCTETS:
    		{
    			Value.val = MEA_Stats.Rx_Bytes.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_COLLISIONS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_CRC_ALIGN_ERRORS:
    		{
    			Value.val = MEA_Stats.Rx_CRC_Err.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_TX_NO_ERRORS:
    		{
    			Value.val = MEA_Stats.Tx_Pkts.val - MEA_Stats.Tx_CRC_Err.val;
    		}
    		break;
    		case ENET_NP_STAT_ETHER_RX_NO_ERRORS:
    		{
    			Value.val = MEA_Stats.Rx_Pkts.val - MEA_Stats.Rx_CRC_Err.val - MEA_Stats.Rx_Error_Pkts.val;
    		}
    		break;

    		case ENET_NP_STAT_DOT3_ALIGNMENT_ERRORS:
    		case ENET_NP_STAT_DOT3_FCS_ERRORS:
    		case ENET_NP_STAT_DOT3_SINGLE_COLLISION_ERRORS:
    		case ENET_NP_STAT_DOT3_MULTIPLE_COLLISION_ERRORS:
    		case ENET_NP_STAT_DOT3_SQET_TEST_ERRORS:
    		case ENET_NP_STAT_DOT3_DEFERRED_TRANSMISSIONS:
    		case ENET_NP_STAT_DOT3_LATE_COLLISIONS:
    		case ENET_NP_STAT_DOT3_EXCESSIVE_COLLISIONS:
    		case ENET_NP_STAT_DOT3_INTERNAL_MAC_TX_ERRORS:
    		case ENET_NP_STAT_DOT3_CARRIER_SENSE_ERRORS:
    		case ENET_NP_STAT_DOT3_FRAME_TOO_LONGS:
    		case ENET_NP_STAT_DOT3_INTERNAL_MAC_RX_ERRORS:
    		case ENET_NP_STAT_DOT3_SYMBOL_ERRORS:
    		case ENET_NP_STAT_DOT3_CONTROL_IN_UNK_OPCODES:
    		case ENET_NP_STAT_DOT3_IN_PAUSE_FRAMES:
    		case ENET_NP_STAT_DOT3_OUT_PAUSE_FRAMES:
    		{
    			Value.val = 0;
    		}
    		break;

    		case ENET_NP_STAT_IF_HC_IN_OCTETS:
    		{
    			Value.val = 0;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_IN_UCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_UC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_IN_MCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_IN_BCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_BC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_OUT_OCTETS:
    		{
    			Value.val = MEA_Stats.Tx_Bytes.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_OUT_UCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_UC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_OUT_MCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_HC_OUT_BCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_BC_Pkts.val;
    		}
    		break;

    		case ENET_NP_STAT_IPMC_BRIDGED_PKTS:
    		case ENET_NP_STAT_IPMC_ROUTED_PKTS:
    		case ENET_NP_STAT_IPMC_IN_DROPPED_PKTS:
    		case ENET_NP_STAT_IPMC_OUT_DROPPED_PKTS:
    		case ENET_NP_STAT_PKTS_1519_TO_1522_OCTETS:
    		case ENET_NP_STAT_PKTS_1522_TO_2047_OCTETS:
    		case ENET_NP_STAT_PKTS_2048_TO_4095_OCTETS:
    		case ENET_NP_STAT_PKTS_4095_TO_9216_OCTETS:
    		{
    			Value.val = 0;
    		}
    		break;


    		case ENET_NP_STAT_IF_IN_MCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_IN_BCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Rx_BC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_MCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_MC_Pkts.val;
    		}
    		break;
    		case ENET_NP_STAT_IF_OUT_BCAST_PKTS:
    		{
    			Value.val = MEA_Stats.Tx_BC_Pkts.val;
    		}
    		break;

    		case ENET_NP_STAT_DOT1D_PORT_LEARNED_ENTRY_DISCARDS:
    		case ENET_NP_STAT_DOT1D_PORT_BCAST_OUT_PKTS:
    		case ENET_NP_STAT_DOT1D_PORT_MCAST_OUT_PKTS:
    		case ENET_NP_STAT_VLAN_PORT_IN_DISCARDS:
    		case ENET_NP_STAT_VLAN_PORT_IN_FRAMES:
    		case ENET_NP_STAT_VLAN_PORT_OUT_FRAMES:
    		case ENET_NP_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW:
    		case ENET_NP_STAT_VLAN_PORT_OUT_OVERFLOW:
    		case ENET_NP_STAT_VLAN_PORT_IN_OVERFLOW:
    		case ENET_NP_STAT_IF_IN_BAD_OCTETS:
    		case ENET_NP_STAT_IF_IN_SYMBOLS:
    		case ENET_NP_STAT_VLAN_UCAST_IN_FRAMES:
    		case ENET_NP_STAT_VLAN_MCAST_BCAST_IN_FRAMES:
    		case ENET_NP_STAT_VLAN_UNKNOWN_UCAST_OUT_FRAMES:
    		case ENET_NP_STAT_VLAN_UCAST_OUT_FRAMES:
    		case ENET_NP_STAT_VLAN_BCAST_OUT_FRAMES:
    		case ENET_NP_STAT_IF_HC_IN_DISCARDS:
    		case ENET_NP_STAT_IF_HC_IN_ERRORS:
    		case ENET_NP_STAT_IF_HC_IN_UNKNOWN_PROTOS:
    		case ENET_NP_STAT_IF_HC_OUT_DISCARDS:
    		case ENET_NP_STAT_IF_HC_IN_OUT_ERRORS:
    		case ENET_NP_STAT_ETHER_OUT_FCS_ERRORS:
    		{
    			Value.val = 0;
    		}
    		break;
    		default:
    		{
    			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Unsupported statistic type \n");
    			return ENET_FAILURE;
    		}
    	}

    	pValue->lsn = Value.s.lsw;
    	pValue->msn = Value.s.msw;

    }

    return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_CfaNpGetLinkStatus (ADAP_Uint32 u4IfIndex)
{
	MEA_Port_t	PhyPort=0;
	MEA_Bool	linkStatus;

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex,&PhyPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
		return ENET_FAILURE;
	}

	MEA_API_Get_Interface_CurrentLinkStatus(MEA_UNIT_0, PhyPort, &linkStatus ,MEA_TRUE);

    return (linkStatus == 1) ? ADAP_PORT_LINK_UP : ADAP_PORT_LINK_DOWN;
}

ADAP_Uint8 EnetHal_CfaNpGetPhyAndLinkStatus (ADAP_Uint16 u2IfIndex)
{
	return EnetHal_CfaNpGetLinkStatus(u2IfIndex);
}

ADAP_Int32 EnetHal_FsCfaHwSetWanTye (tEnetHal_IfWanInfo * pCfaWanInfo)
{
	MEA_Port_t	PhyPort=0;
	ADAP_Uint16     vlanId=0;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," mac address:%s ifIndex:%d u1Opcode:%d\n",
											adap_mac_to_string(&pCfaWanInfo->MacAddr),
											pCfaWanInfo->u4IfIndex,
											pCfaWanInfo->u1Opcode);



	if((pCfaWanInfo->u4IfIndex > 0)
       && (pCfaWanInfo->u4IfIndex < MeaAdapGetNumOfPhyPorts()))
	{
		if(MeaAdapGetPhyPortFromLogPort(pCfaWanInfo->u4IfIndex,&PhyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return ENET_FAILURE;
		}
		MeaDrvHwSetInterfaceCfmOamUCMac (PhyPort,&pCfaWanInfo->MacAddr);
		MeaAdapSetIntType(pCfaWanInfo->u1Opcode,pCfaWanInfo->u4IfIndex);
		if(pCfaWanInfo->u1Opcode == ADAP_SET_TO_WAN)
		{
			vlanId = getPortToPvid(pCfaWanInfo->u4IfIndex);
			if(adap_ReconfigureHwVlan(vlanId,0) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call adap_ReconfigureHwVlan\r\n");
				return ENET_FAILURE;
			}
		}
		// reconfigure the VLAN configuration
	}


    return ENET_SUCCESS;
}

void EnetHal_CfaNpUpdateSwitchMac (tEnetHal_MacAddr *pSwitchMac)
{
	ADAP_Uint32 idx=0;
	MEA_Port_t PhyPort=0;
	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		if(MeaAdapGetPhyPortFromLogPort(idx,&PhyPort) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
			return;
		}
		MeaDrvHwSetInterfaceCfmOamUCMac(PhyPort,pSwitchMac);
	}

	MeaDrvSetGlobalMacAddress(pSwitchMac,MEA_TRUE);
//	if(MeaDrvHwSetGlobalUCMac((char *)pSwitchMac) != MEA_OK)
//	{
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwSetGlobalUCMac\r\n");
//		return;
//	}
}
