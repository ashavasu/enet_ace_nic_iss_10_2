/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#ifndef ADAP_LOGGER_H
#define ADAP_LOGGER_H

#include <syslog.h>
#include "adap_types.h"

#define ENET_ADAP_ALERT 		LOG_ALERT
#define ENET_ADAP_CRIT 			LOG_CRIT
#define ENET_ADAP_ERR 			LOG_ERR
#define ENET_ADAP_WARNING 		LOG_WARNING
#define ENET_ADAP_INFO 			LOG_INFO
#define ENET_ADAP_DEBUG 		LOG_DEBUG
#define	ENET_ADAP_NOTICE        LOG_NOTICE


#define	ENET_ADAP_NOTICE1        100



void DplPrintLog(char *fileName,const char *functionName,int line,int logLevel,char *fmt, ...);
#define ENET_ADAPTOR_PRINT_LOG(logLevel,fmt,...) DplPrintLog(__FILE__,__func__,__LINE__,logLevel,fmt,##__VA_ARGS__);
ADAP_Uint32 DplOpenSyslogApplication(int LogMask);
ADAP_Uint32 DplSetLogSyslogApplication(int LogMask);
ADAP_Uint32 DplCloseSyslogApplication(void);

int read_pring_log_level(void);
#endif
