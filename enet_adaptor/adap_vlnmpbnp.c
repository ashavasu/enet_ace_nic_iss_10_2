/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <stdio.h>
#include <stdarg.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_vlnmpbnp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_acl.h"
#include "adap_enetConvert.h"
#include "adap_vlanminp.h"
#include "EnetHal_L2_Api.h"

static ADAP_Uint8 priority7P1DEncodeBeTrue[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {0,1,2,3,4,4,6,7}; //yellow
static ADAP_Uint8 priority7P1DEncodeBeFalse[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {0,1,2,3,5,5,6,7}; //green
static ADAP_Uint8 priority6P2DEncodeBeTrue[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {0,1,2,2,4,4,6,7}; //yellow
static ADAP_Uint8 priority6P2DEncodeBeFalse[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {0,1,3,3,5,5,6,7}; //green
static ADAP_Uint8 priority5P3DEncodeBeTrue[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {0,0,2,2,4,4,6,7}; //yellow
static ADAP_Uint8 priority5P3DEncodeBeFalse[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY] = {1,1,3,3,5,5,6,7}; //green

static ADAP_Int32 MeaDrvSetVlanPriorityConfig(tEnetHal_VlanId primaryVlan,ADAP_Uint32 u4IfIndex,ADAP_Int32 bLearning,ADAP_Uint16 seconderyVlan,eAdap_EditPriorityTag editTag)
{
	tAdap_PortConfigSetting  portArray[MAX_NUM_OF_SUPPORTED_PORTS+1];
	ADAP_Uint8			iss_port;
	ADAP_Uint16			index=0;
	ADAP_Uint32 		modeType=0;

	adap_memset(&portArray,0,sizeof(tAdap_PortConfigSetting)*(MAX_NUM_OF_SUPPORTED_PORTS+1));

	for(iss_port=1;iss_port<MAX_NUM_OF_SUPPORTED_PORTS;iss_port++)
	{
		if( (iss_port < MeaAdapGetNumOfPhyPorts() ) && (adap_isPortPartOfLag(iss_port) == ENET_SUCCESS) )
		{
			continue;
		}
		if(adap_VlanCheckUntaggedPort(primaryVlan,iss_port) == ADAP_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVlanPriorityConfig: untagged iss port :%d\n", iss_port);
			portArray[index].port_defined=1;
			portArray[index].iss_port_id = iss_port;
			index++;
		}
		else if(adap_VlanCheckTaggedPort(primaryVlan,iss_port) == ADAP_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MeaDrvSetVlanPriorityConfig: tagged iss port :%d\n", iss_port);
			portArray[index].port_defined=2;
			portArray[index].iss_port_id = iss_port;
			index++;
		}
	}

	if(index)
	{
		AdapGetProviderCoreBridgeMode(&modeType,u4IfIndex);
		if(modeType == ADAP_CUSTOMER_EDGE_PORT)
		{
			adap_ConfigPb(primaryVlan);
		}
		else
		{
			// remove any ports that belong to LAG
			for(iss_port=1;iss_port<MeaAdapGetNumOfPhyPorts();iss_port++)
			{
				if(adap_isPortPartOfLag(iss_port) == ENET_SUCCESS)
				{
					MeaDrvDelEntryDeleteSrv (iss_port,0,MEA_PARSING_L2_KEY_Ctag);
				}
			}
			if(adap_VlanHwUpdatePriorityVlan(primaryVlan, u4IfIndex, &portArray[0], editTag,bLearning,seconderyVlan) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetVlanPriorityConfig: adap_VlanHwUpdatePriorityVlan failed for PrimaryVlan:%d\n", primaryVlan);
				return ENET_FAILURE;
			}
		}
	}
	return ENET_SUCCESS;
}

static ADAP_Uint32 MeaDrvSetPBPortCustomerVlan(ADAP_Uint32 PortId, ADAP_Uint16 CustomerVlan)
{
	ADAP_Uint16 sVlan=0;
	tEnetHal_VlanSVlanMap *VlanSVlanMap = NULL;

	if(adap_GetPBPortCustomerVlan(PortId) != CustomerVlan)
	{
		// check if need to remove the old one
		if(adap_GetPBPortServVlanCustomerVlan(PortId) != ADAP_END_OF_TABLE)
		{
			MeaDrvDelEntryDeleteSrv (PortId, adap_GetPBPortCustomerVlan(PortId), MEA_PARSING_L2_KEY_Stag_Ctag);
			MeaDrvSetVlanPriorityConfig(0,PortId,ENET_VLAN_ENABLED,0,ENET_EditVlanPriorityTagged);
		}

		adap_SetPBPortServVlanCustomerVlan(PortId, ADAP_END_OF_TABLE);

		if(CustomerVlan == ADAP_END_OF_TABLE)
		{
			return ENET_SUCCESS;
		}

		//search for service vlan for this customer if need to create configuration
		for(sVlan=2;sVlan<ADAP_NUM_OF_SUPPORTED_VLANS;sVlan++)
		{
			if(MeaAdapGetServiceIdByVlanIfIndex(sVlan, PortId, MEA_PARSING_L2_KEY_Stag_Ctag) != NULL)
			{
				VlanSVlanMap = adap_GetPointerSVlanClassTable(PortId, sVlan);

				if(VlanSVlanMap != NULL)
				{
					if(VlanSVlanMap->CVlanId == CustomerVlan)
					{
						adap_SetPBPortServVlanCustomerVlan(PortId,sVlan);
						if(EnetHal_FsMiVlanHwSetVlanMemberPort(0,sVlan,PortId, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MeaDrvSetPBPortCustomerVlan ERROR: failed to create service\n");
							return ENET_FAILURE;
						}
						if(VlanSVlanMap->u1PepUntag == 1)
						{
							MeaDrvSetVlanPriorityConfig(sVlan,PortId,ENET_VLAN_DISABLED,VlanSVlanMap->CVlanId,ENET_EditVlanPrioritySwapOuter);
						}
						else
						{
							MeaDrvSetVlanPriorityConfig(sVlan,PortId,ENET_VLAN_DISABLED,VlanSVlanMap->CVlanId,ENET_EditVlanPrioritySwapInnerAddOuter);
						}
						break;
					}
				}
			}
		}
	}

	adap_SetPBPortCustomerVlan(PortId, CustomerVlan);

 	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetProviderBridgePortType (ADAP_Uint32 u4ContextId,ADAP_Uint32 u4IfIndex,ADAP_Uint32 u4PortType)
{
	ADAP_UNUSED_PARAM (u4ContextId);
    MEA_Port_t EnetPort;
    MEA_IngressPort_Entry_dbt entry;
    ADAP_Uint16        IssMasterPortId;
	ADAP_Uint32 u4OldPortType;

	if(u4IfIndex >= MAX_NUM_OF_SUPPORTED_PORTS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is out of index\n", u4IfIndex);
		return ENET_SUCCESS;
	}
	IssMasterPortId = u4IfIndex;
	if(adap_IsPortChannel(IssMasterPortId) == ENET_SUCCESS)
	{
		if(adap_NumOfLagPortFromAggrId(IssMasterPortId) == 0)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"IfIndex:%d is not port channel\n", u4IfIndex);
			return ENET_SUCCESS;
		}
		IssMasterPortId = adap_GetLagMasterPort(u4IfIndex);
	}

	if(MeaAdapGetPhyPortFromLogPort(u4IfIndex,&EnetPort) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get phyport from ifIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}

	MeaDrvProviderStpHwLxcpUpdate (u4IfIndex, MEA_FALSE);

	AdapGetProviderCoreBridgeMode(&u4OldPortType,u4IfIndex);

    switch(u4PortType)
    {
    	case ADAP_PROVIDER_NETWORK_PORT:
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Port:%d configured as %s\n",u4IfIndex,"providerNetworkPort");
    		MeaDrvSetIngressPortPriRule(0 ,u4IfIndex);
    		MeaDrvProviderStpHwLxcpUpdate (u4IfIndex, MEA_TRUE);
    		break;
    	case ADAP_CUSTOMER_EDGE_PORT:
     	case ADAP_CNP_PORTBASED_PORT:
     	case ADAP_CUSTOMER_BRIDGE_PORT:
    		break;
    	default:
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"Port:%d configured as unknown state %d\n",u4IfIndex,u4PortType);
    		break;
    }

     if(AdapSetProviderCoreBridgeMode(u4PortType,u4IfIndex) != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProviderBridgePortType ERROR: DPLSetPBPortType failed for Port:%d PortType:%d\n", u4IfIndex, u4PortType);
        return ENET_FAILURE;
    }

     if (ENET_ADAPTOR_Get_IngressPort_Entry(MEA_UNIT_0, EnetPort, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProviderBridgePortType ERROR: ENET_ADAPTOR_Get_IngressPort_Entry FAIL\n");
        return ENET_FAILURE;
    }

    if(u4PortType == ADAP_PROVIDER_NETWORK_PORT)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwSetProviderBridgePortType delete priority\n");
    	MeaDrvDelEntryDeleteSrv (u4IfIndex,0,MEA_PARSING_L2_KEY_Ctag);
    }
    else if(u4OldPortType == ADAP_PROVIDER_NETWORK_PORT)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwSetProviderBridgePortType create priority\n");
    	MeaDrvSetVlanPriorityConfig(0,u4IfIndex,MEA_TRUE,0,ENET_EditVlanPriorityTagged);

    }

	entry.parser_info.evc_external_internal = MEA_FALSE;
	entry.parser_info.net_wildcard_valid = MEA_FALSE;
	entry.parser_info.net_wildcard = 0x0;

    if (ENET_ADAPTOR_Set_IngressPort_Entry(MEA_UNIT_0, EnetPort, &entry) != MEA_OK)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetProviderBridgePortType ERROR: ENET_ADAPTOR_Set_IngressPort_Entry FAIL\n");
        return ENET_FAILURE;
    }

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortSVlanTranslationStatus (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1Status)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u1Status);
    return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwAddSVlanTranslationEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan)
{
	ADAP_UNUSED_PARAM (u4ContextId);
    tSvlanRelayDb 		*pRelay=NULL;
    ADAP_Uint32			issPort=0;

    if( (u2LocalSVlan != 0) && (u2RelaySVlan != 0) )
    {
		pRelay = adap_getServiceVlanRelay(u4IfIndex);

		if(pRelay != NULL)
		{
			if( (pRelay->u2LocalSVlan == u2LocalSVlan) && (pRelay->u2RelaySVlan == u2RelaySVlan) )
			{
				// already confogiured
				return ENET_SUCCESS;
			}
			for(issPort=1;issPort<MAX_NUM_OF_SUPPORTED_PORTS;issPort++)
			{
				if( (adap_VlanCheckUntaggedPort(u2RelaySVlan,issPort) > 0) || (adap_VlanCheckTaggedPort(u2RelaySVlan,issPort) > 0) )
				{
					if(u4IfIndex == issPort)
					{
						MeaDrvCreateSVlanTranslationEntry(issPort,MEA_TRUE,u2RelaySVlan,u2LocalSVlan);
					}
					else
					{
						MeaDrvCreateSVlanTranslationEntry(issPort,MEA_FALSE,u2RelaySVlan,u2LocalSVlan);
					}
				}

			}
		}

		adap_setServiceVlanRelay(u4IfIndex, u2LocalSVlan, u2RelaySVlan);
    }

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwDelSVlanTranslationEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint16 u2LocalSVlan, ADAP_Uint16 u2RelaySVlan)
{
	ADAP_UNUSED_PARAM (u4ContextId);
    tSvlanRelayDb 							*pRelay=NULL;
    tEnetHal_HwPortArray				  	tHwEgressPorts;
    tEnetHal_HwPortArray				  	tHwUnTagPorts;
    ADAP_Uint32								egressTagPort[MAX_NUM_OF_SUPPORTED_PORTS+1];
    ADAP_Uint32								egressUntagPort[MAX_NUM_OF_SUPPORTED_PORTS+1];
    ADAP_Uint32								Index;
    ADAP_Uint32								issPort;

	tHwEgressPorts.pu4PortArray = &egressTagPort[0];
	tHwEgressPorts.i4Length=0;
	tHwUnTagPorts.pu4PortArray = &egressUntagPort[0];
	tHwUnTagPorts.i4Length=0;

    if( (u2LocalSVlan != 0) && (u2RelaySVlan != 0) )
    {
    	pRelay = adap_getServiceVlanRelay(u4IfIndex);

    	if(pRelay != NULL)
    	{
    		if(pRelay->u2RelaySVlan != u2RelaySVlan)
    		{
    			 return ENET_SUCCESS;
    		}

    		adap_setServiceVlanRelay(u4IfIndex,ADAP_END_OF_TABLE,ADAP_END_OF_TABLE);

		    Index=0;
		    for(Index = 1; Index < MAX_NUM_OF_SUPPORTED_PORTS;Index++)
			{
		    	if(Index == u4IfIndex)
		    	{
		    		continue;
		    	}
				issPort = adap_VlanCheckUntaggedPort(u2RelaySVlan,Index);
				if(issPort)
				{
					tHwUnTagPorts.pu4PortArray[tHwUnTagPorts.i4Length++] = Index;
				}
				issPort = adap_VlanCheckTaggedPort(u2RelaySVlan,Index);
				if(issPort)
				{
					tHwEgressPorts.pu4PortArray[tHwEgressPorts.i4Length++] = Index;
				}
			}
		    if(tHwEgressPorts.i4Length > 0)
		    {
				 if(EnetHal_FsMiVlanHwAddVlanEntry (u4ContextId,u2RelaySVlan,&tHwEgressPorts,&tHwUnTagPorts) != ENET_SUCCESS)
				 {
					 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call EnetHal_FsMiVlanHwAddVlanEntry\r\n");
					 return ENET_FAILURE;
				 }
		    }
		}
    }

	return ENET_SUCCESS;
}

static ADAP_Int32 adap_VlanHwSetSVlanMap (ADAP_Uint32 u4ContextId,tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	ADAP_Uint16     idxPort;
	tAdap_PortConfigSetting   portArray[MAX_NUM_OF_SUPPORTED_PORTS+1];
	ADAP_Uint16		index=0;
	ADAP_Uint8		iss_port;
	ADAP_Uint32		port_type=0;
	ADAP_Uint16		masterPort;
	ADAP_Uint16		SvlanMapIndex;
	ADAP_Uint8		u1PepUntag;
	ADAP_Uint8		u1CepUntag;
	ADAP_Uint16     cVlan;
	ADAP_Uint32		CurrentAgingTime=0;
	ADAP_Uint16     ProfileId=MEA_PLAT_GENERATE_NEW_ID;
	tServiceDb 		*pServiceId=NULL;

	adap_memset(&portArray,0,sizeof(tAdap_PortConfigSetting)*(MAX_NUM_OF_SUPPORTED_PORTS+1));

	for(iss_port=1;iss_port<=MAX_NUM_OF_SUPPORTED_PORTS;iss_port++)
	{
		if(adap_VlanCheckUntaggedPort(pVlanSVlanMap->SVlanId,iss_port) > 0)
		{
			masterPort = iss_port;
			if(adap_IsPortChannel(iss_port) == ENET_SUCCESS)
			{
				if(adap_NumOfLagPortFromAggrId(iss_port) > 0)
				{
					masterPort = adap_GetLagMasterPort(iss_port);
				}
			}
			portArray[index].port_defined=1;
			portArray[index].iss_port_id = masterPort;
			index++;
		}
		else if(adap_VlanCheckTaggedPort(pVlanSVlanMap->SVlanId,iss_port) > 0)
		{
			masterPort = iss_port;
			if(adap_IsPortChannel(iss_port) == ENET_SUCCESS)
			{
				if(adap_NumOfLagPortFromAggrId(iss_port) > 0)
				{
					masterPort = adap_GetLagMasterPort(iss_port);
				}
			}
			portArray[index].port_defined=2;
			portArray[index].iss_port_id = masterPort;
			index++;
		}
	}
	//need to delete and create new
	if(index > 1)
	{
		for(idxPort=0;idxPort<index;idxPort++)
		{
			if(portArray[idxPort].port_defined == 1)
			{
				SvlanMapIndex = adap_SVlanClassTableGetFirst(portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"get first svlan map index:%d issPort:%d\n",SvlanMapIndex,pVlanSVlanMap->u2Port);
				while(SvlanMapIndex != ADAP_END_OF_TABLE)
				{
					cVlan = adap_SVlanClassTableGetFromIndex(portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,&u1PepUntag,&u1CepUntag,SvlanMapIndex);
					printf("adap_VlanHwSetSVlanMap port:%d sVlan:%d cVlan:%d\n",portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,cVlan);
					MeaDrvDelEntryDeleteSrv (portArray[idxPort].iss_port_id,cVlan,MEA_PARSING_L2_KEY_Ctag);
					MeaDrvDelEntryDeleteSrv (portArray[idxPort].iss_port_id,cVlan,MEA_PARSING_L2_KEY_Stag_Ctag);
					if(EnetHal_FsMiVlanHwSetVlanMemberPort(u4ContextId,pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to create service\n");
						return ENET_FAILURE;
					}

					if(adap_GetFlowMarkingProfile(portArray[idxPort].iss_port_id, &ProfileId) == ENET_FAILURE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to get flowMarkProfile from port:%d\n", portArray[idxPort].iss_port_id);
						return ENET_FAILURE;
					}

					if(ProfileId != MEA_PLAT_GENERATE_NEW_ID)
					{
						pServiceId = MeaAdapGetServiceIdByEditType(MEA_PARSING_L2_KEY_Stag_Ctag, portArray[idxPort].iss_port_id);

				   		if (pServiceId->serviceId != MEA_PLAT_GENERATE_NEW_ID)
				    		{
								if(MeaDrvSetFlowMarkingProfile (portArray[idxPort].iss_port_id, pServiceId->serviceId, ProfileId) != MEA_OK)
								{
									ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to set flowMarkProfile:%d to service:%d\r\n", ProfileId, pServiceId->serviceId);
									return ENET_FAILURE;
								}

								if(adap_SetFlowMarkingProfile(portArray[idxPort].iss_port_id, ProfileId) == ENET_FAILURE)
								{
									ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to set flowMarkProfile:%d to port:%d\n", ProfileId, portArray[idxPort].iss_port_id);
									return ENET_FAILURE;
								}
				    		}

					}
					
					MeaDrvDelEntryDeleteSrv (pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, MEA_PARSING_L2_KEY_Stag_Ctag);
					adap_ReconfigureHwVlan (pVlanSVlanMap->SVlanId,u4ContextId);
					SvlanMapIndex = adap_SVlanClassTableGetNext(portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,SvlanMapIndex);
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"get next svlan map index:%d\n",SvlanMapIndex);
				}
			}
			else if(portArray[idxPort].port_defined == 2)
			{
				AdapGetProviderCoreBridgeMode(&port_type,portArray[idxPort].iss_port_id);

				SvlanMapIndex = adap_SVlanClassTableGetFirst(pVlanSVlanMap->u2Port,pVlanSVlanMap->SVlanId);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"get first svlan map index:%d issPort:%d\n",SvlanMapIndex,pVlanSVlanMap->u2Port);
				while(SvlanMapIndex != ADAP_END_OF_TABLE)
				{
					cVlan = adap_SVlanClassTableGetFromIndex(pVlanSVlanMap->u2Port,pVlanSVlanMap->SVlanId,&u1PepUntag,&u1CepUntag,SvlanMapIndex);
					printf("adap_VlanHwSetSVlanMap port:%d sVlan:%d cVlan:%d\n",portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,cVlan);
					if( (u1CepUntag == 0) && (u1PepUntag == 0) && ( (port_type == ADAP_PROVIDER_NETWORK_PORT)  || (port_type == ADAP_CNP_CTAGGED_PORT) ) )
					{
						 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"===FsMiVlanHwSetSVlanMap tag should not created index:%d cvlan:%d\n",SvlanMapIndex,cVlan);
						MeaDrvDelEntryDeleteSrv (portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,MEA_PARSING_L2_KEY_Ctag);
						if(EnetHal_FsMiVlanHwSetVlanMemberPort(u4ContextId,pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to create service\n");
							return ENET_FAILURE;
						}

						if(port_type == ADAP_CNP_CTAGGED_PORT)
						{
							printf("tagged port_type:VLAN_CNP_TAGGED_PORT\r\n");
							if(EnetHal_FsMiVlanHwSetVlanMemberPort(0xFFFFFFFF,pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to create service\n");
								return ENET_FAILURE;
							}

						}
					}
					else
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"going to create SvlanMacvlan:%d\n",SvlanMapIndex,cVlan);
						MeaDrvDelEntryDeleteSrv (portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,MEA_PARSING_L2_KEY_Ctag);
						if(EnetHal_FsMiVlanHwSetVlanMemberPort(u4ContextId,pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to create service\n");
							return ENET_FAILURE;
						}
					}
					SvlanMapIndex = adap_SVlanClassTableGetNext(pVlanSVlanMap->u2Port,pVlanSVlanMap->SVlanId,SvlanMapIndex);
				}
				MeaDrvDelEntryDeleteSrv (portArray[idxPort].iss_port_id,pVlanSVlanMap->SVlanId,MEA_PARSING_L2_KEY_Stag_Ctag);
				if(EnetHal_FsMiVlanHwSetVlanMemberPort(u4ContextId,pVlanSVlanMap->SVlanId,portArray[idxPort].iss_port_id, VLAN_TAGGED_MEMBER_PORT) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"adap_VlanHwSetSVlanMap ERROR: failed to create service\n");
					return ENET_FAILURE;
				}
			}
		}

		CurrentAgingTime = adap_GetAgingTime();
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsBrgSetAgingTime reset\n");

	    if (MEA_API_Set_SE_Aging(MEA_UNIT_0, 1, MEA_TRUE) != MEA_OK)
	    {
	    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsBrgSetAgingTime failed to reset\n");
	    }

	    sleep(1);

		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"FsBrgSetAgingTime restore\n");
	    if (MEA_API_Set_SE_Aging(MEA_UNIT_0, CurrentAgingTime, MEA_TRUE) != MEA_OK)
	    {
	    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsBrgSetAgingTime failed to restore\n");
	    }
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwAddSVlanMap (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	tEnetHal_VlanSVlanMap *pNewVlanSVlanMap=NULL;
	tEnetHal_VlanSVlanMap *pVlanSVlanMapSelect;

	if(pVlanSVlanMap->u4TableType != ADAP_VLAN_SVLAN_PORT_CVLAN_TYPE)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddSVlanMap: Unsupported TableType:%d\n", pVlanSVlanMap->u4TableType);
		return ENET_FAILURE;
	}

	pNewVlanSVlanMap = adap_malloc(sizeof(tEnetHal_VlanSVlanMap));
	if(pNewVlanSVlanMap == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
		return ENET_FAILURE;
	}

	adap_memcpy(pNewVlanSVlanMap,pVlanSVlanMap,sizeof(tEnetHal_VlanSVlanMap));

	if(Adap_addSVlanMapListToLinkList(pNewVlanSVlanMap) != ENET_SUCCESS)
	{
		 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call Adap_addSVlanMapListToLinkList\r\n");
		 return ENET_FAILURE;
	}

	// in case of provider edge the cvlan is secondery vlan
	if(adap_SVlanClassTableUpdate(pVlanSVlanMap) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddSVlanMap: adap_SVlanClassTableUpdate failed for CVlan:%d SVlan:%d\n", pVlanSVlanMap->CVlanId, pVlanSVlanMap->SVlanId);
		return ENET_FAILURE;
	}

	pVlanSVlanMapSelect = adap_GetPointerSVlanClassTable(pVlanSVlanMap->u2Port,pVlanSVlanMap->SVlanId);
	if(pVlanSVlanMapSelect != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwAddSVlanMap: CvlanSelect:%d current c-vlan:%d\n",pVlanSVlanMapSelect->CVlanId,pVlanSVlanMap->SVlanId);
		if(pVlanSVlanMapSelect->CVlanId == pVlanSVlanMap->CVlanId)
		{
			adap_SetSvlanMap(pVlanSVlanMap->u2Port,pVlanSVlanMap);
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwAddSVlanMap: set port tunnel\n");
			//DPLSetAllPortTunnelConfig(VlanSVlanMap.u2Port,u4ContextId,&VlanSVlanMap);
		}
	}

	if(adap_VlanHwSetSVlanMap (u4ContextId,pVlanSVlanMap) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddSVlanMap: FsMiVlanHwSetSVlanMap failed for CVlan:%d SVlan:%d\n", pVlanSVlanMap->CVlanId, pVlanSVlanMap->SVlanId);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwDeleteSVlanMap (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tEnetHal_VlanSVlanMap *pFreeVlanSVlanMap=NULL;

	pFreeVlanSVlanMap  = Adap_freeSVlanMapListLinkList(pVlanSVlanMap);
	adap_safe_free(pFreeVlanSVlanMap);

    if(pVlanSVlanMap->u4TableType != ADAP_VLAN_SVLAN_PORT_CVLAN_TYPE)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwDeleteSVlanMap FAIL: Unsupported TableType:%d\n", pVlanSVlanMap->u4TableType);
        return ENET_FAILURE;
    }

    adap_SVlanClassTableDelete(pVlanSVlanMap);

	if(adap_VlanHwSetSVlanMap (u4ContextId,pVlanSVlanMap) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwAddSVlanMap: FsMiVlanHwSetSVlanMap failed for CVlan:%d SVlan:%d\n", pVlanSVlanMap->CVlanId, pVlanSVlanMap->SVlanId);
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortSVlanClassifyMethod (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,ADAP_Uint8 u1TableType)
{
	ADAP_UNUSED_PARAM (u4ContextId);

		switch(u1TableType)
		{
	    case ADAP_VLAN_SVLAN_PORT_SRCMAC_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_DSTMAC_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_DSCP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_SRCIP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_DSTIP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_SRCDSTIP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_CVLAN_TYPE:
			break;
	    case ADAP_VLAN_SVLAN_PORT_PVID_TYPE:
			break;
	    case ADAP_VLAN_PB_LOGICAL_PORT_ENTRY_TYPE:
			break;
	    case ADAP_VLAN_PB_PORT_ENTRY_TYPE:
			break;
		default:
			break;
		}

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortCustomerVlan (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId CVlanId)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    if(u4IfIndex<1 || u4IfIndex>MeaAdapGetNumOfPhyPorts())
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPortCustomerVlan ERROR: Invalid ISS Port ID:%d!\n", u4IfIndex);
        return ENET_FAILURE;
    }

    MeaDrvSetPBPortCustomerVlan(u4IfIndex, CVlanId);

	return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwResetPortCustomerVlan (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    if(u4IfIndex<1 || u4IfIndex>MeaAdapGetNumOfPhyPorts())
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPortCustomerVlan ERROR: Invalid ISS Port ID:%d!\n", u4IfIndex);
        return ENET_FAILURE;
    }

    MeaDrvSetPBPortCustomerVlan(u4IfIndex, ADAP_END_OF_TABLE);

    return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPepPvid (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, tEnetHal_VlanId SVlanId,tEnetHal_VlanId Pvid)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tPepPvid *pPepPvid=NULL;
	tPepPvid PepPvid;
	ADAP_Int32 ret=ENET_SUCCESS;
	tEnetHal_VlanSVlanMap *pVlanSVlanMap;
	tEnetHal_VlanSVlanMap VlanSVlanMapTemp;

	PepPvid.Pvid = Pvid;
	PepPvid.SVlanId = SVlanId;
	PepPvid.u4IfIndex = u4IfIndex;

	pPepPvid = Adap_getPepPvidListLinkList(&PepPvid);

	if(pPepPvid == NULL)
	{
		pPepPvid = adap_malloc(sizeof(tPepPvid));
		if(pPepPvid == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate memory\r\n");
			return ENET_FAILURE;
		}

		pPepPvid->Pvid = Pvid;
		pPepPvid->SVlanId = SVlanId;
		pPepPvid->u4IfIndex = u4IfIndex;

		ret = Adap_addPepPvidListToLinkList(pPepPvid);

		// check pep pvid selection
	}

	VlanSVlanMapTemp.CVlanId = Pvid;
	VlanSVlanMapTemp.SVlanId = SVlanId;
	VlanSVlanMapTemp.u2Port = u4IfIndex;

	pVlanSVlanMap = adap_GetPointerSVlanClassTable(u4IfIndex,SVlanId);
	if(pVlanSVlanMap != NULL)
	{
		// in case of provider edge the cvlan is secondery vlan
		if(adap_SVlanClassTableUpdate(&VlanSVlanMapTemp) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPepPvid: adap_SVlanClassTableUpdate failed for CVlan:%d SVlan:%d\n", VlanSVlanMapTemp.CVlanId, VlanSVlanMapTemp.SVlanId);
			return ENET_FAILURE;
		}

		if(adap_VlanHwSetSVlanMap (u4ContextId,pVlanSVlanMap) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPepPvid: adap_VlanHwSetSVlanMap failed for CVlan:%d SVlan:%d\n", pVlanSVlanMap->CVlanId, pVlanSVlanMap->SVlanId);
			return ENET_FAILURE;
		}
	}

	return ret;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetPepAccFrameType (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Uint8 u1AccepFrameType)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	tEnetHal_HwPortArray				  tHwEgressPorts;
    tEnetHal_HwPortArray				  tHwUnTagPorts;
    ADAP_Uint32							  egressTagPort[MAX_NUM_OF_SUPPORTED_PORTS+1];
    ADAP_Uint32							  egressUntagPort[MAX_NUM_OF_SUPPORTED_PORTS+1];
    ADAP_Uint16							  Index=0;
    ADAP_Uint16							  issPortExist=0;

	AdapSetAccepFrameTypeMode(u4IfIndex,SVlanId,u1AccepFrameType);

	tHwEgressPorts.pu4PortArray = &egressTagPort[0];
	tHwEgressPorts.i4Length=0;
	tHwUnTagPorts.pu4PortArray = &egressUntagPort[0];
	tHwUnTagPorts.i4Length=0;
	for(Index = 1; Index < MeaAdapGetNumOfPhyPorts();Index++)
	{
		issPortExist = adap_VlanCheckUntaggedPort(SVlanId,Index);
		if(issPortExist)
		{
			tHwUnTagPorts.pu4PortArray[tHwUnTagPorts.i4Length++] = Index;
		}
		issPortExist = adap_VlanCheckTaggedPort(SVlanId,Index);
		if(issPortExist)
		{
			tHwEgressPorts.pu4PortArray[tHwEgressPorts.i4Length++] = Index;
		}
	}
	if(tHwEgressPorts.i4Length > 0)
	{
		 if(EnetHal_FsMiVlanHwAddVlanEntry (u4ContextId,SVlanId,&tHwEgressPorts,&tHwUnTagPorts) != ENET_SUCCESS)
		 {
			 ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call EnetHal_FsMiVlanHwAddVlanEntry\r\n");
			 return ENET_FAILURE;
		 }
	}

	return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetPepDefUserPriority (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Int32 i4DefUsrPri)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	MEA_Service_Entry_Key_dbt               Service_key;
	MEA_Service_Entry_Data_dbt              Service_data;
	MEA_OutPorts_Entry_dbt                  Service_outPorts;
	MEA_Policer_Entry_dbt                   Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
	MEA_Bool                                valid;
	ADAP_Uint16								index=0;
	ADAP_Uint16								issPortIndex;
	ADAP_Uint32								port_type;
	tServiceDb 							    *pServiceInfo=NULL;
	tBridgeDomain 						    *pBridgeDomain=NULL;
	ADAP_Int32 								ret_check=ENET_SUCCESS;
	tServiceDb							    *pServiceAlloc=NULL;

	Adap_bridgeDomain_semLock();
	pBridgeDomain = Adap_getBridgeDomainByVlan(SVlanId,0);
	if(pBridgeDomain != NULL)
	{
		ret_check = Adap_getFirstServiceFromLinkList(&pServiceInfo,pBridgeDomain);

		while( (ret_check == ENET_SUCCESS) && (pServiceInfo != NULL) )
		{
			if( pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
			{
				issPortIndex = pServiceInfo->ifIndex;
				if(issPortIndex == u4IfIndex)
				{
					ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
					continue;
				}
				AdapGetProviderCoreBridgeMode(&port_type,issPortIndex);
				if( (port_type != ADAP_PROVIDER_NETWORK_PORT) || (port_type == ADAP_CNP_CTAGGED_PORT) )
				{
					ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
					continue;
				}

				if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,pServiceInfo->serviceId,&valid) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPepDefUserPriority Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", pServiceInfo->serviceId);
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsMiVlanHwSetPepDefUserPriority: serviceId:%d\n",pServiceInfo->serviceId);

				adap_memset(&Service_key,       0 , sizeof(Service_key));
				adap_memset(&Service_data,      0 , sizeof(Service_data ));
				adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
				adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
				adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

			  if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
					  pServiceInfo->serviceId,
										&Service_key,
										&Service_data,
										&Service_outPorts,
										&Service_policer,
										&Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", pServiceInfo->serviceId);
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}
				Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

			   if (Service_editing.ehp_info == NULL )
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority ERROR: FAIL to malloc ehp_info\n");
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}

				if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
										pServiceInfo->serviceId,
										&Service_key,
										&Service_data,
										&Service_outPorts, /* outPorts */
										&Service_policer, /* policer  */
										&Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",pServiceInfo->serviceId);
					MEA_OS_free(Service_editing.ehp_info);
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}

				MeaDeleteEnetServiceId(pServiceInfo->serviceId);

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",pServiceInfo->serviceId);
				Service_data.editId =0; /*create new Id*/


				for(index=0;index<Service_editing.num_of_entries;index++)
				{
					Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.pri = i4DefUsrPri;
					Service_editing.ehp_info[index].ehp_data.eth_info.stamp_color       = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_info.stamp_priority    = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_valid = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_loc   = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;
				}

				pServiceAlloc = MeaAdapAllocateServiceId();
				if(pServiceAlloc == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
				}

				pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

				if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
										   &Service_key,
										   &Service_data,
										   &Service_outPorts,
										   &Service_policer,
										   &Service_editing,
										   &pServiceAlloc->serviceId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority FAIL: Unable to create the new service %d\n",pServiceAlloc->serviceId);
					MEA_OS_free(Service_editing.ehp_info);
					Adap_bridgeDomain_semUnlock();
					return ENET_FAILURE;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create/set the new service %d\n",pServiceAlloc->serviceId);

				MEA_OS_free(Service_editing.ehp_info);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority: succeeded to set serviceId:%d\n",pServiceAlloc->serviceId);

				// save configuration
				MeaAdapGetPhyPortFromLogPort(issPortIndex, &pServiceAlloc->inPort);
				pServiceAlloc->ifIndex=issPortIndex;
				pServiceAlloc->outerVlan=SVlanId;
				pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
				pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
				pServiceAlloc->vpnId = Service_data.vpn;
			}
			ret_check = Adap_getNextServiceFromLinkList(&pServiceInfo,pBridgeDomain);
		}
	}
	Adap_bridgeDomain_semUnlock();

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetCvidUntagPep (ADAP_Uint32 u4ContextId, tEnetHal_VlanSVlanMap *pVlanSVlanMap)
{
    pVlanSVlanMap->u4TableType = ADAP_VLAN_SVLAN_PORT_CVLAN_TYPE;

    EnetHal_FsMiVlanHwAddSVlanMap(u4ContextId,pVlanSVlanMap);

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPcpEncodTbl (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_HwVlanPbPcpInfo *pNpPbVlanPcpInfo)
{
	ADAP_UNUSED_PARAM (u4ContextId);

    tPbPcpInfoPri 							*pIngressPcpPri=NULL;
#if 0
    tServiceDb 							    *pServiceInfo=NULL;
	tServiceDb							 	*pServiceAlloc=NULL;
	MEA_Bool								valid;
	MEA_Service_Entry_Key_dbt               Service_key;
	MEA_Service_Entry_Data_dbt              Service_data;
	MEA_OutPorts_Entry_dbt                  Service_outPorts;
	MEA_Policer_Entry_dbt                   Service_policer;
	MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
	ADAP_Uint32								index;
#endif

     pIngressPcpPri = adap_GetIngressPcpInfo(u4IfIndex);

     if(pIngressPcpPri != NULL)
     {
		if(pNpPbVlanPcpInfo->u2Priority < ADAP_MAX_NUMBER_OF_VLAN_PRIORITY)
		{
			pIngressPcpPri->pcpInfo[pNpPbVlanPcpInfo->u2Priority].u1DropEligible = pNpPbVlanPcpInfo->u1DropEligible;
			pIngressPcpPri->pcpInfo[pNpPbVlanPcpInfo->u2Priority].u2PcpSelRow = pNpPbVlanPcpInfo->u2PcpSelRow;
			pIngressPcpPri->pcpInfo[pNpPbVlanPcpInfo->u2Priority].u2PcpValue = pNpPbVlanPcpInfo->u2PcpValue;
			pIngressPcpPri->pcpInfo[pNpPbVlanPcpInfo->u2Priority].valid = MEA_TRUE;

			// check if need to create service
#if 0
			pServiceInfo = MeaAdapGetServiceIdByPri(u4IfIndex, pNpPbVlanPcpInfo->u2PcpValue);
			if(pServiceInfo != NULL)
			{
				if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,pServiceInfo->serviceId,&valid) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPcpEncodTbl Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", pServiceInfo->serviceId);
					return ENET_FAILURE;
				}
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"EnetHal_FsMiVlanHwSetPcpEncodTbl: serviceId:%d\n",pServiceInfo->serviceId);

				adap_memset(&Service_key,       0 , sizeof(Service_key));
				adap_memset(&Service_data,      0 , sizeof(Service_data ));
				adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
				adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
				adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));

			    if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
										pServiceInfo->serviceId,
										&Service_key,
										&Service_data,
										&Service_outPorts,
										&Service_policer,
										&Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPcpEncodTbl Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", pServiceInfo->serviceId);
					return ENET_FAILURE;
				}
				Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

			   if (Service_editing.ehp_info == NULL )
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetPcpEncodTbl ERROR: FAIL to malloc ehp_info\n");
					return ENET_FAILURE;
				}

				if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
										pServiceInfo->serviceId,
										&Service_key,
										&Service_data,
										&Service_outPorts, /* outPorts */
										&Service_policer, /* policer  */
										&Service_editing) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",pServiceInfo->serviceId);
					MEA_OS_free(Service_editing.ehp_info);
					return ENET_FAILURE;
				}

				if(pIngressPcpPri->pcpInfo[pNpPbVlanPcpInfo->u2Priority].valid == MEA_TRUE)
				{
					MeaDeleteEnetServiceId(pServiceInfo->serviceId);
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"delete service %d\n",pServiceInfo->serviceId);
				Service_data.editId =0; /*create new Id*/


				for(index=0;index<Service_editing.num_of_entries;index++)
				{
					Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.pri = pNpPbVlanPcpInfo->u2PcpValue;
					Service_editing.ehp_info[index].ehp_data.eth_info.stamp_color       = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_info.stamp_priority    = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_valid = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_loc   = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_FALSE;
					Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;
				}

				pServiceAlloc = MeaAdapAllocateServiceId();
				if(pServiceAlloc == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
				}

				pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

				if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
										   &Service_key,
										   &Service_data,
										   &Service_outPorts,
										   &Service_policer,
										   &Service_editing,
										   &pServiceAlloc->serviceId) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority FAIL: Unable to create the new service %d\n",pServiceAlloc->serviceId);
					MEA_OS_free(Service_editing.ehp_info);
					return ENET_FAILURE;
				}

				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create/set the new service %d\n",pServiceAlloc->serviceId);

				MEA_OS_free(Service_editing.ehp_info);
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetPepDefUserPriority: succeeded to set serviceId:%d\n",pServiceAlloc->serviceId);

				// save configuration
				MeaAdapGetPhyPortFromLogPort(u4IfIndex, &pServiceAlloc->inPort);
				pServiceAlloc->ifIndex=u4IfIndex;
				pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
				pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
				pServiceAlloc->enet_IncPriority = Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.pri;
				pServiceAlloc->vpnId = Service_data.vpn;
			}
#endif
		}
    }

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetPortUseDei (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex, ADAP_Uint8 u1UseDei)
{
	ADAP_UNUSED_PARAM (u4ContextId);
    tPcpConfiguration *pPcpConfig=NULL;
    ADAP_Uint32								index=0;
	MEA_EditingMappingProfile_Entry_dbt 	mappingEntry;

    adap_memset(&mappingEntry,0,sizeof(MEA_EditingMappingProfile_Entry_dbt));

    pPcpConfig = adap_GetPcpConfiguration(u4IfIndex);

    if(pPcpConfig != NULL)
    {
    	if( (pPcpConfig->u1UseDei != u1UseDei) && (pPcpConfig->u2PcpSelection != ADAP_VLAN_8P0D_SEL_ROW) )
		{
    		if(pPcpConfig->ProfileId != MEA_PLAT_GENERATE_NEW_ID)
    		{
				if(MEA_API_Get_EditingMappingProfile_Entry(MEA_UNIT_0,pPcpConfig->ProfileId,
															&mappingEntry) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_FlowMarkingMappingProfile_Entry failed\n");
					return ENET_FAILURE;
				}

       			if(pPcpConfig->u2PcpSelection == ADAP_VLAN_7P1D_SEL_ROW)
        		{
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=priority7P1DEncodeBeFalse[index];
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=ADAP_PCP_GREEN;
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=priority7P1DEncodeBeTrue[index];
					if(u1UseDei == ADAP_VLAN_SNMP_TRUE)
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_YELLOW;
					}
					else
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_GREEN;
					}
        		}
        		else if(pPcpConfig->u2PcpSelection == ADAP_VLAN_6P2D_SEL_ROW)
        		{
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=priority6P2DEncodeBeFalse[index];
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=ADAP_PCP_GREEN;
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=priority6P2DEncodeBeTrue[index];
					if(u1UseDei == ADAP_VLAN_SNMP_TRUE)
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_YELLOW;
        			}
					else
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_GREEN;
					}
        		}
        		else if(pPcpConfig->u2PcpSelection == ADAP_VLAN_5P3D_SEL_ROW)
        		{
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_priority_value=priority5P3DEncodeBeFalse[index];
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_GREEN].stamp_color_value=ADAP_PCP_GREEN;
					mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_priority_value=priority5P3DEncodeBeTrue[index];
					if(u1UseDei == ADAP_VLAN_SNMP_TRUE)
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_YELLOW;
					}
					else
					{
						mappingEntry.item_table[index].colorItem_table[MEA_EDITING_MAPPING_PROFILE_COLOR_ITEM_ID_YELLOW].stamp_color_value=ADAP_PCP_GREEN;
					}
        		}

    			if(MEA_API_Set_EditingMappingProfile_Entry(MEA_UNIT_0,pPcpConfig->ProfileId,
    						   &mappingEntry) != MEA_OK)
    			{
    				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_FlowMarkingMappingProfile_Entry failed\n");
    				return ENET_FAILURE;
    			}
    		}
		}
    	pPcpConfig->u1UseDei = u1UseDei;
    }

	return ENET_SUCCESS;
}

ADAP_Int32 EnetHal_FsMiVlanHwSetServicePriRegenEntry (ADAP_Uint32 u4ContextId, ADAP_Uint32 u4IfIndex,tEnetHal_VlanId SVlanId, ADAP_Int32 i4RecvPriority,ADAP_Int32 i4RegenPriority)
{
	ADAP_UNUSED_PARAM (u4ContextId);

	MEA_Service_t							serviceId;
	MEA_Service_Entry_Key_dbt               Service_key;
    MEA_Service_Entry_Data_dbt              Service_data;
    MEA_OutPorts_Entry_dbt                  Service_outPorts;
    MEA_Policer_Entry_dbt                   Service_policer;
    MEA_EgressHeaderProc_Array_Entry_dbt    Service_editing;
	 MEA_Bool                               valid;
	 ADAP_Uint16							index=0;
	 ADAP_Uint16							CustomerVlanId;
	 MEA_Policer_Entry_dbt					Action_policer;
	 MEA_EgressHeaderProc_Array_Entry_dbt	Action_editing;
	 MEA_EHP_Info_dbt						Action_editing_info;
	 MEA_Action_t							Action_id;
	 MEA_Action_Entry_Data_dbt              Action_data;
	 MEA_OutPorts_Entry_dbt					Action_outPorts;
	 MEA_Bool								useMarkingProfile=MEA_TRUE;
	 ADAP_Uint8								priority[ADAP_MAX_NUMBER_OF_VLAN_PRIORITY];
	 ADAP_Uint16                            ProfileId=MEA_PLAT_GENERATE_NEW_ID;
    MEA_FlowMarkingMappingProfile_Entry_dbt FlowMarkingMappingProfileEntry;
	tServiceDb 							    *pServiceInfo=NULL;
	tEnetHal_VlanSVlanMap 					*pVlanSVlanMap = NULL;
	   tServiceDb							  *pServiceAlloc=NULL;

    adap_memset(&FlowMarkingMappingProfileEntry,0,sizeof(MEA_FlowMarkingMappingProfile_Entry_dbt));

	for(index=0;index<ADAP_MAX_NUMBER_OF_VLAN_PRIORITY;index++)
	{
		priority[index] = index;
	}
	priority[i4RecvPriority] = i4RegenPriority;
	index=0;

	if(useMarkingProfile == MEA_TRUE)
	{
		if(adap_GetFlowMarkingProfile(u4IfIndex, &ProfileId) == ENET_FAILURE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry ERROR: failed to get flowMarkProfile from port:%d\n", u4IfIndex);
			return ENET_FAILURE;
		}

		if(ProfileId == MEA_PLAT_GENERATE_NEW_ID)
		{
			// create one
			FlowMarkingMappingProfileEntry.type = MEA_FLOW_MARKING_MAPPING_PROFILE_TYPE_L2_PRI_OUTER_VLAN;
			for (index=0; index < 8 ; index++)
			{
				FlowMarkingMappingProfileEntry.item_table[index].valid=1;
				FlowMarkingMappingProfileEntry.item_table[index].L2_PRI = priority[index];
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"priority :%d regenPriority %d\n",
										index,FlowMarkingMappingProfileEntry.item_table[index].L2_PRI);
			}
			ProfileId = MEA_PLAT_GENERATE_NEW_ID;
			if(MEA_API_Create_FlowMarkingMappingProfile_Entry(MEA_UNIT_0, &FlowMarkingMappingProfileEntry, &ProfileId) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry MEA_API_Create_FlowMarkingMappingProfile_Entry failed\n");
				return ENET_FAILURE;
			}
			if(adap_SetFlowMarkingProfile(u4IfIndex, ProfileId) == ENET_FAILURE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry ERROR: failed to set flowMarkProfile:%d to port:%d\n", ProfileId, u4IfIndex);
				return ENET_FAILURE;
			}
		}
		else
		{
			if(MEA_API_Get_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,ProfileId,
														&FlowMarkingMappingProfileEntry) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Get_FlowMarkingMappingProfile_Entry failed\n");
				return ENET_FAILURE;
			}
			for (index=0; index < 8 ; index++)
			{
				if(i4RecvPriority == index)
				{
					FlowMarkingMappingProfileEntry.item_table[index].valid=1;
					FlowMarkingMappingProfileEntry.item_table[index].L2_PRI = i4RegenPriority;
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"priority :%d regenPriority %d\n",
											index,FlowMarkingMappingProfileEntry.item_table[index].L2_PRI);
				}
			}
			if(MEA_API_Set_FlowMarkingMappingProfile_Entry(MEA_UNIT_0,
														ProfileId,
														&FlowMarkingMappingProfileEntry) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MEA_API_Set_FlowMarkingMappingProfile_Entry failed\n");
				return ENET_FAILURE;
			}
		}
	}

	// get vlan
    pServiceInfo = MeaAdapGetFirstServicePort(u4IfIndex);
	while(pServiceInfo != NULL)
	{
		if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
		{
			if( (pServiceInfo->outerVlan == SVlanId) && (pServiceInfo->L2_protocol_type == MEA_PARSING_L2_KEY_Stag_Ctag))
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"EnetHal_FsMiVlanHwSetServicePriRegenEntry serviceId:%d assign priority profile:%d\n",pServiceInfo->serviceId,ProfileId);

				pVlanSVlanMap = adap_GetPointerSVlanClassTable(u4IfIndex,SVlanId);
				if(pVlanSVlanMap != NULL)
				{
					CustomerVlanId = pVlanSVlanMap->CVlanId;

					if(CustomerVlanId == ADAP_END_OF_TABLE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry error: customer vlan not exist for SVLAN:%d\n",SVlanId);
						return ENET_FAILURE;
					}

					adap_memset(&Service_key,       0 , sizeof(Service_key));
					adap_memset(&Service_data,      0 , sizeof(Service_data ));
					adap_memset(&Service_outPorts , 0 , sizeof(Service_outPorts ));
					adap_memset(&Service_policer  , 0 , sizeof(Service_policer ));
					adap_memset(&Service_editing  , 0 , sizeof(Service_editing ));
					adap_memset(&FlowMarkingMappingProfileEntry  , 0 , sizeof(MEA_FlowMarkingMappingProfile_Entry_dbt));

					/* check if the service exist */
					if(ENET_ADAPTOR_IsExist_Service_ById(MEA_UNIT_0,pServiceInfo->serviceId,&valid) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: MEA_API_IsExist_Service_ByiD for service Id %d failed\n", pServiceInfo->serviceId);
						return ENET_FAILURE;
					}
					if(valid == MEA_FALSE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: ServiceId %d does not exists\n", pServiceInfo->serviceId);
						return ENET_FAILURE;
					}

				   if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
											pServiceInfo->serviceId,
											&Service_key,
											&Service_data,
											&Service_outPorts,
											&Service_policer,
											&Service_editing) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: ENET_ADAPTOR_Get_Service FAIL for service:%d\n", pServiceInfo->serviceId);
						return ENET_FAILURE;
					}
					Service_editing.ehp_info = (MEA_EHP_Info_dbt*)adap_malloc(sizeof(MEA_EHP_Info_dbt) * Service_editing.num_of_entries);

				   if (Service_editing.ehp_info == NULL )
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry ERROR: FAIL to malloc ehp_info\n");
						return ENET_FAILURE;
					}

					if (ENET_ADAPTOR_Get_Service(MEA_UNIT_0,
											pServiceInfo->serviceId,
											&Service_key,
											&Service_data,
											&Service_outPorts, /* outPorts */
											&Service_policer, /* policer  */
											&Service_editing) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: ENET_ADAPTOR_Get_Service(2nd) FAIL for service %d failed\n",pServiceInfo->serviceId);
						MEA_OS_free(Service_editing.ehp_info);
						return ENET_FAILURE;
					}

					if(useMarkingProfile == MEA_TRUE)
					{
						if( (Service_data.flowMarkingMappingProfile_force == 1) && (Service_data.flowMarkingMappingProfile_id == ProfileId) )
						{
							MEA_OS_free(Service_editing.ehp_info);
							return ENET_SUCCESS;
						}

						Service_data.flowMarkingMappingProfile_force = 1;
						Service_data.flowMarkingMappingProfile_id = ProfileId;

						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"service Id:%d force:%d profileId:%d\n",
											pServiceInfo->serviceId,
											Service_data.flowMarkingMappingProfile_force,
											Service_data.flowMarkingMappingProfile_id);

						Service_data.editId = 0; /*create new Id*/
						if (ENET_ADAPTOR_Set_Service(MEA_UNIT_0,
												pServiceInfo->serviceId,
												&Service_data,
												&Service_outPorts,
												&Service_policer,
												&Service_editing) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry ERROR: ENET_ADAPTOR_Set_Service failed for service:%d\n",
									pServiceInfo->serviceId);
							MEA_OS_free(Service_editing.ehp_info);
							return ENET_FAILURE;
						}
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"set service:%d\n", pServiceInfo->serviceId);
						MEA_OS_free(Service_editing.ehp_info);
					}
					else
					{
						// create action
						if(Service_data.DSE_learning_enable==1)
						{
							Action_id = Service_data.DSE_learning_actionId;
							adap_memset(&Action_policer  , 0 , sizeof(Action_policer ));
							adap_memset(&Action_editing  , 0 , sizeof(Action_editing ));
							adap_memset(&Action_editing_info,0,sizeof(Action_editing_info));
							Action_editing.num_of_entries = 1;
							Action_editing.ehp_info = &Action_editing_info;

							if (ENET_ADAPTOR_Get_Action (MEA_UNIT_0,
												Action_id,
												&Action_data,
												&Action_outPorts,
												&Action_policer,
												&Action_editing)!= MEA_OK)
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: ENET_ADAPTOR_Get_Action FAIL for actionId %d failed\n",Action_id);
								MEA_OS_free(Service_editing.ehp_info);
								return ENET_FAILURE;
							}

							/* Create the reverse action */
							Action_id = MEA_PLAT_GENERATE_NEW_ID;
							if (ENET_ADAPTOR_Create_Action (MEA_UNIT_0,
													   &Action_data,
													   &Action_outPorts,
													   &Action_policer,
													   &Action_editing,
													   &Action_id) != MEA_OK)
							{
								ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetServicePriRegenEntry Error: ENET_ADAPTOR_Create_Action failed for action:%d\n", Action_id);
								MEA_OS_free(Service_editing.ehp_info);
								return ENET_FAILURE;
							}
						}
						Service_data.editId =0; /*create new Id*/

						Service_key.L2_protocol_type =MEA_PARSING_L2_KEY_Ctag; /* Ethertypes: 88a8/8100*/
						Service_key.net_tag          = CustomerVlanId;
						Service_key.net_tag         |= MEA_OR_NET_TAG_MASK_VLAN_DEF_VAL;
						Service_key.evc_enable = MEA_FALSE;
						Service_key.external_internal = MEA_FALSE;
						Service_key.priType = MEA_INGRESS_PORT_PARSER_INFO_IP_PRI_MASK_TYPE_NOIP;
						Service_key.pri = i4RecvPriority;

						serviceId=MEA_PLAT_GENERATE_NEW_ID;

						for(index=0;index<Service_editing.num_of_entries;index++)
						{
							Service_editing.ehp_info[index].ehp_data.eth_info.val.vlan.pri = i4RegenPriority;
							Service_editing.ehp_info[index].ehp_data.eth_info.stamp_color       = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_info.stamp_priority    = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_valid = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit0_loc   = 0;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_valid = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.color_bit1_loc   = 0;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_valid   = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit0_loc     = 0;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_valid   = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit1_loc     = 0;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_valid   = MEA_FALSE;
							Service_editing.ehp_info[index].ehp_data.eth_stamping_info.pri_bit2_loc     = 0;
						}

						pServiceAlloc = MeaAdapAllocateServiceId();
						if(pServiceAlloc == NULL)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate service\r\n");
						}

						pServiceAlloc->serviceId = MEA_PLAT_GENERATE_NEW_ID;

					   if (ENET_ADAPTOR_Create_Service(MEA_UNIT_0,
												   &Service_key,
												   &Service_data,
												   &Service_outPorts,
												   &Service_policer,
												   &Service_editing,
												   &pServiceAlloc->serviceId) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"FsMiVlanHwSetServicePriRegenEntry Error: Unable to create the new service %d in DPL\n",  serviceId);
							MEA_OS_free(Service_editing.ehp_info);
							return ENET_FAILURE;
						}

					   ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"create/set the new service %d\n",serviceId);

						MEA_OS_free(Service_editing.ehp_info);

						// save configuration
						pServiceAlloc->ifIndex=u4IfIndex;
						MeaAdapGetPhyPortFromLogPort(u4IfIndex, &pServiceAlloc->inPort);
						pServiceAlloc->outerVlan=SVlanId;
						pServiceAlloc->innerVlan=Service_key.inner_netTag_from_value;
						pServiceAlloc->L2_protocol_type=Service_key.L2_protocol_type;
						pServiceAlloc->vpnId = Service_data.vpn;
					}
				}
			}
		}
		pServiceInfo = MeaAdapGetNextServicePort(u4IfIndex, pServiceInfo->serviceId);
	}

	return ENET_SUCCESS;
}
ADAP_Int32 EnetHal_FsMiVlanHwSetTunnelMacAddress (ADAP_Uint32 u4ContextId, tEnetHal_MacAddr *MacAddr,ADAP_Uint16 u2Protocol)
{
	ADAP_UNUSED_PARAM (u4ContextId);
	ADAP_Uint16		issPort;
	tEnetHal_VlanSVlanMap   *pVlanMap;

	for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
	{
		if(adap_GetTunnelOption(issPort,u2Protocol) == ENET_VLAN_TUNNEL_PROTOCOL_TUNNEL)
		{
			pVlanMap = adap_GetSvlanMap(issPort);
			if(pVlanMap != NULL)
			{
				if(MeaDrvSetTunnelToNormalMode(issPort,u4ContextId,u2Protocol, MacAddr, pVlanMap,ENET_VLAN_TUNNEL_PROTOCOL_PEER) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelMacAddress failed to set protocol to nornal mode:%d\n",u2Protocol);
					return ENET_FAILURE;
				}
				if( (pVlanMap->CVlanId != ADAP_END_OF_TABLE) && (pVlanMap->SVlanId != ADAP_END_OF_TABLE) )
				{
					if(MeaDrvSetPortTunnelConfiguration(issPort,u2Protocol,u4ContextId,pVlanMap) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"EnetHal_FsMiVlanHwSetTunnelMacAddress failed to set protocol to tunnel mode:%d\n",u2Protocol);
						return ENET_FAILURE;
					}
					if(u2Protocol == ENETHAL_VLAN_NP_STP_PROTO_ID)
					{
						MeaDrvSetAllPortTunnelConfigBack(pVlanMap);
					}
				}
			}
		}
	}

	return ENET_SUCCESS;
}
