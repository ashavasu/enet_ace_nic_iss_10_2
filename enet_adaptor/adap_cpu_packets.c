/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include <sys/ioctl.h>
//#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "mea_api.h"

#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <linux/if_tun.h>
#include <unistd.h>
#include <pthread.h>

#include "adap_types.h"
#include "mea_api.h"
#include "adap_logger.h"
#include "adap_cfanp.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_ipnp.h"
#include "adap_lpm.h"
#include "mea_drv_common.h"
#include "enetadaptor.h"

//#define ETH_NAME "ens1"
#define  MAX_PACKET_LEN 					2000
#define  MAX_BUFF_FRAGMENT 65535
//#define  ENET_VLAN_TAG_OFFSET            	12
//#define  ENET_VLAN_TAGGED_HEADER_SIZE        16




int raw_socket_configured=0;
int raw_socket=0;
int eth_ifindex=0;
pthread_t packet_task_p;
int exit_process=0;

//typedef int (*EnetHal_rx_packet_callback_func)(ADAP_Uint32 logport,void *Buf,ADAP_Uint32 len);

static EnetHal_rx_packet_callback_func rx_packet_callback_ptr=NULL;

void register_rx_packet_callback(EnetHal_rx_packet_callback_func func)
{
	rx_packet_callback_ptr = func;
}

ADAP_Int32 MeaAdapSendPacket (ADAP_Uint8 *pu1DataBuf, ADAP_Uint16 LogIndex, ADAP_Uint32 PktSize)
{
    int         	bytesSend;
    ADAP_Uint8    	tempBuf[MAX_PACKET_LEN];    /* Temporary buffer to insert the VLAN tag into the packet */
    MEA_Port_t  	EnetPortId;
    ADAP_Uint32		u4NewPktSize=0;
#ifdef PRINT_PACKETS
    ADAP_Uint8		dbgPacket[80];
    ADAP_Uint32		len=0;
    ADAP_Uint32		idx=0;
#endif
    if(raw_socket_configured ==0)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"raw socket not configured\r\n");
    	return ENET_SUCCESS;
    }

	if(PktSize > 2000)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"packet size %d greater then max \r\n",PktSize);
		return ENET_FAILURE;
	}

	if(pu1DataBuf == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"pu1DataBuf is NULL\r\n");
		return ENET_FAILURE;
	}

	if(MeaAdapGetNumOfPhyPorts() <= LogIndex)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ifIndex:%d is out of scope\r\n",LogIndex);
		return ENET_FAILURE;
	}
	if(MeaAdapGetPhyPortFromLogPort(LogIndex,&EnetPortId) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
		return ENET_FAILURE;
	}
	//printf("LogIndex:%d phy:%d\r\n",LogIndex,EnetPortId);



	/* Copy the MAC source and dest. addresses as-is */
	adap_memcpy(tempBuf, pu1DataBuf, ENET_VLAN_TAG_OFFSET);

	/* Add the special VLAN tag for routing inside the ENET HW:
	VLAN ID == the ENET port id, everything else are fill with zeroes */
	if ((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
		(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
	{
		tempBuf[ENET_VLAN_TAG_OFFSET]=0x88;
		tempBuf[ENET_VLAN_TAG_OFFSET+1]=0xa8;
	}
	else
	{
		tempBuf[ENET_VLAN_TAG_OFFSET]=0x81;
		tempBuf[ENET_VLAN_TAG_OFFSET+1]=0x00;
	}
	tempBuf[ENET_VLAN_TAG_OFFSET+2]=0x00;
	tempBuf[ENET_VLAN_TAG_OFFSET+3]=(ADAP_Uint8)(EnetPortId & 0x000000FF);


#if 1
	/* to be included for regression with simulation */
	if ((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
		(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
	{
		tempBuf[ENET_VLAN_TAG_OFFSET+4]=0x88;
		tempBuf[ENET_VLAN_TAG_OFFSET+5]=0xa8;
	}
	else
	{
		tempBuf[ENET_VLAN_TAG_OFFSET+4]=0x81;
		tempBuf[ENET_VLAN_TAG_OFFSET+5]=0x00;
	}
	tempBuf[ENET_VLAN_TAG_OFFSET+6]=0x00;
	tempBuf[ENET_VLAN_TAG_OFFSET+7]=(ADAP_Uint8)(EnetPortId & 0x000000FF);

	adap_memcpy(tempBuf+(ENET_VLAN_TAGGED_HEADER_SIZE +4),
   			     pu1DataBuf+ENET_VLAN_TAG_OFFSET,
				 PktSize-ENET_VLAN_TAG_OFFSET);
	u4NewPktSize = PktSize + 8; 
#endif

	if (u4NewPktSize < 64)
	{
		u4NewPktSize = 64;
	}
#ifdef PRINT_PACKETS
	 adap_memset(&dbgPacket[0],0,sizeof(dbgPacket));
	 len=0;
	 //printf("send packet len:%d\r\n",PktSize);
	 for(idx=0;idx<PktSize;idx++)
	 {
		 len += sprintf((char *)&dbgPacket[len],"0x%x ",tempBuf[idx]);
		 if( ( (idx%16) == 0) && (idx!= 0) )
		 {
			printf("%s\r\n",dbgPacket);
			 adap_memset(&dbgPacket[0],0,sizeof(dbgPacket));
			 len=0;
		 }
	 }
	 if(len != 0)
	 {
		 printf("%s\r\n",dbgPacket);
	 }
#endif // PRINT_PACKETS

    /* Send the data through the Linux raw socket */
	bytesSend = write(raw_socket, tempBuf, u4NewPktSize);
	if ( bytesSend < 0)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to write to socket pkt len %d\r\n",u4NewPktSize);
		return ENET_FAILURE;
	}

	//MEA_OS_free(tempBuf);
	if((ADAP_Uint32)bytesSend != u4NewPktSize)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"byted sent differ than expected\r\n");
		return ENET_FAILURE;
	}

	return ENET_SUCCESS;
}
#ifdef	MEA_NAT_DEMO
static void adap_tcp_cksum(sniff_tcp *pTdp,ADAP_Uint8 *buf,ADAP_Uint32 dataIndex,ADAP_Uint32 total_len)
{
	ADAP_Uint32 sum = 0;
	ADAP_Uint16 ip_buff[10];
	ADAP_Uint32	idx;

	ADAP_UNUSED_PARAM (total_len);

	ip_buff[0] =  pTdp->src_port;
	ip_buff[1] = pTdp->dst_port;
	ip_buff[2] = (ADAP_Uint16)((pTdp->seq & 0xFFFF0000) >> 16);
	ip_buff[3] = (ADAP_Uint16)((pTdp->seq & 0x0000FFFF) );
	ip_buff[4] = (ADAP_Uint16)((pTdp->ack & 0xFFFF0000) >> 16);
	ip_buff[5] = (ADAP_Uint16)((pTdp->ack & 0x0000FFFF) );
	ip_buff[6] = (ADAP_Uint16)((pTdp->data_offset & 0xF) << 12);
	ip_buff[6] |= (ADAP_Uint16)((pTdp->flags & 0xF) );
	ip_buff[7] = pTdp->window_size;
	ip_buff[8] = 0;
	ip_buff[9] = pTdp->urgent_p;

	for(idx=0;idx<10;idx++)
	{
		//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip_buff:%d=%x\r\n",idx,ip_buff[idx]);
		sum += ip_buff[idx];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	idx= (ADAP_Uint16)(pTdp->data_offset) * 4 - 20;

	   for(;idx > 1; idx -= 2 )
	   {
			ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF) << 8);
			ip_buff[0] |= ( (ADAP_Uint16)(buf[dataIndex+1] & 0xFF) );

			sum += ip_buff[0];
			if(sum & 0x80000000)   /* if high order bit set, fold */
			sum = (sum & 0xFFFF) + (sum >> 16);

			dataIndex += 2;
	   }
	   if(idx == 1)
	   {
			ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF));

			sum += ip_buff[0];
			if(sum & 0x80000000)   /* if high order bit set, fold */
			sum = (sum & 0xFFFF) + (sum >> 16);
	   }
	   while(sum>>16)
	   {
		 sum = (sum & 0xFFFF) + (sum >> 16);
	   }

	   pTdp->checksum = (ADAP_Uint16)((~sum) & 0xFFFF);

}

static void adap_udp_cksum(sniff_udp *pUdp,ADAP_Uint8 *buf,ADAP_Uint32 dataIndex,ADAP_Uint32 total_len)
{
	ADAP_Uint32 sum = 0;
	ADAP_Uint16 ip_buff[5];
	ADAP_Uint32	idx;

	ADAP_UNUSED_PARAM (total_len);

	ip_buff[0] = pUdp->udph_srcport;
	ip_buff[1] = pUdp->udph_destport;
	ip_buff[2] = pUdp->udph_len;
	ip_buff[3] = 0;

   for(idx=0;idx<4;idx++)
   {
		//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip_buff:%d=%x\r\n",idx,ip_buff[idx]);
		sum += ip_buff[idx];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);
   }

   idx=pUdp->udph_len;

   for(;idx > 1; idx -= 2 )
   {
		ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF) << 8);
		ip_buff[0] |= ( (ADAP_Uint16)(buf[dataIndex+1] & 0xFF) );

		sum += ip_buff[0];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);

		dataIndex += 2;
   }
   if(idx == 1)
   {
		ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF));

		sum += ip_buff[0];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);
   }
   while(sum>>16)
   {
	 sum = (sum & 0xFFFF) + (sum >> 16);
   }

   pUdp->udph_chksum = (ADAP_Uint16)((~sum) & 0xFFFF);
}

static void adap_icmp_cksum(sniff_icmp	*pIcmp,ADAP_Uint8 *buf,ADAP_Uint32 dataIndex,ADAP_Uint32 total_len)
{
	ADAP_Uint32 sum = 0;
	ADAP_Uint16 ip_buff[5];
	ADAP_Uint32	idx;

	ip_buff[0] = ( (ADAP_Uint16)(pIcmp->type & 0xFF) << 8);
	ip_buff[0] |= ( (ADAP_Uint16)(pIcmp->code & 0xFF) );
	ip_buff[1] = 0;
	ip_buff[2] = pIcmp->un.echo.id;
	ip_buff[3] = pIcmp->un.echo.sequence;


	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"sequence number:%x\r\n",pIcmp->un.echo.sequence);
   for(idx=0;idx<4;idx++)
   {
		//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip_buff:%d=%x\r\n",idx,ip_buff[idx]);
		sum += ip_buff[idx];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);
   }

   idx=total_len-dataIndex;

   for(;idx > 1; idx -= 2 )
   {
		ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF) << 8);
		ip_buff[0] |= ( (ADAP_Uint16)(buf[dataIndex+1] & 0xFF) );

		sum += ip_buff[0];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);

		dataIndex += 2;
   }

   if(idx == 1)
   {
		ip_buff[0] = ( (ADAP_Uint16)(buf[dataIndex] & 0xFF));

		sum += ip_buff[0];
		if(sum & 0x80000000)   /* if high order bit set, fold */
		sum = (sum & 0xFFFF) + (sum >> 16);
   }


   //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"sum=%x\r\n",sum);

   while(sum>>16)
   {
	 sum = (sum & 0xFFFF) + (sum >> 16);
   }

   pIcmp->checksum = (ADAP_Uint16)((~sum) & 0xFFFF);

   //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"checksum=%x\r\n",pIcmp->checksum);
}

static void adap_ip_cksum(sniff_ip *ip)
{
	ADAP_Uint32 sum = 0;
	ADAP_Uint16 ip_buff[10];
	ADAP_Uint32	idx;


//	ADAP_Uint16 ip_sum;                 /* checksum */
//	ADAP_Uint8  ip_src[4];
//	ADAP_Uint8 ip_dst[4];  /* source and dest address */

	ip_buff[0] = ( (ADAP_Uint16)(ip->ip_vhl & 0xFF) << 8);
	ip_buff[0] |= ( (ADAP_Uint16)(ip->ip_tos & 0xFF) );

	ip_buff[1] = ip->ip_len;

	ip_buff[2] = ip->ip_id;

	ip_buff[3] = ip->ip_off;

	ip_buff[4] = ( (ADAP_Uint16)(ip->ip_ttl & 0xFF) << 8);
	ip_buff[4] |= ( (ADAP_Uint16)(ip->ip_p & 0xFF) );

	ip_buff[5] = 0; //ip_sum

	ip_buff[6] = ( (ADAP_Uint16)(ip->ip_src[0] & 0xFF) << 8);
	ip_buff[6] |= ( (ADAP_Uint16)(ip->ip_src[1] & 0xFF) );

	ip_buff[7] = ( (ADAP_Uint16)(ip->ip_src[2] & 0xFF) << 8);
	ip_buff[7] |= ( (ADAP_Uint16)(ip->ip_src[3] & 0xFF) );

	ip_buff[8] = ( (ADAP_Uint16)(ip->ip_dst[0] & 0xFF) << 8);
	ip_buff[8] |= ( (ADAP_Uint16)(ip->ip_dst[1] & 0xFF) );

	ip_buff[9] = ( (ADAP_Uint16)(ip->ip_dst[2] & 0xFF) << 8);
	ip_buff[9] |= ( (ADAP_Uint16)(ip->ip_dst[3] & 0xFF) );

   for(idx=0;idx<10;idx++)
   {

	 sum += ip_buff[idx];
	 if(sum & 0x80000000)   /* if high order bit set, fold */
	   sum = (sum & 0xFFFF) + (sum >> 16);
   }

   while(sum>>16)
   {
	 sum = (sum & 0xFFFF) + (sum >> 16);
   }

   ip->ip_sum = ( (ADAP_Uint16)~sum) & 0xFFFF;
}

static void copy_ipPacket(tProtocl_sniff *pProtoSniff,unsigned char *buf)
{
	ADAP_Uint32 idx=0;
	ADAP_Uint32 index=0;

	for(idx=0;idx<ENETHAL_MAC_ADDR_LEN;idx++)
	{
		buf[index++] = pProtoSniff->ethernet.ether_dhost[idx];
	}
	for(idx=0;idx<ENETHAL_MAC_ADDR_LEN;idx++)
	{
		buf[index++] = pProtoSniff->ethernet.ether_shost[idx];
	}
	index += 2;

	if(pProtoSniff->ethernet.ether_type == ADAP_IP_PROTOCOL)
	{
		index++;  //pProtoSniff->ip.ip_vhl;
		index++; //pProtoSniff->ip.ip_tos;
		index++; // pProtoSniff->ip.ip_len
		index++; // pProtoSniff->ip.ip_len
		index++; //pProtoSniff->ip.ip_id
		index++; //pProtoSniff->ip.ip_id
		index++; //pProtoSniff->ip.ip_off
		index++; //pProtoSniff->ip.ip_off;
		index++; //pProtoSniff->ip.ip_ttl
		index++; //pProtoSniff->ip.ip_p
		buf[index++] = (ADAP_Uint8)( (pProtoSniff->ip.ip_sum & 0xFF00) >> 8);
		buf[index++] = (ADAP_Uint8)( (pProtoSniff->ip.ip_sum & 0x00FF) );

		for(idx=0;idx<4;idx++)
		{
			buf[index++] = pProtoSniff->ip.ip_src[idx];
		}
		for(idx=0;idx<4;idx++)
		{
			buf[index++] = pProtoSniff->ip.ip_dst[idx];
		}
		if(pProtoSniff->ip.ip_p == IPPROTO_ICMP)
		{
			index++; //pProtoSniff->icmp.type
			index++; //pProtoSniff->icmp.code

			buf[index++] = (ADAP_Uint8)( (pProtoSniff->icmp.checksum & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->icmp.checksum & 0x00FF) );

			buf[index++] = (ADAP_Uint8)( (pProtoSniff->icmp.un.echo.id & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->icmp.un.echo.id & 0x00FF) );

			index++; //pProtoSniff->icmp.un.echo.sequence
			index++; //pProtoSniff->icmp.un.echo.sequence
		}
		if(pProtoSniff->ip.ip_p == ADAP_TCP_PROTOCOL)
		{
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.src_port & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.src_port & 0x00FF) );

			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.dst_port & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.dst_port & 0x00FF) );

			index += 12;
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.checksum & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->tcp.checksum & 0x00FF) );
		}
		if(pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL)
		{
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_srcport & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_srcport & 0x00FF) );

			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_destport & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_destport & 0x00FF) );

			index += 2;
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_chksum & 0xFF00) >> 8);
			buf[index++] = (ADAP_Uint8)( (pProtoSniff->udp.udph_chksum & 0x00FF) );
		}
	}
}
//#ifdef	MEA_NAT_DEMO
static MEA_Bool calculate_IpPacket(tProtocl_sniff *pProtoSniff,ADAP_Uint32 IngressIfIndex,ADAP_Uint32 egressIfIndex,ADAP_Uint8 *buf,ADAP_Uint32 dataIndex,ADAP_Uint32 total_len)
{
	tIpDestV4Routing *pAddNextHop=NULL;
	tIpV4NextRoute   *pNextRoute=NULL;
//	ADAP_Uint8		 gatewayIp[4] = {0x0A,0x64,0x01,0x01};
	MEA_Port_t 		PhyPort;
	ADAP_Uint32		idx=0;
	tIpInterfaceTable *pVlanInterface;
	ADAP_Uint32 cfaPort;
	tIcmpTable *pAlloIcmpTable;
	tIcmpTable tLocalIcmpTable;



	if(pProtoSniff->ethernet.ether_type == ADAP_IP_PROTOCOL)
	{
		//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ip protocol:%d icmp type:%d\r\n",pProtoSniff->ip.ip_p,pProtoSniff->icmp.type);
		if( (pProtoSniff->ip.ip_p == IPPROTO_ICMP) && (pProtoSniff->icmp.type == ADAP_ICMP_ECHO) )
		{
			// for echo request
			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"ingress IfIndex:%d egressIfIndex:%d\r\n",IngressIfIndex,egressIfIndex);
			if( (IngressIfIndex == NAT_LAN_IFINDEX) && (egressIfIndex == NAT_WAN_IFINDEX) )
			{
				getCfaFromIfindexInterface(&cfaPort,egressIfIndex);

				pVlanInterface = AdapGetIpInterface(cfaPort);

				if(pVlanInterface == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"cfa index:%d not exist\r\n",cfaPort);
					return MEA_FALSE;
				}
				// check if there is destination MAC address
				//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"looking from despIp:%x MAC address\r\n",pProtoSniff->destIp);
				if(AdapGetEntryIpTable(&pAddNextHop,pProtoSniff->destIp) != ENET_SUCCESS)
				{
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"dest ip Addr not found:%x check def gateway\r\n",pProtoSniff->destIp);
					if(AdapGetDefaultGatewayRouteTable(&pNextRoute) == ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default gateway:%x\r\n",pNextRoute->u4NextHopGt);
						if(AdapGetEntryIpTable(&pAddNextHop,pNextRoute->u4NextHopGt) != ENET_SUCCESS)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"default gateway not found\r\n");
							return MEA_FALSE;
						}
					}
					else
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"default gateway not exist\r\n");
						return MEA_FALSE;
					}
				}
				if(pAddNextHop == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ip addr:%d not found\r\n");
					return MEA_FALSE;
				}
#if 0
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MAC %s\r\n",
													adap_mac_to_string(pAddNextHop->macAddress);
#endif

				if(adap_if_mac_zero(&pAddNextHop->macAddress) == ADAP_FALSE)
				{
					// so now we have the source mac of the destination IP
					// source Mac of the gateway
					if(MeaAdapGetPhyPortFromLogPort(egressIfIndex,&PhyPort) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
						return MEA_FALSE;
					}

					// replace the source MAC with source MAC of the gateway
					if( MeaDrvHwGetInterfaceCfmOamUCMac (PhyPort,(char *)&pProtoSniff->ethernet.ether_shost[0]) != MEA_FALSE)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwGetInterfaceCfmOamUCMac\r\n");
						return MEA_FALSE;
					}

					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"start building IP packet\r\n");

					// replace to real destMac address
					adap_memcpy(pProtoSniff->ethernet.ether_dhost, pAddNextHop->macAddress, ENETHAL_MAC_ADDR_LEN);
					tLocalIcmpTable.sourceIpAddr = ((ADAP_Uint32)pProtoSniff->ip.ip_src[0]) << 24;
					tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[1]) << 16;
					tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[2]) << 8;
					tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[3]);

					tLocalIcmpTable.destIpAddr = ((ADAP_Uint32)pProtoSniff->ip.ip_dst[0]) << 24;
					tLocalIcmpTable.destIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[1]) << 16;
					tLocalIcmpTable.destIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[2]) << 8;
					tLocalIcmpTable.destIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[3]);

					tLocalIcmpTable.origEchoId=pProtoSniff->icmp.un.echo.id;

					pAlloIcmpTable = allocate_IcmpTable(&tLocalIcmpTable);

					if(pAlloIcmpTable == NULL)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call allocate_IcmpTable\r\n");
						return MEA_FALSE;
					}


					pProtoSniff->ip.ip_src[0] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0xFF000000) >> 24);
					pProtoSniff->ip.ip_src[1] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x00FF0000) >> 16);
					pProtoSniff->ip.ip_src[2] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x0000FF00) >> 8);
					pProtoSniff->ip.ip_src[3] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x000000FF) );

					pProtoSniff->icmp.un.echo.id = pAlloIcmpTable->newEchoId;

					adap_ip_cksum(&pProtoSniff->ip);
					adap_icmp_cksum(&pProtoSniff->icmp,buf,dataIndex,total_len);


					return MEA_TRUE;
				}
				else
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ipAddr:%x with zero MAC address\r\n",pAddNextHop->ipAddr);
				}
			}
		}
		if( (pProtoSniff->ip.ip_p == IPPROTO_ICMP) && (pProtoSniff->icmp.type == ADAP_ICMP_ECHOREPLY) )
		{
			//
			//and if source IP exist and echoReply found
			tLocalIcmpTable.sourceIpAddr = ((ADAP_Uint32)pProtoSniff->ip.ip_src[0]) << 24;
			tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[1]) << 16;
			tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[2]) << 8;
			tLocalIcmpTable.sourceIpAddr |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[3]);

			pAlloIcmpTable = getIcmpTableFromEchoReply(tLocalIcmpTable.sourceIpAddr,pProtoSniff->icmp.un.echo.id);

			if(pAlloIcmpTable != NULL)
			{
				// get the destination mac from arp table
				if(AdapGetEntryIpTable(&pAddNextHop,pAlloIcmpTable->sourceIpAddr) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"IpAdd:%x not found in arp table\r\n",pAlloIcmpTable->sourceIpAddr);
					return MEA_FALSE;
				}
				if(pAddNextHop == NULL)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"pAddNextHop==NULL for destIp:%d\r\n",pAlloIcmpTable->sourceIpAddr);
					return MEA_FALSE;
				}
#if 0
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IP:%x MAC %x-%x-%x-%x-%x-%x\r\n",
												pAlloIcmpTable->sourceIpAddr,
												pAddNextHop->macAddress[0],
												pAddNextHop->macAddress[1],
												pAddNextHop->macAddress[2],
												pAddNextHop->macAddress[3],
												pAddNextHop->macAddress[4],
												pAddNextHop->macAddress[5]);
#endif
				adap_memcpy(pProtoSniff->ethernet.ether_dhost, pAddNextHop->macAddress, ENETHAL_MAC_ADDR_LEN);

				// get the source mac from lan
				if(MeaAdapGetPhyPortFromLogPort(IngressIfIndex,&PhyPort) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
					return MEA_FALSE;
				}

				// replace the source MAC with source MAC of the gateway
				if( MeaDrvHwGetInterfaceCfmOamUCMac (PhyPort,(char *)&pProtoSniff->ethernet.ether_shost[0]) != MEA_FALSE)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwGetInterfaceCfmOamUCMac\r\n");
					return MEA_FALSE;
				}

				pProtoSniff->ip.ip_dst[0] =  (ADAP_Uint8)( (pAlloIcmpTable->sourceIpAddr & 0xFF000000) >> 24);
				pProtoSniff->ip.ip_dst[1] =  (ADAP_Uint8)( (pAlloIcmpTable->sourceIpAddr & 0x00FF0000) >> 16);
				pProtoSniff->ip.ip_dst[2] =  (ADAP_Uint8)( (pAlloIcmpTable->sourceIpAddr & 0x0000FF00) >> 8);
				pProtoSniff->ip.ip_dst[3] =  (ADAP_Uint8)( (pAlloIcmpTable->sourceIpAddr & 0x000000FF) );

				pProtoSniff->icmp.un.echo.id = pAlloIcmpTable->origEchoId;


				adap_ip_cksum(&pProtoSniff->ip);
				adap_icmp_cksum(&pProtoSniff->icmp,buf,dataIndex,total_len);

				return MEA_TRUE;
			}
			else
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"IpAdd:%x icmp echo id:%d\r\n",tLocalIcmpTable.sourceIpAddr,pProtoSniff->icmp.un.echo.id);
			}
		}
	}
	return MEA_FALSE;
}


static MEA_Bool check_tcp_udp_connection(tProtocl_sniff *pProtoSniff,ADAP_Uint32 IngressIfIndex,ADAP_Uint32 egressIfIndex,ADAP_Uint8 *buf,ADAP_Uint32 dataIndex,ADAP_Uint32 total_len)
{
	ADAP_Uint32 cfaPort;
	tIpInterfaceTable *pVlanInterface;
	tIpDestV4Routing *pAddNextHop=NULL;
	tIpV4NextRoute   *pNextRoute=NULL;
	tEnetHal_MacAddr zeroAddress={0,0,0,0,0,0};
	MEA_Port_t 		IngPhyPort=0;
	MEA_Port_t 		EgrPhyPort=0;
//	ADAP_Uint8  		ether_shost[6];
	tTcpUdpTable	*pTcpUdpTable=NULL;
	tTcpUdpTable	tTcpMyUdpTable;
	ADAP_Uint32		idx=0;
	tEnetHal_MacAddr origSrcAddress;


	// for netbios
	if( (pProtoSniff->ip.ip_p == ADAP_TCP_PROTOCOL) && (pProtoSniff->tcp.src_port == 137) && (pProtoSniff->tcp.dst_port == 137))
	{
		return MEA_FALSE;
	}
	if( (pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL) && (pProtoSniff->udp.udph_srcport == 137) && (pProtoSniff->udp.udph_destport == 137))
	{
		return MEA_FALSE;
	}

	// for bootp
	if( (pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL) && ( (pProtoSniff->udp.udph_srcport == 67) || (pProtoSniff->udp.udph_destport == 68) ) )
	{
		return MEA_FALSE;
	}
	if( (pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL) && ( (pProtoSniff->udp.udph_srcport == 68) || (pProtoSniff->udp.udph_destport == 67) ) )
	{
		return MEA_FALSE;
	}


	// from LAN to WAN
	if( (IngressIfIndex == NAT_LAN_IFINDEX) && (egressIfIndex == NAT_WAN_IFINDEX) )
	{
		getCfaFromIfindexInterface(&cfaPort,egressIfIndex);

		pVlanInterface = AdapGetIpInterface(cfaPort);

		if(pVlanInterface == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"cfa index:%d not exist\r\n",cfaPort);
			return MEA_FALSE;
		}
		if(AdapGetEntryIpTable(&pAddNextHop,pProtoSniff->destIp) != ENET_SUCCESS)
		{
			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"AdapGetEntryIpTable:%x not found\r\n",pProtoSniff->destIp);
			if(AdapGetEntryIpTable(&pAddNextHop,pProtoSniff->destIp) != ENET_SUCCESS)
			{
				//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"dest ip Addr not found:%x check def gateway\r\n",pProtoSniff->destIp);
				if(AdapGetDefaultGatewayRouteTable(&pNextRoute) == ENET_SUCCESS)
				{
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"default gateway:%x\r\n",pNextRoute->u4NextHopGt);
					if(AdapGetEntryIpTable(&pAddNextHop,pNextRoute->u4NextHopGt) != ENET_SUCCESS)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"default gateway not found next Ip:%x\r\n",pNextRoute->u4NextHopGt);
						return MEA_FALSE;
					}
				}
				else
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"default gateway not exist\r\n");
					return MEA_FALSE;
				}
			}
		}

		if(pAddNextHop == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"pAddNextHop==NULL for destIp:%x\r\n",pProtoSniff->destIp);
			return MEA_FALSE;
		}
		if(adap_memcmp(pAddNextHop->macAddress,zeroAddress,ENETHAL_MAC_ADDR_LEN) != 0)
		{
			// so now we have the source mac of the destination IP
			// source Mac of the gateway
			if(MeaAdapGetPhyPortFromLogPort(egressIfIndex,&EgrPhyPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
				return MEA_FALSE;
			}

			adap_memcpy(origSrcAddress,pProtoSniff->ethernet.ether_shost,ENETHAL_MAC_ADDR_LEN);

			// replace the source MAC with source MAC of the gateway
			if( MeaDrvHwGetInterfaceCfmOamUCMac (EgrPhyPort,(char *)&pProtoSniff->ethernet.ether_shost[0]) != MEA_FALSE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwGetInterfaceCfmOamUCMac\r\n");
				return MEA_FALSE;
			}

			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"start building IP packet\r\n");

			// replace to real destMac address
			adap_memcpy(pProtoSniff->ethernet.ether_dhost, pAddNextHop->macAddress, ENETHAL_MAC_ADDR_LEN);

			pProtoSniff->ip.ip_src[0] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0xFF000000) >> 24);
			pProtoSniff->ip.ip_src[1] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x00FF0000) >> 16);
			pProtoSniff->ip.ip_src[2] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x0000FF00) >> 8);
			pProtoSniff->ip.ip_src[3] =  (ADAP_Uint8)( (pVlanInterface->MyIpAddr.ipAddr & 0x000000FF) );


			tTcpMyUdpTable.destIpAddr = pProtoSniff->destIp;
			tTcpMyUdpTable.sourceIpAddr = pProtoSniff->sourceIp;

			if(pProtoSniff->ip.ip_p == ADAP_TCP_PROTOCOL)
			{
				tTcpMyUdpTable.destPort = pProtoSniff->tcp.dst_port;
				tTcpMyUdpTable.sourcePort = pProtoSniff->tcp.src_port;
				tTcpMyUdpTable.protocol = ADAP_TCP_PROTOCOL;
			}
			if(pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL)
			{
				tTcpMyUdpTable.destPort = pProtoSniff->udp.udph_destport;
				tTcpMyUdpTable.sourcePort = pProtoSniff->udp.udph_srcport;
				tTcpMyUdpTable.protocol = ADAP_UDP_PROTOCOL;
			}

//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"sourceIp:%d-%d-%d-%d SAport:%d\r\n",
//													pProtoSniff->ip.ip_src[0],
//													pProtoSniff->ip.ip_src[1],
//													pProtoSniff->ip.ip_src[2],
//													pProtoSniff->ip.ip_src[3],
//													tTcpMyUdpTable.sourcePort);

			pTcpUdpTable = allocate_tcpUdpTable(&tTcpMyUdpTable);



			if(pTcpUdpTable == NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to allocate tcp/udp table\r\n");
				return MEA_FALSE;
			}
			adap_ip_cksum(&pProtoSniff->ip);
			if(pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL)
			{
				// just for debug
				pTcpUdpTable->newSourcePort=pProtoSniff->udp.udph_srcport;

				pProtoSniff->udp.udph_srcport = pTcpUdpTable->newSourcePort;
				adap_udp_cksum(&pProtoSniff->udp,buf,dataIndex,total_len);
			}
			else
			{
				// just for debug
				pTcpUdpTable->newSourcePort=pProtoSniff->tcp.src_port;

				pProtoSniff->tcp.src_port = pTcpUdpTable->newSourcePort;
				adap_tcp_cksum(&pProtoSniff->tcp,buf,dataIndex,total_len);
			}
#if 0
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"insert entry for DestIp:%x DAport:%d SAport:%d newSAport:%d proto:%d\r\n",
													tTcpMyUdpTable.destIpAddr,
													tTcpMyUdpTable.destPort,
													tTcpMyUdpTable.sourcePort,
													pTcpUdpTable->newSourcePort,
													tTcpMyUdpTable.protocol);
#endif



			/* if HTTP */
			if( (pProtoSniff->ip.ip_p == ADAP_TCP_PROTOCOL) && ( (pProtoSniff->tcp.dst_port == 80) || (pProtoSniff->tcp.dst_port == 443) ) )
			{
				// create action from LAN to WAN based on source_port and source_ip,
				//        action replace MAC, replace source IP and replace source port
				if(pTcpUdpTable->HostToNetAtionId == MEA_PLAT_GENERATE_NEW_ID)
				{

					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"going to create HostToNetAtionId\r\n");
					if(MeaDrvNatCreateAction(&pTcpUdpTable->HostToNetAtionId,
											&pProtoSniff->ethernet.ether_dhost[0],
											pVlanInterface->MyIpAddr.ipAddr,
											pTcpUdpTable->newSourcePort,
											MEA_TRUE,
											EgrPhyPort,
											&pTcpUdpTable->HostToNetpmId) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvNatCreateAction\r\n");
						return MEA_FALSE;
					}
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"action id:%d pmId:%d\r\n",pTcpUdpTable->HostToNetAtionId,pTcpUdpTable->HostToNetpmId);

					if(MeaDrvHwCreateNatForwarder(pTcpUdpTable->HostToNetAtionId,
												tTcpMyUdpTable.sourceIpAddr,
												tTcpMyUdpTable.sourcePort,
												VPN_HOST_TO_NETWORK,
												EgrPhyPort,
												MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwCreateNatForwarder\r\n");
						return MEA_FALSE;
					}
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"new sourcePort:%d\r\n",pTcpUdpTable->newSourcePort);
					MeaDrvHwGetNatForwarder(tTcpMyUdpTable.sourceIpAddr,tTcpMyUdpTable.sourcePort,VPN_HOST_TO_NETWORK,MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN);

				}

				if(MeaAdapGetPhyPortFromLogPort(IngressIfIndex,&IngPhyPort) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
					return MEA_FALSE;
				}
				// create action from WAN to LAN based on destination IP and destination port
				//        action replace MAC , replace destination IP and replace destination port
				if(pTcpUdpTable->NetToHostAtionId == MEA_PLAT_GENERATE_NEW_ID)
				{
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"going to create NetToHostAtionId\r\n");
					if(MeaDrvNatCreateAction(&pTcpUdpTable->NetToHostAtionId,
											&origSrcAddress[0],
											tTcpMyUdpTable.sourceIpAddr,
											tTcpMyUdpTable.sourcePort,
											MEA_FALSE,
											IngPhyPort,
											&pTcpUdpTable->NetToHostpmId) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvNatCreateAction\r\n");
						return MEA_FALSE;
					}
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"going to create NetToHostAtionId=%d pmId:%d\r\n",pTcpUdpTable->NetToHostAtionId,pTcpUdpTable->NetToHostpmId);

					if(MeaDrvHwCreateNatForwarder(pTcpUdpTable->NetToHostAtionId,
												pVlanInterface->MyIpAddr.ipAddr,
												pTcpUdpTable->newSourcePort,
												START_L3_VPN,
												IngPhyPort,
												MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwCreateNatForwarder\r\n");
						return MEA_FALSE;
					}
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"created NetToHostAtionId in the forwarder\r\n");
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"cnew sourcePort:%d\r\n",tTcpMyUdpTable.sourcePort);
					MeaDrvHwGetNatForwarder(pVlanInterface->MyIpAddr.ipAddr,pTcpUdpTable->newSourcePort,START_L3_VPN,MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN);
				}

				pTcpUdpTable->handle_by_cpu = MEA_FALSE;
			}

			return MEA_TRUE;
		}
		else
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"ipAddr:%d.%d.%d.%d with zero MAC address\r\n",
													(pAddNextHop->ipAddr & 0xFF000000) >> 24,
													(pAddNextHop->ipAddr & 0x00FF0000) >> 16,
													(pAddNextHop->ipAddr & 0x0000FF00) >> 8,
													(pAddNextHop->ipAddr & 0xFF000000) );
		}
	}
	else
	{

		tTcpMyUdpTable.sourceIpAddr = pProtoSniff->sourceIp;
		if(pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL)
		{
			tTcpMyUdpTable.sourcePort = pProtoSniff->udp.udph_srcport;
			tTcpMyUdpTable.destPort = pProtoSniff->udp.udph_destport;
			tTcpMyUdpTable.protocol = ADAP_UDP_PROTOCOL;
		}
		else
		{
			tTcpMyUdpTable.sourcePort = pProtoSniff->tcp.src_port;
			tTcpMyUdpTable.destPort = pProtoSniff->tcp.dst_port;
			tTcpMyUdpTable.protocol = ADAP_TCP_PROTOCOL;
		}

//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"checking reply destIp:%x sourcePort:%d destPort:%d protocol:%d\r\n",
//				tTcpMyUdpTable.sourceIpAddr,tTcpMyUdpTable.destPort,tTcpMyUdpTable.destPort,tTcpMyUdpTable.protocol);

		pTcpUdpTable = getTcpUdpTable(&tTcpMyUdpTable);

		if(pTcpUdpTable != NULL)
		{
			if( (tTcpMyUdpTable.sourcePort == 80) || (tTcpMyUdpTable.sourcePort == 443) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"DAIpAdd:%d.%d.%d.%d DAport:%d not catch by forwarder\r\n",
														(pProtoSniff->destIp & 0xFF000000)>>24,
														(pProtoSniff->destIp & 0x00FF0000)>>16,
														(pProtoSniff->destIp & 0x0000FF00)>>8,
														(pProtoSniff->destIp & 0x000000FF),
														tTcpMyUdpTable.destPort);
			}
			if(AdapGetEntryIpTable(&pAddNextHop,pTcpUdpTable->sourceIpAddr) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"IpAdd:%x not found in arp table\r\n",pTcpUdpTable->sourceIpAddr);
				return MEA_FALSE;
			}
			if(pAddNextHop == NULL)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"pAddNextHop==NULL for destIp:%d\r\n",pTcpUdpTable->sourceIpAddr);
				return MEA_FALSE;
			}

			adap_memcpy(pProtoSniff->ethernet.ether_dhost, pAddNextHop->macAddress, ENETHAL_MAC_ADDR_LEN);
#if 0
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"daMac %x-%x-%x-%x-%x-%x\r\n",
													pProtoSniff->ethernet.ether_dhost[0],
													pProtoSniff->ethernet.ether_dhost[1],
													pProtoSniff->ethernet.ether_dhost[2],
													pProtoSniff->ethernet.ether_dhost[3],
													pProtoSniff->ethernet.ether_dhost[4],
													pProtoSniff->ethernet.ether_dhost[5]);
#endif
			// get the source mac from lan
			if(MeaAdapGetPhyPortFromLogPort(IngressIfIndex,&IngPhyPort) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
				return MEA_FALSE;
			}

			// replace the source MAC with source MAC of the gateway
			if( MeaDrvHwGetInterfaceCfmOamUCMac (IngPhyPort,(char *)&pProtoSniff->ethernet.ether_shost[0]) != MEA_FALSE)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaDrvHwGetInterfaceCfmOamUCMac\r\n");
				return MEA_FALSE;
			}

			pProtoSniff->ip.ip_dst[0] =  (ADAP_Uint8)( (pTcpUdpTable->sourceIpAddr & 0xFF000000) >> 24);
			pProtoSniff->ip.ip_dst[1] =  (ADAP_Uint8)( (pTcpUdpTable->sourceIpAddr & 0x00FF0000) >> 16);
			pProtoSniff->ip.ip_dst[2] =  (ADAP_Uint8)( (pTcpUdpTable->sourceIpAddr & 0x0000FF00) >> 8);
			pProtoSniff->ip.ip_dst[3] =  (ADAP_Uint8)( (pTcpUdpTable->sourceIpAddr & 0x000000FF) );

			adap_ip_cksum(&pProtoSniff->ip);
			if(pProtoSniff->ip.ip_p == ADAP_UDP_PROTOCOL)
			{
				//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"rcv udp reply was DAport:%d now DAport:%d \r\n",pProtoSniff->udp.udph_destport,pTcpUdpTable->sourcePort);
				pProtoSniff->udp.udph_destport = pTcpUdpTable->sourcePort;
				adap_udp_cksum(&pProtoSniff->udp,buf,dataIndex,total_len);
			}
			else
			{
				//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"rcv udp reply was DAport:%d now DAport:%d \r\n",pProtoSniff->tcp.dst_port ,pTcpUdpTable->sourcePort);
				pProtoSniff->tcp.dst_port = pTcpUdpTable->sourcePort;
				adap_tcp_cksum(&pProtoSniff->tcp,buf,dataIndex,total_len);
			}

			return MEA_TRUE;
		}
		else
		{
#if 0
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"can't find entry for SrcIp %d.%d.%d.%d DstIp %d.%d.%d.%d DAport:%d SAport:%d\r\n",
													(pProtoSniff->sourceIp & 0xFF000000) >> 24,
													(pProtoSniff->sourceIp & 0x00FF0000) >> 16,
													(pProtoSniff->sourceIp & 0x0000FF00) >> 8,
													(pProtoSniff->sourceIp & 0x000000FF),
													(pProtoSniff->destIp & 0xFF000000) >> 24,
													(pProtoSniff->destIp & 0x00FF0000) >> 16,
													(pProtoSniff->destIp & 0x0000FF00) >> 8,
													(pProtoSniff->destIp & 0x000000FF),
													tTcpMyUdpTable.destPort,
													tTcpMyUdpTable.sourcePort);
#endif
			return MEA_FALSE;
		}
	}
	return MEA_FALSE;
}
#endif
ADAP_Bool get_sourceip_from_ipPacket(unsigned char *buf, int len,tProtocl_sniff *pProtoSniff,ADAP_Uint32 *pIndex_data)
{
//	sniff_ethernet ethernet;
//	sniff_ip	   ip;
//	sniff_arp	   arp;
//	sniff_icmp	   icmp;
//	ADAP_Uint32 vlan_len=0;
	int idx=0;
	int idx1=0;
//	ADAP_Uint8 my_mac[6];
//	unsigned char *str=NULL;

	(*pIndex_data) = len;

	adap_mac_from_arr(&pProtoSniff->ethernet.ether_dhost, buf);
	idx += ETH_ALEN;

	// only if its my mac or broadcast mac
//	if(pProtoSniff->ethernet.ether_dhost[0] != 0xFF)
//	{
//		// check if my MAC
//		MeaDrvHwGetGlobalUCMac((char*)&my_mac[0]);
//		if(memcmp((const char *)&my_mac[0],(const char *)&pProtoSniff->ethernet.ether_dhost[0],6) != 0)
//		{
//			return MEA_FALSE;
//		}
//	}

	adap_mac_from_arr(&pProtoSniff->ethernet.ether_shost, buf);
	idx += ETH_ALEN;

	pProtoSniff->ethernet.ether_type =  ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
	pProtoSniff->ethernet.ether_type |=  ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

	if(buf[12]==0x81 && buf[13]==0x00)
	{
//		vlan_len=4;
                pProtoSniff->ethernet.vlanId = (buf[15])|((buf[14]&0xf)<<8);
		idx += 2;
		pProtoSniff->ethernet.ether_type =  ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
		pProtoSniff->ethernet.ether_type |=  ( (ADAP_Uint16)(buf[idx++] & 0xFF) );
	}
	if(pProtoSniff->ethernet.ether_type == ADAP_ARP_PROTOCOL)
	{
		for(idx1=0;idx1<8;idx1++)
		{
			pProtoSniff->arp.ea_hdr[idx1] = buf[idx++];
		}

		adap_mac_from_arr(&pProtoSniff->arp.arp_sha, buf);
		idx += ETH_ALEN;

		for(idx1=0;idx1<4;idx1++)
		{
			pProtoSniff->arp.src_ipaddr[idx1] = buf[idx++];
		}
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->arp.src_ipaddr[0]) << 24;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->arp.src_ipaddr[1]) << 16;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->arp.src_ipaddr[2]) << 8;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->arp.src_ipaddr[3]) << 0;
		(*pIndex_data) = idx;

		return ADAP_TRUE;
	}
	if(pProtoSniff->ethernet.ether_type == ADAP_IP_PROTOCOL)
	{
		pProtoSniff->ip.ip_vhl = buf[idx++];
		pProtoSniff->ip.ip_tos = buf[idx++];
		pProtoSniff->ip.ip_len = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
		pProtoSniff->ip.ip_len += ( (ADAP_Uint16)(buf[idx++] & 0xFF) );
		pProtoSniff->ip.ip_id = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
		pProtoSniff->ip.ip_id += ( (ADAP_Uint16)(buf[idx++] & 0xFF) );
		pProtoSniff->ip.ip_off = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
		pProtoSniff->ip.ip_off += ( (ADAP_Uint16)(buf[idx++] & 0xFF) );
		pProtoSniff->ip.ip_ttl = buf[idx++];
		pProtoSniff->ip.ip_p = buf[idx++];
		pProtoSniff->ip.ip_sum = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
		pProtoSniff->ip.ip_sum += ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

		pProtoSniff->ip.ip_src[0] = buf[idx++];
		pProtoSniff->ip.ip_src[1] = buf[idx++];
		pProtoSniff->ip.ip_src[2] = buf[idx++];
		pProtoSniff->ip.ip_src[3] = buf[idx++];

		pProtoSniff->ip.ip_dst[0] = buf[idx++];
		pProtoSniff->ip.ip_dst[1] = buf[idx++];
		pProtoSniff->ip.ip_dst[2] = buf[idx++];
		pProtoSniff->ip.ip_dst[3] = buf[idx++];


		pProtoSniff->destIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[0]) << 24;
		pProtoSniff->destIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[1]) << 16;
		pProtoSniff->destIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[2]) << 8;
		pProtoSniff->destIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_dst[3]) << 0;

		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[0]) << 24;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[1]) << 16;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[2]) << 8;
		pProtoSniff->sourceIp |= ((ADAP_Uint32)pProtoSniff->ip.ip_src[3]) << 0;

		(*pIndex_data) = idx;

		if(pProtoSniff->ip.ip_p == IPPROTO_ICMP)
		{
			pProtoSniff->icmp.type = buf[idx++];
			pProtoSniff->icmp.code = buf[idx++];
			pProtoSniff->icmp.checksum = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->icmp.checksum |=  ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->icmp.un.echo.id = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->icmp.un.echo.id |=  ( (ADAP_Uint16)(buf[idx++] & 0xFF) );
			pProtoSniff->icmp.un.echo.sequence = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->icmp.un.echo.sequence |=  ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			(*pIndex_data) = idx;
		}
		else if(pProtoSniff->ip.ip_p == IPPROTO_UDP)
		{
			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"udp protocol\r\n");
			pProtoSniff->udp.udph_srcport = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->udp.udph_srcport |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->udp.udph_destport = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->udp.udph_destport |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"udp sourcePort:%d destPort:%d\r\n",pProtoSniff->udp.udph_srcport,pProtoSniff->udp.udph_destport);

			pProtoSniff->udp.udph_len = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->udp.udph_len |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->udp.udph_chksum = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->udp.udph_chksum |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			(*pIndex_data) = idx;

		}
		else if(pProtoSniff->ip.ip_p == IPPROTO_TCP)
		{
			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"tcp protocol\r\n");
			pProtoSniff->tcp.src_port = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->tcp.src_port |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->tcp.dst_port = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->tcp.dst_port |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"tcp sourcePort:%d destPort:%d\r\n",pProtoSniff->tcp.src_port,pProtoSniff->tcp.dst_port);

			pProtoSniff->tcp.seq = ((ADAP_Uint32)buf[idx++] & 0xFF) << 24;
			pProtoSniff->tcp.seq |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 16;
			pProtoSniff->tcp.seq |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 8;
			pProtoSniff->tcp.seq |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 0;

			pProtoSniff->tcp.ack = ((ADAP_Uint32)buf[idx++] & 0xFF) << 24;
			pProtoSniff->tcp.ack |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 16;
			pProtoSniff->tcp.ack |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 8;
			pProtoSniff->tcp.ack |= ((ADAP_Uint32)buf[idx++] & 0xFF) << 0;


			pProtoSniff->tcp.data_offset = (buf[idx++] & 0xF0) >> 4;
			pProtoSniff->tcp.flags = buf[idx++] & 0x3F;

			pProtoSniff->tcp.window_size = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->tcp.window_size |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->tcp.checksum = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->tcp.checksum |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			pProtoSniff->tcp.urgent_p = ( (ADAP_Uint16)(buf[idx++] & 0xFF) << 8);
			pProtoSniff->tcp.urgent_p |= ( (ADAP_Uint16)(buf[idx++] & 0xFF) );

			(*pIndex_data) = idx;
		}
		return ADAP_TRUE;

	}
	return ADAP_FALSE;
}




static void *packet_rcv_packet_thread(void *arg)
{
	ADAP_UNUSED_PARAM (arg);

    int s;
    ADAP_Uint32 len;
    unsigned char buf[MAX_PACKET_LEN];
//    ADAP_Uint32 			   srcIp=0;
    tProtocl_sniff 			   tProtoSniff;
//#ifdef PRINT_PACKETS
    ADAP_Uint8		dbgPacket[80];
    ADAP_Uint32		dbglen=0;
    ADAP_Uint32		idx=0;
//#endif
    ADAP_Uint32     num_of_packets_received=0;
    ADAP_Uint32		dataIndex=0;

    ADAP_Uint32 prio = 0;
    ADAP_Uint32 logport=0;
//    ADAP_Uint32 egressIfIndex=0;
    MEA_Port_t PhyPort=0;
    //MEA_Port_t IPPhyPort;
    ADAP_Uint8 IngIntfType=0;
    ADAP_Uint16		VlanId = 0;
    ADAP_Bool managementPortSet = ADAP_FALSE;
    ADAP_UNUSED_PARAM (prio);

    s=raw_socket;

    // Checking if management was set
    if (getenv("MEA_CREATE_MANAGEMENT_PORT"))
    {
        managementPortSet = ADAP_TRUE;
    }

    while(!exit_process)
    {

    	//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"wait on socket\r\n");
        len = read(s, buf, MAX_PACKET_LEN);
        if(len<=0)
        {
        	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to read from socket\r\n");
            goto Over;
        }
    	if(adap_isProcessTerminated() == ADAP_TRUE)
    	{
    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"request to terminate\r\n");
    		goto Over;
    	}

        if(len>18)
        {

            if(buf[12]==0x91 && buf[13]==0x00 && buf[16]==0x81 && buf[17]==0x00) /*We have Ethernity-attached VLAN tag */
            {
                prio = buf[18]>>5;
                PhyPort = (buf[19])|((buf[18]&0xf)<<8);

                if (managementPortSet == ADAP_TRUE)
                    if(PhyPort == ADAP_MANAGEMENT_PORT)
                    {
                        continue;
                    }
//#ifdef PRINT_PACKETS
            	num_of_packets_received++;
		if( (num_of_packets_received % 100) == 0)
		{
			adap_memset(&dbgPacket[0],0,sizeof(dbgPacket));
			dbglen=0;
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"received packet len:%d num of packets:%d\r\n",len,num_of_packets_received);
			for(idx=0;idx<len;idx++)
			{
				dbglen += sprintf((char *)&dbgPacket[dbglen],"0x%x ",buf[idx]);
				if( ( (idx%16) == 0) && (idx!= 0) )
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"%s\r\n",dbgPacket);
					adap_memset(&dbgPacket[0],0,sizeof(dbgPacket));
					dbglen=0;
				}
			}
			if(dbglen != 0)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"%s\r\n",dbgPacket);
			}
		}
//#endif
		if (buf[14]==0x0f && buf[15]==0xab) /* Vlan 4011 for packet to CFA */
		{
			memmove(buf+12,buf+20,len-20);

			/* handle NAPT for the packet */
			if (AdapGetNATRouterStatus() == ADAP_TRUE)
			{
				if(MeaAdapGetLogPortFromPhyPort(&logport,PhyPort) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"get log port failed\r\n");
					continue;
				}

				adap_memset(&tProtoSniff,0,sizeof(tProtoSniff));

				if(get_sourceip_from_ipPacket(buf,len-4,&tProtoSniff,&dataIndex) != ADAP_TRUE)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"its not ip packet\r\n");
					continue;
				}

				MeaAdapGetIntType(&IngIntfType, logport);
				if( (tProtoSniff.ip.ip_p == IPPROTO_UDP) ||
						(tProtoSniff.ip.ip_p == IPPROTO_TCP) )
				{
					if (IngIntfType == ADAP_SET_TO_WAN)
					{
						EnetAdapHandleNATForPktFromWAN (&tProtoSniff);
					}
					else
					{
						EnetAdapHandleNATForPktFromLAN (&tProtoSniff);
					}
				}
			}
			if(rx_packet_callback_ptr!=NULL)
			{
				if(MeaAdapIsInternalPort(PhyPort) == ENET_SUCCESS)
				{
					//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"internal port:%d\r\n",PhyPort);

					adap_memset(&tProtoSniff,0,sizeof(tProtoSniff));

					if(get_sourceip_from_ipPacket(buf,len-4,&tProtoSniff,&dataIndex) != ADAP_TRUE)
					{
						//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"packet not or not to me\r\n");
					}
#ifdef	MEA_NAT_DEMO
					// need to check if need to forward ICMP from DHCP client to next router
					if(tProtoSniff.ethernet.ether_type == ADAP_IP_PROTOCOL)
					{
						if(tProtoSniff.ip.ip_p == IPPROTO_ICMP)
						{
							if(tProtoSniff.icmp.type == ADAP_ICMP_ECHO)
							{
								if( (adap_GetOutInterfaceAddrFromIpClassL3Interface(tProtoSniff.sourceIp,PhyPort,&logport) == ENET_SUCCESS) &&
										(adap_GetOutInterfaceAddrFromIpClassL3Interface(tProtoSniff.destIp,0,&egressIfIndex) == ENET_SUCCESS) )
								{
									if(calculate_IpPacket(&tProtoSniff,logport,egressIfIndex,buf,dataIndex,len-4) == MEA_TRUE)
									{
										// copy to buf
										copy_ipPacket(&tProtoSniff,buf);
										//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"send packet to interface:%d len:%d\r\n",egressIfIndex,len);

										//printf("dest mac %x-%x-%x-%x-%x-%x\r\n",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
										//printf("source mac %x-%x-%x-%x-%x-%x\r\n",buf[6],buf[7],buf[8],buf[9],buf[10],buf[11]);
										//printf("ethertype %x-%x\r\n",buf[12],buf[13]);

										MeaAdapSendPacket (buf,egressIfIndex,len-4);
										continue;
									}
								}
							}
							else if(tProtoSniff.icmp.type == ADAP_ICMP_ECHOREPLY)
							{
								if(calculate_IpPacket(&tProtoSniff,NAT_LAN_IFINDEX,NAT_LAN_IFINDEX,buf,dataIndex,len-4) == MEA_TRUE)
								{
									// copy to buf
									copy_ipPacket(&tProtoSniff,buf);
									//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"send packet to interface:%d len:%d\r\n",NAT_LAN_IFINDEX,len);

									//printf("dest mac %x-%x-%x-%x-%x-%x\r\n",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
									//printf("source mac %x-%x-%x-%x-%x-%x\r\n",buf[6],buf[7],buf[8],buf[9],buf[10],buf[11]);
									//printf("ethertype %x-%x\r\n",buf[12],buf[13]);

									MeaAdapSendPacket (buf,NAT_LAN_IFINDEX,len-4);
									continue;
								}
							}
						}
						else if( (tProtoSniff.ip.ip_p == ADAP_UDP_PROTOCOL) || (tProtoSniff.ip.ip_p == ADAP_TCP_PROTOCOL) )
						{
							if( (adap_GetOutInterfaceAddrFromIpClassL3Interface(tProtoSniff.sourceIp,PhyPort,&logport) == ENET_SUCCESS) &&
									(adap_EnatAdapGetOutInterfaceAddrFromIpClassL3Interface(tProtoSniff.destIp,0,&egressIfIndex) == ENET_SUCCESS) )
							{
								if(logport == NAT_LAN_IFINDEX)
								{
									if(check_tcp_udp_connection(&tProtoSniff,logport,egressIfIndex,buf,dataIndex,len-4) == MEA_TRUE)
									{
										copy_ipPacket(&tProtoSniff,buf);
										MeaAdapSendPacket (buf,egressIfIndex,len-4);
										continue;
									}
								}
								else
								{
									if(check_tcp_udp_connection(&tProtoSniff,NAT_LAN_IFINDEX,NAT_LAN_IFINDEX,buf,dataIndex,len-4) == MEA_TRUE)
									{
										copy_ipPacket(&tProtoSniff,buf);
										MeaAdapSendPacket (buf,NAT_LAN_IFINDEX,len-4);
										continue;
									}
								}
							}
							else
							{
								if(check_tcp_udp_connection(&tProtoSniff,NAT_LAN_IFINDEX,NAT_LAN_IFINDEX,buf,dataIndex,len-4) == MEA_TRUE)
								{
									copy_ipPacket(&tProtoSniff,buf);
									MeaAdapSendPacket (buf,NAT_LAN_IFINDEX,len-4);
									continue;
								}
							}
						}
					}
#endif
				if((buf[12]==0x81 && buf[13]==0x00) ||
					((buf[12]==0x88 && buf[13]==0xa8)))
				{
					VlanId = (buf[15])|((buf[14]&0xf)<<8);
					logport = adap_VlanGetPortByVlanId (VlanId);
				}
                		rx_packet_callback_ptr(logport,buf,len-4);
				}
				//printf("physicalport=%d\r\n",PhyPort);
				else if(MeaAdapGetLogPortFromPhyPort(&logport,PhyPort) == ENET_SUCCESS)
				{
					//printf("ifIndex=%d packet len:%d\r\n",logport,len-4);
#ifdef	MEA_NAT_DEMO
					if(logport == NAT_WAN_IFINDEX)
					{
						adap_memset(&tProtoSniff,0,sizeof(tProtoSniff));

						if(get_sourceip_from_ipPacket(buf,len-4,&tProtoSniff,&dataIndex) != ADAP_TRUE)
						{
							//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"its not ip packet\r\n");
						}

						// need to check if need to forward ICMP from DHCP client to next router
						if(tProtoSniff.ethernet.ether_type == ADAP_IP_PROTOCOL)
						{
							if(tProtoSniff.ip.ip_p == IPPROTO_ICMP)
							{
								if(tProtoSniff.icmp.type == ADAP_ICMP_ECHOREPLY)
								{
									if(calculate_IpPacket(&tProtoSniff,NAT_LAN_IFINDEX,NAT_LAN_IFINDEX,buf,dataIndex,len-4) == MEA_TRUE)
									{
										// copy to buf
										copy_ipPacket(&tProtoSniff,buf);
										//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"send icmp reply to interface:%d len:%d\r\n",NAT_LAN_IFINDEX,len);

										//printf("dest mac %x-%x-%x-%x-%x-%x\r\n",buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]);
										//printf("source mac %x-%x-%x-%x-%x-%x\r\n",buf[6],buf[7],buf[8],buf[9],buf[10],buf[11]);
										//printf("ethertype %x-%x\r\n",buf[12],buf[13]);

										MeaAdapSendPacket (buf,NAT_LAN_IFINDEX,len-4);
										continue;
									}
								}
							}
							else if( (tProtoSniff.ip.ip_p == ADAP_UDP_PROTOCOL) || (tProtoSniff.ip.ip_p == ADAP_TCP_PROTOCOL) )
							{
								if(check_tcp_udp_connection(&tProtoSniff,NAT_LAN_IFINDEX,NAT_LAN_IFINDEX,buf,dataIndex,len-4) == MEA_TRUE)
								{
									copy_ipPacket(&tProtoSniff,buf);
									MeaAdapSendPacket (buf,NAT_LAN_IFINDEX,len-4);
									continue;
								}

							}
						}
					}
#endif
					rx_packet_callback_ptr(logport,buf,len-4);
				}
				else
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_WARNING,"can't get logical port from phy port\r\n");
				}
			}
		}
	    }
	    else
	    {
		    //ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"packet receive untagged\n");
	    }
	}
        else
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Wrong packet length received: len\n");
        }
    }

Over:
    ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"packet_task_thread finished\n");


    close(s);
    return NULL;
}



ADAP_Uint32 init_socket_task(void)
{
    int res;
    int s;
    struct ifreq eth_req;
    struct sockaddr_ll addr;
    int maxbuf = MAX_BUFF_FRAGMENT;


    s=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL));
    //s=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_8021Q));
    if(s<0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error openning RAW socket\n");
        return ENET_FAILURE;
    }

    //strcpy(eth_req.ifr_name, ETH_NAME);
    if(MEA_device_environment_info.MEA_DEVICE_CPU_ETH_enable == MEA_TRUE)
    {
        strncpy(eth_req.ifr_name,
            &MEA_device_environment_info.MEA_DEVICE_CPU_ETH[0], /*&device_cpu127[0],*/
            sizeof(eth_req.ifr_name));
        MEA_OS_LOG_logMsg(MEA_OS_LOG_EVENT,"------ ifr_name %s -----\n",eth_req.ifr_name);

    }else{
        MEA_OS_LOG_logMsg(MEA_OS_LOG_ERROR,"The cpu is not set the ethX  use the MEA_DEVICE_CPU_ETH\n");
        return MEA_ERROR;
    }

    if (ioctl(s, SIOCGIFINDEX, &eth_req) < 0) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error getting ifindex for %s\n",eth_req.ifr_name);
        return ENET_FAILURE;
    }
    eth_ifindex=eth_req.ifr_ifindex;

    if (ioctl(s, SIOCGIFFLAGS, &eth_req) < 0) {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error binding RAW socket to %s\n",eth_req.ifr_name);
        return ENET_FAILURE;
    }

    eth_req.ifr_flags |= IFF_PROMISC;
    if (ioctl(s, SIOCSIFFLAGS, &eth_req) < 0)
    {
        return ENET_FAILURE;
    }

    adap_memset(&addr,0, sizeof(addr));
    addr.sll_family=AF_PACKET;
    addr.sll_protocol=htons(ETH_P_ALL);
    addr.sll_ifindex = eth_ifindex;


    res = bind(s, (struct sockaddr*)(&addr), sizeof(addr));
    if(res!=0)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error binding raw socket: res=%i\n",res);
    }

    if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, &maxbuf, sizeof(int)) == -1) 
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error setting sockopt for rcv buf:%s\n",strerror(errno));
    }
    raw_socket=s;

    raw_socket_configured=1;

    adap_task_id taskId;
	EnetHal_Status_t status = adap_task_manager_run(packet_rcv_packet_thread, NULL, &taskId);
	if(status != ENETHAL_STATUS_SUCCESS)	{
		printf("failed to run rcv packet task (status = %d)\n", status);
	    return ENET_FAILURE;
	}
	return ENET_SUCCESS;
}


ADAP_Uint32 EnetAdapHandleNATForPktFromWAN (tProtocl_sniff *pProtoSniff)
{
        MEA_Action_t            action_Id = MEA_PLAT_GENERATE_NEW_ID;
        MEA_Port_t              PhyPort;
        ADAP_Uint32             logPort = 0;
        tIpDestV4Routing        *pIpv4Routing=NULL;
        tEnetHal_MacAddr 		daMac;
	MEA_PmId_t              pmId;
	ADAP_Uint32		destPort = 0;
	ADAP_Uint16		VlanId = 0;
	tNaptAddressTable 	*pNatEntry = NULL;

    adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	if (pProtoSniff->ip.ip_p == IPPROTO_TCP)
	{
		destPort = pProtoSniff->tcp.dst_port;
	}
	else if (pProtoSniff->ip.ip_p == IPPROTO_TCP)
	{
		destPort = pProtoSniff->udp.udph_destport;
	}

	if (MeaDrvHwGetNatForwarder(pProtoSniff->destIp, destPort, VPN_NETWORK_TO_HOST,
				MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"NAT Forwarder entry exists already \n");
		return ENET_SUCCESS;
	}

	/* Get NAT entry from static mapping */
	pNatEntry = AdapGetNatEntryFromNetworktoHost (pProtoSniff->destIp, destPort);
	if (pNatEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to get static NAT entry \n");
		return ENET_FAILURE;
	}

	/* Get Next hop MAC */
	if(AdapGetEntryIpTable(&pIpv4Routing,pNatEntry->localIpAddr) == ENET_SUCCESS)
	{
		adap_memcpy(&daMac,&pIpv4Routing->macAddress,sizeof(tEnetHal_MacAddr));
	}

	adap_GetVlanAddrFromIpClassL3Interface (pNatEntry->localIpAddr, &VlanId);

	logPort = adap_VlanGetPortByVlanId (VlanId);
	MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);

	if(MeaDrvNatCreateAction(&action_Id, &daMac, pNatEntry->localIpAddr, pNatEntry->localPort, MEA_FALSE,PhyPort,&pmId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call MeaDrvNatCreateAction \n");
		return ENET_FAILURE;
	}

	if(MeaDrvHwCreateNatForwarder(action_Id, pProtoSniff->destIp, destPort,
				VPN_NETWORK_TO_HOST, PhyPort,
				MEA_SE_FORWARDING_KEY_TYPE_IP_DEST_PLUS_L4_DEST_PORT_PLUS_VPN) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call MeaDrvNatCreateAction \n");
		return ENET_FAILURE;
	}
	
	return ENET_SUCCESS;
}


ADAP_Uint32 EnetAdapHandleNATForPktFromLAN (tProtocl_sniff *pProtoSniff)
{
        MEA_Action_t            action_Id = MEA_PLAT_GENERATE_NEW_ID;
        MEA_Port_t              PhyPort;
        ADAP_Uint32             logPort = 0;
        tIpDestV4Routing        *pIpv4Routing=NULL;
        tEnetHal_MacAddr 		daMac;
	MEA_PmId_t              pmId;
	ADAP_Uint32		srcPort = 0;
	ADAP_Uint16		VlanId = 0;
	tNaptAddressTable 	*pNatEntry = NULL;

    adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	if (pProtoSniff->ip.ip_p == IPPROTO_TCP)
	{
		srcPort = pProtoSniff->tcp.src_port;
	}
	else if (pProtoSniff->ip.ip_p == IPPROTO_UDP)
	{
		srcPort = pProtoSniff->udp.udph_srcport;
	}

	if (MeaDrvHwGetNatForwarder(pProtoSniff->sourceIp, srcPort, VPN_HOST_TO_NETWORK,
				MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) == MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"NAT Forwarder entry exists already \n");
		return ENET_SUCCESS;
	}

	/* Get NAT entry from static mapping */
	pNatEntry = AdapGetNatEntryFromHosttoNetwork (pProtoSniff->sourceIp, srcPort);
	if (pNatEntry == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to get static NAT entry \n");
		return ENET_FAILURE;
	}

	/* Get Next hop MAC */
	if(AdapGetEntryIpTable(&pIpv4Routing,pProtoSniff->destIp) == ENET_SUCCESS)
	{
		adap_memcpy(&daMac,&pIpv4Routing->macAddress,sizeof(tEnetHal_MacAddr));
	}

	adap_GetVlanAddrFromIpClassL3Interface (pNatEntry->globalIpAddr, &VlanId);

	logPort = adap_VlanGetPortByVlanId (VlanId);
	MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);

	if(MeaDrvNatCreateAction(&action_Id, &daMac, pNatEntry->globalIpAddr, pNatEntry->globalPort, MEA_TRUE,PhyPort,&pmId) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call MeaDrvNatCreateAction \n");
		return ENET_FAILURE;
	}

	if(MeaDrvHwCreateNatForwarder(action_Id, pProtoSniff->sourceIp, srcPort,
				VPN_HOST_TO_NETWORK, PhyPort,
				MEA_SE_FORWARDING_KEY_TYPE_IP_SOURCE_PLUS_L4_SOURCE_PORT_PLUS_VPN) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call MeaDrvNatCreateAction \n");
		return ENET_FAILURE;
	}
	
	return ENET_SUCCESS;
}
