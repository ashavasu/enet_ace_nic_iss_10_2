/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/
#include  <string.h>

#include "mea_api.h"
#include "adap_types.h"
#include "adap_logger.h"
#include "adap_linklist.h"
#include "adap_database.h"
#include "adap_utils.h"
#include "adap_vlanminp.h"
#include "adap_acl.h"
#include "adap_service_config.h"
#include "adap_search_engine.h"
#include "adap_ipnp.h"
#include "adap_enetConvert.h"
#include "adap_lpm.h"
#include "adap_pppnp.h"
#include "EnetHal_L3_Api.h"


static ADAP_Uint8 ForwardingStatusEnable=ADAP_FNP_FORW_ENABLE;
static ADAP_Uint32 rip_multicast_ip = 0xE0000009; /*224.0.0.9 */

ADAP_Uint32 adap_GetVlanAddrFromIpClassL3Interface(ADAP_Uint32 ipAddr,ADAP_Uint16 *pVlanId)
{
	tIpInterfaceTable *routerParams=NULL;
	tBridgeDomain *pBridgeDomain=NULL;
	(*pVlanId)=0;

	routerParams = AdapGetFirstIpInterface();
	while(routerParams != NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"subnet1 0x%x\n",routerParams->MyIpAddr.ipAddr & routerParams->MyIpAddr.subnetMask);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"subnet2 0x%x\n",ipAddr & routerParams->MyIpAddr.subnetMask);
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"u2VlanId:%d\n",routerParams->u2VlanId);
		if( (routerParams->MyIpAddr.ipAddr & routerParams->MyIpAddr.subnetMask) ==
			(ipAddr & routerParams->MyIpAddr.subnetMask) )
		{
			Adap_bridgeDomain_semLock();
			pBridgeDomain = Adap_getBridgeDomainByVlan(routerParams->u2VlanId,0);

			if(pBridgeDomain != NULL)
			{
				(*pVlanId)=routerParams->u2VlanId;
			}
			Adap_bridgeDomain_semUnlock();
			return ENET_SUCCESS;
		}
		routerParams = AdapGetNextIpInterface(routerParams->u4CfaIfIndex);
	}
	return ENET_SUCCESS;
}

ADAP_Uint32 adap_GetOutInterfaceAddrFromIpClassL3Interface(ADAP_Uint32 ipAddr,ADAP_Uint16 internalPort,ADAP_Uint32 *pIfIndex)
{
//	ADAP_Uint32 idx=0;
//	ADAP_Uint32 nextHopIdx=0;
//	ADAP_Uint32 minIpAddr=0;
//	ADAP_Uint32 maxIpAddr=0;
//	ADAP_Uint32 mask=0;
//	ADAP_Uint32 nextHopIpAddr=0;
//	ADAP_Uint32 Mask=0;
	tIpInterfaceTable *routerParams=NULL;
	tIpV4NextRoute	  *nextRouter;

	ADAP_UNUSED_PARAM (internalPort);

	(*pIfIndex) = 0;

#if 0
	if(MeaAdapGetInternalPortMask(internalPort,&Mask) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call MeaAdapGetInternalPortMask\n");
		return ENET_FAILURE;
	}


	if(Mask == D_IP_MASK_24)
	{
		mask = ipAddr & 0xFFFFFF00;
	}
	else if(Mask == D_IP_MASK_16)
	{
		mask = ipAddr & 0xFFFF0000;
	}
	else
	{
		mask = ipAddr & 0xFF000000;
	}
#endif
	routerParams = AdapGetFirstIpInterface();
	while(routerParams != NULL)
	{
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"found interface ipAddr:%x subnetMask:%x ipAddr:%x\r\n",
//									routerParams->MyIpAddr.ipAddr,
//									routerParams->MyIpAddr.subnetMask,
//									ipAddr);
		if( (routerParams->MyIpAddr.ipAddr & routerParams->MyIpAddr.subnetMask) ==
			(ipAddr & routerParams->MyIpAddr.subnetMask) )
		{
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"found interface:%d\r\n",routerParams->u4CfaIfIndex);
			if(getPhyFromCfaInterface(routerParams->u4CfaIfIndex,pIfIndex) != ENET_SUCCESS)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call getPhyFromCfaInterface\n");
				return ENET_FAILURE;
			}
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," source ifIndex:%d\n",(*pIfIndex));
			return ENET_SUCCESS;
		}
		routerParams = AdapGetNextIpInterface(routerParams->u4CfaIfIndex);
	}

	nextRouter = AdapGetFirstIpNextRouteTable();
	while(nextRouter != NULL)
	{
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"found next router ipAddr:%x subnetMask:%x ipAddr:%x\r\n",
//									nextRouter->u4IpDestAddr,
//									nextRouter->u4IpSubNetMask,
//									ipAddr);

		if( (nextRouter->u4IpDestAddr & nextRouter->u4IpSubNetMask) ==
			(ipAddr & nextRouter->u4IpSubNetMask) )
		{
//			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"found interface:%d\r\n",nextRouter->u4IfIndex);
			if(nextRouter->u4NextHopGt == 0)
			{
				(*pIfIndex) = nextRouter->u4IfIndex;
//				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," source ifIndex:%d\n",(*pIfIndex));
				return ENET_SUCCESS;
			}
			else
			{
				if(getPhyFromCfaInterface(nextRouter->u4IfIndex,pIfIndex) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed by call getPhyFromCfaInterface for defaultGateway:%d\n",nextRouter->u4IfIndex);
					return ENET_FAILURE;
				}
				//ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," interface id:%d\n",(*pIfIndex));
			}
		}
		nextRouter = AdapGetNextIpNextRouteTable(nextRouter);
	}

	(*pIfIndex) = NAT_WAN_IFINDEX;
	return ENET_SUCCESS;
}

ADAP_Uint32 MiFiHwCreateRouterEhp(MEA_EHP_Info_dbt *pEhp_info,tEnetHal_MacAddr *pSrcMac,tEnetHal_MacAddr *destMac,ADAP_Uint32 command,
					ADAP_Uint32 vlanId, ADAP_Uint32 InnervlanId)
{
	pEhp_info->ehp_data.martini_info.EtherType = 0;
//	pEhp_info->ehp_data.martini_info.martini_cmd = MEA_EGRESS_HEADER_PROC_CMD_SWAP;



	adap_mac_to_arr(pSrcMac, pEhp_info->ehp_data.martini_info.SA.b);
	adap_mac_to_arr(destMac, pEhp_info->ehp_data.martini_info.DA.b);


	//pEhp_info->ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
	pEhp_info->ehp_data.eth_info.command           = command;
//	pEhp_info->ehp_data.eth_info.val.vlan.eth_type = 0x8100;
//	pEhp_info->ehp_data.eth_info.val.vlan.vid      = 0;
	pEhp_info->ehp_data.eth_info.val.all      = vlanId;

	pEhp_info->ehp_data.LmCounterId_info.valid = MEA_TRUE;
	//pEhp_info->ehp_data.LmCounterId_info.LmId = srcInfo->lmId;
	pEhp_info->ehp_data.LmCounterId_info.Command_dasa=1;
	//pEhp_info->ehp_data.LmCounterId_info.Command_cfm=MEA_LM_COMMAND_TYPE_STAMP_TX;


//	pEhp_info->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
	if (InnervlanId != 0)
	{
		pEhp_info->ehp_data.atm_info.double_tag_cmd = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
		pEhp_info->ehp_data.atm_info.val.all = vlanId;
		pEhp_info->ehp_data.eth_info.val.all = ADAP_ETHERTYPE | InnervlanId;
	}

	pEhp_info->ehp_data.eth_info.stamp_color       = MEA_FALSE;
	pEhp_info->ehp_data.eth_info.stamp_priority    = MEA_FALSE;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"src_mac:%s dest_mac:%s\n",
			adap_mac_to_string(pSrcMac), adap_mac_to_string(destMac));
	return ENET_SUCCESS;
}


ADAP_Uint32 MiFiDivideIpClass(ADAP_Uint32 u4IpDestAddr,ADAP_Uint32 u4IpSubNetMask,ADAP_Uint32 *pIpClass,ADAP_Uint32 *pIpAddr,ADAP_Uint32 *pMaxNumTable)
{
	ADAP_Uint32 ipAddr;
	ADAP_Uint32 idx;
	ADAP_Uint32 routeIdx=(*pMaxNumTable);

	(*pMaxNumTable) = 0;
	if( (0xFFFFFFFF-u4IpSubNetMask) < 0x000000FF)
	{
		// class c
		(*pIpClass) = D_IP_MASK_32;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4UcAddRoute mask 32\n");
	}
	else if((0xFFFFFFFF-u4IpSubNetMask) < 0x0000FFFF)
	{
		// class b
		(*pIpClass) = D_IP_MASK_24;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4UcAddRoute mask 24\n");
	}
	else if((0xFFFFFFFF-u4IpSubNetMask) < 0x00FFFFFF)
	{
		// class a
		(*pIpClass) = D_IP_MASK_16;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4UcAddRoute mask 16\n");
	}

	else
	{
		(*pIpClass) = D_IP_MASK_08;
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4UcAddRoute mask 8\n");
	}

	ipAddr=u4IpDestAddr;
	for(idx=0;idx<(0xFFFFFFFF-u4IpSubNetMask);idx+=(*pIpClass) )
	{
		if((*pIpClass) == D_IP_MASK_32)
		{
			if( ( (ipAddr & 0x000000FF) == 0) || ((ipAddr & 0x000000FF) == 0xFF) )
			{
				ipAddr+=(*pIpClass);
				continue;
			}
		}
		if( (*pMaxNumTable) < routeIdx)
		{
			pIpAddr[(*pMaxNumTable)++] = ipAddr;
			ipAddr+=(*pIpClass);
		}
		else
		{
			break;
		}
	}
	if (((*pIpClass) == D_IP_MASK_32) && (*pMaxNumTable == 0))
        {
             pIpAddr[(*pMaxNumTable)] = ipAddr;     
            *pMaxNumTable = 1;
        }
	 return ENET_SUCCESS;
}



void EnetHal_FsNpIpInit (void)
{
    MEA_Port_t PhyPort=0;
    ADAP_Uint32 issPort;
    ADAP_Uint8 ret = ENET_SUCCESS;
    MEA_LxCp_t lxcp_Id=0;
    ADAP_Uint32 idx=0;

    adapIpInit();

    for(issPort=1; issPort<MeaAdapGetNumOfPhyPorts(); issPort++)
    {
        if(MeaAdapGetPhyPortFromLogPort(issPort,&PhyPort) != ENET_SUCCESS)
	{
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by call MeaAdapGetPhyPortFromLogPort\r\n");
        }
        MeaAdapsetIngressPortGlobalMac(PhyPort,ADAP_TRUE);
    }
    /* LXCP for DHCP client IP address allocation */
    for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
    {
        ret = getLxcpId(idx,&lxcp_Id);
        if(ret == ENET_SUCCESS)
        {
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
            MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
        }
    }
    /* set internal port */
    MeaAdapSetEgressPort(INTERNAL_PORT1_NUMBER,ADAP_TRUE,ADAP_TRUE);
    MeaAdapSetIngressPort(INTERNAL_PORT1_NUMBER,ADAP_TRUE,ADAP_FALSE);
    MeaAdapsetIngressPortGlobalMac(INTERNAL_PORT1_NUMBER,ADAP_TRUE);
    MeaAdapSetEgressPort(INTERNAL_PORT2_NUMBER,ADAP_TRUE,ADAP_TRUE);
    MeaAdapSetIngressPort(INTERNAL_PORT2_NUMBER,ADAP_TRUE,ADAP_FALSE);
    MeaAdapsetIngressPortGlobalMac(INTERNAL_PORT2_NUMBER,ADAP_TRUE);
    MeaAdapSetEgressPort(INTERNAL_PORT3_NUMBER,ADAP_TRUE,ADAP_TRUE);
    MeaAdapSetIngressPort(INTERNAL_PORT3_NUMBER,ADAP_TRUE,ADAP_FALSE);
    MeaAdapsetIngressPortGlobalMac(INTERNAL_PORT3_NUMBER,ADAP_TRUE);
    return;
}

ADAP_Uint8 EnetHal_FsNpOspfInit (void)
{
	MEA_OutPorts_Entry_dbt tOutPorts;
	tIpInterfaceTable *pInterface=NULL;
	MEA_Action_t		action=MEA_PLAT_GENERATE_NEW_ID;
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

	action = AdapGetRouteActionToCpu();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"got actionId:%d\n",action);
	if(action == MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MeaDrvCreateActionToCpu(&action) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create action\n");
			return ENET_FAILURE;
		}
		else
		{
			AdapSetRouteActionToCpu(action);
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"actionId:%d created\n",action);
	}

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	MEA_SET_OUTPORT(&tOutPorts, 127);

	if(MeaDrvHwCreateDestMacForwarder(action,MEA_TRUE,AdapGetOspfMacMulticastAllRoutes(),START_L3_VPN,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create DestMacForwarder\n");
		return ENET_FAILURE;
	}

	if(MeaDrvHwCreateDestMacForwarder(action,MEA_TRUE,AdapGetOspfMacMulticastAllDrs(),START_L3_VPN,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create DestMacForwarder\n");
		return ENET_FAILURE;
	}

	// check if interface VLAN configured
	pInterface = AdapGetFirstIpInterface();
	while(pInterface != NULL)
	{
		adap_ReconfigureHwVlan (pInterface->u2VlanId,0);
		pInterface = AdapGetNextIpInterface(pInterface->ifIndex);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		ret = getLxcpId(idx,&lxcp_Id);
		if(ret == ENET_SUCCESS)
		{
			ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_IN_OSPF,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		}
	}

    return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpOspfDeInit (void)
{
	return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpDhcpSrvInit (void)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		ret = getLxcpId(idx,&lxcp_Id);
		if(ret != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
			break;
		}

		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

    return ret;
}

ADAP_Uint8 EnetHal_FsNpMLDEnable (void)
{
    ADAP_Uint8 ret = ENET_SUCCESS;
    MEA_LxCp_t lxcp_Id=0;
    ADAP_Uint32 idx=0;

    for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
    {
        ret = getLxcpId(idx,&lxcp_Id);
        if(ret != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
            break;
        }

        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ICMPoV6,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
    }

    return ret;
}

ADAP_Uint8 EnetHal_FsNpMLDDisable (void)
{
    ADAP_Uint8 ret = ENET_SUCCESS;
    MEA_LxCp_t lxcp_Id=0;
    ADAP_Uint32 idx=0;

    for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
    {
        ret = getLxcpId(idx,&lxcp_Id);
        if(ret != ENET_SUCCESS)
        {
            ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
            break;
        }

        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ICMPoV6,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
    }

    return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Device_DhcpSrv_DeInit (void)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		ret = getLxcpId(idx,&lxcp_Id);
		if(ret != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed by getLxcpId\r\n");
			break;
		}

		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
	}

    return ret;
}

ADAP_Uint8 EnetHal_FsNpRipInit(void)
{
	MEA_OutPorts_Entry_dbt tOutPorts;
	tIpInterfaceTable *pInterface=NULL;
	MEA_Action_t		action=MEA_PLAT_GENERATE_NEW_ID;
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

	action = AdapGetRouteActionToCpu();
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"got actionId:%d\n",action);
	if(action == MEA_PLAT_GENERATE_NEW_ID)
	{
		if(MeaDrvCreateActionToCpu(&action) != MEA_OK)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create action\n");
			return ENET_FAILURE;
		}
		else
		{
			AdapSetRouteActionToCpu(action);
		}
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"actionId:%d created\n",action);
	}

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
	MEA_SET_OUTPORT(&tOutPorts, 127);

	if(MeaDrvHwCreateDestMacForwarder(action,MEA_TRUE,AdapGetRipMacMulticastAllRoutes(),START_L3_VPN,&tOutPorts) != MEA_OK)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create DestIpForwarder\n");
		return ENET_FAILURE;
	}

	MeaDrvSetGlobalIpAddress(rip_multicast_ip,MEA_TRUE);

	// check if interface VLAN configured
	pInterface = AdapGetFirstIpInterface();
	while(pInterface != NULL)
	{
		adap_ReconfigureHwVlan (pInterface->u2VlanId,0);
		pInterface = AdapGetNextIpInterface(pInterface->ifIndex);
	}

	for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
	{
		ret = getLxcpId(idx,&lxcp_Id);
		if(ret == ENET_SUCCESS)
		{
			ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_RIP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		}
	}

	return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpRipDeInit (void)
{
	return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpDhcpRlyInit (void)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

        for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
        {
                ret = getLxcpId(idx,&lxcp_Id);
                if(ret == ENET_SUCCESS)
                {
                        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
                        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
                }
        }

	return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Device_DhcpRly_DeInit (void)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;
	ADAP_Uint32 idx=0;

        for(idx=1;idx<MeaAdapGetNumOfPhyPorts();idx++)
        {
                ret = getLxcpId(idx,&lxcp_Id);
                if(ret == ENET_SUCCESS)
                {
                        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
                        ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
                }
        }

	return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpRly_Init (ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;

	ret = getLxcpId(u4IfIndex,&lxcp_Id);
	if(ret == ENET_SUCCESS)
	{
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

	return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpSrv_Init (ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;

	ret = getLxcpId(u4IfIndex,&lxcp_Id);
	if(ret == ENET_SUCCESS)
	{
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
	}

	return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpRly_DeInit (ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;

	ret = getLxcpId(u4IfIndex,&lxcp_Id);
	if(ret == ENET_SUCCESS)
	{
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_CLIENT,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
	}

	return ret;
}

ADAP_Uint8 EnetHal_CtrlProto_Port_DhcpSrv_DeInit (ADAP_Uint32 u4IfIndex)
{
	ADAP_Uint8 ret = ENET_SUCCESS;
	MEA_LxCp_t lxcp_Id=0;

	ret = getLxcpId(u4IfIndex,&lxcp_Id);
	if(ret == ENET_SUCCESS)
	{
		ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_DHCP_SERVER,MEA_LXCP_PROTOCOL_ACTION_TRANSPARENT,lxcp_Id,NULL,NULL);
	}

	return ret;
}

ADAP_Uint32 SetArpToCpuConfiguration(tBridgeDomain *pBridgeDomain, ADAP_Uint32 u4IpAddr,
                                    ADAP_Uint16 u2VlanId)
{
    ADAP_Uint32 ret = ENET_SUCCESS;
    int idx;
    ADAP_Uint16 portMap[MAX_NUM_OF_SUPPORTED_PORTS];
    MeaDrvSetGlobalIpAddress(u4IpAddr, MEA_TRUE);

    memset(portMap, 0, sizeof(portMap));
    for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
        if(pBridgeDomain->ifType[idx] != 0)
        {
           portMap[pBridgeDomain->ifIndex[idx]] = 1;
        }

    ret = ConfigureArpRedirection(u2VlanId, portMap,
                ADAP_TRUE /* send to cpu */);

    if (ret != ENET_SUCCESS)
    {
        ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"Error during ARP configuration. VlanId = %d\n", u2VlanId);
        return ENET_FAILURE;
    }

    return ret;
}

ADAP_Uint32 EnetHal_FsNpIpv4CreateIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4CfaIfIndex,
											ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
											ADAP_Uint16 u2VlanId, ADAP_Uint8 *au1MacAddr)
{
	ADAP_UNUSED_PARAM (u4VrId);
	ADAP_UNUSED_PARAM (pu1IfName);
	ADAP_UNUSED_PARAM (u4IpAddr);
	ADAP_UNUSED_PARAM (u4IpSubNetMask);
	ADAP_UNUSED_PARAM (u4CfaIfIndex);
	ADAP_UNUSED_PARAM (u2VlanId);
	ADAP_UNUSED_PARAM (au1MacAddr);

	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	//ADAP_Uint32 ifIndex=0;
	char ipAddr[30];
	char netAddr[30];
	MEA_LxCp_t lxcp_Id=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

	// init variables
	adap_memset(&ipAddr[0],0,sizeof(ipAddr));
	adap_memset(&netAddr[0],0,sizeof(netAddr));


	/***********************  start of debug print out ************************/
	sprintf(ipAddr,"%d.%d.%d.%d",( (u4IpAddr & 0xFF000000) >> 24),
								 ( (u4IpAddr & 0x00FF0000) >> 16),
								 ( (u4IpAddr & 0x0000FF00) >> 8),
								 ( (u4IpAddr & 0x000000FF)) );

	sprintf(netAddr,"%d.%d.%d.%d",( (u4IpSubNetMask & 0xFF000000) >> 24),
								 ( (u4IpSubNetMask & 0x00FF0000) >> 16),
								 ( (u4IpSubNetMask & 0x0000FF00) >> 8),
								 ( (u4IpSubNetMask & 0x000000FF)) );


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pu1IfName:%s\n",pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4CfaIfIndex:%d\n",u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%s\n",ipAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpSubNetMask:%s\n",netAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	/**************************************  end of print out *******************/

    	pInterfaceTable = AdapAllocateIpInterface(u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		pInterfaceTable->MyIpAddr.ipAddr = u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = u2VlanId;
		pInterfaceTable->u4CfaIfIndex = u4CfaIfIndex;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pu1IfName,20);

		//getVLanInterface
		pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);

		if(pBridgeDomain == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pBridgeDomain not found for vlanId:%d not exist\n",u2VlanId);
		}
		else
		{
			if(strncmp((const char *)pu1IfName,"loopback",8) == 0)
			{
				/* loopback interface not installed in ENET */
				return ENET_SUCCESS;
			}

			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," interface :%d vlan:%d configured\n",pBridgeDomain->ifIndex[idx],u2VlanId);
					MeaDrvSetGlobalIpAddress(u4IpAddr,MEA_TRUE);
					ret = getLxcpId(pBridgeDomain->ifIndex[idx],&lxcp_Id);
					if(ret == ENET_SUCCESS)
					{
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						//ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ICMPoV4,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					}
				}
			}
			// need to move the interface to L3
			ret = adap_ReconfigureHwVlan (u2VlanId,0);
		}

	}

    return ret;
}

ADAP_Uint32 EnetHal_FsNpIpv4ModifyIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4CfaIfIndex,
											ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
											ADAP_Uint16 u2VlanId, ADAP_Uint8 *au1MacAddr)
{
	ADAP_UNUSED_PARAM (u4VrId);
	ADAP_UNUSED_PARAM (pu1IfName);
	ADAP_UNUSED_PARAM (u4IpAddr);
	ADAP_UNUSED_PARAM (u4IpSubNetMask);
	ADAP_UNUSED_PARAM (u4CfaIfIndex);
	ADAP_UNUSED_PARAM (u2VlanId);
	ADAP_UNUSED_PARAM (au1MacAddr);


	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	//ADAP_Uint32 ifIndex=0;
	char ipAddr[30];
	char netAddr[30];
	MEA_LxCp_t lxcp_Id=0;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx;
        tEnetHal_FsNpL3IfInfo *pRouterPort=NULL;

	// init variables
	adap_memset(&ipAddr[0],0,sizeof(ipAddr));
	adap_memset(&netAddr[0],0,sizeof(netAddr));


	/***********************  start of debug print out ************************/
	sprintf(ipAddr,"%d.%d.%d.%d",( (u4IpAddr & 0xFF000000) >> 24),
								 ( (u4IpAddr & 0x00FF0000) >> 16),
								 ( (u4IpAddr & 0x0000FF00) >> 8),
								 ( (u4IpAddr & 0x000000FF)) );

	sprintf(netAddr,"%d.%d.%d.%d",( (u4IpSubNetMask & 0xFF000000) >> 24),
								 ( (u4IpSubNetMask & 0x00FF0000) >> 16),
								 ( (u4IpSubNetMask & 0x0000FF00) >> 8),
								 ( (u4IpSubNetMask & 0x000000FF)) );


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pu1IfName:%s\n",pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4CfaIfIndex:%d\n",u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%s\n",ipAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpSubNetMask:%s\n",netAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	/**************************************  end of print out *******************/

        pInterfaceTable = AdapGetIpInterface(u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		if (u2VlanId == 0)
		{
			pRouterPort = getRouterPortInfo(u4CfaIfIndex);
			if (pRouterPort != NULL)
			{
				u2VlanId = pRouterPort->u2PortVlanId;
			}
		}            

		MeaDrvSetGlobalIpAddress(pInterfaceTable->MyIpAddr.ipAddr,MEA_FALSE);

		pInterfaceTable->MyIpAddr.ipAddr = u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = u2VlanId;
		pInterfaceTable->u4CfaIfIndex = u4CfaIfIndex;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pu1IfName,20);

		pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);

		if(pBridgeDomain == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pBridgeDomain not found for vlanId:%d not exist\n",u2VlanId);
		}
		else
		{
			if(strncmp((const char *)pu1IfName,"loopback",8) == 0)
			{
				/* loopback interface not installed in ENET */
				return ENET_SUCCESS;
			}

			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vlan:%d num of ports:%d\n",u2VlanId,pBridgeDomain->num_of_ports);
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," interface :%d vlan:%d configured\n",pBridgeDomain->ifIndex[idx],u2VlanId);
					MeaDrvSetGlobalIpAddress(u4IpAddr,MEA_TRUE);
					ret = getLxcpId(pBridgeDomain->ifIndex[idx],&lxcp_Id);
					if(ret == ENET_SUCCESS)
					{
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						//ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ICMPoV4,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					}
				}
			}
			// need to move the interface to L3
			ret = adap_ReconfigureHwVlan (u2VlanId,0);
		}

	}

    return ret;
}


ADAP_Uint32 EnetHal_FsNpIpv4DeleteIpInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
											ADAP_Uint32 u4IfIndex, ADAP_Uint16 u2VlanId)
{
	ADAP_UNUSED_PARAM (u4VrId);
	ADAP_UNUSED_PARAM (pu1IfName);
	ADAP_UNUSED_PARAM (u4IfIndex);
	ADAP_UNUSED_PARAM (u2VlanId);

	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	//ADAP_Uint32 ifIndex=0;
	//MEA_LxCp_t lxcp_Id=0;





	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pu1IfName:%s\n",pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4CfaIfIndex:%d\n",u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	/**************************************  end of print out *******************/


    pInterfaceTable = AdapGetIpInterface(u4IfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4IfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		MeaDrvSetGlobalIpAddress(pInterfaceTable->MyIpAddr.ipAddr,MEA_FALSE);
		ret = AdapDeleteIpInterface(u4IfIndex);
	}


    return ret;
}

ADAP_Int32 adap_FsNpIpv4SetForwardingStatus (ADAP_Uint32 u4VrId, ADAP_Uint8 u1Status)
{
     MEA_SE_Entry_key_dbt arrKeyAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
     ADAP_Uint32 numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
     ADAP_Uint32 idx;
	tIpInterfaceTable *pIpInterface=NULL;
	ADAP_Uint32 arrIpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
	ADAP_Uint32 IpClass;
	MEA_OutPorts_Entry_dbt tOutPorts;
	 MEA_LxCp_t            LxCp_Id;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));

    ForwardingStatusEnable=u1Status;

    if(u1Status==ADAP_FNP_FORW_ENABLE)
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsNpIpv4SetForwardingStatus status:enable\n");
    	pIpInterface = AdapGetFirstIpInterface();
    	while(pIpInterface)
    	{
    		numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
			MiFiDivideIpClass(pIpInterface->MyIpAddr.ipAddr,pIpInterface->MyIpAddr.subnetMask,&IpClass,&arrIpAddr[0],&numOfEntries);

			for(idx=0;idx<numOfEntries;idx++)
			{
				if(arrIpAddr[idx] == pIpInterface->MyIpAddr.ipAddr)
				{
					continue;
				}
				adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
				if(MeaDrvHwCreateDestIpForwarder(0,MEA_FALSE,arrIpAddr[idx],IpClass,START_L3_VPN,&tOutPorts) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"failed to create DestIpForwarder\n");
					return ENET_FAILURE;
				}
			}
			if(pIpInterface->ifIndex != ADAP_END_OF_TABLE)
			{
				if(getLxcpId(pIpInterface->ifIndex,&LxCp_Id) != ENET_SUCCESS)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lxcp id from database ifIndex:%d\r\n",pIpInterface->ifIndex);
					return ENET_FAILURE;
				}

				MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,MEA_LXCP_PROTOCOL_ACTION_CPU,LxCp_Id,NULL,NULL);
			}
			pIpInterface = AdapGetNextIpInterface(pIpInterface->ifIndex);
    	}
    }
    else
    {
    	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsNpIpv4SetForwardingStatus status:disable\n");

    	MeaDrvHwGetAllDestIpForwarder(&arrKeyAddr[0],&numOfEntries);
    	for(idx=0;idx<numOfEntries;idx++)
    	{
    		MeaDrvHwDeleteDestIpForwarder(arrKeyAddr[idx].DstIPv4_plus_L4_DstPort_plus_vpn.IPv4,
    									arrKeyAddr[idx].DstIPv4_plus_L4_DstPort_plus_vpn.mask_type,
    									arrKeyAddr[idx].DstIPv4_plus_L4_DstPort_plus_vpn.VPN);

    		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"adap_FsNpIpv4SetForwardingStatus deleted ip_addr:0x%x\n",arrKeyAddr[idx].DstIPv4_plus_L4_DstPort_plus_vpn.IPv4);

    	}
    }

    return ENET_SUCCESS;
}

ADAP_Uint32 EnetHal_FsNpIpv4UcDelRoute (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr,
									ADAP_Uint32 u4IpSubNetMask, tEnetHal_NpNextHopInfo routeEntry,
									ADAP_Int32 *pi4FreeDefIpB4Del)
{
	ADAP_Uint32 numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
	ADAP_Uint32 idx=0;
	ADAP_Uint32 arrIpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
	ADAP_Uint32	IpClass=0;
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpV4NextRoute tTempNextHop;
	tIpV4NextRoute *pAddNextHop=NULL;
    	ADAP_Uint32         logPort = 0;
	tIpDestV4Routing *pIpv4Routing=NULL;
    	MEA_Port_t          PhyPort;
	MEA_OutPorts_Entry_dbt tOutPorts;
	MEA_Action_t        Action_id=0;
        MEA_EHP_Info_dbt tEhp_info;
        ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        ADAP_Uint32 vlanIdTag=0;
        tEnetHal_MacAddr srcMac;
        tEnetHal_MacAddr daMac;
        MEA_Bool action_valid=MEA_FALSE;
    	ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    	ADAP_Uint32         modeType = 0;
	tBridgeDomain *pBridgeDomain=NULL;
        tServiceDb      *pServiceInfo=NULL;
        MEA_Port_t       enetPort;
        ADAP_Uint32      Index=0;

#ifdef ADAP_SW_LPM
	if (Enet_Lpm_Route_Delete(u4VrId, u4IpDestAddr, 32 - __builtin_ctz(u4IpSubNetMask)) != LPM_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete ipRoute u4IpDestAddr:%x u4IpSubNetMask:%x\n",u4IpDestAddr,u4IpSubNetMask);
		return ENET_FAILURE;
	}
#endif

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
        adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
        adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
        adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	tTempNextHop.u4NextHopGt=routeEntry.u4NextHopGt;
	tTempNextHop.u4IpDestAddr=u4IpDestAddr;
	tTempNextHop.u4IpSubNetMask=u4IpSubNetMask;
	tTempNextHop.u2VlanId = routeEntry.u2VlanId;
	tTempNextHop.u4IfIndex = routeEntry.u4IfIndex;

	if(AdapGetIpNextRouteTable(&pAddNextHop,&tTempNextHop) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to get ipRoute u4IpDestAddr:0x%x u4IpSubNetMask:0x%x\n",u4IpDestAddr,u4IpSubNetMask);
		return ENET_FAILURE;
	}

	if( (u4IpDestAddr == 0x00000000) && (u4IpSubNetMask == 0x00000000) )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default route handling\r\n");
                for(Index = 1; Index < MeaAdapGetNumOfPhyPorts(); Index ++)
                {
                    if(MeaAdapGetPhyPortFromLogPort(Index, &enetPort) == ENET_FAILURE)
                    {
                        continue;
                    }
                    pServiceInfo = MeaAdapGetFirstL3ServiceIdByEnetPort (enetPort);
                    while(pServiceInfo != NULL)
                    {
                        if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
                        {
                            MeaDrvResetDefaultRouteService (pServiceInfo->serviceId);
                        }
                        pServiceInfo = MeaAdapGetNextL3ServiceIdByEnetPort(enetPort, pServiceInfo->serviceId);
                    }
                }
		return ENET_SUCCESS;
	}

	if(MiFiDivideIpClass(u4IpDestAddr,u4IpSubNetMask,&IpClass,&arrIpAddr[0],&numOfEntries) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to calculate the subnetting\n");
		ret = ENET_FAILURE;
	}
	for(idx=0;idx<numOfEntries;idx++)
	{
		// delete from forwarder
		MeaDrvHwDeleteDestIpForwarder(arrIpAddr[idx],IpClass,START_L3_VPN);
	}

	if(AdapFreeIpNextRouteTable(pAddNextHop) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete ipRoute u4IpDestAddr:%d u4IpSubNetMask:%d\n",
			u4IpDestAddr,u4IpSubNetMask);
		ret = ENET_FAILURE;
	}

	/* If there exists an ECMP route for the dest, add the route to forwarder */
	pAddNextHop = NULL;
	if(AdapGetIpRouteTable(&pAddNextHop,&tTempNextHop) == ENET_SUCCESS)
	{
		if(MiFiDivideIpClass(u4IpDestAddr,u4IpSubNetMask,&IpClass,&arrIpAddr[0],&numOfEntries) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to calculate the subnetting\n");
		}
		pBridgeDomain = Adap_getBridgeDomainByVlan(pAddNextHop->u2VlanId,0);
		if ((pAddNextHop->u4NextHopGt != 0) && (pBridgeDomain != NULL))
		{
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] == 0)
				{
					continue;
				}
				logPort = pBridgeDomain->ifIndex[idx];
				MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
				MEA_SET_OUTPORT(&tOutPorts,PhyPort);
				if(AdapGetEntryIpTable(&pIpv4Routing,pAddNextHop->u4NextHopGt) == ENET_SUCCESS)
				{
					adap_memcpy(&daMac,&pIpv4Routing->macAddress,sizeof(tEnetHal_MacAddr));
				}
				MeaDrvGetFirstGlobalMacAddress(&srcMac);

				MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
			}
			if (adap_VlanCheckTaggedPort(pAddNextHop->u2VlanId,logPort) == ADAP_TRUE)
			{
				if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
				(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
				{
					AdapGetProviderCoreBridgeMode(&modeType,logPort);
					if (modeType == ADAP_PROVIDER_NETWORK_PORT)
					{
						EtherType = 0x88a8;
					}
				}
		    		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
				tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_APPEND;
				vlanIdTag = (EtherType << 16);
				vlanIdTag |= pAddNextHop->u2VlanId;
			}
			else
			{
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
				tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		    		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
			}
			/* create action for the route */
			MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
					&daMac, vlanCommand, vlanIdTag, 0);

			if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,numOfEntries,
						&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
			}
			action_valid = MEA_TRUE;
		}
		for(idx=0;idx<numOfEntries;idx++)
		{
			// create entry in forwarder
			if(MeaDrvHwCreateDestIpForwarder(Action_id,action_valid,arrIpAddr[idx],IpClass,START_L3_VPN,&tOutPorts) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
						(arrIpAddr[idx] & 0xFF000000) >> 24,
						(arrIpAddr[idx] & 0x00FF0000) >> 16,
						(arrIpAddr[idx] & 0x0000FF00) >> 8,
						(arrIpAddr[idx] & 0x000000FF) );
			}
		}
	}

    return ret;
}

MEA_EHP_Info_dbt tEhp_info_Arp;

ADAP_Uint32 EnetHal_FsNpIpv4ArpAdd (tEnetHal_VlanId u2VlanId, ADAP_Uint32 u4IfIndex,
									ADAP_Uint32 u4IpAddr, tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,
									ADAP_Int32 i1State, ADAP_Uint32 *pu4TblFull)
{
	ADAP_UNUSED_PARAM (*pu4TblFull);
	ADAP_UNUSED_PARAM (i1State);
	ADAP_UNUSED_PARAM (pu1IfName);
	tIpInterfaceTable *pInterfaceTable=NULL;
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpDestV4Routing *pIpv4Routing=NULL;
	MEA_OutPorts_Entry_dbt tOutPorts;
        ADAP_Uint32         logPort = 0;
        MEA_Port_t          PhyPort;
        MEA_Action_t        Action_id=0;
        ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        ADAP_Uint32 vlanIdTag=0;
        tEnetHal_MacAddr srcMac;
        tEnetHal_MacAddr daMac;
    	ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    	ADAP_Uint32         modeType = 0;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx=0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
        adap_memset(&tEhp_info_Arp,0,sizeof(MEA_EHP_Info_dbt));
        adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
        adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IfIndex:%d\n",u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%x\n",u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pMacAddr:%s\n",adap_mac_to_string(pMacAddr));
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," i1State:%d\n",i1State);

    pInterfaceTable = AdapGetIpInterface(u4IfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4IfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		if(AdapGetEntryIpTable(&pIpv4Routing,u4IpAddr) != ENET_SUCCESS)
		{
			ret = AdapAllocateEntryIpTable(&pIpv4Routing,u4IpAddr);
			if( (ret != ENET_SUCCESS) || (pIpv4Routing == NULL) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to allocate routeEntry for IfIndex:%d\r\n",u4IfIndex);
			}
			else
			{
				adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));
			}
		}
		else
		{
			//modify entry
			adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));
		}
                if ((i1State == NP_ARP_DYNAMIC) || (i1State == NP_ARP_STATIC))
                {
			pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] == 0)
				{
					continue;
				}
				if (u2VlanId != 0)
				{
					logPort = pBridgeDomain->ifIndex[idx];
				}
				else
				{
					logPort = u4IfIndex;
				}

			        if(adap_IsPortChannel(logPort) == ENET_SUCCESS)
        			{
                			if( adap_NumOfLagPortFromAggrId(logPort)  > 0)
                			{
                        			tDbaseHwAggEntry *pLagEntry = adap_getLagInstanceFromAggrId(logPort);
                        			if (pLagEntry != NULL)
                        			{
                                			int masterPort = pLagEntry->masterPort;
							MeaAdapGetPhyPortFromLogPort (masterPort, &PhyPort);
						}
                        			else
                        			{
                                			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to get lag entry by AggrId:%d\r\n", logPort);
                                			return MEA_ERROR;
                        			}
                			}
                			else
                			{
                        			return MEA_OK;
                			}
        			}
        			else
				{
					MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
				}
				MEA_SET_OUTPORT(&tOutPorts,PhyPort);

				adap_memcpy(&daMac,pMacAddr,sizeof(tEnetHal_MacAddr));
				MeaDrvGetFirstGlobalMacAddress(&srcMac);

				MEA_SET_OUTPORT(&tEhp_info_Arp.output_info,PhyPort);
			} 
			if  ((u2VlanId != 0) &&
			     (adap_VlanCheckTaggedPort(u2VlanId,logPort) == ADAP_TRUE))
			{
				if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
				(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
				{
					AdapGetProviderCoreBridgeMode(&modeType,logPort);
					if (modeType == ADAP_PROVIDER_NETWORK_PORT)
					{
						EtherType = 0x88a8;
					}
				}

				if (pInterfaceTable->u2InnerVlanId == 0)
				{
					tEhp_info_Arp.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
				}
				else
				{
					tEhp_info_Arp.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_APPEND;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_swap_mode = MEA_EHP_FLOW_TYPE_QTAG;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_outer_type = MEA_TRUE;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_inner_type = MEA_FALSE;
				}

				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
				vlanIdTag = (EtherType << 16);
				vlanIdTag |= u2VlanId;
				/* create action for the route */
				MiFiHwCreateRouterEhp(&tEhp_info_Arp,&srcMac,
						&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
				if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info_Arp,1,
							&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
				}
				// create entry in forwarder
				if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
							(u4IpAddr & 0xFF000000) >> 24,
							(u4IpAddr & 0x00FF0000) >> 16,
							(u4IpAddr & 0x0000FF00) >> 8,
							(u4IpAddr & 0x000000FF) );
				}
				if(((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
				   (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE)) &&
                                   (pInterfaceTable->u2InnerVlanId == 0))
				{
					vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
					tEhp_info_Arp.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_EXTRACT;
		    			tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
					vlanIdTag = (EtherType << 16);
					vlanIdTag |= u2VlanId;
					/* create action for the route */
					MiFiHwCreateRouterEhp(&tEhp_info_Arp,&srcMac,
							&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
					if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info_Arp,1, &tOutPorts,
							MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
					}
					// create entry in forwarder
					if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,
							START_L3_VPN1,&tOutPorts) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
								(u4IpAddr & 0xFF000000) >> 24,
								(u4IpAddr & 0x00FF0000) >> 16,
								(u4IpAddr & 0x0000FF00) >> 8,
								(u4IpAddr & 0x000000FF) );
					}
				}
			}
			else
			{
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
				tEhp_info_Arp.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		    		tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;

				/* create action for the route */
				MiFiHwCreateRouterEhp(&tEhp_info_Arp,&srcMac,
						&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);

				if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info_Arp,1,
							&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
				}
				// create entry in forwarder
				if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
							(u4IpAddr & 0xFF000000) >> 24,
							(u4IpAddr & 0x00FF0000) >> 16,
							(u4IpAddr & 0x0000FF00) >> 8,
							(u4IpAddr & 0x000000FF) );
				}
			}
		}
		if (AdapCheckFailedArp (u4IpAddr) == ADAP_TRUE)
                {
		    adap_HandleFailedPPPoESession ();
                    AdapRemoveFailedArp (u4IpAddr);
                }
	}

#ifdef ADAP_SW_LPM
	if (Enet_Lpm_Arp_Add  (0, u4IpAddr, u4IfIndex, u2VlanId, pMacAddr) != LPM_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to add ARP entry for IfIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}
#endif
    return ret;
}

ADAP_Uint32 EnetHal_FsNpIpv4ArpModify (tEnetHal_VlanId u2VlanId, ADAP_Uint32 u4IfIndex, ADAP_Uint32 u4PhyIfIndex,
									ADAP_Uint32 u4IpAddr, tEnetHal_MacAddr *pMacAddr, ADAP_Uint8 *pu1IfName,ADAP_Int32 i1State)
{
	ADAP_UNUSED_PARAM (u4PhyIfIndex);
	ADAP_UNUSED_PARAM (i1State);
	ADAP_UNUSED_PARAM (pu1IfName);
	tIpInterfaceTable *pInterfaceTable=NULL;
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpDestV4Routing *pIpv4Routing=NULL;
	MEA_OutPorts_Entry_dbt tOutPorts;
        ADAP_Uint32         logPort = 0;
        MEA_Port_t          PhyPort;
        MEA_Action_t        Action_id=0;
        MEA_EHP_Info_dbt tEhp_info;
        ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        ADAP_Uint32 vlanIdTag=0;
        tEnetHal_MacAddr srcMac;
        tEnetHal_MacAddr daMac;
    	ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    	ADAP_Uint32         modeType = 0;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx=0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
        adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
        adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
        adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IfIndex:%d\n",u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%x\n",u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pMacAddr:%s\n",adap_mac_to_string(pMacAddr));
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," i1State:%d\n",i1State);

    	pInterfaceTable = AdapGetIpInterface(u4IfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4IfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		if(AdapGetEntryIpTable(&pIpv4Routing,u4IpAddr) != ENET_SUCCESS)
		{
			ret = AdapAllocateEntryIpTable(&pIpv4Routing,u4IpAddr);
			if( (ret != ENET_SUCCESS) || (pIpv4Routing == NULL) )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to allocate routeEntry ifIndex:%d \n",u4IfIndex);
				ret = ENET_FAILURE;
			}
			else
			{
				adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));

				pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);
				for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
				{
					if(pBridgeDomain->ifType[idx] == 0)
					{
						continue;
					}
					if (u2VlanId != 0)
					{
						logPort = pBridgeDomain->ifIndex[idx];
					}
					else
					{
						logPort = u4IfIndex;
					}
					MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
					MEA_SET_OUTPORT(&tOutPorts,PhyPort);

					adap_memcpy(&daMac,pMacAddr,sizeof(tEnetHal_MacAddr));
					MeaDrvGetFirstGlobalMacAddress(&srcMac);

					MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
				}
				if ( (u2VlanId != 0) &&
				    (adap_VlanCheckTaggedPort(u2VlanId,logPort) == ADAP_TRUE))
				{
					if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
							(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
					{
						AdapGetProviderCoreBridgeMode(&modeType,logPort);
						if (modeType == ADAP_PROVIDER_NETWORK_PORT)
						{
							EtherType = 0x88a8;
						}
					}

					if (pInterfaceTable->u2InnerVlanId == 0)
					{
						tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
		    				tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
					}
					else
					{
						tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_APPEND;
						tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
						tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_swap_mode = MEA_EHP_FLOW_TYPE_QTAG;
						tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_outer_type = MEA_TRUE;
						tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_inner_type = MEA_FALSE;
					}

					vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
					vlanIdTag = (EtherType << 16);
					vlanIdTag |= u2VlanId;
					/* create action for the route */
					MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
							&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
					if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
								&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
					}
					// create entry in forwarder
					if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
								(u4IpAddr & 0xFF000000) >> 24,
								(u4IpAddr & 0x00FF0000) >> 16,
								(u4IpAddr & 0x0000FF00) >> 8,
								(u4IpAddr & 0x000000FF) );
					}
				        if(((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
				            (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE)) &&
                                           (pInterfaceTable->u2InnerVlanId == 0))
					{
		    				tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
						vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
						tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_EXTRACT;
						vlanIdTag = (EtherType << 16);
						vlanIdTag |= u2VlanId;
						/* create action for the route */
						MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
								&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
						if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1, &tOutPorts,
									MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
						}
						// create entry in forwarder
						if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,
									START_L3_VPN1,&tOutPorts) != MEA_OK)
						{
							ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
									(u4IpAddr & 0xFF000000) >> 24,
									(u4IpAddr & 0x00FF0000) >> 16,
									(u4IpAddr & 0x0000FF00) >> 8,
									(u4IpAddr & 0x000000FF) );
						}
					}
				}
				else
				{
					vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
					tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		    			tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;

					/* create action for the route */
					MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
							&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);

					if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
								&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
					}
					// create entry in forwarder
					if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
								(u4IpAddr & 0xFF000000) >> 24,
								(u4IpAddr & 0x00FF0000) >> 16,
								(u4IpAddr & 0x0000FF00) >> 8,
								(u4IpAddr & 0x000000FF) );
					}
				}
			}
		}
		else
		{
			//modify entry
			adap_memcpy(&pIpv4Routing->macAddress,pMacAddr,sizeof(tEnetHal_MacAddr));

			pBridgeDomain = Adap_getBridgeDomainByVlan(u2VlanId,0);
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] == 0)
				{
					continue;
				}
				if (u2VlanId != 0)
				{
					logPort = pBridgeDomain->ifIndex[idx];
				}
				else
				{
					logPort = u4IfIndex;
				}
				MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
				MEA_SET_OUTPORT(&tOutPorts,PhyPort);

				adap_memcpy(&daMac,pMacAddr,sizeof(tEnetHal_MacAddr));
				MeaDrvGetFirstGlobalMacAddress(&srcMac);

				MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
			}
			if ((u2VlanId != 0) &&
			   (adap_VlanCheckTaggedPort(u2VlanId,logPort) == ADAP_TRUE))
			{
				if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
						(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
				{
					AdapGetProviderCoreBridgeMode(&modeType,logPort);
					if (modeType == ADAP_PROVIDER_NETWORK_PORT)
					{
						EtherType = 0x88a8;
					}
				}

				if (pInterfaceTable->u2InnerVlanId == 0)
				{
					tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP;
		    			tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
				}
				else
				{
					tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_APPEND;
					tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_QTAG;
					tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_swap_mode = MEA_EHP_FLOW_TYPE_QTAG;
					tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_outer_type = MEA_TRUE;
					tEhp_info_Arp.ehp_data.wbrg_info.val.data_wbrg.enc_inner_type = MEA_FALSE;
				}

				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
				vlanIdTag = (EtherType << 16);
				vlanIdTag |= u2VlanId;
				/* create action for the route */
				MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
						&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
				if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
							&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
				}
				// create entry in forwarder
				if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
							(u4IpAddr & 0xFF000000) >> 24,
							(u4IpAddr & 0x00FF0000) >> 16,
							(u4IpAddr & 0x0000FF00) >> 8,
							(u4IpAddr & 0x000000FF) );
				}
				if(((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) || 
				    (AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE)) &&
                                   (pInterfaceTable->u2InnerVlanId == 0))
				{
					vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_SWAP;
					tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_SWAP_EXTRACT;
		    			tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
					vlanIdTag = (EtherType << 16);
					vlanIdTag |= u2VlanId;
					/* create action for the route */
					MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
							&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);
					if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1, &tOutPorts,
								MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
					}
					// create entry in forwarder
					if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,
								START_L3_VPN1,&tOutPorts) != MEA_OK)
					{
						ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
								(u4IpAddr & 0xFF000000) >> 24,
								(u4IpAddr & 0x00FF0000) >> 16,
								(u4IpAddr & 0x0000FF00) >> 8,
								(u4IpAddr & 0x000000FF) );
					}
				}
			}
			else
			{
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
				tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		    		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;

				/* create action for the route */
				MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
						&daMac, vlanCommand, vlanIdTag, pInterfaceTable->u2InnerVlanId);

				if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,1,
							&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
				}
				// create entry in forwarder
				if(MeaDrvHwCreateDestIpForwarder(Action_id,MEA_TRUE,u4IpAddr,D_IP_MASK_32,START_L3_VPN,&tOutPorts) != MEA_OK)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
							(u4IpAddr & 0xFF000000) >> 24,
							(u4IpAddr & 0x00FF0000) >> 16,
							(u4IpAddr & 0x0000FF00) >> 8,
							(u4IpAddr & 0x000000FF) );
				}
			}
		}
	}

#ifdef ADAP_SW_LPM
	if (Enet_Lpm_Arp_Add(0, u4IpAddr, u4IfIndex, u2VlanId, pMacAddr) != LPM_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to add ARP entry for IfIndex:%d\r\n",u4IfIndex);
		return ENET_FAILURE;
	}
#endif
    return ret;
}

MEA_Status MeaDrvDeleteActionIp(tIpInterfaceTable *pInterfaceTable,ADAP_Uint32 routeIpAddr)
{
	ADAP_Uint32 idx;
	ADAP_Uint32 routeIdx;
	ADAP_Uint32 IpClass;
	ADAP_Uint32 IpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
	ADAP_Uint32 MaxNumTable=0;
	tIpV4NextRoute *nextRouter;

	nextRouter = AdapGetFirstIpNextRouteTable();
	while(nextRouter != NULL)
	{
		if(pInterfaceTable == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MiFiDeleteActionIp pInterfaceTable=NULL pointer\n");
			return MEA_ERROR;
		}

		for(idx=0;idx<MAX_NUM_OF_IP_CLASS_ENTRIES;idx++)
		{
			if( (nextRouter->valid == ADAP_TRUE) && (nextRouter->u4NextHopGt == routeIpAddr) )
			{
				MaxNumTable = MAX_NUM_OF_IP_CLASS_ENTRIES;
				MiFiDivideIpClass(nextRouter->u4IpDestAddr,nextRouter->u4IpSubNetMask,&IpClass,&IpAddr[0],&MaxNumTable);
				for(routeIdx=0;routeIdx<MaxNumTable;routeIdx++)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG,"delete ip:%x class:%d from forwarder:%d\n",IpAddr[routeIdx],IpClass);
					MeaDrvHwDeleteDestIpForwarder(IpAddr[routeIdx],
												IpClass,
												START_L3_VPN);
				}
			}
		}
		nextRouter = AdapGetNextIpNextRouteTable(nextRouter);
	}
	// delete the next Hop
	MeaDrvHwDeleteDestIpForwarder(routeIpAddr,
								D_IP_MASK_32,
								START_L3_VPN);


	if( (pInterfaceTable->MyIpAddr.valid == ADAP_TRUE) && (pInterfaceTable->MyIpAddr.ipAddr == routeIpAddr) )
	{
		adap_memset(&pInterfaceTable->MyIpAddr.macAddress,0,sizeof(tEnetHal_MacAddr));
		if(pInterfaceTable->MyIpAddr.ActionId != MEA_PLAT_GENERATE_NEW_ID)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"MiFiDeleteActionIp actionId:%d\n",pInterfaceTable->MyIpAddr.ActionId);
			if ( ENET_ADAPTOR_Delete_Action(MEA_UNIT_0, pInterfaceTable->MyIpAddr.ActionId) != MEA_OK )
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"MiFiDeleteActionIp ERROR: ENET_ADAPTOR_Delete_Action FAIL !!! ===\n");
				return MEA_ERROR;
			}
			pInterfaceTable->MyIpAddr.ActionId = MEA_PLAT_GENERATE_NEW_ID;
			pInterfaceTable->MyIpAddr.valid=MEA_FALSE;
			return MEA_OK;
		}
	}


	return MEA_OK;
}

ADAP_Uint32 EnetHal_FsNpIpv4ArpDel (ADAP_Uint32 u4IpAddr, ADAP_Uint8 *pu1IfName, ADAP_Int8 i1State)
{
	tIpDestV4Routing *pIpv4Routing=NULL;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4ArpDel u4IpAddr:%d.%d.%d.%d\n",
										( (u4IpAddr & 0xFF000000) >> 24),
										( (u4IpAddr & 0x00FF0000) >> 16),
										( (u4IpAddr & 0x0000FF00) >> 8),
										( (u4IpAddr & 0x000000FF)) );
	if(AdapGetEntryIpTable(&pIpv4Routing,u4IpAddr) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete ARP %x \n",u4IpAddr);
		return ENET_SUCCESS;
	}

	if (AdapFreeEntryIpTable(pIpv4Routing) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete ARP %x \n",u4IpAddr);
		return ENET_FAILURE;
	}

	MeaDrvHwDeleteDestIpForwarder(u4IpAddr,D_IP_MASK_32,START_L3_VPN);
	/* For double tagged interface, entry can be present with VPN1, hence deleting for it */
	MeaDrvHwDeleteDestIpForwarder(u4IpAddr,D_IP_MASK_32,START_L3_VPN1);

#ifdef ADAP_SW_LPM
	if (Enet_Lpm_Arp_Delete  (0, u4IpAddr) != LPM_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to delete ARP %x \n",u4IpAddr);
		return ENET_FAILURE;
	}
#endif
    return ENET_SUCCESS;
}

ADAP_Uint8 EnetHal_FsNpIpv4CheckHitOnArpEntry (ADAP_Uint32 u4IpAddress, ADAP_Uint8 u1NextHopFlag)
{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," IpAddress:%d.%d.%d.%d nextHop:%d\n",
														(u4IpAddress & 0xFF000000) >> 24,
														(u4IpAddress & 0x00FF0000) >> 16,
														(u4IpAddress & 0x0000FF00) >> 8,
														(u4IpAddress & 0x000000FF) ,
														u1NextHopFlag);
		//check if IpAddrees still in the forwarding
		if(MeaDrvIsIpAddressInIpForwarder(u4IpAddress,START_L3_VPN) == MEA_TRUE)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_DEBUG," FsNpIpv4CheckHitOnArpEntry: ip found the forwarding table\r\n");
			return ADAP_TRUE;
		}

    return ADAP_FALSE;
}


ADAP_Uint32 EnetHal_FsNpIpv4UcAddRoute (ADAP_Uint32 u4VrId, ADAP_Uint32 u4IpDestAddr, ADAP_Uint32 u4IpSubNetMask,
		tEnetHal_NpNextHopInfo * pRouteEntry, ADAP_Uint8 *pbu1TblFull)
{
#ifdef ADAP_SW_LPM
	if (Enet_Lpm_Route_Add(u4VrId, u4IpDestAddr, 32 - __builtin_ctz(u4IpSubNetMask), pRouteEntry->u4NextHopGt, pRouteEntry->u4IfIndex, pRouteEntry->u2VlanId) != LPM_SUCCESS) {
		return ENET_FAILURE;
	}

	/*LPM will handle route programming to FPGA for non-local routes */
	if (pRouteEntry->u4NextHopGt != 0) {
		return ENET_SUCCESS;
	}
#endif

//	tIpInterfaceTable *pInterfaceTable=NULL;
	ADAP_Uint32 ret=ENET_SUCCESS;
	ADAP_Uint32 arrIpAddr[MAX_NUM_OF_IP_CLASS_ENTRIES];
	ADAP_Uint32 numOfEntries=MAX_NUM_OF_IP_CLASS_ENTRIES;
	ADAP_Uint32	IpClass=0;
	ADAP_Uint32	idx=0;
	MEA_OutPorts_Entry_dbt tOutPorts;
	tIpV4NextRoute *pAddNextHop=NULL;
	tIpV4NextRoute tTempNextHop;
    	ADAP_Uint32         logPort = 0;
	tIpDestV4Routing *pIpv4Routing=NULL;
    	MEA_Port_t          PhyPort;
	MEA_Action_t        Action_id=0;
        MEA_EHP_Info_dbt tEhp_info;
        ADAP_Uint32 vlanCommand=MEA_EGRESS_HEADER_PROC_CMD_TRANS;
        ADAP_Uint32 vlanIdTag=0;
        tEnetHal_MacAddr srcMac;
        tEnetHal_MacAddr daMac;
        MEA_Bool action_valid=MEA_FALSE;
    	ADAP_Uint16         EtherType = MEA_EGRESS_HEADER_PROC_VLAN_ETH_TYPE;
    	ADAP_Uint32         modeType = 0;
        tServiceDb      *pServiceInfo=NULL;
        MEA_Port_t       enetPort;
        ADAP_Uint32      Index=0;

	adap_memset(&tOutPorts,0,sizeof(MEA_OutPorts_Entry_dbt));
        adap_memset(&tEhp_info,0,sizeof(MEA_EHP_Info_dbt));
        adap_memset(&srcMac,0,sizeof(tEnetHal_MacAddr));
        adap_memset(&daMac,0,sizeof(tEnetHal_MacAddr));

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4VrId:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpDestAddr:%x\n",u4IpDestAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpSubNetMask:%x\n",u4IpSubNetMask);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",pRouteEntry->u2VlanId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IfIndex:%d\n",pRouteEntry->u4IfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4NextHopGt:%d\n",pRouteEntry->u4NextHopGt);

	if(u4IpDestAddr == 0x01000000)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," not insert ip:%x\n",u4IpDestAddr);
		return ret;
	}


//    pInterfaceTable = AdapGetIpInterface(pRouteEntry->u4IfIndex);
//	if(pInterfaceTable == NULL)
//	{
//		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",pRouteEntry->u4IfIndex);
//		ret = ENET_FAILURE;
//	}

	tTempNextHop.u2VlanId=pRouteEntry->u2VlanId;
	tTempNextHop.u4NextHopGt=pRouteEntry->u4NextHopGt;
	tTempNextHop.u4IfIndex=pRouteEntry->u4IfIndex;
	tTempNextHop.u4IpDestAddr=u4IpDestAddr;
	tTempNextHop.u4IpSubNetMask=u4IpSubNetMask;

	if(AdapGetIpNextRouteTable(&pAddNextHop,&tTempNextHop) != ENET_SUCCESS)
	{
		if(AdapAllocIpNextRouteTable(&pAddNextHop,&tTempNextHop) != ENET_SUCCESS)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to allocate memory\n");
			return ENET_FAILURE;
		}
	}
	else
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," ip addr:%x dest:%x allready configured\r\n",u4IpDestAddr,u4IpSubNetMask);
		return ret;
	}
	pAddNextHop->u2VlanId=pRouteEntry->u2VlanId;
	pAddNextHop->u4NextHopGt=pRouteEntry->u4NextHopGt;
	pAddNextHop->u4IfIndex=pRouteEntry->u4IfIndex;
	pAddNextHop->u4IpDestAddr=u4IpDestAddr;
	pAddNextHop->u4IpSubNetMask=u4IpSubNetMask;

	if( (u4IpDestAddr == 0x00000000) && (u4IpSubNetMask == 0x00000000) )
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"default route handling\r\n");
                for(Index = 1; Index < MeaAdapGetNumOfPhyPorts(); Index ++)
                {
                    if(MeaAdapGetPhyPortFromLogPort(Index, &enetPort) == ENET_FAILURE)
                    {
                        continue;
                    }
                    pServiceInfo = MeaAdapGetFirstL3ServiceIdByEnetPort (enetPort);
                    while(pServiceInfo != NULL)
                    {
                        if (pServiceInfo->serviceId != MEA_PLAT_GENERATE_NEW_ID)
                        {
                            MeaDrvHandleDefaultRouteService (pServiceInfo->serviceId, pRouteEntry);
                        }
                        pServiceInfo = MeaAdapGetNextL3ServiceIdByEnetPort(enetPort, pServiceInfo->serviceId);
                    }
                }
		return ret;
	}

	if(MiFiDivideIpClass(u4IpDestAddr,u4IpSubNetMask,&IpClass,&arrIpAddr[0],&numOfEntries) != ENET_SUCCESS)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," failed to calculate the subnetting\n");
		ret = ENET_FAILURE;
	}


	for(idx=0;idx<numOfEntries;idx++)
	{
		if (pRouteEntry->u4NextHopGt != 0)
		{
			if (pRouteEntry->u2VlanId != 0)
			{
				logPort = adap_VlanGetPortByVlanId (pRouteEntry->u2VlanId);
			}
			else
			{
				logPort = pRouteEntry->u4IfIndex;
			}
			MeaAdapGetPhyPortFromLogPort (logPort, &PhyPort);
			MEA_SET_OUTPORT(&tOutPorts,PhyPort);
			if(AdapGetEntryIpTable(&pIpv4Routing,pRouteEntry->u4NextHopGt) == ENET_SUCCESS)
			{
				adap_memcpy(&daMac,&pIpv4Routing->macAddress,sizeof(tEnetHal_MacAddr));
			}
			MeaDrvGetFirstGlobalMacAddress(&srcMac);

			if ((pRouteEntry->u2VlanId != 0) &&
					(adap_VlanCheckTaggedPort(pRouteEntry->u2VlanId,logPort) == ADAP_TRUE))
			{
				if((AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
						(AdapGetBridgeMode() == ADAP_VLAN_PROVIDER_CORE_BRIDGE_MODE))
				{
					AdapGetProviderCoreBridgeMode(&modeType,logPort);
					if (modeType == ADAP_PROVIDER_NETWORK_PORT)
					{
						EtherType = 0x88a8;
					}
				}
		    		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_TAG;
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_APPEND;
				tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_APPEND;
				vlanIdTag = (EtherType << 16);
				vlanIdTag |= pRouteEntry->u2VlanId;
			}
			else
			{
				vlanCommand = MEA_EGRESS_HEADER_PROC_CMD_EXTRACT;
				tEhp_info.ehp_data.EditingType = MEA_EDITINGTYPE_EXTRACT;
		    		tEhp_info.ehp_data.wbrg_info.val.data_wbrg.flow_type = MEA_EHP_FLOW_TYPE_UNTAG;
			}

			MEA_SET_OUTPORT(&tEhp_info.output_info,PhyPort);
			/* create action for the route */
			MiFiHwCreateRouterEhp(&tEhp_info,&srcMac,
					&daMac, vlanCommand, vlanIdTag, 0);

			if(FsMiHwCreateRouterAction(&Action_id,&tEhp_info,numOfEntries,
						&tOutPorts,MEA_ACTION_TYPE_FWD,0,0,MEA_PLAT_GENERATE_NEW_ID) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed to create action\r\n");
			}
			action_valid = MEA_TRUE;
			// create entry in forwarder
			if(MeaDrvHwCreateDestIpForwarder(Action_id,action_valid,arrIpAddr[idx],
						IpClass,START_L3_VPN,&tOutPorts) != MEA_OK)
			{
				ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR,"failed add to add ip to forwarder:%d.%d.%d.%d\n",
						(arrIpAddr[idx] & 0xFF000000) >> 24,
						(arrIpAddr[idx] & 0x00FF0000) >> 16,
						(arrIpAddr[idx] & 0x0000FF00) >> 8,
						(arrIpAddr[idx] & 0x000000FF) );
				//return FNP_FAILURE;
			}
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO,"add ip to forwarder:%d.%d.%d.%d\n",
					(arrIpAddr[idx] & 0xFF000000) >> 24,
					(arrIpAddr[idx] & 0x00FF0000) >> 16,
					(arrIpAddr[idx] & 0x0000FF00) >> 8,
					(arrIpAddr[idx] & 0x000000FF) );

		}
		//EnetMilisecondSleep(1);
	}
	return ret;
}

ADAP_Uint32 EnetHal_FsNpIpv4CreateL3IpInterface (tEnetHal_NpIpInterface *pL3Interface)
{
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	MEA_LxCp_t lxcp_Id=0;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",pL3Interface->u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IfName:%s\n",pL3Interface->pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," CfaIfIndex:%d\n",pL3Interface->u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IpAddr:%x\n",pL3Interface->u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IpSubNetMask:%x\n",pL3Interface->u4IpSubNetMask);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," VlanId:%d\n",pL3Interface->u2VlanId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," InnerVlanId:%d\n",pL3Interface->u2InnerVlan);
	/**************************************  end of print out *******************/

    	pInterfaceTable = AdapAllocateIpInterface(pL3Interface->u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",pL3Interface->u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		pInterfaceTable->MyIpAddr.ipAddr = pL3Interface->u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = pL3Interface->u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = pL3Interface->u2VlanId;
		pInterfaceTable->u2InnerVlanId = pL3Interface->u2InnerVlan;
		pInterfaceTable->u4CfaIfIndex = pL3Interface->u4CfaIfIndex;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pL3Interface->pu1IfName,20);

		//getVLanInterface
		pBridgeDomain = Adap_getBridgeDomainByVlan(pL3Interface->u2VlanId,0);

		if(pBridgeDomain == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pBridgeDomain not found for vlanId:%d not exist\n",pL3Interface->u2VlanId);
		}
		else
		{
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," interface :%d vlan:%d configured\n",
						pBridgeDomain->ifIndex[idx],pL3Interface->u2VlanId);
					MeaDrvSetGlobalIpAddress(pL3Interface->u4IpAddr,MEA_TRUE);
					ret = getLxcpId(pBridgeDomain->ifIndex[idx],&lxcp_Id);
					if(ret == ENET_SUCCESS)
					{
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					}
				}
			}
			ret = adap_ReconfigureHwVlan (pL3Interface->u2VlanId,0);
		}
	}
    	return ret;
}

ADAP_Uint32 EnetHal_FsNpIpv4ModifyL3IpInterface (tEnetHal_NpIpInterface *pL3Interface)
{
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	MEA_LxCp_t lxcp_Id=0;
	tBridgeDomain *pBridgeDomain=NULL;
	ADAP_Uint32 idx;

	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",pL3Interface->u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IfName:%s\n",pL3Interface->pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," CfaIfIndex:%d\n",pL3Interface->u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IpAddr:%x\n",pL3Interface->u4IpAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," IpSubNetMask:%x\n",pL3Interface->u4IpSubNetMask);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," VlanId:%d\n",pL3Interface->u2VlanId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," InnerVlanId:%d\n",pL3Interface->u2InnerVlan);
	/**************************************  end of print out *******************/

    	pInterfaceTable = AdapGetIpInterface(pL3Interface->u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",pL3Interface->u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		pInterfaceTable->MyIpAddr.ipAddr = pL3Interface->u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = pL3Interface->u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = pL3Interface->u2VlanId;
		pInterfaceTable->u2InnerVlanId = pL3Interface->u2InnerVlan;
		pInterfaceTable->u4CfaIfIndex = pL3Interface->u4CfaIfIndex;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pL3Interface->pu1IfName,20);

		//getVLanInterface
		pBridgeDomain = Adap_getBridgeDomainByVlan(pL3Interface->u2VlanId,0);

		if(pBridgeDomain == NULL)
		{
			ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pBridgeDomain not found for vlanId:%d not exist\n",pL3Interface->u2VlanId);
		}
		else
		{
			for(idx=0;idx<pBridgeDomain->num_of_ports;idx++)
			{
				if(pBridgeDomain->ifType[idx] != 0)
				{
					ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," interface :%d vlan:%d configured\n",
						pBridgeDomain->ifIndex[idx],pL3Interface->u2VlanId);
					MeaDrvSetGlobalIpAddress(pL3Interface->u4IpAddr,MEA_TRUE);
					ret = getLxcpId(pBridgeDomain->ifIndex[idx],&lxcp_Id);
					if(ret == ENET_SUCCESS)
					{
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_MY_IP_ARP_OSPF_RIP_BGP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
						ret = MeaDrvSetLxcpParamaters(MEA_LXCP_PROTOCOL_ARP_Replay,MEA_LXCP_PROTOCOL_ACTION_CPU,lxcp_Id,NULL,NULL);
					}
				}
			}
			ret = adap_ReconfigureHwVlan (pL3Interface->u2VlanId,0);
		}
	}
    	return ret;
}


ADAP_Uint32 EnetHal_FsNpIpv4CreatePPPInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
						ADAP_Uint32 u4CfaIfIndex,
						ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
						ADAP_Uint16 u2VlanId, ADAP_Uint8 u1Port)
{
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	char ipAddr[30];
	char netAddr[30];

	// init variables
	adap_memset(&ipAddr[0],0,sizeof(ipAddr));
	adap_memset(&netAddr[0],0,sizeof(netAddr));


	/***********************  start of debug print out ************************/
	sprintf(ipAddr,"%d.%d.%d.%d",( (u4IpAddr & 0xFF000000) >> 24),
								 ( (u4IpAddr & 0x00FF0000) >> 16),
								 ( (u4IpAddr & 0x0000FF00) >> 8),
								 ( (u4IpAddr & 0x000000FF)) );

	sprintf(netAddr,"%d.%d.%d.%d",( (u4IpSubNetMask & 0xFF000000) >> 24),
								 ( (u4IpSubNetMask & 0x00FF0000) >> 16),
								 ( (u4IpSubNetMask & 0x0000FF00) >> 8),
								 ( (u4IpSubNetMask & 0x000000FF)) );


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pu1IfName:%s\n",pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4CfaIfIndex:%d\n",u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%s\n",ipAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpSubNetMask:%s\n",netAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	/**************************************  end of print out *******************/


    	pInterfaceTable = AdapAllocateIpInterface(u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		pInterfaceTable->MyIpAddr.ipAddr = u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = u2VlanId;
		pInterfaceTable->u4CfaIfIndex = u4CfaIfIndex;
		pInterfaceTable->ifIndex = u1Port;
		pInterfaceTable->IfType = IF_PPP;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pu1IfName,20);
	}

	return ret;
}


ADAP_Uint32 EnetHal_FsNpIpv4ModifyPPPInterface (ADAP_Uint32 u4VrId, ADAP_Uint8 *pu1IfName,
						ADAP_Uint32 u4CfaIfIndex,
						ADAP_Uint32 u4IpAddr, ADAP_Uint32 u4IpSubNetMask,
						ADAP_Uint16 u2VlanId, ADAP_Uint8 u1Port)
{
	ADAP_Uint32 ret=ENET_SUCCESS;
	tIpInterfaceTable *pInterfaceTable=NULL;
	char ipAddr[30];
	char netAddr[30];

	// init variables
	adap_memset(&ipAddr[0],0,sizeof(ipAddr));
	adap_memset(&netAddr[0],0,sizeof(netAddr));


	/***********************  start of debug print out ************************/
	sprintf(ipAddr,"%d.%d.%d.%d",( (u4IpAddr & 0xFF000000) >> 24),
								 ( (u4IpAddr & 0x00FF0000) >> 16),
								 ( (u4IpAddr & 0x0000FF00) >> 8),
								 ( (u4IpAddr & 0x000000FF)) );

	sprintf(netAddr,"%d.%d.%d.%d",( (u4IpSubNetMask & 0xFF000000) >> 24),
								 ( (u4IpSubNetMask & 0x00FF0000) >> 16),
								 ( (u4IpSubNetMask & 0x0000FF00) >> 8),
								 ( (u4IpSubNetMask & 0x000000FF)) );


	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," vrf:%d\n",u4VrId);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," pu1IfName:%s\n",pu1IfName);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4CfaIfIndex:%d\n",u4CfaIfIndex);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpAddr:%s\n",ipAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u4IpSubNetMask:%s\n",netAddr);
	ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_INFO," u2VlanId:%d\n",u2VlanId);
	/**************************************  end of print out *******************/


    	pInterfaceTable = AdapGetIpInterface(u4CfaIfIndex);
	if(pInterfaceTable == NULL)
	{
		ENET_ADAPTOR_PRINT_LOG(ENET_ADAP_ERR," interface table for ifIndex:%d not exist\n",u4CfaIfIndex);
		ret = ENET_FAILURE;
	}
	else
	{
		pInterfaceTable->MyIpAddr.ipAddr = u4IpAddr;
		pInterfaceTable->MyIpAddr.subnetMask = u4IpSubNetMask;
		pInterfaceTable->valid = MEA_TRUE;
		pInterfaceTable->u2VlanId = u2VlanId;
		pInterfaceTable->u4CfaIfIndex = u4CfaIfIndex;
		pInterfaceTable->ifIndex = u1Port;
		pInterfaceTable->IfType = IF_PPP;
		strncpy((char *)&pInterfaceTable->pu1IfName[0],(char *)pu1IfName,20);

		MeaDrvSetGlobalIpAddress(u4IpAddr,MEA_TRUE);
	}

	return ret;
}


void EnetHal_FsNpIpv4ClearArpTable ()
{
    AdapClearAllArpTable ();
}



