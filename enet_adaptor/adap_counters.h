/*
   This file/directory and the information contained in it are
   proprietary and confidential to Ethernity Networks Ltd.  
   No person is allowed to copy, reprint, reproduce or publish
   any part of this document, nor disclose its contents to others, 
   nor make any use of it, nor allow or assist others to make any
   use of it - unless by prior written express 
   authorization of Neralink  Networks Ltd and then only to the
   extent authorized (c) copyright Ethernity Networks Ltd. 2002 
*/

#ifndef ADAP_COUNTERS_H_
#define ADAP_COUNTERS_H_

typedef struct  {

	ADAP_Uint64 rx_packets;      /* Number of received packets. */
	ADAP_Uint64 tx_packets;      /* Number of transmitted packets. */
	ADAP_Uint64 rx_bytes;        /* Number of received bytes. */
	ADAP_Uint64 tx_bytes;        /* Number of transmitted bytes. */
	ADAP_Uint64 rx_dropped;      /* Number of packets dropped by RX. */
	ADAP_Uint64 tx_dropped;      /* Number of packets dropped by TX. */
	ADAP_Uint64 rx_errors;       /* Number of receive errors.  This is a
                                 super-set of receive errors and should be
                                 great than or equal to the sum of all
                                 rx_*_err values. */
	ADAP_Uint64 tx_errors;       /* Number of transmit errors.  This is a
                                 super-set of transmit errors. */
	ADAP_Uint64 rx_frame_err;    /* Number of frame alignment errors. */
	ADAP_Uint64 rx_over_err;     /* Number of packets with RX overrun. */
	ADAP_Uint64 rx_crc_err;      /* Number of CRC errors. */
	ADAP_Uint64 collisions;      /* Number of collisions. */
}tEnetAdapRmonCounter;

MEA_Status MeaAdapGetPortCounter (ADAP_Uint32  ifIndex,tEnetAdapRmonCounter *pCounter);

typedef struct {

	ADAP_Uint64 packet_count;
	ADAP_Uint64 byte_count;
} flow_stats_record;


MEA_Status MeaAdap_get_counter_records(ADAP_Uint32 counter_id_base, ADAP_Uint32 num_counters, flow_stats_record *records_arr);

#endif /* ADAP_COUNTERS_H_ */
