#ifndef _ADAP_LPM_RB_H
#define _ADAP_LPM_RB_H


#include "adap_lpminc.h"
/** Red-black Trees *********************************************************/
typedef struct node NODE;
typedef struct tree TREE;

struct node {
        NODE *child[2];
        NODE *parent; 
        unsigned char red;
    UINT1   au1Pad[3];
};

struct  tree {
        NODE
                root;
        INT4
                (*compare)(void *, void *);     
        void
                *(*keyof)(TREE *T, NODE *);
        unsigned int    
                count;
};

enum {
        RedBlkCREATE_SEM = 0, 
        RedBlkCREATE_NOSEM
};

enum eRedBlkNodeType { 
        RedBlk_EMBD_NODE = 0, 
        RedBlk_NOT_EMBD_NODE 
};

typedef enum {
        RedBlk_WALK_BREAK, 
        RedBlk_WALK_CONT
} eRedBlkWalkReturns;

typedef enum {
        preorder, 
        postorder, 
        endorder, 
        leaf
} eRedBlkVisit;

typedef void           tRedBlkElem;

typedef INT4 (*tRedBlkCompareFn) (tRedBlkElem *e1, tRedBlkElem *e2);
typedef INT4 (*tRedBlkKeyFreeFn) (tRedBlkElem *elem, UINT4 u4Arg);
typedef INT4 (*tRedBlkWalkFn) (tRedBlkElem *e, eRedBlkVisit, UINT4 level, void *arg, void *out);

typedef struct rbnode
{
    NODE
            __node__;
    tRedBlkElem
            *key;
} tRedBlkNode;

typedef struct RedBlkNodeEmbd{
        NODE __node__;
} tRedBlkNodeEmbd;

typedef struct rbtree
{
    TREE __tree__;

    enum eRedBlkNodeType
            NodeType;
    UINT4
            u4Offset;

    tUtilSemId
            SemId;
    tRedBlkNode
       *next_cache;
    BOOL1  b1MutualExclusive;
    UINT1  au1Pad[3];
} *tRedBlkTree;

#define RedBlk_OFFSET_SCAN(T,pCurrent,pNext,TypeCast)\
        for(pCurrent = (TypeCast)RedBlkTreeGetFirst(T),\
            pNext = (TypeCast)RedBlkTreeGetNext(T,(tRedBlkElem *)pCurrent,0);\
            pCurrent != 0; \
            pCurrent = pNext,\
            pNext = (pCurrent == 0) ? 0 :\
            (TypeCast)RedBlkTreeGetNext(T,(tRedBlkElem *)pCurrent,0))

/* In Linux kernel assert is not supported, so insmod of FutureKernel.o
 * will fail. Hence, assert function is used only if LINUX_KERN
 * is not defined. */
#define ASSERT(s) assert(s)

UINT4        RedBlkTreeLibInit (void);
void         RedBlkTreeLibShut (void);
tRedBlkTree      RedBlkTreeCreateEmbedded(UINT4 u4Offset, tRedBlkCompareFn Cmp);
void         RedBlkTreeDelete(tRedBlkTree);
void         RedBlkTreeDestroy(tRedBlkTree, tRedBlkKeyFreeFn FreeFn, UINT4 u4Arg);
void         RedBlkTreeDrain(tRedBlkTree, tRedBlkKeyFreeFn FreeFn, UINT4 u4Arg);
UINT4        RedBlkTreeAdd(tRedBlkTree, tRedBlkElem *);
UINT4        RedBlkTreeRemove(tRedBlkTree, tRedBlkElem *);
tRedBlkElem *    RedBlkTreeRem (struct rbtree *rbinfo, tRedBlkElem * key);
tRedBlkElem *    RedBlkTreeGet(tRedBlkTree, tRedBlkElem *);
tRedBlkElem *    RedBlkTreeGetFirst(tRedBlkTree);
tRedBlkElem *    RedBlkTreeGetNext(tRedBlkTree, tRedBlkElem *, tRedBlkCompareFn);
void         RedBlkTreeWalk (tRedBlkTree, tRedBlkWalkFn, void *arg, void *out);
UINT4        RedBlkTreeCount (tRedBlkTree, UINT4 *);
VOID       RedBlkSemTake (tRedBlkTree);
VOID       RedBlkSemGive (tRedBlkTree);


#define    IS_LEFT(p)    ((p) == (p)->parent->child[0])
#define    IS_RIGHT(p)    ((p) == (p)->parent->child[1])
#define    IS_ROOT(p)    ((p)->parent->parent == 0)
#define SIBLING(p)    (((p)->parent->child[0] == (p)) ? ((p)->parent->child[1]) : ((p)->parent->child[0]))
#define    SLANT(p)    ((p) == (p)->parent->child[1])
#define IS_RED(e)    ((e) && (e)->red)
#define IS_BLACK(e)    (((e) == 0) || !((e)->red))
#define IS_BLACK_STRICT(e)    ((e) && !((e)->red))

#define RedBlk_SEM_TAKE(T)\
do{\
if(T->b1MutualExclusive==TRUE)\
{UtilSemTake(T->SemId);}\
}while(0)
#define RedBlk_SEM_GIVE(T)\
do{\
if(T->b1MutualExclusive==TRUE)\
{UtilSemGive(T->SemId);}\
}while(0)
#define RedBlk_SEM_CRT(au1Name,pSemId) UtilSemCrt(au1Name,pSemId)
#define RedBlk_SEM_DEL(semId) UtilSemDel(semId)
#define T_INORDER 0
#define T_PREORDER 1
#define T_POSTORDER 2

#endif
